<html>
 <head>
  <title>#SITE_NAME#: Новогодняя рассылка</title>
   <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
 </head>
 <body style="margin:0px; padding:0px; margin:auto; padding:10px; font-family: Georgia; font-size:12px;">
  <table cellpadding="0" cellspacing="0" width="100%" style="background:#f2f2f2;  padding:10px 40px; padding-top:0px;">
   <tr>
     <td>
       <table align="center" height="148" cellpadding="0" cellspacing="0"  width="860" style="position:relative;height:148px; overflow:hidden; max-height:148px;">
         <tr>
             <td height="148" style=""><img src="http://www.restoran.ru/tpl/images/mail/ny/header.jpg" alt="Новый год с Restoran.ru" /></td>
         </tr>
       </table>
       <table align="center" cellpadding="0" cellspacing="10" height="659" width="860" style="position:relative; background:url(http://www.restoran.ru/tpl/images/mail/ny/main.jpg) left top no-repeat; height:659px; max-height:659px">
         <tr>
           <td height="601" style="padding:0px 20px; vertical-align:top; width:282px; height:601px;">                
				<p style="font-size:26px; color:#FFF; line-height: 34px; font-family: Georgia;"><i>Поздравляем</i><br /> с Новым Годом!</p>
				<p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Желаем вам только приятных новых впечатлений, открытий, счастья 
и удовольствий! Мы обещаем вас радовать, публиковать все самое свежее и интересное!</p>
           </td>
			<td height="601" style="padding:0px 20px; vertical-align:top; width:282px; height:601px;">
				<p style="font-size:26px; color:#FFF; line-height: 34px; font-family: Georgia;">Определились,<br /><i>где встречаете</i> новый год?</p>
				<p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Если еще нет, не паникуйте, мы знаем, в каких ресторанах еще есть места! Поможем определиться в выбором.</p>
				<p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Звоните: +7 (495) 988-26-56</p>
			</td>
             <td height="601" style="padding:0px 10px; vertical-align:top; width:282px; height:601px;">
				<p style="font-size:26px; color:#FFF; line-height: 34px; font-family: Georgia;">Вы уже продумали,<br /><i>меню новогоднего стола?</i></p>
				<p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Мы собрали прекрасную коллекцию новогодних рецептов! </p>
				<ul style="margin:0px;">
				   <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; "><a href="http://www.restoran.ru/content/cookery/editor/tryufeli_s_pertsem/" style="color:#0097d6; text-decoration: none">Трюфели с перцем</a></li>
				   <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; "><a href="http://www.restoran.ru/content/cookery/editor/sushi_iz_kartofelya_i_seledki/" style="color:#0097d6; text-decoration: none">Суши с селедкой</a></li>
				   <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; "><a href="http://www.restoran.ru/content/cookery/editor/kanape_iz_lososya/" style="color:#0097d6; text-decoration: none">Канапе из лосося</a></li>
				   <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; "><a href="http://www.restoran.ru/content/cookery/editor/klyukvennyiy_sous/" style="color:#0097d6; text-decoration: none">Клюквенный соус</a></li>
			   </ul>
				<p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">
				<a href="http://www.restoran.ru/content/cookery/spec/novogodnee_menyu/" style="color:#0097d6; text-decoration: none">и еще 65 отличных блюд</a>
с пошаговыми рецептами 
для вашего праздника!
				</p>
			</td>
        </tr>
		<tr>
			<td align="center"><a href="http://restoran.ru" style="font-size:16px; font-family: Georgia; color:#FFF; text-decoration: underline; font-style:italic">Restoran.ru</a></td>
			<td align="center"><a href="http://www.restoran.ru" style="font-size:16px; font-family: Georgia; color:#FFF; text-decoration: underline; font-style:italic">Подобрать ресторан</a></td>
			<td align="center"><a href="http://www.restoran.ru/content/cookery/spec/novogodnee_menyu/" style="font-size:16px; font-family: Georgia; color:#FFF; text-decoration: underline; font-style:italic">Новогодние рецепты</a></td>
		</tr>
       </table>       
     </td>
   </tr>
  </table>    
 </body>
</html>