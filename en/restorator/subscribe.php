<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Подписки");
?>
<div id="content">
    <div class="left" style="width:728px;">
        <h2>Подписки</h2>
        <?
        $APPLICATION->IncludeComponent(
                "restoran:subscribe.edit_category", "with_period2", Array(
            "AJAX_MODE" => "N",
            "SHOW_HIDDEN" => "N",
            "ALLOW_ANONYMOUS" => "Y",
            "SHOW_AUTH_LINKS" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "SET_TITLE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        
        global $arrFilterTop4;
        $arrFilterTop4["PROPERTY_SUBSCRIBE_USER_ID"] = (int)$USER->GetId();
        $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "subscribe_mails",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "system",
                    "IBLOCK_ID" => 2868,
                    "NEWS_COUNT" => 1,
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "ASC",
                    "FILTER_NAME" => "arrFilterTop4",
                    "FIELD_CODE" => Array("ACTIVE_TO", "ACTIVE_FROM"),
                    "PROPERTY_CODE" => array("SUBSCRIBE_USER_ID"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Купоны",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "kupon_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "PRICE_CODE" => "BASE"
                ),
            false
        );
//        $APPLICATION->IncludeComponent(
//                "restoran:subscribe.edit_category", "", Array(
//            "AJAX_MODE" => "N",
//            "SHOW_HIDDEN" => "N",
//            "ALLOW_ANONYMOUS" => "Y",
//            "SHOW_AUTH_LINKS" => "N",
//            "CACHE_TYPE" => "A",
//            "CACHE_TIME" => "3600",
//            "SET_TITLE" => "N",
//            "AJAX_OPTION_JUMP" => "N",
//            "AJAX_OPTION_STYLE" => "Y",
//            "AJAX_OPTION_HISTORY" => "N"
//                ), false
//        );
        ?>
        <div class="clear"></div>
        <?
//        $APPLICATION->IncludeComponent(
//                "restoran:subscribe.select_category", "with_period", Array(
//            "AJAX_MODE" => "N",
//            "SHOW_HIDDEN" => "N",
//            "ALLOW_ANONYMOUS" => "Y",
//            "SHOW_AUTH_LINKS" => "N",
//            "CACHE_TYPE" => "A",
//            "CACHE_TIME" => "3600",
//            "SET_TITLE" => "N",
//            "AJAX_OPTION_JUMP" => "N",
//            "AJAX_OPTION_STYLE" => "Y",
//            "AJAX_OPTION_HISTORY" => "N"
//                ), false
//        );
        ?>

    <!--<p class="font18">Комментарии к статьям</p>-->
        <? /* $APPLICATION->IncludeComponent(
          "restoran:subscribe.comments_sub_add",
          "",
          Array(
          "IBLOCK_TYPE_ARTICLE" => "afisha",
          "IBLOCK_ID_ARTICLE" => "48",
          "IBLOCK_TYPE_COMMENTS" => "comments",
          "IBLOCK_ID_COMMENTS" => "57",
          "ARTICLE_SECTION_ID" => "",//"130", // задается ID раздела, если статьи в разделах
          "ARTICLE_ELEMENT_ID" => "183179",//"183179", // задается ID элемента, если статьи в элементах (афиша и т.д.)
          "USER_ID" => $USER->GetID()
          ),
          false
          ); */ ?>

        <? /* $APPLICATION->IncludeComponent(
          "restoran:subscribe.user_comments_list",
          "",
          Array(
          "IBLOCK_ID" => array("57", "49", "88"),
          "ITEMS_LIMIT" => "3",
          "USER_ID" => $USER->GetID(),
          "CACHE_TYPE" => "A",
          "CACHE_TIME" => "3600"
          ),
          false
          ); */ ?>
    </div>
    <div class="right" style="width:240px;">
        <div class="baner2">
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_2_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
        </div>
        <br />
        <div class="top_block">Популярное сегодня</div>
        <p class="font18">Обзоры</p>
            <?
            $arOverIB = getArIblock("overviews", CITY_ID);
            $APPLICATION->IncludeComponent(
                    "restoran:catalog.list", "interview_one_with_border", Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "overviews",
                "IBLOCK_ID" => $arOverIB["ID"],
                "NEWS_COUNT" => 1,
                "SORT_BY1" => "shows",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arrFil",
                "FIELD_CODE" => array("CREATED_BY"),
                "PROPERTY_CODE" => array("ratio", "reviews_bind"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
                    ), false
            );
            ?>
        <div class="dotted"></div>
        <p class="font18">Фотоотчеты</p>
        <?
        $arOverIB = getArIblock("interview", CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.list", "interview_one_with_border", Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "interview",
            "IBLOCK_ID" => $arOverIB["ID"],
            "NEWS_COUNT" => 1,
            "SORT_BY1" => "shows",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "",
            "SORT_ORDER2" => "",
            "FILTER_NAME" => "arrFil",
            "FIELD_CODE" => array("CREATED_BY"),
            "PROPERTY_CODE" => array("ratio", "reviews_bind"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "120",
            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        ?>
        <div class="dotted"></div>
        <p class="font18">Мастер-классы</p>
        <?
        $arOverIB = getArIblock("interview", CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.list", "interview_one_with_border", Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "cookery",
            "IBLOCK_ID" => "145",
            "NEWS_COUNT" => 1,
            "SORT_BY1" => "shows",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "",
            "SORT_ORDER2" => "",
            "FILTER_NAME" => "arrFil",
            "FIELD_CODE" => array("CREATED_BY"),
            "PROPERTY_CODE" => array("ratio", "reviews_bind"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "120",
            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        ?>
        <div class="dotted"></div>
        <p class="font18">Фотоотчеты</p>
        <?
        $arOverIB = getArIblock("photoreports", CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.list", "interview_one_with_border", Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "photoreports",
            "IBLOCK_ID" => $arOverIB["ID"],
            "NEWS_COUNT" => 1,
            "SORT_BY1" => "shows",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "",
            "SORT_ORDER2" => "",
            "FILTER_NAME" => "arrFil",
            "FIELD_CODE" => array("CREATED_BY"),
            "PROPERTY_CODE" => array("ratio", "reviews_bind"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "120",
            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        ?>
    </div>
    <div class="clear"></div>
</div>
        <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>