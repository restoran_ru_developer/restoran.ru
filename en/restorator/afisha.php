<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Блоги пользователя");
?>

<?/*$APPLICATION->IncludeComponent("bitrix:news.list", "editor_afisha_list", Array(
	"IBLOCK_TYPE" => "afisha",	// Тип информационного блока (используется только для проверки)
	"IBLOCK_ID" => "48",	// Код информационного блока
	"NEWS_COUNT" => "2",	// Количество новостей на странице
	"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
	"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
	"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
	"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
	"FILTER_NAME" => "",	// Фильтр
	"FIELD_CODE" => array(	// Поля
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(	// Свойства
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
	"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "N",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
	"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
	"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
	"PARENT_SECTION" => "",	// ID раздела
	"PARENT_SECTION_CODE" => "",	// Код раздела
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
	"PAGER_TITLE" => "Новости",	// Название категорий
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "",	// Название шаблона
	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
	"DISPLAY_DATE" => "Y",	// Выводить дату элемента
	"DISPLAY_NAME" => "Y",	// Выводить название элемента
	"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
	"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор,
	"ARTICLE_EDOTOR_PAGE"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/editor/afisha.php"
	),
	false
);*/?>
<?
$arIIIB = getArIblock("afisha", CITY_ID);


$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;



if($_REQUEST["RAZDEL"]!=""){
	global $arrFilter;
	$arrFilter["PROPERTY_cat"]=$_REQUEST["RAZDEL"];
}

//$arrFilter["CREATED_BY"] = (int)$_REQUEST["USER_ID"];

/*if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
	$SECTION=57442;
}else{
	$SECTION=83651;
}*/



if($USER->GetID()==(int)$_REQUEST["USER_ID"] || in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin() || (in_array(23,$GRs)&&CITY_ID=="ast")  || (in_array(24,$GRs)&&CITY_ID=="tmn")  || (in_array(28,$GRs)&&CITY_ID=="kld")){
	$REDACTOR="Y";
}else{
	$REDACTOR="N";
}

if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin()){
	$arrFilter["CREATED_BY"] = (int)$_REQUEST["USER_ID"];
}

if($_REQUEST["act"]=="search_by_name" && $_REQUEST["NAME"]!="") $arrFilter["NAME"]="%".$_REQUEST["NAME"]."%";
?>


<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section_notact",
	"redactor_editor",
	Array(
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "afisha",
		"IBLOCK_ID" => $arIIIB["ID"],
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => "",
		"ELEMENT_SORT_FIELD" => "created_date",
		"ELEMENT_SORT_ORDER" => "DESC",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => array("cat"),
		"PRICE_CODE" => "",
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => "",
		"USE_PRODUCT_QUANTITY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Публикации",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ARTICLE_EDOTOR_PAGE"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/editor/afisha.php",
		"REDACTOR"=>$REDACTOR
	)
);?>

