<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	
     
    <?
    $APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Автор", "TYPE"=>"short_text", "CODE"=>"USER_NICK", "VALUE_FROM"=>"USER__PERSONAL_PROFESSION", "REQUIRED"=>"Y"),
			array("NAME"=>"Comments", "TYPE"=>"hidden", "CODE"=>"PROPERTY_COMMENTS", "VALUE_FROM"=>"PROPERTIES__COMMENTS__VALUE"),
			array("NAME"=>"minus", "TYPE"=>"hidden", "CODE"=>"PROPERTY_minus", "VALUE_FROM"=>"PROPERTIES__minus__VALUE"),
			array("NAME"=>"plus", "TYPE"=>"hidden", "CODE"=>"PROPERTY_plus", "VALUE_FROM"=>"PROPERTIES__plus__VALUE"),
			array("NAME"=>"summa_golosov", "TYPE"=>"hidden", "CODE"=>"PROPERTY_summa_golosov", "VALUE_FROM"=>"PROPERTIES__summa_golosov__VALUE"),
			array("NAME"=>"Ресторан", "TYPE"=>"select", "CODE"=>"PROPERTY_ELEMENT", "VALUE_FROM"=>"PROPERTIES__ELEMENT__VALUE", "VALUES_LIST"=>"PROPS__ELEMENT__LIST", "REQUIRED"=>"Y"),
			array("NAME"=>"Анонс отзыва", "TYPE"=>"vis_red2", "CODE"=>"PREVIEW_TEXT", "VALUE_FROM"=>"PREVIEW_TEXT", "REQUIRED"=>"Y"),
			array("NAME"=>"Показывать на сайте", "TYPE"=>"hidden", "CODE"=>"ACTIVE", "VALUE_FROM"=>"ACTIVE", "VALUES_LIST"=>"PROPS__ACTIVE__LIST"),
			array("NAME"=>"Достоинства", "TYPE"=>"vis_red2", "CODE"=>"PROPERTY_plus", "VALUE_FROM"=>"PROPERTIES__plus__VALUE"),
			array("NAME"=>"Недостатки", "TYPE"=>"vis_red2", "CODE"=>"PROPERTY_minus", "VALUE_FROM"=>"PROPERTIES__minus__VALUE"),
			array("NAME"=>"Рейтинг", "TYPE"=>"stars", "CODE"=>"DETAIL_TEXT", "VALUE_FROM"=>"DETAIL_TEXT"),			
			//array("NAME"=>"Видео", "TYPE"=>"video", "CODE"=>"PROPERTY_video", "VALUE_FROM"=>"PROPERTIES__video__VALUE"),
			array("NAME"=>"Видео c YouTube", "TYPE"=>"video_code", "CODE"=>"PROPERTY_video_youtube", "VALUE_FROM"=>"PROPERTIES__video_youtube__VALUE"),
			array("NAME"=>"Фото", "TYPE"=>"photos", "CODE"=>"photos", "VALUE_FROM"=>"PROPERTIES__photos__VALUE"),
		),
		"TAG_SECTION"=>44203,
		"CAN_ADD_TEXT"=>"Для создания записи заполните модули в основном поле страницы.
Поля со значком * обязательны для заполнения.<br/>
Модули до пунктирной линии - статичны.<br/>
Блоки, размещенные после пунктирной линии, можно добавлять и менять
местами. Для этого наведите курсор на блок и, удерживая
мышку, перетащите в появившийся под/между уже размещенными на странице блоками пунктирный
прямоугольник.<br/>
Дополнительные блоки,  с помощью которых можно дополнить публикацию
фото-, видеоматериалами, высказываниями с фотографией автора (Прямая
речь), находятся в левом поле.",
		"BACK_LINK"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/#response.php",
        "HIDE_TAGS"=>"Y", 
        "ALWAYS_ACTIVE"=>"Y"        
		
	),
false
);

?>      
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>