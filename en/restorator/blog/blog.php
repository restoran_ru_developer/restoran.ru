<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Блоги пользователя");
?>


<?
$arIIIB = getArIblock("blogs",CITY_ID);
$arIIIB2 = getArIblock2("blogs");

$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;



if($_REQUEST["RAZDEL"]!=""){
	global $arrFilter;
	$arrFilter["PROPERTY_cat"]=$_REQUEST["RAZDEL"];
}

$arrFilter["ACTIVE"]=array("Y","N");

//$arrFilter["CREATED_BY"] = (int)$_REQUEST["USER_ID"];

/*if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
	$SECTION=57442;
}else{
	$SECTION=83651;
}*/



if($USER->GetID()==(int)$_REQUEST["USER_ID"] || in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()|| (in_array(23,$GRs)&&CITY_ID=="ast")){
	$REDACTOR="Y";
}else{
	$REDACTOR="N";
}

if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin()){
	$arrFilter["CREATED_BY"] = (int)$_REQUEST["USER_ID"];
}

if($_REQUEST["act"]=="search_by_name" && $_REQUEST["NAME"]!="") $arrFilter["NAME"]="%".$_REQUEST["NAME"]."%";

//var_dump($arIIIB2);
?>


<?$APPLICATION->IncludeComponent(
	"restoran:catalog.list_multi",
	"redactor_editor",
	Array(
		            "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "blogs",
                    "IBLOCK_ID" => $arIIIB2["ID"],
                    "NEWS_COUNT" => "30",
                    "SORT_BY1" => "created_date",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => array("ACTIVE","ACTIVE_TO","ACTIVE_FROM","DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "43200",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
            "DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Публикации",
		"ARTICLE_EDOTOR_PAGE"=>"/restorator/blog/editor/blog.php",
		"REDACTOR"=>$REDACTOR,
		"ADD2CITY"=>$arIIIB["ID"]
	)
);?>
