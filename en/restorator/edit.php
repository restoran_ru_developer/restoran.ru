<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Редактирование ресторана");
?>

<?
if (CSite::InGroup( array(14,15,1,20,9))):
$APPLICATION->AddHeadString('<link href="/tpl/css/chosen/chosen.css"  type="text/css" rel="stylesheet" />', true);
$APPLICATION->AddHeadScript('/tpl/css/chosen/chosen.jquery.js');
$APPLICATION->AddHeadScript('/tpl/js/jquery.checkbox_2_1.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/promise.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/ajax.requester.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/deletefile.ajax.requester.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/window.receive.message.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/paste.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/jquery-plugin.js');

$APPLICATION->AddHeadString('<link href="/tpl/js/fu/fineuploader.css" rel="stylesheet" type="text/css"/>');  
?>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<div id="content">
    <?if ($_REQUEST["ID"]):?>    
	 <?$APPLICATION->IncludeComponent(
        "restoran:restoran.edit_form_new",
        "",
        Array(
            "REST_ID" => $_REQUEST["ID"],
        ),
        false
    );?>
	
	<div class="clear"></div>
    <?else:?>
        <?ShowError("Do not choose a restaurant")?>
    <?endif;?>
</div>
<?else:?>
    <div id="content">
        <?=ShowError("No access");?>
    </div>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>