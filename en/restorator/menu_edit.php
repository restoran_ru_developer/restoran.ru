<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Меню ресторана");
?>
    <style>
        .btn {
            display: inline-block;
            *display: inline;
            padding: 4px 12px;
            margin-bottom: 0;
            *margin-left: .3em;
            font-size: 14px;
            line-height: 20px;
            color: #333333;
            text-align: center;
            text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
            vertical-align: middle;
            cursor: pointer;
            background-color: #f5f5f5;
            *background-color: #e6e6e6;
            background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
            background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
            background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
            background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
            background-repeat: repeat-x;
            border: 1px solid #cccccc;
            *border: 0;
            border-color: #e6e6e6 #e6e6e6 #bfbfbf;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            border-bottom-color: #b3b3b3;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            *zoom: 1;
            -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
            font-style:normal;
            text-decoration:none;
        }
        .btn-info {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            *background-color: #2f96b4;
            background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));
            background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: -o-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            background-repeat: repeat-x;
            border-color: #2f96b4 #2f96b4 #1f6377;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            text-decoration:none;
        }
        .btn-info:hover
        {
            color: #ffffff;
            background-color: #2f96b4;
            *background-color: #2a85a0;
        }
    </style>
    <div id="content">
        <?
        if (CSite::InGroup( array(14,15,1,20,9))):
            $APPLICATION->AddHeadScript('/tpl/js/jQuery.fileinput.js');
            $APPLICATION->AddHeadScript('/tpl/js/jquery.form.js');
            $APPLICATION->AddHeadScript('/tpl/js/chosen.jquery.js');
            ?>
            <script type="text/javascript">
                $(".add_new_section").live("click",function(){
                    var lnk = $(this).attr("href");
                    $.post(lnk, function(html){

                        if (!$("#rzdl_modal").size()){
                            $("<div class='popup popup_modal' id='rzdl_modal'></div>").appendTo("body");
                        }

                        $('#rzdl_modal').html(html);

                        setCenter($("#rzdl_modal"));
                        $("#rzdl_modal").fadeIn("300");

                        updt_frm_rzd();

                    });

                    return false;
                });
                function updt_frm_rzd(){

                    //проверка формы
                    function check_editor_form2(a,f,o){
                        var ret=true;
                        o.dataType = "html";
                        return ret;
                    }

                    //Отправка формы
                    $('#rzdl_modal form').ajaxForm({
                        beforeSubmit: check_editor_form2,
                        success: function(data) {
                            console.log(data);

                            window.location.reload();

                        }
                    });
                }

                $(".menu_edit .del_section").live("click",function(){
                    if (confirm("Вы точно хотите удалить этот раздел?"))
                    {
                        var lnk = $(this).attr("href");
                        $.post(lnk, function(html){
                            window.location.reload();
                        });
                    }
                    return false;
                });


                $(".menu_edit .edite_section").live("click",function(){

                    var lnk = $(this).attr("href");
                    $.post(lnk, function(html){

                        if (!$("#rzdl_modal").size()){
                            $("<div class='popup popup_modal' id='rzdl_modal'></div>").appendTo("body");
                        }

                        $('#rzdl_modal').html(html);

                        setCenter($("#rzdl_modal"));
                        $("#rzdl_modal").fadeIn("300");

                        updt_frm_rzd();

                    });
                    return false;
                });
                $(document).ready(function(){
                    $(".popup_close").live('click',function(){
                        $(".popup_modal").hide();
                    });

                    $("#from_csv").click(function() {
                        var lnk = "/tpl/ajax/csv.php?ELEMENT=<?=$_REQUEST["RESTORAN"]?>&<?=bitrix_sessid_get()?>";
                        $.post(lnk, function(html){
                            if (!$("#csv_modal").size()){
                                $("<div class='popup popup_modal' id='csv_modal'></div>").appendTo("body");
                            }

                            $('#csv_modal').html(html);

                            setCenter($("#csv_modal"));
                            $("#csv_modal").fadeIn("300");
                        });

                    });
                });
            </script>
        <?
        if($_REQUEST["RESTORAN"]>0)
        {
        $res = CIBlockElement::GetByID($_REQUEST["RESTORAN"]);
        if($ar_res = $res->GetNext());
        ?>
            <h2>Меню ресторана <?=$ar_res["NAME"]?></h2>
            <div style="position:absolute;top:0px; right:180px; width:395px">
                <!--                        <a class="light_button" style="width:200px; float:left; line-height:22px; height:22px; font-size:11px; position: relative; padding:0px 10px" href="/bitrix/templates/main/components/bitrix/catalog/dostavka_redactor/bitrix/catalog.section.list/.default/core.php?act=new_rzdel&SECTION_ID=<?=$arResult["SECTION"]["ID"]?>&ELEMENT_ID=<?=$_REQUEST["RESTORAN"]?>" class="add_new_section">Добавить новый раздел</a>                                                -->
            </div>
        <?//if ($_REQUEST["SECTION_ID"]):?>
            <div style="">
                <!--                        <a class="light_button add_new_section" style="width:200px; float:left; line-height:22px; height:22px; font-size:11px; position: relative; padding:0px 10px" href="/bitrix/templates/main/components/bitrix/catalog/dostavka_redactor/bitrix/catalog.section.list/.default/core.php?act=new_rzdel&SECTION_ID=<?=$arResult["SECTION"]["ID"]?>&ELEMENT_ID=<?=$_REQUEST["RESTORAN"]?>">Добавить новый раздел</a>-->


            </div>
        <?//endif;?>
        <?

        global $DB;
        $sql = "SELECT ID FROM b_iblock WHERE DESCRIPTION=".$DB->ForSql($ar_res["ID"]);
        $db_list = $DB->Query($sql);
        if($ar_result = $db_list->GetNext())
        {
            $sql = "SELECT ID FROM b_iblock_section WHERE IBLOCK_ID=".$DB->ForSql($ar_result["ID"])." ORDER BY NAME ASC";
            $db_list = $DB->Query($sql);
            $ar = $db_list->GetNext();
            $menu = $ar_result['ID'];
            if (!$_REQUEST["SECTION_ID"])
            {
                //$_REQUEST["SECTION_ID"] = $ar["ID"];
            }
            else
            {
                $_REQUEST["PARENT_SECTION_ID"] = false;
            }
            $APPLICATION->IncludeComponent(
                "bitrix:catalog",
                "dostavka_redactor",
                Array(
                    "AJAX_MODE" => "N",
                    "SEF_MODE" => "N",
                    "IBLOCK_TYPE" => "rest_menu_ru",
                    "IBLOCK_ID" => $menu,
                    "USE_FILTER" => "N",
                    "USE_REVIEW" => "N",
                    "USE_COMPARE" => "N",
                    "SHOW_TOP_ELEMENTS" => "N",
                    "PAGE_ELEMENT_COUNT" => "999",
                    "LINE_ELEMENT_COUNT" => "3",
                    "ELEMENT_SORT_FIELD" => "id",
                    "ELEMENT_SORT_ORDER" => "desc",
                    "LIST_PROPERTY_CODE" => array("map"),
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LIST_META_KEYWORDS" => "-",
                    "LIST_META_DESCRIPTION" => "-",
                    "LIST_BROWSER_TITLE" => "-",
                    "DETAIL_PROPERTY_CODE" => array("map"),
                    "DETAIL_META_KEYWORDS" => "map",
                    "DETAIL_META_DESCRIPTION" => "map",
                    "DETAIL_BROWSER_TITLE" => "map",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "PRICE_CODE" => array("BASE"),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "LINK_IBLOCK_TYPE" => "",
                    "LINK_IBLOCK_ID" => "",
                    "LINK_PROPERTY_SID" => "",
                    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                    "USE_ALSO_BUY" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "TOP_ELEMENT_COUNT" => "9",
                    "TOP_LINE_ELEMENT_COUNT" => "3",
                    "TOP_ELEMENT_SORT_FIELD" => "sort",
                    "TOP_ELEMENT_SORT_ORDER" => "asc",
                    "TOP_PROPERTY_CODE" => array("map"),
                    "VARIABLE_ALIASES" => Array(
                        "SECTION_ID" => "SECTION_ID",
                        "ELEMENT_ID" => "ELEMENT_ID"
                    ),
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "Y",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => ""
                ),
                false
            );

        }
        else
        {
        $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/components/bitrix/catalog/dostavka_redactor/style.css"  type="text/css" rel="stylesheet" />',true);
        echo "<h3><i>Для начала, необходимо добавить новый раздел вашего меню</i></h3>";
        ?>
            <script>
                $(document).ready(function(){
                    $(".popup_close").live("click", function(){
                        $("#bludo_modal").fadeOut("100");
                        $("#rzdl_modal").fadeOut("100");
                        hideOverflow();
                        return false;
                    });
                });
            </script>
            <div align="center">
                <a href="/bitrix/templates/main/components/bitrix/catalog/dostavka_redactor/bitrix/catalog.section.list/.default/core.php?act=new_rzdel&SECTION_ID=<?=$arResult["SECTION"]["ID"]?>&ELEMENT_ID=<?=$_REQUEST["RESTORAN"]?>" class="btn add_new_section">+ Добавить новый раздел</a>
            </div>
        <?
        }
        }
            echo "";
        else:?>
            <?=ShowError("Нет доступа");?>
        <?endif;?>
        <br /><br /><br /><br />
        <hr />
        <?/*$APPLICATION->IncludeComponent(
                "bitrix:form.result.new",
                "menu",
                Array(
                        "SEF_MODE" => "N",
                        "WEB_FORM_ID" => 20,
                        "LIST_URL" => "",
                        "EDIT_URL" => "",
                        "SUCCESS_URL" => "",
                        "CHAIN_ITEM_TEXT" => "",
                        "CHAIN_ITEM_LINK" => "",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                        "USE_EXTENDED_ERRORS" => "N",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "3600",
                        "VARIABLE_ALIASES" => Array(
                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                "RESULT_ID" => "RESULT_ID"
                        )
                ),
        false
        );*/?>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>