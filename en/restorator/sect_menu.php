    <div class="block" >
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "restorator",
            Array(
                "ROOT_MENU_TYPE" => "restorator",
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600000000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(CITY_ID)
            ),
        false
        );?>
        <div class="menu-horizontal-long">
            <ul>
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "users",
                Array(
                    "ROOT_MENU_TYPE" => "personal",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(CITY_ID)
                ),
            false
            );?>
            </ul>
        </div>
    </div>