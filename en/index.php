<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "The catalog of restaurants of Moscow. Reserve a table and banquet. Search and selection of restaurants. Master class on cooking. Beauty parlors and saunas Moscow.");
$APPLICATION->SetPageProperty("keywords", "restaurants in Moscow Petersburg news restaurants in Moscow catalog restaurants cooking recipes news menu menu cafe table reservation banquet beauty salon sauna dish");
$APPLICATION->SetPageProperty("title", "Restoran.ru - all restaurants in Moscow and St. Petersburg, new restaurants, cafes Moscow. Restaurant rating. Tables reservation Restoran.ru");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Restoran.ru");
// get restaurants iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
$arIndexIB = getArIblock("index_page", CITY_ID);
// add script for tab switcher
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/index_page.js");
//FirePHP::getInstance()->info($APPLICATION->GetCurPageParam());
?>
<?//if($USER->IsAdmin()):?>
    <?

    if (!$_REQUEST["page"])
    {
        $_REQUEST["page"] = 1;
        $_REQUEST["PAGEN_1"] = 1;
    }
//    else {
//        $_REQUEST["PAGEN_1"] = $_REQUEST['page'];
//    }

?>
    <div class="block">
        <h2 class="like-h1-for-index h1_style"><?=$APPLICATION->ShowTitle("h1")?></h2>
        <div class="sort">
            <?
            // set rest sort links
            $excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1","by","index_php?page","index_php");
            $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=new&".(($_REQUEST["pageRestSort"]=="new"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
            $sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=price&".(($_REQUEST["pageRestSort"]=="price"&&$_REQUEST["by"]=="asc") ? "by=desc": "by=asc"), $excUrlParams);
            //        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=ratio&".(($_REQUEST["pageRestSort"]=="ratio"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
            $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=popular&".(($_REQUEST["pageRestSort"]=="popular"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
            $sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=alphabet&".((($_REQUEST["pageRestSort"]=="alphabet"&&$_REQUEST["by"]=="asc")|| !$_REQUEST["pageRestSort"]) ? "by=desc": "by=asc"), $excUrlParams);
            ?>
            <?$by = ($_REQUEST["by"]=="desc")?"asc":"desc";?>
            <?
            if ($_REQUEST["pageRestSort"] == "new")
            {
                echo '<a class="'.$by.'" href="'.$sortNewPage.'">by newest</a>';
            }
            else
            {
                echo '<a href="'.$sortNewPage.'">by newest</a>';
            }
            if ($_REQUEST["pageRestSort"] == "price")
            {
                echo '<a class="'.$by.'" href="'.$sortPricePage.'">by average check</a>';
            }
            else
            {
                echo '<a href="'.$sortPricePage.'">by average check</a>';
            }
            //            if ($_REQUEST["pageRestSort"] == "ratio")
            if ($_REQUEST["pageRestSort"] == "popular")
            {
                echo '<a class="'.$by.'" href="'.$sortRatioPage.'">by rating</a>';
            }
            else
            {
                echo '<a href="'.$sortRatioPage.'">by rating</a>';
            }
            if ($_REQUEST["pageRestSort"] == "alphabet" || !$_REQUEST["pageRestSort"])
            {
                echo '<a class="'.$by.'" href="'.$sortAlphabetPage.'">by name</a>';
            }
            else
            {
                echo '<a href="'.$sortAlphabetPage.'">by name</a>';
            }
            ?>
        </div>
        <div class="clearfix"></div>
        <?

        $APPLICATION->IncludeComponent("restoran:restoraunts.list_optimized", "rest_list_full_size", Array(
            "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => CITY_ID,	// Код информационного блока
            "PARENT_SECTION_CODE" => 'restaurants',	// Код раздела
            "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
            "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
            "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arrFilter",	// Фильтр
            "PROPERTY_CODE" => array(	// Свойства
                0 => "type",
                1 => "kitchen",
                2 => "average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "ratio",
                8 => "sale10",
            ),
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//y	// Тип кеширования
            "CACHE_TIME" => "86412",	// Время кеширования (сек.)
            "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "CACHE_NOTES" => "new",
            "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "PAGER_TITLE" => "Рестораны",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36002",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        ),
            false
        );
        ?>
        <div class="navigation"></div>
        <script>
            $(function(){
                $(".navigation").html($(".navigation_temp").html());
                $(".navigation_temp").remove();
            })
        </script>
        <div id ="for_seo"></div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                "TYPE" => "bottom_rest_list",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
<?//else:

if(2==1):?>
    <div class="content">
        <div class="block">
            <div class="left-side">
                <?
                if($arIndexIB["ID"]):
                    global $arrFirstBlock;
                    $arrFirstBlock["SECTION_CODE"] = "first";

                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "index_block_scrollable",
                        Array(
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "index_page",
                            "IBLOCK_ID" => $arIndexIB["ID"],
                            "NEWS_COUNT" => "999",
                            "SORT_BY1" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "arrFirstBlock",
                            "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                            "PROPERTY_CODE" => array(
                                0  => "ELEMENTS",
                                1  => "IBLOCK_ID",
                                2  => "IBLOCK_TYPE",
                                3  => "SECTION",
                                4  => "COUNT",
                                5  => "LINK",
                            ),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "j F Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "14400",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "CACHE_NOTES" => "n",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "TABS"=>"Y"
                        ),
                        false
                    );
                endif;
                ?>
            </div>
            <div class="right-side">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                        "TYPE" => "right_index_1",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
        </div>
        <?
        if($arIndexIB["ID"]):
            global $arrFirstBlock;
            $arrFirstBlock["SECTION_CODE"] = "first";

            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "index_block_scrollable",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFirstBlock",
                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "14402",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_NOTES" => "n",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                ),
                false
            );
        endif;?>
    </div>
    <div class="block">
        <div class="left-side">
            <?
            if($arIndexIB["ID"]):
                global $arrOthersBlock;
                $arrOthersBlock["!SECTION_CODE"] = "first";

                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    //($USER->IsAdmin())?"index_block_new":"index_block",
                    "index_block_new",
                    Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "index_page",
                        "IBLOCK_ID" => $arIndexIB["ID"],
                        "NEWS_COUNT" => "999",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrOthersBlock",
                        "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                        "PROPERTY_CODE" => array(
                            0  => "ELEMENTS",
                            1  => "IBLOCK_ID",
                            2  => "IBLOCK_TYPE",
                            3  => "SECTION",
                            4  => "COUNT",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "14409",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",

                    ),
                    false
                );
            endif;?>
        </div>
        <div class="right-side">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "page",
                    "AREA_FILE_SUFFIX" => "inc_right_block",
                    "EDIT_TEMPLATE" => ""
                ),
                false
            );?>
            <br /><br />
            <?  require_once $_SERVER["DOCUMENT_ROOT"].'/index_inc_right_block_vote.php';?>
            <?/*$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "inc_right_block_vote",
                "EDIT_TEMPLATE" => ""
            ),
        false
        );*/?>
        </div>
        <div class="clearfix"></div>
        <div class="i_b_r">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    </div>
<?endif;?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>