<?if (substr_count($APPLICATION->GetCurDir(),"/auth/")==0):?>
<div class="left">
    <?if ($USER->IsAuthorized()):?>
        <a href="/users/id<?=$USER->GetID()?>/rest_edit/" class="font18 add_block_blue">+ Добавить ресторан</a>
    <?else:?>
        <a href="/auth/" class="font18 add_block_blue">+ Добавить ресторан</a>
    <?endif;?>
</div>
<?endif;?>
<div class="right ajax_forms_menu">
	<div class="right" id="call_me" style="z-index:100000">
		<a href="<?=SITE_TEMPLATE_PATH?>/ajax/call_me.php" class="ajax font18" onclick="show_popup(this,{'url':true,'css':{'top':'-12px','right':'-15px'}});"><?=GetMessage("CALL_ME_LINK")?></a>
	</div>
	<div class="right" id="order_online">
		<a href="<?=SITE_TEMPLATE_PATH?>/ajax/online_order.php" class="ajax font18" onclick="show_popup(this,{'url':true,'css':{'width':'400px','top':'-12px','right':'-15px'}});"><?=GetMessage("ORDER_ONLINE_LINK")?></a>
	</div>            
	<div class="clear"></div>
</div>