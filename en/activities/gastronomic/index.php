<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Московский гастрономический фестиваль");
?>
<!--<script src="javascripts/components/handlebars.js/handlebars.runtime-1.0.0-rc.1.js"></script>
  <script src="javascripts/templates/photo-list.tmpl.js"></script>
  <script src='javascripts/app.js' type='text/javascript' charset='utf-8'></script>-->
  <link rel='stylesheet' href='styles.css' type='text/css' media='screen'>
  <div id="content">
    <div style="width:980px; margin-top:30px;">
        <div class="left" style="width:345px; text-center: left;">
            Призываем фотографировать и выкладывать 
в &nbsp;<img src="images/gn_inst.png" align="middle" />&nbsp;&nbsp; потрясающие блюда Гастрономического Фестиваля.  
Делитесь своими впечатлениями!

        </div>
        <div class="left" style="width:300px; text-align: center;">
            <noindex><a href="http://www.gastronomic.ru/" target="_blank"><img src="images/gn_logo.png"></a></noindex>
        </div>
        <div class="right" style="width:320px;">
            Ставьте тег  <span class="red">&nbsp;#mgf&nbsp;</span>
Призывайте друзей ставить лайки.
Кто наберет больше всех —
получит специальный приз от Ресторан.ру.
        </div>
        <div class="clear"></div>
<!--        <div class="loading">
            <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01">
            </div>
            <div class="f_circleG" id="frotateG_02">
            </div>
            <div class="f_circleG" id="frotateG_03">
            </div>
            <div class="f_circleG" id="frotateG_04">
            </div>
            <div class="f_circleG" id="frotateG_05">
            </div>
            <div class="f_circleG" id="frotateG_06">
            </div>
            <div class="f_circleG" id="frotateG_07">
            </div>
            <div class="f_circleG" id="frotateG_08">
            </div>
            </div>
            </div>
            <div id='photos-wrap'>                
            </div>
            <div class='paginate'>
            <a class='button more'  style='display:none;' data-max-tag-id='' href='#'>Показать еще...</a>
            </div>        
        <div class="clear"></div>-->
<h2>Дорогие гурме-фотографы!</h2>

    <p align="center">Спасибо за участие в конкурсе, спасибо, что ходили по ресторанам,
    дегустрировали, хорошо проводили время и делились впечатлениями в
    Инстаграмме.</p>

    <br /><br />

    <p><b>Мы рады объявить победителей:</b></p>

    

    <p><span class="red">&nbsp;1 место&nbsp;</span> - shtierlitz, 30 лайков</p>

    <p><span class="red">&nbsp;2 место&nbsp;</span> - vladimir_osipchik, 28 лайков</p>

    <p><span class="red">&nbsp;3 место&nbsp;</span> - aisilulut, 25 лайков</p>

    
<br /><Br />
    <p align="center"><b>Победители, свяжитесь с нами по телефону +7(495)745-0566</b></p>
    <br /><br />
    <br /><br />
    </div>    
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>