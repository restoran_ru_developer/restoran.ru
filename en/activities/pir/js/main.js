$(document).ready(function(){
        $('.slider1').bxSlider({
            slideWidth: 240,
            minSlides: 1,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 1,
            pager: false

          });
        $('.slider2').bxSlider({
            slideWidth: 240,
            minSlides: 4,
            maxSlides: 4,
            slideMargin: 1,
            moveSlides: 1,
            pager: false
          });                   
    $(".menu a").click(function(e){
        e.preventDefault();
        var pos=$($(this).attr("href")).offset().top-10;
        $('html,body').animate({scrollTop:pos},"300");
        return false; 
    });
});