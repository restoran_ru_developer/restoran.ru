<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("VK");
?>
<div id="content">
    <h1>Комментирование Вконтакте</h1>
    <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>

    <div id="vk_comments"></div>
    <script type="text/javascript">
    window.onload = function () {
    VK.init({apiId: <?=(CITY_ID=="tmn")?"3559173":"2881483"?>, onlyWidgets: true});
    VK.Widgets.CommentsBrowse('vk_comments', {width: 980, limit: 30, mini: 0});
    }
    </script>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>