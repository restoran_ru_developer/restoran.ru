<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
CModule::IncludeModule("iblock");
if ($_REQUEST["city"]):
$arRestIB = getArIblock("catalog", $_REQUEST["city"]);
$obCache = new CPHPCache; 
$life_time = 60*60*24*7; 
$cache_id = $_REQUEST["city"]; 

if($obCache->InitCache($life_time, $cache_id, "/"))
{
    $vars = $obCache->GetVars();
    $ITEMS = $vars["ITEMS"];
}
elseif( $obCache->StartDataCache())
{

    $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>$arRestIB["ID"], "ACTIVE"=>"Y"),false,false,Array("ID","NAME","DETAIL_PAGE_URL"));
    while($ar = $res->GetNext())
    {
        $ITEMS[] = $ar;
    }    
}
?>

<h1><?=$arRestIB["NAME"]?></h1>
<table colspan="15" width="1000" border="1">
    <tr>
        <th>Название</th>
        <th>Ссылка</th>
    </tr>
    <?foreach($ITEMS as $rest):?>
        <tr>
            <td style="word-wrap:break-word"><?=$rest["NAME"]?></td>
            <td style="word-wrap:break-word"><a href="http://restoran.ru<?=$rest["DETAIL_PAGE_URL"]?>" target="_blank" />http://restoran.ru<?=$rest["DETAIL_PAGE_URL"]?></a></td>
        </tr>
    <?endforeach;?>
        <tr>
            <td align="right">Всего:</td>
            <td><?=count($ITEMS)?></td>
        </tr>
</table>
<?endif;?>