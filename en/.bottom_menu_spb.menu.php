<?
$aMenuLinks = Array(
	Array(
		"Рестораны", 
		"/spb/catalog/restaurants/all", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Афиша", 
		"/spb/afisha/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Отзывы", 
		"/spb/opinions/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Рецепты", 
		"/content/cookery/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Контакты", 
		"/spb/contacts/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Банкетные залы", 
		"/spb/catalog/banket/all", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Новости", 
		"/spb/news/restoransnewsspb/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Блоги", 
		"/spb/blogs/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Мастер-классы", 
		"/spb/news/mcfromchif/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Реклама на сайте", 
		"/spb/articles/reklama/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Фирмы", 
		"/spb/catalog/firms/all/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Обзоры", 
		"/spb/news/restvew/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Пользователи", 
		"/users/list/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Готовим сами", 
		"/content/cookery/users/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Свадьбы", 
		"/spb/articles/wedding/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Критика", 
		"/spb/blogs/65/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Рейтинги", 
		"/spb/ratings/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Советы", 
		"/content/cookery/sovety/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Вход", 
		"/auth/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"FAQ", 
		"/content/articles/help/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Фотоотчеты", 
		"/spb/photos/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Интервью", 
		"/spb/news/pryamayarech/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Диеты", 
		"/content/cookery/diety/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Регистрация", 
		"/auth/register.php", 
		Array(), 
		Array(), 
		"" 
	)
);
?>