<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//Для Москвы
$ar = Array("Авиамоторная","Автозаводская","Академическая","Александровский сад","Алексеевская","Алтуфьево","Аннино","Арбатская (Арбатско-Покровская линия)","Арбатская (Филевская линия)","Аэропорт","Бабушкинская","Багратионовская","Баррикадная","Бауманская","Беговая","Белорусская","Беляево","Бибирево","Библиотека имени Ленина","Битцевский парк","Боровицкая","Ботанический сад","Братиславская","Бульвар адмирала Ушакова","Бульвар Дмитрия Донского","Бунинская аллея","Варшавская","ВДНХ","Владыкино","Водный стадион","Войковская","Волгоградский проспект","Волжская","Волоколамская","Воробьевы горы","Выставочная","Выхино","Деловой центр","Динамо","Дмитровская","Добрынинская","Домодедовская","Достоевская","Дубровка","Жулебино","Измайловская","Калужская","Кантемировская","Каховская","Каширская","Киевская","Китай-город","Кожуховская","Коломенская","Комсомольская","Коньково","Красногвардейская","Краснопресненская","Красносельская","Красные ворота","Крестьянская застава","Кропоткинская","Крылатское","Кузнецкий мост","Кузьминки","Кунцевская","Курская","Кутузовская","Ленинский проспект","Лубянка","Люблино","Марксистская","Марьина роща","Марьино","Маяковская","Медведково","Международная","Менделеевская","Митино","Молодежная","Мякинино","Нагатинская","Нагорная","Нахимовский проспект","Новогиреево","Новокузнецкая","Новослободская","Новоясеневская","Новые Черемушки","Октябрьская","Октябрьское поле","Орехово","Отрадное","Охотныйряд","Павелецкая","Парк культуры","Парк Победы","Партизанская","Первомайская","Перово","Петровско-Разумовская","Печатники","Пионерская","Планерная","Площадь Ильича","Площадь Революции","Полежаевская","Полянка","Пражская","Преображенская площадь","Пролетарская","Проспект Вернадского","Проспект Мира","Профсоюзная","Пушкинская","Речной вокзал","Рижская","Римская","Рязанский проспект","Савеловская","Свиблово","Севастопольская","Семеновская","Серпуховская","Славянский бульвар","Смоленская (Арбатско-Покровская линия)","Смоленская (Филевская линия)","Сокол","Сокольники","Спортивная","Сретенский бульвар","Строгино","Студенческая","Сухаревская","Сходненская","Таганская","Тверская","Театральная","Текстильщики","ТеплыйСтан","Тимирязевская","Третьяковская","Трубная","Тульская","Тургеневская","Тушинская","Улица 1905года","Улица Академика Янгеля","Улица Горчакова","Улица Подбельского","Улица Скобелевская","Улица Старокачаловская","Университет","Филевский парк","Фили","Фрунзенская","Царицыно","Цветной бульвар","Черкизовская","Чертановская","Чеховская","Чистые пруды","Чкаловская","Шаболовская","Шоссе Энтузиастов","Щелковская","Щукинская","Электрозаводская","Юго-Западная","Южная","Ясенево");
CModule::IncludeModule("iblock");
/*$ibpenum = new CIBlockPropertyEnum;
foreach ($ar as $a)
{
    if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>54, 'VALUE'=>$a)))
        echo 'New ID:'.$PropID;
    else
        echo $ibpenum->LAST_ERROR;
}*/
$el = new CIBlockElement;
foreach ($ar as $a)
{
    $arLoadProductArray = Array(  
        "MODIFIED_BY"    => $USER->GetID(), 
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID"      => 30,  
        "NAME" => $a,  
        "ACTIVE" => "Y"
    );
    if($PRODUCT_ID = $el->Add($arLoadProductArray))  
        echo "New ID: ".$PRODUCT_ID;
    else  echo "Error: ".$el->LAST_ERROR;
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>