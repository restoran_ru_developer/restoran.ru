<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Invites");
// get resume iblock info
$arResumeIB = getArIblock2("invites");
global $USER;
?>
<!--<script src="/bitrix/components/restoran/ajax.invite2restoran/templates/.default/script.js"></script>-->
<script src="/bitrix/components/restoran/ajax.invite2restoran/templates/.default/chosen.jquery.js"></script>

<script>
/** from old fight**/

function favorite_success(data)
{
    //data = eval('(' + data + ')');
    if (!$("#favorite_form").size())
    {
        $("<div class='popup popup_modal' id='favorite_form' style='width:335px'></div>").appendTo("body");
    }
    $('#favorite_form').html(data);
    showOverflow();
    setCenter($("#favorite_form"));
    $("#favorite_form").fadeIn("300");
    if (!data.ERROR)
    {
        if (data.MESSAGE)
            $("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.MESSAGE);
    }
    else
        $("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.ERROR);
}



/** from old fight**/


function add_invite(id) {
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/invite2rest.php",
        data: "ID=" + id + "&<?=bitrix_sessid_get()?>",
        success: function (data) {
//            console.log(data);
            if (!$("#invite2rest_modal").size()) {
                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");
            }
            $('#invite2rest_modal').html(data);
            showOverflow();
            setCenter($("#invite2rest_modal"));
            $("#invite2rest_modal").css("top", "150px")
            $("#invite2rest_modal").fadeIn("300");



            $(".datew").datepicker({format: 'dd.mm.yyyy', language: 'ru'});
            $.maski.definitions['~']='[0-2]';
            $.maski.definitions['!']='[0-5]';
            $(".time").maski("~9   !9",{placeholder:" "});
            $("#choose_user").data("placeholder","Выберите пользователя...").chosen();

            $("#top_search_input2").autocomplete("/search/rest_bron.php", {
                limit: 5,
                minChars: 3,
                formatItem: function(data, i, n, value) {
                    return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
                },
                formatResult: function(data, value) {
                    return value.split("###")[0];
                },
                onItemSelect: function(value) {

                    var a = value.split("###")[1];
                    $("#top_search_rest2").attr("value",a);
                },
                extraParams: {
                    //search_in: function() { return $('#search_in').val(); },
                }
            });

            $('#order_online form').on('submit', function(){

                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: form.attr("action"),
                    dataType: "json",
                    data: form.serialize(),
                    success: function(data) {
                        $('.ok').html(data.MESSAGE);
                        $('.ok').fadeIn(500);
                        if (data.STATUS==1)
                            $("#ivite_button").attr("disabled",true);
                        if (data.RELOAD && data.RELOAD=="2")
                        {
                            location.reload();
                        }
                        if (data.RELOAD && data.RELOAD=="1")
                            location.reload();
                    }
                });
                return false;
            });
            $(".ajax_form > form").bind("onFail", function (e, errors) {
                if (e.originalEvent.type == 'submit') {
                    $.each(errors, function () {
                        var input = this.input;
                    });
                }
            });
        }
    });
}
function delete_invite(id)
{
    if (confirm("Are you sure you want to cancel this invitation?"))
    {
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=delete&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); 
                $('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });
    }
    else
    {
        return false;
    }
}
function del_invite(id)
{
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=delete&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                /*if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); */
                $('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });    
}

function confirm_invite(id,element)
{
    $.ajax({
        type: "POST",
        url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
        data: "ACTION=confirm&ELEMENT="+element+"&ID="+id+"&<?=bitrix_sessid_get()?>",
        success: function(data) {
            if (!$("#invite2rest_modal").size())
            {
                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
            }
            $('#invite2rest_modal').html(data);
            showOverflow();
            setCenter($("#invite2rest_modal"));
            $("#invite2rest_modal").css("top","150px")
            $("#invite2rest_modal").fadeIn("300"); 
            $('#inv'+id).hide(500);        
            setTimeout("location.reload()",1000);
        }
    });
}

function declain_invite(id,element)
{
    if (confirm("Are you sure you want to cancel this invitation?"))
    {
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=declain&ELEMENT="+element+"&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); 
                $('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });
    }
    else
        return false;
}
function decl_invite(id,element)
{
    $.ajax({
        type: "POST",
        url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
        data: "ACTION=declain&ac=del&ELEMENT="+element+"&ID="+id+"&<?=bitrix_sessid_get()?>",
        success: function(data) {
            /*if (!$("#invite2rest_modal").size())
            {
                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
            }
            $('#invite2rest_modal').html(data);
            showOverflow();
            setCenter($("#invite2rest_modal"));
            $("#invite2rest_modal").css("top","150px")
            $("#invite2rest_modal").fadeIn("300"); */
            $('#inv'+id).hide(500);   
            setTimeout("location.reload()",1000);
        }
    });
}
function del_user(id,element)
{
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=del_user&ac=del&ELEMENT="+element+"&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                /*if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); */
                //$('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });    
}


</script>
<div class="block">
    <div class="left-side">        
                <ul class="nav nav-tabs">
                    <li class='active'><a href="#my_invite"  data-toggle="tab">I invited</a></li>
                    <li><a href="#me_invite"  data-toggle="tab">I was invited</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id='my_invite'>
                        <?
                            global $arResumeFilter;
                            $arResumeFilter = Array("CREATED_BY" => (int)$_REQUEST["USER_ID"]);
                            if ($arResumeFilter){
                                $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list_multi",
                                    "my_invites",
                                    Array(
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "N",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "invites",
                                        "IBLOCK_ID" => $arResumeIB["ID"],
                                        "NEWS_COUNT" => "999",
                                        "SORT_BY1" => "ACTIVE_FROM",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "arResumeFilter",
                                        "FIELD_CODE" => array("ACTIVE"),
                                        "PROPERTY_CODE" => array("RESTORAN", "BRON", "USERS", "NA_USERS"),
                                        "CHECK_DATES" => "N",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "ACTIVE_DATE_FORMAT" => "j F Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "CACHE_TYPE" => "N",
                                        "CACHE_TIME" => "30",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "Y",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "Y",
                                        "PAGER_TITLE" => "Резюме",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N"
                                    ),
                                    false
                                );
                            } else {
                                ?>
                                No added resume
                            <? } ?>
                            <div style=""><input type="button" onclick="add_invite(0)" class="btn btn-info" value="Add invitation"/></div>
                    </div>
                    <div class="tab-pane" id='me_invite'>
                        <?
                        global $arVacFilter;
                            $arVacFilter = Array("PROPERTY_USERS" => $USER->GetID(), "ACTIVE" => "",
                                Array(
                                    "LOGIC"=>"OR",
                                    Array("PROPERTY_DECLAIN"=>false),
                                    //Array("!PROPERTY_DECLAIN"=>false,">PROPERTY_DECLAIN"=>$USER->GetID(), "<PROPERTY_DECLAIN"=>$USER->GetID()),
                                    Array("!PROPERTY_DECLAIN"=>false,"!><PROPERTY_DECLAIN"=> Array($USER->GetID(),$USER->GetID()))
                                )
                            );
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list_multi",
                                "me_invites",
                                Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "invites",
                                    "IBLOCK_ID" => $arResumeIB["ID"],
                                    "NEWS_COUNT" => "10",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arVacFilter",
                                    "FIELD_CODE" => array("ACTIVE","CREATED_BY"),
                                    "PROPERTY_CODE" => array("RESTORAN", "BRON", "USERS", "NA_USERS"),
                                    "CHECK_DATES" => "N",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "30",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Резюме",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                                ),
                                false
                            );?>                        
                    </div>
                </div>        
    </div>
    <div class="right-side">
            <div class="baner2">
                <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            </div>
            <br /><br />
            <div class="title">Recommended</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "PROPERTY_restoran_ratio",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",//s
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended"
                ),
            false
            );?>
            <br /><br />

        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
            <br /><br />
        <div class="title">Top</div>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "show_counter",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "popular"
                ),
            false
            );?>
        <br />        
        
        <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_3_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>        
    </div>
    <div class="clearfix"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>