<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("Статус бронирования")?>
<?
if (check_bitrix_sessid() && $_REQUEST["yid"] && $_REQUEST["act"]=="cnl"):
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>105,"PROPERTY_yandexid"=>$_REQUEST["yid"]),false,Array("nTopCount"=>1),Array("ID","IBLOCK_ID"));
        if ($ar = $res->Fetch())
        {
            CIBlockElement::SetPropertyValuesEx($ar["ID"], $ar["IBLOCK_ID"], array("status" => 1501));
            CIBlockElement::SetPropertyValuesEx($ar["ID"], $ar["IBLOCK_ID"], array("new" => ""));            
        }      
        LocalRedirect("/order.php?yid=".$_REQUEST["yid"]);
endif; 
?>
<div id="content">
    <div class="left" style="width:728px">   
        <h1><?=$APPLICATION->ShowTitle(false)?></h1>
    <?
    $elem = array();
    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>105,"PROPERTY_yandexid"=>$_REQUEST["yid"]),false,Array("nTopCount"=>1),Array("ID","ACTIVE_FROM","IBLOCK_ID","NAME","PROPERTY_rest","PROPERTY_guest","PROPERTY_status"));
    if ($ar = $res->Fetch())
    {       
        global $DB;
        //$date = $DB->FormatDate($ar["ACTIVE_FROM"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y, G:i', MakeTimeStamp($ar["ACTIVE_FROM"], CSite::GetDateFormat()));        
        //$arTmpDate = explode(" ", $arTmpDate);        
        $elem["DATE"] = $arTmpDate;//$arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
        
        $elem["STATUS"] = $ar["PROPERTY_STATUS_VALUE"];
        $r = CIBlockElement::GetByID($ar["PROPERTY_REST_VALUE"]);
        if ($a = $r->GetNext())
        {
            $elem["REST"] = Array("NAME"=>$a["NAME"],"LINK"=>$a["DETAIL_PAGE_URL"]);
        }        
    }
    ?>
        <form>
                <p class="font16">
                    <?=bitrix_sessid_post()?>
                    <input type="hidden" name="yid" value="<?=$_REQUEST["yid"]?>" />
                    <input type="hidden" name="act" value="cnl" />
                    <b>Дата</b>: <?=$elem["DATE"]?><Br />
                    <b>Ресторан</b>: <a class="another" href="<?=$elem["REST"]["LINK"]?>"><?=$elem["REST"]["NAME"]?></a><Br/>
                    <b>Статус</b>: <?=$elem["STATUS"]?><Br /><Br />
                    <input type="submit" class="light_button" value="Отменить">
                </p>                
        </form>

    </div>
    <div class="right">
        
    </div>
    <div class="clear"></div>
    <Br /><Br /><Br />
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>