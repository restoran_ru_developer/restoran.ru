<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<script>
    $(document).ready(function(){
        $(".baner2 object").css("z-index","9998");
    });
</script>
<style>
    .baner2 object
    {
        z-index:9999;
    }
</style>
<?
global $USER;
$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");
//$arIB = getArIblock("special_projects", CITY_ID, "new_year_corp_");
//v_dump($arIB);
if ($arIB["ID"]) {
    ?>    
    <div class="left" style="width:750px;">
        <?
        if (CITY_ID!="tmn"):
//            if ($_REQUEST["PAGEN_1"])
//            {
//                $res = CIBlockElement::GetList(Array("SORT"=>"ASC","NAME"=>"ASC"),Array("IBLOCK_ID"=>$arIB["ID"]),false,Array("nTopCount"=>6));
//                while ($ar = $res->Fetch())
//                {
//                    $ids[] = $ar["ID"];
//                }                
//                global $arrFilter;
//                $arrFilter["!ID"] = $ids;                  
//            }
            $APPLICATION->IncludeComponent("restoran:catalog.list", 
                //(!$_REQUEST["PAGEN_1"]&&!$_REQUEST["arrFilter_pf"])?"articles_main":"articles", 
                "articles", 
                array(
                "IBLOCK_TYPE" => "special_projects",
                "IBLOCK_ID" => $arIB["ID"],
                "PARENT_SECTION_CODE" => "articles",
                "NEWS_COUNT" => "12",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "PROPERTY_CODE" => array(
                    0 => "RESTORAN",
                    1 => "ratio",
                    2 => "reviews_bind",
                    3 => "average_bill",
                ),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list1",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
                    ), false
            );
        else:
            $APPLICATION->IncludeComponent("restoran:catalog.list", 
                    "articles", 
                    array(
                    "IBLOCK_TYPE" => "special_projects",
                    "IBLOCK_ID" => $arIB["ID"],
                    "PARENT_SECTION_CODE" => "articles",
                    "NEWS_COUNT" => "12",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "NAME",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "ratio",
                        2 => "reviews_bind",
                        3 => "",
                    ),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                        ), false
                );        
        endif;
        ?>   
        <div class="preview_seo_text">
            <? $APPLICATION->ShowViewContent("preview_seo_text"); ?>
        </div>
    </div>
    <div class="left" style="width:230px;">
        <div class="filter-block">
            <div class="filter-block-1"></div>
            <div class="filter-block-1-content">
                <?                           
                $arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");                
                CModule::IncludeModule("iblock");
                $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arIB["ID"]));
                while ($prop_fields = $properties->Fetch())
                {
                    if ($prop_fields["CODE"]!="RESTORAN")
                        $filter[] = $prop_fields["CODE"];                        
                                        
                }
//                if ($USER->IsAdmin())
//                    v_dump($filter);
//                if ($_REQUEST["SECTION_CODE"]=="valentine")
//                    $filter = array("subway", "zagorod", "bill", "compliment","with_dm");
//                if ($_REQUEST["SECTION_CODE"]=="8marta")
//                    $filter = array("subway", "bill", "compliment","with_dm");
//                if ($_REQUEST["SECTION_CODE"]=="post")
//                    $filter = array("subway", "bill","out_city");
//                if ($_REQUEST["SECTION_CODE"]=="sochi2014")
//                    $filter = array("subway", "area","average_bill", "music","show");
                $APPLICATION->IncludeComponent( "restoran:catalog.filter", "restoran", 
                    Array(
                        "IBLOCK_TYPE" => "special_project",
                        "IBLOCK_ID" => $arIB["ID"],
                        "FILTER_NAME" => "arrFilter",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => (CITY_ID=="tmn")?array("area","bill","kitchen", "alc", "with_dm"):$filter,
                        "PRICE_CODE" => array(),
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "N",
                        "LIST_HEIGHT" => "5",
                        "TEXT_WIDTH" => "20",
                        "NUMBER_WIDTH" => "5",
                        "SAVE_IN_SESSION" => "N"
                            )
                    );
                ?>
            </div>
            <div class="filter-block-2"></div>
            <div class="filter-block-2-content">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:search.title", "ny_suggest_new", Array(), false
                );
                ?>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>    
        <br /><Br />
        <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "Y",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_3_main_page",
                            "NOINDEX" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>
        <br /><br />
        <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest_".CITY_ID.".php"),
                    Array(),
                    Array("MODE"=>"html")
            );?>
        <br /><br />   
    </div>
    <?
} else {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>