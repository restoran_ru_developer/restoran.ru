<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Где провести свадьбу в Москве");
// get IB params from iblock code
//$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
global $USER;
$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");
if ($arIB["ID"])
{   
?>
<div class="left" style="width:728px; height:385px;">
    <br />
    <div class="rating_rest">Рейтинг <span class="georgia">ресторанов</span></div>
    <div class="popular_m"><a href="popular" class="active">Самые популярные</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="bron">Последние забронированные</a></div>    
    <div id="popular" class="top_blocks">
    <?
    $arTopBlock["CODE"] = "popular";
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "index_block_top4",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "special_projects",
                "IBLOCK_ID" => $arIB["ID"],
                "NEWS_COUNT" => "1",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arTopBlock",
                "FIELD_CODE" => "",
                "PROPERTY_CODE" => array(
                    0  => "ELEMENTS",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
            );?>
    </div>
    <div id="bron" style="display:none;"  class="top_blocks">
    <?
    $arTopBlock["CODE"] = "bron";
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "index_block_top4",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "special_projects",
                "IBLOCK_ID" => $arIB["ID"],
                "NEWS_COUNT" => "1",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arTopBlock",
                "FIELD_CODE" => "",
                "PROPERTY_CODE" => array(
                    0  => "ELEMENTS",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
            );?>
    </div>
</div>
<div class="right">
    <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
</div>
<div class="clear"></div>
<br />
<?$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner",
        "",
        Array(
                "TYPE" => "new_year_960_80",
                "NOINDEX" => "Y",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "600"
        ),
false
);?>
<br /><br />
<div class="left" style="width:728px;">
    <div class="title_rest">Новогодние идеи<br /><span class="georgia">от Restoran.ru</span></div>
    <Br />
    <?
    $arTopBlock2["CODE"] = "news";
    $APPLICATION->IncludeComponent(
        "restoran:catalog.list",
        "news_main",
        Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "special_projects",
                "IBLOCK_ID" => $arIB["ID"],
                "NEWS_COUNT" => "1",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arTopBlock2",
                "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                "PROPERTY_CODE" => array("ELEMENTS"),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "500",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "ANOTHER_LINK" => $arItem["PROPERTIES"]["LINK"]["VALUE"]
        ),
    false
    );?>
    
    <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "index_block",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "special_projects",
                    "IBLOCK_ID" => $arIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrOthersBlock",
                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "7200",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
                );
    ?>   
</div>
<div class="right" style="width:240px;">
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
    
    <br /><br />
    <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    <br /><Br />
    <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
</div>
<div class="clear"></div>
<?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
            "TYPE" => "bottom_rest_list",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "600"
    ),
false
);?>
<Br /><Br />
<?}
else
{
    @define("ERROR_404", "Y");	
    CHTTP::SetStatus("404 Not Found");    
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>