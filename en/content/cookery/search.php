<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Кулинария");
if (!$_REQUEST["PROPERTY"]&&!$_REQUEST["PROPERTY_VALUE"])
{
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["cook"])==1&&$_REQUEST["arrFilter_pf"]["cook"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["cook"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/content/cookery/search/cook/".$ar1["CODE"]."/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["osn_ingr"])==1&&$_REQUEST["arrFilter_pf"]["osn_ingr"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["osn_ingr"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/content/cookery/search/osn_ingr/".$ar1["CODE"]."/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["prig_time"])==1&&$_REQUEST["arrFilter_pf"]["prig_time"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["prig_time"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/content/cookery/search/prig_time/".$ar1["CODE"]."/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["povod"])==1&&$_REQUEST["arrFilter_pf"]["povod"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["povod"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/content/cookery/search/povod/".$ar1["CODE"]."/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["prig"])==1&&$_REQUEST["arrFilter_pf"]["prig"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["prig"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/content/cookery/search/prig/".$ar1["CODE"]."/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["cat"])==1&&$_REQUEST["arrFilter_pf"]["cat"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["cat"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/content/cookery/search/cat/".$ar1["CODE"]."/");
    }
}
?>
<div class="block">
    <div class="left-side">
        <?$APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "news",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "cookery",
                    "IBLOCK_ID" => 139,
                    "NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "21"),
                    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
                    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFilterTop4",
                    "FIELD_CODE" => array("CREATED_BY"),
                    "PROPERTY_CODE" => array("ratio","COMMENTS"),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "200",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => ($_REQUEST["SECTION_CODE"]=="all")?"":$_REQUEST["SECTION_CODE"],
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );
        ?>
    </div>
    <div class="right-side">
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_2_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
        <div class="tags">
            <?            
            if (LANGUAGE_ID == 'ru') {
                $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                        "SORT" => "CNT",
                        "PAGE_ELEMENTS" => "20",
                        "PERIOD" => "",
                        "URL_SEARCH" => "/search/index.php",
                        "TAGS_INHERIT" => "Y",
                        "CHECK_DATES" => "Y",
                        "FILTER_NAME" => "",
                        "arrFILTER" => array(
                                0 => "iblock_cookery",
                        ),
                        "arrFILTER_iblock_cookery" => array(
                                0 => 139,
                        ),
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "86400",
                        "FONT_MAX" => "24",
                        "FONT_MIN" => "12",
                        "COLOR_NEW" => "30c5f0",
                        "COLOR_OLD" => "24A6CF",
                        "PERIOD_NEW_TAGS" => "",
                        "SHOW_CHAIN" => "Y",
                        "COLOR_TYPE" => "N",
                        "WIDTH" => "100%"
                        ),
                        $component
                );
            }
            ?> 
        </div>                        
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_1_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>                   
        <div class="title">Топ-3 рецептов</div>        
        <?
            $obCache = new CPHPCache; 
            $life_time = 8*60*60; 
            $cache_id = "cookerytop3"; 
            if($obCache->InitCache($life_time, $cache_id, "/")) :
                $vars = $obCache->GetVars();
                $top3 = $vars["TOP3"];
            else :
                CModule::IncludeModule("iblock");
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>70,"CODE"=>"top3ru","ACTIVE"=>"Y","ACTIVE_DATE"=>"Y"),false,Array("nTopCount"=>1));
                if ($ar = $res->Fetch())
                {
                    $db_props = CIBlockElement::GetProperty(70, $ar["ID"], "", "", Array("CODE"=>"recepts"));
                    while ($ob = $db_props->Fetch())
                    {
                        $top3[] = $ob["VALUE"];
                    }                            
                }
            endif;                    
            if($obCache->StartDataCache()):
                $obCache->EndDataCache(array(
                    "TOP3"    => $top3
                )); 
            endif;
        ?>
        <?
        if (count($top3)>1)
        {
            global $arrFilterTop3;
            $arrFilterTop3["ID"] = $top3; 
        }
        $_REQUEST["arrFilter_pf"] = array();
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "cookery",
                        "IBLOCK_ID" => 139,
                        "NEWS_COUNT" => (count($top3)>1) ? 9 : 3,
                        "SORT_BY1" => "shows",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop3",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "200",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );
        ?>                                                                                                  
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_3_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
    </div>
    <div class="clearfix"></div>
    <noindex>
        <div id="yandex_direct">
            <script type="text/javascript"> 
                //<![CDATA[
                yandex_partner_id = 47434;
                yandex_site_bg_color = 'FFFFFF';
                yandex_site_charset = 'utf-8';
                yandex_ad_format = 'direct';
                yandex_font_size = 1;
                yandex_direct_type = 'horizontal';
                yandex_direct_limit = 4;
                yandex_direct_title_color = '24A6CF';
                yandex_direct_url_color = '24A6CF';
                yandex_direct_all_color = '24A6CF';
                yandex_direct_text_color = '000000';
                yandex_direct_hover_color = '1A1A1A';
                document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                //]]>
            </script>
        </div>        
    </noindex>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:advertising.banner", "", Array(
        "TYPE" => "bottom_rest_list",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
            ), false
    );
    ?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>