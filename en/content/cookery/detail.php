<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Интервью");
?>
<div class="block">
    <div class="left-side">
        <?
        if (substr_count($APPLICATION->GetCurPage(), "msk")||substr_count($APPLICATION->GetCurPage(), "spb"))
        {
            $url = str_replace("msk","content",$APPLICATION->GetCurPage());
            $url = str_replace("spb","content",$url);
            LocalRedirect("http://www.restoran.ru".$url,true,"301 Moved permanently");
        }
        ?>
        <?
         $APPLICATION->IncludeComponent(
                "bitrix:news.detail",
                "cook_detail",
                Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "USE_SHARE" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "cookery",
                        "IBLOCK_ID" => 2606,
                        "ELEMENT_ID" => "",
                        "ELEMENT_CODE" => $_REQUEST["CODE"],
                        "CHECK_DATES" => "N",
                        "FIELD_CODE" => Array("CREATED_BY","DATE_CREATE","DETAIL_PICTURE","PREVIEW_TEXT"),
                        "PROPERTY_CODE" => Array("COMMENTS","RESTORAN","cat","osn_ingr","prig_time","cook","povod","predp","prig","slognost","similar_recepts"),
                        "IBLOCK_URL" => "",
                        "META_KEYWORDS" => (SITE_ID=="s1")?"":"keywords",
                        "META_DESCRIPTION" => (SITE_ID=="s1")?"":"description",
                        "BROWSER_TITLE" => (SITE_ID=="s1")?"":"title",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "USE_PERMISSIONS" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600000001",
                        "CACHE_NOTES" => "new2",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Страница",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_SHOW_ALL" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                ),
        false
        );
         ?>
    </div>
    <div class="right-side">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        
        <div class="title">Similar recipes</div>
        <?
        global $arrFilterTop4;
        global $props;        
        if (is_array($props["similar"]))
        {
            $arrFilterTop4 = Array(
                "ID"=>$props["similar"]
            );
        }
        else
        {
            $arrFilterTop4 = Array(
                "!ID"=>$arResult["ID"],
                "PROPERTY_cat" => $props["cat"],
                Array("LOGIC"=>"OR",
                    Array( "PROPERTY_osn_ingr" => $props["osn_ingr"]),
                    //Array("!PROPERTY_osn_ingr" => false)
                ),            
            );            
        }
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "interview_one_with_border",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "cookery",
                    "IBLOCK_ID" => (SITE_ID=="s1")?139:2606,
                    "NEWS_COUNT" => 3,
                    "SORT_BY1" => "ID",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFilterTop4",
                    "FIELD_CODE" => array("CREATED_BY"),
                    "PROPERTY_CODE" => array("ratio","reviews_bind"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "200",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );
        ?>         
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>                 
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_3_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>        
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>