<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("FAQ");
$APPLICATION->AddHeadString('<link href="/tpl/css/faq.css";  type="text/css" rel="stylesheet" />', true);
$APPLICATION->AddHeadScript('/tpl/js/jquery-ui-1.8.21.custom.min.js');
$APPLICATION->AddHeadScript('/tpl/js/jquery.scrollview.js');
?>

<script type="text/javascript">
    function navigateTo(x, y)
    {
        x = x * -1;
        y = y * -1;
        //alert(1);
        $("#slider-div").stop(true,false).animate(
            {
                left: x+'px',
                top: y+'px'
            },
            1000
        );
    }


    /**
     * Получает смещение относительно левого верхнего угла
     */
    function calculateCorrection()
    {
        correctionX = $("#slider-data").width() / 2;
        correctionY = $("#slider-data").height() / 2;
    }


    $(document).ready(function(){

        $("#slider-data").scrollview();

        var tX, tY;

        // Получение коррекции
        calculateCorrection()

        $(".nv").click(function(){

            var id = $(this).attr('id');
            ids = id.substr(3);
            coords = ids.split('-');
            coords[0] = parseInt(coords[0]);
            coords[1] = parseInt(coords[1]);

            navigateTo(coords[0],coords[1]);

        });
    });
</script>

<div id="content">

<div id="slider-data">
<div id="slider-div">

<div id="bb1" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87632/"><span
            class="ap">Бронирование столиков</span> <span class="q">?</span></a></div>
        <div class="bcont-text">Простые способы бронирования
            на любой вкус.
        </div>
    </div>
</div>

<div id="bb2" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87646/"><span class="ap">Купоны</span> <span
            class="q">?</span></a></div>
        <div class="bcont-text">Выбери предложение<br/>
            Оплати Online<br/>
            Воспользуйся скидкой.
        </div>
    </div>
</div>
<div id="bb3" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87636/"><span class="ap">Отзывы <br/>о ресторанах</span>
            <span class="q">?</span></a></div>
        <div class="bcont-text">Оставляй отзывы, загружай фото!</div>
    </div>
</div>
<div id="bb4" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87645/"><span class="ap">Кулинария</span> <span
            class="q">?</span></a></div>
        <div class="bcont-text">Отличные рецепты в новом формате!</div>
    </div>
</div>
<div id="bb5" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87643/"><span class="ap">Преимущества зарегистрированных пользователей</span>
            <span class="q">?</span></a></div>
        <div class="bcont-text">Регистрация на Restoran.ru стала невероятно простой.</div>
    </div>
</div>


<div id="crcl1" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><span class="ap"><a href="/<?=CITY_ID?>/articles/help/87649/">Размещаем <br/><span
            class="corr"></span> ресторан</span> <span class="q">?</span></a></div>
        <div class="bcont-text">Заполни данные <br/> Преобрети пакет размещения <br/> Размещай ресторан.</div>
    </div>
</div>
<div id="crcl2" class="bbl">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87653/"><span
            class="ap">Размещаем <br/> сетевой <br/> ресторан</span> <span class="q">?</span></a></div>
        <div class="bcont-text">Единые фотография и описание ресторана сети.</div>
    </div>
</div>


<div id="b_small_ord_01" class="bbl"></div>
<div id="b_small_ord_02" class="bbl"></div>
<div id="b_small_ord_03" class="bbl"></div>
<div id="b_small_reg_01" class="bbl"></div>
<div id="b_small_reg_02" class="bbl"></div>
<div id="b_small_reg_03" class="bbl"></div>
<div id="b_small_reg_04" class="bbl"></div>
<div id="b_small_reg_05" class="bbl"></div>
<div id="b_small_reg_06" class="bbl"></div>


<div id="b_circle_white_01" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87650/"><span
            class="ap">Размещаем<br/> доставку<br/><span class="corr"></span> еды</span> <span class="q">?</span></a>
        </div>

    </div>
</div>
<div id="b_circle_white_02" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87654/"><span
            class="ap">Размещаем<br/> компанию-<br/><span class="corr"></span>поставщика</span> <span class="q">?</span></a>
        </div>

    </div>
</div>
<div id="b_circle_white_03" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87655/"><span class="ap">Приобретаем<br/> баннеры</span>
            <span class="q">?</span></a></div>
        <div class="bcont-text">Выбери раздел размещения <br/>
            Выбери тип баннера <br/>
            Оплати
        </div>
    </div>
</div>
<div id="b_circle_white_04" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87648/"><span class="ap">Публикуем<br/><span
            class="corr"></span> вакансии</span> <span class="q">?</span></a></div>

    </div>
</div>
<div id="b_circle_white_05" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87651/"><span class="ap">Добавляем<br/><span
            class="corr"></span> купоны</span> <span class="q">?</span></a></div>

    </div>
</div>
<div id="b_circle_white_06" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87652/"><span class="ap">Отслеживаем<br/><span
            class="corr"></span> бронирования</span> <span class="q">?</span></a></div>

    </div>
</div>
<div id="b_circle_white_07" class="bbl wc">
    <div class="bcont">
        <div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87647/"><span class="ap">Общайтесь</span> <span
            class="q">?</span></a></div>
        <div class="bcont-text">
            Общайтесь с коллегами,<br/> комментируйте блоги,<br/> приглашайте <br/>
            в ресторан!
        </div>
    </div>
</div>


<div id="text_01" class="bbl ap">Новый <br/>Ресторан.ру</div>
<div id="text_02" class="bbl ap">Для <br/>рестораторов</div>


<div class="textblock" id="tb_reg_01">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87642/">Пригласи в
        ресторан <span class="q">?</span></a></div>
    <div class="textblock-content">Теперь ты можешь пригласить
        в ресторан, как понравившихся
        пользователей, так и своих друзей, незарегистрированных на портале.
    </div>
</div>

<div class="textblock" id="tb_reg_02">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87641/">Добавить в
        друзья <span class="q">?</span></a></div>
    <div class="textblock-content">Теперь ты можешь подписаться
        на интересных пользователей,
        добавив их в друзья.
    </div>
</div>

<div class="textblock" id="tb_reg_03">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87637/">Твой личный блог
        <span class="q">?</span></a></div>
    <div class="textblock-content">Отличная площадка
        для общения Online.
    </div>
</div>


<div class="textblock" id="tb_reg_04">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87638/">Обмен
        сообщениями <span class="q">?</span></a></div>
    <div class="textblock-content">Можешь вести переписку
        с понравившимися пользователями.
    </div>
</div>


<div class="textblock" id="tb_reg_05">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87639/">Твое резюме
        <span class="q">?</span></a></div>
    <div class="textblock-content">Ищешь работу в ресторнной сфере?</div>
</div>


<div class="textblock" id="tb_reg_06">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87640/">Еда с доставкой
        <span class="q">?</span></a></div>
    <div class="textblock-content">Теперь ты можешь заказать еду
        с доставкой в любое удобное место!
    </div>
</div>


<div class="textblock" id="tb_ord_01">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87633/">По телефону
        <span class="q">?</span></a></div>
    <div class="textblock-content">Теперь ты можешь заказать еду
        с доставкой в любое удобное место!
    </div>
</div>

<div class="textblock" id="tb_ord_02">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87634/">Бронирование
        Online <span class="q">?</span></a></div>
    <div class="textblock-content">Теперь ты можешь заказать еду
        с доставкой в любое удобное место!
    </div>
</div>
<div class="textblock" id="tb_ord_03">
    <div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87632/">По SMS <span
        class="q">?</span></a></div>
    <div class="textblock-content">Теперь ты можешь заказать еду
        с доставкой в любое удобное место!
    </div>
</div>


<div class="textblock" id="tb_new">
    <div class="textblock-title" style="font-weight: bold;">Что изменилось?</div>
    <div class="textblock-content">Ура! Свершилось! Теперь Ресторан.ру
        не только стильный и красивый,
        но и невероятно удобный!
    </div>
</div>


</div>

</div>

<div class="clear"></div>

</div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>