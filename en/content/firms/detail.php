<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$arFirmsIB = getArIblock("firms", CITY_ID);?>
<div class="block">
    <div class="restoran-detail">
        <div class="restoran_name_view_content">
            <?//$APPLICATION->ShowViewContent("restoran_name_view_content");?>   
            <h1><?=$APPLICATION->ShowTitle(false)?></h1>
        </div>   
        <div class="left-side">
            <?$APPLICATION->IncludeComponent("restoran:restoraunts.detail", "firms", array(
                "IBLOCK_TYPE" => "firms",
                "IBLOCK_ID" => $arFirmsIB["ID"],
                "ELEMENT_ID" => "",
                "ELEMENT_CODE" => $_REQUEST["FIRM"],
                "CHECK_DATES" => "Y",
                "PROPERTY_CODE" => array(
                        0 => "adres",
                        1 => "subway",
                        2 => "kitchen",
                        3 => "opening_hours",
                        4 => "phone",
                        5 => "site",
                        6 => "area",
                        7 => "address",
                        8 => "subway",
                        9 => "map",
                        10 => "email",
                        11 => "information"
                ),
                "ADD_REVIEWS" => "Y",
                "REVIEWS_BLOG_ID" => "3",
                "SIMILAR_OUTPUT" => "Y",
                "SIMILAR_PROPERTIES" => array(
                        0 => "kitchen",
                        1 => "average_bill",
                        2 => "",
                ),
                "IBLOCK_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "Y",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Страница",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "ADD_COMMENT_TEMPLATE" =>"with_rating"
                ),
                false
            );?> 
        </div>
        <div class="right-side">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
            false
            );?>                        
        </div>
        <div class="clearfix"></div>
        <?
        if ($count["NEWS_COUNT"]):
        ?>
            <ul class="nav nav-tabs">
                <?if ($count["NEWS_COUNT"]):?>
                    <li class="active"><a href="#news" data-toggle="tab">Новости</a></li>                        
                <?endif;?>                
            </ul>
            <div class="tab-content">                
                <div class="tab-pane active medium" id="news">                                    
                    <?  $arNewsIB = getArIblock("news", CITY_ID);
                        global $arrNewsFilter;
                        if ($arNewsIB["ID"]):
                            $arrNewsFilter = Array("PROPERTY_RESTORAN"=>(int)$element_id);
                            $APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "detailed_news_rest",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "news",
                                    "IBLOCK_ID" => $arNewsIB["ID"],
                                    "NEWS_COUNT" => "2",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arrNewsFilter",
                                    "FIELD_CODE" => array("DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("COMMENTS","RESTORAN"),
                                    "CHECK_DATES" => "N",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "150",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => ($_REQUEST["letn"]=="Y")?"letnie_verandy":"",
                                    "CACHE_TYPE" => "Y",
                                    "CACHE_TIME" => "14402",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "REST_ID" => (int)$element_id,
                                    "WITHOUT_LINK" => "Y"
                                ),
                            false
                            );
                        endif;?>
                </div>                
            </div>
        <?endif;?>
        <?        
            if ($count["SPEC_COUNT"]):
                $ar1 = getArIblock("special_projects", CITY_ID,"easter_");
                $ar2 = getArIblock("special_projects", CITY_ID,"8marta_");
                $ar3 = getArIblock("special_projects", CITY_ID,"valentine_");
                $ar4 = getArIblock("special_projects", CITY_ID,"post_");
                $ar5 = getArIblock("special_projects", CITY_ID,"new_year_night_");
                $ar6 = getArIblock("special_projects", CITY_ID,"new_year_corp_");
                $ar7 = getArIblock("special_projects", CITY_ID,"letnie_verandy_");    
                $ar8 = getArIblock("special_projects", CITY_ID,"sport_");    
                $ar_spec = Array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],$ar6["ID"],$ar7["ID"],$ar8["ID"]);
                global $arrNewsFilter;
                $arrNewsFilter = Array("PROPERTY_RESTORAN"=>(int)$element_id);

                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list_multi",
                    "special_projects",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "special_projects",
                            "IBLOCK_ID" => $ar_spec,
                            "NEWS_COUNT" => 3,
                            "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "timestamp_x"),
                            "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "arrNewsFilter",
                            "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array("COMMENTS","summa_golosov"),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "3604",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "search_rest_list",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                    ),
                false
                );
            endif;            
            ?>
    </div>
</div>   
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>