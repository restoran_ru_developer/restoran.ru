<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Firms");?>
<div class="block">    
    <h1><?=$APPLICATION->ShowTitle(false)?></h1>
    <div class="left-side">
        <?
        $arFirmsIB = getArIblock("firms", CITY_ID);
        $APPLICATION->IncludeComponent("bitrix:news.list", "firms", Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "firms",
                "IBLOCK_ID" => $arFirmsIB["ID"],
                "NEWS_COUNT" => $_REQUEST["pageRestCnt"]?$_REQUEST["pageRestCnt"]:"15",
                "SORT_BY1" => "NAME",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array("DETAIL_PICTURE","TAGS"),
                "PROPERTY_CODE" => array(
                        "information",
                        "phone",
                        "subway",
                        "adres",
                        "COMMENTS",
                        "RATIO"
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "200",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => $_REQUEST["FIRM_SECTION"],
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
                ),
                false
        );?>
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "bottom_content_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
        false
        );?>
    </div>
    <div class="right-side">                 
        <?/*if (CSite::InGroup(Array(1,4,15))):?>
            <script>
                $(document).ready(function(){
                   $("#add_firm").click(function(){
                       $.ajax({
                           type:"POST",
                           url:"/tpl/ajax/add_firm.php",                                   
                           success:function(data){
                               if(!$("#add_firm_form").size())
                               {
                                   $("<div class='popup popup_modal' id='add_firm_form' style='width:735px'></div>").appendTo("body");
                               }
                               $('#add_firm_form').html(data);
                               showOverflow();
                               setCenter($("#add_firm_form"));
                               $("#add_firm_form").css("display","block");
                           }});
                   });                        
                });                         
            </script>
           <a class="add_recipe" id="add_firm" href="javascript:void(0)">+ Добавить фирму</a>
        <?endif;*/?>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_2_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>                   
<!--            <div class="title">For restorators</div>-->
<!--            --><?//
//            $APPLICATION->IncludeComponent(
//                    "restoran:catalog.list",
//                    "interview_one_with_border",
//                    Array(
//                        "DISPLAY_DATE" => "N",
//                        "DISPLAY_NAME" => "Y",
//                        "DISPLAY_PICTURE" => "Y",
//                        "DISPLAY_PREVIEW_TEXT" => "N",
//                        "AJAX_MODE" => "N",
//                        "IBLOCK_TYPE" => "firms_news",
//                        "IBLOCK_ID" => 2423,
//                        "NEWS_COUNT" => "3",
//                        "SORT_BY1" => "ACTIVE_FROM",
//                        "SORT_ORDER1" => "DESC",
//                        "SORT_BY2" => "",
//                        "SORT_ORDER2" => "",
//                        "FILTER_NAME" => "",
//                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
//                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
//                        "CHECK_DATES" => "Y",
//                        "DETAIL_URL" => "",
//                        "PREVIEW_TRUNCATE_LEN" => "120",
//                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
//                        "SET_TITLE" => "N",
//                        "SET_STATUS_404" => "N",
//                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                        "ADD_SECTIONS_CHAIN" => "N",
//                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                        "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
//                        "PARENT_SECTION_CODE" => "",
//                        "CACHE_TYPE" => "N",
//                        "CACHE_TIME" => "36000000",
//                        "CACHE_FILTER" => "Y",
//                        "CACHE_GROUPS" => "N",
//                        "DISPLAY_TOP_PAGER" => "N",
//                        "DISPLAY_BOTTOM_PAGER" => "N",
//                        "PAGER_TITLE" => "Новости",
//                        "PAGER_SHOW_ALWAYS" => "N",
//                        "PAGER_TEMPLATE" => "",
//                        "PAGER_DESC_NUMBERING" => "N",
//                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                        "PAGER_SHOW_ALL" => "N",
//                        "AJAX_OPTION_JUMP" => "N",
//                        "AJAX_OPTION_STYLE" => "Y",
//                        "AJAX_OPTION_HISTORY" => "N"
//                    ),
//                false
//                );?>
            <br /><br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
    </div>
    <div class="clearfix"></div>    
    <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>