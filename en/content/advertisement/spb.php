<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Реклама на сайте");
$APPLICATION->SetPageProperty("description", "Реклама на сайте");
$APPLICATION->SetTitle("Реклама на сайте");
?> 
<div id="content"> 
  <h1><?=$APPLICATION->GetTitle()?></h1>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">VIP</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">60 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города. 
    <br />
   2. Описательная статья на русском и английском языках. 
    <br />
   3. Меню и винная карта на русском и английском языках. 
    <br />
   4. Фотографии (до 30 штук). 
    <br />
   5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик). 
    <br />
   6. Субдомен www.название ресторана.restoran.ru , почтовый ящик: название ресторана@restoran.ru. 
    <br />
   7. Контекстная реклама по названию ресторана в Яндексе. 
    <br />
   8. Баннер 240х400 в разделе Рестораны сроком на 1 месяц в ротации! (кроме октября-декабря). 
    <br />
   9. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице &ndash; без ограничений. 
    <br />
   10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). 
    <br />
   11. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;. 
    <br />
   12. Бесплатное обновление меню 1 раз в месяц (только на русском языке). 
    <br />
   13. Cкидка 50% на размещение в других рубриках (&quot;Банкетные залы&quot;).   
    <br />
   14. Возможно изготовление 3D-тура (оплачивается отдельно).</p>
 
  <p class="MsoNormal"><b>Пример: <a href="http://spb.restoran.ru/spb/detailed/restaurants/narechke/" target="_blank" >ресторан &laquo;На речке&raquo;</a></b></p>
 
  <p class="MsoNormal"> <hr/></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal" style="text-align: center;"><font size="4"><b>PREMIUM 
        <br />
       </b><b>40 000 рублей в год</b></font></p>
 
  <p class="MsoNormal" style="text-align: center;"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт. 
    <br />
   2. Описательная статья на русском и английском языках. 
    <br />
   3. Меню и винная карта на русском и английском языках. 
    <br />
   4. Фотографии (до 30 штук).   
    <br />
   5. Видеопанорамы залов.  
    <br />
   6. Есть возможность размещать видео (предоставляет заказчик       
    <br />
   7. Субдомен www. название ресторана.restoran.ru, почтовый ящик название ресторана@restoran.ru.     
    <br />
   8. Бесплатное рекламное место сроком на 1 месяц (кроме сентября-октября). 
    <br />
   9. Высокая степень прописки по названию в поисковых системах www.yandex.ru или www.google.ru (1-3 место). 
    <br />
   10. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений. 
    <br />
   11. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). 
    <br />
   12. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;. 
    <br />
   13. Бесплатное обновление меню 1 раз в месяц (только на русском языке). 
    <br />
   14. Cкидка 50% на размещение в других рубриках.   
    <br />
   15. Возможно изготовление 3D-тура (оплачивается отдельно).</p>
 
  <p class="MsoNormal"><b>Пример: <a href="http://spb.restoran.ru/spb/detailed/restaurants/francesco/" target="_blank" >ресторан «Франческо»</a></b></p>
 
  <p class="MsoNormal"> <hr/></p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">BUSINESS</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">30 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города. 
    <br />
   2. Меню и винная карта на русском языке. 
    <br />
   3. Фотографии (до 20 штук).          
    <br />
   4. Описательная статья.  
    <br />
   5. Субдомен www. название ресторана.restoran.ru, почтовый ящик «название ресторана@restoran.ru. 
    <br />
   6. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений. 
    <br />
   7. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). 
    <br />
   8. Бесплатное участие в рубрике «Мастер-класс от шеф-повара». 
    <br />
   9. Бесплатное обновление меню 1 раз в месяц. 
    <br />
   10. Скидка 50% на размещение в других рубриках («Банкетные залы»). </p>
 
  <p class="MsoNormal"><b>Пример: <a href="http://spb.restoran.ru/spb/detailed/restaurants/the_kitchen/" target="_blank" >ресторан «The Kitchen»</a></b></p>
 
  <p style="text-align: center;" class="MsoNormal">____________________________________________________________________________________________</p>
 
  <div style="text-align: center;" yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><font size="4"><b>FOREVER</b></font></div>
 
  <div style="text-align: center;" yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><b><font size="4">90 000 рублей</font></b></div>
 
  <div style="text-align: center;" yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><font size="4"><b>Однократная оплата. Бессрочное размещение</b></font></div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><font size="4"><b> 
        <br />
       </b></font></div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">2. Описательная статья на русском и английском языках.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">3. Меню и винная карта на русском и английском языках.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">4. Фотографии (до 30 штук).  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик).  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">6. Субдомен www.название ресторана.restoran.ru , почтовый ящик: название ресторана@restoran.ru.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">7. Баннер 980x90 сроком на 1 месяц (кроме октября-декабря) в разделах: 
    <br />
   
    <ul> 
      <li>Каталог ресторанов или</li>
     
      <li>Внутренние страницы ресторанов или</li>
     
      <li>Каталог банкетных залов или</li>
     
      <li>Внутренние страницы банкетных залов или</li>
     
      <li>Все остальные страницы сайта.</li>
     </ul>
   8. Бесплатное рекламное место сроком на 1 месяц (кроме сентября-октября). </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">9. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">11. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">12. Бесплатное обновление меню 1 раз в месяц (только на русском языке).  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">13. Cкидка 50% на размещение в других рубриках (&quot;Банкетные залы&quot;).   
    <br />
   14. Возможно изготовление 3D-тура (оплачивается отдельно).</div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st=""> 
    <br />
   </div>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>Дополнительные услуги:</b><o:p></o:p></p>
 
  <p class="MsoNormal"> </p>
 
  <ul> 
    <li>Дополнительный выезд фотографа - 1800 руб. и 2300 руб. ( в пределах КАД)</li>
   
    <li>Дополнительный выезд фотографа – 2200 руб. и 2800 руб. (за пределами КАД)</li>
   
    <li>Дополнительный выезд журналиста – 1000 руб. (в пределах КАД)</li>
   
    <li>Дополнительный выезд журналиста – 1300 руб. (за пределами КАД)</li>
   </ul>
 <o:p></o:p> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>НДС не облагается.</b></p>
 
  <p class="MsoNormal"><b>Наши менеджеры с удовольствием ответят на все Ваши вопросы: 
      <br />
     </b> 
    <br />
   <b>В Санкт-Петербурге:  
      <br />
     </b>Контакты:  
    <br />
   941-15-48, Олег (oleg@restoran.ru)</p>
 
  <p class="MsoNormal"><b>В Москве:</b> 
    <br />
   (495) 745-05-66 
    <br />
   E-mail: manager@restoran.ru</p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"> 
    <br />
   </p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>