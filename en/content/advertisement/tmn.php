<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Реклама на сайте");
$APPLICATION->SetPageProperty("description", "Реклама на сайте");
$APPLICATION->SetTitle("Реклама на сайте");
?> 
<div id="content"> 
  <h1><?=$APPLICATION->GetTitle()?></h1>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"> 
    <div><font size="4"><b>VIP</b></font></div>
   
    <div><font size="4"><b>33 000 рублей в год</b></font></div>
   
    <div><font size="4"><b> 
          <br />
         </b></font></div>
   
    <div style="text-align: justify;">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города; </div>
   
    <div style="text-align: justify;">2. Авторская статья; </div>
   
    <div style="text-align: justify;">3. Меню и винная карта; </div>
   
    <div style="text-align: justify;">4. Фотографии (до 30 штук); </div>
   
    <div style="text-align: justify;">5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик); </div>
   
    <div style="text-align: justify;">6. Субдомен www.название ресторана.restoran.ru , почтовый ящик: название ресторана@restoran.ru; </div>
   
    <div style="text-align: justify;">7. Баннер 240х400 в разделе Рестораны, срок ротации 3 месяца (кроме октября-декабря); </div>
   
    <div style="text-align: justify;">8. Высокая степень прописки по названию в поисковых системах www.yandex.ru или www.google.ru (1-3 место);</div>
   
    <div style="text-align: justify;">9. Размещение новостей и фоторепортажей на главной странице (без ограничений); на собственной странице &ndash; без ограничений; </div>
   
    <div style="text-align: justify;">10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.); </div>
   
    <div style="text-align: justify;">11. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;; </div>
   
    <div style="text-align: justify;">12. Бесплатное обновление меню 1 раз в месяц. </div>
   </div>
 
  <p class="MsoNormal"> 
    <br />
   <hr/></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal" style="text-align: center;"><font size="4"><b>PREMIUM 
        <br />
         </b><b><font size="4">20</font> 000 рублей в год</b></font></p>
 
  <p class="MsoNormal" style="text-align: center;"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт; 
    <br />
   2. Авторская статья; 
    <br />
   3. Меню и винная карта; 
    <br />
   4. Фотографии (до 30 штук); 
    <br />
   5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик); 
    <br />
   6. Субдомен www. название ресторана.restoran.ru, почтовый ящик названиересторана@restoran.ru; 
    <br />
   7. Высокая степень прописки по названию в поисковых системах www.yandex.ru или www.google.ru (1-3 место); 
    <br />
   8. Размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений; 
    <br />
   9. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.); 
    <br />
   10. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;; 
    <br />
   11. Бесплатное обновление меню 1 раз в месяц. 
    <br />
   
    <br />
   <hr/></p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">BUSINESS</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">17 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города; 
    <br />
   2. Меню и винная карта на русском языке (частично); 
    <br />
   3. Фотографии (до 10 штук); 
    <br />
   4. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик); 
    <br />
   5. Статья о ресторане предоставляется заказчиком; 
    <br />
   6. Субдомен www. название ресторана.restoran.ru, почтовый ящик &laquo;название ресторана@restoran.ru; 
    <br />
   7. Размещение новостей и фоторепортажей на собственной странице – без ограничений; </p>

  <p class="MsoNormal">8. Бесплатное обновление меню 1 раз в месяц. </p>
 <hr/> 
  <p class="MsoNormal" style="text-align: center;"><b><font size="4">LIGHT 
        <br />
         <font size="4">12 </font>000 рублей в год</font></b></p>
 
  <p class="MsoNormal" style="text-align: center;"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт; 
    <br />
   2. Фотографии (до 10 штук); 
    <br />
   3. Субдомен www. название ресторана.restoran.ru, почтовый ящик названиересторана@restoran.ru; 
    <br />
   4. Размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений. 
    <br />
    <hr/></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>
      <br />
     </b></p>
 
  <p class="MsoNormal"><b>Дополнительные услуги:</b><o:p></o:p></p>
 
  <p class="MsoNormal"> </p>
 Репортажная фотосъемка мероприятий 
  <br />
 Предметная фотосъемка блюд и напитков для меню и другой полиграфии 
  <br />
 <o:p></o:p> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>НДС не облагается.</b></p>
 
  <p class="MsoNormal"><b>Мы с удовольствием ответим на все Ваши вопросы: 
      <br />
     </b> Контакты:  +7 (909) 184 83 33; tmn@restoran.ru</p>
 <b>С Уважением к Вам и Вашему бизнесу. 
    <br />
   Команда портала Restoran.ru Тюмень</b> 
  <br />
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>