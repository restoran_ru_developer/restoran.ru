<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Реклама на сайте");
$APPLICATION->SetPageProperty("description", "Реклама на сайте");
$APPLICATION->SetTitle("Реклама на сайте");
?> 
<div id="content"> 
  <h1><?=$APPLICATION->GetTitle()?></h1>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">VIP</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">30 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города; </p>

  <p class="MsoNormal">2. Авторская статья; </p>

  <p class="MsoNormal">3. Меню и винная карта; </p>

  <p class="MsoNormal">4. Фотографии (до 30 штук); </p>

  <p class="MsoNormal">5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик); </p>

  <p class="MsoNormal">6. Субдомен www.название ресторана.restoran.ru , почтовый ящик: название ресторана@restoran.ru; </p>

  <p class="MsoNormal">7. Высокая степень прописки по названию в поисковых системах www.yandex.ru или www.google.ru (1-3 место);</p>

  <p class="MsoNormal">9. Размещение новостей и фоторепортажей на главной странице (без ограничений); на собственной странице &ndash; без ограничений; </p>

  <p class="MsoNormal">10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.); </p>

  <p class="MsoNormal">Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;; </p>

  <p class="MsoNormal">Бесплатное обновление меню 1 раз в месяц;</p>

  <p class="MsoNormal">Бесплатное размещение в рубрике &laquo;Банкетные залы&raquo;; </p>

  <p class="MsoNormal">Размещение вакансий в рубрике «Работа».</p>

  <p class="MsoNormal"> <hr/></p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">BUSINESS</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">20 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1.Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт;</p>

  <p class="MsoNormal">2.Фотографии (до 10 штук);</p>

  <p class="MsoNormal">3. Субдомен www. название ресторана.restoran.ru, почтовый ящик названиересторана@restoran.ru;</p>

  <p class="MsoNormal">4. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений;</p>

  <p class="MsoNormal">5. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т. д.);</p>

  <p class="MsoNormal">6.Бесплатное размещение в рубрике «Банкетные залы».</p>

  <p class="MsoNormal"><hr/></p>
 
  <p style="text-align: center;" class="MsoNormal"><b><font size="4">LIGHT 
        <br />
       12 000 рублей в год</font></b></p>
 
  <p style="text-align: center;" class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт;</p>

  <p class="MsoNormal">2. Фотографии (до 10 штук);</p>

  <p class="MsoNormal">3. Субдомен www. название ресторана.restoran.ru, почтовый ящик названиересторана@restoran.ru;</p>

  <p class="MsoNormal">4. Размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений.<hr/></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>Дополнительные услуги:</b><o:p></o:p></p>
 
  <p class="MsoNormal"> </p>
 
  <ul> 
    <li>
      <li>Репортажная фотосъемка мероприятий </li>
    
      <li>Предметная фотосъемка блюд и напитков для меню и другой полиграфии </li>
    </li>
   </ul>
 <o:p></o:p> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>НДС не облагается.</b></p>
 
  <p class="MsoNormal"><b>Наши менеджеры с удовольствием ответят на все Ваши вопросы: 
      <br />
     </b> 
    <br />
  </p>

  <p class="MsoNormal"><b>Контакты: </b>ufa@restoran.ru </p>

  <p class="MsoNormal"><b>С уважением к вам и вашему бизнесу. </b></p>

  <p class="MsoNormal"><b>Команда портала Restoran.ru Уфа</b></p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>