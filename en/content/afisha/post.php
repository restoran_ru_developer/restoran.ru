<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
// get blog url by post id
$arBlog = getBlogAr($_REQUEST["BLOG_POST_ID"]);
?>

<?$APPLICATION->IncludeComponent("restoran:blog.post", "afisha_post", Array(
	"SEO_USER" => "N",	// Запретить индексацию ссылки на профиль пользователя поисковыми ботами
	"BLOG_URL" => $arBlog["URL"],	// Путь блога
	"PATH_TO_BLOG" => "/".CITY_ID."/afisha/?item=#blog#",	// Шаблон пути к странице блога
	"PATH_TO_BLOG_CATEGORY" => "",	// Шаблон пути к странице блога c фильтром по тегу
	"PATH_TO_POST_EDIT" => "/".CITY_ID."/afisha/?blog_id=#blog#&post_id=#post_id#&action=edit",	// Шаблон пути к странице редактирования сообщения блога
	"PATH_TO_USER" => "",	// Шаблон пути к странице пользователя блога
	"PATH_TO_SMILE" => "",	// Путь к папке со смайликами относительно корня сайта
	"BLOG_VAR" => "",	// Имя переменной для идентификатора блога
	"POST_VAR" => "",	// Имя переменной для идентификатора сообщения блога
	"USER_VAR" => "",	// Имя переменной для идентификатора пользователя блога
	"PAGE_VAR" => "",	// Имя переменной для страницы
	"ID" => $_REQUEST["BLOG_POST_ID"],	// Идентификатор сообщения
	"SET_NAV_CHAIN" => "N",	// Добавлять пункт в цепочку навигации
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "86400",	// Время кеширования (сек.)
	"POST_PROPERTY" => "",	// Показывать доп. свойства сообщения
	"DATE_TIME_FORMAT" => "d.m.Y",	// Формат показа даты и времени
	"SHOW_RATING" => "N",	// Использовать рейтинги
	"IMAGE_MAX_WIDTH" => "300",	// Максимальная ширина изображения
	"IMAGE_MAX_HEIGHT" => "300",	// Максимальная высота изображения
	),
	false
);?>

<p><a href="/<?=CITY_ID?>/afisha/?item=<?=$arBlog["URL"]?>">Все мероприятия</a></p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>