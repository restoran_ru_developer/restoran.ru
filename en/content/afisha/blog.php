<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<?$APPLICATION->IncludeComponent("restoran:blog.posts_list", "afisha_blog", Array(
        "SEO_USER" => "Y",	// Запретить индексацию ссылки на профиль пользователя поисковыми ботами
        "MESSAGE_COUNT" => "25",	// Количество сообщений, выводимых на страницу
        "DATE_TIME_FORMAT" => "d.m.Y",	// Формат показа даты и времени
        "PATH_TO_BLOG" => "/".CITY_ID."/afisha/?item=#blog#",	// Шаблон пути к странице блога
        "PATH_TO_BLOG_CATEGORY" => "",	// Шаблон пути к странице блога c фильтром по тегу
        "PATH_TO_POST" => "/".CITY_ID."/afisha/#post_id#/",	// Шаблон пути к странице с сообщением блога
        "PATH_TO_POST_EDIT" => "/".CITY_ID."/afisha/?blog_id=#blog#&post_id=#post_id#&action=edit",	// Шаблон пути к странице редактирования сообщения блога
        "PATH_TO_USER" => "",	// Шаблон пути к странице пользователя блога
        "PATH_TO_SMILE" => "",	// Путь к папке со смайликами относительно корня сайта
        "BLOG_VAR" => "",	// Имя переменной для идентификатора блога
        "POST_VAR" => "",	// Имя переменной для идентификатора сообщения блога
        "USER_VAR" => "",	// Имя переменной для идентификатора пользователя блога
        "PAGE_VAR" => "",	// Имя переменной для страницы
        "BLOG_URL" => $_REQUEST["BLOG_URL"],	// Путь блога
        "YEAR" => $year,	// Год для фильтрации
        "MONTH" => $month,	// Месяц для фильтрации
        "DAY" => $day,	// День для фильтрации
        "CATEGORY_ID" => $category,	// Идентификатор тега для фильтрации
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CACHE_TIME" => "7200",	// Время кеширования (сек.)
        "CACHE_TIME_LONG" => "604600",	// Время кэширования остальных страниц
        "SET_NAV_CHAIN" => "N",	// Добавлять пункт в цепочку навигации
        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
        "FILTER_NAME" => "arFilter",	// Имя массива со значениями фильтра для фильтрации сообщений
        "NAV_TEMPLATE" => "",	// Имя шаблона для постраничной навигации
        "POST_PROPERTY_LIST" => "",	// Показывать доп. свойства сообщения в блоге
        "SHOW_RATING" => "N",	// Использовать рейтинги
        "IMAGE_MAX_WIDTH" => "300",	// Максимальная ширина изображения
        "IMAGE_MAX_HEIGHT" => "300",	// Максимальная высота изображения
        "SORT_BY1" => "DATE_PUBLISH",
        "SORT_ORDER1" => "ASC"
	),
	false
);?>

<p><a href="?item=<?=$_REQUEST["BLOG_URL"]?>&action=add">Добавить мероприятие</a></p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>