<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Афиша");
$arKuponsIB = getArIblock("afisha", CITY_ID);
?>
<style>
    #content .poster_pane
    {
        max-height:100%;
    }
</style>
<script>
    $(function(){
       $("ul#poster_tabs").each(function(){
            if($(this).attr("ajax")=="ajax")
            {
                var url="";
                if($(this).attr("ajax_url"))
                    url=$(this).attr("ajax_url");
                $(this).tabs("div.poster_panes > .poster_pane",{history:'true',onBeforeClick:function(event,i){var pane=this.getPanes().eq(i);if(pane.is(":empty")){pane.load(url+this.getTabs().eq(i).attr("href"),function(){if($(this).parents(".pane").attr("main")=="main_page")
                    $('.poster_pane').jScrollPane({verticalGutter:15});});}}});
            }
            else
                $(this).tabs("div.poster_panes > .poster_pane",{effect:'fade'});
        });
            
        $("ul.history_tabs").each(function(){if($(this).attr("ajax")=="ajax")
{var url="";if($(this).attr("ajax_url"))
url=$(this).attr("ajax_url");$(this).tabs("div.panes > .pane",{history:'true',onBeforeClick:function(event,i){var pane=this.getPanes().eq(i);if(pane.is(":empty")){pane.load(url+this.getTabs().eq(i).attr("href"));}}});}
else
$(this).tabs("div.panes > .pane",{effect:'fade'});}); 
    });
</script>
<div class="block">    
    <div class="left-side">
        <h1>Афиша</h1>       
        <div class="clearfix"></div>
        <?
        global $arrFilter;
            $arrFilter["DATE_ACTIVE_FROM"] = Array(false,date("d.m.Y"));
            $arrFilter["PROPERTY_EVENT_DATE"] = date("Y-m-d");
            if ($_REQUEST["type"])
                $arrFilter["PROPERTY_EVENT_TYPE"] = $_REQUEST["type"];
            if ($_REQUEST["restoran"])
            {
                $arrFilter["PROPERTY_RESTORAN"] = $_REQUEST["restoran"];
                //$arrFilter["!PROPERTY_RESTORAN"] = false;
            }

            if ($_REQUEST["tags"])
            {
/*                $temp = explode(" ",trim($_REQUEST["tags"]));
                $fil =implode(" || ".$temp);
                $arrFilter["PROPERTY_TAGS"] = $fil;*/
                $arrFilter[] = Array(
                    "LOGIC" => "OR",
                    Array("NAME" => "%".trim($_REQUEST["tags"])."%"),
                    Array("DETAIL_TEXT" => "%".trim($_REQUEST["tags"])."%")
                );
            }

        ?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "afisha",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "kupons",
                    "IBLOCK_ID" => $arKuponsIB["ID"],
                    "NEWS_COUNT" => "580",//($_REQUEST["pageKuponCnt"] ? $_REQUEST["pageKuponCnt"] : "20"),
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => Array("NAME", "ACTIVE_TO","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("RESTORAN","EVENT_TYPE","EVENT_DATE"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "7200",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Купоны",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "kupon_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
            ),
        false
        );?>
        <div class="clearfix"></div>
    </div>
    <div class="right-side">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
                <?
        /*
        <div class="top_block"><?=GetMessage("TOP_4_TITLE")?></div>
// get restaurants iblock info
        $arRestIB = getArIblock("catalog", CITY_ID);

        // get only restaurants with reviews
        global $arrFilterTop4;
        $arrFilterTop4 = Array(
            "!PROPERTY_reviews_bind" => false
        );

        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "top4_rest_main",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "4",
                    "SORT_BY1" => "PROPERTY_ratio",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilterTop4",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array("ratio", "reviews_bind"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );?>
        <br /><br />
        <div align="right"><a class="uppercase" href="#">ВЕСЬ РЕЙТИНГ</a></div>
         * 
         */?>
        <div class="title">Популярные</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "show_counter",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "popular",
                    "REST_PROPS"=>"Y"
                ),
            false
            );?>          
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>        
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_3_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
    <div class="clearfix"></div>    
    <div id="yandex_direct">
        <script type="text/javascript"> 
        //<![CDATA[
        yandex_partner_id = 47434;
        yandex_site_bg_color = 'FFFFFF';
        yandex_site_charset = 'utf-8';
        yandex_ad_format = 'direct';
        yandex_font_size = 1;
        yandex_direct_type = 'horizontal';
        yandex_direct_limit = 4;
        yandex_direct_title_color = '24A6CF';
        yandex_direct_url_color = '24A6CF';
        yandex_direct_all_color = '24A6CF';
        yandex_direct_text_color = '000000';
        yandex_direct_hover_color = '1A1A1A';
        document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
        //]]>
        </script>
    </div>    
    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>      
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>