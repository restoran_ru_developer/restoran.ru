<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Афиша");
$arAfishaIB = getArIblock("afisha", CITY_ID);
?>
<div class="block">
    <div class="left-side">
<?
 $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"statya_detail",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "afisha",
		"IBLOCK_ID" => $arAfishaIB["ID"],
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"CHECK_DATES" => "N",
		"FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
		"PROPERTY_CODE" => Array("COMMENTS","RESTORAN","EVENT_DATE","TIME","D1","D2","D3","D4","D5","D6","D7"),
		"IBLOCK_URL" => "",
		"META_KEYWORDS" => "keywords",
		"META_DESCRIPTION" => "description",
		"BROWSER_TITLE" => "title",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
		"USE_PERMISSIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);
 ?>
        </div>
    <div class="right-side">
        <?
        global $REST_ID;
        $_REQUEST['id'] = $REST_ID;
        if($REST_ID) {?>
            <div class="modal-dialog floating-bron-form">
                <div class="modal-content">
                    <div class="modal-body en-bg">
                        <noindex>
                            <?
                            $APPLICATION->IncludeComponent(
                                CITY_ID == 'rga' ? "bitrix:form.result.new2" : "restoran:form.result.new",
                                "order_2016_news_form",
                                Array(
                                    "SEF_MODE" => "N",
                                    "WEB_FORM_ID" => CITY_ID == 'rga' ? "24" : "3",
                                    "LIST_URL" => "",
                                    "EDIT_URL" => "",
                                    "SUCCESS_URL" => "",
                                    "CHAIN_ITEM_TEXT" => "",
                                    "CHAIN_ITEM_LINK" => "",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "USE_EXTENDED_ERRORS" => "N",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "3600",
                                    "MY_CAPTCHA" => "Y",
                                    "VARIABLE_ALIASES" => Array(
                                        "WEB_FORM_ID" => "WEB_FORM_ID",
                                        "RESULT_ID" => "RESULT_ID"
                                    )
                                ),
                                false
                            );
                            ?>
                        </noindex>
                    </div>
                </div>
            </div>
        <?}?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        <br />        
        <?//if (($arResult["MORE_ARTICLES"]>0&&$arResult["IBLOCK_TYPE_ID"]=="afisha")||$arResult["IBLOCK_TYPE_ID"]!="afisha"):?>
            <div class="title"><?=GetMessage("READ_A")?></div>
            <?
            global $arrFil;
            $arrFil = Array("!ID"=>$arResult["ID"]);        
            ?>
            <?            
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "afisha",
                        "IBLOCK_ID" => $arAfishaIB["ID"],
                        "NEWS_COUNT" => 3,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "shows",
                        "SORT_ORDER2" => "DESC",
                        "FILTER_NAME" => "arrFil",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("ratio","reviews_bind"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );
            ?>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "content_article_list",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        <?//endif;?>       
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>        
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>    
    </div>
    <div class="clearfix"></div>
    <div class='next_prev_article'>        
        <?if ($links["PREV"]):?>
            <div class='left'><a class="span" href="<?=$links["PREV"]?>"><span class="icon-arrow-left"></span></a> <a href="<?=$links["PREV"]?>">Предыдущий пост</a></div>
        <?endif;?>
        <?if ($links["NEXT"]):?>
            <div class='right'><a  href="<?=$links["NEXT"]?>">Следующий пост</a> <a class="span" href="<?=$links["PREV"]?>"><span class="icon-arrow-right"></span></a></div>
        <?endif;?>            
    </div>
    <div id="yandex_direct">
        <script type="text/javascript"> 
        //<![CDATA[
        yandex_partner_id = 47434;
        yandex_site_bg_color = 'FFFFFF';
        yandex_site_charset = 'utf-8';
        yandex_ad_format = 'direct';
        yandex_font_size = 1;
        yandex_direct_type = 'horizontal';
        yandex_direct_limit = 4;
        yandex_direct_title_color = '30c5f0';
        yandex_direct_url_color = '30c5f0';
        yandex_direct_all_color = '30c5f0';
        yandex_direct_text_color = '000000';
        yandex_direct_hover_color = '1A1A1A';
        document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
        //]]>
        </script>
    </div>    
    <?$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner",
        "",
        Array(
                "TYPE" => "bottom_rest_list",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
        ),
        false
    );
    ?>        
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>