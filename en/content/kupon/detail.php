<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("kupon.restoran.ru");
$arKuponsIB = getArIblock("kupons", CITY_ID);
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"kupon",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "kupons",
		"IBLOCK_ID" => $arKuponsIB["ID"],
		//"ELEMENT_ID" => (int)$_REQUEST["ID"],
		"ELEMENT_CODE" => $_REQUEST["KUPON"],
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => Array("ACTIVE_TO","TAGS"),
		"PROPERTY_CODE" => Array("PHOTOS","RESTORAN_PREVIEW","subway"),
		"IBLOCK_URL" => "",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"USE_PERMISSIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);?> 
<div class="clear"></div>
<!--<div class="kupon_long_baner">
    <img src="<?=SITE_TEMPLATE_PATH?>/images/top_baner.png" />
</div>
<br /><br />-->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>