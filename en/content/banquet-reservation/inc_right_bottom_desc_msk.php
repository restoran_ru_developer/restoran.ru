Все выбранные для заказа столика и заказа банкета рестораны Петербурга вы можете подробно
изучить в нашем каталоге: меню, фотогалерею, особенности. Если вы оставили онлайн-заявку на
заказ столика в ресторане, в самое ближайшее время с вами свяжется консультант службы заказов
Ресторан.ру, подтвердит заказ выбранных вами заведений и ответит на все вопросы, связанные с
вашим комфортным пребыванием в ресторане. Если вы оставили онлайн-заявку на заказ банкета в
ресторане, то в самое ближайшее время с вами свяжется консультант службы заказа банкетов
Ресторан.ру, уточнит детали предстоящего торжества и подберет несколько подходящих вариантов для
вашего праздника. После того, как вы выберете подходящий банкетный зал или ресторан для
праздника, мы закажем его для вас на нужную дату.