<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?>
<?
if($_REQUEST["CONTEXT"]=="Y"){
    if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
}
if ($_REQUEST["PROPERTY"] == 'prigorody') {
	$REQUEST_PROPERTY = "out_city";
	$_REQUEST["PROPERTY"] = "out_city";
}
else $REQUEST_PROPERTY = $_REQUEST["PROPERTY"];
//if (!empty($_REQUEST["arrFilter_pf"]["prigorody"])) $_REQUEST["arrFilter_pf"]["out_city"] = $_REQUEST["arrFilter_pf"]["prigorody"];
// denied get special rest
$arrFilter["!PROPERTY_top_spec_place_VALUE"] = Array("Да");
$arrFilter["!PROPERTY_str_spec_place_VALUE"] = Array("Да");

//if (CITY_ID!="spb"&&CITY_ID!="msk"&&CITY_ID!="rga"&&!$_REQUEST["page"])
if (!$_REQUEST["page"])
{
    $_REQUEST["page"] = 1;
    $_REQUEST["PAGEN_1"] = 1;
}

//elseif(CITY_ID=="rga") {
//    $_REQUEST["PAGEN_1"] = 1;
//}
//v_dump($_REQUEST);

$arIB = getArIblock("catalog",CITY_ID);
if ($_REQUEST["PROPERTY"]&&$_REQUEST["PROPERTY_VALUE"])
{
    $arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
    $properties = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arRestIB["ID"], "CODE"=>$REQUEST_PROPERTY));
    if ($prop_fields = $properties->GetNext())
    {
        if ($prop_fields["LINK_IBLOCK_ID"])
        {
            if ($_REQUEST["PROPERTY_VALUE"]!="all")
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y","CODE"=>$_REQUEST["PROPERTY_VALUE"]),false,false,Array("ID", "NAME"));
                if($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]] = $ar["ID"];
                        if ($_REQUEST["PROPERTY"]=="kitchen")
                        {
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_kitchen"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_kitchen"]=$ar["ID"];
                            }
                        }
                        elseif($_REQUEST["PROPERTY"]=="type")
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_type"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_type"]=$ar["ID"]; 
                            }
                        }
						
						elseif($_REQUEST["PROPERTY"]=="subway")
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_subway"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_subway"]=$ar["ID"]; 
                            }
                        }
						
						elseif($_REQUEST["PROPERTY"]=="area" || $_REQUEST["PROPERTY"]=="rajon")
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_area"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_area"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="out_city") || ($_REQUEST["PROPERTY"]=="prigorody"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_out_city"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_out_city"]=$ar["ID"]; 
                                //$_REQUEST["PRIORITY_prigorody"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="average_bill") || ($_REQUEST["PROPERTY"]=="srednij_schet"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_average_bill"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_average_bill"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="children") || ($_REQUEST["PROPERTY"]=="detyam"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_children"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_children"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="proposals") || ($_REQUEST["PROPERTY"]=="predlozheniya"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_proposals"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_proposals"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="features") || ($_REQUEST["PROPERTY"]=="osobennosti"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_features"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_features"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="entertainment") || ($_REQUEST["PROPERTY"]=="razvlecheniya"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_entertainment"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_entertainment"]=$ar["ID"]; 
                            }
                        }
						
						elseif(($_REQUEST["PROPERTY"]=="ideal_place_for") || ($_REQUEST["PROPERTY"]=="idealnoe_mesto_dlya"))
                        {                                      
                            $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_ideal_place_for"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                            if ($r->SelectedRowsCount())
                            {
                                $_REQUEST["PRIORITY_ideal_place_for"]=$ar["ID"]; 
                            }
                        }

                }  
            }
            else
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"),false,false,Array("ID"));
                while($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]][] = $ar["ID"];
                }
            }
        }
    }
}
else
{
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["kitchen"])==1&&$_REQUEST["arrFilter_pf"]["kitchen"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["kitchen"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/kitchen/".$ar1["CODE"]);
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["type"])==1&&$_REQUEST["arrFilter_pf"]["type"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["type"][0]);
        $ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/type/".$ar1["CODE"]);
    }
	
	// метро
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["subway"])==1&&$_REQUEST["arrFilter_pf"]["subway"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["subway"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/metro/".$ar1["CODE"]);
	}
	
	// район города
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["area"])==1&&$_REQUEST["arrFilter_pf"]["area"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["area"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/rajon/".$ar1["CODE"]);
	}
	
	// Пригороды 
	if (is_array($_REQUEST["arrFilter_pf"])&&((count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["out_city"])==1&&$_REQUEST["arrFilter_pf"]["out_city"][0])))
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["out_city"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/prigorody/".$ar1["CODE"]);
	}
	
	// средний счёт average_bill
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["average_bill"])==1&&$_REQUEST["arrFilter_pf"]["average_bill"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["average_bill"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/srednij_schet/".$ar1["CODE"]);
	}
	
	// Детям
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["children"])==1&&$_REQUEST["arrFilter_pf"]["children"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["children"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/detyam/".$ar1["CODE"]);
	}
	
	// Предложения
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["proposals"])==1&&$_REQUEST["arrFilter_pf"]["proposals"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["proposals"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/predlozheniya/".$ar1["CODE"]);
	}
	
	// Особенности
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["features"])==1&&$_REQUEST["arrFilter_pf"]["features"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["features"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/osobennosti/".$ar1["CODE"]);
	}
	
	// Развлечения
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["entertainment"])==1&&$_REQUEST["arrFilter_pf"]["entertainment"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["entertainment"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/razvlecheniya/".$ar1["CODE"]);
	}
	
	// Идеальное место для
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["ideal_place_for"])==1&&$_REQUEST["arrFilter_pf"]["ideal_place_for"][0])
	{
		$r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["ideal_place_for"][0]);
		$ar1 = $r->Fetch();
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/idealnoe_mesto_dlya/".$ar1["CODE"]);
	}
        if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&$_REQUEST["arrFilter_pf"]["wi_fi"])
	{		
		LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/wi_fi/y/");
	}
	if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==2)
        {
            $url_str = "";
			if ($key == 'kitchen' || $key == 'type') {
			// добавил проверку на тип и кухню (первоначально редиректы делались только под эти параметры. 
			// а вообще - вроде всё правильно, но ломается, если выбрать, например 2 типа и одно метро - в выдаче происходит неведомая ересь)
            foreach ($_REQUEST["arrFilter_pf"] as $key=>$filter)
            {
                if (count($filter)==1)
                {
                    $r = CIBlockElement::GetByID($filter[0]);
                    if ($ar1 = $r->Fetch())
                        $url_str .= "".$key."/".$ar1["CODE"]."/";
                    else
                        $url_str .= "".$key."/Y/";
				//if (count($key)==1) $redirect = true;
                }                
            }
            LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/".$url_str);
			}
        }
	
}
?>
<div class="block">
    <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
    <div class="sort">
        <?
        // set rest sort links
        $excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1","by","index_php?page","index_php");
        $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=new&".(($_REQUEST["pageRestSort"]=="new"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=price&".(($_REQUEST["pageRestSort"]=="price"&&$_REQUEST["by"]=="asc") ? "by=desc": "by=asc"), $excUrlParams);
//        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=ratio&".(($_REQUEST["pageRestSort"]=="ratio"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=popular&".(($_REQUEST["pageRestSort"]=="popular"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=alphabet&".((($_REQUEST["pageRestSort"]=="alphabet"&&$_REQUEST["by"]=="asc")|| !$_REQUEST["pageRestSort"]) ? "by=desc": "by=asc"), $excUrlParams);
        ?>            
            <?$by = ($_REQUEST["by"]=="desc")?"asc":"desc";?>
            <?
            if ($_REQUEST["pageRestSort"] == "new")
            {
                echo '<a class="'.$by.'" href="'.$sortNewPage.'">by newest</a>';
            }
            else
            {
                echo '<a href="'.$sortNewPage.'">by newest</a>';
            }
            if ($_REQUEST["pageRestSort"] == "price")
            {
                echo '<a class="'.$by.'" href="'.$sortPricePage.'">by average check</a>';
            }
            else
            {
                echo '<a href="'.$sortPricePage.'">by average check</a>';
            }
//            if ($_REQUEST["pageRestSort"] == "ratio")
            if ($_REQUEST["pageRestSort"] == "popular")
            {
                echo '<a class="'.$by.'" href="'.$sortRatioPage.'">by rating</a>';
            }
            else
            {
                echo '<a href="'.$sortRatioPage.'">by rating</a>';
            }
            if ($_REQUEST["pageRestSort"] == "alphabet" || !$_REQUEST["pageRestSort"])
            {
                echo '<a class="'.$by.'" href="'.$sortAlphabetPage.'">by name</a>';
            }
            else
            {
                echo '<a href="'.$sortAlphabetPage.'">by name</a>';
            }
            ?>        
    </div>   
    <div class="clearfix"></div>
    <?if ((!$_REQUEST["arrFilter_pf"]&&!$_REQUEST["letter"]&&!$_REQUEST["RUBRIC"])||$_REQUEST["PRIORITY_kitchen"]||$_REQUEST["PRIORITY_type"]):?>
        <div class="left-side">
            <div class="special">
                <script type="text/javascript">
                $(document).ready(function(){
                    $(".photogalery_top_spec").galery();
                });
                </script>
                <?
                global $arrFilterTop1;
                if (intval($_REQUEST["PRIORITY_kitchen"]))
                {
                    $arrFilterTop1 = Array(
                        "PROPERTY_priority_kitchen" => intval($_REQUEST["PRIORITY_kitchen"]),
                        "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
                    );      
                }
                elseif(intval($_REQUEST["PRIORITY_type"]))
                {
                    $arrFilterTop1 = Array(
                        "PROPERTY_priority_type" => intval($_REQUEST["PRIORITY_type"]),
                        "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
                    );
                }
                else
                {
                    $arrFilterTop1 = Array(
                        "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
                        "PROPERTY_priority_type" => false,
                        "PROPERTY_priority_kitchen" => false,
                        "!PROPERTY_sleeping_rest_VALUE" => "Да"
                    );        
                }
                $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "rest_list_top_spec",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => "1",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arrFilterTop1",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "opening_hours", "phone", "address", "subway","RATIO"),
                                "CHECK_DATES" => "N",
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "160",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "86402",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "CACHE_NOTES" => "Nww",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "CONTEXT"=>$_REQUEST["CONTEXT"]
                        ),
                    $component
                );?>
            </div>
        </div>
        <div class="right-side">
                <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
                );?>
        </div>  
        <div class="clearfix"></div>
    <?endif;?>

        <?
        $APPLICATION->IncludeComponent("restoran:restoraunts.list_optimized", "rest_list_full_size", Array(
                "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => $_REQUEST["CITY_ID"],	// Код информационного блока
                "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
                "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
                "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
                "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
                "FILTER_NAME" => "arrFilter",	// Фильтр
                "PROPERTY_CODE" => array(	// Свойства
                        0 => "type",
                        1 => "kitchen",
                        2 => "average_bill",
                        3 => "opening_hours",
                        4 => "phone",
                        5 => "address",
                        6 => "subway",
                        7 => "ratio",
                ),
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//y	// Тип кеширования
                "CACHE_TIME" => "86409",	// Время кеширования (сек.)
                "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "N",	// Учитывать права доступа
                "CACHE_NOTES" => "new",
                "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "PAGER_TITLE" => "Рестораны",	// Название категорий
                "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36002",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                ),
                false
        );

        ?>
        <?if ((!$_REQUEST["arrFilter_pf"]&&!$_REQUEST["letter"]&&!$_REQUEST["RUBRIC"])||$_REQUEST["PRIORITY_kitchen"]||$_REQUEST["PRIORITY_type"]):?>
            <div class="left-side">  
                <div class="special">
                    <?
                    global $arrFilterTop;
                    global $TopSpec;
                    if (intval($_REQUEST["PRIORITY_kitchen"]))
                    {
                        $arrFilterTop = Array(
                            "!ID" => $TopSpec[0],
                            "PROPERTY_priority_kitchen" => intval($_REQUEST["PRIORITY_kitchen"]),
                            "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                        );      
                    }
                    elseif(intval($_REQUEST["PRIORITY_type"]))
                    {
                        $arrFilterTop = Array(
                            "!ID" => $TopSpec[0],
                            "PROPERTY_priority_type" => intval($_REQUEST["PRIORITY_type"]),
                            "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                        );
                    }
                    else
                    {
                        $arrFilterTop = Array(
                            "!ID" => $TopSpec[0],
                            "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                            "PROPERTY_priority_type" => false,
                            "PROPERTY_priority_kitchen" => false,
                            "!PROPERTY_sleeping_rest_VALUE" => "Да"
                        );
                    }
                    $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "rest_list_top_spec",
                            Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "ASC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arrFilterTop",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "opening_hours", "phone", "address", "subway"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "160",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                                    "CACHE_TYPE" => "Y",//Y
                                    "CACHE_TIME" => "86404",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "CACHE_NOTES" => "Nw",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CONTEXT"=>$_REQUEST["CONTEXT"]
                            ),
                        $component
                    );?>        
                </div>
            </div>
            <div class="right-side">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                false
                );?>
            </div>
            <div class="clearfix"></div>
        <?endif;?>
        <div class="navigation"></div>
        <script>
            $(function(){
                $(".navigation").html($(".navigation_temp").html());
                $(".navigation_temp").remove();
            })
        </script>        
        <div id ="for_seo"></div>    
        <noindex>
            <div id="yandex_direct">
                <script type="text/javascript"> 
                //<![CDATA[
                yandex_partner_id = 47434;
                yandex_site_bg_color = 'FFFFFF';
                yandex_site_charset = 'utf-8';
                yandex_ad_format = 'direct';
                yandex_font_size = 1;
                yandex_direct_type = 'horizontal';
                yandex_direct_limit = 4;
                yandex_direct_title_color = '24A6CF';
                yandex_direct_url_color = '24A6CF';
                yandex_direct_all_color = '24A6CF';
                yandex_direct_text_color = '000000';
                yandex_direct_hover_color = '1A1A1A';
                document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                //]]>
                </script>
            </div>
        </noindex>    
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>    
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>