<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.js"></script>


<?
//if (RestIBlock::IsSleeping($_REQUEST["RESTOURANT"]))
//    $templ = "sleep";
//
//if (RestIBlock::IsClosed($_REQUEST["RESTOURANT"]))
//    $templ2 = "closed";
//$arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
//if ($templ2 == "closed"&&$templ == "sleep")
//    $templ = "closed";

$arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);

if (RestIBlock::IsSleeping($_REQUEST["RESTOURANT"]))
    $templ = "sleep";

if (RestIBlock::IsClosed($_REQUEST["RESTOURANT"]))
    $templ2 = "closed";

if ($templ2 == "closed"&&$templ == "sleep")
    $templ = "closed";

if(!$templ){
    if(!RestIBlock::IsOldNetwork($_REQUEST["RESTOURANT"])){
        $templ='_.default';
    }

    if(!RestIBlock::IsNetwork($_REQUEST["RESTOURANT"])){
        $SHOW_YA_SCHEME = true;
    }
}
?>
<?if($templ=='_.default'):?>
    <script type="text/javascript">
        var slider_phone;
        $(document).ready(function(){

            $(".fancy").fancybox({
                afterShow: function() {
                    if(slider_phone){
                        $("<div class='phone-in-detail-photo'>"+slider_phone+"</div>").prependTo('.fancybox-inner');
                    }
                },
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'cyclic'            :       true,
                'overlayColor'      :       '#1a1a1a',
                'speedIn'		:	200,
                'speedOut'		:	200

            });

        });
    </script>
<?else:?>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".fancy").fancybox({
                afterShow: function() {
                    if($(this.element).next('.photo-phone-wrapper').html()){
                        $("<div class='phone-in-detail-photo'>"+$(this.element).next('.photo-phone-wrapper').html()+"</div>").prependTo('.fancybox-inner');
                    }
                },
                afterLoad: function() {
                    console.log('test');
                },
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'cyclic'            :       true,
                'overlayColor'      :       '#1a1a1a',
                'speedIn'		:	200,
                'speedOut'		:	200

            });
        });
    </script>
<?endif?>
<?FirePHP::getInstance()->info($templ);
if($templ=='_.default'):?>
    <div class="block">
        <div class="restoran-detail network-design" <?if($SHOW_YA_SCHEME):?>itemscope itemtype="http://schema.org/Organization"<?endif?>>
            <div class="restoran_name_view_content">
                <?$APPLICATION->ShowViewContent("restoran_name_view_content");?>
            </div>

            <?$APPLICATION->ShowViewContent("restoran_counter_content");?>

            <?$APPLICATION->IncludeComponent("restoran:restoraunts.detail", $templ, array(
//                'REST_PREVIEW'=>'Y',
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arIB["ID"],
                "ELEMENT_ID" => "",
                "ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
                "CHECK_DATES" => "Y",
                "PROPERTY_CODE" => array(
                    0 => "type",
                    1 => "average_bill",
                    2 => "kitchen",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "administrative_distr",
                    6 => "area",
                    7 => "address",
                    8 => "subway",
                    9 => "number_of_rooms",
                    10 => "music",
                    11 => "clothes",
                    12 => "proposals",
                    13 => "credit_cards",
                    14 => "discouncards",
                    15 => "bankets",
                    16 => "touristgroups_1",
                    17 => "banketnayasluzhba",
//                            18 => "kolichestvochelovek",
                    19 => "stoimostmenyu",
                    20 => "max_check",
                    21 => "entertainment",
                    22 => "wi_fi",
                    23 => "hrs_24",
                    24 => "parking",
                    25 => "features",
                    26 => "out_city",
                    27 => "min_check",
                    28 => "site",
                    29 => "children",
                    30 => "ideal_place_for",
                    31 => "offers",
                    32 => "email",
                    33 => "discounts",
                    34 => "landmarks",
                    35 => "map",
                    36 => "RATIO",
                    37 => "COMMENTS",
                    38 => "okrugdel",
                    39 => "kuhnyadostavki",
                    40 => "viduslug",
                    41 => "add_props",
                    42 => "network",
                    43 => "rest_group",
                    44 => "breakfasts",
                    45 => "business_lunch",
                    46 => "branch",
                    47 => "rent",
                    48 => "my_alcohol",
                    49 => "catering",
                    50 => "food_delivery",
                    51 => "proposals",
                    52 => "d_tours",
                    53 => "sale10",
                    54 => "banket_average_bill",
                    55 => "place_new_rest",
                    56=>"FOURSQUARE_USER_LOGIN",
                    57=>"INSTAGRAM_USER_LOGIN",
                    58=>"without_reviews",
                    59=>$_REQUEST['CATALOG_ID']=='banket'?"RENTING_HALLS_COST":'',
                    60=>$_REQUEST['CATALOG_ID']=='banket'?"ALLOWED_ALCOHOL":'',
                    61=>$_REQUEST['CATALOG_ID']=='banket'?"BANKET_SPECIAL_EQUIPMENT":'',
                    62=>$_REQUEST['CATALOG_ID']=='banket'?"BANKET_ADDITIONAL_OPTION":'',
                    63=>$_REQUEST['CATALOG_ID']=='banket'?"BANKET_MENU_SUM":'',
                    64=>'STREET',
                    65=>'FEATURES_IN_PICS',
                    66=>'opening_hours_google',
                    67=>'REST_NETWORK',
                    68=>'NETWORK_REST'
                ),
                "ADD_REVIEWS" => "Y",
                "REVIEWS_BLOG_ID" => "3",
                "SIMILAR_OUTPUT" => "Y",
                "SIMILAR_PROPERTIES" => array(
                    0 => "kitchen",
                    //1 => "average_bill",
                ),
                "IBLOCK_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => $USER->IsAdmin()?'N':"Y",//y
                "CACHE_TIME" => "36000119",
                "CACHE_GROUPS" => ($templ=="sleep")?"N":"N",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Страница",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                'TO_LINKS_CATALOG_ID' => $_REQUEST['CATALOG_ID']
            ),
                false
            );?>


            <?$APPLICATION->ShowViewContent("restoran_menu");?>


            <div class="clearfix"></div>
            <?if(ERROR_404!='Y'):?>
                <?$en_ru_element_id = $en_ru_element_id?array($element_id,$en_ru_element_id):array($element_id)?>

                <?if ($FOURSQUARE_USER_LOGIN||$INSTAGRAM_USER_LOGIN||$count['PHOTO_REVIEWS_COUNTER']):?>
            <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
                <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.js"></script>
                <script>
                    $(function(){
                        $(".fancybox").fancybox();
                    });
                </script>
            <?//$count["PHOTO_COUNT"]?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        <?//TODO заменить lang?>
                        User photos
                    </div>
                    <ul class="nav nav-tabs">
                        <?if($count['PHOTO_REVIEWS_COUNTER']):?>
                            <li class=""><a href="#reviews-photos" id="reviews_photos_link" data-toggle="tab" class="ajax" data-href="/tpl/ajax/get_reviews_photos.php?ID=<?=implode('|',$en_ru_element_id)?>&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>&CITY_ID=<?=CITY_ID?>">Restoran.ru</a><span>|</span></li>
                            <?if(!$FOURSQUARE_USER_LOGIN&&!$INSTAGRAM_USER_LOGIN):?>
                                <script>
                                    $(function(){
                                        $("#reviews_photos_link").click();
                                    })
                                </script>
                            <?endif?>
                        <?endif;?>
                        <?if($FOURSQUARE_USER_LOGIN):?>
                            <li class="<?if(!$INSTAGRAM_USER_LOGIN)echo "active";?>"><a href="#forsquare-widget" data-toggle="tab">Foursquare</a><span>|</span></li>
                        <?endif?>
                        <?if($INSTAGRAM_USER_LOGIN):?>
                            <li class="active"><a href="#instagram-widget" data-toggle="tab">Instagram</a><span>|</span></li>
                        <?endif;?>

                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content">
                    <?if($FOURSQUARE_USER_LOGIN):?>
                        <div class="tab-pane sm <?=(!$INSTAGRAM_USER_LOGIN)?"active":""?>" id="forsquare-widget">
                            <script>
                                $(function(){
                                    var iteration = 0;
                                    var remote_data_sq;
                                    var params = {"sq_place_id":$('.get-sq-photos-trigger').attr("restoran-fq-id")};
                                    $.ajax({
                                        type: "POST",
                                        url: $('.get-sq-photos-trigger').attr("href"),
                                        data: params,
                                        dataType: 'json'
                                    })
                                        .done(function(data) {
                                            console.log(data);
                                            remote_data_sq = data;
                                            if(data.meta.code==200){
                                                $('.sq-photos-count').text(data.response.photos.count);

                                                for(key in data.response.photos.items){
                                                    if(key>9)
                                                        break;
                                                    one_pic_url = data.response.photos.items[key].prefix+'width81'+data.response.photos.items[key].suffix;
                                                    full_pic_url = data.response.photos.items[key].prefix+'width'+data.response.photos.items[key].width+data.response.photos.items[key].suffix;
                                                    $('.sq-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                }
                                            }
                                            else {
                                                $('.sq-photos-container').text('access error, try refreshing the page or come back later');
                                                $('a.get-sq-photos-trigger').addClass('no-active');
                                            }
                                        });

                                    $('.clear-ig-fs-result.sq-photos-link').on('click', function(){
                                        if(!$(this).hasClass('no-active')){
                                            $('a.get-sq-photos-trigger').removeClass('no-active');
                                            $('.sq-photos-container ul li').each(function(indx, element){
                                                if(indx>9){
                                                    $(element).remove();
                                                }
                                            });
                                            $(this).addClass('no-active');

                                            $('html, body').stop().animate({
                                                scrollTop: $('#forsquare-widget').offset().top-60
                                            }, 500);
                                        }
                                        return false;
                                    });

                                    $(document).on('click', 'a.get-sq-photos-trigger', function (e) {
                                        if($(this).hasClass('no-active'))
                                            return false;
                                        iteration++;

                                        count_photos = $('.sq-photos-container ul li').length;

                                        if(remote_data_sq.meta.code==200){
                                            for(key in remote_data_sq.response.photos.items){
                                                key = parseInt(key)+count_photos;

                                                module_key = 20*iteration;
                                                if(key>=remote_data_sq.response.photos.count){
                                                    $('a.get-sq-photos-trigger').addClass('no-active');
                                                    break;
                                                }
                                                if(key%module_key==0)
                                                    break;
                                                one_pic_url = remote_data_sq.response.photos.items[key].prefix+'width232'+remote_data_sq.response.photos.items[key].suffix;
                                                full_pic_url = remote_data_sq.response.photos.items[key].prefix+'width'+remote_data_sq.response.photos.items[key].width+remote_data_sq.response.photos.items[key].suffix;
                                                $('.sq-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                $('.clear-ig-fs-result.sq-photos-link').removeClass('no-active');
                                            }
                                        }
                                        else {
                                            $('.sq-photos-container').text('access error, try refreshing the page or come back later');
                                        }

                                        return false;
                                    });
                                })
                            </script>
                            <div class="sq-photos-container">
                                <ul></ul>
                            </div>
                            <noindex>
                                <a href="/tpl/ajax/get_sq_photos.php" restoran-fq-id="<?=$FOURSQUARE_USER_LOGIN?>" class="get-sq-photos-trigger" >Show more photos</a>
                                <a href="#" class="clear-ig-fs-result no-active sq-photos-link" >Hide</a>
                            </noindex>
                        </div>
                    <?endif?>
                    <?if($INSTAGRAM_USER_LOGIN):?>
                        <div class="tab-pane active sm" id="instagram-widget">
                            <script>
                                $(function(){
                                    var iteration = 0;
                                    var remote_data_instagram;
                                    var params = {"USER_LOGIN":$('.get-instagram-photos-trigger').attr("restoran-instagram-id")};

                                    $.ajax({
                                        type: "POST",
                                        url: $('.get-instagram-photos-trigger').attr("href"),
                                        data: params,
                                        dataType: 'json'
                                    })
                                        .done(function(data) {

//                                            console.log(data,'inst data');
                                            remote_data_instagram = data;
                                            if(data.meta.code==200){

                                                $('.instagram-photos-count').text(data.data.length);
                                                for(key in data.data){
                                                    if(key>9)
                                                        break;
                                                    one_pic_url = data.data[key].images.thumbnail.url;
                                                    full_pic_url = data.data[key].images.standard_resolution.url;
                                                    $('.instagram-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                }
                                            }
                                            else {
                                                $('.instagram-photos-container').text('access error, try refreshing the page or come back later');
                                                $('a.get-instagram-photos-trigger').addClass('no-active');
                                            }
                                        });

                                    $('.clear-ig-fs-result.instagram-photos-link').on('click', function(){
                                        if(!$(this).hasClass('no-active')){
                                            $('a.get-instagram-photos-trigger').removeClass('no-active');
                                            $('.instagram-photos-container ul li').each(function(indx, element){
                                                if(indx>9){
                                                    $(element).remove();
                                                }
                                            });
                                            $(this).addClass('no-active');

                                            $('html, body').stop().animate({
                                                scrollTop: $('#instagram-widget').offset().top-60
                                            }, 500);
                                        }
                                        return false;
                                    });

                                    $('a.get-instagram-photos-trigger').on('click', function (e) {
                                        if($(this).hasClass('no-active'))
                                            return false;

                                        iteration++;

                                        count_photos = $('.instagram-photos-container ul li').length;
                                        //console.log(count_photos);

                                        if(remote_data_instagram.meta.code==200){
                                            for(key in remote_data_instagram.data){
                                                key = parseInt(key)+count_photos;
                                                module_key = 20*iteration;

                                                if(key>=remote_data_instagram.data.length){
                                                    $('a.get-instagram-photos-trigger').addClass('no-active');
                                                }
                                                if(key%module_key==0 || key>=remote_data_instagram.data.length)
                                                    break;
                                                one_pic_url = remote_data_instagram.data[key].images.thumbnail.url;
                                                full_pic_url = remote_data_instagram.data[key].images.standard_resolution.url;
                                                $('.instagram-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                $('.clear-ig-fs-result.instagram-photos-link').removeClass('no-active');
                                            }
                                        }
                                        else {
                                            $('.instagram-photos-container').text('access error, try refreshing the page or come back later');
                                        }


                                        return false;
                                    });
                                });
                            </script>
                            <div class="instagram-photos-container">
                                <ul></ul>
                            </div>
                            <noindex>
                                <a href="/tpl/ajax/get_instagram_photos.php" restoran-instagram-id="<?=$INSTAGRAM_USER_LOGIN?>" class="get-instagram-photos-trigger" >Show more photos</a>
                                <a href="#" class="clear-ig-fs-result no-active instagram-photos-link" >Hide</a>
                            </noindex>
                        </div>
                    <?endif?>
                    <?if($count['PHOTO_REVIEWS_COUNTER']):?>
                        <div class="tab-pane sm" id="reviews-photos"></div>
                    <?endif;?>
                </div>
                <?unset($FOURSQUARE_USER_LOGIN, $INSTAGRAM_USER_LOGIN)?>
            <?endif;?>

                <!--            <div class="left-side">-->
            <?
            if ($without_reviews!="Да"):?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        <?//TODO заменить lang?>
                        Reviews
                    </div>
                    <ul class="nav nav-tabs">
                        <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <script>
                            $(function(){
                                ajax_load = 1;
                                $.ajax({
                                    'url':'/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/reviews.php?id=<?=implode('|',$en_ru_element_id)?>&wr=<?=$without_reviews?>&url=<?=$APPLICATION->GetCurPage()?>&NEW_DESIGN=Y&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>'
                                })
                                    .done(function(data){
                                        $("#reviews").html(data);
                                        ajax_load = 0;
                                    });
                            });
                        </script>

                        <li class=""><a href="#so34c" data-toggle="tab">Facebook</a></li>
                        <li class=""><a href="#so3c" data-toggle="tab">Vkontakte</a><span>|</span></li>
                        <li class="active"><a href="#reviews" data-toggle="tab">Restoran</a><span>|</span></li>
                    </ul>
                    <div class="tabs-center-line"></div>
                </div>


                <div class="tab-content float-review-block-wrapper" >
                    <div class="tab-pane active sm reviews network-design-reviews" id="reviews"></div>
                    <div class="tab-pane sm" id="so3c">
                        <!-- Put this script tag to the <head> of your page -->
                        <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>
                        <script type="text/javascript">
                            VK.init({apiId: <?=(CITY_ID=="tmn")?"3559173":"2881483"?>, onlyWidgets: true});
                        </script>
                        <!-- Put this div tag to the place, where the Comments block will be -->
                        <div id="vk_comments"></div>
                        <script type="text/javascript">
                            VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                        </script>
                    </div>
                    <div class="tab-pane sm" id="so34c">
                        <div class="fb-comments" data-href="http://<?=SITE_SERVER_NAME?><?=$APPLICATION->GetCurPage()?>" data-numposts="20" data-colorscheme="light" data-width="728"></div>
                    </div>
                </div>
            <?else:?>
                <div class="tab-str-wrapper" >
                    <div class="nav-tab-str-title">
                        Reviews
                    </div>
                    <ul class="nav nav-tabs">
                        <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <script>
                            $(function(){
                                ajax_load = 1;
                                $.ajax({
                                    'url':'/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/reviews.php?id=<?=$element_id?>&wr=<?=$without_reviews?>&url=<?=$APPLICATION->GetCurPage()?>&NEW_DESIGN=Y&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>'
                                })
                                    .done(function(data){
                                        $("#reviews").html(data);
                                        ajax_load = 0;
                                    });
                            });
                        </script>
                        <li class="active"><a href="#reviews" data-toggle="tab">Reviews</a></li>
                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content float-review-block-wrapper" >
                    <div class="tab-pane active sm reviews network-design-reviews" id="reviews"></div>
                </div>
            <?endif;?>
                <div class="right-side">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0",
                            'FROM_RIGHT_OF_REVIEWS' => 'Y'
                        ),
                        false
                    );?>
                </div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>


            <?if($count["NEWS_COUNT"]||$count["VIDEO_NEWS_COUNT"]||$count["OVERVIEWS_COUNT"]||$count["PHOTO_COUNT"])://?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        Restaurant news
                    </div>
                    <ul class="nav nav-tabs">

                        <?if ($count["PHOTO_COUNT"]):?>
                            <li class="">
                                <a id="photo_link" href="#photoreviews" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/photoreviews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Photoreports</a>
                            </li>
                        <?if(!$count["NEWS_COUNT"]&&!$count["VIDEO_NEWS_COUNT"]&&!$count["OVERVIEWS_COUNT"]):?>
                            <script>
                                $(function(){
                                    $("#photo_link").click();
                                })
                            </script>
                        <?endif;?>
                        <?endif;?>

                        <?if ($count["OVERVIEWS_COUNT"]):?>
                            <li >
                                <a id="overviews_link" href="#overviews" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/overviews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Overviews</a><span>|</span>
                            </li>
                        <?if(!$count["NEWS_COUNT"]&&!$count["VIDEO_NEWS_COUNT"]):?>
                            <script>
                                $(function(){
                                    $("#overviews_link").click();
                                })
                            </script>
                        <?endif;?>
                        <?endif;?>

                        <?if ($count["VIDEO_NEWS_COUNT"]):?>
                            <li>
                                <a href="#videonews" id="videonews_link" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/videonews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Video news</a><span>|</span>
                            </li>
                        <?if(!$count["NEWS_COUNT"]):?>
                            <script>
                                $(function(){
                                    $("#videonews_link").click();
                                })
                            </script>
                        <?endif;?>
                        <?endif?>

                        <?if ($count["NEWS_COUNT"]):?>
                            <li class="active" ><a href="#news" id="news_link" data-toggle="tab">News</a><span>|</span></li>
                        <?endif;?>
                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content">
                    <?if ($count["NEWS_COUNT"]):?>
                        <div class="tab-pane medium active" id="news">
                            <?  $arNewsIB = getArIblock("news", CITY_ID);
                            global $arrNewsFilter;
                            if ($arNewsIB["ID"]):
                                $arrNewsFilter = Array("PROPERTY_RESTORAN"=>(int)$element_id);
                                $APPLICATION->IncludeComponent(
                                    "bitrix:news.list",
                                    "detailed_news_rest",
                                    Array(
                                        "DISPLAY_DATE" => "Y",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "Y",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "news",
                                        "IBLOCK_ID" => $arNewsIB["ID"],
                                        "NEWS_COUNT" => "2",
                                        "SORT_BY1" => "ACTIVE_FROM",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "arrNewsFilter",
                                        "FIELD_CODE" => array("DETAIL_PICTURE"),
                                        "PROPERTY_CODE" => array("COMMENTS","RESTORAN"),
                                        "CHECK_DATES" => "N",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "150",
                                        "ACTIVE_DATE_FORMAT" => "j F Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => ($_REQUEST["letn"]=="Y")?"letnie_verandy":"",
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "14402",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "REST_ID" => (int)$element_id,
                                        "WITHOUT_LINK" => ($_REQUEST["letn"]=="Y")?"Y":""
                                    ),
                                    false
                                );
                            endif;?>
                        </div>
                    <?endif;?>
                    <?if ($count["VIDEO_NEWS_COUNT"]):?>
                        <div class="tab-pane medium" id="videonews"></div>
                    <?endif?>
                    <?if ($count["OVERVIEWS_COUNT"]):?>
                        <div class="tab-pane medium" id="overviews"></div>
                    <?endif;?>
                    <?if ($count["PHOTO_COUNT"]):?>
                        <div class="tab-pane medium" id="photoreviews"></div>
                    <?endif;?>
                </div>
            <?endif?>

            <?if ($count["BLOG_COUNT"]||$count["MC_COUNT"]||$count["INTERVIEW_COUNT"]):?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        <?//TODO заменить lang?>
                        Blogs
                    </div>
                    <ul class="nav nav-tabs">
                        <?if ($count["BLOG_COUNT"]):?>
                            <li class="active"><a href="#blogs" id="blogs_link" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/blogs.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Blogs</a></li>
                            <script>
                                $(function(){
                                    $("#blogs_link").click();
                                })
                            </script>
                        <?endif;?>
                        <?if ($count["MC_COUNT"]):?>
                            <li class="<?//if(!$count["BLOG_COUNT"])echo "active";?>">
                                <a href="#masterclass" id="master_link" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/masterclass.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Master Classes</a>
                            </li>
                        <?if(!$count["BLOG_COUNT"]):?>
                            <script>
                                $(function(){
                                    $("#master_link").click();
                                })
                            </script>
                        <?endif;?>
                        <?endif;?>
                        <?if ($count["INTERVIEW_COUNT"]):?>
                            <li class="<?//if(!$count["BLOG_COUNT"]&&!$count["MC_COUNT"])echo "active";?>">
                                <a id="interview_link" href="#interview" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/interviews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Interview</a>
                            </li>
                        <?if(!$count["BLOG_COUNT"]&&!$count["MC_COUNT"]):?>
                            <script>
                                $(function(){
                                    $("#interview_link").click();
                                })
                            </script>
                        <?endif;?>
                        <?endif;?>
                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content">
                    <?if ($count["BLOG_COUNT"]):?>
                        <div class="tab-pane medium active" id="blogs"></div>
                    <?endif;?>
                    <?if ($count["MC_COUNT"]):?>
                        <div class="tab-pane medium" id="masterclass"></div>
                    <?endif;?>

                    <?if ($count["INTERVIEW_COUNT"]):?>
                        <div class="tab-pane medium" id="interview"></div>
                    <?endif;?>
                </div>
            <?endif;?>


            <?
            if ($RGROUP["ID"]): //  ресторанная группа
            ?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        Restaurant group <?=$RGROUP["NAME"]?>
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane active sm" id="restoran-group">
                        <?
                        global $arSimilar;
                        $arSimilar["!ID"] = $element_id;?>
                        <?$arSimilar["PROPERTY_rest_group"] = $RGROUP["ID"];?>
                        <?$APPLICATION->IncludeComponent("restoran:catalog.list", "restoran_group", array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arIB["ID"],
                            "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                            "NEWS_COUNT" => "4",
                            "SORT_BY1" => "property_sleeping",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arSimilar",
                            "PROPERTY_CODE" => Array("RATIO"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_SHADOW" => "Y",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Рестораны",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "NO_SLEEP" => "Y",
                            "REST_PROPS"=>"Y"
                        ),
                            false
                        );?>
                    </div>
                </div>
            <?endif;?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>

            <?if($count["CONTACTS"]==1):?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        Nearby restaurants
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active sm">
                        <?
                        if (!$_REQUEST["pageRestSort"])
                            $_REQUEST["pageRestSort"] = "distance";
                        if (!$_REQUEST["by"])
                            $_REQUEST["by"] = "asc";
                        $_REQUEST["set_filter"] = "Y";
                        global $arrFilter;


                        $_REQUEST['lat'] = $count["LAT"];
                        $_REQUEST['lon'] = $count["LON"];
                        $arrFilter["!ID"] = $element_id;
                        $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                        //$arrFilter["!PROPERTY_no_mobile_VALUE"] = "Да";
                        $arrFilter["PROPERTY_REST_NETWORK"] = false;    //  сеть ресторанов

                        //$arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                        $APPLICATION->IncludeComponent("restoran:restoraunts.list", "simular_in_detail_bottom", Array(
                            "main" => "Y",
                            "NO_SLEEP_TO_NEAR" => "Y",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID),
                            "PARENT_SECTION_CODE" => "restaurants", // Код раздела
                            "NEWS_COUNT" => 4,
                            "SORT_BY1" => ($_REQUEST["pageRestSort"] ? $_REQUEST["pageRestSort"] : "NAME"),
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => $sort2,
                            "SORT_ORDER2" => $sortOreder2,
                            "FILTER_NAME" => "arrFilter",
                            "PROPERTY_CODE" => array(
                                0 => "type",
                                1 => "kitchen",
                                2 => "average_bill",
                                3 => "opening_hours",
                                4 => "phone",
                                5 => "address",
                                6 => "subway",
                                7 => "RATIO",
                            ),
                            "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                            "AJAX_MODE" => "N", // Включить режим AJAX
                            "AJAX_OPTION_SHADOW" => "Y",
                            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
                            "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
                            "CACHE_TIME" => "36000076", // Время кеширования (сек.)
                            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "N", // Учитывать права доступа
                            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
                            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                            "SET_TITLE" => "N", // Устанавливать заголовок страницы
                            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                            "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
                            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                            "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
                            "PAGER_TITLE" => "Рестораны", // Название категорий
                            "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
                            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
                            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
                            "DISPLAY_DATE" => "Y", // Выводить дату элемента
                            "DISPLAY_NAME" => "Y", // Выводить название элемента
                            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "N", // Выводить текст анонса
                            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                            "REST_PROPS"=>"Y"
                        ), false
                        );?>
                    </div>
                </div>
            <?elseif($similar_rest):?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        Related restaurants
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active sm">
                        <? global $arSimilarFilter;
                        $arSimilarFilter = Array("ID"=>$similar_rest);
                        $APPLICATION->IncludeComponent("restoran:catalog.list", "simular_in_detail_bottom", array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arIB["ID"],
                            "PARENT_SECTION_CODE" => "",//$_REQUEST["CATALOG_ID"],
                            "NEWS_COUNT" => "4",
                            "SORT_BY1" => "rand",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arSimilarFilter",
                            "PROPERTY_CODE" => $arParams["SIMILAR_PROPERTIES"],
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_SHADOW" => "Y",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000001",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Рестораны",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "REST_PROPS"=>"Y"
                        ),
                            false
                        );?>
                    </div>
                </div>
            <?endif;?>

            <?endif//404?>
        </div>
    </div>
<?else:?>
    <?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/en_old_detail_page.php");?>
<?endif;?>
    <br /><br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>