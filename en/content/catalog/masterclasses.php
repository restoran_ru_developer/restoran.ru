<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мастер-классы");
// get IB params from iblock code
$rest = RestIBlock::GetRestIDByCode($_REQUEST["RESTOURANT"]);
global $arrFilterTop4;
$arrFilterTop4 = Array("PROPERTY_RESTORAN"=>$rest);
?>
<?$APPLICATION->IncludeComponent(
    	"restoran:catalog.list",
    	"news",
    	Array(
    		"DISPLAY_DATE" => "N",
    		"DISPLAY_NAME" => "Y",
    		"DISPLAY_PICTURE" => "Y",
    		"DISPLAY_PREVIEW_TEXT" => "N",
    		"AJAX_MODE" => "N",
    		"IBLOCK_TYPE" => "cookery",
    		"IBLOCK_ID" => 145,
    		"NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "20"),
    		"SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "ACTIVE_FROM"),
    		"SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    		"SORT_BY2" => "",
    		"SORT_ORDER2" => "",
    		"FILTER_NAME" => "arrFilterTop4",
    		"FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
    		"PROPERTY_CODE" => array("COMMENTS","RESTORAN"),
    		"CHECK_DATES" => "N",
    		"DETAIL_URL" => "",
    		"PREVIEW_TRUNCATE_LEN" => "120",
    		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
    		"SET_TITLE" => "Y",
    		"SET_STATUS_404" => "N",
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    		"ADD_SECTIONS_CHAIN" => "N",
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
    		"PARENT_SECTION" => "",
    		"PARENT_SECTION_CODE" => "",
    		"CACHE_TYPE" => "Y",
    		"CACHE_TIME" => "7200",
    		"CACHE_FILTER" => "Y",
    		"CACHE_GROUPS" => "N",
    		"DISPLAY_TOP_PAGER" => "N",
    		"DISPLAY_BOTTOM_PAGER" => "Y",
    		"PAGER_TITLE" => "Новости",
    		"PAGER_SHOW_ALWAYS" => "N",
    		"PAGER_TEMPLATE" => "search_rest_list",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    		"PAGER_SHOW_ALL" => "N",
    		"AJAX_OPTION_JUMP" => "N",
    		"AJAX_OPTION_STYLE" => "Y",
    		"AJAX_OPTION_HISTORY" => "N",
                "REST_ID" => $rest
    	),
    false
    );
?>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>