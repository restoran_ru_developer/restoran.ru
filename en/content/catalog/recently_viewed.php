<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Недавно просмотренное");
?>
<div id="news">	
    <div class="left">
        <h1>Недавно просмотренное</h1>
            <ul class="tabs" ajax="ajax" ajax_url="/bitrix/templates/main/components/restoran/recently_viewed.list/.default/ajax.php?part=">
                <li>
                    <a href="catalog">
                            <div class="left tab_left"></div>
                            <div class="left name">Рестораны</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>
                <!--<li>
                    <a href="kupons">
                            <div class="left tab_left"></div>
                            <div class="left name">Купоны</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>-->
                <li>
                    <a href="cookery">
                            <div class="left tab_left"></div>
                            <div class="left name">Книга рецептов</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>
                <!--<li>
                    <a href="birzha">
                            <div class="left tab_left"></div>
                            <div class="left name">Биржа труда</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>-->
            </ul>
            <!-- tab "panes" -->
            <div class="panes">
                <div class="pane"></div>
                <div class="pane"></div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
            <br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "bottom_content_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
    </div>
    <div class="right">
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_2_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
            </div>
            <br /><br />
            <div class="top_block">Популярные</div>
            <?
            $arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
            $arrFilterTop4["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "show_counter",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "popular"
                ),
            false
            );?>
            <br /><br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>