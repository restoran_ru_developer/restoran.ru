<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	
      
     <?$APPLICATION->IncludeComponent(
	"restoran:editor",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest")				
		),		
		"SECTION_ID"=>$_REQUEST["SECTION_ID"],
		"IBLOCK_ID"=>54,
		"PARENT_SECTION"=>"",
		"DEFAULT_SECTION"=>44327,
		"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_NAME", "VALUE_FROM"=>"SECTION__NAME"),
			array("NAME"=>"Анонс публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_DESCRIPTION", "VALUE_FROM"=>"SECTION__DESCRIPTION"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"SECTION_PICTURE", "VALUE_FROM"=>"SECTION__PICTURE")
		),
		"TAG_SECTION"=>44330,
		"NEW_ARTICLE"=>$_REQUEST["NEW_ARTICLE"]
		
	),
false
);?>  
      
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>