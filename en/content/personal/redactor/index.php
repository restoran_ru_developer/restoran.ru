<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет пользователя");
?>
<div id="content">
    <h1>Личный кабинет</h1>
    <?$APPLICATION->IncludeComponent("bitrix:main.profile", "users_profile", Array(
        "USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
        "AJAX_MODE" => "N",	// Включить режим AJAX
        "USER_PROPERTY" => "",	// Показывать доп. свойства
        "SEND_INFO" => "N",	// Генерировать почтовое событие
        "CHECK_RIGHTS" => "N",	// Проверять права доступа
        "AJAX_OPTION_SHADOW" => "Y",	// Включить затенение
        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        ),
        false
    );?>
</div>
<div id="rating_overlay" style="width:600px;">
	<div class="close"></div>                           
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>