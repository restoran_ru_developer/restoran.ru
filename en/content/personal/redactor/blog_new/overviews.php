<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Обзоры");
?>

<?$APPLICATION->IncludeComponent(
	"restoran:blog.publications",
	"razdel_publications",
	Array(
		"IBLOCK_TYPE" => "overviews",
		"IBLOCK_ID" => 51,
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => "",
		"SECTION_USER_FIELDS" => array("UF_SECTION_COMM_CNT"),
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"ARTICLE_EDOTOR_PAGE"=>"/content/personal/redactor/blog/editor/overviews.php",
		"NOT_SHOW_PARENT"=>"Y",
		"NOT_SHOW_SECTIONS"=>array(44332)
	),
false
);?> 


