<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	
     <?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Шаг", "CODE"=>"add_step"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),
			array("NAME"=>"Видео", "CODE"=>"add_video")			
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Навание рецепта", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			/*array("NAME"=>"Родительский раздел", "TYPE"=>"select", "CODE"=>"SECTION_ID", "VALUE_FROM"=>"IBLOCK_SECTION_ID", "VALUES_LIST"=>"SECTIONS"),*/
			array("NAME"=>"Анонс публикации", "TYPE"=>"short_text", "CODE"=>"PREVIEW_TEXT", "VALUE_FROM"=>"PREVIEW_TEXT"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"DETAIL_PICTURE", "VALUE_FROM"=>"DETAIL_PICTURE"),
			array("NAME"=>"Порций", "TYPE"=>"short_text", "CODE"=>"PROPERTY_porc", "VALUE_FROM"=>"PROPERTIES__porc__VALUE"),
			array("NAME"=>"Кухня", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_cook", "VALUE_FROM"=>"PROPERTIES__cook__VALUE", "VALUES_LIST"=>"PROPS__cook__LIST"),
			array("NAME"=>"Повод", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_povod", "VALUE_FROM"=>"PROPERTIES__povod__VALUE", "VALUES_LIST"=>"PROPS__povod__LIST"),
			array("NAME"=>"Предпочтения", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_predp", "VALUE_FROM"=>"PROPERTIES__predp__VALUE", "VALUES_LIST"=>"PROPS__predp__LIST"),
			array("NAME"=>"Время приготовления", "TYPE"=>"select", "CODE"=>"PROPERTY_prig_time", "VALUE_FROM"=>"PROPERTIES__prig_time__VALUE", "VALUES_LIST"=>"PROPS__prig_time__LIST"),
			array("NAME"=>"Приготовление", "TYPE"=>"select", "CODE"=>"PROPERTY_prig", "VALUE_FROM"=>"PROPERTIES__prig__VALUE", "VALUES_LIST"=>"PROPS__prig__LIST"),
			array("NAME"=>"Категории", "TYPE"=>"select", "CODE"=>"PROPERTY_cat", "VALUE_FROM"=>"PROPERTIES__cat__VALUE", "VALUES_LIST"=>"PROPS__cat__LIST"),
			array("NAME"=>"Основной ингридиент", "TYPE"=>"select", "CODE"=>"PROPERTY_osn_ingr", "VALUE_FROM"=>"PROPERTIES__osn_ingr__VALUE", "VALUES_LIST"=>"PROPS__osn_ingr__LIST"),
			array("NAME"=>"Сложность", "TYPE"=>"select", "CODE"=>"PROPERTY_slognost", "VALUE_FROM"=>"PROPERTIES__slognost__VALUE", "VALUES_LIST"=>"PROPS__slognost__LIST")
		),
		"SHOW_VBLOCK"=>"Y",
		"TAG_SECTION"=>44203,
		"CAN_ADD_TEXT"=>"Сначала заполните обязательные поля. Затем создайте запись, используя доступные модули. Например, к стандартным «дата и время проведения», «мето проведения» и «текстовое поле» мероприятия раздела «Афиша» можно добавить фотографии и видео, дать красивую ссылку на ресторан («Упомянуть ресторан»), добавить цитату с фотографией автора («Прямая речь»).  Дополнительне модули можно менять местами.",
		"BACK_LINK"=>"/content/personal/redactor/blog/#video_recepts.php"
		
	),
false
);?>      
      
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>