<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	

      
      
      <?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),
			array("NAME"=>"Видео", "CODE"=>"add_video")					
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Анонс публикации", "TYPE"=>"short_text", "CODE"=>"PREVIEW_TEXT", "VALUE_FROM"=>"PREVIEW_TEXT"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"DETAIL_PICTURE", "VALUE_FROM"=>"DETAIL_PICTURE"),
			array("NAME"=>"Показывать с", "TYPE"=>"short_text2", "CODE"=>"ACTIVE_FROM", "VALUE_FROM"=>"ACTIVE_FROM"),
			array("NAME"=>"Показывать до", "TYPE"=>"short_text2", "CODE"=>"ACTIVE_TO", "VALUE_FROM"=>"ACTIVE_TO"),
		),
		"SHOW_VBLOCK"=>"Y",
		"TAG_SECTION"=>44495,
		"CAN_ADD_TEXT"=>"Сначала заполните обязательные поля. Затем создайте запись, используя доступные модули. Например, к стандартным «дата и время проведения», «мето проведения» и «текстовое поле» мероприятия раздела «Афиша» можно добавить фотографии и видео, дать красивую ссылку на ресторан («Упомянуть ресторан»), добавить цитату с фотографией автора («Прямая речь»).  Дополнительне модули можно менять местами.",
		"BACK_LINK"=>"/content/personal/redactor/blog/#video_news.php"
		
	),
false
);?>
      
      
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>