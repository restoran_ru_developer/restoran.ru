<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Редактирование ресторана");
?>

<?
$APPLICATION->AddHeadString('<link href="/tpl/css/chosen/chosen.css"  type="text/css" rel="stylesheet" />', true);
$APPLICATION->AddHeadScript('/tpl/css/chosen/chosen.jquery.js');
$APPLICATION->AddHeadScript('/tpl/js/jquery.checkbox_2.js');
?>

<div id="content">
    <div class="left" style="width:720px;">
        <?$APPLICATION->IncludeComponent(
        "restoran:restoraunts.edit_form",
        "",
        Array(
            "USER_ID" => $USER->GetID(),
        ),
        false
    );?>
    </div>
    <div class="clear"></div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>