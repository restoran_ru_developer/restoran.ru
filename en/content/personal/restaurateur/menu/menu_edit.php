<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Меню ресторана");
?>
<div id="content">
	<?
	$APPLICATION->AddHeadScript('/tpl/js/jQuery.fileinput.js');
	$APPLICATION->AddHeadScript('/tpl/js/jquery.form.js');
	$APPLICATION->AddHeadScript('/tpl/js/chosen.jquery.js');
	?>
	<script type="text/javascript">
	
		
		$(".add_new_section").live("click",function(){
			var lnk = $(this).attr("href");
			$.post(lnk, function(html){
		
				if (!$("#rzdl_modal").size()){
					$("<div class='popup popup_modal' id='rzdl_modal'></div>").appendTo("body");                                                               
				}
        
		 		$('#rzdl_modal').html(html);

				setCenter($("#rzdl_modal"));
				$("#rzdl_modal").fadeIn("300");
			
				updt_frm_rzd();

			});

			return false;	
		});
		
		function updt_frm_rzd(){
		
			//проверка формы
			function check_editor_form2(a,f,o){
				var ret=true;
				o.dataType = "html";
				return ret;
			}

			//Отправка формы
			$('#rzdl_modal form').ajaxForm({
				beforeSubmit: check_editor_form2,
				success: function(data) {
					console.log(data);
			
					window.location.reload();
			
				}
			});
		}
		
		$(".menu_menu .del_section").live("click",function(){
			var lnk = $(this).attr("href");
			$.post(lnk, function(html){
				window.location.reload();
			});
			
			return false;
		});
			
		
		$(".menu_menu .edite_section").live("click",function(){
	
			var lnk = $(this).attr("href");
			$.post(lnk, function(html){
		
				if (!$("#rzdl_modal").size()){
					$("<div class='popup popup_modal' id='rzdl_modal'></div>").appendTo("body");                                                               
				}
        
		 		$('#rzdl_modal').html(html);

				setCenter($("#rzdl_modal"));
				$("#rzdl_modal").fadeIn("300");
			
				updt_frm_rzd();

			});
			return false;
		});
                $(document).ready(function(){
                    $("#from_csv").click(function(){
                        var lnk = "/tpl/ajax/csv.php?ELEMENT=<?=$_REQUEST["ID"]?>&<?=bitrix_sessid_get()?>";
			$.post(lnk, function(html){
                            if (!$("#csv_modal").size()){
                                $("<div class='popup popup_modal' id='csv_modal'></div>").appendTo("body");                                                               
                            }
                            
                            $('#csv_modal').html(html);

                            setCenter($("#csv_modal"));
                            $("#csv_modal").fadeIn("300");
			});
                        
                    });
                });
	</script>
	<?
	if($_REQUEST["ID"]>0){
		$res = CIBlockElement::GetByID($_REQUEST["ID"]);
		if($ar_res = $res->GetNext()){
			
			
		}
	?>
	<h2>Меню ресторана <?=$ar_res["NAME"]?></h2>       
        <?//if ($_REQUEST["SECTION_ID"]):?>
            <div style="position:absolute;top:0px; right:200px;">            
                <input type="button" id="from_csv" class="light_button" value="Загрузить из файла" />
            </div>
        <?//endif;?>
  	<?
  	
  		//запрашиваем id секции с меню    	
		$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>$_REQUEST["ID"]);
		$db_list = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, true);
		if($ar_result = $db_list->GetNext()){
			$_REQUEST["PARENT_SECTION_ID"]=	$ar_result["ID"];
			if($_REQUEST["SECTION_ID"]=="") $_REQUEST["SECTION_ID"]=$_REQUEST["PARENT_SECTION_ID"];
  			
  			
  					//v_dump($_REQUEST["SECTION_ID"]);
  					//v_dump($_REQUEST["PARENT_SECTION_ID"]);
  					
                	$APPLICATION->IncludeComponent(
      		        "bitrix:catalog",
            		"dostavka_redactor",
		            Array(
                    "AJAX_MODE" => "Y",
                    "SEF_MODE" => "N",
                    "IBLOCK_TYPE" => "menu",
                    "IBLOCK_ID" => "151",
                    "USE_FILTER" => "N",
                    "USE_REVIEW" => "N",
                    "USE_COMPARE" => "N",
                    "SHOW_TOP_ELEMENTS" => "Y",
                    "PAGE_ELEMENT_COUNT" => "12",
                    "LINE_ELEMENT_COUNT" => "3",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "LIST_PROPERTY_CODE" => array("map"),
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LIST_META_KEYWORDS" => "-",
                    "LIST_META_DESCRIPTION" => "-",
                    "LIST_BROWSER_TITLE" => "-",
                    "DETAIL_PROPERTY_CODE" => array("map"),
                    "DETAIL_META_KEYWORDS" => "map",
                    "DETAIL_META_DESCRIPTION" => "map",
                    "DETAIL_BROWSER_TITLE" => "map",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "PRICE_CODE" => array("BASE"),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "LINK_IBLOCK_TYPE" => "",
                    "LINK_IBLOCK_ID" => "",
                    "LINK_PROPERTY_SID" => "",
                    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                    "USE_ALSO_BUY" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "TOP_ELEMENT_COUNT" => "9",
                    "TOP_LINE_ELEMENT_COUNT" => "3",
                    "TOP_ELEMENT_SORT_FIELD" => "sort",
                    "TOP_ELEMENT_SORT_ORDER" => "asc",
                    "TOP_PROPERTY_CODE" => array("map"),
                    "VARIABLE_ALIASES" => Array(
                            "SECTION_ID" => "SECTION_ID",
                            "ELEMENT_ID" => "ELEMENT_ID"
                    ),
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "Y",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
            ),
   		 	false
    		);
    		
    	}
    }
    		?>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>