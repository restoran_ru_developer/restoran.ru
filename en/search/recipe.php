<?
global $arrFilter;
//$arrFilter = array("PARAMS" => array("iblock_section" => Array(234,235,240), "depth" => 3));
?>
<?$APPLICATION->IncludeComponent(
        "bitrix:search.page",
        "news",
        Array(
                "USE_SUGGEST" => "N",
                "AJAX_MODE" => "N",
                "RESTART" => "Y",
                "NO_WORD_LOGIC" => "N",
                "USE_LANGUAGE_GUESS" => "N",
                "CHECK_DATES" => "Y",
                "USE_TITLE_RANK" => "Y",
                "DEFAULT_SORT" => "rank",
                "FILTER_NAME" => "arrFilter",
                "SHOW_WHERE" => "N",
                "SHOW_WHEN" => "N",
                "PAGE_RESULT_COUNT" => "3",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "86400",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Search result",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "search_rest_list",
                "arrFILTER" => array("iblock_cookery"),
                "arrFILTER_iblock_cookery" => array("139"),
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "SECTION_NAME" => "Recipes",
                "SEARCH_IN" => "recepts"
        )
);?>
<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "news",
    Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "N",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "date",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "3",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86400",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Search result",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_cookery"),
        "arrFILTER_iblock_cookery" => array(145),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "SECTION_NAME" => "Master classes",
        "SEARCH_IN" => "master_classes"
    ),
false
);?>