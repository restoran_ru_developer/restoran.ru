<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "reviews",
    Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "Y",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "20",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "7200",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Search result",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_reviews"),
        "arrFILTER_iblock_reviews" => array($arReviewsIB["ID"]),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "SECTION_NAME" => "Reviews"
    ),
false
);?>