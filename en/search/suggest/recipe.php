<?
if(!$_REQUEST["q"])
    return;
?>
<?
//global $arrFilter;
//$arrFilter = array("PARAMS" => array("iblock_section" => Array(234,235,240), "depth" => 3));
?>
<?
$obCache = new CPHPCache;
// время кеширования - 60 минут * 24
$q = $_REQUEST["q"];
$life_time = 60*60*27;
$cache_id = $q."_revipes_1".CITY_ID.LANGUAGE_ID;
$SEARCH_RESULT = '';

if($obCache->InitCache($life_time, $cache_id, "/")):
    $vars = $obCache->GetVars();
    $SEARCH_RESULT = $vars["SEARCH_RESULT"];
else :
    CModule::IncludeModule("iblock");
    $q = ToLower($q);
    $lang = get_lang($q);
    $shown_category_title = false;
    if ($lang=="ru")
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"cookery","IBLOCK_ID"=>2606,"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $arFileTmp = CFile::ResizeImageGet(
                $ar_res['DETAIL_PICTURE'],
                array("width" => 40, "height" => 40),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            if(!$shown_category_title) {
                $shown_category_title = true;
                $SEARCH_RESULT .= $ar_res["NAME"] . "###" . $ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."###recipe"."\n";
            }
            else {
                $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."\n";
            }
        }
        $q = decode2anotherlang_ru($q);
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"cookery","IBLOCK_ID"=>2606,"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $arFileTmp = CFile::ResizeImageGet(
                $ar_res['DETAIL_PICTURE'],
                array("width" => 40, "height" => 40),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            if(!$shown_category_title) {
                $shown_category_title = true;
                $SEARCH_RESULT .= $ar_res["NAME"] . "###" . $ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."###recipe"."\n";
            }
            else {
                $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."\n";
            }
        }
        if($SEARCH_RESULT!=''){
            $SEARCH_RESULT .= 'Показать еще рецепты###last###/'.CITY_ID.'/search/?q='.$_REQUEST['q'].'&search_in=recipe'."\n";
        }
    }
    elseif ($lang=="en")
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"cookery","IBLOCK_ID"=>2606,"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $arFileTmp = CFile::ResizeImageGet(
                $ar_res['DETAIL_PICTURE'],
                array("width" => 40, "height" => 40),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            if(!$shown_category_title) {
                $shown_category_title = true;
                $SEARCH_RESULT .= $ar_res["NAME"] . "###" . $ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."###recipe"."\n";
            }
            else {
                $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."\n";
            }
        }
        $q = decode2anotherlang_en($q);
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"cookery","IBLOCK_ID"=>2606,"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $arFileTmp = CFile::ResizeImageGet(
                $ar_res['DETAIL_PICTURE'],
                array("width" => 40, "height" => 40),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            if(!$shown_category_title) {
                $shown_category_title = true;
                $SEARCH_RESULT .= $ar_res["NAME"] . "###" . $ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."###recipe"."\n";
            }
            else {
                $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."\n";
            }
        }
        if($SEARCH_RESULT!=''){
            $SEARCH_RESULT .= 'Show more recipes###last###/'.CITY_ID.'/search/?q='.$_REQUEST['q'].'&search_in=recipe'."\n";
        }
    }
    else
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"cookery","IBLOCK_ID"=>2606,"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $arFileTmp = CFile::ResizeImageGet(
                $ar_res['DETAIL_PICTURE'],
                array("width" => 40, "height" => 40),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            if(!$shown_category_title) {
                $shown_category_title = true;
                $SEARCH_RESULT .= $ar_res["NAME"] . "###" . $ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."###recipe"."\n";
            }
            else {
                $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."###".($arFileTmp["src"]?$arFileTmp["src"]:'')."\n";
            }
        }
        if($SEARCH_RESULT!=''){
            $SEARCH_RESULT .= 'Показать еще рецепты###last###/'.CITY_ID.'/search/?q='.$_REQUEST['q'].'&search_in=recipe'."\n";
        }
    }

endif;

if($obCache->StartDataCache()):
    $obCache->EndDataCache(array(
        "SEARCH_RESULT"    => $SEARCH_RESULT
    ));
endif;
echo $SEARCH_RESULT;
?>
<?
//if (!$SEARCH_RESULT)
//{
//    $APPLICATION->IncludeComponent(
//            "bitrix:search.page",
//            "suggest",
//            Array(
//                    "USE_SUGGEST" => "N",
//                    "AJAX_MODE" => "N",
//                    "RESTART" => "Y",
//                    "NO_WORD_LOGIC" => "Y",
//                    "USE_LANGUAGE_GUESS" => "Y",
//                    "CHECK_DATES" => "Y",
//                    "USE_TITLE_RANK" => "Y",
//                    "DEFAULT_SORT" => "rank",
//                    "FILTER_NAME" => "arrFilter",
//                    "SHOW_WHERE" => "N",
//                    "SHOW_WHEN" => "N",
//                    "PAGE_RESULT_COUNT" => "6",
//                    "CACHE_TYPE" => "Y",
//                    "CACHE_TIME" => "86400",
//                    "DISPLAY_TOP_PAGER" => "N",
//                    "DISPLAY_BOTTOM_PAGER" => "N",
//                    "PAGER_TITLE" => "Результаты поиска",
//                    "PAGER_SHOW_ALWAYS" => "N",
//                    "PAGER_TEMPLATE" => "search_rest_list",
//                    "arrFILTER" => array("iblock_cookery"),
//                    "arrFILTER_iblock_cookery" => array("2606"),
//                    "AJAX_OPTION_JUMP" => "N",
//                    "AJAX_OPTION_STYLE" => "Y",
//                    "AJAX_OPTION_HISTORY" => "N"
//            )
//    );
//}?>