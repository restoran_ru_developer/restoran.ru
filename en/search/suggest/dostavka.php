<?
global $arrFilter;
CModule::IncludeModule("iblock");
$arRestIB = getArIblock("catalog", CITY_ID);
$res = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"CODE"=>"dostavka"),false);
if ($ar = $res->GetNext())
    $arrFilter = array("PARAMS" => array("iblock_section" => $ar["ID"], "depth" => 1));
?>
<?
$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "suggest",
    Array(
        "USE_SUGGEST" => "Y",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "Y",
        "USE_LANGUAGE_GUESS" => "Y",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "arrFilter",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "6",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "360000",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_catalog"),
        "arrFILTER_iblock_catalog" => array($arRestIB["ID"]),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        //"SECTION_NAME" => "Обзоры"
    ),
    false
);
?>