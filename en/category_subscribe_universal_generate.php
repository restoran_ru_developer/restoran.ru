<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

// НОВОСТИ SPB
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83653",
    "IBLOCK_ID" => 41,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВОСТИ",
    "CITY" => "spb",
    "ALL_URL" => "http://restoran.ru/spb/news/restoransnewsspb/",
    "ALL_TITLE" => "ВСЕ НОВОСТИ",
    "FILE_NAME" => "news",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// НОВОСТИ MSK
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83649",
    "IBLOCK_ID" => 40,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВОСТИ",
    "CITY" => "msk",
    "ALL_URL" => "http://restoran.ru/msk/news/restoransnewsmsk/",
    "ALL_TITLE" => "ВСЕ НОВОСТИ",
    "FILE_NAME" => "news",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// НОВОСТИ KLD
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "210850",
    "IBLOCK_ID" => 2822,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВОСТИ",
    "CITY" => "kld",
    "ALL_URL" => "http://www.restoran.ru/kld/news/novosti_kld/",
    "ALL_TITLE" => "ВСЕ НОВОСТИ",
    "FILE_NAME" => "news",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// НОВОСТИ TMN
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "191742",
    "IBLOCK_ID" => 2626,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВОСТИ",
    "CITY" => "tmn",
    "ALL_URL" => "http://www.restoran.ru/tmn/news/restoransnewssch/",
    "ALL_TITLE" => "ВСЕ НОВОСТИ",
    "FILE_NAME" => "news",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// НОВЫЕ РЕСТОРАНЫ SPB
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83832",
    "IBLOCK_ID" => 41,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВЫЕ РЕСТОРАНЫ",
    "CITY" => "spb",
    "ALL_URL" => "http://spb.restoran.ru/spb/news/newplacespb/",
    "ALL_TITLE" => "ВСЕ НОВЫЕ РЕСТОРАНЫ",
    "FILE_NAME" => "new_rests",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// НОВЫЕ РЕСТОРАНЫ MSK
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83831",
    "IBLOCK_ID" => 40,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВЫЕ РЕСТОРАНЫ",
    "CITY" => "msk",
    "ALL_URL" => "http://restoran.ru/msk/news/newplacemsk/",
    "ALL_TITLE" => "ВСЕ НОВЫЕ РЕСТОРАНЫ",
    "FILE_NAME" => "new_rests",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// НОВЫЕ РЕСТОРАНЫ KLD
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "211015",
    "IBLOCK_ID" => 2822,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "НОВЫЕ РЕСТОРАНЫ",
    "CITY" => "kld",
    "ALL_URL" => "http://www.restoran.ru/kld/news/novye_restorany/",
    "ALL_TITLE" => "ВСЕ НОВЫЕ РЕСТОРАНЫ",
    "FILE_NAME" => "new_rests",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// ОБЗОРЫ MSK
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "overviews",
    "IBLOCK_ID" => 51,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "ОБЗОРЫ",
    "CITY" => "msk",
    "ALL_URL" => "http://www.restoran.ru/msk/news/restvew/",
    "ALL_TITLE" => "ВСЕ ОБЗОРЫ",
    "FILE_NAME" => "reviews",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// ОБЗОРЫ SPB
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "overviews",
    "IBLOCK_ID" => 117,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "ОБЗОРЫ",
    "CITY" => "spb",
    "ALL_URL" => "http://spb.restoran.ru/spb/news/restvew/",
    "ALL_TITLE" => "ВСЕ ОБЗОРЫ",
    "FILE_NAME" => "reviews",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// ОБЗОРЫ TMN
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "overviews",
    "IBLOCK_ID" => 2632,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "ОБЗОРЫ",
    "CITY" => "tmn",
    "ALL_URL" => "http://www.restoran.ru/tmn/news/restvew/",
    "ALL_TITLE" => "ВСЕ ОБЗОРЫ",
    "FILE_NAME" => "reviews",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// ФОТООТЧЕТЫ SPB
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "photoreports",
    "IBLOCK_ID" => 119,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "ФОТООТЧЕТЫ",
    "CITY" => "spb",
    "ALL_URL" => "http://spb.restoran.ru/spb/photos/",
    "ALL_TITLE" => "ВСЕ ФОТООТЧЕТЫ",
    "FILE_NAME" => "photoreports",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// ФОТООТЧЕТЫ MSK
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "photoreports",
    "IBLOCK_ID" => 55,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "ФОТООТЧЕТЫ",
    "CITY" => "msk",
    "ALL_URL" => "http://restoran.ru/msk/photos/",
    "ALL_TITLE" => "ВСЕ ФОТООТЧЕТЫ",
    "FILE_NAME" => "photoreports",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// ФОТООТЧЕТЫ TMN
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "photoreports",
    "IBLOCK_ID" => 2634,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "ФОТООТЧЕТЫ",
    "CITY" => "tmn",
    "ALL_URL" => "http://www.restoran.ru/tmn/photos/",
    "ALL_TITLE" => "ВСЕ ФОТООТЧЕТЫ",
    "FILE_NAME" => "photoreports",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// АФИША MSK
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "afisha",
    "IBLOCK_ID" => 48,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "АФИША",
    "CITY" => "msk",
    "ALL_URL" => "http://restoran.ru/msk/afisha/",
    "ALL_TITLE" => "ВСЯ АФИША",
    "FILE_NAME" => "afisha",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

// АФИША SPB
$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "category_subscribe_universal", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "afisha",
    "IBLOCK_ID" => 116,
    "SORT_BY" => "ACTIVE_FROM",
    "CHECK_DATES" => "Y",
    "ACTIVE" => "Y",
    "SORT_ORDER" => "DESC",
    ///////////////////////
    "BLOCK_TITLE" => "АФИША",
    "CITY" => "spb",
    "ALL_URL" => "http://spb.restoran.ru/spb/afisha/",
    "ALL_TITLE" => "ВСЯ АФИША",
    "FILE_NAME" => "afisha",
        ///////////////
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

//РЕЦЕПТЫ MSK
$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "category_subscribe_universal", Array(
            ///////////////////////
    "BLOCK_TITLE" => "РЕЦЕПТЫ",
    "CITY" => "msk",
    "ALL_URL" => "http://restoran.ru/content/cookery/",
    "ALL_TITLE" => "ВСЕ РЕЦЕПТЫ",
    "FILE_NAME" => "receipts",
        ///////////////
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "cookery",
    "IBLOCK_ID" => array(139,145),
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "CHECK_DATES" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

//РЕЦЕПТЫ SPB
$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "category_subscribe_universal", Array(
            ///////////////////////
    "BLOCK_TITLE" => "РЕЦЕПТЫ",
    "CITY" => "spb",
    "ALL_URL" => "http://spb.restoran.ru/content/cookery/",
    "ALL_TITLE" => "ВСЕ РЕЦЕПТЫ",
    "FILE_NAME" => "receipts",
        ///////////////
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "cookery",
    "IBLOCK_ID" => array(139,145),
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "CHECK_DATES" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

//РЕЦЕПТЫ TMN
$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "category_subscribe_universal", Array(
            ///////////////////////
    "BLOCK_TITLE" => "РЕЦЕПТЫ",
    "CITY" => "tmn",
    "ALL_URL" => "http://restoran.ru/content/cookery/",
    "ALL_TITLE" => "ВСЕ РЕЦЕПТЫ",
    "FILE_NAME" => "receipts",
        ///////////////
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "cookery",
    "IBLOCK_ID" => array(139,145),
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "CHECK_DATES" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

//РЕЦЕПТЫ KLD
$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "category_subscribe_universal", Array(
            ///////////////////////
    "BLOCK_TITLE" => "РЕЦЕПТЫ",
    "CITY" => "kld",
    "ALL_URL" => "http://restoran.ru/content/cookery/",
    "ALL_TITLE" => "ВСЕ РЕЦЕПТЫ",
    "FILE_NAME" => "receipts",
        ///////////////
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "cookery",
    "IBLOCK_ID" => array(139,145),
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "CHECK_DATES" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

// КРИТИКА SPB
$res = CIBlock::GetList(
                Array(), Array(
            'TYPE' => 'blogs'
                ), true
);
while ($ar_res = $res->Fetch()) {
    $result["ID"][] = $ar_res['ID'];
}

global $arrFilterTop4;
    $arrFilterTop4 = array();
    $arrFilterTop4["CREATED_BY"] = 65;

$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "category_subscribe_universal", Array(
            ///////////////////////
    "BLOCK_TITLE" => "КРИТИКА",
    "CITY" => "spb",
    "ALL_URL" => "http://spb.restoran.ru/spb/blogs/65/",
    "ALL_TITLE" => "ВСЯ КРИТИКА",
    "FILE_NAME" => "blogs",
        ///////////////
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "blogs",
    "IBLOCK_ID" => 157,
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "arrFilterTop4",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CHECK_DATES" => "Y",
    "CACHE_TYPE" => "N", 
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);


// КРИТИКА MSK
global $arrFilterTop4;
    $arrFilterTop4 = array();
    $arrFilterTop4["CREATED_BY"] = 93;

$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "category_subscribe_universal", Array(
            ///////////////////////
    "BLOCK_TITLE" => "КРИТИКА",
    "CITY" => "msk",
    "ALL_URL" => "http://www.restoran.ru/msk/blogs/93/",
    "ALL_TITLE" => "ВСЯ КРИТИКА",
    "FILE_NAME" => "blogs",
        ///////////////
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "blogs",
    "IBLOCK_ID" => 156,
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "arrFilterTop4",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CHECK_DATES" => "Y",
    "CACHE_TYPE" => "N", 
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>