<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
$arOverviewsIB = getArIblock("reviews", $_REQUEST["CITY_ID"]);
if ($_REQUEST["CODE"]):
    $id = RestIBlock::GetRestIDByCode($_REQUEST["CODE"]);
endif;
?>
<div id="content">
	<div class="left" style="width:728px;position:relative">
            <h1 id="opinion_h1" class="with_link">
                <?if (!$_REQUEST["tid"]):?>
                    <a href="javascript:void(0)" class="js" onclick="$('#reviews').slideUp(300)"><?=$APPLICATION->ShowTitle(false)?></a> / <a href="javascript:void(0)" class="js" onclick="$('#reviews').toggle(300)">Добавить отзыв</a>
                <?else:?>
                    <?=$APPLICATION->ShowTitle(false)?> / <a href="javascript:void(0)" class="js" onclick="$('#reviews').toggle(300)">Добавить отзыв</a>
                <?endif;?>
            </h1>
                <?if (!$_REQUEST["tid"]):?>
                    <div class="sorting" style="top:10px">
                        <div class="left">
                            <?                        
                            $excUrlParams = array("video", "photos","?PAGEN_1");
                            $with_photo = $APPLICATION->GetCurPageParam("photos=Y", $excUrlParams);
                            $with_video = $APPLICATION->GetCurPageParam("video=Y", $excUrlParams);       
                            ?>
                            <?if ($_REQUEST["photos"]!="Y"):?>
                                <span><a class="another" href="<?=$with_photo?>"> с фото</a></span>    
                            <?else:?>
                                <span> с фото</span>    
                            <?endif;?>
                            / 
                            <?if ($_REQUEST["video"]!="Y"):?>
                                <span><a class="another" href="<?=$with_video?>"> с видео</a></span>                    
                            <?else:?>
                                <span> с видео</span>                    
                            <?endif;?>
                        </div>
                        <div class="right"></div>                    
                        <div class="clear"></div>
                    </div>
                <?endif;?>
		<?
                if (!$_REQUEST["tid"]):
                    $arReviewsIB = getArIblock("reviews", $_REQUEST["CITY_ID"]);
                    echo '<div id="reviews" style="display:none">';
                    $APPLICATION->IncludeComponent(
                        "restoran:comments_add",
                        "with_rating_new_on_page",
                        Array(
                                "IBLOCK_TYPE" => "reviews",
                                "IBLOCK_ID" => $arReviewsIB["ID"],
                                "ELEMENT_ID" => $_REQUEST["id"],
                                "IS_SECTION" => "N",                        
                        ),false
                    );
                    echo '</div>';
                    if ($id)
                    {
                        global $arrFilter;
                        $arrFilter = array();
                        $arrFilter["PROPERTY_ELEMENT"] = $id;
                    }
                    if ($_REQUEST["photos"]=="Y")
                    {
                        global $arrFilter;
                        $arrFilter["!PROPERTY_photos"] = false;
                    }
                    if ($_REQUEST["video"]=="Y")
                    {
                        global $arrFilter;
                        $arrFilter[0] = Array("LOGIC"=>"OR",
                            Array("!PROPERTY_video"=>false),
                            Array("!PROPERTY_video_youtube"=>false)
                        );                        
                    }
                $APPLICATION->IncludeComponent(
			"restoran:catalog.list",
			"reviews",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "reviews",
				"IBLOCK_ID" => $arOverviewsIB["ID"],
				"NEWS_COUNT" => ($_REQUEST["pageCnt"])?$_REQUEST["pageCnt"]:"20",
				"SORT_BY1" => "created_date",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arrFilter",
				"FIELD_CODE" => array("DATE_CREATE","CREATED_BY","DETAIL_PAGE_URL"),
				"PROPERTY_CODE" => array("ELEMENT","minus","plus","photos","video","video_youtube","COMMENTS"),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "search_rest_list",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
                                "NO_CACHE_TEMPLATE" => "Y"
			),
		   false
		);
                else:?>
                <?
                global $arrFilter;                
                $arrFilter = array();
                $old_id = RestIBlock::getOldOpinionID((int)$_REQUEST["tid"]);
                if ($old_id)
                {                           
                    LocalRedirect($APPLICATION->GetCurPageParam("tid=".$old_id, array("tid","CITY_ID")),true,"301 Moved permanently");
                    $arrFilter["ID"] = $old_id;
                }
                else
                    $arrFilter["ID"] = $_REQUEST["tid"];
                $APPLICATION->IncludeComponent(
			"restoran:catalog.list",
			"review_with_edit",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "reviews",
				"IBLOCK_ID" => $arOverviewsIB["ID"],
				"NEWS_COUNT" => "1",
				"SORT_BY1" => "created_date",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arrFilter",
				"FIELD_CODE" => array("DATE_CREATE","CREATED_BY"),
				"PROPERTY_CODE" => array("ELEMENT","minus","plus","photos","video","video_youtube","COMMENTS"),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "search_rest_list",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			),
		   false
		);?>
            <?endif;?>
	</div>

<div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>