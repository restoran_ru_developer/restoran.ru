<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Редактирование ресторана");
$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor/css/redactor.css"  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadScript('/tpl/js/jquery-ui-1.8.17.custom.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/redactor_list_script.js');
?>

<?
if (CSite::InGroup( array(14,15,1,20))|| (CSite::InGroup( array(23)) && (CITY_ID=="ast" || CITY_ID=="amt")) || (CSite::InGroup( array(24)) && CITY_ID=="tmn")):
$APPLICATION->AddHeadString('<link href="/tpl/css/chosen/chosen.css"  type="text/css" rel="stylesheet" />', true);
$APPLICATION->AddHeadScript('/tpl/css/chosen/chosen.jquery.js');
$APPLICATION->AddHeadScript('/tpl/js/jquery.checkbox_2.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/promise.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/ajax.requester.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/deletefile.ajax.requester.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/window.receive.message.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/paste.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu1/js/jquery-plugin.js');

$APPLICATION->AddHeadString('<link href="/tpl/js/fu/fineuploader.css" rel="stylesheet" type="text/css"/>');  
?>
<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<div id="content">
    <?if ($_REQUEST["ID"]):?>    
	 <?$APPLICATION->IncludeComponent(
        "restoran:restoran.edit_form_new",
        "editor",
        Array(
            "REST_ID" => $_REQUEST["ID"],
        ),
        false
    );?>
	
	<div class="clear"></div>
    <?else:?>
        <?ShowError("Не выбран ресторан")?>
    <?endif;?>
</div>
<?else:?>
    <div id="content">
        <?=ShowError("Нет доступа");?>
    </div>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>