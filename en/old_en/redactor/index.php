<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Редактирование ресторана");
?>
<?
if (CSite::InGroup( array(14,15,1,20))):
//$APPLICATION->AddHeadString('<link href="/tpl/css/chosen/chosen.css"  type="text/css" rel="stylesheet" />', true);
//$APPLICATION->AddHeadScript('/tpl/css/chosen/chosen.jquery.js');
//$APPLICATION->AddHeadScript('/tpl/js/jquery.checkbox_2.js');
?>
<script>
    $(document).ready(function(){
       $("#add_new_rest").click(function(){
           $.ajax({
               type:"POST",
               url:"/tpl/ajax/add_new_rest.php",               
               success:function(data)
               {
                   if(!$("#add_rest_form").size())
                   {
                        $("<div class='popup popup_modal' id='add_rest_form' style='width:340px'></div>").appendTo("body");
                   }
                   $('#add_rest_form').html(data);
                   showOverflow();
                   setCenter($("#add_rest_form"));
                   $("#add_rest_form").css("display","block");
               }
           });
        });
    }); 
</script>
<div id="content">
<!--    <div class="left" style="width:720px;">-->
        <input id="add_new_rest" type="button" class="light_button" value="+ Добавить ресторан" />
        <!--<input type="button" class="dark_button" value="+ Добавить рестораную группу" />-->
        <br /><Br />
        <?$APPLICATION->IncludeComponent(
        "restoran:restorators.restorans",
        "",
        Array(
            "IBLOCK_TYPE" => "catalog",
            "USER_ID" => $USER->GetID(),
        ),
        false
    );?>
<!--    </div>
    <div class="right">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
        <br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
        <br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_3_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
    <div class="clear"></div>-->
</div>
<?else:?>
   <div id="content">
        <p class="font16" style="color:#24A6CF">Приносим свои извинения, данная опция находится в разработке. Но вы сможете ею воспользоваться совсем скоро! </p>
        <Br />
    </div>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>