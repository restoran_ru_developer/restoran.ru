<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();

$q = trim(rawurlencode($_REQUEST["q"]));
if ($_REQUEST["search_in"]=="adres")
{
    $s = file_get_contents("http://geocode-maps.yandex.ru/1.x/?format=json&rspn=1&ll=".COption::GetOptionString("main",CITY_ID."_center")."&spn=3.552069,2.400552&geocode=".$q."&results=5&key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==");
    $s = json_decode($s,true);
    //v_dump($s["response"]["GeoObjectCollection"]["featureMember"]);
    foreach ($s["response"]["GeoObjectCollection"]["featureMember"] as $r)
    {
        echo $r["GeoObject"]["description"]." ".$r["GeoObject"]["name"]."###".$r["GeoObject"]["Point"]["pos"]."\n";

    }
}
if ($_REQUEST["search_in"]=="rest")
{
    $APPLICATION->IncludeComponent(
        "restoran:simple.search",
        "map_suggest",
        Array(
            "COUNT" => 10
        )
    );
}
?>