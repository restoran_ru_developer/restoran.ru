<?
$arAfishaIB = getArIblock("afisha", CITY_ID);
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "afisha",
    Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "Y",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "date",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "15",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86400",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "arrFILTER" => array("iblock_afisha"),
        "arrFILTER_iblock_afisha" => array($arAfishaIB["ID"]),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
false
);?>