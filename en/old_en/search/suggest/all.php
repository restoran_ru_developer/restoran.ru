<?
/*if(!$_REQUEST["q"])
    return;
$arRestIB = getArIblock("catalog", CITY_ID);
$arKuponIB = getArIblock("kupons", CITY_ID);
$arOverviewsIB = getArIblock("overviews", CITY_ID);
$arInterviewIB = getArIblock("interview", CITY_ID);
$arPhotoreportsIB = getArIblock("photoreports", CITY_ID);
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arNewsIB = getArIblock("news", CITY_ID);
$arVideoIB = getArIblock("videonews", CITY_ID);
?>
<?$APPLICATION->IncludeComponent(
        "bitrix:search.page",
        "suggest",
        Array(
                "USE_SUGGEST" => "N",
                "AJAX_MODE" => "N",
                "RESTART" => "Y",
                "NO_WORD_LOGIC" => "Y",
                "USE_LANGUAGE_GUESS" => "Y",
                "CHECK_DATES" => "Y",
                "USE_TITLE_RANK" => "N",
                "DEFAULT_SORT" => "rank",
                "FILTER_NAME" => "",
                "SHOW_WHERE" => "N",
                "SHOW_WHEN" => "N",
                "PAGE_RESULT_COUNT" => "10",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "360000",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Результаты поиска",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "search_rest_list",
                "arrFILTER" => array("iblock_cookery", "iblock_kupons", "iblock_overviews", "iblock_interview", "iblock_photoreports", "iblock_afisha", "iblock_news", "iblock_videonews"),
                "arrFILTER_iblock_cookery" => array("139","145"),
                "arrFILTER_iblock_kupons" => array($arKuponIB["ID"]),
                "arrFILTER_iblock_overviews" => array($arOverviewsIB["ID"]),
                "arrFILTER_iblock_interview" => array($arInterviewIB["ID"]),
                "arrFILTER_iblock_photoreports" => array($arPhotoreportsIB["ID"]),
                "arrFILTER_iblock_afisha" => array($arAfishaIB["ID"]),
                "arrFILTER_iblock_news" => array($arNewsIB["ID"]),
                "arrFILTER_iblock_videonews" => array($arVideoIB["ID"]),
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
        )
);*/
?>
<?
if(!$_REQUEST["q"] || strlen(trim($_REQUEST["q"])) <= 0)
    return;

$APPLICATION->IncludeComponent(
    "restoran:simple.search",
    "suggest",
    Array(
        "COUNT" => 10
    )
);
?>
<?
global $SEARCH_RESULT;
if (!$SEARCH_RESULT)
{
    $_REQUEST["q"] = change_search_adr($_REQUEST['q']);
    $APPLICATION->IncludeComponent(
        "bitrix:search.page",
        "suggest",
        Array(
           "USE_SUGGEST" => "N",
            "AJAX_MODE" => "N",
            "RESTART" => "Y",
            "NO_WORD_LOGIC" => "N",
            "USE_LANGUAGE_GUESS" => "Y",
            "CHECK_DATES" => "Y",
            "USE_TITLE_RANK" => "Y",
            "DEFAULT_SORT" => "rank",
            "FILTER_NAME" => "",
            "SHOW_WHERE" => "N",
            "SHOW_WHEN" => "N",
            "PAGE_RESULT_COUNT" => "10",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "86400",
            "CACHE_NOTES" => "",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Результаты поиска",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "search_rest_list",
            "arrFILTER" => array("iblock_catalog"),
            "arrFILTER_iblock_catalog" => array($arRestIB["ID"]),
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
            //"SECTION_NAME" => "Обзоры"
        ),
    false
    );
}
?>