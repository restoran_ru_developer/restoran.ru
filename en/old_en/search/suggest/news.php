<?
if(!$_REQUEST["q"])
    return;

$arRestNewsIB = getArIblock("news", CITY_ID);
$arVideoIB = getArIblock("videonews", CITY_ID);
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "suggest",
    Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "Y",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "date",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "10",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86400",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_news", "iblock_videonews"),
        "arrFILTER_iblock_news" => array($arRestNewsIB["ID"]),
        "arrFILTER_iblock_iblock_videonews" => array($arVideoIB["ID"]),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
false
);?>