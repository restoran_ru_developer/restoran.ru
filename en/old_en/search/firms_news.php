<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "news",
    Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "N",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "date",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "15",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86400",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_firms_news"),
        "arrFILTER_iblock_firms_news" => array(2423),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "SECTION_NAME" => "Рестораторам",
        "SEARCH_IN" => "firms_news"
    ),
false
);?>
