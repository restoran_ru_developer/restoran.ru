<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Работа");
// get resume iblock info
$arResumeIB = getArIblock("resume", CITY_ID);
$arVacIB = getArIblock("vacancy", CITY_ID);
?>
<div id="content">
<!--<h2>Работа</h2>-->

<?
// examples
// set respond user
//$arFileds = Array(
//    "IBLOCK_ID" => $arResumeIB["ID"], // iblock id
//    "ELEMENT_ID" => 588, // resume id
//    "USER_ID" => $USER->GetID(), // user id (for auth user)
//    "USER_EMAIL" => "kiokumicu@gmail.com", // email (for not auth)
//    "USER_FIO" => "Денис" // user fio (for not auth)
//);
//LaborExchange::respondSet($arFileds);
?>

<?
// get current user resume
global $USER;
?>
    <div id="tabs_block6" class="tabs">
            <ul class="tabs big">
                    <li>                                
                        <a href="#" class="current">
                            <div class="left tab_left"></div>
                            <div class="left name">Мои резюме</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                        </a>                                                                
                    </li>
                    <li><a href="#">
                            <div class="left tab_left"></div>
                            <div class="left name">Мои вакансии</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>                                    
                        </a>
                    </li>
            </ul>
            <div class="panes">
                <div class="pane big" style="display: block; ">
                    <?
                        global $arResumeFilter;
                        /*
                        $arFilterUser = Array(
                            "ACTIVE" => "Y",
                            "ID" => (int)$_REQUEST["USER_ID"],
                        );
                        $arSelectFieldsUser["SELECT"] = Array(
                            "UF_USER_RESUME"
                        );
                        $rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilterUser, $arSelectFieldsUser);
                        if($arUser = $rsUser->Fetch()) {    
                        	if(count($arUser["UF_USER_RESUME"])>0)
                        		$arResumeFilter = Array("ID" => $arUser["UF_USER_RESUME"], "ACTIVE" => Array("Y", "N"));
                        }
                        */
                        
                        $arResumeFilter = Array("CREATED_BY" => (int)$_REQUEST["USER_ID"], "ACTIVE" => Array("Y", "N"));
                        
                        //var_dump($arResumeFilter);
                        if ($arResumeFilter){
                            $APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "user_resumes",
                                Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "resume",
                                    "IBLOCK_ID" => $arResumeIB["ID"],
                                    "NEWS_COUNT" => "20",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arResumeFilter",
                                    "FIELD_CODE" => array("ACTIVE"),
                                    "PROPERTY_CODE" => array("EDUCATION", "EXPERIENCE", "SHEDULE", "WAGES_OF"),
                                    "CHECK_DATES" => "N",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Резюме",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                                ),
                                false
                            );
                           }else{?>
								Нет добавленных резюме
							<?}?>
                           <div style="position:absolute; top:0px; right:0px;"><a href="/users/id<?=$_REQUEST["USER_ID"]?>/work/resume_add/"><input type="button" class="light_button" value="Добавить резюме"/></a></div>
                        
                       
                </div>
                <div class="pane big">
                     <?
                    
                        $arVacFilter = Array("PROPERTY_USER_CONTACT_BIND" => $_REQUEST["USER_ID"], "ACTIVE" => Array("Y", "N"));
                        
                        $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "user_vacancy",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "vacancy",
                                "IBLOCK_ID" => $arVacIB["ID"],
                                "NEWS_COUNT" => "20",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arVacFilter",
                                "FIELD_CODE" => array("ACTIVE"),
                                "PROPERTY_CODE" => array("EDUCATION", "EXPERIENCE", "SHEDULE", "WAGES_OF"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Резюме",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                            false
                        );?>
                    <div style="position:absolute; top:0px; right:0px;"><a href="/users/id<?=$_REQUEST["USER_ID"]?>/work/vacancy_add/"><input type="button" class="light_button" value="Добавить вакансию"/></a></div>
                </div>
            </div>
    </div>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>