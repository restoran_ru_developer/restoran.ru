<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список пользователей");
?>
<div id="content">
    <h1>Список пользователей</h1>
    <!--<form>
        <span class="uppercase">Поиск по пользователям:</span> <input type="text" value="<?=$_REQUEST["user_name"]?>" class="inputtext" name="user_name" style="width:200px" />
        <input type="submit" value="Найти" class="light_button" />
    </form>-->
    <script>
        $(document).ready(function(){
           var params = {  
                changedEl: "#type"
            };
            cuSel(params); 
            
        });
    </script>
    <div id="afisha_filter">
        <form id="a_filter">
            <!--<div style="position: absolute; top:-22px; right:0px;"><a href="/users/list/" class="js">сбросить фильтр</a></div>-->
            <div class="left" style="margin-right:20px;"><span class="uppercase" style="line-height:24px;">ФИльтр:</span> </div>
            <div class="left"><input type="text" value="<?=($_REQUEST["user_name"])?$_REQUEST["user_name"]:""?>" placeholder="Имя пользователя" id="user_name" class="filter_text" name="user_name" style="width:200px" /></div>
            <div class="left"><input type="text" value="<?=($_REQUEST["email"])?$_REQUEST["email"]:""?>" placeholder="email" id="email" class="filter_text" name="email" style="width:200px" /></div>
            <div class="left">
                <select id="type" name="gender">
                    <option value="0" selected="selected">Пол</option>
                    <option <?=($_REQUEST["gender"]=="M")?"selected":""?> value="M">Мужской</option>                    
                    <option <?=($_REQUEST["gender"]=="F")?"selected":""?> value="F">Женский</option>                    
                </select>
            </div>            
            <div class="left" style="margin-right:10px;"><input style="padding:0px 4px; width:96px;" id="afisha_filter_button" type="submit" class="light_button" value="Найти"/></div>
            <div class="left"><a href="/users/moderator/users_list.php" style="text-decoration: none"><input style="" type="button" class="dark_button" value="сбросить фильтр"/></a></div>
            <div class="clear"></div>
        </form>
    </div> 
    <br />
     <?$APPLICATION->IncludeComponent(
	"restoran:user.list",
	"moderator",
	Array(
                "PAGE_COUNT" => 10,
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
                "PAGER_TEMPLATE" => "search_rest_list",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	),
    false
    );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>