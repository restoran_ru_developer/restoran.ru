<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Приглашения");
// get resume iblock info
$arResumeIB = getArIblock2("invites");
global $USER;
?>
<script>
function add_invite(id)
{
        $.ajax({
            type: "POST",
            url: "/tpl/ajax/invite2rest.php",
            data: "ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300");                 
            }
        });
}
function delete_invite(id)
{
    if (confirm("Вы точно хотите отменить данное приглашение?"))
    {
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=delete&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); 
                $('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });
    }
    else
    {
        return false;
    }
}
function del_invite(id)
{
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=delete&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                /*if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); */
                $('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });    
}

function confirm_invite(id)
{
    $.ajax({
        type: "POST",
        url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
        data: "ACTION=confirm&ID="+id+"&<?=bitrix_sessid_get()?>",
        success: function(data) {
            if (!$("#invite2rest_modal").size())
            {
                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
            }
            $('#invite2rest_modal').html(data);
            showOverflow();
            setCenter($("#invite2rest_modal"));
            $("#invite2rest_modal").css("top","150px")
            $("#invite2rest_modal").fadeIn("300"); 
            $('#inv'+id).hide(500);        
            setTimeout("location.reload()",1000);
        }
    });
}

function declain_invite(id,element)
{
    if (confirm("Вы точно хотите отменить данное приглашение?"))
    {
        $.ajax({
            type: "POST",
            url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
            data: "ACTION=declain&ELEMENT="+element+"&ID="+id+"&<?=bitrix_sessid_get()?>",
            success: function(data) {
                if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); 
                $('#inv'+id).hide(500);   
                setTimeout("location.reload()",1000);
            }
        });
    }
    else
        return false;
}
function decl_invite(id,element)
{
    $.ajax({
        type: "POST",
        url: "/bitrix/components/restoran/ajax.invite2restoran/templates/.default/core.php",
        data: "ACTION=declain&ELEMENT="+element+"&ID="+id+"&<?=bitrix_sessid_get()?>",
        success: function(data) {
            /*if (!$("#invite2rest_modal").size())
            {
                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
            }
            $('#invite2rest_modal').html(data);
            showOverflow();
            setCenter($("#invite2rest_modal"));
            $("#invite2rest_modal").css("top","150px")
            $("#invite2rest_modal").fadeIn("300"); */
            $('#inv'+id).hide(500);   
            setTimeout("location.reload()",1000);
        }
    });
}
</script>
<div id="content">
    <div class="left" style="position:relative">
        <div id="tabs_block6" class="tabs">
                <ul class="tabs" history="true">
                        <li>                                
                            <a href="my_invite" class="current">
                                <div class="left tab_left"></div>
                                <div class="left name">Я пригласил</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>                                                                
                        </li>
                        <li><a href="me_invite">
                                <div class="left tab_left"></div>
                                <div class="left name">Меня пригласили</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>                                    
                            </a>
                        </li>
                </ul>
                <div class="panes">
                    <div class="pane" style="display: block; ">
                        <?
                            global $arResumeFilter;
                            $arResumeFilter = Array("CREATED_BY" => (int)$_REQUEST["USER_ID"]);
                            if ($arResumeFilter){
                                $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list_multi",
                                    "my_invites",
                                    Array(
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "N",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "invites",
                                        "IBLOCK_ID" => $arResumeIB["ID"],
                                        "NEWS_COUNT" => "999",
                                        "SORT_BY1" => "ACTIVE_FROM",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "arResumeFilter",
                                        "FIELD_CODE" => array("ACTIVE"),
                                        "PROPERTY_CODE" => array("RESTORAN", "BRON", "USERS", "NA_USERS"),
                                        "CHECK_DATES" => "N",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "ACTIVE_DATE_FORMAT" => "j F Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "CACHE_TYPE" => "N",
                                        "CACHE_TIME" => "30",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "Y",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "Y",
                                        "PAGER_TITLE" => "Резюме",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N"
                                    ),
                                    false
                                );
                            }else{?>
                                                                    Нет добавленных резюме
                                                            <?}?>
                            <div style="position:absolute; top:10px; right:0px;"><input type="button" onclick="add_invite(0)" class="light_button" value="Добавить приглашение"/></div>


                    </div>
                    <div class="pane">
                        <?
                        global $arVacFilter;
                            $arVacFilter = Array("PROPERTY_USERS" => $USER->GetID(), "ACTIVE" => "",
                                Array(
                                    "LOGIC"=>"OR",
                                    Array("PROPERTY_DECLAIN"=>false),
                                    //Array("!PROPERTY_DECLAIN"=>false,">PROPERTY_DECLAIN"=>$USER->GetID(), "<PROPERTY_DECLAIN"=>$USER->GetID()),
                                    Array("!PROPERTY_DECLAIN"=>false,"!><PROPERTY_DECLAIN"=> Array($USER->GetID(),$USER->GetID()))
                                )
                            );
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list_multi",
                                "me_invites",
                                Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "invites",
                                    "IBLOCK_ID" => $arResumeIB["ID"],
                                    "NEWS_COUNT" => "10",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arVacFilter",
                                    "FIELD_CODE" => array("ACTIVE","CREATED_BY"),
                                    "PROPERTY_CODE" => array("RESTORAN", "BRON", "USERS", "NA_USERS"),
                                    "CHECK_DATES" => "N",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "30",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Резюме",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                                ),
                                false
                            );?>                        
                    </div>
                </div>
        </div>
    </div>
    <div class="right" style="width:240px;">
            <div class="baner2">
                <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            </div>
            <br /><br />
            <div class="top_block">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "PROPERTY_restoran_ratio",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",//s
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended"
                ),
            false
            );?>
            <br /><br />
            <div align="right">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
            <br /><br />
        <div class="top_block">Популярные</div>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "show_counter",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "popular"
                ),
            false
            );?>
        <br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br />
        <div align="right">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>