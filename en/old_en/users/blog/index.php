<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Блоги пользователя");
?>
<script>
$(document).ready(function(){
    $("ul.left_menu_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")
        {
            if ($(this).attr("history")=="true")
                var history = true;
            else
                var history = false;
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.left_menu_panes > .left_menu_pane", {                
                effect: 'fade',
                history: history,
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href")+"?USER_ID=<?=$_REQUEST["USER_ID"]?>");			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.left_menu_panes > .left_menu_pane",{effect: 'fade'});
    });  
});
</script>
<div id="content">
    <h2 class="left" style="margin-top:0px"><?=$APPLICATION->ShowTitle(false)?></h2>
    <div class="right">
        
    </div>
    
    	<?$APPLICATION->IncludeComponent("bitrix:menu", "blog", Array(
	"ROOT_MENU_TYPE" => "left_menu",	// Тип меню для первого уровня
	"MENU_CACHE_TYPE" => "N",	// Тип кеширования
	"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	"MAX_LEVEL" => "1",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
	"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"DELAY" => "N",	// Откладывать выполнение шаблона меню
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);?>
    

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>