
<div>Restaurant.Ru is an informational portal about the restaurant life of Russia's two capitals (Moscow &amp; Saint Petersburg. The website provides the most current information for restaurant reviews and ratings, club life, cafes, novelties, and restaurant deals. Everyday a catalogue of restaurants is updated with new listings. </div>
 
<div>Everyday restaurants and cafes of Moscow and Saint Petersburg publish gastronomical news. Our restaurant guides will help you get to know the very best of our gastronomical capitals! The most popular and interesting places with surely be highlighted. </div>
 
<div>Besides, we offer a free service to book a table or banquet in restaurants and cafes in Moscow and Saint Petersburg. If you have a big event coming, we are ready to become your restaurant consultants. To provide you with the best banquet halls of the two capitals. </div>
 
<div>Our section &quot;Culinary&quot; acquired the best gastronomical recipes from Russia and all over the world by world-class chefs of well-known restaurants. </div>
 
<div>Culinary by Restaurant.Ru &ndash; trendy recipes for the holidays as well as every day life.</div>
