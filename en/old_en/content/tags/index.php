<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$arRestIB = getArIblock($_REQUEST["IBLOCK_TYPE_ID"] , CITY_ID);?>
<div id="content">
    <div class="left"  style="width:730px; line-height:24px">
        <h1>Популярные теги</h1>
        <i>
        <?
            $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                "SORT" => "CNT",
                "PAGE_ELEMENTS" => "60",
                "PERIOD" => "",
                "URL_SEARCH" => "/search/index.php",
                "TAGS_INHERIT" => "Y",
                "CHECK_DATES" => "Y",
                "FILTER_NAME" => "",
                "arrFILTER" => array(
                                0 => "iblock_".$_REQUEST["IBLOCK_TYPE_ID"],
                ),
                "arrFILTER_iblock_".$_REQUEST["IBLOCK_TYPE_ID"] => array(
                                0 => $arRestIB["ID"],
                ),
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "36000000",
                "FONT_MAX" => "40",
                "FONT_MIN" => "14",
                "COLOR_NEW" => "24A6CF",
                "COLOR_OLD" => "24A6CF",
                "PERIOD_NEW_TAGS" => "",
                "SHOW_CHAIN" => "Y",
                "COLOR_TYPE" => "N",
                "SEARCH_IN" => $arItem["IBLOCK_TYPE_ID"],
                "WIDTH" => "100%"
                ),
                $component
            );?> 
            </i>
        <div class="clear"></div>
    </div>
    <div class="right" style="width:240px">
        <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
      <br /><br />
      <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>