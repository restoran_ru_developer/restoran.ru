<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Advanced search");
?>
<script>
    $(document).ready(function(){   
        $('.subway_station_extended_filter').live('click', function(e){
            if($('.hidden-checkboxes').find('.station'+$(this).attr('station_id')).attr('checked') !== 'checked'){
                $(this).addClass('selected');
                $('.hidden-checkboxes').find('.station'+$(this).attr('station_id')).attr('checked', 'checked');
                $('.selected-station-container ul').append('<li class="fil" id="selected_station_'+$(this).attr('station_id')+'" val="'+$(this).attr('station_id')+'">'+$(this).attr('val')+'<a style="margin-top:6px;" href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png"></a></li>');
                $('.selected-station-container').show();
            } else {
                $('.hidden-checkboxes').find('.station'+$(this).attr('station_id')).removeAttr('checked');
                $(this).removeClass('selected');
                $('#selected_station_'+$(this).attr('station_id')).remove();
                if( typeof $('.selected-station-container ul li')[0] == 'undefined' ){
                    $('.selected-station-container').hide();
                }
            }
        });
    
        $('.selected-station-container a').live('click', function(e){
            $('.hidden-checkboxes').find('.station'+$(this).parent().attr('val')).removeAttr('checked');
            $('[station_id='+$(this).parent().attr('val')+']').removeClass('selected');
            $(this).parent().remove();
            if( typeof $('.selected-station-container ul li')[0] == 'undefined' ){
                $('.selected-station-container').hide();
            }
        });
    
    });
</script>
<div id="content">	
    <?/*$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "11",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => array("type", "subway", "out_city","average_bill", "kitchen", "credit_cards","menu","children","features","entertainment","ideal_place_for","music","parking","wi_fi","hrs_24"),
		"PRICE_CODE" => array(),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"SAVE_IN_SESSION" => "N"
	)
);*/?>
	<div class="left" style="width:720px;">
	<h1>Advanced search</h1>
		<div id="tabs_block2">
            <ul class="tabs" ajax="ajax" ajax_url="/bitrix/components/restoran/catalog.filter/templates/.default/">
                <li>
                    <a href="rest.php?CITY_ID=<?=CITY_ID?>">
                        <div class="left tab_left"></div>
                        <div class="left name">Restaurants</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="banket.php?CITY_ID=<?=CITY_ID?>">
                        <div class="left tab_left"></div>
                        <div class="left name">Banquet halls</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <!--<li>
                    <a href="dostavka.php">
                        <div class="left tab_left"></div>
                        <div class="left name">Доставка</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>-->
                <!--<li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name">Фирмы</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>-->
            </ul>
            <!-- tab "panes" -->
            <div class="panes">
               <div class="pane" style="display:block"></div>
                <div class="pane"></div>
                <!--<div class="pane"></div>-->
                <!--<div class="pane"></div>-->
            </div>
        </div> 	
	</div>
	<div class="right" style="width:240px">        
            <!--<img src="/bitrix_personal/templates/main/images/right_baner1.png" />-->
            <div class="baner2">
                <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            </div>
            <br /><br />
            <div class="top_block">Recommend</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            //$arrFilterSim["PROPERTY_RECOMENDED_VALUE"] = "Да";
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arPFilter["!PREVIEW_PICTURE"] = false;
            $arPFilter["!PROPERTY_restoran_ratio"] = false;  
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "rand",
                        "SORT_ORDER1" => "",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "10",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended"
                ),
            false
            );?>
            <br /><br />
            <div align="right">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
            <br /><br />
        <div class="top_block">Popular</div>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "show_counter",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "popular"
                ),
            false
            );?>
        <br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br />
        <div align="right">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
    </div>
    <div class="clear"></div>
    <br /><br />
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
    false
    );?>  
    <br /><br />
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>