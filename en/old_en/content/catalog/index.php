<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?>
<?
$arIB = getArIblock("catalog", CITY_ID);
// denied get special rest
$arrFilter["!PROPERTY_top_spec_place_VALUE"] = Array("Да");
$arrFilter["!PROPERTY_str_spec_place_VALUE"] = Array("Да");

if (!$_REQUEST["page"])
{
    $_REQUEST["page"] = 1;
    $_REQUEST["PAGEN_1"] = 1;
}
//v_dump($_REQUEST);
if ($_REQUEST["PROPERTY"]&&$_REQUEST["PROPERTY_VALUE"])
{
    $arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
    $properties = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arRestIB["ID"], "CODE"=>$_REQUEST["PROPERTY"]));
    if ($prop_fields = $properties->GetNext())
    {
        if ($prop_fields["LINK_IBLOCK_ID"])
        {
            $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y","CODE"=>$_REQUEST["PROPERTY_VALUE"]),false,false,Array("ID"));
            if($ar = $res->Fetch())
            {
                $arrFilter["PROPERTY_".$prop_fields["CODE"]] = $ar["ID"];
                    if ($_REQUEST["PROPERTY"]=="kitchen")
                    {
                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_kitchen"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                        if ($r->SelectedRowsCount())
                        {
                            $_REQUEST["PRIORITY_kitchen"]=$ar["ID"];
                        }
                    }
                    elseif($_REQUEST["PROPERTY"]=="type")
                    {                                      
                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_type"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
                        if ($r->SelectedRowsCount())
                        {
                            $_REQUEST["PRIORITY_type"]=$ar["ID"]; 
                        }
                    }
                
            }            
        }
    }
}
else
{
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["kitchen"])==1&&$_REQUEST["arrFilter_pf"]["kitchen"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["kitchen"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/kitchen/".$ar1["CODE"]);
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["type"])==1&&$_REQUEST["arrFilter_pf"]["type"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["type"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/type/".$ar1["CODE"]);
    }
}
?>
<div id="catalog">
    <!--<h1><?=$arResult["NAME"].", ".$arResult["SECTION"]["PATH"][0]["NAME"]?></h1>-->
    <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
    <div class="sort">
        <?
        // set rest sort links
        $excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1","by","index_php?page","index_php");
        $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=new&".(($_REQUEST["pageRestSort"]=="new"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=price&".(($_REQUEST["pageRestSort"]=="price"&&$_REQUEST["by"]=="asc") ? "by=desc": "by=asc"), $excUrlParams);
        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=ratio&".(($_REQUEST["pageRestSort"]=="ratio"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=alphabet&".((($_REQUEST["pageRestSort"]=="alphabet"&&$_REQUEST["by"]=="asc")|| !$_REQUEST["pageRestSort"]) ? "by=desc": "by=asc"), $excUrlParams);
        ?>
        <?//if($_REQUEST["page"] || $_REQUEST["PAGEN_1"]):?>
            <?$by = ($_REQUEST["by"]=="desc")?"z":"a";?>
            <?
            if ($_REQUEST["pageRestSort"] == "new")
            {
                echo '<span class="'.$by.'"><a class="another" href="'.$sortNewPage.'">by newest</a></span> / ';
            }
            else
            {
                echo '<a class="another" href="'.$sortNewPage.'">by newest</a> / ';
            }
            if ($_REQUEST["pageRestSort"] == "price")
            {
                echo '<span class="'.$by.'"><a class="another" href="'.$sortPricePage.'">by average bill</a></span> / ';
            }
            else
            {
                echo '<a class="another" href="'.$sortPricePage.'">by average check</a> / ';
            }
            if ($_REQUEST["pageRestSort"] == "ratio")
            {
                echo '<span class="'.$by.'"><a class="another" href="'.$sortRatioPage.'">by rating</a></span> / ';
            }
            else
            {
                echo '<a class="another" href="'.$sortRatioPage.'">by rating</a> / ';
            }
            if ($_REQUEST["pageRestSort"] == "alphabet" || !$_REQUEST["pageRestSort"])
            {
                echo '<span class="'.$by.'"><a class="another" href="'.$sortAlphabetPage.'">by name</a></span>';
            }
            else
            {
                echo '<a class="another" href="'.$sortAlphabetPage.'">by name</a>';
            }
            ?>        
    </div>
    <?$APPLICATION->IncludeComponent("restoran:restoraunts.list", "rest_list", Array(
            "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => CITY_ID,	// Код информационного блока
            "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
            "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
            "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
            "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arrFilter",	// Фильтр
            "PROPERTY_CODE" => array(	// Свойства
                    0 => "type",
                    1 => "kitchen",
                    2 => "average_bill",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "address",
                    6 => "subway",
                    7 => "ratio",
            ),
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "Y",	// Тип кеширования
            "CACHE_TIME" => "10800",	// Время кеширования (сек.)
            "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "PAGER_TITLE" => "Рестораны",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            ),
            false
    );?>
</div>
<div id="baner_inv_block">
    <div id="baner_right_2_main_page">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
    <div id="baner_right_1_main_page">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>