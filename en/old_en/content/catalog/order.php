<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Доставка еды");
$APPLICATION->SetTitle("Доставка еды");
?>
<div id="content">	
	<div class="left" style="width:720px;">
		<h1>Доставка еды</h1>
		<div class="grey_block">	                    
			<?if (!$_REQUEST["ORDER_ID"]):?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket",
					"dostavka_order",
					Array(
						"PATH_TO_ORDER" => "/personal/order.php",
						"HIDE_COUPON" => "Y",
						"COLUMNS_LIST" => array("NAME", "PRICE", "QUANTITY"),
						"QUANTITY_FLOAT" => "N",
						"PRICE_VAT_SHOW_VALUE" => "N",
						"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
						"SET_TITLE" => "N"
					),
				false
				);?>
				<br />
				<div class="dotted"></div>			
			<?endif;?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:sale.order.ajax",
				"order1",
				Array(
					"PATH_TO_BASKET" => "/".CITY_ID."/catalog/dostavka/all",
					"PATH_TO_PERSONAL" => "index.php",
					"PATH_TO_PAYMENT" => "payment.php",
					"PATH_TO_AUTH" => "/auth/",
					"PAY_FROM_ACCOUNT" => "Y",
					"COUNT_DELIVERY_TAX" => "N",
					"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
					"ONLY_FULL_PAY_FROM_ACCOUNT" => "Y",
					"ALLOW_AUTO_REGISTER" => "N",
					"SEND_NEW_USER_NOTIFY" => "N",
					"DELIVERY_NO_AJAX" => "Y",
					"SET_TITLE" => "N",
					"PROP_1" => array("1", "2", "4", "5", "6", "7"),
					"PROP_2" => array("3", "8", "9", "10", "11", "12", "13", "14", "15", "16")
				),
			false
			);?>
		</div>
                <br /><br />
                <div id="tabs_block2">
                    <ul class="tabs">
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Количество</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Способы оплаты</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Перевод денег</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                    </ul>
                    <!-- tab "panes" -->
                    <div class="panes">
                        <div class="pane" style="display:block">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                    </div>
                </div>
	</div>
	<div class="right" style="width:240px">
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>	
		<br /><br />
                <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest.php"),
                        Array(),
                        Array("MODE"=>"html")
                );?>
		<br /><br />
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
	</div>
	<div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>