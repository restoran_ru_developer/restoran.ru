<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Блоги");
//$info = getArSection($_REQUEST["SECTION_ID"]);
$arBlogIB = getArIblock("blogs", $_REQUEST["CITY_ID"]);
?>
<?
 $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"blog_detail",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "blogs",
		"IBLOCK_ID" => $arBlogIB["ID"],
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
		"PROPERTY_CODE" => Array("COMMENTS","RESTORAN"),
		"IBLOCK_URL" => "",
		"META_KEYWORDS" => "keywords",
		"META_DESCRIPTION" => "description",
		"BROWSER_TITLE" => "title",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
		"USE_PERMISSIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);
 ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>