<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Кулинария");
$arInterviewIB = getArIblock("interview", CITY_ID);
?>   
	<?$APPLICATION->IncludeComponent(
		"restoran:article.list",
		"interview",
		Array(
			"IBLOCK_TYPE" => "cookery",
			"IBLOCK_ID" => 70,
			"SECTION_ID" => "",
			"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
			"PAGE_COUNT" => 20,
			"SECTION_URL" => "/content/cookery/detail.php?SECTION_ID=#ID#",
			"PICTURE_WIDTH" => 232,
			"PICTURE_HEIGHT" => 127,
			"DESCRIPTION_TRUNCATE_LEN" => 200,
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "3",
			"SECTION_FIELDS" => "",
			"SECTION_USER_FIELDS" => Array("UF_SECTION_BIND", "UF_SECTION_COMM_CNT"),
			"SORT_FIELDS" => "SORT",
			"SORT_BY" => "ASC",
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_TEMPLATE" => "kupon_list",	
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",					
		),
		false
	);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>