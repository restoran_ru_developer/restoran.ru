<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Cookery");
//s$arInterviewIB = getArIblock("interview", CITY_ID);
?>    
<div id="content">
    <div class="left" style="width:730px">      		
		<div id="tabs_block1">
			<ul class="tabs">		
				<li>
					<a href="#">
						<div class="left tab_left"></div>
						<div class="left name">Admin recipes</div>
						<div class="left tab_right"></div>
						<div class="clear"></div>
					</a>
				</li>				
			</ul>
			<!-- tab "panes" -->
			<div class="panes">
                            <div class="pane">
                               <?$APPLICATION->IncludeComponent(
                                    "restoran:catalog.list",
                                    "cook",
                                    Array(
                                            "DISPLAY_DATE" => "N",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "N",
                                            "AJAX_MODE" => "N",
                                            "IBLOCK_TYPE" => "cookery",
                                            "IBLOCK_ID" => 2606,
                                            "NEWS_COUNT" => 8,
                                            "SORT_BY1" => "date_created",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_BY2" => "",
                                            "SORT_ORDER2" => "",
                                            "FILTER_NAME" => "arrFilterTop4",
                                            "FIELD_CODE" => array("CREATED_BY"),
                                            "PROPERTY_CODE" => array("ratio","reviews_bind","COMMENTS"),
                                            "CHECK_DATES" => "N",
                                            "DETAIL_URL" => "",
                                            "PREVIEW_TRUNCATE_LEN" => "200",
                                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                            "SET_TITLE" => "N",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "188471",
                                            "PARENT_SECTION_CODE" => "",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_FILTER" => "Y",
                                            "CACHE_GROUPS" => "N",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "N",
                                            "PAGER_TITLE" => "Новости",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_TEMPLATE" => "",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N"
                                    ),
                                false
                                );
                                ?>
				<Br />
				<div class="right"><a href="/content/cookery/editor/" class="uppercase">All admin recipes</a></div>
			   </div>			   
			</div>
		</div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "cookery_728_90",
            		"NOINDEX" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
            );?>
		<br /><br />
<!--		<div id="tabs_block2">
			<ul class="tabs">
                                <li>
					<a href="#">
						<div class="left tab_left"></div>
						<div class="left name">Master-classes</div>
						<div class="left tab_right"></div>
						<div class="clear"></div>
					</a>
				</li>                                
			</ul>
			 tab "panes" 
			<div class="panes">
			   <div class="pane">			   
                               <?/*$APPLICATION->IncludeComponent(
                                    "restoran:catalog.list",
                                    "cook_all",
                                    Array(
                                            "DISPLAY_DATE" => "N",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "N",
                                            "AJAX_MODE" => "N",
                                            "IBLOCK_TYPE" => "cookery",
                                            "IBLOCK_ID" => 2606,
                                            "NEWS_COUNT" => 3,
                                            "SORT_BY1" => "ID",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_BY2" => "",
                                            "SORT_ORDER2" => "",
                                            "FILTER_NAME" => "",
                                            "FIELD_CODE" => array("CREATED_BY"),
                                            "PROPERTY_CODE" => array("COMMENTS"),
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "PREVIEW_TRUNCATE_LEN" => "120",
                                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                            "SET_TITLE" => "N",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "masterclass",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_FILTER" => "Y",
                                            "CACHE_GROUPS" => "N",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "N",
                                            "PAGER_TITLE" => "Новости",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_TEMPLATE" => "",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "ALL" => "/content/cookery/masterclass/",
                                            "ALL_D" => "All Recipes"
                                    ),
                                false
                                );*/
                                ?>
			   </div>                            
			</div>
		</div>-->
   </div>    
    <div class="right" style="width:240px">
            <div align="right">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
        
       <!--<div id="search_article">
            <?/*$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.form",
                    "main_page_subscribe",
                    Array(
                            "USE_PERSONALIZATION" => "Y",
                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                            "SHOW_HIDDEN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => ""
                    ),
            false
            );*/?>           
        </div>-->
        <!--<br />
        <div class="tags">
			<?/*$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
					"SORT" => "CNT",
					"PAGE_ELEMENTS" => "20",
					"PERIOD" => "",
					"URL_SEARCH" => "/search/index.php",
					"TAGS_INHERIT" => "Y",
					"CHECK_DATES" => "Y",
					"FILTER_NAME" => "",
					"arrFILTER" => array(
							0 => "iblock_cookery",
					),
					"arrFILTER_iblock_cookery" => array(
							0 => "2606",
					),
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"FONT_MAX" => "24",
					"FONT_MIN" => "12",
					"COLOR_NEW" => "24A6CF",
					"COLOR_OLD" => "24A6CF",
					"PERIOD_NEW_TAGS" => "",
					"SHOW_CHAIN" => "Y",
					"COLOR_TYPE" => "N",
					"WIDTH" => "100%",
                                        "SEARCH_IN" => "recepts"
					),
					$component
			);*/?> 
        </div>
        <br />-->
		<!--<div class="black_hr"></div>
        <div align="right">
            <a href="#" class="uppercase">ВСЕ ТЕГИ</a>
        </div>-->
        <?/*?>
        <br />
		<div class="top_block">Видео-рецепты</div>		
		<?
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 139,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => 57429,
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                ?>
		<div class="black_hr"></div>
		<div align="right"><a class="uppercase" href="/content/cookery/videoretsepty/">ВСЕ ВИДЕО</a></div>
		<br /><br />
        <?*/?>
		<div class="top_block"><i>Top-3</i></div>
                <?
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 2606,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "PROPERTY_COMMENTS",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "shows",
                                "SORT_ORDER2" => "DESC",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "7200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                ?>
		<!--<hr />
		<div align="right"><a href="#">ВСЕ НОВЫЕ</a></div>-->
		<br /><br />
                <?//require_once $_SERVER["DOCUMENT_ROOT"].'/content/cookery/vote.php';?>                		
                <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
    </div>
    <div class="clear"></div>
    <br /><br />
    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>  
    <br /><br />
</div>   
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>