<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
// get IB params from iblock code
$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
if ($ttt)
{
    $type = $ttt;
}
else
{
    $type = str_replace($_REQUEST["CITY_ID"], "", $_REQUEST["SECTION_CODE"]);
    if ($type=="restoransnews")
        $type = "news";
    elseif ($type=="restoratoram")
        $type = "firms_news";
    elseif($type=="restvew"||$type=="obzor_")
        $type = "overviews";
    elseif($type=="pryamayarech"||$type=="intervew")
        $type = "interview";
    elseif($type=="photos")
        $type = "photoreports";
    elseif($type=="videonovosti")
        $type = "videonews";
    elseif($type=="moskvanatarelke"||$type=="onplate")
        $type = "on_plate";
    elseif($type=="mcfromchif")
    {
        $APPLICATION->SetTitle("Master-classes");
        $type = "cookery";
    }    
    else
    {
        $type = "news";
    }
}
if ($_REQUEST["SECTION_CODE"]=="restoratoram")
{
    $type = "firms_news";
    $arIB = getArIblock($type, "msk");
}
else
    $arIB = getArIblock($type, $_REQUEST["CITY_ID"]);
if ($_REQUEST["arrFilter_pf"])
{
    //global $arrFilterTop4;
    foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
    {        
            $arrFilterTop4["PROPERTY_".$key] = $ar;
    }    
}
?>
<?
/*if ($type!="photoreports"&&$type!="overviews"&&$type!="news"&&$type!="cookery")
{
    $APPLICATION->IncludeComponent(
            "restoran:article.list",
            "news",
            Array(
                    "IBLOCK_TYPE" => $type,
                    "IBLOCK_ID" => ($type=="cookery")?70:$arIB["ID"],
                    "SECTION_ID" => ($type=="cookery")?238:"",
                    "SECTION_CODE" => "",
                    "PAGE_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "20"),
                    "SECTION_URL" => "",
                    "PICTURE_WIDTH" => 232,
                    "PICTURE_HEIGHT" => 127,
                    "DESCRIPTION_TRUNCATE_LEN" => 200,
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => ($type=="cookery")?3:1,
                    "SET_TITLE" => ($type=="cookery")?"N":"Y",
                    "SECTION_FIELDS" => "",
                    "SECTION_USER_FIELDS" => Array("UF_COMMENTS"),
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "Y",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_TEMPLATE" => "kupon_list",	
                    "PAGER_DESC_NUMBERING" => "N",	
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",

            ),
    false
    );
}else{*/
$r = "";
if ($type=="news")
    $r = $_REQUEST["SECTION_CODE"];
elseif ($type=="cookery")
    $r = "masterclass";
?>
    <?$APPLICATION->IncludeComponent(
    	"restoran:catalog.list",
    	"news",
    	Array(
    		"DISPLAY_DATE" => "N",
    		"DISPLAY_NAME" => "Y",
    		"DISPLAY_PICTURE" => "Y",
    		"DISPLAY_PREVIEW_TEXT" => "N",
    		"AJAX_MODE" => "N",
    		"IBLOCK_TYPE" => $type,
    		"IBLOCK_ID" => ($type=="cookery")?"2606":$arIB["ID"],
    		"NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "20"),
    		"SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "ACTIVE_FROM"),
    		"SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    		"SORT_BY2" => "SORT",
    		"SORT_ORDER2" => "DESC",
    		"FILTER_NAME" => "arrFilterTop4",
    		"FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
    		"PROPERTY_CODE" => array("COMMENTS","summa_golosov"),
    		"CHECK_DATES" => "N",
    		"DETAIL_URL" => "",
    		"PREVIEW_TRUNCATE_LEN" => "120",
    		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
    		"SET_TITLE" => "Y",
    		"SET_STATUS_404" => "N",
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    		"ADD_SECTIONS_CHAIN" => "N",
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
    		"PARENT_SECTION" => "",
    		"PARENT_SECTION_CODE" => ($type=="news")?$_REQUEST["SECTION_CODE"]:"",
    		"CACHE_TYPE" => "Y",
    		"CACHE_TIME" => "7200",
    		"CACHE_FILTER" => "Y",
    		"CACHE_GROUPS" => "N",
    		"DISPLAY_TOP_PAGER" => "N",
    		"DISPLAY_BOTTOM_PAGER" => "Y",
    		"PAGER_TITLE" => "Новости",
    		"PAGER_SHOW_ALWAYS" => "N",
    		"PAGER_TEMPLATE" => "search_rest_list",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    		"PAGER_SHOW_ALL" => "N",
    		"AJAX_OPTION_JUMP" => "N",
    		"AJAX_OPTION_STYLE" => "Y",
    		"AJAX_OPTION_HISTORY" => "N"
    	),
    false
    );
    
//}?>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>