<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?

if(!$USER->IsAdmin())
    return false;

define("ADD_IBLOCK_ID", 2609);
//define("REST_IBLOCK_ID", 13);
define("ADD_IBLOCK_SECTION_ID", 188788);
define("OLD_DB_SECTION_ID", 4584);

CModule::IncludeModule("iblock");

$el = new CIBlockElement;
//$bs = new CIBlockSection;
global $DB;

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "werhfpbl";

/*
 * Simple function to replicate PHP 5 behaviour
 */
function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


// stepping
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

// start execution
$time_start = microtime_float();

mysql_select_db($DBName1, $link1);

$strSql = "
        SELECT
            en_news1.*
        FROM
            en_news1
        WHERE
            en_news1.category_id = ".OLD_DB_SECTION_ID." AND en_news1.id > {$curStepID} AND date > '2012-01-01 00:00:00'
        ORDER BY
            en_news1.id ASC
    ";
$res = mysql_query($strSql, $link1);
while($ar = mysql_fetch_array($res)) {
	mysql_select_db($DBName1, $link1);
	$arFields = Array();

    // get rest bind
    $strSqlNewsBind = "
        SELECT
            en_news_items_link.*
        FROM
            en_news_items_link
        WHERE
            en_news_items_link.news_id = {$ar["id"]}
    ";
    $resNewsBind = mysql_query($strSqlNewsBind, $link1);
    if($arNewsBind = mysql_fetch_array($resNewsBind)) {
        mysql_select_db($DB->DBName, $DB->db_Conn);
        // check existing in rests
        $exsRestID = 0;
        $arExRestIblock = Array("2586");
        foreach($arExRestIblock as $restIblock) {
            $rsRestCheck = CIBlockElement::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "IBLOCK_ID" => $restIblock,
                    "PROPERTY_old_item_id" => $arNewsBind["item_id"]
                ),
                false,
                false,
                Array()
            );
            if($arRestCheck = $rsRestCheck->Fetch()) {
                $arFields["PROPERTY_VALUES"]["RESTORAN"] = $arRestCheck["ID"];
                break;
            }
        }
    }
	
	// iblock for add
    $arFields["IBLOCK_ID"] = ADD_IBLOCK_ID;
    // step id
    $curStepID = $ar["id"];
    // code
    $arFields["CODE"] = $ar["path"];
    // set activity
    $arFields["ACTIVE"] = ($ar["is_enable"] ? "Y" : "N");
    // set name
    $arFields["NAME"] = html_entity_decode(htmlspecialchars_decode(stripcslashes($ar["title"])));
	// set DESCRIPTION
    //$search = array ("'&(amp|#249);'i", "'&(amp|#34);'i");
    //$replace = array ("u", '"');
    //$arFields["PREVIEW_TEXT"] = preg_replace($search, $replace, $ar["text_preview"]);
    $arFields["PREVIEW_TEXT"] = clearStrHTML($ar["text_preview"]);
    $arFields["PREVIEW_TEXT"] = strip_tags(html_entity_decode(htmlspecialchars_decode(stripcslashes($arFields["PREVIEW_TEXT"]))));
    $arFields["PREVIEW_TEXT_TYPE"] = "text";

    // save img from text
	$dom = new domDocument;
	$dom->loadHTML($arFields["DESCRIPTION"]);
	$dom->preserveWhiteSpace = false;
	$images = $dom->getElementsByTagName('img');
	foreach($images as $img){
		$url = $img->getAttribute('src');
                $url = str_replace("www", "en", $url);
                $url = str_replace("http://ru", "http://en", $url);
                //$url = str_replace("old", "en", $url);
                if (substr_count($url, "en.restoran.ru")||substr_count($url, "old.restoran.ru"))
                    $fileArray = CFile::MakeFileArray($url);
                else
                    $fileArray = CFile::MakeFileArray("http://en.restoran.ru/".$url);
		$fileArray["MODULE_ID"] = "iblock";
		$fid = CFile::SaveFile($fileArray, "iblock");
		$arFields["DESCRIPTION"] = str_replace($url, '#FID_'.$fid.'#', $arFields["DESCRIPTION"]);
	}

	// get big photo
    /*if($ar["big_img"])
    {
	    $arFields["DETAIL_PICTURE"] = CFile::MakeFileArray("http://en.restoran.ru/uploads/news/".$ar["big_img"]);    
    }
    //get small photo
    if($ar["small_img"])
    {
        $arFields["PREVIEW_PICTURE"] = CFile::MakeFileArray("http://en.restoran.ru/uploads/news/".$ar["small_img"]);        
    }*/
    //dates
    $arFields["ACTIVE_FROM"] = $DB->FormatDate($ar["date"], "YYYY-MM-DD HH:MI:SS", "DD.MM.YYYY HH:MI:SS");

    $search = array ("'&(amp|#249);'i", "'&(amp|#34);'i");
    $replace = array ("u", '"');
    $arFields["DETAIL_TEXT"] = preg_replace($search, $replace, $ar["text_full"]);

    $arFields["DETAIL_TEXT"] = trim(htmlspecialchars_decode(stripcslashes($arFields["DETAIL_TEXT"])));
    $arFields["DETAIL_TEXT"] = preg_replace('~\s+~mu', ' ', $arFields["DETAIL_TEXT"]);
    $arFields["DETAIL_TEXT"] = html_entity_decode(htmlspecialchars_decode(stripcslashes($arFields["DETAIL_TEXT"])));
    $arFields["DETAIL_TEXT_TYPE"] = "html";
    $arFields["DETAIL_TEXT"] = str_replace(Array("http://en.restoran.ru/"), Array("/"), $arFields["DETAIL_TEXT"]);

    if(!$arFields["PREVIEW_PICTURE"]) {

        mysql_select_db($DB->DBName, $DB->db_Conn);
        // save img from text
        $dom = new domDocument;
        $dom->loadHTML($arFields["DETAIL_TEXT"]);
        $dom->preserveWhiteSpace = false;
        $images = $dom->getElementsByTagName('img');
        foreach($images as $img){
            $url = $img->getAttribute('src');
            $url = str_replace("www", "en", $url);
            $url = str_replace("http://ru", "http://en", $url);
            //$url = str_replace("old", "en", $url);
            if (substr_count($url, "en.restoran.ru")||substr_count($url, "old.restoran.ru"))
                $fileArray = CFile::MakeFileArray($url);
            else
                $fileArray = CFile::MakeFileArray("http://en.restoran.ru/".$url);
            $arFields["PREVIEW_PICTURE"] = $fileArray;
            break;            
        }

        $arFields["DETAIL_TEXT"] = preg_replace("/<img[^>]+\>/i", "", $arFields["DETAIL_TEXT"]);
    }
    $arFields["DETAIL_TEXT"] = strip_tags($arFields["DETAIL_TEXT"], '<br><br /><br/><p><b><strong>');

    if(!$arFields["DETAIL_PICTURE"])
        $arFields["DETAIL_PICTURE"] = $arFields["PREVIEW_PICTURE"];

    $arFields["IBLOCK_SECTION_ID"] = ADD_IBLOCK_SECTION_ID;
    //v_dump($arFields);
    mysql_select_db($DB->DBName, $DB->db_Conn);
    //$EL_ID = $el->Add($arFields);
    //echo $el->LAST_ERROR;
    //v_dump($arFields);
    //return true;

    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;

    if($time > 2) {
        //echo "<script>setTimeout(function() {location.href = '/parser_news.php?curStepID=".$curStepID."'}, 2000);</script>";
        break;
    }
}
?>