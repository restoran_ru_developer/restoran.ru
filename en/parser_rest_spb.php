<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

define("CITY_NUM_ID", 1);
define("ADD_IBLOCK_ID", 11);
define("ADD_IBLOCK_SECTION_ID", 226);
define("OLD_DB_SECTION_ID", 17);
// msk
$arIBProp["my_alcohol"][11] = 1392;
$arIBProp["bankets"][11] = 397	;
$arIBProp["catering"][11] = 1413;
$arIBProp["food_delivery"][11] = 1414;
$arIBProp["wi_fi"][11] = 40;
$arIBProp["subway"][11] = 30;
$arIBProp["out_city"][11] = 79;
$arIBProp["administrative_distr"][11] = 59;
$arIBProp["area"][11] = 60;
$arIBProp["okrugdel"][11] = 111;
// spb
$arIBProp["my_alcohol"][12] = 1421;
$arIBProp["bankets"][12] = 1422	;
$arIBProp["catering"][12] = 1423;
$arIBProp["food_delivery"][12] = 1424;
$arIBProp["wi_fi"][12] = 1425;
$arIBProp["subway"][12] = 89;
$arIBProp["out_city"][12] = 106;
$arIBProp["administrative_distr"][12] = 109;
$arIBProp["area"][12] = 110;
$arIBProp["okrugdel"][12] = 112;
// sch
$arIBProp["my_alcohol"][13] = 1489;
$arIBProp["bankets"][13] = 1490	;
$arIBProp["catering"][13] = 1491;
$arIBProp["food_delivery"][13] = 1492;
$arIBProp["wi_fi"][13] = 1493;
$arIBProp["subway"][13] = 126;
$arIBProp["out_city"][13] = 132;
$arIBProp["administrative_distr"][13] = 128;
$arIBProp["area"][13] = 131;
$arIBProp["okrugdel"][13] = 135;
// anp
$arIBProp["my_alcohol"][15] = 1471;
$arIBProp["bankets"][15] = 1472	;
$arIBProp["catering"][15] = 1473;
$arIBProp["food_delivery"][15] = 1474;
$arIBProp["wi_fi"][15] = 1475;
$arIBProp["subway"][15] = 121;
$arIBProp["out_city"][15] = 122;
$arIBProp["administrative_distr"][15] = 124;
$arIBProp["area"][15] = 123;
$arIBProp["okrugdel"][15] = 125;
// krd
$arIBProp["my_alcohol"][16] = 1480;
$arIBProp["bankets"][16] = 1481	;
$arIBProp["catering"][16] = 1482;
$arIBProp["food_delivery"][16] = 1483;
$arIBProp["wi_fi"][16] = 1484;
$arIBProp["subway"][16] = 127;
$arIBProp["out_city"][16] = 133;
$arIBProp["administrative_distr"][16] = 129;
$arIBProp["area"][16] = 130;
$arIBProp["okrugdel"][16] = 134;

CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");

$el = new CIBlockElement;
$bs = new CIBlockSection;

/*
 * Simple function to replicate PHP 5 behaviour
 */
function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

/*
 * check el code
 */
function checkElCode($iblockID, $code) {
    global $DB;
    mysql_select_db($DB->DBName, $DB->db_Conn);

    $rsRes = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $iblockID,
            "CODE" => $code
        ),
        false,
        false,
        Array()
    );
    if($arRes = $rsRes->GetNext())
        return $arRes["ID"];
    else
        return false;
}

/*
 * add el to IB
 */
function addElToIB($arAddFields) {
    $el = new CIBlockElement;

    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID" => ($arAddFields["IBLOCK_SECTION_ID"] ? $arAddFields["IBLOCK_SECTION_ID"] : false),
        "IBLOCK_ID"         => $arAddFields["IBLOCK_ID"],
        "PROPERTY_VALUES"   => $arAddFields["PROPERTIES"],
        "NAME"              => trim(htmlspecialchars_decode(stripcslashes($arAddFields["NAME"]))),
        "CODE"              => $arAddFields["CODE"],
        "ACTIVE"            => "Y",
        "PREVIEW_TEXT"      => $arAddFields["PREVIEW_TEXT"],
        "DETAIL_TEXT"       => trim(htmlspecialchars_decode(stripcslashes($arAddFields["DETAIL_TEXT"]))),
        "PREVIEW_PICTURE"   => CFile::MakeFileArray($arAddFields["PICTURE"]),
        "DETAIL_PICTURE"    => CFile::MakeFileArray($arAddFields["PICTURE"])
    );
    if($PRODUCT_ID = $el->Add($arLoadProductArray))
        return $PRODUCT_ID;
    else
        return false;
}

/*
 * check existing element
 */
function checkExElement($iblockID, $oldItemID) {
    global $DB;
    mysql_select_db($DB->DBName, $DB->db_Conn);

    $rsRes = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "IBLOCK_ID" => $iblockID,
            "PROPERTY_old_item_id" => $oldItemID
        ),
        false,
        false,
        Array()
    );
    if($arRes = $rsRes->GetNext()) {
        return $arRes["ID"];
    } else {
        return false;
    }
}

function extract_emails_from($string){
    preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);
    return $matches[0][0];
}

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "gamekonezimo";


// stepping
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

// start execution
$time_start = microtime_float();


$db_selected1 = mysql_select_db($DBName1, $link1);
$strSql = "
        SELECT
            ru_items_catalog_link.*
        FROM
            ru_items_catalog_link
        WHERE
            ru_items_catalog_link.category_id = ".OLD_DB_SECTION_ID." AND ru_items_catalog_link.item_id > {$curStepID}
        ORDER BY
            item_id ASC
    ";
$res = mysql_query($strSql, $link1);
while($ar = mysql_fetch_array($res)) {
    // select restoran DB
    mysql_select_db($DBName1, $link1);

    // get item info
    $strSql = "
        SELECT
          ru_items.*
        FROM
          ru_items
        WHERE
          ru_items.item_id = {$ar["item_id"]}
          AND ru_items.city_id = ".CITY_NUM_ID."
    ";
    $resItem = mysql_query($strSql, $link1);
    if($arItem = mysql_fetch_array($resItem)) {
        // select restoran DB
        mysql_select_db($DBName1, $link1);
        $arFields = Array();
        
        // get panoramas
        // TODO uncomment
        $strSql = "
            SELECT
                ru_items_panorams.*
            FROM
                ru_items_panorams
            WHERE
                ru_items_panorams.item_id =".$ar["item_id"];              
        $resPanoramas = mysql_query($strSql, $link1);
        while($arPanoramas = mysql_fetch_array($resPanoramas)) {
            // videopanoramy
            $arFields["PROPERTY_VALUES"]["videopanoramy"][] = CFile::MakeFileArray("http://www.restoran.ru/uploads/panorams/".$arPanoramas["video_file"]);
            $arFields["PROPERTY_VALUES"]["videopanoramy_audio"][] = CFile::MakeFileArray("http://www.restoran.ru/uploads/panorams/music/".$arPanoramas["audio_file"]);
        }

        // get photos
        // TODO uncomment
        $strSql = "
            SELECT
                ru_items_photos.*
            FROM
                ru_items_photos
            WHERE
                ru_items_photos.item_id =".$ar["item_id"];
        $resPhotos = mysql_query($strSql, $link1);
        while($arPhotos = mysql_fetch_array($resPhotos)) {
            // photos
            if($arPhotos["is_logo"] != "1") {
                $arFields["PROPERTY_VALUES"]["photos"][] = CFile::MakeFileArray("http://www.restoran.ru/uploads/item_photos/".$ar["item_id"]."/".$arPhotos["file"]);
            }
        }
        // set rest logo
        $arFields["PREVIEW_PICTURE"] = $arFields["PROPERTY_VALUES"]["photos"][0];

        // get rest description
        $strSql = "
            SELECT
              ru_items_logos.*
            FROM
              ru_items_logos
            WHERE
              ru_items_logos.item_id = ".$ar["item_id"];
        $resText = mysql_query($strSql, $link1);
        $arText = mysql_fetch_array($resText);

        $arFields["DETAIL_TEXT"] = trim(htmlspecialchars_decode(stripcslashes(iconv("windows-1251", "utf-8//IGNORE", html_entity_decode($arText["logo"])))));
        $arFields["DETAIL_TEXT"] = str_replace('document.write(&#39;&#39;);', '', $arFields["DETAIL_TEXT"]);
        $arFields["DETAIL_TEXT_TYPE"] = "html";

        if(!$arFields["PREVIEW_PICTURE"]) {
            $arFields["DETAIL_TEXT"] = str_replace(Array("http://www.restoran.ru/"), Array("/"), $arFields["DETAIL_TEXT"]);
            // save img from text
            $dom = new domDocument;
            $dom->loadHTML($arFields["DETAIL_TEXT"]);
            $dom->preserveWhiteSpace = false;
            $images = $dom->getElementsByTagName('img');
            foreach($images as $img){
                $url = $img->getAttribute('src');
                $fileArray = CFile::MakeFileArray("http://www.restoran.ru".$url);
                $fileArray["MODULE_ID"] = "iblock";
                $fid = CFile::SaveFile($fileArray, "iblock");
                $arFile = CFile::GetFileArray($fid);
                $arFields["DETAIL_TEXT"] = str_replace($url, "#FID_".$fid."#", $arFields["DETAIL_TEXT"]);
                $arFields["PREVIEW_PICTURE"] = $fileArray;
            }
        }
        $arFields["DETAIL_TEXT"] = strip_tags($arFields["DETAIL_TEXT"], '<br><br /><br/><p><b><strong><img>');

        $arFields["DETAIL_TEXT"] = preg_replace("/<img[^>]+\>/i", "", $arFields["DETAIL_TEXT"]);

        // iblock for add
        $arFields["IBLOCK_ID"] = ADD_IBLOCK_ID;
        // iblock section id for add
        $arFields["IBLOCK_SECTION_ID"] = ADD_IBLOCK_SECTION_ID;
        // set old item id
        $arFields["PROPERTY_VALUES"]["old_item_id"] = $curStepID = $arItem["item_id"];

        // code
        $arFields["CODE"] = $arItem["path"];
        /*
        if(checkElCode(ADD_IBLOCK_ID, $arFields["CODE"]) > 0) {
            $arFields["CODE"] = $arFields["CODE"]."_".randString(3, Array("0123456789"));
        }
        */

        // set activity
        $arFields["ACTIVE"] = ($arItem["is_enable"] ? "Y" : "N");
        // set map coordinates
        $arFields["PROPERTY_VALUES"]["map"] = Array("VALUE" => $arItem["map_x"].",".$arItem["map_y"]);
        // set coordinates
        $arFields["PROPERTY_VALUES"]["lat"] = $arItem["map_x"];
        $arFields["PROPERTY_VALUES"]["lon"] = $arItem["map_y"];
        // select restoran DB
        mysql_select_db($DBName1, $link1);        
        // get item props
        $strSql = "
            SELECT
              ru_items_values.*
            FROM
              ru_items_values
            WHERE
              ru_items_values.item_id = {$ar["item_id"]}
        ";
        $resItemVal = mysql_query($strSql, $link1);
        while($arItemVal = mysql_fetch_array($resItemVal)) {
            // select restoran DB
            mysql_select_db($DBName1, $link1);
            $arItemVal["value"] = htmlspecialchars_decode($arItemVal["value"]);
            // set props
            switch($arItemVal["param_id"]) {
                // firm name
                case "1":
                    $arItemVal["value"] = stripslashes($arItemVal["value"]);
                    if($arItemVal["_primary"])
                        $arFields["NAME"]  = $arItemVal["value"];
                    elseif(!$arFields["NAME"])
                        $arFields["NAME"]  = $arItemVal["value"];
                break;
                // address
                case "17":
                    $arFields["PROPERTY_VALUES"]["address"] = array_map('trim', explode("<br>", $arItemVal["value"]));
                break;
                // opening_hours
                case "23":
                    $arFields["PROPERTY_VALUES"]["opening_hours"] = array_map('trim', explode("<br>", $arItemVal["value"]));
                break;
                // phone
                case "98":
                case "21":
                    $arFields["PROPERTY_VALUES"]["phone"] = str_replace(Array("<br>", "<br/>", "<br />"), ",", $arFields["PROPERTY_VALUES"]["phone"]);
                    $arFields["PROPERTY_VALUES"]["phone"] = array_map('trim', explode(",", $arItemVal["value"]));
                break;
                // number_of_rooms
                case "22":
                    $arFields["PROPERTY_VALUES"]["number_of_rooms"] = trim($arItemVal["value"]);
                break;
                // rent
                case "135":
                    $arFields["PROPERTY_VALUES"]["rent"] = trim($arItemVal["value"]);
                break;
                // my_alcohol
                case "136":
                    if($arItemVal["value"] && $arItemVal["value"] != "Нет")
                        $arFields["PROPERTY_VALUES"]["my_alcohol"] = $arIBProp["my_alcohol"][ADD_IBLOCK_ID];
                break;
                // bankets
                case "9":
                    if($arItemVal["value"] != "Да")
                        $arFields["PROPERTY_VALUES"]["bankets"] = $arIBProp["bankets"][ADD_IBLOCK_ID];
                break;
                // catering
                case "10":
                    if($arItemVal["value"] != "Да")
                        $arFields["PROPERTY_VALUES"]["catering"] = $arIBProp["catering"][ADD_IBLOCK_ID];
                break;
                // food_delivery
                case "11":
                    if($arItemVal["value"] != "Да")
                        $arFields["PROPERTY_VALUES"]["food_delivery"] = $arIBProp["food_delivery"][ADD_IBLOCK_ID];
                break;
                // email
                case "37":
                    $arFields["PROPERTY_VALUES"]["email"] = extract_emails_from($arItemVal["value"]);
                break;
                // landmarks
                case "33":
                    $arFields["PROPERTY_VALUES"]["landmarks"] = trim($arItemVal["value"]);
                break;
                // site
                case "38":
                    $pattern = '`.*?((http|ftp)://[\w#$&+,\/:;=?@.-]+)[^\w#$&+,\/:;=?@.-]*?`i';
                    if (preg_match_all($pattern, trim($arItemVal["value"]), $matches))
                        $site = $matches[1][0];
                        $arFields["PROPERTY_VALUES"]["site"] = $site;
                break;
                // add_props
                case "119":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Рабочие дни - ".trim($arItemVal["value"])."<br />";
                break;
                case "117":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Часы приема заказов - ".trim($arItemVal["value"])."<br />";
                break;
                case "113":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Часы доставки - ".trim($arItemVal["value"])."<br />";
                break;
                case "118":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Время доставки заказа - ".trim($arItemVal["value"])."<br />";
                break;
                case "114":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Минимальная сумма заказа - ".trim($arItemVal["value"])."<br />";
                break;
                case "115":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Цена доставки - ".trim($arItemVal["value"])."<br />";
                break;
                case "116":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= "Специальное предложение:<br />".trim($arItemVal["value"])."<br />";
                break;
                case "34":
                    $arFields["PROPERTY_VALUES"]["add_props"] .= trim($arItemVal["value"]);
                break;
            }
        }

        $arFields["PROPERTY_VALUES"]["add_props"] = array("VALUE" => array("TEXT"=>$arFields["PROPERTY_VALUES"]["add_props"], "TYPE"=>"html"));

        // get item list props
        $strSql = "
            SELECT
              ru_items_list_values.*
            FROM
              ru_items_list_values
            WHERE
              ru_items_list_values.item_id = {$ar["item_id"]}
        ";
        $resItemListVal = mysql_query($strSql, $link1);
        while($arItemListVal = mysql_fetch_array($resItemListVal)) {
            // select restoran DB
            mysql_select_db($DBName1, $link1);
            // sel param
            $strSql = "
                SELECT
                    ru_params_lists.*
                FROM
                    ru_params_lists
                WHERE
                    ru_params_lists.param_id = ".$arItemListVal["param_id"]." AND ru_params_lists.item_id = ".$arItemListVal["value_id"]
            ;
            $resItemListValParam = mysql_query($strSql, $link1);
            $arItemListValParam = mysql_fetch_array($resItemListValParam);
            switch($arItemListValParam["param_id"]) {
                // type
                case "3":
                    if(!$elID = checkElCode(67, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 67,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["type"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["type"][] = intval($elID);
                    }
                break;
                // kolichestvochelovek
                case "133":
                    if(!$elID = checkElCode(68, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 68,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["kolichestvochelovek"] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["kolichestvochelovek"] = intval($elID);
                    }
                break;
                // XXX city dep
                // subway
                case "18":
                    if(!$elID = checkElCode($arIBProp["subway"][ADD_IBLOCK_ID], $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => $arIBProp["subway"][ADD_IBLOCK_ID],
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["subway"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["subway"][] = intval($elID);
                    }
                break;
                // XXX city dep
                // out_city
                case "15":
                    if(!$elID = checkElCode($arIBProp["out_city"][ADD_IBLOCK_ID], $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => $arIBProp["out_city"][ADD_IBLOCK_ID],
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["out_city"] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["out_city"] = intval($elID);
                    }
                break;
                // average_bill
                case "24":
                    if(!$elID = checkElCode(78, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 78,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["average_bill"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["average_bill"][] = intval($elID);
                    }
                break;
                // XXX city dep
                // kitchen
                case "5":
                    if(!$elID = checkElCode(114, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 114,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["kitchen"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["kitchen"][] = intval($elID);
                    }
                break;
                // credit_cards
                case "25":
                    if(!$elID = checkElCode(65, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 65,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["credit_cards"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["credit_cards"][] = intval($elID);
                    }
                break;
                // children
                case "16":
                    if(!$elID = checkElCode(100, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 100,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["children"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["children"][] = intval($elID);
                    }
                break;
                // features
                case "29":
                    if(!$elID = checkElCode(76, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 76,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["features"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["features"][] = intval($elID);
                    }
                break;
                // entertainment
                case "30":
                    if(!$elID = checkElCode(63, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 63,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["entertainment"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["entertainment"][] = intval($elID);
                    }
                break;
                // ideal_place_for
                case "6":
                    if(!$elID = checkElCode(66, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 66,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["ideal_place_for"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["ideal_place_for"][] = intval($elID);
                    }
                break;
                // music
                case "31":
                    if(!$elID = checkElCode(61, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 61,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["music"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["music"][] = intval($elID);
                    }
                break;
                // parking
                case "35":
                    if(!$elID = checkElCode(75, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 75,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["parking"] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["parking"] = intval($elID);
                    }
                break;
                // XXX city dep
                // administrative_distr
                case "19":
                    if(!$elID = checkElCode($arIBProp["administrative_distr"][ADD_IBLOCK_ID], $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => $arIBProp["administrative_distr"][ADD_IBLOCK_ID],
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["administrative_distr"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["administrative_distr"][] = intval($elID);
                    }
                break;
                // XXX city dep
                // area
                case "20":
                    if(!$elID = checkElCode($arIBProp["area"][ADD_IBLOCK_ID], $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => $arIBProp["area"][ADD_IBLOCK_ID],
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["area"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["area"][] = intval($elID);
                    }
                break;
                // wi_fi
                case "125":
                    $arFields["PROPERTY_VALUES"]["wi_fi"] = $arIBProp["wi_fi"][ADD_IBLOCK_ID];
                break;
                // XXX city dep
                // okrugdel
                case "112":
                    if(!$elID = checkElCode($arIBProp["okrugdel"][ADD_IBLOCK_ID], $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => $arIBProp["okrugdel"][ADD_IBLOCK_ID],
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["okrugdel"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["okrugdel"][] = intval($elID);
                    }
                break;
                // kuhnyadostavki
                case "111":
                    //v_dump($arItemListValParam["path"]);
                    if(!$elID = checkElCode(73, $arItemListValParam["path"])) {
                        // add el to IB
                        $elID = addElToIB(
                            Array(
                                "IBLOCK_ID" => 73,
                                "NAME" => $arItemListValParam["title"],
                                "CODE" => $arItemListValParam["path"],
                            )
                        );
                        $arFields["PROPERTY_VALUES"]["kuhnyadostavki"][] = intval($elID);
                    } else {
                        $arFields["PROPERTY_VALUES"]["kuhnyadostavki"][] = intval($elID);
                    }
                break;
            }
        }


        // set rest db
        global $DB;
        mysql_select_db($DB->DBName, $DB->db_Conn);
        $dom = new domDocument;
        $dom->loadHTML($arFields["DETAIL_TEXT"]);
        $dom->preserveWhiteSpace = false;
        $images = $dom->getElementsByTagName('img');
        foreach($images as $img){
            $url = $img->getAttribute('src');
            $fileArray = CFile::MakeFileArray($url);
            $fileArray["MODULE_ID"] = "iblock";
            $fid = CFile::SaveFile($fileArray, "iblock");
            $arFile = CFile::GetFileArray($fid);
            $arFields["DETAIL_TEXT"] = str_replace($url, $arFile["SRC"], $arFields["DETAIL_TEXT"]);
        }

        v_dump($arFields["NAME"]);

        // add rest
        global $DB;
        mysql_select_db($DB->DBName, $DB->db_Conn);

// uncoment when run
/*
        // add or update element
        $arCheckEl = checkExElement(ADD_IBLOCK_ID, $arItem["item_id"]);
        if(!$arCheckEl) {
            $checkRestCode = checkElCode(ADD_IBLOCK_ID, $arFields["CODE"]);
            if($checkRestCode <= 0) {
                if($REST_ID = $el->Add($arFields)) {
                    echo "New rest ID: ".$REST_ID;
                    CIBlockElement::SetElementSection($REST_ID, $arFields["IBLOCK_SECTION_ID"]);
                } else {
                    mail('dk@cakelabs.ru', 'Error add msk rest', "Error: ".$el->LAST_ERROR."\n".$arFields["PROPERTY_VALUES"]["old_item_id"]);
                    echo "Error: ".$el->LAST_ERROR;
                }
            } else {
                $arSec = Array();
                $rsOldGroups = CIBlockElement::GetElementGroups($checkRestCode, true);
                while($arOldGroups = $rsOldGroups->Fetch())
                    $arSec[] = $arOldGroups["ID"];
                $arSec[] = $arFields["IBLOCK_SECTION_ID"];
                CIBlockElement::SetElementSection($checkRestCode, $arFields["IBLOCK_SECTION_ID"]);
            }
        } else {
            // delete photos
            $rsPhotoProps = CIBlockElement::GetProperty(ADD_IBLOCK_ID, $arCheckEl, "sort", "asc", Array("CODE"=>"photos"));
            while($arPhotoProps = $rsPhotoProps->Fetch()) {
                if ($arPhotoProps["VALUE"]) {
                    $arr[$arPhotoProps['PROPERTY_VALUE_ID']] = Array("VALUE" => Array("del" => "Y"));
                    CIBlockElement::SetPropertyValueCode($arCheckEl, "photos", $arr);
                    CFile::Delete($arPhotoProps["VALUE"]);
                }
            }

            // delete videopanoramy
            $rsVideopanoramyProps = CIBlockElement::GetProperty(ADD_IBLOCK_ID, $arCheckEl, "sort", "asc", Array("CODE"=>"videopanoramy"));
            while($arVideopanoramyProps = $rsVideopanoramyProps->Fetch()) {
                if ($arVideopanoramyProps["VALUE"]) {
                    $arr[$arVideopanoramyProps['PROPERTY_VALUE_ID']] = Array("VALUE" => Array("del" => "Y"));
                    CIBlockElement::SetPropertyValueCode($arCheckEl, "videopanoramy", $arr);
                    CFile::Delete($arVideopanoramyProps["VALUE"]);
                }
            }

            //delete videopanoramy_audio
            $rsVideopanoramyAudioProps = CIBlockElement::GetProperty(ADD_IBLOCK_ID, $arCheckEl, "sort", "asc", Array("CODE"=>"videopanoramy_audio"));
            while($arVideopanoramyAudioProps = $rsVideopanoramyAudioProps->Fetch()) {
                if ($arVideopanoramyAudioProps["VALUE"]) {
                    $arr[$arVideopanoramyAudioProps['PROPERTY_VALUE_ID']] = Array("VALUE" => Array("del" => "Y"));
                    CIBlockElement::SetPropertyValueCode($arCheckEl, "videopanoramy_audio", $arr);
                    CFile::Delete($arVideopanoramyAudioProps["VALUE"]);
                }
            }

            // delete d_tour
            $rs3DtourProps = CIBlockElement::GetProperty(ADD_IBLOCK_ID, $arCheckEl, "sort", "asc", Array("CODE"=>"d_tour"));
            while($ar3DtourProps = $rs3DtourProps->Fetch()) {
                if ($ar3DtourProps["VALUE"]) {
                    $arr[$ar3DtourProps['PROPERTY_VALUE_ID']] = Array("VALUE" => Array("del" => "Y"));
                    CIBlockElement::SetPropertyValueCode($arCheckEl, "d_tour", $arr);
                    CFile::Delete($ar3DtourProps["VALUE"]);
                }
            }


            $el->Update($arCheckEl, $arFields);

            echo "Update: ".$arCheckEl;
        }
*/

    }
    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;

    if($time > 35) {
        echo "<script>setTimeout(function() {location.href = '/parser_rest_spb.php?curStepID=".$curStepID."'}, 5000);</script>";
        break;
    }

    //v_dump($time);
}

mysql_close($link1);
?>