<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
//global $aMenuLinks;
if (CITY_ID=="ast"||CITY_ID=="alm")
{
    $kitchen = 2623;
}
elseif (CITY_ID=="tmn")
    $kitchen = 2635;
else
    $kitchen = 114;
$type = 67;
$top_menu = array();
$menu = new CMenu("bottom_menu_".CITY_ID);
$menu->Init($APPLICATION->GetCurDir());
foreach ($menu->arMenu as $m)
{
    if ($m[0] == "Рестораны"||$m[0] == "Банкетные залы")
    {
        $sub = array();
        $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>$kitchen,"ACTIVE"=>"Y"));
        $link = str_replace("all","kitchen",$m[1]);
        while ($ar=$res->Fetch())
        {
            $sub[] = Array("NAME"=>$ar["NAME"]." кухня","LINK"=>$link."/".$ar["CODE"]."/");
        }
                
        $arClosed = getArIblock("closed_rubrics_rest",CITY_ID);        
        $link = str_replace("all","rubrics",$m[1]);
        $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>$arClosed["ID"],"ACTIVE"=>"Y"));        
        while ($ar=$res->Fetch())
        {
            $sub[] = Array("NAME"=>$ar["NAME"],"LINK"=>$link."/".$ar["CODE"]."/");            
        }
        $top_menu[] = Array("NAME"=>$m[0],"LINK"=>$m[1], "SUB"=>$sub);        
        
        if ($m[0] == "Рестораны")
        {
            $sub = array();
            $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>$type,"ACTIVE"=>"Y"));
            $link = str_replace("all","type",$m[1]);
            while ($ar=$res->Fetch())
            {
                $sub[] = Array("NAME"=>$ar["NAME"],"LINK"=>$link."/".$ar["CODE"]."/");
            }
            $top_menu[] = Array("NAME"=>"Заведения по типу","LINK"=>"", "SUB"=>$sub);
        }           
        if ($m[0] == "Рестораны"&&(CITY_ID=="msk"||CITY_ID=="spb"))
        {
            $sub = array();
            $arGroup = getArIblock("rest_group",CITY_ID);        
            $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>$arGroup["ID"],"ACTIVE"=>"Y"));
            $link = str_replace("restaurants/all","group",$m[1]);
            $link = str_replace("banket/all","group",$link);
            while ($ar=$res->Fetch())
            {
                $sub[] = Array("NAME"=>$ar["NAME"],"LINK"=>$link."/".$ar["CODE"]."/");
            }
            $top_menu[] = Array("NAME"=>"Ресторанные группы","LINK"=>"", "SUB"=>$sub);
        }           
    }
    elseif($m[0] == "Отзывы")
    {
        $sub = array();
        $sub[] = Array("NAME"=>"с фото","LINK"=>$m[1]."?photos=Y");
        $sub[] = Array("NAME"=>"с видео","LINK"=>$m[1]."?video=Y");
        $top_menu[] = Array("NAME"=>$m[0],"LINK"=>$m[1], "SUB"=>$sub);
    }
    elseif($m[0] == "Рецепты")
    {
        $sub = array();
        $sub[] = Array("NAME"=>"Рецепты редактора","LINK"=>$m[1]."editor/");
        $sub[] = Array("NAME"=>"Рецепты пользователей","LINK"=>$m[1]."user/");
        $sub[] = Array("NAME"=>"Рецепты от шефа","LINK"=>"/".CITY_ID."/news/mcfromchif/");
        //$sub[] = Array("NAME"=>"Диеты","LINK"=>$m[1]."diety/");
        //$sub[] = Array("NAME"=>"Советы","LINK"=>$m[1]."sovety/");
                
        $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>2751,"ACTIVE"=>"Y"));        
        while ($ar=$res->Fetch())
        {
            $sub[] = Array("NAME"=>$ar["NAME"],"LINK"=>$m[1]."rubric/".$ar["CODE"]."/");            
        }
        $top_menu[] = Array("NAME"=>$m[0],"LINK"=>$m[1], "SUB"=>$sub);
    }
    elseif($m[0] == "Готовим сами")
    {
        
    }
    elseif($m[0] == "Мастер-классы")
    {
        
    }
    else
        $top_menu[] = Array("NAME"=>$m[0],"LINK"=>$m[1]);
}
?>
<style>
    .site_map ul
    {
        padding:0px;
        margin:0px;
    }
    .site_map ul ul
    {
        margin-left:35px;
    }
    .site_map ul li
    {
        list-style-type:none;
        color:#24A6CF;
        font-size:14px;
    }
    .site_map ul li a
    {
        color:#24A6CF;
        font-size:14px;
        text-decoration: underline;
        line-height:20px;
    }
    .site_map ul li a:hover
    {
        text-decoration: none;
    }
    .site_map .left
    {
        width:360px;
    }
</style>
<div id="content" class="site_map">
    <h1>Карта сайта</h1>
    <?
    foreach ($top_menu as $key=>$menu)
    {
        $top1[] = $menu;
        if ($key==6)
            break;
    }
    foreach ($top_menu as $key=>$menu)
    {
        if ($key>6)
        {
            $top2[] = $menu;    
        }
    }
    echo "<div class='left'>";
        echo "<ul>";
        foreach ($top1 as $menu)
        {
            if ($menu["LINK"])
                echo "<li><a href='".$menu["LINK"]."'>".$menu["NAME"]."</a>";
            else
                echo "<li>".$menu["NAME"];
            if (is_array($menu["SUB"]))
            {
                echo "<ul>";
                foreach ($menu["SUB"] as $sub)
                {
                    echo "<li>&ndash; <a href='".$sub["LINK"]."'>".$sub["NAME"]."</a></li>";
                }
                echo "</ul>";
            }
            echo "</li>";
        }
        echo "</ul>";
    echo "</div>";


    echo "<div class='left'>";
        echo "<ul>";
        foreach ($top2 as $menu)
        {
            echo "<li><a href='".$menu["LINK"]."'>".$menu["NAME"]."</a>";
            if (is_array($menu["SUB"]))
            {
                echo "<ul>";
                foreach ($menu["SUB"] as $sub)
                {
                    echo "<li>&ndash; <a href='".$sub["LINK"]."'>".$sub["NAME"]."</a></li>";
                }
                echo "</ul>";
            }
            echo "</li>";
        }
        echo "</ul>";
    echo "</div>";    
    ?>
    <div class="right" style="width: 240px">
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
                <br /><Br />
                <div class="top_block">Популярные</div>
            <?
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arRestIB = getArIblock("catalog", CITY_ID);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "5",
                        "SORT_BY1" => "show_counter",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?>
                <br /><Br/>
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
                <br /><Br />
                <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br /><Br/>
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
                <br /><Br />
	</div>
	<div class="clear"></div>
        <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
	<br /><Br />
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>