<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0)
	LocalRedirect($backurl);

$APPLICATION->SetTitle("Authorization");
?>
<div class="block">
<p>You have successfully registered and logged in.</p>
 
<p> Use the unit with your nickname in the header to access the Members Area</p>
 
<p><a href="<?=SITE_DIR?>">Return to main page</a></p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>