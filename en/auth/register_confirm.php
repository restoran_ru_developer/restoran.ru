<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подтверждение регистрации");
?>
<div id="content">
    <?
    /*if($_POST && $_POST["submit_reg"] && check_bitrix_sessid()) {
        $regCheckWord = trim($_POST["REGISTER_CHECKWORD"]);
        if($_SESSION["REGISTER_CHECKWORD"] != $regCheckWord) {
            ShowError("Не верный код подтверждения!");
            $strError = true;
        } else {
            $user = new CUser;
            $rsUser = $user->GetByLogin($_SESSION["REGISTER_USER_EMAIL"]);
            $arUser = $rsUser->Fetch();

            $arUserFields = Array(
                "ACTIVE" => "Y",
            );
            $user->Update($arUser["ID"], $arUserFields);
            $USER->Authorize($arUser["ID"]);
            echo "Ваш аккаунт активрован!<br />Через несколько секунд Вы будете переадресованы на главную страницу.";
            echo "<script type=\"text/javascript\">setTimeout(location.href='/', 4000);</script>";
        }
    }*/
    ?>

    <?if(!$_POST || $strError):?>
        <form action="<?=$APPLICATION->GetCurUri()?>" method="post" name="reg_confirm">
            <?=bitrix_sessid_post()?>
            <h2>Регистрация нового пользователя</h2>
            <p>На указанный Вами E-mail было отправленно сообщение с подтверждением регистрации.</p><p> Следуйте инструкциям указанным в письме.</p>
           
            <?if ($_COOKIE["BITRIX_SM_RESTORATOR"]=="Y"):?>            
            <p class="another_color">
                Вы прошли первый этап регистрации.<Br />Ваш статус ресторатора проходит проверку администрацией сайта.<br />Вы получите уведомление о подтверждении статуса и доступ к функционалу кабинета ресторатора.<br />Спасибо!
            </p>
                <?$APPLICATION->set_cookie("RESTORATOR", "", time() - 36000000, "/", ".restoran.ru", false, true);?>
            <?endif;?>
            <!--<label>Код подтверждения:</label>
            <input type="text" class="inputtext" name="REGISTER_CHECKWORD" size="35" />
            <br />
            <input type="submit" class="light_button" name="submit_reg" value="Подтвердить" style="margin-top:10px;">-->
        </form>
    <?endif?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>