<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
if ($_REQUEST["del"]&&check_bitrix_sessid())
{
    global $DB;
    CModule::IncludeModule("iblock");
    $DB->StartTransaction();	
    if(!CIBlockElement::Delete((int)$_REQUEST["del"]))
        $DB->Rollback();	            
    else		
	{
        $DB->Commit();
		LocalRedirect($APPLICATION->GetCurPage());
	}
}
?>
<div class="block">	
    <div class="left-side">
            <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
            <div class="clearfix"></div>
            <ul class="nav nav-tabs" >
                <li class="active">
                    <a href="#catalog" class="ajax" data-toggle="tab" data-href="/bitrix/templates/main/components/restoran/favorite.list/.default/ajax.php?part=catalog">
                            <div class="left tab_left"></div>
                            <div class="left name">Рестораны</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>                
                <li>
                    <a href="#cookery" class="ajax" data-toggle="tab" data-href="/bitrix/templates/main/components/restoran/favorite.list/.default/ajax.php?part=cookery">
                            <div class="left tab_left"></div>
                            <div class="left name">Книга рецептов</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="#vacancy" class="ajax" data-toggle="tab"  data-href="/bitrix/templates/main/components/restoran/favorite.list/.default/ajax.php?part=vacancy">
                            <div class="left tab_left"></div>
                            <div class="left name">Вакансии</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="#resume" class="ajax" data-toggle="tab"  data-href="/bitrix/templates/main/components/restoran/favorite.list/.default/ajax.php?part=resume">
                            <div class="left tab_left"></div>
                            <div class="left name">Резюме</div>
                            <div class="left tab_right"></div>
                            <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <!-- tab "panes" -->
            <div class="tab-content">
                <div class="tab-pane sm active" id="catalog">
                    <? 
global $arrFilter;
$arrFilter["CREATED_BY"] = $USER->GetID();

                    $APPLICATION->IncludeComponent("restoran:favorite.list", ".default", array(
                        "IBLOCK_TYPE" => "system",
                        "IBLOCK_ID" => "29",
                        "PARENT_SECTION_CODE" => "catalog",
                        "NEWS_COUNT" => "100",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrFilter",
                        "PROPERTY_CODE" => array(
                            0 => "phone",
                            1 => "address",
                            2 => "subway",
                            3 => "reviews_bind",
                            4 => 'REVIEWS'
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_SHADOW" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "200",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => ""
                            ), false
                    );?>
                </div>
                <div class="tab-pane sm " id="cookery"></div>    
                <div class="tab-pane sm " id="vacancy"></div>
                <div class="tab-pane sm" id="resume"></div>
            </div>
            <div class="clear"></div>
            <br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "bottom_content_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
	</div>
	<div class="right-side">        
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_2_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>            
            <div class="title">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            //$arrFilterSim["PROPERTY_RECOMENDED_VALUE"] = "Да";
            $arrFilterTop4["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arrFilterTop4["!PREVIEW_PICTURE"] = false;
            $arrFilterTop4["!PROPERTY_restoran_ratio"] = false;  
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "rand",
                        "SORT_ORDER1" => "",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "10",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended"
                ),
            false
            );?>
            <br /><br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
	</div>
	<div class="clearfix"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>