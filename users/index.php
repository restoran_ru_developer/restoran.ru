<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профиль пользователя");
if (CSite::InGroup(Array(9))&&$_REQUEST["USER_ID"]==$USER->GetID())
    LocalRedirect("/restorator/profile.php");
?>

<?$APPLICATION->IncludeComponent("restoran:main.profile", "users_profile", Array(
    "ID" => (int)$_REQUEST["USER_ID"],
    "USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "USER_PROPERTY" => Array("UF_HIDE_CONTACTS"),	// Показывать доп. свойства
    "SEND_INFO" => "N",	// Генерировать почтовое событие
    "CHECK_RIGHTS" => "N",	// Проверять права доступа
    "AJAX_OPTION_SHADOW" => "Y",	// Включить затенение
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    ),
    false
);?>
<div id="content" class="block">
    <div class="left-side">
        <?
        global $arrFilterTop4;
        $arrFilterTop4["CREATED_BY"] = (int)$_REQUEST["USER_ID"];
        $arrFilterTop4["IBLOCK_TYPE"] = Array("on_plate","cookery","reviews","comment","news","overviews","afisha","interview","photoreports","blogs");
        //$arrFilterTop4["IBLOCK_TYPE"] = Array("cookery","reviews","comment","blogs");        
        
        ?>
        <?$APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "activity",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "",
                        "IBLOCK_ID" => "",
                        "NEWS_COUNT" => 20,
                        "SORT_BY1" => "DATE_CREATE",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("ratio","reviews_bind"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3601",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "ACTIVITY" => "Y"
                ),
            false
            );?>
    </div>
    <div class="right-side">
         <?$APPLICATION->IncludeComponent(
            "bitrix:socialnetwork.user_friends",
            "profile_friends_list",
            Array(
                "SET_NAV_CHAIN" => "N",
                "ITEMS_COUNT" => "35",
                "PATH_TO_USER" => "",
                "PATH_TO_LOG" => "",
                "PATH_TO_USER_FRIENDS_ADD" => "",
                "PATH_TO_USER_FRIENDS_DELETE" => "",
                "PATH_TO_SEARCH" => "",
                "PAGE_VAR" => "",
                "USER_VAR" => "",
                "ID" => $arResult["ID"],
                "SET_TITLE" => "N"
            ),
            false
        );?>
        <br />
        <a href="/users/list/" class="uppercase font12">Все пользователи</a>
        <br /><br />
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>