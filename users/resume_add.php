<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Добавление резюме");
// get resume iblock info
$arResumeIB = getArIblock("resume", CITY_ID);

//var_dump($arResumeIB);
?>
<div id="content">
    <? if (isset($_REQUEST['ID'])): ?>
        <h1>Изменение резюме</h1>
    <? else: ?>
        <h1>Добавление резюме</h1>
    <? endif; ?>


<div class="grey"><i>Резюме хранится на сайте 30 дней, после этого сохраняется в вашем личном кабинете</i></div>

    <?
    //v_dump($_REQUEST);
    $APPLICATION->IncludeComponent(
            "restoran:editor2_fixed", "vacancy", Array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600",
        "CACHE_NOTES" => "",
        "ELEMENT_ID" => $_REQUEST["ID"],
        "IBLOCK_ID" => $arResumeIB["ID"],
        "PARENT_SECTION" => $_REQUEST["SECTION"],
        "REQ" => array(
            array("NAME" => "Должность", "TYPE" => "short_text", "CODE" => "NAME", "VALUE_FROM" => "NAME", "REQUIRED"=>"Y"),
            array("NAME" => "Дата (с)", "TYPE" => "short_text", "CODE" => "ACTIVE_FROM", "VALUE_FROM" => "ACTIVE_FROM"),
            //array("NAME" => "Дата (по)", "TYPE" => "short_text", "CODE" => "ACTIVE_TO", "VALUE_FROM" => "ACTIVE_TO"),
            array("NAME"=>"Раздел", "TYPE"=>"select", "CODE"=>"PROPERTY_POSITION", "VALUE_FROM"=>"PROPERTIES__POSITION__VALUE", "REQUIRED"=>"Y", "VALUES_LIST" => "PROPS__POSITION__LIST",),			
            array("NAME" => "Образование", "TYPE" => "select", "CODE" => "PROPERTY_EDUCATION", "VALUE_FROM" => "PROPERTIES__EDUCATION__VALUE", "VALUES_LIST" => "PROPS__EDUCATION__LIST", "REQUIRED"=>"Y"),
            array("NAME" => "Опыт работы", "TYPE" => "select", "CODE" => "PROPERTY_EXPERIENCE", "VALUE_FROM" => "PROPERTIES__EXPERIENCE__VALUE", "VALUES_LIST" => "PROPS__EXPERIENCE__LIST", "REQUIRED"=>"Y"),
            array("NAME" => "Контактный телефон", "TYPE" => "short_text", "CODE" => "PROPERTY_CONTACT_PHONE", "VALUE_FROM" => "PROPERTIES__CONTACT_PHONE__VALUE", "REQUIRED"=>"Y"),
            array("NAME" => "Заработная плата (от)", "TYPE" => "short_text", "CODE" => "PROPERTY_SUGG_WORK_ZP_FROM", "VALUE_FROM" => "PROPERTIES__SUGG_WORK_ZP_FROM__VALUE"),
            array("NAME" => "Заработная плата (до)", "TYPE" => "short_text", "CODE" => "PROPERTY_SUGG_WORK_ZP_TO", "VALUE_FROM" => "PROPERTIES__SUGG_WORK_ZP_TO__VALUE"),
            array("NAME" => "График работы", "TYPE" => "multi_select", "CODE" => "PROPERTY_SUGG_WORK_GRAFIK", "VALUE_FROM" => "PROPERTIES__SUGG_WORK_GRAFIK__VALUE_ENUM_ID", "VALUES_LIST" => "PROPS__SUGG_WORK_GRAFIK__LIST"),
            array("NAME" => "Метро", "TYPE" => "multi_select", "CODE" => "PROPERTY_SUGG_WORK_SUBWAY", "VALUE_FROM" => "PROPERTIES__SUGG_WORK_SUBWAY__VALUE", "VALUES_LIST" => "PROPS__SUGG_WORK_SUBWAY__LIST"),
            array("NAME" => "Пожелания к месту работы", "TYPE" => "short_text", "CODE" => "PROPERTY_SUGG_WORK_MOVE", "VALUE_FROM" => "PROPERTIES__SUGG_WORK_MOVE__VALUE"),
            array("NAME" => "О себе", "TYPE" => "short_text", "CODE" => "PROPERTY_ADD_USER_INFO", "VALUE_FROM" => "PROPERTIES__ADD_USER_INFO__VALUE","SHOW_CO"=>"Y","LEN"=>500),
            array("NAME" => "Фотография", "TYPE"=>"photo", "CODE"=>"DETAIL_PICTURE", "VALUE_FROM"=>"DETAIL_PICTURE"),
            array("NAME" => "Начало работы", "TYPE" => "short_text2", "CODE" => "PROPERTY_EXP_WHEN_1", "VALUE_FROM" => "PROPERTIES__EXP_WHEN_1__VALUE", "BL" => "Y"),
            array("NAME" => "Окончание", "TYPE" => "short_text2", "CODE" => "PROPERTY_EXP_WHEN_2", "VALUE_FROM" => "PROPERTIES__EXP_WHEN_2__VALUE", "BL" => "Y"),
            array("NAME" => "Организация", "TYPE" => "short_text2", "CODE" => "PROPERTY_EXP_WHERE", "VALUE_FROM" => "PROPERTIES__EXP_WHERE__VALUE", "BL" => "Y"),
            array("NAME" => "Должность", "TYPE" => "short_text2", "CODE" => "PROPERTY_EXP_POST", "VALUE_FROM" => "PROPERTIES__EXP_POST__VALUE", "BL" => "Y"),
            array("NAME" => "Обязанности", "TYPE" => "short_text2", "CODE" => "PROPERTY_EXP_RESPONS", "VALUE_FROM" => "PROPERTIES__EXP_RESPONS__VALUE", "BL" => "Y"),
        ),
        "BACK_LINK" => "/users/id" . (int) $_REQUEST["USER_ID"] . "/work/#resume"
            ), false
    );
    ?>    

<input type="hidden" id="type_job" value="resume" />
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>