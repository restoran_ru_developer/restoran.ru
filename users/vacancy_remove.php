<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


$res = CIBlockElement::GetByID($_REQUEST["id"]);
if ($ar_res = $res->GetNext()) {
    $creatorId = ($ar_res['CREATED_BY']);
}
if ($creatorId == $USER->GetId()) {
    if (CIBlockElement::Delete($_REQUEST["id"])) {
        echo 'ok';
    } else {
        echo 'error';
    }
}
?>