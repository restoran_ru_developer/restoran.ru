<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Банеры на Restoran.ru");
if(isset($_GET['formresult']) && $_GET['formresult'] == 'addok')
{
	LocalRedirect($APPLICATION->GetCurPage(false)."?success=Y");
}
?>
<style>
    .layout {width:975px; margin:0 auto;}
    .layout .col1 {width:728px;}
    .layout .col2 {width:227px;}
    .layout .empt {width:20px;}

    
                .profile-rating strong {font-size:27px; margin-bottom:6px; line-height:27px;}
                .profile-rating em {display:block; font-style:normal; margin-top:6px;}

    .profile-data {margin-left:229px; width:495px; margin-left:-36px;}
            .profile-data td {padding-left:36px; padding-bottom:18px; color:#1a1a1a; font-size:14px;}
            .profile-data td em {font-size:12px;}
    .profile-stat {color:#c0c0c0;}

    .profile-activity {width:100%; font:12px Georgia; border-bottom:1px solid #1a1a1a; margin-bottom:12px; margin-top:-18px;}
    .profile-activity tr {background:url(../_images/border-1px.gif) 0 100% repeat-x;}
    .profile-activity td {padding:18px 0;}
    .profile-activity th {background:#f2f2f2; font:italic 12px Georgia, "Times New Roman", Times, serif; padding:12px 0;}
    .profile-activity th.first {padding-left:16px; width:180px}
    .profile-activity a {text-decoration:none;}
    .profile-activity img {float:left; margin-right:18px; margin-bottom: 20px;}
            td.profile-activity-col1 {width:108px; color:#c0c0c0; font:italic 12px Georgia, "Times New Roman", Times, serif;}
                    td.profile-activity-col1 a {color:#c0c0c0;}
                    td.profile-activity-col1 p {background:#fff; position:relative; top:-19px; padding-top:19px;}
            td.profile-activity-col2 {padding-right:14px; width:374px;}
            td.profile-activity-col3 {padding-left:12px;}

    .profile-activity.w100 th {padding:18px 14px; white-space:nowrap;}
    .profile-activity.w100 td {padding:12px 14px;}

    .more-activity {text-align:right;}
            .more-activity a {text-transform:uppercase; text-decoration:none; color:#1a1a1a;}

    .profile-activity-rating img {float:none;}


    /* === Premium === */
    .block-premium {width:100%; background:url(../_images/premium.png) center 0 no-repeat #3b414e; height:182px; padding-top:32px; text-align:center; color:#fff; font-size:14px;}
    .block-premium-bottom {width:100%; background:#3b414e; height:133px; padding-top:32px; text-align:center; color:#fff; font-size:14px;}
            .block-premium-bottom .colorize {font-size:16px;}
                    .block-premium-bottom .colorize big {font-size:36px;}
                    .block-premium-bottom .colorize span {font-size:36px; color:#24a6cf;}
            .block-premium .upcase {font-size:11px; color:#fff;}
            .block-premium-bottom .upcase {font-size:11px; color:#fff;}
            .block-premium h3.upcase {font:36px Georgia, "Times New Roman", Times, serif}
            .block-premium h3.upcase:first-letter {font-size:47px;}
            .upcase-premium {font:14px Georgia, "Times New Roman", Times, serif; text-transform:uppercase;}
            .upcase-premium:first-letter {font:20px Georgia, "Times New Roman", Times, serif; text-transform:uppercase;}

    .w-new {padding-right:0 !important;}
            .activity-actions {position:relative; width:185px}
                    .activity-actions div {position:absolute; right:0; top:0; margin-right:-14px;}
                    .activity-actions a {display:block; font:italic 12px Georgia, "Times New Roman", Times, serif; color:#1a1a1a; text-decoration:underline; padding-left:24px; margin-bottom:6px; width:80px;}
                    .activity-actions a:hover {text-decoration:none;}
                            a.icon-continue {background:url(../_images/icon-continue.gif) no-repeat 0 0;}
                            a.icon-delete {background:url(../_images/icon-delete.gif) no-repeat 0 0;}
                            a.icon-print {background:url(../_images/icon-print.png) no-repeat 0 0;}			
                            a.razmes {padding:8px 12px; background:#f2f2f2; font:11px Georgia, "Times New Roman", Times, serif;text-align:center; text-decoration:none; text-transform:uppercase; margin-top:14px; margin-bottom:12px}
    .w-auto {width:auto !important; padding-left:0 !important; padding-right:0 !important;}
    .w-auto div {width:auto;}
    .lh24 {font:14px/24px Georgia, "Times New Roman", Times, serif}
    .lh24 img {margin-top:7px}
    .nowrap {white-space:nowrap; font-size:20px;}

    .plan-1, .plan-2, .plan-3 {float:left; width:220px; padding-right:27px;}
    .plans {overflow:hidden;}
            .plans .upcase-premium {font:18px Georgia, "Times New Roman", Times, serif; text-transform:uppercase;}
            .plans .upcase-premium:first-letter {font:28px Georgia, "Times New Roman", Times, serif; text-transform:uppercase;}
            .plan-desc {font:30px Georgia, "Times New Roman", Times, serif}
            .plans li {margin-bottom:12px;}
            .plan-desc i {font:italic 12px Georgia, "Times New Roman", Times, serif}
    .plan-desc {Noverflow:hidden}
    .plan-desc form .submit-blue {float:none; background:url(../_images/blue-plus.png) 12px 50% no-repeat #24a6cf; padding-left:28px}
    .plan-desc-ul {font-size:12px;}
            .plan-desc-ul strong {font:20px Georgia, "Times New Roman", Times, serif}
            .plan-desc-ul em {}
            .plan {margin-bottom:60px;}


    /* === Else === */
    .social-icons {background:#f5f5f5; height:54px; font:14px/54px Georgia, "Times New Roman", Times, serif; color:#1a1a1a; margin-bottom:48px;}
    .wrap-div {width:975px; margin:0 auto;}
    .upcase {color:#1a1a1a; font-size:9px; text-transform:uppercase;}
    big {font-size:18px; line-height:12px;; vertical-align:baseline; }
    .profile-activity td ul li {margin-bottom:6px;}
    .bg-gray {background:#f2f2f2; padding-top:90px;}
    .w-small {width:116px !important;}
    td.status {width:80px;}
    th.status {text-indent:30px}
    .status .w-small {width:auto !important;}

    /* === TYPO === */
    h2 {font:24px Georgia, "Times New Roman", Times, serif; color:#1a1a1a; margin-bottom:22px;}
    p {margin-bottom:12px;}

    .user-friends {background-color:#f2f2f2 !important;}
    .user-friends ul {overflow:hidden; width:100%;}
    .user-friends li {position:relative; overflow:hidden; float:left; margin-bottom:24px !important; width:50%; padding-bottom:18px; min-height:80px;}
    .user-friends img {float:left;}
    .user-friend {line-height:18px; position:relative !important; overflow:hidden; padding-bottom:12px;}
            .user-friend-mail {background:url(../_images/user-friend-mail.gif) 0 3px no-repeat; position:absolute; bottom:0; margin-bottom:0;}
            .user-friend-mail a {color:#1a1a1a; text-decoration:underline; margin-left:24px;}
    .rbord {
    -moz-border-radius: 5px; /* Firefox */
    -webkit-border-radius: 5px; /* Safari, Chrome */
    -khtml-border-radius: 5px; /* KHTML */
    border-radius: 5px; /* CSS3 */
    behavior: url('border-radius.htc'); /* Û˜ËÏ IE border-radius */
            border:1px solid #ccc;
            padding:4px 6px;
    }

    /* === ELSE === */
    .profile-activity .user-friends.nofloat {width:200px; background-color:transparent !important; /* padding-bottom:28px !important */}
    .profile-activity .nofloat .user-friend {overflow:hidden; position:relative;}
    .profile-activity .nofloat ul {}
    .profile-activity .nofloat li {width:auto; float:none; overflow:hidden; position:relative; background:url(../_images/border-1px.gif) 0 100% repeat-x;}
    .profile-activity .nofloat .user-friend-mail {margin-bottom:0;}
    .profile-activity .nofloat p {}
    .profile-activity .nopic {cbackground:none !important;}
    .profile-activity .nobg {background:none !important;}
    .profile-activity .nopic .user-friend p {margin-bottom:0;}

    .profile-tabs {margin-bottom:48px; width:100%; border-bottom:2px solid #000;}
    .profile-tabs:after{content:''; display:block; clear:both;}
    .profile-tabs li {float:left; padding:12px 24px; position:relative; top:4px;}
    .profile-tabs li.active {border:1px solid #000; border-bottom:2px solid #fff; top:2px;}
    .profile-tabs li a {font:18px Georgia;}
    .profile-tabs li.active a {color:#1a1a1a; text-decoration:none;}


    p.block-expand {text-align:left !important; height:28px; margin-bottom:-5px;}
    p.block-expand a {line-height:28px !important; background:url(../_images/expand-arrow.gif) 100% 50% no-repeat; text-align:left !important;}

    table.compact-table th {padding-left:8px !important; padding-right:8px !important}
    table.compact-table td {padding-left:8px !important; padding-right:8px !important}

    .guest-name {margin-top:12px;}
    .guest-name em {display:block; margin-bottom:6px}

    .my_rest ul strong {float:left; width:164px; padding-top:8px; font:14px Georgia; color:#1a1a1a;}
    .my_rest ul p.inpwrap {}
            .my_rest li.item-row:after {content:''; clear:both; display:block; overflow:hidden}
            .my_rest li.item-row {margin-bottom:0px}
    .my_rest ul input {width:793px; padding:8px; border:1px solid #aaa;}
            .my_rest ul input.checkbox {width:auto; margin-top:5px; border:1px solid #aaa;}
            .my_rest ul input.submit {width:auto; border:0; background:#f2f2f2; padding:10px 18px; color:#1a1a1a; font:12px Georgia; text-transform:uppercase; float:right; margin-top:10px; margin-bottom:12px;}
    .my_rest ul textarea {width:793px; padding:8px; border:1px solid #aaa; height:132px; font:12px Georgia}
    .my_rest ul .fl-right {float:right}
    .my_rest ul.chzn-choices {width:809px}
    .my_rest li.item-row.flr input {width:470px}
    .my_rest li.item-row.flr .fl-right input {width:100px; margin-left:12px}
    .search-choice span {font:12px Georgia !important; color:#1a1a1a; padding:0 4px 0 0}
    .my_rest .default {top:3px; position:relative;}
    input.submit-blue {width:auto !important; border:0; background:#24a6cf; padding:10px 18px; color:#fff; font:12px Georgia; text-transform:uppercase; float:right; margin-top:10px; margin-bottom:12px;}
    input.submit-gray {width:auto; border:0; background:#3b414e; padding:10px 18px; color:#fff; font:12px Georgia; text-transform:uppercase; float:right; margin-top:10px; margin-bottom:12px; position:relative; right:-106px;}

    a.gray-link {padding:11px 18px; color:#1a1a1a; font:12px Georgia; text-transform:uppercase; float:left; margin-top:10px; margin-bottom:12px; text-decoration:none; margin-left:-18px;}
    .mail-actions {overflow:hidden;}

    .niceCheck {
            width: 33px;
            height: 33px;
            display: inline-block;
            cursor: pointer;
            background: url(../_images/checkbox-sprite.png);
    }
    .niceCheck input {
            display: none;
    }



    .mail {width:100%; font:12px/19px Georgia, "Times New Roman", Times, serif; color:#1a1a1a;}
    .mail td {padding:14px;}
    .mail tr.active {background:#f5f5f5;}
    .mail tr.active .mail-avatar {background:#f5f5f5;}
    .mail tr.active .mail-who {background:#f5f5f5;}

    .mail .mail-avatar {background:#fff;}
    .mail .mail-who {white-space:nowrap; background:#fff; padding-right:28px;}
    .mail .mail-text {padding-left:0}

    .mail .mail-autor {min-height:3em;}
    .mail .mail-del {width:64px; text-align:right;}
    .mail .mail-del a {color:#1a1a1a}
    .mail .mail-text textarea {width:100%; height:136px; border:1px solid #d7d7d7; margin-top:24px}

    tr.dotted {background:url(../_images/border-1px.gif) 0 100% repeat-x;}
    tr.solid {border-bottom:1px solid #000;}



    .friendlist {width:100%}
    .friendlist tr {background:url(../_images/border-1px.gif) 0 100% repeat-x;}
    .friendlist td {padding:20px; padding-left:0; padding-top:30px}

    .friendlist .fleft {float:left; font:14px Georgia, "Times New Roman", Times, serif; text-align:center; margin-right:12px;}
    .friendlist .fleft a {text-decoration:none; color:#1a1a1a; font:9px Georgia, "Times New Roman", Times, serif; text-transform:uppercase;}
    .friendlist .fleft img {display:block; margin-bottom:20px}

    .friendlist .fright {overflow:hidden; position:relative; padding-bottom:16px;}
    .friendlist .fright p {line-height:28px}
    .friendlist .fright em {line-height:20px}

    .friendlist p {font:14px Georgia, "Times New Roman", Times, serif;}
    .friendlist a {font:14px Georgia, "Times New Roman", Times, serif;}
    .friendlist .user-mail {background:url(../_images/user-friend-mail.gif) 0 3px no-repeat;}
    .friendlist .user-mail a {color:#1a1a1a; text-decoration:underline; margin-left:24px; font:12px Georgia, "Times New Roman", Times, serif;}
    .friendlist .user-invitation {white-space:nowrap; margin-bottom:0;}
    .friendlist .user-invitation a {color:#1a1a1a; text-decoration:underline; font:12px Georgia, "Times New Roman", Times, serif;}


    .blog-menu {float:left; width:165px;}
    .blog-items {overflow:hidden;}
    .blog-menu li {margin-bottom:12px;}
    .blog-menu em {font:12px Georgia, "Times New Roman", Times, serif; color:#1a1a1a; border-bottom:2px solid #c6c6c6}
    .blog-menu a {font:14px Georgia, "Times New Roman", Times, serif; text-decoration:none;}

    .blog-item {border-bottom:1px solid #000; padding-right:200px; position:relative; margin-bottom:24px; padding-bottom:6px}
    .blog-item p {margin-bottom:18px}
    .blog-actions {position:absolute; right:0; top:0; text-align:center}
    .blog-actions .blog-rborder {border:1px solid #e3e3e3; padding:6px 12px; margin-bottom:12px;
    -moz-border-radius: 4px; /* Firefox */
    -webkit-border-radius: 4px; /* Safari, Chrome */
    -khtml-border-radius: 4px; /* KHTML */
    border-radius: 4px; /* CSS3 */
    behavior: url('border-radius.htc'); /* Û˜ËÏ IE border-radius */
    }
    .blog-actions .blog-rborder a {text-decoration:none;}

    .blog-actions a {color:#1a1a1a; font:12px Georgia, "Times New Roman", Times, serif;}
            .blog-top {font:12px Georgia, "Times New Roman", Times, serif; }	
            .blog-top strong {font:12px Georgia, "Times New Roman", Times, serif; color:#1a1a1a; text-transform:uppercase; letter-spacing:1px}
            .blog-top em {font:18px Georgia, "Times New Roman", Times, serif; }	


    .blog-caption {font:16px Georgia, "Times New Roman", Times, serif;}	
    .blog-text {font:Italic 12px Georgia, "Times New Roman", Times, serif; color:#1a1a1a; overflow:hidden;}
    .blog-info {overflow:hidden; position:relative;}
            .blog-item .blog-comments {float:left; width:158px; overflow:hidden;}
            .blog-item .blog-tags {float:left; width:370px; overflow:hidden; padding-left:12px;}
            .blog-item .blog-more {padding-left:24px;}

    .blog-img-prev { text-indent:-5000px; margin-right:6px; margin-bottm:12px;
            width: 38px;
            height: 38px;
            display: -moz-inline-stack;
            display: inline-block;
            vertical-align: middle;
            zoom: 1;
            *display: inline;
            _height: 38px;
                    background:url(../_images/blog-arrows.png) 0 0 no-repeat;
    }

    .blog-img-prev:hover {background:url(../_images/blog-arrows.png) 0 -38px no-repeat;}

    .blog-img-next { text-indent:-5000px; margin-left:6px; margin-bottm:12px;
            width: 38px;
            height: 38px;
            display: -moz-inline-stack;
            display: inline-block;
            vertical-align: middle;
            zoom: 1;
            *display: inline;
            _height: 38px;
                    background:url(../_images/blog-arrows.png) -38px 0 no-repeat;
    }

    .blog-img-next:hover {background:url(../_images/blog-arrows.png) -38px -38px no-repeat;}


    .banner-empty {border-bottom:0 !important; margin-top:6px;}
    .banner-empty tr {background:none !important;}
    .banner-empty td.first {padding-left:16px; width:178px;}
    .banner-empty input.submit {width:auto; border:0; background:#f2f2f2; padding:10px 18px; color:#1a1a1a; font:12px Georgia; text-transform:uppercase; float:left; margin-top:10px; margin-bottom:12px; cursor:pointer; margin-top:13px}

    .bnrs {white-space:nowrap;}
    .bnrs td {padding-right:0 !important; padding-bottom:32px !important;}
    .bnrs td.first {background:#fff;}
    .blue-inline {background:#24a6cf; color:#fff; padding:3px;}


</style>
<div class="wrap-div">
	<h2>Размещение на Restoran.ru</h2>
	<? if(isset($_GET['success']) && $_GET['success'] == 'Y') { ?> 
	<p>Ваша заявка на размещение баннера успешно отрпавлена</p>
	<?}?>
</div>


<?$APPLICATION->IncludeComponent(
	"restoran:banner_list_profile",
	"",
	Array(
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "",
		"SECTION_ID" => ""
	),
false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"banners_suggestions",
	Array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "sale",
		"IBLOCK_ID" => "155",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array("PREVIEW_PICTURE"),
		"PROPERTY_CODE" => array("POSITION", "SIZE", "PRICE_ANIMATION", "PRICE_STATIC"),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?>


<div style="display:none" id="div-order-banner-form">
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"ajax_form_banner_order",
	Array(
		"SEF_MODE" => "N",
		"WEB_FORM_ID" => "6",
		"LIST_URL" => "",
		"EDIT_URL" => "",
		"SUCCESS_URL" => "?success=Y",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"VARIABLE_ALIASES" => Array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID"
		)
	)
);?>
</div>

<div style="display:block" id="div-prolongate-banner-form">
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"ajax_form_banner_prolongate",
	Array(
		"SEF_MODE" => "N",
		"WEB_FORM_ID" => "7",
		"LIST_URL" => "",
		"EDIT_URL" => "",
		"SUCCESS_URL" => "?successp=Y",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"VARIABLE_ALIASES" => Array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID"
		)
	)
);?>
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>