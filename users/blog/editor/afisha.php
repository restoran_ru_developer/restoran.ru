<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<?
if (!CSite::InGroup(Array(14))&&!CSite::InGroup(Array(15)))
{
if (CSite::InGroup(Array(23))&&(CITY_ID!="ast"||CITY_ID!="amt")):
    ShowError("Permission denied");
    die;
endif;?>
<?if (CSite::InGroup(Array(24))&&CITY_ID!="tmn"):
    ShowError("Permission denied");
    die;
endif;
if (CSite::InGroup(Array(28))&&CITY_ID!="kld"):
    ShowError("Permission denied");
    die;
endif;
if (CSite::InGroup(Array(30))&&CITY_ID!="nsk"):
        ShowError("Permission denied");
        die;
    endif;
    if (CSite::InGroup(Array(32))&&CITY_ID!="rga"):
        ShowError("Permission denied");
        die;
    endif;

if (CSite::InGroup(Array(33))&&CITY_ID!="urm"):
        ShowError("Permission denied");
        die;
    endif;

}
?>

<div id="content">	
<?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Шаг", "CODE"=>"add_step"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),					
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Название публикации", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Показывать на сайте", "TYPE"=>"select", "CODE"=>"ACTIVE", "VALUE_FROM"=>"ACTIVE", "VALUES_LIST"=>"PROPS__ACTIVE__LIST"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"DETAIL_PICTURE", "VALUE_FROM"=>"DETAIL_PICTURE"),
			array("NAME"=>"Начало активности", "TYPE"=>"short_text", "CODE"=>"ACTIVE_FROM", "VALUE_FROM"=>"ACTIVE_FROM", "ADD_CLASS"=>"datew"),
                        array("NAME"=>"Окончание активности", "TYPE"=>"short_text", "CODE"=>"ACTIVE_TO", "VALUE_FROM"=>"ACTIVE_TO", "ADD_CLASS"=>"datew"),
                        array("NAME"=>"Дата события", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EVENT_DATE", "VALUE_FROM"=>"PROPERTIES__EVENT_DATE__VALUE", "ADD_CLASS"=>"datew"),
			array("NAME"=>"Время", "TYPE"=>"short_text", "CODE"=>"PROPERTY_TIME", "VALUE_FROM"=>"PROPERTIES__TIME__VALUE"),
			array("NAME"=>"Тип события", "TYPE"=>"select", "CODE"=>"PROPERTY_EVENT_TYPE", "VALUE_FROM"=>"PROPERTIES__EVENT_TYPE__VALUE", "VALUES_LIST"=>"PROPS__EVENT_TYPE__LIST"),
			array("NAME"=>"Адрес", "TYPE"=>"short_text", "CODE"=>"PROPERTY_ADRES", "VALUE_FROM"=>"PROPERTIES__ADRES__VALUE"),
			array("NAME"=>"Дни недели", "TYPE"=>"week", "CODE"=>array("PROPERTY_d1", "PROPERTY_d2", "PROPERTY_d3", "PROPERTY_d4", "PROPERTY_d5", "PROPERTY_d6", "PROPERTY_d7"), "VALUE_FROM"=>array("PROPERTIES__d1__VALUE_ENUM_ID","PROPERTIES__d2__VALUE_ENUM_ID","PROPERTIES__d3__VALUE_ENUM_ID","PROPERTIES__d4__VALUE_ENUM_ID","PROPERTIES__d5__VALUE_ENUM_ID","PROPERTIES__d6__VALUE_ENUM_ID","PROPERTIES__d7__VALUE_ENUM_ID"), "VALUES_LIST"=>array("PROPS__d1__LIST","PROPS__d2__LIST","PROPS__d3__LIST","PROPS__d4__LIST","PROPS__d5__LIST","PROPS__d6__LIST","PROPS__d7__LIST"))
		),
		"TAG_SECTION"=>44504,
		"CAN_ADD_TEXT"=>(CSite::InGroup(Array(15,14)))?"":"Для создания записи заполните модули в основном поле страницы.
Поля со значком * обязательны для заполнения.<br/>
Модули до пунктирной линии - статичны.<br/>
Блоки, размещенные после пунктирной линии, можно добавлять и менять
местами. Для этого наведите курсор на блок и, удерживая
мышку, перетащите в появившийся под/между уже размещенными на странице блоками пунктирный
прямоугольник.<br/>
Дополнительные блоки,  с помощью которых можно дополнить публикацию
фото-, видеоматериалами, высказываниями с фотографией автора (Прямая
речь), находятся в левом поле.",
		"BACK_LINK"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/#afisha.php",
                "SHOW_PHOTO"=>"Y",
                "HIDE_VISRED" =>"Y"
	),
false
);?>
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>