<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">
     <?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Шаг", "CODE"=>"add_step"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),
			array("NAME"=>"Видео", "CODE"=>"add_video"),
			array("NAME"=>"Код видео", "CODE"=>"add_video_code"),
			array("NAME"=>"Ингредиенты", "CODE"=>"ingr")
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Название рецепта", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Показывать на сайте", "TYPE"=>"hidden", "CODE"=>"ACTIVE", "VALUE_FROM"=>"ACTIVE", "VALUES_LIST"=>"PROPS__ACTIVE__LIST", "REQUIRED"=>"Y"),
			array("NAME"=>"Comments", "TYPE"=>"hidden", "CODE"=>"PROPERTY_COMMENTS", "VALUE_FROM"=>"PROPERTIES__COMMENTS__VALUE"),
			array("NAME"=>"minus", "TYPE"=>"hidden", "CODE"=>"PROPERTY_minus", "VALUE_FROM"=>"PROPERTIES__minus__VALUE"),
			array("NAME"=>"plus", "TYPE"=>"hidden", "CODE"=>"PROPERTY_plus", "VALUE_FROM"=>"PROPERTIES__plus__VALUE"),
			array("NAME"=>"summa_golosov", "TYPE"=>"hidden", "CODE"=>"PROPERTY_summa_golosov", "VALUE_FROM"=>"PROPERTIES__summa_golosov__VALUE"),
			/*array("NAME"=>"Родительский раздел", "TYPE"=>"select", "CODE"=>"SECTION_ID", "VALUE_FROM"=>"IBLOCK_SECTION_ID", "VALUES_LIST"=>"SECTIONS"),*/
			array("NAME"=>"Анонс публикации", "TYPE"=>"vis_red2", "CODE"=>"PREVIEW_TEXT", "VALUE_FROM"=>"PREVIEW_TEXT", "REQUIRED"=>"Y"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"DETAIL_PICTURE", "VALUE_FROM"=>"DETAIL_PICTURE"),
			array("NAME"=>"Порций", "TYPE"=>"short_text", "CODE"=>"PROPERTY_porc", "VALUE_FROM"=>"PROPERTIES__porc__VALUE"),
			array("NAME"=>"Кухня", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_cook", "VALUE_FROM"=>"PROPERTIES__cook__VALUE", "VALUES_LIST"=>"PROPS__cook__LIST"),
			array("NAME"=>"Повод", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_povod", "VALUE_FROM"=>"PROPERTIES__povod__VALUE", "VALUES_LIST"=>"PROPS__povod__LIST"),
			array("NAME"=>"Предпочтения", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_predp", "VALUE_FROM"=>"PROPERTIES__predp__VALUE", "VALUES_LIST"=>"PROPS__predp__LIST"),
			array("NAME"=>"Время приготовления", "TYPE"=>"select", "CODE"=>"PROPERTY_prig_time", "VALUE_FROM"=>"PROPERTIES__prig_time__VALUE", "VALUES_LIST"=>"PROPS__prig_time__LIST"),
			array("NAME"=>"Приготовление", "TYPE"=>"select", "CODE"=>"PROPERTY_prig", "VALUE_FROM"=>"PROPERTIES__prig__VALUE", "VALUES_LIST"=>"PROPS__prig__LIST"),
			array("NAME"=>"Категории", "TYPE"=>"select", "CODE"=>"PROPERTY_cat", "VALUE_FROM"=>"PROPERTIES__cat__VALUE", "VALUES_LIST"=>"PROPS__cat__LIST"),
			array("NAME"=>"Основной ингридиент", "TYPE"=>"select", "CODE"=>"PROPERTY_osn_ingr", "VALUE_FROM"=>"PROPERTIES__osn_ingr__VALUE", "VALUES_LIST"=>"PROPS__osn_ingr__LIST"),
			array("NAME"=>"Сложность", "TYPE"=>"select", "CODE"=>"PROPERTY_slognost", "VALUE_FROM"=>"PROPERTIES__slognost__VALUE", "VALUES_LIST"=>"PROPS__slognost__LIST")
		),
		"SHOW_VBLOCK"=>"Y",
		"TAG_SECTION"=>44203,
		"CAN_ADD_TEXT"=>"Для создания записи заполните модули в основном поле страницы.
Поля со значком * обязательны для заполнения.<br/>
Модули до пунктирной линии - статичны.<br/>
Блоки, размещенные после пунктирной линии, можно добавлять и менять
местами. Для этого наведите курсор на блок и, удерживая
мышку, перетащите в появившийся под/между уже размещенными на странице блоками пунктирный
прямоугольник.<br/>
Дополнительные блоки,  с помощью которых можно дополнить публикацию
фото-, видеоматериалами, высказываниями с фотографией автора (Прямая
речь), находятся в левом поле.",
                "BACK_LINK"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/#video_recepts.php"
		
	),
false
);?>      
      
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>