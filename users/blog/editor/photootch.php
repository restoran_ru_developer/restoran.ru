<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<?
if (!CSite::InGroup(Array(14))&&!CSite::InGroup(Array(15)))
{
if (CSite::InGroup(Array(23))&&(CITY_ID!="ast"||CITY_ID!="amt")):
    ShowError("Permission denied");
    die;
endif;?>
<?if (CSite::InGroup(Array(24))&&CITY_ID!="tmn"):
    ShowError("Permission denied");
    die;
endif;
}
?>
<div id="content">	

	<?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Шаг", "CODE"=>"add_step"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest")				
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Название публикации", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME", "REQUIRED"=>"Y"),
			array("NAME"=>"Comments", "TYPE"=>"hidden", "CODE"=>"PROPERTY_COMMENTS", "VALUE_FROM"=>"PROPERTIES__COMMENTS__VALUE"),
			array("NAME"=>"minus", "TYPE"=>"hidden", "CODE"=>"PROPERTY_minus", "VALUE_FROM"=>"PROPERTIES__minus__VALUE"),
			array("NAME"=>"plus", "TYPE"=>"hidden", "CODE"=>"PROPERTY_plus", "VALUE_FROM"=>"PROPERTIES__plus__VALUE"),
			array("NAME"=>"summa_golosov", "TYPE"=>"hidden", "CODE"=>"PROPERTY_summa_golosov", "VALUE_FROM"=>"PROPERTIES__summa_golosov__VALUE"),
			array("NAME"=>"Показывать на сайте", "TYPE"=>"hidden", "CODE"=>"ACTIVE", "VALUE_FROM"=>"ACTIVE", "VALUES_LIST"=>"PROPS__ACTIVE__LIST"),
			array("NAME"=>"Анонс публикации", "TYPE"=>"vis_red2", "CODE"=>"PREVIEW_TEXT", "VALUE_FROM"=>"PREVIEW_TEXT", "REQUIRED"=>"Y"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"DETAIL_PICTURE", "VALUE_FROM"=>"DETAIL_PICTURE"),
			array("NAME"=>"Показывать с", "TYPE"=>"short_text2", "CODE"=>"ACTIVE_FROM", "VALUE_FROM"=>"ACTIVE_FROM"),
			array("NAME"=>"Показывать до", "TYPE"=>"short_text2", "CODE"=>"ACTIVE_TO", "VALUE_FROM"=>"ACTIVE_TO"),
		),
		"TAG_SECTION"=>44502,
		"CAN_ADD_TEXT"=>"Для создания записи заполните модули в основном поле страницы.
Поля со значком * обязательны для заполнения.<br/>
Модули до пунктирной линии - статичны.<br/>
Блоки, размещенные после пунктирной линии, можно добавлять и менять
местами. Для этого наведите курсор на блок и, удерживая
мышку, перетащите в появившийся под/между уже размещенными на странице блоками пунктирный
прямоугольник.<br/>
Дополнительные блоки,  с помощью которых можно дополнить публикацию
фото-, видеоматериалами, высказываниями с фотографией автора (Прямая
речь), находятся в левом поле.",
                "BACK_LINK"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/#photootch.php"
		
	),
false
);?>
	
	     
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>