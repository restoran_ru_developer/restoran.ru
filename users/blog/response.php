<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Блоги пользователя");


$arOverviewsIB = getArIblock("reviews", CITY_ID);
if ($_REQUEST["CODE"]) $id = RestIBlock::GetRestIDByCode($_REQUEST["CODE"]);


$arGroups = $USER->GetUserGroupArray();?>

<?foreach($arGroups as $v) $GRs[]=$v;
if($USER->GetID()==(int)$_REQUEST["USER_ID"] || in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
	$REDACTOR="Y";
}else{
	$REDACTOR="N";
}


if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin()){
	$arrFilter["CREATED_BY"] = (int)$_REQUEST["USER_ID"];
}
$arrFilter["ACTIVE"]=array("Y","N");
?>
<div style="display: none">
    <?print_r($_REQUEST);?>
</div>
<?$APPLICATION->IncludeComponent(
			"restoran:catalog.list",
			"reviews_blog",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "reviews",
				"IBLOCK_ID" => array(115,49), //Вывести массив городов
				"NEWS_COUNT" => "20",
				"SORT_BY1" => "DATE_CREATE",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arrFilter",
				"FIELD_CODE" => array("DATE_CREATE","CREATED_BY"),
				"PROPERTY_CODE" => array("ELEMENT","minus","plus","photos","video","COMMENTS"),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"REDACTOR"=>$REDACTOR,
				//"ARTICLE_EDOTOR_PAGE"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/editor/response.php",
				"ARTICLE_EDOTOR_PAGE"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/editor/review_edit.php",
				"ADD_TO_IB"=>$arOverviewsIB["ID"]
			),
	false);?>