<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Бронирования");
$APPLICATION->AddHeadScript('/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/script.js');
?>
<style type="text/css">
    table{border-collapse:collapse; border-spacing:0; empty-cells:show;}
    table a{color:#30C5F0!important}
    li{vertical-align:bottom; list-style:none;}
    .profile-activity {width:100%; font:12px; border-bottom:0px solid #1a1a1a; margin-bottom:12px; margin-top:-18px;}
    .profile-activity tr {background:url(/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/images/border-1px.gif) 0 100% repeat-x;}
    .profile-activity td {padding:18px 0; font-size:12px;}
    .profile-activity th {background:#f2f2f2; font:italic 12px; padding:12px 0;}
    .profile-activity th.first {padding-left:16px; width:180px}
    .profile-activity td.first p.name {line-height: 20px;}
    .profile-activity a {text-decoration:none;}
    .profile-activity a:before {content:''}
    .profile-activity img {float:left; margin-right:18px;}
    td.profile-activity-col1 {width:108px; color:#c0c0c0; font:italic 12px; white-space:nowrap;}
    td.profile-activity-col1 a {color:#c0c0c0; text-decoration:underline; margin-right:20px;}
    td.profile-activity-col1 p {background:#fff; position:relative; top:-19px; padding-top:19px;}
    td.profile-activity-col2 {padding-right:14px; width:374px;  padding-right:24px;}
    td.profile-activity-col3 {padding-left:12px;}

    .profile-activity.w100 th {padding:18px 14px; white-space:nowrap;}
    .profile-activity.w100 td {padding:12px 14px;}

    .more-activity {text-align:right;}
    .more-activity a {text-transform:uppercase; text-decoration:none; color:#1a1a1a;}

    .profile-activity-rating img {float:none;}

    .w-new {padding-right:0 !important;}
    .activity-actions {position:relative; width:185px}
    .activity-actions div {position:absolute; right:0; top:0; margin-right:-14px;}
    .activity-actions a {display:block; font:italic 12px; color:#1a1a1a; text-decoration:underline; padding-left:24px; margin-bottom:6px; width:80px;height:15px;}
    .activity-actions a:hover {text-decoration:none;}
    a.icon-continue {background:url(/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/images/icon-continue.gif) no-repeat 0 0;margin-left: 30px;}
    a.icon-save {background:url(/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/images/icon-save.gif) no-repeat 0 0;margin-left: 30px;}
    a.icon-delete {background:url(/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/images/icon-delete.gif) no-repeat 0 0;margin-left: 30px;}
    a.icon-print {background:url(/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/images/icon-print.png) no-repeat 0 0;}
    a.razmes {padding:8px 12px; background:#f2f2f2; font-size:11px;text-align:center; text-decoration:none; text-transform:uppercase; margin-top:14px; margin-bottom:12px}
    .w-auto {width:auto !important; padding-left:0 !important; padding-right:0 !important;}
    .w-auto div {width:auto;}
    .lh24 {font:14px/24px}
    .lh24 img {margin-top:7px}
    .nowrap {white-space:nowrap; font-size:20px;}
    .pole {
        width:40px;
    }
</style>

<div id="content">
    <h2>Мои бронирования</h2>
    <br />
    <?
    // TODO move to component //
    // get rest iblock array
    $arRestIB = getArIblock("catalog", CITY_ID);
    // get user booking list
    $rsBron = CIBlockElement::GetList(
        Array("DATE_ACTIVE_FROM" => "DESC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 105,
            //"SECTION_CODE" => CITY_ID,
            "PROPERTY_user" => $USER->GetID(),
            "PROPERTY_hide" => false
        ),
        false,
        false,
        Array("ID", "NAME","ACTIVE_FROM", "PROPERTY_rest", "PROPERTY_status", "PROPERTY_stol", "PROPERTY_guest")
    );
    while($arBron = $rsBron->GetNext()) {        
        // get restaurant info
        //v_dump($arBron);
        if ($arBron["PROPERTY_REST_VALUE"])
        {
            $rsRest = CIBlockElement::GetList(
                Array("SORT" => "ASC"),
                Array(
                    //"IBLOCK_ID" => $arRestIB["ID"],
                    "ID" => $arBron["PROPERTY_REST_VALUE"]
                ),
                false,
                false,
                Array("ID", "NAME", "SECTION_ID", "ACTIVE", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PROPERTY_RATIO")
            );
            if($arRest = $rsRest->Fetch()) {            
                // get and resize img
                $arTmpBron["PICTURE"] = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 73, 'height' => 60), BX_RESIZE_IMAGE_EXACT, true);
                // detail page url
                $arTmpBron["DETAIL_PAGE_URL"] = $arRest["DETAIL_PAGE_URL"];
                // active
                $arTmpBron["ACTIVE"] = $arRest["ACTIVE"];
                // restaurant name
                $arTmpBron["NAME"] = $arRest["NAME"];
                // ratio
                $arTmpBron["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
                // get rest type
                $rsSect = CIblockSection::GetByID($arRest["SECTION_ID"]);
                if($arSect = $rsSect->GetNext())
                    $arTmpBron["REST_TYPE"] = $arSect["NAME"];
            }
        }
            // bron status
            $arTmpBron["STATUS"] = $arBron["PROPERTY_STATUS_VALUE"];
            // bron status enum id
            $arTmpBron["STATUS_ENUM_ID"] = $arBron["PROPERTY_STATUS_ENUM_ID"];
            // get table number
            $arTmpBron["TABLE_NUMBER"] = $arBron["PROPERTY_STOL_VALUE"];
            // get date and time
            $dateTime = explode(" ", $arBron["ACTIVE_FROM"]);
            $tmpTime = explode(":", $dateTime[1]);
            $date = $dateTime[0];
            $time = $tmpTime[0].":".$tmpTime[1];
            if($time == ":") $time = "";
            $arTmpBron["BOOKING_DATE"] = $date;
            $arTmpBron["BOOKING_TIME"] = $time;
            // get table number
            $arTmpBron["PERSON_NUMBER"] = $arBron["PROPERTY_GUEST_VALUE"];
            // get bron ID
            $arTmpBron["BRON_ID"] = $arBron["ID"];

            $arRes["BRON"][] = $arTmpBron;        
    }

    // get scores for booking
    $scores = getUserActionBalls("bronirovanie-stolika", $USER->GetID());
    ?>

    <table class="profile-activity w100" id="my_bron">
        <tr>
            <th class="first">Место</th>
            <th>Статус</th>
            <th>Расположение</th>
            <th>Дата</th>
            <th>Время</th>
            <th style='width:80px'>Персон</th>
            <th></th>
        </tr>
        <?foreach($arRes["BRON"] as $bron):?>
        <tr>
            <td class="first w-new">
                <?if ($bron["NAME"]):?>
                    <?if($bron["ACTIVE"] == "Y"):?><a href="<?=$bron["DETAIL_PAGE_URL"]?>" target="_blank"><?endif?><img src="<?=$bron["PICTURE"]["src"]?>" width="73" height="60" alt="<?=$bron["NAME"]?>" /><?if($bron["ACTIVE"] == "Y"):?></a><?endif?>
                    <p class="name"><?if($bron["ACTIVE"] == "Y"):?><a href="<?=$bron["DETAIL_PAGE_URL"]?>" target="_blank"><?endif?><?=$bron["NAME"]?><?if($bron["ACTIVE"] == "Y"):?></a><?endif?></p>
                    <div class="ratio" style="padding:0px;font-size:9px">
                        <?for($i = 1; $i <= 5; $i++):?>
                            <span class="icon-star<?if($i > round($bron["RATIO"])):?>-empty<?endif?>" alt="<?=$i?>"></span>
                        <?endfor?>
                        <div class="clear"></div>
                    </div>
<!--                    <p class="upcase"><?=($bron["REST_TYPE"] ? $bron["REST_TYPE"] : "ресторан")?></p>-->
                <?else:?>
                    <img src="/tpl/images/noname/rest_nnm_new.png" width="73" height="60" />
                    <p class="name">Подбор ресторана</p>
                <?endif;?>
            </td>
            <td align="center">
                <p class="status"><?=($bron["STATUS"]=="отчет")?"Событие состоялось":$bron["STATUS"]?></p>                
                <?if(($bron["STATUS_ENUM_ID"] == "1506" || $bron["STATUS_ENUM_ID"] == "1499") && $scores) echo '<p>начислено<br />'.$scores.' баллов</p>';
                else
                {
                    echo "<i>Внести в бронь изменения вы можете по телефону";
                    if (CITY_ID=="msk")
                        echo "(495) 988-26-56";
                    elseif(CITY_ID=="spb")
                        echo "(812) 740-18-20";
                    elseif(CITY_ID=="kld")
                        echo "(952) 110-75-55";
                    echo "</i>";
                }?>
            </td>
            <td align="center">
                <p class="type"><?=$bron["TABLE_NUMBER"]?></p>
            </td>
            <td align="center">
                <?if($bron["BOOKING_DATE"] == "" && $bron["STATUS_ENUM_ID"] != "1501"):?>
                    <input type="text" name="date" value="" class="pole pl1" />
                <?else:?>
                    <?=$bron["BOOKING_DATE"];?>
                <?endif?>
            </td>
            <td align="center">
                <?if($bron["BOOKING_TIME"] == "" && $bron["STATUS_ENUM_ID"] != "1501"):?>
                    <input type="text" name="time" value="" class="pole pl2 time" />
                <?else:?>
                    <?=$bron["BOOKING_TIME"];?>
                <?endif?>
            </td>
            <td align="center">
                <p class="guests">
                    <?if($bron["PERSON_NUMBER"] == "" && $bron["STATUS_ENUM_ID"] != "1501"):?>
                        <input type="text" name="guest" value="" class="pole pl3" />
                    <?else:?>
                        <?=$bron["PERSON_NUMBER"]?>
                    <?endif?>
                </p>
            </td>
            <td>
                <div class="activity-actions w-small">
                    <?if($bron["STATUS_ENUM_ID"] == "1506" || $bron["STATUS_ENUM_ID"] == "1499"):?>
                        <a class="icon-continue" href="/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/core.php?act=repeat&ID=<?=$bron["BRON_ID"]?>">Повторить</a>
                    <?endif?>

                    <?if(($bron["PERSON_NUMBER"] == "" || $bron["BOOKING_DATE"] == "" || $bron["BOOKING_TIME"] == ":") && $bron["STATUS_ENUM_ID"] != "1501"):?>
                        <a class="icon-save" href="/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/core.php?act=save&ID=<?=$bron["BRON_ID"]?>">Сохранить</a>
                    <?endif?>

                    <?if($bron["STATUS_ENUM_ID"] == "1418" || $bron["STATUS_ENUM_ID"] == "1504"):?>
                        <a class="icon-delete" href="/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/core.php?act=cancel&ID=<?=$bron["BRON_ID"]?>">Отменить</a>
                    <?endif?>
                    <?if ($bron["STATUS_ENUM_ID"]!="1467"):?>
                        <a class="icon-delete" href="/bitrix/templates/.default/components/bitrix/catalog.section_notact/my_bron_list/core.php?act=hide&ID=<?=$bron["BRON_ID"]?>">Удалить</a>
                    <?endif;?>
                </div>
            </td>
        </tr>
        <?endforeach?>
    </table>

	<?
    /*
    $arrFilter["PROPERTY_user"] = $USER->GetID();?>

  <?$APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "my_bron_list", array(
	"IBLOCK_TYPE" => "booking_service",
	"IBLOCK_ID" => "105",
	"SECTION_ID" => "",
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $_SESSION["ORDERS_ORDER"],
	"ELEMENT_SORT_ORDER" => $_SESSION["ORDERS_BY"],
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"],
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "guest",
		1 => "rest",
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);*/?>
     
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>