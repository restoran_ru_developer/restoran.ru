<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

define("CITY_NUM_ID", 2);
define("ADD_IBLOCK_ID", 18);
define("SUBWAY_IBLOCK_ID", 89);

CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");

$el = new CIBlockElement;
$bs = new CIBlockSection;

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

/*
 * check section code
 */
function checkSecCode($iblockID, $code) {
    global $DB;
    mysql_select_db($DB->DBName, $DB->db_Conn);

    $rsRes = CIBlockSection::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $iblockID,
            "CODE" => $code
        ),
        false,
        false,
        Array()
    );
    if($arRes = $rsRes->GetNext())
        return $arRes["ID"];
    else
        return false;
}

/*
 * check el code
 */
function checkElCode($iblockID, $code) {
    global $DB;
    mysql_select_db($DB->DBName, $DB->db_Conn);

    $rsRes = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $iblockID,
            "CODE" => $code
        ),
        false,
        false,
        Array()
    );
    if($arRes = $rsRes->GetNext())
        return $arRes["ID"];
    else
        return false;
}

/*
 * add el to IB
 */
function addElToIB($arAddFields) {
    $el = new CIBlockElement;

    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID" => ($arAddFields["IBLOCK_SECTION_ID"] ? $arAddFields["IBLOCK_SECTION_ID"] : false),
        "IBLOCK_ID"         => $arAddFields["IBLOCK_ID"],
        "PROPERTY_VALUES"   => $arAddFields["PROPERTIES"],
        "NAME"              => $arAddFields["NAME"],
        "CODE"              => $arAddFields["CODE"],
        "ACTIVE"            => "Y",
        "PREVIEW_TEXT"      => $arAddFields["PREVIEW_TEXT"],
        "DETAIL_TEXT"       => $arAddFields["DETAIL_TEXT"],
        "PREVIEW_PICTURE"   => CFile::MakeFileArray($arAddFields["PICTURE"]),
        "DETAIL_PICTURE"    => CFile::MakeFileArray($arAddFields["PICTURE"])
    );
    if($PRODUCT_ID = $el->Add($arLoadProductArray))
        return $PRODUCT_ID;
    else
        return false;
}

/*
 * add sec to IB
 */
function addSecToIB($arSecFields) {
    global $DB;
    mysql_select_db($DB->DBName, $DB->db_Conn);

    $bs = new CIBlockSection;

    $arSecFields = Array(
        "ACTIVE" => $arSecFields["ACTIVE"],
        "IBLOCK_SECTION_ID" => $arSecFields["IBLOCK_SECTION_ID"],
        "IBLOCK_ID" => $arSecFields["IBLOCK_ID"],
        "NAME" => $arSecFields["NAME"],
        "CODE" => $arSecFields["CODE"]
    );
    if($menuSecID = $bs->Add($arSecFields))
        return $menuSecID;
    else
        return false;
}

function replace_urls($string, $rel = 'nofollow'){
    $host = "([a-z\d][-a-z\d]*[a-z\d]\.)+[a-z][-a-z\d]*[a-z]";
    $port = "(:\d{1,})?";
    $path = "(\/[^?<>\#\"\s]+)?";
    $query = "(\?[^<>\#\"\s]+)?";
    return preg_replace("#((ht|f)tps?:\/\/{$host}{$port}{$path}{$query})#i", "&lt;a href=\"$1\" rel=\"{$rel}\"&gt;$1&lt;/a&gt;", $string);
}

function extract_emails_from($string){
    preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);
    return $matches[0][0];
}

function doExternalLinks($str){

    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";

    if(preg_match_all("/$regexp/siU", $str, $matches, PREG_SET_ORDER)) {

        $replaceUrl = 'http://www.'.$_SERVER['HTTP_HOST'].'/';

        foreach($matches as &$match){

            if(!strpos($match[2],$_SERVER['HTTP_HOST'])){// Dont mess with "ondomain" links

                $url = (!strpos('#'.$match[2],'http://')) ? 'http://'.$match[2] : $match[2];//Make sure url has a protocol (lots dont)

                $newUrl = 'http://www.'.$_SERVER['HTTP_HOST'].'/doRedirect.php?goto='.base64_encode($url);

                $str = str_replace('"'.$match[2].'"','"'.$newUrl.'"',$str);

            }
        }
    }
    return $str;
}

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "Hr55RW.C";

// stepping
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
} else {
    $firmsCnt = 0;
    // start execution
    $time_start = microtime_float();

    $db_selected1 = mysql_select_db($DBName1, $link1);
    $strSql = "
        SELECT
            ru_items_catalog_link.*
        FROM
            ru_items_catalog_link
        WHERE
            ru_items_catalog_link.category_id = 6 AND ru_items_catalog_link.item_id > {$curStepID}
        ORDER BY
            item_id ASC
    ";
    $res = mysql_query($strSql, $link1);
    while($ar = mysql_fetch_array($res)) {
        // select restoran DB
        mysql_select_db($DBName1, $link1);

        // get item info
        $strSql = "
            SELECT
              ru_items.*
            FROM
              ru_items
            WHERE
              ru_items.item_id = {$ar["item_id"]}
              AND ru_items.city_id = ".CITY_NUM_ID."
        ";
        $resItem = mysql_query($strSql, $link1);
        if($arItem = mysql_fetch_array($resItem)) {
            // select restoran DB
            mysql_select_db($DBName1, $link1);

            $arFields = Array();

            // set code
            $arFields["CODE"] = $arItem["path"];

            // get text
            $strSql = "
                SELECT
                  ru_items_logos.*
                FROM
                  ru_items_logos
                WHERE
                  ru_items_logos.item_id = {$arItem["item_id"]}
            ";
            $resLogos = mysql_query($strSql, $link1);
            if($arLogos = mysql_fetch_array($resLogos)) {
                $arFields["DETAIL_TEXT"] = trim(strip_tags(htmlspecialchars_decode(stripcslashes(iconv("windows-1251", "utf-8", $arLogos["logo"]))), '<ul><li><br><p>'));
                $arFields["DETAIL_TEXT_TYPE"] = "html";
            }

            // set old item id
            $arFields["PROPERTY_VALUES"]["old_item_id"] = $curStepID = $arItem["item_id"];
            // set activity
            $arFields["ACTIVE"] = ($arItem["is_enable"] ? "Y" : "N");
            // set map coordinates
            $arFields["PROPERTY_VALUES"]["map"] = Array("VALUE" => $arItem["map_x"].",".$arItem["map_y"]);
            // set coordinates            
            $arFields["PROPERTY_VALUES"]["lat"] = $arItem["map_x"];
            $arFields["PROPERTY_VALUES"]["lon"] = $arItem["map_y"];

            // get item props
            $strSql = "
                SELECT
                  ru_items_values.*
                FROM
                  ru_items_values
                WHERE
                  ru_items_values.item_id = {$ar["item_id"]}
            ";
            $arTmpVal = Array();
            $resItemVal = mysql_query($strSql, $link1);
            while($arItemVal = mysql_fetch_array($resItemVal)) {
                // select restoran DB
                mysql_select_db($DBName1, $link1);

                // set props
                switch($arItemVal["param_id"]) {
                    // firm name
                    case "1":
	                    $arItemVal["value"] = stripslashes($arItemVal["value"]);
                        if($arItemVal["_primary"])
                            $arFields["NAME"]  = $arItemVal["value"];
                        elseif(!$arFields["NAME"])
                            $arFields["NAME"]  = $arItemVal["value"];
                    break;
                    case "42":
                        $arFields["PROPERTY_VALUES"]["information"] = array('VALUE' => array('TEXT' => stripslashes($arItemVal["value"]), 'TYPE' => 'html'));
                    break;
                    case "17":
                        $arFields["PROPERTY_VALUES"]["adres"] = htmlspecialchars_decode($arItemVal["value"]);
                    break;
                    case "44":
                        $arFields["PROPERTY_VALUES"]["phone"] = $arItemVal["value"];
                    break;
                    case "23":
                        $arFields["PROPERTY_VALUES"]["opening_hours"] = $arItemVal["value"];
                    break;
                    case "37":
                        $arFields["PROPERTY_VALUES"]["email"] = extract_emails_from($arItemVal["value"]);
                    break;
                    case "38":
						$str = $arItemVal["value"];
						$pattern = '`.*?((http|ftp)://[\w#$&+,\/:;=?@.-]+)[^\w#$&+,\/:;=?@.-]*?`i';
						if(preg_match_all($pattern,$str,$matches))
	                        $arFields["PROPERTY_VALUES"]["site"] = $matches[1][0];
                    break;
                    case "47":
                        $arFields["PROPERTY_VALUES"]["products_services"] = strip_tags($arItemVal["value"]);
                    break;
                }
            }

            $strSql = "
                SELECT
                  ru_items_list_values.*
                FROM
                  ru_items_list_values
                WHERE
                  ru_items_list_values.item_id = {$ar["item_id"]}
            ";
            $arTmpVal = Array();
            $resItemVal = mysql_query($strSql, $link1);
            while($arItemVal = mysql_fetch_array($resItemVal)) {
                // select restoran DB
                mysql_select_db($DBName1, $link1);

                $strSql = "
                    SELECT
                        ru_params_lists.*
                    FROM
                        ru_params_lists
                    WHERE
                        ru_params_lists.param_id = ".$arItemVal["param_id"]." AND ru_params_lists.item_id = ".$arItemVal["value_id"];
                $resTextVal = mysql_query($strSql, $link1);
                $arTextVal = mysql_fetch_array($resTextVal);

                switch($arTextVal["param_id"]) {
                    // subway
                    case "18":
                        if(!$elID = checkElCode(SUBWAY_IBLOCK_ID, $arTextVal["path"])) {
                            // add el to IB
                            $elID = addElToIB(Array(
                                    "IBLOCK_ID" => SUBWAY_IBLOCK_ID,
                                    "NAME" => $arTextVal["title"],
                                    "CODE" => $arTextVal["path"],
                                )
                            );
                            //v_dump($arTextVal["title"]." - ".$arTextVal["path"]);
                            $arFields["PROPERTY_VALUES"]["subway"][] = intval($elID);
                        } else {
                            $arFields["PROPERTY_VALUES"]["subway"][] = intval($elID);
                        }
                    break;
                    // section
                    case "48":
                        if(!$secID = checkSecCode(ADD_IBLOCK_ID, $arTextVal["path"])) {
                            // add sec to IB
                            $secID = addSecToIB(
                                Array(
                                    "ACTIVE" => "Y",
                                    "IBLOCK_ID" => ADD_IBLOCK_ID,
                                    "IBLOCK_SECTION_ID" => false,
                                    "NAME" => $arTextVal["title"],
                                    "CODE" => $arTextVal["path"],
                                )
                            );
                            //v_dump($arTextVal["title"]." - ".$arTextVal["path"]);
                            $arFields["IBLOCK_SECTION_ID"][] = intval($secID);
                        } else {
                            $arFields["IBLOCK_SECTION_ID"][] = intval($secID);
                        }
                    break;
                }
            }

            // select restoran DB
            mysql_select_db($DBName1, $link1);
            // get photos
            $strSql = "
                SELECT
                    ru_items_photos.*
                FROM
                    ru_items_photos
                WHERE
                    ru_items_photos.item_id =".$ar["item_id"];
            $resPhoto = mysql_query($strSql, $link1);
            while($arPhoto = mysql_fetch_array($resPhoto)) {
                //$arFields["PROPERTY_VALUES"]["photos"][] = CFile::MakeFileArray("http://www.restoran.ru/uploads/item_photos/".$ar["item_id"]."/".$arPhoto["file"]);
            }
            $arFields["PREVIEW_PICTURE"] = $arFields["PROPERTY_VALUES"]["photos"][0];

            // set iblock id
            $arFields["IBLOCK_ID"] = ADD_IBLOCK_ID;

            // add rest
            //v_dump($arFields);
            global $DB;
            mysql_select_db($DB->DBName, $DB->db_Conn);
            if($REST_ID = $el->Add($arFields)) {
                echo "Old Item ID: ".$curStepID." New rest ID: ".$REST_ID. " Name: ".$arFields["NAME"]."<br />";
                CIBlockElement::SetElementSection($REST_ID, $arFields["IBLOCK_SECTION_ID"]);
            } else {
                echo "Error: ".$el->LAST_ERROR;
            }
            $firmsCnt++;
        }

        // execution time
        $time_end = microtime_float();
        $time = $time_end - $time_start;
        if($time > 30) {
            echo "<script>setTimeout(function() {location.href = '/parser_firms.php?curStepID=".$curStepID."'}, 3000);</script>";
            break;
        }
    }
    mysql_close($link1);
}
?>