<? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad")):?>
    <div class="today-tomorrow ">
        <?if(LANGUAGE_ID!='en'):?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('d.m.Y')?>" selected="selected" >Сегодня</option>
                <option value="<?=date('d.m.Y',strtotime("+1 day"))?>" >Завтра</option>
                <option value="<?=date('d.m.Y',strtotime("+2 days"))?>"  >Послезавтра</option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?else:?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('d.m.Y')?>" selected="selected" >Today</option>
                <option value="<?=date('d.m.Y',strtotime("+1 day"))?>" >Tomorrow</option>
                <option value="<?=date('d.m.Y',strtotime("+2 days"))?>"  >Day after tomorrow</option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?endif?>
    </div>
<?else:?>
    <div class="today-tomorrow ">
        <?if(LANGUAGE_ID!='en'):?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('Y-m-d')?>" selected="selected" >Сегодня</option>
                <option value="<?=date('Y-m-d',strtotime("+1 day"))?>" >Завтра</option>
                <option value="<?=date('Y-m-d',strtotime("+2 days"))?>"  >Послезавтра</option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?else:?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('Y-m-d')?>" selected="selected" >Today</option>
                <option value="<?=date('Y-m-d',strtotime("+1 day"))?>" >Tomorrow</option>
                <option value="<?=date('Y-m-d',strtotime("+2 days"))?>"  >Day after tomorrow</option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?endif?>
    </div>
<?endif?>