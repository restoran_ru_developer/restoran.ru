<? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad")):?>
    <div class="today-tomorrow ">
        <?if(LANGUAGE_ID!='en'):?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('d.m.Y')?>" selected="selected" >Сегодня, <?=date('j')?> <?=strtolower(FormatDate("F",time()))?></option>
                <option value="<?=date('d.m.Y',strtotime("+1 day"))?>" >Завтра, <?=date('j',strtotime("+1 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+1 day")))?></option>
                <option value="<?=date('d.m.Y',strtotime("+2 days"))?>"  >Послезавтра, <?=date('j',strtotime("+2 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+2 day")))?></option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?else:?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('d.m.Y')?>" selected="selected" >Today, <?=date('j')?> <?=strtolower(FormatDate("F",time()))?></option>
                <option value="<?=date('d.m.Y',strtotime("+1 day"))?>" >Tomorrow, <?=date('j',strtotime("+1 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+1 day")))?></option>
                <option value="<?=date('d.m.Y',strtotime("+2 days"))?>"  >Day after tomorrow, <?=date('j',strtotime("+2 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+2 day")))?></option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?endif?>
    </div>
<?else:?>
    <div class="today-tomorrow ">
        <?if(LANGUAGE_ID!='en'):?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('Y-m-d')?>" selected="selected" >Сегодня, <?=date('j')?> <?=strtolower(FormatDate("F",time()))?></option>
                <option value="<?=date('Y-m-d',strtotime("+1 day"))?>" >Завтра, <?=date('j',strtotime("+1 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+1 day")))?></option>
                <option value="<?=date('Y-m-d',strtotime("+2 days"))?>"  >Послезавтра, <?=date('j',strtotime("+2 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+2 day")))?></option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?else:?>
            <select class="today-tomorrow-select" style="">
                <option value="<?=date('Y-m-d')?>" selected="selected" >Today, <?=date('j')?> <?=strtolower(FormatDate("F",time()))?></option>
                <option value="<?=date('Y-m-d',strtotime("+1 day"))?>" >Tomorrow, <?=date('j',strtotime("+1 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+1 day")))?></option>
                <option value="<?=date('Y-m-d',strtotime("+2 days"))?>"  >Day after tomorrow, <?=date('j',strtotime("+2 day"))?> <?=strtolower(FormatDate("F",time(),strtotime("+2 day")))?></option>
                <option value=""  style="display: none" class="in-select-for-date"></option>
            </select>
        <?endif?>
    </div>
<?endif?>