<span class="wish-list-new-trigger"></span>
<div class="wishes-input-trigger">
    <div class="wishes-list-wrapper">
        <?if(LANGUAGE_ID!='en'):?>
            <span>Частые пожелания:</span>
            <ul>
                <li >По возможности у окна</li>
                <li>Желательно с диванчиками</li>
                <li >Нужны детские стульчики</li>
            </ul>
        <?else:?>
            <span>Frequent requests:</span>
            <ul>
                <li >If possible, the window</li>
                <li>Preferably with sofas</li>
                <li >Need highchairs</li>
            </ul>
        <?endif?>
    </div>
</div>