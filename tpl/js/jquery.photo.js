jQuery.fn.photo = function(options){
    // настройки по умолчанию
    var options = jQuery.extend({    
        
    },options);
    this.next = function() {
        if ($(this).hasClass("photo-plugin"));
        var img = new Image();                                               
        var alt = '';
        img.src = $("#photogalery").find(".img").find("img").attr("src");
        alt = $("#photogalery").find(".img").find("img").attr("alt");
        img.onload = new function(){
            $("#detail_galery_modal img").attr("src",img.src);
            $("#detail_galery_modal p").text(alt);
            showOverflow();
            if ($(window).width()<1100||$(window).height()<700)
            {
                img.width = img.width*0.65;
                img.height = img.height*0.65;
            }
            $("#detail_galery_modal img").animate({"width":+img.width,"height":+img.height},100);
            $("#detail_galery_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))},100);
        }
    };
    this.prev = function() {
            
    };
    this.init = function() {        
        var _this = this;       
        $(this).find("img[photo-url]").addClass("photo-plugin");        
        $("body").append("<div class=big_modal id=detail_galery_modal><div class='modal_close_galery'></div><div class='slider_left1'><a></a></div><img src='' /><p></p><div class='slider_right1'><a></a></div></div>");
        $("#detail_galery_modal .slider_left1").on("click",function(){            
            _this.prev();
        });
        $("#detail_galery_modal .slider_right1").on("click",function(){
            _this.next();
        });
        /*$("#detail_galery_modal").hover(function(){
            $("#detail_galery_modal .slider_left1").fadeIn(300);
            $("#detail_galery_modal .slider_right1").fadeIn(300);
        },function(){
            $("#detail_galery_modal .slider_left1").fadeOut(300);
            $("#detail_galery_modal .slider_right1").fadeOut(300);
        });*/
        $(this).on('click','img[photo-url]',function(e){                  
            var img = new Image();
            img.src = $(this).attr("photo-url");
            img.onload = new function(){
                $("#detail_galery_modal img").attr("src",img.src);
                showOverflow();
                if ($(window).height()<img.height)
                {
                    var old_height = img.height;
                    img.height = $(window).height()*0.75;
                    img.width = img.height*img.width/old_height;
                }
                //$("#detail_galery img").css({"width":"0px","height":"0px"});
                $("#detail_galery_modal img").css({"width":+img.width,"height":+img.height});
                $("#detail_galery_modal").css({"width":+img.width,"height":+img.height,"top":eval($(window).height()/2-img.height/2),"left":eval(($(window).width()/2)-(img.width/2))});
                $("#detail_galery_modal").fadeIn(300);
            }
        });
        $(window).resize(function(){
            if (!$(".big_modal").is("hidden"))
            {
                var img = new Image();
                img.src = $("#detail_galery_modal img").attr("src");
                if ($(window).height()<img.height)
                {
                    var old_height = img.height;
                    img.height = $(window).height()*0.75;
                    img.width = img.height*img.width/old_height;
                }
                $("#detail_galery_modal img").css({"width":+img.width,"height":+img.height});
                $("#detail_galery_modal").css({"width":+img.width,"height":+img.height,"top":eval($(window).height()/2-img.height/2),"left":eval(($(window).width()/2)-(img.width/2))});
            }
        });
        /*$(_this).on('click','.popup_close',function(e){
            _this.fadeOut(options.speed);
        });
        _this.click(function(e){
            e.stopPropagation();
        });
        $('html,body').click(function() {
            _this.fadeOut(options.speed);
        });
        $(window).keyup(function(event) {
          if (event.keyCode == '27') {
            _this.fadeOut(options.speed);
           }
        });        */
        return _this;
    };
    return this.init();
}