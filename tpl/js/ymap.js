function GetMap ()
{
    YMaps.jQuery ( function () {
        map = new YMaps.Map ( YMaps.jQuery ( "#map_area" )[0] );
        var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
        var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
        var typeControl = new YMaps.TypeControl ( [] );
        typeControl.addType ( MTmap );
        typeControl.addType ( MTsat );

        map.setMinZoom (13);
        map.addControl (typeControl);
        map.addControl(new YMaps.Zoom());

    })
    my_style = new YMaps.Style();
       my_style.iconStyle = new YMaps.IconStyle();
       my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
       my_style.iconStyle.size = new YMaps.Point(27, 32);
       my_style.iconStyle.offset = new YMaps.Point(-15, -32);
}

function SetMapCenter ( lat, lng, zoom_i )
{
    zoom = zoom_i ? zoom_i : 16;
    map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
}

function ShowMyBalloon (id)
{
    var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
    jQuery("#YMapsID").append("<div id='baloon"+id+"' class='map_ballon'><a href='"+markers_data[id].url+"' title='Перейти к ресторану' alt='Перейти к ресторану'><div class='balloon_content'>"+markers_data[id].name+"<br />"+markers_data[id].adres+"</div><div class='balloon_tail'> </div></a></div>");
    jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
    jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
}

function hideMyBalloon ()
{
    jQuery(".map_ballon").remove();
}
