<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<noindex>
    <?
    if (CITY_ID=="ast"):
        ShowError("Услуга временно не работает");
        die;
    endif;?>    	
    <?
    global $USER;
    $APPLICATION->IncludeComponent(
        "restoran:form.result.new",
        //($USER->IsAdmin())?"ajax_form_bron_rest_sms_new":"ajax_form_bron_rest_sms",
//        ($_REQUEST["banket"]=="Y")?($USER->IsAdmin()&&SITE_TEMPLATE_ID=='main_2014'?"banket_2016":"banket_2014"):($USER->IsAdmin()&&SITE_TEMPLATE_ID=='main_2014'?"order_2016":"order_2014"),
        ($_REQUEST["banket"]=="Y")?"banket_2016":"order_2016",
        Array(
            "SEF_MODE" => "N",
            "WEB_FORM_ID" => "3",
            "LIST_URL" => "",
            "EDIT_URL" => "",
            "SUCCESS_URL" => "",
            "CHAIN_ITEM_TEXT" => "",
            "CHAIN_ITEM_LINK" => "",
            "IGNORE_CUSTOM_TEMPLATE" => "N",
            "USE_EXTENDED_ERRORS" => "Y",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "3600",
            "MY_CAPTCHA" => "Y",
            "VARIABLE_ALIASES" => Array(
                "WEB_FORM_ID" => "WEB_FORM_ID",
                "RESULT_ID" => "RESULT_ID"
            )
        ),
        false
    );?>
</noindex>


<noindex>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-33504724-1']);
        _gaq.push(['_setDomainName', 'restoran.ru']);


        _gaq.push (['_addOrganic', 'images.yandex.ru', 'text']);
        _gaq.push (['_addOrganic', 'blogs.yandex.ru', 'text']);
        _gaq.push (['_addOrganic', 'video.yandex.ru', 'text']);
        _gaq.push (['_addOrganic', 'mail.ru', 'q']);
        _gaq.push (['_addOrganic', 'go.mail.ru', 'q']);
        _gaq.push (['_addOrganic', 'google.com.ua', 'q']);
        _gaq.push (['_addOrganic', 'images.google.ru', 'q']);
        _gaq.push (['_addOrganic', 'maps.google.ru', 'q']);
        _gaq.push (['_addOrganic', 'rambler.ru', 'words']);
        _gaq.push (['_addOrganic', 'nova.rambler.ru', 'query']);
        _gaq.push (['_addOrganic', 'nova.rambler.ru', 'words']);
        _gaq.push (['_addOrganic', 'gogo.ru', 'q']);
        _gaq.push (['_addOrganic', 'nigma.ru', 's']);
        _gaq.push (['_addOrganic', 'search.qip.ru', 'query']);
        _gaq.push (['_addOrganic', 'webalta.ru', 'q']);
        _gaq.push (['_addOrganic', 'sm.aport.ru', 'r']);
        _gaq.push (['_addOrganic', 'meta.ua', 'q']);
        _gaq.push (['_addOrganic', 'search.bigmir.net', 'z']);
        _gaq.push (['_addOrganic', 'search.i.ua', 'q']);
        _gaq.push (['_addOrganic', 'index.online.ua', 'q']);
        _gaq.push (['_addOrganic', 'web20.a.ua', 'query']);
        _gaq.push (['_addOrganic', 'search.ukr.net', 'search_query']);
        _gaq.push (['_addOrganic', 'search.com.ua', 'q']);
        _gaq.push (['_addOrganic', 'search.ua', 'q']);
        _gaq.push (['_addOrganic', 'poisk.ru', 'text']);
        _gaq.push (['_addOrganic', 'go.km.ru', 'sq']);
        _gaq.push (['_addOrganic', 'liveinternet.ru', 'ask']);
        _gaq.push (['_addOrganic', 'gde.ru', 'keywords']);
        _gaq.push (['_addOrganic', 'affiliates.quintura.com', 'request']);
        _gaq.push (['_addOrganic', 'akavita.by', 'z']);
        _gaq.push (['_addOrganic', 'search.tut.by', 'query']);
        _gaq.push (['_addOrganic', 'all.by', 'query']);


        _gaq.push(['_trackPageview']);
        setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])",15000);


        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter17073367 = new Ya.Metrika({id:17073367,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>


</noindex>
<!-- /Yandex.Metrika counter -->

<iframe src="<?=SITE_TEMPLATE_PATH?>/include/write_counters.php" style="display: none"></iframe>
