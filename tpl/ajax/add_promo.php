<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (check_bitrix_sessid())
{
    $APPLICATION->IncludeComponent(
            "restoran:ajax.promo_activate",
            "",
            Array(
                    "EMAIL" => trim($_REQUEST["email"]),
                    "RUBRIC" => $_REQUEST["rubric"],
            ),
    false
    );
}
else {
    echo "SESS_ID ERROR";
}
?>