<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (!$_REQUEST["CITY_ID"])
    $_REQUEST["CITY_ID"] = CITY_ID;
$arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
$APPLICATION->IncludeComponent(
        "restoran:catalog.filter",
        "json",
        Array(
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arRestIB["ID"],                
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array("kitchen", "average_bill", "subway", "out_city"),
                "PRICE_CODE" => array(),
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "N",
                "LIST_HEIGHT" => "5",
                "TEXT_WIDTH" => "20",
                "NUMBER_WIDTH" => "5",
                "SAVE_IN_SESSION" => "N"
        )
);
?>