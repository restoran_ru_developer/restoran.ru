<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if ($_REQUEST["ID"])
{
    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    $ar_res = $res->GetNext();



    $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE"=>"rating_date"));
    if($ar_props = $db_props->Fetch())
    {
        $old_value = 0;
        if ($ar_props["VALUE"]!=date("d.m.Y"))
        {
            CIBlockElement::SetPropertyValueCode($ar_res["ID"], "rating_date", Array("VALUE"=>date("d.m.Y"),"DESCRIPTION"=>($ar_res["SHOW_COUNTER"]-1)));
            $old_value = $ar_res["SHOW_COUNTER"]-1;
        }
        else
            $old_value = $ar_props["DESCRIPTION"];


        CIBlockElement::SetPropertyValueCode($ar_res["ID"], "stat_day", Array("VALUE"=>($ar_res["SHOW_COUNTER"]-$old_value)));
    }
    $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE"=>"RATIO"));
    if($ar_props = $db_props->Fetch())
    {
        $ration = sprintf("%01.1f",$ar_props["VALUE"]);
    }

    if (is_file($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/images/counter_new_new.png')||SITE_ID=="s2")
    {
        if(LANGUAGE_ID=="en"){
            $img = ImageCreateFromPNG($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/images/counter_new_en.png');
        }
        else {
            $img = ImageCreateFromPNG($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/images/counter_new_new.png');
        }

//        FirePHP::getInstance()->info($ar_res["SHOW_COUNTER"]);
//        FirePHP::getInstance()->info($old_value);


        // Назначаем черный цвет
        $blue = ImagecolorAllocate($img,0,0,0);
        $black = ImagecolorAllocate($img,0,0,0);
        // Выводим счет на изображение        
        $str1 = 210 - strlen(($ar_res["SHOW_COUNTER"]-$old_value))*5;
        $str2 = 195 - strlen($ar_res["SHOW_COUNTER"])*3;


        $text = $ar_res["SHOW_COUNTER"];
        $font = $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/fonts/PTSansRegular.ttf';
        imagettftext($img, 9, 0, $str1, 31, $blue, $font, $ar_res["SHOW_COUNTER"]-$old_value);
        imagettftext($img, 9, 0, $str2, 49, $black, $font, $ar_res["SHOW_COUNTER"]);
        imagettftext($img, 9, 0, 77, 55, $black, $font, $ration);
        // Выводим изображение в стандартный поток вывода
        Header("Content-type: image/png");
        ImagePng($img);
    }
}
?>