<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (check_bitrix_sessid()&&$_REQUEST["arrFilter_pf"]["subway"]):
    if (CModule::IncludeModule("iblock")):
        $arRestIB = getArIblock("catalog", CITY_ID);
        if ($_REQUEST["arrFilter_pf"]["subway"])
        {
            $coord = array();
            $prop_r = CIBlockElement::GetByID(end($_REQUEST["arrFilter_pf"]["subway"]));
            if ($prop_ar = $prop_r->Fetch())
            {
                $db_props = CIBlockElement::GetProperty($prop_ar["IBLOCK_ID"], $prop_ar["ID"], array(), Array("CODE"=>"LAT"));
                if($ar_props = $db_props->Fetch())
                    $coord[0] = floatVal($ar_props["VALUE"]);
                $db_props = CIBlockElement::GetProperty($prop_ar["IBLOCK_ID"], $prop_ar["ID"], array(), Array("CODE"=>"LON"));
                if($ar_props = $db_props->Fetch())
                    $coord[1] = floatVal($ar_props["VALUE"]);
            }
            if (count($coord)>0)
            {   
                echo json_encode($coord);
            }
        }       
    endif;
endif;
?>