<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div class="modal-close">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">╳ <!--&times;--></span><span class="sr-only">Close</span></button>
  </div>
<?
if (check_bitrix_sessid()):
    $APPLICATION->IncludeComponent(
        "restoran:city.selector",
        "city_select_ajax2",
        Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_URL" => "",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "36000004",
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => "Y"
        ),
    false
    );
endif;
?>