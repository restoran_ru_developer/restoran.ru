<?include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>
<?
$answer = array();
$answer['error'] = false;
if(!empty($_POST['restoran']) && !empty($_POST['name'])){
    global $APPLICATION;
    $dataFilter = json_decode($APPLICATION->get_cookie($_POST['name']), true);
    if(empty($dataFilter)){
        $MY_BRONS[0] = $_POST['restoran'];
        $APPLICATION->set_cookie($_POST['name'], json_encode($MY_BRONS), time()+60*60*24*30);
        $answer['message'] = 'Заведение добавленно в избранное';
    }
    else {
        $already_add = false;
        foreach($dataFilter as $one_cookie_key=>$one_cookie_val){
            if($one_cookie_val==$_POST['restoran']){
                $answer['message'] = 'Заведение уже добавленно';
                $already_add = true;
            }
        }
        if(!$already_add){

            $dataFilter[count($dataFilter)+1] = intval($_POST['restoran']);

            $to_add = json_encode($dataFilter);

            $APPLICATION->set_cookie($_POST['name'], $to_add, time()+60*60*24*30);
            $answer['message'] = 'Заведение добавленно в избранное';
        }
    }
}
echo json_encode($answer);?>