<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (!$_REQUEST["CITY_ID"])
    $_REQUEST["CITY_ID"] = CITY_ID;
//$arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
/*    global $arPFilter;
    $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';    
    if ($_REQUEST["subway"])
        $arPFilter["PROPERTY_subway"] = $_REQUEST["subway"];    */
    //$arPFilter["!SECTION_ID"] = false;
   /* $APPLICATION->IncludeComponent("restoran:catalog.list", "rest_json", array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arRestIB["ID"],
            "PARENT_SECTION_CODE" => "restaurants",
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "PROPERTY_rating_date",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "PROPERTY_stat_day",
            "SORT_ORDER2" => "DESC",
            "SORT_BY3" => "NAME",
            "SORT_ORDER3" => "ASC",
            "FILTER_NAME" => "arPFilter",
            "PROPERTY_CODE" => array(
                    //0 => "phone",
                    0 => "address",
                    1 => "subway",
                    //4 => "photos",
                    2 => "ratio",                    
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "Y",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "360000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => "100",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Рестораны",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "search_rest_list",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "N",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
    );*/
    if (!$_REQUEST["page"])
        $_REQUEST["page"] = 1;
    if (!$_REQUEST["PAGEN_1"])
        $_REQUEST["PAGEN_1"] = 1;
    global $arPFilter;
    $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';    
    if ($_REQUEST["subway"])
        $arPFilter["PROPERTY_subway"] = $_REQUEST["subway"];
    if ($_REQUEST["average_bill"])
        $arPFilter["PROPERTY_average_bill"] = $_REQUEST["average_bill"];
    if ($_REQUEST["kitchen"])
        $arPFilter["PROPERTY_kitchen"] = $_REQUEST["kitchen"];
    if ($_REQUEST["feature"])
        $arPFilter["PROPERTY_feature"] = $_REQUEST["feature"];
    if ($_REQUEST["suburban"])
        $arPFilter["PROPERTY_out_city"] = $_REQUEST["suburban"];    
    $APPLICATION->IncludeComponent("restoran:restoraunts.list", "rest_json", Array(
        "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID), // Код информационного блока
        "PARENT_SECTION_CODE" => "restaurants", // Код раздела
        "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"), // Количество ресторанов на странице
        "SORT_BY1" => ($_REQUEST["pageRestSort"] ? $_REQUEST["pageRestSort"] : "NAME"), // Поле для первой сортировки ресторанов
        "SORT_ORDER1" => "ASC", // Направление для первой сортировки ресторанов
        "SORT_BY2" => $sort2, // Поле для второй сортировки ресторанов
        "SORT_ORDER2" => $sortOreder2, // Направление для второй сортировки ресторанов
        "FILTER_NAME" => "arPFilter", // Фильтр
        "PROPERTY_CODE" => array(// Свойства
            0 => "type",
            1 => "kitchen",
            2 => "average_bill",
            3 => "opening_hours",
            4 => "phone",
            5 => "address",
            6 => "subway",
            7 => "ratio",
            8 => "lat",
            9 => "lon",
        ),
        "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
        "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        "CACHE_TYPE" => ($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
        "CACHE_TIME" => "21600", // Время кеширования (сек.)
        "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "N", // Учитывать права доступа
        "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
        "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
        "PAGER_TITLE" => "Рестораны", // Название категорий
        "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
        "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
        "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
        "DISPLAY_DATE" => "Y", // Выводить дату элемента
        "DISPLAY_NAME" => "Y", // Выводить название элемента
        "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
            ), false
    );
?>