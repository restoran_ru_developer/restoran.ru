<?
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    if ($CITY_CODE = htmlspecialchars($_REQUEST['call_me_back_city_id']) && $phone = preg_replace('/\D/', '', $_REQUEST['call_me_back_phone'])) {
        //Питер - banket@restoran.ru
        //Москва - zakaz@restoran.ru
        //riga@restoran.ru
        $CITY_CODE = htmlspecialchars($_REQUEST['call_me_back_city_id']);

        if($CITY_CODE=='spb'){
            $mail_address = 'banket@restoran.ru';
            $city_name = 'Санкт-Петербург';
        }
        elseif($CITY_CODE=='rga'){
            $mail_address = 'rigabron@restoran.ru';
            $city_name = 'Рига';
        }
        elseif($CITY_CODE=='urm'){
            $mail_address = 'rigabron@restoran.ru';
            $city_name = 'Юрмала';
        }
        else {
            $mail_address = 'zakaz@restoran.ru';
            $city_name = 'Москва';
        }

        $arMessage = Array(
            "EMAIL_TO"=> $mail_address,
            "AUTHOR_PHONE"=> $phone,
            'CITY_NAME' => $city_name
        );

        $res = CEvent::Send("CALL_ME_BACK_ORDER", "s1", $arMessage);




        CModule::IncludeModule("iblock");
        $el = new CIBlockElement;
        /**ADD_BRON_TO_SERVICE**/

        if($CITY_CODE){
            if($CITY_CODE=="spb") $SECTION=83823;
            if($CITY_CODE=="msk") $SECTION=83822;
            if($CITY_CODE=="nsk") $SECTION=283288;
            if($CITY_CODE=="rga") $SECTION=274888;
        }
        else {
            if(CITY_ID=="spb") $SECTION=83823;
            if(CITY_ID=="msk") $SECTION=83822;
            if(CITY_ID=="nsk") $SECTION=283288;
            if(CITY_ID=="rga") $SECTION=274888;
        }
        global $USER;
        $PROP["user"]=$USER->GetID();


        $arrFilter=array("PROPERTY_TELEPHONE"=>$phone, "IBLOCK_ID"=>104);
        $res = CIBlockElement::GetList(Array(), $arrFilter, false, false, false);
        if($ob = $res->GetNextElement()){
            $arFieldsu = $ob->GetFields();
            //такой телефон или email есть в базе клиентов

            $PROP["client"]=$arFieldsu["ID"];
        }
        else {
            //  такого клиента нет в базе, нужно его добавить
            $PROP_client["SURNAME"] = 'Перезвоните мне';
            $PROP_client["TELEPHONE"] = $phone;

            $PROP_client["user"] = $USER->GetID();

            //$PROP_client["TELEPHONE"] = str_replace(" ", "", $PROP_client["TELEPHONE"]);

            $arLoadProductArray_client = Array(
                "IBLOCK_ID"=>104,
                "IBLOCK_SECTION" => $SECTION,
                "PROPERTY_VALUES"=> $PROP_client,
                "NAME"           => 'Перезвоните мне',
                "ACTIVE"         => "Y",
                "PREVIEW_TEXT"   => "Клиент создан автоматически",
            );

            if($CLIENT_ID = $el->Add($arLoadProductArray_client)){
                $PROP["client"]=$CLIENT_ID;
            }

        }

//        FirePHP::getInstance()->info($PROP["client"],'client');
        $PROP["new"] = 1549;
        $PROP["status"] = 1418;
        $PROP["source"] = 2853;
        $PROP["type"] = 1498;

        if($CITY_CODE){
            if($CITY_CODE=="spb") $iblock_section_id=83660;
            if($CITY_CODE=="msk") $iblock_section_id=83659;
            if($CITY_CODE=="nsk") $iblock_section_id=283287;
            if($CITY_CODE=="rga") $iblock_section_id=274887;
        }
        else {
            if(CITY_ID=="spb") $iblock_section_id=83660;
            if(CITY_ID=="msk") $iblock_section_id=83659;
            if(CITY_ID=="nsk") $iblock_section_id=283287;
            if(CITY_ID=="rga") $iblock_section_id=274887;
        }

        $arLoadProductArray = Array(
            "ACTIVE_FROM" => date('d.m.Y H:i').":00",
            "MODIFIED_BY"    => false,
            "IBLOCK_SECTION_ID" => $iblock_section_id,
            "IBLOCK_ID"      => 105,
            "PROPERTY_VALUES"=> $PROP,
            "PREVIEW_TEXT" => 'Перезвоните мне',
            "PREVIEW_TEXT_TYPE" => "text",
            "NAME"           => ($USER->GetFullName())?$USER->GetFullName():"Незарегистированный пользователь",
            "ACTIVE"         => "Y",
            "CREATED_BY"=>false
        );
//        FirePHP::getInstance()->info($arLoadProductArray,'$arLoadProductArray');
        $PRODUCT_ID = $el->Add($arLoadProductArray);
//        if()
//            FirePHP::getInstance()->info($PRODUCT_ID,'$PRODUCT_ID');
//        else
//            FirePHP::getInstance()->info("Error: ".$el->LAST_ERROR,'Error');


        if ($PRODUCT_ID){
            $el->Update($PRODUCT_ID, array("CREATED_BY"=>false, "MODIFIED_BY"=>false));
            $ser = str_replace("/m", "", $_SERVER["DOCUMENT_ROOT"]);
            $fp = fopen($ser."/bs/orders_log.txt", "a");
            $mytext = $PRODUCT_ID."\r\n".serialize($_SERVER)."\r\n\r\n";
            fwrite($fp, $mytext);
            fclose($fp);


            $f = fopen($ser."/bs/alerts/orders/".$PRODUCT_ID, "w");
            fwrite($f, CITY_ID);
        }else{
            $ser = str_replace("/m", "", $_SERVER["DOCUMENT_ROOT"]);
            $f = fopen($ser."/bs/alerts/".date("d_m_Y"), "w");
            fwrite($f, $PRODUCT_ID." ".$el->LAST_ERROR);
        }
        fclose($f);

        /**ADD_BRON_TO_SERVICE**/

    }
}
?>