<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (!$_REQUEST["CITY_ID"])
    $_REQUEST["CITY_ID"] = CITY_ID;
if ($_REQUEST["type"]&&(int)$_REQUEST["id"]):
    $arNews = getArIblock(trim($_REQUEST["type"]), $_REQUEST["CITY_ID"]);
    $APPLICATION->IncludeComponent(
            "bitrix:news.detail",
            "json",
            Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "USE_SHARE" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => trim($_REQUEST["type"]),
                    "IBLOCK_ID" => $arNews["ID"],
                    "ELEMENT_ID" => (int)$_REQUEST["id"],
                    "ELEMENT_CODE" => "",
                    "CHECK_DATES" => "N",
                    "FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
                    "PROPERTY_CODE" => Array("COMMENTS","RESTORAN"),
                    "IBLOCK_URL" => "",
                    "META_KEYWORDS" => "keywords",
                    "META_DESCRIPTION" => "description",
                    "BROWSER_TITLE" => "title",
                    "SET_TITLE" => "Y",
                    "SET_STATUS_404" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "USE_PERMISSIONS" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Страница",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_SHOW_ALL" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
            ),
    false
    );
endif;
 ?>
