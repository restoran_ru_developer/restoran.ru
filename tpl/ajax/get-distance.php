<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$answer = array();

function calculateTheDistance($LAT1, $LON1, $LAT2, $LON2) {

    // перевести координаты в радианы
    $lat1 = $LAT1 * M_PI / 180;
    $lat2 = $LAT2 * M_PI / 180;
    $long1 = $LON1 * M_PI / 180;
    $long2 = $LON2 * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

    $ad = atan2($y, $x);
    $dist = $ad * 6371000;

    return $dist;
}

foreach($_REQUEST['lat_lon_list'] as $id_key=>$lat_lon_val):
    $this_lat_lon_arr = explode(',',$lat_lon_val);
    if(($_SESSION['lat']&&$_SESSION['lon'])||($APPLICATION->get_cookie("lat")&&$APPLICATION->get_cookie("lon"))&&$this_lat_lon_arr[0]&&$this_lat_lon_arr[1]){
        $distance = calculateTheDistance($_SESSION['lat']?$_SESSION['lat']:$APPLICATION->get_cookie("lat"),$_SESSION['lon']?$_SESSION['lon']:$APPLICATION->get_cookie("lon"),$this_lat_lon_arr[0],$this_lat_lon_arr[1]);
        $answer[$id_key] = round($distance/1000,1);
    }
endforeach;

echo json_encode($answer);
?>