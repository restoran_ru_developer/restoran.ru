<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)" onclick="$(this).parent().parent().fadeOut(300)"></a>
</div>
<?
$q = (int) $_REQUEST["q"];
$id = (int) $_REQUEST["id"];
if (!$_REQUEST["action"])
{
	if ($id && $q && check_bitrix_sessid())
	{
		if (!CModule::IncludeModule("catalog"))
			return false;
		if (!CModule::IncludeModule("sale"))
			return false;
		Add2BasketByProductID($id, $q);
		$APPLICATION->IncludeComponent(
			"bitrix:sale.basket.basket",
			"dostavka",
			Array(
				"PATH_TO_ORDER" => "/personal/order.php",
				"HIDE_COUPON" => "Y",
				"COLUMNS_LIST" => array("NAME", "PRICE", "QUANTITY"),
				"QUANTITY_FLOAT" => "N",
				"PRICE_VAT_SHOW_VALUE" => "N",
				"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
				"SET_TITLE" => "N"
			),
		false
		);
	}
}
elseif ($_REQUEST["action"]=="delete")
{
	if ($id && check_bitrix_sessid())
	{
		if (!CModule::IncludeModule("catalog"))
			return false;
		if (!CModule::IncludeModule("sale"))
			return false;
		CSaleBasket::Delete($id);
		$APPLICATION->IncludeComponent(
			"bitrix:sale.basket.basket",
			"dostavka",
			Array(
				"PATH_TO_ORDER" => "/personal/order.php",
				"HIDE_COUPON" => "Y",
				"COLUMNS_LIST" => array("NAME", "PRICE", "QUANTITY"),
				"QUANTITY_FLOAT" => "N",
				"PRICE_VAT_SHOW_VALUE" => "N",
				"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
				"SET_TITLE" => "N"
			),
		false
		);
	}
}
?>