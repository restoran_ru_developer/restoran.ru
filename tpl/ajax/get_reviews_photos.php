<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
//$arReviewsIB = getArIblock("reviews", $_REQUEST['CITY_ID']);
//if(!$_REQUEST['REST_NETWORK']){
//    $arReviewsIB = getArIblock_EN_RU("reviews", $_REQUEST['CITY_ID']);
//}
//else {
    $arReviewsIB = getArIblock("reviews", $_REQUEST['CITY_ID']);
//}


if ($_REQUEST["ID"]&&count($arReviewsIB)):
    global $arrFilter;
    $arrFilter = array();

    if($_REQUEST['REST_NETWORK']){
        $REST_NETWORK = explode('|',$_REQUEST['REST_NETWORK']);
        $arrFilter['PROPERTY_ELEMENT'] = $REST_NETWORK;
    }
    else {
        $EN_RU_REST_ID = explode('|',$_REQUEST["ID"]);
        $arrFilter["PROPERTY_ELEMENT"] = $EN_RU_REST_ID;//explode('|',);
    }
    $arrFilter['!PROPERTY_photos'] = false;
    $APPLICATION->IncludeComponent(
        "restoran:catalog.list",
        "reviews-photos",
        Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "reviews",
            "IBLOCK_ID" => $arReviewsIB["ID"],//!$_REQUEST['REST_NETWORK'] ? array($arReviewsIB[0]["ID"],$arReviewsIB[1]["ID"]) : ,
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "created_date",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array("photos"),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",//a
            "CACHE_TIME" => "3600010",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "search_rest_list",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
        ),
        false
    );
endif;
?>