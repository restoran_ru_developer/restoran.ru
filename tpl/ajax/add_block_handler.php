<?
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

    $answer = array();
    $answer['freeze_a'] = false;
    global $APPLICATION;
    if($_REQUEST['act']=='check'){
        $A_B_FREEZE = $APPLICATION->get_cookie("A_B_FREEZE");
        if($A_B_FREEZE=='Y'){
            $answer['freeze_a'] = true;
        }
    }
    else {
        $APPLICATION->set_cookie("A_B_FREEZE", "Y", time()+60*60*24*7, "/","restoran.ru", false, true);
    }
    echo json_encode($answer);
}
?>