<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (check_bitrix_sessid()&&$_REQUEST["ID"]>0)
{
    CModule::IncludeModule("iblock");
    $arRestIB = getArIblock("catalog", CITY_ID);
    $db_props = CIBlockElement::GetProperty($arRestIB["ID"], (int)$_REQUEST["ID"], array("sort" => "asc"), Array("CODE"=>"RATIO"));
    if($ar_props = $db_props->Fetch())
    {        
        $ratio = ceil($ar_props["VALUE"]);           
    ?>
        <?for ($i=1;$i<=5;$i++):?>
            <div class="star<?=($i<=$ratio)?"_a":""?>" alt="<?=$i?>"></div>
        <?endfor;?>    
    <?
    }
}
?>
