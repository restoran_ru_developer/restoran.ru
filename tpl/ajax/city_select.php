<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (check_bitrix_sessid()):
    $APPLICATION->IncludeComponent(
        "restoran:city.selector",
        "city_select_ajax",
        Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_URL" => "",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "36000002",
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => "N"
        ),
    false
    );
endif;
?>