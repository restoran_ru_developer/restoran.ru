<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;
    
define("ADD_IBLOCK_ID", 233);

CModule::IncludeModule("iblock");
$el = new CIBlockElement;

global $DB;

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "Hr55RW.C";

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// stepping
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

// start execution
$time_start = microtime_float();

mysql_select_db($DBName1, $link1);

$strSql = "
    SELECT
        ru_meta.*
    FROM
        ru_meta
    WHERE
        ru_meta.id > {$curStepID}
    ORDER BY
        ru_meta.id ASC
";
$res = mysql_query($strSql, $link1);
while($ar = mysql_fetch_array($res)) {

	$arProp = Array();	
	$arProp = Array(
		"title" => iconv("windows-1251", "utf-8", $ar["title"]),
		"keywords" => iconv("windows-1251", "utf-8", $ar["keywords"]),
		"description" => iconv("windows-1251", "utf-8", $ar["description"]),		
		"canonical" => iconv("windows-1251", "utf-8", $ar["canonical"]),		
		"addons" => iconv("windows-1251", "utf-8", $ar["addons"]),				
	);
	$arLoadMetaArray = Array();
	$arLoadMetaArray = Array(
		"IBLOCK_SECTION_ID"	=> false,
		"IBLOCK_ID"     	=> ADD_IBLOCK_ID,
		"PROPERTY_VALUES"	=> $arProp,
		"NAME"           	=> (substr(trim($ar["path"]), -1) == "/" ? trim($ar["path"]) : trim($ar["path"])."/"),
		"ACTIVE"         	=> ($ar["is_enable"] ? "Y" : "N"),
	);
	
	/*
	if($metaID = $el->Add($arLoadMetaArray))
	  echo "New ID: ".$metaID;
	else
	  echo "Error: ".$el->LAST_ERROR;	
	*/
	
	//break;
	
    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;
    
    $curStepID = $ar["ID"];

    if($time > 35) {
        echo "<script>setTimeout(function() {location.href = '/parser_meta.php?curStepID=".$curStepID."'}, 5000);</script>";
        break;
    }	
}