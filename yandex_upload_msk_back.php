<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

CModule::IncludeModule("iblock");

$lastEl = false;
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);
$lastElID = ($_REQUEST["lastElID"] ? $_REQUEST["lastElID"] : 0);
//$curIblockID = ($_REQUEST["curIblockID"] ? $_REQUEST["curIblockID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// start execution
$time_start = microtime_float();

// set last el id
if($lastElID <= 0) {
    /*
	$rsIblock = CIBlock::GetList(
		Array("ID" => "ASC"),
		Array(
			"SITE_ID" => "s1",
			"TYPE" => "catalog",
			">ID" => $curIblockID		
		),
		false
	);
	while($arIblockLast = $rsIblock->Fetch()) {
    */
    $rsLastEl = CIBlockElement::GetList(
        Array("ID"=>"DESC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 11,
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),            
            "!PROPERTY_sleeping_rest_VALUE" => "Да"
        ),
        false,
        false,
        Array()
    );
    if($arLastEl = $rsLastEl->GetNext())
        $lastElID = $arLastEl["ID"];
    //}
}

if($curStepID == 0 /*&& $curIblockID <= 0*/) {
    file_put_contents("yandex_upload_msk.xml", '', LOCK_EX);

    $str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    $str .= "<companies xmlns:xi='http://www.w3.org/2001/XInclude' version='2.1'>\n";

}
$watermark = Array(
            Array( 'name' => 'watermark',
            'position' => 'br',
            'size'=>'small',
            'type'=>'image',
            'alpha_level'=>'40',
            'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
            ),
        );
$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
		//">ID" => $curIblockID
        "ID" => 11
	),
	false
);

while($arIblock = $rsIblock->Fetch()) {
	//$curStepID = 0;    
	$rsRest = CIBlockElement::GetList(
		Array("ID" => "ASC"),
		Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => 11,
			//">ID" => $curStepID,
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
                        "!PROPERTY_photos" => false,
                        "!PROPERTY_sleeping_rest_VALUE" => "Да"
			/*"!PROPERTY_google_photo" => false*/
		),
		false,
		Array("nTopCount"=>1),
		Array("ID","TIMESTAMP_X", "NAME", "TAGS","DETAIL_PAGE_URL","DETAIL_TEXT", "DATE_CREATE", "PROPERTY_*")
	);
	
	while($arRest = $rsRest->GetNext()) {            
		$curStepID = $arRest["ID"];
		//$curIblockID = $arRest["IBLOCK_ID"];
	
		// get photo
		$photoKey = 0;
		$strPhoto = "\t\t<photos>\n";
		$rsProp = CIBlockElement::GetProperty(
			$arRest["IBLOCK_ID"],
			$arRest["ID"],
			Array(),
			Array(
				"CODE" => "photos"
			)
		);
		while($arProp = $rsProp->GetNext()) {
			if(intval($arProp["VALUE"]) > 0) {                            
				$arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array(), BX_RESIZE_IMAGE_PROPORTIONAL, true,$watermark);                                  
                                
				$strPhoto .= "\t\t\t<photo>\n";
                                    $strPhoto .= "\t\t\t\t<url>http://".SITE_SERVER_NAME.$arPhoto["src"]."</url>\n";
                                if ($arProp["DESCRIPTION"])
                                    $strPhoto .= "\t\t\t\t<alt>".$arProp["DESCRIPTION"]."</alt>\n";
                                else
                                    $strPhoto .= "\t\t\t\t<alt/>\n";
                                    $strPhoto .= "\t\t\t\t<type/>\n";
                                $strPhoto .= "\t\t\t</photo>\n";
				$photoKey++;
				if($photoKey > 2)
					break;
			}
		}
                $strPhoto .= "\t\t</photos>\n";
		if($photoKey > 0) {
			// tmp date
			echo $tmpDate = strtotime($arRest["TIMESTAMP_X"])*1000;
	
			$str .= "\t<company>\n";
				$str .= "\t\t<company-id>".$arRest["ID"]."</company-id>\n";
				$str .= "\t\t<name lang='ru'>".htmlspecialchars($arRest["NAME"])."</name>\n";
                                if ($arRest["TAGS"])
                                    $str .= "\t\t<name-other lang='ru'>".htmlspecialchars($arRest["TAGS"])."</name-other>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "address"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"]) {
						$address = str_replace($arIblock["NAME"].",", "", $arProp["VALUE"]);
						$address = $arIblock["NAME"].", ".$address;
						$str .= "\t\t<address lang='ru'>".$address."</address>\n";
					}	
	
				$str .= "\t\t<country lang='ru'>Россия</country>\n";                                      
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "out_city"
					)
				);
				if($arProp = $rsProp->GetNext())
                                {
					if($arProp["VALUE"])
                                        {
                                            $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                if($arPayment = $rsPayment->GetNext())   
						$str .= "\t\t<sub-locality-name>".$arPayment["NAME"]."</sub-locality-name>\n";
                                        }
                                }
                                else
                                {
                                    $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "area"
					)
                                    );
                                    if($arProp = $rsProp->GetNext())
                                    {
                                            if($arProp["VALUE"])
                                            {
                                                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                if($arPayment = $rsPayment->GetNext())   
                                                    $str .= "\t\t\t<sub-locality-name>".$arPayment["NAME"]."</sub-locality-name>\n";
                                            }
                                    }
                                }   
                                        
                                
                                $f = 0;                                
                                $str_l = "";
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lat"
					)
				);
				if($arProp = $rsProp->GetNext())
                                {
					if($arProp["VALUE"])
                                        {
						$str_l .= "\t\t\t<lat>".$arProp["VALUE"]."</lat>\n";
                                                $f = 1;
                                        }
                                }
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lon"
					)
				);
				if($arProp = $rsProp->GetNext())
                                {
					if($arProp["VALUE"])
					{
                                            $str_l .= "\t\t\t<lon>".$arProp["VALUE"]."</lon>\n";
                                            $f = 1;
                                        }
                                }
                                if ($f == 1)
                                {
                                    $str .= "\t\t<coordinates>\n";
                                    $str .= $str_l;
                                    $str .= "\t\t</coordinates>\n";
                                }
                                    
                                    $str .= "\t\t<phone>\n";
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "phone"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t\t<ext/>\n\t\t\t<number>".$arProp["VALUE"]."</number>\n\t\t\t<info/>\n";
                                $str .= "\t\t</phone>\n";
                                //Email
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "email"
					)
				);
				if($arProp = $rsProp->GetNext())
                                {
					if($arProp["VALUE"])
                                            $str .= "\t\t<email>".strtolower($arProp["VALUE"])."</email>\n";
                                }
                                //Site
                                $rsProp = CIBlockElement::GetProperty(
                                        $arRest["IBLOCK_ID"],
                                        $arRest["ID"],
                                        Array(),
                                        Array(
                                                "CODE" => "site"
                                        )
                                );
                                if($arProp = $rsProp->GetNext())
                                        if($arProp["VALUE"]) {
                                            if(strstr($arProp["VALUE"], ','))
                                                $tmpWebsite = explode(",", $arProp["VALUE"]);
                                            elseif(strstr($arProp["VALUE"], ';'))
                                                $tmpWebsite = explode(";", $arProp["VALUE"]);
                                            else
                                                $tmpWebsite[0] = $arProp["VALUE"];

                                            $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);
                                        }
                                if ($tmpWebsite[0])
                                    $str .= "\t\t<url>"."http://".$tmpWebsite[0]."</url>\n";                                
                                        
                                //Working time
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "opening_hours"
					)
				);
                                if($arProp = $rsProp->GetNext())
                                {
                                    if($arProp["VALUE"])
                                    {
                                        $arProp["~VALUE"] = html_entity_decode($arProp["~VALUE"]);
                                        $arProp["~VALUE"]  = htmlspecialchars_decode($arProp["~VALUE"] );                                        
                                        $arProp["~VALUE"]  = stripslashes($arProp["~VALUE"]);                                        
                                        $str .= "\t\t<working-time lang='ru'>".htmlspecialchars($arProp["~VALUE"])."</working-time>\n";
                                    }
                                }
                                $str .= "\t\t<actualization-date>".$tmpDate."</actualization-date>\n";
                                if ($arRest["DETAIL_TEXT"])
                                {
                                    $obParser = new CTextParser;                                                                        
                                    $arRest["DETAIL_TEXT"] = str_replace("&ndash;","-", $arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = str_replace("&nbsp;"," ", $arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = html_entity_decode($arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"]  = htmlspecialchars_decode($arRest["DETAIL_TEXT"] );                                        
                                    $arRest["DETAIL_TEXT"]  = stripslashes($arRest["DETAIL_TEXT"]);                                        
                                    $arRest["DETAIL_TEXT"] = strip_tags($arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = substr($arRest["DETAIL_TEXT"], 0,255)."...";
                                    $str .= "\t\t<description lang='ru'>".htmlspecialchars($arRest["DETAIL_TEXT"])."</description>\n";
                                }
	
				$str .= $strPhoto;
                                //Wi-fi
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "wi_fi"
                                                )
                                );
                                if($arProp = $rsProp->GetNext())
                                	if($arProp["VALUE"]=="Да")	
                                            $str .= "\t\t<feature-boolean name='wi_fi' value='1'/>\n";
                                
                                                                                
                                //karaoke
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "entertainment"
                                                )
                                );
                                if($arProp = $rsProp->GetNext())
                                	if($arProp["VALUE"]=="1290")	
                                            $str .= "\t\t<feature-boolean name='karaoke' value='1'/>\n"; 
                                        
                                //Sport_translating
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "entertainment"
                                                )
                                );
                                if($arProp = $rsProp->GetNext())
                                	if($arProp["VALUE"]=="2013")	
                                            $str .= "\t\t<feature-boolean name='sports_broadcasts' value='1'/>\n";                                        
                                        
                                //Breakfast
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "breakfast"
                                                )
                                );
                                if($arProp = $rsProp->GetNext())
                                	if($arProp["VALUE"]=="Y")	
                                            $str .= "\t\t<feature-boolean name='breakfast' value='1'/>\n";
                                
                                 //business_lunch
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "business_lunch"
                                                )
                                );
                                if($arProp = $rsProp->GetNext())
                                	if($arProp["VALUE"])	
                                            $str .= "\t\t<feature-boolean name='business_lunch' value='1'/>\n";       
                                 
                                        
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kitchen"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->GetNext())
								$str .= "\t\t<feature-enum-multiple  name='type_cuisine' value='".$arPayment["NAME"]."'>\n";
						}
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "type"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->GetNext())
								$str .= "\t\t<feature-enum-multiple  name='type_public_catering' value='".$arPayment["NAME"]."'>\n";
						}
                                        
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "credit_cards"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->GetNext())
								$str .= "\t\t<feature-enum-multiple  name='accepted_credit_cards' value='".$arPayment["NAME"]."'>\n";
						}
	
						//$str .= "\t\t\t\t<attr name=\"Attire\">Casual</attr>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kolichestvochelovek"
							)
						);
						if($arProp = $rsProp->GetNext())
							$rsSeating = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arSeating = $rsSeating->GetNext())
								$str .= "\t\t<feature-numeric-single name='seats' value='".str_replace("&nbsp;", "", $arSeating["NAME"])."'/>";
	
						//$str .= "\t\t\t\t<attr name=\"Phone Orders\">No</attr>\n";	
			$str .= "\t</company>\n";
	
			v_dump($arRest["IBLOCK_ID"]);
			v_dump($arRest["ID"]);
	
		}
	
		// check last element id
		if($lastElID == $arRest["ID"]) {
			$str .= "</companies>";
			$lastEl = true;
		}
	
		// execution time
		$time_end = microtime_float();
		$time = $time_end - $time_start;
                file_put_contents("yandex_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);
		/*if($time > 10) {
			file_put_contents("yandex_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);
	
			echo "<script>setTimeout(function() {location.href = '/yandex_upload_msk.php?curStepID=".$curStepID."&lastElID=".$lastElID."&curIblockID=".$curIblockID."'}, 5000);</script>";
			break;
		}
	
		if($lastEl) {
			v_dump("Last EL");
			file_put_contents("yandex_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);
		}*/
	}
}
?>
