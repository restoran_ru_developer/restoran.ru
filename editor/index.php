<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Title</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
	<link href="css/general.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="js/uploaderObject.js"></script>
	<script type="text/javascript" src="js/interface.js"></script>
	
	<script src="js/redactor/redactor.js"></script>
	<link rel="stylesheet" href="js/redactor/css/redactor.css" />

	<script type="text/javascript">
	var redactor_object;
	$(function() {
		redactor_object = $('#redactor').redactor({ css: ['blank.css'], toolbar: 'my' });
		$('#inp-caption-del a').click(function() {
			$('.block-editor').remove();
		});		
	});
	</script> 
	
</head>
<body>
<div id="root">
	<div id="main">
	
		<div class="block-editor">
			<form action="#" method="post" id="form-editor">
				<h2>Текстовое поле</h2>
				<div id="inp-caption">				
					<input type="text" class="text" name="caption" />
					<p id="inp-caption-del"><a href="#">Удалить</a></p>
				</div>
				<textarea name="redactor" id="redactor" style="height: 300px; width: 100%;">
					123
				</textarea>
				<p class="editor-submit"><input type="submit" value="Сохранить" /></p>
			</form>				
		</div>

		<div class="block-uploader">
			
			<div id="workfield">
				<div id="img-container">				
					<h2>Фотографии</h2>
					<div id="content">
						<div class="upl-description">Перетащите фотографии в это поле <br />
							или<br />
							<p>
								<input type="file" name="file" value="" id="file-field" multiple="true" /><br/>
								<em>Выберите файлы</em>
							</p>
						</div>
					</div>
				</div>			
				
				<div class="upl-imgs">
					<h2>Фотографии</h2>
					<ul id="img-list"></ul>
					<div id="leftpanel">
						<div id="actions">
						<span id="info-count">Фотографии не выбраны.</span><br/>
							Общий размер: <span id="info-size">0</span> Кб<br/><br/>
						<button id="upload-all" class="notactive">+ Загрузить</button>
					</div>
				<div id="console">
				</div>
			</div>
			
								
				</div>					
			</div>

			
		</div>

	</div>
</div>

</body>
</html>