<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function parse_excel(){
	global $USER;
	$EXIT = array();
	//var_dump($_FILES["xls_file"]);
	//substr_count($_FILES["xls_file"]["name"], ".xls") //$_FILES["xls_file"]["type"]!="application/vnd.ms-excel"
	if(substr_count($_FILES["xls_file"]["name"], ".xls")==0){
		$EXIT["message"] = '<span class="alert_icon"></span>Неверный формат файла!';	
	}else{
		
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("catalog");
		
		require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel.php';
		require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel/IOFactory.php';
		//require_once 'Classes/PHPExcel/IOFactory.php';
  		
    	$objPHPExcel = PHPExcel_IOFactory::load($_FILES["xls_file"]["tmp_name"]);
		
	
		$objWorksheet = $objPHPExcel->getActiveSheet(0);

		$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
		$highestColumn = "O"; // e.g 'F'
		$EXIT["message"]='<span class="alert_icon"></span>Заказы из файла были изменены';
		$CO=0;
		for ($row =0; $row <= $highestRow; ++$row) {
	
			$ID=trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());	
			$REST=trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
			$DENGI=trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue());
			
			if(intval($ID)>0 && (strtoupper($DENGI)=="ДА" || $DENGI=="Да" || $DENGI=="да")){
				//ставим галочку "деньги получены"
				CIBlockElement::SetPropertyValueCode($ID, "dengi", 1553);
				//$EXIT["message"].=$ID;
				//if($row!=$highestRow) $EXIT["message"].=", ";
				
			}
			
			$CO++;
			
		}
		
		$EXIT["message"].=".";
		
		if($CO==0) $EXIT["message"]='<span class="alert_icon"></span>В файле нет заказов';
		
	}
	

   echo json_encode($EXIT);
    
    
}

if($_REQUEST["act"]=="parse_excel") parse_excel();

/*


define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);

function nav_round($v, $prec = 1) {
    // Seems to fix a bug with the ceil function
    $v = explode('.',$v);
    $v = implode('.',$v);
    // The actual calculation
    $v = $v * pow(10,$prec) - 0.5;
    $a = ceil($v) * pow(10,-$prec);
    return number_format( $a, 1, '.', '' );
}

function parse_file($xls_file){
	global $USER;
	require_once 'Classes/PHPExcel/IOFactory.php';

	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");

	echo date('H:i:s') . " Load from Excel5 file<br/>";
	$objPHPExcel = PHPExcel_IOFactory::load("..".$xls_file);

	$objWorksheet = $objPHPExcel->getActiveSheet(0);

	$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
	$highestColumn = "J"; // e.g 'F'


	$now_marka="";
	$now_model="";
	
	$l=-1;
	for ($row =1; $row <= $highestRow; ++$row) {
	
		$marka=trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());	
		$model=trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
	
		if($marka=="" || $model=="" || $marka=="Бренд") continue;
		
		$l++;
		//Грузим Шины
		if($_REQUEST["razdel"]==14){
		
			
			/*
			$zima=trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
			$leto=trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
			
			$sezon=trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());//всесезонная, летняя, зимняя
			
			$shirina=trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
			$profile=trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
			$R=trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
			
			$ship=trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());

			$price=trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
			
			$profile=str_replace(",",".",$profile);
	
			var_dump($profile);
			echo "<br/>";
			
			//$is=$objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
			//$menee=trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());	
			//if($menee!='') $ship="Y";
		
		
		
		
			//Получаем id значений свойств
			
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"shirina","VALUE"=>$shirina));
			if($enum_fields = $property_enums->GetNext()){
  				$shirina=$enum_fields["ID"];
			}else{
				echo 'добавлена ширина '.$shirina."<br/>";
				$shirina = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>67, 'VALUE'=>$shirina));
			}
				
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"height","VALUE"=>$profile));
			if($enum_fields = $property_enums->GetNext()){
  				$profile=$enum_fields["ID"];
			}else{
				echo 'добавлен профиль '.$profile."<br/>";
				$profile = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>68, 'VALUE'=>$profile));
			}
				
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"R","VALUE"=>$R));
			if($enum_fields = $property_enums->GetNext()){
  				$R=$enum_fields["ID"];
			}else{
				echo 'добавлен диаметр '.$R."<br/>";
				$R = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>69, 'VALUE'=>$R));
			}
			
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"is","VALUE"=>$is));
			if($enum_fields = $property_enums->GetNext()){
  				$is=$enum_fields["ID"];
			}else{
				$is = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>90, 'VALUE'=>$is));
			}
			
			
			$PROP = array();
			$PROP[67] = $shirina;  
			$PROP[68] = $profile;
			$PROP[69] = $R;
			$PROP[90] = $is;      
			
			     
				
			if($sezon=="зимняя") $PROP[82]=143;
  			if($sezon=="летняя") $PROP[81]=142;
  			if($ship=="шип") $PROP[71]=68;
  			
  			//менее комплекта
  			if($menee=="Y") $PROP[92]=472;
			
		}

		
		//Грузим Диски
		if($_REQUEST["razdel"]==15){
	
			$R=$objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
			$shirina=$objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
			$otv=$objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
		
			$pcd=$objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
			$vilet=$objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
			
			$cb=$objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
			$color=$objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
			
			$cb=str_replace(",",".",$cb);
			$cb=nav_round($cb,1);
			//$cb = round_half_down($cb,1) . "<br/>";

			//echo $color."<br/>";
			
			$pcd=str_replace(",",".",$pcd);
			$shirina=str_replace(",",".",$shirina);
	
			$krepl=$otv."x".$pcd;
	
	
			$price=$objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
			
	
			//Получаем id значений свойств
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"shirina","VALUE"=>$shirina));
			if($enum_fields = $property_enums->GetNext()){
  				$shirina=$enum_fields["ID"];
			}else{
				$shirina = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>72, 'VALUE'=>$shirina));
			}
				
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"krepl","VALUE"=>$krepl));
			if($enum_fields = $property_enums->GetNext()){
  				$krepl=$enum_fields["ID"];
			}else{
				$krepl = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>74, 'VALUE'=>$krepl));
			}
			
				
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"R","VALUE"=>$R));
			if($enum_fields = $property_enums->GetNext()){
 	 			$R=$enum_fields["ID"];
			}else{
				$R = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>73, 'VALUE'=>$R));
			}
	
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"vilet","VALUE"=>$vilet));
			if($enum_fields = $property_enums->GetNext()){
  				$vilet=$enum_fields["ID"];
			}else{
				$vilet = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>86, 'VALUE'=>$vilet));
			}
			
			
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"cb","VALUE"=>$cb));
			if($enum_fields = $property_enums->GetNext()){
  				$cb=$enum_fields["ID"];
			}else{
				$cb = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>95, 'VALUE'=>$cb));
			}
			
			$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "CODE"=>"color","VALUE"=>$color));
			if($enum_fields = $property_enums->GetNext()){
  				$color=$enum_fields["ID"];
			}else{
				$color = CIBlockPropertyEnum::Add(Array('PROPERTY_ID'=>94, 'VALUE'=>$color));
			}

		
		
			$PROP = array();
			$PROP[72] = $shirina;  
			$PROP[74] = $krepl;
			$PROP[86] = $vilet;
			$PROP[73] = $R;	
			
			$PROP[94] = $color;	
			$PROP[95] = $cb;	
			
			
			
			
		}


		
		
		
		
		//Изменилась марка
		if($now_marka!=$marka){
			//Проверяем, есть ли такой производитель уже
			$ar_result=CIBlockSection::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "NAME"=>$marka),false);
			if($res=$ar_result->GetNext()){
				$marka_id=$res["ID"];
			}else{
				//Добавляем марку в инфоблок
				$bs = new CIBlockSection;
			
				$arFields = Array(
  					"ACTIVE" => "Y",
  					"IBLOCK_ID" => $_REQUEST["razdel"],
  					"NAME" => $marka	
  				);
				$marka_id = $bs->Add($arFields);
			}
			
			$mar=explode(" ",$model);
			$mar_s="";
			for($i=0;$i<count($mar);$i++){
				if($mar[$i]!=' '){
					$mar_s.=$mar[$i];
					if(isset($mar[$i+1]) && $mar[$i+1]!="" && $mar[$i+1]!=" ") $mar_s.="%";
				}
			}
		
			//Проверяем, есть ли такая модель
			$ar_result=CIBlockSection::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "NAME"=>$mar_s,"IBLOCK_SECTION_ID"=>$marka_id),false);
			if($res=$ar_result->GetNext()){
				$model_id=$res["ID"];
				print "модель $model($model_id $mar_s) есть<br/>";
			}else{
				//Добавляем модель в инфоблок
				$bs = new CIBlockSection;
			
				$arFields = Array(
  					"ACTIVE" => "Y",
  					"IBLOCK_ID" => $_REQUEST["razdel"],
  					"IBLOCK_SECTION_ID"=>$marka_id,
  					"NAME" => $model	
  				);
  				
  				if($_REQUEST["razdel"]==14){
  					if($zima=="Y" && $leto=="Y") $arFields["UF_SEASON"]=4;
  					else{
  						if($zima=="Y") $arFields["UF_SEASON"]=3;
  						if($leto=="Y") $arFields["UF_SEASON"]=2;
  					} 
  				}
  				
				$model_id = $bs->Add($arFields);
				
				print "модель $model добавлена<br/>";
			}
		
			//Смотрим, есть ли такие размеры
			$arSelect = Array("ID");
			if($_REQUEST["razdel"]==14) $arFilter = Array("IBLOCK_ID"=>$_REQUEST["razdel"], "SECTION_ID"=>$model_id, "PROPERTY_R"=>$R, "PROPERTY_height"=>$profile,"PROPERTY_shirina"=>$shirina);
			if($_REQUEST["razdel"]==15) $arFilter = Array("IBLOCK_ID"=>$_REQUEST["razdel"], "SECTION_ID"=>$model_id, "PROPERTY_R"=>$R, "PROPERTY_krepl"=>$krepl,"PROPERTY_shirina"=>$shirina,"PROPERTY_vilet"=>$vilet,"PROPERTY_color"=>$color,"PROPERTY_cb"=>$cb);
			$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
			if($ob = $res->GetNext()){
  				print "есть ";
  			
  				$arFields = Array(
    				"PRODUCT_ID" => $ob["ID"],
    				"CATALOG_GROUP_ID" => 1,
    				"PRICE" => $price,
    				"CURRENCY" => "RUB",

				);
				
				if($_REQUEST["razdel"]==14 && $ship=="Y") CIBlockElement::SetPropertyValueCode($ob["ID"], "ship", 68);
				if($_REQUEST["razdel"]==14 && $is!="") CIBlockElement::SetPropertyValueCode($ob["ID"], "is", $is);
				
				if($_REQUEST["razdel"]==14 && $menee!="") CIBlockElement::SetPropertyValueCode($ob["ID"], "menee", $menee);
				
  				$price_res=CPrice::GetBasePrice($ob["ID"]);
  				if($price_res!=null){
  					CPrice::Update($price_res["ID"], $arFields);  						
  				}else CPrice::Add($arFields);
  				
			}else{
				print "нет ";
											
				$el = new CIBlockElement;
			
				$arLoadProductArray = Array(
  					"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  					"IBLOCK_SECTION_ID" => $model_id,      
  					"IBLOCK_ID"      => $_REQUEST["razdel"],
  					"PROPERTY_VALUES"=> $PROP,
  					"NAME"           => $model,
  					"ACTIVE"         => "Y",            // активен
  				);

				$PRODUCT_ID = $el->Add($arLoadProductArray);
				CCatalogProduct::Add(array("ID"=>$PRODUCT_ID));
			}
		
			$now_marka=$marka;
			$now_model=$model;
		}else{
			//Марка осталась прежней
		
			if($now_model!=$model){
				//Изменилась модель
				
				$mar=explode(" ",trim($model));
				$mar_s="";
				for($i=0;$i<count($mar);$i++){
					if($mar[$i]!=' '){
						$mar_s.=$mar[$i];
						if(isset($mar[$i+1]) && $mar[$i+1]!="" && $mar[$i+1]!=" ") $mar_s.="%";
					}
				}
				
				//Проверяем, есть ли такая модель
				$ar_result=CIBlockSection::GetList(Array("ID"=>"DESC"), Array("IBLOCK_ID"=>$_REQUEST["razdel"], "NAME"=>$mar_s,"IBLOCK_SECTION_ID"=>$marka_id),false);
				if($res=$ar_result->GetNext()){
					$model_id=$res["ID"];
					
					print "модель $model есть<br/>";
				}else{
					//Добавляем модель в инфоблок
					$bs = new CIBlockSection;
			
					$arFields = Array(
  						"ACTIVE" => "Y",
  						"IBLOCK_ID" => $_REQUEST["razdel"],
  						"IBLOCK_SECTION_ID"=>$marka_id,
  						"NAME" => $model	
  					);
  				
  					if($_REQUEST["razdel"]==14){
  						if($sezon=="всесезонная") $arFields["UF_SEASON"]=4;
  						else{
  							if($sezon=="зимняя") $arFields["UF_SEASON"]=3;
  							if($sezon=="летняя") $arFields["UF_SEASON"]=2;
  						} 
  					}
  				
					$model_id = $bs->Add($arFields);
					print "модель $model добавлена<br/>";
					//var_dump($ar_result);
				}
			
				//Смотрим, есть ли такие размеры
				$arSelect = Array("ID");
				if($_REQUEST["razdel"]==14) $arFilter = Array("IBLOCK_ID"=>$_REQUEST["razdel"], "SECTION_ID"=>$model_id, "PROPERTY_R"=>$R, "PROPERTY_height"=>$profile,"PROPERTY_shirina"=>$shirina);
				if($_REQUEST["razdel"]==15) $arFilter = Array("IBLOCK_ID"=>$_REQUEST["razdel"], "SECTION_ID"=>$model_id, "PROPERTY_R"=>$R, "PROPERTY_krepl"=>$krepl,"PROPERTY_shirina"=>$shirina,"PROPERTY_vilet"=>$vilet,"PROPERTY_color"=>$color,"PROPERTY_cb"=>$cb);
				$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
				if($ob = $res->GetNext()){
  					//print "есть ";
  				
  					$arFields = Array(
    					"PRODUCT_ID" => $ob["ID"],
    					"CATALOG_GROUP_ID" => 1,
    					"PRICE" => $price,
    					"CURRENCY" => "RUB",
					);
					
					if($_REQUEST["razdel"]==14 && $ship=="Y") CIBlockElement::SetPropertyValueCode($ob["ID"], "ship", 68);
					if($_REQUEST["razdel"]==14 && $is!="") CIBlockElement::SetPropertyValueCode($ob["ID"], "is", $is);
					
					
					if($_REQUEST["razdel"]==14 && $menee!="") CIBlockElement::SetPropertyValueCode($ob["ID"], "menee", $menee);
					
  					$price_res=CPrice::GetBasePrice($ob["ID"]);
  					if($price_res!=null){
  						CPrice::Update($price_res["ID"], $arFields);  						
  					}else CPrice::Add($arFields);
  					
				}else{
					print "нет ";
										
					$el = new CIBlockElement;

					$arLoadProductArray = Array(
  						"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  						"IBLOCK_SECTION_ID" => $model_id,      
  						"IBLOCK_ID"      => $_REQUEST["razdel"],
  						"PROPERTY_VALUES"=> $PROP,
  						"NAME"           => $model,
  						"ACTIVE"         => "Y",            // активен
  					);

					$PRODUCT_ID = $el->Add($arLoadProductArray);
					CCatalogProduct::Add(array("ID"=>$PRODUCT_ID));
				}
						
				$now_model=$model;		
			}else{
				//Модель осталась прежней
			
				//Смотрим, есть ли такие размеры
				$arSelect = Array("ID");
				if($_REQUEST["razdel"]==14) $arFilter = Array("IBLOCK_ID"=>$_REQUEST["razdel"], "SECTION_ID"=>$model_id, "PROPERTY_R"=>$R, "PROPERTY_height"=>$profile,"PROPERTY_shirina"=>$shirina);
				if($_REQUEST["razdel"]==15) $arFilter = Array("IBLOCK_ID"=>$_REQUEST["razdel"], "SECTION_ID"=>$model_id, "PROPERTY_R"=>$R, "PROPERTY_krepl"=>$krepl,"PROPERTY_shirina"=>$shirina,"PROPERTY_vilet"=>$vilet,"PROPERTY_color"=>$color,"PROPERTY_cb"=>$cb);
				$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
				if($ob = $res->GetNext()){
  					print "есть ";
  				
  					$arFields = Array(
    					"PRODUCT_ID" => $ob["ID"],
    					"CATALOG_GROUP_ID" => 1,
    					"PRICE" => $price,
    					"CURRENCY" => "RUB",

					);
					
					if($_REQUEST["razdel"]==14 && $ship=="Y") CIBlockElement::SetPropertyValueCode($ob["ID"], "ship", 68);
					if($_REQUEST["razdel"]==14 && $is!="") CIBlockElement::SetPropertyValueCode($ob["ID"], "is", $is);
					
					if($_REQUEST["razdel"]==14 && $menee!="") CIBlockElement::SetPropertyValueCode($ob["ID"], "menee", $menee);
					
  					$price_res=CPrice::GetBasePrice($ob["ID"]);
  					if($price_res!=null){
  						CPrice::Update($price_res["ID"], $arFields);  						
  					}else CPrice::Add($arFields);
  					
				}else{
					print "нет ";
						
					$el = new CIBlockElement;

					$arLoadProductArray = Array(
  						"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  						"IBLOCK_SECTION_ID" => $model_id,      
  						"IBLOCK_ID"      => $_REQUEST["razdel"],
  						"PROPERTY_VALUES"=> $PROP,
  						"NAME"           => $model,
  						"ACTIVE"         => "Y",            // активен
  					);

					$PRODUCT_ID = $el->Add($arLoadProductArray);
					CCatalogProduct::Add(array("ID"=>$PRODUCT_ID));
				}
		
			}
		}
	
		print " $l $marka $model($model_id) $zima $leto $ship $R $shirina $profile $price $is<br/>";
	}

}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ФУНКЦИЯ ЗАГРУЖАЕТ ФАЙЛ НА СЕРВЕР
function upload_file(){
	global $_FILES;
	global $_POST;
	
	//var_dump($_FILES);
	
	if ($_FILES){	
		$ar_XLS=Array(
	    "name" => $_FILES["xls_f"]["name"][0],
	    "size" => @filesize($_FILES["xls_f"]["tmp_name"][0]),
	    "tmp_name" => $_FILES["xls_f"]["tmp_name"][0],
	    "type" => "xls",
    	"old_file" => "",
    	"del" => "N",
	    "MODULE_ID" => "iblock");
		
		if($fid = CFile::SaveFile($ar_XLS, "iblock")){
			if(!isset($_REQUEST["razdel"])) print "Не указан раздел!";
			else{
				$file_src=CFile::GetPath($fid);
				parse_file($file_src);
			}
			
			CFile::Delete($fid);		
		} else {
		 	echo "errorload";
		}

	} 
	else echo "errorall";
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


if($_REQUEST["act"]=="upload_file") upload_file();



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");



*/


?>









