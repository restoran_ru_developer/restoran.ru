<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function create_report(){
	if(count($_SESSION["ALL_ORDERS_FLTR"])==0) die("Нет параметров для фильтрации!");
    
	global $USER;
	require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel.php';
  	
  	    
    $objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"]."/bs/oparator_report.xls");
 
	
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
  
    //Делаем у ячеек рамки
	$styleArray = array(
	'borders' => array(
		'inside' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb'=>'FF000000')
		),
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		)
	),
	);
	

	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$ONAME="(";
	$i=3;
	$CO=0;
	$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "SECTION_CODE"=>$_SESSION["CITY"]);
	
	
	$arFilter = array_merge($arFilter, $_SESSION["ALL_ORDERS_FLTR"]);	

	
	//СТАТУС
	if($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]>0){
		$db_enum_list = CIBlockProperty::GetPropertyEnum("status", Array(), Array("IBLOCK_ID"=>105, "ID"=>$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]));
		if($ar_enum_list = $db_enum_list->GetNext()){
  			$ONAME.=$ar_enum_list["VALUE"].",";
		}
	} 
	
	//РЕСТОРАН
	if($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]>0){
		$res_rest = CIBlockElement::GetByID($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]);
		if($ob_rest = $res_rest->GetNextElement()){
			$arFields_rest = $ob_rest->GetFields();  
 		}
 		
 		$ONAME.=$arFields_rest["NAME"].",";	
	} 
	
	//ОПЕРАТОР
	if($_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]>0){
		
		$rsUser = CUser::GetByID($_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]);
		$OPERATOR = $rsUser->Fetch();
		
		$ONAME.=$OPERATOR["LAST_NAME"]." ".$OPERATOR["NAME"].",";
	} 
	
	
	if($_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]!="" && $_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]){		
		$ONAME.=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]."-".date( 'd.m.Y' ,strtotime('-1 day', strtotime($_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]))).",";
	} 
	
	
	
	
	if($_REQUEST["date_s2"]!=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"] && $_REQUEST["date_s2"]!="") $_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]=$_REQUEST["date_s2"];
	if($_REQUEST["date_po2"]!=$_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"] && $_REQUEST["date_po2"]!="") $_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]=$_REQUEST["date_po2"];

	$res = CIBlockElement::GetList(Array("PROPERTY_rest.NAME"=>"ASC","date_active_from"=>"ASC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){ 
 		$arFields = $ob->GetFields();  
 		$arFields["PROPERTIES"] = $ob->GetProperties();
 		
 		$ORDERS[]=$arFields;
 		$CO++;
 	}
 	
 	$ONAME.=")";
 	$ONAME = str_replace(",)", ")", $ONAME);
 	
 	
 	
 	if($ONAME=="()") $ONAME="";
 
 	//var_dump($arFilter);
 	
 	
 
 	
 	if($CO==0){
 		$EXIT["message"]="not_orders";
 	}else{
 		$ALL_SUMM=0;
 		foreach($ORDERS as $O){
 	
 			//Ресторан
 			$res_rest = CIBlockElement::GetByID($O["PROPERTIES"]["rest"]["VALUE"]);
			if($ob_rest = $res_rest->GetNextElement()){
				$arFields_rest = $ob_rest->GetFields();  
 			}else{
 				$arFields_rest["NAME"]="подбор";
 			}
 			
 			//Клиент
 			$res_cl = CIBlockElement::GetByID($O["PROPERTIES"]["client"]["VALUE"]);
			if($ob_cl = $res_cl->GetNextElement()){
				$arFields_cl = $ob_cl->GetFields();
				$arProps_cl = $ob_cl->GetProperties();  
 			}
 			
 			$tar=explode(" ", $O["ACTIVE_FROM"]);
 			$tar2=explode(":", $tar[1]);
	 		$DATE = $tar[0];
 			$TIME = $tar2[0].":".$tar2[1];
 			
 			if($TIME==":") $TIME="";
 			
 			$tar=explode(" ", $O["DATE_CREATE"]);
 			$O["DATE_CREATE"] = $tar[0];
 			
 			$rsUser = CUser::GetByID($O["CREATED_BY"]);
			$arUser = $rsUser->Fetch();
 		
 			$O["CREATED_BY"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];
 			
 			if($O["PROPERTIES"]["dengi"]["VALUE"]=="Y") $dengi="Да";
 			else $dengi="Нет";
 		
 			$objPHPExcel->setActiveSheetIndex(0)
   			->setCellValue('A'.$i, $arFields_rest["NAME"])
   			->setCellValue('B'.$i, $arFields_cl["NAME"])
   			->setCellValue('C'.$i, $DATE)
   			->setCellValue('D'.$i, $TIME)
   			->setCellValue('E'.$i, $O["PROPERTIES"]["guest"]["VALUE"])
   			->setCellValue('F'.$i, $O["PROPERTIES"]["summ_sch"]["VALUE"])
   			->setCellValue('G'.$i, $O["PROPERTIES"]["prinyal"]["VALUE"])
   			->setCellValue('H'.$i, $O["PROPERTIES"]["podtv"]["VALUE"]);

			
			$ALL_SUMM+=	$O["PROPERTIES"]["summ_sch"]["VALUE"];
			/*
			$COLOR="";
			
			if($O["PROPERTIES"]["status"]["VALUE"]=="заказ принят") $COLOR = "FFfbf205";
			if($O["PROPERTIES"]["status"]["VALUE"]=="забронирован") $COLOR = "FFB5F562";
			if($O["PROPERTIES"]["status"]["VALUE"]=="гости пришли") $COLOR = "FF71C207";
			if($O["PROPERTIES"]["status"]["VALUE"]=="гости отменили заказ") $COLOR = "FFfb2d05";
			if($O["PROPERTIES"]["status"]["VALUE"]=="ошибочный заказ") $COLOR = "FF3b3b3b";
			if($O["PROPERTIES"]["status"]["VALUE"]=="банкет в работе") $COLOR = "FFFBFB61";
			if($O["PROPERTIES"]["status"]["VALUE"]=="внесена предоплата") $COLOR = "FFFBFB61";
			if($O["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен") $COLOR = "FF48C7EF";
	
			
			
			if($COLOR!=""){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getFill()->getStartColor()->setARGB($COLOR);
			}
			*/
			$i++;	
 		}
 		
 		$i--;
 		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->applyFromArray($styleArray);
 		
 		$i++;
 		//ИТОГОВАЯ СУММА
 		$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getFont()->setBold(true);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $ALL_SUMM);
 		
 		
 		
		$FILE_NAME2 = $_SESSION["ALL_ORDERS_FLTR"][">=DATE_ACTIVE_FROM"]." - ".$_SESSION["ALL_ORDERS_FLTR"]["<=DATE_ACTIVE_FROM"]." ".$ONAME;
		if($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]!="") $FILE_NAME2.=" ".$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"];
		
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$FILE_NAME2.'"');
		header('Cache-Control: max-age=0');
		$objWriter->save("php://output");
		
	}
 
    echo json_encode($EXIT);
    
    
}

create_report();
?>