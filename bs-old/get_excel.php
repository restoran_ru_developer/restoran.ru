<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function create_report(){
   // if($_REQUEST["date_s"]=="" && $_REQUEST["date_po"]=="" && $_REQUEST["date_s2"]=="" && $_REQUEST["date_po2"]=="") die("Укажите даты формирования отчета");
	
	global $USER;
	require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel.php';
  	
    $objPHPExcel = new PHPExcel();

  	$EXIT = array();
    $objPHPExcel->getProperties()->setCreator("Ресторан.ру")                         
                         ->setTitle("Отчет")
                         ->setSubject("Отчет");
 
	
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
  
    //Делаем у ячеек рамки
	$styleArray = array(
	'borders' => array(
		'inside' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb'=>'FF000000')
		),
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		)
	),
	);
	
    
    $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setSize(11);
    $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->getStartColor()->setARGB('FFeae5d8');

    $objPHPExcel->setActiveSheetIndex(0)
    	->setCellValue('A1', "ID")
   		->setCellValue('B1', "Статус")
   		->setCellValue('C1', "Ресторан")
	    ->setCellValue('D1', "Дата")
	    ->setCellValue('E1', "Время")
	    ->setCellValue('F1', "Кол-во гостей")
		->setCellValue('G1', "ФИО")
		->setCellValue('H1', "Телефон")
		->setCellValue('I1', "Оператор")
		->setCellValue('J1', "Принял заказ")
		->setCellValue('K1', "Подтвердил заказ")
		->setCellValue('L1', "Сумма счета")
		->setCellValue('M1', "№ стола")
		->setCellValue('N1', "Подтвердил сумму счета/№ стола")
		->setCellValue('O1', "Деньги получены");
    
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);  
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(14);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(35);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
	
	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$ONAME="(";
	$i=2;
	$CO=0;
	

	
	$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y");
	
	$arFilter["SECTION_CODE"] = $_SESSION["CITY"];
	
	
	if($_REQUEST["date_s"]!=""){
		$arFilter[">=DATE_ACTIVE_FROM"]=$_REQUEST["date_s"];
	}
	
	if($_REQUEST["date_po"]!=""){
		$arFilter["<=DATE_ACTIVE_FROM"]=date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po"])));
	}
	
	if($_REQUEST["date_s2"]!=""){
		$arFilter[">=DATE_CREATE"]=$_REQUEST["date_s2"];
	}
	
	if($_REQUEST["date_po2"]!=""){
		$arFilter["<=DATE_CREATE"]=date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po2"])));
	}
	
	
	//ИСТОЧНИК
	if($_REQUEST["source"]>0){
		$arFilter["PROPERTY_source"]=$_REQUEST["source"];
		
		$db_enum_list = CIBlockProperty::GetPropertyEnum("source", Array(), Array("IBLOCK_ID"=>105, "ID"=>$_REQUEST["source"]));
		if($ar_enum_list = $db_enum_list->GetNext()){
  			$ONAME.=$ar_enum_list["VALUE"].",";
		}


	}
	 
	//ТИП
	if($_REQUEST["type"]>0){
		$arFilter["PROPERTY_type"]=$_REQUEST["type"];
			
		$db_enum_list = CIBlockProperty::GetPropertyEnum("type", Array(), Array("IBLOCK_ID"=>105, "ID"=>$_REQUEST["type"]));
		if($ar_enum_list = $db_enum_list->GetNext()){
  			$ONAME.=$ar_enum_list["VALUE"].",";
		}
		
	}
	
	//СТАТУС
	if($_REQUEST["status"]>0){
		$arFilter["PROPERTY_status"]=$_REQUEST["status"];
		
		$db_enum_list = CIBlockProperty::GetPropertyEnum("status", Array(), Array("IBLOCK_ID"=>105, "ID"=>$_REQUEST["status"]));
		if($ar_enum_list = $db_enum_list->GetNext()){
  			$ONAME.=$ar_enum_list["VALUE"].",";
		}
	} 
	
	
	//РЕСТОРАН
	if($_REQUEST["restoran_id"]>0){
		$arFilter["PROPERTY_rest"]=$_REQUEST["restoran_id"];
		
		$res_rest = CIBlockElement::GetByID($_REQUEST["restoran_id"]);
		if($ob_rest = $res_rest->GetNextElement()){
			$arFields_rest = $ob_rest->GetFields();  
 		}
 		
 		$ONAME.=$arFields_rest["NAME"].",";	
	} 

	//ВИД
	if($_REQUEST["vid"]=="dengi") {
		$arFilter["PROPERTY_dengi_VALUE"]="Y";
		$ONAME.="Деньги получены,";
	}
	
	if($_REQUEST["vid"]=="all") {
		//$arFilter["PROPERTY_dengi_VALUE"]="Y";
		$ONAME.="Общий,";
	}
	
	//ОПЕРАТОР
	if($_REQUEST["operator"]>0){
		$arFilter["CREATED_BY"]=$_REQUEST["operator"];
		$rsUser = CUser::GetByID($_REQUEST["operator"]);
		$OPERATOR = $rsUser->Fetch();
		
		$ONAME.=$OPERATOR["LAST_NAME"]." ".$OPERATOR["NAME"].",";
	} 
	
	//var_dump($arFilter);
		
	$res = CIBlockElement::GetList(Array("PROPERTY_rest.NAME"=>"ASC","date_active_from"=>"ASC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){ 
 		$arFields = $ob->GetFields();  
 		$arFields["PROPERTIES"] = $ob->GetProperties();
 		
 		$ORDERS[]=$arFields;
 		$CO++;
 	}
 	
 	$ONAME.=")";
 	$ONAME = str_replace(",)", ")", $ONAME);
 	
 	if($ONAME=="()") $ONAME="";
 
 	
 	$EXIT["CO"]=$CO;
 	
 	if($CO==0){
 		$EXIT["message"]="not_orders";
 	}else{
 		foreach($ORDERS as $O){
 	
 			//Ресторан
 			$res_rest = CIBlockElement::GetByID($O["PROPERTIES"]["rest"]["VALUE"]);
			if($ob_rest = $res_rest->GetNextElement()){
				$arFields_rest = $ob_rest->GetFields();  
 			}else{
 				$arFields_rest["NAME"]="подбор";
 			}
 			
 			//Клиент
 			$res_cl = CIBlockElement::GetByID($O["PROPERTIES"]["client"]["VALUE"]);
			if($ob_cl = $res_cl->GetNextElement()){
				$arFields_cl = $ob_cl->GetFields();
				$arProps_cl = $ob_cl->GetProperties();  
 			}
 			
 			$tar=explode(" ", $O["ACTIVE_FROM"]);
 			$tar2=explode(":", $tar[1]);
	 		$DATE = $tar[0];
 			$TIME = $tar2[0].":".$tar2[1];
 			
 			if($TIME==":") $TIME="";
 		
 			$rsUser = CUser::GetByID($O["CREATED_BY"]);
			$arUser = $rsUser->Fetch();
 		
 			$O["CREATED_BY"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];
 			
 			if($O["PROPERTIES"]["dengi"]["VALUE"]=="Y") $dengi="Да";
 			else $dengi="Нет";
 		
 			$objPHPExcel->setActiveSheetIndex(0)
	   		->setCellValue('A'.$i, $O["ID"])
   			->setCellValue('B'.$i, $O["PROPERTIES"]["status"]["VALUE"])
   			->setCellValue('C'.$i, $arFields_rest["NAME"])
	    	->setCellValue('D'.$i, $DATE)
		    ->setCellValue('E'.$i, $TIME)
		    ->setCellValue('F'.$i, $O["PROPERTIES"]["guest"]["VALUE"])
			->setCellValue('G'.$i, $arFields_cl["NAME"])
			->setCellValue('H'.$i, $arProps_cl["TELEPHONE"]["VALUE"])
			->setCellValue('I'.$i, $O["CREATED_BY"])
			->setCellValue('J'.$i, $O["PROPERTIES"]["prinyal"]["VALUE"])
			->setCellValue('K'.$i, $O["PROPERTIES"]["podtv"]["VALUE"])
			->setCellValue('L'.$i, $O["PROPERTIES"]["summ_sch"]["VALUE"])
			->setCellValue('M'.$i, $O["PROPERTIES"]["stol"]["VALUE"])
			->setCellValue('N'.$i, $O["PROPERTIES"]["podtv2"]["VALUE"])
			->setCellValue('O'.$i, $dengi);
			
			
			
			$COLOR="";
			
			if($O["PROPERTIES"]["status"]["VALUE"]=="заказ принят") $COLOR = "FFfbf205";
			if($O["PROPERTIES"]["status"]["VALUE"]=="забронирован") $COLOR = "FFB5F562";
			if($O["PROPERTIES"]["status"]["VALUE"]=="гости пришли") $COLOR = "FF71C207";
			if($O["PROPERTIES"]["status"]["VALUE"]=="гости отменили заказ") $COLOR = "FFfb2d05";
			if($O["PROPERTIES"]["status"]["VALUE"]=="ошибочный заказ") $COLOR = "FF3b3b3b";
			if($O["PROPERTIES"]["status"]["VALUE"]=="банкет в работе") $COLOR = "FFFBFB61";
			if($O["PROPERTIES"]["status"]["VALUE"]=="внесена предоплата") $COLOR = "FFFBFB61";
			if($O["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен") $COLOR = "FF48C7EF";
	
			
			
			if($COLOR!=""){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getFill()->getStartColor()->setARGB($COLOR);
			}
			$i++;	
 		}
 		
 		$i--;
 		$objPHPExcel->getActiveSheet()->getStyle('A1:O'.$i)->applyFromArray($styleArray);
 		
 		
	
		if($_REQUEST["date_s"]!="" &&  $_REQUEST["date_po"]!=""){
	    	$FILE_NAME = $_SERVER["DOCUMENT_ROOT"].'/bs/reports/'.str_replace('.', '-', $_REQUEST["date_s"])."_".str_replace('.', '-', $_REQUEST["date_po"])."_".time().".xls";
			$FILE_NAME2 = $_REQUEST["date_s"]." - ".$_REQUEST["date_po"]." ".$ONAME;
		}else{
			$FILE_NAME = $_SERVER["DOCUMENT_ROOT"].'/bs/reports/'.str_replace('.', '-', $_REQUEST["date_s2"])."_".str_replace('.', '-', $_REQUEST["date_po2"])."_".time().".xls";
			$FILE_NAME2 = "созданы ".$_REQUEST["date_s2"]." - ".$_REQUEST["date_po2"]." ".$ONAME;
		}
		
		
		if($_REQUEST["rastoran_id"]!="") $FILE_NAME2.=" ".$_REQUEST["rastoran_id"];

   	 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save($FILE_NAME);
    
	    $el = new CIBlockElement;

		$PROP = array();
		$PROP["xls"] = CFile::MakeFileArray($FILE_NAME);
	
		if($_SESSION["CITY"]=="spb") $SECTION=83662;
		else $SECTION=83661;

		$arLoadProductArray = Array(
	  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  			"IBLOCK_SECTION_ID" => $SECTION,          // элемент лежит в корне раздела
  			"IBLOCK_ID"      => 113,
  			"PROPERTY_VALUES"=> $PROP,
	  		"NAME"           => $FILE_NAME2,
  			"ACTIVE"         => "Y",            // активен
  		);

	if($PRODUCT_ID = $el->Add($arLoadProductArray)){
  		$EXIT["message"]="report_created";
  		//unlink($FILE_NAME);
	}else
  		echo "Error: ".$el->LAST_ERROR;


    }
 
    echo json_encode($EXIT);
    
    
}

if($_REQUEST["act"]=="create_report") create_report();
?>