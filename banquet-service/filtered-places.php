<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("THIS_SECTION_DESC_SPB", "Новые краски и любимые вкусы. Встречайте второй масштабный яркий и, как всегда, стильный                    ресторан «Пряности&amp;Радости» на Малой Посадской 3. Еще один ресторан холдинга Ginza Project в                    историческом сердце Петербурга на Петроградской стороне. Укромные уголки для двоих, удобные                    диваны для дружных компаний и просторные столы для бурных торжеств. В фильтре.");

$APPLICATION->SetPageProperty("description", "Заказ банкета: Фильтр бакетной службы");
$APPLICATION->SetPageProperty("keywords", "фильтр,банкетной,службы,заказ,банкет");
$APPLICATION->SetTitle("Фильтр бакетной службы");
?>

<?
$APPLICATION->IncludeComponent("restoran:restoraunts.list",
    "rest_list_new",
    array(
        "IBLOCK_TYPE" => "selection_of_restaurants",
        "IBLOCK_ID" => $_REQUEST["CITY_ID"],
        "PARENT_SECTION_CODE" => "",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY9" => "NAME",
        "SORT_ORDER9" => "ASC",
        "FILTER_NAME" => "",
        "PROPERTY_CODE" => array(
            0 => "address",
            //                    1 => "opening_hours",
            2 => "phone",
            3 => "type",
            4 => "kitchen",
            5 => "average_bill",
            6 => "subway",
            8 => "sale10",
//            9 => "IDEALLY",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86401",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "PREVIEW_PICTURE_MAX_WIDTH" => "231",
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163",
        "PREVIEW_TRUNCATE_LEN" => "150",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PAGER_TEMPLATE" => "rest_list_arrows",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Рестораны",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        'PARENT_NEWS_LIST' => $convert_news
    ),
    false
);
?>
    <div class="clearfix"></div>
    <div class="preview_seo_text">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>