<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("THIS_SECTION_DESC_SPB", "Новые краски и любимые вкусы. Встречайте второй масштабный яркий и, как всегда, стильный                    ресторан «Пряности&amp;Радости» на Малой Посадской 3. Еще один ресторан холдинга Ginza Project в                    историческом сердце Петербурга на Петроградской стороне. Укромные уголки для двоих, удобные                    диваны для дружных компаний и просторные столы для бурных торжеств. В фильтре.");
$APPLICATION->SetTitle("Фильтр бакетной службы");
//print_r($APPLICATION->GetPagePropertyList("THIS_SECTION_DESC_".strtoupper(CITY_ID), 'empty'));
?>

<?
$arIB = getArIblock("selection_of_restaurants", CITY_ID);
$new_filter_arr = array();

$compare_prop_arr = array('area'=>'area', 'subway'=>'subway', 'average_bill'=>'average_bill', 'kolichestvochelovek'=>'kolichestvochelovek');
//        print_r($_GET['banquetArrFilter']);
$inner_key = 0;
foreach($_GET['banquetArrFilter'] as $key=>$one_prop){
    if(count($one_prop)==1){
        $new_filter_arr['PROPERTY_'.$compare_prop_arr[$key]] = $one_prop[0];
    }
    elseif(count($one_prop)>1) {
        $inner_key++;
        $new_filter_arr[$inner_key] = array("LOGIC" => "OR",);
        foreach($one_prop as $this_inner_key=>$one_multiple_prop){
            $new_filter_arr[$inner_key][$this_inner_key]['PROPERTY_'.$compare_prop_arr[$key]] = $one_multiple_prop;
        }
    }
}

//        print_r($new_filter_arr);
$arSelect = Array('ID',"PROPERTY_THIS_RESTAURANT");
$arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y", 'INCLUDE_SUBSECTIONS'=>'Y');
$arFilter = array_merge($arFilter, $new_filter_arr);

//        print_r($arFilter);

$res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, false, $arSelect);    //  ограничить кол-во
while($ob = $res->GetNextElement())
{
    $obj_field = $ob->GetFields();
    $output_news[$obj_field['ID']] = $obj_field;
    $convert_news[$obj_field['PROPERTY_THIS_RESTAURANT_VALUE']] = $obj_field['ID'];
}

//                print_r($output_news);

$GLOBALS['lastSectionFilter'] = array(
    array(
        "LOGIC" => "OR",
    ),
    'PROPERTY_sale10_VALUE'=>1
);

foreach ($output_news as $key=>$one_news) {
    $GLOBALS['lastSectionFilter'][0][] = array("ID"=>$one_news['PROPERTY_THIS_RESTAURANT_VALUE']);
}

//        print_r($GLOBALS['lastSectionFilter']);
?>

<?
if(count($output_news)):
    $APPLICATION->IncludeComponent("restoran:restoraunts.list",
        "rest_list",
        array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $_REQUEST["CITY_ID"],
            "PARENT_SECTION_CODE" => "",
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "NAME",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "lastSectionFilter",
            "PROPERTY_CODE" => array(
                0 => "address",
                //                    1 => "opening_hours",
                2 => "phone",
                3 => "type",
                4 => "kitchen",
                5 => "average_bill",
                6 => "subway",
                7 => "ratio",
                8 => "sale10",
                9 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "86400",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "PREVIEW_PICTURE_MAX_WIDTH" => "231",
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163",
            "PREVIEW_TRUNCATE_LEN" => "150",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PAGER_TEMPLATE" => "rest_list_arrows",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Рестораны",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => "",
            'PARENT_NEWS_LIST' => $convert_news
        ),
        false
    );
else:?>
    <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
<?
endif;
unset($output_news, $GLOBALS['lastSectionFilter']);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>