<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("THIS_SECTION_DESC_SPB", "21 Новые краски и любимые вкусы. Встречайте второй масштабный яркий и, как всегда, стильный                    ресторан «Пряности&amp;Радости» на Малой Посадской 3. Еще один ресторан холдинга Ginza Project в                    историческом сердце Петербурга на Петроградской стороне. Укромные уголки для двоих, удобные                    диваны для дружных компаний и просторные столы для бурных торжеств.");
$APPLICATION->SetTitle("Все подборки банкетной службы");
?> <?
    $arSiteMenuIB = getArIblock("selection_of_restaurants", CITY_ID);
    $APPLICATION->IncludeComponent("bitrix:catalog.section.list",
        "banquet_selections_section_new",
        array(
            "IBLOCK_TYPE" => "selection_of_restaurants",
            "IBLOCK_ID" => $arSiteMenuIB['ID'],
            "SECTION_ID" => "",
            "SECTION_CODE" => $_REQUEST['SECTION_CODE'] ? $_REQUEST['SECTION_CODE'] : "",
            "COUNT_ELEMENTS" => "Y",
            "TOP_DEPTH" => "1",
            "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_USER_FIELDS" => array(
                0 => "UF_SHOW_IN_B_ROOT",
                1 => "UF_ALL_NEWS_TITLE",
                2 => "UF_IDEALLY_FOR_SECT",
                3 => "UF_FOUR_SECTION1",
                4 => "UF_FOUR_SECTION2",
                5 => "UF_FOUR_SECTION3",
                6 => "UF_TOP_SLIDER"
            ),
            "SECTION_URL" => "",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "VIEW_MODE" => "LINE",
            "SHOW_PARENT_NAME" => "Y",
            "MAX_SECTION_NUM" => "ALL"
        ),
        false
    );?>
    <div class="clearfix"></div>
    <div class="preview_seo_text">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>