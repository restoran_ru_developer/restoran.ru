<?
define('CUR_DIR','banquet-service');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("THIS_SECTION_DESC_SPB", "3232 Новые краски и любимые вкусы. Встречайте второй масштабный яркий и, как всегда, стильный                    ресторан «Пряности&amp;Радости» на Малой Посадской 3. Еще один ресторан холдинга Ginza Project в                    историческом сердце Петербурга на Петроградской стороне. Укромные уголки для двоих, удобные                    диваны для дружных компаний и просторные столы для бурных торжеств. 09009");

$APPLICATION->SetPageProperty("description", "Заказ банкета: Банкетная служба");
$APPLICATION->SetPageProperty("keywords", "банкетная,служба,заказ,банкет");
$APPLICATION->SetTitle("Банкетная служба");
?>
    <div class="left-side">

        <?$arRestIB = getArIblock("slider_banquet_service", CITY_ID);?>
        <?
        $GLOBALS['startArrFilter']['PROPERTY_THIS_RESTAURANT.ACTIVE'] = 'Y';
        $APPLICATION->IncludeComponent("bitrix:news.list", "banquet_slider", array(
                "IBLOCK_TYPE" => "slider_banquet_service",
                "IBLOCK_ID" => $arRestIB["ID"],
                "NEWS_COUNT" => "20",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "startArrFilter",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "LINK",
                    2 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>




        <?
        $arSiteMenuIB = getArIblock("selection_of_restaurants", CITY_ID);
        $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "tabs-section-new", array(
                "IBLOCK_TYPE" => "selection_of_restaurants",
                "IBLOCK_ID" => $arSiteMenuIB['ID'],
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "Y",
                "TOP_DEPTH" => "2",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "UF_SHOW_IN_B_ROOT",
                    1 => "UF_ALL_NEWS_TITLE",
                    2 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "A",//a
                "CACHE_TIME" => "36000003",
                "CACHE_GROUPS" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "VIEW_MODE" => "LINE",
                "SHOW_PARENT_NAME" => "Y"
            ),
            false
        );?>



    </div>
    <div class="right-side">

        <?
        $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "banquet_sections", array(
                "IBLOCK_TYPE" => "selection_of_restaurants",
                "IBLOCK_ID" => $arSiteMenuIB['ID'],
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "N",
                "TOP_DEPTH" => "1",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "A",//a
                "CACHE_TIME" => "36000003",
                "CACHE_GROUPS" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "VIEW_MODE" => "LINE",
                "SHOW_PARENT_NAME" => "Y",
                'MAX_SECTION_NUM' => 999
            ),
            false
        );?>
<!--        --><?//$APPLICATION->IncludeComponent(
//            "bitrix:main.include",
//            "",
//            Array(
//                "AREA_FILE_SHOW" => "file",
//                "PATH" => SITE_TEMPLATE_PATH."/include_areas/banquet-right-banner-".CITY_ID.".php",
//                "EDIT_TEMPLATE" => ""
//            ),
//            false
//        );?>

        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/bitrix/templates/main_2014/include_areas/new-order-form-with-city.php",
                "EDIT_TEMPLATE" => ""
            ),
            false
        );?>


    </div>
<div class="clearfix"></div>
    <div class="preview_seo_text">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>