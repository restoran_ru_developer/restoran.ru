<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if ($_REQUEST["CITY_ID"]&&$_REQUEST["id"]):
?>
<script>
    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=get_location_a';
        document.body.appendChild(script);
    }
    $(function(){
        loadScript();
    })
</script>
<?$arReviewsIB = getArIblock("reviews", $_REQUEST['CITY_ID']);?>
<?$APPLICATION->IncludeComponent(
    "restoran:comments_add_simple",
    "add_review",
    Array(
            "IBLOCK_TYPE" => "reviews",
            "IBLOCK_ID" => $arReviewsIB["ID"],
            "ELEMENT_ID" => $_REQUEST["id"],
            "IS_SECTION" => "N",      
            "CACHE_TYPE" => "N",
            "NEED_CAPTCHA" => "N"
    ),false
);?>
<?
else:
    echo "Произошла ошибка, попробуйте позже";
endif;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>