<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Выбор города");?>

    <script type="text/javascript">
        var sessionLat = '<?= $_SESSION['lat'] ?>';
        var sessionLon = '<?= $_SESSION['lon'] ?>';
        var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
        coord = coord.split(",");

        function initialize() {
            google.maps.event.addDomListener(window, 'load', initialize_map_how_get(sessionLat,sessionLon));
        }
        function loadScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
            '&signed_in=true&callback=initialize';
            document.body.appendChild(script);
        }
        <?if($_REQUEST['ajax']=='Y'):?>
        initialize();
        <?elseif($_REQUEST['ajax']!='Y'):?>
        loadScript();
        <?endif?>


    </script>
    <div class="popup map-canvas-wrapper">
        <div class="close-map-canvas"></div>
        <div id="map-canvas" ></div>
    </div>

    <?$APPLICATION->IncludeComponent(
                        "restoran:city.selector",
                        "city_select_2014",
                        Array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_URL" => "",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "360000000005",
                            "CACHE_NOTES" => "new3212236624228",
                            "CACHE_GROUPS" => "N"
                        ),
                        false
                    );
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>