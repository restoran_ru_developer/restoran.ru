<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<?
if (/*$USER->IsAuthorized()*/1) {
    $oldDate = explode('-', $_REQUEST['form_date_33']);
    if (count($oldDate) == 3) {
        $newDate = $oldDate[2] . '.' . $oldDate[1] . '.' . $oldDate[0];
        $_REQUEST['form_date_33'] = $newDate;
    }

    if (CITY_ID == "ast"):
        ShowError("Услуга временно не работает");
        die;
    endif;

    global $USER;
    global $arrFilterSpecStr;
    $arrFilterSpecStr = Array(
        "PROPERTY_s_place_" . $_REQUEST["CATALOG_ID"] . "_VALUE" => Array("Да")
    );
    $APPLICATION->IncludeComponent(
            "restoran:form.result.new", "mobile_form_bron_rest", Array(
        "SEF_MODE" => "N",
        "WEB_FORM_ID" => "3",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "CACHE_TYPE" => "N",
        //"CACHE_TIME" => "3600",
        "USE_CAPTCHA" => "Y", // Использовать CAPTCHA
        "VARIABLE_ALIASES" => Array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID"
        )
            ), false
    );
} else {
    ?>
    Только авторизованные пользователи могут бронировать столики. Для авторизации или регистрации, пожалуйста, проследуйте в 
    <a class="another ui-link" href="/mobile/auth/" data-ajax="false">Личный кабинет</a>
<? } ?> 


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>	