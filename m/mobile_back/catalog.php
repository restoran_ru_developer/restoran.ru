<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<?
$arIndexIB = getArIblock("index_page", CITY_ID);
global $arTopBlock;
$arTopBlock["CODE"] = "mobile_catalog";    
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "index_block_top4",
    Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "index_page",
        "IBLOCK_ID" => $arIndexIB["ID"],
        "NEWS_COUNT" => "1",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "arTopBlock",
        "FIELD_CODE" => "",
        "PROPERTY_CODE" => array(
            0  => "ELEMENTS",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "120",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "360000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
    false
    );
?>
<script>
    function addInfoWindow(marker, message, map, link) {
        var infoString = '<div id="partnerNotice">' +
            '<p style="margin:0px;">' + message + '</p>' +
            '<a href="'+ link +'">Перейти к ресторану</a>' +
            '</div>';

        var infoWindow = new google.maps.InfoWindow({
            content: infoString
        });

        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(map, marker);
        });
    }
    
    function success_a(position) {        
          
        $.ajax({
            type: "POST",
            url: "/ajax-coordinates.php",
            data: {
                lat:position.coords.latitude,
                lon:position.coords.longitude
            },
            success: function(data) {
            
            }
        });
        setTimeout("location.reload();",300);
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if(navigatorOn !== "Y"){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }
        }                       
    }
           
</script>
<script type="text/javascript">

    function initialize() {          
        get_location_a(); 
    }

    function loadScript() {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBIPD9UKtX4nflE-8ktUp62E8DVReinvUo&sensor=true&callback=initialize";
        document.body.appendChild(script);
    }
    navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';
    window.onload = loadScript;
    
    
</script>
<!--
<?
// set rest sort links
//$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1", "by", "index_php?page", "index_php");
//$sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "page=1&") . "pageRestSort=new&" . (($_REQUEST["pageRestSort"] == "new" && $_REQUEST["by"] == "desc") ? "by=asc" : "by=desc"), $excUrlParams);
//$sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "page=1&") . "pageRestSort=price&" . (($_REQUEST["pageRestSort"] == "price" && $_REQUEST["by"] == "asc") ? "by=desc" : "by=asc"), $excUrlParams);
//$sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "page=1&") . "pageRestSort=ratio&" . (($_REQUEST["pageRestSort"] == "ratio" && $_REQUEST["by"] == "desc") ? "by=asc" : "by=desc"), $excUrlParams);
//$sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "page=1&") . "pageRestSort=alphabet&" . ((($_REQUEST["pageRestSort"] == "alphabet" && $_REQUEST["by"] == "asc") || !$_REQUEST["pageRestSort"]) ? "by=desc" : "by=asc"), $excUrlParams);
?>

<div class="fieldset-container">
    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" >
<? //if($_REQUEST["page"] || $_REQUEST["PAGEN_1"]):?>
<? $by = ($_REQUEST["by"] == "desc") ? "z" : "a"; ?>
<?
if ($_REQUEST["pageRestSort"] == "new") {
    echo '<a data-role="button" data-mini="true" class="ui-btn-active" data-theme="e" data-ajax="false" href="' . $sortNewPage . '">по новизне</a>';
} else {
    echo '<a data-role="button" data-mini="true" data-theme="d" data-ajax="false" href="' . $sortNewPage . '">по новизне</a>';
}
if ($_REQUEST["pageRestSort"] == "ratio") {
    echo '<a data-role="button" data-mini="true" class="ui-btn-active" data-theme="e" data-ajax="false" href="' . $sortRatioPage . '">по рейтингу</a>';
} else {
    echo '<a data-role="button" data-mini="true" data-theme="d" data-ajax="false" href="' . $sortRatioPage . '">по рейтингу</a>';
}
?>   
    </fieldset>
</div>
<div class="clear"></div>-->

<?
if (!$_REQUEST["page"])
    $_REQUEST["page"] = 1;
if (!$_REQUEST["PAGEN_1"])
    $_REQUEST["PAGEN_1"] = 1;
global $arrFilter;
$arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
$APPLICATION->IncludeComponent("restoran:restoraunts.list", "rest_list", Array(
    "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
    "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID), // Код информационного блока
    "PARENT_SECTION_CODE" => "restaurants", // Код раздела
    "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"), // Количество ресторанов на странице
    "SORT_BY1" => ($_REQUEST["pageRestSort"] ? $_REQUEST["pageRestSort"] : "NAME"), // Поле для первой сортировки ресторанов
    "SORT_ORDER1" => "ASC", // Направление для первой сортировки ресторанов
    "SORT_BY2" => $sort2, // Поле для второй сортировки ресторанов
    "SORT_ORDER2" => $sortOreder2, // Направление для второй сортировки ресторанов
    "FILTER_NAME" => "arrFilter", // Фильтр
    "PROPERTY_CODE" => array(// Свойства
        0 => "type",
        1 => "kitchen",
        2 => "average_bill",
        3 => "opening_hours",
        4 => "phone",
        5 => "address",
        6 => "subway",
        7 => "ratio",
    ),
    "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
    "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
    "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
    "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
    "AJAX_MODE" => "N", // Включить режим AJAX
    "AJAX_OPTION_SHADOW" => "Y",
    "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
    "CACHE_TYPE" => ($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
    "CACHE_TIME" => "21600", // Время кеширования (сек.)
    "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
    "CACHE_GROUPS" => "N", // Учитывать права доступа
    "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
    "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
    "SET_TITLE" => "Y", // Устанавливать заголовок страницы
    "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
    "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
    "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
    "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
    "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
    "PAGER_TITLE" => "Рестораны", // Название категорий
    "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
    "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
    "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
    "DISPLAY_DATE" => "Y", // Выводить дату элемента
    "DISPLAY_NAME" => "Y", // Выводить название элемента
    "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
    "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
        ), false
);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>			