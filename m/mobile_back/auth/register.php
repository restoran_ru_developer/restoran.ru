<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<div id="content">
    <?
    $APPLICATION->IncludeComponent("bitrix:main.register", "register", Array(
        "USER_PROPERTY_NAME" => "", // Название блока пользовательских свойств
        "SHOW_FIELDS" => array(// Поля, которые показывать в форме
            0 => "LAST_NAME",
            1 => "NAME",
            2 => "EMAIL",
            3 => "PERSONAL_PHONE",
            4 => "PERSONAL_NOTES",
            5 => "PERSONAL_GENDER",
            6 => "PERSONAL_BIRTHDAY",
            7 => "WORK_COMPANY",
            8 => "WORK_POSITION",
            9 => "WORK_NOTES",
            10 => "PERSONAL_PROFESSION",
            11 => "PERSONAL_CITY",
            12 => "PERSONAL_PAGER"
        ),
        "REQUIRED_FIELDS" => array(// Поля, обязательные для заполнения
            0 => "LAST_NAME",
            1 => "NAME",
            2 => "EMAIL",
            3 => "PERSONAL_PROFESSION",
        ),
        "AUTH" => "N", // Автоматически авторизовать пользователей
        "USE_BACKURL" => "N", // Отправлять пользователя по обратной ссылке, если она есть
        "SUCCESS_PAGE" => "/auth/register_confirm.php", // Страница окончания регистрации
        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
        "USER_PROPERTY" => array(// Показывать доп. свойства
            0 => "UF_USER_AGREEMENT",
            1 => "UF_KITCHEN"
        )
            ), false
    );
    ?>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>