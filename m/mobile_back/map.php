<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<style type="text/css">
    .ymaps-b-balloon__content-body a {color: black; text-decoration:none; text-shadow: none;}
    .ymaps-b-balloon__content-body a:hover {text-decoration: underline;}
    .rest-name {font-weight: bold;}
    .ymaps-default-cluster {background-image: url("/tpl/images/map/black-large.png") !important;}
    .ymaps-b-clusters-content__layout a {color: black; text-decoration:none; text-shadow: none;}
    .ymaps-b-clusters-content__layout a:hover {text-decoration: underline;}
</style>
<input type="hidden" value="0" id="balloonopen"/>
<script type="text/javascript">
    function success_a(position) {        
        myMap.setCenter([position.coords.latitude,position.coords.longitude],14);
        var new_point = [sessionLat,sessionLon];
        var c_point = [coord[1],coord[0]];        
        var hhh = ymaps.coordSystem.geo.getDistance(c_point, new_point);
        console.log(hhh);
        if (hhh>500000)
            myMap.setCenter(c_point,14);                
        $.ajax({
            type: "POST",
            url: "/mobile/ajax-coordinates.php",
            data: {
                lat:position.coords.latitude,
                lon:position.coords.longitude
            },
            success: function(data) {
            
            }
        });
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if (navigatorOn != 'Y') {
            if (navigator.geolocation) {                
                navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }
        } else {            
            setTimeout("myMap.setCenter([sessionLat, sessionLon],14)", 500);
            //myMap.setCenter([sessionLat, sessionLon],15)
        }
    }
    function UpdateMarkers()
    {
        if ($('#balloonopen').val() == 1){
            $('#balloonopen').val(0);
            return false;
        }
        
        // get short side of screen
        mapwidth = $('#map').width();
        mapheight = $('#map').height();
        if (mapwidth < mapheight){
            paddingValue = mapwidth;
        } else {
            paddingValue = mapheight;
        }
        // get parts for clustering
        if (mapwidth > 239 && mapwidth <= 321){
            parts = 2;
        }
        if (mapwidth > 321 && mapwidth <= 800){
            parts = 3;
        }
        if (mapwidth > 800 ){
            parts = 7;
        }
        //get borderCoeff for padding from borders
        if (paddingValue > 239 && paddingValue <= 321){
            borderCoeff = 9;
        }
        if (paddingValue > 321 && paddingValue <= 800){
            borderCoeff = 11;
        }
        if (paddingValue > 800 ){
            borderCoeff = 13;
        }
        
        var bounds = myMap.getBounds ();
        map_filter['yl'] = bounds[0][1]; //left
        map_filter['yr'] = bounds[1][1]; //right
        map_filter['xr'] = bounds[1][0]; //top
        map_filter['xl'] = bounds[0][0]; //bottom
        if (mapwidth < mapheight){
            padding = (parseFloat(bounds[1][1])-parseFloat(bounds[0][1]))/borderCoeff;
        } else {
            padding = (parseFloat(bounds[1][0])-parseFloat(bounds[0][0]))/borderCoeff;
        }
        
        //padding from borders of map
        map_filter['yl'] = parseFloat(bounds[0][1])+(padding*1.2);
        map_filter['yr'] = parseFloat(bounds[1][1])-padding*0.2;
        map_filter['xr'] = parseFloat(bounds[1][0])-padding*0.5;
        map_filter['xl'] = parseFloat(bounds[0][0])+padding*0.1;
        //if(map_fil)
        //    realrequestMarkersUrl = requestMarkersUrl+"&"+map_fil;
        //else
        realrequestMarkersUrl = requestMarkersUrl;
        jQuery.get ( realrequestMarkersUrl, map_filter, function ( data ) {
            if (data)
            {
                myMap.geoObjects.each(function (geoObject) {
                    myMap.geoObjects.remove(geoObject)
                })
                var coords = [];
                for ( var i in data )
                {
                    if ( i != 'length' && typeof ( map_markers[data[i].id] ) == 'undefined')
                    {   
                        coords.push([parseFloat(data[i].lat), parseFloat(data[i].lon)]);
                    }
                }
                
                //creating geoobjects
                var myGeoObjects = [];
                for (var i = 0; i<data.length; i++) {
                    //if restoran is selected, get content for balloon
                    if(startLat != '' && startLon != ''){
                        if(coords[i][0] == startLat){
                            balloonContent = '<a data-ajax="false" href="'+data[i]["url"]+'"  data-transition="fade"><span class="rest-name">'+data[i]['name']+'</span><br>'+data[i]['adres']+'</a>';
                        }
                    }
                    
                    myGeoObjects[i] = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: coords[i]
                        },
                        properties: {
                            clusterCaption: data[i]['name'],
                            balloonContentBody: '<a data-ajax="false" href="'+data[i]["url"]+'"  data-transition="fade"><span class="rest-name">'+data[i]['name']+'</span><br>'+data[i]['adres']+'</a>'
                        }
                    }, {
                        iconImageSize: [27, 32],
                        iconImageHref : "/tpl/images/map/ico_rest.png",
                        iconImageOffset: [-15, -32],
                        preset: 'twirl#invertedWhiteClusterIcons'
                
                    });
                }
                
                if(sessionLat != '' && sessionLon != ''){
                    var myPlacemark = new ymaps.Placemark([sessionLat, sessionLon], {}, {
                        preset: 'twirl#lightblueDotIcon'
                    });
                    myMap.geoObjects.add(myPlacemark);
                }
                
                //clusters
                var myClusterer = new ymaps.Clusterer();
                myClusterer.add(myGeoObjects);
                if (myMap.getZoom() <= 14 ){
                    
                    myClusterer.options.set({
                        gridSize: mapwidth/parts,
                        minClusterSize: 33333
                    });
                }
                    
                if (myMap.getZoom() == 15 ){   
                  
                    myClusterer.options.set({
                        gridSize: mapwidth/parts,
                        minClusterSize: 33333
                    });
                }    
                if (myMap.getZoom() > 15 ){   
                   
                    myClusterer.options.set({
                        gridSize: mapwidth/parts,
                        minClusterSize: 33333
                    });
                }
                // add clusters
                myMap.geoObjects.add(myClusterer);
                
                if(startLat != '' && startLon != '' && firsttimeballoon == 1 ){
                    myMap.balloon.open(
                    [startLat, startLon], {
                        content: balloonContent
                    }, {
                        closeButton: true
                    });
                    firsttimeballoon = 0;
                }
            }
            else
            {
                //if (map_fil)
                // $("#how_much").html("<div style='margin-top:45px'>Ничего не найдено</div>");
            }
        }, 'json' );
    }
    
    // map init
    ymaps.ready(init);
    var myMap,
    myPlacemark;
    // center on selected restaurant
    startLat = '<?= $_GET['lat'] ?>';
    startLon = '<?= $_GET['lon'] ?>';

    function init(){
        // if restaurant selected
        firsttimeballoon = 1;
        if(startLat != '' && startLon != ''){
            myMap = new ymaps.Map ("map", {
                center: [startLat, startLon],
                zoom: 17
            });
        } else {
            // just a map
            get_location_a();
            myMap = new ymaps.Map ("map", {
                center: [coord[1], coord[0]],
                //center: [57, 30],
                //center: [ymaps.geolocation.latitude, ymaps.geolocation.longitude],
                zoom: 14
            });                    
        }
        
        myMap.controls.add(
            new ymaps.control.ZoomControl()
        );   
            
        myMap.events.add("boundschange",
        function(e) {
            oldCenter = e.get('oldGlobalPixelCenter');
            console.log(oldCenter);
            newCenter = e.get('newGlobalPixelCenter');
            differnce = Math.sqrt((parseInt(oldCenter[0])-parseInt(newCenter[0]))*(parseInt(oldCenter[0])-parseInt(newCenter[0]))+(parseInt(oldCenter[1])-parseInt(newCenter[1]))*(parseInt(oldCenter[1])-parseInt(newCenter[1])));

            if (differnce > 30){
                UpdateMarkers();
            }
        });
        
        myMap.balloon.events.add('open', function () {
            $('#balloonopen').val(1);
        });
        // close ballon when click on map
        myMap.events.add('click', function () {
            myMap.balloon.close();   
        });
                
        //UpdateMarkers();
        
        UpdateMarkers();
        myMap.options.set('openBalloonOnClick', true);
        myMap.options.set('minZoom', 14);
    }
</script>
<script type="text/javascript">
    
    var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';
    var sessionLat = '<?= $_SESSION['lat'] ?>';
    var sessionLon = '<?= $_SESSION['lon'] ?>';
    
    $(document).ready(function(){   
        $('#map').height($('html').height()-45);
        //$('#header').hide();
        var balloonOpen = 0;
    });
    var map = null;
    var map_markers = [];
    var markers_data = {};
    var map_filter = {};
    var hidden_marker = null;
    var sat_map_type = false;
    var map_type = 'default';
    var to_show = null;
    var cur_user = 0;
    var requestMarkersUrl = "/tpl/ajax/map_load_markers_mobile.php?<?= bitrix_sessid_get() ?>";
    var realrequestMarkersUrl = "";
    var my_style = {};
    var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
    coord = coord.split(",");
</script>

<div id="map" style="width: 100%; height: 400px; position: relative;"></div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>