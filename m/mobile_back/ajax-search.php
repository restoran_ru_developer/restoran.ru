<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$arRestIB = getArIblock("catalog", CITY_ID);
$temp = explode("[", $_REQUEST["q"]);
$_REQUEST["q"] = $temp[0];
if (strlen($_REQUEST["q"]) > 1) {
    $APPLICATION->IncludeComponent(
            "bitrix:search.page", "rest_ajax", Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "N",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "arrFilter",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "15",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86400",
        "CACHE_NOTES" => "",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_catalog"),
        "arrFILTER_iblock_catalog" => array($arRestIB["ID"]),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
            )
    );
}
?>