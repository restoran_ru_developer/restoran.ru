<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Поиcк Ресторан.ру");
$arRestIB = getArIblock("catalog", CITY_ID);
$temp = explode("[", $_REQUEST["q"]);
$_REQUEST["q"] = $temp[0];
?>
<script>
$( document ).on( "pageinit", "#page", function() {
    $( "#autocomplete" ).on( "listviewbeforefilter", function ( e, data ) {
        var $ul = $( this ),
            $input = $( data.input ),
            value = $input.val(),
            html = "";
        $ul.html( "" );
        if ( value && value.length > 2 ) {
            $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
            $ul.listview( "refresh" );
            $.ajax({
                url: "/suggest.php",
                dataType: "json",                
                data: {
                    q: $input.val()
                }
            })
            .then( function ( response ) {
                $.each( response, function ( i, val ) {
                    html += "<li onclick='location.href=\"/detail.php?RESTOURANT="+val.CODE+"\"'>" + val.NAME + "</li>";
                });
                $ul.html( html );
                $ul.listview( "refresh" );
                $ul.trigger( "updatelayout");
            });
        }
    });
});
</script>
<div class="fieldset-container" style="
   background: rgb(102,102,102);
background: -moz-linear-gradient(top,  rgba(102,102,102,1) 0%, rgba(255,255,255,1) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(102,102,102,1)), color-stop(100%,rgba(255,255,255,1)));
background: -webkit-linear-gradient(top,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
background: -o-linear-gradient(top,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
background: -ms-linear-gradient(top,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
background: linear-gradient(to bottom,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#666666', endColorstr='#ffffff',GradientType=0 );

border-radius: 5px;
padding: 5px 0px;
height: 35px;
text-shadow: none !important;
margin-top: -12px;
margin-left: -10px;
margin-right: -10px;
text-align: center; float:none;margin-bottom: 10px;
box-shadow: 0px 0px 5px #000;">
    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" >
        <?
        echo '<a data-role="button" data-mini="true" class="ui-btn-active" data-theme="c" data-ajax="true" href="/search.php">по названию</a>';
        echo '<a data-role="button" data-mini="true" data-theme="c" data-ajax="true" href="/filter.php">по параметрам</a>';
        echo '<a data-role="button" data-mini="true" data-theme="c" data-ajax="false" href="/map.php">по карте</a>';
        ?>    
    </fieldset>
</div>
<style>
    .ui-input-search{
        margin:10px!important;
    }
</style>
<div class="clear"></div>
<div id="content" style="margin:0px -10px;padding-top:15px;">    
<!--    <form name="rest_filter_form" method="get" action="/search.php" >        
        <input type="hidden" value="rest" name="search_in" id="by_name">
        <input type="text" onspeechchange="startSearch" data-theme="b" value="" name="q" speech="" x-webkit-speech="" id="search_input" autocomplete="off" data-role="listview" data-inset="true" data-filter="true">            
        <input type="submit" value="Поиск" data-icon="search" data-theme="b" id="search_submit_new">                    
    </form>
    <div class="clear"></div>-->
    <ul id="autocomplete" data-corners="false" data-shadow="false" data-iconshadow="false" data-role="listview" data-inset="true" data-filter="true" data-filter-placeholder="Поиск ресторана..." data-filter-theme="c"></ul>
    <?    
    /*if (strlen($_REQUEST["q"]) > 1) {
        $APPLICATION->IncludeComponent(
            "restoran:simple.search", "rest", Array(
            "COUNT" => 20,
            "NO_SLEEP"=>"Y"
            )
        );
    }*/
    ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>