<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(!$_REQUEST["q"] || !check_bitrix_sessid())
    return;

$APPLICATION->IncludeComponent(
    "restoran:simple.search",
    "suggest",
    Array(
        "COUNT" => 5
    )
);
?>