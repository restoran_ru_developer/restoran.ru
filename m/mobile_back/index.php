<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<style type="text/css">
    .ymaps-b-balloon__content-body a {color: black; text-decoration:none; text-shadow: none;}
    .ymaps-b-balloon__content-body a:hover {text-decoration: underline;}
    .rest-name {font-weight: bold;}
    .ymaps-default-cluster {background-image: url("/tpl/images/map/black-large.png") !important;}
    .ymaps-b-clusters-content__layout a {color: black; text-decoration:none; text-shadow: none;}
    .ymaps-b-clusters-content__layout a:hover {text-decoration: underline;}
</style>
<input type="hidden" value="0" id="balloonopen"/>
<script type="text/javascript">
    var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';
    var sessionLat = '<?= $_SESSION['lat'] ?>';
    var sessionLon = '<?= $_SESSION['lon'] ?>';  
    var myMap;
    var myPlacemark;
    var map = null;
    var map_markers = [];
    var markers_data = {};
    var map_filter = {};
    var hidden_marker = null;
    var sat_map_type = false;
    var map_type = 'default';
    var to_show = null;
    var cur_user = 0;
    var requestMarkersUrl = "/tpl/ajax/map_load_markers_mobile.php?<?= bitrix_sessid_get() ?>";
    var realrequestMarkersUrl = "";
    var my_style = {};
    var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
    var city_id = "<?= CITY_ID ?>";
    coord = coord.split(",");
    function success_a(position) {
        //myMap.setCenter([position.coords.latitude,position.coords.longitude],15);                
        
        $.ajax({
            type: "POST",
            url: "/ajax-coordinates.php",
            data: {
                lat:position.coords.latitude,
                lon:position.coords.longitude
            },
            success: function(data) {             
                location.reload();
            }
        });
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if (navigatorOn != 'Y') {
            if (navigator.geolocation) {                
                navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }
        } else {            
            //setTimeout("myMap.setCenter([sessionLat, sessionLon],15)", 500);
            myMap.setCenter([sessionLat, sessionLon], 14);
        }
    }
    function UpdateMarkers()
    {
        var borderCoeff = "";
        var padding = "";
        if ($('#balloonopen').val() == 1){
            $('#balloonopen').val(0);
            return false;
        }
        
        // get short side of screen
        mapwidth = $('#map').width();
        mapheight = $('#map').height();
        if (mapwidth < mapheight){
            paddingValue = mapwidth;
        } else {
            paddingValue = mapheight;
        }
        // get parts for clustering
        if (mapwidth > 239 && mapwidth <= 321){
            parts = 2;
        }
        if (mapwidth > 321 && mapwidth <= 800){
            parts = 3;
        }
        if (mapwidth > 800 ){
            parts = 7;
        }
        //get borderCoeff for padding from borders
        if (paddingValue > 239 && paddingValue <= 321){
            borderCoeff = 9;
        }
        if (paddingValue > 321 && paddingValue <= 800){
            borderCoeff = 11;
        }
        if (paddingValue > 800 ){
            borderCoeff = 13;
        }
        
        var bounds = myMap.getBounds();        
        
        map_filter["yl"] = bounds[0][1]; //left
        map_filter["yr"] = bounds[1][1]; //right
        map_filter["xr"] = bounds[1][0]; //top
        map_filter["xl"] = bounds[0][0]; //bottom
        
        
        if (mapwidth < mapheight){           
            padding = (parseFloat(bounds[1][1])-parseFloat(bounds[0][1]))/borderCoeff;
        } else {            
            
            padding = (parseFloat(bounds[1][0])-parseFloat(bounds[0][0]))/borderCoeff;
        }        
        //padding from borders of map
        map_filter["yl"] = parseFloat(bounds[0][1])+(padding*1.2);
        map_filter["yr"] = parseFloat(bounds[1][1])-padding*0.2;
        map_filter["xr"] = parseFloat(bounds[1][0])-padding*0.5;
        map_filter["xl"] = parseFloat(bounds[0][0])+padding*0.1;
        //if(map_fil)
        //    realrequestMarkersUrl = requestMarkersUrl+"&"+map_fil;
        //else
        
        realrequestMarkersUrl = requestMarkersUrl;        
        
        jQuery.get ( realrequestMarkersUrl, map_filter, function ( data ) {
            if (data)
            {                            
                /*$.ajax({
                    type: "POST",
                    url: "/mobile/opinions_index.php",
                    data: {
                        data:data
                    },
                    success: function(data) {
                        $('#reviews').html(data);
                        $('#reviews').fadeIn();
                        $('.more-reviews').fadeIn();
                        $('.rest-container').fadeIn();
                        
                    }
                });*/
                myMap.geoObjects.each(function (geoObject) {
                    myMap.geoObjects.remove(geoObject)
                })
                var coords = [];
                for ( var i in data )
                {
                    if ( i != 'length' && typeof ( map_markers[data[i].id] ) == 'undefined')
                    {   
                        coords.push([parseFloat(data[i].lat), parseFloat(data[i].lon)]);
                    }
                }
                
                //creating geoobjects
                var myGeoObjects = [];
                var  balloonContent = "";
                
                for (var i = 0; i<data.length; i++) {
                    //if restoran is selected, get content for balloon
                    if(startLat != '' && startLon != ''){
                        if(coords[i][0] == startLat){
                            balloonContent = '<a data-ajax="false" href="'+data[i]["url"]+'"  data-transition="slide"><span class="rest-name">'+data[i]['name']+'</span><br>'+data[i]['adres']+'</a>';
                        }
                    }
                    
                    if(sessionLat != '' && sessionLon != ''){
                        var myPlacemark = new ymaps.Placemark([sessionLat, sessionLon], {}, {
                            //preset: 'twirl#lightblueDotIcon'
                        });
                        myMap.geoObjects.add(myPlacemark);
                    }
                    
                    myGeoObjects[i] = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: coords[i]
                        },
                        properties: {
                            clusterCaption: data[i]['name'],
                            balloonContentBody: '<a data-ajax="false" href="'+data[i]["url"]+'"  data-transition="slide"><span class="rest-name">'+data[i]['name']+'</span><br>'+data[i]['adres']+'</a>'
                        }
                    }, {
                        iconImageSize: [27, 32],
                        iconImageHref : "/tpl/images/map/ico_rest.png",
                        iconImageOffset: [-15, -32],
                        preset: 'twirl#invertedWhiteClusterIcons'
                
                    });
                }                
                //alert(data.length);
               
                
                //clusters
                var myClusterer = new ymaps.Clusterer();
                myClusterer.add(myGeoObjects);
                if (myMap.getZoom() <= 14 ){
                    
                    myClusterer.options.set({
                        gridSize: mapwidth/parts,
                        //minClusterSize: 3,
                        minClusterSize: 33333,
                        zoomMargin : 30
                    });0
                }                    
                if (myMap.getZoom() == 15 ){
                  
                    myClusterer.options.set({
                        gridSize: mapwidth/parts,
                        //minClusterSize: 3,
                        minClusterSize: 33333,
                        zoomMargin : 30
                    });
                }    
                if (myMap.getZoom() > 15 ){   
                   
                    myClusterer.options.set({
                        gridSize: mapwidth/parts,
                        minClusterSize: 33333,
                        zoomMargin : 30
                    });
                }
                
                // add clusters
                myMap.geoObjects.add(myClusterer);
                
                if(startLat != '' && startLon != '' && firsttimeballoon == 1 ){
                    myMap.balloon.open(
                    [startLat, startLon], {
                        content: balloonContent
                    }, {
                        closeButton: true
                    });
                    firsttimeballoon = 0;
                }

                //alert(12);
            }
            else
            {
                //if (map_fil)
                // $("#how_much").html("<div style='margin-top:45px'>Ничего не найдено</div>");
            }
        }, 'json' );
    }
    
    // map init    
    ymaps.ready(init);    
    // center on selected restaurant
    startLat = '<?= $_GET['lat'] ?>';
    startLon = '<?= $_GET['lon'] ?>';

    function init(){
        // if restaurant selected
        firsttimeballoon = 1;
        if(startLat !== '' && startLon !== ''){            
            myMap = new ymaps.Map ("map", {
                center: [startLat, startLon],
                zoom: 14                
            });
        } else {            
            // just a map            
            myMap = new ymaps.Map ("map", {
                center: [coord[1], coord[0]],
                zoom: 14              
            });          
            get_location_a();
        }
        myMap.behaviors.disable(['drag']);
        myMap.controls.add( new ymaps.control.ZoomControl());   
            
        myMap.events.add("boundschange",
        function(e) {
            oldCenter = e.get('oldGlobalPixelCenter');
            newCenter = e.get('newGlobalPixelCenter');            
            differnce = Math.sqrt((parseInt(oldCenter[0])-parseInt(newCenter[0]))*(parseInt(oldCenter[0])-parseInt(newCenter[0]))+(parseInt(oldCenter[1])-parseInt(newCenter[1]))*(parseInt(oldCenter[1])-parseInt(newCenter[1])));

            if (differnce > 30){
                UpdateMarkers();
            }
        }
    );
        
        myMap.balloon.events.add('open', function () {
            $('#balloonopen').val(1);
        });
        // close ballon when click on map
        myMap.events.add('click', function () {
            myMap.balloon.close();   
        });
                
        myMap.options.set('openBalloonOnClick', true);
        myMap.options.set('minZoom', 13);
        
        var new_point = [sessionLat,sessionLon];
        var c_point = [coord[1],coord[0]];        
        var hhh = ymaps.coordSystem.geo.getDistance(c_point, new_point);
        if (hhh>500000)
            myMap.setCenter(c_point,15);                
        UpdateMarkers();    
    }
</script>
<script type="text/javascript">        
    $(document).ready(function(){   
        //$('#map').height($('html').height()-50);
        //$('#header').hide();
        var balloonOpen = 0;               
        if ($.browser.safari==true)
        {                                               
            $("#map").css("height",($(window).height()-($(window).height()*0.3))+"px");
            $("#reviews").css("margin-top",($(window).height()-($(window).height()*0.25))+"px");    
        }
        else
        {
            $("#map").css("height",($(window).height()-($(window).height()*0.35))+"px");
            $("#reviews").css("margin-top",($(window).height()-($(window).height()*0.33))+"px");    
        }
//        $(".slides_container").slides({
//            responsive: true,
//            navigation: true,
//            height: 75
//        });
//        
//        if(typeof intervalID !== 'undefined'){
//            clearInterval(intervalID); 
//        }         
        //intervalID = setInterval("$('.slidesNext').click();", 5000);
                
        //        $( ".swipe-block" ).on( 'swipeleft', swipeleftHandler );
        //        function swipeleftHandler( event ) {
        //            $(".slidesNext").click();
        //            //alert(3);
        //            //setTimeout('clearTimeout(ID);$(".slidesNext").click();', 25);
        //        }
        //        $( ".swipe-block" ).on( 'swiperight', swiperightHandler );
        //        function swiperightHandler( event ) {
        //            //alert(2);
        //            //setTimeout('clearTimeout(ID);$(".slidesPrevious").click();', 25);
        //            $(".slidesPrevious").click();
        //        }
        //$( ".swipe-block" ).on( 'taphold', clickHandler );
        $( ".swipe-block" ).on( 'click', clickHandler );
        function clickHandler( event ) {
            href = $(".one-slide:visible").attr('href');
            location.replace(href);
        }	
    });    
</script>

<div id="map" style="width: 100%; height: 200px; margin-top:22px; overflow: hidden;"></div>
<!--<div id="map_overlay" style="width:100%; height:400px" onclick="location.href='map.php'"></div>-->
<!--<div id="map_link" onclick="location.href='map.php'">
    <div>
        <input type="button" value="Ближайшие рестораны"  data-theme="e" onclick="location.href='map.php'"  />    
    </div>
</div>-->
<div class="clear"></div>
<div id="reviews" style="display:block"></div>
<div class="clear"></div>
<div class="button-font13">
    <a style="width:49%; float: left; margin-right: 2%;" data-role="button" data-theme="b" data-mini="false" data-ajax="false" href="/online_order.php">Забронировать</a>
    <a style="width:49%; float: left;" data-role="button" data-theme="b" data-mini="false" data-ajax="true" href="/filter.php">Фильтр</a>
</div>
<? if ($_SESSION['lat']): ?>
    <a style="width:100%; float: left;" data-role="button" data-theme="b" data-mini="false" data-ajax="true" href="/catalog.php?page=1&pageRestSort=distance&by=asc&CITY_ID=<?=CITY_ID?>&set_filter=Y">Ближайшие рестораны</a>
<? endif; ?>
<div class="clear"></div>
<?
$arIndexIB = getArIblock("index_page", CITY_ID);
global $arTopBlock;
$arTopBlock["CODE"] = "mobile_main";
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "index_block_top4",
    Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "index_page",
        "IBLOCK_ID" => $arIndexIB["ID"],
        "NEWS_COUNT" => "1",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "arTopBlock",
        "FIELD_CODE" => "",
        "PROPERTY_CODE" => array(
            0  => "ELEMENTS",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "120",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
    false
    );
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>			