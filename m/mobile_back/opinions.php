<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
$arOverviewsIB = getArIblock("reviews", CITY_ID);
if ($_REQUEST["RESTOURANT"]):
    $id = RestIBlock::GetRestIDByCode($_REQUEST["RESTOURANT"]);
endif;
?>
<div id="content" class="opinions-page">
    <input type="hidden" id="rest" value="<?= $_REQUEST["RESTOURANT"] ?>">

    <?
    if (!$_REQUEST["tid"]):
        $arReviewsIB = getArIblock("reviews", CITY_ID);
        if ($id) {
            global $arrFilter;
            $arrFilter = array();
            $arrFilter["PROPERTY_ELEMENT"] = $id;
        }
        if ($_REQUEST["photos"] == "Y") {
            global $arrFilter;
            $arrFilter["!PROPERTY_photos"] = false;
        }
        if ($_REQUEST["video"] == "Y") {
            global $arrFilter;
            $arrFilter[0] = Array("LOGIC" => "OR",
                Array("!PROPERTY_video" => false),
                Array("!PROPERTY_video_youtube" => false)
            );
        }
        $APPLICATION->IncludeComponent(
                "restoran:catalog.list", "reviews_all", Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "reviews",
            "IBLOCK_ID" => $arOverviewsIB["ID"],
            "NEWS_COUNT" => 10,
            "SORT_BY1" => "created_date",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PAGE_URL"),
            "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "video_youtube", "COMMENTS"),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            //"CACHE_TIME" => "3600000",
            "CACHE_TIME" => "3600000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "search_rest_list",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "NO_CACHE_TEMPLATE" => "Y"
                ), false
        );
    else:
        ?>
        <?
        global $arrFilter;
        $arrFilter = array();
        $old_id = RestIBlock::getOldOpinionID((int) $_REQUEST["tid"]);
        if ($old_id) {
            LocalRedirect($APPLICATION->GetCurPageParam("tid=" . $old_id, array("tid", CITY_ID)), true, "301 Moved permanently");
            $arrFilter["ID"] = $old_id;
        }
        else
            $arrFilter["ID"] = $_REQUEST["tid"];
        $APPLICATION->IncludeComponent(
                "restoran:catalog.list", "reviews_all_tid", Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "reviews",
            "IBLOCK_ID" => $arOverviewsIB["ID"],
            "NEWS_COUNT" => "1",
            "SORT_BY1" => "created_date",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY"),
            "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "video_youtube", "COMMENTS"),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "Y",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "search_rest_list",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        ?>
        <div  id="comments" >
            <?
            $APPLICATION->IncludeComponent(
                    "restoran:comments_add_new", "review_comment", Array(
                "IBLOCK_TYPE" => "comment",
                "IBLOCK_ID" => 2438,
                "ELEMENT_ID" => $arrFilter["ID"],
                "IS_SECTION" => "N",
                "MY_CAPTCHA" => "Y",
                "OPEN" => "Y"
                    ), false
            );
            ?>
            <?
            $APPLICATION->IncludeComponent(
                    "restoran:comments", "review_comment_new", Array(
                "IBLOCK_TYPE" => "comment",
                "ELEMENT_ID" => $arrFilter["ID"],
                "IBLOCK_ID" => $arCommentIB["ID"],
                "PROPERTY_CODE" => Array("photos"),
                "IS_SECTION" => "N",
                "ADD_COMMENT_TEMPLATE" => "",
                "COUNT" => 999,
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                    ), false
            );
            ?>
        </div>
    <? endif; ?>
    <div class="clear"></div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>