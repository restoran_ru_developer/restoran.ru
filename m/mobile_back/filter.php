<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расширенный поиск");
$arIB = getArIblock('catalog', CITY_ID);
?>
<div class="fieldset-container" style="
   background: rgb(102,102,102);
background: -moz-linear-gradient(top,  rgba(102,102,102,1) 0%, rgba(255,255,255,1) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(102,102,102,1)), color-stop(100%,rgba(255,255,255,1)));
background: -webkit-linear-gradient(top,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
background: -o-linear-gradient(top,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
background: -ms-linear-gradient(top,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
background: linear-gradient(to bottom,  rgba(102,102,102,1) 0%,rgba(255,255,255,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#666666', endColorstr='#ffffff',GradientType=0 );

border-radius: 5px;
padding: 5px 0px;
height: 35px;
text-shadow: none !important;
margin-top: -12px;
margin-left: -10px;
margin-right: -10px;
text-align: center; float:none;margin-bottom: 10px;
box-shadow: 0px 0px 5px #000;">
    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" >
        <?
        echo '<a data-role="button" data-mini="true" data-theme="c" data-ajax="true" href="/search.php">по названию</a>';
        echo '<a data-role="button" data-mini="true" class="ui-btn-active" data-theme="c" data-ajax="true" href="/filter.php">по параметрам</a>';
        echo '<a data-role="button" data-mini="true" data-theme="c" data-ajax="false" href="/map.php">по карте</a>';
        ?>    
    </fieldset>
</div>
<div class="clear"></div>

<?$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"rests",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => array("subway", "out_city", "average_bill", "kitchen", "features"),
		"PRICE_CODE" => array(),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"CACHE_NOTES" => "mobile",
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"SAVE_IN_SESSION" => "N",
                "NO_NAME" => "Y"
	)
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>