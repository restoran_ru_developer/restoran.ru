<?
ob_start();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
function callback($buffer){    
    echo $_SESSION["text"] = $buffer;
    return '';    
}
if ($_REQUEST["page"])
    $_REQUEST["PAGEN_1"] = (int)$_REQUEST["page"];
    if (!$_REQUEST["PARENT_SECTION_ID"]):
        //global $DB;
        CModule::IncludeModule("iblock");
        /*$sql = "SELECT ID,NAME FROM b_iblock_element WHERE CODE='".$DB->ForSql($_REQUEST["RESTOURANT"])."' LIMIT 1";
        $res = $DB->Query($sql);
        if ($ar = $res->Fetch())
        {
            $id = $ar["ID"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
        }*/
        $arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arIB["ID"],"ID"=>$_REQUEST["ID"]),false,Array("nTopCount"=>1),Array("ID","NAME","DETAIL_PAGE_URL"));
        if ($ar = $res->GetNext())
        {
            $id = $ar["ID"];
            $name = $ar["NAME"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
            $url = "/detail.php?ID=".$ar["ID"]."&CITY_ID=".$_REQUEST["CITY_ID"];
        }
               
        /*$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>(int)$id);
        $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
        if($ar_result = $db_list->Fetch())
        {
            $menu = $ar_result['ID'];
        }*/
        global $DB;
        $sql = "SELECT ID FROM b_iblock WHERE DESCRIPTION=".$DB->ForSql($id);
        $db_list = $DB->Query($sql);
        if($ar_result = $db_list->GetNext())
        {
            $menu = $ar_result['ID'];
        }
        //$_REQUEST["SECTION_ID"] = false;
        //$_REQUEST["PARENT_SECTION_ID"] = false;
    endif;
    ?>
    <h1 class="with_link" align="center">Меню ресторана «<a href="<?=$url?>" alt="<?=$ar["NAME"]?>" title="<?=$ar["NAME"]?>"><?=$ar["NAME"]?></a>»</h1>

<!--    <meta name="viewport" content="width=device-width; target-densityDpi=device-dpi; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>-->

<!--    <meta name="apple-mobile-web-app-capable" content="yes" />-->
<!--    <meta name="apple-mobile-web-app-status-bar-style" content="black" />-->
<!--    <meta name="HandheldFriendly" content="true"/>-->

<!--    <meta name="mobile-web-app-capable" content="yes">-->
<!--    <meta name="user-scalable" content="no">-->

    <style>
        * {
            font-family: Arial;
        }
        /*@media (max-width: 767px) {*/
            /*span.menu_price {*/
                /*font-weight: 600;*/
                /*font-size: 50px;*/
            /*}*/
        /*}*/

        @media (min-width: 768px) and (max-width: 1024px) {
            span.menu_price {
                font-size: 36px;
            }
        }

        .menu_price, a { 
            color:#03b1ff;
            font-size: 140%;
        }
        .name {
/*            font-weight:bold;*/
            font-size:140%;
            line-height: 24px;
        }
        i {
            font-weight:normal;
            font-size: 120%;
            line-height: 18px;                        
        }
        table td {
            border-bottom:1px solid #ccc;
            padding:10px 0px;
            font-size:16px;
        }
        table .price td{
            border-bottom:none;
            text-align:right;
        }
        table td:nth-child(2) {
            width: 22%;
        }
    </style>
<?
if ($menu)
{

    $APPLICATION->IncludeComponent(
        "bitrix:catalog",
        "dostavka",
        Array(
                "AJAX_MODE" => "Y",
                "SEF_MODE" => "N",
                "IBLOCK_TYPE" => "rest_menu_ru",
                "IBLOCK_ID" => $menu,
                "USE_FILTER" => "N",
                "USE_REVIEW" => "N",
                "USE_COMPARE" => "N",
                "SHOW_TOP_ELEMENTS" => "Y",
                "PAGE_ELEMENT_COUNT" => "12",
                "LINE_ELEMENT_COUNT" => "3",
                "ELEMENT_SORT_FIELD" => "sort",
                "ELEMENT_SORT_ORDER" => "asc",
                "LIST_PROPERTY_CODE" => array("map"),
                "INCLUDE_SUBSECTIONS" => "Y",
                "LIST_META_KEYWORDS" => "-",
                "LIST_META_DESCRIPTION" => "-",
                "LIST_BROWSER_TITLE" => "-",
                "DETAIL_PROPERTY_CODE" => array("map"),
                "DETAIL_META_KEYWORDS" => "map",
                "DETAIL_META_DESCRIPTION" => "map",
                "DETAIL_BROWSER_TITLE" => "map",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "CACHE_TYPE" => "Y",//y
                "CACHE_TIME" => "7203",
                "CACHE_NOTES" => "",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "PRICE_CODE" => array("BASE"),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "LINK_IBLOCK_TYPE" => "",
                "LINK_IBLOCK_ID" => "",
                "LINK_PROPERTY_SID" => "",
                "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                "USE_ALSO_BUY" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "TOP_ELEMENT_COUNT" => "9",
                "TOP_LINE_ELEMENT_COUNT" => "3",
                "TOP_ELEMENT_SORT_FIELD" => "sort",
                "TOP_ELEMENT_SORT_ORDER" => "asc",
                "TOP_PROPERTY_CODE" => array("map"),
                "VARIABLE_ALIASES" => Array(
                        "SECTION_ID" => "SECTION_ID",
                        "ELEMENT_ID" => "ELEMENT_ID"
                ),
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
        ),
    false
    );
}
else
{
    ShowError("Меню не найдено");
}
ob_end_flush();
$_SESSION["TEXT"] = ob_get_contents();
if (CITY_ID=="msk")
    $_SESSION["HEADER"] = '<table width="100%"><tr><td><a href="http://m.restoran.ru/"><img src="/tpl/images/logo.png" height="40"></a></td><td style="font-size:14px;text-align:right"><b>Заказ банкетов и столиков</b><Br />(495) 988-26-56</td></tr></table>';
elseif (CITY_ID=="spb")
    $_SESSION["HEADER"] = '<table width="100%"><tr><td><a href="http://m.restoran.ru/"><img src="/tpl/images/logo.png" height="40"></a></td><td style="font-size:14px;text-align:right"><b>Заказ банкетов и столиков</b><Br />(812) 740-18-20</td></tr></table>';
else
    $_SESSION["HEADER"] = '<table width="100%"><tr><td><a href="http://m.restoran.ru/"><img src="/tpl/images/logo.png" height="40"></a></td></tr></table>';

    LocalRedirect("/menu/menu.php");
?>