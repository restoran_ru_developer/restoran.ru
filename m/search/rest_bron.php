<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();
if(!$_REQUEST["q"])
    return;

/*$obCache = new CPHPCache; 
// время кеширования - 60 минут * 24
$life_time = 60*60*24; 
$cache_id = $q."bron_rest".CITY_ID; 

if($obCache->InitCache($life_time, $cache_id, "/")):
    $vars = $obCache->GetVars();
    $SEARCH_RESULT = $vars["SEARCH_RESULT"];
else :
    $arRestIB = getArIblock("catalog", CITY_ID);
    CModule::IncludeModule("iblock");
    $q = ToLower($q);
    $lang = get_lang($q);
    if ($lang=="ru")
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC","CODE"=>"DESC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $res1 = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res1 = $res1->GetNext())
                $section = $ar_res1["NAME"];
            $SEARCH_RESULT .= $ar_res["NAME"]." [".$section."]###".$ar_res["ID"]."\n";
        }
        $q = decode2anotherlang_ru($q);
        $res = CIblockElement::GetList(Array("NAME"=>"ASC","CODE"=>"DESC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $res1 = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res1 = $res1->GetNext())
                $section = $ar_res1["NAME"];
            $SEARCH_RESULT .= $ar_res["NAME"]." [".$section."]###".$ar_res["ID"]."\n";
        }
    }
    elseif ($lang=="en")
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC","CODE"=>"DESC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $res1 = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res1 = $res1->GetNext())
                $section = $ar_res1["NAME"];
            $SEARCH_RESULT .= $ar_res["NAME"]." [".$section."]###".$ar_res["ID"]."\n";
        }
        $q = decode2anotherlang_en($q);
        $res = CIblockElement::GetList(Array("NAME"=>"ASC","CODE"=>"DESC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $res1 = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res1 = $res1->GetNext())
                $section = $ar_res1["NAME"];
            $SEARCH_RESULT .= $ar_res["NAME"]." [".$section."]###".$ar_res["ID"]."\n";
        }
    }   
    else
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC","CODE"=>"DESC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $res1 = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res1 = $res1->GetNext())
                $section = $ar_res1["NAME"];
            $SEARCH_RESULT .= $ar_res["NAME"]." [".$section."]###".$ar_res["ID"]."\n";
        }
    }
    
endif;

if($obCache->StartDataCache()):      
    $obCache->EndDataCache(array(
        "SEARCH_RESULT"    => $SEARCH_RESULT
        )); 
endif;
 echo $SEARCH_RESULT; */

$APPLICATION->IncludeComponent(
    "restoran:simple.search",
    "suggest_1",
    Array(
        "COUNT" => 10,
        'NO_MOBILE'=>'Y',
    )
);
?>