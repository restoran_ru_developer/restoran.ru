<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по карте");
?>
<a class='locate'></a>
<div id="map" style="width:100%; height:100%;"></div>


<script>
    var map;
    var map_markers = [];
    <?if (!$_REQUEST["l"]):?>
        var default_center = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
        default_center = default_center.split(",");
        var zoom = 13;
    <?else:?>
        var default_center = "<?= $_REQUEST["l"] ?>";
        default_center = default_center.split(",");
        var temp = default_center[0];
        default_center[0] = default_center[1];
        default_center[1] = temp;
        var zoom = 18;
    <?endif;?>    
    var infoWindow = [];
    var infoString = [];
    var pos;
    var map_open = 0;
    function addInfoWindow(i,marker, message, map, link) {
        infoString[i] = '<div id="partnerNotice">' +
            '<a href="'+ link +'">' + message + '</a>' +            
            '</div>';        
        infoWindow[i] = new google.maps.InfoWindow({
            content: infoString[i],         
            //disableAutoPan: true
        });        
        google.maps.event.addListener(marker, 'click', function () {
            for(var p=0; p<infoWindow.length;p++)
            {
                infoWindow[p].close();
            }
            console.log()
            infoWindow[i].open(map, marker);
        });        
        <?if ($_REQUEST["id"]):?>                
            var b = <?=$_REQUEST["id"]?>;            
            if (b==marker["id"].id)
            {                
                infoWindow[i].open(map, marker);
            }
        <?endif;?>
    }
    
    function load_markers() {
        //Load markers from DB
         var image = new google.maps.MarkerImage(
            '<?=SITE_TEMPLATE_PATH?>/images/map_point.png',        
            null,        
            null,
            null,
            new google.maps.Size(25, 29)
        );
        var me = new google.maps.MarkerImage(
            '<?=SITE_TEMPLATE_PATH?>/images/m_m.png',        
            null,        
            null,
            null,
            new google.maps.Size(25, 32)
        );
         var shape = {
            coord: [1, 1, 25, 29],
            type: 'rect'
        };
        var requestMarkersUrl = "/ajax/load_markers.php?<?= bitrix_sessid_get() ?>";
          jQuery.get ( requestMarkersUrl, '', function ( data ) {            
            if (data)
            {                       
                var coords = [];
                var marker = [];
                for ( var i in data )
                {       
                    if ( i != 'length')
                    {   
                        coords.push([parseFloat(data[i].lat), parseFloat(data[i].lon)]);
                    }                
                }
                
//                for (var i = 0; i < map_markers.length; i++) {
//                    map_markers[i].setMap(null);
//                }
                map_markers = [];
                
                for (var i = 0; i<data.length; i++) {
                    //console.log(data[i]);
                    marker[i] = new google.maps.Marker({
                        //map:map,
                        id:data[i],
                        draggable:false,
                        icon: image,
                        shape: shape,
                        optimized:true,
                        animation: false,
                        position: new google.maps.LatLng(coords[i][0], coords[i][1])
                    });
                    
                    addInfoWindow(i,marker[i], "<a class='ajax' href='"+data[i]["url"]+"'>"+data[i]["name"]+"</a><br /><i>"+data[i]["adres"]+"</i>", map, data[i]["url"]);                                                            
                }                
                var markerClusterer = new MarkerClusterer(map, marker, 
                { 
                    maxZoom: 14,
                    gridSize: 50,
                    styles: null
                });
            }                
        }, 'json' );
    }
    function initialize() {        
        var mapOptions = {
          zoom: zoom,
          center: new google.maps.LatLng(default_center[1], default_center[0]),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          panControl: false,
//            zoomControl: false,
            scaleControl: false,
            mapTypeControl: false,
            overviewMapControl: false,
            streetViewControl: false,
            rotateControl: false,
            scrollwheel : false,
        }
        var styles =
        [
            {
                "featureType": "transit.line",
                "stylers": [
                  { "visibility": "off" }
                ]
            },
            {
                "featureType": "landscape",
                "stylers":
                [
                    { "color": "#ffffff" }
                ]
            },            
            {
                "featureType": "water",
                "stylers":
                [
                    { "color": "#b3d1ff" }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.stroke",
                "stylers":
                [
                    { "color": "#444444" }
                ]
            }                        
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');
                
        var me = new google.maps.MarkerImage(
            '<?=SITE_TEMPLATE_PATH?>/images/m_m.png',        
            null,        
            null,
            null,
            new google.maps.Size(25, 32)
        );        
         // Try HTML5 geolocation
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              pos = new google.maps.LatLng(position.coords.latitude,
                                               position.coords.longitude);

                    new google.maps.Marker({
                        map:map,
                        draggable:false,
                        icon: me,                                                
                        animation: google.maps.Animation.DROP,
                        position: pos
                    });
              <?if (!$_REQUEST["l"]):?>
                map.setCenter(pos);
              <?endif;?>
              $(".gm-style").eq(0).find("div").eq(0).find("div").eq(0).next().remove();
            }, function() {
              handleNoGeolocation(true);
            });
        } else {
            // Browser doesn't support Geolocation
            handleNoGeolocation(false);
        }            
        setTimeout("load_markers()",1000);                  
    }
        
    function loadScript() {
        map_open = 1;
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBIPD9UKtX4nflE-8ktUp62E8DVReinvUo&sensor=true&callback=initialize";
        document.body.appendChild(script);
    }
    $(function(){                                      
          $("#map").width($(window).width());          
          $("#map").height($(window).height());          
          $(".locate").click(function(){
              map.setCenter(pos);
              map.panBy(0,30);
          });
          if (!map_open)
            loadScript();              
          else
            initialize();
        $(".myhead .right_btn").html("Список").attr("href","/").addClass("ajax");            
        
    });    
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>