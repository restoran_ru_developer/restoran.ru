<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Restoran.ru");
?>
<?
$detect = new Mobile_Detect();
if ($detect->isTablet()):?>
    <div class="for_tablet"><a href="http://restoran.ru/?from_mobile=Y" >Перейти на полную версию сайта</a></div>
<?endif;?>
<?
//if ($USER->IsAdmin()):
////    $_SESSION['lat'] = '56.95827';
////    $_SESSION['lon'] = '24.1185025';
//    $_SESSION['lat'] = '59.9490252';
//    $_SESSION['lon'] = '30.3467879';
//endif;
?>

<script>
    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=get_location_a';
        document.body.appendChild(script);
    }

    var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';
    var sessionLat = '<?= $_SESSION['lat'] ?>';
    var sessionLon = '<?= $_SESSION['lon'] ?>';
    var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
    var city_id = "<?= CITY_ID ?>";
    var page = 1;
    var ajax_load = 0;
    coord = coord.split(",");
    var options_for_get_position = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 0
    };

    function success_a(position) {
        sessionLat = position.coords.latitude;
        sessionLon = position.coords.longitude;
        $.ajax({
            type: "POST",
            url: "/ajax/ajax-coordinates.php",
            data: {
                lat:position.coords.latitude,
                lon:position.coords.longitude
            },
            success: function(data) {
                //location.reload();
                $("#bubbling_text").text("Загружаем ближайшие рестораны...");
                $.get( "/",{ajax:"Y"}, function( data ) {
                    $( "main" ).html($(data).find("#ajax"));//
                    $(".bubblingG").hide();
                    history.pushState({foo: 'bar'}, $(data).find("#title").val(), "/");
                    $("title").html($(data).find("#title").val());

                    if($('.mobile-banner').length){
                        $('.mobile-banner').height($(window).height()).css({'margin-top':'-'+$('#main').css('padding-top'),'margin-bottom':$('#main').css('padding-top')});
                        $('.skip-this-banner').on('click',function(){
                            $.get( "/ajax/hideAppBanner.php",{appSliderHide:"Y"}, function( data ) {
                                $("html, body").animate({scrollTop: $(window).height() }); //крутим враппер
                            });
                        })
                    }
                });

                google.maps.event.addDomListener(window, 'load', initialize_map_how_get(sessionLat,sessionLon));
            }
        });
    }

    function error_a()
    {
        alert("Невозможно определить местоположение");
//        $("#popular_link").click();
        //if ($("#popular_link").is("hidden"))
            location.href = "/popular/?no-position=Y";
        return false;
    }
    function get_location_a()
    {
        //if (navigatorOn != 'Y'||!sessionLat) {
        if (navigatorOn != 'Y') {
            if (navigator.geolocation) {
               navigator.geolocation.getCurrentPosition(success_a, error_a,options_for_get_position);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }
        }
        else {
            console.log('add dom listener');
            google.maps.event.addDomListener(window, 'load', initialize_map_how_get(sessionLat,sessionLon));
            $(".bubblingG").hide();
            $("#bubbling_text").text("");
            //setTimeout("myMap.setCenter([sessionLat, sessionLon],15)", 500);
        }
    }

    $(function(){
        $(".bubblingG").show();
        $("#bubbling_text").text("Определяем местоположение...");

        <?//if($_REQUEST['ajax']=='Y'):?>
        <?//if(!$_REQUEST['mobile_check']):?>
//            get_location_a();
        <?if($_REQUEST['ajax']=='Y'):?>
            get_location_a();
        <?elseif($_REQUEST['ajax']!='Y'):?>
            loadScript();
        <?endif?>

        $(window).scroll(function(){
            scroll_load();
        });
    });
</script>
<?//endif?>
<div class="popup map-canvas-wrapper">
    <div class="close-map-canvas"></div>
    <div id="map-canvas" ></div>
</div>

<?
if ($APPLICATION->get_cookie("COORDINATES")):

//    FirePHP::getInstance()->info($_REQUEST["arrFilter_pf"]);
    if ($_REQUEST["arrFilter_pf"])
    {
        foreach ($_REQUEST["arrFilter_pf"] as $key=>&$fil)
        {
            if ($fil[0]==0)
                unset($_REQUEST["arrFilter_pf"][$key]);
            else {
                foreach ($fil as $key=>$f)
                {
                    if ($f==0)
                        unset($fil[$key]);
                }
            }
        }
    }
    if (!$_REQUEST["page"])
        $_REQUEST["page"] = 1;
    if (!$_REQUEST["PAGEN_1"])
        $_REQUEST["PAGEN_1"] = 1;
    if (!$_REQUEST["pageRestSort"])
        $_REQUEST["pageRestSort"] = "distance";
    if (!$_REQUEST["by"])
        $_REQUEST["by"] = "asc";
    $_REQUEST["set_filter"] = "Y";
    global $arrFilter;
    
    $arrFilter["!PROPERTY_no_mobile_VALUE"] = "Да";
    $arrFilter["PROPERTY_REST_NETWORK"] = false;
//    if($USER->IsAdmin()){
//        $arrFilter["!ID"] = array(387488,1196063,388078,388357);
//    }

    $APPLICATION->IncludeComponent("restoran:restoraunts.list", "main_rest", Array(
        "main" => "Y",
        "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID), // Код информационного блока
        "PARENT_SECTION_CODE" => "restaurants", // Код раздела
        "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "10"), // Количество ресторанов на странице
        "SORT_BY1" => ($_REQUEST["pageRestSort"] ? $_REQUEST["pageRestSort"] : "NAME"), // Поле для первой сортировки ресторанов
        "SORT_ORDER1" => "ASC", // Направление для первой сортировки ресторанов
        "SORT_BY2" => $sort2, // Поле для второй сортировки ресторанов
        "SORT_ORDER2" => $sortOreder2, // Направление для второй сортировки ресторанов
        "FILTER_NAME" => "arrFilter", // Фильтр
        "PROPERTY_CODE" => array(// Свойства
            0 => "type",
            1 => "kitchen",
            2 => "average_bill",
            3 => "opening_hours",
            4 => "phone",
            5 => "address",
            6 => "subway",
            7 => "ratio",
            8 => CITY_ID=='urm'||CITY_ID=='rga'?"area":''
        ),
        "CHECK_DATES" => "N", // Показывать только активные на данный момент элементы
        "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//Y ($_REQUEST["pageRestSort"]=="distance")?"N":"Y", //
        "CACHE_TIME" => "36000082", // Время кеширования (сек.)
        "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "N", // Учитывать права доступа
        "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
        "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
        "PAGER_TITLE" => "Рестораны", // Название категорий
        "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
        "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
        "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
        "DISPLAY_DATE" => "Y", // Выводить дату элемента
        "DISPLAY_NAME" => "Y", // Выводить название элемента
        "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
//        "DEVICE_TYPE" => substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")?'Y':'', //
        'INCLUDE_SUBSECTIONS'=>'N'
            ), false
    );
endif;
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>