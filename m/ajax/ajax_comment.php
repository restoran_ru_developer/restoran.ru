<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
CModule::IncludeModule("forum");
$text = CFilterUnquotableWords::Filter(trim($_REQUEST["review"]));
$text2 = CFilterUnquotableWords::Filter(trim($_REQUEST["plus"]));
$text3 = CFilterUnquotableWords::Filter(trim($_REQUEST["minus"]));
if (substr_count($text,"%8!*$")>0||substr_count($text2,"%8!*$")>0||substr_count($text3,"%8!*$")>0)
{
?>
    <?
    $ar["ERROR"]=1;
    if (LANGUAGE_ID=="en")
        $ar["MESSAGE"] = "Comments on the site are prohibited from crude and profanity";
    else
        $ar["MESSAGE"] = "На сайте запрещены комментарии с грубой и ненормативной лексикой";
    echo json_encode($ar);
    die;
}
?>
<?if (check_bitrix_sessid()&&$_REQUEST["review"]&&$_REQUEST["IBLOCK_TYPE"]&&(int)$_REQUEST["IBLOCK_ID"]&&(int)$_REQUEST["ELEMENT_ID"]):?>

    <?
    //$APPLICATION->ShowHeadScripts();
    $APPLICATION->IncludeComponent(
            "restoran:comments_add_simple",
            ".default",
            Array(
                    "REVIEW" => trim($_REQUEST["review"]),
                    "ACTION" => "save",
                    "RATIO" => (int)$_REQUEST["ratio"],
                    "IBLOCK_TYPE" => trim($_REQUEST["IBLOCK_TYPE"]),
                    "IBLOCK_ID" => (int)$_REQUEST["IBLOCK_ID"],
                    "ELEMENT_ID" => (int)$_REQUEST["ELEMENT_ID"],
                    "IS_SECTION" => trim($_REQUEST["IS_SECTION"]), 
                    "PARENT" => intval($_REQUEST["PARENT"]), 
                    "photos" => $_REQUEST["image"], 
                    "video" => $_REQUEST["video"], 
                    "plus" => trim($_REQUEST["plus"]), 
                    "minus" => trim($_REQUEST["minus"]), 
                    "video_youtube" => trim($_REQUEST["video_youtube"]), 
                    "NEED_CAPTCHA"=>"N"
            )
    );?>
<?endif;?>
<?require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>