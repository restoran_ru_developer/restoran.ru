<?
header('Content-type: text/css');
ob_start("compress");
function compress($buffer) {
    /* remove comments */
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    /* remove tabs, spaces, newlines, etc. */
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
    return $buffer;
}

/* your css files */
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/h5bp.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/components/components.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/main.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/ratio.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/swiper.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/mobiscroll/mobiscroll.animation.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/mobiscroll/mobiscroll.icons.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/mobiscroll/mobiscroll.scroller.css');
include('/home/bitrix/www/bitrix/templates/mobile_new/styles/mobiscroll/mobiscroll.widget.css');

ob_end_flush();
?>