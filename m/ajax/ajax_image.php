<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if ($_REQUEST["id"])
{
    //$ids = json_decode($_REQUEST["id"],true);
    
    foreach ($_REQUEST["id"] as $key=>$i)
    {
        if ($key<5)
            unset($_REQUEST["id"][$key]);        
    }
    
    $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'medium',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark1.png', 
        ),
    );
    $a = array();
    foreach ($_REQUEST["id"] as $i)
    {
        $b = CFile::ResizeImageGet((int)$i,Array("width"=>864,"height"=>420),BX_RESIZE_IMAGE_EXACT,false,$watermark);
        $a[] = $b["src"];
    }    
    echo json_encode($a);
    //$a = CFile::ResizeImageGet((int)$_REQUEST["id"],Array("width"=>864,"height"=>420),BX_RESIZE_IMAGE_EXACT,true,$watermark);
    //echo $a["src"];
}
?>