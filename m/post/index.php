<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Restoran.ru");
?>

    <script type="text/javascript">
        function loadScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
            '&signed_in=true&callback=get_location_a';
            document.body.appendChild(script);
        }
        var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';

        var sessionLat = '<?= $_SESSION['lat'] ?>';
        var sessionLon = '<?= $_SESSION['lon'] ?>';
        var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
        var city_id = "<?= CITY_ID ?>";
        var page = 1;
        var ajax_load = 0;
        coord = coord.split(",");
        var options_for_get_position = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };

        function success_a(position) {
            sessionLat = position.coords.latitude;
            sessionLon = position.coords.longitude;
            $.ajax({
                type: "POST",
                url: "/ajax/ajax-coordinates.php",
                data: {
                    lat:position.coords.latitude,
                    lon:position.coords.longitude
                },
                success: function(data) {
                    //location.reload();
                    $("#bubbling_text").text("Загружаем ближайшие рестораны...");
                    $.get( "/",{ajax:"Y"}, function( data ) {
                        $( "main" ).html($(data).find("#ajax"));
                        $(".bubblingG").hide();
                        history.pushState({foo: 'bar'}, $(data).find("#title").val(), "/");
                        $("title").html($(data).find("#title").val());
                    });

                    google.maps.event.addDomListener(window, 'load', initialize_map_how_get(sessionLat,sessionLon));
                }
            });
        }



        function error_a()
        {
            alert("Невозможно определить местоположение");
            $("#popular_link").click();
            //if ($("#popular_link").is("hidden"))
            location.href = "/popular/?no-position=Y";
            return false;
        }
        function get_location_a()
        {
            //if (navigatorOn != 'Y'||!sessionLat) {
            if (navigatorOn != 'Y') {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(success_a, error_a,options_for_get_position);
                } else {
                    alert("Ваш браузер не поддерживает\nопределение местоположения");
                    return false;
                }
            } else {
                google.maps.event.addDomListener(window, 'load', initialize_map_how_get(sessionLat,sessionLon));
                $(".bubblingG").hide();
                $("#bubbling_text").text("");
                //setTimeout("myMap.setCenter([sessionLat, sessionLon],15)", 500);
            }
        }
        $(function(){
            $(".bubblingG").show();
            $("#bubbling_text").text("Определяем местоположение...");
            <?if($_REQUEST['ajax']=='Y'):?>
            get_location_a();
            <?elseif($_REQUEST['ajax']!='Y'):?>
            loadScript();
            <?endif?>
            $(window).scroll(function(){
                scroll_load_letnie_verandy();
            });

        });
        function scroll_load_letnie_verandy() {
            //if (($(".item").last().offset().top-$(".item").last().height()*5)<$(window).scrollTop()&&!ajax_load)
            if (($(document).height()-$(".item").last().height()*4)<$(window).scrollTop()&&!ajax_load&&max_page>page)
            {
                $(".bubblingG").show();
                $("#overflow").show();
                $(".bubblingG #bubbling_text").text("Загрузка следующей страницы ресторанов...");
                ajax_load = 1;
                $.ajax({
                    type: "POST",
                    url: "/ajax/post-list.php"+location.search,
                    data: {
                        page:++page,
                        PAGEN_1:page,
                        pageRestSort:$("#sort").val()
                    },
                    success: function(data) {
                        $(".bubblingG").hide();
                        $(".bubblingG #bubbling_text").text("");
                        $("#overflow").hide();
                        $(".item").last().after(data);
                        ajax_load = 0;
                        setOverflowD();
                    }
                });
            }
        }

    </script>

    <div class="popup map-canvas-wrapper">
        <div class="close-map-canvas"></div>
        <div id="map-canvas" ></div>
    </div>

<?


    if (!$_REQUEST["page"])
        $_REQUEST["page"] = 1;
    if (!$_REQUEST["PAGEN_1"])
        $_REQUEST["PAGEN_1"] = 1;
if (!$_REQUEST["pageRestSort"])
    $_REQUEST["pageRestSort"] = "distance";
if (!$_REQUEST["by"])
    $_REQUEST["by"] = "asc";
$_REQUEST["set_filter"] = "Y";

    global $arrFilter;

    $APPLICATION->IncludeComponent("restoran:restoraunts.list", "letnie_verandy", Array(
        "main" => "Y",
        "IBLOCK_TYPE" => "special_projects", // Тип информационного блока (используется только для проверки)
        "IBLOCK_INT_ID" => CITY_ID=='msk'?2753:2754, //
        "PARENT_SECTION_CODE" => "", // Код раздела
        "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "10"), // Количество ресторанов на странице
        "SORT_BY1" => ($_REQUEST["pageRestSort"] ? $_REQUEST["pageRestSort"] : "SORT"), // Поле для первой сортировки ресторанов
        "SORT_ORDER1" => "asc", // Направление для первой сортировки ресторанов
        "SORT_BY2" => 'NAME', // Поле для второй сортировки ресторанов
        "SORT_ORDER2" => 'ASC', // Направление для второй сортировки ресторанов
        "FILTER_NAME" => "", // Фильтр
        "PROPERTY_CODE" => array(// Свойства
            0 => "RESTORAN",
            1 => "lat",
            2 => "lon"
        ),
        "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
        "DETAIL_URL" => "", // URL страницы детального посмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        "CACHE_TYPE" => $USER->IsAdmin()?'N':"A",//Y ($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
        "CACHE_TIME" => "36000012", // Время кеширования (сек.)
        "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "N", // Учитывать права доступа
        "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
        "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
        "PAGER_TITLE" => "Рестораны", // Название категорий
        "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
        "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
        "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
        "DISPLAY_DATE" => "Y", // Выводить дату элемента
        "DISPLAY_NAME" => "Y", // Выводить название элемента
        "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
    ), false
    );

?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>