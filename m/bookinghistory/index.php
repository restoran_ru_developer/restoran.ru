<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои бронирования");
?>
<script>
    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=get_location_a';
        document.body.appendChild(script);
    }
    $(function(){
        loadScript();
    })
</script>
<?
//$rest = array();
//$rsBron = CIBlockElement::GetList(
//    Array("DATE_ACTIVE_FROM" => "DESC"),
//    Array(
//        "ACTIVE" => "Y",
//        "IBLOCK_ID" => 105,        
//        "PROPERTY_user" => $USER->GetID(),
//        "PROPERTY_hide" => false,
//        "SECTION_CODE" => CITY_ID,
//    ),
//    false,
//    Array("nTopCount"=>15),
//    Array("ID", "NAME","ACTIVE_FROM", "PROPERTY_rest", "PROPERTY_status", "PROPERTY_stol", "PROPERTY_guest")
//);
//while($arBron = $rsBron->GetNext()) {            
//    if ($arBron["PROPERTY_REST_VALUE"])
//    {
//        $rest[] = $arBron["PROPERTY_REST_VALUE"];        
//    }        
//}
$r = $APPLICATION->get_cookie("MY_MOBILE_BRONS");
$r = json_decode($r,true);           
    if (is_array($r)&&$r[0])
    {        
        global $arrFilter;
        $arrFilter["ID"] = $r;                
        $APPLICATION->IncludeComponent("restoran:restoraunts.list", "history", Array(    
            "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID), // Код информационного блока
            "PARENT_SECTION_CODE" => "", // Код раздела
            "NEWS_COUNT" => 15, // Количество ресторанов на странице
            "SORT_BY1" => "", // Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "", // Направление для первой сортировки ресторанов
            "SORT_BY2" => "", // Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "", // Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arrFilter", // Фильтр
            "PROPERTY_CODE" => array(// Свойства
                0 => "type",
                1 => "kitchen",
                2 => "average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "ratio",
            ),
            "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
            "CACHE_TIME" => "36000010", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N", // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
            "SET_TITLE" => "Y", // Устанавливать заголовок страницы
            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
            "PAGER_TITLE" => "Рестораны", // Название категорий
            "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y", // Выводить дату элемента
            "DISPLAY_NAME" => "Y", // Выводить название элемента
            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                ), false
        );         
    }
    else
        echo '<h3>Вы пока ничего не добавили</h3>';
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>