<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск ресторанов");
?>

<?

if(!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")) {
    $oldDate = explode('-', $_REQUEST['form_date_33']);
    if (count($oldDate) == 3) {
        $newDate = $oldDate[2] . '.' . $oldDate[1] . '.' . $oldDate[0];
        $_REQUEST['form_date_33'] = $newDate;
    }
    $oldDate = explode('-', $_REQUEST['form_date_156']);
    if (count($oldDate) == 3) {
        $newDate = $oldDate[2] . '.' . $oldDate[1] . '.' . $oldDate[0];
        $_REQUEST['form_date_156'] = $newDate;
    }
    $APPLICATION->IncludeComponent(
        (CITY_ID == "rga" || CITY_ID == "urm") ? "bitrix:form.result.new2" : "restoran:form.result.new", "booking", Array(
        "SEF_MODE" => "Y",
        "WEB_FORM_ID" => (CITY_ID == "rga" || CITY_ID == "urm") ? "24" : "3",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "/detail.php?ID=" . ((CITY_ID == "rga" || CITY_ID == "urm") ? $_REQUEST["form_text_164"] : $_REQUEST["form_text_44"]),
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "Y",
        "CACHE_TYPE" => "N",
        "USE_CAPTCHA" => "Y",
        "VARIABLE_ALIASES" => Array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID"
        )
    ), false
    );
}
else {?>

    <link href="//fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>


    <link href="/bitrix/templates/main_2014/css/bootstrap.css"  type="text/css" rel="stylesheet" />
    <link href="/bitrix/templates/main_2014/css/lightbox.css"  type="text/css" rel="stylesheet" />
    <link href="/bitrix/templates/main_2014/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
    <link href="/bitrix/templates/main_2014/css/datepicker.css"  type="text/css" rel="stylesheet" />
<!--    <link href="/bitrix/templates/main_2014/styles.css"  type="text/css" rel="stylesheet" />-->

    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>
    <script src="/bitrix/templates/main_2014/js/bootstrap.js"></script>
    <script src="/bitrix/templates/main_2014/js/bootstrap-datepicker.js"></script>
    <script src="/bitrix/templates/main_2014/js/jquery.autocomplete.min.js"></script>

<!--    <script src="/bitrix/templates/main_2014/js/maskedinput.js"></script>-->

<!--    <script src="/bitrix/templates/main_2014/js/maskedinput.js"></script>-->
<!--    --><?//$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/bootstrap.js')?>
<!--    --><?//$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/bootstrap-datepicker.js')?>
<!--    --><?//$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/maskedinput.js')?>

    <?if (CITY_ID=="ast"):
        ShowError("Услуга временно не работает");
        die;
    endif;?>
    <?
    if(CITY_ID=='msk'||CITY_ID=='spb'){
        $APPLICATION->IncludeComponent(
            "restoran:form.result.new",
            "order_2014",
            Array(
                "SEF_MODE" => "Y",
                "WEB_FORM_ID" => "3",
                "LIST_URL" => "",
                "EDIT_URL" => "",
                "SUCCESS_URL" => "/detail.php?ID=" . ((CITY_ID == "rga" || CITY_ID == "urm") ? $_REQUEST["form_text_164"] : $_REQUEST["form_text_44"]),
                "CHAIN_ITEM_TEXT" => "",
                "CHAIN_ITEM_LINK" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "USE_EXTENDED_ERRORS" => "Y",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "MY_CAPTCHA" => "Y",
                "VARIABLE_ALIASES" => Array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID"
                )
            ),
            false
        );
    }
    if(CITY_ID=='urm'||CITY_ID=='rga'){
        $APPLICATION->IncludeComponent(
            "bitrix:form.result.new2",
            "order_2014",
            Array(
                "SEF_MODE" => "Y",
                "WEB_FORM_ID" => "24",
                "LIST_URL" => "",
                "EDIT_URL" => "",
                "SUCCESS_URL" => "/detail.php?ID=" . ((CITY_ID == "rga" || CITY_ID == "urm") ? $_REQUEST["form_text_164"] : $_REQUEST["form_text_44"]),
                "CHAIN_ITEM_TEXT" => "",
                "CHAIN_ITEM_LINK" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "USE_EXTENDED_ERRORS" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "MY_CAPTCHA" => "Y",
                "VARIABLE_ALIASES" => Array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID"
                )
            ),
            false
        );
    }


//                    $APPLICATION->IncludeComponent(
//                        ($_REQUEST['CITY_ID']=='urm'||$_REQUEST['CITY_ID']=='rga')?"bitrix:form.result.new2":"restoran:form.result.new",
//                        //($USER->IsAdmin())?"ajax_form_bron_rest_sms_new":"ajax_form_bron_rest_sms",
////                            ($_REQUEST["banket"]=="Y")?"banket_2014":"order_2014",
//                        "order_2014",
//                        Array(
//                            "SEF_MODE" => "N",
//                            "WEB_FORM_ID" => "3",
//                            "LIST_URL" => "",
//                            "EDIT_URL" => "",
//                            "SUCCESS_URL" => "",
//                            "CHAIN_ITEM_TEXT" => "",
//                            "CHAIN_ITEM_LINK" => "",
//                            "IGNORE_CUSTOM_TEMPLATE" => "N",
//                            "USE_EXTENDED_ERRORS" => "N",
//                            "CACHE_TYPE" => "N",
//                            "CACHE_TIME" => "3600",
//                            "MY_CAPTCHA" => "Y",
//                            "VARIABLE_ALIASES" => Array(
//                                "WEB_FORM_ID" => "WEB_FORM_ID",
//                                "RESULT_ID" => "RESULT_ID"
//                            )
//                        ),
//                        false
//                    );?>

<?}
?>
<script>
    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=get_location_a';
        document.body.appendChild(script);
    }
    $(function(){
        $(".menu").unbind("click");
        $(".logo").html("<h1>Бронирование</h1><h4>").attr("href","javascript:void(0)").removeClass("ajax");
        //$(".logo h1").css("line-height",$('header').height()+"px");
        $(".app-bar-actions").remove();
        $(".menu").after($(".menu").clone().removeClass("menu").addClass("arrow_r"));//.removeClass("menu").addClass("arrow_r")));                
        $(".arrow_r").find("img").attr("src",$(".arrow_r img").attr("src").replace("menu.png","arrow_r.png"));
        $(".menu").hide();
        //$(".menu").addClass("arrow_r").removeClass("menu");
        <?if(($_REQUEST['ajax']=='Y'&&!preg_match('/detail\.php/',$_SERVER['HTTP_REFERER']))||$_REQUEST['ajax']!='Y'):?>
        $(".arrow_r, .logo").click(function(){
            console.log('history back in booking');
            history.go(-1);
        });
        <?endif?>

        <?if($_REQUEST['ajax']!='Y'):?>
        loadScript();
        <?endif?>
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>