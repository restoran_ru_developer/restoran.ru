<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// start execution
$time_start = microtime_float();

// get rest list
$rsRestList = CIBlockElement::GetList(
    Array("ID"=>"DESC"),
    Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => "11",
        "IBLOCK_SECTION_ID" => 32,
        ">ID" => $curStepID
    ),
    false,
    Array("nPageSize" => 57),// false,
    Array("ID", "NAME", "IBLOCK_ID", "IBLOCK_CODE", "CODE", "DETAIL_PAGE_URL", "PROPERTY_address", "PROPERTY_phone", "PROPERTY_site", "PROPERTY_opening_hours", "PROPERTY_average_bill")
);
while($arRestList = $rsRestList->GetNext()) {
    $curStepID = $arRestList["ID"];
    // check menu
    /*
    $rsMenu = CIBlockSection::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => "151",
            "UF_RESTORAN" => $arRestList["ID"],
            "ELEMENT_SUBSECTIONS" => "Y",
            "CNT_ACTIVE" => "Y",
        ),
        true
    );
    if($arMenu = $rsMenu->GetNext()) {
        // check menu elements
        if(intval($arMenu["ELEMENT_CNT"]) > 0) {
    */
            // replace
            $arRestList["PROPERTY_ADDRESS_VALUE"][0] = str_replace("Москва, ", "", $arRestList["PROPERTY_ADDRESS_VALUE"][0]);
            $arRestList["PROPERTY_ADDRESS_VALUE"][0] = str_replace(Array(",", ";", ":"), "", $arRestList["PROPERTY_ADDRESS_VALUE"][0]);
            $arRestList["PROPERTY_OPENING_HOURS_VALUE"][0] = str_replace(Array(",", ";", ":"), "", $arRestList["PROPERTY_OPENING_HOURS_VALUE"][0]);

            // name
            $str = "\n".$arRestList["NAME"].",";
            // address
            $str .= ($arRestList["PROPERTY_ADDRESS_VALUE"][0] ? $arRestList["PROPERTY_ADDRESS_VALUE"][0] : "-").",";
            // city
            $arIblock = getArIblock("catalog", $arRestList["IBLOCK_CODE"]);
            $str .= $arIblock["NAME"].",";
            // country
            $str .= "Россия,";
            // phone
            $str .= ($arRestList["PROPERTY_PHONE_VALUE"][0] ? $arRestList["PROPERTY_PHONE_VALUE"][0] : "-").",";
            // category
            $str .= "ресторан,";
            // website
            $str .= ($arRestList["PROPERTY_SITE_VALUE"] ? $arRestList["PROPERTY_SITE_VALUE"] : "-").",";
            // hours
            $str .= ($arRestList["PROPERTY_OPENING_HOURS_VALUE"][0] ? $arRestList["PROPERTY_OPENING_HOURS_VALUE"][0] : "-").",";
            // price_level
            if($arRestList["PROPERTY_AVERAGE_BILL_VALUE"][0]) {
                $rsAvBill = CIBlockElement::GetByID($arRestList["PROPERTY_AVERAGE_BILL_VALUE"][0]);
                $arAvBill = $rsAvBill->Fetch();
                $str .= $arAvBill["NAME"].",";
            } else {
                $str .= "-,";
            }
            // reservation_link
            $str .= "http://".SITE_SERVER_NAME.$arRestList["DETAIL_PAGE_URL"]."bron/".",";
            // menu_link
            $str .= "-";

            //file_put_contents("google_upload_full.csv", $str, FILE_APPEND | LOCK_EX);
        //}
    //}
    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;
    if($time > 30) {
        echo "<script>setTimeout(function() {location.href = '/google_upload.php?curStepID=".$curStepID."'}, 5000);</script>";
        break;
    }
}
?>