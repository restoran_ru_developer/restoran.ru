<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<?$APPLICATION->IncludeComponent("bitrix:socialnetwork.messages_users", "users_msg_list", Array(
	"SET_NAVCHAIN" => "N",	// Устанавливать цепочку навигации
	"PATH_TO_USER" => "user.php?user_id=#user_id#",	// Шаблон пути к странице пользователя
	"PATH_TO_MESSAGE_FORM" => "",	// Шаблон пути к странице отправки сообщений
	"PATH_TO_MESSAGES_CHAT" => "/people/chat/?user_id=#user_id#",	// Шаблон пути к странице чата с пользователем
	"PATH_TO_MESSAGES_USERS" => "",	// Шаблон пути к странице переписки
	"PATH_TO_MESSAGES_USERS_MESSAGES" => "user.php?user_id=#user_id#",	// Шаблон пути к переписке с пользователем
	"PAGE_VAR" => "",	// Имя переменной для страницы
	"USER_VAR" => "",	// Имя переменной для пользователя
	"USER_ID" => $USER->GetID(),	// Идентификатор пользователя
	"PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",	// Путь к папке со смайликам относительно корня сайта
	"ITEMS_COUNT" => "20",	// Число записей на странице
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>