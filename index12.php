<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Restoran.ru");
// get restaurants iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
// get news iblock info
$arNewsIB = getArIblock("news", CITY_ID);
$arOnPlateIB = getArIblock("on_plate", CITY_ID);
$arKuponsIB = getArIblock("kupons", CITY_ID);
// get afisha iblock info
$arAfishaIB = getArIblock("afisha", CITY_ID);
// get overviews iblock info
$arReviewsIB = getArIblock("reviews", CITY_ID);
// add script for tab switcher
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/index_page.js");
?>

<div id="news">
    <div class="left">
        <ul class="tabs">
            <li>
                <a class="news_scr" scrCont="news_scrollable" href="#">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=GetMessage("NEWS_TITLE")?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a class="news_scr" scrCont="plate_scrollable" href="#">
                    <div class="left tab_left"></div>
                    <div class="left name" id="msk_on_plate"><?=GetMessage("MOSCOW_ON_PLATE_TITLE")?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a class="news_scr" scrCont="holiday_scrollable" href="#">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=GetMessage("FOR_THE_HOLIDAY_TITLE")?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
        <!-- tab "panes" -->
        <div class="panes">
           <div class="pane newsItem">
               <?
               $arrNoFilter = Array(
                   "!PROPERTY_ON_PLATE_VALUE" => Array("Да"),
                   "!PROPERTY_TO_HOLIDAY_VALUE" => Array("Да"),
               );

               $APPLICATION->IncludeComponent(
               	"bitrix:news.list",
               	"news_main",
               	Array(
               		"DISPLAY_DATE" => "Y",
               		"DISPLAY_NAME" => "Y",
               		"DISPLAY_PICTURE" => "N",
               		"DISPLAY_PREVIEW_TEXT" => "Y",
               		"AJAX_MODE" => "N",
               		"IBLOCK_TYPE" => "news",
               		"IBLOCK_ID" => $arNewsIB["ID"],
               		"NEWS_COUNT" => "1",
               		"SORT_BY1" => "ACTIVE_FROM",
               		"SORT_ORDER1" => "DESC",
               		"SORT_BY2" => "",
               		"SORT_ORDER2" => "",
               		"FILTER_NAME" => "arrNoFilter",
               		"FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
               		"PROPERTY_CODE" => array(),
               		"CHECK_DATES" => "Y",
               		"DETAIL_URL" => "",
               		"PREVIEW_TRUNCATE_LEN" => "500",
               		"ACTIVE_DATE_FORMAT" => "j F Y",
               		"SET_TITLE" => "N",
               		"SET_STATUS_404" => "N",
               		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
               		"ADD_SECTIONS_CHAIN" => "N",
               		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
               		"PARENT_SECTION" => "",
               		"PARENT_SECTION_CODE" => "",
               		"CACHE_TYPE" => "A",
               		"CACHE_TIME" => "36000000",
               		"CACHE_FILTER" => "Y",
               		"CACHE_GROUPS" => "Y",
               		"DISPLAY_TOP_PAGER" => "N",
               		"DISPLAY_BOTTOM_PAGER" => "N",
               		"PAGER_TITLE" => "Новости",
               		"PAGER_SHOW_ALWAYS" => "N",
               		"PAGER_TEMPLATE" => "",
               		"PAGER_DESC_NUMBERING" => "N",
               		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
               		"PAGER_SHOW_ALL" => "N",
               		"AJAX_OPTION_JUMP" => "N",
               		"AJAX_OPTION_STYLE" => "Y",
               		"AJAX_OPTION_HISTORY" => "N"
               	),
               false
               );?>
            </div>
            <div class="pane newsItem">
                <?
                $arrFilterPlate = Array(
                    "PROPERTY_ON_PLATE_VALUE" => Array("Да"),
                );

                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "news_main",
                    Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "on_plate",
                        "IBLOCK_ID" => $arOnPlateIB["ID"],
                        "NEWS_COUNT" => "1",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrFilterPlate",
                        "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                        "PROPERTY_CODE" => array(),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                    ),
                   false
                   );?>
            </div>
            <div class="pane newsItem">
                <?
                $arrFilterHoliday = Array(
                    "PROPERTY_TO_HOLIDAY_VALUE" => Array("Да"),
                );

                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "news_main",
                    Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "news",
                        "IBLOCK_ID" => $arNewsIB["ID"],
                        "NEWS_COUNT" => "1",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrFilterHoliday",
                        "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                        "PROPERTY_CODE" => array(
                            0  => "FORUM_TOPIC_ID",
                            1  => "FORUM_MESSAGE_CNT",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                    ),
                   false
                   );?>
            </div>
        </div>
    </div>
    <?$APPLICATION->IncludeComponent(
    	"bitrix:main.include",
    	"",
    	Array(
    		"AREA_FILE_SHOW" => "sect",
    		"AREA_FILE_SUFFIX" => "inc_right_banner",
    		"AREA_FILE_RECURSIVE" => "Y",
    		"EDIT_TEMPLATE" => ""
    	)
    );?>
    <div class="clear"></div>
</div>
<div class="rest_news_cont" id="news_scrollable">
    <?
    $APPLICATION->IncludeComponent("bitrix:news.list", "main_news_scrollable", Array(
        "DISPLAY_DATE" => "N",	// Выводить дату элемента
        "DISPLAY_NAME" => "Y",	// Выводить название элемента
        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
        "AJAX_MODE" => "N",	// Включить режим AJAX
        "IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => $arNewsIB["ID"],	// Код информационного блока
        "NEWS_COUNT" => "20",	// Количество новостей на странице
        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        "FILTER_NAME" => "",	// Фильтр
        "FIELD_CODE" => Array("DETAIL_PICTURE"),	// Поля
        "PROPERTY_CODE" => "",	// Свойства
        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
        "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
        "PARENT_SECTION" => "",	// ID раздела
        "PARENT_SECTION_CODE" => "",	// Код раздела
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
        "PAGER_TITLE" => "Новости",	// Название категорий
        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
        "PAGER_TEMPLATE" => "",	// Название шаблона
        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        ),
        false
    );?>
</div>
<div class="rest_news_cont" id="plate_scrollable" style="display: none;">
    <?
    $APPLICATION->IncludeComponent(
    	"bitrix:news.list",
    	"plate_scrollable",
    	Array(
    		"DISPLAY_DATE" => "N",
    		"DISPLAY_NAME" => "Y",
    		"DISPLAY_PICTURE" => "Y",
    		"DISPLAY_PREVIEW_TEXT" => "Y",
    		"AJAX_MODE" => "N",
    		"IBLOCK_TYPE" => "on_plate",
    		"IBLOCK_ID" => $arOnPlateIB["ID"],
    		"NEWS_COUNT" => "20",
    		"SORT_BY1" => "ACTIVE_FROM",
    		"SORT_ORDER1" => "DESC",
    		"SORT_BY2" => "SORT",
    		"SORT_ORDER2" => "ASC",
    		"FILTER_NAME" => "",
    		"FIELD_CODE" => array("DETAIL_PICTURE"),
    		"PROPERTY_CODE" => array("ratio", "reviews"),
    		"CHECK_DATES" => "Y",
    		"DETAIL_URL" => "",
    		"PREVIEW_TRUNCATE_LEN" => "100",
    		"ACTIVE_DATE_FORMAT" => "d.m.Y",
    		"SET_TITLE" => "N",
    		"SET_STATUS_404" => "N",
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    		"ADD_SECTIONS_CHAIN" => "N",
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
    		"PARENT_SECTION" => "",
    		"PARENT_SECTION_CODE" => "",
    		"CACHE_TYPE" => "A",
    		"CACHE_TIME" => "36000000",
    		"CACHE_FILTER" => "Y",
    		"CACHE_GROUPS" => "Y",
    		"DISPLAY_TOP_PAGER" => "N",
    		"DISPLAY_BOTTOM_PAGER" => "N",
    		"PAGER_TITLE" => "Новости",
    		"PAGER_SHOW_ALWAYS" => "N",
    		"PAGER_TEMPLATE" => "",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    		"PAGER_SHOW_ALL" => "N",
    		"AJAX_OPTION_JUMP" => "N",
    		"AJAX_OPTION_STYLE" => "Y",
    		"AJAX_OPTION_HISTORY" => "N"
    	),
    false
    );?>
</div>
<div class="rest_news_cont" id="holiday_scrollable" style="display: none;">
    <?
    global $arrFilterPlate;
    $arrFilterPlate = Array(
        "PROPERTY_TO_HOLIDAY_VALUE" => Array("Да"),
    );

    $APPLICATION->IncludeComponent(
    	"bitrix:news.list",
    	"plate_scrollable",
    	Array(
    		"DISPLAY_DATE" => "N",
    		"DISPLAY_NAME" => "Y",
    		"DISPLAY_PICTURE" => "Y",
    		"DISPLAY_PREVIEW_TEXT" => "Y",
    		"AJAX_MODE" => "N",
    		"IBLOCK_TYPE" => "news",
    		"IBLOCK_ID" => $arNewsIB["ID"],
    		"NEWS_COUNT" => "20",
    		"SORT_BY1" => "ACTIVE_FROM",
    		"SORT_ORDER1" => "DESC",
    		"SORT_BY2" => "SORT",
    		"SORT_ORDER2" => "ASC",
    		"FILTER_NAME" => "arrFilterPlate",
    		"FIELD_CODE" => array("DETAIL_PICTURE"),
    		"PROPERTY_CODE" => array("ratio", "reviews"),
    		"CHECK_DATES" => "Y",
    		"DETAIL_URL" => "",
    		"PREVIEW_TRUNCATE_LEN" => "100",
    		"ACTIVE_DATE_FORMAT" => "d.m.Y",
    		"SET_TITLE" => "N",
    		"SET_STATUS_404" => "N",
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    		"ADD_SECTIONS_CHAIN" => "N",
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
    		"PARENT_SECTION" => "",
    		"PARENT_SECTION_CODE" => "",
    		"CACHE_TYPE" => "A",
    		"CACHE_TIME" => "36000000",
    		"CACHE_FILTER" => "Y",
    		"CACHE_GROUPS" => "Y",
    		"DISPLAY_TOP_PAGER" => "N",
    		"DISPLAY_BOTTOM_PAGER" => "N",
    		"PAGER_TITLE" => "Новости",
    		"PAGER_SHOW_ALWAYS" => "N",
    		"PAGER_TEMPLATE" => "",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    		"PAGER_SHOW_ALL" => "N",
    		"AJAX_OPTION_JUMP" => "N",
    		"AJAX_OPTION_STYLE" => "Y",
    		"AJAX_OPTION_HISTORY" => "N"
    	),
    false
    );?>
</div>

<div id="content">
    <div class="left">
        <div id="tabs_block2">
            <ul class="tabs" ajax="ajax" ajax_url="/tpl/ajax/get_index_block.php?<?=bitrix_sessid_get()?>&block=">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("NEW_RESTAURANTS_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="video">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("VIDEO_NEWS_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <!-- tab "panes" -->
            <div class="panes">
               <div class="pane">
                   <?$APPLICATION->IncludeComponent(
                   	"restoran:catalog.list",
                   	"new_rest_main",
                   	Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arRestIB["ID"],
                            "NEWS_COUNT" => "3",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "",
                            "FIELD_CODE" => array("CREATED_BY"),
                            "PROPERTY_CODE" => array("ratio","reviews_quantity"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                   	),
                   false
                   );?>
                   <div class="clear"></div>
                </div>
                <div class="pane"></div>
            </div>
        </div>
        <div id="tabs_block3">
            <ul class="tabs" ajax="ajax" ajax_url="/tpl/ajax/get_index_block.php?<?=bitrix_sessid_get()?>&block=">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("STOCK_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <!--<li>
                    <a href="reviews">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("REVIEWS_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>-->
                <li>
                    <a href="master">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("MASTER_CLASSES_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="interview">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("INTERVIEW_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane">
                  <? $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "kupons",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "kupons",
                                "IBLOCK_ID" => $arKuponsIB["ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterPlate",
                                "FIELD_CODE" => array("ACTIVE_TO"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS","subway"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "100",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );?>
                </div>
                <!--<div class="pane"></div>-->
                <div class="pane"></div>
                <div class="pane"></div>
            </div>
        </div>
        <div id="tabs_block4">
            <ul class="tabs" ajax="ajax" ajax_url="/tpl/ajax/get_index_block.php?<?=bitrix_sessid_get()?>&block=">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("AFISHA_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li><a href="photoreport">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("PHOTOREPORTS_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane">
                   <div id="tabs_block5">
                       <?$APPLICATION->IncludeComponent(
                       	"bitrix:news.list",
                       	"afisha_list_main",
                       	Array(
                       		"DISPLAY_DATE" => "N",
                       		"DISPLAY_NAME" => "Y",
                       		"DISPLAY_PICTURE" => "Y",
                       		"DISPLAY_PREVIEW_TEXT" => "Y",
                       		"AJAX_MODE" => "N",
                       		"IBLOCK_TYPE" => "afisha",
                       		"IBLOCK_ID" => $arAfishaIB["ID"],
                       		"NEWS_COUNT" => "5",
                       		"SORT_BY1" => "ACTIVE_FROM",
                       		"SORT_ORDER1" => "DESC",
                       		"SORT_BY2" => "SORT",
                       		"SORT_ORDER2" => "ASC",
                       		"FILTER_NAME" => "",
                       		"FIELD_CODE" => array(),
                       		"PROPERTY_CODE" => array("EVENT_TYPE"),
                       		"CHECK_DATES" => "N",
                       		"DETAIL_URL" => "",
                       		"PREVIEW_TRUNCATE_LEN" => "200",
                       		"ACTIVE_DATE_FORMAT" => "j F Y",
                       		"SET_TITLE" => "N",
                       		"SET_STATUS_404" => "N",
                       		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                       		"ADD_SECTIONS_CHAIN" => "N",
                       		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
                       		"PARENT_SECTION" => "",
                       		"PARENT_SECTION_CODE" => "",
                       		"CACHE_TYPE" => "A",
                       		"CACHE_TIME" => "36000000",
                       		"CACHE_FILTER" => "N",
                       		"CACHE_GROUPS" => "Y",
                       		"DISPLAY_TOP_PAGER" => "N",
                       		"DISPLAY_BOTTOM_PAGER" => "N",
                       		"PAGER_TITLE" => "Новости",
                       		"PAGER_SHOW_ALWAYS" => "N",
                       		"PAGER_TEMPLATE" => "",
                       		"PAGER_DESC_NUMBERING" => "N",
                       		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                       		"PAGER_SHOW_ALL" => "N",
                       		"AJAX_OPTION_JUMP" => "N",
                       		"AJAX_OPTION_STYLE" => "Y",
                       		"AJAX_OPTION_HISTORY" => "N"
                       	),
                       false
                       );?>
                   </div>
                </div>
                <div class="pane"></div>
            </div>
        </div>
        <br />
        <div id="tabs_block6">
            <ul class="tabs"  ajax="ajax" ajax_url="/tpl/ajax/get_index_block.php?<?=bitrix_sessid_get()?>&block=">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("BLOGS_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li><a href="comments">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("COMMENTS1_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li><a href="cookery">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("COOKERY_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <!-- tab "panes" -->
            <div class="panes">
               <div class="pane">
                   <?$APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "article_main",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "blogs",
                        "IBLOCK_ID" => 103,
                        "NEWS_COUNT" => 3,
                        "SORT_BY1" => "ID",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("ratio"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "search_rest_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?>
                    <?/*$APPLICATION->IncludeComponent(
                                "restoran:article.list",
                                "blogs_post_main",
                                Array(
                                                        "IBLOCK_TYPE" => array(
                                                                0 => "blogs",
                                                                1 => "master_classes",
                                                                2 => "interview",
                                                                3 => "photoreports",
                                                        ),
                                        "IBLOCK_ID" => array(
                                                                0 => "54",
                                                                1 => "53",
                                                                2 => "51",
                                                                3 => "55",
                                                                4 => "40"
                                                        ),
                                        "SECTION_ID" => "",
                                        "SECTION_CODE" => "",
                                                        "SORT_FIELD" => "DATE_CREATE",
                                                        "SORT_BY" => "DESC",
                                                        "PAGE_COUNT" => 3,
                                        "SECTION_URL" => "/content/blog/detail.php?SECTION_ID=#ID#",
                                                        "PICTURE_WIDTH" => 232,
                                                        "PICTURE_HEIGHT" => 127,
                                                        "DESCRIPTION_TRUNCATE_LEN" => 200,
                                        "COUNT_ELEMENTS" => "N",
                                        "TOP_DEPTH" => "2",
                                        "SECTION_FIELDS" => "",
                                        "SECTION_USER_FIELDS" => Array("UF_SECTION_BIND", "UF_SECTION_COMM_CNT"),
                                        "ADD_SECTIONS_CHAIN" => "Y",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_NOTES" => "",
                                        "CACHE_GROUPS" => "Y",            
                                                        "PAGER_SHOW_ALWAYS" => "Y",
                                                        "PAGER_TEMPLATE" => "kupon_list",	
                                                        "PAGER_DESC_NUMBERING" => "N",	
                                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                ),
                        false
                        );*/?> 
                   <div class="clear"></div>
                   <div align="right"><a class="uppercase" href="#">ВСЕ БЛОГИ</a></div>
                </div>
                <div class="pane"></div>
                <div class="pane"></div>
            </div>
        </div>
        <div id="tabs_block7">
            <ul class="tabs">
                <li>
                    <a href="reviews">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("REVIEWS_TITLE")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane">
                  <?$arOverviewsIB = getArIblock("overviews", CITY_ID);
                     $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "article_main",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "overviews",
                        "IBLOCK_ID" => $arOverviewsIB["ID"],
                        "NEWS_COUNT" => 3,
                        "SORT_BY1" => "ID",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("ratio"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "search_rest_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?>
                </div>
            </div>
        </div>
        <div class="baner">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "bottom_content_main_page",
            		"NOINDEX" => "N",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
            );?>
        </div>
    </div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "page",
            "AREA_FILE_SUFFIX" => "inc_right_block",
            "EDIT_TEMPLATE" => ""
        ),
    false
    );?>
    <div class="clear"></div>
    <h2><?=GetMessage("TO_USER_TITLE")?></h2>
    <p class="columns">
        <?$APPLICATION->IncludeFile(
            "/index_inc.php",
            Array(),
            Array("MODE"=>"html")
        );?>
    </p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>