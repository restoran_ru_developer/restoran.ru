<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по карте Ресторан.ру");

if ($_GET["a"] != "u") {


if ($_REQUEST['arrFilter_pf']) $zoomer = 11;
else $zoomer = 14;
?>

    <style>
        .YMaps-slider
        {
            top:125px!important;
        }
    </style>
    <link href="/tpl/css/map.css" rel="stylesheet">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
        <?$some['arrFilter_pf'] = $_REQUEST['arrFilter_pf'];?>


        var myMap,
        objectManager;

        function init() {
		var zoomer = <? echo $zoomer; ?>;

        var coord = "<?=COption::GetOptionString("main",CITY_ID."_center")?>";
        var u_pos = coord.split(',');
        myMap = new ymaps.Map("map",
			{
				center: [u_pos[1], u_pos[0]],
				zoom: zoomer,
				controls: ["geolocationControl", "zoomControl", "rulerControl"],
				type: "yandex#map"

	        },
			{
				minZoom: 10,
				maxZoom: 18,
			}
		);
        myMap.behaviors.disable('scrollZoom');

        navigator.geolocation.getCurrentPosition(foundLocation, noLocation);



		objectManager = new ymaps.ObjectManager({
			clusterize: true,
		    clusterHasBalloon: true,
		    geoObjectOpenBalloonOnClick: true,
		});

		objectManager.objects.options.set({
        	iconLayout: 'default#image',
			iconImageHref: '/tpl/images/map/pin1-2.png',
			iconImageSize: [37, 47],
        	iconImageOffset: [-18, -46],
            hideIconOnBalloonOpen: false,
            openEmptyBalloon: false
    	});

		//objectManager.clusters.options.set({
		//	preset: 'islands#blueCircleIcon'
		//});

        objectManager.clusters.options.set({

            clusterIcons: [{
            href: '/tpl/images/map/pin1-4.png',
            size: [43, 43],
            offset: [-21, -21]

            }]
        });

		myMap.geoObjects.add(objectManager);


        objectManager.objects.events.add('click', function (e) {

            var objectId = e.get('objectId'),
            object = objectManager.objects.getById(objectId);
            var blnContent = object.properties.balloonContent;

            /* пробуем назначать ID из базы
            $("#blnTemp").html(blnContent);
            var restId = $("#blnTemp").children(".mbs").data("aid");
            console.log (restId);
            */

            restId = objectId;

            if (blnContent.length) {
                objectManager.objects.balloon.open(objectId);
                return;
            }

            $.ajax({
                method: "GET",
                dataType: "json",
                url: "/tpl/ajax/map_get_rest_info.php?ID="+restId
            }).done(function(data) {
                //console.log (data);
                $("#blnProto").children(".mb_r_title").children("a").attr("href", data.detail);
                $("#blnProto").children(".mb_r_title").children("a").html(data.type+" "+data.name);

                if (data.bill == null) data.bill = "нет информации";
                if (data.open == null) data.open = "нет информации";
                if (data.phone == null) data.phone = "нет информации";
                if (data.address == null) data.address = "нет информации";
                if (data.pic == null) data.pic = "/tpl/images/no_photo_map.png";

                $("#blnProto").children(".mb_r_body").children(".mb_r_pic").children("a").attr("href", data.detail);
                $("#blnProto").children(".mb_r_body").children(".mb_r_pic").children("a").children("img").attr("src", data.pic);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = bill]").children("span").html(data.bill);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = open]").children("span").html(data.open);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = phone]").children("span").find('a').html(data.phone);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = address]").children("span").html(data.address);


                if (restId != 2222385894) {
                    console.log (object);
                    objectManager.objects.setObjectProperties( objectId, {balloonContent: $("#blnProto").html()} );
                    objectManager.objects.balloon.open(objectId);

                }
            });





		});

		objectManager.objects.events.add('mouseenter', function (e) {

            var objectId = e.get('objectId');
            objectManager.objects.setObjectOptions(objectId, {
                iconImageHref: '/tpl/images/map/pin1-3.png'
            });

		});

		objectManager.objects.events.add('mouseleave', function (e) {
            var objectId = e.get('objectId');
            objectManager.objects.setObjectOptions(objectId, {
                iconImageHref: '/tpl/images/map/pin1-2.png'
            });
		});

		myMap.events.add('boundschange', function () {
			//console.log (myMap.getZoom());
            if (myMap.getZoom() > 16) {
                objectManager.options.set({
                    clusterize: false
                });
            } else {
                objectManager.options.set({
                    clusterize: true
                });
            }
		});

		getRests();
    }

	ymaps.ready(init);



    function foundLocation (position) {
        // повесим посетителя

        var ulat = position.coords.latitude;
        var ulon = position.coords.longitude;

        console.log (ulat+" "+ulon);
		UserObject = new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [ulat, ulon]
            },
            properties: {
                hintContent: "Вы здесь"
            }
        }, {
            iconLayout: 'default#image',
			iconImageHref: '/tpl/images/map/pin1-1.png',
			iconImageSize: [101, 78],
        	iconImageOffset: [-50, -53]

        });

        myMap.geoObjects.add(UserObject);
        //myMap.setCenter ([ulat, ulon], 14);

    }

    function noLocation () {
        return false;
    }

    function getRests () {
		var bounds = myMap.getBounds ();
		var bounds_list = bounds[0][0]+","+bounds[0][1]+","+bounds[1][0]+","+bounds[1][1];
		$.ajax({
	        url: "/tpl/ajax/map_load_all_markers_new.php?CITY_ID=<?=CITY_ID?>&<?=http_build_query($some)?>&<?=bitrix_sessid_get()?>"
	    }).done(function(data) {
			//objectManager.removeAll();
	    	if (data) objectManager.add(data);
			//showAirports_2();
	    });
	}

    </script>




<div id="map" style="width:100%;margin:0 auto;height:800px; top:-146px; margin-bottom: -128px; position: relative"></div>
<div style="display:none;">
    <div id="blnProto">
        <div class="mb_r_title"><a target="_blank"></a></div>
        <div class="mb_r_body">
            <div class="mb_r_pic"><a target="_blank"><img width="137" height="90" /></a></div>
            <div class="mb_r_ank">
                <div data-type="bill">СРЕДНИЙ СЧЕТ:&nbsp;<span></span></div>
                <div data-type="open">ОТКРЫТ:&nbsp;<span></span></div>
                <div data-type="phone">ТЕЛЕФОН:&nbsp;<span><a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php" class="booking"></a></span></div>
                <div data-type="address">АДРЕС:&nbsp;<span></span></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?

}
else {
?>
    <style>
        .YMaps-slider
        {
            top:125px!important;
        }


    </style>
    <!--<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==" type="text/javascript"></script>-->
    <script src="https://api-maps.yandex.ru/1.1/index.xml?key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript"></script>
    <script type="text/javascript">
        <?$some['arrFilter_pf'] = $_REQUEST['arrFilter_pf'];?>
        var map = null;
        var map_markers = {};
        var markers_data = {};
        var map_filter = {};
        var hidden_marker = null;
        var sat_map_type = false;
        var map_type = 'default';
        var to_show = null;
        var cur_user = 0;
        var requestMarkersUrl = "/tpl/ajax/map_load_all_markers.php?CITY_ID=<?=CITY_ID?>&<?=http_build_query($some)?>&<?=bitrix_sessid_get()?>";
        var realrequestMarkersUrl = "";
        var my_style = {};
        var my_json = "";
        var coord = "<?=COption::GetOptionString("main",CITY_ID."_center")?>";

        window.onload = OnPageLoad;
        function OnPageLoad (){
            GetMap();

            coord = coord.split(",");
            <?
            if ($_REQUEST["near"]=="Y"):?>
            if (!get_location_a())
            {
                SetMapCenter ( coord[1], coord[0], 16 );
                <?if(!$_REQUEST['arrFilter_pf']):?>
                UpdateMarkers();
                <?endif?>
            }
            <?else:?>
            SetMapCenter ( coord[1], coord[0], 16 );
            UpdateMarkers();
            <?endif;?>
        }


        var map_fil = '';
        function GetMap ()
        {
            YMaps.jQuery ( function () {
                map = new YMaps.Map ( YMaps.jQuery ( "#YMapsID" )[0] );
                var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
                var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
                var typeControl = new YMaps.TypeControl ( [] );
                typeControl.addType ( MTmap );
                typeControl.addType ( MTsat );

                map.setMinZoom (9);
                map.addControl (typeControl);
                map.addControl(new YMaps.Zoom());

                YMaps.Events.observe ( map, map.Events.Update, function () {
                    //UpdateMarkers ();
                } );
                YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                    UpdateMarkers ();
                } );
                YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                    hideMyBalloon ();
                } );
                YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                    hideMyBalloon ();
                } );
                /*YMaps.Events.observe ( map, map.Events.Click, function () {
                 if(map.getZoom()<=16)
                 hideMyBalloon ();
                 });*/

            })
            my_style = new YMaps.Style();
            my_style.iconStyle = new YMaps.IconStyle();
            my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
            my_style.iconStyle.size = new YMaps.Point(27, 32);
            my_style.iconStyle.offset = new YMaps.Point(-15, -32);

            marker_style = new YMaps.Style();
            marker_style.iconStyle = new YMaps.IconStyle();
            marker_style.iconStyle.href = "/tpl/images/map/marker.png";
            marker_style.iconStyle.size = new YMaps.Point(50, 81);
            marker_style.iconStyle.offset = new YMaps.Point(-8, -42);
        }

        function SetMapCenter ( lat, lng, zoom_i )
        {
            zoom = zoom_i ? zoom_i : 16;
            map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
        }

        function UpdateMarkers()
        {
            hideMyBalloon();
            var bounds = map.getBounds ();
            map_filter['yl'] = bounds.getLeft ();
            map_filter['yr'] = bounds.getRight ();
            map_filter['xr'] = bounds.getTop ();
            map_filter['xl'] = bounds.getBottom ();
            if(map_fil)
                realrequestMarkersUrl = requestMarkersUrl+"&"+map_fil;
            else
                realrequestMarkersUrl = requestMarkersUrl;
            console.log(realrequestMarkersUrl,'realrequestMarkersUrl');
            console.log(map_filter,'map_filter');
            var request = jQuery.get ( realrequestMarkersUrl, map_filter, function ( data ) {

                if (my_json&&map_fil)
                {
                    for ( var i in map_markers )
                    {
                        map.removeOverlay ( map_markers[i] );
                        delete ( map_markers[i] );
                        delete ( markers_data[i] );
                    }
                }
                if ((data && !my_json)||(my_json&&map_fil))
                {
                    my_json = data;

                    console.log(my_json.length,'my_json.length')

                    var points = [];

                    for ( var i in data )
                    {
                        if ( i != 'length' && typeof ( map_markers[data[i].id] ) == 'undefined' )
                        {
                            <?if($_REQUEST['arrFilter_pf']):?>
                            if(data[i].lon&&data[i].lat&&data[i].lon!=undefined&&data[i].lat!=undefined&&i<10){
                                points.push(new YMaps.GeoPoint(data[i].lon, data[i].lat));
                            }
                            <?endif?>

                            map_markers[data[i].id] = new YMaps.Placemark ( new YMaps.GeoPoint ( data[i].lon, data[i].lat ), { style: my_style, hasBalloon: false, db_id: data[i].id,hideIcon: false } );
                            //map_markers[data[i].id].setIconContent(data[i].name+"<br />"+data[i].adres);
                            //map_markers[data[i].id].name = data[i].name;
                            //map_markers[data[i].id].adres = data[i].adres;
                            map.addOverlay ( map_markers[data[i].id] );

                            map_markers[data[i].id].id = data[i].id;
                            markers_data[data[i].id] = data[i];
                            //if(map.getZoom()>16)
                            // ShowMyBalloon(data[i].id);
                            YMaps.Events.observe ( map_markers[data[i].id], map_markers[data[i].id].Events.Click, function ( m, e ) {
                                hideMyBalloon();
                                ShowMyBalloon(m.id);
                            } );
                        }
                    }

                    // Создаем область показа по группе точек
                    <?if($_REQUEST['arrFilter_pf']):?>
                        console.log(points,'points');
                        var bounds = new YMaps.GeoCollectionBounds(points);
                        // Применяем область показа к карте
    //                    console.log(bounds instanceof YMaps.GeoBounds,'jfjdhfjhdj'); // Должно выдать true

                        map.setBounds(bounds);
                    <?endif?>
                }
                else
                {
                    if (map_fil)
                        $("#how_much").html("<div style='margin-top:45px'>Ничего не найдено</div>");
                }
            }, 'json' );
            request.fail(function(jqXHR, textStatus) {
                console.log( "Request failed: " + textStatus );
            });

        }

        function ShowMyBalloon (id)
        {
            var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
            //jQuery("#YMapsID").append("<div id='baloon"+id+"' class='map_ballon'><a href='"+markers_data[id].url+"' title='Перейти к ресторану' alt='Перейти к ресторану'><div class='balloon_content'>"+markers_data[id].name+"<br />"+markers_data[id].adres+"</div><div class='balloon_tail'> </div></a></div>");
            jQuery("#YMapsID").append("<div id='baloon"+id+"' class='map_ballon'><a target='_blank' href='"+markers_data[id].url+"' title='Перейти к ресторану' alt='Перейти к ресторану'><div class='balloon_content'>"+markers_data[id].name+"</div><div class='balloon_tail'> </div></a></div>");
            jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
            jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
        }

        function hideMyBalloon ()
        {
            jQuery(".map_ballon").remove();
        }
        function success_a(position) {
            var new_point = new YMaps.GeoPoint(position.coords.latitude,position.coords.longitude);
            var coord = "<?=COption::GetOptionString("main",CITY_ID."_center")?>";
            coord = coord.split(",");
            var c_point = new YMaps.GeoPoint(coord[1],coord[0]);
            var hhh = c_point.distance(new_point);
            var marker = new YMaps.Placemark ( new YMaps.GeoPoint ( position.coords.longitude, position.coords.latitude ), { style: marker_style, hasBalloon: false, db_id: "",hideIcon: false } );
            map.addOverlay ( marker );
            //console.log(hhh);
            if(hhh>600000)
            {
                if (!$("#city_modal").size())
                {
                    $("<div class='popup popup_modal' id='city_modal'></div>").appendTo("body");
                    $('#city_modal').load('/tpl/ajax/city_select.php?<?=bitrix_sessid_get()?>', function(data) {
                        setCenter($("#city_modal"));
                        showOverflow();
                        $("#city_modal").fadeIn("300");
                        $('.dont-show').hide();
                    });
                }

            }
            SetMapCenter ( position.coords.latitude, position.coords.longitude);
            //map.panTo(new YMaps.GeoPoint(position.coords.latitude,position.coords.longitude), {flying:1});
            UpdateMarkers();
            hideMyBalloon ();
        }
        function error_a()
        {
            alert("Невозможно определить местоположение");
            SetMapCenter (  coord[1], coord[0], 16 );
            UpdateMarkers();
            return false;
        }
        function get_location_a()
        {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                SetMapCenter (  coord[1], coord[0], 16 );
                UpdateMarkers();
                return false;
            }
        }
        function go_adres(x,y){
            need_coord = new YMaps.GeoPoint(x,y);
            map.panTo(need_coord, {flying: 1});
            console.log(map_markers);
//        return false;
        }



    </script>
    <div id="YMapsID" style="width:100%;margin:0 auto;height:1005px; top:-146px; margin-bottom: -128px; position: relative"></div>


    <? } ?>
<div id="map-page" style="display: none">http://<?=$_SERVER["HTTP_HOST"].$APPLICATION->GetCurPage(false)?></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>