<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по карте Ресторан.ру");
?>
<!--<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==" type="text/javascript"></script>-->
<script src="http://api-maps.yandex.ru/1.1/index.xml?key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript"></script>
<script type="text/javascript">
    function GetMap ()
    {
        YMaps.jQuery ( function () {
            map = new YMaps.Map ( YMaps.jQuery ( "#YMapsID" )[0] );
            var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
            var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
            var typeControl = new YMaps.TypeControl ( [] );
            typeControl.addType ( MTmap );
            typeControl.addType ( MTsat );

            map.setMinZoom (14);
            map.addControl (typeControl);
            map.addControl(new YMaps.Zoom());
            
            YMaps.Events.observe ( map, map.Events.Update, function () {
                //UpdateMarkers ();
            } );
            YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                UpdateMarkers ();
            } );
            YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                hideMyBalloon ();
            } );
             YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                hideMyBalloon ();
            } );
            /*YMaps.Events.observe ( map, map.Events.Click, function () {
                if(map.getZoom()<=16)
                    hideMyBalloon ();
            });*/
            
        })
        my_style = new YMaps.Style();
        my_style.iconStyle = new YMaps.IconStyle();
        my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
        my_style.iconStyle.size = new YMaps.Point(27, 32);
        my_style.iconStyle.offset = new YMaps.Point(-15, -32);
 
        marker_style = new YMaps.Style();
        marker_style.iconStyle = new YMaps.IconStyle();
        marker_style.iconStyle.href = "/tpl/images/map/marker.png";
        marker_style.iconStyle.size = new YMaps.Point(50, 81);
        marker_style.iconStyle.offset = new YMaps.Point(-8, -42);
    }

    function SetMapCenter ( lat, lng, zoom_i )
    {
        zoom = zoom_i ? zoom_i : 16;
        map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
    }

    function UpdateMarkers()
    {
        hideMyBalloon();
        var bounds = map.getBounds ();
        map_filter['yl'] = bounds.getLeft ();
        map_filter['yr'] = bounds.getRight ();
        map_filter['xr'] = bounds.getTop ();
        map_filter['xl'] = bounds.getBottom ();
        if(map_fil)
            realrequestMarkersUrl = requestMarkersUrl+"&"+map_fil;
        else
            realrequestMarkersUrl = requestMarkersUrl;
        jQuery.get ( realrequestMarkersUrl, map_filter, function ( data ) {
            for ( var i in map_markers )
            {
                map.removeOverlay ( map_markers[i] );
                delete ( map_markers[i] );
                delete ( markers_data[i] );
            }
            if (data)
            {
                if (map_fil)
                    $("#how_much").html("<span>"+data.length+"</span><br />"+pluralForm(data.length,"ресторан","ресторана","ресторанов"));
               
                for ( var i in data )
                {
                    if ( i != 'length' && typeof ( map_markers[data[i].id] ) == 'undefined' )
                    {          
                        map_markers[data[i].id] = new YMaps.Placemark ( new YMaps.GeoPoint ( data[i].lon, data[i].lat ), { style: my_style, hasBalloon: false, db_id: data[i].id,hideIcon: false } );
                        //map_markers[data[i].id].setIconContent(data[i].name+"<br />"+data[i].adres);
                        //map_markers[data[i].id].name = data[i].name;
                        //map_markers[data[i].id].adres = data[i].adres;
                        map.addOverlay ( map_markers[data[i].id] );

                        map_markers[data[i].id].id = data[i].id;
                        markers_data[data[i].id] = data[i];
                        //if(map.getZoom()>16)
                            ShowMyBalloon(data[i].id);
                        /*YMaps.Events.observe ( map_markers[data[i].id], map_markers[data[i].id].Events.Click, function ( m, e ) {
                            hideMyBalloon();
                            ShowMyBalloon(m.id);
                        } );*/
                    }
                }
            }
            else
            {
                if (map_fil)
                 $("#how_much").html("<div style='margin-top:45px'>Ничего не найдено</div>");
            }
        }, 'json' );
    }
    
    function ShowMyBalloon (id)
    {
        var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
        //jQuery("#YMapsID").append("<div id='baloon"+id+"' class='map_ballon'><a href='"+markers_data[id].url+"' title='Перейти к ресторану' alt='Перейти к ресторану'><div class='balloon_content'>"+markers_data[id].name+"<br />"+markers_data[id].adres+"</div><div class='balloon_tail'> </div></a></div>");
        jQuery("#YMapsID").append("<div id='baloon"+id+"' class='map_ballon'><a href='"+markers_data[id].url+"' title='Перейти к ресторану' alt='Перейти к ресторану'><div class='balloon_content'>"+markers_data[id].name+"</div><div class='balloon_tail'> </div></a></div>");
        jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
        jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
    }
    
    function hideMyBalloon ()
    {
        jQuery(".map_ballon").remove();
    }
    function success_a(position) {
            var new_point = new YMaps.GeoPoint(position.coords.latitude,position.coords.longitude);
            var coord = "<?=COption::GetOptionString("main",CITY_ID."_center")?>";
            coord = coord.split(",");
            var c_point = new YMaps.GeoPoint(coord[1],coord[0]);
            var hhh = c_point.distance(new_point);
            var marker = new YMaps.Placemark ( new YMaps.GeoPoint ( position.coords.longitude, position.coords.latitude ), { style: marker_style, hasBalloon: false, db_id: "",hideIcon: false } );
            map.addOverlay ( marker );
            //console.log(hhh);
            if(hhh>600000)
            {
                if (!$("#city_modal").size())
                {
                    $("<div class='popup popup_modal' id='city_modal'></div>").appendTo("body");
                    $('#city_modal').load('/tpl/ajax/city_select.php?<?=bitrix_sessid_get()?>', function(data) {
                        setCenter($("#city_modal"));
                        showOverflow();
                        $("#city_modal").fadeIn("300");
                    });
                }
                
            }
            SetMapCenter ( position.coords.latitude, position.coords.longitude );
            //map.panTo(new YMaps.GeoPoint(position.coords.latitude,position.coords.longitude), {flying:1});
            UpdateMarkers();                                                 
        }
        function error_a()
        {
            alert("Невозможно определить местоположение");
            return false;
        }
        function get_location_a()
        {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }                            
        }
</script>
<script type="text/javascript">
    var map = null;
    var map_markers = {};
    var markers_data = {};
    var map_filter = {};
    var hidden_marker = null;
    var sat_map_type = false;
    var map_type = 'default';
    var to_show = null;
    var cur_user = 0;
    var requestMarkersUrl = "/tpl/ajax/map_load_markers.php?<?=bitrix_sessid_get()?>";
    var realrequestMarkersUrl = "";
    var my_style = {};
    
    window.onload = OnPageLoad;
    function OnPageLoad (){
        GetMap();
        var coord = "<?=COption::GetOptionString("main",CITY_ID."_center")?>";
        coord = coord.split(",");
        <?if ($_REQUEST["near"]=="Y"):?>
            if (!get_location_a())
            {
                SetMapCenter ( coord[1], coord[0], 16 );
                //UpdateMarkers();
            }                
        <?else:?>
            SetMapCenter ( coord[1], coord[0], 16 );
            //UpdateMarkers();
        <?endif;?>
    }
</script>
    <div id="YMapsID" style="width:100%;margin:0 auto;height:1005px; top:-142px; margin-bottom: -172px; position: relative"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>