<?
if(!$_REQUEST["q"])
    return;
?>
<?
$arInterviewIB = getArIblock("interview", CITY_ID);
?>
<?
$obCache = new CPHPCache; 
// время кеширования - 60 минут * 24
$life_time = 60*60*24; 
$cache_id = $q."kupons"; 

if($obCache->InitCache($life_time, $cache_id, "/")):
    $vars = $obCache->GetVars();
    $SEARCH_RESULT = $vars["SEARCH_RESULT"];
else :
    CModule::IncludeModule("iblock");
    $q = ToLower($q);
    $lang = get_lang($q);
    if ($lang=="ru")
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"interview","IBLOCK_ID"=>$arInterviewIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."\n";
        }
        $q = decode2anotherlang_ru($q);
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"interview","IBLOCK_ID"=>$arInterviewIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."\n";
        }
    }
    elseif ($lang=="en")
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"interview","IBLOCK_ID"=>$arInterviewIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."\n";
        }
        $q = decode2anotherlang_en($q);
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"interview","IBLOCK_ID"=>$arInterviewIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."\n";
        }
    }   
    else
    {
        $res = CIblockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"interview","IBLOCK_ID"=>$arInterviewIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>5));
        while($ar_res = $res->GetNext())
        {
            $SEARCH_RESULT .= $ar_res["NAME"]."###".$ar_res["DETAIL_PAGE_URL"]."\n";
        }
    }
    
endif;

if($obCache->StartDataCache()):      
    $obCache->EndDataCache(array(
        "SEARCH_RESULT"    => $SEARCH_RESULT
        )); 
endif;
 echo $SEARCH_RESULT; 
?>
<?
if (!$SEARCH_RESULT)
{
    $APPLICATION->IncludeComponent(
        "bitrix:search.page",
        "suggest",
        Array(
            "USE_SUGGEST" => "N",
            "AJAX_MODE" => "N",
            "RESTART" => "Y",
            "NO_WORD_LOGIC" => "N",
            "USE_LANGUAGE_GUESS" => "Y",
            "CHECK_DATES" => "Y",
            "USE_TITLE_RANK" => "Y",
            "DEFAULT_SORT" => "date",
            "FILTER_NAME" => "",
            "SHOW_WHERE" => "N",
            "SHOW_WHEN" => "N",
            "PAGE_RESULT_COUNT" => "10",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "360000",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Результаты поиска",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "arrFILTER" => array("iblock_interview"),
            "arrFILTER_iblock_interview" => array($arInterviewIB["ID"]),
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
        ),
    false
    );
}
?>