<?
if(!$_REQUEST["q"])
    return;

$arRestNewsIB = getArIblock("firms", CITY_ID);

?>

<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "suggest",
    Array(
        "USE_SUGGEST" => "N",
        "AJAX_MODE" => "N",
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "USE_LANGUAGE_GUESS" => "Y",
        "CHECK_DATES" => "Y",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "date",
        "FILTER_NAME" => "",
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "10",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "86402",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "search_rest_list",
        "arrFILTER" => array("iblock_firms"),
        "arrFILTER_iblock_firms" => array($arRestNewsIB["ID"]),
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
    false
);?>