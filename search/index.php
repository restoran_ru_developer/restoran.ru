<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расширенный поиcк Ресторан.ру");
$arRestIB = getArIblock("catalog", CITY_ID);
$arKuponIB = getArIblock("kupons", CITY_ID);
$arOverviewsIB = getArIblock("overviews", CITY_ID);
$arInterviewIB = getArIblock("interview", CITY_ID);
$arPhotoreportsIB = getArIblock("photoreports", CITY_ID);
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arNewsIB = getArIblock("news", CITY_ID);
$arVideoIB = getArIblock("videonews", CITY_ID);
$arReviewsIB = getArIblock("reviews", CITY_ID);
$arFirmsIB = getArIblock("firms", CITY_ID);
$arBlogIB = getArIblock("blogs", CITY_ID);
$temp = explode("[",$_REQUEST["q"]);
$_REQUEST["q"] = $temp[0];
?>
<div class="block">
    <div class="left-side">
        <style>
            .search_a_check {
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 5px;
                padding: 5px;
                font-size: 12px;
                text-decoration: none;
                font-family: PT Sans;
                padding: 0px 10px;
                margin-left: 15px;
            }
            .search_a_sug {
                font-family: PT Sans;
                font-size:18px;
            }
        </style>

        <?
        if ($_REQUEST["q"]&&strlen($_REQUEST["q"])>4)
        {
            $_REQUEST["q"] = str_replace("рестораны", "ресторан", $_REQUEST["q"]);
            $_REQUEST["q"] = str_replace(" у ", " ", $_REQUEST["q"]);
            $_REQUEST["q"] = str_replace(" на ", " ", $_REQUEST["q"]);
            $_REQUEST["q"] = str_replace(" в ", " ", $_REQUEST["q"]);
            $_REQUEST["q"] = str_replace(" к ", " ", $_REQUEST["q"]);
            $_REQUEST["q"] = str_replace(" над ", " ", $_REQUEST["q"]);
            $q = "";
            $er = 0;
            $temp = explode(" ",$_REQUEST["q"]);
            foreach ($temp as &$word)
            {
                $url = "http://speller.yandex.net/services/spellservice.json/checkText?text=".rawurlencode($word);
                $f = file_get_contents($url);
                $j = json_decode($f,true);
                if (is_array($j[0])&&$j[0]["s"][0]&&$j[0]["code"]!=3)
                {
                    $word = $j[0]["s"][0];
                    $er++;
                }
            }
            if ($er)
            {
                $q = implode(" ",$temp);
            }
            if ($er&&!$_REQUEST["ch_q"])
            {
                $u = $APPLICATION->GetCurPageParam("q=".$q,Array("q","ch_q","nocheck"));
                echo '<p class="font14"><font class="notetext">Возможно, Вы имели в виду: </font><a class="search_a_sug" href="'.$u.'">'.$q.'</a></p> ';
            }
        }
        if ($_REQUEST["ch_q"])
        {
            $u = $APPLICATION->GetCurPageParam("q=".$_REQUEST["q"]."&nocheck=1",Array("q","ch_q"));
            echo '<p class="font14"><font class="notetext">Показаны результаты для: </font> '.$q.' <a class="search_a_check" href="'.$u.'">Отменить</a></p>';
            $_REQUEST["q"] = $q;

        }

        ?>


        <?
        switch($_GET["search_in"]) {
            case 'all':
                include('all.php');
            break;
            case 'rest':
                include('rest.php');
            break;
            case 'dostavka':
                include('dostavka.php');
            break;
            case 'kupons':
                include('kupons.php');
            break;
            case 'afisha':
                include('afisha.php');
            break;
            case 'news':
                include('news.php');
            break;    
            case 'newss':
                include('newss.php');
            break;    
            case 'recepts':
                include('recept.php');
            break;
            case 'recipe':
                include('recipe.php');
            break;
            case 'firms':
                include('firms.php');
            break;
            case 'firms_news':
                include('firms_news.php');
            break;
            case 'blogs':
                include('blogs.php');
            break;
            case 'blog':
                include('blog.php');
            break;
            case 'overviews':
                include('overviews.php');
            break;
            case 'interview':
                include('interview.php');
            break;
            case 'reviews':
                include('reviews.php');
            break;
            case 'master_classes':
                include('master_classes.php');
            break;
            default:
                include('rest.php');
            break;
        }

        global $search;
        if (!$search&&strlen($q)>0&&!$_REQUEST["nocheck"]&&!$_REQUEST["ch_q"])
        {
            $u = $APPLICATION->GetCurPageParam("q=".$_REQUEST["q"]."&ch_q=".$q,Array("q"));
            LocalRedirect($u);
            //echo "fff";
        }
        ?>
    </div>
    <div class="right-side">        
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                "TYPE" => "right_2_main_page",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
            ),
            false
        );?>                    
        <div class="title">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arPFilter["!PREVIEW_PICTURE"] = false;
            $arPFilter["!PROPERTY_restoran_ratio"] = false;            
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "rand",
                    "SORT_ORDER1" => "",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "recomended",
                    "REST_PROPS"=>"Y"
                ),
            false
            );?>
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        <div class="title">Популярные</div>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "show_counter",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "rand",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "popular",
                    "REST_PROPS"=>"Y"
                ),
            false
            );?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_3_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
    </div>
    <div class="clearfix"></div>
    <noindex>
        <div id="yandex_direct">
            <script type="text/javascript">
                //<![CDATA[
                yandex_partner_id = 47434;
                yandex_site_bg_color = 'FFFFFF';
                yandex_site_charset = 'utf-8';
                yandex_ad_format = 'direct';
                yandex_font_size = 1;
                yandex_direct_type = 'horizontal';
                yandex_direct_limit = 4;
                yandex_direct_title_color = '24A6CF';
                yandex_direct_url_color = '24A6CF';
                yandex_direct_all_color = '24A6CF';
                yandex_direct_text_color = '000000';
                yandex_direct_hover_color = '1A1A1A';
                document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                //]]>
            </script>
        </div>
    </noindex>
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
    false
    );?>      
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>