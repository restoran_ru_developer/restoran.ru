window.Loading = Loading = (function ($) {
  var
  img = new Image,
  Self = null,
  Inits = 0,
  IsTrack = false,
  Track = function(e) {
    Self.css({
      'top'  : (e.clientY + 10) + 'px',
      'left' : (e.clientX + 25) + 'px'
    });
  };
  img.src = 'images/core/ajax-loader.gif';
  
  return {
    init : function(boolTrack, initialE) {
      Inits++;
      if (Inits == 1) {
        Self = $('<img />').attr('src', img.src).attr('style', 'left: 50%; margin: -6px 0 0 -6px; z-index: 1003; top: 50%; position: fixed;')
        $('body').append(Self);
      }
      if (boolTrack && !IsTrack) {
        IsTrack = true
        if (initialE && initialE.clientY & initialE.clientX)
          Track(initialE);
        $(document).bind('mousemove', Track);
      }
    },
    stop : function() {
      Inits--;
      if (Inits == 0) {
        Self.remove();
        if (IsTrack) {
          $(document).unbind('mousemove', Track);
          IsTrack = false;
        }
      }
    },
    insert : function(container) {      
      $('<img />').attr('src', img.src).attr('style', 'left: 50%; margin: -6px 0 0 -6px; top: 50%; position: relative;').attr('class', '_inserted_loading_image').appendTo(container);
    },
    remove : function(container) {
      $('._inserted_loading_image', container).remove();
    },
    getSrc : function() {
      return img.src;
    }
  };
})(jQuery);
window.translate = function(word, path, params) {
  if(!path && !word) {
    return false;
  }
  if(!path) {
    path = 'default';
  }
  var translation = word;
  if (window.TRANSLATIONS && TRANSLATIONS[path] && TRANSLATIONS[path][word]) {
    translation = TRANSLATIONS[path][word];
    if (params) {
      $.each(params, function(key, value){
        translation = translation.replace('%' + key + '%', value);
      })
    }
  } else {
    if(!window.TRANSLATIONS) {
      window.TRANSLATIONS = {};
    }
    if(!TRANSLATIONS[path]) {
      TRANSLATIONS[path] = {}
    }    
    TRANSLATIONS[path][word] = word;
    $.post('json.php?type=translate', {
      path : path,
      word : word
    });
  }
  return translation;
};
window.reloadPage = function() {
  var href = document.location.href.replace(document.location.hash, ''); 
  var r = (new RegExp("[\\?&]_=([^&]*)")).exec(href);
  if(r && r[1]) {
    href.replace('_=' + href, '_=' + (new Date()).getTime());
  } else {
    if(href.indexOf('?')!=-1) {
      href += '&';
    } else {
      if(href.substr(href.length-1) != '/') {
        href+= '/';
      }
      href += '?';     
    }
    href += '_=' + (new Date()).getTime() 
  }
  document.location.href = href + document.location.hash;
};
window.getDialogHtml = function(o) {
  if(!o) {
    o = {};
  }
  var h = $('<div class="dialog' + (o['class']? ' ' + o['class'] : '') + '"><div class="wrapper"><div class="content"></div><div class="clear"></div></div></div>');
  var c = h.find('.content');
  if(o.title) {
    c.append('<h2>' + o.title + '</h2>');
  }  
  if(o.message) {
    c.append('<div class="description">' + o.message + '</div>');
  }  
  if(o.content) {
    c.append(o.content);
  }   
  if(!o.buttons) {
    o.buttons = [{
        'class' : 'cancel',
        'click' : function(){
          $(this).closest('.dialog').dialog('close');
        },
        'text' : translate('button_close', 'base')
      }]
  }
  var b, i;
  for (i = 0; i < o.buttons.length; i++) {
    b = $('<button class="main-button"></button>');
    if(o.buttons[i]['class']) {
      b.addClass(o.buttons[i]['class']);
    }
    if(o.buttons[i].text) {
      b.append(o.buttons[i].text);
    }
    if(o.buttons[i].click) {
      b.click(o.buttons[i].click);
    }
    c.append(b);
  }
  return h;
}