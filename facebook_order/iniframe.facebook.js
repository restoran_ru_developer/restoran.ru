;
(function( window, undefined){
 
  var LastHeight = 0, LastPostedHeight = 0, c = document.getElementById('wrapper');
  try {
    if(window.FB && window.FB.Canvas  && window.FB.Canvas.setSize) {   
      FB.Canvas.scrollTo(0,0);          
      document.body.style.overflow = 'hidden';
      setInterval(function(){
        LastHeight = c.offsetHeight;      
        if(LastPostedHeight != LastHeight) {
          LastPostedHeight = LastHeight;
          FB.Canvas.setSize({
            height: LastPostedHeight
          });
        }
      }, 50);
 
    }
  } catch(e) {

  }
})(this);