#!/usr/bin/php -q
<?
set_time_limit(0);
$_SERVER["DOCUMENT_ROOT"] = "/var/www/restoran.cakelabs.ru";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>

<?
if(!CModule::IncludeModule("form"))
    return;

$rsFormResult = CFormResult::GetList(
    3,
    $by = "s_timestamp",
    $order = "desc",
    array(
        "STATUS_ID" => 5 // столик забронирован
    ),
    $is_filtered,
    "N",
    false
);
while($arFormResult = $rsFormResult->Fetch()) {
    $arAnswer = CFormResult::GetDataByID(
        $arFormResult["ID"],
        array("SIMPLE_QUESTION_977", "SIMPLE_QUESTION_536", "SIMPLE_QUESTION_371", "SIMPLE_QUESTION_482"),
        $arResult,
        $arAnswer2
    );
    // calculate date diff
    $startdatetime = strtotime($arAnswer["SIMPLE_QUESTION_977"][0]["USER_TEXT"] . " " . str_replace(Array("-", " ", "/"), ":", $arAnswer["SIMPLE_QUESTION_536"][0]["USER_TEXT"]));
    $enddatetime = strtotime(date("d.m.Y G:i:s"));
    $difference = $startdatetime - $enddatetime;
    $hours = $difference / 3600;
    $minutes = ($hours - floor($hours)) * 60;
    //$final_hours = round($hours,0);
    $final_minutes = round($minutes);

    // check
    if($final_minutes <= 30 && CModule::IncludeModule("sozdavatel.sms")) {
        $message = "У вас забронирован столик в ".$arAnswer["SIMPLE_QUESTION_371"][0]["USER_TEXT"]." на ".$arAnswer["SIMPLE_QUESTION_536"][0]["USER_TEXT"];
        CSMS::Send($message, $arAnswer["SIMPLE_QUESTION_482"][0]["USER_TEXT"], "UTF-8");
    }
}
?>