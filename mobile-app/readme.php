<?php
/**
 * Created by PhpStorm.
 * User: konstantingreat
 * Date: 29.12.15
 * Time: 17:56
 */


/*
 *
 *
 * работает на основе https://github.com/fguillot/JsonRPC
 *
 *
 * */

 /**read me**/
 /**
 -> метод getPropsCode() используется для получения списка возможных свойств (их символьного кода)

 -> метод getPropValues($CODE, $CITY_CODE = 'msk') используется для получения ID свойств, которые используются для фильтрации ресторанов: $CODE - код запрашиваемого св-ва; $CITY_CODE - код города (msk,spb,rga,urm)

 -> метод getRestaurantList($CITY_CODE = 'msk', $SORT_BY = 'distance', $SORT_ORDER = 'ASC', $SORT_BY2 = '', $SORT_ORDER2 = '', $COUNT = 1, $PAGE = '',
 $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = false,
 $KITCHEN = array(), $AVERAGE_BILL = array(), $SUBWAY = array(), $FEATURES = array(),$PROPOSALS_FEATURES = array(), $REST_GROUP = array())

 $SORT_BY - поле для первой сортировки, если используется значение distance, и не указаны координаты пользователя, будут использованы координаты центра города;
 возможные значения: NAME, PROPERTY_rating_date(рейтинг), PROPERTY_stat_day(Просмотров за день)
 $SORT_ORDER - направление сортировки (ASC,DESC)
 $SORT_BY2 - поле для второй сортировки
 SORT_ORDER2 - направление второй сортировки (ASC,DESC)
 $COUNT - количество на странице
 $PAGE - страница, оставлять пустой, если требуется только первая
 $USER_LATITUDE - широта
 $USER_LONGITUDE - долгота
 $RADIUS - радиус в метрах
 $KITCHEN - массив id типа кухни
 $AVERAGE_BILL - массив id типа серднего счета
 $SUBWAY - массив id метро
 $FEATURES - массив id особенностей
 $PROPOSALS_FEATURES - массив id предложений
 $REST_GROUP - массив id ресторанных групп

 -> метод getDetailRestaurant($ID) детальная информация по ресторану, $ID - id ресторана

 -> метод getRestaurantReviews($ID,$COUNT = 5) отзывы ресторана

 -> метод getRestaurantMenu($ID,$COUNT = 5) меню ресторана

 -> метод sendRestaurantReview($REST_ID, $USER_ID, $RATING, $COMMENT) отправить отзыв в ресторан

 -> метод bBronRestaurant($restoran_id, $date, $time, $guets, $name, $phone, $wish) сделать бронь, $time - время например в таком формате 12:00

 -> метод searchRestByTitle($queryStr, $CITY_CODE='spb') - поиск ресторанов по строке


  **/
/**read me**/