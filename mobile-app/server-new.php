<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
include('JsonRPC/Server.php');
include('JsonRPC/ResponseException.php');
include('JsonRPC/AccessDeniedException.php');

use JsonRPC\Server;
use JsonRPC\AuthenticationFailure;

class Api
{
//    public function beforeProcedure($username, $password, $class, $method)
//    {
//        if ($login_condition_failed) {
//            throw new AuthenticationFailure('Wrong credentials!');
//        }
//    }
    public function addition($a, $b)
    {
//        $str = array(1,3,4);
////        return $a + $b;
//        return json_encode($str, true);
        return $a + $b;
    }



}
/**read me**/
    /**
        -> метод getPropsCode() используется для получения списка возможных свойств (их символьного кода)

        -> метод getPropValues($CODE, $CITY_CODE = 'msk') используется для получения ID свойств, которые используются для фильтрации ресторанов: $CODE - код запрашиваемого св-ва; $CITY_CODE - код города (msk,spb,rga,urm)

        -> метод getRestaurantList($CITY_CODE = 'msk', $SORT_BY = 'distance', $SORT_ORDER = 'ASC', $SORT_BY2 = '', $SORT_ORDER2 = '', $COUNT = 1, $PAGE = '',
                    $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = false,
                        $KITCHEN = array(), $AVERAGE_BILL = array(), $SUBWAY = array(), $FEATURES = array(),$PROPOSALS_FEATURES = array(), $REST_GROUP = array())

                $SORT_BY - поле для первой сортировки, если используется значение distance, и не указаны координаты пользователя, будут использованы координаты центра города;
                    возможные значения: NAME, PROPERTY_rating_date(рейтинг), PROPERTY_stat_day(Просмотров за день)
                $SORT_ORDER - направление сортировки (ASC,DESC)
                $SORT_BY2 - поле для второй сортировки
                SORT_ORDER2 - направление второй сортировки (ASC,DESC)
                $COUNT - количество на странице
                $PAGE - страница, оставлять пустой, если требуется только первая
                $USER_LATITUDE - широта
                $USER_LONGITUDE - долгота
                $RADIUS - радиус в метрах
                $KITCHEN - массив id типа кухни
                $AVERAGE_BILL - массив id типа серднего счета
                $SUBWAY - массив id метро
                $FEATURES - массив id особенностей
                $PROPOSALS_FEATURES - массив id предложений
                $REST_GROUP - массив id ресторанных групп

        -> метод getDetailRestaurant($ID) детальная информация по ресторану, $ID - id ресторана

        -> метод getRestaurantReviews($ID,$COUNT = 5) отзывы ресторана

        -> метод getRestaurantMenu($ID,$COUNT = 5) меню ресторана

        -> метод sendRestaurantReview($REST_ID, $USER_ID, $RATING, $COMMENT) отправить отзыв в ресторан

        -> метод bBronRestaurant($restoran_id, $date, $time, $guets, $name, $phone, $wish) сделать бронь, $time - время например в таком формате 12:00

        -> метод searchRestByTitle($queryStr, $CITY_CODE='spb') - поиск ресторанов по строке


    **/
/**read me**/

//$server->attach(new Api);
//
//echo $server->execute();




//if($USER->IsAdmin()):
$server = new Server;
//    $server->authentication(array('admin' => '12345'));

// Register the before callback
//$server->before('beforeProcedure');


$server->attach(new Api);
//
echo $server->execute();


//endif;







//$a = new Api;
////$some = $a->getRestaurantList('msk','NAME','ASC',5,1,false,false,false,array(),array(),array(),array(),array(444604));
////$some = $a->getRestaurantList();
////$some = $a->getDetailRestaurant(1962116);
////$some = $a->getRestaurantMenu(1962116);
////$some = $a->getRestaurantReviews(1962116);
////$some = $a->searchRestByTitle('нев');
//
//
//
////$some = $a->sendRestaurantReview(388337, '28.12.15', '14:00', 2, 'Тест', '9523969381', 'test');
////$some = $a->bBronRestaurant(2173912, '28.12.15', '14:00', 2, 'Тест', '9523969381', 'test');
////$some = $a->getUserByPhone(9523969381);
//$some = $a->getPropsCode();
////$some = $a->getPropValues('average_bill','urm');

//var_dump($some);
?>