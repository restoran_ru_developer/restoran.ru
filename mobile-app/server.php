<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
include('JsonRPC/Server.php');
include('JsonRPC/ResponseException.php');
include('JsonRPC/AccessDeniedException.php');

use JsonRPC\Server;
use JsonRPC\AuthenticationFailure;

class Api
{
//    public function beforeProcedure($username, $password, $class, $method)
//    {
//        if ($login_condition_failed) {
//            throw new AuthenticationFailure('Wrong credentials!');
//        }
//    }
//    public function addition($a, $b)
//    {
//        $str = array(1,3,4);
////        return $a + $b;
//        return json_encode($str);
//    }

    //$SORT_BY2 = 'distance', $SORT_ORDER2 = 'ASC',
    public function getRestaurantList(
        $CITY_CODE = 'msk', $SORT_BY = 'distance', $SORT_ORDER = 'ASC', $SORT_BY2 = '', $SORT_ORDER2 = '', $COUNT = 1, $PAGE = '',
          $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = false,
              $KITCHEN = array(), $AVERAGE_BILL = array(), $SUBWAY = array(), $FEATURES = array(),$PROPOSALS_FEATURES = array(), $REST_GROUP = array(), $DISTRICT = array(), $OUT_OF_CITY = array(),
                  $KEY_VALUE_FILTER = array()
    )
    {
        // $SORT_BY: PROPERTY_rating_date; PROPERTY_stat_day;

        global $APPLICATION;
        global $arrFilter;
        global $USER;

//        $RADIUS = 1000;

        //FirePHP::getInstance()->info($KEY_VALUE_FILTER,'$KEY_VALUE_FILTER');

        if(!$KEY_VALUE_FILTER){
            if($KITCHEN){
                $arrFilter['PROPERTY_kitchen'] = $KITCHEN;
            }
            if($AVERAGE_BILL){
                $arrFilter['PROPERTY_average_bill'] = $AVERAGE_BILL;
            }
            if($SUBWAY){
                $arrFilter['PROPERTY_subway'] = $SUBWAY;
            }
            if($FEATURES){
                $arrFilter['PROPERTY_features'] = $FEATURES;
            }
            if($PROPOSALS_FEATURES){
                $arrFilter['PROPERTY_proposals'] = $PROPOSALS_FEATURES;
            }
            if($REST_GROUP){
                $arrFilter['PROPERTY_rest_group'] = $REST_GROUP;
            }
            if($DISTRICT){
                $arrFilter['PROPERTY_area'] = $DISTRICT;
            }
            if($OUT_OF_CITY){
                $arrFilter['PROPERTY_out_city'] = $OUT_OF_CITY;
            }

            if(in_array(1921,$FEATURES)&&($CITY_CODE=='msk'||$CITY_CODE=='spb')){
                $result = $this->getRestaurantListAnyIBlock($CITY_CODE, $SORT_BY, $SORT_ORDER, $COUNT, $PAGE,
                    $USER_LATITUDE, $USER_LONGITUDE, $RADIUS);
                $arrFilter['ID'] = $result['RestListIds'];
                if(($key = array_search(1921, $arrFilter['PROPERTY_features'])) !== false) {
                    if(!$key){
                        unset($arrFilter['PROPERTY_features']);
                    }
                    else {
                        unset($arrFilter['PROPERTY_features'][$key]);
                    }
                }
                //  FirePHP::getInstance()->info($arrFilter['ID'],'ID');

                $SORT_BY = 'IDS';

            }
            else {
                if($SORT_BY=='NAME'&&$SORT_ORDER=='ASC'&&!$USER_LONGITUDE&&!$RADIUS&&!$KITCHEN&&!$AVERAGE_BILL&&!$SUBWAY&&!$FEATURES&&!$PROPOSALS_FEATURES&&!$REST_GROUP&&!$DISTRICT&&!$OUT_OF_CITY){
                    $NEED_ADVERT = true;
                }

                //  //  $SORT possible values: NAME, PROPERTY_rating_date(рейтинг), PROPERTY_stat_day(Просмотров за день)
                if($SORT_BY=='distance'){
                    $_REQUEST['lat'] = $USER_LATITUDE;
                    $_REQUEST['lon'] = $USER_LONGITUDE;
                    if(!$USER_LATITUDE || !$USER_LONGITUDE){
                        $coords = COption::GetOptionString("main", $CITY_CODE . "_center");
                        $coords_arr = explode(',',$coords);
                        $_REQUEST['lat'] = $coords_arr[1];
                        $_REQUEST['lon'] = $coords_arr[0];
                    }
                }

                $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                $arrFilter["!PROPERTY_NETWORK_REST_VALUE"] = "Да";  //  ресторан сети
            }
        }
        else {
            foreach ($KEY_VALUE_FILTER as $prop_key=>$prop_value) {
                if ($prop_key=="d_tours"){
                    $arrFilter["!PROPERTY_".$prop_key] = false;
                }
                else {
                    $arrFilter["PROPERTY_$prop_key".($prop_value=='Y'?'_VALUE':'')] = $prop_value;
                }
            }

            if(in_array(1921,$KEY_VALUE_FILTER)&&($CITY_CODE=='msk'||$CITY_CODE=='spb')){   // летние веранды
                $result = $this->getRestaurantListAnyIBlock($CITY_CODE, $SORT_BY, $SORT_ORDER, $COUNT, $PAGE,
                    $USER_LATITUDE, $USER_LONGITUDE, $RADIUS);
                $arrFilter['ID'] = $result['RestListIds'];
                if(($key = array_search(1921, $arrFilter['PROPERTY_features'])) !== false) {
                    if(!$key){
                        unset($arrFilter['PROPERTY_features']);
                    }
                    else {
                        unset($arrFilter['PROPERTY_features'][$key]);
                    }
                }
                $SORT_BY = 'IDS';

            }
            else {
                if($SORT_BY=='NAME'&&$SORT_ORDER=='ASC'&&!$USER_LONGITUDE&&!$RADIUS&&!$KITCHEN&&!$AVERAGE_BILL&&!$SUBWAY&&!$FEATURES&&!$PROPOSALS_FEATURES&&!$REST_GROUP&&!$DISTRICT&&!$OUT_OF_CITY){
                    $NEED_ADVERT = true;
                }

                //  //  $SORT possible values: NAME, PROPERTY_rating_date(рейтинг), PROPERTY_stat_day(Просмотров за день)
                if($SORT_BY=='distance'){
                    $_REQUEST['lat'] = $USER_LATITUDE;
                    $_REQUEST['lon'] = $USER_LONGITUDE;
                    if(!$USER_LATITUDE || !$USER_LONGITUDE){
                        $coords = COption::GetOptionString("main", $CITY_CODE . "_center");
                        $coords_arr = explode(',',$coords);
                        $_REQUEST['lat'] = $coords_arr[1];
                        $_REQUEST['lon'] = $coords_arr[0];
                    }
                }

                $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                $arrFilter["!PROPERTY_NETWORK_REST_VALUE"] = "Да";  //  ресторан сети
            }
        }


        FirePHP::getInstance()->info($arrFilter,'$arrFilter');


        $_REQUEST['PAGEN'] = $PAGE;

        $items = $APPLICATION->IncludeComponent("mobile-app:restoraunts.list", "", Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $CITY_CODE,
            "PARENT_SECTION_CODE" => "restaurants", // Код раздела
            "NEWS_COUNT" => $COUNT,
            "SORT_BY1" => $SORT_BY,
            "SORT_ORDER1" => $SORT_ORDER,
            "SORT_BY2" => $SORT_BY2,
            "SORT_ORDER2" => $SORT_ORDER2,
            "FILTER_NAME" => "arrFilter",//
            "PROPERTY_CODE" => array(
                1 => "kitchen",
                2 => "average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "lat",
                8 => "lon",
                9 => "opening_hours_google",
//                9 => "area",
//                10 => "out_city",
            ),
            "CHECK_DATES" => "N", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
            "CACHE_TIME" => "36000079", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N", // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
            "SET_TITLE" => "N", // Устанавливать заголовок страницы
            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => $PAGE?"Y":'N', // Выводить под списком
            "PAGER_TITLE" => "Рестораны", // Название категорий
            "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y", // Выводить дату элемента
            "DISPLAY_NAME" => "Y", // Выводить название элемента
            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N", // Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
            'NEED_ADVERT' => $NEED_ADVERT,
            'DISTANCE_VALUE' => $RADIUS
        ), false
        );

        if($result['NavPageCount']&&$result['NavPageCount']!==false){
            $items['NavPageCount'] = $result['NavPageCount'];
        }
        return json_encode($items);
    }


    public function getRestaurantListAnyIBlock($CITY_CODE = 'msk', $SORT_BY = 'distance', $SORT_ORDER = 'ASC', $COUNT = 1, $PAGE = '',
                                               $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = false){   //  Спецпроект летние веранды
        global $USER,$APPLICATION,$RestListIds,$arrFilterAny;

        if($SORT_BY=='distance'){
            $_REQUEST['lat'] = $USER_LATITUDE;
            $_REQUEST['lon'] = $USER_LONGITUDE;
            if(!$USER_LATITUDE || !$USER_LONGITUDE){
                $coords = COption::GetOptionString("main", $CITY_CODE . "_center");
                $coords_arr = explode(',',$coords);
                $_REQUEST['lat'] = $coords_arr[1];
                $_REQUEST['lon'] = $coords_arr[0];
            }
        }

        $_REQUEST['PAGEN'] = $PAGE ? $PAGE : 1;

        $APPLICATION->IncludeComponent(
            "mobile-app:restaurant.list_any_iblock",
            "",
            Array(
                'IBLOCK_TYPE'=>'special_projects',
                "IBLOCK_ID" => $CITY_CODE=='msk'?222:223,
                "NEWS_COUNT" => $COUNT, // Количество ресторанов на странице
                "SORT_BY1" => $SORT_BY, // Поле для первой сортировки ресторанов
                "SORT_ORDER1" => $SORT_ORDER, // Направление для первой сортировки ресторанов
                "FILTER_NAME" => "arrFilterAny",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "36000010",
                'FIELD_CODE' => array(),
                'PROPERTY_CODE' => array('RESTORAN'),
                'PARENT_SECTION_CODE' => 'articles',
                'DISTANCE_VALUE' => $RADIUS,
                "DISPLAY_BOTTOM_PAGER" => $PAGE?"Y":'N',
                "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
            ),
            false
        );
        return $RestListIds;
    }

    public function getRestaurantListOnMap($CITY_CODE = 'msk', $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = 1000, $KITCHEN = array(), $AVERAGE_BILL = array(), $SUBWAY = array(), $FEATURES = array(),$PROPOSALS_FEATURES = array(), $REST_GROUP = array(), $DISTRICT = array(), $OUT_OF_CITY = array(), $KEY_VALUE_FILTER = array())//
    {
        /**одно отличие от метода getRestaurantList() - отсутвие пагинации и nTopCount**/

        // $SORT_BY: PROPERTY_rating_date; PROPERTY_stat_day;

        global $APPLICATION;
        global $arrFilter;
        global $USER;


        if(!$KEY_VALUE_FILTER){
            if($KITCHEN){
                $arrFilter['PROPERTY_kitchen'] = $KITCHEN;
            }
            if($AVERAGE_BILL){
                $arrFilter['PROPERTY_average_bill'] = $AVERAGE_BILL;
            }
            if($SUBWAY){
                $arrFilter['PROPERTY_subway'] = $SUBWAY;
            }
            if($FEATURES){
                $arrFilter['PROPERTY_features'] = $FEATURES;
            }
            if($PROPOSALS_FEATURES){
                $arrFilter['PROPERTY_proposals'] = $PROPOSALS_FEATURES;
            }
            if($REST_GROUP){
                $arrFilter['PROPERTY_rest_group'] = $REST_GROUP;
            }
            if($DISTRICT){
                $arrFilter['PROPERTY_area'] = $DISTRICT;
            }
            if($OUT_OF_CITY){
                $arrFilter['PROPERTY_out_city'] = $OUT_OF_CITY;
            }


            if(in_array(1921,$FEATURES)&&($CITY_CODE=='msk'||$CITY_CODE=='spb')){
                $result = $this->getRestaurantListAnyIBlock($CITY_CODE, 'distance', 'asc', false, false,
                    $USER_LATITUDE, $USER_LONGITUDE, $RADIUS);
                $arrFilter['ID'] = $result['RestListIds'];
                FirePHP::getInstance()->info($arrFilter['ID']);

                if(($key = array_search(1921, $arrFilter['PROPERTY_features'])) !== false) {
                    if(!$key){
                        unset($arrFilter['PROPERTY_features']);
                    }
                    else {
                        unset($arrFilter['PROPERTY_features'][$key]);
                    }
                }
                $SORT_BY = 'IDS';
            }
            else {
                $_REQUEST['lat'] = $USER_LATITUDE;
                $_REQUEST['lon'] = $USER_LONGITUDE;
                if(!$USER_LATITUDE || !$USER_LONGITUDE){
                    $coords = COption::GetOptionString("main", $CITY_CODE . "_center");
                    $coords_arr = explode(',',$coords);
                    $_REQUEST['lat'] = $coords_arr[1];
                    $_REQUEST['lon'] = $coords_arr[0];
                }

                $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                $arrFilter["!PROPERTY_NETWORK_REST_VALUE"] = "Да";  //  ресторан сети
            }
        }
        else {
            foreach ($KEY_VALUE_FILTER as $prop_key=>$prop_value) {
                if ($prop_key=="d_tours"){
                    $arrFilter["!PROPERTY_".$prop_key] = false;
                }
                else {
                    $arrFilter["PROPERTY_$prop_key".($prop_value=='Y'?'_VALUE':'')] = $prop_value;
                }
            }

            if(in_array(1921,$KEY_VALUE_FILTER)&&($CITY_CODE=='msk'||$CITY_CODE=='spb')){   // летние веранды
                $result = $this->getRestaurantListAnyIBlock($CITY_CODE, 'distance', 'asc', false, false,
                    $USER_LATITUDE, $USER_LONGITUDE, $RADIUS);
                $arrFilter['ID'] = $result['RestListIds'];
                if(($key = array_search(1921, $arrFilter['PROPERTY_features'])) !== false) {
                    if(!$key){
                        unset($arrFilter['PROPERTY_features']);
                    }
                    else {
                        unset($arrFilter['PROPERTY_features'][$key]);
                    }
                }
                $SORT_BY = 'IDS';

            }
            else {
                $_REQUEST['lat'] = $USER_LATITUDE;
                $_REQUEST['lon'] = $USER_LONGITUDE;
                if(!$USER_LATITUDE || !$USER_LONGITUDE){
                    $coords = COption::GetOptionString("main", $CITY_CODE . "_center");
                    $coords_arr = explode(',',$coords);
                    $_REQUEST['lat'] = $coords_arr[1];
                    $_REQUEST['lon'] = $coords_arr[0];
                }

                $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                $arrFilter["!PROPERTY_NETWORK_REST_VALUE"] = "Да";  //  ресторан сети
            }
        }


        $items = $APPLICATION->IncludeComponent("mobile-app:restoraunts.list_on_map", "", Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $CITY_CODE,
            "PARENT_SECTION_CODE" => "restaurants", // Код раздела
            "INCLUDE_SUBSECTIONS" => "N", // Код раздела
            "NEWS_COUNT" => '',
            "SORT_BY1" => $SORT_BY=='IDS'?$SORT_BY:'distance',
            "SORT_ORDER1" => 'ASC',
            "SORT_BY2" => '',
            "SORT_ORDER2" => '',
            "FILTER_NAME" => "arrFilter",//
            "PROPERTY_CODE" => array(
                1 => "kitchen",
                2 => "average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "lat",
                8 => "lon",
                9 => "opening_hours_google",
            ),
            "CHECK_DATES" => "N", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
            "CACHE_TIME" => "36000079", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N", // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
            "SET_TITLE" => "N", // Устанавливать заголовок страницы
            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => 'N', // Выводить под списком
            "PAGER_TITLE" => "Рестораны", // Название категорий
            "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y", // Выводить дату элемента
            "DISPLAY_NAME" => "Y", // Выводить название элемента
            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N", // Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
            'DISTANCE_VALUE' => $RADIUS
        ), false
        );

        return json_encode($items);
    }

    public function getDetailRestaurant($ID) {
        global $APPLICATION;
        global $arrFilter;
        global $USER;

        $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
        $items = $APPLICATION->IncludeComponent("mobile-app:restoraunts.detail", "", Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => '',
            "PARENT_SECTION_CODE" => "", // Код раздела
            'ELEMENT_ID' => intval($ID),
            "NEWS_COUNT" => 1,
            "SORT_BY1" => 'SORT',
            "SORT_ORDER1" => 'ASC',
            "FILTER_NAME" => "arrFilter",//
            "PROPERTY_CODE" => array(
                1 => "kitchen",
                2 => "average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "lat",
                8 => "lon",
                9 => "photos",
                10 => "RATIO",
                11 => "type",
                12 => "music",
                13 => "parking",
                14 => "features",
                15 => "children",
                16 => "proposals",
                17 => "breakfast",
                18 => "entertainment",
                19 => "d_tours",
                20 => "ALLOWED_ALCOHOL",
                21 => "area",
                22 => "out_city",
                23 => "FEATURES_IN_PICS",
                24 => "opening_hours_google",
            ),
            "CHECK_DATES" => "N", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
            "CACHE_TIME" => "36000081", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N", // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
            "SET_TITLE" => "N", // Устанавливать заголовок страницы
            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
            "PAGER_TITLE" => "Рестораны", // Название категорий
            "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y", // Выводить дату элемента
            "DISPLAY_NAME" => "Y", // Выводить название элемента
            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N", // Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
        ), false
        );

        return json_encode($items);
    }

    public function getRestaurantReviews($ID,$COUNT = 5) {
        $obCache = new CPHPCache;
        $life_time = 60*60*24*30;
        $cache_id = $ID.$COUNT.'_REVIEWS_3600';
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars["PROP_VALUES"];
        }
        else {
            $res = CIBlockElement::GetByID(intval($ID));
            if ($ar_res = $res->Fetch()) {
                $arReviewsIB = getArIblock("reviews", $ar_res['IBLOCK_CODE']);
                $arSelect = Array("ID", 'CREATED_BY', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'TIMESTAMP_X', 'NAME');
                $arFilter = Array("IBLOCK_ID" => $arReviewsIB['ID'], "ACTIVE" => "Y", 'PROPERTY_ELEMENT' => $ar_res['ID']);
                $resReviews = CIBlockElement::GetList(Array('timestamp_x' => 'DESC'), $arFilter, false, array('nTopCount' => $COUNT), $arSelect);
                $review_key = 0;
                while ($ob = $resReviews->Fetch()) {
//                    FirePHP::getInstance()->info($ob);
                    $res = CUser::GetByID($ob["CREATED_BY"]);
                    if ($ar = $res->Fetch()) {
                        $result[$review_key]['username'] = $ar["PERSONAL_PROFESSION"];
                        if (!$result[$review_key]['username']) {
                            $temp = explode("@", $ar["EMAIL"]);
                            $result[$review_key]['username'] = $temp[0];
                        }
                    } else {
                        $temp = explode("@", $ob["NAME"]);
                        $result[$review_key]['username'] = $temp[0];
                    }

                    $result[$review_key]['date'] = strtotime($ob['TIMESTAMP_X']);
                    $result[$review_key]['comment'] = $ob['PREVIEW_TEXT'];
                    $result[$review_key]['rating'] = $ob['DETAIL_TEXT'];

                    $review_key++;
                }
            }
        }

        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "PROP_VALUES"    => $result
            ));
        }
        return json_encode($result);

    }

    public function getRestaurantReviewsNEW($ID,$COUNT = 5,$PAGE = 1) {
        global $APPLICATION;
        global $arrFilter;
        $_REQUEST['PAGEN'] = intval($PAGE);
        $items = $APPLICATION->IncludeComponent("mobile-app:reviews.list", "", Array(
                "ID" => intval($ID),
                "NEWS_COUNT" => $COUNT,
                "FILTER_NAME" => "arrFilter",//
                "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
                "CACHE_TYPE" => "A",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
                "CACHE_TIME" => "36000076", // Время кеширования (сек.)
                "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
                "CACHE_GROUPS" => "N", // Учитывать права доступа
            ),
            false
        );

        return json_encode($items);
    }

    public function getRestaurantReviewsPhotos($ID,$COUNT = 5,$PAGE = 1) {
        global $APPLICATION;
        global $arrFilter;
        $_REQUEST['PAGEN'] = intval($PAGE);
//        $arrFilter['!PROPERTY_photos'] = false;
        $items = $APPLICATION->IncludeComponent("mobile-app:reviews.list", "", Array(
            "ID" => intval($ID),
            "NEWS_COUNT" => $COUNT,
            "FILTER_NAME" => "arrFilter",//
            "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
            "CACHE_TYPE" => "A",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
            "CACHE_TIME" => "36000076", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N", // Учитывать права доступа
            "REVIEW_PHOTOS" => "Y"
        ),
            false
        );

        return json_encode($items);

    }

    public function getRestaurantMenuNEW($ID) {

        $obCache = new CPHPCache;
        $life_time = 60*60*24;// день
        $cache_id = $ID.'_MENU_1';
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars["PROP_VALUES"];
        }
        else {

        //tsitaty  foto

            global $DB;
            $sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a
              JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=" . $DB->ForSql($ID) . " ";
            $db_list = $DB->Query($sql);
            if ($ar_result = $db_list->Fetch()) {
//            FirePHP::getInstance()->info($ar_result,'$ar_result$ar_result$ar_result$ar_result$ar_result$ar_result');
                if ($ar_result["ELEMENT_CNT"] > 0) {
                    $MENU_ID = $ar_result['ID'];



                    //  получение id фото раздела
                    $menu_key = 0;
//                FirePHP::getInstance()->info();

                    //  TODO собрать разделы, по разделам элементы

                    // возвращаем меню без раздела фото
                    $MenuSectionsArr = array();
                    $MenuSectionsAllArr = array();
                    $db_list = CIBlockSection::getList(array(), array('IBLOCK_ID' => $MENU_ID,'!CODE'=>'foto'), true);//array('ID', 'NAME', 'CODE')
                    if($db_list->SelectedRowsCount()==1){
                        $menu_key = 0;

                        // возвращаем меню без раздела фото
                        $db_list = CIBlockSection::getList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'CODE' => 'foto'), false, array('ID'));
                        if ($ar_result = $db_list->Fetch()) {
                            $photoSectionId = $ar_result['ID'];
                            $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, '!SECTION_ID' => $photoSectionId, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME','TIMESTAMP_X'));
                        } else {
                            $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME','TIMESTAMP_X'));//'>CATALOG_PRICE_1'=>0
                        }



//                        $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));//'>CATALOG_PRICE_1'=>0
                        CModule::IncludeModule("sale");
                        CModule::IncludeModule("catalog");
                        while ($ar_result = $db_list->Fetch()) {
                            $result[$menu_key]['name'] = $ar_result['NAME'];
                            $result[$menu_key]['update'] = date('j.m.Y', strtotime($ar_result['TIMESTAMP_X']));

//
//                            if($ar_result['PREVIEW_PICTURE']){
//                                $file = CFile::GetFileArray($ar_result['PREVIEW_PICTURE']);
//                                $result[$menu_key]['image'] = $file['SRC'];
//                            }
//                            else {
//                                $result[$menu_key]['image'] = '';
//                            }

                            $db_res = CPrice::GetList(
                                array(),
                                array(
                                    "PRODUCT_ID" => $ar_result["ID"],
                                    "CATALOG_GROUP_ID" => 1,
                                )
                            );
                            if ($price = $db_res->Fetch()) {
                                $this_price = $price;
                            }

                            if ($this_price["CURRENCY"] == "EUR")
                                $result[$menu_key]['price'] = sprintf("%01.2f", $this_price["PRICE"]);
                            else
                                $result[$menu_key]['price'] = sprintf("%01.0f", $this_price["PRICE"]);

                            $menu_key++;
                        }
                    }
                    elseif($db_list->SelectedRowsCount()>1) {



                        while ($ar_result = $db_list->Fetch()) {
                            if($ar_result['IBLOCK_SECTION_ID']){
                                $MenuSectionsArr[$ar_result['IBLOCK_SECTION_ID']][$ar_result['ID']] = $ar_result['ID'];
                            }
                            else {
                                $MenuSectionsArr[$ar_result['ID']]['self'] = $ar_result['ID'];
                            }
                            $MenuSectionsAllArr[$ar_result['ID']] = $ar_result;
                        }


                        // возвращаем меню без раздела фото
                        $db_list = CIBlockSection::getList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'CODE' => 'foto'), false, array('ID'));
                        if ($ar_result = $db_list->Fetch()) {
                            $photoSectionId = $ar_result['ID'];
                            $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, '!SECTION_ID' => $photoSectionId, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID','TIMESTAMP_X'));
                        } else {
                            $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID','TIMESTAMP_X'));//'>CATALOG_PRICE_1'=>0
                        }
//                        $db_list = CIBlockElement::GetList(array('IBLOCK_SECTION_ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID'));//'>CATALOG_PRICE_1'=>0

                        CModule::IncludeModule("sale");
                        CModule::IncludeModule("catalog");
                        while ($ar_result = $db_list->Fetch()) {
                            $needle = $ar_result['IBLOCK_SECTION_ID'];

                            $arr_this_section = array_filter($MenuSectionsArr,function($v, $k) use($needle){
                                return array_search($needle,$v);//
                            });


                            $arr_this_section = reset($arr_this_section);
                            if($key = array_search($needle,$arr_this_section)){
                                if($key=='self'){
                                    $result[$arr_this_section['self']]['name'] = $MenuSectionsAllArr[$arr_this_section['self']]['NAME'];
                                    $result[$arr_this_section['self']]['menu'][$menu_key]['name'] = $ar_result['NAME'];
                                    $result[$arr_this_section['self']]['update'] = date('j.m.Y', strtotime($ar_result['TIMESTAMP_X']));

                                    //if($ar_result['PREVIEW_PICTURE']){
                                    //    $file = CFile::GetFileArray($ar_result['PREVIEW_PICTURE']);
                                    //    $result[$arr_this_section['self']]['menu'][$menu_key]['image'] = $file['SRC'];
                                    //}
                                    //else {
                                    //    $result[$arr_this_section['self']]['menu'][$menu_key]['image'] = '';
                                    //}

                                    $db_res = CPrice::GetList(
                                        array(),
                                        array(
                                            "PRODUCT_ID" => $ar_result["ID"],
                                            "CATALOG_GROUP_ID" => 1,
                                        )
                                    );
                                    if ($price = $db_res->Fetch()) {
                                        $this_price = $price;
                                    }

                                    if ($this_price["CURRENCY"] == "EUR")
                                        $result[$arr_this_section['self']]['menu'][$menu_key]['price'] = sprintf("%01.2f", $this_price["PRICE"]);
                                    else
                                        $result[$arr_this_section['self']]['menu'][$menu_key]['price'] = sprintf("%01.0f", $this_price["PRICE"]);

                                    $menu_key++;
                                }
                                else {

                                    $result[$arr_this_section['self']]['name'] = $MenuSectionsAllArr[$arr_this_section['self']]['NAME'];
                                    $result[$arr_this_section['self']]['menu'][$key]['name'] = $MenuSectionsAllArr[$key]['NAME'];
                                    $result[$arr_this_section['self']]['menu'][$key]['menu'][$menu_key]['name'] = $ar_result['NAME'];

                                    //if($ar_result['PREVIEW_PICTURE']){
                                    //    $file = CFile::GetFileArray($ar_result['PREVIEW_PICTURE']);
                                    //    $result[$arr_this_section['self']]['menu'][$key]['menu'][$menu_key]['image'] = $file['SRC'];
                                    //}
                                    //else {
                                    //    $result[$arr_this_section['self']]['menu'][$key]['menu'][$menu_key]['image'] = '';
                                    //}

                                    $db_res = CPrice::GetList(
                                        array(),
                                        array(
                                            "PRODUCT_ID" => $ar_result["ID"],
                                            "CATALOG_GROUP_ID" => 1,
                                        )
                                    );
                                    if ($price = $db_res->Fetch()) {
                                        $this_price = $price;
                                    }

                                    if ($this_price["CURRENCY"] == "EUR")
                                        $result[$arr_this_section['self']]['menu'][$key]['menu'][$menu_key]['price'] = sprintf("%01.2f", $this_price["PRICE"]);
                                    else
                                        $result[$arr_this_section['self']]['menu'][$key]['menu'][$menu_key]['price'] = sprintf("%01.0f", $this_price["PRICE"]);

                                    $menu_key++;
                                }
                            }
                        }
                    }

                }
            }
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "PROP_VALUES"    => $result
            ));
        }
//        FirePHP::getInstance()->info(array_values($result));
        $result = array_values($result);
        foreach($result as &$first_menu_step){
            $first_menu_step['menu'] = array_values($first_menu_step['menu']);
            foreach($first_menu_step['menu'] as &$second_menu_step){
                $second_menu_step['menu'] = array_values($second_menu_step['menu']);
            }
        }

        return json_encode($result);

    }

    public function getRestaurantMenu($ID) {

        $obCache = new CPHPCache;
        $life_time = 60*60*24*30;// месяц
        $cache_id = $ID.'_MENU';
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars["PROP_VALUES"];
        }
        else {

            global $DB;
            $sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a
              JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=" . $DB->ForSql($ID) . " ";
            $db_list = $DB->Query($sql);
            if ($ar_result = $db_list->Fetch()) {
//            FirePHP::getInstance()->info($ar_result,'$ar_result$ar_result$ar_result$ar_result$ar_result$ar_result');
                if ($ar_result["ELEMENT_CNT"] > 0) {
                    $MENU_ID = $ar_result['ID'];

                    //  получение id фото раздела
                    $menu_key = 0;
//                FirePHP::getInstance()->info();

                    //  TODO собрать разделы, по разделам элементы

                    // возвращаем меню без раздела фото
                    $db_list = CIBlockSection::getList(array(), array('IBLOCK_ID' => $MENU_ID, 'CODE' => 'foto'), false, array('ID'));
                    if ($ar_result = $db_list->Fetch()) {
                        $photoSectionId = $ar_result['ID'];
                        $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, '!SECTION_ID' => $photoSectionId, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));
                    } else {
                        $db_list = CIBlockElement::GetList(array('ID' => 'DESC'), array('IBLOCK_ID' => $MENU_ID, 'INCLUDE_SUBSECTIONS' => 'Y', 'ACTIVE' => 'Y'), false, false, array('ID', 'NAME'));//'>CATALOG_PRICE_1'=>0
                    }
                    CModule::IncludeModule("sale");
                    CModule::IncludeModule("catalog");
                    while ($ar_result = $db_list->Fetch()) {
                        $result[$menu_key]['name'] = $ar_result['NAME'];

                        $db_res = CPrice::GetList(
                            array(),
                            array(
                                "PRODUCT_ID" => $ar_result["ID"],
                                "CATALOG_GROUP_ID" => 1,
                            )
                        );
                        if ($price = $db_res->Fetch()) {
                            $this_price = $price;
                        }

                        if ($this_price["CURRENCY"] == "EUR")
                            $result[$menu_key]['price'] = sprintf("%01.2f", $this_price["PRICE"]);
                        else
                            $result[$menu_key]['price'] = sprintf("%01.0f", $this_price["PRICE"]);

                        $menu_key++;
                    }
                }
            }
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "PROP_VALUES"    => $result
            ));
        }

        return json_encode($result);

    }

    public function getPropValues($CODE, $CITY_CODE = 'msk') {
        $obCache = new CPHPCache;
        $life_time = 60*60*24*30*13;
        $cache_id = $CITY_CODE.$CODE;
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars["PROP_VALUES"];
        }
        else {

            if($CODE=='kitchen'){
                $PROP_IBLOCK_ID = 114;
            }
            if($CODE=='average_bill'){
                if($CITY_CODE=='rga'||$CITY_CODE=='urm'){
                    $PROP_IBLOCK_ID = 3603;
                }
                else {
                    $PROP_IBLOCK_ID = 78;
                }
            }
            if($CODE=='subway'){
                if($CITY_CODE=='msk'){
                    $PROP_IBLOCK_ID = 30;
                }
                elseif($CITY_CODE=='spb') {
                    $PROP_IBLOCK_ID = 89;
                }
                elseif($CITY_CODE=='rga') {
                    $PROP_IBLOCK_ID = 3586;
                }
                elseif($CITY_CODE=='urm') {
                    $PROP_IBLOCK_ID = 3587;
                }
            }
            if($CODE=='features'){
                $PROP_IBLOCK_ID = 76;
            }
            if($CODE=='proposals'){
                $PROP_IBLOCK_ID = 232;
            }
            if($CODE=='rest_group'){
                $PROP_IBLOCK_ID = 137;
            }

            if($PROP_IBLOCK_ID){
                $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PROP_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("ID", "NAME"));
                while ($ar = $res->Fetch()) {
                    $result[] = $ar;
                }
            }
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "PROP_VALUES"    => $result
            ));
        }

        return json_encode($result);
    }

    public function getPropsCode() {
        $result = array(
            0=>array('CODE'=>'KITCHEN','NAME'=>'Кухня'),
            1=>array('CODE'=>'AVERAGE_BILL','NAME'=>'Средний счет'),
            2=>array('CODE'=>'SUBWAY','NAME'=>'Метро'),
            3=>array('CODE'=>'FEATURES','NAME'=>'Особенности'),
            4=>array('CODE'=>'PROPOSALS_FEATURES','NAME'=>'Предложения'),
            5=>array('CODE'=>'REST_GROUP','NAME'=>'Ресторанная группа'),
            6=>array('CODE'=>'DISTRICT','NAME'=>'Район'),
            7=>array('CODE'=>'OUT_OF_CITY','NAME'=>'Пригороды')
        );
        return json_encode($result);
    }

    public function getPropsForCity($CITY_CODE, $PROP_CODE = '') {

        $obCache = new CPHPCache;
        $life_time = 60*60*24*30*13;
        $cache_id = $CITY_CODE.'_getPropsForCity_12';
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars["PROP_VALUES"];
        }
        else {

            $propsArr = array(
//                0=>array('CODE'=>'type','NAME'=>'Тип','OUTPUT_CODE'=>'TYPE'),
                1=>array('CODE'=>'kitchen','NAME'=>'Кухня','OUTPUT_CODE'=>'KITCHEN'),
                2=>array('CODE'=>'average_bill','NAME'=>'Средний счет','OUTPUT_CODE'=>'AVERAGE_BILL'),
                3=>array('CODE'=>'features','NAME'=>'Особенности','OUTPUT_CODE'=>'FEATURES'),
                4=>array('CODE'=>'rest_group','NAME'=>'Ресторанная группа','OUTPUT_CODE'=>'REST_GROUP'),
                5=>array('CODE'=>'subway','NAME'=>'Метро','OUTPUT_CODE'=>'SUBWAY'),
                6=>array('CODE'=>'area','NAME'=>'Районы','OUTPUT_CODE'=>'DISTRICT'),
                7=>array('CODE'=>'out_city','NAME'=>'Пригороды','OUTPUT_CODE'=>'OUT_OF_CITY'),
                8=>array('CODE'=>'proposals','NAME'=>'Предложения','OUTPUT_CODE'=>'PROPOSALS_FEATURES'),

//                8=>array('CODE'=>'FEATURES_IN_PICS','NAME'=>'Особенности в картинках'),
            );

            $key = 0;
            foreach($propsArr as $CODE){
//                if($CODE['CODE']=='type'){

//                }
                if($CODE['CODE']=='area'){
                    if($CITY_CODE=='msk'){
                        $PROP_IBLOCK_ID = 60;
                    }
                    elseif($CITY_CODE=='spb') {
                        $PROP_IBLOCK_ID = 110;
                    }
                    elseif($CITY_CODE=='rga') {
                        $PROP_IBLOCK_ID = 3588;
                    }
                    elseif($CITY_CODE=='urm') {
                        $PROP_IBLOCK_ID = 3589;
                    }
                }

                if($CODE['CODE']=='out_city'){
                    if($CITY_CODE=='msk'){
                        $PROP_IBLOCK_ID = 79;
                    }
                    elseif($CITY_CODE=='spb') {
                        $PROP_IBLOCK_ID = 106;
                    }
                    elseif($CITY_CODE=='rga') {
                        $PROP_IBLOCK_ID = 3590;
                    }
                    elseif($CITY_CODE=='urm') {
                        $PROP_IBLOCK_ID = 3591;
                    }
                }

                if($CODE['CODE']=='kitchen'){
                    $PROP_IBLOCK_ID = 114;
                }
                if($CODE['CODE']=='average_bill'){
                    if($CITY_CODE=='rga'||$CITY_CODE=='urm'){
                        $PROP_IBLOCK_ID = 3603;
                    }
                    else {
                        $PROP_IBLOCK_ID = 78;
                    }
                }
                if($CODE['CODE']=='subway'){
                    if($CITY_CODE=='msk'){
                        $PROP_IBLOCK_ID = 30;
                    }
                    elseif($CITY_CODE=='spb') {
                        $PROP_IBLOCK_ID = 89;
                    }
                    elseif($CITY_CODE=='rga') {
                        $PROP_IBLOCK_ID = 3586;
                    }
                    elseif($CITY_CODE=='urm') {
                        $PROP_IBLOCK_ID = 3587;
                    }
                }
                if($CODE['CODE']=='features'){
                    $PROP_IBLOCK_ID = 76;
                }
                if($CODE['CODE']=='proposals'){
                    $PROP_IBLOCK_ID = 232;
                }
                if($CODE['CODE']=='rest_group'){
                    if($CITY_CODE=='msk'){
                        $PROP_IBLOCK_ID = 2735;
                    }
                    if($CITY_CODE=='spb'){
                        $PROP_IBLOCK_ID = 2736;
                    }
                    if($CITY_CODE=='urm'){
                        $PROP_IBLOCK_ID = 3577;
                    }
                    if($CITY_CODE=='rga'){
                        $PROP_IBLOCK_ID = 3576;
                    }
                }

//                if($CODE['CODE']=='FEATURES_IN_PICS'){
//                    $PROP_IBLOCK_ID = 4349;
//                }

                if($PROP_IBLOCK_ID){
                    $result[$key]['CODE'] = $CODE['OUTPUT_CODE'];
                    $result[$key]['NAME'] = $CODE['NAME'];

                    if($CODE['CODE']=='features'){
                        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PROP_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("ID", "NAME",'PREVIEW_PICTURE'));
                    }
                    else {
                        $res = CIBlockElement::GetList($CODE['CODE']=='average_bill'?Array('SORT'=>'ASC'):Array(), Array("IBLOCK_ID" => $PROP_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("ID", "NAME"));
                    }

                    while ($ar = $res->Fetch()) {
                        if($CODE['CODE']=='features'&&$ar['PREVIEW_PICTURE']){
                            $arFile = CFile::GetFileArray($ar["PREVIEW_PICTURE"]);
                            $ar['PREVIEW_PICTURE'] = $arFile['SRC'];
                        }
                        $result[$key]['VALUES'][] = $ar;
                    }
                    $key++;
                }
            }

        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "PROP_VALUES"    => $result
            ));
        }

        if($PROP_CODE){
            foreach ($result as $key_prop_for_return=>$prop_result) {
                if($prop_result['CODE']==$PROP_CODE){
                    return json_encode($prop_result);
                }
            }
        }

        return json_encode($result);
    }


    public function sendRestaurantReview($ID, $USER_NAME, $EMAIL, $RATING, $COMMENT, $PHOTO_STR='') {
        $REST_ID = $ID;
        /**
        restoran_id - id ресторана
        username - имя пользователя
        rating - 5ти бальный рейтинг
        comment - комментарий
         **/

        $arResult["RESPONSE"] = false;
        $user = new CUser;

//        $name_str = strtolower(trim($USER_NAME));
        $arParams = array("replace_space"=>"_","replace_other"=>"_");
        $rand_string = time();
//        $translate_name = Cutil::translit($name_str,"ru",$arParams).$rand_string;
        $EMAIL = 'mobile_app_'.$rand_string.$EMAIL;
        $NIC = 'mobile_app_'.$rand_string;

        $arFields = Array(
//            "EMAIL"             => 'mobile_app_'.$translate_name.'@mail.ru',
            "EMAIL"             => $EMAIL,
            'NAME' => htmlspecialchars($USER_NAME),
            "LOGIN"             => $EMAIL,
            "PERSONAL_PROFESSION"             => htmlspecialchars($USER_NAME),
            "ACTIVE"            => "Y",
            "GROUP_ID"          => array(5),
            "PASSWORD"          => randString(7),
        );
        $ID = $user->Add($arFields);
        if (!$ID)
        {
//            FirePHP::getInstance()->info($user->LAST_ERROR,'$user->LAST_ERROR');
            $temp = explode("<br>",$user->LAST_ERROR);
            $arResult["ERROR_TEXT"] = $temp[0];
            $arResult["RESPONSE"] = false;
        }
        else
        {
            unset($arFields["PASSWORD"]);
            $arFields["USER_ID"] = $ID;
//            $event = new CEvent;
//            $event->SendImmediate("NEW_USER", SITE_ID, $arFields,"N", 1);



            $el = new CIBlockElement;
            $PROP = array();
            $PROP["ELEMENT"] = intval($REST_ID);

            $res = CIBlockElement::GetByID($PROP["ELEMENT"]);
            if ($ar_res = $res->Fetch()) {
//                FirePHP::getInstance()->info($ar_res,'$ar_res');
                $CITY_CODE = $ar_res['IBLOCK_CODE'];

                $arReviewsIB = getArIblock("reviews", $CITY_CODE);


//                FirePHP::getInstance()->info('try to decode');
                /**BASE_PHOTO**/
                if($PHOTO_STR){

                    $data = base64_decode($PHOTO_STR);//preg_replace('#^data:image/\w+;base64,#i', '',
                    if($data==false){
                        $arResult["ERROR_TEXT"] = 'cant decode pic';
                        $arResult["RESPONSE_FOR_PIC"] = false;
                    }
                    else {
//                        preg_match('/image\/(.+);base64,/i', $PHOTO_STR, $type_str);
                        $file_name = 'mobile_app_pic_'.uniqid().'.jpg';

                        if(file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name, $data)){

                            $arIMAGE["del"] = 'N';
                            $arIMAGE["MODULE_ID"] = "mobile-app";

                            $PROP["photos"] = array("VALUE"=>CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/mobile-app-user-photos/".$file_name));

//                            if (strlen($arFile["name"])>0)
//                            {
//                                $fid = CFile::SaveFile($arFile, "mobile-app-user-photos");
//                                if (intval($fid)>0){
//                                    $arFields["IMAGE_ID"] = intval($fid);
//                                    unlink($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name);
//                                    $PROP["photos"] = $arFields["IMAGE_ID"];
//                                }
//                                else $arFields["IMAGE_ID"] = "null";
//
////                                $arResult["RESPONSE"] = true;
//                            }

                        }
                        else {
                            $arResult["ERROR_TEXT"] = 'cant save pic to server';
                            $arResult["RESPONSE_FOR_PIC"] = false;
                        }
                    }
                }

                /**BASE_PHOTO**/
//                FirePHP::getInstance()->info($arResult,'$arResult');
                if($arResult["RESPONSE_FOR_PIC"]!==false){

//                    FirePHP::getInstance()->info('try to add');
                    $arLoadProductArray = Array(  "MODIFIED_BY"    => $ID,
                        "IBLOCK_ID"      => $arReviewsIB["ID"],
                        "PROPERTY_VALUES"=> $PROP,
                        "ACTIVE_FROM"=> date("d.m.Y H:i:s"),
                        "NAME"  => 'Пользователь мобильного приложения - '.htmlspecialchars($USER_NAME),
                        "ACTIVE" => "Y",
                        "PREVIEW_TEXT"   => htmlspecialchars($COMMENT),
                        'DETAIL_TEXT' => intval($RATING),
                        'CREATED_BY' =>$arFields["USER_ID"]
                    );

                    if($PRODUCT_ID = $el->Add($arLoadProductArray))
                    {
                        //   отправка письма и смс ресторатору
//                    $arIB = getArIblock("catalog",$CITY_CODE);
//                    $db_props = CIBlockElement::GetProperty($arIB['ID'], $PROP["ELEMENT"], array("sort" => "asc"), Array("CODE"=>"user_bind"));
//                    if($ar_props = $db_props->Fetch()){
//
//                        $restorator_obj = CUser::GetByID($ar_props['VALUE']);
//                        if($restorator_email_arr = $restorator_obj->Fetch()) {
//                            $arEventFields = array(
//                                "EMAIL" => $restorator_email_arr["EMAIL"],  //    ресторатору
//                                "URL" => $_SERVER["HTTP_REFERER"],
//                                "NAME" => 'Пользователь мобильного приложения - '.htmlspecialchars($USER_NAME),
//                                "COMMENT" => htmlspecialchars($COMMENT)
//                            );
//                            CEvent::Send("NEW_COMMENT", "s1", $arEventFields, false, 154);
//
//                            if($restorator_email_arr['PERSONAL_PHONE']){
//                                if (CModule::IncludeModule("sozdavatel.sms"))
//                                {
//                                    $message = "На странице Вашего заведения на Ресторан.ру оставлен отзыв";
//
//                                    $res = CIBlockElement::GetByID(intval($REST_ID));
//                                    if($ar_res = $res->GetNext()){
//                                        $show = false;
//                                        $rsData = CBXShortUri::GetList(Array(), Array());
//                                        while($arRes = $rsData->Fetch()) {
//                                            if ($arRes["URI"] == $ar_res['DETAIL_PAGE_URL']) {
//                                                $str_SHORT_URI = $arRes["SHORT_URI"];
//                                                $show = true;
//                                            }
//                                        }
//                                        if ($show):
//                                            $map_url = "http://restoran.ru/".$str_SHORT_URI;
//                                        else:
//                                            $str_SHORT_URI = CBXShortUri::GenerateShortUri();
//                                            $arFields = Array(
//                                                "URI" => $ar_res['DETAIL_PAGE_URL'],
//                                                "SHORT_URI" => $str_SHORT_URI,
//                                                "STATUS" => "301",
//                                            );
//                                            $ID = CBXShortUri::Add($arFields);
//                                            $map_url = "http://restoran.ru/".$str_SHORT_URI;
//                                        endif;
//                                    }
//                                    else {
//                                        $map_url = 'http://restoran.ru/';
//                                    }
//
//                                    $message .= " (".$map_url.").";
//
//                                    CSMS::Send($message, '7'.$restorator_email_arr['PERSONAL_PHONE'], "UTF-8");
//                                }
//                            }
//
//
//                        }
//                    }
                        $arResult["RESPONSE"] = true;

                        if($file_name){
                            unlink($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name);
                        }
                    }
                    else
                    {
//                        FirePHP::getInstance()->info($el->LAST_ERROR,'$el->LAST_ERROR');
                        $temp = explode("<br>",$el->LAST_ERROR);
                        $arResult["ERROR_TEXT"] = $temp[0];
                        $arResult["RESPONSE"] = false;
                    }
                }

            }

        }
        return json_encode($arResult);
    }

    public function checkSavePhoto($PHOTO_STR){

        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $PHOTO_STR));
        if($data==false){
            $arResult["ERROR_TEXT"] = 'cant decode pic';
            $arResult["RESPONSE"] = false;
        }
        else {
            preg_match('/image\/(.+);base64,/i', $PHOTO_STR, $type_str);
            $file_name = 'mobile_app_pic_'.uniqid().'.'.$type_str[1];

            if(file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name, $data)){

                $arIMAGE["del"] = 'N';
                $arIMAGE["MODULE_ID"] = "mobile-app";

                $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/mobile-app-user-photos/".$file_name);

                if (strlen($arFile["name"])>0)
                {
                    $fid = CFile::SaveFile($arFile, "mobile-app-user-photos");
                    if (intval($fid)>0){
                        $arFields["IMAGE_ID"] = intval($fid);
                        unlink($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name);
                    }
                    else $arFields["IMAGE_ID"] = "null";

                    $arResult["RESPONSE"] = true;
                }

            }
            else {
                $arResult["RESPONSE"] = false;
                $arResult["ERROR_TEXT"] = 'cant save pic to server';
            }


        }

        echo json_encode($arResult);
    }

    public function checkSavePhotoNEW($PHOTO_STR){

        $data = base64_decode($PHOTO_STR);
        if($data==false){
            $arResult["ERROR_TEXT"] = 'cant decode pic';
            $arResult["RESPONSE"] = false;
        }
        else {
            $file_name = 'mobile_app_pic_'.uniqid().'.jpg';

            if(file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name, $data)){

                $arIMAGE["del"] = 'N';
                $arIMAGE["MODULE_ID"] = "mobile-app";

                $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/mobile-app-user-photos/".$file_name);

                if (strlen($arFile["name"])>0)
                {
                    $fid = CFile::SaveFile($arFile, "mobile-app-user-photos");
                    if (intval($fid)>0){
                        $arFields["IMAGE_ID"] = intval($fid);
                        unlink($_SERVER['DOCUMENT_ROOT'].'/upload/mobile-app-user-photos/'.$file_name);
                    }
                    else $arFields["IMAGE_ID"] = "null";

                    $arResult["RESPONSE"] = true;
                }

            }
            else {
                $arResult["RESPONSE"] = false;
                $arResult["ERROR_TEXT"] = 'cant save pic to server';
            }


        }

        echo json_encode($arResult);
    }

    public function bBronRestaurant($ID, $date, $time, $guets, $name, $phone, $push_token, $wish='') {
        /**
        restoran_id - id ресторана
        date - дата посещения (21.06.2015)
        time - время посещения (11:00)
        guets - кол-во посетителей (3)
        name - имя пользователя (текст)
        phone - телефон пользователя (9500021435)
        wish - пожелание (текст)
         **/

        global $USER;
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement;
        $PROP = array();
        $result = false;

        $PROP["rest"] = intval($ID);

        $res = CIBlockElement::GetByID($ID);
        if ($ar = $res->Fetch())
        {
            $rest_iblock = $ar["IBLOCK_ID"];
            if ($ar["IBLOCK_ID"]==11)
                $SECTION=83659;
            elseif($ar["IBLOCK_ID"]==12)
                $SECTION=83660;
            elseif($ar["IBLOCK_ID"]==3566||$ar["IBLOCK_ID"]==3567)
                $SECTION=274887;

            $phone = preg_replace("/\D/", "", $phone);
            if (substr($phone, 0, 1)=="7")
                $phone = substr($phone, 1);

            $arrFilter=array("PROPERTY_TELEPHONE"=>$phone, "IBLOCK_ID"=>104);
            $res = CIBlockElement::GetList(Array(), $arrFilter, false, false, false);
            if($ob = $res->GetNextElement())
            {
                $arFieldsu = $ob->GetFields();
                //такой телефон или email есть в базе клиентов
                $PROP["client"]=$arFieldsu["ID"];

//                $PROP["user"]
            }
            else
            {
                //такого клиента нет в базе, нужно его добавить
                $PROP_client["SURNAME"] = htmlspecialchars($name);
                $PROP_client["TELEPHONE"] = $phone;
                $PROP_client["EMAIL"] = '';

                //  TODO найти пользователя по телефону

                $PROP_client["user"] = '';


                $filter = Array
                (
                    "PERSONAL_PHONE" => $phone,
                    "ACTIVE" => "Y",
                );

                $rsUsers = CUser::GetList($sort_by = array("id"=>'asc'), $sort_order = "", $filter); // выбираем пользователей
                if($user_res = $rsUsers->Fetch()){
                    $PROP_client["user"] = $user_res['ID'];
                }

                $arLoadProductArray_client = Array(
                    "IBLOCK_ID"=>104,
                    "IBLOCK_SECTION" => $SECTION,
                    "PROPERTY_VALUES"=> $PROP_client,
                    "NAME"           => htmlspecialchars($name),
                    "ACTIVE"         => "Y",
                    "PREVIEW_TEXT"   => "Клиент создан автоматически",
                );

                if($CLIENT_ID = $el->Add($arLoadProductArray_client)){
                    $PROP["client"]=$CLIENT_ID;
                }
            }


            if($PROP["client"]){
                $PROP["new"] = 1549;
                $PROP["status"] = 1418;
                $PROP["source"] = 2812;
                $PROP["guest"] = intval($guets);

                $PROP["type"] = 1498;
                $date = date("d.m.Y",strtotime($date));
                $time = date("H:i",strtotime($date.' '.$time));

                //        if ($a["resourceId"]==1)
                //            $smoke = "Некурящий зал";
                //        if ($a["resourceId"]==2)
                //            $smoke = "Курящий зал";

                $arLoadProductArray = Array(
                    "ACTIVE_FROM" => $date." ".$time.":00",
                    "MODIFIED_BY"    => false,
                    "IBLOCK_SECTION_ID" => $SECTION,
                    "IBLOCK_ID"      => 105,
                    "PROPERTY_VALUES"=> $PROP,
                    "PREVIEW_TEXT" => "Пожелания:".htmlspecialchars($wish)."\n",
                    "PREVIEW_TEXT_TYPE" => "text",
                    "NAME"           => ($USER->GetFullName())?$USER->GetFullName():"Незарегистированный пользователь",
                    "ACTIVE"         => "Y",
                    "CREATED_BY"=>false,
                    'DETAIL_TEXT' => $push_token
                );

                $PRODUCT_ID = $el->Add($arLoadProductArray);
                if ($PRODUCT_ID)
                {
                    $el->Update($PRODUCT_ID, array("CREATED_BY"=>false, "MODIFIED_BY"=>false));
                    $result = true;
                }
            }
        }

        return $result;
    }

    public function searchRestByTitle($queryStr,$CITY_CODE='spb') {
        global $APPLICATION;
        $items = $APPLICATION->IncludeComponent(
            "mobile-app:simple.search",
            "suggest",
            Array(
                "COUNT" => 10,
                'NO_SLEEP'=>'Y',
                'q' => $queryStr,
                'CACHE_TYPE' => 'Y',
                'CACHE_TIME' => '3601',
                'IBLOCK_ID' => $CITY_CODE
            )
        );
        return json_encode($items);
    }

    public function searchRestByTitleNEW($queryStr,$CITY_CODE='spb',$COUNT = 6) {

        global $APPLICATION;
        if($queryStr&&$CITY_CODE){
            $arRestIB = getArIblock("catalog", $CITY_CODE);
            $_REQUEST["q"] = $queryStr;
            global $arrFilter;
            $arrFilter = array("PARAMS" => array("sleeping"=>"N"));
            $items = $APPLICATION->IncludeComponent(
                "mobile-app:search.page",
                "",
                Array(
                    "USE_SUGGEST" => "N",
                    "AJAX_MODE" => "N",
                    "RESTART" => "Y",
                    "NO_WORD_LOGIC" => "Y",
                    "USE_LANGUAGE_GUESS" => "N",
                    "CHECK_DATES" => "Y",
                    "USE_TITLE_RANK" => "Y",
                    "DEFAULT_SORT" => "rank",
                    "FILTER_NAME" => "arrFilter",
                    "SHOW_WHERE" => "N",
                    "SHOW_WHEN" => "N",
                    "PAGE_RESULT_COUNT" => intval($COUNT),//6
                    "CACHE_TYPE" => 'Y',//y
                    "CACHE_TIME" => "86406",
                    "CACHE_NOTES" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Результаты поиска",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "arrFILTER" => array("iblock_catalog"),
                    "arrFILTER_iblock_catalog" => array($arRestIB["ID"]),
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                )
            );
            return json_encode($items);
        }
    }


    public function getUserByPhone($phone) {
        $filter = Array
        (
            "PERSONAL_PHONE" => $phone,
            "ACTIVE" => "Y",
        );

        $rsUsers = CUser::GetList($sort_by = array("id"=>'asc'), $sort_order = "", $filter); // выбираем пользователей
        if($user_res = $rsUsers->Fetch()){
//            FirePHP::getInstance()->info($user_res);
            $result = $user_res['ID'];
        }
        return $result;
    }

    public function getCitiesInf(){
        $arResult = array(
            0=> array('city'=>'Москва','city_code'=>'msk','phone'=>'+7 (495) 988 26 56','city_en_name'=>'Moscow'),
            1=> array('city'=>'Санкт-Петербург','city_code'=>'spb','phone'=>'+7 (812) 740 18 20','city_en_name'=>'Saint Petersburg'),
            2=> array('city'=>'Рига/Юрмала','city_code'=>'rga','phone'=>'+371 661 031 06','city_en_name'=>'Riga/Jurmala'),
//            3=> array('city'=>'Юрмала','city_code'=>'urm','phone'=>'+371 661 031 06','city_en_name'=>'Jurmala')
        );
        return json_encode($arResult);
    }

    public function getCollectionsForCity($CITY_CODE) {

        //  TODO to cache
        if($CITY_CODE=='msk'){
            $PROP_IBLOCK_ID = 4364;
        }
        elseif($CITY_CODE=='spb') {
            $PROP_IBLOCK_ID = 4363;
        }
        elseif($CITY_CODE=='rga') {
            $PROP_IBLOCK_ID = 4365;
        }

        if($PROP_IBLOCK_ID){
            $arFilter = Array('IBLOCK_ID'=>$PROP_IBLOCK_ID, 'ACTIVE'=>'Y','CNT_ACTIVE'=>'Y','ELEMENT_SUBSECTIONS'=>'N','SECTION_ID'=>false);//'SECTION_ID'=>false,
            $db_list = CIBlockSection::GetList(Array('name'=>'asc'), $arFilter, true, array('NAME','ID','ELEMENT_CNT','PICTURE'),false);//,'IBLOCK_SECTION_ID'
            while ($ar = $db_list->Fetch()) {
                if($ar['ELEMENT_CNT']){
                    if($ar['PICTURE']){
                        $resize_img = CFile::ResizeImageGet($ar['PICTURE'], array('width'=>1024, 'height'=>1024), BX_RESIZE_IMAGE_EXACT, true, Array());
                        $ar['PICTURE'] = $resize_img['src'];
                    }
                    $result[] = $ar;
                }
            }
        }

        return json_encode($result);
    }

    public function getCollectionList($CITY_CODE, $COLLECTION_ID) {
        if($CITY_CODE=='msk'){
            $PROP_IBLOCK_ID = 4364;
        }
        elseif($CITY_CODE=='spb') {
            $PROP_IBLOCK_ID = 4363;
        }
        elseif($CITY_CODE=='rga') {
            $PROP_IBLOCK_ID = 4365;
        }

        if($PROP_IBLOCK_ID && $COLLECTION_ID = intval($COLLECTION_ID)){
            global $APPLICATION;
            global $arrFilter;

            //  TODO PLACE TO CACHE OR TO COMPONENT OR NEW COMPONENT
            $arSelect = Array("ID", 'NAME', 'PROPERTY_RESTAURANT');
            $arFilter = Array("IBLOCK_ID" => $PROP_IBLOCK_ID, "ACTIVE" => "Y", 'SECTION_ID' => $COLLECTION_ID);
            $res = CIBlockElement::GetList(Array('SORT' => 'DESC', 'NAME'=>'ASC'), $arFilter, false, false, $arSelect);
            while ($ob = $res->Fetch()) {
                $arrFilter['ID'][] = $ob['PROPERTY_RESTAURANT_VALUE'];
            }
            $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
            $items = $APPLICATION->IncludeComponent("mobile-app:restoraunts.list", "", Array(
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $CITY_CODE,
                "PARENT_SECTION_CODE" => "restaurants", // Код раздела
                "NEWS_COUNT" => 1000,
                "SORT_BY1" => 'IDS',
                "SORT_ORDER1" => '',
                "SORT_BY2" => '',
                "SORT_ORDER2" => '',
                "FILTER_NAME" => "arrFilter",//
                "PROPERTY_CODE" => array(
                    1 => "kitchen",
                    2 => "average_bill",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "address",
                    6 => "subway",
                    7 => "lat",
                    8 => "lon",
                    9 => "area",
                    10 => "out_city",
                    11 => "opening_hours_google",
                ),
                "CHECK_DATES" => "N", // Показывать только активные на данный момент элементы
                "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                "AJAX_MODE" => "N", // Включить режим AJAX
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
                "CACHE_TYPE" => "Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
                "CACHE_TIME" => "36000079", // Время кеширования (сек.)
                "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
                "CACHE_GROUPS" => "N", // Учитывать права доступа
                "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
                "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                "SET_TITLE" => "N", // Устанавливать заголовок страницы
                "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
                "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
                "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => 'N', // Выводить под списком
                "PAGER_TITLE" => "Рестораны", // Название категорий
                "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
                "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
                "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
                "DISPLAY_DATE" => "Y", // Выводить дату элемента
                "DISPLAY_NAME" => "Y", // Выводить название элемента
                "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "N", // Выводить текст анонса
                "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
            ), false
            );
        }

        return json_encode($items);
    }

    public function getReadMe(){
        $docArr = array(
//            0=>'-> метод getPropsCode() используется для получения списка возможных свойств (их символьного кода)',
//            1=>'-> метод getPropValues($CODE, $CITY_CODE = "msk") используется для получения ID свойств, которые используются для фильтрации ресторанов: $CODE - код запрашиваемого св-ва; $CITY_CODE - код города (msk,spb,rga,urm)',
            'getRestaurantList'=>'-> метод getRestaurantList($CITY_CODE = "msk", $SORT_BY = "distance", $SORT_ORDER = "ASC", $SORT_BY2 = "", $SORT_ORDER2 = "", $COUNT = 1, $PAGE = "",
                $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = false,
                $KITCHEN = array(), $AVERAGE_BILL = array(), $SUBWAY = array(), $FEATURES = array(),$PROPOSALS_FEATURES = array(), $REST_GROUP = array(), $DISTRICT = array(), $OUT_OF_CITY = array(), $KEY_VALUE_FILTER = array())

                $SORT_BY - поле для первой сортировки, если используется значение distance, и не указаны координаты пользователя, будут использованы координаты центра города;
                возможные значения: NAME, PROPERTY_rating_date(рейтинг), PROPERTY_stat_day(Просмотров за день)
                сортировка для популярных, как на сайте "SORT_BY1" => "PROPERTY_rating_date", "SORT_ORDER1" => "DESC", "SORT_BY2" => "PROPERTY_stat_day" "SORT_ORDER2" => "DESC"

                $SORT_ORDER - направление сортировки (ASC,DESC)
                $SORT_BY2 - поле для второй сортировки
                SORT_ORDER2 - направление второй сортировки (ASC,DESC)
                $COUNT - количество на странице
                $PAGE - страница, оставлять пустой, если требуется только первая
                $USER_LATITUDE - широта
                $USER_LONGITUDE - долгота
                $RADIUS - радиус в метрах
                $KITCHEN - массив id типа кухни
                $AVERAGE_BILL - массив id типа серднего счета
                $SUBWAY - массив id метро
                $FEATURES - массив id особенностей
                $PROPOSALS_FEATURES - массив id предложений
                $REST_GROUP - массив id ресторанных групп
                $DISTRICT - массив id районов
                $OUT_OF_CITY - массив id пригородов
                $KEY_VALUE_FILTER - ассоциативный массив, получаемый из метода getFeaturesList(), отменяет фильтрацию по остальным параметрам
                ',
            'getDetailRestaurant'=>'-> метод getDetailRestaurant($ID) детальная информация по ресторану, $ID - id ресторана',
//            4=>'-> метод getRestaurantReviews($ID,$COUNT = 5) отзывы ресторана',
            'getRestaurantMenu'=>'-> метод getRestaurantMenu($ID) меню ресторана',
            'sendRestaurantReview'=>'-> метод sendRestaurantReview($ID, $USER_NAME, $EMAIL, $RATING, $COMMENT, $PHOTO_STR="") отправить отзыв в ресторан',
            'bBronRestaurant'=>'-> метод bBronRestaurant($ID, $date, $time, $guets, $name, $phone, $push_token, $wish="") сделать бронь, $time - время например в таком формате 12:00',
            'searchRestByTitle'=>'-> метод searchRestByTitle($queryStr, $CITY_CODE="spb") - поиск ресторанов по строке',
            'getCitiesInf'=>'-> метод getCitiesInf() - краткая информация по городам',
            'getPropsForCity'=>'-> метод getPropsForCity($CITY_CODE, $PROP_CODE = "") - свойства и значения свойств для фильтрации ресторанов, выводятся в зависимости от города;
                    $PROP_CODE - код необходимого свойства',
            'getRestaurantListOnMap'=>'-> метод getRestaurantListOnMap($CITY_CODE = "msk", $USER_LATITUDE = false, $USER_LONGITUDE = false, $RADIUS = 1000, $KITCHEN = array(), $AVERAGE_BILL = array(),
                $SUBWAY = array(), $FEATURES = array(),$PROPOSALS_FEATURES = array(), $REST_GROUP = array(), $DISTRICT = array(), $OUT_OF_CITY = array())
                    - поиск ресторанов на карте, если координаты не заполненны, берутся координаты центра города, $KEY_VALUE_FILTER - ассоциативный массив, получаемый из метода getFeaturesList(), отменяет фильтрацию по остальным параметрам',
            'getRestaurantReviewsNEW'=>'-> метод getRestaurantReviewsNEW($ID,$COUNT = 5,$PAGE = 1) - отзывы ресторана',
            'getCollectionList'=>'-> метод getCollectionList($CITY_CODE, $COLLECTION_ID) - рестораны подборки (первые 20 ресторанов определенных подборок, например панормамный вид, кальяны, детская комната)',
            'getCollectionsForCity'=>'-> метод getCollectionsForCity($CITY_CODE) - подбоки ресторанов для города (первые 20 ресторанов определенных подборок, например панормамный вид, кальяны, детская комната)',
            'searchRestByTitleNEW'=>'-> метод searchRestByTitleNEW($queryStr,$CITY_CODE="spb",$COUNT = 6) - полнотекстовый поиск ресторанов',
            'getRestaurantReviewsPhotos'=>'-> метод getRestaurantReviewsPhotos($ID,$COUNT = 5,$PAGE = 1) - фото пользователей для ресторана',
            'getFeaturesList'=>'-> метод getFeaturesList($CITY_CODE = "msk") - актуальные особенности, возвращает значения:
                COLLECTION_VALUE - id подборки, если особенностью является только одна страница подборки - передается в метод getCollectionList(...) ,
                PROP_CODE - код особенности - ключ для фильтрации списка ресторанов,
                PROP_VALUE - значение особенности; PROP_CODE и PROP_VALUE передается в getRestaurantList() или getRestaurantListOnMap() параметром $KEY_VALUE_FILTER = array(PROP_CODE => PROP_VALUE), как ключ-значение,
                при использовании этого поля, фильтрация по другим полям не производится
                ',

        );
        return json_encode($docArr);
    }

    public function getFeaturesList($CITY_CODE = 'msk'){
        global $APPLICATION, $USER;
        $arIB_best = getArIblock("filter_features", $CITY_CODE);
        $items = $APPLICATION->IncludeComponent(
            "mobile-app:element.list",
            "",
            Array(
                'IBLOCK_TYPE'=>'filter_features',
                "IBLOCK_ID" => $arIB_best['ID'],
                "CACHE_TYPE" => $USER->IsAdmin()?"N":"A",//
                "CACHE_TIME" => "36000002",
                "PARENT_SECTION_CODE" => "restaurants",
                "PROPERTY_CODE" => array('FEATURES','FIRST_20','ICON'),
            ),
            false
        );
        return json_encode($items);
        //PROP_CODE, COLLECTION_VALUE, PROP_VALUE
    }
    public function getFeaturesListNEW($CITY_CODE = 'msk'){
        $items['RESTORAN_ITEMS'] = array(
            Array("PROP_CODE"=>"features","NAME"=>"Круглосуточные","PROP_VALUE"=>2392461),
            Array("PROP_CODE"=>"features","NAME"=>"Панорамный вид","PROP_VALUE"=>6766),
            Array("PROP_CODE"=>"children","NAME"=>"Детская комната","PROP_VALUE"=>184520),
            Array("PROP_CODE"=>"children","NAME"=>"Детские программы","PROP_VALUE"=>185511),
            Array("PROP_CODE"=>"proposals","NAME"=>"Кальяны","PROP_VALUE"=>444604),
            Array("PROP_CODE"=>"breakfast","NAME"=>"Завтраки","PROP_VALUE"=>"Y"),
            Array("PROP_CODE"=>"entertainment","NAME"=>"Спорт на большом экране","PROP_VALUE"=>2013),
            Array("PROP_CODE"=>"entertainment","NAME"=>"Караоке","PROP_VALUE"=>1290),
            Array("PROP_CODE"=>"features","NAME"=>"Действующий камин","PROP_VALUE"=>151655),
            Array("PROP_CODE"=>"type","NAME"=>"Пивной ресторан","PROP_VALUE"=>432588),
            Array("PROP_CODE"=>"features","NAME"=>"Отдельный кабинет","PROP_VALUE"=>1059243),
            Array("PROP_CODE"=>"features","NAME"=>"У воды","PROP_VALUE"=>6784),
            Array("PROP_CODE"=>"features","NAME"=>"Танцпол","PROP_VALUE"=>6628),
            Array("PROP_CODE"=>"features","NAME"=>"Разрешено с питомцами","PROP_VALUE"=>6797),
        );

//        global $USER;
//        if($CITY_CODE=='msk'&&$USER->IsAdmin()){
//            $items['RESTORAN_ITEMS'] = array_merge($items['RESTORAN_ITEMS'], Array("PROP_CODE"=>"","NAME"=>"Круглосуточные","PROP_VALUE"=>'','COLLECTION_VALUE'=>312866,'PICTURE'=>''));
//        }
//        elseif($CITY_CODE=='spb'&&$USER->IsAdmin()){
//            $items['RESTORAN_ITEMS'] = array_merge($items['RESTORAN_ITEMS'], Array("PROP_CODE"=>"","NAME"=>"Круглосуточные","PROP_VALUE"=>'','COLLECTION_VALUE'=>316039,'PICTURE'=>''));
//        }

        CModule::IncludeModule('iblock');
        foreach($items['RESTORAN_ITEMS'] as &$item){
            if(intval($item['PROP_VALUE'])){
                $res_obj = CIBlockElement::GetByID(intval($item['PROP_VALUE']));
                if($res = $res_obj->Fetch()){
                    if($res['PREVIEW_PICTURE']){
                        $file = CFile::GetFileArray($res['PREVIEW_PICTURE']);
                        $item['PICTURE'] = $file['SRC'];
                    }
                }
            }
            elseif($item['PROP_CODE']=='breakfast'){
                $res_obj = CIBlockElement::GetList(array(),array('IBLOCK_ID'=>5652, 'CODE'=>'breakfast'),false,array('nTopCount'=>1),array('IBLOCK_ID','ID','NAME','PREVIEW_PICTURE'));
                if($res = $res_obj->Fetch()){
                    if($res['PREVIEW_PICTURE']){
                        $file = CFile::GetFileArray($res['PREVIEW_PICTURE']);
                        $item['PICTURE'] = $file['SRC'];
                    }
                }
            }
        }

        return json_encode($items);
    }
}
/**read me**/
/**



 **/
/**read me**/

//$server->attach(new Api);
//
//echo $server->execute();




if(!$USER->IsAdmin()):

    $server = new Server;
    $server->authentication(array('admin' => '12345'));

    // Register the before callback
    //$server->before('beforeProcedure');


    $server->attach(new Api);
    //
    echo $server->execute();

else:

$a = new Api;
//$some = $a->getRestaurantList('spb','distance','ASC','','',10,1, 59.953713699999994, 30.378722999999994,false,array(),array(),array(),array(1921));

//$some = $a->getDetailRestaurant(385718);
//$some = $a->getRestaurantMenu(1982423);
//$some = $a->searchRestByTitle('Москв');
//$some = $a->getCollectionsForCity('msk');
//$some = $a->getRestaurantMenuNEW(1267803);

//$some = $a->getRestaurantListAnyIBlock('msk','distance','ASC',10);
//$some = $a->getRestaurantReviews(1962116);
//$some = $a->searchRestByTitle('Italy','spb');



//$some = $a->sendRestaurantReview(388337, 'Тест', 'ggreat@mail.ru', '4', 'Отличное место');
//$some = $a->sendRestaurantReview(2115970, 'Username', 'test@test.com', '4', 'Test review');
//$some = $a->getUserByPhone(9523969381);
//$some = $a->getPropsCode();
//$some = $a->getPropValues('features','msk');
//    $some = $a->getRestaurantList('msk','distance','ASC','','',20,1,false,false,false,array(184611),array(1953,1997),array(),array(1950),array(),array());
//    $some = $a->getRestaurantList('msk','distance','ASC','','',20,1,false,false,200,false,false,array(),false,array(),array());

//    $some = $a->getRestaurantListOnMap('spb',59.953713699999994, 30.378722999999994, false,array(),array(),array(),array(1921));
//    $some = $a->getRestaurantReviewsNew(388303);
//$some = $a->getReadMe();
//$some = $a->getPropsForCity('spb');
//$some = $a->getDetailRestaurant(2183791);

//$some = $a->getFeaturesList('msk');
//$some = $a->getFeaturesListNEW();
$some = $a->getCollectionsForCity('spb');
FirePHP::getInstance()->info($APPLICATION->GetCurDir());

//    $some = $a->bBronRestaurant(2265746, '19.02.2016', '21:38', 2, 'тест', '79992027488');
//    $some = $a->searchRestByTitleNEW('шеф-повер');

//$some = $a->getCollectionList('spb',316039);
//$some = $a->getRestaurantReviewsPhotos(1854244,false);

//    $some = $a->getRestaurantList('spb', 'NAME', 'ASC', '', '', 10, 1, false, false, false, array(), array(), array(), array(), array(), array(1128953), array(), array());
//$some = $a->getPropsForCity('urm','AVERAGE_BILL');
//$some = $a->getPropsForCity('msk','FEATURES');


//    $some = $a->getRestaurantList('spb','NAME','ASC','','',20,1,59.944187500000005,30.3658433,false,array(),array(),array(),array(),array(),array(),array(),array(),array('rest_group'=>1128953));


echo $some;
    /**
     * TODO getDetailRestaurant() - убрать book_counter и comments из кеша
     *
     **/
endif;
?>