<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Друзья");
?>

<?// pass users IDs for relationship?>
<?/*$APPLICATION->IncludeComponent(
    "restoran:user.friends_add",
    "",
    Array(
        "FIRST_USER_ID" => "1",
        "SECOND_USER_ID" => "3",
        "RESULT_CONTAINER_ID" => "add_friend_result"
    ),
    false
);*/?>
    <script>
        $(function () {
            $("ul.tabs").each(function () {
                if ($(this).attr("ajax") == "ajax") {
                    if ($(this).attr("history") == "true")
                        var history = true; else
                        var history = false;
                    var url = "";
                    if ($(this).attr("ajax_url"))
                        url = $(this).attr("ajax_url");
                    $(this).tabs("div.panes > .pane", {
                        history: history, onBeforeClick: function (event, i) {
                            var pane = this.getPanes().eq(i);
                            if (pane.is(":empty")) {
                                pane.load(url + this.getTabs().eq(i).attr("href"));
                            }
                        }
                    });
                }
                else {
                    if ($(this).attr("history") == "true")
                        var history = true; else
                        var history = false;
                    $(this).tabs("div.panes  .pane", {
                        history: history, onClick: function (event, i) {
                            $(".bx-yandex-map").each(function () {
                                var i = $(this).attr("id");
                                i = i.split('BX_YMAP_');
                                m = window.GLOBAL_arMapObjects[i[1]];
                                if (m)m.container.fitToViewport();
                            });
                        }
                    });
                }
            });
        })
    </script>
    <style>
        .panes {min-height:150px;}
        .panes div.pane { display:none; min-height:150px; padding:15px 0px;  margin-top:-3px; width:728px; line-height: 20px}
        .panes div span.date_size{ font-size: 1.6em}
        .panes div img{ margin-right:10px;margin-bottom:10px;}
        .poster_panes div img{ margin-right:10px;margin-bottom:10px;}
        .panes div a { color:#24a6cf; text-decoration: underline;}
        .panes div a:hover { color:#000; text-decoration: none;}
        .panes div a.uppercase { color:#1a1a1a; letter-spacing: 1px; text-decoration: none; }
        .panes div a.uppercase:hover { color:#24a6cf; text-decoration: none; }

        ul.tabs { margin:0 !important; padding:0; height:41px; width:728px;border-bottom:1px solid #000;}
        ul.tabs li { float:left; padding:0; margin:0; list-style-type:none;}
        ul.tabs li a {
            padding: 8px 36px;
            font-size: 16px;
            font-family: 'futurafuturiscregular';
            -webkit-text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
            -moz-text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
            text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
            margin-right: 2px;
            line-height: 1.42857143;
            border: 1px solid transparent;
            background: #f3f6f7;
            position: relative;
            display: block;
        }
        ul.tabs a.current {
            color: #000;
            background-color: #ffffff;
            border: 1px solid #000;
            border-top-width: 3px;
            border-bottom-color: transparent;
            border-bottom: 0px;
            cursor: default;
        }
        .more_cities {
            padding:0px 10px;
        }
    </style>
<style type="text/css">
    /* TODO tmp style */
    .wrap-div {width:975px; margin:0 auto;}
</style>
<div class="wrap-div">
    <div id="tabs_block6" class="tabs">
        <ul class="tabs big">
                <li>                                
                    <a href="#" class="current">
                        <div class="left tab_left"></div>
                        <div class="left name">Друзья</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>                                                                
                </li>
                <li><a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name">Подписки</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>                                    
                    </a>
                </li>
        </ul>
        <div class="panes">
            <div class="pane big" style="display: block; ">
                <?$APPLICATION->IncludeComponent("bitrix:socialnetwork.messages_requests","",Array(
                        "SET_NAVCHAIN" => "N", 
                        "PATH_TO_USER" => "/users/id#user_id#/", 
                        "PATH_TO_MESSAGE_FORM" => "", 
                        "PAGE_VAR" => "page", 
                        "USER_VAR" => "user_id", 
                        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/", 
                        "ITEMS_COUNT" => "30" 
                    )
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:socialnetwork.user_friends",
                    "user_friends_list",
                    Array(
                        "SET_NAV_CHAIN" => "N",
                        "ITEMS_COUNT" => "16",
                        "PATH_TO_USER" => "",
                        "PATH_TO_LOG" => "",
                        "PATH_TO_USER_FRIENDS_ADD" => "",
                        "PATH_TO_USER_FRIENDS_DELETE" => "",
                        "PATH_TO_SEARCH" => "",
                        "PAGE_VAR" => "",
                        "USER_VAR" => "",
                        "ID" => (int)$_REQUEST["USER_ID"],
                        "SET_TITLE" => "N"
                    ),
                    false
                );?>
            </div>
            <div class="pane big">
                <?$APPLICATION->IncludeComponent(
                    "restoran:subscribe.users",
                    "",
                    Array(
                        "USER_ID" => (int)$_REQUEST["USER_ID"]
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
    
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>