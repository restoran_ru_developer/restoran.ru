<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профиль пользователя");
?>
<div id="content">
    <div class="left" style="width:728px;"> 
        <?$APPLICATION->IncludeComponent("restoran:main.profile", "users_profile", Array(
            "ID" => (int)$_REQUEST["USER_ID"],
            "USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "USER_PROPERTY" => Array("UF_HIDE_CONTACTS"),	// Показывать доп. свойства
            "SEND_INFO" => "N",	// Генерировать почтовое событие
            "CHECK_RIGHTS" => "N",	// Проверять права доступа
            "AJAX_OPTION_SHADOW" => "Y",	// Включить затенение
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            ),
            false
        );?>       
    </div>
    <div class="right">         
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>