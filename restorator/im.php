<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обмен сообщениями");
?>
<?
// get current user ID
$userID = $USER->GetID();
?>
<div id="content">
<h1><?=$APPLICATION->ShowTitle(false)?></h1>    
        <?if (!$_REQUEST["SECOND_USER_ID"]):?>
            <ul class="nav nav-tabs">
                    <li class="active"><a href="#input" data-toggle="tab">Входящие</a></li>
                    <li><a href="#output" data-toggle="tab">Отправленные</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane medium active" id="input">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:socialnetwork.messages_input",
                    "user_in_msg",
                    Array(
                        "MESSAGE_LENGTH" => "150",
                        "SET_NAVCHAIN" => "N",
                        "MESSAGE_VAR" => "",
                        "PATH_TO_USER" => "",
                        "PATH_TO_MESSAGE_FORM" => "",
                        "PATH_TO_MESSAGE_FORM_MESS" => "",
                        "PATH_TO_MESSAGES_INPUT" => "",
                        "PATH_TO_MESSAGES_INPUT_USER" => "",
                        "PAGE_VAR" => "",
                        "USER_VAR" => "",
                        "USER_ID" => $userID,
                        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
                        "ITEMS_COUNT" => "30"
                    ),
                    false
                );?>                
                </div>
                <div class="tab-pane medium" id="output">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:socialnetwork.messages_output",
                        "user_out_msg",
                        Array(
                            "MESSAGE_LENGTH" => "150",
                            "SET_NAVCHAIN" => "N",
                            "MESSAGE_VAR" => "",
                            "PATH_TO_USER" => "",
                            "PATH_TO_MESSAGE_FORM" => "",
                            "PATH_TO_MESSAGE_FORM_MESS" => "",
                            "PATH_TO_MESSAGES_INPUT" => "",
                            "PATH_TO_MESSAGES_INPUT_USER" => "",
                            "PAGE_VAR" => "",
                            "USER_VAR" => "",
                            "USER_ID" => $userID,
                            "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
                            "ITEMS_COUNT" => "30"
                        ),
                        false
                    );?>
                </div>
            </div>
            <div class="write_message">
                <script>
                    $(document).ready(function(){
                        $("#write_message").click(function(){
                            var _this = $(this);
                            $.ajax({
                                type: "POST",
                                url: "/tpl/ajax/im.php",
                                data: "<?=bitrix_sessid_get()?>",
                                success: function(data) {
                                    if (!$("#mail_modal").size())
                                    {
                                        $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                                    }
                                    $('#mail_modal').html(data);
                                    showOverflow();
                                    setCenter($("#mail_modal"));
                                    $("#mail_modal").fadeIn("300"); 
                                }
                            });
                            return false;
                        });
                    });
                </script>
                <input type="button" id="write_message" class="btn btn-info" value="+ Написать сообщение" />
            </div>
        <?else:?>
            <?$APPLICATION->IncludeComponent(
                    "restoran:message.list",
                    "user_in_msg",
                    Array(
                        "MESSAGE_LENGTH" => "150",
                        "SET_NAVCHAIN" => "N",
                        "MESSAGE_VAR" => "",//sj
                        "PATH_TO_USER" => "",
                        "PATH_TO_MESSAGE_FORM" => "",
                        "PATH_TO_MESSAGE_FORM_MESS" => "",
                        "PATH_TO_MESSAGES_INPUT" => "",
                        "PATH_TO_MESSAGES_INPUT_USER" => "",
                        "PAGE_VAR" => "",
                        "USER_VAR" => "",
                        "SECOND_USER_ID" => $_REQUEST["SECOND_USER_ID"],
                        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
                        "ITEMS_COUNT" => "30"
                    ),
                    false
                );?>
        <?endif;?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>