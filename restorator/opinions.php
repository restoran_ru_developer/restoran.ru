<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
$arOverviewsIB = getArIblock("reviews", CITY_ID);
if ($_REQUEST["CODE"]):
    $id = RestIBlock::GetRestIDByCode($_REQUEST["CODE"]);
    if (!$id)
        $id = 1;
endif;
?>
<?/*$APPLICATION->IncludeComponent("restoran:blog.new_comments", ".default", array(
	"GROUP_ID" => "9",
	"BLOG_URL" => "",
	"COMMENT_COUNT" => "20",
	"MESSAGE_LENGTH" => "100",
	"POST_ID" => $_REQUEST["POST_ID"],
	"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
	"PATH_TO_BLOG" => "",
	"PATH_TO_POST" => "",
	"PATH_TO_USER" => "",
	"PATH_TO_GROUP_BLOG_POST" => "",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "86400",
	"PATH_TO_SMILE" => "",
	"BLOG_VAR" => "",
	"POST_VAR" => "",
	"USER_VAR" => "",
	"PAGE_VAR" => "",
	"SEO_USER" => "N"
	),
	false
);*/?>
<div id="content">
	<div class="left" style="width:728px;position:relative">
            <h1 id="opinion_h1" class="with_link">
                <?if (!$_REQUEST["tid"]):?>
                    <a href="javascript:void(0)" class="js" onclick="$('#reviews').slideUp(300)"><?=$APPLICATION->ShowTitle(false)?></a>                    
                <?else:?>
                    <?=$APPLICATION->ShowTitle(false)?>                     
                <?endif;?>
            </h1>
                <?if (!$_REQUEST["tid"]):?>
                    <div class="sorting" style="top:10px">
                        <div class="left">
                            <?                        
                            $excUrlParams = array("video", "photos","?PAGEN_1");
                            $with_photo = $APPLICATION->GetCurPageParam("photos=Y", $excUrlParams);
                            $with_video = $APPLICATION->GetCurPageParam("video=Y", $excUrlParams);       
                            ?>
                            <?if ($_REQUEST["photos"]!="Y"):?>
                                <span><a class="another" href="<?=$with_photo?>"> с фото</a></span>    
                            <?else:?>
                                <span> с фото</span>    
                            <?endif;?>
                            / 
                            <?if ($_REQUEST["video"]!="Y"):?>
                                <span><a class="another" href="<?=$with_video?>"> с видео</a></span>                    
                            <?else:?>
                                <span> с видео</span>                    
                            <?endif;?>
                        </div>
                        <div class="right"></div>                    
                        <div class="clear"></div>
                    </div>
                <?endif;?>
		<?
                if (!$_REQUEST["tid"]):                                       
                    if ($id)
                    {
                        global $arrFilter;
                        $arrFilter = array();
                        $arrFilter["PROPERTY_ELEMENT"] = $id;
                    }
                    if ($_REQUEST["photos"]=="Y")
                    {
                        global $arrFilter;
                        $arrFilter["!PROPERTY_photos"] = false;
                    }
                    if ($_REQUEST["video"]=="Y")
                    {
                        global $arrFilter;
                        $arrFilter[0] = Array("LOGIC"=>"OR",
                            Array("!PROPERTY_video"=>false),
                            Array("!PROPERTY_video_youtube"=>false)
                        );                        
                    }?>

                <?
                    if($_REQUEST['CODE']):
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "reviews",
                            Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "reviews",
                                "IBLOCK_ID" => $arOverviewsIB["ID"],
                                "NEWS_COUNT" => ($_REQUEST["pageCnt"]) ? $_REQUEST["pageCnt"] : "20",
                                "SORT_BY1" => "created_date",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PAGE_URL"),
                                "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "video_youtube", "COMMENTS"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "3600000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "NO_CACHE_TEMPLATE" => "Y"
                            ),
                            false
                        );
                    else:?>
                        <p>Отсутствует символьный код ресторана</p>
                    <?endif;
                else:?>
                <?
                global $arrFilter;                
                $arrFilter = array();
                $old_id = RestIBlock::getOldOpinionID((int)$_REQUEST["tid"]);
                if ($old_id)
                {                           
                    LocalRedirect($APPLICATION->GetCurPageParam("tid=".$old_id, array("tid","CITY_ID")),true,"301 Moved permanently");
                    $arrFilter["ID"] = $old_id;
                }
                else
                    $arrFilter["ID"] = $_REQUEST["tid"];
?>

                    <?


                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "review",
                    Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "reviews",
                        "IBLOCK_ID" => $arOverviewsIB["ID"],
                        "NEWS_COUNT" => "1",
                        "SORT_BY1" => "created_date",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrFilter",
                        "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY"),
                        "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "video_youtube", "COMMENTS"),
                        "CHECK_DATES" => "N",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "search_rest_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                    ),
                    false
                );

                        ?>
                <?if (ERROR_404!="Y"):?>
                    <div  id="comments" >
                        <?
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
                        $APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
                        ?>
                        <link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
                        <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
                        <script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
                        <script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
                        <script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
                        <link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />
                        <script type="text/javascript">
                                $(document).ready(function(){
                                        $('.QapTcha').QapTcha({
                                                txtLock : 'Сдвиньте слайдер вправо.',
                                                txtUnlock : 'Готово. Теперь можно отправлять',
                                                disabledSubmit : true,
                                                autoRevert : true,
                                                PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
                                                autoSubmit : false});


                                        $(".fancy").fancybox({
                                            'transitionIn'	:	'elastic',
                                            'transitionOut'	:	'elastic',
                                            'cyclic'            :       true,
                                            'overlayColor'      :       '#1a1a1a',
                                            'speedIn'		:	200, 
                                            'speedOut'		:	200, 
                                            //'overlayShow'	:	false
                                        });
                                });
                        </script>
                        <?
//                        if (!$USER->IsAdmin()):
//                            $APPLICATION->IncludeComponent(
//                                        "restoran:comments_add",
//                                        "review_comment",
//                                        Array(
//                                                "IBLOCK_TYPE" => "comment",
//                                                "IBLOCK_ID" => 2438,
//                                                "ELEMENT_ID" => $arrFilter["ID"],
//                                                "IS_SECTION" => "N",      
//                                                "MY_CAPTCHA" => "Y",
//                                                "OPEN" => "Y"
//                                        ),false
//                            );
//                            $APPLICATION->IncludeComponent(
//                                    "restoran:comments",
//                                    "review_comment",
//                                    Array(
//                                        "IBLOCK_TYPE" => "comment",
//                                        "ELEMENT_ID" => $arrFilter["ID"],
//                                        "IBLOCK_ID" => $arCommentIB["ID"],
//                                        "IS_SECTION" => "N",
//                                        "ADD_COMMENT_TEMPLATE" => "",
//                                        "COUNT" => 999,
//                                        "CACHE_TYPE" => "A",
//                                        "CACHE_TIME" => "36000000",
//                                        "CACHE_FILTER" => "Y",
//                                        "CACHE_GROUPS" => "Y",
//                                    ),
//                                false
//                                );
//                        else:
                            $APPLICATION->IncludeComponent(
                                    "restoran:comments_add_new",
                                    "new",
                                    Array(
                                            "IBLOCK_TYPE" => "comment",
                                            "IBLOCK_ID" => (SITE_ID=="s1")?"2438":"2641",
                                            "ELEMENT_ID" => $arrFilter["ID"],
                                            "IS_SECTION" => "N",       
                                            "OPEN" => "Y"
                                    ),false
                            );
                            echo "<br />";
                            $APPLICATION->IncludeComponent(
                                    "restoran:comments",
                                    "review_comment_new",
                                    Array(
                                        "IBLOCK_TYPE" => "comment",
                                        "ELEMENT_ID" => $arrFilter["ID"],
                                        "IBLOCK_ID" => $arCommentIB["ID"],
                                        "IS_SECTION" => "N",
                                        "ADD_COMMENT_TEMPLATE" => "",
                                        "COUNT" => 999,
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "Y",
                                        "PROPERTY_CODE" => Array("photos","video")
                                    ),
                                false
                            );
                        //endif;
                        ?>
                    </div>
                <?endif;?>
            <?endif;?>
	</div>
	<div class="right" style="width: 240px">
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
                <br /><Br />
                <div class="top_block">Популярные</div>
            <?
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arRestIB = getArIblock("catalog", CITY_ID);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "5",
                        "SORT_BY1" => "PROPERTY_rating_date",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "PROPERTY_stat_day",
                        "SORT_ORDER2" => "DESC",
                        "SORT_BY3" => "NAME",
                        "SORT_ORDER3" => "ASC",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?>
                <br /><br />
            <div class="top_block">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            //$arrFilterSim["PROPERTY_RECOMENDED_VALUE"] = "Да";
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arPFilter["!PREVIEW_PICTURE"] = false;
            $arPFilter["!PROPERTY_restoran_ratio"] = false;  
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "PROPERTY_restoran_ratio",
                        "SORT_ORDER1" => "asc,nulls",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "10",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended"
                ),
            false
            );?>
                <br /><Br/>
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
                <br /><Br />
                <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br /><Br/>
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
                <br /><Br />
	</div>
	<div class="clear"></div>
        <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
	<br /><Br />
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>