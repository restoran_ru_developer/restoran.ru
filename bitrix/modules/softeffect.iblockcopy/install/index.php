<?
global $MESS;
Class softeffect_iblockcopy extends CModule
{
	var $MODULE_ID = "softeffect.iblockcopy";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	var $errors;

	function softeffect_iblockcopy() {
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = xml_inp_VERSION;
			$this->MODULE_VERSION_DATE = xml_inp_VERSION_DATE;
		}
        IncludeModuleLangFile(__FILE__);
		
	
	
		$this->MODULE_NAME =GetMessage("IBLOCK_INSTALL_COPY_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("IBLOCK_INSTALL_COPY_DESCRIPTION");
		
	
		// ������� ��� ����������� ��������:
		$softeffect = GetMessage('IBLOCK_INSTALL_COPY_COMPANI_NAME');
		$this->PARTNER_NAME = "$softeffect";
		
		
		
		
		$this->PARTNER_URI = GetMessage("IBLOCK_INSTALL_COPY_COMPANI_SITE");
		
	
	}

	function InstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/softeffect.iblockcopy/install/admin', $_SERVER['DOCUMENT_ROOT']."/bitrix/admin");
			
		
			
		}
		return true;
	}

	function UnInstallFiles()
	{
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/softeffect.iblockcopy/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			
		}
		return true;
	}


	function DoInstall() {
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule("softeffect.iblockcopy");
		
		if (CModule::IncludeModule('security')) {
			$arWhiteList=array();
			$rs = CSecurityAntiVirus::GetWhiteList();
			while($ar = $rs->Fetch()) {
				if ($ar["WHITE_SUBSTR"]!='function OnTypeChanged1' && $ar["WHITE_SUBSTR"]!='function checkNew') {
					$arWhiteList[] = $ar["WHITE_SUBSTR"];
				}
			}
			
			$arWhiteList[]="function OnTypeChanged1";
			$arWhiteList[]="function checkNew";
			
			CSecurityAntiVirus::UpdateWhiteList($arWhiteList);
		}
		
		$APPLICATION->IncludeAdminFile(GetMessage("IBLOCK_INSTALL_COPY_M"), $DOCUMENT_ROOT."/bitrix/modules/softeffect.iblockcopy/install/step.php");
	}

	function DoUninstall() {
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallFiles();
		
		CModule::IncludeModule('security');
		$arWhiteList=array();
		$rs = CSecurityAntiVirus::GetWhiteList();
		while($ar = $rs->Fetch()) {
			if ($ar["WHITE_SUBSTR"]!='function OnTypeChanged1' && $ar["WHITE_SUBSTR"]!='function checkNew') {
				$arWhiteList[] = $ar["WHITE_SUBSTR"];
			}
		}
		
		CSecurityAntiVirus::UpdateWhiteList($arWhiteList);
		
		UnRegisterModule("softeffect.iblockcopy");
		$APPLICATION->IncludeAdminFile(GetMessage("IBLOCK_INSTALL_COPY_DEINSTAL"), $DOCUMENT_ROOT."/bitrix/modules/softeffect.iblockcopy/install/unstep.php");
	}
	
	function InstallDB($install_wizard = true)
	{
		return true;
	}
	
}
?>