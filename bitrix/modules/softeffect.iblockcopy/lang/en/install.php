<?
$MESS ['xml_inp_INSTALL_NAME'] = "Модуль импорта из 111 файлов";
$MESS ['xml_inp_INSTALL_DESCRIPTION'] = "Модуль работы с информационными блоками позволяет управлять и каталогизировать информацию различного характера - новости, вакансии, список продуктов.";
$MESS ['xml_inp_INSTALL_TITLE'] = "Установка модуля работы с информационными блоками";

$MESS ['IBLOCK_INSTALL_COPY_NAME'] = "Module copying the block with properties";
$MESS ['IBLOCK_INSTALL_COPY_DESCRIPTION'] = "The module allows you to copy information blocks and create new ones on the basis of existing ones. Indispensable in the creation of sites in several languages.";
$MESS ['IBLOCK_INSTALL_COPY_COMPANI_NAME'] = "«Softeffect»";
$MESS ['IBLOCK_INSTALL_COPY_COMPANI_SITE'] = "http://www.softeffect.ru";
$MESS ['IBLOCK_INSTALL_COPY_M'] = "Installation of the module softeffect.iblockcopy";
$MESS ['IBLOCK_INSTALL_COPY_DEINSTAL'] = "Uninstalling a softeffect.iblockcopy";
?>