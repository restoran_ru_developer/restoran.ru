<?
$MESS ['IBLOCK_MENU_ITYPE'] = "Type of information blocks";
$MESS ['IBLOCK_MENU_EXPORT'] = "Export";
$MESS ['IBLOCK_MENU_IMPORT'] = "Import";
$MESS ['IBLOCK_MENU_ALL_EL'] = "All of the elements";
$MESS ['IBLOCK_MENU_ALL_OTH'] = "Other...";
$MESS ['IBLOCK_MENU_ALL_OTH_TITLE'] = "A complete list of sections";
$MESS ['IBLOCK_MENU_SEC_EL'] = "Elements of the group";
$MESS ['IBLOCK_MENU_ITYPE_TITLE'] = "Type of information blocks";
$MESS ['IBLOCK_MENU_SETTINGS_TITLE'] = "Setting up of information blocks";
$MESS ['IBLOCK_MENU_ELEMENTS'] = "Elements";
$MESS ['IBLOCK_MENU_SEPARATOR'] = "Information blocks";
$MESS ['IBLOCK_MENU_COPI_IB'] = "Copy IB";
$MESS ['IBLOCK_MENU_COPI_IB_POL'] = "Copying of the information block";
?>