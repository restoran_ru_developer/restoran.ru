<?
$MESS ['IBLOCK_ADM_IMP_NO_DATA_FILE'] = "Файл с данными не загружен и не выбран. Загрузка невозможна.";
$MESS ['IBLOCK_ADM_IMP_NO_IBLOCK'] = "Информационный блок не выбран. Загрузка невозможна.";
$MESS ['IBLOCK_ADM_IMP_NO_FILE_FORMAT'] = "Укажите формат файла данных и его свойства.";
$MESS ['IBLOCK_ADM_IMP_NO_DELIMITER'] = "Укажите символ-разделитель полей.";
$MESS ['IBLOCK_ADM_IMP_NO_METKI'] = "Укажите метки разделителя для определения границ полей.";
$MESS ['IBLOCK_ADM_IMP_NO_DATA'] = "Файл данных не содержит данных. Загрузка невозможна.";
$MESS ['IBLOCK_ADM_IMP_NO_FIELDS'] = "Укажите соответствия для полей.";
$MESS ['IBLOCK_ADM_IMP_NOMAME'] = "<Пустое название>";
$MESS ['IBLOCK_ADM_IMP_LINE_NO'] = "Строка";
$MESS ['IBLOCK_ADM_IMP_NOIDNAME'] = "Невозможно идентифицировать элемент: название и уникальный код не установлены.";
$MESS ['IBLOCK_ADM_IMP_ERROR_LOADING'] = "Ошибка загрузки элемента:";
$MESS ['IBLOCK_ADM_IMP_TOTAL_ERRS'] = "Итого записей с ошибками:";
$MESS ['IBLOCK_ADM_IMP_TOTAL_COR1'] = "Загружено корректно";
$MESS ['IBLOCK_ADM_IMP_TOTAL_COR2'] = "записей.";


$MESS ['IBLOCK_ADM_IMP_DATA_FILE'] = "Файл данных:";
$MESS ['IBLOCK_ADM_IMP_INFOBLOCK'] = "Информационный блок:";
$MESS ['IBLOCK_ADM_IMP_RAZDELITEL'] = "с разделителями - поля разделяются специальным символом";
$MESS ['IBLOCK_ADM_IMP_FIXED'] = "фиксированная ширина полей";
$MESS ['IBLOCK_ADM_IMP_RAZDEL1'] = "С разделителями";
$MESS ['IBLOCK_ADM_IMP_RAZDEL_TYPE'] = "Разделитель полей";
$MESS ['IBLOCK_ADM_IMP_TAB'] = "табуляция";
$MESS ['IBLOCK_ADM_IMP_TZP'] = "точка с запятой";
$MESS ['IBLOCK_ADM_IMP_ZPT'] = "запятая";
$MESS ['IBLOCK_ADM_IMP_SPS'] = "пробел";
$MESS ['IBLOCK_ADM_IMP_OTR'] = "другой";
$MESS ['IBLOCK_ADM_IMP_FIRST_NAMES'] = "Первая строка содержит имена полей";
$MESS ['IBLOCK_ADM_IMP_FIX1'] = "Фиксированная ширина полей";
$MESS ['IBLOCK_ADM_IMP_FIX_MET'] = "Метки разделителя";
$MESS ['IBLOCK_ADM_IMP_FIX_MET_DESCR'] = "Укажите в столбик номера колонок, которые разделяют поля";
$MESS ['IBLOCK_ADM_IMP_DATA_SAMPLES'] = "Образцы данных";
$MESS ['IBLOCK_ADM_IMP_FIELDS_SOOT'] = "Задайте соответствие полей в файле полям в базе";
$MESS ['IBLOCK_ADM_IMP_FI_NAME'] = "Название";
$MESS ['IBLOCK_ADM_IMP_FI_ACTIV'] = "Активность";
$MESS ['IBLOCK_ADM_IMP_FI_ACTIVFROM'] = "Активность с";
$MESS ['IBLOCK_ADM_IMP_FI_ACTIVTO'] = "Активность до";
$MESS ['IBLOCK_ADM_IMP_FI_CATIMG'] = "Картинка для списка";
$MESS ['IBLOCK_ADM_IMP_FI_CATDESCR'] = "Описание для списка";
$MESS ['IBLOCK_ADM_IMP_FI_DETIMG'] = "Картинка";
$MESS ['IBLOCK_ADM_IMP_FI_DETDESCR'] = "Описание";
$MESS ['IBLOCK_ADM_IMP_FI_UNIXML'] = "Уникальный идентификатор";
$MESS ['IBLOCK_ADM_IMP_FI_PROPS'] = "Свойство";
$MESS ['IBLOCK_ADM_IMP_FI_GROUP_LEV'] = "Группа уровня";
$MESS ['IBLOCK_ADM_IMP_FIELD'] = "Поле";
$MESS ['IBLOCK_ADM_IMP_ADDIT_SETTINGS'] = "Дополнительные настройки";
$MESS ['IBLOCK_ADM_IMP_IMG_PATH'] = "Путь к картинкам";
$MESS ['IBLOCK_ADM_IMP_IMG_PATH_DESCR'] = "Полный путь будет формироваться от корня сайта как \"&lt;путь к картинкам&gt;/&lt;имя файла&gt;\"";
$MESS ['IBLOCK_ADM_IMP_OUTFILE'] = "Товары, которых не было в файле";
$MESS ['IBLOCK_ADM_IMP_OF_DEACT'] = "деактивировать";
$MESS ['IBLOCK_ADM_IMP_OF_DEL'] = "удалить";
$MESS ['IBLOCK_ADM_IMP_OF_KEEP'] = "не трогать";
$MESS ['IBLOCK_ADM_IMP_SUCCESS'] = "Загрузка завершена";
$MESS ['IBLOCK_ADM_IMP_SU_ALL'] = "Всего обработано строк:";
$MESS ['IBLOCK_ADM_IMP_SU_CORR'] = "Из них полностью корректных:";
$MESS ['IBLOCK_ADM_IMP_SU_ER'] = "С ошибками:";


$MESS ['IBLOCK_ADM_IMP_SU_KILLED'] = "Удалено устаревших элементов:";
$MESS ['IBLOCK_ADM_IMP_SU_HIDED'] = "Деактивировано устаревших элементов:";
$MESS ['IBLOCK_ADM_IMP_AUTO_REFRESH'] = "Если страница не обновляется автоматически, нажмите на ссылку";
$MESS ['IBLOCK_ADM_IMP_AUTO_REFRESH_STEP'] = "Следующий шаг";
$MESS ['IBLOCK_ADM_IMP_INACTIVE_PRODS'] = "Неактивные элементы / группы, которые есть в файле";
$MESS ['IBLOCK_ADM_IMP_KEEP_AS_IS'] = "оставить как есть";
$MESS ['IBLOCK_ADM_IMP_ACTIVATE_PROD'] = "активировать";
$MESS ['IBLOCK_ADM_IMP_AUTO_STEP_TIME'] = "Время выполнения шага";
$MESS ['IBLOCK_ADM_IMP_AUTO_STEP_TIME_NOTE'] = "0 - загрузить все сразу<br> положительное значение - число секунд на выполнение одного шага";
$MESS ['IBLOCK_ADM_IMP_AUTO_REFRESH_CONTINUE'] = "Продолжение пошаговой загрузки...";
$MESS ['IBLOCK_ADM_IMP_NOT_CSV'] = "Загружаемый файл на является CSV файлом";


$MESS ['IBLOCK_ADM_IMP_TAB3'] = "Соответствие полей";
$MESS ['IBLOCK_ADM_IMP_TAB3_ALT'] = "Соответствие свойств Файл - Инфоблок";
$MESS ['IBLOCK_ADM_IMP_TAB4'] = "Загружаемые элементы";
$MESS ['IBLOCK_ADM_IMP_TAB4_ALT'] = "Соответствие свойств Файл - 1С-Битрикс";
$MESS ['IBLOCK_ADM_IMP_TAB5'] = "Результат";
$MESS ['IBLOCK_ADM_IMP_TAB5_ALT'] = "Результат импорта";
$MESS ['IBLOCK_ADM_IMP_OPEN'] = "Открыть";
$MESS ['IBLOCK_ADM_IMP_2_1_STEP'] = "Вернуться на первый шаг";
$MESS ['IBLOCK_ADM_IMP_FI_SORT'] = "Индекс сортировки";
$MESS ['IBLOCK_ADM_IMP_FI_CATDESCRTYPE'] = "Тип описания для списка";
$MESS ['IBLOCK_ADM_IMP_FI_DETDESCRTYPE'] = "Тип описания";
$MESS ['IBLOCK_ADM_IMP_FI_CODE'] = "Мнемонический код";
$MESS ['IBLOCK_ADM_IMP_FI_TAGS'] = "Теги";
$MESS ['IBLOCK_ADM_IMP_FI_ID'] = "Первичный ключ";
$MESS ['IBLOCK_ADM_IMP_FG_NAME'] = "Название группы";
$MESS ['IBLOCK_ADM_IMP_FG_UNIXML'] = "Уникальный идентификатор";
$MESS ['IBLOCK_ADM_IMP_FG_ACTIV'] = "Активность";
$MESS ['IBLOCK_ADM_IMP_FG_SORT'] = "Индекс сортировки";
$MESS ['IBLOCK_ADM_IMP_FG_DESCR'] = "Описание";
$MESS ['IBLOCK_ADM_IMP_FG_DESCRTYPE'] = "Тип описания";
$MESS ['IBLOCK_ADM_IMP_FG_CODE'] = "Мнемонический код";
$MESS ['IBLOCK_ADM_IMP_IMG_RESIZE'] = "Использовать настройки инфоблока для обработки изображений";
$MESS ['IBLOCK_ADM_IMP_PROP_PATH'] = "Путь к файлам свойств";
$MESS ['IBLOCK_ADM_IMP_PROP_PATH_DESCR'] = "Полный путь будет формироваться от корня сайта как \"&lt;путь к файлам свойств&gt;/&lt;имя файла&gt;\"";



$MESS ['ONLY_XLS_XLSX'] = "Формат файла должен быть xls или xlsx";
$MESS ['IBLOCK_CHOOSE_SECTION'] = "(выберите раздел)";


$MESS ['XLS_PROFILE_HEADER'] = "Профиль загрузки";
$MESS ['XLS_PROFILE_COMMENT'] = "Профиль загрузки сохраняет настройки считывания.<br>Например вы можете настроить свой профиль загрузки для каждого поставщика,<br>и в дальнейшем использовать эти параметры";
$MESS ['XLS_PROFILE_SELECT'] = "Выберите профиль";
$MESS ['XLS_PROFILE_NAME'] = "Введите название нового профиля";
$MESS ['XLS_CHANGE_LIST'] = "Название листа";
$MESS ['XLS_FIRST_ROW_HEADER'] = "Строка, в которой искать заголовки";
$MESS ['XLS_FIRST_COLUMN_HEADER'] = "Столбец, с которого начинать считывать заголовки";
$MESS ['XLS_FIRST_ROW'] = "Строка, в которой искать первый элемент";
$MESS ['XLS_FIRST_COLUMN'] = "Столбец, с которого начинать считывать первый элемент";
$MESS ['XLS_FIRST_HEADER'] = "Строка заголовков";
$MESS ['XLS_FIRST_DATA'] = "Строка с первым элементом";
$MESS ['XLS_LIST_HEADER'] = "Лист считывания";
$MESS ['XLS_BITRIX_PARAMS'] = "Параметры в 1С-Битрикс";
$MESS ['XLS_BITRIX_COUNT_NULL'] = "Если кол-во нулевое, то делать элемент неактивным";
$MESS ['XLS_BITRIX_PRICE_NULL'] = "Если цена нулевая, то делать элемент неактивным";
$MESS ['XLS_PRICE_PARAMS'] = "Параметры цены товара";
$MESS ['XLS_PRICE_PARAM_CURRENCY'] = "Валюта";
$MESS ['XLS_PRICE_PARAM_TYPE'] = "Тип цены";

$MESS ['XLS_TABLE_NAME_COLUMN'] = "Название свойства в файле (Адрес ячейки)";
$MESS ['XLS_TABLE_NAME_IBLOCK_PROPERTY'] = "Название свойства в инфоблоке";
$MESS ['XLS_TABLE_TYPE_IBLOCK_PROPERTY'] = "Тип свойства";
$MESS ['XLS_TABLE_IS_UPLOAD'] = "Надо ли загружать это свойство";

$MESS ['XLS_DATA_TABLE_LOAD_IN_BITRIX'] = "Загружать в 1С-Битрикс";
$MESS ['XLS_DATA_TABLE_ELEMENT_BITRIX'] = "ID элемента в Битриксе";
$MESS ['XLS_DATA_TABLE_NAME'] = "Наименование";
$MESS ['XLS_DATA_TABLE_NAME_IN_BITRIX'] = "Наименование в 1С-Битрикс";
$MESS ['XLS_DATA_TABLE_PRICE'] = "Цена в файле";
$MESS ['XLS_DATA_TABLE_PRICE_IN_BITRIX'] = "Цена в 1С-Битрикс";
$MESS ['XLS_DATA_TABLE_COUNT'] = "Кол-во в файле";
$MESS ['XLS_DATA_TABLE_COUNT_IN_BITRIX'] = "Кол-во в 1С-Битрикс";

$MESS ['XLS_RESULT_HEADER'] = "The result of loading";
$MESS ['XLS_RESULT_COUNT_INSERT'] = "Created new:";
$MESS ['XLS_RESULT_COUNT_UPDATE'] = "Updated old:";



$MESS ['IBLOCK_ADM_IMP_PAGE_TITLE'] = "Download information block: step ";
$MESS ['IBLOCK_ADM_IMP_TAB1'] = "Setup";
$MESS ['IBLOCK_ADM_IMP_TAB1_ALT'] = "Adjust the copy settings information block";
$MESS ['IBLOCK_ADM_IMP_TAB2'] = "Download";
$MESS ['IBLOCK_ADM_IMP_TAB2_ALT'] = "The status of the download";
$MESS ['UPLOAD_ELSE'] = "Start again";
$MESS ['INTO_IBLOCK'] = "Go to the information block";
$MESS ['IBLOCK_ADM_IMP_BACK'] = "Back";
$MESS ['IBLOCK_ADM_IMP_NEXT_STEP_F'] = "Load data";
$MESS ['IBLOCK_ADM_IMP_NEXT_STEP'] = "Next";
$MESS ['IBLOCK_COPY_ERROR_UPBLOCK'] = "\nNot the type of information block from which the copy";
$MESS ['IBLOCK_COPY_ERROR1_UPBLOCK'] = "\nDo not select the information block which copy";
$MESS ['IBLOCK_COPY_ERROR2_UPBLOCK'] = "\nNot the type of information block in which copy";

$MESS ['IBLOCKCOPI_TITLE_STEP_1'] = "Copy setting IB";
$MESS ['UPBLOCK_STEP_1'] = "Copy the information in the new information block";
$MESS ['IBLOCKCOPI_STEP_1_ADD'] = "Add new properties";
$MESS ['IBLOCKCOPI_STEP_1_COPI'] = "Where to copy?";
$MESS ['XLS_RESULT_COUNT_UPDATE'] = "Обновлено старых:";
$MESS ['COPY_UPBLOCK_KAKOY'] = "What information block copy?";
$MESS ['IBLOCKCOPI_COPI_ELEM_STEP_2'] = "When elements";
$MESS ['IBLOCKCOPI_STEP_2_CAPTION'] = "Properties -";
$MESS ['IBLOCKCOPI_STEP_2_ELEM'] = "Elements - ";

$MESS ['IBLOCKCOPI_STEP_TIP'] = "Information block type";
$MESS ['IBLOCKCOPI_STEP_BL'] = "Information Block";
$MESS ['IBLOCKCOPI_STEP_COOYS'] = "Choose from the existing";
$MESS ['IBLOCKCOPI_STEP_CREATN'] = "Create a new";



?>