<?
$MESS ['xml_inp_INSTALL_NAME'] = "Модуль импорта из 111 файлов";
$MESS ['xml_inp_INSTALL_DESCRIPTION'] = "Модуль работы с информационными блоками позволяет управлять и каталогизировать информацию различного характера - новости, вакансии, список продуктов.";
$MESS ['xml_inp_INSTALL_TITLE'] = "Установка модуля работы с информационными блоками";
$MESS ['IBLOCK_INSTALL_COPY_NAME'] = "Модуль копирования инфоблоков со свойствами";
$MESS ['IBLOCK_INSTALL_COPY_DESCRIPTION'] = "Модуль позволяет копировать  инфвоблоки и создавать новые на основе существующих. Незаменим при создании сайтов на нескольких языках.";
$MESS ['IBLOCK_INSTALL_COPY_COMPANI_NAME'] = "ООО «СофтЭффект»";
$MESS ['IBLOCK_INSTALL_COPY_COMPANI_SITE'] = "http://www.softeffect.ru";
$MESS ['IBLOCK_INSTALL_COPY_M'] = "Установка модуля softeffect.iblockcopy";
$MESS ['IBLOCK_INSTALL_COPY_DEINSTAL'] = "Деинсталляция модуля softeffect.iblockcopy";
$MESS ['IBLOCK_INSTALL_PUBLIC_DIR'] = "Публичная папка";
$MESS ['IBLOCK_INSTALL_NEWS'] = "Новости";
$MESS ['IBLOCK_INSTALL_CATALOG'] = "Каталог";
$MESS ['IBLOCK_INSTALL_MODULE_UPBLOCKSS'] = "Модуль Импорт softeffect.iblockcopy установлен";
$MESS ['MOD_UNINST_CATALOG_INSTALLED'] = "Удаление модуля невозможно. Сначала необходимо удалить модуль торгового каталога.";
$MESS ['MOD_UNINST_BACK'] = "Вернуться к списку модулей";
$MESS ['MOD_UNINST_USPESHNO'] = "Модуль успешно удален из системы";

?>