<?

IncludeModuleLangFile(__FILE__);

// TODO: � �������� ����������� ������� ���������� ���� � ���������� � ���������� ���������
// TODO: � ��������� ������ ������
// TODO: � onchange -> ��������������
// TODO: � �������� ������ ������ � ������ 
// TODO: � �������� �������� �������� ���� � ���������� �� �������� ��������� ��������� 

class CCitrusAce
{
	function OnBeforeChangeFileHandler($abs_path, $strContent)
	{
		$strPathToPHP = COption::GetOptionString('citrus.ace', 'phpPath', '');

		if (strlen($strPathToPHP) <= 0)
			return true;

		if (substr($abs_path, strlen($abs_path)-3, 3) == "php")
		{
			$tmpPath = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/cache/".substr(md5(time()), 0,3)."_".basename($abs_path);
			if (file_exists($tmpPath ))
				unlink ($tmpPath);
			
			$f = fopen($tmpPath, "a+");
			if ($f)
			{
				$tmpProlog = '<?php $_SERVER["DOCUMENT_ROOT"] = "'.$_SERVER["DOCUMENT_ROOT"].'"; ?>';
				fwrite($f, $tmpProlog);
				fwrite($f, $strContent);
				fclose($f);
				 
				//syntax analyse
				$parse_error = "";
				$command1 = '"'.$strPathToPHP.'" -l "'.$tmpPath.'"';
				exec($command1, $text1, $errNum1);
				
				if ($errNum1 !== 0 && count($text1) <= 0)
				{
					unlink ($tmpPath);
					$GLOBALS['APPLICATION']->ThrowException(GetMessage("ACE_EDITOR_PHP_EXEC_ERROR", Array("#PATH#" => $strPathToPHP)));
					return false;
				}
				elseif (!($errNum1 === 0 && is_array($text1) && strpos($text1[0], "No syntax errors detected") === 0))
				{
					unlink ($tmpPath);
					$errMsg = $text1[1];
					if (preg_match('#(.*) in (.*) on line (\d+).*?#i', $errMsg, $arMatches))
						$errMsg = $arMatches[1] . ' on line ' . $arMatches[3];
					$GLOBALS['APPLICATION']->ThrowException($errMsg);
					return false;
				}
/*
				$command2 = '"'.PATH_TO_PHP.'" -f "'.$tmpPath.'"';
				$fatal_error = exec($command2, $text2, $errNum2);
				if ($fatal_error !== "" && strpos($fatal_error, "on line") !== false)
				{
					unlink ($tmpPath);
					if (preg_match('#(.*) in (.*) on line (\d+).*?#i', $fatal_error, $arMatches))
						$fatal_error = $arMatches[1] . ' on line ' . $arMatches[3];
					$GLOBALS['APPLICATION']->ThrowException($fatal_error);
					return false;
				}*/
				fclose($f);
				unlink ($tmpPath);
			}
		}
		return true;
	}

	function GetFileList($preg = '#^theme-(.*?)\.js$#')
	{
		$arThemes = Array();
		$arFiles = scandir(str_replace('\\', '/', dirname(__FILE__)) . '/src/');
		foreach ($arFiles as $key => $filename)
		{
			if (preg_match($preg, $filename, $match))
				$arThemes[$match[1]] = $match[0];
		}
		return $arThemes;
	}
	
	function TabControlBegin(&$form)
	{
		if ($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/user_settings.php")
		{

			$aceTheme = CUserOptions::GetOption('citrus.ace.editor', 'theme', 'twilight');
			$aceFontSize = CUserOptions::GetOption('citrus.ace.editor', 'fontSize', '13');
			$aceFontFamily = CUserOptions::GetOption('citrus.ace.editor', 'fontFamily', 'Consolas, Monaco, "Courier New"');
			
			$aceHighlightActiveLine = CUserOptions::GetOption('citrus.ace.editor', 'highlightActiveLine', true);
			$aceShowInvisibles = CUserOptions::GetOption('citrus.ace.editor', 'showInvisibles', false);
			$aceShowGutter = CUserOptions::GetOption('citrus.ace.editor', 'showGutter', true);
			$acePrintMargin = CUserOptions::GetOption('citrus.ace.editor', 'printMargin', 80);
			$aceUseSoftTabs = CUserOptions::GetOption('citrus.ace.editor', 'useSoftTabs', false);
			$aceKeybinding = CUserOptions::GetOption('citrus.ace.editor', 'keybinding', false);
			$aceHighlightSelectedWord = CUserOptions::GetOption('citrus.ace.editor', 'highlightSelectedWord', true);
			$aceCodeFolding = CUserOptions::GetOption('citrus.ace.editor', 'codeFolding', true);
			$aceEnableBehaviours = CUserOptions::GetOption('citrus.ace.editor', 'enableBehaviours', true);
			
			$pathToPHP = COption::GetOptionString('citrus.ace', 'phpPath', '');

			$sThemesHTML = '';
			$arThemes = self::GetFileList();
			foreach ($arThemes as $theme => $filename)
			{
				$tmpTheme = $theme;
				$imgPath = str_replace('\\', '/', dirname(__FILE__)) . '/src/i/' . $tmpTheme . '.png';
				if (!file_exists($imgPath) || !is_file($imgPath))
				{
					$tmpTheme = 'no-preview';
					$imgPath = str_replace('\\', '/', dirname(__FILE__)) . '/src/i/' . $tmpTheme . '.png';
				}
				$sThemesHTML .= '
					<label class="ace-theme-select' . ($theme == $aceTheme ? ' ace-theme-select-active' : '') . '" title="' . $theme . '">
						<input type="radio" name="citrus_ace_theme" value="' . $theme . '"' . ($theme == $aceTheme ? ' checked="checked"' : '') . ' />
						<img src="/bitrix/modules/citrus.ace/src/i/' . $tmpTheme . '.png" alt="' . $theme .'" style="vertical-align: middle;" />
					</label>
				';
			}
			
			$arKeybindings = array(
				"REFERENCE" => array(
					GetMessage("ACE_EDITOR_KEYBINDINGS_DEFAULT"),
				),
				"REFERENCE_ID" => array(
					0,
				),
			); 
			$arFiles = self::GetFileList('#^keybinding-(.*?)\.js$#');
			foreach ($arFiles as $theme => $filename)
			{
				$title = GetMessage("ACE_EDITOR_KEYBINDINGS_" . strtoupper($theme));
				$arKeybindings["REFERENCE"][] = strlen($title) > 0 ? $title : $theme; 
				$arKeybindings["REFERENCE_ID"][] = $theme; 
			}
			
			$strCommonSettings = '';
			if ($GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
			{
				$strCommonSettings .= '
				<tr valign="top">
					<td>
						' . GetMessage("ACE_COMMON_SETTINGS") . '
					</td>
					<td>
						<input type="checkbox" name="citrus_ace_common_settings" value="1" />
					</td>
				</tr>
				';
			}
			
			$arFontSize = array(
				"REFERENCE" => range(8,40),
				"REFERENCE_ID" => range(8,40),
			); 
			$arFontFamily = array(
				"REFERENCE" => array(
					'Consolas',
					'Courier New'
				),
				"REFERENCE_ID" => array(
					htmlspecialchars('Consolas, Monaco, "Courier New", Courier'),
					htmlspecialchars('Monaco, "Courier New, Courier"')
				),
			); 
			$arPrintMargin = array(
				"REFERENCE" => array(
					GetMessage("ACE_EDITOR_PRINT_MARGIN_OFF"),
					80,
					100,
					120,
				),
				"REFERENCE_ID" => array(
					0,
					80,
					100,
					120,
				),
			); 
						
			$form->tabs[] = array("DIV" => "citrus_ace_edit", "TAB" => GetMessage("ACE_TAB_NAME"), "ICON"=>"main_user_edit", "TITLE"=>GetMessage("ACE_TAB_TITLE"), "CONTENT"=> '
				<tr valign="top">
					<td style="width: 40%;">' . GetMessage("ACE_EDITOR_FONT") .'</td>
					<td>' . SelectBoxFromArray("citrus_ace_font_family", $arFontFamily, htmlspecialchars($aceFontFamily)) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_FONT_SIZE") .'</td>
					<td>' . SelectBoxFromArray("citrus_ace_font_size", $arFontSize, $aceFontSize) .'</td>
				</tr>

				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_SHOW_GUTTER") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_show_gutter", "1", $aceShowGutter) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_HIGHLIGHT_ACTIVE_LINE") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_highlight_active_line", "1", $aceHighlightActiveLine) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_PRINT_MARGIN") .'</td>
					<td>' . SelectBoxFromArray("citrus_ace_print_margin", $arPrintMargin, $acePrintMargin) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_USE_WRAP_MODE") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_use_wrap_mode", "1", $aceUseWrapMode) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_KEYBINDINGS") .'</td>
					<td>' . SelectBoxFromArray("citrus_ace_keybinding", $arKeybindings, $aceKeybinding) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_SHOW_INVISIBLES") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_show_invisibles", "1", $aceShowInvisibles) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_USE_SOFT_TABS") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_use_soft_tabs", "1", $aceUseSoftTabs) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_HIGHLIGHT_SELECTED_WORD") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_highlight_selected_word", "1", $aceHighlightSelectedWord) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_CODE_FOLDING") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_code_folding", "1", $aceCodeFolding) .'</td>
				</tr>
				<tr valign="top">
					<td>' . GetMessage("ACE_EDITOR_ENABLE_BEHAVIOURS") .'</td>
					<td>' . InputType("checkbox", "citrus_ace_enable_behaviours", "1", $aceEnableBehaviours) .'</td>
				</tr>
				' .
				($GLOBALS['USER']->IsAdmin() ? '
					<tr valign="top" class="heading">
						<td colspan="2">' . GetMessage("ACE_EDITOR_PHP_SYNTAX_CHECK") .'</td>
					</tr>
					<tr valign="top">
						<td>' . GetMessage("ACE_EDITOR_PHP_PATH") .'</td>
						<td><input type="string" size="50" name="citrus_ace_php_path" value="' . htmlspecialchars($pathToPHP) . '" />' . BeginNote() . GetMessage("ACE_EDITOR_PHP_PATH_NOTE") . EndNote() . '</td>
					</tr>
					'  : '') . '
				<tr valign="top" class="heading">
					<td colspan="2">' . GetMessage("ACE_EDITOR_THEME") .'</td>
				</tr>
				<tr valign="top">
					<td colspan="2">' . $sThemesHTML . '</pre></td>
					<style>
						label.ace-theme-select { display: *inline; zoom: 1; display: -moz-inline-stack; display: inline-block; white-space: nowrap; margin: 0 10px 10px 0; }
						label.ace-theme-select img { margin: 2px; }
						label.ace-theme-select-active {
							outline: 3px solid #a00;
						}
					</style>
				</tr>
								
				' . $strCommonSettings . ' 
			');
			if ($_REQUEST['active_tab'] == 'citrus_ace_edit')
				$form->selectedTab = 'citrus_ace_edit';
		}
	}
	
	function BeforeProlog()
	{
		if($_SERVER["REQUEST_METHOD"] == "POST" && strlen($_POST["apply"]) > 0 && $GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/user_settings.php")
		{
			$bCommon = $GLOBALS["USER"]->CanDoOperation('edit_other_settings') && array_key_exists('citrus_ace_common_settings', $_POST) && $_POST['citrus_ace_common_settings'];
			
			$arSetOptions = Array(
				'theme' => array_key_exists($_POST['citrus_ace_theme'], self::GetFileList()) ? $_POST['citrus_ace_theme'] : 'twilight',
				'fontFamily' => $_POST['citrus_ace_font_family'],
				'fontSize' => in_array($_POST['citrus_ace_font_size'], range(8,40)) ? $_POST['citrus_ace_font_size'] : 13,
				
				'highlightActiveLine' => $_POST['citrus_ace_highlight_active_line'] ? true : false,
				'showInvisibles' => $_POST['citrus_ace_show_invisibles'] ? true : false,
				'showGutter' => $_POST['citrus_ace_show_gutter'] ? true : false,
				'printMargin' => in_array($_POST['citrus_ace_print_margin'], range(20,300)) ? $_POST['citrus_ace_print_margin'] : false,
				'useSoftTabs' => $_POST['citrus_ace_use_soft_tabs'] ? true : false,
				'useWrapMode' => $_POST['citrus_ace_use_wrap_mode'] ? true : false,
				'highlightSelectedWord' => $_POST['citrus_ace_highlight_selected_word'] ? true : false,
				'codeFolding' => $_POST['citrus_ace_code_folding'] ? true : false,
				'enableBehaviours' => $_POST['citrus_ace_enable_behaviours'] ? true : false,
				
				'keybinding' => array_key_exists($_POST['citrus_ace_keybinding'], self::GetFileList('#^keybinding-(.*?)\.js$#')) ? $_POST['citrus_ace_keybinding'] : false,
	
			);
			
			if ($GLOBALS['USER']->IsAdmin() && array_key_exists('citrus_ace_php_path', $_POST))
				COption::SetOptionString('citrus.ace', 'phpPath', $_POST['citrus_ace_php_path']);

			foreach ($arSetOptions as $option => $value)
			{
				CUserOptions::SetOption('citrus.ace.editor', $option, $value, false);
				if ($bCommon)
					CUserOptions::SetOption('citrus.ace.editor', $option, $value, true);
			}
		}

		global $APPLICATION;
		
		if (!defined("ADMIN_SECTION"))
			return;

		$curPage = $APPLICATION->GetCurPage(false);
		$arEnabledPages = Array(
			'/bitrix/admin/fileman_file_edit.php' => 'filesrc',
			'/bitrix/admin/public_file_edit_src.php' => 'filesrc',
			'/bitrix/admin/public_file_edit.php' => 'filesrc_pub',
			
			'/bitrix/admin/php_command_line.php' => 'php',
			'/bitrix/admin/sql.php' => 'sql',
		);
		if (array_key_exists($curPage, $arEnabledPages) && method_exists('CUtil', 'InitJSCore'))
		{
			$sMode = $arEnabledPages[$curPage];
			$bPublic = preg_match('#^.*/public_file_edit.*$#', $curPage);
			$bHTML = $curPage == '/bitrix/admin/public_file_edit.php';

			CUtil::InitJSCore(Array('popup'));

			$aceTheme = CUserOptions::GetOption('citrus.ace.editor', 'theme', 'twilight');
			$arFiles = self::GetFileList('#^theme-(.*?)\.js$#');
			if (!array_key_exists($aceTheme, $arFiles)) { $aceTheme = 'twilight'; }
			
			$aceFontSize = CUserOptions::GetOption('citrus.ace.editor', 'fontSize', '13');
			$aceFontFamily = CUserOptions::GetOption('citrus.ace.editor', 'fontFamily', 'Consolas, Monaco, "Courier New"');

			$aceKeybinding = CUserOptions::GetOption('citrus.ace.editor', 'keybinding', false);
			$arFiles = self::GetFileList('#^keybinding-(.*?)\.js$#');
			if (!array_key_exists((string)$aceKeybinding, $arFiles)) { $aceKeybinding = false; }
			
			if ($curPage == '/bitrix/admin/public_file_edit.php' && $_REQUEST['noeditor'] != "Y") // ��������� ��������
				return;

			$aceMode = 'php';
			switch ($sMode)
			{
				case 'filesrc':
				case 'filesrc_pub':
					$fileExt = pathinfo($_REQUEST['path'], PATHINFO_EXTENSION);
					switch ($fileExt)
					{
						case 'js':
							$aceMode = 'javascript';
							break;
						case 'xml':
						case 'html':
						case 'css':
						case 'sql':
						case 'php':
							$aceMode = $fileExt;
							break;
						case 'htm':
							$aceMode = 'html';
							break;
						case 'py':
							$aceMode = 'python';
							break;
						case 'rb':
							$aceMode = 'ruby';
							break;
					}
					break;

				case 'sql':
					$aceMode = 'sql';
					break;
					
				case 'php':
					$aceMode = 'php';
					break;
			}
			
			$scriptRoot = BX_ROOT . '/modules/citrus.ace/src/';
			$arLang = Array(
				'aceSearchTitle' => GetMessage("ACE_EDITOR_SEARCH_TITLE"),
				'aceSearchButton' => GetMessage("ACE_EDITOR_SEARCH_BUTTON"),
			);
			$strLang = CUtil::PhpToJsObject($arLang);
			$add2Head = <<<HTML
<script type="text/javascript" src="{$scriptRoot}bx-ace.js"></script>
<script type="text/javascript" src="{$scriptRoot}ace-uncompressed.js"></script>
<script type="text/javascript" src="{$scriptRoot}theme-{$aceTheme}.js"></script>
<script type="text/javascript" src="{$scriptRoot}mode-{$aceMode}.js"></script>
<script type="text/javascript">
BX.message($strLang);
</script>
HTML;
			if ($aceKeybinding)
				$add2Head .= <<<HTML
<script type="text/javascript" src="{$scriptRoot}keybinding-{$aceKeybinding}.js"></script>
HTML;

			$arParams = Array(
				'editMode' => $sMode,
				'mode' => $aceMode,
				'theme' => $aceTheme,
				'keybinding' => $aceKeybinding,
				'highlightActiveLine' => CUserOptions::GetOption('citrus.ace.editor', 'highlightActiveLine', true) ? true : false,
				'showInvisibles' => CUserOptions::GetOption('citrus.ace.editor', 'showInvisibles', false) ? true : false,
				'showGutter' => CUserOptions::GetOption('citrus.ace.editor', 'showGutter', true) ? true : false,
				'showPrintMargin' => IntVal(CUserOptions::GetOption('citrus.ace.editor', 'printMargin', 80)) > 0,
				'printMarginColumn' => IntVal(CUserOptions::GetOption('citrus.ace.editor', 'printMargin', 80)),
				'useSoftTabs' => CUserOptions::GetOption('citrus.ace.editor', 'useSoftTabs', false) ? true : false,
				'useWrapMode' => CUserOptions::GetOption('citrus.ace.editor', 'useWrapMode', false) ? true : false,

				'highlightSelectedWord' => CUserOptions::GetOption('citrus.ace.editor', 'highlightSelectedWord', true) ? true : false,
				'codeFolding' => CUserOptions::GetOption('citrus.ace.editor', 'codeFolding', true) ? true : false,
				'enableBehaviours' => CUserOptions::GetOption('citrus.ace.editor', 'enableBehaviours', true) ? true : false,
				
				'searchDialog' => true,
			);
			$jsParams = CUtil::PhpToJsObject($arParams);
			
			$bCommand = in_array($sMode, Array('php', 'sql')); 
			$heightDiv = $bCommand ? '30' : '50';
			$height = $bCommand ? '26' : '43';
			
			$add2Head .= <<<HTML
<script type="text/javascript">
BX.ready(function () {
	BX.ace.Init($jsParams);
});
</script>
<style type="text/css">
#editorDiv { width: 100%; height: {$heightDiv}em; position: relative; overflow: hidden; margin-bottom: -1em; }
#aceEditor { margin: 0; width: 100%; height: {$height}em; font: normal normal {$aceFontSize}px/120% {$aceFontFamily}, monospace; }
#aceEditor .ace_text-layer { font: normal normal {$aceFontSize}px/120% {$aceFontFamily}, monospace; }
</style>
HTML;
		
			if ($bPublic)
				echo $add2Head;
			else
				$APPLICATION->AddHeadString($add2Head);
		}
	}

	
}


?>