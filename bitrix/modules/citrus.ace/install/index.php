<?

IncludeModuleLangFile(__FILE__);

class citrus_ace extends CModule {
		
	var $MODULE_ID = "citrus.ace";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";
	
	function citrus_ace() {
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include ($path . "/version.php");
		
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = GetMessage("MODULE_PARTNER_NAME");
		$this->PARTNER_URI = "http://www.citrus-soft.ru/";
		$this->MODULE_NAME = GetMessage("MODULE_NAME_ACE");
		$this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION_ACE");
	}

	function InstallDB() {
		
		RegisterModule($this->MODULE_ID);
		RegisterModuleDependences("main", "OnAdminTabControlBegin", "citrus.ace", "CCitrusAce", "TabControlBegin");
		RegisterModuleDependences("main", "OnBeforeProlog", "citrus.ace", "CCitrusAce", "BeforeProlog");
		RegisterModuleDependences("main", "OnBeforeChangeFile", "citrus.ace", "CCitrusAce", "OnBeforeChangeFileHandler");

		return true;
	} 
		
	function UnInstallDB() {

		UnRegisterModuleDependences("main", "OnAdminTabControlBegin", "citrus.ace", "CCitrusAce", "TabControlBegin");
		UnRegisterModuleDependences("main", "OnBeforeProlog", "citrus.ace", "CCitrusAce", "BeforeProlog");
		UnRegisterModuleDependences("main", "OnBeforeChangeFile", "citrus.ace", "CCitrusAce", "OnBeforeChangeFileHandler");
		UnRegisterModule($this->MODULE_ID);
		
		return true;
	}
	
	function InstallEvents() {
		return true;
	}

	function UnInstallEvents() {
		return true;
	}

	function InstallFiles() {
		return true;
	}

	function UnInstallFiles() {
		DeleteDirFilesEx("/bitrix/themes/.default/icons/citrus.ace/"); //icons
		return true;
	}
	
	function DoInstall()
	{
		global $APPLICATION;
		
		if(!IsModuleInstalled($this->MODULE_ID))
		{
			$this->InstallDB();
			$this->InstallEvents();
			$this->InstallFiles();
		}
	}

	function DoUninstall()
	{
		$this->UnInstallDB();
		$this->UnInstallEvents();
		$this->UnInstallFiles();
	}

}?>