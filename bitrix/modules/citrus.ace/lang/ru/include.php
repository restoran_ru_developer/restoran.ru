<?
$MESS ['ACE_TAB_NAME'] = "Подсветка синтаксиса";
$MESS ['ACE_TAB_TITLE'] = "Подсветка синтаксиса для редактора исходного кода";
$MESS ['ACE_SETTINGS_TITLE'] = "Настойки редактора ACE";
$MESS ['ACE_EDITOR_THEME'] = "Цветовая схема:";
$MESS ['ACE_COMMON_SETTINGS'] = "Установить данные настройки по умолчанию для всех пользователей:";
$MESS ['ACE_EDITOR_FONT'] = "Шрифт:";
$MESS ['ACE_EDITOR_FONT_SIZE'] = "Размер шрифта:";

$MESS ['ACE_EDITOR_HIGHLIGHT_ACTIVE_LINE'] = "Подсветка текущей строки:";
$MESS ['ACE_EDITOR_SHOW_INVISIBLES'] = "Показывать непечатаемые символы:";
$MESS ['ACE_EDITOR_SHOW_GUTTER'] = "Нумерация строк:";
$MESS ['ACE_EDITOR_PRINT_MARGIN'] = "Вертикальная граница:";
$MESS ['ACE_EDITOR_PRINT_MARGIN_OFF'] = "(отключена)";
$MESS ['ACE_EDITOR_USE_SOFT_TABS'] = "Заменять табуляцию пробелом:";
$MESS ['ACE_EDITOR_KEYBINDINGS'] = "Сочетания клавиш:";
$MESS ['ACE_EDITOR_KEYBINDINGS_DEFAULT'] = "(по умолчанию)";
$MESS ['ACE_EDITOR_KEYBINDINGS_VIM'] = "VIM";
$MESS ['ACE_EDITOR_KEYBINDINGS_EMACS'] = "Emacs";
$MESS ['ACE_EDITOR_USE_WRAP_MODE'] = "Переносить длинные строки:";
$MESS ['ACE_EDITOR_HIGHLIGHT_SELECTED_WORD'] = "Подсветка выделенного слова:";
$MESS ['ACE_EDITOR_CODE_FOLDING'] = "Сворачивание блоков:";
$MESS ['ACE_EDITOR_ENABLE_BEHAVIOURS'] = "Автоматически закрывать скобки:";

$MESS ['ACE_EDITOR_SEARCH_TITLE'] = "Поиск";
$MESS ['ACE_EDITOR_SEARCH_BUTTON'] = "Найти";

$MESS["ACE_EDITOR_PHP_SYNTAX_CHECK"] = "Проверка синтаксиса для файлов PHP";
$MESS["ACE_EDITOR_PHP_PATH"] = "Путь к интерпретатору:";
$MESS["ACE_EDITOR_PHP_PATH_NOTE"] = "Введите полный путь к интерпретатору PHP, оставьте поле пустым чтобы отключить проверку.<br/><br/>Например, для Битрикс Веб-окружения на Windows путь будет таким:<br/><i>C:/Program Files/Bitrix Environment/apache2/zendserver/bin/php.exe</i><br/> для 64-битных систем: <br/><i>C:/Program Files/Bitrix Environment (x86)/apache2/zendserver/bin/php.exe</i>";
$MESS["ACE_EDITOR_PHP_EXEC_ERROR"] = "Произошла ошибка при запуске #PATH#\n\nПроверьте правильность указания пути к интерпретатору PHP в настройках интерфейса (Настройки &ndash; Настройки продукта &ndash; Интерфейс)";

?>