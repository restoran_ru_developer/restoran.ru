(function (window) {
if (BX.ace) return;

BX.ace = {
	editor: false,
	session: false,
	textArea: false,
	
	searchDialog: false,
	searchInput: false,
	wnd: false,
	
	// editor setup
	ApplySettings: function () {
		var editor = this.editor;
		if (!editor) return;
		var session = editor.getSession();
		
		editor.setHighlightActiveLine(this.params.highlightActiveLine);
		editor.setShowInvisibles(this.params.showInvisibles);
		editor.renderer.setShowGutter(this.params.showGutter);
		editor.renderer.setShowPrintMargin(this.params.showPrintMargin);
		editor.setPrintMarginColumn(this.params.printMarginColumn);
		session.setUseSoftTabs(this.params.useSoftTabs);
		session.setUseWrapMode(this.params.useWrapMode);
		//session.setWrapLimitRange(40, 40);
		editor.setHighlightSelectedWord(this.params.highlightSelectedWord);
		editor.setBehavioursEnabled(this.params.enableBehaviours);
		session.setFoldStyle(this.params.codeFolding ? "markbegin" : "manual");
		
	},
	
	// set custom keybindings
	SetCommands: function() {
		var editor = this.editor;
		if (!editor) return;
		var commands = this.editor.commands;
		commands.addCommand({
			name: 'savefile',
			bindKey: {
				win: "Ctrl-S",
				mac: "Command-S",
				sender: "editor"
			},
			exec: BX.delegate(this.SaveAction, this)
		});
		if (this.params.searchDialog)
		{
			commands.addCommand({
				name:'find',
				bindKey: {
					win: "Ctrl-F",
					mac: "Command-F",
					sender: "editor"
				},
				exec: BX.delegate(this.showSearch, this)
			});
			commands.addCommand({
				name: 'findnext',
				bindKey: {
					win: "F3",
					mac: "F3",
					sender: "editor"
				},
				exec: BX.delegate(this.doSearch, this)
			});
		}
	},
	
	// set textarea value and submit form
	SaveAction: function() {
		var editor = this.editor;
		if (!editor) return;
		
		this.textArea.value = this.session.getValue();
		if (this.form.apply)
			this.form.apply.click();
		else if (this.form.save)
			this.form.save.click();
		else if (BX.ace.form.execute)
			this.form.execute.click();
	},

	showSearch: function() {
		
		if (typeof(this.searchDialog) !== 'object')
		{
			this.searchDialog = new BX.PopupWindow('aceSearchDialog', false, {
				zIndex: 100,
				content: '<p style="padding: 5px 20px 0 5px;">' + BX.message('aceSearchTitle') + ': <input type="text" value="" id="aceSearchInput"/></p>',
				/*titleBar: {
					content: BX.create('span', {text: BX.message('aceSearchTitle')}),
				},*/
				//lightShadow: true,
				autoHide: true,
				closeIcon: true,
				buttons: [
					new BX.PopupWindowButton({
						text : BX.message('aceSearchButton'),
						id : 'aceSearchDialogButton',
						events : {
							click: BX.delegate(this.doSearch, this)
						}
					})
				],
				events: {
					onPopupClose: BX.delegate(function () {
							this.editor.focus();
						}, this),
					
					onPopupFirstShow: BX.delegate(function() {
							this.searchInput = BX('aceSearchInput');
							if (this.searchInput)
							{
								BX.bind(this.searchInput, 'keydown', BX.delegate(function (e) {
									e = e ? e : window.event;
									if (e && e.keyCode == 13)
									{
										 this.doSearch();
										 BX.PreventDefault(e);
										 this.editor.focus();
									}
								}, this));
							}
						}, this),
					
					onAfterPopupShow: BX.delegate(function () {
							if (this.searchInput)
							{
								this.searchInput.focus();
								this.searchInput.select();
							}
						}, this)
				}
			});
			// key bindings
			BX.bind(this.searchDialog.popupContainer, 'keyup', BX.delegate(function(e) {
				if (!e) { e = window.event; }
				if (e.keyCode == 27)
					this.closeSearch();
			}, this));
		}
		
		this.searchDialog.show();
	},
	
	doSearch: function() {
		if (this.searchInput && this.searchInput.value.length > 0)
		{
			this.editor.find(this.searchInput.value, {
				backwards: false,
				wrap: true,
				caseSensitive: false,
				wholeWord: false,
				//scope: Search.ALL,
				regExp: false
			});
			this.closeSearch();
		}
	},
	
	closeSearch: function() {
		this.searchDialog.close();
	},
	
	Init: function (params) {
		this.params = params;
		
		this.textArea = BX.findChild(document.body, {tag: 'textarea', attr: {name: this.params.editMode}}, true);
		
		if (!this.textArea || typeof(this.textArea) != 'object')
			return false;
		this.form = BX.findParent(this.textArea, {tag: 'form'});
		this.wnd = BX.WindowManager ? BX.WindowManager.Get() : false

		// hide new codeeditor (v11.5)
		if (window.JCCodeEditor) {
			window.setTimeout(function() {
				var bxCE = BX.findChild(document.body, {tag: 'div', className: 'bxce'}, true);
				if (bxCE)
					bxCE.style.display = 'none';
			}, 200);
		}

		var pre = BX.create(
			'pre',
			{props: {id: 'aceEditor'}, html: BX.util.htmlspecialchars(this.textArea.value)}
		);
		var div = BX.create('div', {props: {id: 'editorDiv'}});
		div.appendChild(pre);
		this.textArea.parentNode.insertBefore(div, this.textArea);
		this.textArea.style.display = 'none';
		this.editor = ace.edit('aceEditor');
		this.editor.setTheme("ace/theme/" + this.params.theme);
		
		this.session = this.editor.getSession(); 
		this.session.setMode(new (require("ace/mode/" + this.params.mode).Mode));
		// settings
		this.ApplySettings();
		// add custom keybindings
		this.SetCommands();
		// search form
		//this.searchDialog = new BX.CDialog(this.params.searchDialog);

		// update form data before submit
		BX.bind(this.form, 'submit', BX.delegate(function(e) {
			this.textArea.value = this.session.getValue();
		}, this));
		// adjust form data before bitrix autosaving
		BX.addCustomEvent(this.form, 'onAutoSave', BX.delegate(function(obj, form_data) {
			form_data[this.textArea.name] = this.session.getValue();
		}, this));
		// set editor value after autosave restore
		BX.addCustomEvent(this.form, 'onAutoSaveRestoreFinished', BX.delegate(function(obj) {
			BX.ace.session.setValue(BX.ace.textArea.value);
		}, this));

		// php commandline
		if (this.params.editMode == 'php')
		{
			document.form1.execute.onclick = '';
			BX.bind(document.form1.execute, 'click', BX.delegate(function (e) {
				BX.eventReturnFalse(e);
				this.textArea.value = this.session.getValue();
				__FPHPSubmit();
			}, this));
		}

		// public mode
		if (this.wnd)
		{
			window.setTimeout(BX.delegate(function () {
				// public php
				if (typeof(this.wnd) != 'undefined' && this.wnd.btnSave && this.wnd.btnSave.btn)
				{
					BX.unbindAll(this.wnd.btnSave.btn);
					this.SaveAction = function() {
						this.wnd.btnSave.disableUntilError();
						this.textArea.value = this.session.getValue();
						this.wnd.PostParameters();
					};
					BX.bind(this.wnd.btnSave.btn, 'click', BX.delegate(this.SaveAction, this));
				}
				
				// public html
				var btn = BX('btn_popup_save');
				if (btn)
				{
					btn.onclick = '';
					this.SaveAction = function() {
						BX.ace.textArea.value = BX.ace.session.getValue();
						BXFormSubmit();
					};
					BX.bind(btn, 'click', BX.delegate(this.SaveAction, this));
				}
				
				BX.addCustomEvent(wnd, 'onWindowResizeExt', this.ResizeWindow);
				this.ResizeWindow();
			}, this), 50);
		}
		
	},
	
	ResizeWindow: function () {
		var eEditorDiv, eEditor;
		data = this.wnd.GetInnerPos();
		if (null == eEditorDiv) eEditorDiv = BX('editorDiv');
		if (null == eEditor) eEditor = BX('aceEditor');

		if (null != eEditorDiv && null != eEditor)
		{
			var add = BX('bx_additional_params');
			if (data.height) eEditorDiv.style.height = eEditor.style.height = (data.height - border - wnd.PARTS.HEAD.offsetHeight - (add ? add.offsetHeight : 0) - 35) + 'px';
			if (data.width) eEditorDiv.style.width = eEditor.style.width = (data.width - border - 10) + 'px';
			this.editor.resize();
		}
		else
		{
			window.setTimeout('BX.ace.ResizeWindow()', 50);
		}
	}
	
}

})(window);