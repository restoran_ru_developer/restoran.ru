<?
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/citrus.ace/include.php");

$moduleID = 'citrus.ace';

$accessPermissions = $APPLICATION->GetGroupRight($moduleID);
if ($accessPermissions < "W")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

//IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
IncludeModuleLangFile(__FILE__);


if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$Cancel)>0 && check_bitrix_sessid())
{
	if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
		LocalRedirect($_REQUEST["back_url_settings"]);
	else
		LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
}


$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "ace_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);


$tabControl->Begin();
?><form method="post" action="<?echo $APPLICATION->GetCurPage()?>?>&amp;lang=<?=LANGUAGE_ID?>">
	<?=bitrix_sessid_post();?>
<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td colspan="2">
			<?=BeginNote()?>
			<?=GetMessage("CITUS_ACE_OPTIONS_NOTE")?>
			<?=EndNote()?>
		</td>
	</tr>

<?$tabControl->Buttons();?>
	<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" disabled="disabled">
	<input type="hidden" name="Update" value="Y">
	<?if(strlen($_REQUEST["back_url_settings"])>0):?>
		<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
		<input type="hidden" name="back_url_settings" value="<?=htmlspecialchars($_REQUEST["back_url_settings"])?>">
	<?endif?>
</form>
<?$tabControl->End();?>