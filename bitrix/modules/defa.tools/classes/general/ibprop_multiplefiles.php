<?

IncludeModuleLangFile(__FILE__);

if (!class_exists("DefaTools_IBProp_MultipleFiles"))
{

	class DefaTools_IBProp_MultipleFiles
	{
		function OnAfterIBlockPropertyHandler(&$arFields)
		{
			if($arFields['USER_TYPE'] == 'DefaToolsMultipleFiles') {
				$arFields['MULTIPLE'] = 'Y';

				if($arFields['MULTIPLE_CNT'] < 2)
					$arFields['MULTIPLE_CNT'] = 2;
			}
		}

		function GetUserTypeDescription()
		{
			return array(
				'PROPERTY_TYPE'			=> 'F',
				'USER_TYPE'				=> 'DefaToolsMultipleFiles',
				'DESCRIPTION'			=> GetMessage("DEFATOOLS_PROP_NAME"),
				'GetPropertyFieldHtml'	=> array('DefaTools_IBProp_MultipleFiles','GetPropertyFieldHtml'),
				'ConvertToDB'			=> array('DefaTools_IBProp_MultipleFiles','ConvertToDB'),
				'ConvertFromDB'			=> array('DefaTools_IBProp_MultipleFiles','ConvertFromDB'),
				'GetSettingsHTML'		=> array('DefaTools_IBProp_MultipleFiles','GetSettingsHTML'),
				'PrepareSettings'		=> array('DefaTools_IBProp_MultipleFiles','PrepareSettings')
			  );
		}

		function PrepareSettings($arFields)
		{
			if(empty($arFields["USER_TYPE_SETTINGS"]["PLUGIN_TYPE_EDIT"]))
				$arFields["USER_TYPE_SETTINGS"]["PLUGIN_TYPE_EDIT"] = 'java';

			return array('PLUGIN_TYPE_EDIT' => trim($arFields["USER_TYPE_SETTINGS"]["PLUGIN_TYPE_EDIT"]));
		}

		function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
		{
			$arPropertyFields = array("USER_TYPE_SETTINGS_TITLE" => GetMessage("DEFATOOLS_USER_TYPE_SETTINGS_TITLE"));

			$html  = '<tr>
						<td>'.GetMessage("DEFATOOLS_PLUGIN_TYPE").':</td>
						<td>
							<select name="'.$strHTMLControlName["NAME"].'[PLUGIN_TYPE_EDIT]">';
			if($arProperty['USER_TYPE_SETTINGS']['PLUGIN_TYPE_EDIT'] == 'java'):
				$html .= ' <option	value="flash">Flash</option>
						   <option  selected="selected" value="java">Java applet</option>
				';
			else:
				$html .= ' <option	selected="selected" value="flash">Flash</option>
						   <option  value="java">Java applet</option>
				';
			endif;
			$html .= '  	</select>
						</td>
					</tr>';

			return $html;
		}

		function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
		{

			global $APPLICATION;
			$plugin_type_edit = $arProperty['USER_TYPE_SETTINGS']['PLUGIN_TYPE_EDIT'];

			$bFileman = CModule::IncludeModule('fileman');

			ob_start();

			echo '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb'.md5('PROP['.$arProperty["ID"].']').'">';

			$cols = $arProperty["COL_COUNT"];

			echo '<tr><td>';

			$val = $value["VALUE"];
			$val_description = $value["DESCRIPTION"];

			if($bFileman)
			{
				// LANG_SHOW_MESS
				if((!isset($_REQUEST['ID']) || $_REQUEST['ID'] <= 0) && empty($GLOBALS['NOTE_ELID_IS_ON_PAGE']))
				{
					$GLOBALS['NOTE_ELID_IS_ON_PAGE'] = 'Y';
					echo BeginNote(). GetMessage("DEFATOOLS_BEFORE_ELEM_SAVE") .EndNote();
				}
				else
				{

					///////////////////////////////////////////
					/// UPLOAD  // FLASH
					///////////////////////////////////////////
					if(isset($_REQUEST['ID']) && $_REQUEST['ID'] > 0 && empty($GLOBALS['FLASH_IS_ON_PAGE']) &&  $plugin_type_edit == 'flash'):
						$GLOBALS['FLASH_IS_ON_PAGE'] = 'Y';

						$tools_dir = '/bitrix/tools/defatools';

						$APPLICATION->AddHeadString('<link rel="stylesheet" type="text/css" href="'.$tools_dir.'/uploadify/css/uploadify.css">');
						$APPLICATION->AddHeadString('<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>');
						$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$tools_dir.'/uploadify/js/swfobject.js"></script>');
						$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$tools_dir.'/uploadify/js/jquery.uploadify.v2.1.0.min.js"></script>');

					?>
						<script type="text/javascript">
							$(document).ready(function() {
								$('#fileinput').uploadify({
									'uploader': '<?=$tools_dir?>/uploadify/swf/uploadify.swf',
									'script': '<?=$tools_dir?>/uploadify/uploadify.php',
									'folder': '/bitrix/tmp',
									'cancelImg': '<?=$tools_dir?>/uploadify/toolimages/cancel.png',
									'scriptData': {'el_id':<?=$_REQUEST['ID']?>, 'iblock_id':<?=$_REQUEST['IBLOCK_ID']?>, 'prop_id':<?=$arProperty["ID"]?>, 'sessid': '<?=bitrix_sessid()?>', "<?=session_name()?>": "<?=session_id()?>"},
									'multi' : true,
									<?if(LANGUAGE_ID != 'en'):?>
									'buttonImg': '<?=$tools_dir?>/uploadify/toolimages/browse_<?=LANGUAGE_ID?>.gif',
									<?endif;?>

									<?if(!empty($arProperty['FILE_TYPE'])):?>
									<?
									  $str_exts = $arProperty['FILE_TYPE'];
									  if(strpos($arProperty['FILE_TYPE'], ',')):
										$str_exts =	'*.'.str_replace(', ', ';*.', trim($arProperty['FILE_TYPE']));
									  endif;
									?>
									'fileExt' : '<?=$str_exts?>',
									'fileDesc' : '<?=$str_exts?>',
									<?endif;?>

									'onAllComplete' : function(event, data) {
										window.location.href = '<?=$APPLICATION->GetCurPageParam(bitrix_sessid_get(), array("sessid"))?>';
									},
									'onInit' : function() {
										if(IfFlashIsAvailable())
										{
											$('#file_input_outer')
												.css({'width': '420px'})
												.find('.adm-input-file')
												.css({'height': 'auto'})
												.end().find('.adm-input-file').children('span').css({'padding-bottom': '10px', 'display': 'block'});
										}
										else
										{
											$('#file_input_outer').hide();
											$('#dt_flash_unav').show();
										}
									},
									'onComplete' : function(event, queueID, fileObj, response, data) {
										if(response != 'ok')
											alert( response );
									}

								});
							});

							function IfFlashIsAvailable () {
								try {
									var flashVersion = '0,0,0',
									Plugin = navigator.plugins['Shockwave Flash'] || ActiveXObject;
									flashVersion = Plugin.description || (function () {
										try {
											return (new Plugin('ShockwaveFlash.ShockwaveFlash')).GetVariable('$version');
										}
										catch (eIE) {}
									}());
								} catch(e) {}
								flashVersion = flashVersion.match(/^[A-Za-z\s]*?(\d+)[\.|,](\d+)(?:\s+[d|r]|,)(\d+)/);

								return flashVersion[1] > 0;

							}

						</script>

					<div id="file_input_outer">
						<input id="fileinput" name="fileinput" type="file" /> <br /><br />
						<a href="javascript:$('#fileinput').uploadifyUpload();"><?=GetMessage("DEFATOOLS_UPLOAD_FILES")?></a> | <a href="javascript:$('#fileinput').uploadifyClearQueue();"><?=GetMessage("DEFATOOLS_CLEAR_QUEUE")?></a>
					</div>

					<div id="dt_flash_unav" style="display:none;">
						<?=BeginNote().  GetMessage("DEFATOOLS_NOFLASH") .EndNote().'<br />'?>
					</div>

					<br clear="all" /><br clear="all" />

					<?
					endif;

					///////////////////////////////////////////
					/// UPLOAD  // JAVA
					///////////////////////////////////////////

					//  ������� ������ ������� - �� ������ ���� ������ ���� �� ��������
					if(isset($_REQUEST['ID']) && $_REQUEST['ID'] > 0 && empty($GLOBALS['APPLET_IS_ON_PAGE']) && $plugin_type_edit == 'java' ):

						$GLOBALS['APPLET_IS_ON_PAGE'] = 'Y'; // ��������� �����, ��� ���� ������ ��� �������

						?><script type="text/javascript" src="/bitrix/image_uploader/iuembed.js"></script><?
						require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/image_uploader/version.php");
						require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/image_uploader/localization.php");
						?>

					<div style="border: 1px solid #94918C; float: left; padding: 5px;">
					<script>

					function itemColsSelChange2(pEl, e) {
						return true;
					}

					<?
					$strFileMask = '*';
					if(strpos($arProperty['FILE_TYPE'], ',')):
						$strFileMask =	'*.'.str_replace(', ', ';*.', trim($arProperty['FILE_TYPE']));
					endif;
					?>

					window.oColAccess = {};

					//Create JavaScript object that will embed Image Uploader to the page.
					var iu = new ImageUploaderWriter("ImageUploaderML", 555, 300);
					iu.activeXControlCodeBase = "<?=$arAppletVersion["activeXControlCodeBase"]?>";
					iu.activeXClassId = "<?=$arAppletVersion["IuActiveXClassId"]?>";
					iu.activeXControlVersion = "<?=$arAppletVersion["IuActiveXControlVersion"]?>";
					//For Java applet only path to directory with JAR files should be specified (without file name).
					iu.javaAppletCodeBase = "<?=$arAppletVersion["javaAppletCodeBase"]?>";
					iu.javaAppletClassName = "<?=$arAppletVersion["javaAppletClassName"]?>";
					iu.javaAppletJarFileName = "<?=$arAppletVersion["javaAppletJarFileName"]?>";
					iu.javaAppletCached = false;
					iu.javaAppletVersion = "<?=$arAppletVersion["IuJavaAppletVersion"]?>";
					iu.addParam("LicenseKey", "Bitrix");
					iu.addParam("ShowDescriptions", "false");
					iu.addParam("AllowLargePreview", "true");

					//iu.showNonemptyResponse = "on"; // debug
					iu.showNonemptyResponse = "off";
					//Configure appearance.
					iu.addParam("PaneLayout", "TwoPanes");
					iu.addParam("ShowDebugWindow", "true");
					iu.addParam("AllowRotate", "true");
					iu.addParam("BackgroundColor", "#ffffff");
					//Configure URL files are uploaded to.
					//iu.addParam("AdditionalFormName", "ml_upload");
					iu.addParam("Action", "<?=$APPLICATION->GetCurPageParam('action=upload&'.bitrix_sessid_get(), array("action", "sessid"))?>");
					iu.addParam("RedirectUrl", "");
					iu.addParam("FileMask", "<?= $strFileMask?>");
					language_resources.addParams(iu);

					function ImageUploaderML_AfterUpload(Html)
					{
						try
						{
							var i1 = Html.indexOf('#JS#') + 4,
								i2 = Html.lastIndexOf('#JS#'),
								sGet = (i1 != -1 && i2 != i1) ? Html.substring(i1, i2) : '';
							return jsUtils.Redirect([], "<?=$APPLICATION->GetCurPageParam('action=redirect&'.bitrix_sessid_get(), array("action", "sessid"))?>" + sGet);

						}
						catch(e)
						{
							return jsUtils.Redirect([], "<?=$APPLICATION->GetCurPageParam(bitrix_sessid_get(), array("sessid"))?>");
						}
					}
					iu.addEventListener("AfterUpload", "ImageUploaderML_AfterUpload");

					//Tell Image Uploader writer object to generate all necessary HTML code to embed Image Uploader to the page.
					iu.writeHtml();
					</script>
					</div>
					<br clear="all" /><br clear="all" />

					<?

					elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'upload'):

						$fileCount = intval($_POST['FileCount']);
						if ($fileCount > 0):

							for ($i = 1; $i <= $fileCount; $i++):
								$arFile = $_FILES['SourceFile_'.$i];
								$arFile["MODULE_ID"] = "iblock";

								CIBlockElement::SetPropertyValues($_REQUEST['ID'], $_REQUEST['IBLOCK_ID'], $arFile, $arProperty["ID"]);

							endfor;
						endif;
						die('#JS#&files_up='. ($i - 1) .'#JS#');

					endif; // APPLET_IS_ON_PAGE

					echo CMedialib::InputFile(	$strHTMLControlName["VALUE"], $val,
						array("IMAGE" => "Y", "PATH" => "Y", "FILE_SIZE" => "Y", "DIMENSIONS" => "Y",
						"IMAGE_POPUP"=>"Y", "MAX_SIZE" => array("W" => 200, "H"=>200)), //info
						array("SIZE"=>$cols), //file
						array(), //server
						array(), //media lib
						($arProperty["WITH_DESCRIPTION"]=="Y"?
							array("NAME" => "DESCRIPTION_".$strHTMLControlName["VALUE"]
								, "VALUE" => $val_description
							):
							false
						), //descr
						array() //delete
					);
				}
			} //if($bFileman)
			else
			{
				echo CFile::InputFile($strHTMLControlName["VALUE"], $cols, $val, false, 0, "")."<br>";
				echo CFile::ShowFile($val, $max_file_size_show, 400, 400, true)."<br>";

				if($arProperty["WITH_DESCRIPTION"]=="Y")
					echo ' <span title="'.GetMessage("DEFATOOLS_ELEMENT_EDIT_PROP_DESC").'">'.GetMessage("DEFATOOLS_ELEMENT_EDIT_PROP_DESC_1").'<input name="DESCRIPTION_'.$strHTMLControlName["VALUE"].'" value="'.htmlspecialcharsex($val_description).'" size="18" type="text"></span>';

			}
			echo '<br></td></tr>';
			echo '</table>';

			$return = ob_get_contents();
			ob_end_clean();

			return $return;
		}

		function ConvertToDB($arProperty, $value){
			$return = array();

			// setting description
			if ($arProperty['WITH_DESCRIPTION'] == "Y" && is_array($value["DESCRIPTION"])) {
				$description = trim($value["DESCRIPTION"]["VALUE"]);
			} else {
				$description = "";
			}

			// assume that $value contains value only for 1 item
			$value = $value['VALUE'];

			// and again)
			if (isset($value['VALUE'])) {
				$value = $value['VALUE'];
			}

			// save file
			$return['DESCRIPTION'] = $description;

			if (isset($value["size"]) && $value["size"] > 0){

				$arFile              = $value;
				$arFile["MODULE_ID"] = "iblock";

				$file_id = CFile::SaveFile($arFile, "iblock");

				if (intval($file_id) > 0){
					$return['VALUE'] = $file_id;
				}
				// remove file
			} elseif (!empty($value["del"]['VALUE'])) {
				$arFile["MODULE_ID"] = "iblock";
				$arFile["del"]       = "Y";

				$return['VALUE'] = $arFile;
			} else {
				$return['VALUE'] = $value;
			}

			return $return;
		}

		function ConvertFromDB($arProperty, $value)
		{
			return $value;
		}
	}
} // class exists

?>
