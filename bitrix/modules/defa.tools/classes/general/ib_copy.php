<?

IncludeModuleLangFile(__FILE__);

class DefaTools_IB_Copy extends DefaTools_AdminService
{

	/**
	 * Top level keys of this array are names of function, that use it
	 * @var array
	 */
	private $urlPatterns = array(
		'GetActionsMenu' => array(
			'url' => array("/bitrix/admin/iblock_admin.php"),
		),
		'GetTopMenu' => array(
			'url' => array("/bitrix/admin/iblock_element_admin.php", "/bitrix/admin/iblock_section_admin.php", "/bitrix/admin/iblock_list_admin.php"),
		),
		'GetGroupActions' => array(
			'url' => array("/bitrix/admin/iblock_element_admin.php", "/bitrix/admin/iblock_section_admin.php", "/bitrix/admin/iblock_list_admin.php"),
		)
	);

	public function __construct()
	{
		parent::__construct();
	}

	public function getUrlPatterns()
	{
		return $this->urlPatterns;
	}

	public function GetGroupActions()
	{
		if(in_array($GLOBALS['APPLICATION']->GetCurPage(), $this->urlPatterns[__FUNCTION__]['url']))
		{
			return array("ib_copy_ib_section_to_new_ib" => GetMessage("DEFATOOLS_IB_DEMO_COPY_SECTION_TO_THE_NEW_IB"));
		}
	}

	/**
	 * Returns menu array for CAdminList item
	 * @return array|null
	 */
	public function GetActionsMenu()
	{
		if(in_array($GLOBALS['APPLICATION']->GetCurPage(), $this->urlPatterns[__FUNCTION__]['url'])
			&& $_REQUEST['admin'] == "Y")
		{
			$arTypesEx = CIBlockParameters::GetIBlockTypes();
			$strSelect = '<select name="defa_custom_param[type]">';
			foreach ($arTypesEx as $type => $name)
				$strSelect .= "<option value=\"" . $type . "\" " . ($type == $_REQUEST['type'] ? "selected=true" : "") .
					">" . $name . "</option>";

			$strSelect .= "</select>";
			$menu = array(
				array(
					"ICON" => "copy",
					"DEFAULT" => true,
					"TEXT" => GetMessage("DEFATOOLS_IB_DEMO_COPYIB"),
					"ACTION" => "javascript:(new BX.CDialog({
									content_url: '" . $GLOBALS["APPLICATION"]->GetCurPageParam("", array("mode", "table_id", "defa_custom_action")) . "',
									width: 500,
									height: 140,
									resizable: false,
									draggable: false,
									title: '" . GetMessage("DEFATOOLS_IB_DEMO_COPYIB") . "',
									head: '" . GetMessage("DEFATOOLS_IB_DEMO_CHOOSE_COPY_PARAM") . "',
									content: '<form action=\"\" name=\"defa_custom_action_form\"><input type=\"hidden\" name=\"defa_custom_action\" value=\"ib_copy_ib\"><table><tr><td>"
						. GetMessage("DEFATOOLS_IB_DEMO_COPYIB_TO_TYPE") . ": </td><td>" . $strSelect . "</td></tr><tr><td>"
						. GetMessage("DEFATOOLS_IB_DEMO_COPYIB_CONTENT") . ": </td><td><input name=\"defa_custom_param[copy_content]\" "
						. (COption::GetOptionString("main", "_demo_content_copy_content", "Y") == "Y" ? "checked" : "")
						. " type=\"checkbox\" value=\"Y\" /></td></tr>"),
			);
			return $menu;
		} else return null;
	}

	/**
	 * Returns menu array for CAdminContextMenu
	 * @return array|null
	 */
	public function GetTopMenu()
	{
		if (in_array($GLOBALS['APPLICATION']->GetCurPage(), $this->urlPatterns[__FUNCTION__]['url']) && $this->IBLOCK_ID > 0) {
			$arTypesEx = CIBlockParameters::GetIBlockTypes();
			$resIblock = CIBlock::GetById($this->IBLOCK_ID)->GetNext();
			$strSelect = '<select name="defa_custom_param[type]">';
			foreach ($arTypesEx as $type => $name)
				$strSelect .= "<option value=\"" . $type . "\" " . ($type == $resIblock["IBLOCK_TYPE_ID"] ? "selected=true" : "") .
					">" . $name . "</option>";
			$strSelect .= "</select>";
			$menu = array();
			$menu[] = array(
				"TEXT" => GetMessage("DEFATOOLS_IB_DEMO_COPYIB"),
				"TITLE" => GetMessage("DEFATOOLS_IB_DEMO_COPYIB"),
				"ACTION" => "javascript:(new BX.CDialog({
					content_url: '" . $GLOBALS["APPLICATION"]->GetCurPageParam("", array("mode", "table_id", "defa_custom_action")) . "',
					width: 500,
					height: 140,
					resizable: false,
					draggable: false,
					title: '" . GetMessage("DEFATOOLS_IB_DEMO_COPYIB") . "',
					head: '" . GetMessage("DEFATOOLS_IB_DEMO_CHOOSE_COPY_PARAM") . "',
					content: '<form action=\"\" name=\"defa_custom_action_form\"><input type=\"hidden\" name=\"defa_custom_action\" value=\"ib_copy_ib\"><table><tr><td>"
					. GetMessage("DEFATOOLS_IB_DEMO_COPYIB_TO_TYPE") . ": </td><td>" . $strSelect . "</td></tr><tr><td>"
					. GetMessage("DEFATOOLS_IB_DEMO_COPYIB_CONTENT") . ": </td><td><input name=\"defa_custom_param[copy_content]\" "
					. (COption::GetOptionString("main", "_demo_content_copy_content", "Y") == "Y" ? "checked" : "")
					. " type=\"checkbox\" value=\"Y\" /></td></tr></table></form>',
					buttons: [BX.CDialog.btnSave, BX.CDialog.btnCancel]
				})).Show()",
				"ICON" => "copy",
			);
			return $menu;
		} else return null;
	}

	/**
	 * This function is called by AdminServiceManager and do action if it specified in switch.
	 * @param $action
	 * @return null
	 */
	public function doAction($action)
	{
		if (!CModule::IncludeModule("iblock") || !$GLOBALS['USER']->IsAdmin())
			return;

		switch ($action) {
			case "ib_copy_ib":
				$this->doCopyIB();
				break;
			default:
				return null;
		}
	}

	/**
	 * This function is called by AdminServiceManager and do action if it specified in switch.
	 * @param $action
	 * @return null
	 */
	public function doGroupAction($action)
	{
		if (!CModule::IncludeModule("iblock") || !$GLOBALS['USER']->IsAdmin())
			return;

		switch ($action) {
			case "ib_copy_ib_section_to_new_ib":
				$this->doCopySectionToIB();
				break;
			default:
				return null;
		}
	}

	private function doCopySectionToIB()
	{
		$sectionIDs = array();

		foreach ($_REQUEST["ID"] as $mixedID)
		{
			if (substr($mixedID, 0, 1) == "S")
			{
				$sectionIds[] = substr($mixedID, 1);
			}
		}

		if (!empty($sectionIds))
		{
			foreach ($sectionIds as $id){
				$rsSections = CIBlockSection::GetList(array(), array("SECTION_ID" => $id), true, array("ID", "LEFT_MARGIN", "RIGHT_MARGIN"));
				while($arSection = $rsSections->GetNext()){
					$sectionIds[] = $arSection['ID'];
				}
			}

			$NEW_IBLOCK_ID = $this->CopyIBlock($this->IBLOCK_ID, $_REQUEST['type'], true, false, $sectionIds);

		}
		else
		{
			$NEW_IBLOCK_ID = $this->CopyIBlock($this->IBLOCK_ID, $_REQUEST['type'], true, false);
		}
		if ($NEW_IBLOCK_ID > 0)
		{
			?><script>top.location.href='<?=$GLOBALS["APPLICATION"]->GetCurPageParam("IBLOCK_ID=".$NEW_IBLOCK_ID, array("IBLOCK_ID", "mode", "find_section_section"))?>';</script><?
		}
	}

	public function syncSectionCatalogToIblockCatalog($FROM_IBLOCK_ID, $TO_IBLOCK_ID)
	{
		if (!CModule::IncludeModule("iblock")) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
			return false;
		}
		if (CModule::IncludeModule("catalog") && CCatalog::GetByID($FROM_IBLOCK_ID)) {
			CCatalog::Add(array("IBLOCK_ID" => $TO_IBLOCK_ID, "YANDEX_EXPORT" => "N", "SUBSCRIPTION" => "N"));
		}
	}

	/**
	 *Private function, that recieve params, makes a copy of infoblock using CopyIBlock and
	 * print result of those actions with link to just created infoblock.
	 * Must be used only with doAction public function.
	 * If you wand to copy infoblock from your code, you must do it via CopyIBlock function.
	 * It works only in admin section.
	 */
	private function doCopyIB()
	{
		$GLOBALS["APPLICATION"]->RestartBuffer();
		if(is_numeric($_REQUEST["defa_custom_id"]))
		{
			$this->IBLOCK_ID = $_REQUEST["defa_custom_id"];
		}
		$NEW_ID = $this->CopyIBlock($this->IBLOCK_ID, $_REQUEST["defa_custom_param"]["type"],
			($_REQUEST["defa_custom_param"]["copy_content"] == "Y"));
		$GLOBALS["APPLICATION"]->RestartBuffer();
		$new_url = preg_replace("/(iblock_admin)/i", "iblock_list_admin", $GLOBALS["APPLICATION"]->GetCurPageParam("IBLOCK_ID=" . $NEW_ID . "&type="
			. $_REQUEST["defa_custom_param"]["type"], array("defa_custom_action", "IBLOCK_ID", "type", "find_section_section", "admin")));
		echo "<div align=\"center\"><a style=\"font-size: 20px\" href=\"javascript:window.location='"
			. CUtil::JSEscape($new_url)
			. "'\">" . GetMessage("DEFATOOLS_IB_DEMO_GOTO_NEW_IB") . "</a></div>";
		die();
	}

	/**
	 * Copies infoblock with specified ID and returns ID of created.
	 * @param $ID
	 * @param $type
	 * @param bool $copyContent
	 * @param bool $checkUnique
	 * @param null $sectionIds
	 * @return bool|int
	 */
	public function CopyIBlock($ID, $type, $copyContent = true, $checkUnique = false, $sectionIds = null)
	{
		$res = CIBlock::GetByID($ID)->Fetch();
		$res["IBLOCK_TYPE_ID"] = $type;
		if (is_set($res, "PICTURE") && intval($res["PICTURE"]) > 0)
			$res["PICTURE"] = CFile::MakeFileArray($res["PICTURE"]);

		if ($checkUnique) {
			$res["EXTERNAL_ID"] = 'copy_' . $res["ID"];

			$rsIBlock = CIBlock::GetList(array(), array("TYPE" => $res["IBLOCK_TYPE_ID"], "=XML_ID" => $res["EXTERNAL_ID"]));
			if ($arIBlock = $rsIBlock->Fetch())
				return $arIBlock["ID"];
		}

		$ib = new CIBlock();
		if ($NEW_ID = $ib->Add($res)) {
			CIBlock::SetFields($NEW_ID, CIBlock::GetFields($ID));
			CIBlock::SetPermission($NEW_ID, CIBlock::GetGroupPermissions($ID));

			$arProperties = $arPropertyEnums = $arUFProperties = $arUFPropertyEnums = array();

			self::syncIblockCatalog($ID, $NEW_ID);
			self::syncIblockProperties($ID, $NEW_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums);
			self::syncIblockPropertiesUserSettings($ID, $NEW_ID, $arProperties, $arUFProperties);

			if ($copyContent) {
				self::syncIblockContent($ID, $NEW_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums, $sectionIds);
			}
		}
		return $NEW_ID;
	}

	public function syncIblockContent($FROM_IBLOCK_ID, $TO_IBLOCK_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums, $sectionIds = null)
	{

		global $DB, $USER_FIELD_MANAGER;

		if (!CModule::IncludeModule("iblock")) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
			return false;
		}

		if (CModule::IncludeModule("catalog") && CCatalog::GetByID($FROM_IBLOCK_ID)) {
			$flagCopyCatalogProperties = true;
		} else {
			$flagCopyCatalogProperties = false;
		}

		$bs = new CIBlockSection;
		$be = new CIBlockElement;

		$arSections = $arElements = $arElementProps = array();

		if ($sectionIds) {
			$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $FROM_IBLOCK_ID, "ID" => $sectionIds), false, array("SELECT" => "UF_*"));
		} else {
			$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $FROM_IBLOCK_ID), false, array("SELECT" => "UF_*"));
		}
		while ($resSection = $rsSection->Fetch()) {

			$ufProps = $USER_FIELD_MANAGER->GetUserFields("IBLOCK_" . $FROM_IBLOCK_ID . "_SECTION", $resSection["ID"]);

			foreach (array("PICTURE", "DETAIL_PICTURE") as $code) {
				if (is_set($resSection, $code) && intval($resSection[$code]) > 0)
					$resSection[$code] = CFile::MakeFileArray($resSection[$code]);
			}

			$resSection["IBLOCK_ID"] = $TO_IBLOCK_ID;

			foreach (array("GLOBAL_ACTIVE", "LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL", "IBLOCK_TYPE_ID", "IBLOCK_CODE", "IBLOCK_EXTERNAL_ID", "LIST_PAGE_URL", "SECTION_PAGE_URL", "SEARCHABLE_CONTENT") as $code)
				unset($resSection[$code]);

			if (intval($resSection["IBLOCK_SECTION_ID"]) > 0) {
				$resSection["IBLOCK_SECTION_ID"] = $arSections[$resSection["IBLOCK_SECTION_ID"]]["NEW_ID"];
			}

			unset($resSection["TIMESTAMP_X"]);

			foreach ($ufProps as $k => $v) {
				if (!empty($v["VALUE"]) && is_set($resSection, $k) && !empty($resSection[$k])) {

					switch ($v["USER_TYPE_ID"]) {
						case "enumeration":

							if (is_array($resSection[$k])) {
								foreach ($resSection[$k] as $kk => $vv)
									$resSection[$k][$kk] = $arUFPropertyEnums[$vv];
							} else
								$resSection[$k] = $arUFPropertyEnums[$resSection[$k]];

							break;
						case "file":

							if (is_array($resSection[$k])) {
								foreach ($resSection[$k] as $kk => $vv)
									$resSection[$k][$kk] = CFile::MakeFileArray($vv);
							} else
								$resSection[$k] = CFile::MakeFileArray($resSection[$k]);

							break;
					}
				}
			}

			if ($NEW_SECTION_ID = $bs->Add($resSection, true, true, true)) {
				$resSection["NEW_ID"] = $NEW_SECTION_ID;
			} else {
				// error
			}

			$arSections[$resSection["ID"]] = $resSection;
		}

		$old2newIDS = array();
		if ($sectionIds) {
			$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $FROM_IBLOCK_ID, "SECTION_ID" => $sectionIds));
		} else {
			$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $FROM_IBLOCK_ID));
		}

		while ($obElement = $rsElement->GetNextElement()) {
			$oldResElement = $obElement->GetFields();
			$resElement = array();

			$OLD_ID = $oldResElement["ID"];
			$resElementProps = $obElement->GetProperties();

			foreach ($oldResElement as $k => $v)
				if (substr($k, 0, 1) == "~" && !empty($v))
					$resElement[substr($k, 1)] = $v;

			foreach (array("PREVIEW_PICTURE", "DETAIL_PICTURE") as $code) {
				if (is_set($resElement, $code) && intval($resElement[$code]) > 0)
					$resElement[$code] = CFile::MakeFileArray($resElement[$code]);
				else {
					unset($resElement[$code]);
					unset($resElement["~" . $code]);
				}
			}

			foreach (array("ID", "LOCK_STATUS", "WF_DATE_LOCK", "WF_LAST_HISTORY_ID", "WF_LOCKED_BY", "WF_NEW", "WF_PARENT_ELEMENT_ID") as $code) {
				unset($resElement[$code]);
				unset($resElement["~" . $code]);
			}

			if ($resElement["IBLOCK_SECTION_ID"] > 0)
				$resElement["IBLOCK_SECTION_ID"] = $arSections[$resElement["IBLOCK_SECTION_ID"]]["NEW_ID"];

			$resElement["IBLOCK_ID"] = $TO_IBLOCK_ID;
			$arElementProps = array();

			foreach ($resElementProps as $fields) {

				foreach (array("VALUE", "DESCRIPTION", "VALUE_XML_ID", "VALUE_ENUM_ID") as $code) {
					if (!is_array($fields[$code]))
						$fields[$code] = array($fields[$code]);
				}

				foreach ($fields["VALUE"] as $propKey => $propValue) {
					switch ($fields["PROPERTY_TYPE"]) {
						case "F":
							$arElementProps[$arProperties[$fields["ID"]]][] = array("VALUE" => CFile::MakeFileArray($fields["VALUE"][$propKey]), "DESCRIPTION" => $fields["DESCRIPTION"][$propKey]);
							break;
						case "L":
							$arElementProps[$arProperties[$fields["ID"]]][] = array("VALUE" => $arPropertyEnums[$fields["VALUE_ENUM_ID"][$propKey]], "DESCRIPTION" => $arPropertyEnums[$fields["DESCRIPTION"][$propKey]]);
							break;
						default:
							$arElementProps[$arProperties[$fields["ID"]]][] = array("VALUE" => $fields["VALUE"][$propKey], "DESCRIPTION" => $fields["DESCRIPTION"][$propKey]);
							break;
					}
				}
			}
			$resElement["PROPERTY_VALUES"] = $arElementProps;

			if ($NEW_ELEMENT_ID = $be->Add($resElement)) {

				$rsElementSections = CIBlockElement::GetElementGroups($OLD_ID, true);
				$resElementSections = array();

				while ($section = $rsElementSections->Fetch())
					$resElementSections[] = $arSections[$section["ID"]]["NEW_ID"];

				CIBlockElement::SetElementSection($NEW_ELEMENT_ID, $resElementSections);

			} else {

				$GLOBALS["APPLICATION"]->ThrowException($be->LAST_ERROR);
				return false;
				// error
			}
			$old2newIDS[$OLD_ID] = $NEW_ELEMENT_ID;
		}

		// if infoblock mapped to catalog
		if ($flagCopyCatalogProperties) {
			foreach ($old2newIDS as $old => $new) {

				// 1. copy CPrice data
				$priceRes = CPrice::GetList(array(), array("PRODUCT_ID" => $old));

				while ($price = $priceRes->Fetch()) {

					$price['PRODUCT_ID'] = $new;
					unset($price['ID'], $price['TIMESTAMP_X']);
					CPrice::Add($price);
				}

				unset($fields, $priceRes, $price);

				// 2. copy CCatalogProduct data
				$product = CCatalogProduct::GetByID($old);
				$product['ID'] = $new;
				unset($product['TIMESTAMP_X']);

				CCatalogProduct::Add($product);
				unset($product);
			}
		}
	}

	function syncIblockPropertiesUserSettings($FROM_IBLOCK_ID, $TO_IBLOCK_ID, $arProperties = array(), $arUFProperties)
	{

		CModule::IncludeModule("iblock");

		foreach ($arProperties as $k => $v) {
			$arPropertiesNew["--PROPERTY_" . $k . "--"] = "--PROPERTY_" . $v . "--";
			$arPropertiesNewClear["PROPERTY_" . $k] = "PROPERTY_" . $v;
		}

		$iblockHashes = array();
		foreach (array($FROM_IBLOCK_ID, $TO_IBLOCK_ID) as $iblock) {
			$rs = CIBlock::GetById($iblock);
			$res = $rs->Fetch();
			$iblockHashes[$res["ID"]] = "tbl_iblock_list_" . md5($res["IBLOCK_TYPE_ID"] . "." . $res["ID"]);
		}

		foreach (array($GLOBALS['USER']->GetID, false) as $user) {

			foreach (array("section", "element") as $type) {
				// form
				$res = CUserOptions::GetOption("form", "form_" . $type . "_" . $FROM_IBLOCK_ID, false, $user);

				if (!empty($res["tabs"])) {
					$res["tabs"] = str_replace(array_keys($arPropertiesNew), array_values($arPropertiesNew), $res["tabs"]);
					CUserOptions::SetOption("form", "form_" . $type . "_" . $TO_IBLOCK_ID, $res, ($user === false ? "Y" : "N"), $user);
				}
				// /form
			}

			// list
			$res = CUserOptions::GetOption("list", $iblockHashes[$FROM_IBLOCK_ID], false, $user);

			if ($res["columns"]) {
				$res["columns"] = explode(",", $res["columns"]);
				foreach ($res["columns"] as $k => $v) {
					if (isset($arPropertiesNewClear[$v]))
						$res["columns"][$k] = $arPropertiesNewClear[$v];
				}
				$res["columns"] = implode(",", $res["columns"]);
			}

			if (isset($res["by"]) && isset($arPropertiesNewClear[$res["by"]]))
				$res["by"] = $arPropertiesNewClear[$res["by"]];

			CUserOptions::SetOption("list", $iblockHashes[$TO_IBLOCK_ID], $res, ($user === false ? "Y" : "N"), $user);
			// /list
		}

	}

	function syncIblockProperties($FROM_IBLOCK_ID, $TO_IBLOCK_ID, &$arProperties = array(), &$arPropertyEnums = array(), &$arUFProperties = array(), &$arUFPropertyEnums = array())
	{
		if (!CModule::IncludeModule("iblock")) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
			return false;
		}

		if (!is_array($arUFPropertyEnums))
			$arUFPropertyEnums = array();


		$obUserField = new CUserTypeEntity;
		$obEnum = new CUserFieldEnum;

		$arFilter = array(
			"ENTITY_ID" => "IBLOCK_" . $FROM_IBLOCK_ID . "_SECTION",
		);
		$rsData = CUserTypeEntity::GetList(array(), $arFilter);
		while ($resData = $rsData->Fetch()) {

			$UF_ID = $resData["ID"];
			$resData = CUserTypeEntity::GetByID($UF_ID);

			if ($resData["USER_TYPE_ID"] == "enumeration") {
				$rsEnum = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $resData["ID"]));
				$enum = 0;
				while ($resEnum = $rsEnum->Fetch()) {
					$resData["ENUM"]["n" . $enum++] = $resEnum;
				}
			}

			$resData["ENTITY_ID"] = "IBLOCK_" . $TO_IBLOCK_ID . "_SECTION";

			$rsDataExists = CUserTypeEntity::GetList(array(), array("ENTITY_ID" => $resData["ENTITY_ID"],
					"FIELD_NAME" => $resData["FIELD_NAME"],)
			);

			if ($resDataExists = $rsDataExists->Fetch()) {

				$NEW_ID = $resDataExists["ID"];
				unset($resData["ID"]);

				$obUserField->Update($NEW_ID, $resData);
				if (!empty($resData["ENUM"])) {

					foreach ($resData["ENUM"] as $k => $v) {
						unset($resData["ENUM"][$k]["USER_FIELD_ID"]);
						unset($resData["ENUM"][$k]["ID"]);
					}

					$obEnum->SetEnumValues($NEW_ID, $resData["ENUM"]);
				}
			} else {
				unset($resData["ID"]);
				$NEW_ID = $obUserField->Add($resData);

				if (!empty($resData["ENUM"])) {
					foreach ($resData["ENUM"] as $k => $v) {
						unset($resData["ENUM"][$k]["USER_FIELD_ID"]);
						unset($resData["ENUM"][$k]["ID"]);
					}

					$obEnum->SetEnumValues($NEW_ID, $resData["ENUM"]);
				}
			}

			$resEnumsOld = $resEnumsNew = array();
			$rsEnums = $obEnum->GetList(array(), array("USER_FIELD_ID" => $UF_ID));
			while ($resEnums = $rsEnums->Fetch()) {
				$resEnumsOld[] = $resEnums["ID"];
			}

			$rsEnums = $obEnum->GetList(array(), array("USER_FIELD_ID" => $NEW_ID));
			while ($resEnums = $rsEnums->Fetch()) {
				$resEnumsNew[] = $resEnums["ID"];
			}

			if (count($resEnumsOld) == count($resEnumsOld) && !empty($resEnumsOld)) {
				$arUFPropertyEnums += array_combine($resEnumsOld, $resEnumsNew);
			}

			$arUFProperties[$UF_ID] = $NEW_ID;

		}

		$arUpdateProperties = array();
		$rsProperty = CIBlockProperty::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $FROM_IBLOCK_ID));
		while ($arProperty = $rsProperty->Fetch()) {
			$arUpdateProperties[] = $arProperty;
		}

		foreach ($arUpdateProperties as $arProperty) {

			$arProperty["IBLOCK_ID"] = $TO_IBLOCK_ID;
			$arProperty["XML_ID"] = "PROP_" . $arProperty["ID"];

			$ibp = new CIBlockProperty;
			$rs = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arProperty["IBLOCK_ID"], "XML_ID" => $arProperty["XML_ID"]));
			if ($ar = $rs->Fetch()) {
				$ID = $ar["ID"];
				$ibp->Update($ar["ID"], $arProperty);
				$arProperties[$arProperty["ID"]] = $ar["ID"];
			} else {
				if (!($ID = $ibp->Add($arProperty))) {
					$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_PROP_CREATE_ERR") . ': ' . $ibp->LAST_ERROR);
				}
				$arProperties[$arProperty["ID"]] = $ID;
			}

			$arUpdateEnums = array();
			if ($arProperty["PROPERTY_TYPE"] == 'L') {
				if ($ID) {
					$rs = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array("PROPERTY_ID" => $arProperty["ID"]));
					while ($ar = $rs->Fetch()) {

						$ar["IBLOCK_ID"] = $arProperty["IBLOCK_ID"];
						$ar["PROPERTY_ID"] = $ID;
//									$ar["XML_ID"] = "ENUM_".$ar["ID"];
						$arUpdateEnums[] = $ar;
					}
				}
			}

			foreach ($arUpdateEnums as $arEnum) {
				$ibpenum = new CIBlockPropertyEnum;

				$rs = CIBlockPropertyEnum::GetList(array(), array("PROPERTY_ID" => $ID, "XML_ID" => $arEnum["XML_ID"]));
				if ($ar = $rs->Fetch()) {
					$arPropertyEnums[$arEnum["ID"]] = $ar["ID"];
					unset($arEnum["ID"]);
					$ibpenum->Update($ar["ID"], $arEnum);
				} else {
					$arEnumID = $arEnum["ID"];
					unset($arEnum["ID"]);
					if (!($ENUM_ID = $ibpenum->Add($arEnum))) {
						$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_PROP_ADD_LTYPE_ERR"));
					}
					$arPropertyEnums[$arEnumID] = $ENUM_ID;
				}
			}
		}

	}

	function syncIblockCatalog($FROM_IBLOCK_ID, $TO_IBLOCK_ID)
	{
		if (!CModule::IncludeModule("iblock")) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
			return false;
		}

		if (CModule::IncludeModule("catalog") && CCatalog::GetByID($FROM_IBLOCK_ID)) {
			CCatalog::Add(array("IBLOCK_ID" => $TO_IBLOCK_ID, "YANDEX_EXPORT" => "N", "SUBSCRIPTION" => "N"));
		}
	}

	/**
	 * Check user permissions for modify element. If check fails return false.
	 * @param $arFields
	 * @return bool
	 */
	public function CheckElementModifyPermissions(&$arFields)
	{
		$element = CIBlockElement::GetList(array(), array("ID" => $arFields['ID']), false, false, array("XML_ID", "CREATED_BY"))->Fetch();

		if (!strncmp($element['XML_ID'], 'DEFADEMO', 8) && !($GLOBALS['USER']->GetID() == $element['CREATED_BY'])) {
			$GLOBALS['APPLICATION']->throwException(GetMessage("DEFATOOLS_IB_DEMO_ELEMENT_OWNER_CHECK_FAILS"));
			return false;
		}
	}

	/**
	 * Check user permissions for modify sections. If check fails return false.
	 * @param $arFields
	 * @return bool
	 */
	public function CheckSectionModifyPermissions(&$arFields)
	{
		$element = CIBlockSection::GetList(array(), array("ID" => $arFields['ID']), false)->Fetch();

		if (!strncmp($element['XML_ID'], 'DEFADEMO', 8) && !($GLOBALS['USER']->GetID() == $element['CREATED_BY'])) {
			$GLOBALS['APPLICATION']->throwException(GetMessage("DEFATOOLS_IB_DEMO_SECTION_OWNER_CHECK_FAILS"));
			return false;
		}
	}
}
