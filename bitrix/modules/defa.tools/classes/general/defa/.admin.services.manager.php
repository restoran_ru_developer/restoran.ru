<?
interface IDefaTools_AdminServicesManager
{

    /**
     * @static
     * @abstract
     * @return mixed
     */
    public static function getInstance();


    /**
     * @abstract
     * @param DefaTools_AdminService $tab
     */
    public function addService(DefaTools_AdminService $tab);


    /**
     * @abstract
     * @return array
     */
    public function getServices();
}
?>