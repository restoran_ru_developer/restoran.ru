<?
//require_once("defa.inc.php");

class DefaTools_AdminServicesManager implements IDefaTools_AdminServicesManager
{
    /**
     * @var DefaTools_AdminServicesManager
     */
    protected static $instance;

    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    /**
     * Singleton
     * @static
     * @return DefaTools_AdminServicesManager
     */
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new DefaTools_AdminServicesManager();
        }
        return self::$instance;
    }

    /**
     *@var array[DefaTools_AdminService]
     */
    protected $services = array();

    /**
     * Add service to this manager. All addings are in the "include.php" file at the root of this module.
     * Using like this: DefaTools_AdminServicesManager::getInstance()->addService(new DefaTools_IB_Demo());
     * @param DefaTools_AdminService $service
     */
    public function addService(DefaTools_AdminService $service)
    {
        $this->services[] = $service;
    }

    /**
     * Return array of current services
     * @return array[DefaTools_AdminService]
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Handler for OnAdminContextMenuShow event
     * @param $mainMenu CAdminContextMenu
     */
    public function ContextMenuShowHandler(&$mainMenu)
    {
        // parse defa_custom_action param
        if($_REQUEST['defa_custom_action'])
		{
            foreach(self::$instance->getServices() as $srv)
                $srv->doAction($_REQUEST['defa_custom_action']);
		}
		elseif(is_set($_REQUEST, "action"))
		{
            foreach(self::$instance->getServices() as $srv)
                $srv->doGroupAction($_REQUEST["action"]);
		}

        // get top menu from stored services
        $menu = array();

        foreach(self::$instance->getServices() as $srv)
            if($srv->GetTopMenu())
                foreach($srv->GetTopMenu() as $item)
                    $menu[] = $item;

        // add Defa Menu
        if(count($menu) > 0)
        {
            $mainMenu[] = array("SEPARATOR" => "1");
            $mainMenu[] = array(
                "TEXT" => GetMessage("DEFATOOLS_IB_DEMO_IB_TOOLS"),
                "TITLE" => GetMessage("DEFATOOLS_IB_DEMO_IB_TOOLS"),
                "ICON" => "btn_new_defatools",
                "MENU" => $menu
            );
            $GLOBALS["APPLICATION"]->AddHeadString('<style> table.contextmenu #btn_new_defatools {  background-image:'
                .' url("/bitrix/tools/defatools/menu/images/defatools_menu_icon.gif"); }  </style>');
        }
    }

    /**
     * Handler for OnAdminListDisplay event
     * @param $list CAdminList
     */
    public function ListDisplayHandler(&$list)
    {
        $menu = array();
        foreach(self::$instance->getServices() as $srv)
		{
            if($srv->GetActionsMenu())
			{
                foreach($srv->GetActionsMenu() as $item)
				{
                    $menu[] = $item;
				}
			}

            if($srv->GetGroupActions())
			{
                foreach($srv->GetGroupActions() as $action => $item)
				{
                    $list->arActions[$action] = $item;
				}
			}
		}

        if (!strncmp($list->table_id, 'tbl_iblock_admin', 16 && false) && count($menu)) {
            foreach ($list->aRows as $row) {
                $row_menu = $menu;
                $row_menu[0]['ACTION'] .= "<input type=\"hidden\" name=\"defa_custom_id\" value=\"".$row->id."\">"
                ."</table></form>',
                 buttons: [BX.CDialog.btnSave, BX.CDialog.btnCancel]})).Show()";
	            $row->addActions(array_merge($row->aActions, $row_menu));
            }
        }

    }

}
?>
