<?

abstract class DefaTools_AdminService implements IDefaTools_AdminService
{
    const MODULE_ID = "defa.tools";
    protected $IBLOCK_ID;

    public function __construct()
    {
		global $USER;

		if (!is_object($USER))
			$USER = new CUser;

        if (!$USER->IsAdmin())
            return;

        $this->IBLOCK_ID = intval($_REQUEST["IBLOCK_ID"]);
    }

    /**
     * Return url patterns. using them you can determine should you do something or not.
     * top level keys of array are names of functions, which use this pattens.
     * @abstract
     * @return array
     */
    abstract function GetUrlPatterns();

    /**
     * @abstract
     * @return array
     */
    abstract function GetTopMenu();

    /**
     * @abstract
     * @return array
     */
    abstract function GetActionsMenu();

	/**
     * @abstract
     * @return array
     */
	abstract function GetGroupActions();
}
?>
