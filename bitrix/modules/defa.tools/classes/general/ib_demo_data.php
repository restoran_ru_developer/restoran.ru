<?
IncludeModuleLangFile(__FILE__);

class DefaTools_IB_DemoData {   
	
	function ContextMenuShowHandler(&$mainMenu) {
	
		global $USER, $DB;
	
		if(!$USER->IsAdmin())
			return;
	
		$ID = intval($_REQUEST["IBLOCK_ID"]);
	
		if (in_array($_SERVER["PHP_SELF"], array("/bitrix/admin/iblock_element_admin.php", "/bitrix/admin/iblock_section_admin.php", "/bitrix/admin/iblock_list_admin.php")) && $ID>0) {
	
			if ($_REQUEST["defa_custom_action"] == "ib_add_demo_content" && isset($_REQUEST["defa_custom_param"])) {
				foreach($_REQUEST["defa_custom_param"] as $code => $value)
					COption::SetOptionString("main", "_demo_content_".$code, $value);
			}
	
			// copy IB
			CModule::IncludeModule("iblock");
			
			$resIblock = CIBlock::GetById($ID)->GetNext();
			$resIblockType = CIBlockType::GetById($resIblock["IBLOCK_TYPE_ID"])->GetNext();
			
			$arTypesEx = CIBlockParameters::GetIBlockTypes();
	
			$strSelect = '<select name="defa_custom_param[type]">';
			foreach ($arTypesEx as $type => $name)
				$strSelect .= "<option value=\"".$type."\" ".($type==$resIblock["IBLOCK_TYPE_ID"]?"selected=true":"").">".$name."</option>";
			
			$strSelect .= "</select>";
	
			$menu[] = array(
				"TEXT" => GetMessage("DEFATOOLS_IB_DEMO_COPYIB"),
				"TITLE" => GetMessage("DEFATOOLS_IB_DEMO_COPYIB"),
				"ACTION" => htmlspecialchars("javascript:(new BX.CDialog({
					content_url: '".$GLOBALS["APPLICATION"]->GetCurPageParam("", array("mode", "table_id", "defa_custom_action"))."',
					width: 500,
					height: 140,
					resizable: false,
					draggable: false, 
					title: '".GetMessage("DEFATOOLS_IB_DEMO_COPYIB")."',
					head: '".GetMessage("DEFATOOLS_IB_DEMO_CHOOSE_COPY_PARAM")."',
					content: '<form action=\"\" name=\"defa_custom_action_form\"><input type=\"hidden\" name=\"defa_custom_action\" value=\"ib_copy_ib\"><table><tr><td>".GetMessage("DEFATOOLS_IB_DEMO_COPYIB_TO_TYPE").": </td><td>".$strSelect."</td></tr><tr><td>".GetMessage("DEFATOOLS_IB_DEMO_COPYIB_CONTENT").": </td><td><input name=\"defa_custom_param[copy_content]\" ".(COption::GetOptionString("main", "_demo_content_copy_content", "Y")=="Y"?"checked":"")." type=\"checkbox\" value=\"Y\" /></td></tr></table></form>',
					buttons: [BX.CDialog.btnSave, BX.CDialog.btnCancel]
				})).Show()"),
				"ICON" => "btn_copy",
			);
			// /copy IB
	
			// add demo content to IB
			$demoParams = array(
				"cnt" => array(
					"TYPE" => "T",
					"NAME" => GetMessage("DEFATOOLS_IB_DEMO_ADD_ELS"), 
					"DEFAULT" => 15,
				),
				"notactive" => array(
					"TYPE" => "B", 
					"NAME" => GetMessage("DEFATOOLS_IB_DEMO_CREATE_ACT_NONACT"),
					"DEFAULT" => "Y",
				),
			);
			if ($resIblockType["SECTIONS"] == "Y") {
				$demoParams += array(
					"notactive" => array(
						"TYPE" => "B", 
						"NAME" => GetMessage("DEFATOOLS_IB_DEMO_CREATE_ACT_NONACT_SECT"),
						"DEFAULT" => "Y",
					),
					"sections_cnt" => array(
						"TYPE" => "T",
						"NAME" => GetMessage("DEFATOOLS_IB_DEMO_ADD_SECTS"), 
						"DEFAULT" => 15,
					),
					"create_sections_depth_level" => array(
						"TYPE" => "T", 
						"NAME" => GetMessage("DEFATOOLS_IB_DEMO_SECTS_MAX_DEPTH"), 
						"DEFAULT" => "2",
					),
					"elements2last_depth_level" => array(
						"TYPE" => "B", 
						"NAME" => GetMessage("DEFATOOLS_IB_DEMO_ADD_ELS_IN_LAST_SEC"),
						"DEFAULT" => "Y",
					),
				);
			}
	
			$strParams = "";
			$strParams .= "<table>";
			foreach ($demoParams as $code => $val) {
	
				$strParams .= "<tr>";
				$strParams .= "<td>".$val["NAME"].": </td>";
				$strParams .= "<td>";
				
				switch ($val["TYPE"]) {
					case "T":
					case "S":
						$strParams .= "<input name=\"defa_custom_param[".$code."]\" ".($val["TYPE"]=="T"?"size=\"4\"":"")." type=\"text\" value=\"".COption::GetOptionString("main", "_demo_content_".$code, $val["DEFAULT"])."\" />";
					break;
					case "B":
						$strParams .= "<input name=\"defa_custom_param[".$code."]\" ".(COption::GetOptionString("main", "_demo_content_".$code, $val["DEFAULT"])=="Y"?"checked":"")." type=\"checkbox\" value=\"Y\" />";
					break;
				}
	
				$strParams .= "</td></tr>";
			}
	
			$strParams .= "</table>";

			$menu[] = array( 
				"TEXT" => GetMessage("DEFATOOLS_IB_DEMO_FILL_IBLOCK"),
				"TITLE" => GetMessage("DEFATOOLS_IB_DEMO_FILL_IBLOCK"),
				"ACTION" => htmlspecialchars("javascript:(new BX.CDialog({
					content_url: '".$GLOBALS["APPLICATION"]->GetCurPageParam("", array("mode", "table_id", "defa_custom_action"))."',
					width: 500,
					height: 300,
					resizable: false,
					draggable: false, 
					title: '".GetMessage("DEFATOOLS_IB_DEMO_FILL_IBLOCK")."',
					head: '".GetMessage("DEFATOOLS_IB_DEMO_CHOOSE_FILL_PARAM")."',
					content: '<form action=\"\" name=\"defa_custom_action_form\"><input type=\"hidden\" name=\"defa_custom_action\" value=\"ib_add_demo_content\">".$strParams."</form>',
					buttons: [BX.CDialog.btnSave, BX.CDialog.btnCancel]
				})).Show()"),
				"ICON" => "btn_copy",
			);
			$menu[] = array( 
				"TEXT" => GetMessage("DEFATOOLS_IB_DEMO_DEL_DATA"),
				"TITLE" => GetMessage("DEFATOOLS_IB_DEMO_DEL_DATA"),
				"ACTION" => "javascript:if(confirm('".GetMessage("DEFATOOLS_IB_DEMO_CONFIRM_DEL_DATA")."')) window.location='".CUtil::JSEscape($GLOBALS["APPLICATION"]->GetCurPageParam("defa_custom_action=ib_delete_demo_content", array("mode", "table_id", "defa_custom_action")))."'",
				"ICON" => "btn_delete",
			);
			// /add demo content to IB
			
			/* ********************* */
	
			switch ($_REQUEST["defa_custom_action"]) {
			
				case "ib_delete_demo_content":
					$DB->StartTransaction();
	
					$rs = CIBlockElement::GetList(null, array("IBLOCK_ID" => $ID, "XML_ID" => "DEFADEMO_%"));
					
					while($res = $rs->Fetch()) {
						if(!CIBlockElement::Delete($res["ID"]))
							break;
					}
	
					$rs = CIBlockSection::GetList(null, array("IBLOCK_ID" => $ID, "XML_ID" => "DEFADEMO_%"));
					
					while($res = $rs->Fetch()) {
						if(!CIBlockSection::Delete($res["ID"]))
							break;
					}
	
					if ($ex = $GLOBALS["APPLICATION"]->GetException())
						$strError = $ex->GetString();
	
					if (!empty($strError)) {
						ShowError($strError);
						$DB->Rollback();
					}
					else {
						$DB->Commit();
						LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam("", array("defa_custom_action")));
					}
	
				break;
			
				case "ib_add_demo_content":
	
					$GLOBALS["APPLICATION"]->RestartBuffer();
	
					$DB->StartTransaction();
		
					$res = CIBlock::GetById($ID)->Fetch();
	
					$ib = new CIBlock;
					$ibc = new DefaTools_IB_DemoData($ID, COption::GetOptionString("main", "_demo_content_cnt"), COption::GetOptionString("main", "_demo_content_sections_cnt"));
					$el = new CIBlockElement;
					$se = new CIBlockSection;
	
					$sNumSuccess = $eNumSuccess = array();
					$numErrors = array();
	
	
					$depthLevel = COption::GetOptionString("main", "_demo_content_create_sections_depth_level", "2");
	
					$arrSections = array();
					
					$numSections = $ibc->sectionsCount;
					for ($i=1; $i<=$depthLevel; $i++) {
					
						if ($i>1)
							$numSections *= ceil($numSections/$i/2);
					
						for ($k=0; $k<$numSections; $k++) {
							if ($arSection = $ibc->GenerateSection($arrSections[$i-1])) {
		
								if ($sID = $se->Add($arSection)) {
									$sNumSuccess[] = $eID;
									$arrSections[$i][] = $sID;
									$ibc->__destruct();
								}
								else {
									$GLOBALS["APPLICATION"]->ThrowException( GetMessage("DEFATOOLS_IB_DEMO_SEC_CREATE_ERR").': '.$el->LAST_ERROR);
									break;
								}
							}
							
						}
				
					}
	
					$elements2last_depth_level = COption::GetOptionString("main", "elements2last_depth_level", "Y")=="Y";
	
					if ($elements2last_depth_level)
						$arrSections = array_pop($arrSections);

					if (empty($arrSections)) {
						$rs = CIBlockSection::GetList(null, array("IBLOCK_ID" => $ID));
						
						while ($res = $rs->Fetch()) {
							if ($elements2last_depth_level) {
								if ($res["RIGHT_MARGIN"]-$res["LEFT_MARGIN"] == 1)
									$arrSections[] = $res["ID"];
							}
							else {
								$arrSections[] = $res["ID"];
							}
						}
						
					}
	
	
					for ($i=0; $i<$ibc->elementsCount; $i++) {
						if ($arElement = $ibc->GenerateElement($arrSections)) {
	
							if ($eID = $el->Add($arElement)) { 
								$eNumSuccess[] = $eID;
								$ibc->__destruct();
							}
							else {
								$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_EL_CREATE_ERR").': '.$el->LAST_ERROR);
								break;
							}
						}
					} // for
					
					
					$arCatInitData = self::GetInitDataForCatalog();	
					
					if( !empty($eNumSuccess) && CModule::IncludeModule("catalog")  && CCatalog::GetByID($ibc->iblock) && is_array($arCatInitData) && !empty($arCatInitData["PRICE_TYPES"])  ) {
						
						$arVats = $arCatInitData["VATS"];
						$arCurrency = $arCatInitData["CURRENCY"];
						$arExtra = $arCatInitData["EXTRA"];
						$arPriceTypes = $arCatInitData["PRICE_TYPES"];
						$base_price_id = $arCatInitData["BASE_PRICE_ID"];
						
						$arConfirm = array("Y", "N");
						$arPriceSettings = array("plain", "extended");							
						
						foreach($eNumSuccess as $key => $PRODUCT_ID) {
									
							$arProdFields = array(
								"ID" => $PRODUCT_ID, 
								"QUANTITY" => rand(1, 100000),
								"QUANTITY_TRACE" => $arConfirm[rand(0, 1)], 
								"WEIGHT" => rand(100, 10000),
								"VAT_INCLUDED" => $arConfirm[rand(0, 1)], 
							);
							
							if(!empty($arVats)) {
								$arProdFields["VAT_ID"] = $arVats[rand(0, count($arVats) - 1)]; 
							}
							
							CCatalogProduct::Add($arProdFields);
											
							$prices_count = rand(2, 6);				
											
							$FROM = 1;
							$TO = rand(2, 10);
							$arDiapazon = array();
						
							for($i = 1; $i <= $prices_count; $i++ ) {
								$arPriceFields = Array(
									"PRODUCT_ID" => $PRODUCT_ID,
									"CATALOG_GROUP_ID" => $base_price_id,
									"PRICE" => rand(1, 99999),
									"CURRENCY" => $arCurrency[rand(0, count($arCurrency) - 1)],
									"QUANTITY_FROM" => $FROM,
									"QUANTITY_TO" => $TO
								);
								
								$arDiapazon[$i] = array( "FROM" => $FROM, "TO" => $TO);
						
								$FROM = $arPriceFields["QUANTITY_TO"] + 1;	
								$TO = rand($FROM + 1, $i * 10);
						
								CPrice::Add($arPriceFields);
							} // end for				
							
							foreach($arPriceTypes as $key => $arPriceType) {
								if($arPriceType["BASE"] == "Y")
									continue;
								
								foreach($arDiapazon as $key => $arFromTo) {	
									$arPriceFields = Array(
										"PRODUCT_ID" => $PRODUCT_ID,
										"CATALOG_GROUP_ID" => $arPriceType["ID"],
										"PRICE" => rand(1, 99999),
										"CURRENCY" => $arCurrency[rand(0, count($arCurrency) - 1)],
										"QUANTITY_FROM" => $arFromTo["FROM"],
										"QUANTITY_TO" => $arFromTo["TO"]
									);
									
									if(!empty($arExtra)) {
										$arPriceFields["EXTRA_ID"] = $arExtra[rand(0, count($arExtra) - 1)];
									}
						
									CPrice::Add($arPriceFields);
								}	
							} // foreach
						
						} // foreach // $eNumSuccess
													
					} // if
					
	
					if ($ex = $GLOBALS["APPLICATION"]->GetException())
						$strError = $ex->GetString();
	
	
					if (!empty($strError)) {
						ShowError($strError);
						$DB->Rollback();
					}
					else {
						$DB->Commit(); 
						CAdminMessage::ShowMessage(array("TYPE" => "OK", "MESSAGE" => GetMessage("DEFATOOLS_IB_DEMO_EL_CREATED_NUM").": ".count($eNumSuccess).(count($sNumSuccess)>0?"<br>".GetMessage("DEFATOOLS_IB_DEMO_SECTIONS").": ".count($sNumSuccess):"")));
						echo "<div align=\"center\"><a style=\"font-size: 20px\" href=\"javascript:window.location=window.location\">".GetMessage("DEFATOOLS_IB_DEMO_REFRESH_PAGE")."</a></div>";
					} 
	
					die();
	
				break;
			
				case "ib_copy_ib":
	
					$DB->StartTransaction();
	
					$ib = new CIBlock;
	
					$res = CIBlock::GetById($ID)->Fetch();
	
					foreach (array("CODE", "XML_ID", "EXTERNAL_ID", "NAME") as $field)
						if (!empty($res[$field]))
							$res[$field] .= "_copy";
	
					if (is_set($_REQUEST["defa_custom_param"], "type"))
						$res["IBLOCK_TYPE_ID"] = $_REQUEST["defa_custom_param"]["type"];

					if (is_set($res, "PICTURE") && intval($res["PICTURE"]) > 0)
						$res["PICTURE"] = CFile::MakeFileArray($res["PICTURE"]);
	
					$resFields = CIBlock::GetFields($ID);
					$resPerms = CIBlock::GetGroupPermissions($ID);

					if ($NEW_ID = $ib->Add($res)) {
						CIBlock::SetFields($NEW_ID, $resFields);
						CIBlock::SetPermission($NEW_ID, $resPerms);
						$arProperties = $arPropertyEnums = $arUFProperties = $arUFPropertyEnums = array();
						
						DefaTools_IB_DemoData::syncIblockCatalog($ID, $NEW_ID);												
						DefaTools_IB_DemoData::syncIblockProperties($ID, $NEW_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums);
						DefaTools_IB_DemoData::syncIblockPropertiesUserSettings($ID, $NEW_ID, $arProperties, $arUFProperties);
										
						
						if (is_set($_REQUEST["defa_custom_param"], "copy_content")) {
							COption::SetOptionString("main", "_demo_content_copy_content", $DB->ForSQL($_REQUEST["defa_custom_param"]["copy_content"]));

							if ($_REQUEST["defa_custom_param"]["copy_content"] == "Y")
								DefaTools_IB_DemoData::syncIblockContent($ID, $NEW_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums);
						}
						
					}

					if ($ex = $GLOBALS["APPLICATION"]->GetException())
						$strError = $ex->GetString();
	
					if (!empty($strError)) {
						$GLOBALS["APPLICATION"]->RestartBuffer();
						ShowError($strError);
						$DB->Rollback();
					}
					else {
						$DB->Commit();
						$GLOBALS["APPLICATION"]->RestartBuffer();
						echo "<div align=\"center\"><a style=\"font-size: 20px\" href=\"javascript:window.location='".CUtil::JSEscape($GLOBALS["APPLICATION"]->GetCurPageParam("IBLOCK_ID=".$NEW_ID."&type=".$res["IBLOCK_TYPE_ID"], array("defa_custom_action", "IBLOCK_ID", "type")))."'\">".GetMessage("DEFATOOLS_IB_DEMO_GOTO_NEW_IB")."</a></div>";
					}
	
					die();
				break;
			
			}

			$mainMenu[] = array("SEPARATOR" => "1");
			$mainMenu[] = array(
				"TEXT" => GetMessage("DEFATOOLS_IB_DEMO_IB_TOOLS"),
				"TITLE" => GetMessage("DEFATOOLS_IB_DEMO_IB_TOOLS"),
				"ICON" => "btn_new_defatools",
				"MENU" => $menu
			);

			$GLOBALS["APPLICATION"]->AddHeadString('<style> table.contextmenu #btn_new_defatools {  background-image: url("/bitrix/tools/defatools/ib_demo/images/defatools_menu_icon.gif"); }  </style>');

		}
	}


	function __construct($iblock, $elementsCount = 0, $sectionsCount = 0) {

		if (intval($elementsCount) <= 0)
			$elementsCount = 15;

//		if (intval($sectionsCount) <= 0)
//			$sectionsCount = 15;

		$this->iblock = $iblock;

		$this->elementsCount = $elementsCount;
		$this->sectionsCount = $sectionsCount;
		$this->tmpFiles = array();
		$this->text = "";
		$this->textName = "";
		$this->sourcePath = $_SERVER['DOCUMENT_ROOT']."/bitrix/tools/defatools/demo_files";
		$this->tmpPath = $this->sourcePath."/tmp";
		$this->isUTF = defined("BX_UTF") && BX_UTF == true;

		$this->GetText();

	}

	function __destruct() {
		
		foreach ($this->tmpFiles as $file) {
			if (self::IsInTmpDir($file))
				unlink($file);
		}

	}

	function IsInTmpDir($file) {
		
		if (substr($file, 0, strlen($this->tmpPath)) == $this->tmpPath)
			return true;
		
		return false;
	}

	function CheckTmpDirPath() {
		CheckDirPath($this->tmpPath."/");
	}

	function Copy($file) {

		$fileinfo = pathinfo($file);
		$newFile = $this->sourcePath."/tmp/".$fileinfo["filename"]."_".md5(uniqid(mt_rand(), true)).".".$fileinfo["extension"];

		if (copy($file, $newFile)) {
			$this->tmpFiles[] = $newFile;
			return $newFile;
		}

		return false;
	}

	function _GetDate($time = "now") {
		
		if ($time == "past")
			$year = rand(2000, date("Y")-1);
		elseif($time == "future")
			$year = rand(date("Y")+1, date("Y")+10);
		else
			$year = rand(date("Y")-10,date("Y")+10);

		return ConvertTimeStamp(
			mktime(
				0, 0, 0, 
				rand(1,28), rand(1,12), $year
			), "SHORT", SITE_ID
		);
	}
	
	function _GetMapPoint() {
		
		$precision = 1000000;

		return implode(
			",",
			array(
				rand(50*$precision,60*$precision)/$precision,
				rand(30*$precision,50*$precision)/$precision
			)
		);
	}
	
	function _GetFile($type = "", $count = 1) {  
		$arImageTypes = array("jpeg", "jpg", "png", "gif", "bmp");
		$arFileTypes = array();
		if ($type == "image")
			$type = "jpg,gif,png";
		elseif ($type == "video")
			$type = "flv";

		if (empty($type)) {
			$types = "*";
			$arFileTypes = array(  "doc", "xls", "pdf", "jpg","png", "mp3", "flv");
		}
		elseif (strpos($type, ",") > 0) {
			$types = explode(",", $type);
			$arFileTypes =  $types;
			TrimArr($types, true);
			$types = "[".implode("|",$types)."]+";
		}
		else {
			$types = $type;
		}
	
		$files = array();

		self::CheckTmpDirPath();
	
		foreach (glob($this->sourcePath."/*") as $file) {
			if (preg_match("/.*\.".$types."$/", $file)) {
				if ($_tmpFile = self::Copy($file)) {
					$files[] = CFile::MakeFileArray($_tmpFile);
				}
			}
		}
		
		if (empty($files)) {
			foreach ($arFileTypes as $file_type) {
				$demo_file_name = "demo_file.".trim($file_type);
				$abs_demo_file_name = $this->tmpPath."/".$demo_file_name;
				
				if(in_array(trim($file_type), $arImageTypes)) {
					
					$arImgSize = array(
						"w" => mt_rand(150, 600),
						"h" => mt_rand(150, 600)
					);
					
					$image_handle = @imagecreate($arImgSize["w"], $arImgSize["h"]);
					if($image_handle) {
						imagecolorallocate($image_handle, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
						imagestring($image_handle, 5, 50, 50,  $arImgSize["w"].'x'.$arImgSize["h"], imagecolorallocate($image_handle, 0, 0, 0));
						if($file_type == "jpeg" || $file_type == "jpg") {
							imagejpeg($image_handle, $abs_demo_file_name);
						} elseif($file_type == "png") {
							imagepng($image_handle, $abs_demo_file_name);
						} elseif($file_type == "gif") {
							imagegif($image_handle, $abs_demo_file_name);
						} elseif($file_type == "bmp") {
							imagewbmp($image_handle, $abs_demo_file_name);
						}
						imagedestroy($image_handle);
					}	
				} elseif(!file_exists($abs_demo_file_name)) {
					mt_srand((double)microtime()*1000000);
					$put_content = str_pad(" ", mt_rand(10000, 50000) );
					fputs(fopen($abs_demo_file_name, "w"), $put_content);
				}
				
				$files[] = CFile::MakeFileArray($abs_demo_file_name);
			}
		}
		
		shuffle($files);
		$files = array_slice($files, 0, $count);
		
		if (empty($files)) { 
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_FILES_NOT_FOUND", Array ("#TYPES#" => $types)));
			return false; 
		}
	
		return count($files)==1?$files[0]:$files;
	}

	function _GetTextPart($length = 0, $renew = false, $type = "text") {

		$this->GetText($renew);

		$text = $this->text;
		
		if ($type == "text" || $length > 0)
			$text = strip_tags($text);

		if ($length > 0) {
			
			if ($length >= strlen($text))
				return $text;

			$text = $this->ucfirst(
									trim(
										substr(
											$text, 
											strpos($text, " ", rand(0, strlen($text)-$length)), 
											$length*2
										)
					)
			);

		}
		else
			$text = $text;

		return $text;

	}

	function ucfirst($str) {
		
		if ($this->isUTF) {
			$str = mb_ereg_replace('^[\ ]+', '', $str);
			$str = mb_strtoupper(mb_substr($str, 0, 1, "UTF-8"), "UTF-8").mb_substr($str, 1, mb_strlen($str), "UTF-8");
		}
		else {
			$str[0] = ToUpper($str[0]);
		}
		
		return $str;
	}
	
	function GetText($renew = false) {

		if (empty($this->text) || empty($this->textName) || $renew) {
			$errno = 0;
			$errstr = "";
	
			$res = str_replace("\n", "", QueryGetData("vesna.yandex.ru", 80, "/all.xml", "mix=".urlencode("astronomy,geology,gyroscope,literature,marketing,mathematics,music,polit,agrobiologia,law,psychology,geography,physics,philosophy,chemistry,estetica"), &$errno, &$errstr, "GET"));
	
			if (!$this->isUTF)
				$res = utf8win1251($res);

			preg_match("/\<h1 [^\>]+\>([^\<]+)\<\/h1\>/", $res, $name);
			$this->textName = substr($name[1], 7, -1);

			preg_match("/<\/h1\>(.*)/is", $res, $text);
			$text = substr($text[1], 0, strpos($text[1], "</div>"));

			$this->text = $text;
		}

		return array($this->textName, $this->text);
	}

	function GenerateSection($parentsArray = array()) {

		if (!is_array($parentsArray))
			$parentsArray = array();

		$this->GetText(true);

		$active = "Y";

		if (COption::GetOptionString("main", "_demo_content_notactive", "Y") == "Y")
			$active = (rand(0,100)>20)?"Y":"N";

		$section = array(
			"IBLOCK_SECTION_ID" => $parentsArray[array_rand($parentsArray)],
			"SORT" => rand(1,1000)*10,
			"IBLOCK_ID" => $this->iblock,
			"XML_ID" => "DEFADEMO_".RandString(5),
//			"CODE" => substr(CUtil::translit($this->textName, "ru"), 0, 50),
			"CODE" => CUtil::translit($this->textName, "ru"),
			"ACTIVE" => $active,
			"NAME" => $this->textName,
			"DESCRIPTION" => $this->_GetTextPart(0, false, "html"),
			"DESCRIPTION_TYPE" => "html",
			"PICTURE" => $this->_GetFile("image"),
		);

		return $section;
	}

	function GenerateElement($parentsArray = array()) {

		if (!is_array($parentsArray))
			$parentsArray = array();

		$this->GetText(true);

		$active = "Y";

		if (COption::GetOptionString("main", "_demo_content_notactive", "Y") == "Y")
			$active = (rand(0,10)>2)?"Y":"N";

		$element = array(
			"IBLOCK_SECTION_ID" => $parentsArray[array_rand($parentsArray)],
			"SORT" => rand(1,1000)*10,
			"IBLOCK_ID" => $this->iblock,
			"XML_ID" => "DEFADEMO_".RandString(5),
//			"CODE" => substr(CUtil::translit($this->textName, "ru"), 0, 50),
			"CODE" => CUtil::translit($this->textName, "ru"),
			"TAGS" => ToLower(str_replace(array(" ", ".", ":", ";", "?", "!", ",,"), ",", $this->textName)),
			"ACTIVE" => $active,
			"ACTIVE_FROM" => $this->_GetDate("past"),
			"ACTIVE_TO" => $this->_GetDate("future"),
			"NAME" => $this->textName,
			"PREVIEW_TEXT_TYPE" => "text",
			"PREVIEW_TEXT" => $this->_GetTextPart(200, false),
			"DETAIL_TEXT_TYPE" => "html",
			"DETAIL_TEXT" => $this->_GetTextPart(0, false, "html"),
			"DETAIL_PICTURE" => $this->_GetFile("image"),
			"PREVIEW_PICTURE" => $this->_GetFile("image"),
		);
		


		$rsProps = CIBlock::GetProperties($this->iblock);
		
		while ($resProp = $rsProps->Fetch()) {
			$rsTopicID = $rsElementXmlID = $rsUser = $val = false;

			$count = 1;
			if ($resProp["MULTIPLE"] == "Y")
				$count = rand(0,5);

			switch ($resProp["PROPERTY_TYPE"]) {

				case "S":
					for ($i=0; $i<$count; $i++) {

						switch ($resProp["USER_TYPE"]) {
							
							case "":
								$val[] = $this->_GetTextPart(30);
							break;

							case "DateTime":
								$val[] = $this->_GetDate();
								$val = array_unique($val);
							break;
						
							case "ElementXmlID":
								if (!$rsElementXmlID)
									$rsElementXmlID = CIBlockElement::GetList(array("rand" => ""), array(), false, false, array("XML_ID"));
							
								if ($_val = $rsElementXmlID->Fetch())
									$val[] = $_val["XML_ID"];

								$val = array_unique($val);
							break;

							case "TopicID":
								if (CModule::IncludeModule("forum")) {
	
									if (!$rsTopicID)
										$rsTopicID = CForumTopic::GetList(array("rand" => ""), array());
	
									if ($_val = $rsTopicID->Fetch())
										$val[] = $_val["ID"];
									
									shuffle($val);
									$val = array_unique($val);
								}
							break;

							case "UserID":
								if (!$rsUser)
									$rsUser = CUser::GetList(($by="rand"), ($order="asc"), array("!ID" => "1"));

									if ($res = $rsUser->GetNext())
									$val[] = $res["ID"];

								$val = array_unique($val);
							break;

							case "map_yandex":
							case "map_google":
								$val[] = $this->_GetMapPoint();
							break;

							case "HTML":
								$val[] = array("VALUE" => array("TEXT" => "<strong>".$this->_GetTextPart(100)."</strong>", "TYPE" => "html"));
							break;

							case "FileMan":
								if (CModule::IncludeModule("fileman")) {
	
									$arFilter["MIN_PERMISSION"] = "R";
									$arPathes = CFileMan::GetDirList(Array($site_id, $path), $arDirs, $arFiles, $arFilter, Array($by=>$order), "DF");
		
									$arAll = array_merge($arDirs, $arFiles);
									$values = array();
									foreach ($arAll as $k => $v) {
										if (
											($v["TYPE"] == "D" && in_array($v["ABS_PATH"], array("/bitrix", "/upload")))
											||
											($v["TYPE"] == "F" && substr($v["ABS_PATH"], 0, 2) == "/.")
											||
											($v["TYPE"] == "F" && in_array($v["NAME"], array("404.php", "urlrewrite.php")))
											||
											($v["TYPE"] == "F" && substr($v["NAME"], -4) != ".php")
											||
											($v["TYPE"] == "F" && substr($v["NAME"], -8) == "_inc.php")
											||
											($v["TYPE"] == "D" && !file_exists($v["PATH"]."/index.php"))
										)
											continue;
		
										$values[] = $v["TYPE"]=="D"?$v["ABS_PATH"]."/index.php":$v["ABS_PATH"];
									}
		
									$val[] = $values[rand(0, count($values)-1)];
									
									$val = array_unique($val);
								}
							break;

							case "video":
								// не работает в Битриксе
							break;

							default: 
								$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_PROP_TYPE")." &laquo;".$resProp["PROPERTY_TYPE"].":".$resProp["USER_TYPE"]."&raquo; ".GetMessage("DEFATOOLS_IB_DEMO_DONT_SUPPORT"));
//								return false;
							break;

						}
					}				
				break;

				case "N":
					for ($i=0; $i<$count; $i++)
						$val[] = rand(0,10000);
				break;

				case "L":
					$values = array();

					$enums = CIBlockPropertyEnum::GetList(array(), Array("IBLOCK_ID" => $resProp["IBLOCK_ID"], "PROPERTY_ID" => $resProp["ID"]));
					while ($e = $enums->Fetch()) {
						$values[] = $e["ID"];
					}
					
					for ($i=0; $i<$count; $i++) {
						$val[] = $values[rand(0, count($values)-1)];
					}				
				break;

				case "E":
					$linkRs = CIBlockElement::GetList(array("rand" => ""), array("IBLOCK_ID" => $resProp["LINK_IBLOCK_ID"]), false, false, array("ID"));
				
					for ($i=0; $i<$count; $i++) {
						if ($_val = $linkRs->Fetch())
							$val[] = $_val["ID"];
					}
				break;

				case "G":
					$linkRs = $GLOBALS["DB"]->Query("SELECT * FROM b_iblock_section WHERE IBLOCK_ID = '".$resProp["LINK_IBLOCK_ID"]."' ORDER BY RAND()");
				
					for ($i=0; $i<$count; $i++) {
						if ($_val = $linkRs->Fetch())
							$val[] = $_val["ID"];
					}
				break;

				case "F":
					for ($i=0; $i<$count; $i++) {
						$val = $this->_GetFile($resProp["FILE_TYPE"], $count);
					}
				break;

				default: 
					$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_PROP_TYPE")." &laquo;".$resProp["PROPERTY_TYPE"]."&raquo; ".GetMessage("DEFATOOLS_IB_DEMO_DONT_SUPPORT"));
					return false;
				break;
			}

			$element["PROPERTY_VALUES"][$resProp["ID"]] = (count($val)==1)?$val[0]:$val;

		}

		return $element;
		
	}

	function syncIblockContent($FROM_IBLOCK_ID, $TO_IBLOCK_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums)
	{

		global $DB, $USER_FIELD_MANAGER;

		if(!CModule::IncludeModule("iblock"))
		{
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
			return false;
		}

		$bs = new CIBlockSection;
		$be = new CIBlockElement;

		$arSections = $arElements = $arElementProps = array();

		$rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $FROM_IBLOCK_ID), false, array("SELECT" => "UF_*"));
		while ($resSection = $rsSection->Fetch()) {

			$ufProps = $USER_FIELD_MANAGER->GetUserFields("IBLOCK_".$FROM_IBLOCK_ID."_SECTION", $resSection["ID"]);

			foreach (array("PICTURE", "DETAIL_PICTURE") as $code) {
				if (is_set($resSection, $code) && intval($resSection[$code]) > 0)
					$resSection[$code] = CFile::MakeFileArray($resSection[$code]);
			}

			$resSection["IBLOCK_ID"] = $TO_IBLOCK_ID;

			foreach (array("GLOBAL_ACTIVE", "LEFT_MARGIN", "RIGHT_MARGIN", "DEPTH_LEVEL", "IBLOCK_TYPE_ID", "IBLOCK_CODE", "IBLOCK_EXTERNAL_ID", "LIST_PAGE_URL", "SECTION_PAGE_URL", "SEARCHABLE_CONTENT") as $code)
				unset($resSection[$code]);

			if (intval($resSection["IBLOCK_SECTION_ID"]) > 0) {
				$resSection["IBLOCK_SECTION_ID"] = $arSections[$resSection["IBLOCK_SECTION_ID"]]["NEW_ID"];
			}

			$resSection["TIMESTAMP_X"] = $DB->CurrentTimeFunction();

			foreach ($ufProps as $k => $v) {
				if (!empty($v["VALUE"]) && is_set($resSection, $k) && !empty($resSection[$k])) {

					switch($v["USER_TYPE_ID"]) {
						case "enumeration":

							if (is_array($resSection[$k])) {
								foreach ($resSection[$k] as $kk => $vv)
									$resSection[$k][$kk] = $arUFPropertyEnums[$vv];
							}
							else
								$resSection[$k] = $arUFPropertyEnums[$resSection[$k]];

						break;
						case "file":

							if (is_array($resSection[$k])) {
								foreach ($resSection[$k] as $kk => $vv)
									$resSection[$k][$kk] = CFile::MakeFileArray($vv);
							}
							else
								$resSection[$k] = CFile::MakeFileArray($resSection[$k]);

						break;
					}
				}
			}

			if ($NEW_SECTION_ID = $bs->Add($resSection, true, true, true)) {
				$resSection["NEW_ID"] = $NEW_SECTION_ID;
			}
			else {
				// error
			}

			$arSections[$resSection["ID"]] = $resSection;
		}

		$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $FROM_IBLOCK_ID));
		while ($obElement = $rsElement->GetNextElement()) {
			$resElement = $obElement->GetFields();

			$OLD_ID = $resElement["ID"];
			$resElementProps = $obElement->GetProperties(array(), array("ACTIVE" => ""));

			foreach ($resElement as $k => $v)
				if (substr($v, 0, 1) == "~" || empty($v))
					unset($resElement[$k]);

			foreach (array("PREVIEW_PICTURE", "DETAIL_PICTURE") as $code) {
				if (is_set($resElement, $code) && intval($resElement[$code]) > 0)
					$resElement[$code] = CFile::MakeFileArray($resElement[$code]);
				else {
					unset($resElement[$code]);
					unset($resElement["~".$code]);
				}
			}

			foreach (array("ID", "LOCK_STATUS", "WF_DATE_LOCK", "WF_LAST_HISTORY_ID", "WF_LOCKED_BY", "WF_NEW", "WF_PARENT_ELEMENT_ID") as $code) {
				unset($resElement[$code]);
				unset($resElement["~".$code]);
			}

			if ($resElement["IBLOCK_SECTION_ID"] > 0)
				$resElement["IBLOCK_SECTION_ID"] = $arSections[$resElement["IBLOCK_SECTION_ID"]]["NEW_ID"];

			$resElement["IBLOCK_ID"] = $TO_IBLOCK_ID;

			foreach($resElementProps as $fields) {

				foreach (array("VALUE", "DESCRIPTION", "VALUE_XML_ID", "VALUE_ENUM_ID") as $code) {
					if (!is_array($fields[$code]))
						$fields[$code] = array($fields[$code]);
				}

				foreach ($fields["VALUE"] as $propKey => $propValue) {
					switch ($fields["PROPERTY_TYPE"]) {
						case "F":
							$arElementProps[$arProperties[$fields["ID"]]][] = array("VALUE" => CFile::MakeFileArray($fields["VALUE"][$propKey]), "DESCRIPTION" => $fields["DESCRIPTION"][$propKey]);
						break;
						case "L":
							$arElementProps[$arProperties[$fields["ID"]]][] = array("VALUE" => $arPropertyEnums[$fields["VALUE_ENUM_ID"][$propKey]], "DESCRIPTION" => $arPropertyEnums[$fields["DESCRIPTION"][$propKey]]);
						break;
						default:
							$arElementProps[$arProperties[$fields["ID"]]][] = array("VALUE" => $fields["VALUE"][$propKey], "DESCRIPTION" => $fields["DESCRIPTION"][$propKey]);
						break;
					}
				}
			}
			$resElement["PROPERTY_VALUES"] = $arElementProps;

			if ($NEW_ELEMENT_ID = $be->Add($resElement)) {

				$rsElementSections = CIBlockElement::GetElementGroups($OLD_ID, true);
				$resElementSections = array();
	
				while($section = $rsElementSections->Fetch())
					$resElementSections[] = $arSections[$section["ID"]]["NEW_ID"];

				CIBlockElement::SetElementSection($NEW_ELEMENT_ID, $resElementSections);

			}
			else {

				$GLOBALS["APPLICATION"]->ThrowException($be->LAST_ERROR);
				return false;
				// error
			}
		}
	}

	function syncIblockPropertiesUserSettings($FROM_IBLOCK_ID, $TO_IBLOCK_ID, $arProperties=array(), $arUFProperties)
	{

		CModule::IncludeModule("iblock");

		foreach ($arProperties as $k => $v) {
			$arPropertiesNew["--PROPERTY_".$k."--"] = "--PROPERTY_".$v."--";
			$arPropertiesNewClear["PROPERTY_".$k] = "PROPERTY_".$v;
		}

		$iblockHashes = array();
		foreach (array($FROM_IBLOCK_ID, $TO_IBLOCK_ID) as $iblock) {
			$rs = CIBlock::GetById($iblock);
			$res = $rs->Fetch();
			$iblockHashes[$res["ID"]] = "tbl_iblock_list_".md5($res["IBLOCK_TYPE_ID"].".".$res["ID"]);
		}

		foreach (array($USER->GetID, false) as $user) {

			foreach (array("section", "element") as $type) {
				// form
				$res = CUserOptions::GetOption("form", "form_".$type."_".$FROM_IBLOCK_ID, false, $user);
				
				if (!empty($res["tabs"])) {
					$res["tabs"] = str_replace(array_keys($arPropertiesNew), array_values($arPropertiesNew), $res["tabs"]);
					CUserOptions::SetOption("form", "form_".$type."_".$TO_IBLOCK_ID, $res, ($user===false?"Y":"N"), $user);
				}
				// /form
			}
			
			// list
			$res = CUserOptions::GetOption("list", $iblockHashes[$FROM_IBLOCK_ID], false, $user);
			
			if ($res["columns"]) {
				$res["columns"] = explode(",", $res["columns"]);
				foreach ($res["columns"] as $k => $v) {
					if (isset($arPropertiesNewClear[$v]))
						$res["columns"][$k] = $arPropertiesNewClear[$v];
				}
				$res["columns"] = implode(",", $res["columns"]);
			}
			
			if (isset($res["by"]) && isset($arPropertiesNewClear[$res["by"]]))
				$res["by"] = $arPropertiesNewClear[$res["by"]];
			
			CUserOptions::SetOption("list", $iblockHashes[$TO_IBLOCK_ID], $res, ($user===false?"Y":"N"), $user);
			// /list


		}

	}

	function syncIblockProperties($FROM_IBLOCK_ID, $TO_IBLOCK_ID, &$arProperties=array(), &$arPropertyEnums=array(), &$arUFProperties=array(), &$arUFPropertyEnums=array())
	{
	
			if(!CModule::IncludeModule("iblock"))
			{
				$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
				return false;
			}

			if (!is_array($arUFPropertyEnums))
				$arUFPropertyEnums = array();


			$obUserField = new CUserTypeEntity;
			$obEnum = new CUserFieldEnum;
			
			$arFilter = array(
				"ENTITY_ID" => "IBLOCK_".$FROM_IBLOCK_ID."_SECTION",
			);
			$rsData = CUserTypeEntity::GetList(array(), $arFilter);
			while ($resData = $rsData->Fetch()) {

				$UF_ID = $resData["ID"];
				$resData = CUserTypeEntity::GetByID($UF_ID);

				if ($resData["USER_TYPE_ID"] == "enumeration") {
					$rsEnum = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $resData["ID"]));
					$enum = 0;
					while ($resEnum = $rsEnum->Fetch()) {
						$resData["ENUM"]["n".$enum++] = $resEnum;
					}
				}

				$resData["ENTITY_ID"] = "IBLOCK_".$TO_IBLOCK_ID."_SECTION";

				$rsDataExists = CUserTypeEntity::GetList(
					array(),
					array(
						"ENTITY_ID" => $resData["ENTITY_ID"],
						"FIELD_NAME" => $resData["FIELD_NAME"],
					)
				);

				if ($resDataExists = $rsDataExists->Fetch()) {

					$NEW_ID = $resDataExists["ID"];
					unset($resData["ID"]);
	
					$obUserField->Update($NEW_ID, $resData);
					if (!empty($resData["ENUM"])) {
						
						foreach($resData["ENUM"] as $k => $v) {
							unset($resData["ENUM"][$k]["USER_FIELD_ID"]);
							unset($resData["ENUM"][$k]["ID"]);
						}

						$obEnum->SetEnumValues($NEW_ID, $resData["ENUM"]);
					}
				}
				else {
					unset($resData["ID"]);
					$NEW_ID = $obUserField->Add($resData);
					
					if (!empty($resData["ENUM"])) {
						foreach($resData["ENUM"] as $k => $v) {
							unset($resData["ENUM"][$k]["USER_FIELD_ID"]);
							unset($resData["ENUM"][$k]["ID"]);
						}
					
						$obEnum->SetEnumValues($NEW_ID, $resData["ENUM"]);
					}
				}				

				$resEnumsOld = $resEnumsNew = array();
				$rsEnums = $obEnum->GetList(array(), array("USER_FIELD_ID" => $UF_ID));
				while ($resEnums = $rsEnums->Fetch()) {
					$resEnumsOld[] = $resEnums["ID"];
				}

				$rsEnums = $obEnum->GetList(array(), array("USER_FIELD_ID" => $NEW_ID));
				while ($resEnums = $rsEnums->Fetch()) {
					$resEnumsNew[] = $resEnums["ID"];
				}
				
				if (count($resEnumsOld) == count($resEnumsOld) && !empty($resEnumsOld)) {
					$arUFPropertyEnums += array_combine($resEnumsOld, $resEnumsNew);
				}

				$arUFProperties[$UF_ID] = $NEW_ID;
				
			}

			$arUpdateProperties = array();
			$rsProperty = CIBlockProperty::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $FROM_IBLOCK_ID));
			while($arProperty = $rsProperty->Fetch())
			{
				$arUpdateProperties[] = $arProperty;
			}

			foreach($arUpdateProperties as $arProperty)
			{
			
					$arProperty["IBLOCK_ID"] = $TO_IBLOCK_ID;
					$arProperty["XML_ID"] = "PROP_".$arProperty["ID"];
	
					$ibp = new CIBlockProperty;
					$rs = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arProperty["IBLOCK_ID"], "XML_ID" => $arProperty["XML_ID"]));
					if($ar = $rs->Fetch())
					{
							$ID = $ar["ID"];
							$ibp->Update($ar["ID"], $arProperty);
							$arProperties[$arProperty["ID"]] = $ar["ID"];
					}
					else
					{
							if( !($ID = $ibp->Add($arProperty)) )
							{
								$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_PROP_CREATE_ERR").': '.$ibp->LAST_ERROR);
							}
							$arProperties[$arProperty["ID"]] = $ID;
					}

					$arUpdateEnums = array();
					if($arProperty["PROPERTY_TYPE"] == 'L')
					{
						if ($ID) {
							$rs = CIBlockPropertyEnum::GetList(array("SORT"=>"ASC"), array("PROPERTY_ID" => $arProperty["ID"]));
							while($ar = $rs->Fetch())
							{

									$ar["IBLOCK_ID"] = $arProperty["IBLOCK_ID"];
									$ar["PROPERTY_ID"] = $ID;
//									$ar["XML_ID"] = "ENUM_".$ar["ID"];
									$arUpdateEnums[] = $ar;
							}
						}
					}

					foreach($arUpdateEnums as $arEnum)
					{
							$ibpenum = new CIBlockPropertyEnum;

							$rs = CIBlockPropertyEnum::GetList(array(), array("PROPERTY_ID" => $ID, "XML_ID" => $arEnum["XML_ID"]));
							if($ar = $rs->Fetch())
							{
									$arPropertyEnums[$arEnum["ID"]] = $ar["ID"];
									unset($arEnum["ID"]);
									$ibpenum->Update($ar["ID"], $arEnum);
							}
							else
							{
									$arEnumID = $arEnum["ID"];
									unset($arEnum["ID"]);
									if( !($ENUM_ID = $ibpenum->Add($arEnum)) )
									{
											$GLOBALS["APPLICATION"]->ThrowException(GetMessage("DEFATOOLS_IB_DEMO_PROP_ADD_LTYPE_ERR"));
									}
									$arPropertyEnums[$arEnumID] = $ENUM_ID;
							}
					}
			}

	}
	
	
	function syncIblockCatalog($FROM_IBLOCK_ID, $TO_IBLOCK_ID) {
		
		if(!CModule::IncludeModule("iblock")) {
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CAT_ERROR_IBLOCK_NOT_INSTALLED"));
			return false;
		}
		
		if (CModule::IncludeModule("catalog") && CCatalog::GetByID($FROM_IBLOCK_ID)) {
			CCatalog::Add(
				array(
					"IBLOCK_ID" => $TO_IBLOCK_ID,
	   				"YANDEX_EXPORT" => "N",
	   				"SUBSCRIPTION" => "N",
				)
			);   
		}
	}
		
	function GetInitDataForCatalog() {
		if(!CModule::IncludeModule("catalog"))
			return;

		$arVats = array();
		$dbResVat = CCatalogVat::GetList(
			array(),
			array("ACTIVE" => "Y") 
		);
		while($arVat = $dbResVat->Fetch()) {
			$arVats[] = $arVat["ID"];	
		} 
		
		$arCurrency = array();
		$dbCurrency = CCurrency::GetList(($b="sort"), ($order="asc"), LANGUAGE_ID);
		while($arCurr = $dbCurrency->Fetch()) {
		   $arCurrency[] = $arCurr["CURRENCY"];
		}
		
		$arExtra = array();
		$dbExtra = CExtra::GetList(($b="sort"), ($order="asc") );
		while($arOneExtra = $dbExtra->Fetch()) {
		   $arExtra[] = $arOneExtra["ID"];
		}
		
		$arPriceTypes = $arPriceTypesID = array();
		$dbPriceType = CCatalogGroup::GetList( array("SORT" => "ASC") );
		while($arPriceType = $dbPriceType->Fetch()) { 
			$arPriceTypes[$arPriceType["ID"]] = $arPriceType;
   
			if($arPriceType["BASE"] == "Y") {
				$base_price_id = $arPriceType["ID"];
			}
		}
		
		return array(
			"VATS" => $arVats,
			"CURRENCY" => $arCurrency,
			"EXTRA" => $arExtra,
			"PRICE_TYPES" => $arPriceTypes,
			"BASE_PRICE_ID" => $base_price_id		
		);	
	}
}

?>