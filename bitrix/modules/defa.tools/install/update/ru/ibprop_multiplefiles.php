<?
$MESS['DEFATOOLS_PROP_NAME'] = "DefaTools Множественная загрузка файлов";
$MESS['DEFATOOLS_USER_TYPE_SETTINGS_TITLE'] = "Выбор типа плагина";
$MESS['DEFATOOLS_PLUGIN_TYPE'] = "Тип плагина";
$MESS['DEFATOOLS_BEFORE_ELEM_SAVE'] = "Множественная загрузка файлов доступна только в режиме <b>редактирования</b> элемента";
$MESS['DEFATOOLS_UPLOAD_FILES'] = 'Загрузить файлы';
$MESS['DEFATOOLS_CLEAR_QUEUE'] = 'Очистить очередь загрузки';
$MESS['DEFATOOLS_ELEMENT_EDIT_PROP_DESC'] = "Описание значения свойства";
$MESS['DEFATOOLS_ELEMENT_EDIT_PROP_DESC_1'] = "Описание:";
?>