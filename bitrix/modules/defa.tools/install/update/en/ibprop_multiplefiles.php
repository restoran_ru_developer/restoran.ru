<?
$MESS['DEFATOOLS_PROP_NAME'] = "DefaTools Multiple files upload";
$MESS['DEFATOOLS_USER_TYPE_SETTINGS_TITLE'] = "Plugin type selection";
$MESS['DEFATOOLS_PLUGIN_TYPE'] = "Plugin type";
$MESS['DEFATOOLS_BEFORE_ELEM_SAVE'] = "Multiple files upload is enabled only in <b>edit mode</b> of the element";
$MESS['DEFATOOLS_UPLOAD_FILES'] = 'Upload files';
$MESS['DEFATOOLS_CLEAR_QUEUE'] = 'Clear queue';
$MESS['DEFATOOLS_ELEMENT_EDIT_PROP_DESC'] = "Property value description";
$MESS['DEFATOOLS_ELEMENT_EDIT_PROP_DESC_1'] = "Description";
?>