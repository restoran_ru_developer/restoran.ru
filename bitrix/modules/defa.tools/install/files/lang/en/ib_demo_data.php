<?
$MESS["DEFATOOLS_IB_DEMO_COPYIB"] = "Copy information block";
$MESS["DEFATOOLS_IB_DEMO_CHOOSE_COPY_PARAM"] = "Copying information block parameters";
$MESS["DEFATOOLS_IB_DEMO_ADD_ELS"] = "Add elements";  
$MESS["DEFATOOLS_IB_DEMO_CREATE_ACT_NONACT"] = "Create active and <br>inactive elements";
$MESS['DEFATOOLS_IB_DEMO_CREATE_ACT_NONACT_SECT'] = "Create active and <br>inactive elements/sections";
$MESS["DEFATOOLS_IB_DEMO_ADD_SECTS"] = "Add sections";
$MESS["DEFATOOLS_IB_DEMO_SECTS_MAX_DEPTH"] = "Maximum sections depth level";
$MESS["DEFATOOLS_IB_DEMO_ADD_ELS_IN_LAST_SEC"] = "Add elements only in <br>end subsections";
$MESS["DEFATOOLS_IB_DEMO_FILL_IBLOCK"] = "Fill information block with demo data";
$MESS["DEFATOOLS_IB_DEMO_CHOOSE_FILL_PARAM"] = "Filling information block parameters";
$MESS["DEFATOOLS_IB_DEMO_DEL_DATA"] = "Delete demo data";
$MESS["DEFATOOLS_IB_DEMO_CONFIRM_DEL_DATA"] = "Are you sure you want to permanently delete all the demo data from the information block?";
$MESS["DEFATOOLS_IB_DEMO_SEC_CREATE_ERR"] = "Failed to create section";		
$MESS["DEFATOOLS_IB_DEMO_EL_CREATE_ERR"] = "Failed to create element"; 
$MESS["DEFATOOLS_IB_DEMO_EL_CREATED_NUM"] = "Successfully added <br>elements";
$MESS["DEFATOOLS_IB_DEMO_REFRESH_PAGE"] = "Refresh the page"; 
$MESS["DEFATOOLS_IB_DEMO_FILES_NOT_FOUND"] = "Failed to find files of that type #TYPES# ";  
$MESS["DEFATOOLS_IB_DEMO_PROP_TYPE"] = "Property type" ;
$MESS["DEFATOOLS_IB_DEMO_DONT_SUPPORT"] = "is not supported";
$MESS["DEFATOOLS_IB_DEMO_PROP_CREATE_ERR"] = "Failed to create property";
$MESS["DEFATOOLS_IB_DEMO_PROP_ADD_LTYPE_ERR"] = "Error adding list type property value";
$MESS["DEFATOOLS_IB_DEMO_SECTIONS"] = "sections";
 
?>