<?
$MESS["DEFATOOLS_IB_DEMO_COPYIB"] = "Копировать инфоблок";
$MESS["DEFATOOLS_IB_DEMO_CHOOSE_COPY_PARAM"] = "Выбор параметров копирования инфоблока";
$MESS["DEFATOOLS_IB_DEMO_ADD_ELS"] = "Добавить элементы";  
$MESS["DEFATOOLS_IB_DEMO_CREATE_ACT_NONACT"] = "Создавать активные и <br>неактивные элементы";
$MESS['DEFATOOLS_IB_DEMO_CREATE_ACT_NONACT_SECT'] = "Создавать активные и <br>неактивные элементы/разделы";
$MESS["DEFATOOLS_IB_DEMO_ADD_SECTS"] = "Добавить разделы";
$MESS["DEFATOOLS_IB_DEMO_SECTS_MAX_DEPTH"] = "Максимальный уровень <br>вложенности подразделов";
$MESS["DEFATOOLS_IB_DEMO_ADD_ELS_IN_LAST_SEC"] = "Добавлять элементы только в <br>конечные подразделы";
$MESS["DEFATOOLS_IB_DEMO_FILL_IBLOCK"] = "Наполнить инфоблок демо данными";
$MESS["DEFATOOLS_IB_DEMO_CHOOSE_FILL_PARAM"] = "Выбор параметров наполнения инфоблока";
$MESS["DEFATOOLS_IB_DEMO_DEL_DATA"] = "Удалить демо данные";
$MESS["DEFATOOLS_IB_DEMO_CONFIRM_DEL_DATA"] = "Вы уверены что хотите БЕЗВОЗВРАТНО удалить все демо данные из этого инфоблока?";
$MESS["DEFATOOLS_IB_DEMO_SEC_CREATE_ERR"] = "Ошибка создания раздела";		
$MESS["DEFATOOLS_IB_DEMO_EL_CREATE_ERR"] = "Ошибка создания элемента"; 
$MESS["DEFATOOLS_IB_DEMO_EL_CREATED_NUM"] = "Успешно добавлено <br>элементов";
$MESS["DEFATOOLS_IB_DEMO_REFRESH_PAGE"] = "Обновить страницу"; 
$MESS["DEFATOOLS_IB_DEMO_FILES_NOT_FOUND"] = "Файлов с расширением #TYPES# не найдено";  
$MESS["DEFATOOLS_IB_DEMO_PROP_TYPE"] = "Тип свойства";
$MESS["DEFATOOLS_IB_DEMO_DONT_SUPPORT"] = "не поддерживается";
$MESS["DEFATOOLS_IB_DEMO_PROP_CREATE_ERR"] = "Ошибка создания свойства";
$MESS["DEFATOOLS_IB_DEMO_PROP_ADD_LTYPE_ERR"] = "Ошибка добавления значения свойства типа список свойства";
$MESS["DEFATOOLS_IB_DEMO_SECTIONS"] = "разделов";
 
?>