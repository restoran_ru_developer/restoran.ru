<?
IncludeModuleLangFile(__FILE__);

global $MESS, $DOCUMENT_ROOT;

CModule::AddAutoloadClasses(
    'defa.tools',
    array(
    
        'DefaTools_IBProp_MultipleFiles' => 'classes/general/ibprop_multiplefiles.php',
        'DefaTools_IBProp_FileManEx' => 'classes/general/ibprop_filemanex.php',
        'DefaTools_UserType_Auth' => 'classes/general/usertype_auth.php',
        'DefaTools_IBProp_ElemListDescr' => 'classes/general/ibprop_elemlistdescr.php',
        'DefaTools_IBProp_OptionsGrid' => 'classes/general/ibprop_optionsgrid.php',
        'DefaTools_IBProp_ElemCompleter' => 'classes/general/ibprop_elemcompleter.php',
        'DefaTools_Typograf' => 'classes/general/typograf.php',
        'DefaTools_IB_DemoData' => 'classes/general/ib_demo_data.php'
        
   )
);
?>