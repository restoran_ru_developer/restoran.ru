<?
IncludeModuleLangFile(__FILE__);

global $MESS, $DOCUMENT_ROOT;

require_once('classes/general/defa/defa.inc.php');

CModule::AddAutoloadClasses(
    'defa.tools',
    array(
        'DefaTools_IB_Demo' => 'classes/general/ib_demo.php',
        'DefaTools_IB_DemoData' => 'classes/general/ib_demo_data.php',
        'DefaTools_IB_Copy' => 'classes/general/ib_copy.php',
        'DefaTools_IBProp_MultipleFiles' => 'classes/general/ibprop_multiplefiles.php',
        'DefaTools_IBProp_FileManEx' => 'classes/general/ibprop_filemanex.php',
        'DefaTools_UserType_Auth' => 'classes/general/usertype_auth.php',
        'DefaTools_IBProp_ElemListDescr' => 'classes/general/ibprop_elemlistdescr.php',
        'DefaTools_IBProp_OptionsGrid' => 'classes/general/ibprop_optionsgrid.php',
        'DefaTools_IBProp_ElemCompleter' => 'classes/general/ibprop_elemcompleter.php',
        'DefaTools_Typograf' => 'classes/general/typograf.php',
   )
);

DefaTools_AdminServicesManager::getInstance()->addService(new DefaTools_IB_Copy());
DefaTools_AdminServicesManager::getInstance()->addService(new DefaTools_IB_Demo());
?>