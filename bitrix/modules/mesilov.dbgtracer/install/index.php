<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

//���������, �������� �� �������� ��� �������������� ������� ��� ��������� ������ 
if(!IsModuleInstalled("mesilov.dbgtracer"))
{
	if(function_exists('dd') || function_exists('d'))
	{
		// �������� ��������� ������
		return false;
	}
}

class mesilov_dbgtracer extends CModule
{
	var $MODULE_ID = "mesilov.dbgtracer";
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_SORT;
	var $PARTNER_NAME;
	var $PARTNER_URI;
	// ����������� ��������� ������
	var $MODULE_OPT_NAME_USER_GROUP_ID = 'DBGTRACER_USER_GROUP_ID';
	
	function __construct()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

		$this->MODULE_NAME = GetMessage("DBGTRACER_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("DBGTRACER_MODULE_DESC");
		
		$this->PARTNER_NAME = GetMessage("DBGTRACER_MODULE_PARTNER");
		$this->PARTNER_URI = GetMessage("DBGTRACER_MODULE_PARTNER_URI");
	}
	
	function InstallDB($arParams = array())
	{
		RegisterModule($this->MODULE_ID);
		RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "CAllDbgTracer", "OnBeforePrologEventHandler", 1);
		RegisterModuleDependences("main", "OnPanelCreate", $this->MODULE_ID, "CAllDbgTracer", "OnPanelCreateEventHandler", 1);
        
        // ������ ������, ������� ����� ������ ���������� ��������� 
        // � ���������� � ID � ��������� ������
		// � ������ ������� ���� ������������ - �������������
		$group = new CGroup;
		$arFields = Array(
			"ACTIVE"       => "Y",
			"C_SORT"       => 10000,
			"NAME"         => "CBitrixDebug Users",
			"DESCRIPTION"  => "Members of users CBitrixDebug class, they can see debug messages by function d()",
			"USER_ID"      => array(1)
		);
		$gebugUserGroupID = $group->Add($arFields);
		COption::SetOptionInt($this->MODULE_ID, $this->MODULE_OPT_NAME_USER_GROUP_ID, $gebugUserGroupID);
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		// ������ �� �������� ID ������ ��� ������� 
		$debugUsersGroupId = COption::GetOptionInt($this->MODULE_ID, $this->MODULE_OPT_NAME_USER_GROUP_ID, null);
		// ������� ������
		CGroup::Delete($debugUsersGroupId);
		// ������ ���������
        COption::RemoveOption($this->MODULE_ID, $this->MODULE_OPT_NAME_USER_GROUP_ID);
		
		UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "CAllDbgTracer", "OnBeforePrologEventHandler");
		UnRegisterModuleDependences("main", "OnPanelCreate", $this->MODULE_ID, "CAllDbgTracer", "OnPanelCreateEventHandler");
		UnRegisterModule($this->MODULE_ID);

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		// �������� �������� ��� ������
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/images/",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/images/".$this->MODULE_ID."/", true, true
		);
		return true;
	}

	function UnInstallFiles()
	{
		// ������� �������� ��� ������
		DeleteDirFilesEx("/bitrix/images/".$this->MODULE_ID."/");
		return true;
	}

	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		$APPLICATION->IncludeAdminFile(
			GetMessage("DBGTRACER_INSTALL_TITLE"),
			$DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID."/install/step.php"
		);
	}

	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallFiles();
		$this->UnInstallDB();
		$APPLICATION->IncludeAdminFile(
			GetMessage("DBGTRACER_UNINSTALL_TITLE"),
			$DOCUMENT_ROOT."/bitrix/modules/".$this->MODULE_ID."/install/unstep.php"
		);
	}
}
?>