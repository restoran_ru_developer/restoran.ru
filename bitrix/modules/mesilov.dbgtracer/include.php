<?
/**
* �����-������ ��� ������ ���������� ���������
* @author ������� ������, mesilov.maxim@gmail.com
*/

// ���������� ����� � ������������ � �������� 
IncludeModuleLangFile(__FILE__);
CModule::AddAutoloadClasses('mesilov.dbgtracer', array('CAllDbgTracer' => 'classes/general/CAllDbgTracer.php'));

/**
* ������� ������ ���������� ��������� � ������ ���� �������
* 
* @param mixed $variable - ���� ��������� ���������
* @param string $debugUserNameFromSourceCode - ����� ������������, ��� �������� ����� ��������� ����� ����������
*/
function d($variable, $debugUserNameFromSourceCode = null)
{
	static $objBitrixDebug = null;
	if(!is_object($objBitrixDebug))
	{
		$objBitrixDebug = new CAllDbgTracer();
	}
	if($objBitrixDebug->isDebugSessionActive($debugUserNameFromSourceCode))
	{
		$objBitrixDebug->printVariableView($variable, $debugUserNameFromSourceCode);			
	}
}// end of d()

/**
* ������� ������ ���������� ��������� � ������ ���� ������� � ����������� ������
* 
* @param mixed $variable
* @param mixed $debugUserNameFromSourceCode
*/
function dd($variable, $debugUserNameFromSourceCode = null)
{
	static $objBitrixDebug = null;
	if(!is_object($objBitrixDebug))
	{
		$objBitrixDebug = new CAllDbgTracer();
	}
	if($objBitrixDebug->isDebugSessionActive($debugUserNameFromSourceCode))
	{
		$objBitrixDebug->printVariableView($variable, $debugUserNameFromSourceCode);
		die();			
	}
}// end of dd()
?>