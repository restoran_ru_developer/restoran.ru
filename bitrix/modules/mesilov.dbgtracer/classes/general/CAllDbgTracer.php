<?
/**
* �����-������ ��� ������ ���������� ���������
* @author ������� ������, mesilov.maxim@gmail.com
*/

// ���������� ���� � ������������
IncludeModuleLangFile(__FILE__);

class CAllDbgTracer
{
	// ��������� ��� �������������� � ���������� ������� ��������
	/**
	* @var string - �������� ���������, �������� ID ������ �������������, ������� ����� ������ ���������� ���������
	*/
	private static $OPT_MODULE_USER_GROUP_ID = 'DBGTRACER_USER_GROUP_ID';
	/**
	* @var string - ���������� ��� ������
	*/
	private static $OPT_MODULE_NAME = 'mesilov.dbgtracer';	

	// ��������� ����������� ��������� ������
	/**
	* @var integer - ����� ����� ��������� ���� �� ���������� arDebugUsers
	*/
	private static $OPT_CACHE_TTL = 3600;
	/**
	* @var string - ������������� ����, � ��� ��� ���� ��� ���� �������, ������� ��� ������
	*/
	private static $OPT_CACHE_ID = __CLASS__;
	/**
	* @var string - ���� � ����� ���� �� ��. � ��� ��� ����� �� ����� ������
	*/
	private static $OPT_CACHE_PATH = __CLASS__;
	
	// ��������� ��������������� � �������������
	/**
	* @var string - ������������ ��������� � URL, � ������� ������� ����������� ���� ��������� ������ ���������� ���������
	*/
	private static $URL_PARAM_NAME	= "debug_session_status";
	/**
	* @var string - ��������, ������������ � $URL_PARAM_NAME � ���������� �������
	*/
	private static $URL_PARAM_STATUS_ENABLE	= "enable";
	/**
	* @var string - ��������, ������������ � $URL_PARAM_NAME � ����������� �������
	*/
	private static $URL_PARAM_STATUS_DISABLE = "disable";
	/**
	* @var string - ������� ��� ����, ������� ������������ �������
	*/
	private static $COOKIE_NAME_PREFIX = "_DEBUG_SESSION_STATUS";
	/**
	* @var integer - ����� ����� ���� - 1 ��� 
	*/
	private static $COOKIE_TTL = 31536000;

	//�������� ������
	/**
	* @var array ������ � ������� � debug-�������������, �� ����� ����� ��������, �������� �� �� ��������� ����, ���� ���-������, �� ������ �� �� ������ bx_debug_users
	*/
	private $arDebugUsers;
		
	//����������� ������� ������ main
	/**
	* ����������, ������������� ���� ��������� ������� � ����������� �� ��������� � URL
	*/
	public function OnBeforePrologEventHandler()
	{
		global $USER;
		// ���� ��� ����� ������������ ������� ������ ������� � ������������
		// �����������, �� ������ �� ������
		if($USER->IsAuthorized() && array_key_exists(self::$URL_PARAM_NAME, $_REQUEST))
		{
			// ��������� ������������ � ������ debug_users
			// ����� � ��� 
			// ����������� ����
			// � ����������� �� ����� ��������� cookie ���������� �����
			self::debugSessionCookieSwitcher($_REQUEST[self::$URL_PARAM_NAME]);			
		}
	}// end of OnBeforePrologEventHandler  	

	/**
	* ����������, ���������� ��� ������ ���������������� ������ ����������
	* ��������� ������ ���������� �������� ������ ���������� ���������
	*/
	public function OnPanelCreateEventHandler()
	{
		// ������ ������������ �� ���� �������
		// �������� ������� cookie � ������ �������, ���� ����������, �� 
		// ���������� ������ ���������� ���������� ���������
		// ���� ���, �� ���������� ������ ��������� ���������� ���������
		global $APPLICATION;
		// ������ ���������� ������ ���������� ��������� ������������ ������
		// ��� ������ ������ debug-users
		if(CAllDbgTracer::IsCurrentUserMemberOfDebugUsersGroup())
		{
			$btnHREF = DeleteParam(array(self::$URL_PARAM_NAME));
			if($btnHREF != '')
			{
				$btnHREF = $APPLICATION->GetCurPage().'?'.$btnHREF.'&'.self::$URL_PARAM_NAME.'=';	
			}
			else
			{
				$btnHREF = $APPLICATION->GetCurPage().'?'.self::$URL_PARAM_NAME.'=';					
			}
			
		    $arDebugMenu = array();
		    $btnText='';
		    $btnHint='';
            //��������� ������������� URL � �������
		    $imgPath = "/bitrix/images/".self::$OPT_MODULE_NAME.'/';
			// ���� ������� �������?
			if(CAllDbgTracer::isDebugSessionActive())
			
			{
				$btnHREF .= self::$URL_PARAM_STATUS_DISABLE;	
				$btnText = GetMessage("DBGTRACER_BTN_OFF");
				$btnHint = GetMessage("DBGTRACER_BTN_OFF_HINT");;				
				$btnIcon = $imgPath.'disable_debug.png';				
			}
			else
			{
				$btnHREF .= self::$URL_PARAM_STATUS_ENABLE;
				$btnText = GetMessage("DBGTRACER_BTN_ON");
				$btnHint = GetMessage("DBGTRACER_BTN_ON_HINT");				
				$btnIcon = $imgPath.'enable_debug.png';
			}
			// ���� ��������� ������ ������		  	
		  	$APPLICATION->AddPanelButton(array( 
    	  		"HREF" => $btnHREF,
    			"SRC" => $btnIcon, 
    			"ALT" => $btnHint, 
    			"TEXT" => $btnText, 
    			"MAIN_SORT" => 1000,
    			"SORT" => 100, 
    			"MENU" => $arDebugMenu, 
 				)
 			);
		}
	}// end of OnPanelCreateEventHandler
	
	/**
	* ����������� ������, ���������� ���� ��� �� ����� ����� �������� � ������ ������ ������� d()
	* ��������� ������ arDebugUsers
	* @return null
	*/
	public function __construct()
	{
		// �������� ������� ������ � ������������� �� ����
		if(!$this->ReadDataFromCache($this->arDebugUsers))
		{
			$this->arDebugUsers = $this->GetDebugUsersArray();
			$this->WriteDataToCache($this->arDebugUsers);
		} 
	}// end of __construct

	/**
	* ������� ��������, �������� �� ������� ������������ ������ ������ debug_users
	* @return boolean
	*/
	public function IsCurrentUserMemberOfDebugUsersGroup()
	{
		global $USER;
		$isMember = false;
		// �������� ��������� ������ ��� ������������ �������������
		if($USER->IsAuthorized())
		{
			//�������� id �������� ������������
			$curUserId = $USER->GetID();
			$arTmpDebugUsers = null;
			// �������� ������� ������ � ������������� �� ����
			if(!self::ReadDataFromCache($arTmpDebugUsers))
			{
				$arTmpDebugUsers = self::GetDebugUsersArray();
				self::WriteDataToCache($arTmpDebugUsers);
			}
									
			if(array_key_exists($curUserId, $arTmpDebugUsers))
			{
				$isMember =	true;	
			}                                           
		}
		return $isMember;
	}// end of IsCurrentUserMemberOfDebugUsersGroup

	/**
	* ����� ��������� ���������� ����� ������� ��� �������� ������������
	* @return boolean - true ���� ������ ������ ���������� ��������� ������� � false � ��������� ������
	*/
	public function isDebugSessionActive($debugUserNameFromSourceCode='')
	{
		$isDebugSessionActive = false;
		// ������������ cookie
		$cookieName = COption::GetOptionString("main", "cookie_name", "BITRIX_SM").self::$COOKIE_NAME_PREFIX;
		// ���� ����� ���� ���, �� ����� ��������		
		if(!array_key_exists($cookieName, $_COOKIE))
		{
			return $isDebugSessionActive;
		}	
		// ������������� ������ �� ��������� ����������� �� ����
		$arDebugUser = unserialize($_COOKIE[$cookieName]);
		// ��������� ������������ ������� ��� ���� �������
		if(self::GetBitrixBoxHash() != $arDebugUser['BITRIX_BOX_HASH'])
		{
			return $isDebugSessionActive;
		}
		// ������� d() ���������� ��� ����������� ������������? 
		if($debugUserNameFromSourceCode != '')
		{
			static $arTempDebugUsers = null;
			if(is_null($arTempDebugUsers))
			{
				if(!self::ReadDataFromCache($arTempDebugUsers))
				{
					$arTempDebugUsers = self::GetDebugUsersArray();
					self::WriteDataToCache($arTempDebugUsers);
				}
			}
			// � ��������� ������� �������� ���������� ID ������������
			if(array_key_exists(intval($arDebugUser['USER_ID']), $arTempDebugUsers))
			{
				$userFromCache = md5($arTempDebugUsers[intval($arDebugUser['USER_ID'])]['LOGIN'].$arTempDebugUsers[intval($arDebugUser['USER_ID'])]['DATE_REGISTER']);
				// ��� �� ���� ��������� � ����������� ����� ��� ������������ 
				// � ����� �� ������ d() ��������� � ������� ������������ 				
				if(($userFromCache == $arDebugUser['USER_LOGIN_HASH']) && ($arTempDebugUsers[intval($arDebugUser['USER_ID'])]['LOGIN'] == $debugUserNameFromSourceCode))
				{
					$isDebugSessionActive = true;
				}
			}
		}
		else
		{
			$isDebugSessionActive = true;	
		}			
		return $isDebugSessionActive;
	} //end of isDebugSessionActive

	/**
	* ����� ������� �� ����� �������� ���������� � ������ ���� ������� ��� ������� ������������
	* @param mixed $mixedVariable -  ����������, ������� ����� ��������
	* @param string $debugUserNameFromSourceCode - ����� ������������, ������� ������ ������ ���������� �����
	*/
	public function printVariableView($mixedVariable, $debugUserNameFromSourceCode='')
	{
		$variableViewHTML = $this->getDebugPrintVariableHTML($mixedVariable);
		print($variableViewHTML);
	}// end of printVariableView

	// ------------------------------------------------------------------------	
	// �������� ������ ������
	// ------------------------------------------------------------------------
	/**
	* ����� �������� �������� �� ���� ������ �������������, ������� ����� ������������ �������
	* @param array ��������� ������ �� ������� �������������, ������� �������� ����� ���������� ���������
	* @return boolean true - �� ���� ������� ������� ������, false - ��� ������ ��� ������ ��� ���
	*/
	private function ReadDataFromCache(&$arDebugUsersData)
	{
		// ������� ������ ��� ������ � �����
		$obCache = new CPHPCache;
		// ���� ��� ���� � �� ��� �� ����� �� �������� ������ �� ����
		if($obCache->InitCache(self::$OPT_CACHE_TTL, self::$OPT_CACHE_ID, self::$OPT_CACHE_PATH))
		{   
			// �������� ������
			$arDebugUsersData = $obCache->GetVars();
			return true;
		}
		else
		{
			return false;
		}		
	}// end of GetDataFromCache

	/**
	* ����� ����� ������ � ���
	* @param mixed $arDebugUsersData
	* @return null
	*/
	private function WriteDataToCache($arDebugUsersData)
	{
		// ������� ������ ��� ������ � �����
		$obCache = new CPHPCache;
		$obCache->InitCache(self::$OPT_CACHE_TTL, self::$OPT_CACHE_ID, self::$OPT_CACHE_PATH);
		$obCache->StartDataCache();
		$obCache->EndDataCache($arDebugUsersData);
	}// end of WriteDataToCache

	/**
	* ����� ���������� ������ �������������, ������� �������� ����� ���������� ���������
	* ��� ������ ������������� ������ bx_debug_users
	* $return array - ����� �������������, ������� ��������� �������
	*/
	private function GetDebugUsersArray()
	{
		// �������� �������� ������� ������������� �� ������
		// ��������� ID ������ �� �������� ������
		// ���� ��� ���, �� ������ ������ � ���������� ID � ��������� ������
		// � ������ ��������� ������
		
		// �������� ������������� ������, ������ ���� ������� ��� ��������� ������
		$gebugUserGroupID = COption::GetOptionInt(self::$OPT_MODULE_NAME, self::$OPT_MODULE_USER_GROUP_ID, -1);
		if($gebugUserGroupID == -1)
		{          
			ShowError(GetMessage("DBGTRACER_ERR_USER_GROUP_ID_NOT_FOUND"));
			return null;			
		}
		
		// �������� ������ �������������
		$arDebugUsersGroup = array();
		$arFilter = array(
			'ACTIVE' => 'Y',
			'GROUPS_ID' => array($gebugUserGroupID)
		);
        $rsUsers = CUser::GetList(($by="ID"), ($order="ASC"), $arFilter);
        while($arUser = $rsUsers->NavNext(true))
        {
			$arDebugUsersGroup[$arUser['ID']] = array(
				'LOGIN' => $arUser['LOGIN'],
				'DATE_REGISTER' => $arUser['DATE_REGISTER']
			);
        }
		return $arDebugUsersGroup;
	}//end of GetDebugUsersArray

	/**
	* ����� ���������� ��� ������� ���� ��������������� � ������� �������
	* @param mixed $currentSessionCookieStatus - ���� ���������� ��� �������� ����
	*/
	private function debugSessionCookieSwitcher($currentSessionCookieStatus)
	{
		// �������� ������������� �������� ������������ � ����� ����
		$arUserIdentifier = self::GetCurrentUserCookieIdentifier();
		$strUserIdentifier = serialize($arUserIdentifier);
		$cookieName = COption::GetOptionString("main", "cookie_name", "BITRIX_SM").self::$COOKIE_NAME_PREFIX;
		switch($currentSessionCookieStatus)
		{
			case self::$URL_PARAM_STATUS_ENABLE:
				// ��������� ���� �������
				$res = setcookie($cookieName, $strUserIdentifier, time()+self::$COOKIE_TTL, "/");
				// ��� ��������� ���� ��� �� ����������� � ������ $_COOKIES
				// ������� ���� ��������� � � ���� ������
				$_COOKIE[$cookieName] = $strUserIdentifier;
				break;
			case self::$URL_PARAM_STATUS_DISABLE:
				// ������� ���� �������
				@setcookie($cookieName, "", time()-self::$COOKIE_TTL, "/");
				unset($_COOKIE[$cookieName]);
				break;
			default:
				break;
		}
		return true;
	} //end of debugSessionCookieSwitcher	

	/**
	* ������� ���������� ������ � �������, ������� ������������ � ����
	* @return array
	*/
	private function GetCurrentUserCookieIdentifier()
	{
		// ����� ���������� ������ � ������ ���������� ����
		// ������� ����� ������� ���� ��������
		global $USER;
		// �������� ���������� �� �������� ������������
		$rsCurrentUser = CUser::GetByID($USER->GetID());
		$arCurrentUser = $rsCurrentUser->Fetch();
		//�������� ���������� �� ������
		$rsRootUser = CUser::GetByID(1);
		$arRootUser = $rsRootUser->Fetch();
		$arUserIdentifier = array(
			"USER_ID" => $USER->GetID(),
			// ������ ��� �� ������ + ���� �����������, ��� �� ����������� ������������,
			// ����� ���������, ��� ��� �� ����� ����� ���������� ����
			"USER_LOGIN_HASH" => md5($arCurrentUser['LOGIN'].$arCurrentUser['DATE_REGISTER']),
			// ����������� ��� �������� ��� ���� �������
			// ���� ��� ������������� �������������, ��� �� ���� ���� �������������� 
			// ���������
			// ����� ��� �� ���� ����������� ������ � ������� ���� � ������� �� 
			// �������� �������
			"BITRIX_BOX_HASH" => self::GetBitrixBoxHash()
		);		
		return $arUserIdentifier;
	}// end of GetCurrentUserCookieIdentifier

	/**
	* ������� ���������� ���, ������� ������������� ������ ���� ��� ���� ��������� ��������
	* @return - string
	*/
	private function GetBitrixBoxHash()
	{
		$bitrixBoxHash = '';
		$arTempDebugUsers = null;
		if(!self::ReadDataFromCache($arTempDebugUsers))
		{
			$arTempDebugUsers = self::GetDebugUsersArray();
			self::WriteDataToCache($arTempDebugUsers);
		}
		// �������� ID ����������� �����������. � 99% ��� ������������ �1 - ����� 
		$minUserId = min(array_keys($arTempDebugUsers));
		// �� ���� ����������� ����� ������������ � ����������� ���� �� �� ������������ ��� ��� ������� ���� ���
		// ����������
		$bitrixBoxHash = md5($arTempDebugUsers[$minUserId]['DATE_REGISTER'].__FILE__);		 
    	return $bitrixBoxHash;
	}// end of GetBitrixBoxHash
	
    /**
    * ������� ���������� ���� � ������, � ������� ��� ����� ������� d()
    * @return string - ��� ����� � ����� ������, ��� ��� ������ ����� ���������� �������
    */
    private function GetDebugBacktraceStack()
    {
    	$arDebugBacktrace = debug_backtrace();
    	// 0 - GetDebugBacktraceHTML
    	// 1 - getDebugPrintVariableHTML
    	// 2 - printVariableView
    	// 3 - ����� ������ ������� d()
    	$stackOffset = 3;
    	$strDebugBacktrace = $arDebugBacktrace[$stackOffset]['line'].' | '.$arDebugBacktrace[$stackOffset]['file'];
		return $strDebugBacktrace;		
    } //end of GetDebugBacktraceStack

	/**
	* ����� ������� �� ����� �������� ����������. ����� �������������� � ������������ � ���� ������.
	* @param mixed $mixedVariable - ������������ ����������
	* @return string - ����� �������� ����������
	*/
	private function getDebugPrintVariableHTML($mixedVariable)
	{
		//@2DO - ������� ���������� ����������� �������, ��� ����������� �������������� �����
		$debugBacktraceHTML = $this->GetDebugBacktraceStack();
		$variableViewHTML = '';
		ob_start();
			print('<pre style="color: black; background-color: lightgray;"><code title="'.$debugBacktraceHTML.'">');
			var_dump($mixedVariable);
			print('</code></pre>');
			$variableViewHTML = ob_get_contents();
		ob_end_clean();
		return $variableViewHTML;
	}// end of getVariableView
}// end of CAllBitrixDebug
?>