<?
$MESS ['DBGTRACER_MODULE_NAME'] = "DbgTracer";
$MESS ['DBGTRACER_MODULE_DESC'] = "Module for print debug messages only for developers. Documentation for module and description of his work you can see on <a href=\"http://dev.1c-bitrix.ru/community/webdev/user/14686/blog/dbgtracer-output-debug-messages-for-developers/\" title=\"Go to site \" target=\"_blank\">module page</a>";
$MESS ['DBGTRACER_INSTALL_TITLE'] = "Install module DbgTracer";
$MESS ['DBGTRACER_UNINSTALL_TITLE'] = "uninstall module DbgTracer";
$MESS ['DBGTRACER_MODULE_SORT'] = 10000;
$MESS ['DBGTRACER_MODULE_PARTNER'] = "Mesilov Maxim";
$MESS ['DBGTRACER_MODULE_PARTNER_URI'] = "http://dev.1c-bitrix.ru/community/webdev/user/14686/";
?>