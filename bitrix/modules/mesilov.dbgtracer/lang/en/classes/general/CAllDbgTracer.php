<?
$MESS ["DBGTRACER_ERR_USER_GROUP_ID_NOT_FOUND"] = "Error in module DbgTracer: debug user group not found. Reinstall module.";
$MESS ["DBGTRACER_BTN_OFF"] = "Off d( )";
$MESS ["DBGTRACER_BTN_OFF_HINT"] = "Hide debug messages d()";
$MESS ["DBGTRACER_BTN_ON"] = "On d( )";
$MESS ["DBGTRACER_BTN_ON_HINT"] = "Show debug messages d()";
?>