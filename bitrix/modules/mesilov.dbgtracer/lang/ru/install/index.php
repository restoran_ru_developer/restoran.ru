<?
$MESS ['DBGTRACER_MODULE_NAME'] = "DbgTracer";
$MESS ['DBGTRACER_MODULE_DESC'] = "Модуль для вывода отладочных cобщений во время разработки и сопровождения сайта. Опиание модуля и документацию можно посмотреть на <a href=\"http://dev.1c-bitrix.ru/community/webdev/user/14686/blog/dbgtracer-output-debug-messages-for-developers/\" title=\"Перейти на сайт модуля\" target=\"_blank\">сайте модуля</a>.";
$MESS ['DBGTRACER_INSTALL_TITLE'] = "Установка модуля DbgTracer";
$MESS ['DBGTRACER_UNINSTALL_TITLE'] = "Удаление модуля DbgTracer";
$MESS ['DBGTRACER_MODULE_SORT'] = 10000;
$MESS ['DBGTRACER_MODULE_PARTNER'] = "Месилов Максим";
$MESS ['DBGTRACER_MODULE_PARTNER_URI'] = "http://dev.1c-bitrix.ru/community/webdev/user/14686/";
?>