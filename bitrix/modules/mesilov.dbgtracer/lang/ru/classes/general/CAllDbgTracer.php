<?
$MESS ["DBGTRACER_ERR_USER_GROUP_ID_NOT_FOUND"] = "Ошибка в модуле DbgTracer: не найдена группа пользователей, которая имеет права на просмотр отладочных сообщений. Переустановите модуль.";
$MESS ["DBGTRACER_BTN_OFF"] = "Выключить d( )";
$MESS ["DBGTRACER_BTN_OFF_HINT"] = "Выключить показ отладочных сообщений d()";
$MESS ["DBGTRACER_BTN_ON"] = "Включить d( )";
$MESS ["DBGTRACER_BTN_ON_HINT"] = "Включить показ отладочных сообщений d()";
?>