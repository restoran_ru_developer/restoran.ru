<?php
$MESS ['UNISENDER_AJAX_MODE'] = 'Загрузка через AJAX';
$MESS ['UNISENDER_USE_CACHE'] = 'Кешировать компонент (3600 секунд)';
$MESS ['UNISENDER_LIST'] = 'Список контактов';
$MESS ['UNISENDER_FORM'] = 'Форма подписки';
$MESS ['UNISENDER_TEMPLATE'] = 'Использовать шаблон формы по умолчанию';
