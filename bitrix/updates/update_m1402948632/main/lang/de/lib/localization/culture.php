<?
$MESS["culture_err_del_lang"] = "Regionale Einstellungen können nicht gelöscht werden, weil sie von der Sprache #LID# genutzt werden.";
$MESS["culture_err_del_site"] = "Regionale Einstellungen können nicht gelöscht werden, weil sie von der Website #LID# genutzt werden.";
?>