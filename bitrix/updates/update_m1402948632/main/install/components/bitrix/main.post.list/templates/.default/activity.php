<?
define("PUBLIC_AJAX_MODE", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$arParams["PATH_TO_USER"] = $_REQUEST["PATH_TO_USER"];
$arParams["AVATAR_SIZE"] = $_REQUEST["AVATAR_SIZE"];
$arParams["NAME_TEMPLATE"] = (!!$_REQUEST["NAME_TEMPLATE"] ? $_REQUEST["NAME_TEMPLATE"] : CSite::GetNameFormat());
$arParams["SHOW_LOGIN"] = ($_REQUEST["SHOW_LOGIN"] == "Y" ? "Y" : "N");

if (check_bitrix_sessid() && $_REQUEST["MODE"] == "PUSH&PULL" &&
	$GLOBALS["USER"]->IsAuthorized() && !!$_REQUEST["ENTITY_XML_ID"] &&
	is_array($_SESSION["UC"]) && array_key_exists($_REQUEST["ENTITY_XML_ID"], $_SESSION["UC"]) &&
	(time() - $_SESSION["UC"][$_REQUEST["ENTITY_XML_ID"]]["ACTIVITY"] > 10) &&
	CModule::IncludeModule("pull") && CPullOptions::GetNginxStatus())
{
	$APPLICATION->RestartBuffer();
	$_SESSION["UC"][$_REQUEST["ENTITY_XML_ID"]]["ACTIVITY"] = time();
	$user_id = $GLOBALS["USER"]->GetId();

	CModule::IncludeModule("blog");
	$arUserInfo = CBlogUser::GetUserInfo($user_id,
		$arParams["PATH_TO_USER"], array("AVATAR_SIZE" => $arParams["AVATAR_SIZE"], "AVATAR_SIZE_COMMENT" => $arParams["AVATAR_SIZE"])
	);

	$arTmpUser = array(
		"NAME" => $arUserInfo["~NAME"],
		"LAST_NAME" => $arUserInfo["~LAST_NAME"],
		"SECOND_NAME" => $arUserInfo["~SECOND_NAME"],
		"LOGIN" => $arUserInfo["~LOGIN"],
		"NAME_LIST_FORMATTED" => "",
	);
	$arUserInfo["NAME_FORMATED"] = CUser::FormatName($arParams["NAME_TEMPLATE"], $arTmpUser, ($arParams["SHOW_LOGIN"] != "N" ? true : false), false);

	CPullWatch::AddToStack('UNICOMMENTS'.$_REQUEST["ENTITY_XML_ID"],
		Array(
			'module_id' => 'unicomments',
			'command' => 'answer',
			'params' => Array(
				"USER_ID" => $user_id,
				"ENTITY_XML_ID" => $_REQUEST["ENTITY_XML_ID"],
				"TS" => time(),
				"NAME" => $arUserInfo["NAME_FORMATED"],
				"AVATAR" => $arUserInfo["PERSONAL_PHOTO_resized_30"]["src"]
			)
		)
	);
	die();
}
?>