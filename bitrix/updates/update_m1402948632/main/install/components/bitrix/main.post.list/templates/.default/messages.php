<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript">
BX.message({
	"B_B_MS_LINK" : "<?=GetMessageJS("B_B_MS_LINK")?>",
	"BPC_MES_EDIT" : "<?=GetMessageJS("BPC_MES_EDIT")?>",
	"BPC_MES_HIDE" : "<?=GetMessageJS("BPC_MES_HIDE")?>",
	"BPC_MES_SHOW" : "<?=GetMessageJS("BPC_MES_SHOW")?>",
	"BPC_MES_DELETE" : "<?=GetMessageJS("BPC_MES_DELETE")?>",
	"BPC_MES_DELETE_POST_CONFIRM" : "<?=GetMessageJS("BPC_MES_DELETE_POST_CONFIRM")?>",
	"MPL_RECORD_TEMPLATE" : '<?=CUtil::JSEscape($template)?>',
	"JERROR_NO_MESSAGE" : '<?=GetMessageJS("JERROR_NO_MESSAGE")?>',
	"BLOG_C_HIDE" : '<?=GetMessageJS("BLOG_C_HIDE")?>',
	JQOUTE_AUTHOR_WRITES : '<?=GetMessageJS("JQOUTE_AUTHOR_WRITES")?>',
	FC_ERROR : '<?=GetMessageJS("B_B_PC_COM_ERROR")?>'});
</script>