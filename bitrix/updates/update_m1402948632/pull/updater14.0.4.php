<?
if(IsModuleInstalled('pull'))
{
	$updater->CopyFiles("install/components", "components");
	//Following copy was parsed out from module installer
	$updater->CopyFiles("install/js", "js");
}
if($updater->CanUpdateDatabase())
{
	if (IsModuleInstalled('bitrix24'))
	{
		COption::SetOptionString("pull", "path_to_listener", "http://#DOMAIN#/sub/");
		COption::SetOptionString("pull", "path_to_listener_secure", "https://#DOMAIN#/sub/");
		COption::SetOptionString("pull", "path_to_mobile_listener", "http://#DOMAIN#:8895/sub/");
		COption::SetOptionString("pull", "path_to_mobile_listener_secure", "https://#DOMAIN#:8895/sub/");
	}
	else
	{
		COption::SetOptionString("pull", "path_to_mobile_listener", COption::GetOptionString("pull", "path_to_listener"));
		COption::SetOptionString("pull", "path_to_mobile_listener_secure", COption::GetOptionString("pull", "path_to_listener_secure"));
	}
}
?>
