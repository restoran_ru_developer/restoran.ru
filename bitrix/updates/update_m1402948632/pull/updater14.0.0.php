<?
if($updater->CanUpdateDatabase())
{
	if($updater->TableExists("b_pull_channel"))
	{
		if ($DB->IndexExists("b_pull_channel", Array("USER_ID")))
		{
			$updater->Query(array(
				"MySQL"  => "DROP INDEX IX_PULL_CN_UID ON b_pull_channel",
				"Oracle" => "DROP INDEX IX_PULL_CN_UID",
				"MSSQL"  => "DROP INDEX IX_PULL_CN_UID ON b_pull_channel",
			));
		}

		global $CACHE_MANAGER;

		$arUsers = Array();
		$res = $DB->Query("SELECT USER_ID, COUNT(ID) CNT FROM b_pull_channel GROUP BY USER_ID HAVING COUNT(ID) > 1");
		while ($row = $res->Fetch())
		{
			$arUsers[] = $row['USER_ID'];
			$CACHE_MANAGER->Clean("b_pchc_".$row['USER_ID'], "b_pull_channel");
		}
		if (count($arUsers) > 0)
		{
			$arChannels = Array();
			$res = $DB->Query("SELECT CHANNEL_ID FROM b_pull_channel WHERE USER_ID IN (".implode(',', $arUsers).")");
			while ($row = $res->Fetch())
				$arChannels[] = $row['CHANNEL_ID'];

			$DB->Query("DELETE FROM b_pull_channel WHERE USER_ID IN (".implode(',', $arUsers).")");

			$arMessage = Array(
				'module_id' => 'pull',
				'command' => 'channel_die',
				'params' => Array('from' => 'delete by update')
			);
			CModule::IncludeModule('pull');
			foreach($arChannels as $channelId)
				CPullStack::AddByChannel($channelId, $arMessage);
		}

		$DB->Query("CREATE UNIQUE INDEX IX_PULL_CN_UID ON b_pull_channel(USER_ID)");
	}
}
?>
