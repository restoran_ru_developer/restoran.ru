<?
if(IsModuleInstalled('fileman'))
{
	$updater->CopyFiles("install/admin", "admin");
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/images", "images");
}
if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/fileman/classes/general/html_editor.php",
		"modules/fileman/install/js/fileman/html_editor/html-actions.js",
		"modules/fileman/install/js/fileman/html_editor/html-components.js",
		"modules/fileman/install/js/fileman/html_editor/html-editor.css",
		"modules/fileman/install/js/fileman/html_editor/html-editor.js",
		"modules/fileman/install/js/fileman/html_editor/html-parser.js",
		"modules/fileman/install/js/fileman/html_editor/html-views.js",
		"modules/fileman/install/js/fileman/html_editor/iframe-style.css",
		"modules/fileman/install/js/fileman/html_editor/range.js",
		"modules/fileman/lang/ru/classes/general/html_editor_js.php",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
