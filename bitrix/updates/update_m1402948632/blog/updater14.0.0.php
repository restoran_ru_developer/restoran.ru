<?
if(IsModuleInstalled('blog'))
{
	$updater->CopyFiles("install/components", "components");
}

if($updater->CanUpdateDatabase())
{
	if($updater->TableExists("b_blog_comment"))
	{
		if(!$DB->Query("select SHARE_DEST from b_blog_comment WHERE 1=0", true))
		{
			$updater->Query(array(
					"MySQL" => "alter table b_blog_comment ADD SHARE_DEST VARCHAR(255) default NULL;",
					"Oracle" => "alter table b_blog_comment ADD SHARE_DEST varchar2(255 CHAR) null",
					"MSSQL" => "alter table b_blog_comment ADD SHARE_DEST varchar(255) null",
				));
		}
	}
}
?>