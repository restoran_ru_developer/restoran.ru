<?
$MESS["BLG_GP_EMPTY_TITLE"] = "Die Nachrichtenüberschrift wurde nicht angegeben";
$MESS["BLG_GP_EMPTY_DETAIL_TEXT"] = "Der Nachrichtentext wurde nicht angegeben";
$MESS["BLG_GP_EMPTY_BLOG_ID"] = "Der Nachrichtenblog wurde nicht angegeben";
$MESS["BLG_GP_ERROR_NO_BLOG"] = "Der Blog mit der ID &quot;#ID#&quot; wurde nicht gefunden";
$MESS["BLG_GP_EMPTY_AUTHOR_ID"] = "Der Nachrichtenautor wurde nicht angegeben";
$MESS["BLG_GP_ERROR_NO_AUTHOR"] = "Die ID des Nachrichtenautors ist falsch";
$MESS["BLG_GP_ERROR_DATE_CREATE"] = "Das Erstellungsdatum der Nachricht  wurde falsch angegeben";
$MESS["BLG_GP_ERROR_DATE_PUBLISH"] = "Das Veröffentlichungsdatum wurde falsch angegeben";
$MESS["BLG_GP_ERROR_ATTACH_IMG"] = "Es wurde ein falsches Bild angehänngt";
$MESS["BLG_SONET_TITLE"] = "Beitrag \"#TITLE#\" zum Blog hinzugefügt";
$MESS["BLG_GP_CODE_EXIST"] = "Ein Blogbeitrag mit dem Code <b>#CODE#</b> existiert bereits. Ändern Sie bitte die Adresse des Beitrags.";
$MESS["BLG_GP_RESERVED_CODE"] = "Der Beitrag kann nicht den Code <b>#CODE#</b>haben. Ändern Sie bitte die Adresse des Beitrags.";
$MESS["BLG_GP_IM_1"] = "Hat Ihnen eine Nachricht \"#title#\" gesendet";
$MESS["BLG_GP_IM_4"] = "Hat einen Kommentar zur Nachricht \"#title#\" hinzugefügt";
$MESS["BLG_GP_IM_5"] = "Hat einen Kommentar zu Ihrer Nachricht in der Kommunikation \"#title#\" hinzugefügt";
$MESS["BLG_GP_IM_6"] = "hat Sie in der Nachricht \"#title#\" erwähnt";
$MESS["BLG_GP_IM_7"] = "hat Sie im Kommentar zur Nachricht \"#title#\" erwähnt";
$MESS["SONET_IM_NEW_POST"] = "Zur Gruppe #group_name# wurde eine neue Nachricht #title# hinzugefügt.";
$MESS["BLG_GP_IM_1_FEMALE"] = "hat eine Nachricht \"#title#\" an Sie gesendet";
$MESS["BLG_GP_IM_4_FEMALE"] = "hat einen Kommentar zu \"#title#\" hinzugefügt";
$MESS["BLG_GP_IM_5_FEMALE"] = "hat einen Kommentar zu Ihrem Beitrag \"#title#\" hinzugefügt";
$MESS["BLG_GP_IM_6_FEMALE"] = "erwähnte Sie in \"#title#\"";
$MESS["BLG_GP_IM_7_FEMALE"] = "erwähnte Sie in einem Kommentar zu \"#title#\"";
$MESS["BLG_GP_IM_8"] = "hat neue Empfänger zu Ihrem Beitrag \"#title#\" hinzugefügt";
$MESS["BLG_GP_IM_8_FEMALE"] = "hat neue Empfänger zu Ihrem Beitrag \"#title#\" hinzugefügt";
?>