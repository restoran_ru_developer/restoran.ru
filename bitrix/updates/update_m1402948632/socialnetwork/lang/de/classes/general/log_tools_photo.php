<?
$MESS["SONET_PHOTO_LOG_GUEST"] = "Gast";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# hat ein Foto #TITLE# hinzugefügt";
$MESS["SONET_PHOTO_ADD_COMMENT_SOURCE_ERROR"] = "Der Kommentar konnte zur Eventquelle nicht hinzugefügt werden.";
$MESS["SONET_PHOTO_IM_COMMENT"] = "Hat einen Kommentar zu Ihrem Foto #photo_title# im Album #album_title# hinzugefügt";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# hat eine Datei hinzugefügt #TITLE# ";
$MESS["SONET_PHOTO_LOG_2"] = "Fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Neue Fotos: #LINKS# und andere.";
$MESS["SONET_IM_NEW_PHOTO"] = "In der Gruppe \"#group_name#\" wurde ein neues Foto zum Album \"#title#\" hinzugefügt.";
?>