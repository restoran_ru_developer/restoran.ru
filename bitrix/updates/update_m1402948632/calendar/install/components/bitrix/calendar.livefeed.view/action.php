<?
define("NO_KEEP_STATISTIC", true);
define("BX_STATISTIC_BUFFER_USED", false);
define("NO_LANG_FILES", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_PUBLIC_TOOLS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/bx_root.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("calendar") || !class_exists("CCalendar"))
	return;

$event_feed_action = $_REQUEST["event_feed_action"];
if (!empty($event_feed_action) && check_bitrix_sessid())
{
	$APPLICATION->ShowAjaxHead();
	$userId = $USER->GetId();
	$eventId = intVal($_REQUEST['event_id']);

	if ($event_feed_action == 'delete_event')
	{
		$res = CCalendar::DeleteEvent($eventId);
		if ($res)
			echo '#EVENT_FEED_RESULT_OK#';
	}
	else
	{
		$status = false;
		if ($event_feed_action == 'decline')
			$status = 'N';
		elseif($event_feed_action == 'accept')
			$status = 'Y';

		if ($status && $eventId)
		{
			CCalendarEvent::SetMeetingStatus($userId, $eventId, $status);
			echo '#EVENT_FEED_RESULT_OK#';
		}
	}
	die();
}
?>