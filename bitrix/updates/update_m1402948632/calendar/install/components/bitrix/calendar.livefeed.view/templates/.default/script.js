;(function(window){
	window.ViewEventManager = function(config)
	{
		this.id = config.id;
		this.config = config;
		this.userId = BX.message('sonetLCurrentUserID');

		this.viewEventUrl = this.config.viewEventUrlTemplate;
		this.viewEventUrl = this.viewEventUrl.replace(/#user_id#/ig, this.userId);
		this.viewEventUrl = this.viewEventUrl.replace(/#event_id#/ig, this.config.eventId);

		BX.ready(BX.proxy(this.Init, this));
	}

	window.ViewEventManager.prototype = {
		Init: function()
		{
			var _this = this;
			this.pMoreAttLinkY = BX('feed-event-more-att-link-y-' + this.id);
			this.pMoreAttLinkN = BX('feed-event-more-att-link-n-' + this.id);
			this.pMoreAttPopupContY = BX('feed-event-more-attendees-y-' + this.id);
			this.pMoreAttPopupContN = BX('feed-event-more-attendees-n-' + this.id);

			this.pViewIconLink = BX('feed-event-view-icon-link-' + this.id);
			this.pViewLink = BX('feed-event-view-link-' + this.id);
			this.pViewLink.href = this.pViewIconLink.href = this.viewEventUrl;

			this.pFrom = BX('feed-event-view-from-' + this.id);
			this.pFrom.innerHTML = this.GetFromHtml(this.config.EVENT.DT_FROM_TS, this.config.EVENT.DT_SKIP_TIME, this.config.EVENT.DT_LENGTH);

			if (this.pMoreAttLinkY && this.pMoreAttPopupContY)
			{
				this.popupNotifyMoreY = new BX.PopupWindow('bx-event-attendees-window-y' + this.id, this.pMoreAttLinkY,
				{
					zIndex: 200,
					lightShadow : true,
					offsetTop: -2,
					offsetLeft: 3,
					autoHide: true,
					closeByEsc: true,
					bindOptions: {position: "top"},
					content : this.pMoreAttPopupContY
				});
				this.popupNotifyMoreY.setAngle({});
				this.pMoreAttLinkY.onclick = function(){
					_this.popupNotifyMoreY.show();
				}
			}
			if (this.pMoreAttLinkN && this.pMoreAttPopupContN)
			{
				this.popupNotifyMoreN = new BX.PopupWindow('bx-event-attendees-window-n' + this.id, this.pMoreAttLinkN,
				{
					zIndex: 200,
					lightShadow : true,
					offsetTop: -2,
					offsetLeft: 3,
					autoHide: true,
					closeByEsc: true,
					bindOptions: {position: "top"},
					content : this.pMoreAttPopupContN
				});
				this.popupNotifyMoreN.setAngle({});
				this.pMoreAttLinkN.onclick = function(){
					_this.popupNotifyMoreN.show();
				}
			}

			// Invite controls
			var inviteCont = BX('feed-event-invite-controls-' + this.id);
			if (this.config.EVENT.IS_MEETING)
			{
				var status = this.config.attendees[this.userId].STATUS;
				inviteCont.className = 'feed-cal-view-inv-controls' + ' feed-cal-view-inv-controls-' + status.toLowerCase();

				if (status == 'Y')
				{
					var linkY = BX('feed-event-stat-link-y-' + this.id);
					this.popupAccepted = new BX.PopupWindow('bx-event-change-win-y-' + this.id, linkY, {
						zIndex: 200,
						lightShadow : true,
						offsetTop: -5,
						offsetLeft: (linkY.offsetWidth || 100) + 10,
						autoHide: true,
						closeByEsc: true,
						bindOptions: {position: "top"},
						content : BX('feed-event-stat-link-popup-y-' + this.id)
					});
					this.popupAccepted.setAngle({});
					linkY.onclick = function(){_this.popupAccepted.show();}
					BX('feed-event-decline-2-' + this.id).onclick = BX.proxy(this.Decline, this);
				}
				else if (status == 'N')
				{
					var linkN = BX('feed-event-stat-link-n-' + this.id);
					this.popupDeclined = new BX.PopupWindow('bx-event-change-win-n-' + this.id, linkN, {
						zIndex: 200,
						lightShadow : true,
						offsetTop: -5,
						offsetLeft: (linkN.offsetWidth || 100) + 10,
						autoHide: true,
						closeByEsc: true,
						bindOptions: {position: "top"},
						content : BX('feed-event-stat-link-popup-n-' + this.id)
					});
					this.popupDeclined.setAngle({});
					linkN.onclick = function(){_this.popupDeclined.show();}
					BX('feed-event-accept-2-' + this.id).onclick = BX.proxy(this.Accept, this);
				}
				else
				{
					BX('feed-event-accept-' + this.id).onclick = BX.proxy(this.Accept, this);
					BX('feed-event-decline-' + this.id).onclick = BX.proxy(this.Decline, this);
				}
			}
			else
			{
				inviteCont.style.display = 'none';
			}

			BX.viewElementBind(
				'bx-feed-cal-view-files-' + this.id,
				{showTitle: true},
				function(node){
					return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
				}
			);
		},

		Accept: function()
		{
			// TODO: AJAX reload for this actions
			BX.ajax.get(
				this.config.actionUrl,
				{
					event_feed_action: 'accept',
					sessid: BX.bitrix_sessid(),
					event_id: this.config.eventId
				},
				function(result)
				{
					if (result.indexOf('#EVENT_FEED_RESULT_OK#') !== -1)
						BX.reload();
				}
			);
		},

		Decline: function()
		{
			BX.ajax.get(
				this.config.actionUrl,
				{
					event_feed_action: 'decline',
					sessid: BX.bitrix_sessid(),
					event_id: this.config.eventId
				},
				function(result)
				{
					if (result.indexOf('#EVENT_FEED_RESULT_OK#') !== -1)
						BX.reload();
				}
			);
		},

		DeleteEvent: function()
		{
			if (!this.config.eventId || !confirm(this.config.EC_JS_DEL_EVENT_CONFIRM))
				return false;

			BX.ajax.get(
				this.config.actionUrl,
				{
					event_feed_action: 'delete_event',
					sessid: BX.bitrix_sessid(),
					event_id: this.config.eventId
				},
				function(result)
				{
					if (result.indexOf('#EVENT_FEED_RESULT_OK#') !== -1)
						BX.reload();
				}
			);
		},

		GetFromHtml: function(DT_FROM_TS, DT_SKIP_TIME, DT_LENGTH)
		{
			var
				from = BX.date.getBrowserTimestamp(DT_FROM_TS),
				fromDate = new Date(from),
				dayl = 86400,
				dateFormat = BX.date.convertBitrixFormat(BX.message('FORMAT_DATE')),
				timeFormat = BX.message('FORMAT_DATETIME'),
				timeFormat2 = BX.util.trim(timeFormat.replace(BX.message('FORMAT_DATE'), '')),
				html;

			if (timeFormat2 == dateFormat)
				timeFormat = "HH:MI";
			else
				timeFormat = timeFormat2.replace(/:SS/ig, '');
			timeFormat = BX.date.convertBitrixFormat(timeFormat);

			if (DT_SKIP_TIME == 'Y')
			{
				html = BX.date.format([
					["today", "today"],
					["tommorow", "tommorow"],
					["yesterday", "yesterday"],
					["" , dateFormat]
				], fromDate);
			}
			else
			{
				html = BX.date.format([
					["today", "today"],
					["tommorow", "tommorow"],
					["yesterday", "yesterday"],
					["" , dateFormat]
				], fromDate);

				html += ', ' + BX.date.format(timeFormat, fromDate);
			}

			return html;
		}
	};

//
//	window.__GetFromHtml = function(DT_FROM_TS, DT_SKIP_TIME)
//	{
//		var
//			from = BX.date.getBrowserTimestamp(DT_FROM_TS),
//			fromDate = new Date(from),
//			dayl = 86400,
//			dateFormat = BX.date.convertBitrixFormat(BX.message('FORMAT_DATE')),
//			timeFormat = BX.message('FORMAT_DATETIME'),
//			timeFormat2 = BX.util.trim(timeFormat.replace(dateFormat, '')),
//			html;
//
//		if (timeFormat2 == timeFormat2)
//			timeFormat = "HH:MI";
//		else
//			timeFormat = timeFormat2.replace(/:SS/ig, '');
//		timeFormat = BX.date.convertBitrixFormat(timeFormat);
//
//		if (DT_SKIP_TIME == 'Y')
//		{
//			html = BX.date.format([
//				["today", "today"],
//				["tommorow", "tommorow"],
//				["yesterday", "yesterday"],
//				["" , dateFormat]
//			], fromDate);
//		}
//		else
//		{
//			html = BX.date.format([
//				["today", "today"],
//				["tommorow", "tommorow"],
//				["yesterday", "yesterday"],
//				["" , dateFormat]
//			], fromDate);
//
//			html += ', ' + BX.date.format(timeFormat, fromDate);
//		}
//
//		return html;
//	}
})(window);


