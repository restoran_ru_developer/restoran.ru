<?
if(IsModuleInstalled('forum'))
{
	$updater->CopyFiles("install/components", "components");
}
if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/forum/install/components/bitrix/forum.comments/templates/bitrix24/html_parser.php",
		"components/bitrix/forum.comments/templates/bitrix24/html_parser.php",
		"modules/forum/install/components/bitrix/forum.comments/mention.php",
		"components/bitrix/forum.comments/mention.php",
		"modules/forum/install/components/bitrix/forum.interface/templates/spoiler/script.js",
		"components/bitrix/forum.interface/templates/spoiler/script.js",
		"modules/forum/install/components/bitrix/forum.interface/templates/popup_image/component_epilog.php",
		"components/bitrix/forum.interface/templates/popup_image/component_epilog.php",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
