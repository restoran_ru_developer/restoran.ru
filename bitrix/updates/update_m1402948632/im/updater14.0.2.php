<?
if($updater->CanUpdateKernel())
{
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/public", "public");

	$arToDelete = array(
		"modules/im/install/js/im/images/im-sprite-v6.png",
		"modules/im/install/js/im/images/notifier-message.gif",
		"modules/im/install/js/im/images/no-items.png",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);

	if (CModule::IncludeModule('xmpp'))
	{
		$arMessage = array(
			"query" => array(
				"." => array("type" => "set"),
				"action" => array("#" => "die"),
			),
		);
		CXMPPUtility::SendToServer($arMessage);
	}
}
?>