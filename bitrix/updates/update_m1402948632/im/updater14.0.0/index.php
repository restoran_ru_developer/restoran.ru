<?
if($updater->CanUpdateKernel())
{
	if(IsModuleInstalled('im'))
	{
		$updater->CopyFiles("install/components", "components");
		$updater->CopyFiles("install/js", "js");
	}
	$arToDelete = array(
		"modules/im/install/js/im/images/im-sprite-v4.png",
		"/js/im/images/im-sprite-v4.png",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
if($updater->CanUpdateDatabase())
{
	if(IsModuleInstalled('im'))
	{
		if(!IsModuleInstalled('bitrix24'))
		{
			$MESS = Array();
			if (file_exists($_SERVER["DOCUMENT_ROOT"].$updater->curPath."/lang/".LANGUAGE_ID."/updater.php"))
			{
				include_once($_SERVER["DOCUMENT_ROOT"].$updater->curPath."/lang/".LANGUAGE_ID."/updater.php");
				$ar = Array(
					"MESSAGE" => $MESS['UPDATER_IM_INDEX_1'],
					"TAG" => "IM_INDEX_1",
					"MODULE_ID" => "IM",
				);
				CAdminNotify::Add($ar);
			}
		}
		if (CModule::IncludeModule('xmpp'))
		{
			$arMessage = array(
				"query" => array(
					"." => array("type" => "set"),
					"action" => array("#" => "die"),
				),
			);
			CXMPPUtility::SendToServer($arMessage);
		}
		RegisterModuleDependences('main', 'OnUserDelete', 'im', 'CIMEvent', 'OnUserDelete');
	}
	if($updater->TableExists("b_im_option"))
	{
		$updater->Query(array(
			"MySQL"  => "DROP TABLE b_im_option",
			"Oracle"  => "DROP TABLE B_IM_OPTION CASCADE CONSTRAINTS",
			"MSSQL"  => "DROP TABLE B_IM_OPTION",
		));
		$updater->Query(array("Oracle" => "DROP SEQUENCE SQ_B_IM_OPTION"));
	}
	if($updater->TableExists("B_IM_STATUS"))
	{
		$updater->Query(array("Oracle" => "CREATE SEQUENCE SQ_B_IM_STATUS INCREMENT BY 1 START WITH 1"), true);
	}
}

?>
