<?
$MESS['UPDATER_IM_INDEX_1'] = 'Вы обновили модуль "Веб-мессенджер", для улучшения быстродействия модуля необходимо выполнить следующие запросы: <b>CREATE INDEX IX_IM_MESS_4 ON b_im_message(CHAT_ID, NOTIFY_READ)</b>, <b>CREATE INDEX IX_IM_MESS_5 ON b_im_message(CHAT_ID, DATE_CREATE)</b> и <b>DROP INDEX IX_IM_MESS_1 ON b_im_message</b>';
?>