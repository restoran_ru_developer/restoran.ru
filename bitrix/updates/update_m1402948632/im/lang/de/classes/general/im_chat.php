<?
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "Der Absender ist nicht angegeben";
$MESS["IM_ERROR_EMPTY_TO_CHAT_ID"] = "Der Empfänger ist nicht angegeben";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "Der Nachrichtentext fehlt";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "H:i, d F Y";
$MESS["IM_MESSAGE_FORMAT_TIME"] = "H:i";
$MESS["IM_ERROR_EMPTY_CHAT_ID"] = "Die Chat-ID fehlt.";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "Die Nutzer-ID ist nicht angegeben.";
$MESS["IM_ERROR_EMPTY_USER_OR_CHAT"] = "Die Chat- bzw. Nutzer-ID ist nicht angegeben.";
$MESS["IM_ERROR_USER_NOT_FOUND"] = "Der angegebene Nutzer ist nicht im Chat.";
$MESS["IM_ERROR_KICK"] = "Nur der Chat-Besitzer kann andere Nutzer entfernen.";
$MESS["IM_ERROR_AUTHORIZE_ERROR"] = "Nur Chat-Mitglieder können andere Nutzer einladen.";
$MESS["IM_ERROR_NOTHING_TO_ADD"] = "Alle ausgewählten Nutzer sind bereits im Chat.";
$MESS["IM_ERROR_MIN_USER"] = "Wählen Sie bitte Nutzer aus, bevor Sie einen neuen Chat erstellen.";
$MESS["IM_ERROR_MAX_USER"] = "In diesem Chat können maximal #COUNT# Mitglieder sein";
$MESS["IM_CHAT_JOIN_M"] = "#USER_1_NAME# hat #USER_2_NAME# zum Chat eingeladen";
$MESS["IM_CHAT_JOIN_F"] = "#USER_1_NAME# hat #USER_2_NAME# zum Chat eingeladen";
$MESS["IM_CHAT_KICK_M"] = "#USER_1_NAME# hat #USER_2_NAME# aus dem Chat entfernt";
$MESS["IM_CHAT_KICK_F"] = "#USER_1_NAME# hat #USER_2_NAME# aus dem Chat entfernt";
$MESS["IM_CHAT_CHANGE_TITLE_M"] = "#USER_NAME# hat das Chat-Thema auf \"#CHAT_TITLE#\" geändert";
$MESS["IM_CHAT_CHANGE_TITLE_F"] = "#USER_NAME# hat das Chat-Thema auf \"#CHAT_TITLE#\" geändert";
$MESS["IM_CHAT_LEAVE_M"] = "#USER_NAME# hat den Chat verlassen";
$MESS["IM_CHAT_LEAVE_F"] = "#USER_NAME# hat den Chat verlassen";
$MESS["IM_ERROR_EMPTY_USER_ID_BY_PRIVACY"] = "Diese Nutzer erlaubten es nicht, sie zum Chat einzuladen";
$MESS["IM_ERROR_MIN_USER_BY_PRIVACY"] = "Der Chat kann nicht erstellt werden, weil die Nutzer ihre Einladung nicht erlauben";
?>