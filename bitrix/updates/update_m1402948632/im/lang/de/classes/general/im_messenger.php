<?
$MESS["IM_ERROR_MESSAGE_TYPE"] = "Der Nachrichtentyp ist nicht korrekt.";
$MESS["IM_ERROR_MESSAGE_TO_FROM"] = "Der Empf?nger bzw. Der Absender der Nachricht ist nicht korrekt angegeben.";
$MESS["IM_ERROR_MESSAGE_TO"] = "Der Empf?nger der Nachricht ist nicht korrekt angegeben.";
$MESS["IM_ERROR_MESSAGE_FROM"] = "Der Absender der Nachricht ist nicht korrekt angegeben.";
$MESS["IM_ERROR_MESSAGE_TEXT"] = "Der Nachrichtentext fehlt.";
$MESS["IM_ERROR_MESSAGE_AUTHOR"] = "Der Autor der Nachricht ist nicht korrekt angegeben.";
$MESS["IM_ERROR_NOTIFY_MODULE"] = "Das Absender-Modul f?r diese Nachricht ist nicht angegeben";
$MESS["IM_ERROR_NOTIFY_TYPE"] = "Der Benachrichtigungstyp ist nicht korrekt.";
$MESS["IM_ERROR_NOTIFY_DATE"] = "Das Zeitformat des Termins ist nicht korrekt.";
$MESS["IM_ERROR_NOTIFY_BUTTON"] = "Das Schaltfl?chen-Set ist nicht korrekt.";
$MESS["IM_ERROR_MESSAGE_CREATE"] = "Fehler bei der Erstellung einer Nachricht";
$MESS["IM_ERROR_BUTTON_ACCEPT"] = "Akzeptieren";
$MESS["IM_ERROR_BUTTON_CANCEL"] = "Abbrechen";
$MESS["IM_ERROR_NOTIFY_EVENT"] = "Der mnemonische Name der Benachrichtigung fehlt.";
$MESS["IM_ERROR_IMPORT_ID"] = "Die ID der Nachricht, welche importiert werden soll, ist nicht korrekt.";
$MESS["IM_ERROR_MESSAGE_DATE"] = "Das Zeitformat der Nachrichtenerstellung ist nicht korrekt.";
$MESS["IM_ERROR_MESSAGE_CANCELED"] = "Sie k?nnen Benachrichtigungen nicht an den angegebenen Kontakt senden.";
$MESS["IM_ERROR_GROUP_CANCELED"] = "Sie k?nnen Benachrichtigungen nicht an den angegebenen Chat senden.";
$MESS["IM_ERROR_NOTIFY_CANCELED"] = "Sie k?nnen keine Benachrichtigungen senden.";
$MESS["IM_SMILE_SET_EMPTY"] = "Satz: #ID#";
$MESS["IM_ERROR_MESSAGE_PRIVACY"] = "Die Nachricht konnte nicht zugestellt werden, weil der Nutzer Nachrichten nur von seinen Kontakten akzeptiert.";
$MESS["IM_ERROR_MESSAGE_PRIVACY_SELF"] = "Die Nachricht konnte nicht zugestellt werden, weil der Nutzer wegen Einstellungen Ihrer Privatsphäre darauf nicht antworten kann.";
?>