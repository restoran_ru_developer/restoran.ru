<?
if($updater->CanUpdateKernel())
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");

	$arToDelete = array(
		"modules/im/install/js/im/images/im-sprite-v4.png",
		"modules/im/install/js/im/images/im-sprite-v5.png",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
if($updater->CanUpdateDatabase())
{
	if (CModule::IncludeModule('xmpp'))
	{
		$arMessage = array(
			"query" => array(
				"." => array("type" => "set"),
				"action" => array("#" => "die"),
			),
		);
		CXMPPUtility::SendToServer($arMessage);
	}
	if($updater->TableExists("b_im_chat"))
	{
		if(!$DB->Query("select CALL_TYPE from b_im_chat WHERE 1=0", true))
		{
			$updater->Query(array(
				"MySQL"  => "alter table b_im_chat add column CALL_TYPE smallint(1) DEFAULT 0",
				"Oracle" => "alter table b_im_chat add CALL_TYPE NUMBER(1) DEFAULT 0",
				"MSSQL"  => "alter table b_im_chat add CALL_TYPE tinyint",
			));
			$updater->Query(array("MSSQL" => "ALTER TABLE B_IM_CHAT ADD CONSTRAINT DF_B_IM_CHAT_CT DEFAULT 0 FOR CALL_TYPE"));
		}
	}
	if($updater->TableExists("b_im_relation"))
	{
		if(!$DB->Query("select CALL_STATUS from b_im_relation WHERE 1=0", true))
		{
			$updater->Query(array(
				"MySQL"  => "alter table b_im_relation add column CALL_STATUS smallint(1) DEFAULT 0",
				"Oracle" => "alter table B_IM_RELATION add CALL_STATUS NUMBER(1) DEFAULT 0",
				"MSSQL"  => "alter table B_IM_RELATION add CALL_STATUS tinyint",
			));
			$updater->Query(array("MSSQL" => "ALTER TABLE B_IM_RELATION ADD CONSTRAINT DF_B_IM_REL_CS DEFAULT 0 FOR CALL_STATUS"));
		}
		if(!$DB->IndexExists("b_im_relation", array("CHAT_ID", "USER_ID")))
		{
			$updater->Query(array(
				"MySQL" => "CREATE INDEX IX_IM_REL_6 ON b_im_relation(CHAT_ID, USER_ID)",
				"MSSQL" => "CREATE INDEX IX_IM_REL_6 ON B_IM_RELATION(CHAT_ID, USER_ID)",
				"Oracle" => "CREATE INDEX IX_IM_REL_6 ON B_IM_RELATION(CHAT_ID, USER_ID)",
			));
		}
	}
}
?>
