<?
if(IsModuleInstalled('calendar'))
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/images", "images");
	$updater->CopyFiles("install/js", "js");
}

if($updater->CanUpdateDatabase())
{
	if($updater->TableExists("b_calendar_event") && !$DB->Query("select ATTENDEES_CODES from b_calendar_event WHERE 1=0", true))
	{
		$updater->Query(
			array(
				"MySQL"  => "ALTER TABLE b_calendar_event add ATTENDEES_CODES varchar(255) null",
				"MSSQL" => "ALTER TABLE b_calendar_event add ATTENDEES_CODES varchar(255) null",
				"Oracle"  => "ALTER TABLE b_calendar_event add ATTENDEES_CODES varchar2(255 CHAR) null",
			)
		);
	}

	// Add userfield
	if (IsModuleInstalled("webdav"))
	{
		$ENTITY_ID = 'CALENDAR_EVENT';
		$FIELD_NAME = 'UF_WEBDAV_CAL_EVENT';
		$arElement = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($ENTITY_ID, 0);
		if (empty($arElement) || $arElement == array() ||$arElement == false || !isset($arElement[$FIELD_NAME]))
		{
			$arFields = array(
				"ENTITY_ID" => $ENTITY_ID,
				"FIELD_NAME" => $FIELD_NAME,
				"XML_ID" => $FIELD_NAME,
				"USER_TYPE_ID" => "webdav_element",
				"SORT" => 100,
				"MULTIPLE" => "Y",
				"MANDATORY" => "N",
				"SHOW_FILTER" => "N",
				"SHOW_IN_LIST" => "N",
				"EDIT_IN_LIST" => "Y",
				"IS_SEARCHABLE" => "N"
			);

			$obUserField  = new CUserTypeEntity;
			$intID = $obUserField->Add($arFields, false);
			if (false == $intID)
			{
				if ($strEx = $GLOBALS['APPLICATION']->GetException())
				{
					$errors = $strEx->GetString();
				}
			}
		}
	}
}


if(IsModuleInstalled('calendar') && $updater->CanUpdateDatabase())
{
	RegisterModuleDependences('socialnetwork', 'OnFillSocNetFeaturesList', 'calendar', 'CCalendarLiveFeed', 'AddEvent');
	RegisterModuleDependences('socialnetwork', 'OnSonetLogEntryMenuCreate', 'calendar', 'CCalendarLiveFeed', 'OnSonetLogEntryMenuCreate');
	RegisterModuleDependences('socialnetwork', 'OnAfterSonetLogEntryAddComment', 'calendar', 'CCalendarLiveFeed', 'OnAfterSonetLogEntryAddComment');
	RegisterModuleDependences('socialnetwork', 'OnForumCommentIMNotify', 'calendar', 'CCalendarLiveFeed', 'OnForumCommentIMNotify');
	RegisterModuleDependences('socialnetwork', 'onAfterCommentAddAfter', 'calendar', 'CCalendarLiveFeed', 'onAfterCommentAddAfter');
	RegisterModuleDependences('socialnetwork', 'onAfterCommentUpdateAfter', 'calendar', 'CCalendarLiveFeed', 'onAfterCommentUpdateAfter');
	RegisterModuleDependences('socialnetwork', 'OnFillSocNetFeaturesList', 'calendar', 'CCalendarLiveFeed', 'AddEvent');
	RegisterModuleDependences('forum', 'OnAfterCommentAdd', 'calendar', 'CCalendarLiveFeed', 'onAfterCommentAdd');
	RegisterModuleDependences('forum', 'OnAfterCommentUpdate', 'calendar', 'CCalendarLiveFeed', 'OnAfterCommentUpdate');
	RegisterModuleDependences('socialnetwork', 'OnSonetLogEntryMenuCreate', 'calendar', 'CCalendarLiveFeed', 'OnSonetLogEntryMenuCreate');
	RegisterModuleDependences('socialnetwork', 'OnAfterSonetLogEntryAddComment', 'calendar', 'CCalendarLiveFeed', 'OnAfterSonetLogEntryAddComment');
	RegisterModuleDependences('socialnetwork', 'OnForumCommentIMNotify', 'calendar', 'CCalendarLiveFeed', 'OnForumCommentIMNotify');
	RegisterModuleDependences('socialnetwork', 'onAfterCommentAddAfter', 'calendar', 'CCalendarLiveFeed', 'onAfterCommentAddAfter');
	RegisterModuleDependences('socialnetwork', 'onAfterCommentUpdateAfter', 'calendar', 'CCalendarLiveFeed', 'onAfterCommentUpdateAfter');
}
?>
