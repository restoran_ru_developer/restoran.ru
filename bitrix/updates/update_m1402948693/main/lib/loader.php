<?php
namespace Bitrix\Main;

final class Loader
{
	const SAFE_MODE = false;

	const BITRIX_HOLDER = "bitrix";
	const LOCAL_HOLDER = "local";

	private static $safeModeModules = array("main", "fileman");

	private static $arLoadedModules = array("main" => true);
	private static $arLoadedModulesHolders = array("main" => self::BITRIX_HOLDER);

	private static $arAutoLoadClasses = array();

	private static $isAutoLoadOn = true;

	public static function includeModule($moduleName)
	{
		if (!is_string($moduleName) || $moduleName == "")
			throw new LoaderException("Empty module name");
		if (preg_match("#[^a-zA-Z0-9._]#", $moduleName))
			throw new LoaderException(sprintf("Module name '%s' is not correct", $moduleName));

		$moduleName = strtolower($moduleName);

		if (self::SAFE_MODE)
		{
			if (!in_array($moduleName, self::$safeModeModules))
				return false;
		}

		if (array_key_exists($moduleName, self::$arLoadedModules))
			return self::$arLoadedModules[$moduleName];

		$arInstalledModules = ModuleManager::getInstalledModules();
		if (!array_key_exists($moduleName, $arInstalledModules))
			return self::$arLoadedModules[$moduleName] = false;

		$documentRoot = static::getDocumentRoot();

		$moduleHolder = self::LOCAL_HOLDER;
		$pathToModule = $documentRoot."/".$moduleHolder."/modules/".$moduleName;
		$pathToInclude = $pathToModule."/include_module.php";
		if (!file_exists($pathToInclude))
		{
			$pathToInclude = $pathToModule."/include.php";
			if (!file_exists($pathToInclude))
			{
				$moduleHolder = self::BITRIX_HOLDER;
				$pathToModule = $documentRoot."/".$moduleHolder."/modules/".$moduleName;
				$pathToInclude = $pathToModule."/include_module.php";
				if (!file_exists($pathToInclude))
				{
					$pathToInclude = $pathToModule."/include.php";
					if (!file_exists($pathToInclude))
						return self::$arLoadedModules[$moduleName] = false;
				}
			}
		}

		self::$arLoadedModulesHolders[$moduleName] = $moduleHolder;

		$res = self::includeModuleInternal($pathToInclude);
		if ($res === false)
			return self::$arLoadedModules[$moduleName] = false;

		if (strpos($moduleName, ".") !== false)
		{
			$moduleNameTmp = str_replace(".", "_", $moduleName);
			$className = "\\".$moduleNameTmp."\\".$moduleNameTmp;
		}
		else
		{
			$className = "\\Bitrix\\".$moduleName;
		}

		if (class_exists($className))
			return self::$arLoadedModules[$moduleName] = new $className();

		return self::$arLoadedModules[$moduleName] = true;
	}

	private static function includeModuleInternal($path)
	{
		/** @noinspection PhpUnusedLocalVariableInspection */
		global $DB, $MESS;
		return include_once($path);
	}

	public static function clearModuleCache($moduleName)
	{
		if (!is_string($moduleName) || $moduleName == "")
			throw new LoaderException("Empty module name");

		unset(static::$arLoadedModules[$moduleName]);
		unset(static::$arLoadedModulesHolders[$moduleName]);
	}

	public static function getDocumentRoot()
	{
		static $documentRoot = null;
		if ($documentRoot === null)
		{
			$documentRoot = $_SERVER["DOCUMENT_ROOT"];
			while (($ch = substr($documentRoot, -1)) && ($ch === "/" || $ch === "\\"))
				$documentRoot = substr($documentRoot, 0, -1);
		}
		return $documentRoot;
	}

	public static function switchAutoLoad($value = true)
	{
		static::$isAutoLoadOn = $value;
	}

	public static function registerAutoLoadClasses($moduleName, array $arClasses)
	{
		if (!is_array($arClasses))
			throw new LoaderException("Classes are not specified");

		if (empty($arClasses))
			return;

		if (!is_null($moduleName)
			&& (!is_string($moduleName) || empty($moduleName) || preg_match("#[^a-zA-Z0-9._]#", $moduleName)))
		{
			throw new LoaderException(sprintf("Module name '%s' is not correct", $moduleName));
		}

		if (!static::$isAutoLoadOn)
		{
			if (!array_key_exists($moduleName, static::$arLoadedModulesHolders))
				throw new LoaderException(sprintf("Holder of module '%s' is not found", $moduleName));

			$documentRoot = static::getDocumentRoot();

			if (!is_null($moduleName))
			{
				foreach ($arClasses as $value)
				{
					if (file_exists($documentRoot."/".self::$arLoadedModulesHolders[$moduleName]."/modules/".$moduleName."/".$value))
						require_once($documentRoot."/".self::$arLoadedModulesHolders[$moduleName]."/modules/".$moduleName."/".$value);
				}
			}
			else
			{
				foreach ($arClasses as $value)
				{
					if (($includePath = self::getLocal($value, $documentRoot)) !== false)
						require_once($includePath);
				}
			}
		}
		else
		{
			foreach ($arClasses as $key => $value)
			{
				self::$arAutoLoadClasses[strtolower($key)] = array(
					"module" => $moduleName,
					"file" => $value
				);
			}
		}
	}

	public static function isAutoLoadClassRegistered($className)
	{
		$className = trim($className);
		if ($className == '')
			return false;

		$className = strtolower($className);

		return array_key_exists($className, self::$arAutoLoadClasses);
	}

	/**
	 * \Bitrix\Main\IO\File -> /main/lib/io/file.php
	 * \Bitrix\IBlock\Type -> /iblock/lib/type.php
	 * \Bitrix\IBlock\Section\Type -> /iblock/lib/section/type.php
	 * \QSoft\Catalog\Tools\File -> /qsoft.catalog/lib/tools/file.php
	 *
	 * @param $className
	 */
	public static function autoLoad($className)
	{
		if (!is_string($className))
			return;

		$className = trim($className, '\\/');
		if (empty($className))
			return;

		if (preg_match("#[^\\\\/a-zA-Z0-9_]#", $className))
			return;

		$file = strtolower($className);

		$documentRoot = static::getDocumentRoot();

		if (array_key_exists($file, self::$arAutoLoadClasses))
		{
			$pathInfo = self::$arAutoLoadClasses[$file];
			if ($pathInfo["module"] != "")
			{
				$m = $pathInfo["module"];
				$h = isset(self::$arLoadedModulesHolders[$m]) ? self::$arLoadedModulesHolders[$m] : 'bitrix';
				if (file_exists($documentRoot."/".$h."/modules/".$m."/" .$pathInfo["file"]))
					require_once($documentRoot."/".$h."/modules/".$m."/" .$pathInfo["file"]);
			}
			else
			{
				if (($includePath = self::getLocal($pathInfo["file"], $documentRoot)) !== false)
					require_once($includePath);
			}
			return;
		}

		if (substr($file, -5) == "table")
			$file = substr($file, 0, -5);

		$file = str_replace('\\', '/', $file);
		$arFile = explode("/", $file);

		if ($arFile[0] === "bitrix")
		{
			array_shift($arFile);

			if (empty($arFile))
				return;

			$module = array_shift($arFile);
			if ($module == null || empty($arFile))
				return;
		}
		else
		{
			$module1 = array_shift($arFile);
			$module2 = array_shift($arFile);
			if ($module1 == null || $module2 == null || empty($arFile))
				return;

			$module = $module1.".".$module2;
		}

		if (!array_key_exists($module, self::$arLoadedModules) || self::$arLoadedModules[$module] == null)
			return;

		$filePath = $documentRoot."/".self::$arLoadedModulesHolders[$module]."/modules/".$module."/lib/".implode("/", $arFile).".php";
		if (file_exists($filePath))
			require_once($filePath);
	}

	/*private static function AutoLoadRecursive($basePath, $path, $filePath, &$fl)
	{
		if (($handle = opendir($basePath.$path)) && $fl)
		{
			while ((($dir = readdir($handle)) !== false) && $fl)
			{
				if ($dir == "." || $dir == ".." || !is_dir($basePath.$path."/".$dir))
					continue;

				$path2 = $path.'/'.$dir;
				if (file_exists($basePath.$path2.$filePath))
				{
					$fl = false;
					require_once($basePath.$path2.$filePath);
					break;
				}
				self::autoLoadRecursive($basePath, $path2, $filePath, $fl);
			}
			closedir($handle);
		}
	}*/

	/**
	 * Checks if file exists in /local or /bitrix directories
	 *
	 * @param string $path File path relative to /local/ or /bitrix/
	 * @param string $root Server document root, default static::getDocumentRoot()
	 * @return string|bool Returns combined path or false if the file does not exist in both dirs
	 */
	public static function getLocal($path, $root = null)
	{
		if ($root === null)
			$root = static::getDocumentRoot();

		if (file_exists($root."/local/".$path))
			return $root."/local/".$path;
		elseif (file_exists($root."/bitrix/".$path))
			return $root."/bitrix/".$path;
		else
			return false;
	}

	public static function getPersonal($path)
	{
		$root = static::getDocumentRoot();
		$personal = isset($_SERVER["BX_PERSONAL_ROOT"]) ? $_SERVER["BX_PERSONAL_ROOT"] : "";

		if (!empty($personal) && file_exists($root.$personal."/".$path))
			return $root.$personal."/".$path;

		return self::getLocal($path, $root);
	}
}

class LoaderException
	extends \Exception
{
	public function __construct($message = "", $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}

if (!function_exists("__autoload"))
{
	if (function_exists('spl_autoload_register'))
	{
		\spl_autoload_register('\Bitrix\Main\Loader::autoLoad');
	}
	else
	{
		function __autoload($className)
		{
			Loader::autoLoad($className);
		}
	}

	Loader::switchAutoLoad(true);
}
else
{
	Loader::switchAutoLoad(false);
}
