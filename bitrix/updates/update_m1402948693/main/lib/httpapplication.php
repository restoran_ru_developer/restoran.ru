<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2012 Bitrix
 */
namespace Bitrix\Main;

use Bitrix\Main\IO;
use Bitrix\Main\Security;
use Bitrix\Main\Web\Uri;

/**
 * Http application extends application. Contains http specific methods.
 */
class HttpApplication extends Application
{
	/**
	 * Creates new instance of http application.
	 */
	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 * Initializes context of the current request.
	 */
	protected function initializeContext(array $params)
	{
		$context = new HttpContext($this);

		//$context->setLanguage('en');
		//$context->setSite(SITE_ID);

		$server = new Server($params["server"]);

		$request = new HttpRequest(
			$server,
			$params["get"],
			$params["post"],
			$params["files"],
			$params["cookie"]
		);

		$response = new HttpResponse($context);

		$env = new Environment($params["env"]);

		$context->initialize($request, $response, $server, $env);

		$this->setContext($context);
	}

	/**
	 * Initializes default culture of the current request.
	 */
	protected function initializeCulture()
	{
		$this->context->setCulture(new Context\Culture());
	}

	protected function createExceptionHandlerOutput()
	{
		return new Diag\HttpExceptionHandlerOutput();
	}

	/**
	 * Starts request execution. Should be called after initialize.
	 */
	public function start()
	{
		register_shutdown_function(array($this, "finish"));
	}

	/**
	 * Finishes request execution.
	 * It is registered in start() and called automatically on script shutdown.
	 */
	public function finish()
	{
		$this->managedCache->finalize();
	}
}
