<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2012 Bitrix
 */
namespace Bitrix\Main;

use Bitrix\Main\Data;
use Bitrix\Main\IO;

/**
 * Base class for any application.
 */
abstract class Application
{
	/**
	 * @var Application
	 */
	protected static $instance = null;

	protected $isBasicKernelInitialized = false;
	protected $isExtendedKernelInitialized = false;

	/**
	 * Execution context.
	 *
	 * @var Context
	 */
	protected $context;

	/**
	 * Pool of database connections.
	 *
	 * @var Data\ConnectionPool
	 */
	protected $connectionPool;

	/**
	 * Managed cache instance.
	 *
	 * @var \Bitrix\Main\Data\ManagedCache
	 */
	protected $managedCache;

	/**
	 * LRU cache instance.
	 *
	 * @var \Bitrix\Main\Data\LruCache
	 */
	protected $lruCache;

	/**
	 * @var \Bitrix\Main\Diag\ExceptionHandler
	 */
	protected $exceptionHandler = null;

	/**
	 * @var Dispatcher
	 */
	private $dispatcher = null;

	/**
	 * Creates new application instance.
	 */
	protected function __construct()
	{

	}

	/**
	 * Returns current instance of the Application.
	 * Application should be previously created by createInstance()
	 *
	 * @return Application
	 * @throws SystemException
	 */
	public static function getInstance()
	{
		if (!isset(static::$instance))
			return null;

		return static::$instance;
	}

	/**
	 * Creates current instance of the Application.
	 *
	 * @return Application
	 * @throws SystemException
	 */
	public static function createInstance()
	{
		if (isset(static::$instance))
			throw new SystemException("Application is already created");

		self::$instance = new static();

		return static::$instance;
	}

	public function initializeBasicKernel()
	{
		if ($this->isBasicKernelInitialized)
			throw new SystemException("Basic kernel is already initialized");
		$this->isBasicKernelInitialized = true;

		$this->initializeExceptionHandler();
		$this->createDatabaseConnection();
	}

	public function initializeExtendedKernel(array $params)
	{
		if ($this->isExtendedKernelInitialized)
			throw new SystemException("Extended kernel is already initialized");
		$this->isExtendedKernelInitialized = true;

		$this->initializeContext($params);

		$this->initializeCulture();   // �������� ����� ����� ������������� ����������
		$this->initializeDispatcher();
	}

	/**
	 * Initializes application. Should be called before start() method.
	 */
	/*final public function initialize()
	{
		if ($this->isInitialized)
			throw new SystemException("Application is already initialized");
		$this->isInitialized = true;

		$arParams = func_get_args();
		$this->setInputParameters($arParams);

		$this->initializeKernel();
		$this->initializeShell();
	}*/

	/**
	 * Initializes application kernel.
	 * ��������� ����� ���������� ������ ��������� � ��������� ����������� ��������������
	 * ��������� ���� ���������, �� ������� ��� ��������� � �����.
	 */
	/*final protected function initializeKernel()
	{
		/ *** ������� �������������, ��� ������� ������ *** /

		$this->initializeExceptionHandler();

		$this->createDatabaseConnection();

		$this->initializeContext();

		$this->initializeCulture();   // �������� ����� ����� ������������� ����������

		$this->initializeBasicKernel();

		/ *** �������� � ���������� *** /

		$this->initializeDispatcher();

		/ *** ����������� ������������� *** /

		$this->initializeExtendedKernel();
	}*/

	final public function getDispatcher()
	{
		if (is_null($this->dispatcher))
			throw new NotSupportedException();
		if (!($this->dispatcher instanceof Dispatcher))
			throw new NotSupportedException();

		return clone $this->dispatcher;
	}

	/**
	 * Initializes context of the current request.
	 * Should be implemented in subclass.
	 */
	abstract protected function initializeContext(array $params);

	/**
	 * Initializes default culture of the current request.
	 * Should be implemented in subclass.
	 */
	abstract protected function initializeCulture();

	/**
	 * Starts request execution. Should be called after initialize.
	 * Should be implemented in subclass.
	 */
	abstract public function start();

	/**
	 * Exception handler can be initialized through the Config\Configuration (.settings.php file).
	 *
	 * 'exception_handling' => array(
	 *		'value' => array(
	 *			'debug' => true,        // output exception on screen
	 *			'handled_errors_types' => E_ALL & ~E_STRICT & ~E_NOTICE,    // catchable error types, printed to log
	 *			'exception_errors_types' => E_ALL & ~E_NOTICE & ~E_STRICT,  // error types from catchable which throws exceptions
	 *			'ignore_silence' => false,      // ignore @
	 *			'assertion_throws_exception' => true,       // assertion throws exception
	 *			'assertion_error_type' => 256,
	 *			'log' => array(
	 *              'class_name' => 'MyLog',        // custom log class, must extends ExceptionHandlerLog; can be omited, in this case default Diag\FileExceptionHandlerLog will be used
	 *              'extension' => 'MyLogExt',      // php extension, is used only with 'class_name'
	 *              'required_file' => 'modules/mylog.module/mylog.php'     // included file, is used only with 'class_name'
	 *				'settings' => array(        // any settings for 'class_name'
	 *					'file' => 'bitrix/modules/error.log',
	 *					'log_size' => 1000000,
	 *				),
	 *			),
	 *		),
	 *		'readonly' => false,
	 *	),
	 *
	 */
	protected function initializeExceptionHandler()
	{
		$exceptionHandler = new Diag\ExceptionHandler();

		$exceptionHandling = Config\Configuration::getValue("exception_handling");
		if ($exceptionHandling == null)
			$exceptionHandling = array();

		if (!isset($exceptionHandling["debug"]) || !is_bool($exceptionHandling["debug"]))
			$exceptionHandling["debug"] = false;
		$exceptionHandler->setDebugMode($exceptionHandling["debug"]);

		if (isset($exceptionHandling["handled_errors_types"]) && is_int($exceptionHandling["handled_errors_types"]))
			$exceptionHandler->setHandledErrorsTypes($exceptionHandling["handled_errors_types"]);

		if (isset($exceptionHandling["exception_errors_types"]) && is_int($exceptionHandling["exception_errors_types"]))
			$exceptionHandler->setExceptionErrorsTypes($exceptionHandling["exception_errors_types"]);

		if (isset($exceptionHandling["ignore_silence"]) && is_bool($exceptionHandling["ignore_silence"]))
			$exceptionHandler->setIgnoreSilence($exceptionHandling["ignore_silence"]);

		if (isset($exceptionHandling["assertion_throws_exception"]) && is_bool($exceptionHandling["assertion_throws_exception"]))
			$exceptionHandler->setAssertionThrowsException($exceptionHandling["assertion_throws_exception"]);

		if (isset($exceptionHandling["assertion_error_type"]) && is_int($exceptionHandling["assertion_error_type"]))
			$exceptionHandler->setAssertionErrorType($exceptionHandling["assertion_error_type"]);

		$exceptionHandlerOutput = $this->createExceptionHandlerOutput();
		$exceptionHandlerLog = $this->createExceptionHandlerLog(
			(isset($exceptionHandling["log"]) && is_array($exceptionHandling["log"])) ? $exceptionHandling["log"] : array()
		);

		$exceptionHandler->initialize($exceptionHandlerOutput, $exceptionHandlerLog);

		$this->exceptionHandler = $exceptionHandler;
	}

	protected function createExceptionHandlerLog(array $options)
	{
		$log = null;

		if (isset($options["class_name"]) && !empty($options["class_name"]))
		{
			if (isset($options["extension"]) && !empty($options["extension"]) && !extension_loaded($options["extension"]))
				throw new Config\ConfigurationException(sprintf("Extension '%s' is not loaded for exception handler log", $options["extension"]));

			if (isset($options["required_file"]) && !empty($options["required_file"]) && ($requiredFile = Loader::getLocal($options["required_file"])) !== false)
				require_once($requiredFile);

			$className = $options["class_name"];
			if (!class_exists($className))
				throw new Config\ConfigurationException(sprintf("Class '%s' does not exist for exception handler log", $className));

			$log = new $className();
		}
		elseif (isset($options["settings"]) && is_array($options["settings"]))
		{
			$log = new Diag\FileExceptionHandlerLog();
		}
		else
		{
			return null;
		}

		$log->initialize(
			isset($options["settings"]) && is_array($options["settings"]) ? $options["settings"] : array()
		);

		return $log;
	}

	protected function createExceptionHandlerOutput()
	{
		return new Diag\ExceptionHandlerOutput();
	}

	/**
	 * Creates database connection pool.
	 */
	protected function createDatabaseConnection()
	{
		$this->connectionPool = new Data\ConnectionPool();
	}

	final private function initializeDispatcher()
	{
		$dispatcher = new Dispatcher();
		$dispatcher->initialize();
		$this->dispatcher = $dispatcher;
	}

	/**
	 * @return \Bitrix\Main\Diag\ExceptionHandler
	 */
	public function getExceptionHandler()
	{
		return $this->exceptionHandler;
	}

	/**
	 * Returns database connections pool object.
	 *
	 * @return Data\ConnectionPool
	 */
	public function getConnectionPool()
	{
		return $this->connectionPool;
	}

	/**
	 * Returns context of the current request.
	 *
	 * @return Context
	 */
	public function getContext()
	{
		return $this->context;
	}

	/**
	 * Modifies context of the current request.
	 *
	 * @param Context $context
	 */
	public function setContext(Context $context)
	{
		$this->context = $context;
	}

	/**
	 * Static method returns database connection for the specified name.
	 * If name is empty - default connection is returned.
	 *
	 * @static
	 * @param string $name Name of database connection. If empty - default connection.
	 * @return DB\Connection
	 */
	public static function getConnection($name = "")
	{
		$pool = Application::getInstance()->getConnectionPool();
		return $pool->getConnection($name);
	}

	/**
	 * Returns new instance of the Cache object.
	 *
	 * @return Data\Cache
	 */
	public function getCache()
	{
		return Data\Cache::createInstance();
	}

	/**
	 * Returns manager of the managed cache.
	 *
	 * @return Data\ManagedCache
	 */
	public function getManagedCache()
	{
		if ($this->managedCache == null)
			$this->managedCache = new Data\ManagedCache();
		return $this->managedCache;
	}

	/**
	 * Returns true id server is in utf-8 mode. False - otherwise.
	 *
	 * @return bool
	 */
	public static function isUtfMode()
	{
		static $isUtfMode = null;
		if ($isUtfMode === null)
		{
			$isUtfMode = Config\Configuration::getValue("utf_mode");
			if ($isUtfMode === null)
				$isUtfMode = false;
		}
		return $isUtfMode;
	}

	/**
	 * Returns server document root.
	 *
	 * @return null|string
	 */
	public static function getDocumentRoot()
	{
		static $documentRoot = null;
		if ($documentRoot != null)
			return $documentRoot;

		$context = Application::getInstance()->getContext();
		if ($context != null)
		{
			$server = $context->getServer();
			if ($server != null)
				return $documentRoot = $server->getDocumentRoot();
		}

		return Loader::getDocumentRoot();
	}

	public static function getPersonalRoot()
	{
		static $personalRoot = null;
		if ($personalRoot != null)
			return $personalRoot;

		$context = Application::getInstance()->getContext();
		if ($context != null)
		{
			$server = $context->getServer();
			if ($server != null)
				return $personalRoot = $server->getPersonalRoot();
		}

		return isset($_SERVER["BX_PERSONAL_ROOT"]) ? $_SERVER["BX_PERSONAL_ROOT"] : "/bitrix";
	}

	/**
	 * Resets accelerator if any.
	 */
	public static function resetAccelerator()
	{
		$fl = Config\Configuration::getValue("no_accelerator_reset");
		if ($fl)
			return;

		if (function_exists("accelerator_reset"))
			accelerator_reset();
		elseif (function_exists("wincache_refresh_if_changed"))
			wincache_refresh_if_changed();
	}
}
