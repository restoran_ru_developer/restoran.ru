<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
	define("PUBLIC_AJAX_MODE", true);
	define("NO_KEEP_STATISTIC", "Y");
	define("NO_AGENT_STATISTIC","Y");
	define("NO_AGENT_CHECK", true);
	define("DisableEventsCheck", true);
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	CUtil::JSPostUnescape();
	$arParams = array(
		"PUSH&PULL" => array(),
		"RECORDS" => array(),
		"ENTITY_XML_ID" => $_REQUEST["ENTITY_XML_ID"],
	);
	if (check_bitrix_sessid() && $_REQUEST["MODE"] == "PUSH&PULL" &&
		$GLOBALS["USER"]->IsAuthorized() && !!$_REQUEST["ENTITY_XML_ID"] &&
		is_array($_SESSION["UC"]) && array_key_exists($_REQUEST["ENTITY_XML_ID"], $_SESSION["UC"]) &&
		!in_array($_REQUEST["ID"], $_SESSION["UC"][$_REQUEST["ENTITY_XML_ID"]]["RECORDS"]))
	{
		$res = $_REQUEST["DATA"]["messageFields"];
		$arParams["PUSH&PULL"] = array(
			"ACTION" => "REPLY",
			"ID" => $res["ID"]
		);
		$template = array("#RATING#" => "", "#UF#" => "");
		if (!!$res["RATING"]) {
			$arRating = array(
				"USER_VOTE" => 0,
				"USER_HAS_VOTED" => 'N',
				"TOTAL_VOTES" => 0,
				"TOTAL_POSITIVE_VOTES" => 0,
				"TOTAL_NEGATIVE_VOTES" => 0,
				"TOTAL_VALUE" => 0 );

			$arRatings = CRatings::GetRatingVoteResult($res["RATING"]["RATING_TYPE_ID"], $res["ID"]);

			if ($arRatings && $arRatings[$res["ID"]])
				$arRating = $arRatings[$res["ID"]];
			$arRatingParams = array_merge(Array(
				"ENTITY_TYPE_ID" => $res["RATING"]["RATING_TYPE_ID"],
				"ENTITY_ID" => $res["ID"],
				"OWNER_ID" => $res["AUTHOR"]["ID"],
				"PATH_TO_USER_PROFILE" => $arParams["~URL_TEMPLATES_PROFILE_VIEW"],
				"AJAX_MODE" => "Y" ), $arRating);
			ob_start();
			$GLOBALS["APPLICATION"]->IncludeComponent(
				"bitrix:rating.vote",
				$res["RATING"]["RATING_TYPE"],
				$arRatingParams,
				null,
				array("HIDE_ICONS" => "Y")
			);
			$template["#RATING#"] = ob_get_clean();
		}
		if (!!$res["UF"])
		{
			ob_start();
			foreach ($res["UF"] as $FIELD_NAME => $arPostField)
			{
				if(!empty($arPostField["VALUE"]))
				{
					$APPLICATION->IncludeComponent(
						"bitrix:system.field.view",
						$arPostField["USER_TYPE"]["USER_TYPE_ID"],
						array("arUserField" => $arPostField),
						null,
						array("HIDE_ICONS"=>"Y"));
				}
			}
			$template["#UF#"] = ob_get_clean();
		}
		$sanitizer = new CBXSanitizer();
		$sanitizer->SetLevel(CBXSanitizer::SECURE_LEVEL_LOW);

		$arParams["RECORDS"] = array($res["ID"] =>
			array(
				"ID" => intval($res["ID"]), // integer
				"ENTITY_XML_ID" => htmlspecialcharsbx($arParams["ENTITY_XML_ID"]), // string
				"FULL_ID" => array(htmlspecialcharsbx($arParams["ENTITY_XML_ID"]), htmlspecialcharsbx($res["ID"])),
				"NEW" => ($res["NEW"] == "Y" ? "Y" : "N"), //"Y" | "N"
				"APPROVED" => ($res["APPROVED"] == "Y" ? "Y" : "N"), //"Y" | "N"
				"POST_TIMESTAMP" => preg_replace("/[^\d]/is", "", $res["POST_TIMESTAMP"]),
				"POST_TIME" => htmlspecialcharsbx($res["POST_TIME"]),
				"POST_DATE" => htmlspecialcharsbx($res["POST_DATE"]),
				"~POST_MESSAGE_TEXT" => $res["~POST_MESSAGE_TEXT"],
				"POST_MESSAGE_TEXT" => $sanitizer->SanitizeHtml($res["POST_MESSAGE_TEXT"]),
				"PANELS" => array(
					"EDIT" => "N",
					"MODERATE" => "N",
					"DELETE" => "N"
				),
				"URL" => array(
					"LINK" => str_replace("&amp;", "&", htmlspecialcharsbx($res["URL"]["LINK"])),
					"EDIT" => str_replace("&amp;", "&", htmlspecialcharsbx($res["URL"]["EDIT"])),
					"MODERATE" => str_replace("&amp;", "&", htmlspecialcharsbx($res["URL"]["MODERATE"])),
					"DELETE" => str_replace("&amp;", "&", htmlspecialcharsbx($res["URL"]["DELETE"]))
				),
				"AUTHOR" => array(
					"ID" => intval($res["AUTHOR"]["ID"]),
					"NAME" => str_replace(array("<", ">"), array("&lt;", "&gt;"), $res["AUTHOR"]["NAME"]),
					"URL" => str_replace("&amp;", "&", htmlspecialcharsbx($res["AUTHOR"]["URL"])),
					"AVATAR" => str_replace("&amp;", "&", htmlspecialcharsbx($res["AUTHOR"]["AVATAR"]))),
				"BEFORE_HEADER" => $sanitizer->SanitizeHtml($res["BEFORE_HEADER"]),
				"BEFORE_ACTIONS" => $sanitizer->SanitizeHtml($res["BEFORE_ACTIONS"]),
				"AFTER_ACTIONS" => $sanitizer->SanitizeHtml($res["AFTER_ACTIONS"]),
				"AFTER_HEADER" => $sanitizer->SanitizeHtml($res["AFTER_HEADER"]),
				"BEFORE" => $sanitizer->SanitizeHtml($res["BEFORE"]),
				"AFTER" => $sanitizer->SanitizeHtml($res["AFTER"])
			)
		);
		if (!empty($template))
		{
			foreach(array("BEFORE_HEADER", "BEFORE_ACTIONS", "AFTER_ACTIONS", "AFTER_HEADER", "BEFORE", "AFTER") as $key)
			{
				$arParams["RECORDS"][$res["ID"]][$key] = str_replace(
					array_keys($template),
					array_values($template),
					$arParams["RECORDS"][$res["ID"]][$key]);
			}
		}
	}
}

if ($_REQUEST["AJAX_POST"] == "Y" &&
	$_REQUEST["ENTITY_XML_ID"] == $arParams["ENTITY_XML_ID"] &&
	in_array($_REQUEST["MODE"], array("LIST", "RECORD"))) {
	include_once(__DIR__."/html_parser.php");
	AddEventHandler('main.post.list', 'OnCommentsDisplayTemplate', __MPLParseRecordsHTML);
}
if (check_bitrix_sessid() &&
	$GLOBALS["USER"]->IsAuthorized() &&
	!!$_REQUEST["ENTITY_XML_ID"] && $_REQUEST["ENTITY_XML_ID"] == $arParams["ENTITY_XML_ID"] &&
	is_array($arParams["PUSH&PULL"]) && $arParams["PUSH&PULL"]["ACTION"] == "REPLY" &&
	CModule::IncludeModule("pull") && CPullOptions::GetNginxStatus())
{
	$_SESSION["UC"][$_REQUEST["ENTITY_XML_ID"]]["RECORDS"][] = $arParams["PUSH&PULL"]["ID"];
	$res = $arParams["RECORDS"][$arParams["PUSH&PULL"]["ID"]];

	if ($res["APPROVED"] != "Y")
	{
		$res = array(
			"ID" => $res["ID"],
			"URL" => $res["URL"],
			"ENTITY_XML_ID" => $res["ENTITY_XML_ID"], // string
		);
	}
	else
	{
		$res["PANELS"] = array( "EDIT" => "N", "MODERATE" => "N", "DELETE" => "N" );
	}
	CPullWatch::AddToStack('UNICOMMENTS'.$arParams["ENTITY_XML_ID"],
		Array(
			'module_id' => 'unicomments',
			'command' => 'comment',
			'params' => array_merge(
				$res,
				array("ACTION" => "REPLY")
			)
		)
	);
}

if ($GLOBALS["USER"]->IsAuthorized() && CModule::IncludeModule("pull") && CPullOptions::GetNginxStatus())
{
	CPullWatch::Add($GLOBALS["USER"]->GetId(), 'UNICOMMENTS'.$arParams["ENTITY_XML_ID"]);
?>
<script>
	BX.ready(function(){BX.PULL.extendWatch('UNICOMMENTS<?=$arParams["ENTITY_XML_ID"]?>');});
</script>
<?
}
?>