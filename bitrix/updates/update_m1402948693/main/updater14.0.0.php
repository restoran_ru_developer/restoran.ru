<?
$updater->CopyFiles("install/admin", "admin");
$updater->CopyFiles("install/components", "components");
$updater->CopyFiles("install/gadgets", "gadgets");
$updater->CopyFiles("install/js", "js");
$updater->CopyFiles("install/panel", "panel");
$updater->CopyFiles("install/tools", "tools");
$updater->CopyFiles("install/image_uploader", "image_uploader");

if($updater->CanUpdateDatabase())
{
	if($DB->IndexExists("b_user_option", array("CATEGORY", "NAME")))
	{
		$updater->Query(array(
			"Oracle" => "drop index ix_user_option_param",
			"Mysql" => "drop index ix_user_option_param on b_user_option",
		));
		if(strtolower($DB->type) == "mssql")
		{
			//it can be such names in MSSQL
			$DB->Query("drop index ix_user_option_param on b_user_option", true);
			$DB->Query("drop index IX_B_USER_OPTION on b_user_option", true);
		}
	}
	if($DB->IndexExists("b_user_option", array("USER_ID")))
	{
		$updater->Query(array(
			"Oracle" => "drop index ix_user_option_user",
			"Mysql" => "drop index ix_user_option_user on b_user_option",
			"MSSQL" => "drop index ix_user_option_user on b_user_option",
		));
	}
	if(!$DB->IndexExists("b_user_option", array("USER_ID", "CATEGORY")))
	{
		$updater->Query("create index IX_USER_OPTION_USER on b_user_option (USER_ID, CATEGORY)");
	}

	if(!defined("BX24_DB_NAME"))
	{
		//monitoring gadget
		$defaultDesktop = CUserOptions::GetOption('intranet', "~gadgets_admin_index", false, true);
		if (is_array($defaultDesktop) && is_array($defaultDesktop[0]) && is_array($defaultDesktop[0]["GADGETS"]))
		{
			foreach($defaultDesktop[0]["GADGETS"] as $id => $gadget)
			{
				if ($defaultDesktop[0]["GADGETS"][$id]["COLUMN"] == 0)
				{
					$defaultDesktop[0]["GADGETS"][$id]["ROW"]++;
				}
			}
			$defaultDesktop[0]["GADGETS"]["BITRIX24@99999999"] = array(
				"COLUMN" => 0,
				"ROW" => 0,
				"HIDE" => "N",
			);
			CUserOptions::SetOption('intranet', "~gadgets_admin_index", $defaultDesktop, true);
		}
	}

	//Convert to culture - one time operation. We hope for success.
	$res = $DB->Query("select 'x' from b_culture");
	if(!$res->Fetch())
	{
		$langList = $DB->Query("SELECT * from b_language");
		while(($arLang = $langList->Fetch()))
		{
			$id = $DB->Add("b_culture", array(
				"CODE" => $arLang["LID"],
				"NAME" => $arLang["NAME"],
				"FORMAT_DATE" => $arLang["FORMAT_DATE"],
				"FORMAT_DATETIME" => $arLang["FORMAT_DATETIME"],
				"FORMAT_NAME" => $arLang["FORMAT_NAME"],
				"WEEK_START" => $arLang["WEEK_START"],
				"CHARSET" => $arLang["CHARSET"],
				"DIRECTION" => $arLang["DIRECTION"],
			));
			$DB->Query("UPDATE b_language SET CULTURE_ID=".$id." WHERE LID='".$DB->ForSql($arLang["LID"])."'");
		}
	
		$siteList = $DB->Query("SELECT * from b_lang");
		while(($arSite = $siteList->Fetch()))
		{
			$res = $DB->Query("
				SELECT ID from b_culture where
					FORMAT_DATE = '".$DB->ForSql($arSite["FORMAT_DATE"])."'
					AND FORMAT_DATETIME = '".$DB->ForSql($arSite["FORMAT_DATETIME"])."'
					AND FORMAT_NAME = '".$DB->ForSql($arSite["FORMAT_NAME"])."'
					AND WEEK_START = '".$DB->ForSql($arSite["WEEK_START"])."'
					AND CHARSET = '".$DB->ForSql($arSite["CHARSET"])."'
			");
			if(($arRes = $res->Fetch()))
			{
				$id = $arRes["ID"];
			}
			else
			{
				$id = $DB->Add("b_culture", array(
					"CODE" => "site_".$arSite["LID"],
					"NAME" => $arSite["NAME"],
					"FORMAT_DATE" => $arSite["FORMAT_DATE"],
					"FORMAT_DATETIME" => $arSite["FORMAT_DATETIME"],
					"FORMAT_NAME" => $arSite["FORMAT_NAME"],
					"WEEK_START" => $arSite["WEEK_START"],
					"CHARSET" => $arSite["CHARSET"],
				));
			}
			$DB->Query("UPDATE b_lang SET CULTURE_ID=".$id." WHERE LID='".$DB->ForSql($arSite["LID"])."'");
		}
	}
}

if ($updater->CanUpdateKernel())
{
	$ar = array(
		"utf_mode" => array("value" => defined('BX_UTF'), "readonly" => true),
		"default_charset" => array("value" => defined('BX_DEFAULT_CHARSET'), "readonly" => false),
		"no_accelerator_reset" => array("value" => defined('BX_NO_ACCELERATOR_RESET'), "readonly" => false),
		"http_status" => array("value" => (defined('BX_HTTP_STATUS') && BX_HTTP_STATUS) ? true : false, "readonly" => false),
	);

	$cache = array();
	if (defined('BX_CACHE_SID'))
		$cache["sid"] = BX_CACHE_SID;
	if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/cluster/memcache.php"))
	{
		$arList = null;
		include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/cluster/memcache.php");
		if (defined("BX_MEMCACHE_CLUSTER") && is_array($arList))
		{
			foreach ($arList as $listKey => $listVal)
			{
				$bOtherGroup = defined("BX_CLUSTER_GROUP") && ($listVal["GROUP_ID"] !== BX_CLUSTER_GROUP);

				if (($listVal["STATUS"] !== "ONLINE") || $bOtherGroup)
					unset($arList[$listKey]);
			}

			if (count($arList) > 0)
			{
				$cache["type"] = array(
					"extension" => "memcache",
					"required_file" => "modules/cluster/classes/general/memcache_cache.php",
					"class_name" => "CPHPCacheMemcacheCluster",
				);
			}
		}
	}
	if (!isset($cache["type"]))
	{
		if (defined('BX_CACHE_TYPE'))
		{
			$cache["type"] = BX_CACHE_TYPE;

			switch ($cache["type"])
			{
				case "memcache":
				case "CPHPCacheMemcache":
					$cache["type"] = "memcache";
					break;
				case "eaccelerator":
				case "CPHPCacheEAccelerator":
					$cache["type"] = "eaccelerator";
					break;
				case "apc":
				case "CPHPCacheAPC":
					$cache["type"] = "apc";
					break;
				case "xcache":
				case "CPHPCacheXCache":
					$cache["type"] = array(
						"extension" => "xcache",
						"required_file" => "modules/main/classes/general/cache_xcache.php",
						"class_name" => "CPHPCacheXCache",
					);
					break;
				default:
					if (defined("BX_CACHE_CLASS_FILE") && file_exists(BX_CACHE_CLASS_FILE))
					{
						$cache["type"] = array(
							"required_remote_file" => BX_CACHE_CLASS_FILE,
							"class_name" => BX_CACHE_TYPE
						);
					}
					else
					{
						$cache["type"] = "files";
					}
					break;
			}
		}
		else
		{
			$cache["type"] = "files";
		}
	}
	if (defined("BX_MEMCACHE_PORT"))
		$cache["memcache"]["port"] = intval(BX_MEMCACHE_PORT);
	if (defined("BX_MEMCACHE_HOST"))
		$cache["memcache"]["host"] = BX_MEMCACHE_HOST;
	$ar["cache"] = array("value" => $cache, "readonly" => false);

	$cacheFlags = array();
	$arCacheConsts = array("CACHED_b_option" => "config_options", "CACHED_b_lang_domain" => "site_domain");
	foreach ($arCacheConsts as $const => $name)
		$cacheFlags[$name] = defined($const) ? constant($const) : 0;
	$ar["cache_flags"] = array("value" => $cacheFlags, "readonly" => false);

	$ar["cookies"] = array("value" => array("secure" => false, "http_only" => true), "readonly" => false);

	$ar["exception_handling"] = array(
		"value" => array(
			"debug" => false,
			"handled_errors_types" => E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR,
			"exception_errors_types" => E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR,
			"ignore_silence" => false,
			"assertion_throws_exception" => true,
			"assertion_error_type" => E_USER_ERROR,
			"log" => null,
		),
		"readonly" => false
	);

	global $DBType, $DBHost, $DBName, $DBLogin, $DBPassword;

	$DBType = strtolower($DBType);
	if ($DBType == 'mysql')
		$dbClassName = "\\Bitrix\\Main\\DB\\MysqlConnection";
	elseif ($DBType == 'mssql')
		$dbClassName = "\\Bitrix\\Main\\DB\\MssqlConnection";
	else
		$dbClassName = "\\Bitrix\\Main\\DB\\OracleConnection";

	$ar['connections']['value']['default'] = array(
		'className' => $dbClassName,
		'host' => $DBHost,
		'database' => $DBName,
		'login' => $DBLogin,
		'password' => $DBPassword,
		'options' =>  ((!defined("DBPersistent") || DBPersistent) ? 1 : 0) | ((defined("DELAY_DB_CONNECT") && DELAY_DB_CONNECT === true) ? 2 : 0)
	);
	$ar['connections']['readonly'] = true;

	$path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/.settings.php";
	if(!file_exists($path))
	{
		$data = var_export($ar, true);
		if (!is_writable($path))
			@chmod($path, 0644);
		file_put_contents($path, "<"."?php\n\rreturn ".$data.";\n");
	}

	$filename1 = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/after_connect.php";
	$filename2 = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/after_connect_d7.php";
	if (file_exists($filename1) && !file_exists($filename2))
	{
		$source = file_get_contents($filename1);
		$source = trim($source);
		$pos = 2;
		if (strtolower(substr($source, 0, 5)) == '<?php')
			$pos = 5;
		$source = substr($source, 0, $pos)."\n".'$connection = \Bitrix\Main\Application::getConnection();'.substr($source, $pos);
		$source = preg_replace("#\\\$DB->Query\(#i", "\$connection->queryExecute(", $source);
		file_put_contents($filename2, $source);
	}
}

if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/main/lib/db/dbconnection.php",
		"modules/main/lib/db/dbexception.php",
		"modules/main/lib/db/dbresult.php",
		"modules/main/lib/db/mssqldbconnection.php",
		"modules/main/lib/db/mssqldbresult.php",
		"modules/main/lib/db/mysqldbconnection.php",
		"modules/main/lib/db/mysqldbresult.php",
		"modules/main/lib/db/mysqlidbconnection.php",
		"modules/main/lib/db/mysqlidbresult.php",
		"modules/main/lib/db/oracledbconnection.php",
		"modules/main/lib/db/oracledbresult.php",
		"modules/main/lib/type/readonlydictionary.php",
		"modules/main/admin/file_checker.php",
		"modules/main/install/admin/file_checker.php",
		"admin/file_checker.php",
		"modules/main/lang/de/admin/file_checker.php",
		"modules/main/lang/en/admin/file_checker.php",
		"modules/main/lang/ru/admin/file_checker.php",
		"modules/main/lib/type/idictionaryfilter.php",
		"modules/main/install/components/bitrix/system.field.edit/templates/boolean/script.js",
		"components/bitrix/system.field.edit/templates/boolean/script.js",
		"modules/main/install/components/bitrix/system.field.edit/templates/crm_status/script.js",
		"components/bitrix/system.field.edit/templates/crm_status/script.js",
		"modules/main/install/components/bitrix/system.field.edit/templates/double/script.js",
		"components/bitrix/system.field.edit/templates/double/script.js",
		"modules/main/install/components/bitrix/system.field.edit/templates/file/script.js",
		"components/bitrix/system.field.edit/templates/file/script.js",
		"modules/main/install/components/bitrix/system.field.edit/templates/integer/script.js",
		"components/bitrix/system.field.edit/templates/integer/script.js",
		"modules/main/install/components/bitrix/system.field.edit/templates/string/script.js",
		"components/bitrix/system.field.edit/templates/string/script.js",
		"modules/main/install/components/bitrix/system.field.edit/templates/string_formatted/script.js",
		"components/bitrix/system.field.edit/templates/string_formatted/script.js",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
