<?
if(IsModuleInstalled('mobileapp'))
{
	$updater->CopyFiles("install/admin", "admin");
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");

	if($updater->CanUpdateDatabase())
	{
		RegisterModuleDependences("pull", "OnGetDependentModule", "mobileapp", "CMobileAppPullSchema", "OnGetDependentModule");
	}
}
if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/mobileapp/install/components/bitrix/mobileapp.list/templates/.default/images/gui.png",
		"components/bitrix/mobileapp.list/templates/.default/images/gui.png",
	);

	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>