<?
$MESS["PULL_TAB_SETTINGS"] = "Einstellungen";
$MESS["PULL_TAB_TITLE_SETTINGS"] = "Moduleinstellungen";
$MESS["PULL_OPTIONS_PATH_TO_LISTENER"] = "Pfad zum Nachrichten-Beobachter (HTTP)";
$MESS["PULL_OPTIONS_PATH_TO_LISTENER_SECURE"] = "Pfad zum Nachrichten-Beobachter (HTTPS)";
$MESS["PULL_OPTIONS_PATH_TO_MOBILE_LISTENER"] = "Pfad zum Lesen von Nachrichten in der Mobilen App (HTTP)";
$MESS["PULL_OPTIONS_PATH_TO_MOBILE_LISTENER_SECURE"] = "Pfad zum Lesen von Nachrichten in der Mobilen App (HTTPS)";
$MESS["PULL_OPTIONS_PATH_TO_WEBSOCKET"] = "Pfad zum Nachrichten-Beobachter via WebSocket (HTTP)";
$MESS["PULL_OPTIONS_PATH_TO_WEBSOCKET_SECURE"] = "Pfad zum Nachrichten-Beobachter via WebSocket (HTTPS)";
$MESS["PULL_OPTIONS_PATH_TO_PUBLISH"] = "Pfad zum Nachrichtensender";
$MESS["PULL_OPTIONS_PUSH"] = "PUSH-Benachrichtigungen an mobile Geräte senden";
$MESS["PULL_OPTIONS_WEBSOCKET"] = "WebSocket aktivieren";
$MESS["PULL_OPTIONS_NGINX"] = "nginx-push-stream-module ist installiert.";
$MESS["PULL_OPTIONS_NGINX_CONFIRM"] = "Achtung: Sie müssen das Modul nginx-push-stream-module installieren, bevor Sie diese Option nutzen werden.";
$MESS["PULL_OPTIONS_WS_CONFIRM"] = "Achtung: Sie müssen sicherstellen, dass das Modul nginx-push-stream-module WebSocket unterstützt, bevor Sie die Option nutzen werden.";
$MESS["PULL_OPTIONS_NGINX_DOC"] = "Näheres über Installation und Nutzung von nginx-push-stream-module finden Sie hier:";
$MESS["PULL_OPTIONS_NGINX_DOC_LINK"] = "Online-Hilfe";
$MESS["PULL_OPTIONS_STATUS"] = "Modulstatus";
$MESS["PULL_OPTIONS_STATUS_Y"] = "Aktiv";
$MESS["PULL_OPTIONS_STATUS_N"] = "Nicht aktiv";
$MESS["PULL_OPTIONS_USE"] = "Module genutzt";
$MESS["PULL_OPTIONS_SITES"] = "Das Modul nicht auf der Website nutzen";
?>