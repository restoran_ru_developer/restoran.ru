<?
if(IsModuleInstalled('pull'))
{
	//Following copy was parsed out from module installer
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/components", "components");
}

if($updater->CanUpdateDatabase())
{
	if($updater->TableExists("b_pull_push"))
	{
		if (!$DB->Query("select APP_ID from b_pull_push WHERE 1=0", true))
		{
			$updater->Query(array(
				"MySQL" => "alter table b_pull_push add column APP_ID varchar(50) null",
				"Oracle" => "alter table b_pull_push add APP_ID VARCHAR2(50 CHAR) NULL",
				"MSSQL" => "alter table b_pull_push add APP_ID varchar(50) NULL",
			));
		}

		if (!$DB->Query("select UNIQUE_HASH from b_pull_push WHERE 1=0", true))
		{
			$updater->Query(array(
				"MySQL" => "alter table b_pull_push add column UNIQUE_HASH varchar(50) null",
				"Oracle" => "alter table b_pull_push add UNIQUE_HASH VARCHAR2(50 CHAR) NULL",
				"MSSQL" => "alter table b_pull_push add UNIQUE_HASH varchar(50) NULL",
			));
		}

		if (!$DB->IndexExists("b_pull_push", Array("UNIQUE_HASH")))
		{
			$updater->Query("CREATE INDEX IX_PULL_PSH_UH ON b_pull_push (UNIQUE_HASH)");
		}
	}
	if($updater->TableExists("b_pull_push_queue"))
	{
		if (!$DB->Query("select APP_ID from b_pull_push_queue WHERE 1=0", true))
		{
			$updater->Query(array(
				"MySQL" => "alter table b_pull_push_queue add column APP_ID varchar(50) null",
				"Oracle" => "alter table b_pull_push_queue add APP_ID VARCHAR2(50 CHAR) NULL",
				"MSSQL" => "alter table b_pull_push_queue add APP_ID varchar(50) NULL",
			));
		}

		if (!$DB->IndexExists("b_pull_push_queue", Array("APP_ID")))
		{
			 $updater->Query("CREATE INDEX IX_PULL_PSHQ_AID ON b_pull_push_queue (APP_ID)");
		}
	}
}
?>
