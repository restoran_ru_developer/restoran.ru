<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (!$USER->IsAdmin())
{
	ShowError(GetMessage("BCLMME_ACCESS_DENIED"));
	return;
}

$arResult = array(
				"ACTION" => isset($_REQUEST["action"]) ? $_REQUEST["action"] : '',
				"DOMAIN" => isset($_REQUEST["domain"]) ? $_REQUEST["domain"] : '',
				"AJAX_PATH" => $componentPath."/ajax.php"
				);

if($arResult["DOMAIN"] === '')
{
	if(isset($arParams["LIST_URL"]))
	{
		LocalRedirect($arParams["LIST_URL"]);
	}
	else
	{
		echo GetMessage("BCLMME_NO_DATA");
		return;
	}
}

if (!CModule::IncludeModule('bitrixcloud'))
{
	ShowError(GetMessage("BCLMME_BC_NOT_INSTALLED"));
	return;
}

if (!CModule::IncludeModule('mobileapp'))
{
	ShowError(GetMessage("BCLMME_MA_NOT_INSTALLED"));
	return;
}

CJSCore::Init('ajax');
CUtil::InitJSCore(array("mobile_monitoring"));

$monitoring = CBitrixCloudMonitoring::getInstance();

if(isset($arResult["ACTION"]))
{
	switch ($arResult["ACTION"])
	{
		case 'add':
			$arResult["DOMAIN_PARAMS"] = array(
				"DOMAIN" => $arResult["DOMAIN"],
				"IS_HTTPS" => "N",
				"LANG" => LANGUAGE_ID,
				"EMAILS" => array(
					COption::GetOptionString("main", "email_from", ""),
					),
				"TESTS" => array(
					"test_lic",
					"test_domain_registration",
					"test_http_response_time",
					),
			);

			break;

		case 'edit':
			$arList = $monitoring->getList();

			if (is_string($arList))
			{
				ShowError($arList);
				return;
			}

			foreach ($arList as $arRes)
			{
				if ($arRes["DOMAIN"] === $arResult["DOMAIN"])
				{
					$arResult["DOMAIN_PARAMS"] = $arRes;
					break;
				}
			}

			break;

		case 'update':
			try
			{
				$result = $monitoring->startMonitoring(
					$arResult["DOMAIN"],
					$_REQUEST["IS_HTTPS"]==="Y",
					$_REQUEST["LANG"],
					$_REQUEST["EMAILS"],
					$_REQUEST["TESTS"]
				);

				if ($result != "")
				{
					ShowError($result);
					return;
				}

				LocalRedirect($arParams["LIST_URL"]);
			}
			catch (Exception $e)
			{
				ShowError($e->getMessage());
				return;
			}

			break;

		case 'delete':
			$strError = $monitoring->stopMonitoring($arResult["DOMAIN"]);

			if(strlen($strError) > 0)
			{
				ShowError($strError);
				return;
			}

			LocalRedirect($arParams["LIST_URL"]);
			break;
	}
}

$arResult["LANGS"] =  array();
$by = "sort";
$order = "asc";
$l = CLanguage::GetList($by, $order);
while(($l_arr = $l->Fetch()))
{
	$found = ($l_arr["LID"] == $sValue);
	$arResult["LANGS"][$l_arr["LID"]] = $l_arr["NAME"];
}
$this->IncludeComponentTemplate();
?>
