<?
if(IsModuleInstalled('bitrixcloud'))
{
	$updater->CopyFiles("install/admin", "admin");
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/gadgets", "gadgets");
	if ($updater->canUpdateDatabase() && CModule::IncludeModule('bitrixcloud'))
	{
		if (CBitrixCloudOption::getOption("monitoring_expire_time")->getStringValue() == 0)
		{
			CAgent::AddAgent('CBitrixCloudMonitoring::startMonitoringAgent();', 'bitrixcloud', 'N', 1);
		}

		RegisterModuleDependences("mobileapp", "OnBeforeAdminMobileMenuBuild", "bitrixcloud", "CBitrixCloudMobile", "OnBeforeAdminMobileMenuBuild");

		if(!defined("BX24_DB_NAME"))
		{
			//monitoring gadget
			$defaultDesktop = CUserOptions::GetOption('intranet', "~gadgets_admin_index", false, true);
			if (is_array($defaultDesktop) && is_array($defaultDesktop[0]) && is_array($defaultDesktop[0]["GADGETS"]))
			{
				foreach($defaultDesktop[0]["GADGETS"] as $id => $gadget)
				{
					if ($defaultDesktop[0]["GADGETS"][$id]["COLUMN"] == 0)
					{
						$defaultDesktop[0]["GADGETS"][$id]["ROW"]++;
					}
				}
				$defaultDesktop[0]["GADGETS"]["BITRIXCLOUD_MONITORING@19761009"] = array(
					"COLUMN" => 0,
					"ROW" => 0,
					"HIDE" => "N",
				);
				CUserOptions::SetOption('intranet', "~gadgets_admin_index", $defaultDesktop, true);
			}
		}
	}
}
?>