<?
$MESS["BLG_NS"] = "Nachrichten (Beiträge im Activity Stream)";
$MESS["BLG_NS_POST"] = "Nachricht an Sie gesendet";
$MESS["BLG_NS_COMMENT"] = "Kommentar zu Ihrem Beitrag gemacht";
$MESS["BLG_NS_MENTION"] = "Sie wurden erwähnt";
$MESS["BLG_NS_SHARE"] = "Zu Ihrem Beitrag wurden neue Empfänger hinzugefügt";
?>