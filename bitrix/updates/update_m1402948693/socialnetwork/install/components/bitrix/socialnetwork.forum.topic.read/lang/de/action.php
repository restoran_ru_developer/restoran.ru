<?
$MESS["SONET_FORUM_LOG_TEMPLATE"] = "#AUTHOR_NAME# hat eine Nachricht im Thema hinzugefügt #TITLE#.";
$MESS["SONET_FORUM_LOG_TEMPLATE_GUEST"] = "Autor: #TITLE#.";
$MESS["SONET_FORUM_LOG_TEMPLATE_AUTHOR"] = "Autor: [URL=#URL#]#TITLE#[/URL].";
$MESS["F_ERR_SESS_FINISH"] = "Ihre Session ist abgelaufen. Bitte wiederholen Sie die Aktion.";
$MESS["SONET_FORUM_LOG_TOPIC_TEMPLATE"] = "#AUTHOR_NAME# hat im Forum ein Thema erstellt: #TITLE#.";
$MESS["SONET_FORUM_ACTION_IM_COMMENT"] = "hat einen Kommentar zu Ihrem Thema \"#title#\" hinzugefügt";
?>