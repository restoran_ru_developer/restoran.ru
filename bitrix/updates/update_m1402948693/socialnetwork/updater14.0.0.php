<?
if(IsModuleInstalled('socialnetwork'))
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/gadgets", "gadgets");
	$updater->CopyFiles("install/js", "js");

	if($updater->CanUpdateDatabase())
	{
		UnRegisterModuleDependences('forum', 'OnAfterCommentAdd', 'calendar', 'CCalendarLiveFeed', 'onAfterCommentAdd');
		UnRegisterModuleDependences('forum', 'OnAfterCommentUpdate', 'calendar', 'CCalendarLiveFeed', 'OnAfterCommentUpdate');
		RegisterModuleDependences("forum", "OnAfterCommentAdd", "socialnetwork", "CSocNetForumComments", "onAfterCommentAdd");
		RegisterModuleDependences("forum", "OnAfterCommentUpdate", "socialnetwork", "CSocNetForumComments", "OnAfterCommentUpdate");
	}
}

if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/socialnetwork/install/js/socialnetwork/css/socialnetwork.css",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>