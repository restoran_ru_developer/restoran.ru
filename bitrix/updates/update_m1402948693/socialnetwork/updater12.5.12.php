<?
if(IsModuleInstalled('socialnetwork'))
{
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/gadgets", "gadgets");
}

if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/socialnetwork/classes/general/notification.php",
		"modules/socialnetwork/install/components/bitrix/socialnetwork.blog.post/templates/.default/result_modifier.php",
		"modules/socialnetwork/install/components/bitrix/socialnetwork.blog.post/templates/.default/script.php",
		"components/bitrix/socialnetwork.blog.post/templates/.default/result_modifier.php",
		"components/bitrix/socialnetwork.blog.post/templates/.default/script.php",
	);
	
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}

if($updater->CanUpdateDatabase())
{
	CAgent::RemoveAgent("CSocNetLog::ClearOldAgent();", "socialnetwork");

	$arFields = array(
		array(
			"USER_TYPE_ID" => "file",
			"ENTITY_ID" => "SONET_COMMENT",
			"FIELD_NAME" => "UF_SONET_COM_FILE",
			"XML_ID" => "UF_SONET_COM_FILE", 
			"MAX_ALLOWED_SIZE" => COption::GetOptionString("socialnetwork", "file_max_size", "5000000"),
			"MULTIPLE" => "Y",
			"MANDATORY" => "N",
			"SHOW_FILTER" => "N",
			"SHOW_IN_LIST" => "N",
			"EDIT_IN_LIST" => "Y",
			"IS_SEARCHABLE" => "Y",
		),
		array(
			"USER_TYPE_ID" => "file",
			"ENTITY_ID" => "SONET_LOG",
			"FIELD_NAME" => "UF_SONET_LOG_FILE",
			"XML_ID" => "UF_SONET_LOG_FILE", 
			"MAX_ALLOWED_SIZE" => COption::GetOptionString("socialnetwork", "file_max_size", "5000000"),
			"MULTIPLE" => "Y",
			"MANDATORY" => "N",
			"SHOW_FILTER" => "N",
			"SHOW_IN_LIST" => "N",
			"EDIT_IN_LIST" => "Y",
			"IS_SEARCHABLE" => "Y",
		),
	);

	if (IsModuleInstalled("webdav"))
	{
		$arFields[] = array(
			"USER_TYPE_ID" => "webdav_element",
			"ENTITY_ID" => "SONET_COMMENT",
			"FIELD_NAME" => "UF_SONET_COM_DOC",
			"XML_ID" => "UF_SONET_COM_DOC",
			"MULTIPLE" => "Y",
			"MANDATORY" => "N",
			"SHOW_FILTER" => "N",
			"SHOW_IN_LIST" => "N",
			"EDIT_IN_LIST" => "Y",
			"IS_SEARCHABLE" => "Y",
		);
		$arFields[] = array(
			"USER_TYPE_ID" => "webdav_element",
			"ENTITY_ID" => "SONET_LOG",
			"FIELD_NAME" => "UF_SONET_LOG_DOC",
			"XML_ID" => "UF_SONET_LOG_DOC",
			"MULTIPLE" => "Y",
			"MANDATORY" => "N",
			"SHOW_FILTER" => "N",
			"SHOW_IN_LIST" => "N",
			"EDIT_IN_LIST" => "Y",
			"IS_SEARCHABLE" => "Y",
		);
	}

	foreach ($arFields as $arField)
	{
		$rsData = CUserTypeEntity::GetList(array($by=>$order), $arField);
		if ($arRes = $rsData->Fetch())
			$intID = $arRes["ID"];
		else
		{
			$obUserField = new CUserTypeEntity;
			$obUserField->Add($arField, false);
		}
	}

	if ($updater->TableExists("b_sonet_log") || $updater->TableExists("B_SONET_LOG"))
	{
		if (
			!$DB->IndexExists("b_sonet_log", array("LOG_UPDATE"))
			&& !$DB->IndexExists("B_SONET_LOG", array("LOG_UPDATE"))
		)
			$updater->Query(array(
				"MySQL" => "CREATE INDEX IX_SONET_LOG_4 ON b_sonet_log(LOG_UPDATE)",
				"MSSQL" => "CREATE INDEX IX_SONET_LOG_4 ON B_SONET_LOG(LOG_UPDATE)",
				"Oracle" => "CREATE INDEX IX_SONET_LOG_4 ON B_SONET_LOG(LOG_UPDATE)",
			));

		if (
			($updater->TableExists("b_sonet_log_comment") || $updater->TableExists("B_SONET_LOG_COMMENT"))
			&& !$DB->IndexExists("b_sonet_log_comment", array("LOG_ID"))
			&& !$DB->IndexExists("B_SONET_LOG_COMMENT", array("LOG_ID"))
		)
			$updater->Query(array(
				"MySQL" => "CREATE INDEX IX_SONET_LOG_COMMENT_3 ON b_sonet_log_comment(LOG_ID)",
				"MSSQL" => "CREATE INDEX IX_SONET_LOG_COMMENT_3 ON B_SONET_LOG_COMMENT(LOG_ID)",
				"Oracle" => "CREATE INDEX IX_SONET_LOG_COMMENT_3 ON B_SONET_LOG_COMMENT(LOG_ID)",
			));

		if (!$updater->TableExists("b_sonet_subscription") && !$updater->TableExists("B_SONET_SUBSCRIPTION"))
		{
			$updater->Query(array(
				"MySQL" => "create table b_sonet_subscription
					(
						ID int(11) not null auto_increment,
						USER_ID int(11) not null,
						CODE varchar(50) not null,
						primary key (ID),
						unique IX_SONET_SUBSCRIPTION_1(USER_ID, CODE)
					)",
				"MSSQL" => "CREATE TABLE B_SONET_SUBSCRIPTION
					(
						ID int not null IDENTITY (1, 1),
						USER_ID int not null,
						CODE varchar(50) not null,
						CONSTRAINT PK_B_SONET_SUBSCRIPTION PRIMARY KEY (ID),
						CONSTRAINT IX_SONET_SUBSCRIPTION_1 UNIQUE (USER_ID, CODE)
					)",
				"Oracle" => "CREATE TABLE B_SONET_SUBSCRIPTION
					(
						ID int not null,
						USER_ID int not null,
						CODE varchar2(50 CHAR) not null,
						CONSTRAINT PK_B_SONET_SUBSCRIPTION PRIMARY KEY (ID),
						CONSTRAINT IX_SONET_SUBSCRIPTION_1 UNIQUE (USER_ID, CODE)
					)",
			));

			if ($updater->TableExists("B_SONET_SUBSCRIPTION"))
				$updater->Query(array(
					"MySQL" => "",
					"MSSQL" => "",
					"Oracle" => "CREATE SEQUENCE SQ_B_SONET_SUBSCRIPTION INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER",
				));
		}

		if ($updater->TableExists("b_sonet_subscription") || $updater->TableExists("B_SONET_SUBSCRIPTION"))
		{
			$updater->Query(array(
				"MySQL" => "INSERT IGNORE INTO b_sonet_subscription (USER_ID, CODE) 
					SELECT UG.USER_ID, concat('SG', UG.GROUP_ID)
					FROM b_sonet_user2group UG
					WHERE 
					(
						ROLE <= 'E'
						AND NOT EXISTS (
							SELECT ID 
							FROM b_sonet_log_events UE
							WHERE 
								UE.USER_ID = UG.USER_ID
								AND UE.ENTITY_TYPE = 'G'
								AND UE.ENTITY_ID = UG.GROUP_ID
								AND (UE.EVENT_ID = 'all')
								AND (UE.TRANSPORT = 'N')
						)
					)
					OR
					(
						ROLE = 'K'
						AND EXISTS (
							SELECT ID 
							FROM b_sonet_log_events UE
							WHERE 
								UE.USER_ID = UG.USER_ID
								AND UE.ENTITY_TYPE = 'G'
								AND UE.ENTITY_ID = UG.GROUP_ID
								AND (UE.EVENT_ID = 'all')
								AND (UE.TRANSPORT = 'X' OR UE.TRANSPORT = 'M')
						)
					)",
				"MSSQL" => "INSERT INTO B_SONET_SUBSCRIPTION (USER_ID, CODE) 
					SELECT UG.USER_ID, 'SG' + CAST(UG.GROUP_ID as varchar(17))
					FROM B_SONET_USER2GROUP UG
					WHERE 
					NOT EXISTS (SELECT * FROM B_SONET_SUBSCRIPTION SS WHERE SS.USER_ID = UG.USER_ID AND SS.CODE = 'SG' + CAST(UG.GROUP_ID as varchar(17)))
					AND (
						(
							ROLE <= 'E'
							AND NOT EXISTS (
								SELECT ID 
								FROM B_SONET_LOG_EVENTS UE
								WHERE 
									UE.USER_ID = UG.USER_ID
									AND UE.ENTITY_TYPE = 'G'
									AND UE.ENTITY_ID = UG.GROUP_ID
									AND (UE.EVENT_ID = 'all')
									AND (UE.TRANSPORT = 'N')
							)
						)
						OR
						(
							ROLE = 'K'
							AND EXISTS (
								SELECT ID 
								FROM B_SONET_LOG_EVENTS UE
								WHERE 
									UE.USER_ID = UG.USER_ID
									AND UE.ENTITY_TYPE = 'G'
									AND UE.ENTITY_ID = UG.GROUP_ID
									AND (UE.EVENT_ID = 'all')
									AND (UE.TRANSPORT = 'X' OR UE.TRANSPORT = 'M')
							)
						)
					)",
				"Oracle" => "INSERT INTO B_SONET_SUBSCRIPTION (ID, USER_ID, CODE) 
					SELECT SQ_B_SONET_SUBSCRIPTION.nextval, USER_ID, GROUP_CODE
					FROM (
						SELECT UG.USER_ID USER_ID, 'SG' || UG.GROUP_ID GROUP_CODE
						FROM B_SONET_USER2GROUP UG
						WHERE 
						NOT EXISTS (SELECT * FROM B_SONET_SUBSCRIPTION SS WHERE SS.USER_ID = UG.USER_ID AND SS.CODE = 'SG' || UG.GROUP_ID) 
						AND (
							(

								ROLE <= 'E'
								AND NOT EXISTS (
									SELECT ID 
									FROM B_SONET_LOG_EVENTS UE
									WHERE 
										UE.USER_ID = UG.USER_ID
										AND UE.ENTITY_TYPE = 'G'
										AND UE.ENTITY_ID = UG.GROUP_ID
										AND (UE.EVENT_ID = 'all')
										AND (UE.TRANSPORT = 'N')
								)
							)
							OR
							(
								ROLE = 'K'
								AND EXISTS (
									SELECT ID 
									FROM B_SONET_LOG_EVENTS UE
									WHERE 
										UE.USER_ID = UG.USER_ID
										AND UE.ENTITY_TYPE = 'G'
										AND UE.ENTITY_ID = UG.GROUP_ID
										AND (UE.EVENT_ID = 'all')
										AND (UE.TRANSPORT = 'X' OR UE.TRANSPORT = 'M')
								)
							)
						)
					)",
			));
		}

		if(
			$updater->TableExists("b_sonet_log_follow") 
			|| $updater->TableExists("B_SONET_LOG_FOLLOW")
		)
		{
			if(!$DB->Query("SELECT REF_ID FROM b_sonet_log_follow WHERE 1=0", true))
			{
				$updater->Query(array(
					"MySQL" => "ALTER TABLE b_sonet_log_follow ADD REF_ID INT(11) NOT NULL",
					"Oracle" => "ALTER TABLE B_SONET_LOG_FOLLOW ADD REF_ID NUMBER(18) DEFAULT 0 NOT NULL",
					"MSSQL" => "ALTER TABLE B_SONET_LOG_FOLLOW ADD REF_ID INT CONSTRAINT DF_B_SONET_LOG_FOLLOW_REF_ID DEFAULT 0 NOT NULL",
				));

				$updater->Query(array(
					"MySQL" => "UPDATE b_sonet_log_follow SET REF_ID = SUBSTR(CODE, 2) WHERE CODE LIKE 'L%'",
					"Oracle" => "UPDATE B_SONET_LOG_FOLLOW SET REF_ID = substr(CODE, 2) WHERE CODE LIKE 'L%'",
					"MSSQL" => "UPDATE B_SONET_LOG_FOLLOW SET REF_ID = SUBSTRING(CODE, 2, LEN(CODE)-1) WHERE CODE LIKE 'L%'",
				));

				$updater->Query(array(
					"MySQL" => "UPDATE b_sonet_log_follow SET REF_ID = 0 WHERE CODE = '**'",
					"Oracle" => "UPDATE B_SONET_LOG_FOLLOW SET REF_ID = 0 WHERE CODE = '**'",
					"MSSQL" => "UPDATE B_SONET_LOG_FOLLOW SET REF_ID = 0 WHERE CODE = '**'",
				));
			}

			if (!$DB->IndexExists("b_sonet_log_follow", array("USER_ID", "REF_ID")))
				$updater->Query(array(
					"MySQL" => "CREATE INDEX IX_SONET_FOLLOW_1 ON b_sonet_log_follow(USER_ID, REF_ID)",
					"MSSQL" => "CREATE INDEX IX_SONET_FOLLOW_1 ON B_SONET_LOG_FOLLOW(USER_ID, REF_ID)",
					"Oracle" => "CREATE INDEX IX_SONET_FOLLOW_1 ON B_SONET_LOG_FOLLOW(USER_ID, REF_ID)",
				));
		}
	}

}

if(IsModuleInstalled('socialnetwork') && $updater->CanUpdateDatabase())
{
	RegisterModuleDependences("main", "OnAfterRegisterModule", "main", "socialnetwork", "InstallUserFields", 100, "/modules/socialnetwork/install/index.php");
	RegisterModuleDependences("main", "OnAfterRegisterModule", "socialnetwork", "socialnetwork", "InstallUserFields");
}
?>