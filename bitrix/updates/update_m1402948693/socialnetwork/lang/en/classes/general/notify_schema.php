<?
$MESS["SONET_NS_INVITE_USER"] = "Friend invite";
$MESS["SONET_NS_INVITE_GROUP"] = "Workgroup membership request or invite";
$MESS["SONET_NS_INOUT_GROUP"] = "Joining or leaving a workgroup";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Assigned or unassigned as a workgroup moderator";
$MESS["SONET_NS_OWNER_GROUP"] = "Workgroup owner change";
$MESS["SONET_NS_FRIEND"] = "Listed or unlisted as a friend";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Workgroup updates";
?>