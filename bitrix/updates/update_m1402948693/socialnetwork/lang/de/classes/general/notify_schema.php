<?
$MESS["SONET_NS_INVITE_USER"] = "Einladung zu Freunden";
$MESS["SONET_NS_INVITE_GROUP"] = "Einladung oder Anfrage wegen Gruppenbeitritt";
$MESS["SONET_NS_INOUT_GROUP"] = "Zur Gruppe beitreten/Gruppe verlassen";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Zum Moderator ernannt/aus Moderatoren ausgeschlossen";
$MESS["SONET_NS_OWNER_GROUP"] = "Gruppenbesitzer ändern";
$MESS["SONET_NS_FRIEND"] = "Ist in der Freundesliste/ist nicht n der Freundesliste";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Aktualisierungen in der Arbeitsgruppe";
?>