<?
$MESS['UPDATER_IM_INDEX_1'] = 'The Instant Messenger module has been updated. For best performance, please execute the following queries: <b>CREATE INDEX IX_IM_MESS_4 ON b_im_message(CHAT_ID, NOTIFY_READ)</b>, <b>CREATE INDEX IX_IM_MESS_5 ON b_im_message(CHAT_ID, DATE_CREATE)</b> and <b>DROP INDEX IX_IM_MESS_1 ON b_im_message</b>';
?>