<?
$MESS['UPDATER_IM_INDEX_1'] = 'Das Modul Instant Messaging wurde aktualisiert. Für eine bessere Performance müssen folgende Anfragen ausgeführt werden: <b>CREATE INDEX IX_IM_MESS_4 ON b_im_message(CHAT_ID, NOTIFY_READ)</b>, <b>CREATE INDEX IX_IM_MESS_5 ON b_im_message(CHAT_ID, DATE_CREATE)</b>, <b>DROP INDEX IX_IM_MESS_1 ON b_im_message</b>';
?>