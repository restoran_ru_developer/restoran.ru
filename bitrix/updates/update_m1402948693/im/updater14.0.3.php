<?
if($updater->CanUpdateKernel())
{
	if (IsModuleInstalled('im'))
	{
		$updater->CopyFiles("install/js", "js");
		$updater->CopyFiles("install/components", "components");
		$updater->CopyFiles("install/activities", "activities");

		$arToDelete = array(
			"modules/im/install/js/im/images/im-sprite-v6.png",
			"modules/im/install/js/im/images/im-sprite-v7.png",
			"modules/im/install/js/im/images/notifier-message.gif",
			"modules/im/install/js/im/images/no-items.png",
		);
		foreach($arToDelete as $file)
			CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);

		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].$updater->curPath.'/install/public/', $_SERVER['DOCUMENT_ROOT'], true, true);
	}
}
if($updater->CanUpdateDatabase())
{
	if (CModule::IncludeModule('xmpp'))
	{
		$arMessage = array(
			"query" => array(
				"." => array("type" => "set"),
				"action" => array("#" => "die"),
			),
		);
		CXMPPUtility::SendToServer($arMessage);
	}

	$res = CUserTypeEntity::GetList(Array(), Array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_IM_SEARCH'));
	if (!$res->Fetch())
	{
		$arFields = array();
		$arFields['ENTITY_ID'] = 'USER';
		$arFields['FIELD_NAME'] = 'UF_IM_SEARCH';

		$rs = CUserTypeEntity::GetList(array(), array(
			"ENTITY_ID" => $arFields["ENTITY_ID"],
			"FIELD_NAME" => $arFields["FIELD_NAME"],
		));
		if(!$rs->Fetch())
		{
			$arMess['IM_UF_NAME_SEARCH'] = 'IM: users can find';

			$arFields['USER_TYPE_ID'] = 'string';
			$arFields['EDIT_IN_LIST'] = 'N';
			$arFields['SHOW_IN_LIST'] = 'N';
			$arFields['MULTIPLE'] = 'N';

			$arFields['EDIT_FORM_LABEL'][LANGUAGE_ID] = $arMess['IM_UF_NAME_SEARCH'];
			$arFields['LIST_COLUMN_LABEL'][LANGUAGE_ID] = $arMess['IM_UF_NAME_SEARCH'];
			$arFields['LIST_FILTER_LABEL'][LANGUAGE_ID] = $arMess['IM_UF_NAME_SEARCH'];

			$CUserTypeEntity = new CUserTypeEntity();
			$CUserTypeEntity->Add($arFields);
		}
	}
}

?>