<?
define("STOP_STATISTICS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$ELEMENT_ID = intval($_REQUEST["el_id"]);
$IBLOCK_ID = intval($_REQUEST["iblock_id"]);
$PROP_ID = intval($_REQUEST["prop_id"]);
$arFileUpload = $_FILES;
$fileKeyName = "Filedata";

$result = false;

if (check_bitrix_sessid())
{
	if (
		!empty($arFileUpload) && is_set($arFileUpload, "Filedata")
		&&
		$ELEMENT_ID > 0 && $IBLOCK_ID > 0 && $PROP_ID > 0
	)
	{
		if (CModule::IncludeModule('iblock'))
		{
			$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $ELEMENT_ID), false, false, array("ID"));

			if ($resElement = $rsElement->Fetch())
			{

				if (CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ELEMENT_ID, "element_edit"))
				{
					CFile::SaveForDB($arFileUpload, $fileKeyName, "iblock");

					if ($arFileUpload[$fileKeyName] > 0)
					{
						CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $arFileUpload[$fileKeyName], $PROP_ID);
						$result = true;
					}
				}
			}
		}
	}
}

echo $result===true ? "ok" : "upload failed";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>