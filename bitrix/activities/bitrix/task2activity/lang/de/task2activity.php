<?
$MESS["BPSNMA_EMPTY_TASKASSIGNEDTO"] = "Der Wert \"Verantwortliche Person\" ist nicht angegeben.";
$MESS["BPSNMA_EMPTY_TASKNAME"] = "Der Wert \"Aufgabenname\" ist nicht angegeben.";
$MESS["TASK_PRIORITY_0"] = "Niedrig";
$MESS["TASK_PRIORITY_1"] = "Normal";
$MESS["TASK_PRIORITY_2"] = "Hoch";
$MESS["TASK_EMPTY_GROUP"] = "Persönliche Aufgabe";
$MESS["BPSA_TRACK_OK"] = "Aufgabe mit der ID ##VAL# erstellt";
$MESS["BPSA_TRACK_ERROR"] = "Bei der Aufgabenerstellung ist ein Fehler aufgetreten.";
?>