<?
$MESS["BPTA1A_TASKCREATEDBY"] = "Aufgabe erstellen als";
$MESS["BPTA1A_TASKASSIGNEDTO"] = "Verantwortlich";
$MESS["BPTA1A_TASKACTIVEFROM"] = "Anfang";
$MESS["BPTA1A_TASKACTIVETO"] = "Ende";
$MESS["BPTA1A_TASKNAME"] = "Aufgabenname";
$MESS["BPTA1A_TASKDETAILTEXT"] = "Aufgabenbeschreibung";
$MESS["BPTA1A_TASKTRACKERS"] = "Beobachter";
$MESS["BPTA1A_TASKPRIORITY"] = "Priorität";
$MESS["BPTA1A_TASKFORUM"] = "Forum für Kommentare zu Aufgaben";
$MESS["BPTA1A_ADD_TO_REPORT"] = "Aufgabenergebnis kontrollieren";
$MESS["BPTA1A_CHECK_RESULT"] = "Aufgabe in den Leistungsbericht einschließen";
$MESS["BPTA1A_CHANGE_DEADLINE"] = "Verantwortliche Person kann Frist ändern";
$MESS["BPTA1A_TASKGROUPID"] = "Gruppe im Sozialen Netzwerk";
?>