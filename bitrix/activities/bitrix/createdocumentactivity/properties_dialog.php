<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?= $javascriptFunctions ?>
<?
$runtime = CBPRuntime::GetRuntime();
$documentService = $runtime->GetService("DocumentService");
foreach ($arDocumentFields as $fieldKey => $fieldValue)
{
	?>
	<tr>
		<td align="right" width="40%"><?= ($fieldValue["Required"]) ? "<span style=\"color:#FF0000\">*</span> " : "" ?><?= htmlspecialchars($fieldValue["Name"]) ?>:</td>
		<td width="60%" id="td_<?= htmlspecialchars($fieldKey) ?>">
			<?
			echo $documentService->GetFieldInputControl(
				$documentType,
				$fieldValue,
				array($formName, $fieldKey),
				$arCurrentValues[$fieldKey],
				true,
				false
			);
			?>
		</td>
	</tr>
	<?
}
?>