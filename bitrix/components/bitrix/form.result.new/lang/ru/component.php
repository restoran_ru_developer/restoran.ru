<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Добавить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br /><br />Ваша заявка №";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['FORM_PUBLIC_ICON_EDIT_TPL'] = "Редактировать шаблон веб-формы";
$MESS ['FORM_PUBLIC_ICON_EDIT'] = "Редактировать параметры веб-формы";
$MESS ['FORM_NOTE_ADDOK'] = "Спасибо!
$MESS ['FORM_NOTE_ADDOK_22'] = 'Служба бронирования столиков в ресторанах сайта www.restoran.ru работает с 10.00 до 22.00. Мы свяжемся с вами в рабочее время.';
Ваша заявка #RESULT_ID# принята!";
?>