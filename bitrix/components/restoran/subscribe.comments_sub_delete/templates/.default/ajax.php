<?
define("STOP_STATISTICS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent(
    'restoran:subscribe.comments_sub_delete',
    '',
    array(
        "AJAX_CALL" => "Y",
        "IBLOCK_ID_ARTICLE" => intval($_REQUEST["IBLOCK_ID_ARTICLE"]),
        "IBLOCK_ID_COMMENTS" => intval($_REQUEST["IBLOCK_ID_COMMENTS"]),
        "ARTICLE_SECTION_ID" => intval($_REQUEST["ARTICLE_SECTION_ID"]),
        "ARTICLE_ELEMENT_ID" => intval($_REQUEST["ARTICLE_ELEMENT_ID"]),
        "USER_ID" => intval($_REQUEST["USER_ID"]),
    ),
    null,
    array('HIDE_ICONS' => 'Y'));

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");
?>