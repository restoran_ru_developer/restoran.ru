function removeUserSubscribe(arParams) {
    function __handlerRemoveUserSubscribe(data) {
        var obContainer = document.getElementById("comm_sub_div");
        if (obContainer) {
            obContainer.innerHTML = data;
        }
    }

    $.ajax({
        type: 'POST',
        url: '/bitrix/components/restoran/subscribe.comments_sub_delete/templates/.default/ajax.php',
        data: arParams,
        success: __handlerRemoveUserSubscribe
    });
}
