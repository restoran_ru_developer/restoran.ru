<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["AJAX_CALL"] = $arParams["AJAX_CALL"] == "Y" ? "Y" : "N";

if(!$arParams["IBLOCK_ID_ARTICLE"] || !$arParams["IBLOCK_ID_COMMENTS"]) {
	ShowError("IBLOCK_ID IS NOT DEFINED");
	return false;
}

if(!$arParams["USER_ID"])
    $arParams["USER_ID"] = $USER->GetID();

if(!$arParams["ARTICLE_SECTION_ID"] && !$arParams["ARTICLE_ELEMENT_ID"]) {
    ShowError("ARTICLE ID IS NOT DEFINED");
    return false;
}

if($arParams["AJAX_CALL"] == "Y" && $arParams["USER_ID"] && ($arParams["ARTICLE_SECTION_ID"] || $arParams["ARTICLE_ELEMENT_ID"])) {
    if($arParams["ARTICLE_SECTION_ID"]) {
        // get article info, if section
        $rsArticleSec = CIBlockSection::GetList(
            Array("SORT" => "ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams["IBLOCK_ID_ARTICLE"],
                "ID" => $arParams["ARTICLE_SECTION_ID"]
            ),
            false,
            Array("ID", "UF_SECTION_BIND")
        );
        if($arArticleSec = $rsArticleSec->Fetch()) {
            // get bined users
            $rsCommSec = CIBlockSection::GetList(
                Array("SORT" => "ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_ID" => $arParams["IBLOCK_ID_COMMENTS"],
                    "ID" => $arArticleSec["UF_SECTION_BIND"]
                ),
                false,
                Array("ID", "UF_USER_SUBSCRIBED")
            );
            $arCommSec = $rsCommSec->Fetch();

            // clear 0 users
            $arSubUsers = remove_item_by_value($arCommSec["UF_USER_SUBSCRIBED"], 0);

            // set subscribe section id
            $subSecID = $arArticleSec["UF_SECTION_BIND"];
        }
    } elseif($arParams["ARTICLE_ELEMENT_ID"]) {
        // get article info, if element
        $rsArticleEl = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams["IBLOCK_ID_ARTICLE"],
                "ID" => $arParams["ARTICLE_ELEMENT_ID"]
            ),
            false,
            false,
            Array("ID", "PROPERTY_COMMENTS")
        );
        $arArticleEl = $rsArticleEl->Fetch();

        // get bined users
        $rsCommSec = CIBlockSection::GetList(
            Array("SORT" => "ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams["IBLOCK_ID_COMMENTS"],
                "ID" => $arArticleEl["PROPERTY_COMMENTS_VALUE"]
            ),
            false,
            Array("ID", "UF_USER_SUBSCRIBED")
        );
        $arCommSec = $rsCommSec->Fetch();

        // clear 0 users
        $arSubUsers = remove_item_by_value($arCommSec["UF_USER_SUBSCRIBED"], 0);

        // set subscribe section id
        $subSecID = $arArticleEl["PROPERTY_COMMENTS_VALUE"];
    }

    $bs = new CIBlockSection;

    // add cur user
    $arSubUsers = remove_item_by_value($arSubUsers, $arParams["USER_ID"]);

    // set comments subscribe
    $arFieldsSecUp = Array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID_COMMENTS"],
        "UF_USER_SUBSCRIBED" => $arSubUsers
    );
    $arResult["RESULT"] = $bs->Update($subSecID, $arFieldsSecUp);
}

$arTmpParams = array(
    "IBLOCK_ID_ARTICLE" => $arParams["IBLOCK_ID_ARTICLE"],
    "IBLOCK_ID_COMMENTS" => $arParams["IBLOCK_ID_COMMENTS"],
    "USER_ID" => $arParams["USER_ID"],
    "ARTICLE_SECTION_ID" => $arParams["ARTICLE_SECTION_ID"],
    "ARTICLE_ELEMENT_ID" => $arParams["ARTICLE_ELEMENT_ID"],
);

$arResult["JS_PARAMS"] = CUtil::PhpToJsObject($arTmpParams);

$this->IncludeComponentTemplate();

if ($arParams["AJAX_CALL"] != "Y") {
    IncludeAJAX();
    $template =& $this->GetTemplate();
    $APPLICATION->AddHeadScript($template->GetFolder().'/proceed.js');
}
?>