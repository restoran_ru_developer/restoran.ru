<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$rsIBlockArticle = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE_ARTICLE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlockArticle->Fetch())
    $arIBlockArticle[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];

$rsIBlockComm = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE_COMMENTS"], "ACTIVE"=>"Y"));
while($arr=$rsIBlockComm->Fetch())
    $arIBlockComm[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "IBLOCK_TYPE_ARTICLE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE_ARTICLE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID_ARTICLE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_ID_ARTICLE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlockArticle,
            "REFRESH" => "Y",
        ),
        "IBLOCK_TYPE_COMMENTS" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE_COMMENTS"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID_COMMENTS" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_ID_COMMENTS"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlockComm,
            "REFRESH" => "Y",
        ),
        "ARTICLE_SECTION_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTICLE_SECTION_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "ARTICLE_ELEMENT_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("ARTICLE_ELEMENT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "USER_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("USER_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
	),
);
?>