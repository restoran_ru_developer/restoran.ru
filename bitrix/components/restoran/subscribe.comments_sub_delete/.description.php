<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SUBSCRIBE_COMMENTS_DELETE_TITLE"),
	"DESCRIPTION" => GetMessage("SUBSCRIBE_COMMENTS_DELETE_DESC"),
	"ICON" => "/images/addform.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "subscribe",
	),
	"COMPLEX" => "N",
);

?>