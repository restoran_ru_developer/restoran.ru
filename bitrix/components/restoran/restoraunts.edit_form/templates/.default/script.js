$(document).ready(function() {
    // transform inputs
    $(".chzn-select").chosen();

    // on change name
    $('.inpwrap').on('keyup', '.rest_name', function() {
        var restCode = $(this).attr('code');
        $('#rest_tab_' + restCode).text($(this).val());
    });

    // add more inputs
    $('.add_more').on('click', function() {
        var propID = $(this).parent().prev().find('p.inpwrap:last > input').attr('id');
        $(this).parent().prev().find('p.inpwrap:last')
            .clone()
            .appendTo($(this).parent().prev())
            .before('<strong></strong>')
            .children('input').val('').attr({
                id: propID,
                name: propID + '[]'
            });
    });
	
	$(".del_pho").click(function(){
		$(this).parent("li").hide("fast", function(){
			$(this).remove();
		});
		return false;
	});
	
	//проверка формы
	function check_editor_form(a,f,o){
		var ret=true;
		o.dataType = "html";
		return ret;
	}
	
	$('.my_rest').ajaxForm({
		beforeSubmit: check_editor_form,
		success: function(data) {
			console.log(data);
			
			if (!$("#promo_modal").size()){
				$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
			}
			
            $('#promo_modal').html("<div align=\"right\"><a class=\"modal_close uppercase white\" href=\"javascript:void(0)\"></a></div>Изменения сохранены");
			showOverflow();
			setCenter($("#promo_modal"));
			$("#promo_modal").fadeIn("300");
			//window.location.reload();
			
			
			
		}
	});

    //подменю
    
    $(".add_new_rest_li").click(function(){
    	var lnk = $(this).attr("href");
    	
    	$.post(lnk, function(html){
    		 window.location.reload();
    	});
    	return false;
    });
    

});