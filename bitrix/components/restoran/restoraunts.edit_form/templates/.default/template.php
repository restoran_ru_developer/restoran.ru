<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//die("шаблон");
?>
<script type="text/javascript" src="<?=$templateFolder?>/jq_redactor/redactor.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/uploaderObject.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.form.js"></script>



<?/*<script src="http://api-maps.yandex.ru/0.8/index.xml?key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==" type="text/javascript"></script>*/?>

<div class="wrap-div">
    <div class="gray-block" style="display: none;">
        <p>Организация службы маркетинга деятельно экономит бюджет на размещение, осознав маркетинг как часть производства. Поведенческий таргетинг существенно притягивает conversion rate, опираясь на опыт западных коллег. Стратегия предоставления скидок и бонусов отталкивает жизненный цикл продукции, не считаясь
            с затратами. Пул лояльных изданий осмысленно притягивает сегмент рынка, осознав маркетинг как часть производства. Рекламоноситель усиливает фирменный потребительский рынок, расширяя долю рынка.
        </p>
        <div class="al-right">
            <form action="" method="post">
                Для добавления доставки еды, нажмите <input type="submit" class="submit-blue-plus" value="Добавить доставку" />
            </form>
        </div>
    </div>

    <div>
        <ul class="tabs" style="width: 100%;  margin-bottom: 28px !important;">
            <?foreach($arResult["RESTAURANTS"] as $rest):?>
            <li>
                <a href="<?=$rest["CODE"]?>" rel="<?=$rest["ID"]?>">
                    <div class="left tab_left"></div>
                    <div class="left name" id="rest_tab_<?=$rest["CODE"]?>"><?=$rest["NAME"]?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <?endforeach?>
        	
        	  <li class="add_new_rest_li">
                
            </li>
        </ul>
        
        <a href="<?=$templateFolder?>/core.php?act=add_new_rest" class="add_new_rest_li">+Добавить новый ресторан</a>
        
        <div class="panes">
            <?foreach($arResult["RESTAURANTS"] as $rest):?>
            	<?
            	$PHBL=0;
            	//запрашиваем id секции с меню
            	if($_REQUEST["PARENT_SECTION_ID"]==""){
            		$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>$rest["ID"]);
  					$db_list = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, true);
					if(!$ar_result = $db_list->GetNext()){
    					//если меню не создано, то создаем его
    					
    					$bs = new CIBlockSection;
						$arFields = Array(
  							"ACTIVE" => "Y",
  							"IBLOCK_SECTION_ID" => false,
  							"IBLOCK_ID" => 151,
  							"NAME" => $rest["NAME"],
  							'UF_RESTORAN'=>$rest["ID"]
  						);

  						$ID = $bs->Add($arFields);	
  					}            	
  				}
  				
  				
  				
  				?>
            <div class="pane" style="width: 100%;">
            	
            	<ul class="menu-horizontal-sub" >
					<li><a href="/content/personal/restaurateur/menu/<?=$rest["ID"]?>/" target="_blank">редактировать меню ресторана</a></li>
				</ul>
				
			
                <h2>Параметры ресторана</h2>
                <form class="my_rest" action="<?=$templateFolder?>/core.php" method="post" name="rest_edit" id="rest_<?=$rest["ID"]?>">
                    <?=bitrix_sessid_post()?>
                    
                    <input type="hidden" name="ELEMENT_ID" value="<?=$rest["ID"]?>" />
                    <input type="hidden" name="IBLOCK_ID" value="<?=$rest["IBLOCK_ID"]?>" />
                    <input type="hidden" name="act" value="edit_rest" />
                    <ul>
                        <li class="item-row">
                            <strong>Название:</strong>
                            <p class="inpwrap">
                                <input type="text" class="text rest_name" code="<?=$rest["CODE"]?>" name="NAME" value="<?=$rest["NAME"]?>" />
                            </p>
                        </li>
                        
                        <li class="item-row">
                            <strong>Синонимы:</strong>
                            <p class="inpwrap">
                            	<textarea class="text rest_tags" code="TAGS" name="TAGS"  ><?=$rest["TAGS"]?></textarea>
                            </p>
                        </li>
                        
                        <li class="item-row">
							<strong>Раздел:</strong>
							<p class="inpwrap">
							<select multiple data-placeholder="Выберите раздел" class="chzn-select" style="width:350px;" name="IBLOCK_SECTION_ID">
							<?foreach($arResult["SECTIONS"] as $keyRestType=>$restType):?>
								<option value="<?=$keyRestType?>"<? if($rest["IBLOCK_SECTION_ID"]==$keyRestType) echo  "selected='selected'"?>><?=$restType?></option>
							<?endforeach?>
							</select>
							</p>
						</li>
                        
                        
                         <li class="item-row">
                            <strong>Описание:</strong>
                            <p class="inpwrap" style="float:left;width:811px;">
                            	<textarea name="DETAIL_TEXT"  id="detail_text_<?=$rest["ID"]?>" style="height:200px;"><?=$rest["DETAIL_TEXT"]?></textarea>
                            </p>
                        </li>
                        
                        <?
                        //var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]]);
                        ?>
                        <li class="item-row">
                            <strong>Дополнительное описание:</strong>
                            <p class="inpwrap" style="float:left;width:811px;">
                            	<textarea name="PROP[add_props]"  id="PROP_add_props_<?=$rest["ID"]?>" style="height:200px;"><?=$rest["SELECTED_PROPERTIES"][$rest["ID"]]["add_props"]["VALUE"][0]["TEXT"]?></textarea>
                            </p>
                        </li>
                        
                        <script type="text/javascript">
                        	$("#detail_text_<?=$rest["ID"]?>").redactor({ path: '<?=$templateFolder?>/jq_redactor/', toolbar: 'mini',css: 'redactor.css', resize:false, lang: 'ru' });
                        	$("#PROP_add_props_<?=$rest["ID"]?>").redactor({ path: '<?=$templateFolder?>/jq_redactor/', toolbar: 'mini',css: 'redactor.css', resize:false, lang: 'ru' });
                        </script>
                        
                        <?foreach($rest["PROPERTIES"] as $keyProp=>$prop)://v_dump($rest["SELECTED_PROPERTIES"][$keyProp]);?>
							<?
							if(in_array(strtoupper($prop["CODE"]), $arParams["HIDDEN_PROPERTIES"])) continue;
							?>
                            <?switch($prop["PROPERTY_TYPE"]):
                                case "E":
                            ?>
                                <li class="item-row">
                                    <strong><?=$prop["NAME"]?>:</strong>
                                    <p class="inpwrap">
                                        <select multiple data-placeholder="Выберите вариант" class="chzn-select" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>">
                                        	<?foreach($prop["VALUE"] as $keyRestType=>$restType):?>
                                                <option value="<?=$keyRestType?>"<?if(in_array($keyRestType, $rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"])) echo  "selected='selected'"?>><?=$restType?></option>
                                            <?endforeach?>
                                        </select>
                                    </p>
                                </li>
                                <?
                                break;
                                case "L":
                                ?>
                                    <li class="item-row">
                                        <strong><?=$prop["NAME"]?>:</strong>
                                        <p>
                                            <?if($prop["LIST_TYPE"] == "C" && $prop["MULTIPLE"] == "N"):?>
                                                <span class="niceCheck">
                                                    <input type="checkbox" class="checkbox" name="PROP[<?=$prop["CODE"]?>][<?=$selPropKey?>]"<?if(in_array($prop["VALUE"], $rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"])) echo  "checked='checked'"?> value="<?=$prop["VALUE"]?>" /><br/>
                                                </span>
                                            <?endif?>
                                        </p>
                                    </li>
                                <?
                                break;
                                case "S":
                                ?>
                                    <li class="item-row"><?//v_dump($prop)?>
                                        <strong><?=$prop["NAME"]?>:</strong>
                                        <?
                                        $selPropCnt = 0;
                                        foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"] as $selPropKey=>$selPropVal):?>
                                            <?if($selPropCnt >= 1):?>
                                                <strong></strong>
                                            <?endif?>
                                            <p class="inpwrap">
                                                <input type="text" class="text" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" />
                                            </p>
                                        <?
                                        $selPropCnt++;
                                        endforeach?>
                                    </li>
                                    <?if($prop["MULTIPLE"] == "Y"):?>
                                        <li class="item-row">
                                            <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                                        </li>
                                    <?endif?>
                                <?
                                break;
                                case "F":
                                ?>
                                
                                <?
                                if($PHBL==0){
                           		    $APPLICATION->IncludeFile(
									$templateFolder."/photos.php",
									Array(
										"BLOCK_TITTLE"=>"Фотогалерея",
        	    						"BLOCK_VALUE" =>array($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]),
        	    						"DETAIL_PIC"=>$rest["PREVIEW_PICTURE"],
        	    						"BLOCK_NAME"=>"bl".time().$k,
        	    						"CODE"=>$prop["CODE"]
									),
									Array("MODE"=>"php")
        						);
        						}else{
        							if($PHBL==1){
        								$APPLICATION->IncludeFile(
										$templateFolder."/photos2.php",
										Array(
											"BLOCK_TITTLE"=>$prop["NAME"],
        	    							"BLOCK_VALUE" =>array($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]),
        	    							"BLOCK_NAME"=>"bl".time().$k,
        	    							"CODE"=>$prop["CODE"]
										),
										Array("MODE"=>"php")
        								);
        							}else{
        								
        								
        								$APPLICATION->IncludeFile(
										$templateFolder."/panorams.php",
										Array(
											"BLOCK_TITTLE"=>$prop["NAME"],
        	    							"BLOCK_VALUE" =>array($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]),
        	    							"BLOCK_NAME"=>"bl".time().$k,
        	    							"CODE"=>$prop["CODE"]
										),
										Array("MODE"=>"php")
        								);
        								//var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]);
        							}
        						}
        						$PHBL++;
        						
                                        $k++;
                                      ?>
								<?break;?>
                            <?endswitch?>
                        <?endforeach?>
                      	
                      	 <li class="item-row">
                            <div class="zg krt">Карта:</div>
                            <div class="inpwrap">
                           <?
                            //var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]]["map"]["VALUE"]);
                            $md = explode(",", $rest["SELECTED_PROPERTIES"][$rest["ID"]]["map"]["VALUE"][0]);
                            $mas=array(
                            	"yandex_lat"=>$md[1],
                            	"yandex_lon"=>$md[0],
                            	"yandex_scale"=>14,
                            	"PLACEMARKS"=>array(
                            		array(
                            			"LON"=>$md[0],
                            			"LAT"=>$md[1],
                            			"TEXT"=>""
                            		)
                            	)
                            );
                            
                            
                            
                            if($md[1]==""){
                            	if(CITY_ID=="spb") $DTS = COption::GetOptionString("main", "spb_center");
                            	else  $DTS = COption::GetOptionString("main", "msk_center");
                            	$DTS = explode(",", $DTS);
                            	
                            	unset($mas);
                         		$mas=array(
                            		"yandex_lat"=>$DTS[1],
                            		"yandex_lon"=>$DTS[0],
                            		"yandex_scale"=>13
                            	);
                         	}
                        	
                        	$mas = serialize($mas);
							?>
                          
                            <?$APPLICATION->IncludeComponent(
								"bitrix:map.yandex.view",
								"restoran_edit_or",
								Array(
									"KEY" => "ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==",
									"INIT_MAP_TYPE" => "MAP",
									"MAP_DATA" => $mas,
									"MAP_WIDTH" => "975",
									"MAP_HEIGHT" => "500",
									"CONTROLS" => array("ZOOM"),
									"OPTIONS" => array("ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING"),
									"MAP_ID" => "rest_".$rest["ID"],
								)
							);?>
							</div>           
						</li>
                      <?///die();
                      ?>
                        <li class="item-row">
                            <input type="submit" class="submit-blue" name="submit" value="сохранить" />
                        </li>
                    </ul>
                </form>
                
                	
                
                
                
            </div>
            <?endforeach?>
        </div>
    </div>
</div>