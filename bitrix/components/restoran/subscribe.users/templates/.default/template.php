<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if($arParams["AJAX_CALL"] != "Y"):?>
    <div id="user_sub_div">
<?endif?>
    <table class="friendlist">
        <?if (count($arResult["USER_FRIENDS"][0])>0):?>
            <?foreach($arResult["USER_FRIENDS"] as $cell=>$subFriends):?>
                <?if($subFriends["SUBSCRIBED"]):?>
                    <?if($cell%4 == 0):?>
                        <tr>
                    <?endif;?>
                        <td id="friend_result_<?=$subFriends["SECOND_USER_ID"]?>">
                            <div class="fleft">
                                <div class="avatar">
                                    <img src="<?=CFile::GetPath($subFriends["SECOND_USER_PERSONAL_PHOTO"])?>" width="90" />
                                </div>
                            </div>
                            <div class="fright">
                                <p><a href="/users/id<?=$friend["SECOND_USER_ID"]?>/"><?=$subFriends["SECOND_USER_NAME"]."&nbsp;"?><?=$subFriends["SECOND_USER_LAST_NAME"]?></a></p>
                                <input type="button" class="light_button" value="<?=GetMessage("UNSUB_BUTTON")?>" onclick="userSubAction('del', <?=$subFriends["SECOND_USER_ID"]?>, <?=htmlspecialchars($arResult["JS_PARAMS"])?>)" />
                                <br />
                            </div>
                        </td>
                    <?$cell++;
                    if($cell%4 == 0):?>
                            </tr>
                    <?endif?>
                <?endif;?>
            <?endforeach?>

            <?if($cell%4 != 0):?>
            <?while(($cell++)%4 != 0):?>
                <td>&nbsp;</td>
                <?endwhile;?>
            </tr>
            <?endif?>
        <?else:?>
            <tr><td><div class="errortext">
                <?if ($arParams["ID"]==$USER->GetID()):?>
                    <?=GetMessage("NO_YOU_FRIENDS")?>
                <?else:?>
                    <?=GetMessage("NO_FRIENDS")?>
                <?endif;?>
            </div></td></tr>
        <?endif;?>
    </table>
    <br />
    <!--<?=$subFriends["SECOND_USER_NAME"]."&nbsp;"?>
    <?if($subFriends["SUBSCRIBED"]):?>
        <input type="button" value="<?=GetMessage("UNSUB_BUTTON")?>" onclick="userSubAction('del', <?=$subFriends["SECOND_USER_ID"]?>, <?=htmlspecialchars($arResult["JS_PARAMS"])?>)" />
    <?else:?>
        <input type="button" value="<?=GetMessage("SUB_BUTTON")?>" onclick="userSubAction('add', <?=$subFriends["SECOND_USER_ID"]?>, <?=htmlspecialchars($arResult["JS_PARAMS"])?>)" />
    <?endif?>
    <h4><?=GetMessage("SUB_USERS")?></h4>
    <?foreach($arResult["SUB_USERS"] as $subUsers):?>
        <?=$subUsers["NAME"]."&nbsp;"?>
        <input type="button" value="<?=GetMessage("UNSUB_BUTTON")?>" onclick="userSubAction('del', <?=$subUsers["ID"]?>, <?=htmlspecialchars($arResult["JS_PARAMS"])?>)" />
        <br />
    <?endforeach?>-->

<?if($arParams["AJAX_CALL"] != "Y"):?>
    </div>
<?endif?>