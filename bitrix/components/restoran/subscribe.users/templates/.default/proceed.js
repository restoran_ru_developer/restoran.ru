function userSubAction(action, userID, arParams) {
    function __handlerUserSubAction(data) {
        var obContainer = document.getElementById("user_sub_div");
        if (obContainer) {
            obContainer.innerHTML = data;
        }
    }

    arParams.SUB_USER_ID = parseInt(userID);
    arParams.ACTION = action;

    $.ajax({
        type: 'POST',
        url: '/bitrix/components/restoran/subscribe.users/templates/.default/ajax.php',
        data: arParams,
        success: __handlerUserSubAction
    });
}
