<?
define("STOP_STATISTICS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent(
    'restoran:subscribe.users',
    '',
    array(
        "AJAX_CALL" => "Y",
        "USER_ID" => intval($_REQUEST["USER_ID"]),
        "SUB_USER_ID" => intval($_REQUEST["SUB_USER_ID"]),
        "ACTION" => trim($_REQUEST["ACTION"]),
    ),
    null,
    array('HIDE_ICONS' => 'Y'));

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");
?>