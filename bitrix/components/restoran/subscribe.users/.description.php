<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SUBSCRIBE_USERS_TITLE"),
	"DESCRIPTION" => GetMessage("SUBSCRIBE_USERS_DESC"),
	"ICON" => "/images/addform.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "subscribe",
	),
	"COMPLEX" => "N",
);

?>