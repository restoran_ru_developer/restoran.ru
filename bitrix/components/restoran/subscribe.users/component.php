<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("socialnetwork")) {
    ShowError(GetMessage("SOCIALNETWORK_MODULE_NOT_INSTALL"));
    return;
}

$arParams["AJAX_CALL"] = $arParams["AJAX_CALL"] == "Y" ? "Y" : "N";

if(!$arParams["USER_ID"])
    $arParams["USER_ID"] = $USER->GetID();

if($arParams["AJAX_CALL"] == "Y" && $arParams["SUB_USER_ID"] && $arParams["ACTION"]) {
    $user = new CUser;

    $rsUser = $user->GetByID($arParams["USER_ID"]);
    $arUser = $rsUser->Fetch();

    // get sub users
    switch($arParams["ACTION"]) {
        case 'add':
            $arUser["UF_USER_SUBSCRIBE"]["n0"] = $arParams["SUB_USER_ID"];
            $fields = Array(
                "UF_USER_SUBSCRIBE" => $arUser["UF_USER_SUBSCRIBE"]
            );
            $user->Update($arParams["USER_ID"], $fields);
        break;
        case 'del':
            $arUser["UF_USER_SUBSCRIBE"] = remove_item_by_value($arUser["UF_USER_SUBSCRIBE"], $arParams["SUB_USER_ID"]);
            $fields = Array(
                "UF_USER_SUBSCRIBE" => $arUser["UF_USER_SUBSCRIBE"]
            );
            $user->Update($arParams["USER_ID"], $fields);
            break;
    }
}

if($arParams["USER_ID"]) {
    $rsUser = CUser::GetByID($arParams["USER_ID"]);
    $arUser = $rsUser->Fetch();
    // get sub users
    foreach($arUser["UF_USER_SUBSCRIBE"] as $userSub) {
        // get user info
        $rsUserSub = CUser::GetByID($userSub);
        $arUserSub = $rsUserSub->Fetch();
        if(!CSocNetUserRelations::IsFriends($USER->GetID(), $userSub)) {
            $arResult["SUB_USERS"][] = $arUserSub;
         } else {
            // tmp storage user friends
            $arTmpUserFriends[] = $arUserSub["ID"];
        }
    }
    // get user friends
    $rsFriends = CSocNetUserRelations::GetRelatedUsers($arParams["USER_ID"], SONET_RELATIONS_FRIEND, Array());
    while($arFriends = $rsFriends->Fetch()) {
        // set sub stat
        if(in_array($arFriends["SECOND_USER_ID"], $arTmpUserFriends))
        {
            $arFriends["SUBSCRIBED"] = true;
            $arResult["USER_FRIENDS"][] = $arFriends;
        }
    }
}

$arTmpParams = array(
    "USER_ID" => $arParams["USER_ID"],
);

$arResult["JS_PARAMS"] = CUtil::PhpToJsObject($arTmpParams);

$this->IncludeComponentTemplate();

if ($arParams["AJAX_CALL"] != "Y") {
    IncludeAJAX();
    $template =& $this->GetTemplate();
    $APPLICATION->AddHeadScript($template->GetFolder().'/proceed.js');
}
?>