<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("socialnetwork")) {
    ShowError(GetMessage("SOCIALNETWORK_MODULE_NOT_INSTALL"));
    return;
}

$arParams["AJAX_CALL"] = $arParams["AJAX_CALL"] == "Y" ? "Y" : "N";
$arParams["RESULT_CONTAINER_ID"] = trim($arParams["RESULT_CONTAINER_ID"]);
$arParams["FIRST_USER_ID"] = intval($arParams["FIRST_USER_ID"]);
$arParams["SECOND_USER_ID"] = intval($arParams["SECOND_USER_ID"]);
$arParams["ACTION"] = trim($arParams["ACTION"]);

if(!$arParams["FIRST_USER_ID"] || !$arParams["SECOND_USER_ID"]) {
    ShowError(GetMessage("USER_ID_NOT_SETUP"));
    return;
}

$socRel = new CSocNetUserRelations;

if($arParams["ACTION"] == "delete_friend") {
    $arResult["REL_DEL"] = $socRel->DeleteRelation($arParams["FIRST_USER_ID"], $arParams["SECOND_USER_ID"]);
}

$arTmpParams = array(
    "RESULT_CONTAINER_ID" => $arParams["RESULT_CONTAINER_ID"],
    "FIRST_USER_ID" => $arParams["FIRST_USER_ID"],
    "SECOND_USER_ID" => $arParams["SECOND_USER_ID"],
);

$arResult["JS_PARAMS"] = CUtil::PhpToJsObject($arTmpParams);

$this->IncludeComponentTemplate();

if ($arParams["AJAX_CALL"] != "Y") {
    IncludeAJAX();
    $template =& $this->GetTemplate();
    $APPLICATION->AddHeadScript($template->GetFolder().'/proceed.js');
}
?>