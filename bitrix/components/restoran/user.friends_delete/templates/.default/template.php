<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arParams["AJAX_CALL"] != "Y"):?>
    <div id="<?=$arParams["RESULT_CONTAINER_ID"]?>">
        <input class="greyb" type="button" onclick="deleteUser(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" value="<?=GetMessage("USER_DELETE_FRIEND_BUTTON")?>" />
    </div>
<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["REL_DEL"]):?>
    <h1><?=GetMessage("USER_DELETE_FRIEND")?></h1>
<?endif?>