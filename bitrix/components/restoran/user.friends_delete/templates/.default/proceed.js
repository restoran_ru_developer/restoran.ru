function deleteUser(arParams) {
    res_cont_id = arParams.RESULT_CONTAINER_ID;
    first_user_id = arParams.FIRST_USER_ID;
    second_user_id = arParams.SECOND_USER_ID;

    function __handlerDeleteParams(data) {
        var obContainer = document.getElementById(res_cont_id);
        if (obContainer)
        {
            obContainer.innerHTML = data;
        }
    }

    $.ajax({
        type: 'POST',
        url: '/bitrix/components/restoran/user.friends_delete/templates/.default/ajax.php',
        data: arParams,
        success: __handlerDeleteParams
    });
}
