<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "FIRST_USER_ID" => array(
            "NAME" => GetMessage("FIRST_USER_ID"),
            "TYPE" => "STRING",
            "PARENT" => "BASE",
        ),
        "SECOND_USER_ID" => array(
            "NAME" => GetMessage("SECOND_USER_ID"),
            "TYPE" => "STRING",
            "PARENT" => "BASE",
        ),
        "RESULT_CONTAINER_ID" => array(
            "NAME" => GetMessage("RESULT_CONTAINER_ID"),
            "TYPE" => "STRING",
            "PARENT" => "BASE",
        ),
        "GREETING_MSG" => array(
            "NAME" => GetMessage("GREETING_MSG"),
            "DEFAULT" => "Давай дружить ?",
            "TYPE" => "STRING",
            "PARENT" => "BASE",
        ),
    ),
);
?>