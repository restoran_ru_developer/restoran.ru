<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i==0;?>
<?$page = $arResult["NAV_RESULT"]->PAGEN;
if (!$page)
    $page =1;
?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
<?/*if ($USER->IsAdmin()):
    v_dump($arItem["PROPERTIES"]["rating_date"]);
    v_dump($arItem["PROPERTIES"]["stat_day"]);
endif;*/
?>
    <?if ($key==0):?>
        <div class="left" style="width:370px;">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="370" /></a>
        </div>
        <div class="right" style="width:340px;">
            <div>
                <div class="position left" style="margin-top:5px"><?=($key+(($page-1)*19) + 1)?></div>
                <div class="rating">
                    <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                    <?for($p = 1; $p <= 5; $p++):?>
                        <div class="star<?if($p <= round($arItem["PROPERTIES"]["ratio"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                    <?endfor?>
                    <div class="clear"></div>
                </div> 
                <div class="clear"></div>
            </div>
            <div style="margin-top:5px;">                
                <?if ($arItem["PROPERTIES"]["phone"][0]):?>
                    <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                        <?if (CITY_ID=="spb"):?>
                             <p><b><?=GetMessage("FAV_PHONE")?></b>: <a href="tel:7401820" class="mobt <? echo MOBILE;?>">(812) 740-18-20</a>
                        <?else:?>
                             <p><b><?=GetMessage("FAV_PHONE")?></b>: <a href="tel:9882656" class="mobt <? echo MOBILE;?>">(495) 988-26-56</a>
                        <?endif;?>   
                    <?else:?>
                        <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["PROPERTIES"]["phone"][0]?></p>
                    <?endif;?>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["address"][0]):?>
                    <p>
                        <b><?=GetMessage("FAV_ADRESS")?></b>: 
                        <?$temp = explode(";",$arItem["PROPERTIES"]["address"][0]); echo $temp[0]?>
                    </p>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["subway"][0]):?>
                    <p class="metro_<?=CITY_ID?>"><?=$arItem["PROPERTIES"]["subway"][0]?></p>
                <?endif;?>
            </div>
            <div class="left">
                <i><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
            </div>
            <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
            <div class="clear"></div>
            <Br />
            <?
            $params = array();
            $params["id"] = $arItem["ID"];
            $params = addslashes(json_encode($params));
            $params2 = "";
            $params2 = "name=".rawurlencode($arItem["NAME"])."&id=".$arItem["ID"]."&".bitrix_sessid_get(); 
            ?>
            <div class="right">
                <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>
                <?if (CITY_ID!="tmn"&&CITY_ID!="ast"&&CITY_ID!="amt"&&CITY_ID!="vrn"&&CITY_ID!="tln"&&CITY_ID!="rga"&&CITY_ID!="urm"):?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="ajax_bron('<?=$params2?>')" class="button" value="<?=GetMessage("FAV_BRON")?>"/>
                <?endif;?>
            </div>
            <div class="clear"></div>   
            <br />
        </div>
        <div class="clear"></div>
        <div class="light_hr"></div>
        <?$i = 3;?>
    <?else:?>
        <div class="new_restoraunt left<?if($i%3 == 2):?> end<?endif?>">
            <div style="">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>            
            </div>
            <div style="height:80px;overflow:hidden">
                <div class="position left" style="margin-top:12px;"><?=($key+(($page-1)*19) + 1)?></div>
                <div class="rating">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>   
                    <?for($p = 1; $p <= 5; $p++):?>
                        <div class="small_star<?if($p <= round($arItem["PROPERTIES"]["ratio"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                    <?endfor?>
                    <div class="clear"></div>
                </div>        
            </div>
            <div style="height:120px;overflow:hidden;margin-top:5px;">            
                <?if ($arItem["PROPERTIES"]["phone"][0]):?>
                    <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                        <?if (CITY_ID=="spb"):?>
                             <p><b><?=GetMessage("FAV_PHONE")?></b>: <a href="tel:7401820" class="mobt <? echo MOBILE;?>">(812) 740-18-20</a>
                        <?else:?>
                             <p><b><?=GetMessage("FAV_PHONE")?></b>: <a href="tel:9882656" class="mobt <? echo MOBILE;?>">(495) 988-26-56</a>
                        <?endif;?>   
                    <?else:?>
                        <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["PROPERTIES"]["phone"][0]?></p>
                    <?endif;?>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["address"][0]):?>
                    <p>
                        <b><?=GetMessage("FAV_ADRESS")?></b>: 
                        <?$temp = explode(";",$arItem["PROPERTIES"]["address"][0]); echo $temp[0]?>
                    </p>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["subway"][0]):?>
                    <p class="metro_<?=CITY_ID?>"><?=$arItem["PROPERTIES"]["subway"][0]?></p>
                <?endif;?>
            </div>
            <div class="left">
                <i><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
            </div>
            <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
            <div class="clear"></div>
        </div>
        <?if ($i%3 == 2):?>
                <div class="clear"></div>
        <?endif;?>
        <?if ($i%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
                <div class="light_hr"></div>
        <?endif;?>
        <?if ($i%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
                <div class="clear"></div>
        <?endif;?>
        <?$i++;?>
    <?endif;?>
<?endforeach;?>
<?if ($arResult["NAV_STRING"]&&$arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <div class="hr"></div>           
    <div class="right">            
            <?=$arResult["NAV_STRING"]?>            
    </div>
    <div class="clear"></div>
<?endif;?>