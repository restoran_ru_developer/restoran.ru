<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;
    //$arParams["CACHE_TIME"] = 1;

$_REQUEST["ID"] = intval(str_replace("#", "", $_REQUEST["ID"]));
if($_REQUEST["ID"]<1) die("<error>не указан ресторан</error>");

CModule::IncludeModule("iblock");

    $arResult = array();

	$res = CIBlockElement::GetByID($_REQUEST["ID"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arFields["PROPERTIES"] = $ob->GetProperties();

		//echo '<pre>';
		//var_dump($arFields);
		//echo '</pre>';

		if($arFields['PREVIEW_PICTURE']!=""){
			$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>137, 'height'=>90), BX_RESIZE_IMAGE_EXACT, true);
			$arFields['PREVIEW_PICTURE'] = "http://".$_SERVER["SERVER_NAME"].$file["src"];
		}


		////Очищаем адреса
		$arFields["PROPERTIES"]["address"]["VALUE"][0] = str_replace("Москва, ", "", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		$arFields["PROPERTIES"]["address"]["VALUE"][0] = str_replace("Санкт-Петербург, ", "", $arFields["PROPERTIES"]["address"]["VALUE"][0]);

		$tar = explode(";", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		if(count($tar)>1) $arFields["PROPERTIES"]["address"]["VALUE"][0]=$tar[0];

		// bill
        $res1 = array();
        $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"], array("sort" => "asc"), Array("CODE"=>"average_bill"));
        while($ar_props = $db_props->Fetch()) {
            $res2 = CIBlockElement::GetByID($ar_props["VALUE"]);
            if ($ar1 = $res2->Fetch())
                    $res1[] = $ar1["NAME"];
        }
        $arResult["bill"] = $res1[0];

        // type
        $res1 = array();
        $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"], array("sort" => "asc"), Array("CODE"=>"type"));
        while($ar_props = $db_props->Fetch()) {
            $res2 = CIBlockElement::GetByID($ar_props["VALUE"]);
            if ($ar1 = $res2->Fetch())
                    $res1[] = $ar1["NAME"];
        }
        $arResult["type"] = $res1[0];

        $arResult["id"] = $arFields["ID"];
        $arResult["name"] = $arFields["NAME"];
        $arResult["address"] = $arFields["PROPERTIES"]["address"]["VALUE"][0];
        $arResult["pic"] = $arFields["PREVIEW_PICTURE"];
        $arResult["phone"] = $arFields["PROPERTIES"]["phone"]["VALUE"][0];
        $arResult["pic"] = $arFields["PREVIEW_PICTURE"];
        $arResult["open"] = $arFields["PROPERTIES"]["opening_hours"]["VALUE"][0];
        $arResult["detail"] = $arFields["DETAIL_PAGE_URL"];

	}

    echo json_encode($arResult);
    exit;
?>