<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$resultImage = array();
$resultVideo = array();

$nameImage = array();
$typeImage = array();
$tmpnameImage = array();
$errorImage = array();
$sizeImage = array();

$nameVideo = array();
$typeVideo = array();
$tmpnameVideo = array();
$errorVideo = array();
$sizeVideo = array();
//var_dump($_FILES);
if(count($_FILES) == 0 && $_POST['del'] !== '1'){
    $APPLICATION->IncludeComponent(
            "demo:comments",
            "",
            Array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => "1",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "DELETE" => "N",
                    "AJAX" => "Y",
                    "SUBMIT_BUTTON" => "Y",
                    "PRODUCT_ID" => $_POST['elementid'],
                    "TEXT" => $_POST['textarea'],
            )
        );
}

if($_POST['del'] == '1'){
    $APPLICATION->IncludeComponent(
            "demo:comments",
            "",
            Array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => "1",
                    "CACHE_TYPE" => "A",
                    "VALUE" => $_POST['value'],
                    "VALUE_ID" => $_POST['valueid'],
                    "CACHE_TIME" => "3600",
                    "DELETE" => "Y",
                    "AJAX" => "Y",
                    "PRODUCT_ID" => $_POST['product_id'],
                    "FILE_TYPE" => $_POST['type']
            )
        );
}

if(count($_FILES) > 0){
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['image'.$i]['name'] as $value) {
                $nameImage[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['image'.$i]['type'] as $value) {
                $typeImage[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['image'.$i]['tmp_name'] as $value) {
                $tmpnameImage[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['image'.$i]['error'] as $value) {
                $errorImage[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['image'.$i]['size'] as $value) {
                $sizeImage[] = $value;
            }
        }
        $resultImage['name'] = $nameImage;
        $resultImage['type'] = $typeImage;
        $resultImage['tmp_name'] = $tmpnameImage;
        $resultImage['error'] = $errorImage;
        $resultImage['size'] = $sizeImage;
        //var_dump($resultImage);
        if($resultImage["name"][0] == ""){
            $resultImage = array();
        }
    }
    
if(count($_FILES) > 0){
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['video'.$i]['name'] as $value) {
                $nameVideo[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['video'.$i]['type'] as $value) {
                $typeVideo[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['video'.$i]['tmp_name'] as $value) {
                $tmpnameVideo[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['video'.$i]['error'] as $value) {
                $errorVideo[] = $value;
            }
        }
        for($i=0; $i<count($_FILES); $i++){
            foreach ($_FILES['video'.$i]['size'] as $value) {
                $sizeVideo[] = $value;
            }
        }
        $resultVideo['name'] = $nameVideo;
        $resultVideo['type'] = $typeVideo;
        $resultVideo['tmp_name'] = $tmpnameVideo;
        $resultVideo['error'] = $errorVideo;
        $resultVideo['size'] = $sizeVideo;
        //if($resultVideo["name"][0] !== ""){
            //$resultImage = array();
        //}
        //var_dump($resultVideo);
        $elementid = $_POST['elementid'];
        $_POST['textarea'] = strip_tags($_POST['textarea']);
        $APPLICATION->IncludeComponent(
            "demo:comments",
            "",
            Array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => "1",
                    "CACHE_TYPE" => "A",
                    "PHOTOS" => $resultImage,
                    "VIDEOS" => $resultVideo,
                    "CACHE_TIME" => "3600",
                    "AJAX" => "Y",
                    "ELEMENT_ID" => $elementid,
                    "TEXT" => $_POST['textarea']
            )
        );
    }
?>
