<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");

if($arParams['AJAX'] == "Y"){

if($arParams['SUBMIT_BUTTON'] == "Y" && $arParams['DELETE'] == "N"){
    $el = new CIBlockElement;
    if($arParams["TEXT"] !== ''){
        if($arParams["PRODUCT_ID"] == 0){
                        $arLoadProductArray = Array(
                            "IBLOCK_ID"      => 14,
                            "PROPERTY_VALUES"=> $PROP,
                            "NAME"           => "Элемент",
                            "ACTIVE"         => "Y",
                            "PREVIEW_TEXT"   => $arParams["TEXT"]
                        );
            $PRODUCT_ID = $el->Add($arLoadProductArray);
            $response['product_id'] = $PRODUCT_ID;
        } else {
            $arLoadProductArray = Array(
                    "IBLOCK_ID"      => 14,
                    "PREVIEW_TEXT"   => $arParams["TEXT"],
                );
            $el->Update($arParams['PRODUCT_ID'],$arLoadProductArray);
            $response['product_id'] = $arParams["PRODUCT_ID"];
        }      
    } else {
        $response['product_id'] = 0;
    }
    $response['type'] = 'submit';
    echo json_encode($response);
}

    if($arParams['DELETE'] == "Y"){
        
        if($arParams['FILE_TYPE'] == "image"){
            $arFile["MODULE_ID"] = "iblock";
            $arFile["del"] = "Y";
            $PROPERTY_VALUE["70"][$arParams['VALUE_ID']] = $arFile;  
            CIBlockElement::SetPropertyValueCode($arParams['PRODUCT_ID'], "image", Array ($arParams['VALUE_ID'] => Array("VALUE"=>$arFile) ) ); 
        }
        if($arParams['FILE_TYPE'] == "video"){
            $arFile["MODULE_ID"] = "iblock";
            $arFile["del"] = "Y";
            $PROPERTY_VALUE["71"][$arParams['VALUE_ID']] = $arFile;  
            CIBlockElement::SetPropertyValueCode($arParams['PRODUCT_ID'], "video", Array ($arParams['VALUE_ID'] => Array("VALUE"=>$arFile) ) ); 
        }
    }
    $el = new CIBlockElement;
    if(!empty($arParams['PHOTOS']) || !empty($arParams['VIDEOS'])){
        if(is_array($arParams["PHOTOS"]["name"])){
                $RET=array();
                foreach($arParams["PHOTOS"] as $N=>$V){
                        for($i=0;$i<count($V);$i++){
                                $RET[$i][$N]=$V[$i];
                        }
                }
                $PROP["image"] = $RET;
            } else {
                $PROP = array('image' => $arParams['PHOTOS']);
            }
            
            if(is_array($arParams["VIDEOS"]["name"])){
                $RET=array();
                foreach($arParams["VIDEOS"] as $N=>$V){
                        for($i=0;$i<count($V);$i++){
                                $RET[$i][$N]=$V[$i];
                        }
                }
                $PROP["video"] = $RET;
            } else {
                $PROP = array('video' => $arParams['VIDEOS']);
            }
            
            
            if($arParams['ELEMENT_ID'] == 0){
                $arLoadProductArray = Array(
                    "IBLOCK_ID"      => 14,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => "Элемент",
                    "ACTIVE"         => "Y",
                    "PREVIEW_TEXT"   => $arParams["TEXT"]
                );
                $PRODUCT_ID = $el->Add($arLoadProductArray);
                $response = array();
                $response['product_id'] = $PRODUCT_ID;
                $VALUES = array();
                $paths = array();
                if(!empty($arParams['PHOTOS']['name'])){
                    $response['type'] = 'image';
                } else {
                    $response['type'] = 'video';
                }
                $res = CIBlockElement::GetProperty(14, $PRODUCT_ID, array("sort" => "asc"), array("CODE"=>$response['type']));
                while ($ob = $res->GetNext())
                {
                    $VALUES[] = $ob['VALUE'];
                    $VALUESID[] = $ob['PROPERTY_VALUE_ID'];
                }
                $m=0;
                foreach ($VALUES as $value) {
                    if($value == null){
                        unset($VALUES[$m]);
                        unset($VALUESID[$m]);
                    }
                    $m++;
                }
                
                foreach ($VALUES as $value) {
                    $paths[] = CFile::GetPath($value);
                }
                
                $response['paths'] = $paths;
                if($response['type'] == 'image'){
                    $response['count'] = count($arParams["PHOTOS"]["name"]);
                } else {
                    $response['count'] = count($arParams["VIDEOS"]["name"]);
                    foreach ($VALUES as $value) {
                        $VALUESFIX[] = $value;
                    }
                    $VALUES = $VALUESFIX;
                    foreach ($VALUESID as $value) {
                        $VALUESIDFIX[] = $value;
                    }
                    $VALUESID = $VALUESIDFIX;
                }
                $response['values_id'] = $VALUESID;
                $response['values'] = $VALUES;
                echo json_encode($response);
            } else {
                if(!empty($PROP['image'])){
                    $res = CIBlockElement::SetPropertyValueCode($arParams['ELEMENT_ID'], "image", $PROP["image"]);
                }
                if(!empty($PROP['video'])){
                    $res = CIBlockElement::SetPropertyValueCode($arParams['ELEMENT_ID'], "video", $PROP["video"]);
                }
                $arLoadProductArray = Array(
                    "IBLOCK_ID"      => 14,
                    "PREVIEW_TEXT"   => $arParams["TEXT"],
                );
                $PRODUCT_ID = $el->Update($arParams['ELEMENT_ID'],$arLoadProductArray);
                
                $response = array();
                $VALUES = array();
                $paths = array();
                if(!empty($arParams['PHOTOS']['name'])){
                    $response['type'] = 'image';
                    $response['count'] = count($arParams["PHOTOS"]["name"]);
                } else {
                    $response['count'] = count($arParams["VIDEOS"]["name"]);
                    $response['type'] = 'video';
                    foreach ($VALUES as $value) {
                        $VALUESFIX[] = $value;
                    }
                    $VALUES = $VALUESFIX;
                    foreach ($VALUESID as $value) {
                        $VALUESIDFIX[] = $value;
                    }
                    $VALUESID = $VALUESIDFIX;
                }
                $res = CIBlockElement::GetProperty(14, $arParams['ELEMENT_ID'], array("sort" => "asc"), array("CODE"=>$response['type']));
                while ($ob = $res->GetNext())
                {
                    $VALUES[] = $ob['VALUE'];
                    $VALUESID[] = $ob['PROPERTY_VALUE_ID'];
                }
                $m=0;
                foreach ($VALUES as $value) {
                    if($value == null){
                        unset($VALUES[$m]);
                        unset($VALUESID[$m]);
                    }
                    $m++;
                }
                foreach ($VALUES as $value) {
                    $paths[] = CFile::GetPath($value);
                }
                
                $response['paths'] = $paths;
                $response['product_id'] = $arParams['ELEMENT_ID'];
                $response['values'] = $VALUES;
                $response['values_id'] = $VALUESID;
                echo json_encode($response);
            }
    }    
} else {
    $this->IncludeComponentTemplate($componentPage);
}

?>

