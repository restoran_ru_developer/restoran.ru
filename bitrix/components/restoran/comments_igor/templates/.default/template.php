<form name="attach" action="/bitrix/components/demo/comments/ajax.php" method="post" id="comment_form" enctype="multipart/form-data">
<div class="comment-form">
    <textarea rows="4" cols="50" name="textarea" class="comment_textarea" onkeypress="SendComment(event);"></textarea>
    <input type="hidden" name="elementid" id="elementid" value="0"/>
    <input type="submit" value="<?=GetMessage("COMMENT")?>">
</div>
<div class="dropdown">
    <a href="#" class="dropdown-trigger"><?=GetMessage("ATTACH")?></span></a>
    <div class="dropdown-values">
        <a href="#" class="dropdown-item image-input-value" value="image"><?=GetMessage("IMAGE")?></a>
        <a href="#" class="dropdown-item video-input-value" value="video"><?=GetMessage("VIDEO")?></a>
    </div>
</div>
</form>
<div id="img-container"></div>
<input type="file" name="image1[]" id="image-field1" number="0" onchange="formChanged();" class="file-input image-input"/>