function formChanged(){
    $("#comment_form").submit();   
    $('#image-field0').val('');
    $('#video-field0').val('');
    $('.dropdown-values').hide();
    $(".dropdown-blind").hide();
    
    //$("#image-field0").replaceWith($("#image-field0").clone(true));
    //$("#video-field0").replaceWith($("#video-field0").clone(true));
}

function SendComment(e) {
    e = e || window.event;
    
    //for chrome & safari
    if (e.ctrlKey) {
        if(e.keyCode == 10){
            $("#comment_form").submit();
        }
    };
    
    //for firefox
    if (e.keyCode == 13 && e.ctrlKey) {
        $("#comment_form").submit();
    };
};

$(document).ready(function() {
   $('.dropdown-trigger').live('click', function(e){
	$(this).parent().find('.dropdown-values').slideDown();
	$(this).addClass('dropdown-trigger-down');
	$(".dropdown-blind").show();
	return false;
    });
		 
    $(".dropdown-blind").live('click', function(e){
	$(this).hide();
	$('.dropdown-values').fadeOut();
	$('.dropdown-trigger').removeClass('dropdown-trigger-down');
        return false;
    });
		
    $(".dropdown-itemd").live('click', function(e){
        if($(this).attr('value') == "image"){
            $('#image-field0').click();
        }
        if($(this).attr('value') == "video"){
            $('#video-field0').click();
        }
        $('.dropdown-values').fadeOut();
	$('.dropdown-trigger').removeClass('dropdown-trigger-down');
	$(".dropdown-blind").hide();
    });
    
    $('.remove-link').live('click', function(e){
        $(this).parent().fadeOut(200);
        valueid = $(this).parent().attr('valueid');
        value = $(this).parent().attr('id');
        productid = $('#elementid').val();
        if($(this).parent().hasClass('image-block')){
            type = 'image';
        }
        if($(this).parent().hasClass('video-block')){
            type = 'video';
        }
        $.ajax({
                type: "POST",
                url: "/bitrix/components/demo/comments/ajax.php",
                data: { del:1 , product_id:productid, value:value, valueid:valueid, type:type },
                success: function(data) {
                    
                }
            });
        return false;
    });

   $('#comment_form').ajaxForm({
        success: function(data) {
            document.getElementById('image-input-container').innerHTML = document.getElementById('image-input-container').innerHTML; 
            document.getElementById('video-input-container').innerHTML = document.getElementById('video-input-container').innerHTML; 
            answer = $.parseJSON(data);
            $('#elementid').val(answer.product_id);
            if(answer.type == 'image'){
                for (var i = answer.paths.length-answer.count; i < answer.paths.length; i++) {
                    $('<div id="'+answer.values[i]+'" valueid="'+answer.values_id[i]+'" class="image-block">').appendTo($('#img-container'));
                    $('<a href="#" class="remove-link">').appendTo($('#'+answer.values[i]));
                    $('<img/ src="'+answer.paths[i]+'" width="100">').appendTo($('#'+answer.values[i]));
                }
            }
            if(answer.type == 'video'){
                for (var i = answer.paths.length-answer.count; i < answer.paths.length; i++) {
                    pathArray = answer.paths[i].split('/');
                    number = pathArray.length-1;
                    nameArray = pathArray[number].split('.');
                    name = nameArray[0];
                    $('<div id="'+answer.values[i]+'" valueid="'+answer.values_id[i]+'" class="video-block">').appendTo($('#img-container'));
                    $('<div class="name">'+name+'</div>').appendTo($('#'+answer.values[i]));
                    $('<a href="#" class="remove-link">').appendTo($('#'+answer.values[i]));
                }
            }
        }                
   });
    
    // Проверка поддержки File API в браузере
    if(window.FileReader == null) {
        $('<div id = "image-input-container"><input type="file" name="image0[]" id="image-field0" number="0" onchange="formChanged();" class="file-input image-input"/></div>').appendTo($(".image-input-value"));
        $('<div id = "video-input-container"><input type="file" name="video0[]" id="video-field0" number="0" onchange="formChanged();" class="file-input video-input"/></div>').appendTo($(".video-input-value"));
    } else {
        //$(' <input type="file" name="image0[]" id="image-field0" number="0" multiple="true" onchange="formChanged();" class="file-input"/>').appendTo($("#comment_form"));
        //$(' <input type="file" name="video0[]" id="video-field0" number="0" multiple="true" onchange="formChanged();" class="file-input"/>').appendTo($("#comment_form"));
        //$('<div id = "image-input-container"><input type="file" name="image0[]" id="image-field0" number="0" onchange="formChanged();" class="file-input image-input"/></div>').appendTo($(".image-input-value"));
        $('<div id = "image-input-container"><input multiple="true" type="file" name="image0[]" id="image-field0" number="0" onchange="formChanged();" class="file-input image-input"/></div>').appendTo($(".image-input-value"));
        $('<div id = "video-input-container"><input multiple="true" type="file" name="video0[]" id="video-field0" number="0" onchange="formChanged();" class="file-input video-input"/></div>').appendTo($(".video-input-value"));
        
    }
    
});