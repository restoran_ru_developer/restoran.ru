<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600;
}

if($arParams["ITEMS_LIMIT"] < 1)
    $arParams["ITEMS_LIMIT"] = 4;

// get cuisine iblock info
$arCuisineIB = getArIblock("cuisine", CITY_ID);
// get restaurants iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
// get subway iblock info
$arSubwayIB = getArIblock("metro", CITY_ID);

$CACHE_ID = CITY_ID."|".SITE_ID."|".$APPLICATION->GetCurPage()."|".$USER->GetGroups()."|".$arParams["ITEMS_LIMIT"]."|".$_REQUEST["search_in"]."|".$_REQUEST["q"];

if($this->StartResultCache(false, $CACHE_ID)) {

	if(!CModule::IncludeModule("iblock")) {
		$this->AbortResultCache();
		ShowError("IBLOCK_MODULE_NOT_INSTALLED");
		return false;
	}	

	$arSort = array("SORT" => "ASC", "NAME" => "ASC", "ID" => "DESC");
    $arSelect = array("ID", "NAME");

    // get cuisine list
	$arFilterCuisine = array("IBLOCK_ID" => $arCuisineIB["ID"], "ACTIVE" => "Y");
    if($arCuisineIB["ID"]) {
        $rsCuisines = CIBlockElement::GetList($arSort, $arFilterCuisine, false, false, $arSelect);
        while($arCuisines = $rsCuisines->GetNext())
            $arResult["CUISINES"][] = $arCuisines;
    }

    // get average bill list
    $arFilterAvBill = array("IBLOCK_ID" => $arParams["AVERAGE_BILL_IB_ID"], "ACTIVE" => "Y");
    if($arParams["AVERAGE_BILL_IB_ID"]) {
        $rsAvBills = CIBlockElement::GetList($arSort, $arFilterAvBill, false, false, $arSelect);
        while($arAvBills = $rsAvBills->GetNext())
            $arResult["AVERAGE_BILL"][] = $arAvBills;
    }

    
    // get subway list
	$arFilterSubway = array("IBLOCK_ID" => $arSubwayIB["ID"], "ACTIVE" => "Y");
    if($arSubwayIB["ID"]) {
        $rsSubway = CIBlockElement::GetList($arSort, $arFilterSubway, false, false, $arSelect);
        while($arSubway = $rsSubway->GetNext())
            $arResult["SUBWAY"][] = $arSubway;
    }

    // get restaurants type list
    $arFilterRestType = array("IBLOCK_ID" => $arParams["REST_TYPE_IB_ID"], "ACTIVE" => "Y");
    if($arParams["REST_TYPE_IB_ID"]) {
        $rsRestType = CIBlockElement::GetList($arSort, $arFilterRestType, false, false, $arSelect);
        while($arRestType = $rsRestType->GetNext())
            $arResult["RESTAURANTS_TYPE"][] = $arRestType;
    }
    
    // get average bill list
    $arFilterZag = array("IBLOCK_ID" => $arParams["ZAG_IB_ID"], "ACTIVE" => "Y");
    if($arParams["ZAG_IB_ID"]) {
        $rsZag = CIBlockElement::GetList($arSort, $arFilterZag, false, false, $arSelect);
        while($arZag = $rsZag->GetNext())
            $arResult["ZAG"][] = $arZag;
    }
    
     // get average bill list
    $arFilterCredit = array("IBLOCK_ID" => $arParams["CREDIT_IB_ID"], "ACTIVE" => "Y");
    if($arParams["CREDIT_IB_ID"]) {
        $rsCredit = CIBlockElement::GetList($arSort, $arFilterCredit, false, false, $arSelect);
        while($arCredit = $rsCredit->GetNext())
            $arResult["CREDIT"][] = $arCredit;
    }
    
    // get average bill list
    $arFilterOsobennost = array("IBLOCK_ID" => $arParams["OSOBENNOST_IB_ID"], "ACTIVE" => "Y");
    if($arParams["OSOBENNOST_IB_ID"]) {
        $rsOsobennost = CIBlockElement::GetList($arSort, $arFilterOsobennost, false, false, $arSelect);
        while($arOsobennost = $rsOsobennost->GetNext())
            $arResult["OSOBENNOST"][] = $arOsobennost;
    }
    
    // get average bill list
    $arFilterParkovka = array("IBLOCK_ID" => $arParams["PARKOVKA_IB_ID"], "ACTIVE" => "Y");
    if($arParams["PARKOVKA_IB_ID"]) {
        $rsParkovka = CIBlockElement::GetList($arSort, $arFilterParkovka, false, false, $arSelect);
        while($arParkovka = $rsParkovka->GetNext())
            $arResult["PARKOVKA"][] = $arParkovka;
    }
    // get average bill list
    $arFilterEntertaiment = array("IBLOCK_ID" => $arParams["ENTARTAIMENT_IB_ID"], "ACTIVE" => "Y");
    if($arParams["ENTARTAIMENT_IB_ID"]) {
        $rsEntertaiment = CIBlockElement::GetList($arSort, $arFilterEntertaiment, false, false, $arSelect);
        while($arEntertaiment = $rsEntertaiment->GetNext())
            $arResult["ENTARTAIMENT"][] = $arEntertaiment;
    }
    // get average bill list
    $arFilterEntertaiment = array("IBLOCK_ID" => $arParams["MUSIC_IB_ID"], "ACTIVE" => "Y");
    if($arParams["MUSIC_IB_ID"]) {
        $rsEntertaiment = CIBlockElement::GetList($arSort, $arFilterEntertaiment, false, false, $arSelect);
        while($arEntertaiment = $rsEntertaiment->GetNext())
            $arResult["MUSIC"][] = $arEntertaiment;
    }
    // get average bill list
    $arFilterIdeal = array("IBLOCK_ID" => $arParams["IDEAL_IB_ID"], "ACTIVE" => "Y");
    if($arParams["IDEAL_IB_ID"]) {
        $rsIdeal = CIBlockElement::GetList($arSort, $arFilterIdeal, false, false, $arSelect);
        while($arIdeal = $rsIdeal->GetNext())
            $arResult["IDEAL"][] = $arIdeal;
    }
    //v_dump($arResult["RESTAURANTS_TYPE"]);

	$this->SetResultCacheKeys(array(
		"ID",
		"IBLOCK_ID",
		"NAME",
                "CUISINES",
                "AVERAGE_BILL",
                "SUBWAY",
                "RESTAURANTS_TYPE",
                "OSOBENNOST",
                "CREDIT",
                "ZAG",
	));

	$this->IncludeComponentTemplate();
}
?>