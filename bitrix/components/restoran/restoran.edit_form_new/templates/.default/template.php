<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<script src="<?//=$templateFolder?>/script.js"></script>-->
<?
$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;
?>
<script>
    var clicked = false;
    $(document).ready(function()
    {
        $("ul.tabss").each(function(){
            if ($(this).attr("ajax")=="ajax")
            {
                if ($(this).attr("history")=="true")
                    var history = true;
                else
                    var history = false;
                var url = "";
                if ($(this).attr("ajax_url"))
                    url = $(this).attr("ajax_url");
                $(this).tabs("div.panes > .pane", {
                    history: history,
                    onBeforeClick: function(event, i) {
                        var _this = this;
                        /*if (clicked)
                         {
                         if (confirm("Вы точно хотите уйти с этой вкладки? Не сохраненная информация может быть утеряна."))
                         {
                         var pane = _this.getPanes().eq(i);
                         if (pane.is(":empty")) {
                         pane.load(url+_this.getTabs().eq(i).attr("href"));

                         }
                         }
                         else
                         return false;
                         }
                         else*/
                        {
                            var pane = _this.getPanes().eq(i);
                            if (pane.is(":empty")) {
                                pane.load(url+_this.getTabs().eq(i).attr("href"));

                            }
                        }
                    }
                });
            }
        });
        $(".tabss a").click(function(){
            clicked = true;
        });
    });
</script>
<div class="wrap-div">
    <p class="font16 ops"><b><?=GetMessage('EDIT_FORM_NEW_attention_text');?></b></p>
    <div class="required_fields"><i class="star">*</i><?=GetMessage('EDIT_FORM_NEW_required_fields');?></div>
    <div>
        <ul class="tabss big" ajax="ajax" history="true" ajax_url="<?=$templateFolder?>/">
            <li>
                <a href="main.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>" class="current">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=GetMessage('EDIT_FORM_NEW_common');?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="contacts.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=GetMessage('EDIT_FORM_NEW_contacts');?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="props.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=GetMessage('EDIT_FORM_NEW_params');?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="photo.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=GetMessage('EDIT_FORM_NEW_photo');?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                <li>
                    <a href="type.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage('EDIT_FORM_NEW_restaurant_location');?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="baner.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage('EDIT_FORM_NEW_banners');?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            <?endif;?>
        </ul>
        <div class="panes">
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                <div class="pane big"></div>
                <div class="pane big"></div>
            <?endif;?>
        </div>
        <input type="hidden" id="aaa" id="tabss_l" value="0" />
        <a href="/restorator/rest_preview.php?ID=<?=$arParams["REST_ID"]?>&IBLOCK_ID=<?=$arResult["RESTAURANTS"][0]["IBLOCK_ID"]?>" class="light_button"><?=GetMessage('EDIT_FORM_NEW_preview');?></a>
    </div>
</div>
<script type="text/javascript" src="<?=$templateFolder?>/jq_redactor/redactor.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/uploaderObject.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.form.js"></script>