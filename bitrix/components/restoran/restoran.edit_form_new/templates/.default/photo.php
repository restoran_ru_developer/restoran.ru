<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$this_file_name = pathinfo(__FILE__);
require_once($BX_DOC_ROOT.'/bitrix/components/restoran/restoran.edit_form_new/templates/.default/lang/'.LANGUAGE_ID.'/'.$this_file_name['basename']);
?>
<script>
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $("#sortable").on("click",".del_pho",function(){
        if(confirm("<?=GetMessage('EDIT_FORM_NEW_photo_tab_delete_photo');?>")) {
            $(this).parents("li").remove();
            height_photo = $("#photo_form").parent().height();
        }
        return false;
    });
    var height_photo = $("#photo_form").parent().height();
    $(document).ready(function(){
        $(window).scroll(function(){
            if ($(this).scrollTop()<(height_photo-700))
            {
                $("#save_block_photo").css("position","fixed");
            }
            else
                $("#save_block_photo").css("position","relative");
        })
    });
</script>
<?
if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
    //$arParams["PROPERTIES"] = Array("PHOTOS","VIDEOPANORAMY");
    $arParams["PROPERTIES"] = Array("PHOTOS");

    CModule::IncludeModule("iblock");
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):
        foreach ($arParams["PROPERTIES"] as $PROP):
            $val = array();
            $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>$PROP));
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $val[] =  $ar_props["VALUE"];
                    $desc[] = $ar_props["DESCRIPTION"];

                }
                $property[$PROP] = $ar_props;
                unset($property[$PROP]["VALUE"]);
                unset($property[$PROP]["DESCRIPTION"]);
            }
            $property[$PROP]["VALUES"] = $val;
            $property[$PROP]["DESCRIPTION"] = $desc;
        endforeach;
        ?>
        <form id="photo_form" class="my_rest" action="/bitrix/components/restoran/restoran.edit_form_new/templates/.default/core.php" method="post" name="rest_edit">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_photos" />
            <input type="hidden" name="ACTIVE" value="<?=$arRests["ACTIVE"]?>" />
            <input type="hidden" name="NAME" value="<?=$arRests["NAME"]?>" />
            <ul style="padding-top:15px;">
                <?
                foreach($property as $keyProp=>$prop):
                    $code_up = strtoupper($prop["CODE"]);
                    switch($prop["PROPERTY_TYPE"]):
                        case "F":?>
                            <h2><?=$prop["NAME"]?></h2>
                            <ul class="upl-imgs bl<?=$code_up?>" id="sortable">
                                <? $i=0;?>
                                <? foreach($prop["VALUES"] as $key=>$VALUE){?>
                                    <? $file = CFile::ResizeImageGet($VALUE, array('width'=>212, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);  ?>
                                    <li class="ui-state-default">
                                        <a href="#" class="del_pho" title="Удалить фото"></a>
                                        <img src="<?=$file["src"]?>"><br />
                                        <input style="width:194px;" type="text" name="PROP[<?=$prop["CODE"]?>][descr][]" <? if($prop["DESCRIPTION"][$key]=="" || $prop["DESCRIPTION"][$key]=="Добавьте описание"){ $prop["DESCRIPTION"][$key]="";?> <?}?> placeholder="Добавьте описание" value="<?=$prop["DESCRIPTION"][$key]?>" />
                                        <input type="hidden" name="PROP[<?=$prop["CODE"]?>][fid_new][]" value="<?=$VALUE?>" />
                                    </li>
                                    <? $i++;?>
                                <?}?>
                                <div class="clear"></div>
                            </ul>
                            <Br /><h4><?=GetMessage('EDIT_FORM_NEW_photo_tab_add_photo');?></h4><br />
                            <div class="content_block ph_gals">
                                <div id="imu_content_<?=$code_up?>" class="imu_content">
                                    <div class="upl-description">
                                        <p align="center">
                                        <ul id="file_field_<?=$prop["CODE"]?>" class="unstyled" style="width:120px; margin:0 auto;"></ul>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <script>
                                $(document).ready(function(){
                                    var count = 0;
                                    var errorHandler = function(event, id, fileName, reason) {
                                        qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
                                    };
                                    $('#file_field_<?=$prop["CODE"]?>').fineUploader({
                                        text: {
                                            uploadButton: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_review');?>",
                                            cancelButton: "",
                                            waitingForResponse: ""
                                        },
                                        messages: {
                                            tooManyItemsError: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_too_much');?>",
                                            typeError: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_noneed_extantion');?>",
                                            sizeError: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_too_big');?>",
                                            minSizeError: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_too_small');?>",
                                            emptyError: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_empty');?>",
                                            noFilesError: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_didnt_selected');?>",
                                            //retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                                            onLeave: "<?=GetMessage('EDIT_FORM_NEW_photo_tab_be_upload');?>"
                                        },
                                        multiple: true,
                                        request: {
                                            endpoint: "/bitrix/components/restoran/restoran.edit_form_new/templates/.default/upload.php",
                                            params: {"generateError": true}
                                        },
                                        validation:{
                                            allowedExtensions : ["jpeg","jpg","bmp","gif","png"],
                                            itemLimit:<?=(CSite::InGroup(Array(1,15,16)))?"30":"30"?>
                                        }
                                        /*failedUploadTextDisplay: {
                                         mode: 'custom',
                                         maxChars: 5
                                         }*/
                                    })
                                        .on('error', errorHandler)
                                        .on('validate',function(){

                                        })
                                        .on('upload', function(id, fileName){
                                            $(".qq-upload-list").show();
                                            count++;
                                        })
                                        .on('complete', function(event, id, fileName, response) {
                                            $('<li class="ui-state-default"><a href="#" class="del_pho"  title="<?=GetMessage('EDIT_FORM_NEW_photo_tab_delete_photo_trigger_title');?>"></a><img width="212" src="'+response.resized+'"><br /> <input style="width:194px;" type="text" name="PROP[<?=$prop["CODE"]?>][descr][]" placeholder="Добавьте описание" /><div class="progress" rel="0"></div><input value="'+response.id+'" type="hidden" name="PROP[<?=$prop["CODE"]?>][fid_new][]" /></li>').insertBefore($(".bl<?=$code_up?> .clear"));
                                            $(".qq-upload-list").hide();
                                            height_photo = $("#photo_form").parent().height();
                                        });
                                });

                            </script>
                            <?break;
                    endswitch;
                endforeach;
                ?>
            </ul>
            <br />
            <div align="right" id="save_block_photo" class="save_block">
                <input type="submit" class="light_button" value="<?=GetMessage('EDIT_FORM_NEW_photo_tab_save_photo');?>" />
            </div>
        </form>

    <?
    endif;
}
?>	