<?
$MESS["EDIT_FORM_NEW_attention_text"] = "Внимание! Информация сохраняется отдельно в каждом блоке. Будьте внимательны!";
$MESS["EDIT_FORM_NEW_required_fields"] = " - поля, обязательные для заполнения.";
$MESS["EDIT_FORM_NEW_common"] = "Основые";
$MESS["EDIT_FORM_NEW_contacts"] = "Контакты";
$MESS["EDIT_FORM_NEW_params"] = "Параметры";
$MESS["EDIT_FORM_NEW_photo"] = "Фото";
$MESS["EDIT_FORM_NEW_restaurant_location"] = "Размещение ресторана";
$MESS["EDIT_FORM_NEW_banners"] = "Баннеры";
$MESS["EDIT_FORM_NEW_preview"] = "Предпросмотр";


?>