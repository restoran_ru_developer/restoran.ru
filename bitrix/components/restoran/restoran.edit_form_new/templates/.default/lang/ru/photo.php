<?
$MESS['EDIT_FORM_NEW_photo_tab_delete_photo'] = 'Удалить фотографию?';
$MESS['EDIT_FORM_NEW_photo_tab_add_photo'] = 'Добавить фотографии';
$MESS['EDIT_FORM_NEW_photo_tab_review'] = 'Обзор';
$MESS['EDIT_FORM_NEW_photo_tab_too_much'] = 'Слишком много файлов ({netItems}) загружается.  Превышен лимит в {itemLimit} файлов.';
$MESS['EDIT_FORM_NEW_photo_tab_noneed_extantion'] = 'Неверное расширение файла {file}. Вы можете загрузить только файлы с расширениями: {extensions}.';
$MESS['EDIT_FORM_NEW_photo_tab_too_big'] = 'Файл {file} слишком большой, максимальный размер загружаемого файла - {sizeLimit}.';
$MESS['EDIT_FORM_NEW_photo_tab_too_small'] = '{file} слишком маленький, минимальный размер файла {minSizeLimit}.';
$MESS['EDIT_FORM_NEW_photo_tab_empty'] = 'Файл {file} - пуст, выберите файлы заново, не включая его.';
$MESS['EDIT_FORM_NEW_photo_tab_didnt_selected'] = 'Не выбраны файлы для загрузки';
$MESS['EDIT_FORM_NEW_photo_tab_be_upload'] = 'Файлы загружаются, если вы закроете окно закачка будет отменена.';
$MESS['EDIT_FORM_NEW_photo_tab_delete_photo_trigger_title'] = 'Удалить фото';
$MESS['EDIT_FORM_NEW_photo_tab_save_photo'] = 'Сохранить фото';
?>