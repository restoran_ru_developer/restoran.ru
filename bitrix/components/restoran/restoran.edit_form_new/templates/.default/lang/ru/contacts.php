<?
$MESS["EDIT_FORM_NEW_contact_tab_choose_option"] = "Выберите вариант";
$MESS["EDIT_FORM_NEW_contact_tab_about_map_marker"] = "Маркер на карте ставится автоматически при заполнении поля Адрес.<br /> Вы можете откорректировать местоположение заведения, перетащив маркер мышкой в нужный участок карты.";
$MESS["EDIT_FORM_NEW_contact_tab_add"] = "+Добавить";
$MESS["EDIT_FORM_NEW_contact_tab_from"] = "С";
$MESS["EDIT_FORM_NEW_contact_tab_to"] = "По";

$MESS["EDIT_FORM_NEW_contact_tab_pn"] = "Пн";
$MESS["EDIT_FORM_NEW_contact_tab_vt"] = "Вт";
$MESS["EDIT_FORM_NEW_contact_tab_sr"] = "Ср";
$MESS["EDIT_FORM_NEW_contact_tab_ch"] = "Чт";
$MESS["EDIT_FORM_NEW_contact_tab_pt"] = "Пт";
$MESS["EDIT_FORM_NEW_contact_tab_sb"] = "Сб";
$MESS["EDIT_FORM_NEW_contact_tab_vs"] = "Вс";
$MESS["EDIT_FORM_NEW_contact_save_contacts"] = "Сохранить контакты";

?>