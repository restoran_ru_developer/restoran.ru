<?
$MESS["EDIT_FORM_NEW_main_tab_name"] = "Название:";
$MESS["EDIT_FORM_NEW_main_tab_section"] = "Раздел:";
$MESS["EDIT_FORM_NEW_main_tab_description"] = "Описание:";
$MESS["EDIT_FORM_NEW_main_tab_main_restaurant_photo"] = "Основная фотография<br/>ресторана:";
$MESS["EDIT_FORM_NEW_main_tab_to_much_letters"] = "Вы привысили ограничение в 3000 знаков";
$MESS["EDIT_FORM_NEW_main_tab_save_data"] = "Сохранить основные данные";
$MESS["EDIT_FORM_NEW_main_tab_to_mach_letters_250"] = "Вы привысили ограничение в 250 символов";
$MESS["EDIT_FORM_NEW_main_tab_choose_section"] = "Выберите раздел";
?>