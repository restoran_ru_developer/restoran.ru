<?
$MESS["EDIT_FORM_NEW_contact_tab_choose_option"] = "Choose option";
$MESS["EDIT_FORM_NEW_contact_tab_about_map_marker"] = "Marker on the map is automatically when you fill in the address. <br /> You can adjust the location of places, dragging the marker mouse in the desired area of the map.";
$MESS["EDIT_FORM_NEW_contact_tab_add"] = "+Add";
$MESS["EDIT_FORM_NEW_contact_tab_from"] = "From";
$MESS["EDIT_FORM_NEW_contact_tab_to"] = "To";

$MESS["EDIT_FORM_NEW_contact_tab_pn"] = "Su";
$MESS["EDIT_FORM_NEW_contact_tab_vt"] = "Mo";
$MESS["EDIT_FORM_NEW_contact_tab_sr"] = "Tu";
$MESS["EDIT_FORM_NEW_contact_tab_ch"] = "We";
$MESS["EDIT_FORM_NEW_contact_tab_pt"] = "Th";
$MESS["EDIT_FORM_NEW_contact_tab_sb"] = "Fr";
$MESS["EDIT_FORM_NEW_contact_tab_vs"] = "Sa";

$MESS["EDIT_FORM_NEW_contact_save_contacts"] = "Save contacts";
?>