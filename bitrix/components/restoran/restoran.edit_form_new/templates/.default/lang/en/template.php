<?
$MESS["EDIT_FORM_NEW_attention_text"] = "Warning! Information is stored separately in each block. Be careful!";
$MESS["EDIT_FORM_NEW_required_fields"] = " - fields are required.";
$MESS["EDIT_FORM_NEW_common"] = "The basis";
$MESS["EDIT_FORM_NEW_contacts"] = "Contact Information";
$MESS["EDIT_FORM_NEW_params"] = "Parameters";
$MESS["EDIT_FORM_NEW_photo"] = "Photos";
$MESS["EDIT_FORM_NEW_restaurant_location"] = "Restaurant location";
$MESS["EDIT_FORM_NEW_banners"] = "Banners";
$MESS["EDIT_FORM_NEW_preview"] = "Preview";

$MESS["EDIT_FORM_NEW_main_tab_name"] = "Name:";
$MESS["EDIT_FORM_NEW_main_tab_section"] = "Section:";
$MESS["EDIT_FORM_NEW_main_tab_description"] = "Description:";
$MESS["EDIT_FORM_NEW_main_tab_main_restaurant_photo"] = "Principal photography<br/>restaurant:";
$MESS["EDIT_FORM_NEW_main_tab_to_much_letters"] = "You are exceeding a limit of 3000 characters";
$MESS["EDIT_FORM_NEW_main_tab_save_data"] = "Save the basic data";
$MESS["EDIT_FORM_NEW_main_tab_to_mach_letters_250"] = "You are exceeding a limit of 250 characters";
?>