<?
$MESS["EDIT_FORM_NEW_main_tab_name"] = "Name:";
$MESS["EDIT_FORM_NEW_main_tab_section"] = "Section:";
$MESS["EDIT_FORM_NEW_main_tab_description"] = "Description:";
$MESS["EDIT_FORM_NEW_main_tab_main_restaurant_photo"] = "Principal photography<br/>restaurant:";
$MESS["EDIT_FORM_NEW_main_tab_to_much_letters"] = "You are exceeding a limit of 3000 characters";
$MESS["EDIT_FORM_NEW_main_tab_save_data"] = "Save the basic data";
$MESS["EDIT_FORM_NEW_main_tab_to_mach_letters_250"] = "You are exceeding a limit of 250 characters";
$MESS["EDIT_FORM_NEW_main_tab_choose_section"] = "Choose section";

?>