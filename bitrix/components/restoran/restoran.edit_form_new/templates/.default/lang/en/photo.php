<?
$MESS['EDIT_FORM_NEW_photo_tab_delete_photo'] = 'Delete photo?';
$MESS['EDIT_FORM_NEW_photo_tab_add_photo'] = 'Add photos';
$MESS['EDIT_FORM_NEW_photo_tab_review'] = 'Review';
$MESS['EDIT_FORM_NEW_photo_tab_too_much'] = 'Too many files ({netItems}) loading.  Limit exceeded in {itemLimit} files.';
$MESS['EDIT_FORM_NEW_photo_tab_noneed_extantion'] = 'Invalid file extension {file}. You can upload only files with extensions: {extensions}.';
$MESS['EDIT_FORM_NEW_photo_tab_too_big'] = 'File {file} too high, maximum file upload size - {sizeLimit}.';
$MESS['EDIT_FORM_NEW_photo_tab_too_small'] = '{file} is too small, minimum file size is {minSizeLimit}.';
$MESS['EDIT_FORM_NEW_photo_tab_empty'] = 'File {file} - is empty, select the files again, but not including.';
$MESS['EDIT_FORM_NEW_photo_tab_didnt_selected'] = 'Do not select a file to download';
$MESS['EDIT_FORM_NEW_photo_tab_be_upload'] = 'The files are being uploaded, if you leave now the upload will be cancelled.';
$MESS['EDIT_FORM_NEW_photo_tab_delete_photo_trigger_title'] = 'Delete this photo';
$MESS['EDIT_FORM_NEW_photo_tab_save_photo'] = 'Save photo';
?>