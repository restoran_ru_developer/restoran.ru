<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$this_file_name = pathinfo(__FILE__);
require_once($BX_DOC_ROOT.'/bitrix/components/restoran/restoran.edit_form_new/templates/.default/lang/'.LANGUAGE_ID.'/'.$this_file_name['basename']);

if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
    $arParams["PROPERTIES"] = Array(
        "SUBWAY","OUT_CITY","SITE", "PHONE","EMAIL","OPENING_HOURS", "ADMINISTRATIVE_DISTR", "AREA", "LANDMARKS","ADDRESS", "MAP");
    $arParams["REQUIRED_PROPERTIES"] = Array("PHONE","OPENING_HOURS","ADDRESS");

    CModule::IncludeModule("iblock");
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):
        foreach ($arParams["PROPERTIES"] as $PROP):
            $val = array();
            $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>$PROP));
            while($ar_props = $db_props->Fetch())
            {
                $val[] =  $ar_props["VALUE"];
                $desc[] = $ar_props["DESCRIPTION"];
                $property[$PROP] = $ar_props;
                unset($property[$PROP]["VALUE"]);
                unset($property[$PROP]["DESCRIPTION"]);
            }
            $property[$PROP]["VALUES"] = $val;
            $property[$PROP]["DESCRIPTION"] = $desc;

            switch($property[$PROP]["PROPERTY_TYPE"]) {
                case "E":
                    $rsEl = CIBlockElement::GetList(
                        Array("SORT"=>"ASC","NAME"=>"ASC"),
                        Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $property[$PROP]["LINK_IBLOCK_ID"],
                        ),
                        false,
                        false,
                        Array("ID", "NAME")
                    );
                    while($arEl = $rsEl->Fetch()) {
                        $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
                    }
                    break;
                case "L":

                    $rsEnumProp = CIBlockProperty::GetPropertyEnum(
                        $property[$PROP]["ID"],
                        Array("SORT"=>"asc","NAME"=>"ASC"),
                        Array("IBLOCK_ID" => $arRests["ID"])
                    );
                    while($arEnumProp = $rsEnumProp->Fetch()) {
                        if($arProp["MULTIPLE"] == "Y")
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEnumProp["ID"]] = $arEnumProp["ID"];
                        else
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = $arEnumProp["ID"];
                    }
                    break;
                case "S":

                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                    break;
                case "F":
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                    break;
            }
        endforeach;
        $pn = $vt = $sr = $ch = $pt = $sb = $vs = $days = $tt = $days2 = array();
        $property["OPENING_HOURS"]["VALUES"] = explode("; ",$property["OPENING_HOURS"]["VALUES"][0]);
        foreach ($property["OPENING_HOURS"]["VALUES"] as $key=>$op)
        {
            if ($op=="Круглосуточно")
            {
                $pn[$key] = $vt[$key] = $sr[$key] = $ch[$key] = $pt[$key] = $sb[$key] = $vs[$key] = 1;
            }
            else
            {
                $t = explode(" ",$op);
                $days = $t[0];
                $time = $t[1];
                $days2 = explode("-",$days);
                if (!$days2[1])
                {
                    $days = explode(",",$days);
                    foreach ($days as $d)
                    {
                        $d = trim($d);
                        if ($d=="пн")
                            $pn[$key]=1;
                        if ($d=="вт")
                            $vt[$key]=1;
                        if ($d=="ср")
                            $sr[$key]=1;
                        if ($d=="чт")
                            $ch[$key]=1;
                        if ($d=="пт")
                            $pt[$key]=1;
                        if ($d=="сб")
                            $sb[$key]=1;
                        if ($d=="вс")
                            $vs[$key]=1;
                    }
                }
                else
                {
                    if ($days=="пн-вс")
                        $pn[$key] = $vt[$key] = $sr[$key] = $ch[$key] = $pt[$key] = $sb[$key] = $vs[$key] = 1;
                    if ($days=="пн-сб")
                        $pn[$key] = $vt[$key] = $sr[$key] = $ch[$key] = $pt[$key] = $sb[$key] = 1;
                    if ($days=="пн-пт")
                        $pn[$key] = $vt[$key] = $sr[$key] = $ch[$key] = $pt[$key] = 1;
                    if ($days=="пн-чт")
                        $pn[$key] = $vt[$key] = $sr[$key] = $ch[$key] =  1;
                    if ($days=="пн-ср")
                        $pn[$key] = $vt[$key] = $sr[$key] =  1;
                }
                $tt = explode("-",$time);
                $time1[$key] = $tt[0];
                $time2[$key] = $tt[1];
            }
        }
        ?>
        <form id="cont_form" class="my_rest" action="/bitrix/components/restoran/restoran.edit_form_new/templates/.default/core.php" method="post" name="rest_edit" id="rest_<?=$arRests["ID"]?>">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_contacts" />
            <input type="hidden" name="ACTIVE" value="<?=$arRests["ACTIVE"]?>" />
            <input type="hidden" name="NAME" value="<?=$arRests["NAME"]?>" />
            <ul style="padding-top:15px;">
                <?foreach($property as $keyProp=>$prop)://v_dump($rest["SELECTED_PROPERTIES"][$keyProp]);?>
                    <?
                    $code_up = strtoupper($prop["CODE"]);
                    //if(in_array(strtoupper($prop["CODE"]), $arParams["HIDDEN_PROPERTIES"])) continue;
                    //if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && in_array(strtoupper($prop["CODE"]), $arParams["ADMIN_ONLY"])) continue;
                    ?>
                    <?
                    switch($prop["PROPERTY_TYPE"]):
                        case "E":
                            ?>
                            <li class="item-row">
                            <strong><?=$prop["NAME"]?>:
                                <?if ($prop["HINT"]):?>
                                    <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                <?endif;?>
                            </strong>
                            <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                            <?
                            $m=1;
                            $NK=0;
                            if ($prop["CODE"]=="subway"){
                                for($c=0;$c<count($prop["DESCRIPTION"]);$c++)
                                {
                                    if($prop["DESCRIPTION"][$c]=="")
                                        $prop["DESCRIPTION"][$c]=$c+1;
                                }
                                ?>
                                <?foreach($prop["DESCRIPTION"] as $KEY => $DSel):?>
                                    <?if($DSel=="") $DSel="1";?>
                                    <?if($NK!==$DSel){?>
                                        <?if ($m > 1):?>
                                            </li><li class="item-row"><strong></strong>
                                        <?endif;?>
                                        <?
                                        $NK=$DSel;
                                        $INSELECT=array();
                                        foreach($prop["DESCRIPTION"] as $KEY2 => $DSel2){
                                            if($DSel2===$DSel) {
                                                $INSELECT[]=$prop["VALUES"][$KEY2];
                                            }
                                        }
                                        ?>
                                        <p class="inpwrap"><?//=($KEY+1)?>
                                            <select prop="<?=$prop["CODE"]?>" multiple data-placeholder="<?=GetMessage('EDIT_FORM_NEW_contact_tab_choose_option');?>" class="chzn-select <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[".$DSel."][]";?>">
                                                <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>
                                                    <option value="<?=$keyRestType?>"<? if(in_array($keyRestType,$INSELECT)) echo  "selected='selected'"?>><?=$restType?></option>
                                                <?endforeach?>
                                            </select>
                                        </p>
                                    <?}?>
                                    <?
                                    $m++;
                                    ?>
                                <?endforeach;?>
                            <?}else{?>
                                <?foreach($prop["VALUES"] as $Sel){?>
                                    <?if($m>1){?>
                                        </li><li class="item-row"><strong></strong>
                                    <?}?>
                                    <p class="inpwrap">
                                        <select prop="<?=$prop["CODE"]?>" multiple data-placeholder="<?=GetMessage('EDIT_FORM_NEW_contact_tab_choose_option');?>" class="chzn-select <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";?>">
                                            <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>
                                                <option value="<?=$keyRestType?>"<? if($keyRestType==$Sel) echo  "selected='selected'"?>><?=$restType?></option>
                                            <?endforeach?>
                                        </select>
                                    </p>

                                    <?
                                    $m++;
                                    ?>
                                <?}
                            }?>
                            </li>
                            <?/*if($prop["MULTIPLE"] == "Y"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                    <?endif*/?>
                            <?
                            break;
                        case "L":
                            ?>
                            <li class="item-row">
                                <strong><?=$prop["NAME"]?>:
                                    <?if ($prop["HINT"]):?>
                                        <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                    <?endif;?>
                                </strong>
                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                <p>
                                    <?if($prop["LIST_TYPE"] == "C" && $prop["MULTIPLE"] == "N"):?>
                                        <span class="niceCheck">
                                        <input type="checkbox" class="checkbox" name="PROP[<?=$prop["CODE"]?>][<?=$selPropKey?>]"<?if(in_array($prop["VALUE"], $rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"])) echo  "checked='checked'"?> value="<?=$prop["VALUE"]?>" /><br/>
                                    </span>
                                    <?endif?>
                                </p>
                            </li>
                            <?
                            break;
                        case "S":
                            ?>
                            <li class="item-row"><?//v_dump($prop)?>
                                <strong><?=$prop["NAME"]?>:
                                    <?if ($prop["HINT"]):?>
                                        <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                    <?endif;?>
                                </strong>

                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                <?if ($prop["CODE"]!="map" && $prop["CODE"]!="opening_hours"):?>
                                    <?
                                    $selPropCnt = 0;
                                    foreach($prop["VALUES"] as $selPropKey=>$selPropVal):?>
                                        <?if($selPropCnt >= 1):?>
                                            <strong></strong>
                                        <?endif?>
                                        <p class="inpwrap"><?if($prop["MULTIPLE"] == "Y"):?><?//=($selPropKey+1)?><?endif;?>
                                            <input type="text" class="<?=$prop["CODE"]?> text <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" />
                                        </p>
                                        <?
                                        $selPropCnt++;
                                    endforeach?>
                                <?elseif ($prop["CODE"]=="opening_hours"):?>
                                    <?=implode(", ",$prop["VALUES"])?>
                                    <?foreach($prop["VALUES"] as $selPropKey=>$selPropVal):?>
                                        <?if($selPropKey >= 1):?>
                                            <strong></strong>
                                        <?endif?>
                                        <p class="inpwrap times_for_op">
                                            <?=GetMessage('EDIT_FORM_NEW_contact_tab_from');?> <input class="time" type="text" name="PROP[opening_hours][]" value="<?=$time1[$selPropKey]?>" name=""> <?=GetMessage('EDIT_FORM_NEW_contact_tab_to');?> <input class="time"  name="opening_hours2[]"  type="text" value="<?=$time2[$selPropKey]?>">
                                            <input class="day <?=($pn[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_pn');?>" /><input class="day1" type="hidden" value="<?=($pn[$selPropKey])?"1":"0"?>" name="pn[]" />
                                            <input class="day <?=($vt[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_vt');?>" /><input class="day1" type="hidden" value="<?=($vt[$selPropKey])?"1":"0"?>" name="vt[]" />
                                            <input class="day <?=($sr[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_sr');?>" /><input class="day1" type="hidden" value="<?=($sr[$selPropKey])?"1":"0"?>" name="sr[]" />
                                            <input class="day <?=($ch[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_ch');?>" /><input class="day1" type="hidden" value="<?=($ch[$selPropKey])?"1":"0"?>" name="ch[]" />
                                            <input class="day <?=($pt[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_pt');?>" /><input class="day1" type="hidden" value="<?=($pt[$selPropKey])?"1":"0"?>" name="pt[]" />
                                            <input class="day <?=($sb[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_sb');?>" /><input class="day1" type="hidden" value="<?=($sb[$selPropKey])?"1":"0"?>" name="sb[]" />
                                            <input class="day <?=($vs[$selPropKey])?"active":""?>" type="button" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_vs');?>" /><input class="day1" type="hidden" value="<?=($vs[$selPropKey])?"1":"0"?>" name="vs[]" />
                                        </p>
                                    <?endforeach;?>

                                <?elseif ($prop["CODE"]=="map"):?>
                                    <div class="inpwrap">
                                        <p style='color:#24a6cf; font-family: "Open Sans",sans-serif; font-size:14px;'><?=GetMessage('EDIT_FORM_NEW_contact_tab_about_map_marker');?></p>
                                        <?
                                        $md = explode(",", $prop["VALUES"][0]);
                                        $mas=array(
                                            "yandex_lat"=>$md[1],
                                            "yandex_lon"=>$md[0],
                                            "yandex_scale"=>14
                                        );

                                        if($md[1]==""){
                                            if(CITY_ID=="spb") $DTS = COption::GetOptionString("main", "spb_center");
                                            elseif(CITY_ID=="tmn") $DTS = COption::GetOptionString("main", "tmn_center");
                                            else  $DTS = COption::GetOptionString("main", "msk_center");
                                            $DTS = explode(",", $DTS);

                                            unset($mas);
                                            $mas=array(
                                                "yandex_lat"=>$DTS[1],
                                                "yandex_lon"=>$DTS[0],
                                                "yandex_scale"=>14
                                            );
                                        }

                                        foreach($prop["VALUES"] as $D){
                                            $md = explode(",", $D);
                                            $mas["PLACEMARKS"][]=array(
                                                "LON"=>$md[0],
                                                "LAT"=>$md[1],
                                                "TEXT"=>""
                                            );
                                        }
                                        ?>
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:map.yandex.view",
                                            "restoran_edit_or",
                                            Array(
                                                "KEY" => "ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==",
                                                "INIT_MAP_TYPE" => "MAP",
                                                "MAP_DATA" => serialize($mas),
                                                "MAP_WIDTH" => "975",
                                                "MAP_HEIGHT" => "500",
                                                "CONTROLS" => array("ZOOM"),
                                                "OPTIONS" => array("ENABLE_SCROLL_ZOOM","DISABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING"),
                                                "MAP_ID" => "rest_".$_REQUEST["ID"],
                                                'LOCALE' => "ru",
                                                "DEV_MODE"=>"Y"
                                            )
                                        );?>

                                    </div>
                                <?endif;?>
                            </li>
                            <?/*if($prop["MULTIPLE"] == "Y"&&$prop["CODE"]!="map"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                        <?endif*/?>
                            <?if($prop["MULTIPLE"] == "Y"&&$prop["CODE"]=="opening_hours"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="<?=GetMessage('EDIT_FORM_NEW_contact_tab_add');?>" />
                            </li>
                        <?endif?>
                            <?
                            break;?>
                        <?endswitch?>
                <?endforeach?>
            </ul>
            <div align="right" id="save_block_cont" class="save_block" style="position:fixed">
                <input type="submit" class="light_button" value="<?=GetMessage('EDIT_FORM_NEW_contact_save_contacts');?>" />
            </div>
        </form>
    <?
    endif;
}
?>
<script>
    $(".chzn-select").chosen();
    $(".time").maski("99:99");
    $(".pane").on('click',".day",function(){
        if ($(this).next().val()=="1")
        {
            $(this).removeClass("active");
            $(this).next().val("0");
        }
        else
        {
            $(this).addClass("active");
            $(this).next().val("1");
        }
    });
    if (!$("#BX_YMAP_rest_<?=$_REQUEST["ID"]?>").html())
    {
        init_rest_<?=$_REQUEST["ID"]?>();
    }
    //
    // send suggest
</script>
<br /><Br />
<script>
    var new_x;
    var new_y;
    var height_cont = $("#cont_form").parent().height();
    $(document).ready(function() {
        $(window).scroll(function(){
            if ($(this).scrollTop()<(height_cont-700))
            {
                $("#save_block_cont").css("position","fixed");
            }
            else
                $("#save_block_cont").css("position","relative");
        });

        $(".my_rest").on('change',".chzn-select",function(){
            var v = $(this).val();
            if (v)
            {
                if ($(this).attr("prop")=="subway"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений");
                if ($(this).attr("prop")=="out_city"&&v.length>1)
                    alert("Внимание! Вы выбрали более 1 значения")
                if ($(this).attr("prop")=="average_bill"&&v.length>1)
                    alert("Внимание! Вы выбрали более 1 значения")
                if ($(this).attr("prop")=="kitchen"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений")
                if ($(this).attr("prop")=="type"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений")
            }
        });
        // send suggest
        $(".address").autocomplete("/search/map_suggest.php", {
            delay:600,
            limit: 5,
            minChars: 3,
            cacheLength:5,
            width:970,
            selectOnly:true,
            formatItem: function(data, i, n, value) {
                var a = value.split("###")[1];
                var x = a.split(" ")[0];
                var y = a.split(" ")[1];
                return "<a class='suggest_res_url' href='javascript:void(0)' onclick='go_adres("+x+","+y+")'> " + value.split("###")[0] + "</a>";
            },
            onItemSelect: function(value,a,b,c) {
                var a = value.split("###")[1];
                new_x = a.split(" ")[0];
                new_y = a.split(" ")[1];
            },
            formatResult: function(data, value) {
                return value.split("###")[0];
            },
            extraParams: {
                search_in: function() {return "adres"}
            }
        });
        $(".se").click(function(){
            new_x = $(this).attr("new_x");
            new_y = $(this).attr("new_y");
            if (new_x&&new_y)
            {
                go_adres(new_x,new_y)
                //        window.GLOBAL_arMapObjects[rest_1148825].setCenter(new ymaps.GeoPoint ( new_x, new_y ), 14 );
            }
            return false;
        });
        $(".address").keyup(function(event){
            if (event.which == 13) {
                return false;
            }
        });
        $(".address").keypress(function(event){
            if (event.which == 13) {
                if (new_x&&new_y)
                {
                    go_adres(new_x,new_y)
                }

                return false;
            }
        });
    });
    function go_adres(x,y)
    {
        var arPlacemark = {};
        arPlacemark.LAT = x;
        arPlacemark.LON = y;
        arPlacemark.TEXT = "";
        window.GLOBAL_arMapObjects["<?=("rest_".$_REQUEST["ID"])?>"].geoObjects.each(function (geoObject) {
            window.GLOBAL_arMapObjects["<?=("rest_".$_REQUEST["ID"])?>"].geoObjects.remove(geoObject);
        });
        window.GLOBAL_arMapObjects["<?=("rest_".$_REQUEST["ID"])?>"].setCenter([y,x], 16 );
        window.BX_YMapAddPlacemark(window.GLOBAL_arMapObjects["<?=("rest_".$_REQUEST["ID"])?>"],arPlacemark);


        var ID = $("input[name=ELEMENT_ID]").val();
        var lnk = $("form#rest_"+ID).attr("action");
        if (!lnk)
            lnk = $("#cont_form").attr("action");
        var IB_ID = $("form#rest_"+ID).find("input[name=IBLOCK_ID]").val();

        var pts = [];
        window.GLOBAL_arMapObjects["<?=("rest_".$_REQUEST["ID"])?>"].geoObjects.each(function (geoObject) {
            pts[pts.length]=geoObject.geometry.getCoordinates();
        });
        if(!ajax_load){
            $.post(lnk, {pts: pts, ELEMENT_ID: ID, act:"save_dots", IBLOCK_ID: IB_ID}, function(data){


            });
        }




    }
</script>