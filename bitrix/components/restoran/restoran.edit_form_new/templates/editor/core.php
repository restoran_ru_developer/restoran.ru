<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || in_array(34, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || in_array(34, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_rest(){
    
        v_dump($_REQUEST);
        exit;
	global $USER;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
		
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $_REQUEST["IBLOCK_SECTION_ID"],          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"NAME"           => $_REQUEST["NAME"],
  		"ACTIVE"         => "Y",            
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"TAGS"           => $_REQUEST["TAGS"],
  	);
	
    if (CSite::InGroup(Array(9)))
       $arLoadProductArray["CODE"]=translitIt($_REQUEST["NAME"]);
	//Основная фотка
	if(isset($_FILES["DETAIL_PICTURE"])){
		
		$arLoadProductArray["DETAIL_PICTURE"] = $_FILES["DETAIL_PICTURE"];
		/*
    	$DF = CFile::SaveFile($FAR, "restorans");
		$MF = CFile::CopyFile($DF);
	
		$arNewFile = CIBlock::ResizePicture(CFile::MakeFileArray($DF), array("WIDTH" => 392,"HEIGHT" => 260,"METHOD" => "resample", "COMPRESSION"=>95));
		$arLoadProductArray["PREVIEW_PICTURE"] = $arNewFile;
		
		$arLoadProductArray["DETAIL_PICTURE"]=$MF;
		*/
		
	}
	
	
		
	
	//var_dump($_REQUEST["PROP"]);
	if(!isset($_REQUEST["PROP"]["photos"])) $_REQUEST["PROP"]["photos"][]="";
	if(!isset($_REQUEST["PROP"]["google_photo"])) $_REQUEST["PROP"]["google_photo"][]="";
	if(!isset($_REQUEST["PROP"]["videopanoramy"])) $_REQUEST["PROP"]["videopanoramy"][]="";
	
	$COMs = $_REQUEST["PROP"]["videopanoramy_radio"];
	
	$COMs_ph = $_REQUEST["PROP"]["photos_comment"];
	
	
	//var_dump($COMs);
	foreach($_REQUEST["PROP"] as $CODE=>$PR){
		if($CODE=="photos"){
			$FL=form_file_array("NEW_PHOTO");
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"restorans");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"restorans");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
			$l=0;	
			foreach($PR as $fid) {
				$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$COMs_ph[$l]);
				$l++;
			}
			$PR=$PR2;
		}
		
		if($CODE=="google_photo"){
			$FL=form_file_array("GGL");
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"restorans");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"restorans");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
				
			foreach($PR as $fid) {$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>"");
			$PR=$PR2;
			}
		}
		
		
		
		
		if($CODE=="videopanoramy"){
			$FL=form_file_array("PANORAMS");
			//var_dump($_FILES);
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"restorans");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"restorans");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
			$l=0;
			var_dump($COMs);
			foreach($PR as $fid){
				$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$COMs[$l]);
				$l++;
			} 
			$PR=$PR2;
			
			
		}
		
		if($CODE=="add_props"){
			$PR=array("VALUE"=>array('TYPE'=>'HTML', 'TEXT'=>$PR));
			//var_dump($PR);
		}
		
		//var_dump($CODE);
		//
		
		CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array($CODE => $PR));
	}	

	//Проверяем права на запись
	if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");
	
	
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию	
	
		unset($arLoadProductArray["IBLOCK_ID"]);
		//var_dump($arLoadProductArray);
		if($el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray, false, true, true)) echo "ok";

        //  карта сайта
        $res = CIBlock::GetByID($_REQUEST["IBLOCK_ID"]);
        if($ar_res = $res->GetNext()) {
            if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                RestIBlock::ObjectSiteMapJson($_REQUEST["ELEMENT_ID"], $_REQUEST['IBLOCK_ID'],$ar_res['CODE']);
            }
        }

        //  CUSTOM RANK у спящих
        RestIBlock::SleepingCustomRank();


		//переиндексация
//		CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
	}else{
		//создаем новую публикацию

		if($ELEMENT_ID = $el->Add($arLoadProductArray))
  			echo $ELEMENT_ID;
		else
  			echo "Error: ".$el->LAST_ERROR;
	}
	
}

function edit_rest_main(){
    
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
        $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
        if ($_REQUEST["IBLOCK_SECTION_ID"][0])
            $sec = $_REQUEST["IBLOCK_SECTION_ID"][0];
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $sec,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"NAME"           => $_REQUEST["NAME"],
  		"SORT"           => $_REQUEST["SORT"],
  		"ACTIVE"         => $_REQUEST["ACTIVE"],            
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"TAGS"           => $_REQUEST["TAGS"],
  	);	
        if ($_REQUEST["SHOW_COUNTER"])
            $arLoadProductArray["SHOW_COUNTER"] = $_REQUEST["SHOW_COUNTER"];
//        if (CSite::InGroup(Array(9)))
//           $arLoadProductArray["CODE"]=translitIt($_REQUEST["NAME"]);



	//Основная фотка
	if(isset($_FILES["DETAIL_PICTURE"])){		
		$arLoadProductArray["DETAIL_PICTURE"] = $_FILES["DETAIL_PICTURE"];		
                //if ($_REQUEST["DETAIL_PICTURE_OLD"])
                  //  CFile::Delete($_REQUEST["DETAIL_PICTURE_OLD"]);
	}        

	//Проверяем права на запись
	if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");		
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию		
		unset($arLoadProductArray["IBLOCK_ID"]);
		//var_dump($arLoadProductArray);

		if($el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray, false, true, true))
        {
                CIBlockElement::SetElementSection($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_SECTION_ID"]);
                $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_' . $iblock_id);

                //  карта сайта
                $res = CIBlock::GetByID($_REQUEST["IBLOCK_ID"]);
                if($ar_res = $res->GetNext()) {
                    if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                        RestIBlock::ObjectSiteMapJson($_REQUEST["ELEMENT_ID"], $_REQUEST['IBLOCK_ID'],$ar_res['CODE']);
                    }
                }

            //  CUSTOM RANK у спящих
            RestIBlock::SleepingCustomRank();

            //переиндексация
//                        CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
                LocalRedirect("/redactor/edit.php?ID=".$_REQUEST["ELEMENT_ID"]);
        }
	}	        
}

function edit_rest_contacts(){
    
	global $USER;
$arParams["PROPERTIES"] = Array(
    "subway","out_city","site", "phone","real_tel","email", "address", "opening_hours", "opening_hours_google","adminisrative_distr", "area", "landmarks");

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
        $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
        $id = (int)$_REQUEST["ELEMENT_ID"];  
        $ar2 = array();
        $ar = array();
	foreach ($_REQUEST["PROP"] as $code=>$prop)
        {
            $PR2 = array();
            if ($code=="subway")
            {
                foreach ($prop as $key=>$P)
                {
                    foreach ($P as $Pp)
                        $PR2[] = array("VALUE"=>$Pp, "DESCRIPTION"=>$key);
                }
                $prop = $PR2;
            }
            /*if ($code=="opening_hours")
            {                
                foreach ($prop as $key=>$P)
                {          
                    $days = "";
                    $val = "";
                    $day = array();
                    if ($_REQUEST["pn"][$key]==1)
                        $day[] = "пн";
                    if ($_REQUEST["vt"][$key]==1)
                        $day[] = "вт";
                    if ($_REQUEST["sr"][$key]==1)
                        $day[] = "ср";
                    if ($_REQUEST["ch"][$key]==1)
                        $day[] = "чт";
                    if ($_REQUEST["pt"][$key]==1)
                        $day[] = "пт";
                    if ($_REQUEST["sb"][$key]==1)
                        $day[] = "сб";
                    if ($_REQUEST["vs"][$key]==1)
                        $day[] = "вс";
                    $days = implode(",",$day);
                    if ($days=="пн,вт,ср,чт,пт,сб,вс")
                        $val = "пн-вс";
                    elseif ($days=="пн,вт,ср,чт,пт,сб")
                        $val = "пн-сб";
                    elseif ($days=="пн,вт,ср,чт,пт")
                        $val = "пн-пт";
                    elseif ($days=="пн,вт,ср,чт")
                        $val = "пн-чт";
                    elseif ($days=="пн,вт,ср")
                        $val = "пн-cр";
                    else
                        $val = $days;
                    if ($val=="пн-вс"&&!$P)
                        $val = "Круглосуточно";
                    elseif ($val||$P)
                        $val = $val." ".$P."-".$_REQUEST["opening_hours2"][$key];
                    $PR2[] = $val;                    
                }                
                $prop = implode("; ",$PR2);
            }*/
            if($code=="add_props")
                $prop=array("VALUE"=>array('TYPE'=>'HTML', 'TEXT'=>$prop));               
            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($code => $prop));
            $ar2[] = $code;
        }        
        $ar = array_diff($arParams["PROPERTIES"],$ar2);
        foreach ($ar as $p)
        {
            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($p => ""));
        }

        //  карта сайта
        $res = CIBlock::GetByID($_REQUEST["IBLOCK_ID"]);
        if($ar_res = $res->GetNext()) {
            if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                RestIBlock::ObjectSiteMapJson($_REQUEST["ELEMENT_ID"], $_REQUEST['IBLOCK_ID'],$ar_res['CODE']);
            }
        }

    //  CUSTOM RANK у спящих
    RestIBlock::SleepingCustomRank();

//        CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
        $el = new CIBlockElement;
        $el->Update($_REQUEST["ELEMENT_ID"], Array(), false, true, true);
        LocalRedirect("/redactor/edit.php?ID=".$_REQUEST["ELEMENT_ID"]."#contacts.php?ID=".$_REQUEST["ELEMENT_ID"]."&".bitrix_sessid_get());        
}
function edit_rest_priority()
{
        global $USER;
        $arParams["PROPERTIES"] = Array(
    "top_spec_place_1","top_spec_place_2","s_place_restaurants","s_place_banket","priority_kitchen","priority_type","day_rest","sleeping_rest","similar_rest","rest_group","working","no_mobile","without_reviews","closed_rubrics","place_new_rest",'SHOW_COUNTER');

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
        $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
        $id = (int)$_REQUEST["ELEMENT_ID"];      
        $ar2 = array();
        $ar = array();

	foreach ($_REQUEST["PROP"] as $code=>$prop)
    {
        if($code=="add_props")
            $prop=array("VALUE"=>array('TYPE'=>'HTML', 'TEXT'=>$prop));
        if($code=="SHOW_COUNTER"){
            $arSelect = Array("ID", "PROPERTY_SHOW_COUNTER",'SHOW_COUNTER');
            $arFilter = Array('ID'=>$id,"IBLOCK_ID"=>IntVal($iblock_id));
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
            if($ob = $res->Fetch())
            {
//                FirePHP::getInstance()->info($ob);

                if(intval($prop)!=$ob['PROPERTY_SHOW_COUNTER_VALUE']){
                    $prop=array("VALUE"=>intval($prop),'DESCRIPTION'=>$ob['SHOW_COUNTER']);
                }
            }
        }
        CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($code => $prop));
        $ar2[] = $code;
    }

    $ar = array_diff($arParams["PROPERTIES"],$ar2);
    foreach ($ar as $p)
    {
        CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($p => ""));
    }


    //  карта сайта
    $res = CIBlock::GetByID($_REQUEST["IBLOCK_ID"]);
    if($ar_res = $res->GetNext()) {
        if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
            RestIBlock::ObjectSiteMapJson($_REQUEST["ELEMENT_ID"], $_REQUEST['IBLOCK_ID'],$ar_res['CODE']);
        }
    }


    //  CUSTOM RANK у спящих
    RestIBlock::SleepingCustomRank();

//    CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
    $el = new CIBlockElement;
    $el->Update($_REQUEST["ELEMENT_ID"], Array(), false, true, true);

    LocalRedirect("/redactor/edit.php?ID=".$_REQUEST["ELEMENT_ID"]."#priority.php?ID=".$_REQUEST["ELEMENT_ID"]."&".bitrix_sessid_get());

    
}

function edit_rest_props(){
    
	global $USER;
    $arParams["PROPERTIES"] = Array(
        "user_bind","type",'AROUND_THE_CLOCK','FOURSQUARE_USER_LOGIN','INSTAGRAM_USER_LOGIN',"average_bill","proposals","banket_average_bill", "kitchen","credit_cards", "children", "features","entertainment", "ideal_place_for", "music", "parking","breakfast","breakfasts","business_lunch","branch","wi_fi","d_tours","add_props",'NETWORK_REST','REST_NETWORK','FEATURES_IN_PICS','SHOW_YA_STREET','WHERE_TO_GO','LIKE_CODE_FIELD','D_N_C_LIKE_CODE_FIELD','SHOW_NETWORK_PHONE_COMMON_PAGE');

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
        $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
        $id = (int)$_REQUEST["ELEMENT_ID"];      
        $ar2 = array();
        $ar = array();


        foreach ($_REQUEST["PROP"] as $code => $prop) {
            if ($code == "subway") {
                foreach ($prop as $key => $P) {
                    foreach ($P as $Pp)
                        $PR2[] = array("VALUE" => $Pp, "DESCRIPTION" => $key);
                }
                $prop = $PR2;
            }

            if ($code == "add_props")
                $prop = array("VALUE" => array('TYPE' => 'HTML', 'TEXT' => $prop));

            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($code => $prop));
            $ar2[] = $code;
        }
        $ar = array_diff($arParams["PROPERTIES"],$ar2);
        foreach ($ar as $p)
        {
            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($p => ""));
        }

        //  карта сайта
        $res = CIBlock::GetByID($_REQUEST["IBLOCK_ID"]);
        if($ar_res = $res->GetNext()) {
            if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                RestIBlock::ObjectSiteMapJson($_REQUEST["ELEMENT_ID"], $_REQUEST['IBLOCK_ID'],$ar_res['CODE']);
            }
        }

    //  CUSTOM RANK у спящих
    RestIBlock::SleepingCustomRank();

//        CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
        $el = new CIBlockElement;
        $el->Update($_REQUEST["ELEMENT_ID"], Array(), false, true,true);
        LocalRedirect("/redactor/edit.php?ID=".$_REQUEST["ELEMENT_ID"]."#props.php?ID=".$_REQUEST["ELEMENT_ID"]."&".bitrix_sessid_get());        
}

function edit_rest_photos(){
    
	global $USER;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
    $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
    $id = (int)$_REQUEST["ELEMENT_ID"];
	foreach ($_REQUEST["PROP"] as $code=>$prop)
    {
        if ($code=="photos"||$code=="videopanoramy"||$code=="google_photo")
        {
            $photos = array();
            $PR2 = array();
            $db_props = CIBlockElement::GetProperty($iblock_id, $id, array(), Array("CODE"=>$code));
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                    $photos[] = $ar_props["VALUE"];
            }
            foreach ($prop["fid_new"] as $key=>$P)
            {
                $PR2[] = array("VALUE"=>CFile::MakeFileArray($P), "DESCRIPTION"=>$prop["descr"][$key]);
            }
            $prop = $PR2;
        }
        CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($code => $prop));
        if ($code=="photos"||$code=="videopanoramy"||$code=="google_photo")
        {
            foreach ($photos as $photo)
                CFile::Delete($photo);
        }
    }
    if (!count($_REQUEST["PROP"]["photos"]))
    {
        $photos = array();
        $db_props = CIBlockElement::GetProperty($iblock_id, $id, array(), Array("CODE"=>"photos"));
        while($ar_props = $db_props->Fetch())
        {
            if ($ar_props["VALUE"])
                $photos[] = $ar_props["VALUE"];
        }
        foreach ($photos as $photo)
                CFile::Delete($photo);
        CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array("photos" => Array("VALUE"=>Array())));
    }
    if (!count($_REQUEST["PROP"]["videopanoramy"]))
    {
        $photos = array();
        $db_props = CIBlockElement::GetProperty($iblock_id, $id, array(), Array("CODE"=>"videopanoramy"));
        while($ar_props = $db_props->Fetch())
        {
            if ($ar_props["VALUE"])
                $photos[] = $ar_props["VALUE"];
        }
        foreach ($photos as $photo)
                CFile::Delete($photo);
        CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array("videopanoramy" => Array("VALUE"=>Array())));
    }
    if (!count($_REQUEST["PROP"]["google_photo"]))
    {
        $photos = array();
        $db_props = CIBlockElement::GetProperty($iblock_id, $id, array(), Array("CODE"=>"google_photo"));
        while($ar_props = $db_props->Fetch())
        {
            if ($ar_props["VALUE"])
                $photos[] = $ar_props["VALUE"];
        }
        foreach ($photos as $photo)
                CFile::Delete($photo);
        CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array("google_photo" => Array("VALUE"=>Array())));
    }

    //  карта сайта
    $res = CIBlock::GetByID($_REQUEST["IBLOCK_ID"]);
    if($ar_res = $res->GetNext()) {
        if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
            RestIBlock::ObjectSiteMapJson($_REQUEST["ELEMENT_ID"], $_REQUEST['IBLOCK_ID'],$ar_res['CODE']);
        }
    }

//  CUSTOM RANK у спящих
    RestIBlock::SleepingCustomRank();

//    CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
    $el = new CIBlockElement;
    $el->Update($_REQUEST["ELEMENT_ID"], Array(), false, true, true);
    LocalRedirect("/redactor/edit.php?ID=".$_REQUEST["ELEMENT_ID"]."#photo.php?ID=".$_REQUEST["ELEMENT_ID"]."&".bitrix_sessid_get());
}
/***********ФОРМИРУЕМ МАССИВ ФАЙЛА**************/
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($BL_NAME);
	//var_dump($_FILES);
	
	foreach($_FILES[$BL_NAME] as $A=>$C){
		if(is_array($C)){
			foreach($C as $index=>$V){
				$FAR[$index][$A]=$V;
			}
		}
	}
	return $FAR;
}



function save_dots(){
	global $DB;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	//var_dump($_REQUEST["pts"]);
//	$D = explode(",", $_REQUEST["pts"]);
//	$_REQUEST["pts"]=$D[0].",".$D[1];
	foreach($_REQUEST["pts"] as $D){
		$PTS[]=$D[0].",".$D[1];
		
		$LON[]=$D[1];
		$LAT[]=$D[0];
	}
	
	//var_dump($PTS);
	//$DB->StartUsingMasterOnly();

	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("map" => $PTS));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("lat" => $LAT));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("lon" => $LON));
	
	//$DB->StopUsingMasterOnly();
}



function add_new_rest(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	global $USER;
	
	$arRestIB = getArIblock("catalog", CITY_ID);

	$el = new CIBlockElement;

	$PROP = array();
	$PROP["user_bind"] = $USER->GetID();
	
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $arRestIB["ID"],
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => "Новый ресторан",
  		"ACTIVE"         => "N",            // активен
  	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray))
  		echo "ok";
	else
  		echo "Error: ".$el->LAST_ERROR;
}
/*
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($_FILES);
	foreach($_FILES["block"] as $A=>$C){		
		$VAL="";
		if(is_array($C[$BL_NAME])){
			foreach($C[$BL_NAME] as $V) $VAL=$V;
		}
		$FAR[$A]=$V;
	}
	
	if(is_array($FAR["name"])){
		$RET=array();
		foreach($FAR as $N=>$V){
			for($i=0;$i<count($V);$i++){
				$RET[$i][$N]=$V[$i];
			}
		}
		//var_dump($RET);
		return $RET;
	}else return $FAR;
}
*/

if($_REQUEST["act"]=="edit_rest") edit_rest();
if($_REQUEST["act"]=="edit_rest_main") edit_rest_main();
if($_REQUEST["act"]=="edit_rest_priority") edit_rest_priority();
if($_REQUEST["act"]=="edit_rest_contacts") edit_rest_contacts();
if($_REQUEST["act"]=="edit_rest_props") edit_rest_props();
if($_REQUEST["act"]=="edit_rest_photos") edit_rest_photos();
if($_REQUEST["act"]=="save_dots") save_dots();
if($_REQUEST["act"]=="add_new_rest") add_new_rest();

?>
