<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;
?>
<script>
    var clicked = false;
    $(document).ready(function()
    {
        $("ul.tabss").each(function(){
                if ($(this).attr("ajax")=="ajax")
                {
                    if ($(this).attr("history")=="true")
                        var history = true;
                    else
                        var history = false;
                    var url = "";
                    if ($(this).attr("ajax_url"))
                        url = $(this).attr("ajax_url");
                    $(this).tabs("div.panes > .pane", {                
                        history: history,
                        onBeforeClick: function(event, i) {                           
                            var _this = this;                            
                            /*if (clicked)
                            {
                                if (confirm("Вы точно хотите уйти с этой вкладки? Не сохраненная информация может быть утеряна."))
                                {
                                    var pane = _this.getPanes().eq(i);
                                    if (pane.is(":empty")) {
                                        pane.load(url+_this.getTabs().eq(i).attr("href"));

                                    }
                                }
                                else
                                    return false;                                
                            }
                            else*/
                            {
                                var pane = _this.getPanes().eq(i);
                                if (pane.is(":empty")) {
                                    pane.load(url+_this.getTabs().eq(i).attr("href"));

                                }
                            }                            
                        }
                    });
                }
        });
        $(".tabss a").click(function(){
            clicked = true;            
        });
    });     	
</script>
<div class="wrap-div">
    <p class="font16"><b>Внимание! Информация сохраняется отдельно в каждом блоке. Будьте внимательны!</b></p>
    <div>
        <ul class="tabss big" ajax="ajax" history="true" ajax_url="<?=$templateFolder?>/">
            <li>
                <a href="main.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>" class="current">
                    <div class="left tab_left"></div>
                    <div class="left name">Основые данные</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="priority.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name">Приоритеты</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="contacts.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name">Контакты</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="props.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name">Параметры</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="photo.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name">Фотогалерея</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
        <div class="panes">
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
        </div>    
        <input type="hidden" id="aaa" id="tabss_l" value="0" />
        <a href="/redactor/rest_preview.php?ID=<?=$arParams["REST_ID"]?>&IBLOCK_ID=<?=$arResult["RESTAURANTS"][0]["IBLOCK_ID"]?>" class="light_button">Предпросмотр</a>
        <a href="/redactor/menu_edit.php?RESTORAN=<?=$arParams["REST_ID"]?>" style="position:absolute; text-transform: uppercase; top:30px; right:0px;">Редактировать меню</a>
    </div>    
</div>
<script type="text/javascript" src="<?=$templateFolder?>/jq_redactor/redactor.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/uploaderObject.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.form.js"></script>