<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script src="/bitrix/components/restoran/restoran.edit_form_new/templates/editor/script.js"></script>
<?
if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
$arParams["PROPERTIES"] = Array(
    "TOP_SPEC_PLACE_1","TOP_SPEC_PLACE_2","S_PLACE_RESTAURANTS","S_PLACE_BANKET","PRIORITY_KITCHEN","PRIORITY_TYPE","DAY_REST","SLEEPING_REST","SIMILAR_REST","REST_GROUP","WORKING","NO_MOBILE","WITHOUT_REVIEWS","CLOSED_RUBRICS","PLACE_NEW_REST",'SHOW_COUNTER');
$arParams["REQUIRED_PROPERTIES"] = Array();

    CModule::IncludeModule("iblock");    
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):
        foreach ($arParams["PROPERTIES"] as $PROP):
            $val = array();
            $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>$PROP));
            while($ar_props = $db_props->Fetch())
            {                                         
                $val[] =  $ar_props["VALUE"];
                $desc[] = $ar_props["DESCRIPTION"];
                $property[$PROP] = $ar_props;                
                unset($property[$PROP]["VALUE"]);
                unset($property[$PROP]["DESCRIPTION"]);
            }
            $property[$PROP]["VALUES"] = $val;                
            $property[$PROP]["DESCRIPTION"] = $desc;                           
            
            switch($property[$PROP]["PROPERTY_TYPE"]) {
                case "E":
                    $rsEl = CIBlockElement::GetList(
                        Array("SORT"=>"ASC","NAME"=>"ASC"),
                        Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $property[$PROP]["LINK_IBLOCK_ID"],
                        ),
                        false,
                        false,
                        Array("ID", "NAME")
                    );
                    while($arEl = $rsEl->Fetch()) {
                        $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
                    }
                break;
                case "L":                    
                    $rsEnumProp = CIBlockProperty::GetPropertyEnum(
                        $property[$PROP]["ID"],
                        Array("SORT"=>"asc","NAME"=>"ASC"),
                        Array("IBLOCK_ID" => $arRests["IBLOCK_ID"])
                    );
                    while($arEnumProp = $rsEnumProp->Fetch()) {
                        
                        if($arProp["MULTIPLE"] == "Y")
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEnumProp["ID"]] = $arEnumProp["ID"];
                        else
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = $arEnumProp["ID"];
                    }
                    
                break;
                case "S":
                    
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                break;
                case "F":
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                break;
        }
        endforeach;             
?>        
        <form id="priority_form" class="my_rest" action="/bitrix/components/restoran/restoran.edit_form_new/templates/editor/core.php" method="post" name="rest_edit" id="rest_<?=$arRests["ID"]?>">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_priority" />
            <ul style="padding-top:15px;">
            <?foreach($property as $keyProp=>$prop)://v_dump($rest["SELECTED_PROPERTIES"][$keyProp]);?>                
                <?
                $code_up = strtoupper($prop["CODE"]);
                //if(in_array(strtoupper($prop["CODE"]), $arParams["HIDDEN_PROPERTIES"])) continue;
                //if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && in_array(strtoupper($prop["CODE"]), $arParams["ADMIN_ONLY"])) continue;
                ?>
                <?                
                switch($prop["PROPERTY_TYPE"]):
                    case "E":
                ?>
                    <li class="item-row">                        
                        <strong><?=$prop["NAME"]?>:
                            <?if ($prop["HINT"]):?>
                                <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                            <?endif;?>
                        </strong>
                        <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                <?
                                $m=1;
                                $NK=0;	
                                if ($prop["CODE"]=="subway"){                                          
                                        for($c=0;$c<count($prop["DESCRIPTION"]);$c++)
                                        {
                                            if($prop["DESCRIPTION"][$c]=="") 
                                                $prop["DESCRIPTION"][$c]=$c+1;
                                        }        
                                        ?>                        
                                        <?foreach($prop["DESCRIPTION"] as $KEY => $DSel):?>	
                                            <?if($DSel=="") $DSel="1";?>
                                            <?if($NK!==$DSel){?>
                                                <?if ($m > 1):?>
                                                    </li><li class="item-row"><strong></strong>
                                                <?endif;?>
                                                <?
                                                $NK=$DSel;
                                                $INSELECT=array();                                    
                                                foreach($prop["DESCRIPTION"] as $KEY2 => $DSel2){
                                                        if($DSel2===$DSel) {                                                    
                                                                $INSELECT[]=$prop["VALUES"][$KEY2];
                                                        }
                                                }                                                 
                                                ?>                                                
                                                <p class="inpwrap"><?//=($KEY+1)?>
                                                    <select prop="<?=$prop["CODE"]?>" multiple data-placeholder="Выберите вариант" class="chzn-select <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[".$DSel."][]";?>">
                                                        <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>    
                                                            <option value="<?=$keyRestType?>"<? if(in_array($keyRestType,$INSELECT)) echo  "selected='selected'"?>><?=$restType?></option>
                                                        <?endforeach?>
                                                    </select>
                                                </p>
                                            <?}?>
                                            <?
                                            $m++;
                                            ?>
                                        <?endforeach;?>
                                <?}else{?>
                                        <?foreach($prop["VALUES"] as $Sel){?>	
                                            <?if($m>1){?>
                                                    </li><li class="item-row"><strong></strong>
                                            <?}?>                                                       
                                            <p class="inpwrap">
                                                    <select prop="<?=$prop["CODE"]?>" multiple data-placeholder="Выберите вариант" class="chzn-select <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";?>">
                                                            <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>    
                                                                <option value="<?=$keyRestType?>"<? if($keyRestType==$Sel) echo  "selected='selected'"?>><?=$restType?></option>
                                                            <?endforeach?>
                                                    </select>
                                            </p>

                                        <?
                                        $m++;
                                        ?>
                                        <?}
                                }?>
                    </li>
                    <?/*if($prop["MULTIPLE"] == "Y"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                    <?endif*/?>
                    <?
                    break;
                    case "L":
                    ?>
                        <li class="item-row">
                            <strong><?=$prop["NAME"]?>:
                                <?if ($prop["HINT"]):?>
                                    <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                <?endif;?>
                            </strong>
                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                            <p>                                                               
                                <?if($prop["LIST_TYPE"] == "C" && $prop["MULTIPLE"] == "N"):?>                                
                                    <span class="niceCheck">
                                        <input type="checkbox" class="checkbox" name="PROP[<?=$prop["CODE"]?>]"<?if($prop["VALUES"][0]==$arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]) echo  "checked='checked'"?> value="<?=$arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]?>" /><br/>                                        
                                    </span>
                                <?endif?>
                            </p>
                        </li>                        
                    <?
                    break;
                    case "S":?>
                        <li class="item-row"><?//v_dump($prop)?>
                            <strong><?=$prop["NAME"]?>:
                                <?if ($prop["HINT"]):?>
                        <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                    <?endif;?>
                            </strong>

                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                            <?if ($prop["CODE"]!="map" && $prop["CODE"]!="opening_hours"):?>
                        <?
                        $selPropCnt = 0;
                        foreach($prop["VALUES"] as $selPropKey=>$selPropVal):?>
                            <?if($selPropCnt >= 1):?>
                                <strong></strong>
                            <?endif?>
                            <p class="inpwrap"><?if($prop["MULTIPLE"] == "Y"):?><?//=($selPropKey+1)?><?endif;?>
                                <input type="text" class="<?=$prop["CODE"]?> text <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" />
                            </p>
                            <?
                            $selPropCnt++;
                        endforeach?>
                    <?endif;?>
                        </li>
                        <?/*if($prop["MULTIPLE"] == "Y"&&$prop["CODE"]!="map"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                        <?endif*/?>
                        <?if($prop["MULTIPLE"] == "Y"&&$prop["CODE"]=="opening_hours"):?>
                        <li class="item-row">
                            <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                        </li>
                    <?endif?>
                    <?
                    break;?>
                    <?
                    case "N":?>
                        <li class="item-row"><?//v_dump($prop)?>
                            <strong><?=$prop["NAME"]?>:
                                <?if ($prop["HINT"]):?>
                                    <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                <?endif;?>
                            </strong>

                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                            <?if ($prop["CODE"]!="map" && $prop["CODE"]!="opening_hours"):?>
                                <?
                                $selPropCnt = 0;
                                foreach($prop["VALUES"] as $selPropKey=>$selPropVal):?>
                                    <?if($selPropCnt >= 1):?>
                                        <strong></strong>
                                    <?endif?>
                                    <p class="inpwrap"><?if($prop["MULTIPLE"] == "Y"):?><?//=($selPropKey+1)?><?endif;?>
                                        <input type="number" class="<?=$prop["CODE"]?> text <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" <?if($prop["CODE"]=='SHOW_COUNTER'):?>step="100"<?endif?>/>

                                        <?if($prop["CODE"]=='SHOW_COUNTER'):?>
                                            <a href="/redactor/show_counter_statistic.php?ID=<?=(int)$_REQUEST["ID"]?>" style="margin: 10px 0;overflow: hidden;display: block;font-weight: 900;text-decoration: underline;" target="_blank">Скачать статистику счетчика показов</a>
                                        <?endif?>
                                    </p>
                                <?
                                $selPropCnt++;
                                endforeach?>
                            <?endif;?>
                        </li>
                        <?/*if($prop["MULTIPLE"] == "Y"&&$prop["CODE"]!="map"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                        <?endif*/?>
                        <?if($prop["MULTIPLE"] == "Y"&&$prop["CODE"]=="opening_hours"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                        <?endif?>
                    <?break;?>
                <?endswitch?>
            <?endforeach?>   
            </ul>
            <div align="right" id="save_block_pri" class="save_block" style="position:fixed">
                <input type="submit" class="light_button" value="Сохранить приоритеты" />
            </div>
        </form>
<?        
    endif;
}
?>
<script>
    $(".chzn-select").chosen();
    //$(".time").maski("99:99");  
</script>
<br /><Br />
<script>
    var height_pri = $("#priority_form").parent().height();
    $(document).ready(function() {
         jQuery(".niceCheck").mousedown(
		function() {
			changeCheck(jQuery(this));
		});

	jQuery(".niceCheck").each(
		function() {
			changeCheckStart(jQuery(this));
		});
        $(window).scroll(function(){           
           if ($(this).scrollTop()<(height_pri-450))
           {
               $("#save_block_pri").css("position","fixed");               
           }
           else
               $("#save_block_pri").css("position","relative");
       });      
    });       
</script>