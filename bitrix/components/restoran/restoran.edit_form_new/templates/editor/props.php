<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script>
    var height_prop = $("#prop_form").parent().height();
    $(document).ready(function(){
       $(window).scroll(function(){           
           if ($(this).scrollTop()<(height_prop-700))
           {
               $("#save_block_prop").css("position","fixed");               
           }
           else
               $("#save_block_prop").css("position","relative");
       }) 
    });
</script>
<?
if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
    $arParams["PROPERTIES"] = Array(
        "USER_BIND","TYPE",'AROUND_THE_CLOCK','FOURSQUARE_USER_LOGIN','INSTAGRAM_USER_LOGIN',"NUMBER_OF_ROOMS","AVERAGE_BILL", "BANKET_AVERAGE_BILL","KITCHEN","CREDIT_CARDS", "CHILDREN", "PROPOSALS", "FEATURES",'FEATURES_B_H',"ENTERTAINMENT", "IDEAL_PLACE_FOR", "MUSIC", "PARKING","BREAKFAST","BREAKFASTS","BUSINESS_LUNCH","BRANCH","WI_FI","D_TOURS","ADD_PROPS",'RENTING_HALLS_COST','ALLOWED_ALCOHOL','BANKET_SPECIAL_EQUIPMENT','BANKET_ADDITIONAL_OPTION','BANKET_MENU_SUM','SHOW_REST_PHONE','SHOW_NETWORK_PHONE_COMMON_PAGE','KOLICHESTVOCHELOVEK','NETWORK_REST','REST_NETWORK','FEATURES_IN_PICS','SHOW_YA_STREET','WHERE_TO_GO','LIKE_CODE_FIELD','D_N_C_LIKE_CODE_FIELD');
    $arParams["REQUIRED_PROPERTIES"] = Array("TYPE");

    CModule::IncludeModule("iblock");    
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):
        foreach ($arParams["PROPERTIES"] as $PROP):
            $val = array();
            $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>$PROP));
            while($ar_props = $db_props->Fetch())
            {                                         
                $val[] =  $ar_props["VALUE"];
                $desc[] = $ar_props["DESCRIPTION"];
                $property[$PROP] = $ar_props;                
                unset($property[$PROP]["VALUE"]);
                unset($property[$PROP]["DESCRIPTION"]);
            }
            $property[$PROP]["VALUES"] = $val;    
            $property[$PROP]["DESCRIPTION"] = $desc;
            

            switch($property[$PROP]["PROPERTY_TYPE"]) {
                case "E":
                    if($PROP=='REST_NETWORK'){
                        $rsEl = CIBlockElement::GetList(
                            Array("SORT"=>"ASC","NAME"=>"ASC"),
                            Array(
                                "IBLOCK_ID" => $property[$PROP]["LINK_IBLOCK_ID"],
                                'PROPERTY_user_bind'=>$property['USER_BIND']['VALUES'][0],
                                '!ID'=>intval($_REQUEST['ID'])
                            ),
                            false,
                            false,
                            Array("ID", "NAME")
                        );
                        while($arEl = $rsEl->Fetch()) {
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
                        }
                    }
                    elseif($PROP=='WHERE_TO_GO'){
                        $rsEl = CIBlockElement::GetList(
                            Array("SORT"=>"ASC","NAME"=>"ASC"),
                            Array(
                                "IBLOCK_ID" => $property[$PROP]["LINK_IBLOCK_ID"],
                                'SECTION_CODE'=>CITY_ID=='rga'?'spb':CITY_ID
                            ),
                            false,
                            false,
                            Array("ID", "NAME")
                        );
                        while($arEl = $rsEl->Fetch()) {
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
                        }
                    }
                    else {
                        $rsEl = CIBlockElement::GetList(
                            Array("SORT"=>"ASC","NAME"=>"ASC"),
                            Array(
                                "ACTIVE" => "Y",
                                "IBLOCK_ID" => $property[$PROP]["LINK_IBLOCK_ID"],
                            ),
                            false,
                            false,
                            Array("ID", "NAME")
                        );
                        while($arEl = $rsEl->Fetch()) {
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
                        }
                    }

                break;
                case "L":
                    $rsEnumProp = CIBlockProperty::GetPropertyEnum(
                        $property[$PROP]["ID"],
                        Array("SORT"=>"asc","NAME"=>"ASC"),
                        Array("IBLOCK_ID" => $arRests["IBLOCK_ID"])
                    );
                    while($arEnumProp = $rsEnumProp->Fetch()) {
                        if($arProp["MULTIPLE"] == "Y")
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEnumProp["ID"]] = $arEnumProp["ID"];
                        else
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = $arEnumProp["ID"];                        
                    }                    
                break;
                case "S":
                    if ($property[$PROP]["USER_TYPE"]=="UserID")
                    {
                        $res = CUser::GetList(($by="name"), ($order="asc"),Array("GROUPS_ID"=>Array(9)));
                        while($arEl = $res->Fetch())
                        {
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["LAST_NAME"]." ".$arEl["NAME"]." [".$arEl["PERSONAL_PROFESSION"]."]";
                        }                        
                    }
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                break;
                case "F":
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                break;                
            }
        endforeach;
?>
        <form id="prop_form" class="my_rest" action="/bitrix/components/restoran/restoran.edit_form_new/templates/editor/core.php" method="post" name="rest_edit">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_props" />
            <ul style="padding-top:15px;">
            <?

            foreach($property as $keyProp=>$prop)://v_dump($rest["SELECTED_PROPERTIES"][$keyProp]);?>
                <?
                $code_up = strtoupper($prop["CODE"]);
                //if(in_array(strtoupper($prop["CODE"]), $arParams["HIDDEN_PROPERTIES"])) continue;
                //if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && in_array(strtoupper($prop["CODE"]), $arParams["ADMIN_ONLY"])) continue;
                ?>
                <?                                
                switch($prop["PROPERTY_TYPE"]):
                    case "E":
                ?>
                    <li class="item-row">
                        <strong><?=$prop["NAME"]?>:
                           <?if ($prop["HINT"]):?>
                                <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                            <?endif;?>
                        </strong>
                        <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                <?
                                $m=1;
                                $NK=0;	 
                                ?>                               
                                <?foreach($prop["VALUES"] as $Sel){?>	
                                            <?if($m>1){?>
                                                    </li><li class="item-row"><strong></strong>
                                            <?}?>                                                       
                                            <p class="inpwrap">
                                                    <select prop="<?=$prop["CODE"]?>"  multiple data-placeholder="Выберите вариант" class="chzn-select2 <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";?>">
<!--                                                        --><?//if(preg_match('/kolichestvochelovek/i',$code_up)){
//                                                            FirePHP::getInstance()->info($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]);
//                                                        }?>
                                                            <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>
                                                                <option value="<?=$keyRestType?>"<? if($keyRestType==$Sel) echo  "selected='selected'"?>><?=$restType?></option>
                                                            <?endforeach?>
                                                    </select>
                                            </p>

                                        <?
                                        $m++;
                                        ?>
                                        <?}?>                                
                    </li>
                    <?if($prop["MULTIPLE"] == "Y"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                    <?endif?>
                    <?
                    break;
                    case "L":
                    ?>
                        <li class="item-row">
                            <strong><?=$prop["NAME"]?>:
                                <?if ($prop["HINT"]):?>
                                    <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                <?endif;?>
                            </strong>
                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                            <p>                          
                                <?if($prop["LIST_TYPE"] == "C" && $prop["MULTIPLE"] == "N"):?>   
                                    <span class="niceCheck">
                                        <input type="checkbox" class="checkbox" name="PROP[<?=$prop["CODE"]?>]"<?if($prop["VALUES"][0] == $arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]) echo  "checked='checked'"?> value="<?=$arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]?>" /><br/>
                                    </span>
                                <?endif?>
                            </p>
                        </li>
                    <?
                    break;
                    case "S":           
                        if ($prop["USER_TYPE"]=="UserID"):                        
                    ?>
                            <li class="item-row">
                                <strong><?=$prop["NAME"]?>:
                                    <?if ($prop["HINT"]):?>
                                        <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                    <?endif;?>
                                </strong>
                                    <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                 <?
                                        $m=1;
                                        $NK=0;	 
                                        ?>                               
                                        <?foreach($prop["VALUES"] as $Sel){?>	
                                                    <?if($m>1){?>
                                                            </li><li class="item-row"><strong></strong>
                                                    <?}?>                                                       
                                                    <p class="inpwrap">
                                                            <select prop="<?=$prop["CODE"]?>"  multiple data-placeholder="Выберите вариант" class="chzn-select2 <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";?>">
                                                                    <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>    
                                                                        <option value="<?=$keyRestType?>"<? if($keyRestType==$Sel) echo  "selected='selected'"?>><?=$restType?></option>
                                                                    <?endforeach?>
                                                            </select>
                                                    </p>

                                                <?
                                                $m++;
                                                ?>
                                                <?}?>  
                            </li>
                        <?else:?>
                            <li class="item-row"><?//v_dump($prop)?>
                                <strong><?=$prop["NAME"]?>:
                                    <?if ($prop["HINT"]):?>
                                        <img src="/bitrix/js/main/core/images/hint.gif" class="hint_img" title="<?=$prop["HINT"]?>"  alt="<?=$prop["HINT"]?>">
                                    <?endif;?>
                                </strong>
                                    <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                    <?if ($prop["CODE"]!="add_props"):?>
                                        <?
                                        $selPropCnt = 0;
                                        foreach($prop["VALUES"] as $selPropKey=>$selPropVal):?>
                                            <?if($selPropCnt >= 1):?>
                                                <strong></strong>
                                            <?endif?>
                                            <p class="inpwrap"><?if($prop["MULTIPLE"] == "Y"):?><?//=($selPropKey+1)?><?endif;?>
                                                <input type="text" class="text <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" />
                                            </p>
                                        <?
                                        $selPropCnt++;
                                        endforeach?>                            
                                    <?else:?>
                                        <p class="inpwrap" style="float:left;width:811px;">
                                            <textarea name="PROP[<?=$prop["CODE"]?>]" id="add_text" style="height:200px;" class=""><?=$prop["VALUES"][0]["TEXT"]?></textarea>        
                                        </p>
                                    <?endif;?>
                            </li>
                            <?if($prop["MULTIPLE"] == "Y"):?>
                                <li class="item-row">
                                    <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                                </li>
                            <?endif?>
                        <?endif;?>
                    <?
                    break;?>                    
                <?endswitch?>
            <?endforeach?>   
            </ul>
            <div align="right" id="save_block_prop" class="save_block" style="position:fixed">
                <input type="submit" class="light_button" value="Сохранить свойства" />
            </div>
        </form>

<?endif;
}
?>
<script>
$("#add_text").redactor({ 
                path: '/components/restoran/restoran.edit_form_new/templates/.default/jq_redactor', 
                buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|','link'], 
                css: 'redactor.css', 
                lang: 'ru',
                focus: false,
                //fileUpload: '<?//=$templateFolder?>/file_upload.php',
                autoresize: false
        }); 
        jQuery(".niceCheck").mousedown(
		function() {
			changeCheck(jQuery(this));
		});

	jQuery(".niceCheck").each(
		function() {
			changeCheckStart(jQuery(this));
		});
                $(".chzn-select2").chosen();
    $(document).ready(function() {  
        $(".my_rest").on('change',".chzn-select2",function(){            
            var v = $(this).val();
            if (v)
            {
                if ($(this).attr("prop")=="subway"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений");
                if ($(this).attr("prop")=="out_city"&&v.length>1)
                    alert("Внимание! Вы выбрали более 1 значения")
                if ($(this).attr("prop")=="average_bill"&&v.length>1)
                    alert("Внимание! Вы выбрали более 1 значения")
                if ($(this).attr("prop")=="kitchen"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений")
                if ($(this).attr("prop")=="type"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений")
            }
        });
    });
</script>