<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
    CModule::IncludeModule("iblock");    
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):       
        $db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), Array('IBLOCK_ID'=>$arRests["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y'), true);
        while($ar_result = $db_list->GetNext()){            
                $arResult["SECTIONS"][$ar_result["ID"]]=$ar_result["NAME"];
        }
        $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>"add_props"));
        if($ar_props = $db_props->Fetch())
        {
            $property["add_props"] = $ar_props["VALUE"]["TEXT"];
        }
        $db_old_groups = CIBlockElement::GetElementGroups($arRests["ID"], true);
        while($ar_group = $db_old_groups->Fetch())
            $ar_groups[$ar_group["ID"]] = $ar_group["ID"];
?>
        <script type="text/javascript" src="/components/restoran/restoran.edit_form_new/templates/.default/jq_redactor/redactor.js"></script>
        <form id="main_form" enctype="multipart/form-data" class="my_rest" action="/bitrix/components/restoran/restoran.edit_form_new/templates/editor/core.php" method="post" name="rest_edit" id="rest_<?=$rest["ID"]?>_main">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_main" />
            <ul style="padding-top:15px;">
                <li class="item-row">
                    <strong>Название:</strong>
                    <i class="star">*</i>
                    <p class="inpwrap">
                        <input type="text" class="text rest_name ob" code="<?=$arRests["CODE"]?>" name="NAME" value="<?=$arRests["NAME"]?>" />
                    </p>
                </li>                
                <li class="item-row">
                    <strong>Символьный код:</strong>
                    <i class="star">*</i>
                    <p class="inpwrap">
                        <input type="text" class="text ob" name="CODE" value="<?=$arRests["CODE"]?>" />
                    </p>
                </li>                
                <li class="item-row">
                    <strong>Синонимы для поиска:</strong>
                    <p class="inpwrap">
                        <textarea id="tags" class="text rest_tags" code="TAGS" name="TAGS"  ><?=$arRests["TAGS"]?></textarea>
                    </p>
                </li>
                <li class="item-row">
                    <strong>Сортировка:</strong>
                    <i class="star">*</i>
                    <p class="inpwrap">
                        <input type="text" class="text ob" name="SORT" value="<?=(!$arRests["SORT"])?"500":$arRests["SORT"]?>" />
                    </p>
                </li>
                <li class="item-row">
                        <strong>Раздел:</strong>
                        <i class="star">*</i>
                        <p class="inpwrap">
                            <select multiple data-placeholder="Выберите раздел" class="chzn-select ob" style="width:350px;" name="IBLOCK_SECTION_ID[]">
                            <?foreach($arResult["SECTIONS"] as $keyRestType=>$restType):?>
                                    <option value="<?=$keyRestType?>"<? if($keyRestType==$ar_groups[$keyRestType]) echo  "selected='selected'"?>><?=$restType?></option>
                            <?endforeach?>
                            </select>
                        </p>
                </li>        
                <li class="item-row">
                    <strong>Активность:</strong>                       
                    <p>                                                  
                        <span class="niceCheck">
                            <input type="checkbox" class="checkbox" name="ACTIVE" <?if($arRests["ACTIVE"]=="Y") echo  "checked='checked'"?> value="Y" /><br/>
<!--                            <input type="hidden" name="OLD_ACTIVE" value="<?=$arRest["ACTIVE"]?>" />-->
                        </span>                        
                    </p>
                </li>
                <li class="item-row">
                    <strong>Кол-во просмотров:</strong>                       
                    <p>                                                                          
                        <input type="text" class="text" name="SHOW_COUNTER" value="<?=$arRests["SHOW_COUNTER"]?>" />                        
                    </p>
                </li>
                <li class="item-row">
                    <strong>Описание:</strong>

                    <p class="inpwrap" style="float:left;width:811px;">
                        <textarea name="DETAIL_TEXT" id="detail_text" style="height:200px;" class=""><?=$arRests["DETAIL_TEXT"]?></textarea>
                    </p>
                </li>                   
                <li class="item-row">
                    <strong>Дополниетльное описание:</strong>

                    <p class="inpwrap" style="float:left;width:811px;">
                        <textarea name="PROP[add_props]" id="add_props" style="height:200px;" class=""><?=$property["add_props"]?></textarea>
                    </p>
                </li>
                <li class="item-row">
                    <strong>Основная фотография<br/>ресторана:</strong>
                    <?
                    $src_big = CFile::GetPath($arRests["DETAIL_PICTURE"]);
                    ?>
                    <p class="inpwrap">
                        <?if($src_big!=""){?>
                        <div class="ph"><img src="<?=$src_big?>"  width="230" height="125"></div>
                        <?}?>
                        <div class="loader">
                        <input type="file" name="DETAIL_PICTURE" value="" id="file-field_detail_DETAIL_PICTURE" class="file-field" />
                        <input type="hidden" name="DETAIL_PICTURE_OLD" value="<?=$arRests["DETAIL_PICTURE"]?>" />
                        </div>
                    </p>
                </li>       
                <script type="text/javascript">
                    jQuery(".niceCheck").mousedown(
                            function() {
                                    changeCheck(jQuery(this));
                            });

                    jQuery(".niceCheck").each(
                            function() {
                                    changeCheckStart(jQuery(this));
                            });
                    $("#detail_text").redactor({ 
                            path: '/components/restoran/restoran.edit_form_new/templates/.default/jq_redactor', 
                            buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|','link'], 
                            css: 'redactor.css', 
                            lang: 'ru',
                            focus: false,
                            //fileUpload: '<?//=$templateFolder?>/file_upload.php',
                            autoresize: false,
//                            keydownCallback: function(a){
//                                if (a.$editor.text().length>1600)
//                                {
//                                    $(".redactor_editor").attr("style","background-color:#ffc3b7!important; height:"+$(".redactor_editor").css("height"));
//                                    alert("Вы привысили ограничение в 1600 знаков");                                    
////                                    var text = a.$editor.html();
////                                    a.$editor.text("");
////                                    a.insertHtml(text.substr(0,20));      
////                                    a.syncCode();
//                                }                                
//                                
//                            }
                    });      
                    $("#add_props").redactor({ 
                            path: '/components/restoran/restoran.edit_form_new/templates/.default/jq_redactor', 
                            buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|','link'], 
                            css: 'redactor.css', 
                            lang: 'ru',
                            focus: false,
                            //fileUpload: '<?//=$templateFolder?>/file_upload.php',
                            autoresize: false,
//                            keydownCallback: function(a){
//                                if (a.$editor.text().length>1600)
//                                {
//                                    $(".redactor_editor").attr("style","background-color:#ffc3b7!important; height:"+$(".redactor_editor").css("height"));
//                                    alert("Вы привысили ограничение в 1600 знаков");                                    
//                                }                                
//                                
//                            }
                    }); 
                    $(".chzn-select").chosen();
                    var fileInput2 = $('#file-field_detail_DETAIL_PICTURE');  
                    fileInput2.customFileInput();                                                            
                </script>
            </ul>
            <div align="right" class="save_block" id="save_block_main" style="position: fixed">
                <input type="submit" class="light_button" value="Сохранить основные данные" />
            </div>
        </form>

<?        
    endif;
}
?>
<script>
/*$(document).ready(function(){
    $("#rest_<?=$rest["ID"]?>_main").submit(function(){
        //Проверка введенных данных
//        if (!$(this).find(".comment_textarea").val())
//        {
//            alert("Введите комментарий!");
//            return false;
//        }
        var params = $(this).serialize();
        $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: params,
                success: function(data) {
                    if(data)
                    {
                        data = eval('('+data+')');                        
//                        if (!$("#comment_modal").size())
//                        {
//                            $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");
//                        }
//                        var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
//                        var html2 = '</div>';                    
//                        $('#comment_modal').html(html+data.MESSAGE+html2);
//                        showOverflow();
//                        setCenter($("#comment_modal"));
//                        $("#comment_modal").fadeIn("300");
//                        if (data.ERROR=="1")
//                        {
//                            $("#add_commm").attr("disabled",false);
//                        }
//                        if (data.STATUS=="1")
//                            setTimeout("location.reload()","3500");
                          console.log(data);
                    }
                }
            });
    return false; 
    }); 
});   */
    var height_main = $("#main_form").parent().height();    
    $(document).ready(function(){
//       $("#tags").keypress(function(){
//            if ($(this).val().length>250)
//            {
//                alert("Вы привысили ограничение в 250 символов");
//                return false;
//            }
//       });
       
       
       $(window).scroll(function(){           
           if ($(this).scrollTop()<(height_main-450))
           {
               $("#save_block_main").css("position","fixed");               
           }
           else
               $("#save_block_main").css("position","relative");
       }) 
    }); 
</script>