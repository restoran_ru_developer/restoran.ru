<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arParams["SECTION_ID"] = $arParams["SECTION_ID"];
$arParams["PICTURE_WIDTH"] = intval($arParams["PICTURE_WIDTH"]);
$arParams["PICTURE_HEIGHT"] = intval($arParams["PICTURE_HEIGHT"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
$arParams["PAGE_COUNT"] = (int)$arParams["PAGE_COUNT"];
$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);
$arParams["SORT_FIELDS"]=trim($arParams["SORT_FIELDS"]);
$arParams["SORT_BY"]=trim($arParams["SORT_BY"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
if (!$_REQUEST["PAGEN_1"])
    $_REQUEST["PAGEN_1"] = 1;
$arResult["ITEMS"]=array();

if ($_REQUEST["pageSort"]=="popular")
{
    $arParams["SORT_FIELDS"] = "UF_COUNT";
    $arParams["SORT_BY"] = $_REQUEST["by"];
}
elseif($arParams["SORT_FIELDS"])
{
    
}
else
{
    $arParams["SORT_FIELDS"] = "ID";
    $arParams["SORT_BY"] = $_REQUEST["by"];
}

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

// get rest name
if ($_GET["q"])
	$arrFilter["NAME"] = "%".$_GET["q"]."%";
// get prop for filter type
if ($_GET["kitchen"])
	$arrFilter["UF_KITCHEN"] = $_GET["kitchen"];
if ($_GET["dificulty"])
	$arrFilter["UF_DIFICULTY"] = $_GET["dificulty"];
if ($_GET["cause"])
	$arrFilter["UF_CAUSE"] = $_GET["cause"];
if ($_GET["time"])	
	$arrFilter["UF_TIME"] = $_GET["time"];
if ($_GET["category"])
	$arrFilter["UF_CATEGORY"] = $_GET["category"];
/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()),$_REQUEST["PAGEN_1"],$arrFilter,$_GET)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CNT_ACTIVE" => "Y",
	);
        if($arIBType = CIBlockType::GetByIDLang($arParams["IBLOCK_TYPE"], LANG))
        {
            $arResult["TITLE"] = $arIBType["NAME"];
        }
	$arSelect = array();
	if(isset($arParams["SECTION_FIELDS"]) && is_array($arParams["SECTION_FIELDS"]))
	{
		foreach($arParams["SECTION_FIELDS"] as $field)
			if(is_string($field) && !empty($field))
				$arSelect[] = $field;
	}

	if(!empty($arSelect))
	{
		$arSelect[] = "ID";
		$arSelect[] = "NAME";
		$arSelect[] = "LEFT_MARGIN";
		$arSelect[] = "RIGHT_MARGIN";
		$arSelect[] = "DEPTH_LEVEL";
		$arSelect[] = "IBLOCK_ID";
		$arSelect[] = "IBLOCK_SECTION_ID";
		$arSelect[] = "LIST_PAGE_URL";
		$arSelect[] = "SECTION_PAGE_URL";
	}

	if(isset($arParams["SECTION_USER_FIELDS"]) && is_array($arParams["SECTION_USER_FIELDS"]))
	{
		foreach($arParams["SECTION_USER_FIELDS"] as $field)
			if(is_string($field) && preg_match("/^UF_/", $field))
				$arSelect[] = $field;
	}

	$arResult["SECTION"] = false;
	if(strlen($arParams["SECTION_CODE"])>0)
	{
		$arFilter["CODE"] = $arParams["SECTION_CODE"];                
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true, Array("ID","NAME","LEFT_MARGIN","RIGHT_MARGIN"));
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
                //$arFilter["SECTION_ID"] = $arResult["SECTION"]["ID"];                
                $arFilter[">LEFT_BORDER"] = $arResult["SECTION"]["LEFT_MARGIN"];
                $arFilter["<RIGHT_BORDER"] = $arResult["SECTION"]["RIGHT_MARGIN"];
                unset($arFilter["CODE"]);
                $arFilter["DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	}
	elseif($arParams["SECTION_ID"]>0 || is_array($arParams["SECTION_ID"]))
	{
            if (is_array($arParams["SECTION_ID"]))
            {
                $res = CIBlockSection::GetByID($arParams["SECTION_ID"]);
                $ar_res = $res->Fetch();
		//$arFilter["SECTION_ID"] = $arParams["SECTION_ID"];
                $arFilter[">=LEFT_BORDER"] = $ar_res["LEFT_MARGIN"];
                $arFilter["<=RIGHT_BORDER"] = $ar_res["RIGHT_MARGIN"];
            }
            else
            {
                $res = CIBlockSection::GetByID($arParams["SECTION_ID"]);
                $ar_res = $res->Fetch();
		//$arFilter["SECTION_ID"] = $arParams["SECTION_ID"];
                $arFilter[">LEFT_BORDER"] = $ar_res["LEFT_MARGIN"];
                $arFilter["<RIGHT_BORDER"] = $ar_res["RIGHT_MARGIN"];
                //$arFilter["SECTION_ID"] = intval($arParams["SECTION_ID"]);
            }
            //v_dump($arFilter);            
		/*$rsSections = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();*/
	}
        $arFilter["DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	/*if(is_array($arResult["SECTION"]))
	{
		unset($arFilter["ID"]);
		unset($arFilter["CODE"]);
		$arFilter["LEFT_MARGIN"]=$arResult["SECTION"]["LEFT_MARGIN"]+1;
		$arFilter["RIGHT_MARGIN"]=$arResult["SECTION"]["RIGHT_MARGIN"];
		$arFilter["<="."DEPTH_LEVEL"]=$arResult["SECTION"]["DEPTH_LEVEL"] + $arParams["TOP_DEPTH"];

		$arResult["SECTION"]["PATH"] = array();
		$rsPath = CIBlockSection::GetNavChain($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$arResult["SECTION"]["PATH"][]=$arPath;
		}
	}
	else
	{
		$arResult["SECTION"] = array("ID"=>0, "DEPTH_LEVEL"=>0);
		$arFilter["<="."DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	}*/

	//ORDER BY
	$arSort = array(
            $arParams["SORT_FIELDS"] => $arParams["SORT_BY"],
            //"ID"=>"DESC",
	);
	//EXECUTE
	if (is_array($arrFilter)&&count($arrFilter)>0)
		$arFilter = array_merge($arFilter, $arrFilter);
	$rsSections = CIBlockSection::GetList($arSort, $arFilter, false/*$arParams["COUNT_ELEMENTS"]*/, $arSelect);
        $rsSections->NavStart($arParams["PAGE_COUNT"]);
	$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
	while($arSection = $rsSections->GetNext())
	{            
		if(isset($arSection["PICTURE"]))
		{
			$arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);
                        $arSection["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arSection["PICTURE"], array('width'=>$arParams["PICTURE_WIDTH"], 'height'=>$arParams["PICTURE_HEIGHT"]), BX_RESIZE_IMAGE_EXACT, true);    
                        //v_dump($arSection["PREVIEW_PICTURE"]);
		}
		$arButtons = CIBlock::GetPanelButtons(
			$arSection["IBLOCK_ID"],
			0,
			$arSection["ID"],
			array("SESSID"=>false)
		);
		$arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
		$arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];
                $arSection["DESCRIPTION"] = TruncateText(strip_tags($arSection["DESCRIPTION"]), $arParams["DESCRIPTION_TRUNCATE_LEN"]);
                
		$r = CIBlockSection::GetByID($arSection["IBLOCK_SECTION_ID"]);
		if($ar = $r->GetNext())
                {
			$arSection["IBLOCK_SECTION_CODE"] = $ar["CODE"];
                        $arSection["PARENT_SECTION"]["SECTION_ID"] = $ar["IBLOCK_SECTION_ID"];
                }
		$arResult["ITEMS"][]=$arSection;
	}

	$arResult["SECTIONS_COUNT"] = count($arResult["SECTIONS"]);
        $arResult["NAV_STRING"] = $rsSections->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
        $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
        $arResult["NAV_RESULT"] = $rsSections;

	$this->SetResultCacheKeys(array(
		"SECTIONS_COUNT",
		"SECTION",
                "TITLE"
	));

	$this->IncludeComponentTemplate();
}
if ($arParams["SET_TITLE"]=="Y")
    $APPLICATION->SetTitle($arResult["TITLE"]);
if($arResult["SECTIONS_COUNT"] > 0 || isset($arResult["SECTION"]))
{
	if(
		$USER->IsAuthorized()
		&& $APPLICATION->GetShowIncludeAreas()
		&& CModule::IncludeModule("iblock")
	)
	{
		$UrlDeleteSectionButton = "";
		if(isset($arResult["SECTION"]) && $arResult["SECTION"]['IBLOCK_SECTION_ID'] > 0)
		{
			$rsSection = CIBlockSection::GetList(
				array(),
				array("=ID" => $arResult["SECTION"]['IBLOCK_SECTION_ID']),
				false,
				array("SECTION_PAGE_URL")
			);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arSection = $rsSection->GetNext();
			$UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
		}

		if(empty($UrlDeleteSectionButton))
		{
			$url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
			$arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
			$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
			$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
		}

		$arReturnUrl = array(
			"add_section" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"add_element" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"delete_section" => $UrlDeleteSectionButton,
		);
		$arButtons = CIBlock::GetPanelButtons(
			$arParams["IBLOCK_ID"],
			0,
			$arResult["SECTION"]["ID"],
			array("RETURN_URL" =>  $arReturnUrl)
		);

		$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
}
?>
