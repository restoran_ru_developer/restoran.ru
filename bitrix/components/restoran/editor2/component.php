<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(substr_count($arParams["IBLOCK_ID"], "|")>0) $arParams["IBLOCK_ID"]= explode("|", $arParams["IBLOCK_ID"]);

//var_dump($arParams["IBLOCK_ID"]);

if($this->StartResultCache(false, array($arNavigation, $arParams["ELEMENT_ID"], $arParams["IBLOCK_ID"],$arParams["PARENT_SECTION"],$USER->GetID(),$USER->GetUserGroupString())))
{
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
        
	
	
	//редактируем уже существующий элемент
	if($arParams["ELEMENT_ID"]>0){
		$res = CIBlockElement::GetByID($arParams["ELEMENT_ID"]);
		if($ob = $res->GetNextElement()){
			$arResult = $ob->GetFields();
			//var_dump($arResult);
			$arResult["PROPERTIES"] = $ob->GetProperties();
			
			
			
			$res_ib = CIBlock::GetByID($arResult["IBLOCK_ID"]);
			if($ar_res_ib = $res_ib->GetNext()){
				//var_dump($ar_res_ib);
				$arResult["IBLOCK_TYPE"]=$ar_res_ib["IBLOCK_TYPE_ID"];
			
			}

			
			
			$ar_res_p = CPrice::GetBasePrice($arParams["ELEMENT_ID"]);
			$arResult["PRICES"]["BASE"]=$ar_res_p["PRICE"];
			
			if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && $arResult["CREATED_BY"]!=$USER->GetID()){
				ShowError("Ошибка доступа");
				return;
			}
		}else{
			ShowError("Такой публикации не существует");
			@define("ERROR_404", "Y");
			if($arParams["SET_STATUS_404"]==="Y")
				CHTTP::SetStatus("404 Not Found");
			return;
		
		}
		
		//var_dump($ar_res_p);
	}
	
	
	//берем инфу по пользователю
	$rsUser = CUser::GetByID($USER->GetID());
	$arResult["USER"] = $rsUser->Fetch();
	
	
	//КЭШИРОВАНИЕ
	$obCache = new CPHPCache;

	$KEY="dsjhfkjsgfhf1g23d_2";
	$life_time = 360000; 
	$cache_id = $arParams["PARENT_SECTION"].$arParams["IBLOCK_ID"].$arParams["TAG_SECTION"].$KEY.$USER->GetID(); 


	if($obCache->InitCache($life_time, $cache_id, "/redactor_vars/")){
		$vars = $obCache->GetVars();
    	$arRES["SECTIONS"] = $vars["SECTIONS"];
    	$arRES["PROPS"] = $vars["PROPS"];
    	$arRES["ALL_TAGS"] = $vars["ALL_TAGS"];
	}else{
		//Получаем список разделов в секции для выбора родительского раздела
		$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$arParams["PARENT_SECTION"]);
  		$db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), $arFilter, true);
		while($ar_result = $db_list->GetNext()){
    		$arRES["SECTIONS"][]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"]);
  		}
		
		
			
		//теперь смотрим, если в инфоблоке есть свойства типа список или привязка к элементам инфоблока, то забираем их значения
		//Привязка к элементам

		$properties = CIBlockProperty::GetList(Array("name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"],"PROPERTY_TYPE"=>"E"));
		while ($prop_fields = $properties->GetNext()){
  			$VALS=array();
  			//Теперь нужно выбрать все элементы из инфоблока
                        if ($prop_fields["CODE"]=="RESTORAN"&&!$prop_fields["LINK_IBLOCK_ID"])
                            $arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"catalog");
                        else
                            $arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"]);
                        if (CSite::InGroup(Array(9))&&!CSite::InGroup(Array(1,15,14)))
                        {
                            $arFilter_prl["PROPERTY_user_bind"] = $USER->GetID();
                            //comment this
                            unset($arFilter_prl["ACTIVE"]);
                        }
                        
                        if ($prop_fields["LINK_IBLOCK_ID"]=='139')
                            $arFilter_prl["SECTION_ID"] = 57432;
            if($prop_fields["CODE"]=="RESTORAN"){
                $res_prl = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter_prl, false, false, array('PROPERTY_address','ID'));
            }
            else {
                $res_prl = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter_prl, false);
            }

            if($prop_fields["CODE"]=="RESTORAN"){
                while($ob_prl = $res_prl->GetNext()){
                    $sec="";
                    $res = CIBlockSection::GetByID($ob_prl["IBLOCK_SECTION_ID"]);
                    if($ar_res = $res->GetNext())
                        $sec = " [".$ar_res['NAME']."]";
                    $VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"].$sec.' '.$ob_prl['PROPERTY_ADDRESS_VALUE']);//
                }
            }
            else {
                while($ob_prl = $res_prl->GetNext()){
                    $sec="";
                    $res = CIBlockSection::GetByID($ob_prl["IBLOCK_SECTION_ID"]);
                    if($ar_res = $res->GetNext())
                        $sec = " [".$ar_res['NAME']."]";
                    $VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"].$sec);
                    //$VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"]);
                }
            }


		
		
		
			//если привязка к ресторану, то нужно запросить еще и фирмы
			if($prop_fields["CODE"]=="RESTORAN" && CSite::InGroup(Array(15,16,20,23,27,35,34))){
				$arIIIB = getArIblock("firms", CITY_ID);
                if ($arIIIB["ID"])
                {
                    $arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"firms","IBLOCK_ID"=>$arIIIB["ID"]);
                    $res_prl = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter_prl, false);
                    while($ob_prl = $res_prl->GetNext()){
                        $VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"]);
                    }
                }
			}		
  			$arRES["PROPS"][$prop_fields["CODE"]]=array("ID"=>$prop_fields["ID"], "NAME"=>$prop_fields["NAME"], "LIST"=>$VALS);	
		} 

		//СПИСКИ
		$properties = CIBlockProperty::GetList(Array("name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"],"PROPERTY_TYPE"=>"L"));
		while ($prop_fields = $properties->GetNext()){
  			$VALS=array();
  			//var_dump($prop_fields);
  		
  			$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>$prop_fields["CODE"]));
			while($ob_prl = $property_enums->GetNext()){
				$VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["VALUE"]);
			}
  			$arRES["PROPS"][$prop_fields["CODE"]]=array("ID"=>$prop_fields["ID"], "NAME"=>$prop_fields["NAME"], "LIST"=>$VALS);	
		} 
	
		$arRES["PROPS"]["ACTIVE"]=array("LIST"=>array(
			array("ID"=>"Y", "NAME"=>"Да"), 
			array("ID"=>"N", "NAME"=>"Нет") 
		));
	
		//Нужно забрать список тэгов
		if(intval($arParams["TAG_SECTION"])>0){
			$arFilter = Array("IBLOCK_ID"=>99, "ACTIVE"=>"Y", "SECTION_ID"=>$arParams["TAG_SECTION"]);
			$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter, false);
			while($ar_fields = $res->GetNext()){
				$arRES["ALL_TAGS"][]=$ar_fields["NAME"];
			}
		}
		
		//Если у нас нет свойства "RESTORAN", то список ресторанов нужно получить отдельно
		//var_dump($arRES["PROPS"]["RESTORAN"]);
		if(!isset($arRES["PROPS"]["RESTORAN"])){
			$VALS=array();
			
  			//Теперь нужно выбрать все элементы из инфоблока
  			$arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"catalog");
			$res_prl = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_prl, false);
			while($ob_prl = $res_prl->GetNext()){
				$sec="";
				$res = CIBlockSection::GetByID($ob_prl["IBLOCK_SECTION_ID"]);
				if($ar_res = $res->GetNext())
					$sec = " [".$ar_res['NAME']."]";
				$VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"].$sec);       
			}
		
			//если привязка к ресторану, то нужно запросить еще и фирмы
			$arIIIB = getArIblock("firms", CITY_ID);	
			$arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"firms");
			$res_prl = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_prl, false);
			while($ob_prl = $res_prl->GetNext()){
				$VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"]);
			}
		
  			$arRES["PROPS"]["RESTORAN"]=array("ID"=>$prop_fields["ID"], "NAME"=>$prop_fields["NAME"], "LIST"=>$VALS);	
  			//var_dump($arResult["PROPS"]["RESTORAN"]);
		}
		
		
		
		if($obCache->StartDataCache()){
			$obCache->EndDataCache(array("SECTIONS"=>$arRES["SECTIONS"], "PROPS"=>$arRES["PROPS"],"ALL_TAGS"=>$arRES["ALL_TAGS"])); 
		}
	
		
	}
	
	
	
	$arResult["SECTIONS"]=$arRES["SECTIONS"];
	$arResult["PROPS"]=$arRES["PROPS"];
	$arResult["ALL_TAGS"]=$arRES["ALL_TAGS"];
	
		
	
	
		
	
	
	$this->SetResultCacheKeys(array(
		"ID",
		"IBLOCK_ID",
		"NAV_CACHED_DATA",
		"NAME",
		"IBLOCK_SECTION_ID",
		"IBLOCK",
		"LIST_PAGE_URL", 
		"~LIST_PAGE_URL",
		"SECTION",
		"PROPERTIES",
		"SORT"
	));

	$this->IncludeComponentTemplate();

}
//var_dump($arParams);
?>