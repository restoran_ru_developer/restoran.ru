<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("BP_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("BP_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 170,
	"PATH" => array(
		"ID" => "content_restoran",
        "CHILD" => array(
            "ID" => "restoran_blog",
            "NAME" => GetMessage("RESTAURANT_BLOG"),
                "CHILD" => array(
                    "ID" => "restoran_blog_post",
                    "NAME" => GetMessage("RESTAURANT_BLOG_POST"),
            ),
        )
	),
);
?>