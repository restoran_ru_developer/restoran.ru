<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<table width="100%" cellpadding="10" cellspacing="0" class="rest_list">
    <tr>
        <th width="24">Акт.</th>
        <th>Название</th>
        <th width="65">Лайки <a href="javascript:void(0)" class="help_ico" title="Количество плюсов/Количество минусов"></a></th>        
        <th width="100">Просмотры <a href="javascript:void(0)" class="help_ico" title="Количество просмотром карточки ресторана за день/за все время"></a></th>
        <th width="75">Отзывы <a href="javascript:void(0)" class="help_ico" title="Количество отзывов"></a></th>
        <th></th>
    </tr>
<?foreach($arResult["REST"] as $arItem):?>
    <tr>
        <td align="center">
            <img width="16" src="<?=$templateFolder?>/images/<?=($arItem["ACTIVE"]=="Y")?"lamp_active.png":"lamp_inactive.png"?>" />
        </td>
        <td>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><?=$arItem["NAME"]?></a>
        </td>
        <td align="center">            
            <?=($arItem["PROPERTY_PLUS_VALUE"])?$arItem["PROPERTY_PLUS_VALUE"]:0?>
            /<?=($arItem["PROPERTY_MINUS_VALUE"])?$arItem["PROPERTY_MINUS_VALUE"]:0?>
        </td>       
        <td align="center">
            <?=($arItem["SHOW_COUNTER"])?$arItem["SHOW_COUNTER"]:0?>
            /
            <?=($arItem["PROPERTY_STAT_DAY_VALUE"])?$arItem["PROPERTY_STAT_DAY_VALUE"]:0?>
        </td>
        <td align="center"><a target="_blank" href="<?=str_replace("detailed","opinions",$arItem["DETAIL_PAGE_URL"])?>"><?=($arItem["PROPERTY_COMMENTS_VALUE"])?$arItem["PROPERTY_COMMENTS_VALUE"]:0?></a></td>
        <td>
            <a href="/businessman/firm_edit.php?ID=<?=$arItem["ID"]?>" class="edit_button" title="Редактировать анкету"></a>
            <!--<a href="/businessman/menu_edit.php?RESTORAN=<?=$arItem["ID"]?>" class="menu_button" title="Редактировать меню ресторана"></a>-->
        </td>
    </tr>
<?endforeach;?>
</table>
</div>
