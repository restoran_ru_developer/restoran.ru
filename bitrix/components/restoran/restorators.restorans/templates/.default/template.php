<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="tech">
    <?=GetMessage('tp-message')?>
</div>
<script>
    $(function(){
        $('.copy-restoran-button').on('click',function(){
            result = confirm('Вы уверенны, что хотите копировать ресторан ?');
            if(result){
                return true;
            }
            return false;
        })
    })
</script>
<div class="news-list">
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?><br />
    <?endif;?>
    <?if (CSite::InGroup(Array(9))):
        $s = "restorator";
    else:
        $s = "redactor";
    endif;
    ?>
    <table width="100%" cellpadding="10" cellspacing="0" class="rest_list">
        <tr>
            <!--        <th width="24">Акт.</th>-->
            <th><?=GetMessage('RESTORATOR_REST_S_NAME')?></th>
            <th width="65"><?=GetMessage('RESTORATOR_REST_S_LIKES')?> <a href="javascript:void(0)" class="help_ico" title="<?=GetMessage('RESTORATOR_REST_S_plus_min_num')?>"></a></th>
            <th width="75"><?=GetMessage('RESTORATOR_REST_S_RATING')?> <a href="javascript:void(0)" class="help_ico" title="<?=GetMessage('RESTORATOR_REST_S_rest_rating')?>"></a></th>
            <th width="100"><?=GetMessage('RESTORATOR_REST_S_WATCHES')?> <a href="javascript:void(0)" class="help_ico" title="<?=GetMessage('RESTORATOR_REST_S_watches_per_day')?>"></a></th>
            <th width="75"><?=GetMessage('RESTORATOR_REST_S_REVIEWS')?> <a href="javascript:void(0)" class="help_ico" title="<?=GetMessage('RESTORATOR_REST_S_comment_num')?>"></a></th>
            <th width="160" style="text-align: center"><?=GetMessage('RESTORATOR_REST_S_DOINGS')?></th>
        </tr>
        <?foreach($arResult["REST"] as $arItem):?>
            <tr <?=($arItem["ACTIVE"]!="Y")?"class='nobor'":""?>>
                <!--        <td align="center">
            <img width="16" src="<?=$templateFolder?>/images/<?=($arItem["ACTIVE"]=="Y")?"lamp_active.png":"lamp_inactive.png"?>" />
        </td>-->
                <td>
                    <?if ($arItem["ACTIVE"]=="Y"):?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><?=$arItem["NAME"]?></a>
                    <?else:?>
                        <a href="/<?=$s?>/rest_preview.php?ID=<?=$arItem["ID"]?>&IBLOCK_ID=<?=$arItem["IBLOCK_ID"]?>" target="_blank"><?=$arItem["NAME"]?></a> <small><?=GetMessage('RESTORATOR_REST_S_preview_title')?></small>
                    <?endif;?>
                    <br />
                    <small><?=GetMessage('RESTORATOR_REST_S_active_from')?><?=$arItem["ACTIVE_FROM"]?> <?=GetMessage('RESTORATOR_REST_S_to')?><?=$arItem["ACTIVE_TO"]?></small>
                    <Br />
                    <small><?=GetMessage('RESTORATOR_REST_S_status_title')?><b><?=($arItem["ACTIVE"]=="N")?GetMessage('RESTORATOR_REST_S_not_active'):GetMessage('RESTORATOR_REST_S_active')?></b></small>
                </td>
                <td align="center">
                    <?=($arItem["PROPERTY_PLUS_VALUE"])?$arItem["PROPERTY_PLUS_VALUE"]:0?>
                    /<?=($arItem["PROPERTY_MINUS_VALUE"])?$arItem["PROPERTY_MINUS_VALUE"]:0?>
                </td>
                <td align="center">
                    <?=($arItem["PROPERTY_RATIO_VALUE"])?$arItem["PROPERTY_RATIO_VALUE"]:0?>
                </td>
                <td align="center">
                    <?=($arItem["SHOW_COUNTER"])?$arItem["SHOW_COUNTER"]:0?>
                    /
                    <?=($arItem["PROPERTY_STAT_DAY_VALUE"])?$arItem["PROPERTY_STAT_DAY_VALUE"]:0?>
                </td>
                <td align="center"><a target="_blank" href="<?=str_replace("detailed","opinions",$arItem["DETAIL_PAGE_URL"])?>"><?=($arItem["PROPERTY_COMMENTS_VALUE"])?$arItem["PROPERTY_COMMENTS_VALUE"]:0?></a></td>
                <td align="right">
                    <a href="/<?=$s?>/edit.php?ID=<?=$arItem["ID"]?>" class="edit_button" title="<?=GetMessage('RESTORATOR_REST_S_edit_questionnaire')?>"><?=GetMessage('RESTORATOR_REST_S_edit_questionnaire')?></a>
                    <a href="/<?=$s?>/menu_edit.php?RESTORAN=<?=$arItem["ID"]?>" class="menu_button" title="<?=GetMessage('RESTORATOR_REST_S_edit_restaurant_menu')?>"><?=GetMessage('RESTORATOR_REST_S_edit_menu')?></a>
                    <?if (CSite::InGroup(Array(9))):?>
                        <a href="/<?=$s?>/blog/#/restorator/blog/afisha.php" class="menu_button" title="<?=GetMessage('RESTORATOR_REST_S_edit_restaurant_poster')?>"><?=GetMessage('RESTORATOR_REST_S_restaurant_poster')?></a>
                        <a href="/<?=$s?>/blog/#/restorator/blog/blog.php" class="menu_button" title="<?=GetMessage('RESTORATOR_REST_S_edit_restaurant_blog')?>"><?=GetMessage('RESTORATOR_REST_S_restaurant_blog')?></a>
                        <a href="/<?=$s?>/opinions.php?CODE=<?=$arItem["CODE"]?>" class="menu_button" title="<?=GetMessage('RESTORATOR_REST_S_watch_comments')?>"><?=GetMessage('RESTORATOR_REST_S_watch_comments')?></a>
                    <?endif;?>
                    <?if (CSite::InGroup(Array(1))):?>
                        <a href="/redactor/rest_copy.php?ID=<?=$arItem["ID"]?>&rest_copy=Y" class="menu_button copy-restoran-button" title="" style="text-transform: uppercase;text-decoration: underline;">Копировать ресторан</a>
                    <?endif?>
                </td>
            </tr>
            <?if ($arItem["ACTIVE"]!="Y"):?>
                <tr>
                    <td colspan="6" style="color:#24a6cf; font-style:italic"><?=GetMessage('RESTORATOR_REST_S_Your_institution_will_be_placed')?><Br />
                        <ol>
                            <li><?=GetMessage('RESTORATOR_REST_S_PHONE_TITLE')?></li>
                            <li><?=GetMessage('RESTORATOR_REST_S_DATA_ABOUT_LOCATION')?></li>
                            <li><?=GetMessage('RESTORATOR_REST_S_logo_or_photo')?></li>
                            <li><?=GetMessage('RESTORATOR_REST_S_average_bill')?></li>
                            <li><?=GetMessage('RESTORATOR_REST_S_kitchen_title')?></li>
                        </ol>
                        <?=GetMessage('RESTORATOR_REST_S_about_another_params')?>

                    </td>
                </tr>
            <?endif;?>
        <?endforeach;?>
    </table>
</div>
