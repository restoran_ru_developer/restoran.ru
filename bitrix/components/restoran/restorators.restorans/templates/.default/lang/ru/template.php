<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["tp-message"] = "По вопросам технической поддержки: perevod@restoran.ru, телефон: (812) 337-55-30, с 10.00 до 18.00 по будням.";
$MESS["RESTORATOR_REST_S_NAME"] = "Название";
$MESS["RESTORATOR_REST_S_LIKES"] = "Лайки";
$MESS["RESTORATOR_REST_S_RATING"] = "Рейтинг";
$MESS["RESTORATOR_REST_S_WATCHES"] = "Просмотры";
$MESS["RESTORATOR_REST_S_REVIEWS"] = "Отзывы";
$MESS["RESTORATOR_REST_S_DOINGS"] = "Действия";
$MESS["RESTORATOR_REST_S_Your_institution_will_be_placed"] = "Ваше заведение будет размещено в каталоге Ресторан.ру при условии заполнения обязательных полей:";
$MESS["RESTORATOR_REST_S_PHONE_TITLE"] = "Телефон";
$MESS["RESTORATOR_REST_S_DATA_ABOUT_LOCATION"] = "Данные о местоположении (район, если есть, метро, если есть, округ, если есть, или иное), отметка на карте";
$MESS["RESTORATOR_REST_S_logo_or_photo"] = "Фотография или логотип (количество фотографий не более 20)";
$MESS["RESTORATOR_REST_S_average_bill"] = "Средний счет";
$MESS["RESTORATOR_REST_S_kitchen_title"] = "Кухня";
$MESS["RESTORATOR_REST_S_about_another_params"] = "Заполнение других параметров анкеты – на ваше усмотрение.
            Присвоение статуса активности происходит в течение 24 часов по будним
            дням";
$MESS["RESTORATOR_REST_S_not_active"] = 'Не активен';
$MESS["RESTORATOR_REST_S_active"] = 'активен';


$MESS["RESTORATOR_REST_S_plus_min_num"] = 'Количество плюсов/Количество минусов';
$MESS["RESTORATOR_REST_S_rest_rating"] = 'Рейтинг ресторанов';
$MESS["RESTORATOR_REST_S_watches_per_day"] = 'Количество просмотром карточки ресторана за день/за все время';
$MESS["RESTORATOR_REST_S_comment_num"] = 'Количество отзывов';
$MESS["RESTORATOR_REST_S_preview_title"] = '[Предпросмотр]';
$MESS["RESTORATOR_REST_S_active_from"] = 'Активность с ';
$MESS["RESTORATOR_REST_S_to"] = 'по ';
$MESS["RESTORATOR_REST_S_status_title"] = 'Статус: ';
$MESS["RESTORATOR_REST_S_edit_questionnaire"] = 'Редактировать анкету';
$MESS["RESTORATOR_REST_S_edit_restaurant_menu"] = 'Редактировать меню ресторана';
$MESS["RESTORATOR_REST_S_edit_menu"] = 'Редактировать меню';
$MESS["RESTORATOR_REST_S_edit_restaurant_poster"] = 'Редактировать афишу ресторана';
$MESS["RESTORATOR_REST_S_restaurant_poster"] = 'Афиша ресторана';
$MESS["RESTORATOR_REST_S_edit_restaurant_blog"] = 'Редактировать блог ресторана';
$MESS["RESTORATOR_REST_S_restaurant_blog"] = 'Блог ресторана';
$MESS["RESTORATOR_REST_S_watch_comments"] = 'Смотреть отзывы';

?>