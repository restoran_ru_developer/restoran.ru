<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["tp-message"] = "For technical support: perevod@restoran.ru, phone: (812) 337-55-30, from 10.00 to 18.00 on weekdays.";
$MESS["RESTORATOR_REST_S_NAME"] = "Name";
$MESS["RESTORATOR_REST_S_LIKES"] = "Likes";
$MESS["RESTORATOR_REST_S_RATING"] = "Rating";
$MESS["RESTORATOR_REST_S_WATCHES"] = "Views";
$MESS["RESTORATOR_REST_S_REVIEWS"] = "Reviews";
$MESS["RESTORATOR_REST_S_DOINGS"] = "Actions";
$MESS["RESTORATOR_REST_S_Your_institution_will_be_placed"] = "Your institution will be placed in the directory Restoran.ru The provided the required fields:";
$MESS["RESTORATOR_REST_S_PHONE_TITLE"] = "Phone";
$MESS["RESTORATOR_REST_S_DATA_ABOUT_LOCATION"] = "Location data (area, if any, subway, if any, county, if any, or otherwise), a mark on the map";
$MESS["RESTORATOR_REST_S_logo_or_photo"] = "Photo or logo (the number of photos no more than 20)";
$MESS["RESTORATOR_REST_S_average_bill"] = "The average bill";
$MESS["RESTORATOR_REST_S_kitchen_title"] = "Kitchen";
$MESS["RESTORATOR_REST_S_about_another_params"] = "Filling other parameters profiles - at your discretion.
             Assignment of the activity takes place within 24 hours on weekdays
             day";
$MESS["RESTORATOR_REST_S_not_active"] = 'Not active';
$MESS["RESTORATOR_REST_S_active"] = 'Active';

$MESS["RESTORATOR_REST_S_plus_min_num"] = 'Number of pros / cons Number';
$MESS["RESTORATOR_REST_S_rest_rating"] = 'Rating of restaurants';
$MESS["RESTORATOR_REST_S_watches_per_day"] = 'Number of views cards restaurant per day / all-time';
$MESS["RESTORATOR_REST_S_comment_num"] = 'Number of reviews';
$MESS["RESTORATOR_REST_S_preview_title"] = '[Preview]';
$MESS["RESTORATOR_REST_S_active_from"] = 'Activity from ';
$MESS["RESTORATOR_REST_S_to"] = 'by ';
$MESS["RESTORATOR_REST_S_status_title"] = 'Status: ';
$MESS["RESTORATOR_REST_S_edit_questionnaire"] = 'Edit profile';
$MESS["RESTORATOR_REST_S_edit_restaurant_menu"] = 'Edit menu of the restaurant';
$MESS["RESTORATOR_REST_S_edit_menu"] = 'Edit Menu';
$MESS["RESTORATOR_REST_S_edit_restaurant_poster"] = 'Edit poster restaurant';
$MESS["RESTORATOR_REST_S_restaurant_poster"] = 'Poster restaurant';
$MESS["RESTORATOR_REST_S_edit_restaurant_blog"] = 'Edit restaurant blog';
$MESS["RESTORATOR_REST_S_restaurant_blog"] = 'Restaurant blog';
$MESS["RESTORATOR_REST_S_watch_comments"] = 'See comments';

?>