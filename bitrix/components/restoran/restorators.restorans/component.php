<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 0;


if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N" ? false: $USER->GetGroups()))))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
        global $USER;
        $arResult = array();
        
        $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"],"SITE_ID"=>SITE_ID,"PROPERTY_user_bind"=>$USER->GetID()),false,false,Array("ID","ACTIVE","ACTIVE_FROM","ACTIVE_TO","CODE","NAME","PROPERTY_plus","PROPERTY_minus","SHOW_COUNTER","PROPERTY_RATIO","PROPERTY_COMMENTS","PROPERTY_stat_day","DETAIL_PAGE_URL"));
        while ($ar = $res->GetNext())
        {
            $arResult["REST"][] = $ar;
        }        
        $this->SetResultCacheKeys(array(
            "REST"                
        ));
        $this->IncludeComponentTemplate();
}
?>