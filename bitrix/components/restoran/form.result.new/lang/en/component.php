<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS ['FORM_ADD'] = "Add";
$MESS ['FORM_ACCESS_DENIED'] = "Web-form access denied.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you. Your application form #";
$MESS ['FORM_DATA_SAVED2'] = " was received.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Web-form module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "Web-form is not found.";
$MESS ['FORM_PUBLIC_ICON_EDIT_TPL'] = "Edit Web-form template";
$MESS ['FORM_PUBLIC_ICON_EDIT'] = "Edit Web-form parameters";
$MESS ['FORM_NOTE_ADDOK'] = 'Thank you. We will confirm your booking in a couple of minutes.';
$MESS ['FORM_NOTE_ADDOK_22'] = "The Restoran.Ru booking service is available from 10 am to 10 pm. We'll contact you in our working hours.";

//$MESS ['FORM_NOTE_ADDOK'] = "Thank you.Your application form #RESULT_ID# was received.";
?>