<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>

    $(document).ready(function(){
        
        if($.browser.safari){
            browser = 'safari';
        }
        if($.browser.opera){
            browser = 'opera';
        }
        if($.browser.chrome){
            browser = 'chrome';
        }
        $(".ui-input-search").find('input').val($('#autocomplete-restaurant-name').val());
        
        $('#dateinput').val('<? echo ConvertDateTime(ConvertTimeStamp(time(), "SHORT", "ru"), "YYYY-MM-DD", "ru"); ?>');
        
        $("#bron_form_my").submit(function(){                        
            if ($("input[name='form_text_40']").val().length!=10)
            {
                alert("Телефон должен содержать 10 цифр.\nНапример:9211234455");
                return false;
            }        
        });
        
    });
    
    $( document ).on( "mobileinit", function() {
        $.mobile.panel.prototype.options.initSelector = ".mypanel";
    });
</script>

<link href="<?= $templateFolder ?>/style.css?7"  type="text/css" rel="stylesheet" />
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <div id="order_online" class="order_online_mobile">
        <div class="ajax_form">
        <? endif; ?>
        <form id="bron_form_my" name="SIMPLE_FORM_3" action="/online_order.php" data-ajax="false" method="POST" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="WEB_FORM_ID" value="3">   
        <? /* if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
          <?if ($arResult["isFormTitle"]):?>
          <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
          <?endif;?>
          <?endif; */ ?>        
        <div class="ok" style="color:#000"><?= $arResult["FORM_NOTE"] ?></div>
        <div class="font12" id="err">
            <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
        </div>
        <span class="font18-container">
        <? if (CITY_ID == 'spb'): ?>
            <a data-role="button" data-theme="e" data-ajax="false" href="tel:+78127401820"><?= $distance ?>Забронировать по телефону</a>
        <? endif; ?>
        <? if (CITY_ID == 'msk'): ?>
            <a data-role="button" data-theme="e" data-ajax="false" href="tel:+74959882656"><?= $distance ?>Забронировать по телефону</a>
        <? endif; ?>
</span>
        <h1 style="text-align:center;color:black; margin-bottom:0px; font-size: 16px;">Выберите ресторан</h1>
        <script>
            $( "body" ).on( "listviewbeforefilter", "#autocomplete4", function ( e, data ) {
                var $ul = $( this ),
                    $input = $( data.input ),
                    value = $input.val(),
                    html = "";
                $ul.html( "" );
                if ( value && value.length > 2 ) {
                    $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
                    $ul.listview( "refresh" );
                    $.ajax({
                        url: "/suggest.php",
                        dataType: "json",                
                        data: {
                            q: $input.val()
                        }
                    })
                    .then( function ( response ) {
                        $.each( response, function ( i, val ) {
                            html += "<li onclick='set_rest(\""+val.NAME+"\","+val.ID+")'>" + val.NAME + "</li>";
                        });
                        $ul.html( html );
                        $ul.listview( "refresh" );
                        $ul.trigger( "updatelayout");
                    });
                }
            });  
            function set_rest(name,id,obj)
            {
                $("#autocomplete-restaurant-name").val(name);
                $("#autocomplete-restaurant-id").val(id);
                $("#autocomplete4").html("");                
                $("#autocomplete4").prev().find("input").val(name);
            }
            </script>
            <br />
            <?
            if ($_REQUEST["form_text_44"])
                $_REQUEST["RESTOURANT"] = $_REQUEST["form_text_44"];
            if ($_REQUEST["form_text_32"])
                $arResult["REST"] = $_REQUEST["form_text_32"];
            ?>
        <ul id="autocomplete4" data-corners="false" data-shadow="false" data-iconshadow="false" data-role="listview" data-inset="true" data-filter="true" data-filter-placeholder="Поиск ресторана..." data-filter-theme="b"></ul>
        <input type="hidden" value="<?=$arResult["REST"]?>" name="form_text_32" id="autocomplete-restaurant-name">
        <input type="hidden" value="<?=$_REQUEST["RESTOURANT"]?>" name="form_text_44" id="autocomplete-restaurant-id">
<!--        <div class="question autocomplete-question">
            <?
//            $APPLICATION->IncludeComponent("restoran:restoraunts.list", "rest_list_select", Array(
//                "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
//                "IBLOCK_ID" => CITY_ID, // Код информационного блока
//                "PARENT_SECTION_CODE" => "restaurants", // Код раздела
//                "NEWS_COUNT" => 10000, // Количество ресторанов на странице
//                "SORT_BY1" => "NAME", // Поле для первой сортировки ресторанов
//                "SORT_ORDER1" => "ASC", // Направление для первой сортировки ресторанов
//                "SORT_BY2" => "SORT", // Поле для второй сортировки ресторанов
//                "SORT_ORDER2" => "ASC", // Направление для второй сортировки ресторанов
//                "FILTER_NAME" => "arrFilter", // Фильтр
//                "PROPERTY_CODE" => array(),
//                "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
//                "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
//                "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
//                "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
//                "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
//                "AJAX_MODE" => "N", // Включить режим AJAX
//                "AJAX_OPTION_SHADOW" => "Y",
//                "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
//                "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
//                "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
//                "CACHE_TYPE" => "N", // Тип кеширования
//                //"CACHE_TIME" => "21600", // Время кеширования (сек.)
//                "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
//                "CACHE_GROUPS" => "Y", // Учитывать права доступа
//                "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
//                "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
//                "SET_TITLE" => "Y", // Устанавливать заголовок страницы
//                "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
//                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
//                "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
//                "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
//                "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
//                "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
//                "PAGER_TITLE" => "Рестораны", // Название категорий
//                "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
//                "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
//                "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
//                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
//                "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
//                "DISPLAY_DATE" => "Y", // Выводить дату элемента
//                "DISPLAY_NAME" => "Y", // Выводить название элемента
//                "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
//                "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
//                "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
//                    ), false
//            );
            ?>
        </div>-->
        <?
        foreach ($arResult["QUESTIONS"] as $key => $question):
            $ar = array();
            $ar["CODE"] = $key;
            $questions[] = array_merge($question, $ar);
        endforeach;
        ?>

        <div class="clear"></div>

        <div class="question-block50 date-block">
            <div class="question-input">
                <input data-theme="b" type="date" id="dateinput" class="text" value="<?=$_REQUEST["form_".$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[2]["STRUCTURE"][0]["ID"]]?>" name="form_<?= $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[2]["STRUCTURE"][0]["ID"] ?>"/>
            </div>
            <div class="question-label">Дата:</div>
        </div>
        <div class="question-block50 time-block">
            <div class="question-input">
                <input data-theme="b" type="time" placeholder="19:00" class="text" value="<?=($_REQUEST["form_".$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[3]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[3]["STRUCTURE"][0]["ID"]]:"19:00"?>" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>"/>
            </div>
            <div class="question-label">Время:</div>
        </div>
        <div class="clear"></div>
        <div class="question-block">
            <div class="question-input">
                <input placeholder="Имя Фамилия" type="text" data-theme="b" class="search_rest" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />
            </div>
            <div class="question-label">Имя:</div>
        </div>
        <div class="clear"></div>
        <?
        $rsUser = CUser::GetByID($USER->GetID());
        $ar = $rsUser->Fetch();
        $ar["PERSONAL_PHONE"] = str_replace(" ", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace("-", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace("(", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace(")", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);
        $phone = $ar["PERSONAL_PHONE"];
        $ar["PERSONAL_PHONE"] = substr($phone, 0, 3) . " " . substr($phone, 3, 3) . "  " . substr($phone, 6, 2) . "  " . substr($phone, 8, 2);
        ?>
        <div class="clear"></div>
        <div class="question-block">
            <div class="question-input">
                <input data-theme="b" type="number" class="text" style="text-align:left" value="<?=($_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]]:$ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" placeholder="9211234567"/>
            </div>
            <div class="question-label">Телефон:</div>
        </div>
        <div class="clear"></div>
        <div class="question-block">
            <div class="question-input" style="width:50%">
                <input id="phone111" data-theme="b" type="number" class="text phone" style="text-align:left" value="<?=$_REQUEST["form_text_35"]?>" placeholder="2" name="form_text_35"/>
            </div>
            <div class="question-label">Кол-во персон:</div>
        </div>
        <div class="clear"></div>
        <? //var_dump($arResult["isUseCaptcha"])?>
        <? if ($arResult["isUseCaptcha"] == "Y"): ?>
            <div class="question">
                <?= GetMessage("CAPTCHA") ?>
            </div>
            <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') 370px 18px no-repeat;">
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
                <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")  ?><? //=$arResult["REQUIRED_SIGN"];  ?>
                <input type="text" data-theme="b" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
            </div>
        <? endif; ?>
        <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" style="width:100%;" class="online-order-radio">
            <input type="radio" name="form_checkbox_smoke" id="form_checkbox_smokea" value="65" <?=($_REQUEST["form_checkbox_smoke"]==65||!$_REQUEST["form_checkbox_smoke"])?"checked='checked'":""?> data-theme="d">
            <label for="form_checkbox_smokea">Некурящий зал</label>
            <input type="radio" name="form_checkbox_smoke" id="form_checkbox_smokeb" value="66" <?=($_REQUEST["form_checkbox_smoke"]==66)?"checked='checked'":""?>  data-theme="d">
            <label for="form_checkbox_smokeb">Курящий зал</label>
        </fieldset>
        <? if ($_REQUEST["invite"]): ?>
            <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
        <? endif; ?>
        <input type="hidden" value="29" name="form_dropdown_SIMPLE_QUESTION_547" id="what_question">
        <? if ($_REQUEST["CITY"]): ?>
            <input type="hidden" name="CITY" value="<?= $_REQUEST["CITY"] ?>" />
        <? else: ?>        
            <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
        <? endif; ?>

        <input type="hidden" name="web_form_apply" value="Y" />
        <input id="form_button" class="light_button"  data-theme="b" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?= strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]; ?>" />
        <div class="clear"></div>

        <?= $arResult["FORM_FOOTER"] ?>
        <?
//} //endif (isFormNote)
        ?>
        <? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
        </div>
    </div>
<? endif; ?>    