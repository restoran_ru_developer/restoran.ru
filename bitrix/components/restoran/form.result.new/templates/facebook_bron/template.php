<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script src="<?=$templateFolder?>/script1.js"></script>
<? //if ($arResult["isFormNote"] != "Y") {     ?>
<link href="<?= $templateFolder ?>/style.css?7"  type="text/css" rel="stylesheet" />
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <div id="order_online">
        <div class="ajax_form">
        <? endif; ?>
        <?= $arResult["FORM_HEADER"] ?>    
        <? /* if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
          <?if ($arResult["isFormTitle"]):?>
          <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
          <?endif;?>
          <?endif; */ ?>        
        <div class="ok"><?= $arResult["FORM_NOTE"] ?></div>
        <div class="font12" id="err">
            <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
        </div>
        <h1 style="text-align:center;color:#FFF; margin-bottom:0px;"><?= GetMessage("I_WANT") ?></h1>
        <? //v_dump($arResult["QUESTIONS"])?>
        <?
        foreach ($arResult["QUESTIONS"] as $key => $question):
            $ar = array();
            $ar["CODE"] = $key;
            $questions[] = array_merge($question, $ar);
        //var_dump(LANGUAGE_ID);
        endforeach;
        ?>
        <? if (LANGUAGE_ID == 'ru'): ?>
            <div class="question">
                <select id="what_question" style="width:220px;" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>">
                    <? foreach ($questions[0]["STRUCTURE"] as $key => $val): ?>
                        <option value="<?= $val["ID"] ?>" <?= ($key == $_REQUEST["what"]) ? "selected" : "" ?>><?= $val["MESSAGE"] ?></option>
                    <? endforeach; ?>
                </select>
                <?= GetMessage("IN_RESTORAN") ?>
            </div>
        <? endif; ?>
        <? if (LANGUAGE_ID == 'en'): ?>
            <input type="hidden" id="what_question_" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29" />
            <div class="question">
                make a reservation
                <?= GetMessage("IN_RESTORAN") ?>
            </div>
        <? endif; ?>
        <div class="question"  style="background:url('<?= $templateFolder ?>/images/s_star.png') right 7px no-repeat;">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:search.title", "bron", Array(
                "NAME" => "form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]
                    ), false
            );
            ?>
        </div>
        <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') 335px 7px no-repeat;">
            <?= GetMessage("NA") ?>
            <input type="text" class="datew" value="<?= ($_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]] : $_REQUEST["date"] ?>" name="form_<?= $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[2]["STRUCTURE"][0]["ID"] ?>"/>
            <?= GetMessage("AT") ?>
            <input type="text" class="time" value="<?= ($_REQUEST["form_" . $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[3]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[3]["STRUCTURE"][0]["ID"]] : $_REQUEST["time"] ?>" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>"/>
        </div>
        <div class="question">
            <?= GetMessage("OUR_COMPANY") ?>
            <input type="text" class="text" value="<?= ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"] ?>" size="4" maxlength="3" name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>" />
            <?= GetMessage("PEOPLE") ?>
        </div>
        <div class="question">
            <?= GetMessage("HOW_MUCH_BUY") ?>
            <input type="text" class="text" value="<?= $_REQUEST["form_" . $questions[5]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[5]["STRUCTURE"][0]["ID"]] ?>" size="8" maxlength="7" name="form_<?= $questions[5]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[5]["STRUCTURE"][0]["ID"] ?>" />
            <?= GetMessage("BY_PERSON") ?>
        </div>
        <div class="question">
            <?= GetMessage("MY_NAME") ?><br />
        </div>
        <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') right 7px no-repeat;">
            <input type="text" placeholder="<?=(LANGUAGE_ID=="en")?"Name":"Фамилия Имя"?>" class="search_rest" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />
        </div>
        <div class="question">
            <?= GetMessage("FOR_FEEDBACK") ?>
        </div>
        <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') 350px 7px no-repeat;">
            <?
            $rsUser = CUser::GetByID($USER->GetID());
            $ar = $rsUser->Fetch();
            $ar["PERSONAL_PHONE"] = str_replace(" ", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("-", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("(", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace(")", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);
            $phone = $ar["PERSONAL_PHONE"];
            $ar["PERSONAL_PHONE"] = substr($phone, 0, 3) . " " . substr($phone, 3, 3) . "  " . substr($phone, 6, 2) . "  " . substr($phone, 8, 2);
            ?>
            <?= GetMessage("MY_PHONE") ?> <span class="font18" style="color:#FFF"> +7 <input type="text" class="phone1" style="text-align:left" value="<?= $ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" /></span>
        </div>
        <div class="question">
            <?= GetMessage("MY_EMAIL") ?> <input type="text" class="text" value="<?= ($_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]] : $USER->GetEmail() ?>" name="form_<?= $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[8]["STRUCTURE"][0]["ID"] ?>" size="30" />
        </div>
        <div class="question">
            <?= GetMessage("MY_WISH") ?><br /> 
            <textarea class="bron_ta" name="form_<?= $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[11]["STRUCTURE"][0]["ID"] ?>">
                <?= trim($_REQUEST["form_" . $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[11]["STRUCTURE"][0]["ID"]]) ?>
            </textarea>    
            <table style="margin-top:10px;" align="center">
                <tbody>
                    <tr>
                        <td>
                            <input type="radio" id="s65" name="form_checkbox_smoke" value="65">
                        </td>
                        <td>
                            <label style="font-size:14px;" for="s65"> <?= GetMessage("NON_SMOKING_HALL") ?></label>
                        </td>
                        <td width="50"></td>
                        <td>
                            <input type="radio" id="s66" name="form_checkbox_smoke" value="66">
                        </td>
                        <td>
                            <label style="font-size:14px;" for="s66"> <?= GetMessage("SMOKING_HALL") ?></label>
                        </td>
                    </tr>
                </tbody>
            </table>        
        </div>
        <? if ($arResult["isUseCaptcha"] == "Y"): ?>
            <div class="question">
                <?= GetMessage("CAPTCHA") ?>
            </div>
            <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') 370px 18px no-repeat;">
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
                <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")   ?><? //=$arResult["REQUIRED_SIGN"];   ?>
                <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
            </div>
        <? endif; ?>
        <Br />
        <div class="inv_cph">
            <input type="checkbox" name="lst_nm" value="1" />
            <input type="checkbox" name="scd_nm" value="1" checked="checked" />
        </div>
        <? if ($_REQUEST["invite"]): ?>
            <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
        <? endif; ?>
        <? if ($_REQUEST["CITY"]): ?>
            <input type="hidden" name="CITY" value="<?= $_REQUEST["CITY"] ?>" />
        <? else: ?>        
            <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
        <? endif; ?>
        <input type="hidden" name="web_form_apply" value="Y" />
        <div class="clear"></div>
        <div class="question"><?= GetMessage("SPEAK_ENGLISH") ?></div>
        <div class="clear"></div>

        <div class="left">
            <p class="another_color font12" style="line-height:24px;">* — <?= GetMessage("FORM_REQUIRED_FIELDS") ?></p>
        </div>



        <div class="right">
            <input class="light_button" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?= GetMessage("RESERVE") ?>" />
            <!--<input class="dark_button popup_close" type="button" value="закрыть"/>-->
        </div>
        <div class="clear"></div>
        <? /* if ($arResult["F_RIGHT"] >= 15):?>
          &nbsp;<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />
          <?endif; */ ?>
        <!--&nbsp;<input type="reset" value="<?= GetMessage("FORM_RESET"); ?>" />-->
    <!--<p><?= $arResult["REQUIRED_SIGN"]; ?> - <?= GetMessage("FORM_REQUIRED_FIELDS") ?></p>-->
        <?= $arResult["FORM_FOOTER"] ?>
        <?
//} //endif (isFormNote)
        ?>
        <? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
        </div>
    </div>
<? endif; ?>