<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?$ar = array();?>
<? if ($arResult["isFormErrors"] != "Y"): ?>
    <?$ar["text"] = strip_tags($arResult["FORM_NOTE"]);?>
    <?$ar["er"] = 0;?>
<?endif?>
<? if ($arResult["isFormErrors"] == "Y"): ?>
    <?//=json_encode($arResult["FORM_ERRORS_TEXT"])?>
    <?$ar["text"] = strip_tags($arResult["FORM_ERRORS_TEXT"]);?>
    <?$ar["er"] = 1;?>
<? endif; ?>
<?=json_encode($ar);?>
