<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$prop_id = 1;
?>
<div class="">
    <ul class="feature-tag-list">
        <?foreach($arResult["ITEMS"] as $arItem):?>
<!--            <li><a href="--><?//if($arItem['FEATURE_CODE']):?><!--/--><?//=CITY_ID?><!--/catalog/--><?//=$arParams['PARENT_SECTION_CODE']?><!--/--><?//=$arItem['FEATURE_CODE']?><!--/--><?//=$arItem['FEATURE_CODE_VALUE']?><!--/--><?//else:?><!----><?//=$arItem['CODE']?><!----><?//endif?><!--" data-code="--><?//=$arItem['FEATURE_REAL_CODE']?><!--" data-val="--><?//=$arItem['FEATURE_CODE']?$arItem['NAME']:''?><!--" id="--><?//=$arItem['FEATURE_VALUE']?><!--" class="--><?//=((in_array($arItem['FEATURE_VALUE'],$_REQUEST["arrFilter_pf"][$arItem['FEATURE_REAL_CODE']])||$_REQUEST["arrFilter_pf"][$arItem['FEATURE_REAL_CODE']]==$arItem['FEATURE_VALUE'])&&$arItem['FEATURE_VALUE'])?"selected":""?><!--">--><?//=$arItem['NAME']?><!--</a></li>-->
        <?endforeach;?>
    </ul>
</div>