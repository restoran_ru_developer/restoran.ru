<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}


$res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']][0]);
if($ar_res = $res->Fetch()){

}


$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, $arSelect);

$cur_key = 0;
while ($obElement = $rsElement->GetNextElement()) {

}
//type
//kitchen
//subway
//area
//out_city
//features


/*
if($_REQUEST['PROPERTY']=='type'){//перекрестные тип-метро
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['type'][0]);
    if($ar_res = $res->Fetch()){
        switch($prop_type){
            case 'subway':
                $type_str = $ar_res['PREVIEW_TEXT']?explode('/',$ar_res['PREVIEW_TEXT']):$ar_res['NAME'];
                $LINK_NAME = (is_array($type_str)?$type_str[0]:$type_str)." у метро ".$ar_res2['NAME'];
                break;
            case 'area':
                if($ar_res2['PREVIEW_TEXT']){
                    $region_str = $ar_res2['PREVIEW_TEXT']." районе";
                }
                else {
                    $region_str = "районе ".$ar_res2['NAME'];
                }

                $type_str = $ar_res['PREVIEW_TEXT']?explode('/',$ar_res['PREVIEW_TEXT']):$ar_res['NAME'];
                $LINK_NAME = (is_array($type_str)?$type_str[0]:$type_str)." в $region_str";
                break;
            case 'out_city':
                $region_str = $ar_res2['PREVIEW_TEXT']?$ar_res2['PREVIEW_TEXT']:$ar_res2['NAME'];

                $type_str = $ar_res['PREVIEW_TEXT']?explode('/',$ar_res['PREVIEW_TEXT']):$ar_res['NAME'];

                $LINK_NAME = (is_array($type_str)?$type_str[0]:$type_str)." в $region_str";
                break;
        }
    }
}
*/




if ($this->StartResultCache(false,array($_REQUEST['CATALOG_ID'],$_REQUEST['CITY_ID'],$_REQUEST['PROPERTY'])))//,$_REQUEST['PROPERTY_VALUE']
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    $arSelect = array_merge($arParams["FIELD_CODE"], array(
        "NAME",
        "CODE",
    ));

    $arFilter = array (
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "IBLOCK_LID" => SITE_ID,
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "N",
    );

    $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
        $arParams["PARENT_SECTION"],
        $arParams["PARENT_SECTION_CODE"],
        array(
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        )
    );

    if($arParams["PARENT_SECTION"] > 0) {
        $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
        if($arParams["INCLUDE_SUBSECTIONS"])
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
    }
    else
    {
        $arResult["SECTION"]= false;
    }

    //ORDER BY
    $arSort = array(
        $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
    );
    if(!array_key_exists("ID", $arSort))
        $arSort["ID"] = "ASC";


    $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, $arSelect);

    $cur_key = 0;
    while ($obElement = $rsElement->GetNextElement()) {

        $arItem = $obElement->GetFields();

        $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;
        if ($bGetProperty) {
//            foreach ($arParams['PROPERTY_CODE'] as $prop_name) {
//                $arItem["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
//            }
            $arItem["PROPERTIES"] = $obElement->GetProperties();
        }

        $propsArr = array(
            'entertainment'=>'razvlecheniya',
            'features'=>'osobennosti',
            'proposals'=>'predlozheniya',
            'children'=>'detyam',
            'features_b_h'=>'osobennostibanket',
            'my_alcohol'=>'razreshenospirtnoe',
            'BANKET_SPECIAL_EQUIPMENT'=>'specoborudovanie',
        );

        $realPropsCode = array(
            'features_b_h'=>'FEATURES_B_H',
            'my_alcohol'=>'ALLOWED_ALCOHOL',
            'specoborudovanie'=>'BANKET_SPECIAL_EQUIPMENT',
        );


        if($arItem['PROPERTIES']['FEATURES']['VALUE']&&$arItem['PROPERTIES']['FEATURES']['VALUE']!='Y'&&!$arItem['CODE']){
            $res = CIBlockElement::GetByID($arItem['PROPERTIES']['FEATURES']['VALUE']);
            if($ar_res = $res->Fetch()){
                if($ar_res['IBLOCK_TYPE_ID']=='system'){
                    //FirePHP::getInstance()->info($ar_res['IBLOCK_CODE'],$ar_res['NAME']);

                    $arItem['FEATURE_CODE'] = $propsArr[$ar_res['IBLOCK_CODE']]?$propsArr[$ar_res['IBLOCK_CODE']]:$ar_res['IBLOCK_CODE'];
                    $arItem['FEATURE_CODE_VALUE'] = $ar_res['CODE'];
                    $arItem['FEATURE_VALUE'] = $ar_res['ID'];
                    $arItem['FEATURE_REAL_CODE'] = $realPropsCode[$ar_res['IBLOCK_CODE']]?$realPropsCode[$ar_res['IBLOCK_CODE']]:$ar_res['IBLOCK_CODE'];
                }
                else {
                    //FirePHP::getInstance()->info($ar_res['IBLOCK_TYPE_ID'],$ar_res['NAME']);
                    $arItem['FEATURE_CODE'] = $propsArr[$ar_res['IBLOCK_TYPE_ID']]?$propsArr[$ar_res['IBLOCK_TYPE_ID']]:$ar_res['IBLOCK_TYPE_ID'];
                    $arItem['FEATURE_CODE_VALUE'] = $ar_res['CODE'];
                    $arItem['FEATURE_VALUE'] = $ar_res['ID'];
                    $arItem['FEATURE_REAL_CODE'] = $realPropsCode[$ar_res['IBLOCK_TYPE_ID']]?$realPropsCode[$ar_res['IBLOCK_TYPE_ID']]:$ar_res['IBLOCK_TYPE_ID'];
                }
            }
//            print_r($ar_res);
        }
        elseif($arItem['PROPERTIES']['FEATURES']['VALUE']=='Y'){
            $arItem['FEATURE_CODE'] = $arItem['FEATURE_REAL_CODE'] = $arItem['CODE'];

            if($arItem['CODE']=='d_tours'){
                $arItem['FEATURE_CODE_VALUE'] = $arItem['FEATURE_VALUE'] = '2';
            }
            else {
                $arItem['FEATURE_CODE_VALUE'] = 'y';
                $arItem['FEATURE_VALUE'] = 'Y';
            }
        }



        $arResult["ITEMS"][] = $arItem;
        $arResult["ELEMENTS"][] = $arItem["ID"];

        $cur_key++;
    }
//    FirePHP::getInstance()->info($arResult['RESTORAN_ITEMS']);


//    $this->SetResultCacheKeys(array(
//        "RESTORAN_ITEMS",
//    ));

//    print_r($arResult);

    $this->EndResultCache();
}
$this->IncludeComponentTemplate();

?>