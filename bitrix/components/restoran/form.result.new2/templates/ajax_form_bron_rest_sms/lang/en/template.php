<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS ['FORM_ADD'] = "Add";
$MESS ['FORM_ACCESS_DENIED'] = "Web-form access denied.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you. Your application form #";
$MESS ['FORM_DATA_SAVED2'] = " was received.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Web-form module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "Web-form is not found.";
$MESS ['I_WANT'] = "I want ";
$MESS ['IN_RESTORAN'] = "at a restaurant";
$MESS ['NA'] = "date";
$MESS ['AT'] = "time";
$MESS ['OUR_COMPANY'] = "Our company consists of";
$MESS ['PEOPLE'] = "people";
$MESS ['HOW_MUCH_BUY'] = "we plan to spend";
$MESS ['BY_PERSON'] = "rub. per person";
$MESS ['MY_NAME'] = "My name is";
$MESS ['FOR_FEEDBACK'] = "For feedback I leave";
$MESS ['MY_PHONE'] = "my phone:";
$MESS ['MY_EMAIL'] = "E-mail:";   
$MESS ['MY_WISH'] = "Your special instructions";   
$MESS ['CAPTCHA'] = "I confirm that I am not a bot, the picture reads:";  
$MESS ['NON_SMOKING_HALL'] = "Non-smoking hall";   
$MESS ['SMOKING_HALL'] = "Smoking hall"; 
$MESS ['RESERVE'] = "Reserve";
$MESS ['SPEAK_ENGLISH'] = "Make restaurant reservations - we speak english"; 
?>
