<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//if ($arResult["isFormNote"] != "Y") { ?>
<link href="<?=$templateFolder?>/style.css?7"  type="text/css" rel="stylesheet" />
<?if (!$_REQUEST["web_form_apply"]&&!$_REQUEST["RESULT_ID"]):?>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
</div>
<div id="order_online" style="padding:10px 30px">
    <div class="ajax_form">
<?endif;?>
    <?=$arResult["FORM_HEADER"]?>       
    <?/*if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
        <?if ($arResult["isFormTitle"]):?>
            <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
        <?endif;?>
    <?endif;*/?>        
    <div class="ok"><?=$arResult["FORM_NOTE"]?></div>
    <div class="font12" id="err">
        <?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
    </div>
    <div align="center" style="line-height:30px; font-size:18px;">
        We will be happy to a restaurant for your celebration! <br />
         Simply fill out the form or call : <?=(CITY_ID=="spb")?"+7 (812) 740 18 20":"+7 (495) 988-26-56"?>
    </div>
    <h1 style="text-align:center;color:#FFF; margin-bottom:0px; font-size:24px;">We will welcome your request!</h1>
    <?//v_dump($arResult["QUESTIONS"])?>
    <?foreach ($arResult["QUESTIONS"] as $key=>$question):
            $ar = array();
            $ar["CODE"] = $key;
            $questions[] = array_merge($question,$ar);
    endforeach;?>    
    <table cellpadding="10" width="100%">
        <tr>
            <td width="35%" valign="top">
                <input type="hidden" value="Y" name="banket" />    
                <input type="hidden" value="31" name="form_<?=$questions[0]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[0]["CODE"]?>" />    
                <input id="top_search_input1" class="search_rest ac_input" autocomplete="off" name="form_text_32" type="hidden" value="подбор">
                <input id="top_search_rest" type="hidden" value="1" name="form_text_44">
                <div class="question" style="text-align:left; background:url('<?=$templateFolder?>/images/s_star.png') 160px 10px no-repeat;">
                    Date:
                    <input type="text" class="datew" value="<?=($_REQUEST["form_".$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[2]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[2]["STRUCTURE"][0]["ID"]]:$_REQUEST["date"]?>" name="form_<?=$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[2]["STRUCTURE"][0]["ID"]?>"/>
                </div>
                <div class="question" style="text-align:left;">
                    Time:
                    <input type="text" class="time" value="<?=($_REQUEST["form_".$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[3]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[3]["STRUCTURE"][0]["ID"]]:$_REQUEST["time"]?>" name="form_<?=$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[3]["STRUCTURE"][0]["ID"]?>"/>
                </div>
                <div class="question" style="text-align:left; background:url('<?=$templateFolder?>/images/s_star.png') 190px 10px no-repeat;">
                    Number of persons:
                    <input type="text" class="text" value="<?=($_REQUEST["form_".$questions[4]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[4]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[4]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[4]["STRUCTURE"][0]["ID"]]:$_REQUEST["person"]?>" size="4" maxlength="3" name="form_<?=$questions[4]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[4]["STRUCTURE"][0]["ID"]?>" />
                </div>
                <div class="question" style="text-align:left; background:url('<?=$templateFolder?>/images/s_star.png') 215px 7px no-repeat;">
                    Budget:
                    <input type="text" class="text" value="<?=$_REQUEST["form_".$questions[5]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[5]["STRUCTURE"][0]["ID"]]?>" size="8" maxlength="7" name="form_<?=$questions[5]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[5]["STRUCTURE"][0]["ID"]?>" />
                    rub./prs.
                </div>
                <div class="question" style="text-align:left; padding-left:60px;">
                    <table>
                        <tr>
                            <td>
                                <span class="niceCheck2">
                                    <input type="checkbox" id="68" name="form_checkbox_alc[]" value="68">
                                </span>                    
                            </td>
                            <td style="padding-left:10px">
                                <label class="niceCheck2Label" for="by_name" style="font-size:14px;"> your alcohol</label>
                            </td>
                        </tr>
                    </table>                        
                </div>
            </td>
            <td></td>
            <td  width="55%">          
                <table>
                    <tr>
                        <td>
                            <div class="question" style="text-align:left;">
                                Your name:
                            </div>
                        </td>
                        <td>
                            <div class="question" style="text-align:left; background:url('<?=$templateFolder?>/images/s_star.png') right 7px no-repeat;">
                                <input size="30" type="text" class="text" placeholder="Фамилия Имя" value="<?=($_REQUEST["form_".$questions[6]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[6]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[6]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[6]["STRUCTURE"][0]["ID"]]:$USER->GetFullName()?>" name="form_<?=$questions[6]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[6]["STRUCTURE"][0]["ID"]?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="question" style="text-align:left;">
                                Company:
                            </div>
                        </td>
                        <td>
                            <div class="question" style="text-align:left;">
                                <input size="30"  type="text" class="text" class="inputtext" name="form_text_67" value="<?=$_REQUEST["form_text_67"]?>">
                            </div>
                        </td>
                    </tr>                    
                    <tr>
                        <td>
                            <div class="question" style="text-align:left;">                                
                                Phone:                                 
                            </div>
                        </td>
                        <td>
                            <div class="question" style="text-align:left; background:url('<?=$templateFolder?>/images/s_star.png') right 7px no-repeat;">
                                <?$rsUser = CUser::GetByID($USER->GetID());
                                $ar = $rsUser->Fetch();
                                $ar["PERSONAL_PHONE"] = str_replace(" ", "", $ar["PERSONAL_PHONE"]);
                                $ar["PERSONAL_PHONE"] = str_replace("-", "", $ar["PERSONAL_PHONE"]);
                                $ar["PERSONAL_PHONE"] = str_replace("(", "", $ar["PERSONAL_PHONE"]);
                                $ar["PERSONAL_PHONE"] = str_replace(")", "", $ar["PERSONAL_PHONE"]);
                                $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);        
                                $phone = $ar["PERSONAL_PHONE"];
                                $ar["PERSONAL_PHONE"] = substr($phone, 0,3)." ".substr($phone, 3,3)."  ".substr($phone, 6,2)."  ".substr($phone, 8,2);
                                ?>
                                <span class="font18" style="color:#FFF"> +7 <input type="text" class="phone1" style="text-align:left" value="<?=$ar["PERSONAL_PHONE"]?>" name="form_<?=$questions[7]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[7]["STRUCTURE"][0]["ID"]?>" /></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="question" style="text-align:left;">
                                E-mail:
                            </div>
                        </td>
                        <td>
                            <div class="question" style="text-align:left;">
                                <input type="text" class="text" value="<?=($_REQUEST["form_".$questions[8]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[8]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[8]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[8]["STRUCTURE"][0]["ID"]]:$USER->GetEmail()?>" name="form_<?=$questions[8]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[8]["STRUCTURE"][0]["ID"]?>" size="30" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <div class="question" style="text-align:left">
                                Wishes: <small style="line-height:18px">(area, kitchen, separate lounge, its own program, etc.)</small>
                            </div>
                        </td>
                        <td valign="top">
                            <div class="question">                                 
                                <textarea class="bron_ta" style="text-align:left; height: 96px; background: url(/tpl/images/textarea_bron.png) left 0px repeat;" name="form_<?=$questions[11]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[11]["STRUCTURE"][0]["ID"]?>"><?=trim($_REQUEST["form_".$questions[11]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[11]["STRUCTURE"][0]["ID"]])?></textarea>                       
                            </div>
                        </td>
                    </tr>
                </table>                                                
            </td>
        </tr>
    </table>    
    <?if($arResult["isUseCaptcha"] == "Y"): ?>
        <div class="question">
            <?=GetMessage("CAPTCHA")?>
        </div>
        <div class="question" style="background:url('<?=$templateFolder?>/images/s_star.png') 370px 18px no-repeat;">
            <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" align="left" />
            <?//=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?//=$arResult["REQUIRED_SIGN"];?>
            <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
        </div>
    <?endif; ?>
    <div class="inv_cph">
        <input type="checkbox" name="lst_nm" value="1" />
        <input type="checkbox" name="scd_nm" value="1" checked="checked" />
    </div>
    <?if ($_REQUEST["invite"]):?>
        <input type="hidden" id="invite" name="invite" value="<?=$_REQUEST["invite"]?>" />
    <?endif;?>
    <?if ($_REQUEST["CITY"]):?>
        <input type="hidden" name="CITY" value="<?=$_REQUEST["CITY"]?>" />
    <?else:?>        
        <input type="hidden" name="CITY" value="<?=$APPLICATION->get_cookie("CITY_ID");?>" />
    <?endif;?>
    <input type="hidden" name="web_form_apply" value="Y" />
    <div>
        <p class="another_color font12" style="line-height:24px;">* — <?=GetMessage("FORM_REQUIRED_FIELDS")?></p>
    </div>
    <div align="center">
        <input class="light_button appetite"  style="background:url(/tpl/images/buttons/button_new_bg.png) left top repeat-x; height:47px; line-height:47px; font-size:18px; text-transform: none; letter-spacing:0px; border-radius:5px; padding:0px 25px;" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"];?>" />
        <!--<input class="dark_button popup_close" type="button" value="закрыть"/>-->
    </div>
    <?/*if ($arResult["F_RIGHT"] >= 15):?>
    &nbsp;<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />
    <?endif;*/?>
    <!--&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />-->
<!--<p><?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?></p>-->
<?=$arResult["FORM_FOOTER"]?>
<?
//} //endif (isFormNote)
?>
    <?if (!$_REQUEST["web_form_apply"]&&!$_REQUEST["RESULT_ID"]):?>
    </div>
</div>
<?endif;?>