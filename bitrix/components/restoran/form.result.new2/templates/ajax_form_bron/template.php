<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//if ($arResult["isFormNote"] != "Y") { ?>
<link href="<?=$templateFolder?>/style.css"  type="text/css" rel="stylesheet" />
    <?=$arResult["FORM_HEADER"]?>    
    <?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
        <?if ($arResult["isFormTitle"]):?>
            <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
        <?endif;?>
    <?endif;?>        
    <div class="ok"><?=$arResult["FORM_NOTE"]?></div>
    <?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
    <h1 style="text-align:center;color:#FFF; margin-bottom:0px;"><?=GetMessage("I_WANT")?></h1>
    <?//v_dump($arResult["QUESTIONS"])?>
    <?foreach ($arResult["QUESTIONS"] as $key=>$question):
            $ar = array();
            $ar["CODE"] = $key;
            $questions[] = array_merge($question,$ar);
    endforeach;?>
    <div class="question">
        <select id="what_question" style="width:220px;" name="form_<?=$questions[0]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[0]["CODE"]?>">
            <?foreach ($questions[0]["STRUCTURE"] as $val):?>
            <option value="<?=$val["ID"]?>"><?=$val["MESSAGE"]?></option>
            <?endforeach;?>
        </select>
        <?=GetMessage("IN_RESTORAN")?>
     </div>
     <div class="question">
         <?$APPLICATION->IncludeComponent(
                "bitrix:search.title",
                "bron",
                Array(
                    "NAME" => "form_".$questions[1]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[1]["STRUCTURE"][0]["ID"]
                ),
            false
        );?>
     </div>
    <div class="question">
        <?=GetMessage("NA")?>
        <input type="text" class="datew" value="<?=date("d.m.Y")?>" name="form_<?=$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[2]["STRUCTURE"][0]["ID"]?>"/>
        <?=GetMessage("AT")?>
        <input type="text" class="time" value="" name="form_<?=$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[3]["STRUCTURE"][0]["ID"]?>"/>
    </div>
    <div class="question">
        <?=GetMessage("OUR_COMPANY")?>
        <input type="text" class="text" value="" size="4" maxlength="3" name="form_<?=$questions[4]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[4]["STRUCTURE"][0]["ID"]?>" />
        <?=GetMessage("PEOPLE")?>
    </div>
    <div class="question">
        <?=GetMessage("HOW_MUCH_BUY")?>
        <input type="text" class="text" value="" size="8" maxlength="7" name="form_<?=$questions[5]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[5]["STRUCTURE"][0]["ID"]?>" />
        <?=GetMessage("BY_PERSON")?>
    </div>
    <div class="question">
        <?=GetMessage("MY_NAME")?><br />
    </div>
    <div class="question">
        <input type="text" class="search_rest" name="form_<?=$questions[6]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[6]["STRUCTURE"][0]["ID"]?>" />
    </div>
        <div class="question">
        <?=GetMessage("FOR_FEEDBACK")?>
    </div>
    <div class="question">
        <?=GetMessage("MY_PHONE")?> <span class="font18" style="color:#FFF"> +7 <input type="text" class="phone1" style="text-align:left"  name="form_<?=$questions[7]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[7]["STRUCTURE"][0]["ID"]?>" /></span>
    </div>
    <div class="question">
        <?=GetMessage("MY_EMAIL")?> <input type="text" class="text"  name="form_<?=$questions[8]["STRUCTURE"][0]["FIELD_TYPE"]?>_<?=$questions[8]["STRUCTURE"][0]["ID"]?>" size="30" />
    </div>
    <?if($arResult["isUseCaptcha"] == "Y"): ?>
        <div class="question">
            <?=GetMessage("CAPTCHA")?>
        </div>
        <div class="question">
            <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" align="left" />
            <?//=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?//=$arResult["REQUIRED_SIGN"];?>
            <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
        </div>
    <?endif; ?>
    <Br />
    <input type="hidden" name="web_form_apply" value="Y" />
    <div align="right">
        <input class="light_button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"];?>" />
        <input class="dark_button popup_close" type="button" value="закрыть"/>
    </div>
    <?/*if ($arResult["F_RIGHT"] >= 15):?>
    &nbsp;<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />
    <?endif;*/?>
    <!--&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />-->
<!--<p><?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?></p>-->
<?=$arResult["FORM_FOOTER"]?>
<?
//} //endif (isFormNote)
?>