<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS ['FORM_ADD'] = "Add";
$MESS ['FORM_ACCESS_DENIED'] = "Web-form access denied.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you. Your application form #";
$MESS ['FORM_DATA_SAVED2'] = " was received.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Web-form module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "Web-form is not found.";

$MESS ['WITH_PLEASURE'] = "We will choose a restaurant for your celebration with pleasure!";   
$MESS ['CALL_PHONE'] = "Just fill the form or call by phone";   
$MESS ['MAKE_RESERVATION'] = "Make restaurant reservations - we speak English.";   
$MESS ['F_DATE'] = "Date";   
$MESS ['F_TIME'] = "Time";   
$MESS ['F_QUANTITY'] = "How many people";   
$MESS ['F_BUDGET'] = "Budget";   
$MESS ['F_PER'] = "rub/person";   
$MESS ['F_ALC'] = "BYOB";   
$MESS ['F_NAME'] = "Name";   
$MESS ['F_COMPANY'] = "Company";   
$MESS ['F_PHONE'] = "Phone number";   
$MESS ['F_POZH'] = "Special instructions";   
$MESS ['F_POZH2'] = "(Neighborhood, cuisine, separate hall, show etc.)";
$MESS ['F_RESERVE'] = "Reserve";   
?>
