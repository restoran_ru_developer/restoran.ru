<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 1;//36000000;


//    ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
$arResult = array();

if($this->StartResultCache(false, http_build_query($_REQUEST["arrFilter_pf"]).CITY_ID))
{
    if (check_bitrix_sessid()):
        if (CModule::IncludeModule("iblock")):
            $arRestIB = getArIblock("catalog", CITY_ID);
            if ($_REQUEST["arrFilter_pf"])
            {
                foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
                {
                    //if ($key=="type")
                        //$arrFilter["?PROPERTY_".$key] = implode(" || ",$ar);
                    //else
                    $arrFilter["PROPERTY_".$key] = $ar;
                }


                $restorans_city_section_id_arr = array('msk'=>32,'spb'=>103,'rga'=>247659,'urm'=>247660);
                $arrFilter["SECTION_ID"] = array($restorans_city_section_id_arr[CITY_ID]);

                if($_REQUEST["arrFilter_pf"]['type']){
                    if(in_array(1349,$_REQUEST["arrFilter_pf"]['type'])){
                        unset($arrFilter["SECTION_ID"]);
                    }
                }
            }

            $arFilter = Array("ACTIVE"=>"Y","IBLOCK_ID"=>$arRestIB["ID"],"!PROPERTY_sleeping_rest_VALUE"=>"Да");
            if (is_array($arrFilter))
            {
                $arFilter = array_merge($arFilter,$arrFilter);
            }

            $res = CIBlockElement::GetList(Array(),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","DETAIL_PAGE_URL","CODE"));
            //$res->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
            $response = array();
			$response["type"] = "FeatureCollection";

            $idx = 0;
            while($ar = $res->GetNext())
            {
                $idx++;
                $lat = array();
                $lon = array();
                $adr = array();
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lat"));
                while($ar_props = $db_props->Fetch())
                    $lat[] = $ar_props["VALUE"];
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lon"));
                while($ar_props = $db_props->Fetch())
                    $lon[] = $ar_props["VALUE"];
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
                while($ar_props = $db_props->Fetch())
                    $adr[] = $ar_props["VALUE"];

                // средний счет
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"PREVIEW_PICTURE"));
				//print_r ($ar_props);
                while($ar_props = $db_props->Fetch())
                    $schet[] = $ar_props["VALUE"];



                foreach($lat as $key=>$l)
                {
//                    if ($lat[$key]>=(double)$arParams["xl"]&&$lat[$key]<=(double)$_REQUEST["xr"]&&$lon[$key]>=(double)$_REQUEST["yl"]&&$lon[$key]<=(double)$arParams["yr"])
                    {
                        /*if ($arParams["mobile"]=="Y")
                            $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$lat[$key],"lon"=>$lon[$key],"adres"=>stripslashes($adr[$key]),"url"=> "/mobile/detail.php?RESTOURANT=".$ar["CODE"]);
                        else*/
//                            $response["features"][] = array(type=>"Feature",id=>$idx,geometry=>array(type=>"Point",coordinates=>array($lat[$key], $lon[$key])), properties=>array(balloonContent=>"<div data-aid=\"".$ar["ID"]."\" class=\"mbs\"><a href=\"".$ar["DETAIL_PAGE_URL"]."\">".stripslashes($ar["NAME"])."</a></div><div data-aid=\"".$ar["ID"]."\" class=\"mb\">".stripslashes($adr[$key])."</div>",hintContent=>stripslashes($ar["NAME"])));


                            $response["features"][] = array(type=>"Feature",id=>$ar["ID"],geometry=>array(type=>"Point",coordinates=>array($lat[$key], $lon[$key])), properties=>array(balloonContent=>"",hintContent=>stripslashes($ar["NAME"])));


                            $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$lat[$key],"lon"=>$lon[$key],"adres"=>stripslashes($adr[$key]),"url"=> $ar["DETAIL_PAGE_URL"]);
                    }
                }
            }
            if (count($arResult)>0 || 1)
            {
                $arResult = $response;
                $this->IncludeComponentTemplate();
            }
        endif;
    endif;
}
?>