<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$APPLICATION->AddHeadScript($templateFolder.'/jq_redactor/redactor.js');
$APPLICATION->SetAdditionalCSS($templateFolder.'/jq_redactor/css/redactor.css');
?>


<textarea name="<?=$arParams["BLOCK_NAME"]?>" id="<?=$arParams["BLOCK_ID"]?>" style="height: 300px; width: 100%;" class="vredactor"><?=$arParams["BLOCK_CONTENT"]?></textarea>
<script type="text/javascript">	
	var buttons = ['bold', 'italic','underline','image','blockquote'];
	$("#<?=$arParams["BLOCK_ID"]?>").redactor({ 
		path: '<?=$templateFolder?>/jq_redactor', 
		css: 'redactor.css', 
		resize:false, 
		buttons: ['bold', 'italic','underline','image','blockquote','link'], 
		formattingTags: ['blockquote'],
		lang: 'ru', 
		focus: false, 
		imageUpload: '<?=$templateFolder?>/image_upload.php'
	});
</script>	