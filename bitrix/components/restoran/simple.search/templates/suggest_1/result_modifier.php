<?
CModule::IncludeModule("iblock");
if (is_array($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $db_props = CIBlockElement::GetProperty($el["IBLOCK_ID"], $el["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch())
                $adr = $ar_props["VALUE"];            
            //$el["PROPERTIES"] = $obElement->GetProperties();            
            $res = CIBlockSection::GetByID($el["IBLOCK_SECTION_ID"]);
            if ($ar = $res->Fetch())
                $el["SECTION_NAME"] = $ar["NAME"];
            $arItem["ELEMENT"] = $el;
            $arItem["ELEMENT"]["ADRES"] = $adr;
         }
    endforeach;?>
<?endif;?>
