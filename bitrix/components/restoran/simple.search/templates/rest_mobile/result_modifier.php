<?
CModule::IncludeModule("iblock");
if (is_array($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            foreach ($el["PROPERTIES"] as $key=>$prop)
            {
                //if (in_array($prop["CODE"], Array("type","kitchen","average_bill","kuhnya_dostavki","subway","address")))
                if (in_array($prop["CODE"], Array("kitchen","average_bill","subway","address")))
                    $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            }
            $arItem["ELEMENT"] = $el;
            /*if ($arItem["ELEMENT"]["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::GetPath($arItem["ELEMENT"]["PREVIEW_PICTURE"]);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm.png";*/
            if ($el["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
            elseif ($el["DETAIL_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
            if(!$arItem["ELEMENT"]["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
         }
    endforeach;?>
<?endif;?>
