<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"])) {
        $arParams["CACHE_TYPE"] = "Y";
	$arParams["CACHE_TIME"] = 0;
}
global $USER;
if((!$_REQUEST["q"] || strlen(trim($_REQUEST["q"]))) <= 0)
{
    if ($arParams["MOB"]!="Y")
        return;
}
$q = $_REQUEST["q"];
$page = $_REQUEST["page"];
if (!$page||$page==1)
    $page = 0;
$n = $page*$arParams["COUNT"];

if($this->StartResultCache(false, $page.$q.CITY_ID.'dsd'))
{
    if (!$arParams["IBLOCK_ID"])
        $arRestIB = getArIblock("catalog", CITY_ID);
    else
        $arRestIB["ID"] = (int)$arParams["IBLOCK_ID"];
    $arRestGroupIB = getArIblock("rest_group", CITY_ID);

    $iblock_group = $DB->ForSQL($arRestGroupIB["ID"]);
    $arClosedGroupIB = getArIblock("closed_rubrics_rest", CITY_ID);


    $iblock_closed = $DB->ForSQL($arClosedGroupIB["ID"]);
    CModule::IncludeModule("iblock");
    if ($q)
    {
        $q = ToLower($q);
        $lang = get_lang($q);
        $arSelect = Array("ID","NAME","DETAIL_PAGE_URL");
        $iblock = $DB->ForSQL($arRestIB["ID"]);        
        $q = $DB->ForSQL($q);
        $count = $DB->ForSQL($arParams["COUNT"]);
        $rest_sql = "";


        $network_prop_compare_arr = array(
            12 => 4534,
            2636 => 4540,
            11 => 4536,
            3567 => 4542,
            3566 => 4538,
            13 => 4544,
            16 => 4546,
            2591 => 4548,
            2624 => 4550,
            2674 => 4552,
            2821 => 4554,
            3418 => 4556,
            3624 => 4622,
            3625 => 4624,
            15 => 4558,
            2820 => 4560,
            2719 => 4562,
            2586 => 4626,
            2587 => 4628
        );


        if ($lang=="ru")
        {
            $q2 = decode2anotherlang_ru($q);
            $q2 = $DB->ForSQL($q2);
                        
            if ($arParams["REST_GROUPS"]=="Y")
            {

                $rest_sql = ' UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')   ';
            }
            if ($arParams["CLOSED_RUBRICS"]=="Y")
            {
                $closed_sql = ' UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')   ';
            }

            global $USER;

            if($this->__templateName!='suggest_1'&&$this->__templateName!='rest'){ //  для строки поиска на сайте (для сетевых, чтобы исключить рестораны сети)
                $res = $DB->Query('(SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')'.$rest_sql.$closed_sql.'LIMIT '.$count);
            }
            else {
                // todo исключить общие сетевые рестораны
                $res = $DB->Query('(SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        '.$rest_sql.$closed_sql.'
                            LIMIT '.$count);
            }


//            SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
//LEFT JOIN b_iblock_element_property
//ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID= 4534
//WHERE b_iblock_element.IBLOCK_ID=12 AND b_iblock_element.NAME LIKE '321%' AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE='Y'
//ORDER BY b_iblock_element.NAME ASC
//LIMIT 10



        }
        elseif ($lang=="en")
        {
            $q2 = decode2anotherlang_en($q);
            $q2 = $DB->ForSQL($q2);
            
            if ($arParams["REST_GROUPS"]=="Y")
            {                     
                $rest_sql = ' UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')     
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')   ';                                
            }
            if ($arParams["CLOSED_RUBRICS"]=="Y")
            {                     
                $closed_sql = ' UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')     
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')   ';                                
            }
            //$res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","?NAME"=>"%".$q."% || %".$q2."%"),false,Array("nTopCount"=>$arParams["COUNT"]),$arSelect);
//            $res = $DB->Query('(SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
//                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
//                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
//                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
//                        '.$rest_sql.'
//                        LIMIT '.$count);

            if($this->__templateName!='suggest_1'&&$this->__templateName!='rest'){ //  для строки поиска на сайте (для сетевых, чтобы исключить рестораны сети)
            $res = $DB->Query('(SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')'.$rest_sql.'LIMIT '.$count);
            }
            else {
                $res = $DB->Query('(SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')
                        '.$rest_sql.$closed_sql.'
                            LIMIT '.$count);
            }
        }   
        else
        {
            if ($arParams["REST_GROUPS"]=="Y")
            {                     
                $rest_sql = ' UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')     
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_group.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')   ';                                
            }
            if ($arParams["CLOSED_RUBRICS"]=="Y")
            {                     
                $closed_sql = ' UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')     
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock_closed.' AND (TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") ORDER BY NAME ASC LIMIT '.$count.')   ';                                
            }
//            $res = $DB->Query('(SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND NAME LIKE "'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.')
//                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND NAME LIKE "%'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.')
//                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND TAGS LIKE "%'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.')
//                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' AND TAGS LIKE "'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.')
//                        '.$rest_sql.$closed_sql.'
//                            LIMIT '.$count);

            $res = $DB->Query('(SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')
                                UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                    LEFT JOIN b_iblock_element_property
                    ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                    WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                    ORDER BY b_iblock_element.NAME ASC
                    LIMIT '.$count.')'.$rest_sql.$closed_sql.'LIMIT '.$count);

            //$res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","NAME"=>"%".$q."%"),false,Array("nTopCount"=>$arParams["COUNT"]),$arSelect);        
        }  
    }
    elseif ($arParams["MOB"]=="Y")
    {
        $iblock = $DB->ForSQL($arRestIB["ID"]);
        $count = $DB->ForSQL($arParams["COUNT"]);
        $res = $DB->Query('SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' ORDER BY NAME ASC LIMIT '.$n.','.$count);                
    }


    if ($res)
    {
        $sleeping = $no_sleeping = array();
        while($ar = $res->GetNext())
        {
            if ($arParams["NO_SLEEP"]=="Y")
            {
                //  Проверяем спящего и сортируем в конец массива
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array(), Array("CODE"=>"sleeping_rest"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sleeping[] = $ar;
                    }
                    else {
                        $no_sleeping[] = $ar;
                    }
                }
                else {
                    $no_sleeping[] = $ar;
                }


//                $arResult["ITEMS"][] = $ar;
            }
            elseif ($arParams["NO_MOBILE"]=="Y")
            {
                //  Проверяем спящего и сортируем в конец массива
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array(), Array("CODE"=>"no_mobile"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $no_mobile[] = $ar;
                    }
                    else {
                        $mobile[] = $ar;
                    }
                }
                else {
                    $mobile[] = $ar;
                }


//                $arResult["ITEMS"][] = $ar;
            }
            elseif ($arParams["ONLY_SLEEP"]=="Y")
            {
                $sleep = "";
                //Проверяем спящего
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array(), Array("CODE"=>"sleeping_rest"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sleep = "Да";
                    }
                    /*$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$ar["IBLOCK_ID"], "ID"=>$ar_props["VALUE"]));
                    if($enum_fields = $property_enums->GetNext())
                    {
                        $sleep = $enum_fields["VALUE"];
                    } */
                }
                if ($sleep=="Да")
                    $arResult["ITEMS"][] = $ar;
            }
            else
            {
                $arResult["ITEMS"][] = $ar;
            }

        }

        if ($arParams["NO_SLEEP"]=="Y") {
//            $arResult["ITEMS"] = array_merge($no_sleeping,$sleeping);
            $arResult["ITEMS"] = $no_sleeping;
        }

        if ($arParams["NO_MOBILE"]=="Y") {
//            $arResult["ITEMS"] = array_merge($no_sleeping,$sleeping);
            $arResult["ITEMS"] = $mobile;
        }

        if($this->__templateName=='suggest_1'){ //  форма бронирования - исключаем собирательные сетевые рестораны
            foreach($arResult["ITEMS"] as $no_network_rest_key=>$arItem){
                $network_db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array(), Array("CODE"=>"REST_NETWORK"));
                if($ar_network_props = $network_db_props->Fetch())
                {
                    if($ar_network_props['VALUE']){
                        unset($arResult["ITEMS"][$no_network_rest_key]);
                    }
                }
            }
        }
    }
    $this->SetResultCacheKeys(array(
            "ITEMS",
    ));    
    $this->IncludeComponentTemplate();
}
?>