<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$arReviewsIB = getArIblock("reviews", CITY_ID);?>
<div id="reviews">
    <?$APPLICATION->IncludeComponent(
        "restoran:comments",
        "with_rating_new",
        Array(
            "IBLOCK_TYPE" => "reviews",
            "ELEMENT_ID" => $_REQUEST["id"],
            "IS_SECTION" => "N",
            "ADD_COMMENT_TEMPLATE" => "with_rating_new",
            "COUNT" => 5,
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "3600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "URL" => $_REQUEST["url"],
            "PROPERTY_CODE" => Array("plus","minus","photos","video")
        ),
    false
    );?>
</div>