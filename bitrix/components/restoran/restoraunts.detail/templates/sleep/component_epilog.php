<?php
$count = count($_COOKIE["RECENTLY_VIEWED"]["catalog"]);
if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["catalog"]))
    setcookie ("RECENTLY_VIEWED[catalog][".$count."]", $arResult["ID"], time() + 3600*12, "/");
global $APPLICATION;
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);

$APPLICATION->SetPageProperty("title",  "Ресторан ".$arResult["NAME"].". Адрес, меню, фото, отзывы на Ресторан.Ру");
?>
<script>
$(document).ready(function(){
    
    <?if(CSite::InGroup( array(14,15,1,23,24,29,28))):?>  
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                $("#for_edit_link").append('<div id="edit_link" style="position:absolute; top:-65px; right:10px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');                            
            <?else:?>
                $("#for_edit_link").append('<div id="edit_link" style="position:absolute; top:-45px; right:10px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');                            
            <?endif;?>
    <?endif;?>
    });
</script>
