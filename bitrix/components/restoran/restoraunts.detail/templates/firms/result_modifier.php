<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek"
);

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
        }
    }
}
//$arResult["NEXT_POST"] = getPost("next",$arParams["IBLOCK_TYPE"],$arResult["IBLOCK_ID"], $arResult["ID"]);                
//$arResult["PREV_POST"] = getPost("prev",$arParams["IBLOCK_TYPE"],$arResult["IBLOCK_ID"], $arResult["ID"]); 
// get preview text
$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut(($arResult["DETAIL_TEXT"]), 250);
$arResult["NEXT_ARTICLE"] = RestIBlock::GetArticleNext($arResult["IBLOCK_ID"],$arResult["ID"]);
$arResult["PREV_ARTICLE"] = RestIBlock::GetArticlePrev($arResult["IBLOCK_ID"],$arResult["ID"]);


$res = array();
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>2423),false,Array("nTopCount"=>1));
$arResult["NEWS_COUNT"] = $res->SelectedRowsCount();

$res = array();
$arReviewsIB = getArIblock("news", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arReviewsIB["ID"]),false,Array("nTopCount"=>1));
$arResult["NEWS1_COUNT"] = $res->SelectedRowsCount();


$res = array();
$arNightIB = getArIblock("special_projects", CITY_ID,"new_year_night"."_");
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNightIB["ID"]),false,Array("nTopCount"=>1));
if ($aNight = $res->GetNext())
{
    $arResult["NIGHT_COUNT"] = 1;
    $arResult["NIGHT_LINK"] = $aNight["DETAIL_PAGE_URL"];
}

$res = array();
$arCorpIB = getArIblock("special_projects", CITY_ID,"new_year_corp"."_");
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
if ($aCorp = $res->GetNext())
{
    $arResult["CORP_COUNT"] = 1;
    $arResult["CORP_LINK"] = $aCorp["DETAIL_PAGE_URL"];
}
if (CITY_ID=="msk"||CITY_ID=="spb"):
    $arVideonewsIB = getArIblock("videonews", CITY_ID);
    if ($arVideonewsIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arVideonewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["VIDEONEWS_COUNT"] = $res->SelectedRowsCount();        
    }
endif;
//if ($USER->IsAdmin())
//{
//    v_dump($arResult["NIGHT_COUNT"]);
//    v_dump($arResult["CORP_COUNT"]);
//}
?>