<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.min.js')?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.validator.js')?>
<?
global $element_id;
$element_id = $arResult["ID"];
$arFirmsIB = getArIblock("firms", CITY_ID);
$arReviewsIB = getArIblock("reviews", CITY_ID);
?>

<script>
$(document).ready(function(){
	
	//minus_plus_buts 
    $("#minus_plus_buts").on("click",".plus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
  			if(parseInt(otvet)){
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				
  			}
  		});
		return false;
	});
	
	$("#minus_plus_buts").on("click",".minus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet){
  			if(parseInt(otvet)){
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				}
  			
  		});
		return false;
	});
  	
  	$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
  		$("#minus_plus_buts").html(otvet);
  	}); 

});
</script> 


<!--<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==" type="text/javascript"></script>-->
<script src="http://api-maps.yandex.ru/1.1/index.xml?key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript"></script>
<div class="news-detail">    
         <div class="catalog_detail">
                <div class="left detail"> 
                    <div class="statya_section_name">
                        <a class="uppercase another font14" href=<?=$arResult["LIST_PAGE_URL"]?>><?=$arResult["IBLOCK"]["ELEMENTS_NAME"]?></a>
                        <div class="statya_nav">
                            <?if ($arResult["PREV_ARTICLE"]):?>
                                <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                                    <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                                </div>
                            <?endif;?>
                            <?if ($arResult["NEXT_ARTICLE"]):?>
                                <div class="right">
                                    <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                                </div>
                            <?endif;?>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <hr class="black" />
                    <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
                        <h1><?=$arResult["NAME"]?></h1>                        
                        <div class="clear"></div>
                    <?endif;?>                    
                    <div class="left">
                        <script src="<?=$templateFolder?>/galery.js?2"></script>
                        <div id="photogalery">
                            <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>
                                <script>
                                    $(document).ready(function(){
                                        //$("#photogalery").galery({});
                                    });
                                </script>
                                    <div class="img img4">
                                        <img src="<?=CFile::GetPath($arResult["PROPERTIES"]["photos"]["VALUE"][0])?>" height="264" />
                                    </div>
                                    <div class="special_scroll">   
                                        <div class="scroll_container">
                                          <?foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo):?>
                                              <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                  <img src="<?=CFile::GetPath($photo)?>" align="bottom" height="50" />
                                              </div>                                          
                                          <?endforeach;?>
                                        </div>
                                    </div>
                                    <div class="left scroll_arrows">
                                        <a class="prev browse left"></a>
                                        <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["photos"]["VALUE"])?></div>
                                        <a class="next browse right"></a>
                                    </div>
                                <div class="clear"></div>    
                            <?else:?>                            
                                    <div class="img">
                                        <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" />
                                    </div>
                            <?endif;?>
                        </div>
                    </div>
                    <div class="right" style="width:305px;position:relative"  id="for_edit_link"> 
                        <?
                        $temp = explode(',',$arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]);
                        $map_data["yandex_lat"] = (double)$temp[0];
                        $map_data["yandex_lon"] = (double)$temp[1];
                        $map_data["yandex_scale"] = 16;
                        $map_data["PLACEMARKS"][0] = array(
                            "LON" => (double)$temp[1],
                            "LAT" => (double)$temp[0],
                            "TEXT" => "",
                        );
                        $map_data = serialize($map_data);
                        ?>                    
                        <?if ($arResult["ID"]=="951678"):?>
                            <div class="position2 left">                                
                                <a href="/spb/articles/new_year_corp/stolichnyy-keytering/"><img alt="Новогодний корпоратив" title="Новогодний корпоратив" src="/bitrix/components/restoran/restoraunts.detail/templates/.default/images/detail_ny_corp.jpg" /></a>
                            </div>
                        <?endif;?>
                        <table class="properties" cellpadding="5" cellspacing="0">
                            <?$i=0;?>
                            <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                                <?if ($i <= 25 && $pid!="map" && $pid!="ratio"):?>
                                    <tr>
                                        <td width="100">
                                            <div class="dotted_td">
                                                <span class="name"><?=$arProperty["NAME"]?>:&nbsp; </span>
                                            </div>
                                        </td>
                                        <td>
                                            <?if ($pid=="subway"):?>
                                                <p class="metro_<?=CITY_ID?>">
                                            <?endif;?>
                                            <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                                <?=strip_tags($arProperty["DISPLAY_VALUE"][0])?>
                                            <?else:?>
                                                <?if ($pid=="site"):?>
                                                <div class="site" style="overflow:hidden;width:180px">
                                                <?endif;?>
                                                <?=$arProperty["DISPLAY_VALUE"];?>
                                                    <?if ($pid=="site"):?>
                                                </div>
                                                <?endif;?>
                                            <?endif?>
                                            <?if ($pid=="subway"):?>
                                                </p>
                                            <?endif;?>
                                        </td>
                                    </tr>                                   
                                <?endif;?>
                                <?$i++;?>
                            <?endforeach;?>
                        </table>                                                  
                        <br /><br />
                        <?
                          $params = array();
                          $params["id"] = $arResult["ID"];
                          $params = addslashes(json_encode($params));            
                        ?>
                        <!--<div align="right">
                            <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a>
                        </div> 
                        <br />-->
                        <!--<div class="preview_text">
                            <?=$arResult["PREVIEW_TEXT"]?>
                        </div>-->
                    </div>
                    <div class="clear"></div>     
                      <Br />  <Br />
                    
                    <div class="staya_likes">
            <!--<div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <?/*<div class="right" style="padding-right:15px;"><input type="button" class="button" value="<?=GetMessage("SUBSCRIBE")?>" /></div>*/?>
            <div id="minus_plus_buts"></div>
            
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
            <div class="clear"></div>
        </div>
                    
                     
                    <Br />                    
                    <div class="preview_text" text="1">
                        <?=$arResult["PREVIEW_TEXT"]?>
                    </div>
                    <div class="preview_text" text="2" style="display:none">
                        <?=$arResult["DETAIL_TEXT"]?>
                    </div>
                    <?if ($arResult["DETAIL_TEXT"]):?>
                        <div class="more_properties"><a href="javascript:void(0)" onclick="show_more(this,'preview_text','<?=GetMessage("R_MORE_TEXT_1")?>','<?=GetMessage("R_MORE_TEXT_2")?>')" class="js"><?=GetMessage("R_MORE_TEXT_1")?></a></div>
                    <?endif;?>
                    
                    
                    
                </div>
                <div class="right">                    
                    <div class="baner2">
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                    false
                    );?>
                    </div>
                    <br />
                    <!--<div id="search_article">
                        <?/*$APPLICATION->IncludeComponent(
                                "bitrix:subscribe.form",
                                "main_page_subscribe",
                                Array(
                                        "USE_PERSONALIZATION" => "Y",
                                        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                                        "SHOW_HIDDEN" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "3600",
                                        "CACHE_NOTES" => ""
                                ),
                        false
                        );*/?>            
                    </div>-->
                    <div class="clear"></div>                
                </div>
                <div class="clear"></div> 
                <br />
            </div>
            <div align="center">
            <?if ($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]):?>
                <div align="center">
                    <script type="text/javascript">
                        function GetMap ()
                        {
                            YMaps.jQuery ( function () {
                                map = new YMaps.Map ( YMaps.jQuery ( "#map_area" )[0] );
                                var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
                                var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
                                var typeControl = new YMaps.TypeControl ( [] );
                                typeControl.addType ( MTmap );
                                typeControl.addType ( MTsat );

                                map.setMinZoom (13);
                                map.addControl (typeControl);
                                map.addControl(new YMaps.Zoom());
                                YMaps.Events.observe ( map, map.Events.Update, function () {
                                        ShowMyBalloon (0);
                                } );
                                YMaps.Events.observe ( map, map.Events.TypeChange, function () {
                                        hideMyBalloon (0);
                                } );
                                YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                                    ShowMyBalloon (0);
                                } );
                                YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                                    hideMyBalloon ();
                                } );
                                YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                                    hideMyBalloon ();
                                } );
                            })
                            my_style = new YMaps.Style();
                               my_style.iconStyle = new YMaps.IconStyle();
                               my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
                               my_style.iconStyle.size = new YMaps.Point(27, 32);
                               my_style.iconStyle.offset = new YMaps.Point(-15, -32);
                        }

                        function SetMapCenter ( lat, lng, zoom_i )
                        {
                            zoom = zoom_i ? zoom_i : 16;
                            map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
                        }

                        function ShowMyBalloon (id)
                        {
                            hideMyBalloon ();
                            var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
                            jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'><?=$arResult["NAME"]?><br /><?=$arResult["PROPERTIES"]["address"]["VALUE"][0]?></div><div class='balloon_tail'> </div></div>");
                            jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
                            jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
                        }

                        function hideMyBalloon ()
                        {
                            jQuery(".map_ballon").remove();
                        }
                    </script>
                    <script type="text/javascript">
                        var map = null;
                        var map_markers = {};
                        var markers_data = {};
                        var map_filter = {};
                        var hidden_marker = null;
                        var sat_map_type = false;
                        var map_type = 'default';
                        var to_show = null;
                        var cur_user = 0;
                        var my_style = {};

                        window.onload = OnPageLoad;
                        function OnPageLoad (){
                            GetMap();
                            var coord = "<?=$arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]?>";
                            coord = coord.split(",");
                            SetMapCenter ( coord[0], coord[1], 16 );
                            map_markers[0] = new YMaps.Placemark ( new YMaps.GeoPoint ( coord[1], coord[0] ), { style: my_style, hasBalloon: false, db_id: 0,hideIcon: false } );
                            map.addOverlay ( map_markers[0] );
                            ShowMyBalloon(0);
                        }
                    </script>
                    <div id="map_area" style="width: 100%;margin: 0 auto;height: 350px;"></div>
                </div>
                <br /><br />  
            <?endif;?>
            </div>
            <div class="catalog_detail">
                <div class="left">
                    <?/*if ($USER->IsAdmin()):
                        v_dump($arResult["PROPERTIES"]);
                    endif;*/
?>
                    <?if($arResult["PROPERTIES"]["products_services"]["VALUE"]&&CSite::InGroup(Array(1,15,16))):?>
                    <div id="tabs_block4">
                            <ul class="tabs" ajax="ajax">
                                <li>                                
                                    <a href="/bitrix/templates/main/components/bitrix/news.list/firms/services.php?services=<?=$arResult["PROPERTIES"]["services_bind"]["VALUE"]?>&<?=bitrix_sessid_get()?>">
                                        <div class="left tab_left"></div>
                                        <div class="left name"><?=GetMessage("R_SERVICES")?></div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>
                                    </a>                                                                
                                </li>
                            </ul>
                            <div class="panes">
                               <div class="pane" style="display:block"></div>
                            </div>
                    </div>
                    <?endif;?>
                    <?if ($arResult["NEWS1_COUNT"]>0):?>
                    <div class="left" id="tabs_block6" class="tabs">                        
                        <ul class="tabs big" ajax="ajax">
                            <?if ($arResult["NEWS1_COUNT"]>0):?>
                            <li>                                
                                <a href="<?=$templateFolder."/news.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name">Новости</div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                            <?endif;?>
                             <?if ($arResult["VIDEONEWS_COUNT"]>0):?>
                                <li><a href="<?=$templateFolder."/videonews.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                        <div class="left tab_left"></div>
                                        <div class="left name"><?=GetMessage("R_VIDEONEWS")?></div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>                                    
                                    </a>
                                </li>
                            <?endif;?>
                        </ul>    
                        <div class="panes">
                        <?if ($arResult["NEWS1_COUNT"]>0):?>
                            <div class="pane big" style="display:block">                        
                                    <?
                                    global $arrFilter1;
                                    $arReviewsIB = getArIblock("news", CITY_ID);
                                    $arrFilter1 = array("PROPERTY_RESTORAN"=>$arResult["ID"]);
                                        $APPLICATION->IncludeComponent(
                                                "restoran:catalog.list",
                                                "new_rest_main",
                                                Array(
                                                    "DISPLAY_DATE" => "N",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "Y",
                                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                                    "AJAX_MODE" => "N",
                                                    "IBLOCK_TYPE" => "news",
                                                    "IBLOCK_ID" => $arReviewsIB["ID"],
                                                    "NEWS_COUNT" => "3",
                                                    "SORT_BY1" => "ACTIVE_FROM",
                                                    "SORT_ORDER1" => "DESC",
                                                    "SORT_BY2" => "",
                                                    "SORT_ORDER2" => "",
                                                    "FILTER_NAME" => "arrFilter1",
                                                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                                    "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                                    "CHECK_DATES" => "N",
                                                    "DETAIL_URL" => "",
                                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                    "SET_TITLE" => "N",
                                                    "SET_STATUS_404" => "N",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                    "ADD_SECTIONS_CHAIN" => "N",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                    "PARENT_SECTION" => "",//$arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                                    "PARENT_SECTION_CODE" => "",
                                                    "CACHE_TYPE" => "N",
                                                    "CACHE_TIME" => "36000000",
                                                    "CACHE_FILTER" => "Y",
                                                    "CACHE_GROUPS" => "N",
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                                    "PAGER_TITLE" => "Новости",
                                                    "PAGER_SHOW_ALWAYS" => "N",
                                                    "PAGER_TEMPLATE" => "",
                                                    "PAGER_DESC_NUMBERING" => "N",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "N",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "AJAX_OPTION_HISTORY" => "N",
                                                    "NOLINK" => "N"
                                                ),
                                            false
                                            );?>
                            </div>
                            <?endif;?>
                            <?if ($arResult["VIDEONEWS_COUNT"]>0):?>
                                <div class="pane big"></div>
                            <?endif;?>
                        </div>
                        <Br />
                    </div>   
                    <?endif;?>
                    <?if ($arResult["NEWS_COUNT"]>0):?>
                    <div class="left" id="tabs_block7" class="tabs">                        
                        <ul class="tabs big" ajax="ajax">
                            <?if ($arResult["NEWS_COUNT"]>0):?>
                            <li>                                
                                <a href="<?=$templateFolder."/news.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name">Рестораторам</div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                            <?endif;?>
                        </ul>    
                        <div class="panes">
                        <?if ($arResult["NEWS_COUNT"]>0):?>
                            <div class="pane big" style="display:block">                        
                                    <?
                                   /* global $arrFilter1;
                                    $arrFilter1 = array("PROPERTY_RESTORAN"=>$arResult["ID"]);
                                        $APPLICATION->IncludeComponent(
                                                "restoran:catalog.list",
                                                "new_rest_main",
                                                Array(
                                                    "DISPLAY_DATE" => "N",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "Y",
                                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                                    "AJAX_MODE" => "N",
                                                    "IBLOCK_TYPE" => "firms_news",
                                                    "IBLOCK_ID" => 2423,
                                                    "NEWS_COUNT" => "3",
                                                    "SORT_BY1" => "ACTIVE_FROM",
                                                    "SORT_ORDER1" => "DESC",
                                                    "SORT_BY2" => "",
                                                    "SORT_ORDER2" => "",
                                                    "FILTER_NAME" => "arrFilter1",
                                                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                                    "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                                    "CHECK_DATES" => "N",
                                                    "DETAIL_URL" => "",
                                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                    "SET_TITLE" => "N",
                                                    "SET_STATUS_404" => "N",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                    "ADD_SECTIONS_CHAIN" => "N",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                    "PARENT_SECTION" => "",//$arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                                    "PARENT_SECTION_CODE" => "",
                                                    "CACHE_TYPE" => "N",
                                                    "CACHE_TIME" => "36000000",
                                                    "CACHE_FILTER" => "Y",
                                                    "CACHE_GROUPS" => "N",
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                                    "PAGER_TITLE" => "Новости",
                                                    "PAGER_SHOW_ALWAYS" => "N",
                                                    "PAGER_TEMPLATE" => "",
                                                    "PAGER_DESC_NUMBERING" => "N",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "N",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "AJAX_OPTION_HISTORY" => "N"
                                                ),
                                            false
                                            );*/?>
                            </div>
                            <?endif;?>
                        </div>
                        <Br />
                    </div>   
                    <?endif;?>
                    <div class="clear"></div>
                </div>
                <div class="right">                    
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_1_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                    false
                    );?>
                </div>
                <div class="clear"></div>
                <br /><br />
                <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
                <br /><Br />
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "bottom_rest_list",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                false
                );?>
                <br /><Br />
            </div>
            