$(document).ready(function(){
    $("body").on("click",".lookon_iframe",function(){
        if (!$("#lookon_iframe").size())
        {
            $("<div class='popup popup_modal' id='lookon_iframe'></div>").appendTo("body");                                                               
        }
        $('#lookon_iframe').css("width","85%");
        $('#lookon_iframe').css("height","80%");
        $('#lookon_iframe').html("<div class='modal_close_lookon'></div><iframe style='border:0px;' width='100%' height='100%' src='"+$(this).attr("href")+"'></iframe>");
        showOverflow();
        setCenter($("#lookon_iframe"));
        $("#lookon_iframe").show();  
        return false;
    });
    $("body").on("click",".modal_close_lookon",function(){
       hideOverflow();
       $("#lookon_iframe").hide();
    });
    /*$(".rating div").hover(
        function(){
            $(".rating div").attr("class","star");
            var count = $(this).attr("alt");
            var i = 1;
            $(".rating div").each(function(){                
                if (i<=count)
                    $(this).attr("class","star_a");
                else
                    return;
                i++;
            })            
        }
    );
        
    $(".rating div").click(
        function(){
              $("#rating_overlay").find("input[name=ratio]").val($(this).attr("alt"));   
              $("#rating_overlay").overlay({
                    top: 260,
                    closeOnClick: false,
                    closeOnEsc: true,
                     mask: {
                        color: '#000',
                        loadSpeed: 200,
                        closeSpeed:0,
                        opacity: 0.5
                    },
                    api: true                        
            }).load();      
        }
    );   */
    //send_ajax("/bitrix/components/restoran/restoraunts.detail/templates/.default/online.php", "html", false, users_onpage);    
    $("#likes").prepend($("#likes_inv").html());
    $("#likes_inv").remove();
    $(".active_rating div").hover(
        function(){
            $(this).parent().find("div").attr("class","star");
            var count = $(this).attr("alt");
            var i = 1;
            $(this).parent().find("div").each(function(){                
                if (i<=count)
                    $(this).attr("class","star_a");
                else
                    return;
                i++;
            })            
        }
    );    
    // validator locale
    $.tools.validator.localize("ru", {
        '*'	: 'Поле обязательно для заполнения',
    	'[req]'	: 'Поле обязательно для заполнения'
    });
    
    /*$("#reviews > form").validator({ lang: 'ru' }).submit(function(e) {
        var form = $(this);
        if (!e.isDefaultPrevented()) {
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $("#rating_overlay").html(data);
                    $(".error").hide();
                    if (typeof(success_submit_form)=="function")
                        success_submit_form();

                }
            });
            e.preventDefault();
        }
    });*/
});
function users_onpage(data)
{
    if (data)
        $("#users_onpage").html(data);
    else
        $("#users_onpage").parent().parent().remove();
}
function ratio(data)
{
    $("#ajax_ratio").html(data);
}
function success_submit_form()
{
    $("#rating_overlay").delay(2000).overlay();
    //.close();
}

function show_more(obj,clas,a,b)
{    
    if ($("."+clas).attr("text"))
    {
        if ($("."+clas+":visible").attr("text")==2)
        {
            $(obj).html(a);
            $(obj).parent().removeClass("more_properties_a");
            $(obj).parent().addClass("more_properties");
        }
        else
        {
            $(obj).html(b);
            $(obj).parent().removeClass("more_properties");
            $(obj).parent().addClass("more_properties_a");
            $('html,body').animate({scrollTop:"1000"}, 500);
        }
    }
    else
    {
        if ($("."+clas).is(":visible")||$("."+clas).attr("text")==2)
        {
            $(obj).html(a);
            $(obj).parent().removeClass("more_properties_a");
            $(obj).parent().addClass("more_properties");
        }
        else
        {        
            $(obj).html(b);
            $(obj).parent().removeClass("more_properties");
            $(obj).parent().addClass("more_properties_a");
            var ll = $(obj).offset().top+100;
            $('html,body').animate({scrollTop:ll}, 500);
        }
    }
    $("."+clas).toggle();
}
