<div id="order_new">
    <div class="left">
        <? if (LANGUAGE_ID == "ru"): ?>
            Организация банкетов и заказ столиков онлайн
        <? endif; ?>                
    </div>
    <div class="left h"></div>
    <div class="right">        
        <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>                  
                <a href="javascript:void(0)" class="small_bron bron_button_gradient2"><?= GetMessage("R_BOOK_TABLE2") ?></a>
                <a href="javascript:void(0)" class="small_ord bron_button_gradient1"><?= GetMessage("R_ORDER_BANKET2") ?></a>
        <?else:?>     
                <a href="javascript:void(0)" class="small_bron bron_button_gradient2"><?= GetMessage("R_BOOK") ?></a>
        <?endif;?>
    </div>    
        <script>
            $(document).ready(function(){                
                $(".small_bron").click(function(){
                    var _this = $(this);
                    $.ajax({
                        type: "POST",
                        url: "/tpl/ajax/online_order_rest_kld.php",
                        data: "name=<?= rawurlencode($arResult["NAME"]) ?>",
                        success: function(data) {
                            if (!$("#bron_modal").size())
                                $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                            $('#bron_modal').html(data);
                            showOverflow();
                            setCenter($("#bron_modal"));
                            $("#bron_modal").fadeIn("300"); 
                        }
                    });
                    return false;
                });

                $(".small_ord").click(function(){
                    var _this = $(this);
                    $.ajax({
                        type: "POST",
                        url: "/tpl/ajax/online_order_rest_kld.php",
                        data: "name=<?= rawurlencode($arResult["NAME"]) ?>&banket=Y",
                        success: function(data) {
                            if (!$("#bron_modal").size())
                                $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                            $('#bron_modal').html(data);
                            showOverflow();
                            setCenter($("#bron_modal"));
                            $("#bron_modal").fadeIn("300"); 
                        }
                    });
                    return false;
                });                                                                        
            });
        </script>                            
        <div class="clear"></div>
</div>
<div id="order_new_overflow">
    <div class="left" style="width:465px;padding-top: 10px;">
        <div class="left otitle">
            <?=$arResult["NAME"]?>
        </div>
        <div class="left orating" ></div>
        <script>
            $(document).ready(function(){
                send_ajax("/tpl/ajax/get_ratio.php?ID=<?=$arResult["ID"]?>&<?=bitrix_sessid_get()?>", "html", false, oratio); 
            });    
            function oratio(data)
            {
                $(".orating").html(data);
            }
        </script>
    </div>
    <div class="left ophone">
        Организация банкетов и заказ столиков онлайн        
    </div>
    <div class="left h"></div>
    <div class="right">
        <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>                  
                <a href="javascript:void(0)" class="small_bron bron_button_gradient2"><?= GetMessage("R_BOOK_TABLE2") ?></a>
                <a href="javascript:void(0)" class="small_ord bron_button_gradient1"><?= GetMessage("R_ORDER_BANKET2") ?></a>
        <?else:?>     
                <a href="javascript:void(0)" class="small_bron bron_button_gradient2"><?= GetMessage("R_BOOK") ?></a>
        <?endif;?>
    </div>
    <div class="clear"></div>
</div>
<script>
    $(document).ready(function(){
        $(window).scroll(function(){            
            if (($(this).scrollTop()>$("#minus_plus_buts").offset().top-100)&&$("#order_new_overflow").css("opacity")=="0")
            {
                $("#order_new_overflow").css({"top":"0px","opacity":1});
            }
            if ($(this).scrollTop()<$("#minus_plus_buts").offset().top-100&&$("#order_new_overflow").css("opacity")=="1")
            {
                $("#order_new_overflow").css({"top":"-80px","opacity":0});
            }
        });
    });
</script>