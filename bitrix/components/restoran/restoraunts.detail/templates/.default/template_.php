<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.min.js')?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.validator.js')?>
<?
global $element_id;
$element_id = $arResult["ID"];
$arAfishaIB = $arResult["AFISHA_IB"];
$arReviewsIB = getArIblock("reviews", CITY_ID);
?>
<!--<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==" type="text/javascript"></script>-->
<script src="http://api-maps.yandex.ru/1.1/index.xml?key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript"></script>
<div class="news-detail">   
    <script>
    $(document).ready(function(){  
        //minus_plus_buts 
        $("#minus_plus_buts").on("click",".plus_but a", function(event){
                var obj = $(this).parent("div");
                $.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
                    if(parseInt(otvet)){
                        $(".all_plus_minus").html($(".all_plus_minus").html()*1+1);
                        obj.html('<span></span>'+otvet);
                    }else{
                        if (!$("#promo_modal").size()){
                                $("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
                        }
                        $('#promo_modal').html(otvet);
                        showOverflow();
                        setCenter($("#promo_modal"));
                        $("#promo_modal").fadeIn("300");
                    }
                });
                return false;
            });
            $("#minus_plus_buts").on("click",".minus_but a", function(event){
                    var obj = $(this).parent("div");
                    $.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet){
                        if(parseInt(otvet)){
                                $(".all_plus_minus").html($(".all_plus_minus").html()*1-1);
                                obj.html('<span></span>'+otvet);
                        }else{
                            if (!$("#promo_modal").size()){
                                    $("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
                            }

                            $('#promo_modal').html(otvet);
                            showOverflow();
                            setCenter($("#promo_modal"));
                            $("#promo_modal").fadeIn("300");
                        }  			
                    });
                    return false;
            });

            $.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
                    $("#minus_plus_buts").html(otvet);
            });  
    });
    </script> 
         <div class="catalog_detail">
                <div class="left detail">                   
                    <?
                    //Подключаем форму бронирования
					if ($_REQUEST["bron_new"] == 'Y'){
						//include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/bron_form.php");
						include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/bron_form_new.php");
					}
					else{
                    if (CITY_ID!="tmn"&&CITY_ID!="ufa"&&CITY_ID!="kld"):
                        //var_dump($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/bron_form.php");
                        include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/bron_form.php");
                    endif;
                    if (CITY_ID=="kld"):
                        //var_dump($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/bron_form.php");
                        include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/bron_form_kld.php");
                    endif;
					}
                    ?>
                    <?if ($arResult["SECTION"]["PATH"][0]["CODE"]=="restaurants"):?>
                        <?$rest_name = GetMessage("R_REST")." "?>
                        <h1><?=GetMessage("R_REST")?> <?=$arResult["NAME"]?></h1>
                    <?else:?>
                        <?$rest_name=GetMessage("R_REST")." "?>
                        <h1><?=GetMessage("R_BH")?> <?=$arResult["NAME"]?></h1>
                    <?endif;?>                                  
                    <div id="ajax_ratio" class="rating left" style="margin-left:30px; padding-top:18px;">                            
                        <script>
                            $(document).ready(function(){
                                send_ajax("/tpl/ajax/get_ratio.php?ID=<?=$arResult["ID"]?>&<?=bitrix_sessid_get()?>", "html", false, ratio); 
                            });                                
                        </script>
                    </div>
                    <div class="clear"></div>           
                    <div class="left" style="width:397px; position:relative">                        
                        <?if ($arResult["PROPERTIES"]["RATIO"]["VALUE"]>0):?>
                        <div class="ratio_box">                                
                            Рейтинг<br />
                            <div><?=sprintf("%01.2f", $arResult["PROPERTIES"]["RATIO"]["VALUE"]);?></div>
                        </div>                        
                        <?endif;?>
                        <div id="photogalery">                               
                            <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>  
                                <?//if ($USER->IsAdmin()):?>                                    
                                        <script src="<?=$templateFolder?>/galery.js?2"></script>                                        
                                        <div id="gal1">
                                            <div class="img img4">
                                                <img src="<?=$arResult["PROPERTIES"]["photos"]["VALUE2"][0]["src"]?>" alt="<?=$rest_name.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0]:""?>" height="264" />                                        
                                            </div>
                                            <div class="special_scroll">   
                                                <div class="scroll_container">                                            
                                                <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>
                                                    <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                        <img src="<?=$photo["src"]?>" alt="<?=$rest_name.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key]:""?>" align="bottom" width="75" />
                                                    </div>                                          
                                                <?endforeach;?>
                                                </div>
                                            </div>
                                            <div class="left scroll_arrows">
                                                <a class="prev browse left"></a>
                                                <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["photos"]["VALUE"])?></div>
                                                <a class="next browse right"></a>
                                            </div>
                                            <div class="right" style="width:255px; padding-top:20px;" align="right">
                                                <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                                    <a href="javascript:void(0)" onclick="$('#gal2').show();$('#gal1').hide()" class="another"><?=GetMessage("R_PHOTO2")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                                <?elseif($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&!$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                                    <a href="javascript:void(0)" onclick="$('#gal2').show();$('#gal1').hide()" class="another"><?=GetMessage("R_PHOTO2")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>
                                                <?elseif(!$arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                                    <a href="javascript:void(0)" onclick="$('#gal2').show();$('#gal1').hide()" class="another"><?=GetMessage("R_PHOTO2")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                                <?else:?>
                                                    <a href="javascript:void(0)" onclick="$('#gal2').show();$('#gal1').hide()" class="another"><?=GetMessage("R_PHOTO2")?></a>
                                                <?endif;?>
                                            </div> 
                                            <div class="clear"></div>
                                        </div>
                                        <div id="gal2">    
                                            <div class="img img4">
                                            <img src="<?=$arResult["PROPERTIES"]["photos"]["VALUE2"][0]["src"]?>" alt="<?=$rest_name.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0]:""?>" height="264" />                                        
                                        </div>
                                            <div class="all_gallery">
                                                <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>
                                                    <div class="item <?=($key==0)?"active":""?> <?=(($key+1)%4==0&&$key!=0)?"end":""?>" num="<?=($key+1)?>">
                                                        <img src="<?=$photo["src"]?>" alt="<?=$rest_name.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key]:""?>" align="bottom" width="93" />
                                                    </div>                                          
                                                <?endforeach;?>                                                
                                            </div>
                                            <div class="right" style="width:255px; padding-top:20px;" align="right">
                                                <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                                    <a href="javascript:void(0)" onclick="$('#gal1').show();$('#gal2').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                                <?elseif($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&!$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                                    <a href="javascript:void(0)" onclick="$('#gal1').show();$('#gal2').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>
                                                <?elseif(!$arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                                    <a href="javascript:void(0)" onclick="$('#gal1').show();$('#gal2').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                                <?else:?>
                                                    <a href="javascript:void(0)" onclick="$('#gal1').show();$('#gal2').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>
                                                <?endif;?>
                                            </div>    
                                            <div class="clear"></div>
                                        </div>                                                                        
                                <?/*else:?>
                                    <script src="<?=$templateFolder?>/galery.js"></script>
                                    <div class="img img4">
                                        <img src="<?=$arResult["PROPERTIES"]["photos"]["VALUE2"][0]["src"]?>" alt="<?=$rest_name.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0]:""?>" height="264" />                                        
                                    </div>
                                    <div class="special_scroll">   
                                        <div class="scroll_container">                                            
                                        <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>
                                            <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                <img src="<?=$photo["src"]?>" alt="<?=$rest_name.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key]:""?>" align="bottom" width="75" />
                                            </div>                                          
                                        <?endforeach;?>
                                        </div>
                                    </div>
                                    <div class="left scroll_arrows">
                                        <a class="prev browse left"></a>
                                        <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["photos"]["VALUE"])?></div>
                                        <a class="next browse right"></a>
                                    </div>
                                    <div class="right" style="width:255px; padding-top:20px;" align="right">
                                        <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                            <?=GetMessage("R_PHOTO")?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                        <?elseif($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&!$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                            <?=GetMessage("R_PHOTO")?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>
                                        <?elseif(!$arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]&&$arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                            <?=GetMessage("R_PHOTO")?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                        <?endif;?>
                                    </div> 
                                <?endif;*/?>
                            <?else:?>                            
                                    <div class="img img2">
                                        <?if ($arResult["PREVIEW_PICTURE"]["SRC"]):?>
                                            <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>"/>
                                        <?else:?>
                                            <img src="/tpl/images/noname/rest_nnm_new.png" width="380" />
                                        <?endif;?>
                                    </div>
                            <?endif;?>
                                <div class="clear"></div>
                        </div>
                        <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]):?>
                            <div id="photogalery2" style="display:none">
                                    <script>
                                        $(document).ready(function(){
                                            $("#photogalery2").galery({});
                                        });
                                    </script>
                                    <?                                    
                                       /* if($arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][0]==1)
                                            $arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][0] = 0;
                                        if(!$arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][0])
                                            $arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][0] = 1;*/
                                        ?>
                                    <div class="img img4">
                                        <?/*if ($USER->IsAdmin()):
                                            v_dump($arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"]);
                                        endif;*/
                                        ?>
                                        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="266" border="0">
                                            <param name="movie" value="/tpl/js/panorama.swf?img=<?=CFile::GetPath($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0])?>&amp;type=<?=($arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][0])?0:1?>">
                                            <param name="quality" value="high">
                                            <param name="bgcolor" value="#FFFFFF">
                                            <embed src="/tpl/js/panorama.swf?img=<?=CFile::GetPath($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0])?>&amp;type=<?=($arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][0])?0:1?>" width="397" height="266" border="0" type="application/x-shockwave-flash">
                                        </object>
                                    </div>
                                    <div class="special_scroll">   
                                        <div class="scroll_container">
                                        <?foreach($arResult["PROPERTIES"]["videopanoramy"]["VALUE"] as $key=>$photo):?>
                                            <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                <img video="video" src="<?=CFile::GetPath($photo)?>" align="bottom" type="<?=(!$arResult["PROPERTIES"]["videopanoramy"]["DESCRIPTION"][$key])?"1":"0"?>" height="50" />
                                            </div>                                          
                                        <?endforeach;?>
                                        </div>
                                    </div>
                                    <div class="left scroll_arrows">
                                        <a class="prev browse left"></a>
                                        <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["videopanoramy"]["VALUE"])?></div>
                                        <a class="next browse right"></a>
                                    </div>
                                    <div class="right" style="width:255px; padding-top:20px;" align="right">  
                                        <?if ($arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>  
                                            <a href="javascript:void(0)" onclick="$('#photogalery').show();$('#photogalery2').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<?=GetMessage("R_VIDEOPRESENTATION")?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery2').hide();" class="another"><?=GetMessage("R_3D")?></a>
                                        <?else:?>
                                            <a href="javascript:void(0)" onclick="$('#photogalery').show();$('#photogalery2').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<?=GetMessage("R_VIDEOPRESENTATION")?>
                                        <?endif;?>
                                    </div>
                                    <div class="clear"></div>
                            </div>
                        <?endif;?>
                        <?if ($arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>                            
                            <div id="photogalery3" style="display:none">
                                <script>
                                        $(document).ready(function(){
                                            $("#photogalery3").galery({});
                                        });
                                    </script>
                                <div class="img img4">
                                    <?if($arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][0]):?>
                                        <a target="_blank" class="lookon_iframe" href="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][0]?>"></a>
                                        <embed type="application/x-shockwave-flash" src="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][0]?>" width="395" height="264" style="undefined" id="krpanoSWFObject" name="krpanoSWFObject" bgcolor="#000000" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="xml=<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][0]?>">    
                                    <?else:?>
                                        <object type="application/x-shockwave-flash" data="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][0]?>" width="397" height="266" allowFullScreen="true"><param name="movie" value="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][0]?>"></object>
                                    <!--<img src="<?=CFile::GetPath($arResult["PROPERTIES"]["d_tour"]["VALUE"][0])?>" alt="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][0]?>" height="264" />-->
                                    <?endif;?>
                                </div>
                                <div class="special_scroll">   
                                    <div class="scroll_container">                                            
                                        <?foreach($arResult["PROPERTIES"]["d_tour"]["VALUE"] as $key=>$photo):?>
                                            <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                <?if($arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key]):?>
                                                    <img lookon="lookon" src="<?=CFile::GetPath($photo)?>" alt_iframe="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][$key]?>" alt="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key]?>" alt_xml="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][$key]?>" align="bottom" width="75" />
                                                <?else:?>
                                                    <img dtour="dtour" src="<?=CFile::GetPath($photo)?>" alt="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][$key]?>" align="bottom" width="75" />
                                                <?endif;?>
                                            </div>                                          
                                        <?endforeach;?>
                                    </div>
                                </div>
                                <div class="left scroll_arrows">
                                    <a class="prev browse left"></a>
                                    <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["d_tour"]["VALUE"])?></div>
                                    <a class="next browse right"></a>
                                </div>
                                <div class="right" style="width:255px; padding-top:20px;" align="right">
                                    <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]):?>
                                    <!--<a href="#" class="another"><?=GetMessage("R_3D")?></a>&nbsp;&nbsp;&nbsp;-->
                                         <a href="javascript:void(0)" onclick="$('#photogalery').show();$('#photogalery3').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery3').hide()" class="another"><?=GetMessage("R_VIDEOPRESENTATION")?></a>&nbsp;&nbsp;&nbsp;<?=GetMessage("R_3D")?>
                                    <?else:?>
                                         <a href="javascript:void(0)" onclick="$('#photogalery').show();$('#photogalery3').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<?=GetMessage("R_3D")?>
                                    <?endif;?>
                                </div> 
                                <div class="clear"></div>
                            </div>
                        <?endif;?>
                        <br />
                        <div style="width:125px;">                            
                            <a href="#comment_form" class="greyb"><?=GetMessage("R_ADD_REVIEW")?></a>
                        </div>
                    </div>
                    <div class="right" style="width:322px; position:relative" id="for_edit_link">           
                        <h3 style="margin-top:0px"><?=GetMessage("SMALL_PROPERTIES")?></h3>
                        <?
                        $temp = explode(',',$arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]);
                        $map_data["yandex_lat"] = (double)$temp[0];
                        $map_data["yandex_lon"] = (double)$temp[1];
                        $map_data["yandex_scale"] = 16;
                        $map_data["PLACEMARKS"][0] = array(
                            "LON" => (double)$temp[1],
                            "LAT" => (double)$temp[0],
                            "TEXT" => $arItem["NAME"],
                        );
                        $map_data = serialize($map_data);
                        ?>                        
                            <?/*if ($arResult["PROPERTIES"]["RATIO"]["VALUE"]>0):?>
                                <div class="position2 left" style="padding-top:8px;">                                
                                    <div><?=sprintf("%01.2f", $arResult["PROPERTIES"]["RATIO"]["VALUE"]);?></div>                                
                                    Рейтинг
                                </div>
                            <?endif;?>
                            <?if ($arResult["VALENTINE_COUNT"]):?>
                                <div class="position2 left">                                
                                    <a href="<?=$arResult["VALENTINE_LINK"]?>"><img alt="День святого Валентина" title="День святого Валентина" src="<?=$templateFolder."/images/valen.png"?>" height="48"/></a>
                                </div>
                            <?endif;?>
                            <?if ($arResult["PROPERTIES"]["d_tours"]["VALUE"]):?>
                                <div class="position2 right" style="margin-top:10px">                                
                                    <a href="javascript:void(0)" onclick="$('#photogalery').hide();$('#photogalery2').hide();$('#photogalery3').show()"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour.png" width="70"/></a>
                                </div>
                            <?endif;?>
                            <?if ($arResult["CORP_COUNT"]):?>
                                <div class="position2 left">                                
                                    <a href="<?=$arResult["CORP_LINK"]?>"><img alt="Новогодний корпоратив" title="Новогодний корпоратив" src="<?=$templateFolder."/images/ny_corp.png"?>" height="48"/></a>
                                </div>
                            <?endif;?>
                            <?if ($arResult["NIGHT_COUNT"]):?>
                                <div class="position2 left">                                
                                    <a href="<?=$arResult["NIGHT_LINK"]?>"><img alt="Новогодняя ночь" title="Новогодняя ночь"  src="<?=$templateFolder."/images/ny_night.png"?>" height="48" /></a>
                                </div>
                            <?endif;?>
                            
                            <div class="clear"></div>
                           */?>
                        <table class="properties" cellpadding="5" cellspacing="0"  itemscope itemtype="http://schema.org/Organization">
                            <?$i=0;?>                            
                                <?if ($arResult["DISPLAY_PROPERTIES"]["kitchen"]["VALUE"]):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_kitchen")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?>                                                
                                            </td>
                                        </tr>
                                <?endif;?> 
                                <?if ($arResult["DISPLAY_PROPERTIES"]["average_bill"]["VALUE"]):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_average_bill")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                
                                                <?$ab = strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))?>                                                
                                                <?if (CITY_ID=="tmn"):
                                                    $ab = str_replace("до 1000р","500 - 1000р",$ab);
                                                    echo $ab;
                                                else:?>
                                                    <?=$ab?>
                                                <?endif;?>
                                            </td>
                                        </tr>
                                <?endif;?> 
                                    <?if ($arResult["DISPLAY_PROPERTIES"]["banket_average_bill"]["VALUE"]):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name" style="line-height:12px;"><?=GetMessage("R_PROPERTY_banket_average_bill")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?=$arResult["DISPLAY_PROPERTIES"]["banket_average_bill"]["VALUE"]?>                                                
                                            </td>
                                        </tr>
                                <?endif;?> 
                                <?if ($arResult["DISPLAY_PROPERTIES"]["subway"]["VALUE"]):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_subway")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?if ($arResult["ID"]==387287):?>
                                                    <p class="metro_<?=CITY_ID?>"><?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))?></p>
                                                <?else:?>
                                                    <?if (is_array($arResult["DISPLAY_PROPERTIES"]["subway"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"])>3):?>
                                                        <p class="metro_<?=CITY_ID?>"><?=$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]?></p>
                                                    <?else:?>
                                                        <p class="metro_<?=CITY_ID?>"><?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))?></p>
                                                    <?endif;?>
                                                <?endif;?>
                                            </td>
                                        </tr>
                                <?endif;?>
                                <?if ($arResult["DISPLAY_PROPERTIES"]["area"]["VALUE"]):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_area")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["area"]["DISPLAY_VALUE"]))?>
                                            </td>
                                        </tr>
                                <?endif;?>
                                <?if ($arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"]):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_phone")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?if ($arResult["ID"]!=387287):?>
                                                    <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                                        <?if (CITY_ID=="spb"):?>
                                                            <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                                        <?else:?>
                                                            <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                                        <?endif;?>   
                                                    <?else:?>

                                                            <?
                                                            if ($arResult["ID"]==387180)
                                                            {
                                                                $temp_phones = $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"];
                                                                $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = Array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);                                                                
                                                            }
                                                            $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = str_replace(";", "", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);

                                                            if(is_array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])) 
                                                                    $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]=implode(", ",$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);							
                                                            else{
                                                                    $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]=$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"];
                                                                    //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                            }
                                                            $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"]=  explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]);

                                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                            preg_match_all($reg, $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"], $matches);


                                                            $TELs=array();
                                                            for($p=1;$p<5;$p++){
                                                                    foreach($matches[$p] as $key=>$v){
                                                                            //if($p==1 && $v!="") $TELs[$key].="+7";
                                                                            $TELs[$key].=$v;
                                                                    }	
                                                            }

                                                            foreach($TELs as $key => $T){
                                                                    if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                                            }
                                                            $p_counter = 0;
                                                            foreach($TELs as $key => $T){
                                                                    ?>
                                                                    <a href="tel:<?=$T?>" class="<? echo MOBILE;?>"><?=$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ',<br /> ';?>
                                                            <?if (is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):
                                                                    break;
                                                            endif;?>
                                                            <?}   

                                                            ?>
                                                            <?
                                                            if ($arResult["ID"]==387180)
                                                            {
                                                                $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] =$temp_phones;                                                                
                                                            }
                                                            ?>
                                                    <?endif;?>
                                                <?endif;?>
                                            </td>
                                        </tr>
                                        <?if ($arResult["ID"]==387287):?>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] as $key=>$ph_bizon):
                                                    echo $ph_bizon." (".str_replace("Москва, ","",$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][$key]).")<Br />";
                                                endforeach;?>
                                            </td>
                                        </tr>
                                        <?endif;?>
                                <?endif;?>
                                <?if ($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])==1):?>
                                        <tr itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_address")?>&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td itemprop="streetAddress">
                                                <?
                                                if (is_array($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])):
                                                    echo $arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"][0];
                                                else:
                                                    echo $arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"];
                                                endif;
                                                ?>
                                            </td>
                                        </tr>
                                <?endif;?>
                                <?if ($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"])==1):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_number_of_rooms")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?
                                                if (is_array($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"])):
                                                    echo $arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"][0];
                                                else:
                                                    echo $arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"];
                                                endif;
                                                ?>
                                            </td>
                                        </tr>
                                <?endif;?>
                                <?if ($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["VALUE"])==1):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=GetMessage("R_PROPERTY_opening_hours")?>:&nbsp; </span>
                                                </div>                                                
                                            </td>
                                            <td>
                                                <?=implode(", ",$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"])?>
                                            </td>
                                        </tr>
                                <?endif;?>
                        </table>                                                  
                        <?if ($arResult["SECTION"]["PATH"][0]["CODE"]=="restaurants"):?>
                            <?$title_v="<h1>".$rest_name.$arResult["NAME"]."</h1>";?>                            
                        <?else:?>
                            <?$title_v="<h1>".$rest_name.$arResult["NAME"]."</h1>";?>
                        <?endif;?>                         
                        <?
                          $params = array();
                          $params["id"] = $arResult["ID"];
                          $params = addslashes(json_encode($params));            
                        ?>
                        
                            <div class="rest_buttons">
                                <?if ($arResult["DISPLAY_PROPERTIES"][$last_pid]!=end($arResult["DISPLAY_PROPERTIES"])):?>
                                    <div class="more_properties" style="margin-bottom:10px;"><a href="javascript:void(0)" onclick="show_more(this,'more_props','<?=GetMessage("R_MORE_PROPERTIES_1")?>','<?=GetMessage("R_MORE_PROPERTIES_2")?>')" class="js"><?=GetMessage("R_MORE_PROPERTIES_1")?></a></div>
                                    <div class="clear"></div>
                                <?endif;?>                        
                                <?if (CITY_ID!="tmn"&&CITY_ID!="ufa"):?>
                                    <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>
                                        <div class="stolik_shadow" style="margin-right:40px;">
                                            <a href="javascript:void(0)" class="small_bron bron_button_gradient1"><?=GetMessage("R_BOOK_TABLE2")?></a>
                                        </div> 
                                        <!--<input type="button" class="small_bron light_button" value="забронировать столик" />                                
                                        <br />-->
                                    <?endif;?>                                                                                                         
                                        <!--<input type="button" class="small_ord dark_button" value="Заказать банкет" />-->
                                        <div class="banket_shadow">
                                            <a href="javascript:void(0)" class="small_ord bron_button_gradient2"><?=GetMessage("R_ORDER_BANKET2")?></a>
                                        </div>            
                                <?endif;?>
                            </div>                                            
                    </div>
                    <div class="clear"></div> 
                    <br/>					
                    <div class="staya_likes">
                        <div id="minus_plus_buts"></div>
                        <div class="left">
                            <div style="padding-left:45px;"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a></div> 
                        </div>
                        <div class="right">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:asd.share.buttons",
                                "likes",
                                Array(
                                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                                    "ASD_PICTURE" => "",
                                    "ASD_TEXT" => ($arResult["PREVIEW_TEXT"] ? $arResult["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                                    "LIKE_TYPE" => "LIKE",
                                    "VK_API_ID" => "2881483",
                                    "VK_LIKE_VIEW" => "mini",
                                    "SCRIPT_IN_HEAD" => "N"
                                )
                            );?> 
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?
                    //Выводим оставшиеся своства
                    if ($arResult["DISPLAY_PROPERTIES"][$last_pid]!=end($arResult["DISPLAY_PROPERTIES"])):
                        include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/properties.php");
                    endif;?>
                    <div class="preview_text" text="1">
                        <?=$arResult["PREVIEW_TEXT"]?>
                    </div>
                    <div class="preview_text" text="2" style="display:none">
                        <?=$arResult["DETAIL_TEXT"]?>
                    </div>
                    <?if ($arResult["DETAIL_TEXT"]):?>
                        <div class="more_properties"><a href="javascript:void(0)" onclick="show_more(this,'preview_text','<?=GetMessage("R_MORE_TEXT_1")?>','<?=GetMessage("R_MORE_TEXT_2")?>')" class="js"><?=GetMessage("R_MORE_TEXT_1")?></a></div>
                    <?endif;?>
                </div>
                <div class="right" style="width:240px;">
                    <div id="right_2_main_page" class=" right_block banner_ajax"></div>                                
                    <div class="clear"></div>
                    <br /><Br />
                    <div id="counter" align="center"><img src='/tpl/ajax/counter.php?ID=<?=$arResult["ID"]?>' ALT="<?=GetMessage('R_COUNTER')?>" title="<?=GetMessage('R_COUNTER')?>" /></div>
                    <br /><br />
                    <?if ($arResult["MENU_ID"]):?>
                        <a href="<?=$arResult["DETAIL_PAGE_URL"]?>menu/" style="text-decoration:none"><div class="top_block"><?=GetMessage("IN_MENU")?></div></a>
                        <?//global $arrFil;
                        //$arrFil["!PREVIEW_PICTURE"]=false?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "menu_rest",
                                Array(
                                        "DISPLAY_DATE" => "Y",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "Y",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "rest_menu_ru",
                                        "IBLOCK_ID" => $arResult["MENU_ID"],
                                        "NEWS_COUNT" => "2",
                                        "SORT_BY1" => "tags",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "date_create",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "arrFil",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => array("ratio", "reviews"),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "150",
                                        "ACTIVE_DATE_FORMAT" => "j F Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N"
                                ),
                            false
                            );?>   
                        <Br />
                        <div class="right"><a href="<?=$arResult["DETAIL_PAGE_URL"]?>menu/" target="_blank" class="uppercase"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_MENU")?></a></div>
                    <?else:?>
                        <div id="right_1_main_page" class=" right_block banner_ajax"></div>
                        <?/*$APPLICATION->IncludeComponent(
                                "bitrix:advertising.banner",
                                "",
                                Array(
                                        "TYPE" => "right_1_main_page",
                                        "NOINDEX" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                ),
                                false
                        );*/?>
                    <?endif;?>
                </div>
                <div class="clear"></div> 
            </div>    
            <div class="catalog_detail">
                <?
                //Выводим контактные данные
                include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/contacts.php");
                ?>
            </div>
            <?if ($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]):
                //Выводим карту
                include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/map.php");
            endif;?>
            <div class="catalog_detail">
                <?if ($arResult["BLOG_COUNT"]||$arResult["NEWS_COUNT"]||$arResult["OVERVIEWS_COUNT"]||$arResult["VIDEONEWS_COUNT"]||$arResult["PHOTO_COUNT"]||$arResult["KUPONS_COUNT"]||$arResult["VERANDA_COUNT"]||$arResult["NIGHT_COUNT"]||$arResult["CORP_COUNT"]||$arResult["MC_COUNT"]||$arResult["VALENTINE_COUNT"]||$arResult["POST_COUNT"]||$arResult["LETN_COUNT"]):?>
                <div id="tabs_block6" class="tabs">
                    <ul class="tabs big" ajax="ajax">
                        <?if ($arResult["NEWS_COUNT"]>0):?>
                        <li>                                
                            <a href="<?=$templateFolder."/news.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_NEWS")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>                                                                
                        </li>
                        <?endif;?>
                        <?if ($arResult["BLOG_COUNT"]>0):?>
                        <li>                                
                            <a href="<?=$templateFolder."/blogs.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_BLOGS")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>                                                                
                        </li>
                        <?endif;?>
                        <?if ($arResult["LETN_COUNT"]>0):?>
                        <li>                                
                            <a href="<?=$templateFolder."/news.php?letn=Y&id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_VERANDA")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>                                                                
                        </li>
                        <?endif;?>
                        <?if ($arResult["OVERVIEWS_COUNT"]>0):?>
                        <li>                                
                            <a href="<?=$templateFolder."/overviews.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_OVERVIEWS")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>                                                                
                        </li>
                        <?endif;?>
                        <?if ($arResult["VIDEONEWS_COUNT"]>0):?>
                        <li><a href="<?=$templateFolder."/videonews.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_VIDEONEWS")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>                                    
                            </a>
                        </li>
                        <?endif;?>
                        <?if ($arResult["PHOTO_COUNT"]>0):?>
                        <li><a href="<?=$templateFolder."/photoreviews.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_PHOTOREVIEWS")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>                                    
                            </a>
                        </li>
                        <?endif;?>
                        <?if ($arResult["KUPONS_COUNT"]>0):?>
                        <li><a href="<?=$templateFolder."/kupons.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_KUPONS")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>                                    
                            </a>
                        </li>
                        <?endif;?>
                        <?if ($arResult["MC_COUNT"]>0):?>
                        <li><a href="<?=$templateFolder."/masterclass.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_MC")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>                                    
                            </a>
                        </li>
                        <?endif;?>
                        <?if ($arResult["VERANDA_COUNT"]>0):?>
                        <li><a href="<?=$templateFolder."/veranda.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                <div class="left tab_left"></div>
                                <div class="left name"><?=GetMessage("R_VERANDA")?></div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>                                    
                            </a>
                        </li>
                        <?endif;?>
                        <?if ($arResult["POST_COUNT"]>0&&LANGUAGE_ID!="en"):?>
                                <li><a href="<?=$templateFolder."/post.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                        <div class="left tab_left"></div>
                                        <div class="left name" style="position:relative;">
                                            Великий пост
                                            <!--<div style="position:absolute; top:-10px; left:-21px; width:60px; height:60px;">
                                            <img src="<?=$templateFolder."/images/ny_corp.png"?>" />
                                            </div>-->
                                        </div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>                                    
                                    </a>                                    
                                </li>
                        <?endif;?>
                            <?/*if ($arResult["CORP_COUNT"]>0):?>
                                <li><a href="<?=$templateFolder."/ny_corp.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                        <div class="left tab_left"></div>
                                        <div class="left name" style="position:relative; padding-left:40px;">
                                            Новогодний корпоратив
                                            <div style="position:absolute; top:-10px; left:-21px; width:60px; height:60px;">
                                            <img src="<?=$templateFolder."/images/ny_corp.png"?>" />
                                            </div>
                                        </div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>                                    
                                    </a>                                    
                                </li>
                            <?endif;?>
                            <?if ($arResult["NIGHT_COUNT"]>0):?>
                                <li><a href="<?=$templateFolder."/ny_night.php?id=".$arResult["ID"]?>&c=<?=$_REQUEST["CITY_ID"]?>">
                                        <div class="left tab_left"></div>
                                        <div class="left name" style="position:relative; padding-left:40px;">
                                            Новогодняя ночь
                                            <div style="position:absolute; top:-10px; left:-21px; width:60px; height:60px;">
                                                <img src="<?=$templateFolder."/images/ny_night.png"?>" />
                                            </div>
                                        </div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>                                    
                                    </a>
                                </li>
                            <?endif;*/?>
                    </ul>
                    <div class="panes">
                        <?if ($arResult["NEWS_COUNT"]>0):?>
                            <div class="pane big" style="display:block"></div>
                        <?endif;?>
                        <?if ($arResult["BLOG_COUNT"]>0):?>
                            <div class="pane big" style="display:block"></div>
                        <?endif;?>
                        <?if ($arResult["LETN_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["OVERVIEWS_COUNT"]>0):?>
                            <div class="pane big" style="display:block"></div>
                        <?endif;?>
                        <?if ($arResult["VIDEONEWS_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["PHOTO_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["MC_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["VERANDA_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["POST_COUNT"]>0&&LANGUAGE_ID!="en"):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?/*if ($arResult["KUPONS_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["CORP_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["NIGHT_COUNT"]>0):?>
                            <div class="pane big"></div>
                        <?endif;*/?>
                    </div>
                </div>
                <?endif;?>
                <div class="left">
                    <?if ($arResult["AFISHA_COUNT"]):?>
                        <div id="tabs_block4">
                                <ul class="tabs" ajax="ajax">
                                    <li>                                
                                        <a href="<?=$templateFolder."/afisha.php?id=".$arResult["ID"]?>">
                                            <div class="left tab_left"></div>
                                            <div class="left name"><?=GetMessage("R_AFISHA")?></div>
                                            <div class="left tab_right"></div>
                                            <div class="clear"></div>
                                        </a>                                                                
                                    </li>
                                </ul>
                                <div class="panes">
                                <div class="pane" style="display:block"></div>
                                </div>
                        </div>
                    <?endif;?>
                    <div id="tabs_block7">
                        <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <ul class="tabs" ajax="ajax">
                            <li>                                
                                <a href="<?=$templateFolder."/reviews.php?id=".$arResult["ID"]?>&url=<?=$url?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_REVIEWS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                            <li>
                                <a href="#">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_VK")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_FACEBOOK")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>
                            </li>
                        </ul>
                        <div class="panes">
                           <div class="pane" style="display:block"></div>
                           <div class="pane" style="display:block">
                            <!-- Put this script tag to the <head> of your page -->
                                <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>

                                <script type="text/javascript">
                                VK.init({apiId: <?=(CITY_ID=="tmn")?"3559173":"2881483"?>, onlyWidgets: true});
                                </script>

                                <!-- Put this div tag to the place, where the Comments block will be -->
                                <div id="vk_comments"></div>
                                <script type="text/javascript">
                                VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                                </script>
                            </div>
                            <div class="pane">
                                    <div class="fb-comments" data-href="http://www.restoran.ru<?=$APPLICATION->GetCurPage()?>" data-num-posts="20" data-width="728"></div>
                            </div>
                        </div>                                               
                    </div>                    
                    <?if ($arResult["BLOGS_COUNT"]>0):?>
                    <br /><br />
                        <div id="tabs_block8">
                            <ul class="tabs" ajax="ajax">
                                <li>                                
                                    <a href="<?=$templateFolder."/blog.php?id=".(int)$arResult["PROPERTIES"]["user_bind"]["VALUE"][0]."&".bitrix_sessid_get()?>">
                                        <div class="left tab_left"></div>
                                        <div class="left name"><?=GetMessage("R_BLOG")?></div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>
                                    </a>                                                                
                                </li>
                            </ul>
                            <div class="panes">
                            <div class="pane"></div>
                            </div>
                        </div>   
                    <?endif;?>
                    <?if ($arResult["PROPERTIES"]["rest_group"]["VALUE"]):?>
                        <br /><br />
                        <div  id="tabs_block9">
                            <ul class="tabs">
                                <li>                                
                                    <a href="#">
                                        <div class="left tab_left"></div>
                                        <div class="left name"><?=GetMessage("R_REST_GROUP")?> <?=strip_tags($arResult["DISPLAY_PROPERTIES"]["rest_group"]["DISPLAY_VALUE"])?></div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>
                                    </a>                                                                
                                </li>
                            </ul>
                            <div class="panes">
                            <div class="pane">
                                <?
                                global $arSimilar;
                                $arSimilar["!ID"] = $arResult["ID"];?>                                        
                                <?$arSimilar["PROPERTY_rest_group"] = $arResult["PROPERTIES"]["rest_group"]["VALUE"];?>        
                                        <?$APPLICATION->IncludeComponent("restoran:restoraunts.list", "similar", array(
                                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                "IBLOCK_ID" => $_REQUEST["CITY_ID"],
                                                "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                                                "NEWS_COUNT" => "99",
                                                "SORT_BY1" => "ACTIVE_FROM",
                                                "SORT_ORDER1" => "DESC",
                                                "SORT_BY2" => "SORT",
                                                "SORT_ORDER2" => "ASC",
                                                "FILTER_NAME" => "arSimilar",
                                                "PROPERTY_CODE" => $arParams["SIMILAR_PROPERTIES"],
                                                "CHECK_DATES" => "Y",
                                                "DETAIL_URL" => "",
                                                "AJAX_MODE" => "N",
                                                "AJAX_OPTION_SHADOW" => "Y",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N",
                                                "CACHE_TYPE" => "A",
                                                "CACHE_TIME" => "36000000",
                                                "CACHE_FILTER" => "Y",
                                                "CACHE_GROUPS" => "N",
                                                "PREVIEW_TRUNCATE_LEN" => "",
                                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                "SET_TITLE" => "Y",
                                                "SET_STATUS_404" => "N",
                                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                                "ADD_SECTIONS_CHAIN" => "Y",
                                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                "DISPLAY_TOP_PAGER" => "N",
                                                "DISPLAY_BOTTOM_PAGER" => "N",
                                                "PAGER_TITLE" => "Рестораны",
                                                "PAGER_SHOW_ALWAYS" => "N",
                                                "PAGER_TEMPLATE" => "",
                                                "PAGER_DESC_NUMBERING" => "N",
                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                "PAGER_SHOW_ALL" => "N",
                                                "DISPLAY_DATE" => "Y",
                                                "DISPLAY_NAME" => "Y",
                                                "DISPLAY_PICTURE" => "Y",
                                                "DISPLAY_PREVIEW_TEXT" => "N",
                                                "AJAX_OPTION_ADDITIONAL" => "",
                                                "NO_SLEEP" => "Y"
                                                ),
                                                false
                                        );?>
                            </div>
                            </div>
                        </div> 
                    <?endif;?>                    
                    <br />
                    <div id="bottom_content_main_page" class=" right_block banner_ajax"></div>
                    <?/*$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_content_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );*/?>
                </div>
                
                <div class="right">    
                    <div id="right_3_main_page" class=" right_block banner_ajax"></div>
                    <?
                    /*$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_3_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                    );*/
                    //Подключаем вывод партнеров (пока не нужен)
                    //include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/partners.php");
                    ?>                    
                    <br /><br />
                    <?
                    //Подключаем вывод похожих ресторанов
                    if (is_array($arResult["PROPERTIES"]["similar_rest"]["VALUE"]))
                        include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/include/similar.php");
                    ?>
                    <div id="right_4_main_page" class=" right_block banner_ajax"></div>
                    <?
                    /*$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_4_main_page",
                                "NOINDEX" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                    );*/?>
                    <br /><br />
                    <?/*$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_3_main_page",
                                "NOINDEX" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                    );*/?>
                </div>
                <div class="clear"></div>
                <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
                <div id="bottom_rest_list" class=" right_block banner_ajax"></div>
                <?/*$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );*/?>
                <br /><br />
            </div>
</div>