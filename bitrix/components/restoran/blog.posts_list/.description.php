<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("BB_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("BB_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 110,
	"PATH" => array(
		"ID" => "content_restoran",
		"CHILD" => array(
			"ID" => "restoran_blog",
			"NAME" => GetMessage("BB_NAME")
		)
	),
);
?>