<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("BMNP_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("BMNP_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 240,
	"PATH" => array(
		"ID" => "content_restoran", // for example "my_project"
                "CHILD" => array(
                    "ID" => "restoraunt",
                ),
	),
);
?>