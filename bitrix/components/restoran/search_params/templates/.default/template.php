<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult['ITEMS'])):

//    if($USER->IsAdmin()){
//        FirePHP::getInstance()->info($arResult,'$arResult');
//    }
?>
    <div class="search-by-params-title">Возможно вы имели в виду поиск по параметрам:</div>
    <div class="group-params-wrapper">
        <?
    foreach($arResult["ITEMS_BY_GROUP"] as $group_key=>$group_items):?>
            <?foreach($group_items as $group_item){$group_item_names[] = $group_item['NAME'];$group_item_urls[] = $group_item['DETAIL_PAGE_URL'];}?>
            <?if($group_key=='type'):?>
                <span class="group-param-title"><span>Тип заведения</span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>
            <?elseif($group_key=='kitchen'):?>
                <span class="group-param-title"><span>Кухня</span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>

            <?elseif($group_key=='metro'):?>
                <span class="group-param-title"><span>Метро</span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>
            <?elseif($group_key=='area'):?>
                <span class="group-param-title"><span>Район</span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>
            <?elseif($group_key=='streets'):?>
                <span class="group-param-title"><span>Улица</span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>
            <?elseif($group_key=='system_suburb'):?>
                <span class="group-param-title"><span>Пригород</span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>
            <?else:?>
                <span class="group-param-title"><span><?=$group_items[0]['IBLOCK_NAME']?></span>: </span><span class="group-param-links-wrap"><?foreach($group_items as $group_item):?><a href="<?=$group_item['DETAIL_PAGE_URL']?>"><?=$group_item['NAME']?></a><?=$group_item!=end($group_items)?", ":"";?><?endforeach;?></span>
            <?endif?>
            <?if(end($arResult["ITEMS_BY_GROUP"])!=$group_items):?>
                </div><div class="group-params-wrapper">
            <?endif?>
        <?endforeach;?>
    </div>

<?endif?>