<?
if($arResult['ID']){

    foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photoID) {
        $arResult["PROPERTIES"]["photos"]["DISPLAY_VALUE"][$pKey] = CFile::ResizeImageGet($photoID, array('width' => 728, 'height' => 250), BX_RESIZE_IMAGE_EXACT, true, Array());
    }

    $arResult["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["PROPERTIES"]["photos"]["VALUE"][0], array('width'=>728, 'height'=>250), BX_RESIZE_IMAGE_EXACT, true, Array());
    if(!$arResult["PREVIEW_PICTURE"])
        $arResult["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    else
        unset($arResult["PROPERTIES"]["photos"]["DISPLAY_VALUE"][0]);

    $arResult["PROPERTIES"]['address']['VALUE'] = $arResult["PROPERTIES"]['address']['VALUE'][0];
}
?>