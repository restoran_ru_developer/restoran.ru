<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}


if ($this->StartResultCache(false,array(CITY_ID, $arrFilter, $_REQUEST["q"])))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    $props = array("children", "proposals", "features","entertainment","music","ideal_place_for",'parking');//"wi_fi","hrs_24",
    $mirror_prop_key_arr = array(
        'entertainment'=>'razvlecheniya',
        'features'=>'osobennosti',
        'proposals'=>'predlozheniya',
        'children'=>'detyam',
        'ideal_place_for'=>'idealnoe_mesto_dlya',
    );

    $arFilter = array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ACTIVE'=>'Y','NAME'=>'%'.$_REQUEST['q'].'%');
    $rsElement = CIBlockElement::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), array_merge($arFilter, $arrFilter), false, false, array("ID",'NAME','DETAIL_PAGE_URL','IBLOCK_NAME'));
    while($obElement = $rsElement->GetNextElement())
    {
        $arItem = $obElement->GetFields();

        if(in_array($arItem['IBLOCK_CODE'],$props)){
            $arItem['DETAIL_PAGE_URL'] = '/'.CITY_ID.'/catalog/restaurants/'.($mirror_prop_key_arr[$arItem['IBLOCK_CODE']]?$mirror_prop_key_arr[$arItem['IBLOCK_CODE']]:$arItem['IBLOCK_CODE']).'/'.$arItem['CODE'].'/';
        }

        if($arItem['IBLOCK_CODE']=='type'){
            $arItem['DETAIL_PAGE_URL'] = '/'.CITY_ID.'/catalog/restaurants/type/'.$arItem['CODE'].'/';
        }
        if($arItem['IBLOCK_CODE']=='kitchen'){
            $arItem['DETAIL_PAGE_URL'] = '/'.CITY_ID.'/catalog/restaurants/kitchen/'.$arItem['CODE'].'/';
        }
        if($arItem['IBLOCK_TYPE_ID']=='metro'){
            $arItem['DETAIL_PAGE_URL'] = '/'.CITY_ID.'/catalog/restaurants/metro/'.$arItem['CODE'].'/';
        }
        if($arItem['IBLOCK_TYPE_ID']=='area'){
            $arItem['DETAIL_PAGE_URL'] = '/'.CITY_ID.'/catalog/restaurants/rajon/'.$arItem['CODE'].'/';
        }
        if($arItem['IBLOCK_TYPE_ID']=='system_suburb'){
            $arItem['DETAIL_PAGE_URL'] = '/'.CITY_ID.'/catalog/restaurants/prigorody/'.$arItem['CODE'].'/';
        }

        $arResult["ITEMS_BY_GROUP"][$arItem['IBLOCK_CODE']=='type'||$arItem['IBLOCK_CODE']=='kitchen'||$arItem['IBLOCK_TYPE_ID']=='system'?$arItem['IBLOCK_CODE']:$arItem['IBLOCK_TYPE_ID']][] = $arItem;

        $arResult["ITEMS"][] = $arItem;

        FirePHP::getInstance()->info($arItem['DETAIL_PAGE_URL'],'DETAIL_PAGE_URL');
    }

    $arIB_street = getArIblock("streets", CITY_ID);

    $arFilter = array('IBLOCK_ID'=>$arIB_street['ID'], 'ACTIVE'=>'Y','NAME'=>'%'.$_REQUEST['q'].'%');

    $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
        $arParams["PARENT_SECTION"],
        $arParams["PARENT_SECTION_CODE"],
        array(
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $arIB_street['ID'],
        )
    );

    if($arParams["PARENT_SECTION"] > 0) {
        $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
        if($arParams["INCLUDE_SUBSECTIONS"])
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
    }


    $rsElement = CIBlockElement::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), array_merge($arFilter, $arrFilter), false, array('nTopCount'=>10), array("ID",'NAME','DETAIL_PAGE_URL'));
    while($obElement = $rsElement->GetNextElement())
    {
        $arItem = $obElement->GetFields();

        $arItem['DETAIL_PAGE_URL'] = preg_replace('/\/banket\//','/restaurants/',$arItem['DETAIL_PAGE_URL']);


        $arResult["ITEMS_BY_GROUP"][$arItem['IBLOCK_TYPE_ID']][] = $arItem;

        $arResult["ITEMS"][] = $arItem;
    }

    $this->IncludeComponentTemplate();
}
?>