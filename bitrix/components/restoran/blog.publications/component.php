<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
$arParams["PAGE_COUNT"] = (int)$arParams["PAGE_COUNT"];
if($arParams["PAGE_COUNT"]==0) $arParams["PAGE_COUNT"] =3;

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default

$arResult["SECTIONS"]=array();

//if($arParams["SECTION_ID"]==0) $arParams["SECTION_ID"]=false;

if (!$_REQUEST["PAGEN_1"])
    $_REQUEST["PAGEN_1"] = 1;
    
    

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()),$_REQUEST["PAGEN_1"],$arrFilter,$arParams["RAZDEL"],$_GET)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$arFilter= $arFilter_pod = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CNT_ACTIVE" => "Y",
		"!ID"=>$arParams["NOT_SHOW_SECTIONS"]
	);

	$arSelect = array();
	if(isset($arParams["SECTION_FIELDS"]) && is_array($arParams["SECTION_FIELDS"]))
	{
		foreach($arParams["SECTION_FIELDS"] as $field)
			if(is_string($field) && !empty($field))
				$arSelect[] = $field;
	}

	if(!empty($arSelect))
	{
		$arSelect[] = "ID";
		$arSelect[] = "NAME";
		$arSelect[] = "LEFT_MARGIN";
		$arSelect[] = "RIGHT_MARGIN";
		$arSelect[] = "DEPTH_LEVEL";
		$arSelect[] = "IBLOCK_ID";
		$arSelect[] = "IBLOCK_SECTION_ID";
		$arSelect[] = "LIST_PAGE_URL";
		$arSelect[] = "SECTION_PAGE_URL";
	}
	
	//var_dump($arFilter);

	if(isset($arParams["SECTION_USER_FIELDS"]) && is_array($arParams["SECTION_USER_FIELDS"]))
	{
		foreach($arParams["SECTION_USER_FIELDS"] as $field)
			if(is_string($field) && preg_match("/^UF_/", $field))
				$arSelect[] = $field;
	}
	//var_dump($arParams["SECTION_ID"]);
	if($arParams["RAZDEL"]!=""){
		if($arParams["SECTION_ID"]>0) $OR = $arParams["SECTION_ID"];
		
		$arParams["SECTION_ID"]=$arParams["RAZDEL"];
		$arParams["TOP_DEPTH"]=$arParams["TOP_DEPTH"]-1;
		
	}else $OR = $arParams["SECTION_ID"];
	
	
	
	
	$arResult["SECTION"] = false;
	if(strlen($arParams["SECTION_CODE"])>0)
	{
		$arFilter["CODE"] = $arParams["SECTION_CODE"];
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}
	elseif($arParams["SECTION_ID"]>0)
	{
		
		$arFilter["ID"] = $arParams["SECTION_ID"];
		
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		$arResult["SECTION"] = $rsSections->GetNext();
	}

	if(is_array($arResult["SECTION"]))
	{
		unset($arFilter["ID"]);
		unset($arFilter["CODE"]);
		$arFilter["!ID"]=$arParams["NOT_SHOW_SECTIONS"];
		$arFilter["LEFT_MARGIN"]=$arResult["SECTION"]["LEFT_MARGIN"]+1;
		$arFilter["RIGHT_MARGIN"]=$arResult["SECTION"]["RIGHT_MARGIN"];
		$arFilter["DEPTH_LEVEL"]=$arResult["SECTION"]["DEPTH_LEVEL"] + $arParams["TOP_DEPTH"];
		
		
		
		
		
		$arResult["SECTION"]["PATH"] = array();
		$rsPath = CIBlockSection::GetNavChain($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["ID"]);
		$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$arResult["SECTION"]["PATH"][]=$arPath;
		}
	}
	else
	{
		$arResult["SECTION"] = array("ID"=>0, "DEPTH_LEVEL"=>0);
		$arFilter["DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
	}

	$arFilter["!ID"]=$arParams["DEFAULT_SECTION"];	
	
	if(count($arParams["NOT_SHOW_SECTIONS"])>0) $arFilter["!ID"]=$arParams["NOT_SHOW_SECTIONS"];
	
	$arFilter["MIN_PERMISSION"]="W";
	
	//var_dump($arFilter);
	
	//ORDER BY
	$arSort = array(
		"ID"=>"desc",
	);
	//EXECUTE
	$rsSections = CIBlockSection::GetList($arSort, $arFilter, $arParams["COUNT_ELEMENTS"], $arSelect);
	$rsSections->NavStart($arParams["PAGE_COUNT"]);
	$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
	while($arSection = $rsSections->GetNext())
	{
		
		//Теперь нужно получить тэги к этому разделу
		//
		$arSelect = Array("ID", "NAME", "TAGS");
		$arFiltert = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "SECTION_ID"=>$arSection["ID"], "PROPERTY_BLOCK_TYPE_VALUE"=>"Тэги");
		$res = CIBlockElement::GetList(Array(), $arFiltert, false, false, $arSelect);
		while($ob = $res->GetNext()){
			$arSection["TAGS"]=$ob["TAGS"];
		}
		////////////////////////////////////////////
		
		
		//Нужно запросить инфу по разделу
		//
		$res = CIBlockSection::GetByID($arSection["IBLOCK_SECTION_ID"]);
		if($ar_res = $res->GetNext()){
			$arSection["IBLOCK_SECTION_NAME"]=$ar_res["NAME"];
		}
		///////////////////////////////////////////
	
		if(isset($arSection["PICTURE"]))
			$arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);

		$arButtons = CIBlock::GetPanelButtons(
			$arSection["IBLOCK_ID"],
			0,
			$arSection["ID"],
			array("SESSID"=>false)
		);
		$arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
		$arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];

		$arSection["DATE_CREATE"] = CIBlockFormatProperties::DateFormat("<\e\m>j</\e\m> F Y", MakeTimeStamp($arSection["DATE_CREATE"], CSite::GetDateFormat()));

		$arResult["SECTIONS"][]=$arSection;
	}
	
	//Запросим названия разделов
	//var_dump($arResult["SECTION"]["DEPTH_LEVEL"]);

	if($OR==NULL) $OR=0; 
	$arFilter_pod["ID"] = $OR;
	//else $arFilter_pod["ID"] = $arParams["SECTION_ID"];
	
	//var_dump($OR);
	//var_dump($arFilter_pod);
	$rsSections2 = CIBlockSection::GetList(array(), $arFilter_pod, true, false);

	$arResult["SECTION_POD"] = $rsSections2->GetNext();
	
	
	if($arParams["NOT_SHOW_PARENT"]!="Y"){
	if($arResult["SECTION_POD"]){
		//	var_dump($arResult["SECTION_POD"]);
	
		unset($arFilter_pod["ID"]);
		unset($arFilter_pod["CODE"]);
		$arFilter_pod["LEFT_MARGIN"]=$arResult["SECTION_POD"]["LEFT_MARGIN"]+1;
		$arFilter_pod["RIGHT_MARGIN"]=$arResult["SECTION_POD"]["RIGHT_MARGIN"];
		$arFilter_pod["DEPTH_LEVEL"] =2;
	
		//var_dump($arFilter_pod);
		$rsSections_rzd = CIBlockSection::GetList(array(), $arFilter_pod, false, false);
		while($arSection_rzds = $rsSections_rzd->GetNext()){
			$arResult["RAZDELS"][]=array("NAME"=>$arSection_rzds["NAME"], "ID"=>$arSection_rzds["ID"]);
		}
	}else{
		unset($arFilter_pod["ID"]);
		unset($arFilter_pod["CODE"]);
		$arFilter_pod["DEPTH_LEVEL"] =1;
		$arFilter_pod["ACTIVE"] ="Y";
	
		//var_dump($arFilter_pod);
		$rsSections_rzd = CIBlockSection::GetList(array(), $arFilter_pod, false, false);
		while($arSection_rzds = $rsSections_rzd->GetNext()){
			$arResult["RAZDELS"][]=array("NAME"=>$arSection_rzds["NAME"], "ID"=>$arSection_rzds["ID"]);
		}
	}
	}
	
	
	

	$arResult["SECTIONS_COUNT"] = count($arResult["SECTIONS"]);
	

    $arResult["NAV_STRING"] = $rsSections->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
    $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
    $arResult["NAV_RESULT"] = $rsSections;

	$this->SetResultCacheKeys(array(
		"SECTIONS_COUNT",
		"SECTION",
	));

	$this->IncludeComponentTemplate();
}

if($arResult["SECTIONS_COUNT"] > 0 || isset($arResult["SECTION"]))
{
	if(
		$USER->IsAuthorized()
		&& $APPLICATION->GetShowIncludeAreas()
		&& CModule::IncludeModule("iblock")
	)
	{
		$UrlDeleteSectionButton = "";
		if(isset($arResult["SECTION"]) && $arResult["SECTION"]['IBLOCK_SECTION_ID'] > 0)
		{
			$rsSection = CIBlockSection::GetList(
				array(),
				array("=ID" => $arResult["SECTION"]['IBLOCK_SECTION_ID']),
				false,
				array("SECTION_PAGE_URL")
			);
			$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
			$arSection = $rsSection->GetNext();
			$UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
		}

		if(empty($UrlDeleteSectionButton))
		{
			$url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
			$arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
			$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
			$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
		}

		$arReturnUrl = array(
			"add_section" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"add_element" => (
				strlen($arParams["SECTION_URL"])?
				$arParams["SECTION_URL"]:
				CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
			),
			"delete_section" => $UrlDeleteSectionButton,
		);
		$arButtons = CIBlock::GetPanelButtons(
			$arParams["IBLOCK_ID"],
			0,
			$arResult["SECTION"]["ID"],
			array("RETURN_URL" =>  $arReturnUrl)
		);

		$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
}
?>
