<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$prop_id = 1;

//FirePHP::getInstance()->info($arResult['ITEMS'][0]);
?>
<div class="dropdown">
    <a data-toggle="dropdown" href="#"><?=GetMessage("FEATURES")?><span class="caret"></span></a>
    <div class="dropdown-menu w480 toc features" data-prop="<?=$prop_id?>" data-code="<?=$arParams['PROPS_BLOCK_CODE']?>">
        <div class="title_box">
            <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_FEATURES")?></div>
            <div class="pull-right"></div>
        </div>
        <ul class="float-list">
        <?foreach($arResult["ITEMS"] as $arItem):
            if(LANGUAGE_ID=='en'){
                if(!$arItem['PROPERTIES']['eng_name']['VALUE']){
                    continue;
                }
                $arItem['NAME'] = $arItem['PROPERTIES']['eng_name']['VALUE'];
            }
            ?>
            <li>
                <a href="<?if($arItem['FEATURE_CODE']):?>/<?=CITY_ID?>/catalog/<?=$arParams['PARENT_SECTION_CODE']?>/<?=$arItem['FEATURE_CODE']?>/<?=$arItem['FEATURE_CODE_VALUE']?>/<?else:?><?=$arItem['CODE']?><?endif?>" data-code="<?=$arItem['FEATURE_REAL_CODE']?>"
                   data-val="<?=$arItem['FEATURE_CODE']?$arItem['NAME']:''?>"
                   id="<?=$arItem['FEATURE_VALUE']?>" class="<?=((in_array($arItem['FEATURE_VALUE'],$_REQUEST["arrFilter_pf"][$arItem['FEATURE_REAL_CODE']])||$_REQUEST["arrFilter_pf"][$arItem['FEATURE_REAL_CODE']]==$arItem['FEATURE_VALUE'])&&$arItem['FEATURE_VALUE'])?"selected":""?>">
                    <?=$arItem['NAME']?></a>
            </li>
        <?endforeach;?>
        </ul>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            if(LANGUAGE_ID=='en'){
                if(!$arItem['PROPERTIES']['eng_name']['VALUE']){
                    continue;
                }
                $arItem['NAME'] = $arItem['PROPERTIES']['eng_name']['VALUE'];
            }
            if($arItem['FEATURE_REAL_CODE']):?>
                <?if(in_array($arItem['FEATURE_VALUE'],$_REQUEST["arrFilter_pf"][$arItem['FEATURE_REAL_CODE']])||$_REQUEST["arrFilter_pf"][$arItem['FEATURE_REAL_CODE']]==$arItem['FEATURE_VALUE']):?>
                    <input type="hidden" data-val="<?=$arItem['NAME']?>"  name="arrFilter_pf[<?=$arItem['FEATURE_REAL_CODE']?>][]" id="hid<?=$arItem['FEATURE_VALUE']?>" value="<?=$arItem['FEATURE_VALUE']?>" />
                <?endif?>
            <?endif?>
        <?endforeach;?>
    </div>
</div>