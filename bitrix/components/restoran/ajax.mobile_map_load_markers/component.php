<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

if($this->StartResultCache(false, array()))
{    
    if ($arParams["x"]&&$arParams["y"]):        
        if (CModule::IncludeModule("iblock")):
            $arRestIB = getArIblock("catalog", CITY_ID);
            if ($_REQUEST["arrFilter_pf"])
            {
                foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
                {
                    //if ($key=="type")
                        //$arrFilter["?PROPERTY_".$key] = implode(" || ",$ar);
                    //else
                        $arrFilter["PROPERTY_".$key] = $ar;
                }
            }
            $arFilter = Array("ACTIVE"=>"Y","IBLOCK_ID"=>$arRestIB["ID"],"!PROPERTY_sleeping_rest_VALUE"=>"Да","!PROPERTY_lat"=>false,"!PROPERTY_lon" => false,"><PROPERTY_lat"=>Array((double)$arParams["x"]-0.01,(double)$arParams["x"]+0.01),"><PROPERTY_lon"=>Array((double)$arParams["y"]-0.01,(double)$arParams["y"]+0.01));
            if (is_array($arrFilter))
            {
                $arFilter = array_merge($arFilter,$arrFilter);
            }            
            $res = CIBlockElement::GetList(Array(),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","DETAIL_PAGE_URL","CODE","PREVIEW_PICTURE"));
            //$res->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
            while($ar = $res->GetNext())
            {
                $lat = array();
                $lon = array();
                $adr = array();
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lat"));
                while($ar_props = $db_props->Fetch())
                    $lat[] = $ar_props["VALUE"];
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lon"));
                while($ar_props = $db_props->Fetch())
                    $lon[] = $ar_props["VALUE"];
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
                while($ar_props = $db_props->Fetch())
                    $adr[] = $ar_props["VALUE"];
                
                $kitchen = array();
                $subway = array();
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"kitchen"));
                while($ar_props = $db_props->Fetch())
                {
                    $res2 = CIBlockElement::GetByID($ar_props["VALUE"]);
                    if ($ar2 = $res2->Fetch())
                        $kitchen[] = $ar2["NAME"];
                }
                
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"subway"));
                while($ar_props = $db_props->Fetch())
                {
                    $res2 = CIBlockElement::GetByID($ar_props["VALUE"]);
                    if ($ar1 = $res2->Fetch())
                        $subway[] = $ar1["NAME"];
                }                               
                $subw = implode(", ",$subway);
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"RATIO"));
                while($ar_props = $db_props->Fetch())
                    $ratio = $ar_props["VALUE"];
                $phone = "+7".trim(strip_tags(file_get_contents("http://restoran.ru/bitrix/templates/main/include_areas/top_phones_".$_REQUEST["CITY_ID"].".php")));
                $preview = CFile::GetPath($ar["PREVIEW_PICTURE"]);
                foreach($lat as $key=>$l)
                {
                    //if ($lat[$key]>=(double)$arParams["x"]&&$lat[$key]<=(double)$_REQUEST["xr"]&&$lon[$key]>=(double)$_REQUEST["yl"]&&$lon[$key]<=(double)$arParams["yr"])
                    {
                        $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$lat[$key],"lon"=>$lon[$key],"adres"=>stripslashes($adr[$key]),"url"=> "/mobile/detail.php?RESTOURANT=".$ar["CODE"],"kitchen"=>$kitchen,"subway"=>$subw,"ratio"=>$ratio,"src"=>$preview,"phone"=>$phone);
                    }
                }                
            }
            if (count($arResult)>0)
            {   
                echo json_encode($arResult);
            }
        endif;
    endif;
}
?>