<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("FAVORITE_ADD_NAME"),
	"DESCRIPTION" => GetMessage("FAVORITE_DESCRIPTION_NAME"),
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content_restoran", // for example "my_project"
                "CHILD" => array(
                    "ID" => "restoraunt",
                ),		
	),
	"COMPLEX" => "N",
);

?>