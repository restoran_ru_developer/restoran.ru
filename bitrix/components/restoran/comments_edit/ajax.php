<?

define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>
<?

CModule::IncludeModule("forum");
$text = CFilterUnquotableWords::Filter(trim($_REQUEST["review"]));
$text2 = CFilterUnquotableWords::Filter(trim($_REQUEST["plus"]));
$text3 = CFilterUnquotableWords::Filter(trim($_REQUEST["minus"]));
if (substr_count($text, "%8!*$") > 0 || substr_count($text2, "%8!*$") > 0 || substr_count($text3, "%8!*$") > 0) {
    ?>
    <?

    $ar["ERROR"] = 1;
    if (LANGUAGE_ID == "en")
        $ar["MESSAGE"] = "Comments on the site are prohibited from crude and profanity";
    else
        $ar["MESSAGE"] = "На сайте запрещены комментарии с грубой и ненормативной лексикой";
    echo json_encode($ar);
    die;
}
?>
<?

if (check_bitrix_sessid() && $_REQUEST["review"] && $_REQUEST["IBLOCK_TYPE"] && (int) $_REQUEST["IBLOCK_ID"] && (int) $_REQUEST["review_id"]):
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        die();
    }
    $PROP["plus"] = $_REQUEST["plus"];
    $PROP["minus"] = $_REQUEST["minus"];
    $PROP["ELEMENT"] = $_REQUEST["rest_id"];

    if (is_array($_REQUEST["image"])) {
        foreach ($_REQUEST["image"] as $key => $photo) {
            $PROP["photos"]["n" . $key] = Array("VALUE" => CFile::MakeFileArray($photo));
        }
    }
    
    if (is_array($_REQUEST["video"])) {
        foreach ($_REQUEST["video"] as $key => $video) {
            $PROP["video"]["n" . $key] = Array("VALUE" => CFile::MakeFileArray($video));
        }
    }
    
    if (is_array($_REQUEST["imagetodel"])) {
        $arFileDel["MODULE_ID"] = "iblock";
        $arFileDel["del"] = "Y";
        foreach ($_REQUEST["imagetodel"] as $key => $photo) {
            CIBlockElement::SetPropertyValueCode($_REQUEST["review_id"], "photos", Array ($photo => Array("VALUE"=>$arFileDel) ) );
        }
    }
    
    if (is_array($_REQUEST["videotodel"])) {
        $arFileDel["MODULE_ID"] = "iblock";
        $arFileDel["del"] = "Y";
        foreach ($_REQUEST["videotodel"] as $key => $photo) {
            CIBlockElement::SetPropertyValueCode($_REQUEST["review_id"], "video", Array ($photo => Array("VALUE"=>$arFileDel) ) );
        }
    }
    
    $arLoadProductArray = Array(
        "PROPERTY_VALUES" => $PROP,
        "DETAIL_TEXT" => (int) $_REQUEST["ratio"],
        "PREVIEW_TEXT" => $_REQUEST["review"],
    );
    
    unset($arLoadProductArray["IBLOCK_ID"]);
    $el = new CIBlockElement;
    if ($el->Update($_REQUEST["review_id"], $arLoadProductArray, false, true, true))
        echo json_encode(array('MESSAGE' => 'Изменения сохранены', 'STATUS' => 1));
    else
        echo json_encode(array('MESSAGE' => 'Ошибка сохранения', 'STATUS' => 0, 'ERROR' => 1));

endif;
?>
<? require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>