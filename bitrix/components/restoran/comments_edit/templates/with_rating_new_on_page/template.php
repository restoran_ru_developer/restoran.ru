<?
$APPLICATION->AddHeadString('<link href="' . $templateFolder . '/fineuploader.css" rel="stylesheet" type="text/css"/>');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
//v_dump($arResult);
?>
<script type="text/javascript" src="<?= $templateFolder ?>/script.js"></script>
<h2><?= GetMessage("YOUR_REVIEW") ?></h2>
<? if (!$USER->IsAuthorized()): ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "no_auth", Array(
        "REGISTER_URL" => "",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "",
        "SHOW_ERRORS" => "N"
            ), false
    );
    ?> 
    <br /><br />
<? endif; ?>
<form name="attach" action="/bitrix/components/restoran/comments_edit/ajax.php" method="post" id="comment_form" enctype="multipart/form-data">  
    <?= bitrix_sessid_post() ?>
    <div class="active_rating">
        <? for ($i = 1; $i <= 5; $i++): ?>
            <? if ($i <= (int) $arResult["CURRENT_REVIEW"]["RATIO"]): ?>
                <div class="star_a" alt="<?= $i ?>"></div>
            <? else: ?>
                <div class="star" alt="<?= $i ?>"></div>
            <? endif; ?>
        <? endfor; ?>
    </div>
    <div class="grey left" style="margin-left:10px;"><i>&ndash; <?= GetMessage("CLICK_TO_RATE") ?></i></div>
    <div class="clear"></div> 
    <br />
    <div class="grey_block" style="padding:10px;">
        <div class="uppercase">Ресторан: <b><?=$arResult["CURRENT_REVIEW"]["RESTORAN"]?></b></div>
    </div>
    <div class="grey_block" id="write_com">        
        <div class="uppercase left"><?= GetMessage("REVIEW_TEXT") ?></div>            
        <div class="clear"></div>
        <textarea class="add_review" id="review" name="review" req="req" style="width:695px;height:180px;font-size:12px;"><?= $arResult["CURRENT_REVIEW"]["PREVIEW_TEXT"] ?></textarea>
        <input type="hidden" value="<?= $arResult["CURRENT_REVIEW"]["RATIO"] ?>" id="input_ratio" name="ratio" />
        <input type="hidden" value="<?= $arResult["CURRENT_REVIEW"]["ID"] ?>" id="review_id" name="review_id" />
        <input type="hidden" id="rest_id" value="<?= $arResult["CURRENT_REVIEW"]["ELEMENT"]["VALUE"] ?>" name="rest_id"/>
        <div class="left"><a href="javascript:void(0)" class="js another" onclick="$('#plus_minus').toggle(300)"><?= GetMessage("ADD_PROS_CONS") ?></a></div>
        <div class="right">
            <div class="left attach">Прикрепить:</div>
            <div id="attach_photo" class="left photo_icon"></div>
            <div id="attach_video" class="left video_icon"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="grey_block" id="plus_minus" <?
        if ($arResult["CURRENT_REVIEW"]["PLUS"]["VALUE"] || $arResult["CURRENT_REVIEW"]["MINUS"]["VALUE"]) {
            
        } else {
            echo 'style="display:none"';
        }
        ?>>
        <div class="left">
            <div class="uppercase"><?= GetMessage("PROS") ?></div>
            <textarea class="add_review plus" name="plus" req="req" style="width:315px"><?= $arResult["CURRENT_REVIEW"]["PLUS"]["VALUE"] ?></textarea>
        </div>
        <div class="right">
            <div class="uppercase"><?= GetMessage("CONS") ?></div>
            <textarea class="add_review" name="minus" req="req" style="width:315px"><?= $arResult["CURRENT_REVIEW"]["MINUS"]["VALUE"] ?></textarea>
        </div>
        <div class="clear"></div>
    </div>        
    <div class="img-container" id="img-container" style="min-height:20px;">
        <?
        foreach ($arResult["CURRENT_REVIEW"]["PHOTOS"] as $key => $value):
            if ($value['original_path']):
                ?>
                <div class="image-block">
                    <a class="remove-link-prev-image" href="#" property_value_id="<?= $value['PROPERTY_VALUE_ID'] ?>"></a>
                    <img height="70" src="<?= $value['original_path'] ?>">
                </div>
                <?
            endif;
        endforeach;
        foreach ($arResult["CURRENT_REVIEW"]["VIDEOS"] as $key => $value):
            if ($value['PROPERTY_VALUE_ID']):
                ?>
                <div class="image-block">
                    <a class="remove-link-prev-video" href="#" property_value_id="<?= $value['PROPERTY_VALUE_ID'] ?>"></a>
                    <img height="70" src="/bitrix/components/restoran/comments_edit/templates/with_rating_new_on_page/images/video_file.png">
                    <div class="name"><?=$value["VIDEO_NAME"]?></div>
                </div>
                <?
            endif;
        endforeach;
        ?>
        <div class="clear"></div>
    </div>    
    <div class="grey_block">
        <div class="right" style="margin-top:5px;">
            <input type="submit" id="add_commm" class="light_button" value="<?= GetMessage("SAVE") ?>">       
        </div>
        <div class="clear"></div>
    </div>
    <div>                    
        <Br />
        <input type="hidden" name="IBLOCK_TYPE" value="<?= $arParams["IBLOCK_TYPE"] ?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
        <input type="hidden" name="IS_SECTION" value="<?= $arParams["IS_SECTION"] ?>" />
    </div>
    <div id="rating_overlay">
        <div class="close"></div>                           
    </div>
    <div class="clear"></div>
    <div class="grey left">
        <?= GetMessage("R_TERMS") ?> 
    </div>
    <div class="clear"></div>
</form>
<script>
    $(document).ready(function(){                   
        var files = new Array();
        var errorHandler = function(event, id, fileName, reason) {
            qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
        $('#attach_photo').fineUploader({
            text: {
                uploadButton: "",
                cancelButton: "",
                waitingForResponse: ""
            },
            multiple: false,
            disableCancelForFormUploads: true,
            request: {
                endpoint: "<?= $templateFolder ?>/upload.php",
                params: {"generateError": true,"f":"images"}
            },
            validation:{
                allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
                typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: jpg, gif, png"
            }
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push(response.resized);            
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
                $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));                        
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        }); 
        $('#attach_video').fineUploader({
            text: {
                uploadButton: "",
                cancelButton: "",
                waitingForResponse: ""
            },
            multiple: true,
            disableCancelForFormUploads : true,
            request: {
                endpoint: "<?= $templateFolder ?>/upload.php",
                params: {"generateError": true, "f":"video"}
            },
            validation:{
                allowedExtensions : ["m4v","avi","mov","flv","3gp"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
                typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: m4v, avi, mov, flv, 3gp"
            }                
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push("video");                        
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="<?= $templateFolder ?>/images/video_file.png" height="70">').appendTo($('#img'+files.length));
                $('<div class="name">'+response.uploadName+'</div>').appendTo($('#img'+files.length));
                $('<input value="'+response.path+'" type="hidden" name="video[]" />').appendTo($('#img'+files.length));                            
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        });
        $("#wr_cm").click(function(){
            $(this).hide();
            $("#write_comment").show("500");                
        });                 
        $(".img-container").on("click",".remove-link",function(){
            var file = $(this).next().attr("src");
            $(this).parents(".image-block").remove();                
            return false;
        });
        $(".img-container").on("click",".remove-link-prev-image",function(){
            $('<input type="hidden" value="'+$(this).attr('property_value_id')+'" name="imagetodel[]">').appendTo($('#comment_form'));
            $(this).parents(".image-block").remove();                
            return false;
        });
        $(".img-container").on("click",".remove-link-prev-video",function(){
            $('<input type="hidden" value="'+$(this).attr('property_value_id')+'" name="videotodel[]">').appendTo($('#comment_form'));
            $(this).parents(".image-block").remove();                
            return false;
        });
        
        $("#comment_form").keypress(function(e){
            e = e || window.event;    
            //for chrome & safari
            if (e.ctrlKey) {
                if(e.keyCode == 10){
                    $("#comment_form").submit();
                    return false;
                }
            };
            //for firefox
            if (e.keyCode == 13 && e.ctrlKey) {
                $("#comment_form").submit();
                return false;
            };
        });
        $("#comment_form").submit(function(){
            if (!$(this).find("#review").val())
            {
                alert("Введите комментарий!");
                return false;
            }
            if (!$(this).find("#input_ratio").val())
            {
                alert("Выберите рейтинг!");
                return false;
            }
            if (!$(this).find("#restoran222").val())
            {
                //alert("Выберите ресторан!");
                //return false;
            }
                
            var params = $(this).serialize();
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: params,
                success: function(data) {
                    if(data)
                    {
                        data = eval('('+data+')');                        
                        if (!$("#comment_modal").size())
                        {
                            $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");
                        }
                        var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
                        var html2 = '</div>';                    
                        $('#comment_modal').html(html+data.MESSAGE+html2);
                        showOverflow();
                        setCenter($("#comment_modal"));
                        $("#comment_modal").fadeIn("300");
                        if (data.ERROR=="1")
                        {
                            $("#add_commm").attr("disabled",false);
                        }
                        if (data.STATUS=="1")
                            setTimeout("location.reload()","3500");
                    }
                }
            });
            return false; 
        });
    });
    
</script>