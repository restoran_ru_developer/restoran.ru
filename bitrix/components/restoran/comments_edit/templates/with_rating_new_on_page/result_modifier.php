<?

$arSelect = Array("ID", "PREVIEW_TEXT", "DETAIL_TEXT", "NAME", "PROPERTY_ELEMENT", "PROPERTY_minus", "PROPERTY_plus", "PROPERTY_photos", "PROPERTY_video", "PROPERTY_video_youtube", "PROPERTY_COMMENTS");
$arFilter = Array("ID" => IntVal($arParams["ELEMENT_ID"]));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while ($ob = $res->GetNextElement()) {
    $reviewInfo = $ob->GetFields();
}

$arReviewsIB = getArIblock("reviews", $_REQUEST["CITY_ID"]);

$res = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arParams["ELEMENT_ID"], "sort", "asc", array("CODE" => "photos"));
while ($ob = $res->GetNext()) {
    $arResult["CURRENT_REVIEW"]["PHOTOS"][] = $ob;
}
foreach ($arResult["CURRENT_REVIEW"]["PHOTOS"] as $key => $value) {
    $arResult["CURRENT_REVIEW"]["PHOTOS"][$key]["FILE"] = CFile::ResizeImageGet($value['VALUE'], array('width' => 370, 'height' => 240), BX_RESIZE_IMAGE_EXACT, true);
}
$res = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arParams["ELEMENT_ID"], "sort", "asc", array("CODE" => "video"));
while ($ob = $res->GetNext()) {
    $arResult["CURRENT_REVIEW"]["VIDEOS"][] = $ob;
}
$res = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arParams["ELEMENT_ID"], "sort", "asc", array("CODE" => "plus"));
while ($ob = $res->GetNext()) {
    $arResult["CURRENT_REVIEW"]["PLUS"] = $ob;
}
$res = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arParams["ELEMENT_ID"], "sort", "asc", array("CODE" => "minus"));
while ($ob = $res->GetNext()) {
    $arResult["CURRENT_REVIEW"]["MINUS"] = $ob;
}
$res = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arParams["ELEMENT_ID"], "sort", "asc", array("CODE" => "ELEMENT"));
if ($ob = $res->GetNext()) {
    $arResult["CURRENT_REVIEW"]["ELEMENT"] = $ob;
    $r = CIBlockElement::GetByID($ob["VALUE"]);
    if ($a = $r->Fetch())
        $arResult["CURRENT_REVIEW"]["RESTORAN"] = $a["NAME"];
}
foreach ($arResult["CURRENT_REVIEW"]["PHOTOS"] as $key => $value) {
    $arResult["CURRENT_REVIEW"]["PHOTOS"][$key]['original_path'] = CFile::GetPath($value["VALUE"]);
}
foreach ($arResult["CURRENT_REVIEW"]["VIDEOS"] as $key => $value) {
    $res = CFile::GetByID($value["VALUE"]);
    if ($ar_res = $res->GetNext())
        $arResult["CURRENT_REVIEW"]["VIDEOS"][$key]['VIDEO_NAME'] = $ar_res["FILE_NAME"];
}

$arResult["CURRENT_REVIEW"]["ID"] = $arParams["ELEMENT_ID"];
$arResult["CURRENT_REVIEW"]["PREVIEW_TEXT"] = $reviewInfo["~PREVIEW_TEXT"];
$arResult["CURRENT_REVIEW"]["RATIO"] = $reviewInfo["DETAIL_TEXT"];

?>