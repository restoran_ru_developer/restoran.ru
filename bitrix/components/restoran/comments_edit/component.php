<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arParams["IBLOCK_TYPE"] = ($arParams["IBLOCK_TYPE"])?trim($arParams["IBLOCK_TYPE"]):$_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"];
$arParams["IS_SECTION"] = ($arParams["IS_SECTION"])?trim($arParams["IS_SECTION"]):$_SESSION["ADD_REVIEW"]["IS_SECTION"];
$arParams["ELEMENT_ID"] = ($arParams["ELEMENT_ID"])?intval($arParams["ELEMENT_ID"]):$_SESSION["ADD_REVIEW"]["ELEMENT_ID"];
$arParams["IBLOCK_ID"] = ($arParams["IBLOCK_ID"])?intval($arParams["IBLOCK_ID"]):$_SESSION["ADD_REVIEW"]["IBLOCK_ID"];
if (!$_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"])
{
    $_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"] = $arParams["IBLOCK_TYPE"];
    $_SESSION["ADD_REVIEW"]["ELEMENT_ID"] = $arParams["ELEMENT_ID"];
    $_SESSION["ADD_REVIEW"]["IS_SECTION"] = $arParams["IS_SECTION"];
    $_SESSION["ADD_REVIEW"]["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
}
$error = array();
$arr = array();
if(!CModule::IncludeModule("iblock"))
{
    $this->AbortResultCache();
    $error[] = GetMessage("IBLOCK_MODULE_NOT_INSTALLED");
}
if ($arParams["MY_CAPTCHA"]=="Y"&&$_REQUEST["ch"])
{
    $this->AbortResultCache();
    echo "Are you a bot?";
    die;
}
if (!$USER->IsAuthorized())
{    
    if ($_REQUEST["email"])
    {
        if(isset($_SESSION['qaptcha_key']) && !empty($_SESSION['qaptcha_key']))
        {
            $QaptChaInput = $_SESSION['qaptcha_key']; 
            /** we can control the random input grace to the QapTchaToTest intpu value **/
            if(isset($_POST[''.$QaptChaInput.'']) && empty($_POST[''.$QaptChaInput.'']))
            {

            }
            else
            {
                unset($_SESSION['qaptcha_key']);
                die;
            }
        }
        else
        {
            unset($_SESSION['qaptcha_key']);
            die;        
        }
    }
    $email = trim($_REQUEST["email"]);
    $nic = explode("@",$email);
    $nic = $nic[0];
    if (check_email($email))
    {
        $user = new CUser;
        $arFields = Array(
            "EMAIL"             => trim($_REQUEST["email"]),
            "LOGIN"             => trim($_REQUEST["email"]),
            "PERSONAL_PROFESSION" => $nic,
            "PERSONAL_MAILBOX" => 1,
            "ACTIVE"            => "Y",
            "GROUP_ID"          => array(5),
            "PASSWORD"          => randString(7),
        );        
        $ID = $user->Add($arFields);
        if (!$ID)      
        {
            $temp = explode("<br>",$user->LAST_ERROR);
            $error[] = $temp[0];
        }
        else
        {
            unset($arFields["PASSWORD"]);
            $arFields["USER_ID"] = $ID;
            $event = new CEvent;
            $event->SendImmediate("NEW_USER", SITE_ID, $arFields,"N", 1);
            $arResult["NEW_USER"] = 1;
            $USER->Authorize($ID);
        }
    }
    else
        $error[] = GetMessage("WRONG_EMAIL");
}
if ($arParams["ELEMENT_ID"]<=0)
    $error[] = GetMessage("ERROR_ID");
if (count($error)>0)
{
    $arResult["STATUS"] = 0;
    $arResult["ERROR"] = implode("<br />",$error);
}
if ($arParams["ACTION"]=="save")
{    
    if (!$arParams["REVIEW"])
        $error[] = GetMessage("REVIEW_ERROR");        
    if (count($error)>0)
    {
        $arResult["ERROR"] = implode("<br />",$error);
    }
    else
    {        

        $el = new CIBlockElement;
        $PROP = array();
        $PROP["ELEMENT"] = $arParams["ELEMENT_ID"];  
        if ($PROP["ELEMENT"])
        {
            $PROP["CITY_ID"] = CITY_ID;
        }
        if ($arParams["PARENT"])
        {
            $PROP["PARENT"] = (int)$arParams["PARENT"];  
            $rrr = CIBlockElement::GetByID((int)$arParams["PARENT"]);
            if ($par_el = $rrr->Fetch())
            {
                $e = CUser::GetByID($par_el["CREATED_BY"]);
                if ($u = $e->Fetch())
                {
                    $arEventFields = array(
                        "EMAIL" => $u["EMAIL"],
                        "URL" => $_SERVER["HTTP_REFERER"],
                        "NAME" =>  ($u["PERSONAL_PROFESSION"])?$u["PERSONAL_PROFESSION"]:$USER->GetLogin(), 
                        "COMMENT" => $par_el["PREVIEW_TEXT"],
                        "COMMENT2" => $arParams["REVIEW"],
                    );
                    CEvent::Send("NEW_COMMENT", "s1", $arEventFields,false,99);
                }
            }
        }
        if ($arParams["plus"])
            $PROP["plus"] = $arParams["plus"];  
            //$PROP["plus"] = Array("VALUE"=>array("TYPE"=>"text","TEXT"=>$arParams["plus"]));  
        if ($arParams["minus"])
            $PROP["minus"] = $arParams["minus"];  
            //$PROP["minus"] = Array("VALUE"=>array("TYPE"=>"text","TEXT"=>$arParams["minus"]));  

        if (is_array($arParams["photos"]))
        {         
            foreach ($arParams["photos"] as $key=>$photo)
            {
                $PROP["photos"]["n".$key] = Array("VALUE"=>CFile::MakeFileArray($photo));
            }
        }
        if (is_array($arParams["video"]))
        {
            foreach ($arParams["video"] as $key=>$video)
            {
                $PROP["video"]["n".$key] = Array("VALUE"=>CFile::MakeFileArray($video));
            }
        }
        $e = CUser::GetByID($USER->GetID());
        if ($u = $e->Fetch())
        {           
            $arLoadProductArray = Array(  "MODIFIED_BY"    => $USER->GetID(), 
                                      "IBLOCK_ID"      => $arParams["IBLOCK_ID"],  
                                      "PROPERTY_VALUES"=> $PROP,  
                                      "ACTIVE_FROM"=> date("d.m.Y H:i:s"),  
                                      "NAME"  => ($u["PERSONAL_PROFESSION"])?$u["PERSONAL_PROFESSION"]:$USER->GetLogin(),  
                                      "ACTIVE" => "Y",
                                      "PREVIEW_TEXT"   => $arParams["REVIEW"]);
        }
        if ($arParams["RATIO"])
            $arLoadProductArray["DETAIL_TEXT"] = $arParams["RATIO"];
        if($PRODUCT_ID = $el->Add($arLoadProductArray))  
        {
            //echo "Спасибо, Ваш голос учтен";
            unset($_SESSION["ADD_REVIEW"]);
        }
        else  
        {
            $error = $el->LAST_ERROR;
            $arResult["ERROR"] = implode("<br />",$error);
            unset($_SESSION["ADD_REVIEW"]);
        }
    }   
}
$this->IncludeComponentTemplate();    
?>