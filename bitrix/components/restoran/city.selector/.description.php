<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_IBLOCK_CITY_SELECTOR"),
	"DESCRIPTION" => GetMessage("T_IBLOCK_CITY_SELECTOR_DESC"),
	"ICON" => "/images/cat_all.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content_restoran",
		"CHILD" => array(
			"ID" => "restoraunt_navigation",
			"NAME" => GetMessage("MAIN_NAVIGATION_SERVICE"),
                        "CHILD" => array(
				"ID" => "city_selector",
                                "NAME" => GetMessage("CITY_NAVIGATIONE"),
			),
		)
	),
);
?>