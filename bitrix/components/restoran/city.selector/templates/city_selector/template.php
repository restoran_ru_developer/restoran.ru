<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<select id="city_selector">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if (substr_count($APPLICATION->GetCurPageParam(),CITY_ID)):?>
            <?$arItem["CODE"] = str_replace(CITY_ID,$arItem["CODE"],$APPLICATION->GetCurPageParam())?>
        <?endif;?>
        <option value="<?=$arItem["CODE"]?>"<?if($arItem["SELECTED"]):?> selected="selected"<?endif?>><?=$arItem["NAME"]?></option>
    <?endforeach?>
</select>
