<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if($arResult['ID']):
?>
    <?if ($arResult["ID"]=="387409"||$arResult["ID"]=="1099834"):?>
        <noindex><a class="like-h2" href="http://www.federicoclub.ru/" target='_blank'><?=$arResult["NAME"]?></a></noindex>
    <?elseif($arResult["ID"]=="1820154"):?>
        <noindex><a class="like-h2" href="http://rdisco.ru/" target='_blank'><?=$arResult["NAME"]?></a></noindex>
    <?elseif($arResult["ID"]=="388191"):?>
        <noindex><a class="like-h2" href="http://www.empress-hall.ru" target='_blank'><?=$arResult["NAME"]?></a></noindex>
    <?elseif($arResult["ID"]=="386634"):?>
        <noindex><a class="like-h2" href="http://www.restorantan.ru" target='_blank'><?=$arResult["NAME"]?></a></noindex>
    <?else:?>
        <div><a class="like-h2" href="<?=$arResult["DETAIL_PAGE_URL"]?>"><?=$arResult["NAME"]?></a></div>
    <?endif;?>
    <div class="name_ratio">
        <?for($k = 1; $k <= 5; $k++):?>
            <span class="icon-star<?if($k > $arResult["PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
        <?endfor?>
    </div>
    <div class="order-buttons">
        <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
        <?elseif(CITY_ID=="rga"):?>
            <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <a href="/tpl/ajax/online_order_rest_rgaurm.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
        <?endif;?>
        <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arResult["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
    </div>

    <div id="rest-<?=$arResult["ID"]?>" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">

            <div class="item active">
                <?if ($arResult["ID"]=="387409"||$arResult["ID"]=="1099834"):?>
                    <noindex><a class="like-h2" href="http://www.federicoclub.ru/" target='_blank'><img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?elseif($arResult["ID"]=="1820154"):?>
                    <noindex><a class="like-h2" href="http://rdisco.ru/" target='_blank'><img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?elseif($arResult["ID"]=="388191"):?>
                    <noindex><a class="like-h2" href="http://www.empress-hall.ru" target='_blank'><img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?elseif($arResult["ID"]=="386634"):?>
                    <noindex><a class="like-h2" href="http://www.restorantan.ru" target='_blank'><img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?else:?>
                    <div><a class="like-h2" href="<?=$arResult["DETAIL_PAGE_URL"]?>"><img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>" /></a></div>
                <?endif;?>
            </div>
            <?if(count($arResult["PROPERTIES"]["photos"]["DISPLAY_VALUE"])>0):?>
                <?foreach($arResult["PROPERTIES"]["photos"]["DISPLAY_VALUE"] as $pKey=>$photo):?>
                    <div class="item">
                        <?if ($arResult["ID"]=="387409"||$arResult["ID"]=="1099834"):?>
                            <noindex><a class="like-h2" href="http://www.federicoclub.ru/" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?elseif($arResult["ID"]=="388191"):?>
                            <noindex><a class="like-h2" href="http://www.empress-hall.ru" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?elseif($arResult["ID"]=="386634"):?>
                            <noindex><a class="like-h2" href="http://www.restorantan.ru" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?elseif($arResult["ID"]=="1820154"):?>
                            <noindex><a class="like-h2" href="http://rdisco.ru/" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?else:?>
                            <div><a class="like-h2" href="<?=$arResult["DETAIL_PAGE_URL"]?>"><img src="<?=$photo["src"]?>" align="bottom" /></a></div>
                        <?endif;?>
                    </div>
                <?endforeach?>
            <?endif;?>
        </div>
        <a class="left carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="prev">
            <span class="galery-control galery-control-left icon-arrow-left2"></span>
        </a>
        <a class="right carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="next">
            <span class="galery-control galery-control-right icon-arrow-right2"></span>
        </a>
    </div>
    <div class="props">
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_opening_hours")?></div>
            <div class="value">
                <?if(!is_array($arResult["PROPERTIES"]["opening_hours"]["VALUE"])):?>
                    <?=$arResult["PROPERTIES"]["opening_hours"]["VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arResult["PROPERTIES"]["opening_hours"]["VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_average_bill")?></div>
            <div class="value">
                <?if(!is_array($arResult["PROPERTIES"]["average_bill"]["VALUE"])):?>
                    <?=$arResult["PROPERTIES"]["average_bill"]["VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arResult["PROPERTIES"]["average_bill"]["VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_kitchen")?></div>
            <div class="value">
                <?if(!is_array($arResult["PROPERTIES"]["kitchen"]["VALUE"])):?>
                    <?=$arResult["PROPERTIES"]["kitchen"]["VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arResult["PROPERTIES"]["kitchen"]["VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col wsubway">
            <div class="name"><?=GetMessage("R_PROPERTY_address")?></div>
            <div class="value">
                <?$arIB = getArIblock("catalog",CITY_ID);?>
                <?=str_replace($arIB["NAME"].", ","",$arResult["PROPERTIES"]["address"]["VALUE"])?><br />
                <?if (is_array($arResult["PROPERTIES"]["subway"]["VALUE"])):?>
                    <div class="subway">M</div>
                    <?=$arResult["PROPERTIES"]["subway"]["VALUE"][0]?>
                <?elseif(!empty($arResult["PROPERTIES"]["subway"]["VALUE"])):?>
                    <div class="subway">M</div>
                    <?=$arResult["PROPERTIES"]["subway"]["VALUE"]?>
                <?endif;?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_phone")?></div>
            <div class="value">
                <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                    <?if (CITY_ID=="spb"):?>
                        <a href="tel:7401820" class="<? echo MOBILE;?>">+7(812) 740-18-20</a>
                    <?else:?>
                        <a href="tel:9882656" class="<? echo MOBILE;?>">+7(495) 988-26-56</a>
                    <?endif;?>
                <?else:


                    $arResult["PROPERTIES"]["phone"]["VALUE"] = str_replace(";", "", $arResult["PROPERTIES"]["phone"]["VALUE"]);

                    if(is_array($arResult["PROPERTIES"]["phone"]["VALUE"]))
                        $arResult["PROPERTIES"]["phone"]["VALUE2"]=implode(", ",$arResult["PROPERTIES"]["phone"]["VALUE"]);
                    else{
                        $arResult["PROPERTIES"]["phone"]["VALUE2"]=$arResult["PROPERTIES"]["phone"]["VALUE"];
                    }

                    $arResult["PROPERTIES"]["phone"]["VALUE3"]=  explode(",", $arResult["PROPERTIES"]["phone"]["VALUE2"]);



                    $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                    preg_match_all($reg, $arResult["PROPERTIES"]["phone"]["VALUE2"], $matches);



                    $TELs=array();
                    for($p=1;$p<5;$p++){
                        foreach($matches[$p] as $key=>$v){
                            $TELs[$key].=$v;
                        }
                    }

                    foreach($TELs as $key => $T){
                        if(strlen($T)>7)  $TELs[$key]="+7".$T;
                    }

                    foreach($TELs as $key => $T){
                        ?>
                        <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=clearUserPhone($arResult["PROPERTIES"]["phone"]["VALUE3"][$key])?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                    <?}
                endif;?>
            </div>
        </div>
    </div>
<!--    <script>-->
<!--        $(function(){-->
<!--            $('.navigation').css('margin-top','40px');-->
<!--        })-->
<!--    </script>-->
<?endif?>