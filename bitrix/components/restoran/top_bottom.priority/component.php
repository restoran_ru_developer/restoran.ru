<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}
if(!$_REQUEST["PROPERTY"]){
    return;
}

if ($this->StartResultCache(false,array($_REQUEST["PROPERTY"].$_REQUEST["PROPERTY_VALUE"], $arrFilter)))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y','CODE'=>$_REQUEST["PROPERTY"],'ELEMENT_SUBSECTIONS'=>'N');
    $db_list = CIBlockSection::GetList(Array(), $arFilter, true);
    if($ar_result = $db_list->Fetch())
    {
        $First20ElementCnt = $ar_result['ELEMENT_CNT'];
        $First20SectionId = $ar_result['ID'];
    }
    if($_REQUEST["PROPERTY_VALUE"]!='y'){
        $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y','CODE'=>$_REQUEST["PROPERTY_VALUE"],'SECTION_ID'=>$ar_result['ID'],'ELEMENT_SUBSECTIONS'=>'N');
        $db_list = CIBlockSection::GetList(Array(), $arFilter, true);
        if($ar_result = $db_list->Fetch())
        {
            $First20ElementCnt = $ar_result['ELEMENT_CNT'];
            $First20SectionId = $ar_result['ID'];
        }
    }

    if ($First20ElementCnt > 0 && $First20SectionId) {
        $arResult['First20SectionId'] = $First20SectionId;
    }

    if($First20SectionId){
        $arFilter = array('IBLOCK_ID'=>$arParams['IBLOCK_ID'],'SECTION_ID'=>$First20SectionId,'ACTIVE'=>'Y');
        $rsElement = CIBlockElement::GetList(array('SORT'=>'ASC'), array_merge($arFilter, $arrFilter), false, false, array('PROPERTY_RESTAURANT'));
        if($obElement = $rsElement->Fetch())
        {
            $res = CIBlockElement::GetById($obElement['PROPERTY_RESTAURANT_VALUE']);
            if($obj = $res->GetNext()){
                $arResult = $obj;
                foreach($arParams['PROPERTIES'] as $param){
                    $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), Array("CODE"=>$param));
                    while($ar_props = $db_props->Fetch()){
                        if($ar_props['PROPERTY_TYPE']=='E'){
                            $inner_res = CIBlockElement::GetById($ar_props['VALUE']);
                            if($inner_obj = $inner_res->Fetch()){
                                $arResult['PROPERTIES'][$param]['VALUE'][] = $inner_obj['NAME'];
                            }
                        }
                        else {
                            $arResult['PROPERTIES'][$param]['VALUE'][] = $ar_props['VALUE'];
                        }

                    }

                }

            }
        }
    }

    $this->SetResultCacheKeys(array(
        "ID"
    ));
    $this->IncludeComponentTemplate();
}
?>