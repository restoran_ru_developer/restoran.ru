<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["PAGE"] = ($_REQUEST["PAGEN_1"])?$_REQUEST["PAGEN_1"]:1;
$arParams["NAME"] = trim($_REQUEST["user_name"]);
$arParams["EMAIL"] = trim($_REQUEST["email"]);
$arParams["GENDER"] = trim($_REQUEST["gender"]);
$arParams["PAGE_COUNT"] = ($arParams["PAGE_COUNT"])?$arParams["PAGE_COUNT"]:"20";
if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 24*30*60;
}
global $USER;
if (!$USER->IsAuthorized())
{
    ShowError("NO_AUTHORIZED");
    die;
}
if ($arParams["NAME"])
    $arFilter = Array(
       //"KEYWORDS"=>"%".$arParams["NAME"]."% || ".$arParams["NAME"]."% || %".$arParams["NAME"],
       "KEYWORDS"=>$arParams["NAME"],
    );
if ($arParams["EMAIL"])
    $arFilter = Array(
       "LOGIN"=>$arParams["EMAIL"],
    );

//v_dump($arFilter);
if ($arParams["GENDER"])
    $arFilter['PERSONAL_GENDER'] = $arParams["GENDER"];
CModule::IncludeModule("socialnetwork");
$res = CSocNetUserRelations::GetRelatedUsers($USER->GetID(),"F");
while ($ar = $res->Fetch())
{
    $friends[] = $ar;
}
$friends = count($friends);

$res = CSocNetUserRelations::GetRelatedUsers($USER->GetID(),"Z");
while ($ar = $res->Fetch())
{
    $friends[] = $ar;
}
$friends2 = count($friends);

if($this->StartResultCache(false, array($arParams,$USER->GetGroups(),$friends,$friends2)))
{
    
    $rsUsers = CUser::GetList(($by="name"), ($order="asc"), $arFilter, array("NAV_PARAMS"=>array("nPageSize"=>$arParams["PAGE_COUNT"],"iNumPage"=>$arParams["PAGE"]),"SELECT"=>Array("UF_VK_USER_ID","UF_FB_USER_ID"))); // выбираем пользователей
    while($arUser = $rsUsers->GetNext()) :
       
        $arResult["ITEMS"][] = $arUser;
    endwhile;
    $arResult["NAV_STRING"] = $rsUsers->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
    $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
    $arResult["NAV_RESULT"] = $rsUsers;
    $this->SetResultCacheKeys(array(
            "ITEMS",
    ));
    $this->IncludeComponentTemplate();
}
?>