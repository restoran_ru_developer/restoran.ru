<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(strlen($arResult["FatalError"])>0):?>
    <?=$arResult["FatalError"]?>
<?else:?>
    <?if(strlen($arResult["ErrorMessage"])>0):?>
        <?=$arResult["ErrorMessage"]?>
    <?endif?>

    <!--<h2><?=GetMessage("FRIENDS_TITLE")?></h2>-->
    <table class="friendlist" cellpadding="10">
        <?if (count($arResult["ITEMS"][0])>0):?>
            <tr>
                <th width="40"></th>
                <th width="20">Акт</th>
                <th width="240">Имя</th>
                <th>Email</th>
                <th>Описание</th>
                <th>FB</th>
                <th>VK</th>
                <th>Рестики</th>
                <th width="12"></th>
            </tr>
            <?foreach ($arResult["ITEMS"] as $cell=>$user):?>
            <tr>
                <td>
                        <div class="avatar_small">
                            <img src="<?=$user["PERSONAL_PHOTO"]["src"]?>" width="46" />
                        </div>    
                        <?if ($USER->GetID()!=$user["ID"]):?>
                        <?else:?>
                        <a><?=GetMessage("ITS_YOU")?></a>
                        <?endif;?>
                </td>
                <td style="text-align: center">
                    <?=($user["ACTIVE"]=="Y")?"Да":"Нет"?>
                </td>
                <td>
                        <p>
                            <?
                            $name = array();
                            $name[] = $user["NAME"];
                            $name[] = $user["PERSONAL_PROFESSION"];
                            $name[] = $user["LAST_NAME"];
                            $name = implode(" ",$name);
                            ?>
                            <a href="activity" class="ajax_link"  params="id=<?=$user["ID"]?>"><?=(trim($name))?$name:$user["ID"]?></a><br/>
                            <span style="font-size:12px;">
                            <?if($user["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
                            <?=$user["PERSONAL_CITY"]?>
                            </span>
                        </p>                        
                </td>
                <td><?=$user["EMAIL"]?></td>
                <td><?=($user["WORK_NOTES"])?substr($user["WORK_NOTES"], 0,100)."...":""?></td>
                <td><?=($user["UF_FB_USER_ID"])?"Да":""?></td>
                <td><?=($user["UF_VK_USER_ID"])?"Да":""?></td>
                <td class="font18">
                    <?if ($user["RESTICS"]>0):?>
                        <a href="account_details" class="ajax_link" params="id=<?=$user["ID"]?>"><?=$user["RESTICS"]?></a>
                    <?else:?>
                        <a href="account_details" class="ajax_link" params="id=<?=$user["ID"]?>"><?=$user["RESTICS"]?></a>
                    <?endif;?>
                </td>
                <td>
                    <p class="user-mail"><a title="Написать сообщение" onclick="send_message(<?=$user["ID"]?>,'<?=$user["NAME"]?> <?=$user["LAST_NAME"]?>')" href="javascript:void(0)">Сообщение</a>
                    <p class="user-edit"><a title="Редактировать профиль <?=$name?>" alt="Редактировать профиль <?=$name?>" target="_blank" href="/users/id<?=$user["ID"]?>/profile_edit/">Редактировать</a></p>
                    <p class="user-delete"><a title="Удалить <?=$name?>" alt="Удалить <?=$name?>" onclick="del_user(<?=$user["ID"]?>,'<?=$user["NAME"]?> <?=$user["LAST_NAME"]?>')" href="javascript:void(0)">Удалить</a></p>                    
                </td>
            </tr>
        <?endforeach?>
        <?else:?>
            <tr><td><div class="errortext">
                <?if ($arParams["ID"]==$USER->GetID()):?>
                    <?=GetMessage("NO_YOU_FRIENDS")?>
                <?else:?>
                    <?=GetMessage("NO_FRIENDS")?>
                <?endif;?>
            </div></td></tr>
        <?endif;?>
    </table>

    <?if(strLen($arResult["NAV_STRING"]) > 0):?>
<div align="right">
        <br><?=$arResult["NAV_STRING"]?><br /><br />
        </div>
    <?endif;?>

<?endif?>