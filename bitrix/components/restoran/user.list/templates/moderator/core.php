<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
function delete_user($id)
{    
    CModule::IncludeModule("sale");
    $id = (int)$id;    
    if (CSite::InGroup(Array(1,20,14,15)))
    {
        $ar = CSaleUserAccount::GetByUserID($id, "RUB");
        if ($ar["ID"])
            if (CSaleUserAccount::Delete($ar["ID"]))
                if ($r = CUser::Delete($id)) 
                    echo "Пользователь удален.";                
    }
    else
        echo "Недостаточно прав доступа";
}

function account_detail($id)
{
    global $APPLICATION;
    $APPLICATION->IncludeComponent(
	"restoran:user.account.details",
	"",
	Array(
                "AJAX" => "Y",
                "USER_ID"=>(int)$id,
                "PAGE_COUNT" => 10,
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
                "PAGER_TEMPLATE" => "search_rest_list",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	),
    false
    );
}
function add_transaction()
{
    $id = (int)$_REQUEST["id"];
    $price = $_REQUEST["price"];
    CModule::IncludeModule("sale");    
//    $r = CSaleUserTransact::Add(
//        Array(
//            "USER_ID" => $id,
//            "AMOUNT" => $price,
//            "CURRENCY" => "RUB",
//            "DEBIT" => $debit,
//            "DESCRIPTION" => $_REQUEST["description"],
//            "EMPLOYEE_ID" => $USER->GetID(),
//        )
//    );
    if ($price&&$id)
    {
        $r = CSaleUserAccount::UpdateAccount(
                $id,
                $price,
                "RUB",
                $_REQUEST["description"]
            );
        if ($r)
            echo 'Успешно добавлено';
        else
            echo 'Произошла ошибка попробуйте позже';
    }
    else
    {
        echo "Вы не заполнили обязательные поля";
    }
}

function activity(){
    global $APPLICATION;
    global $arrFilterTop4;
        $arrFilterTop4["CREATED_BY"] = (int)$_REQUEST["id"];
        $arrFilterTop4["IBLOCK_TYPE"] = Array("on_plate","cookery","reviews","comment","news","overviews","afisha","interview","photoreports","blogs");
        $arrFilterTop4["ACTIVE"] = "";
    $APPLICATION->IncludeComponent(
	"restoran:catalog.list",
	"activity_moderator",
	Array(
                 "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "",
                "IBLOCK_ID" => "",
                "NEWS_COUNT" => 50,
                "SORT_BY1" => "DATE_CREATE",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arrFilterTop4",
                "FIELD_CODE" => array("ACTIVE","CREATED_BY","DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "ACTIVITY" => "Y"
	),
    false
    );
}

if ($_REQUEST["act"]=="delete_user")
    delete_user($_REQUEST["id"]);
if ($_REQUEST["act"]=="account_details")
    account_detail($_REQUEST["id"]);
if ($_REQUEST["act"]=="add_transaction")
    add_transaction();
if ($_REQUEST["act"]=="activity")
    activity();

?>
