<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["ITEMS"] as $key=>$friend) {
    // check user restorator stat
    $arGroups = CUser::GetUserGroup($friend["ID"]);
    if(in_array(RESTORATOR_GROUP, $arGroups))
        $arResult["ITEMS"][$key]["IS_RESTORATOR"] = true;
    CModule::IncludeModule("socialnetwork");
    CModule::IncludeModule("sale");
    $arResult["ITEMS"][$key]["IS_FRIEND"] = CSocNetUserRelations::GetRelation($USER->GetID(), $friend["ID"]);
    // resize photo
    if ($friend["PERSONAL_PHOTO"])
        $arResult["ITEMS"][$key]["PERSONAL_PHOTO"] = CFile::ResizeImageGet($friend["PERSONAL_PHOTO"], array('width'=>46, 'height'=>46), BX_RESIZE_IMAGE_EXACT, true);
    else
    {
        if ($friend["PERSONAL_GENDER"]=="M")
            $arResult["ITEMS"][$key]["PERSONAL_PHOTO"]["src"] = "/tpl/images/noname/man_nnm.png";
        elseif($friend["PERSONAL_GENDER"]=="F")
            $arResult["ITEMS"][$key]["PERSONAL_PHOTO"]["src"] = "/tpl/images/noname/woman_nnm.png";
        else
            $arResult["ITEMS"][$key]["PERSONAL_PHOTO"]["src"] = "/tpl/images/noname/unisx_nnm.png";
    }
    if ($ar = CSaleUserAccount::GetByUserID($friend["ID"], "RUB")) 
    {
        $arResult["ITEMS"][$key]["RESTICS"] = sprintf("%01.0f", $ar["CURRENT_BUDGET"]);
    }

}
?>