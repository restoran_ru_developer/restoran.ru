<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(strlen($arResult["FatalError"])>0):?>
    <?=$arResult["FatalError"]?>
<?else:?>
    <?if(strlen($arResult["ErrorMessage"])>0):?>
        <?=$arResult["ErrorMessage"]?>
    <?endif?>

    <!--<h2><?=GetMessage("FRIENDS_TITLE")?></h2>-->
    <table class="friendlist">
        <?if (count($arResult["ITEMS"][0])>0):?>
            <?foreach ($arResult["ITEMS"] as $cell=>$user):?>
            <?if($cell%4 == 0):?>
                        <tr>
                    <?endif;?>
                <td width="240">
                    <div class="fleft">
                        <div class="avatar">
                            <img src="<?=$user["PERSONAL_PHOTO"]["src"]?>" width="64" />
                        </div>    
                        <?if ($USER->GetID()!=$user["ID"]):?>
                        <div id="friend_result_<?=$user["ID"]?>">
                            <?if ($user["IS_FRIEND"]=="F"):?>
                                <?$APPLICATION->IncludeComponent(
                                    "restoran:user.friends_delete",
                                    "remove_friend_from_list_all",
                                    Array(
                                        "FIRST_USER_ID" => $USER->GetID(),
                                        "SECOND_USER_ID" => $user["ID"],
                                        "RESULT_CONTAINER_ID" => "friend_result_".$user["ID"]
                                    ),
                                    false
                                );?>
                            <?elseif($user["IS_FRIEND"]=="Z"):?>

                            <?else:?>
                                <?$APPLICATION->IncludeComponent(
                                    "restoran:user.friends_add",
                                    "add_friend_from_list_all",
                                    Array(
                                        "FIRST_USER_ID" => $USER->GetID(),
                                        "SECOND_USER_ID" => $user["ID"],
                                        "RESULT_CONTAINER_ID" => "friend_result_".$user["ID"]
                                    ),
                                    false
                                );?>
                            <?endif;?>
                        </div>
                        <?else:?>
                        <a><?=GetMessage("ITS_YOU")?></a>
                        <?endif;?>
                    </div>
                    <div class="fright">
                        <p>
                            <?
                            $name = array();
                            $name[] = $user["NAME"];
                            $name[] = $user["PERSONAL_PROFESSION"];
                            $name[] = $user["LAST_NAME"];
                            $name = implode(" ",$name);
                            ?>
                            <a href="/users/id<?=$user["ID"]?>/"><?=(trim($name))?$name:$user["ID"]?></a><br/>
                            <?if($user["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
                            <?=$user["PERSONAL_CITY"]?>
                        </p>
                        <p class="user-mail"><a onclick="send_message(<?=$user["ID"]?>,'<?=$user["NAME"]?> <?=$user["LAST_NAME"]?>')" href="javascript:void(0)"><?=GetMessage("SEND_MSG")?></a></p>                        
                        <?if ($USER->GetID()!=$user["ID"]):?>
                            <div style="margin-top:10px;">
                                
                            </div>
                        <?endif;?>
                    </div>
                    <div class="clear"></div>
                </td>
            <?$cell++;
            if($cell%4 == 0):?>
                            </tr>
                    <?endif?>
            <?endforeach?>

            <?if($cell%4 != 0):?>
            <?while(($cell++)%4 != 0):?>
                <td>&nbsp;</td>
                <?endwhile;?>
            </tr>
            <?endif?>
        <?else:?>
            <tr><td><div class="errortext">
                <?if ($arParams["ID"]==$USER->GetID()):?>
                    <?=GetMessage("NO_YOU_FRIENDS")?>
                <?else:?>
                    <?=GetMessage("NO_FRIENDS")?>
                <?endif;?>
            </div></td></tr>
        <?endif;?>
    </table>

    <?if(strLen($arResult["NAV_STRING"]) > 0):?>
<div align="right">
        <br><?=$arResult["NAV_STRING"]?><br /><br />
        </div>
    <?endif;?>

<?endif?>