<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("AUTH_VKONTAKTE_TITLE"),
	"DESCRIPTION" => GetMessage("AUTH_VKONTAKTE_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "users_restaurant", // for example "my_project"
        "NAME" => GetMessage("USERS_RESTAURANT"),
	),
	"COMPLEX" => "N",
);

?>