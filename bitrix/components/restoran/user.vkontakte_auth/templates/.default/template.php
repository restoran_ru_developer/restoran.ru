<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>    
<script>
function showRegpromptVK() {       
    $(".ajax_form").html($('#prompt_vk').html());
    $("#vk_user_auth").submit(function(e) {
    	var form = $(this);    	
    	if (!e.isDefaultPrevented()) {    		
            $.ajax({
                type: "POST",
                url: "/bitrix/components/restoran/user.vkontakte_auth/templates/.default/authorize.php",
                data: form.serialize(),
                beforeSend: function(data)
                {
                    /*if (!$("#vk_email").attr("value"))
                    {
                        alert("Введите E-mail");
                        return false;
                    }
                    if (!$("#vk_phone").attr("value"))
                    {
                        alert("Введите номер телефона");
                        return false;
                    }*/
                    
                    return true;
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);
                    // if result OK show confirm code field
                    if (obj.ERROR)
                    {
                        $('#auth_modal2 #ajax_object_response').html(obj.ERROR);
                        return false;
                    }
                    if(obj.TYPE == "AUTH") {
                        location.href = "/";
                    } else if(obj.TYPE == "REQ_CONFIRM_CODE") {
                        $('#auth_modal2 > .center > p:eq(0)').hide();
                        $('#auth_modal2 > .center > p:eq(1)').hide();
                        $('#auth_modal2 > .center > p:eq(2)').show();
                        $('#auth_modal2 > form > div:eq(0)').html(obj.HTML);
                        // reinitialize validator
                       // $("#auth_modal > form").validator({ lang: 'ru' });
                    } else if(obj.TYPE == "ADD") {
                        location.href = "/";
                    }
                }
            });
    		// prevent default form submission logic
    		e.preventDefault();
    	}
        return false;
    });
}
</script>
<div onclick="VK.Auth.login(authInfo);"><img class="pointer" src="<?=SITE_TEMPLATE_PATH?>/images/vk_login.png" /></div>
<!--<div id="login_button<?=$arParams["PREFIX"]?>" onclick="VK.Auth.login(authInfo);"></div>-->

<div id="vk_api_transport"></div>
<script language="javascript">
window.vkAsyncInit = function() {
  VK.init({
    apiId: <?=$arParams["APP_ID"]?>
  });
  //VK.UI.button('login_button<?=$arParams["PREFIX"]?>');
};

setTimeout(function() {
  var el = document.createElement("script");
  el.type = "text/javascript";
  el.src = "//vk.com/js/api/openapi.js";
  el.async = true;
  document.getElementById("vk_api_transport").appendChild(el);
}, 0);

function authInfo(response) {
    // if success authorize require email
    if (response.session) {
        // authorize if user exist and VK ID == user id
        //showExpose();
        $.ajax({
            type: "POST",
            url: "<?=$templateFolder?>/getUserVK.php",
            data: "vk_user_id=" + response.session.mid,
            success: function(data) {
                var obj = jQuery.parseJSON(data);
                if(obj.TYPE == "AUTH") {
                	location.href = "/";
                } else {
                    $('#vk_user_id').val(response.session.mid);
                    showRegpromptVK();
                }
            }
        });
    }
}
//VK.Auth.getLoginStatus(authInfo);

</script>

<div class="modal_vk" id="prompt_vk">    
    <div class="title">Заполните поля ниже</div>
    <div id="ajax_object_response"></div>                
        <form id="vk_user_auth">            
            <div class="form-group">                
                <input id="vk_email" class="form-control" name="vk_email" type="email" required="required" placeholder="E-mail"/>
            </div>                
            <input type="hidden" id="vk_user_id" name="vk_user_id" />
            <input type="hidden" id="vk_step" name="vk_step" value="1" />
            <br />            
            <input type="submit" value="Продожить" class="btn btn-info btn-nb" style="width:100%">                        
        </form>    
 </div>