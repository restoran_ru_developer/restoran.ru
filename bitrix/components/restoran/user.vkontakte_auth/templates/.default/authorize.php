<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/classes/vkapi.class.php");

global $USER;
$userLogin = $_REQUEST["vk_email"];
$vkUserID = $_REQUEST["vk_user_id"];

$step = $_REQUEST["vk_step"];

$user = new CUser;

if($step == 1) {
    // check user fo exist
    $rsUser = CUser::GetByLogin($userLogin);
    $arUser = $rsUser->Fetch();
    if($arUser) {
        // user VK User ID prop update
        $arUserFields = Array(
            "UF_VK_USER_ID" => $vkUserID
        );
//        $api_id = COption::GetOptionString("socialservices", "vkontakte_appid");
//        $secret_key = COption::GetOptionString("socialservices", "vkontakte_appsecret");
//        $VK = new vkapi($api_id, $secret_key);
//        $resp2 = $VK->api('users.get', array('uids' => $vkUserID, "fields"=>"uid, first_name, last_name, nickname, screen_name, sex, bdate, city, country, photo, photo_medium, photo_big, contacts"));
//        
        $user->Update($arUser["ID"], $arUserFields);
        // user authorize
        $USER->Authorize($arUser["ID"]);
        $arResult["TYPE"] = "AUTH";
    } else {
        // save to session and post form
        session_start();
        $_POST["vk_confirm_code"] = randString(5);
        $_SESSION['form_serialize'] = $_POST;
        /*
        // send sms with code via http://sms-kontakt.ru/
        if (CModule::IncludeModule("sozdavatel.sms")) {
            $message = "Код для подтверждения регистрации: ".$_POST["vk_confirm_code"];
            CSMS::Send($message, $_POST["vk_phone"], "UTF-8");
        }

        $arResult["HTML"] = '<div class="question">Код:<br />
                            <input id="vk_confirm_code" name="vk_confirm_code" type="text" class="inputtext" required="required" style="width:380px" />
                            <input type="hidden" id="vk_step" name="vk_step" value="2" /><br /><br />';
        $arResult["TYPE"] = "REQ_CONFIRM_CODE";*/
        $api_id = COption::GetOptionString("socialservices", "vkontakte_appid");
        $secret_key = COption::GetOptionString("socialservices", "vkontakte_appsecret");
        // user add
        $VK = new vkapi($api_id, $secret_key);
        $resp = $VK->api('getProfiles', array('uids' => $_SESSION["form_serialize"]["vk_user_id"]));
        
        $resp2 = $VK->api('users.get', array('uids' => $_SESSION["form_serialize"]["vk_user_id"], "fields"=>"uid, first_name, last_name, nickname, screen_name, sex, bdate, city, country, photo, photo_medium, photo_big, contacts"));

        if ($resp2["response"][0]["sex"]=="2")
            $gender = "M";
        if ($resp2["response"][0]["sex"]=="1")
            $gender = "F";
        
        if ($resp2["response"][0]["photo_big"])
        {
            $a = CFile::MakeFileArray($resp2["response"][0]["photo_big"]);
            $a["MODULE_ID"] = "main";
        }
        if ($resp2["response"][0]["city"]==2)
        {
            $city = "Санкт-Петербург";
        }
        if ($resp2["response"][0]["city"]==1)
        {
            $city = "Москва";
        }
        
        $bdate = $DB->FormatDate($resp2["response"][0]["bdate"], "D.M.YYYY", "DD.MM.YYYY");
        
        $new_password = randString(7);
        $nic = explode("@",$_SESSION["form_serialize"]["vk_email"]);
        $nic = $nic[0];
        $arUserFields = Array(
		"ACTIVE"=>'Y',
            "NAME" => $resp["response"][0]["first_name"],
            "LAST_NAME" => $resp["response"][0]["last_name"],
            "PERSONAL_PHONE" => $_SESSION["form_serialize"]["vk_phone"],
            "PERSONAL_PROFESSION" => $nic,
            "EMAIL" => $_SESSION["form_serialize"]["vk_email"],
            "LOGIN" => $_SESSION["form_serialize"]["vk_email"],
            "PERSONAL_GENDER" => $gender,
            "PERSONAL_PHOTO" => $a,
            "PASSWORD" => $new_password,
            "CONFIRM_PASSWORD" => $new_password,
            "CONFIRM_CODE" => $_SESSION["form_serialize"]["vk_confirm_code"],
            "UF_VK_USER_ID" => $_SESSION["form_serialize"]["vk_user_id"],
            "GROUP_ID"      => array(5),
            "PERSONAL_CITY" => $city,
            //"PERSONAL_BIRTHDAY" => $bdate,
        );                    
        
        
        $ID = $user->Add($arUserFields);
        // user authorize
        if($ID) {
            $USER->Authorize($ID);
            $arResult["TYPE"] = "ADD";
        } else {
            $arResult["ERROR"] = $user->LAST_ERROR;
        }
        // clear garbage
        unset($_SESSION['form_serialize']);
    }
} 
echo json_encode($arResult);
?>