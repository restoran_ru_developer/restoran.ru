<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Количество пользователей на странице",
	"DESCRIPTION" => "Количество пользователей на странице",
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content_restoran", // for example "my_project"
                "NAME" => "Ресторан.ру",
                "CHILD" => array(
                    "ID" => "restoraunt",
                ),	
	),
	"COMPLEX" => "N",
);

?>