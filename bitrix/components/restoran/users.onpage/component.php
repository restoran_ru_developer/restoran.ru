<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (CModule::IncludeModule("statistic"))
{
    $arFilter = array(
       "URL_LAST" => "%".str_replace("http://".SITE_SERVER_NAME, "", $_SERVER["HTTP_REFERER"]),
       "USER_AUTH" => "Y"
    );
    $rs = CUserOnline::GetList($guest_counter, $session_counter, false, $arFilter);   
    $ids = array();
    while ($ar = $rs->Fetch())
    {
        if (!in_array($ar["LAST_USER_ID"], $ids))
        {
            $rsUser = CUser::GetByID($ar["LAST_USER_ID"]);
            $arUser = $rsUser->Fetch();
            $ids[] = $arUser["ID"];
            $arU["NAME"] = $arUser["NAME"];
            $arU["LAST_NAME"] = $arUser["LAST_NAME"];
            $arU["ID"] = $arUser["ID"];
            $arResult["USERS_ON_PAGE"][] = $arU;	
        }
    }
    $arResult["FILTER"] = $arFilter;
    $this->IncludeComponentTemplate();
}
else
{
    echo "STATISTIC MODULE DISABLED";
}
?>