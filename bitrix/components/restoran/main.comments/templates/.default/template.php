<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script>
    $(document).ready(function(){        
        $("#comments tr").hover(function(){            
            $(this).find(".answer").show();
        }, function(){
            $(this).find(".answer").hide();
        } );
    });
    function answer_me(a)
    {
        var b = a.length+2;
        $("#review").html(a+", ");
        $("#review").focus();
        document.getElementById("review").setSelectionRange(b, b);
    }
</script>
<div id="comments">
    <ul>
        <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
        <li>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td width="80" valign="top">
                        <div class="ava">
                            <?if($commVal["AVATAR"]):?>
                                <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" />
                            <?endif?>
                        </div>
                    </td>
                    <td width="150"  valign="top">
                        <span class="name"><?=$commVal["NAME"]?></span>
                        <span class="comment_date"><?=$commVal["FORMATED_DATE_1"]?></span> <?=$commVal["FORMATED_DATE_2"]?>
                    </td>
                    <td  valign="top" class="hover_answer">
                        <i><?=$commVal["PREVIEW_TEXT"]?></i>
                        <div class="answer" style="display: none"><br /><a href="#review" class="another anchor" onclick="answer_me('<?=$commVal["NAME"]?>')">Ответить</a></div>
                    </td>
                </tr>
            </table>
            <?if($commVal["CHILD_COMMENTS"]):?>
                <ul>
                    <?foreach($commVal["CHILD_COMMENTS"] as $childComm):?>
                    <li>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80"   valign="top">
                                    <div class="ava">
                                        <?if($commVal["AVATAR"]):?>
                                            <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" />
                                        <?endif?>
                                    </div>
                                </td>
                                <td width="150"  valign="top">
                                    <span class="name"><?=$childComm["NAME"]?></span>
                                    <span class="comment_date"><?=$childComm["FORMATED_DATE_1"]?></span> <?=$childComm["FORMATED_DATE_2"]?>
                                </td>
                                <td  valign="top" class="hover_answer">
                                    <i><?=$childComm["PREVIEW_TEXT"]?></i>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <?endforeach?>
                </ul>
            <?endif?>
        </li>
        <?endforeach?>
    </ul>
    <?
    $APPLICATION->IncludeComponent(
                "restoran:main.comments_add",
                ($arParams["ADD_COMMENT_TEMPLATE"])?$arParams["ADD_COMMENT_TEMPLATE"]:"",
                Array(
                        "IBLOCK_TYPE" => "comments",
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $arParams["SECTION_ID"],                        
                )
    );?>
</div>