<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div id="comments">
    <?if(count($arResult["COMMENTS"])):?>
        <script>
            $(document).ready(function(){        
                $("#comments tr").hover(function(){            
                    $(this).find(".answer").css("visibility","visible");
                }, function(){
                    $(this).find(".answer").css("visibility","hidden");
                } );
            });
            function answer_me(a)
            {
                var b = a.length+2;
                $("#review").html(a+", ");
                $("#review").focus();
                document.getElementById("review").setSelectionRange(b, b);
            }
        </script>
        <ul>
            <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
            <li>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="80" valign="top">
                            <div class="ava">
                                <?if($commVal["AVATAR"]):?>
                                    <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" />
                                <?endif?>
                            </div>
                        </td>
                        <td width="150"  valign="top">
                            <span class="name"><?=$commVal["NAME"]?></span>
                            <span class="comment_date"><?=$commVal["FORMATED_DATE_1"]?></span> <?=$commVal["FORMATED_DATE_2"]?>
                        </td>
                        <td  valign="top" class="hover_answer">
                            <div class="rating left" style="padding-bottom:0px;">
                                <?for($p=0;$p<$commVal["DETAIL_TEXT"];$p++):?>
                                    <div class="small_star_a"></div>                                        
                                <?endfor?>
                                <?for($p;$p<5;$p++):?>
                                    <div class="small_star"></div>
                                <?endfor;?>
                            </div>                        
                            <div class="clear"></div>
                            <i><?=$commVal["PREVIEW_TEXT"]?></i>                        
                            <div class="answer" style="visibility: hidden"><br /><a href="#review" class="another anchor" onclick="answer_me('<?=$commVal["NAME"]?>')">Ответить</a></div>
                        </td>
                    </tr>
                </table>            
            </li>
            <?endforeach?>
        </ul>
    <?endif;?>
    <?
    $APPLICATION->IncludeComponent(
                "restoran:main.comments_add",
                ($arParams["ADD_COMMENT_TEMPLATE"])?$arParams["ADD_COMMENT_TEMPLATE"]:"",
                Array(
                        "IBLOCK_TYPE" => "comments",
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $arParams["SECTION_ID"],                        
                )
    );?>
</div>