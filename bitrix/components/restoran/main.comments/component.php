<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600;
}

if(!isset($arParams["PROPERTY_PARENT_BIND_CODE"])) {
	$arParams["PROPERTY_PARENT_BIND_CODE"] = 'PARENT_ID';
}

if(!isset($arParams["PROPERTY_USER_BIND_CODE"])) {
	$arParams["PROPERTY_USER_BIND_CODE"] = 'USER_BIND';
}

if(count($arParams["FIELD_CODE"]) <= 0)
    $arParams["FIELD_CODE"] = array(
        "ID",
        "IBLOCK_ID",
        "IBLOCK_SECTION_ID",
        "NAME",
        "ACTIVE_FROM",
        "DATE_CREATE",
        "DETAIL_PAGE_URL",
        "DETAIL_TEXT",
        "DETAIL_TEXT_TYPE",
        "PREVIEW_TEXT",
        "PREVIEW_TEXT_TYPE",
        "PREVIEW_PICTURE",
        "CREATED_BY"
    );

if(count($arParams["PROPERTY_CODE"]) <= 0)
    $arParams["PROPERTY_CODE"] = Array();

if($this->StartResultCache(false, array($arParams["CACHE_NOTES"], $USER->GetGroups(), $arParams["IBLOCK_ID"], $arParams["SECTION_ID"])))
{
    if(!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError("IBLOCK_MODULE_NOT_INSTALLED");
        return false;
    }

    if(!$arParams["IBLOCK_ID"]) {
        $this->AbortResultCache();
        ShowError("ERROR_IBLOCK_ID_NOT_SET");
        return false;
    }

    /*if(!$arParams["SECTION_ID"]) {
        $this->AbortResultCache();
        ShowError("ERROR_SECTION_ID_NOT_SET");
        return false;
    }*/
    if($arParams["SECTION_ID"]):
        // get comments list
        $arSelectFields = array_merge($arParams["FIELD_CODE"], $arParams["PROPERTY_CODE"]);
        $rsComments = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arParams["SECTION_ID"],
            ),
            false,
            false,
            $arSelectFields
        );
        while($arComments = $rsComments->GetNext()) {
            // format date
            $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($arComments["DATE_CREATE"], CSite::GetDateFormat()));
            $arTmpDate = explode(" ", $arTmpDate);
            $arComments["FORMATED_DATE_1"] = $arTmpDate[0];
            $arComments["FORMATED_DATE_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

            // get user avatar
            if($arComments["CREATED_BY"]) {
                $rsUser = CUser::GetByID($arComments["CREATED_BY"]);
                $arUser = $rsUser->Fetch();
                $arComments["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            }

            // set comment info
            if(!$arComments["PROPERTY_".$arParams["PROPERTY_PARENT_BIND_CODE"]."_VALUE"]) {
                $arResult["COMMENTS"][$arComments["ID"]] = $arComments;
            } else {
                // set child comments
                $arResult["COMMENTS"][$arComments["PROPERTY_".$arParams["PROPERTY_PARENT_BIND_CODE"]."_VALUE"]]["CHILD_COMMENTS"][] = $arComments;
            }
        }

        //v_dump($arResult["COMMENTS"]);

            $this->SetResultCacheKeys(array(
                    "ID",
                    "IBLOCK_ID",
                    "NAME",
                    "IBLOCK_SECTION_ID",
                    "IBLOCK",
                    "LIST_PAGE_URL", 
                    "~LIST_PAGE_URL",
                    "SECTION",
                    "PROPERTIES",
            ));
    endif;
    $this->IncludeComponentTemplate();

}

?>