<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Спасибо, Ваш комментарий сохранен.";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут оставлять комментарии.";
?>