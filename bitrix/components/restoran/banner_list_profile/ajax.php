<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if (check_bitrix_sessid()&&$_REQUEST["review"]&&$_REQUEST["IBLOCK_TYPE"]&&(int)$_REQUEST["IBLOCK_ID"]&&(int)$_REQUEST["ELEMENT_ID"]):?>
    <?$APPLICATION->IncludeComponent(
            "restoran:comments_add",
            ".default",
            Array(
                    "REVIEW" => trim($_REQUEST["review"]),
                    "ACTION" => "save",
                    "RATIO" => (int)$_REQUEST["ratio"],
                    "IBLOCK_TYPE" => trim($_REQUEST["IBLOCK_TYPE"]),
                    "IBLOCK_ID" => (int)$_REQUEST["IBLOCK_ID"],
                    "ELEMENT_ID" => (int)$_REQUEST["ELEMENT_ID"],
                    "IS_SECTION" => trim($_REQUEST["IS_SECTION"]), 
            )
    );?>
<?endif;?>
<?require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>