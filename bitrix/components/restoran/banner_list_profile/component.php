<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $USER;



/**
* Основные параметры 
*/
if(empty($arParams['USER_ID']))
{
	$arParams['USER_ID'] = $USER->GetID();
}


/**
* Получаем список контрактов для данного пользователя 
*/
//if($USER->IsAdmin())
    {
	if(CModule::IncludeModule("advertising"))
	{
		
		
		
		/**
		* Получаем все рестораны привязанные к пользователю
		*/
		$arFilter = array(
			"IBLOCK_ID" => Array(11,12),
			"PROPERTY_user_bind" => $USER->GetID(),
                        "ACTIVE" => "Y"
		);
		
		$resRestraunts = CIBlockElement::GetList(array(), $arFilter);
		while($arRestraunt = $resRestraunts->GetNextElement())
		{
			$fields = $arRestraunt->GetFields();
			$fields['PROPERTIES'] =  $arRestraunt->GetProperties();
			$arResult['RESTRAUNTS'][$fields['ID']] = $fields;
		}
		$arResult['RESTRAUNTS_IDS'] = array_keys($arResult['RESTRAUNTS']);
		
		
		
		
		
		
		
		
		
		
		
		$arFilter = array(
			"DESCRIPTION" => $arParams['USER_ID']
		);
		$resAdvContracts = CAdvContract::GetList($by, $order, $arFilter);
		
		while($arAdvContract = $resAdvContracts->Fetch())
		{
			//var_dump($arAdvContract );
			$arResult['RESTRAUNTS_CONTRACT'][$arAdvContract['ADMIN_COMMENTS']] = array();
			$arResult['RESTRAUNT_CONTRACT_IDS'][] = $arAdvContract['ADMIN_COMMENTS'];
			$arResult['CONTRACTS'][$arAdvContract['ID']] = $arAdvContract;
			$arResult['CONTRACT_IDS'][] = $arAdvContract['ID'];
			
		}
		//$rsAdvContract = CAdvContract::GetList($by, $order, $arFilter, $is_filtered, "N");
		
		
		/**
		* Получаем баннеры всех контрактов
		*/
		if(count($arResult['CONTRACT_IDS']) > 0)
		{
			$arFilter = array(
				"CONTRACT_ID" => implode("|",$arResult['CONTRACT_IDS'])
			);
			$resBanners = CAdvBanner::GetList($by, $desc, $arFilter);
			
			while($arBanner = $resBanners->Fetch())
			{
				$arResult['CONTRACT_BANNERS'][$arBanner['CONTRACT_ID']][] = $arBanner;
			}
		}
		
		/*echo "<pre>";
			var_dump($arResult);
		echo "</pre>";*/


		
		
		
		/**
		* Получить разницу между привязанными к пользователю ресторанами ('RESTRAUNTS_IDS') и ресторанами данного пользователя для которых созданы контракты ('RESTRAUNT_CONTRACT_IDS') 
		*/
		if(count($arResult['CONTRACT_IDS'])  === 0)
		{
			$arResult['RESTRAUNT_NO_CONTRACT'] = $arResult['RESTRAUNTS_IDS'];
		}
		else
		{
			$diffNeedToCreate = array_diff($arResult['RESTRAUNTS_IDS'], $arResult['RESTRAUNT_CONTRACT_IDS']);
		
			$arResult['RESTRAUNT_NO_CONTRACT'] = $diffNeedToCreate;
		}
		
		
		
		
		
		/**
		* Если не существует каких-нибудь контрактов для какого-нибудь из ресторана пользователя, то создаём его!
		*/
		
		if(count($arResult['RESTRAUNT_NO_CONTRACT']) > 0)
		{
			foreach($arResult['RESTRAUNT_NO_CONTRACT'] as $key => $value)
			{
			
				$hours = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23);
				$arrWEEKHOURS = array(
					"MONDAY" => $hours,
					"TUESDAY" => $hours,
					"WEDNESDAY" => $hours,
					"THURSDAY" => $hours,
					"FRIDAY" => $hours,
					"SATURDAY" => $hours,
					"SUNDAY" => $hours
				);
			
				$arFields = array(
						"ACTIVE" => "Y",
						"NAME" => $USER->GetFullName()." — ".$arResult['RESTRAUNTS'][$value]['NAME'],
						"DESCRIPTION" => $USER->GetID(),
						"ADMIN_COMMENTS" => $value,
						"DEFAULT_STATUS_SID" => READY,
						"arrWEEKDAY" => $arrWEEKHOURS,
						"arrSITE" => array('s1','s2'),
						"arrTYPE" => array("ALL")
					);
					
				$result = CAdvContract::Set(
					$arFields,
					"",
					"N"
				);
				
			}
		}
		
	
		
		/**
		* Контракты с уже отсутствующим рестораном
		*/
		
		//Получили ID для удаления
		$contractsToDelete = array_diff(array_keys($arResult['RESTRAUNTS_CONTRACT']), $arResult['RESTRAUNTS_IDS']);
		$arResult['RESTRAUNT_CONTRACTS_TO_DELETE'] = $contractsToDelete;
		
		
		if(count($arResult['RESTRAUNT_CONTRACTS_TO_DELETE'] ) > 0)
		{
			//Получить ID контрактов для удаления
			
			foreach($arResult['CONTRACTS'] as $cid => $c)
			{
				if(in_array($c['ADMIN_COMMENTS'], $arResult['RESTRAUNT_CONTRACTS_TO_DELETE']))
				{
					$arResult['CONTRACTS_TO_DELETE'][] = $cid;
					unset($arResult['CONTRACTS'][$cid]);
					unset($arResult['CONTRACT_IDS'][$cid]);
					unset($arResult['CONTRACT_BANNERS'][$cid]);
					/*Удаление баннеров*/
					
					$banners = CAdvBanner::GetList(
						$by, $order,
						array('CONTRACT_ID' => $cid)
					);
					while($ab = $banners->Fetch())
					{
						//var_dump($ab);
						CAdvBanner::Delete($ab['ID'], "N");
					}
					
					
					CAdvContract::Delete( $cid,"N");
				}
			}
					

		}
		
		

	}
		
}
$this->IncludeComponentTemplate();    
?>