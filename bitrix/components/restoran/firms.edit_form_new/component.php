<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if($arParams["REST_ID"]=="" || $arParams["REST_ID"]<=0)
	return;  

if(!CModule::IncludeModule("iblock"))
    return;


$arRestIB = getArIblock("firms", CITY_ID);

if(!$arParams["USER_ID"])
    $arParams["USER_ID"] = $USER->GetID();

$arParams["REQUIRED_PROPERTIES"] = Array(
    "NAME"
);

//entertainment

$arParams["PROPERTIES"] = Array(    
    "INFORMATION","SITE", "PHONE","EMAIL", "SUBWAY", "ADDRESS", "D_TOURS", "OPENING_HOURS","OPENING_HOURS_GOOGLE", "ADMINISTRATIVE_DISTR", "AREA", "ADD_PROPS","PROPOSALS","SIMILAR_REST", "WORKING", "PHOTOS","GOOGLE_PHOTO","VIDEOPANORAMY", "LANDMARKS","MAP","LAT");

$arParams["HIDDEN_PROPERTIES"]=array("MAP", "ADD_PROPS");

$arParams["ADMIN_ONLY"]=array("D_TOURS","PRIORITY_KITCHEN","PRIORITY_TYPE", "TOP_SPEC_PLACE_1", "TOP_SPEC_PLACE_2", "S_PLACE_RESTAURANST", "S_PLACE_BANKET", "S_PLACE_DOSTAVKA", "WORKING");


$arParams["REQUIRED_PROPERTIES"] = Array(
    "TYPE"
);



if(check_bitrix_sessid() && $_POST["submit"]) {
    // merge req fields
    $arReqFields = array_merge($arParams["REQUIRED_PROPERTIES"], $arParams["REQUIRED_PROPERTIES"]);

    // compose arFields
/*    foreach($_POST["PROP"] as $keyProp=>$prop) {
        $arTmpProp[$keyProp] = $prop;
        v_dump($keyProp);
        v_dump($prop);
    }*/
    $el = new CIBlockElement;

    $arFields["ID"] = $_POST["ID"];
    if($_POST["IBLOCK_ID"]) {
        $arFields["IBLOCK_ID"] = $_POST["IBLOCK_ID"];
    } else {
        $arFields["IBLOCK_ID"] = $arRestIB["ID"];
    }
    $arFields["IBLOCK_SECTION_ID"] = $_POST["IBLOCK_SECTION_ID"];
    $arFields["NAME"] = $_POST["NAME"];
    $arFields["PROPERTY_VALUES"] = $_POST["PROP"];
    if($arFields["ID"]) {
        $rs = $el->Update($arFields["ID"], $arFields);
    } else {
        $el->Add($arFields);
    }
    //v_dump($arFields);
} else {
    $arResult["ERROR"] = GetMessage("ERROR_CHECK");
}

foreach ($arParams["PROPERTIES"] as $PropValue) {
    //$arTmpRestProps = Array();
    // set property for select
    $arPropValueCode[] = 'PROPERTY_'.$PropValue;
    // get property params
    $rsProp = CIBlockProperty::GetByID($PropValue, $arRestIB["ID"]);
    $arProp = $rsProp->Fetch();
    
    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["ID"] = $arProp["ID"];
    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["CODE"] = $arProp["CODE"];
    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["NAME"] = $arProp["NAME"];
    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["PROPERTY_TYPE"] = $arProp["PROPERTY_TYPE"];
    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["LIST_TYPE"] = $arProp["LIST_TYPE"];
    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["MULTIPLE"] = $arProp["MULTIPLE"];
    //v_dump($arProp);

    switch($arProp["PROPERTY_TYPE"]) {
        case "E":
            $rsEl = CIBlockElement::GetList(
                Array("SORT"=>"ASC","NAME"=>"ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"],
                ),
                false,
                false,
                Array("ID", "NAME")
            );
            while($arEl = $rsEl->Fetch()) {
                $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
            }
        break;
        case "L":
            $rsEnumProp = CIBlockProperty::GetPropertyEnum(
                $arProp["ID"],
                Array("SORT"=>"asc","NAME"=>"ASC"),
                Array("IBLOCK_ID" => $arRestIB["ID"])
            );
            while($arEnumProp = $rsEnumProp->Fetch()) {
                if($arProp["MULTIPLE"] == "Y")
                    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["VALUE"][$arEnumProp["ID"]] = $arEnumProp["ID"];
                else
                    $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["VALUE"] = $arEnumProp["ID"];
            }
        break;
        case "S":
            $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["VALUE"] = "";
        break;
        case "F":
            $arTmpRestProps["PROPERTIES"][$arProp["CODE"]]["VALUE"] = "";
        break;
    }
}


//v_dump(array_merge(Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "CODE", "NAME","PREVIEW_PICTURE","DETAIL_PICTURE", "DETAIL_TEXT","TAGS","DETAIL_PAGE_URL"), $arPropValueCode));


//Получаем список разделов в секции для выбора родительского раздела
$db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), Array('IBLOCK_ID'=>$arRestIB["ID"], 'GLOBAL_ACTIVE'=>'Y'), true);
while($ar_result = $db_list->GetNext()){
	$arResult["SECTIONS"][$ar_result["ID"]]=$ar_result["NAME"];
}

$rsRests = CIBlockElement::GetList(
    Array("SORT"=>"ASC"),
    Array(
        "IBLOCK_ID" => $arRestIB["ID"],
        "ID" => $arParams["REST_ID"],
    ),
   	false,
    false,
   	Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "CODE", "NAME","PREVIEW_PICTURE","DETAIL_PICTURE", "DETAIL_TEXT","TAGS","DETAIL_PAGE_URL","PROPERTY_*")
);

//var_dump($arTmpRestProps["PROPERTIES"]);


while($arRests = $rsRests->GetNext()) {
    $arTmpRest = Array();    
    $db_old_groups = CIBlockElement::GetElementGroups($arRests["ID"], false);
	while($ar_group = $db_old_groups->Fetch())
		$GR[]=$ar_group["ID"];
		
	
    $arRests["IBLOCK_SECTION_ID"] = $GR;
    $arTmpRest["ID"] = $arRests["ID"];
    $arTmpRest["IBLOCK_ID"] = $arRests["IBLOCK_ID"];
    $arTmpRest["IBLOCK_SECTION_ID"] = $arRests["IBLOCK_SECTION_ID"];
    $arTmpRest["PREVIEW_PICTURE"] = $arRests["PREVIEW_PICTURE"];
    $arTmpRest["DETAIL_PICTURE"] = $arRests["DETAIL_PICTURE"];
    $arTmpRest["DETAIL_TEXT"] = $arRests["DETAIL_TEXT"];
    $arTmpRest["NAME"] = $arRests["NAME"];
    $arTmpRest["CODE"] = $arRests["CODE"];
    $arTmpRest["TAGS"] = $arRests["TAGS"];
    $arTmpRest["PROPERTIES"] = $arTmpRestProps["PROPERTIES"];
   	$arTmpRest["DETAIL_PAGE_URL"]=$arRests["DETAIL_PAGE_URL"];

    // get selected props
    foreach ($arTmpRestProps["PROPERTIES"] as $keyTmpProp=>$propTmp) {
        //v_dump($propTmp);
        //$arTmpRestPropsSel["SELECTED_PROPERTIES"] = Array();
        switch($propTmp["PROPERTY_TYPE"]) {
            case "E":
            case "L":
            case "S":
                $rsSelProp = CIBlockElement::GetProperty(
                    $arRestIB["ID"],
                    $arRests["ID"],
                    Array(),
                    Array(
                        "ACTIVE" => "Y",
                        "CODE" => $propTmp["CODE"],
                    )
                );
                while($arSelProp = $rsSelProp->Fetch()) {
                   // v_dump($arSelProp);
                    $arTmpRestPropsSel["SELECTED_PROPERTIES"][$arRests["ID"]][$arSelProp["CODE"]]["VALUE"][] = $arSelProp["VALUE"];
                    $arTmpRestPropsSel["SELECTED_PROPERTIES"][$arRests["ID"]][$arSelProp["CODE"]]["DESCRIPTION"][] = $arSelProp["DESCRIPTION"];
                }
            break;
            case "N":
                $rsSelProp = CIBlockElement::GetProperty(
                    $arRestIB["ID"],
                    $arRests["ID"],
                    Array(),
                    Array(
                        "ACTIVE" => "Y",
                        "CODE" => $propTmp["CODE"],
                    )
                );
                while($arSelProp = $rsSelProp->Fetch()) {
                    //v_dump($arSelProp);
                    $arTmpRestPropsSel["SELECTED_PROPERTIES"][$arRests["ID"]][$arSelProp["CODE"]]["VALUE"][] = $arSelProp["VALUE"];
                }
            break;
            case "F":
                $rsSelPropFile = CIBlockElement::GetProperty(
                    $arRestIB["ID"],
                    $arRests["ID"],
                    Array(),
                    Array(
                        "ACTIVE" => "Y",
                        "CODE" => $propTmp["CODE"],
                    )
                );
                while($arSelPropFile = $rsSelPropFile->Fetch()) {
                    //v_dump($arSelPropFile);
                    $arTmpRestPropsSel["SELECTED_PROPERTIES"][$arRests["ID"]][$arSelPropFile["CODE"]]["VALUE"][]=array($arSelPropFile["VALUE"],$arSelPropFile["DESCRIPTION"]);
                    //$arTmpRestPropsSel["SELECTED_PROPERTIES"][$arRests["ID"]][$arSelPropFile["ID"]]["VALUE"][$arSelPropFile["PROPERTY_VALUE_ID"]] =  CFile::ResizeImageGet($arSelPropFile["VALUE"], array('width'=>130, 'height'=>130), BX_RESIZE_IMAGE_EXACT, true);
                }
            break;
        }

    }
    $arTmpRest["SELECTED_PROPERTIES"] = $arTmpRestPropsSel["SELECTED_PROPERTIES"];
    //v_dump($arTmpRest["SELECTED_PROPERTIES"]);

    $arResult["RESTAURANTS"][] = $arTmpRest;
}



$this->IncludeComponentTemplate();
?>