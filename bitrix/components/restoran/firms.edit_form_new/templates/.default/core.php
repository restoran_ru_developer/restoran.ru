<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_rest(){
    
        v_dump($_REQUEST);
        exit;
	global $USER;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
		
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $_REQUEST["IBLOCK_SECTION_ID"],          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"NAME"           => $_REQUEST["NAME"],
  		"ACTIVE"         => "Y",            
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"TAGS"           => $_REQUEST["TAGS"],
  	);
	
	//"CODE"=>translitIt($_REQUEST["NAME"])
	//Основная фотка
	if(isset($_FILES["DETAIL_PICTURE"])){
		
		$arLoadProductArray["DETAIL_PICTURE"] = $_FILES["DETAIL_PICTURE"];
		/*
    	$DF = CFile::SaveFile($FAR, "restorans");
		$MF = CFile::CopyFile($DF);
	
		$arNewFile = CIBlock::ResizePicture(CFile::MakeFileArray($DF), array("WIDTH" => 392,"HEIGHT" => 260,"METHOD" => "resample", "COMPRESSION"=>95));
		$arLoadProductArray["PREVIEW_PICTURE"] = $arNewFile;
		
		$arLoadProductArray["DETAIL_PICTURE"]=$MF;
		*/
		
	}
	
	
		
	
	//var_dump($_REQUEST["PROP"]);
	if(!isset($_REQUEST["PROP"]["photos"])) $_REQUEST["PROP"]["photos"][]="";
	if(!isset($_REQUEST["PROP"]["google_photo"])) $_REQUEST["PROP"]["google_photo"][]="";
	if(!isset($_REQUEST["PROP"]["videopanoramy"])) $_REQUEST["PROP"]["videopanoramy"][]="";
	
	$COMs = $_REQUEST["PROP"]["videopanoramy_radio"];
	
	$COMs_ph = $_REQUEST["PROP"]["photos_comment"];
	
	
	//var_dump($COMs);
	foreach($_REQUEST["PROP"] as $CODE=>$PR){
		if($CODE=="photos"){
			$FL=form_file_array("NEW_PHOTO");
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"firms");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"firms");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
			$l=0;	
			foreach($PR as $fid) {
				$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$COMs_ph[$l]);
				$l++;
			}
			$PR=$PR2;
		}
		
		if($CODE=="google_photo"){
			$FL=form_file_array("GGL");
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"firms");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"firms");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
				
			foreach($PR as $fid) {$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>"");
			$PR=$PR2;
			}
		}
		
		
		
		
		if($CODE=="videopanoramy"){
			$FL=form_file_array("PANORAMS");
			//var_dump($_FILES);
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"firms");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"firms");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
			$l=0;
			//var_dump($PR);
			foreach($PR as $fid){
				$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$COMs[$l]);
				$l++;
			} 
			$PR=$PR2;
			
			
		}
		
		if($CODE=="information"){
			$PR=array("VALUE"=>array('TYPE'=>'HTML', 'TEXT'=>$PR));
			//var_dump($PR);
		}
		
		//var_dump($CODE);
		//
		
		CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array($CODE => $PR));
	}	

	//Проверяем права на запись
	if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");
	
	
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию	
	
		unset($arLoadProductArray["IBLOCK_ID"]);
		//var_dump($arLoadProductArray);
		if($el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray, false, false,true)) echo "ok";
		
		//переиндексация
		CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
	}else{
		//создаем новую публикацию
		
		
		
		if($ELEMENT_ID = $el->Add($arLoadProductArray))
  			echo $ELEMENT_ID;
		else
  			echo "Error: ".$el->LAST_ERROR;
	}
	
}

function edit_rest_main(){
    
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
        if ($_REQUEST["IBLOCK_SECTION_ID"][0])
            $sec = $_REQUEST["IBLOCK_SECTION_ID"][0];
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $sec,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"NAME"           => $_REQUEST["NAME"],
  		"ACTIVE"         => "Y",            
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"TAGS"           => $_REQUEST["TAGS"],
  	);	
	//Основная фотка
	if(isset($_FILES["PREVIEW_PICTURE"])){		
		$arLoadProductArray["PREVIEW_PICTURE"] = $_FILES["PREVIEW_PICTURE"];		
                //if ($_REQUEST["DETAIL_PICTURE_OLD"])
                  //  CFile::Delete($_REQUEST["DETAIL_PICTURE_OLD"]);
	}        

	//Проверяем права на запись
	if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");		
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию		
		unset($arLoadProductArray["IBLOCK_ID"]);
		//var_dump($arLoadProductArray);
		if($el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray, false, false,true)) 
                {
                        CIBlockElement::SetElementSection($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_SECTION_ID"]);
                        LocalRedirect("/businessman/firm_edit.php?ID=".$_REQUEST["ELEMENT_ID"]);
                }
		//переиндексация
		CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
	}	
}

function edit_rest_contacts(){
    
	global $USER;
$arParams["PROPERTIES"] = Array(
    "subway","information","site", "phone","email", "adres", "opening_hours");

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
        $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
        $id = (int)$_REQUEST["ELEMENT_ID"];  
        $ar2 = array();
        $ar = array();
	foreach ($_REQUEST["PROP"] as $code=>$prop)
        {
            $PR2 = array();            
            if ($code=="opening_hours")
            {                
                foreach ($prop as $key=>$P)
                {          
                    $days = "";
                    $val = "";
                    $day = array();
                    if ($_REQUEST["pn"][$key]==1)
                        $day[] = "пн";
                    if ($_REQUEST["vt"][$key]==1)
                        $day[] = "вт";
                    if ($_REQUEST["sr"][$key]==1)
                        $day[] = "ср";
                    if ($_REQUEST["ch"][$key]==1)
                        $day[] = "чт";
                    if ($_REQUEST["pt"][$key]==1)
                        $day[] = "пт";
                    if ($_REQUEST["sb"][$key]==1)
                        $day[] = "сб";
                    if ($_REQUEST["vs"][$key]==1)
                        $day[] = "вс";
                    $days = implode(",",$day);
                    if ($days=="пн,вт,ср,чт,пт,сб,вс")
                        $val = "пн-вс";
                    elseif ($days=="пн,вт,ср,чт,пт,сб")
                        $val = "пн-сб";
                    elseif ($days=="пн,вт,ср,чт,пт")
                        $val = "пн-пт";
                    elseif ($days=="пн,вт,ср,чт")
                        $val = "пн-чт";
                    elseif ($days=="пн,вт,ср")
                        $val = "пн-cр";
                    else
                        $val = $days;
                    if ($val=="пн-вс"&&!$P)
                        $val = "Круглосуточно";
                    elseif ($val||$P)
                        $val = $val." ".$P."-".$_REQUEST["opening_hours2"][$key];
                    $PR2[] = $val;                    
                }                
                $prop = $PR2;                
            }
            if($code=="information")
                $prop=array("VALUE"=>array('TYPE'=>'HTML', 'TEXT'=>$prop));               
            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($code => $prop));
            $ar2[] = $code;
        }        
        $ar = array_diff($arParams["PROPERTIES"],$ar2);
        foreach ($ar as $p)
        {
            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($p => ""));
        }  
        CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);		        
        LocalRedirect("/businessman/firm_edit.php?ID=".$_REQUEST["ELEMENT_ID"]."#contacts.php?ID=".$_REQUEST["ELEMENT_ID"]."&".bitrix_sessid_get());        
}

function edit_rest_photos(){
    
	global $USER;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
        $iblock_id = (int)$_REQUEST["IBLOCK_ID"];
        $id = (int)$_REQUEST["ELEMENT_ID"];        
	foreach ($_REQUEST["PROP"] as $code=>$prop)
        {
            if ($code=="photos")
            {    
                $photos = array();
                $db_props = CIBlockElement::GetProperty($iblock_id, $id, array(), Array("CODE"=>"photos"));
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                        $photos[] = $ar_props["VALUE"];
                }                
                foreach ($prop["fid_new"] as $key=>$P)
                {
                    $PR2[] = array("VALUE"=>CFile::MakeFileArray($P), "DESCRIPTION"=>$prop["descr"][$key]);
                }
                $prop = $PR2;                
            }            
            CIBlockElement::SetPropertyValuesEx($id, $iblock_id, array($code => $prop));
            if ($code=="photos")
            {  
                foreach ($photos as $photo)
                    CFile::Delete($photo);
            }

        }
        CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);		
        LocalRedirect("/businessman/firm_edit.php?ID=".$_REQUEST["ELEMENT_ID"]."#photo.php?ID=".$_REQUEST["ELEMENT_ID"]."&".bitrix_sessid_get());        
}
/***********ФОРМИРУЕМ МАССИВ ФАЙЛА**************/
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($BL_NAME);
	//var_dump($_FILES);
	
	foreach($_FILES[$BL_NAME] as $A=>$C){
		if(is_array($C)){
			foreach($C as $index=>$V){
				$FAR[$index][$A]=$V;
			}
		}
	}
	return $FAR;
}



function save_dots(){
	global $DB;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	//var_dump($_REQUEST["pts"]);
//	$D = explode(",", $_REQUEST["pts"]);
//	$_REQUEST["pts"]=$D[0].",".$D[1];
	foreach($_REQUEST["pts"] as $D){
		$PTS[]=$D[0].",".$D[1];
		
		$LON[]=$D[1];
		$LAT[]=$D[0];
	}
	
	//var_dump($PTS);
	//$DB->StartUsingMasterOnly();

	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("map" => $PTS));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("lat" => $LAT));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("lon" => $LON));
	
	//$DB->StopUsingMasterOnly();
}



function add_new_rest(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	global $USER;
	
	$arRestIB = getArIblock("catalog", CITY_ID);

	$el = new CIBlockElement;

	$PROP = array();
	$PROP["user_bind"] = $USER->GetID();
	
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $arRestIB["ID"],
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => "Новый ресторан",
  		"ACTIVE"         => "N",            // активен
  	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray))
  		echo "ok";
	else
  		echo "Error: ".$el->LAST_ERROR;
}
/*
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($_FILES);
	foreach($_FILES["block"] as $A=>$C){		
		$VAL="";
		if(is_array($C[$BL_NAME])){
			foreach($C[$BL_NAME] as $V) $VAL=$V;
		}
		$FAR[$A]=$V;
	}
	
	if(is_array($FAR["name"])){
		$RET=array();
		foreach($FAR as $N=>$V){
			for($i=0;$i<count($V);$i++){
				$RET[$i][$N]=$V[$i];
			}
		}
		//var_dump($RET);
		return $RET;
	}else return $FAR;
}
*/

if($_REQUEST["act"]=="edit_rest") edit_rest();
if($_REQUEST["act"]=="edit_rest_main") edit_rest_main();
if($_REQUEST["act"]=="edit_rest_contacts") edit_rest_contacts();
if($_REQUEST["act"]=="edit_rest_props") edit_rest_props();
if($_REQUEST["act"]=="edit_rest_photos") edit_rest_photos();
if($_REQUEST["act"]=="save_dots") save_dots();
if($_REQUEST["act"]=="add_new_rest") add_new_rest();

?>
