<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER["DOCUMENT_ROOT"].'/tpl/ajax/qqFileUploader.php';

$uploader = new qqFileUploader();

// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
$uploader->allowedExtensions = array("jpeg","jpg","gif","png","bmp");

// Specify max file size in bytes.
$uploader->sizeLimit = 10 * 1024 * 1024;

// Specify the input name set in the javascript.
$uploader->inputName = 'qqfile';

// If you want to use resume feature for uploader, specify the folder to save parts.
//$uploader->chunksFolder = 'chunks';

// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
$result = $uploader->handleUpload($_SERVER["DOCUMENT_ROOT"].'/images/_tmp/');

// To save the upload with a specified name, set the second parameter.
// $result = $uploader->handleUpload('uploads/', md5(mt_rand()).'_'.$uploader->getName());

// To return a name used for uploaded file you can use the following line.
$result['uploadName'] = $uploader->getUploadName();

$DIR=$_SERVER["DOCUMENT_ROOT"].'/images/_tmp';
$new_name = translateFilename(trim($result['uploadName']));

$temp = explode(".",$new_name);        
$rasch = $temp[count($temp)-1];
unset($temp[count($temp)-1]);
$nn = implode("",$temp);
$new_file_name = $nn.substr(md5(mt_rand()), 0, 3).".".$rasch;
$s = $DIR.'/'.$new_file_name;
CFile::ResizeImageFile($DIR.'/'.$result['uploadName'], $s, Array("width"=>1024,"height"=>768),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,array(),90,false);
//unlink($DIR.'/'.$new_name);
$nFile = CFile::MakeFileArray('/images/_tmp/'.$new_file_name);
$nFile["MODULE_ID"] = "iblock";
//
$fid = CFile::SaveFile($nFile,"iblock");
$result["id"] = $fid;
$result["resized"] = CFile::GetPath($fid);
unlink($s);
unlink($DIR."/".trim($result['uploadName']));

header("Content-Type: text/plain");
echo json_encode($result);
?>