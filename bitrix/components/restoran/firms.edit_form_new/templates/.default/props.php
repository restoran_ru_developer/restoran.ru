<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script>
        //$(".chzn-select2").chosen();       
        
</script>
<?
if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
    $arParams["PROPERTIES"] = Array(
        "INFORMATION");
    $arParams["REQUIRED_PROPERTIES"] = Array("TYPE");

    CModule::IncludeModule("iblock");    
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):
        foreach ($arParams["PROPERTIES"] as $PROP):
            $val = array();
            $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>$PROP));
            while($ar_props = $db_props->Fetch())
            {                                         
                $val[] =  $ar_props["VALUE"];
                $desc[] = $ar_props["DESCRIPTION"];
                $property[$PROP] = $ar_props;                
                unset($property[$PROP]["VALUE"]);
                unset($property[$PROP]["DESCRIPTION"]);
            }
            $property[$PROP]["VALUES"] = $val;    
            $property[$PROP]["DESCRIPTION"] = $desc;                
            switch($property[$PROP]["PROPERTY_TYPE"]) {
                case "E":
                    $rsEl = CIBlockElement::GetList(
                        Array("SORT"=>"ASC","NAME"=>"ASC"),
                        Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $property[$PROP]["LINK_IBLOCK_ID"],
                        ),
                        false,
                        false,
                        Array("ID", "NAME")
                    );
                    while($arEl = $rsEl->Fetch()) {
                        $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEl["ID"]] = $arEl["NAME"];
                    }
                break;
                case "L":
                    $rsEnumProp = CIBlockProperty::GetPropertyEnum(
                        $property[$PROP]["ID"],
                        Array("SORT"=>"asc","NAME"=>"ASC"),
                        Array("IBLOCK_ID" => $arRests["IBLOCK_ID"])
                    );
                    while($arEnumProp = $rsEnumProp->Fetch()) {
                        if($arProp["MULTIPLE"] == "Y")
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"][$arEnumProp["ID"]] = $arEnumProp["ID"];
                        else
                            $arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = $arEnumProp["ID"];                        
                    }                    
                break;
                case "S":
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                break;
                case "F":
                    //$arTmpRestProps["PROPERTIES"][$PROP]["VALUE"] = "";
                break;
        }
        endforeach;       
?>        
        <form class="my_rest" action="/bitrix/components/restoran/restoran.firms_form_new/templates/.default/core.php" method="post" name="rest_edit">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_props" />
            <ul style="padding-top:15px;">
            <?foreach($property as $keyProp=>$prop)://v_dump($rest["SELECTED_PROPERTIES"][$keyProp]);?>                
                <?
                $code_up = strtoupper($prop["CODE"]);
                //if(in_array(strtoupper($prop["CODE"]), $arParams["HIDDEN_PROPERTIES"])) continue;
                //if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && in_array(strtoupper($prop["CODE"]), $arParams["ADMIN_ONLY"])) continue;
                ?>
                <?                                
                switch($prop["PROPERTY_TYPE"]):
                    case "E":
                ?>
                    <li class="item-row">
                        <strong><?=$prop["NAME"]?>:</strong>
                        <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                <?
                                $m=1;
                                $NK=0;	 
                                ?>                               
                                <?foreach($prop["VALUES"] as $Sel){?>	
                                            <?if($m>1){?>
                                                    </li><li class="item-row"><strong></strong>
                                            <?}?>                                                       
                                            <p class="inpwrap">
                                                    <select prop="<?=$prop["CODE"]?>"  multiple data-placeholder="Выберите вариант" class="chzn-select2 <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";?>">
                                                            <?foreach($arTmpRestProps["PROPERTIES"][$code_up]["VALUE"] as $keyRestType=>$restType):?>    
                                                                <option value="<?=$keyRestType?>"<? if($keyRestType==$Sel) echo  "selected='selected'"?>><?=$restType?></option>
                                                            <?endforeach?>
                                                    </select>
                                            </p>

                                        <?
                                        $m++;
                                        ?>
                                        <?}?>                                
                    </li>
                    <?if($prop["MULTIPLE"] == "Y"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                    <?endif?>
                    <?
                    break;
                    case "L":
                    ?>
                        <li class="item-row">
                            <strong><?=$prop["NAME"]?>:</strong>
                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                            <p>                          
                                <?if($prop["LIST_TYPE"] == "C" && $prop["MULTIPLE"] == "N"):?>   
                                    <span class="niceCheck">
                                        <input type="checkbox" class="checkbox" name="PROP[<?=$prop["CODE"]?>]"<?if($prop["VALUES"][0] == $arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]) echo  "checked='checked'"?> value="<?=$arTmpRestProps["PROPERTIES"][$code_up]["VALUE"]?>" /><br/>
                                    </span>
                                <?endif?>
                            </p>
                        </li>
                    <?
                    break;
                    case "S":                        
                    ?>
                        <li class="item-row"><?//v_dump($prop)?>
                            <strong><?=$prop["NAME"]?>:</strong>
                                <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                <?if ($prop["CODE"]!="information"):?>
                                    <?
                                    $selPropCnt = 0;
                                    foreach($prop["VALUES"] as $selPropKey=>$selPropVal):?>
                                        <?if($selPropCnt >= 1):?>
                                            <strong></strong>
                                        <?endif?>
                                        <p class="inpwrap"><?if($prop["MULTIPLE"] == "Y"):?><?//=($selPropKey+1)?><?endif;?>
                                            <input type="text" class="text <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" />
                                        </p>
                                    <?
                                    $selPropCnt++;
                                    endforeach?>                            
                                <?else:?>
                                    <p class="inpwrap" style="float:left;width:811px;">
                                        <textarea name="PROP[<?=$prop["CODE"]?>]" id="add_text" style="height:200px;" class=""><?=$prop["VALUES"][0]["TEXT"]?></textarea>        
                                    </p>
                                <?endif;?>
                        </li>
                        <?if($prop["MULTIPLE"] == "Y"):?>
                            <li class="item-row">
                                <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                            </li>
                        <?endif?>
                    <?
                    break;?>                    
                <?endswitch?>
            <?endforeach?>   
            </ul>
            <div align="right">
                <input type="submit" class="light_button" value="Сохранить свойства" />
            </div>
        </form>

<?        
    endif;
}
?>
<script>
$("#add_text").redactor({ 
                path: '/components/restoran/firms.edit_form_new/templates/.default/jq_redactor', 
                buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|','link'], 
                css: 'redactor.css', 
                lang: 'ru',
                focus: false,
                //fileUpload: '<?//=$templateFolder?>/file_upload.php',
                autoresize: false
        }); 
        jQuery(".niceCheck").mousedown(
		function() {
			changeCheck(jQuery(this));
		});

	jQuery(".niceCheck").each(
		function() {
			changeCheckStart(jQuery(this));
		});
                $(".chzn-select2").chosen();
    $(document).ready(function() {  
        $(".my_rest").on('change',".chzn-select2",function(){            
            var v = $(this).val();
            /*if (v)
            {
                if ($(this).attr("prop")=="subway"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений");
                if ($(this).attr("prop")=="out_city"&&v.length>1)
                    alert("Внимание! Вы выбрали более 1 значения")
                if ($(this).attr("prop")=="average_bill"&&v.length>1)
                    alert("Внимание! Вы выбрали более 1 значения")
                if ($(this).attr("prop")=="kitchen"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений")
                if ($(this).attr("prop")=="type"&&v.length>3)
                    alert("Внимание! Вы выбрали более 3 значений")
            }*/
        });
    });
</script>