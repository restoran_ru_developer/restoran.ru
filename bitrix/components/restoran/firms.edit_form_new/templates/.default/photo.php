<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>   
<script>
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();      
    $("#sortable").on("click",".del_pho",function(){
       $(this).parents("li").remove();
       return false;
    });
</script>
<?
if(check_bitrix_sessid() && (int)$_REQUEST["ID"]) {
    //$arParams["PROPERTIES"] = Array("PHOTOS","VIDEOPANORAMY");
    $arParams["PROPERTIES"] = Array("PHOTOS");
    
    CModule::IncludeModule("iblock");    
    $rsRest = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($arRests = $rsRest->Fetch()):
        foreach ($arParams["PROPERTIES"] as $PROP):
            $val = array();
            $db_props = CIBlockElement::GetProperty($arRests["IBLOCK_ID"], $arRests["ID"], array("sort" => "asc"), Array("CODE"=>$PROP));
            while($ar_props = $db_props->Fetch())
            {                        
                if ($ar_props["VALUE"])
                {
                    $val[] =  $ar_props["VALUE"];
                    $desc[] = $ar_props["DESCRIPTION"];
                    
                }
                    $property[$PROP] = $ar_props;                
                    unset($property[$PROP]["VALUE"]);
                    unset($property[$PROP]["DESCRIPTION"]);                
            }
            $property[$PROP]["VALUES"] = $val;    
            $property[$PROP]["DESCRIPTION"] = $desc; 
        endforeach;       
?>        
        <form class="my_rest" action="/bitrix/components/restoran/firms.edit_form_new/templates/.default/core.php" method="post" name="rest_edit">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ELEMENT_ID" value="<?=$arRests["ID"]?>" />
            <input type="hidden" name="IBLOCK_ID" value="<?=$arRests["IBLOCK_ID"]?>" />
            <input type="hidden" name="act" value="edit_rest_photos" />
            <ul style="padding-top:15px;">
            <?
            foreach($property as $keyProp=>$prop):
                $code_up = strtoupper($prop["CODE"]);
                switch($prop["PROPERTY_TYPE"]):
                    case "F":?>
                        <h2><?=$prop["NAME"]?></h2>
                        <ul class="upl-imgs bl<?=$code_up?>" id="sortable">                            
                            <? $i=0;?>
                            <? foreach($prop["VALUES"] as $key=>$VALUE){?>
                                <? $file = CFile::ResizeImageGet($VALUE, array('width'=>212, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);  ?>
                                <li class="ui-state-default">
                                        <a href="#" class="del_pho" title="Удалить фото"></a>
                                        <img src="<?=$file["src"]?>"><br />
                                        <input style="width:194px;" type="text" name="PROP[<?=$prop["CODE"]?>][descr][]" <? if($prop["DESCRIPTION"][$key]=="" || $prop["DESCRIPTION"][$key]=="Добавьте описание"){ $prop["DESCRIPTION"][$key]="";?> <?}?> placeholder="Добавьте описание" value="<?=$prop["DESCRIPTION"][$key]?>" />                                                
                                        <input type="hidden" name="PROP[<?=$prop["CODE"]?>][fid_new][]" value="<?=$VALUE?>" />
                                </li>
                                <? $i++;?>
                            <?}?>
                            <div class="clear"></div>
                        </ul>
                        <Br /><h4>Добавить фотографии</h4><br />
                        <div class="content_block ph_gals">	                                
                                <div id="imu_content_<?=$code_up?>" class="imu_content">
                                        <div class="upl-description">
                                                <p align="center">
                                                    <ul id="file_field_<?=$prop["CODE"]?>" class="unstyled" style="width:120px; margin:0 auto;"></ul>
                                                </p>
                                        </div>
                                </div>											
                        </div>
                        <script>
                            $(document).ready(function(){
                                var count = 0;
                                var errorHandler = function(event, id, fileName, reason) {
                                qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
                                };
                                $('#file_field_<?=$prop["CODE"]?>').fineUploader({
                                    text: {
                                        uploadButton: "Обзор",
                                        cancelButton: "",
                                        waitingForResponse: "",
                                    },
                                    messages: {
                                        tooManyItemsError: "Слишком много файлов ({netItems}) загружается.  Превышен лимит в {itemLimit} файлов.",
                                        typeError: "Неверное расширение файла {file}. Вы можете загрузить только файлы с расширениями: {extensions}.",
                                        sizeError: "Файл {file} слишком большой, максимальный размер загружаемого файла - {sizeLimit}.",
                                        minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                                        emptyError: "Файл {file} - пуст, выберите файлы заново, не включая его.",
                                        noFilesError: "Не выбраны файлы для загрузки",
                                        //retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                                        onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."                                        
                                    },                                    
                                    multiple: true,
                                    request: {
                                        endpoint: "/bitrix/components/restoran/restoran.edit_form_new/templates/.default/upload.php",
                                        params: {"generateError": true}
                                    },          
                                    validation:{
                                        allowedExtensions : ["jpeg","jpg","bmp","gif","png"],    
                                        itemLimit:<?=(CSite::InGroup(Array(1,15,16)))?"999":"15"?>
                                    }
                                    /*failedUploadTextDisplay: {
                                        mode: 'custom',
                                        maxChars: 5
                                    }*/
                                })
                                .on('error', errorHandler)
                                .on('validate',function(){
                                    
                                })
                                .on('upload', function(id, fileName){                                    
                                    $(".qq-upload-list").show();     
                                    count++;
                                })
                                .on('complete', function(event, id, fileName, response) {
                                    $('<li class="ui-state-default"><a href="#" class="del_pho"  title="Удалить фото"></a><img width="212" src="'+response.resized+'"><br /> <input style="width:194px;" type="text" name="PROP[<?=$prop["CODE"]?>][descr][]" placeholder="Добавьте описание" /><div class="progress" rel="0"></div><input value="'+response.id+'" type="hidden" name="PROP[<?=$prop["CODE"]?>][fid_new][]" /></li>').insertBefore($(".bl<?=$code_up?> .clear"));
                                    $(".qq-upload-list").hide();
                                }); 
                            });

                        </script>
                    <?break;
                endswitch;
            endforeach;   
            ?>
            </ul>
            <br />
            <div align="right">
                <input type="submit" class="light_button" value="Сохранить фото" />
            </div>
        </form>

<?        
    endif;
}
?>	