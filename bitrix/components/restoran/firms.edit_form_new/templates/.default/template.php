<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;

?>
<div class="wrap-div">

    <div>
        <ul class="tabs big" ajax="ajax" history="true" ajax_url="<?=$templateFolder?>/">
            <li>
                <a href="main.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>" class="current">
                    <div class="left tab_left"></div>
                    <div class="left name">Основые данные</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="contacts.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name">Контакты</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>            
            <li>
                <a href="photo.php?ID=<?=$arParams["REST_ID"]?>&<?=bitrix_sessid_get()?>">
                    <div class="left tab_left"></div>
                    <div class="left name">Фотогалерея</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
        <div class="panes">
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
            <div class="pane big"></div>
        </div>        
        <a target="_blank" href="/restorator/firm_preview.php?ID=<?=$arParams["REST_ID"]?>&IBLOCK_ID=<?=$arResult["RESTAURANTS"][0]["IBLOCK_ID"]?>" class="light_button">Предпросмотр</a>
    </div>    
</div>
<script type="text/javascript" src="<?=$templateFolder?>/jq_redactor/redactor.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/uploaderObject.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.form.js"></script>