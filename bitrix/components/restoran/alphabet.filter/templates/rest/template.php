<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$abv = Array("А","Б","В","Г","Д","Е","Ё","Ж","З","И","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Э","Ю","Я");
$abc = Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
?>
<?$url = "/".CITY_ID."/catalog/".$_REQUEST["CATALOG_ID"]."/all/"?>
<ul class="alphabet" <?=(LANGUAGE_ID=="en")?"style='width:600px; margin:0 auto;'":""?>>
    <li><a class="white font14 <?=($_REQUEST["letter"]=="09")?"selected":""?>" href='<?=$url."?letter=09"?>'>0-9</a></li>
    <?if (LANGUAGE_ID!="en"):?>
        <?foreach($abv as $a):?>
            <li><a class="white font14 <?=($_REQUEST["letter"]==$a)?"selected":""?>" href='<?=$url."?letter=".ord(iconv("utf-8","windows-1251",$a))?>'><?=$a?></a></li>
        <?endforeach;?>    
        <li style="margin: 0px; height: 24px; width: 20px; background: url(/tpl/images/filter_border.png) center center no-repeat;">&nbsp;</li>
    <?endif;?>
    <?foreach($abc as $b):?>
        <li><a class="white font14 <?=($_REQUEST["letter"]==$b)?"selected":""?>" href='<?=$url."?letter=".ord($b)?>'><?=$b?></a></li>
    <?endforeach;?>
</ul>
<?
$letter = trim($_REQUEST["letter"]);
if ($letter)
{
    global $arrFilter;
    if ($letter=="09")
    {
        $arrFilter[">=NAME"] = "0";
        $arrFilter["<=NAME"] = "9";
    }
    else
        $arrFilter["NAME"] = iconv("windows-1251","utf-8",chr($letter))."%";
}
?>