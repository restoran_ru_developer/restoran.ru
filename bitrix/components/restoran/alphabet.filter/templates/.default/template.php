<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$abv = Array("А","Б","В","Г","Д","Е","Ё","Ж","З","И","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Э","Ю","Я");
$abc = Array("#","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
?>
<ul class="alphabet">
   <?foreach($abv as $a):?>
        <li><a class="white font14 <?=($_REQUEST["letter"]==$a)?"selected":""?>" href='<?=$APPLICATION->GetCurPageParam("letter=".$a,array("letter"))?>'><?=$a?></a></li>
    <?endforeach;?>
        <li style="width:79px;">&nbsp;</li>
    <?foreach($abc as $b):?>
        <li><a class="white font14 <?=($_REQUEST["letter"]==$b)?"selected":""?>" href='<?=$APPLICATION->GetCurPageParam("letter=".$b,array("letter"))?>'><?=$b?></a></li>
    <?endforeach;?>
</ul>
<?
$letter = trim($_REQUEST["letter"]);
if ($letter)
{
    global $arrFilterFirm;
    $arrFilterFirm["NAME"] = $letter."%";
}
?>