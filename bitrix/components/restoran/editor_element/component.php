<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

//создаем новую секцию и редиректим на редактирование
if($arParams['NEW_ARTICLE']=="Y"){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	$bs = new CIBlockSection;
	$arFields = Array(
  		"ACTIVE" => "N",
  		"IBLOCK_SECTION_ID" => $arParams["DEFAULT_SECTION"],
  		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
  		"NAME" => "Название публикации"
  	);
  	$ID = $bs->Add($arFields);
  	 
  	LocalRedirect($APPLICATION->GetCurPage()."?SECTION_ID=".$ID);
  	
}



if($this->StartResultCache(false, array($arNavigation, $arParams["SECTION_ID"])))
{
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	
	$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$arParams["PARENT_SECTION"]);
  	$db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), $arFilter, true);
	while($ar_result = $db_list->GetNext()){
    	$arResult["IBLOCK"]["SECTIONS"][]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"]);
  	}
  
	//var_dump($arResult["IBLOCK"]["SECTIONS"]);
	
	//получаем инфу по секции
	$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], "ID"=>intval($arParams["SECTION_ID"]));
    $res = CIBlockSection::GetList(array(), $arFilter, false, Array("UF_*"));	
	if($ar_res = $res->GetNext()){
  		$arResult["SECTION"] = $ar_res;
  		
  		//Формируем свойства секции
  		foreach($arResult["SECTION"] as $KEY=>$VAL){
  			if(substr_count($KEY, "UF_")>0 && substr_count($KEY, "~")==0){
  				
  				$arUF = GetUserField("IBLOCK_".$arParams['IBLOCK_ID']."_SECTION", $arParams["SECTION_ID"], $KEY);
  				//Привязка к инфоблоку
  				if($arUF["USER_TYPE"]["USER_TYPE_ID"]=="iblock_element"){
  					
  					$arFilter_uf = Array("IBLOCK_ID"=>$arUF["SETTINGS"]["IBLOCK_ID"], "ACTIVE"=>"Y");
					$res_uf = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_uf,false);
					
					while($ar_fields_uf = $res_uf->GetNext()){
  						$arUF["VALUES"][]=array("ID"=>$ar_fields_uf["ID"], "NAME"=>$ar_fields_uf["NAME"]);
					}
  				}
  				
  				$arResult["SECTION"][$KEY]=$arUF;
  				//v_dump($arUF);
  				
  			}
  		}
  		
  		//v_dump($arResult["SECTION"]);
  		//теперь выбираем все элементы из этой секции
  		$arFilter = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>$ar_res["IBLOCK_ID"]);
		if(intval($arParams["SECTION_ID"])>0) $arFilter["SECTION_ID"] = intval($arParams["SECTION_ID"]);
		
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false);
		while($ob = $res->GetNextElement()){
			
			$ART= array();
			$ART = $ob->GetFields();
  			$ART["PROPERTIES"] =  $ob->GetProperties();
  			
  			$arResult["ELEMENTS"][] = $ART;
  			
		}
		
		
		//Теперь нужно забрать тэги для публикаций
		if(intval($arParams["TAG_SECTION"])>0){
			$arFilter = Array("IBLOCK_ID"=>99, "ACTIVE"=>"Y", "SECTION_ID"=>$arParams["TAG_SECTION"]);
			$res = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter, false);
			while($ar_fields = $res->GetNext()){
				$arResult["TAGS"][]=$ar_fields["NAME"];
			}
		}

	}else{
		ShowError(GetMessage("CATALOG_SECTION_NOT_FOUND"));
		@define("ERROR_404", "Y");
		CHTTP::SetStatus("404 Not Found");
		return;
	}
	

	$this->SetResultCacheKeys(array(
		"ID",
		"IBLOCK_ID",
		"NAV_CACHED_DATA",
		"NAME",
		"IBLOCK_SECTION_ID",
		"IBLOCK",
		"LIST_PAGE_URL", 
		"~LIST_PAGE_URL",
		"SECTION",
		"PROPERTIES",
		"SORT"
	));

	$this->IncludeComponentTemplate();

}

?>