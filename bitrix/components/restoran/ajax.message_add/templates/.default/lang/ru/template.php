<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Спасибо, Ваше сообщение отправлено";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут писать сообщения.";
?>