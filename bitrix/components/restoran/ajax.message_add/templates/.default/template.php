<script src="<?=$templateFolder?>/script.js"></script>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
</div>
<?if ($arParams["ACTION"]=="save"):?>
    <div class="center">
        <?if ($arResult["ERROR"]):
            echo $arResult["ERROR"];
        else:
            echo GetMessage("SAVE_COMMENT");
        endif;?>
    </div>
<?else:?>
    <?if ($USER->IsAuthorized()):?>
    <form action="/bitrix/components/restoran/ajax.message_add/ajax.php?<?=bitrix_sessid_get()?>" method="post" novalidate="novalidate">
            <?if ($arParams["USER_ID"]):?>
                <div class="question">
                    Кому: <a href="/users/id<?=$arParams["USER_ID"]?>/" class="another"><?=$arParams['USER_NAME']?></a>
                </div>
            <?else:?>
                    <script>
                        $("#user_search_input").autocomplete("/search/users_suggest.php", {
                                limit: 5,
                                minChars: 3,
                                formatItem: function(data, i, n, value) {
                                    return value.split("###")[0];
                                },
                                formatResult: function(data, value) {
                                    return value.split("###")[0];
                                }
                        });
                        $("#user_search_input").result(function(event, data, formatted) {
                            if (data) {

                                $("#user_id").val(data[0].split("###")[1]);
                            }
                        });
                    </script>
                <div class="question">
                    Кому: <br />
                    <input type="text" id="user_search_input" class="inputtext" name="q" style="width:282px;" />
                    <input type="hidden" id="user_id" name="user_id" />
                </div>
            <?endif;?>
            <div class="question">
            Сообщение:<br />
            <textarea name="message" class="inputtext" style="width:94%; height:120px;"></textarea>
            <?if ($arParams["USER_ID"]):?>
                <input type="hidden" name="user_id" value="<?=$arParams["USER_ID"]?>"/>
            <?endif;?>
            </div>
            <div align="right" style="margin-top:10px;"><input type ="button" id="send_message" class="light_button" value="Отправить" /></div>
    </form>
    <?else:?>
    <div class="errortext"><?=GetMessage("ONLY_AUTHORIZED")?></div>
    <?endif;?>
<?endif;?>