$(document).ready(function(){
    $("#send_message").click(function(){
        var form = $(this).parents('form');
        //console.log(form);
        $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $("#mail_modal").html(data);
                }
        });
    });

    $('textarea[name=message]').keydown(function (e) {
		if (e.ctrlKey && e.keyCode == 13) {
   			var form = $(this).parent().parent();
        	$.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $("#mail_modal").html(data);
                }
        	});

 			return false;
 		}
	});

});
