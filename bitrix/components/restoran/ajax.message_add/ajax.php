<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if (check_bitrix_sessid()&&$_REQUEST["user_id"]):?>
    <?$APPLICATION->IncludeComponent(
            "restoran:ajax.message_add",
            ".default",
            Array(
                    "MESSAGE" => trim($_REQUEST["message"]),
                    "ACTION" => "save",
                    "USER_ID" => intval($_REQUEST["user_id"]),
            )
    );?>
<?endif;?>
<?require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>