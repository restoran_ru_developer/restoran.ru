<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arParams["USER_NAME"] = trim($_REQUEST["USER_NAME"]);
$arParams["USER_ID"] = ($arParams["USER_ID"])?intval($arParams["USER_ID"]):intval($_REQUEST["USER_ID"]);
$arParams["MESSAGE"] = trim($arParams["MESSAGE"]);
$error = array();
$arr = array();
if(!CModule::IncludeModule("socialnetwork"))
{
    $this->AbortResultCache();
    $error[] = GetMessage("IBLOCK_MODULE_NOT_INSTALLED");
}
if (!$USER->IsAuthorized())
    $error[] = GetMessage("ERROR_USER");
if ($arParams["ACTION"]=="save")
{    
    if (!$arParams["MESSAGE"])
        $error[] = GetMessage("REVIEW_ERROR");        
    if (count($error)>0)
    {
        $arResult["ERROR"] = implode("<br />",$error);
    }
    else
    {
        if (!CSocNetMessages::CreateMessage($GLOBALS["USER"]->GetID(), $arParams["USER_ID"], $arParams["MESSAGE"]))
        {
            if ($e = $GLOBALS["APPLICATION"]->GetException())
                    $error[] = $e->GetString();
        }
        else
        {
            $res = CUser::getByID($arParams["USER_ID"]);
            if ($ar = $res->Fetch())
            {
                $arEventFields = array(
                    "ID"                  => $arParams["USER_ID"],
                    "MESSAGE"             => $arParams["MESSAGE"],
                    "EMAIL_TO"            => $ar["EMAIL"],
                    "NAME"         =>   $GLOBALS["USER"]->GetFullName(),
                    "USER_NAME"           => $ar["LAST_NAME"]." ".$ar["NAME"],                    
                );
                $arrSITE =  CAdvContract::GetSiteArray($CONTRACT_ID);
                CEvent::Send("SONET_NEW_MESSAGE", SITE_ID, $arEventFields,35);
            }
        }
        if (count($error)>0)
            $arResult["ERROR"] = implode("<br />",$error);
    }   
}
$this->IncludeComponentTemplate();    
?>