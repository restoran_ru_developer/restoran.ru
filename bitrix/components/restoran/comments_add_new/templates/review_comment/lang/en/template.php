<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Thanks, Your comment has been saved.";
$MESS ["ONLY_AUTHORIZED"] = "Only authorized users can leave comments.";
$MESS ["LEAVE_COMMENT"] = "Leave comment";
$MESS ["YOUR_COMMENT"] = "Your comment";
$MESS ["R_COMMENT"] = "Comment";
$MESS ["R_COMMENT_ADD"] = "Leave comment";
?>