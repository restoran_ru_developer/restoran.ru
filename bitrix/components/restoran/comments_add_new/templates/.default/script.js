$(document).ready(function(){        
    $.tools.validator.localize("ru", {
        '*'	: 'Поле обязательно для заполнения',
    	'[req]'	: 'Поле обязательно для заполнения'
    });
    $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
        if (!value)
            return false;
        return true;
    });
    $("#reviews > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
        var form = $(this);
        if (!e.isDefaultPrevented()) {
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $("#rating_overlay").html(data);
                    //$(".error").hide();
                     $('#rating_overlay').overlay({
                        top: 260,                    
                        closeOnClick: false,
                        closeOnEsc: false,
                        api: true
                    }).load();
                    $(".add_review").html("");
                }
            });
            e.preventDefault();
        }
    });
    $("#reviews > form").bind("onFail", function(e, errors)  {
	if (e.originalEvent.type == 'submit') {
		$.each(errors, function()  {
			var input = this.input;	                        
		});
	}
    });
});
