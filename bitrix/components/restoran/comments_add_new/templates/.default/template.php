<?if ($arParams["ACTION"]=="save"):?>    
        <?
        $APPLICATION->RestartBuffer();
        if ($arResult["ERROR"]):
            
            if (substr_count($arResult["ERROR"], "Пользователь")>0 || substr_count($arResult["ERROR"], "User")>0)
                $ar["MESSAGE"] = $arResult["ERROR"]." ".GetMessage("AUTHORIZE");
            else
                $ar["MESSAGE"] = $arResult["ERROR"];
            $ar["STATUS"] = 0;
            echo json_encode($ar);
        else:
            if ($arResult["NEW_USER"])
                $ar["MESSAGE"] = GetMessage("SAVE_COMMENT")."<br />".GetMessage("NEW_USER");
            else
                $ar["MESSAGE"] = GetMessage("SAVE_COMMENT");
            $ar["STATUS"] = 1;
            echo json_encode($ar);
        endif;?>
<?else:?>
    <?if (!$USER->IsAuthorized()):?>
    <br /><Br />
    <?$APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "no_auth",
            Array(
                    "REGISTER_URL" => "",
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "",
                    "SHOW_ERRORS" => "N"
            ),
    false
    );?>
    <br /><br />
    <?endif;?>
   <script>
       $(document).ready(function(){
            $.tools.validator.localize("ru", {
                '*'	: 'Поле обязательно для заполнения',
                '[req]'	: 'Поле обязательно для заполнения'
            });
            $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
                if (!value)
                    return false;
                return true;
            });
            $("#comments > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $.ajax({
                        type: "POST",
                        //dataType: "json",
                        url: form.attr("action"),
                        data: form.serialize()+"&"+"<?=bitrix_sessid_get()?>",
                        success: function(data) {
                            /*$("#rating_overlay").html(data);
                            //$(".error").hide();
                             $('#rating_overlay').overlay({
                                top: 260,                    
                                closeOnClick: false,
                                closeOnEsc: false,
                                api: true
                            }).load();*/
                            if (!$("#comment_modal").size())
                            {
                                $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");                                                               
                            }
                            var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
                            var html2 = '</div>';
                            data = eval('('+data+')');                            
                            $('#comment_modal').html(html+data.MESSAGE+html2);
                            showOverflow();
                            setCenter($("#comment_modal"));
                            $("#comment_modal").fadeIn("300");
                            if (data.STATUS=="1")
                                setTimeout("location.reload()","3500");
                        }
                    });
                    e.preventDefault();
                }
            });
            $("#comments > form").bind("onFail", function(e, errors)  {
                if (e.originalEvent.type == 'submit') {
                        $.each(errors, function()  {
                                var input = this.input;	                        
                        });
                }
            });
    });
   </script>
   <form action="/bitrix/components/restoran/comments_add/ajax.php" method="post" novalidate="novalidate">
       <div class="grey_block" style="margin-bottom:10px">
           <?if (!$USER->IsAuthorized()):?>
                <div class="uppercase font10 ls1" style="padding-left:6px;">E-mail:</div>
                <input class="inputtext-with_border font14" value="" name="email" req="req" style="width:270px" /><br />
           <?endif?>
           <div class="uppercase font10 ls1" style="padding-left:6px;">Комментарий:</div>
           <textarea class="add_review font14" name="review" req="req" style="width:665px;"></textarea>
       </div>
       <div class="right">
        <input type="submit" class="light_button" value="Комментировать" />          
       </div>
        <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
        <input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
        <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />
        <div id="rating_overlay">
            <div class="close"></div>                           
        </div>
       <div class="clear"></div>
    </form>   
<?endif;?>