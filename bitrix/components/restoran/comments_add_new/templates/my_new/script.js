$(document).ready(function(){
    //iPAD Support
$.fn.addTouch = function(){
  this.each(function(i,el){
    $(el).bind('touchstart touchmove touchend touchcancel',function(){
      //we pass the original event object because the jQuery event
      //object is normalized to w3c specs and does not provide the TouchList
      handleTouch(event);
    });
  });
 
  var handleTouch = function(event)
  {
    var touches = event.changedTouches,
            first = touches[0],
            type = '';
 
    switch(event.type)
    {
      case 'touchstart':
        type = 'mousedown';
        break;
 
      case 'touchmove':
        type = 'mousemove';
        event.preventDefault();
        break;
 
      case 'touchend':
        type = 'mouseup';
        break;
 
      default:
        return;
    }
 
    var simulatedEvent = document.createEvent('MouseEvent');
    simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
    first.target.dispatchEvent(simulatedEvent);
  };
};
   $(".active_rating div").hover(
        function(){
            $(this).parent().find("div").attr("class","star");
            var count = $(this).attr("alt");
            var i = 1;
            $("#input_ratio").attr("value",count);
            $(this).parent().find("div").each(function(){                
                if (i<=count)
                    $(this).attr("class","star_a");
                else
                    return;
                i++;
            })            
        }
    );
        
    $(".active_rating div").click(
        function(){
            $(".plus").focus();
        }
    ); 

    /*    
    $.tools.validator.localize("ru", {
        '*'	: 'Поле обязательно для заполнения',
    	'[req]'	: 'Поле обязательно для заполнения'
    });
    $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
        if (!value)
            return false;
        return true;
    });
    $("#reviews > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
        var form = $(this);
        if (!e.isDefaultPrevented()) {
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $("#rating_overlay").html(data);
                    //$(".error").hide();
                     $('#rating_overlay').overlay({
                        top: 260,                    
                        closeOnClick: false,
                        closeOnEsc: false,
                        api: true
                    }).load();
                    $(".add_review").html("");
                }
            });
            e.preventDefault();
        }
    });
    $("#reviews > form").bind("onFail", function(e, errors)  {
	if (e.originalEvent.type == 'submit') {
		$.each(errors, function()  {
			var input = this.input;	                        
		});
	}
    });*/
});
