<script type="text/javascript" src="<?=$templateFolder?>/script.js"></script>
<br /><br /><br />
   <script>
       $(document).ready(function(){
            $.tools.validator.localize("ru", {
                '*'	: 'Поле обязательно для заполнения',
                '[req]'	: 'Поле обязательно для заполнения'
            });
            $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
                if (!value)
                    return false;
                return true;
            });
            $("#comments > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize()+"&"+"<?=bitrix_sessid_get()?>",
                        success: function(data) {
                            if (!$("#comment_modal").size())
                            {
                                $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");                                                               
                            }
                            $('#comment_modal').html(data);
                                showOverflow();
                                setCenter($("#comment_modal"));
                                $("#comment_modal").fadeIn("300"); 
                            $("#review").html("");
                            location.reload();
                        }
                    });
                    e.preventDefault();
                }
            });
            $("#comments > form").bind("onFail", function(e, errors)  {
                if (e.originalEvent.type == 'submit') {
                        $.each(errors, function()  {
                                var input = this.input;	                        
                        });
                }
            });
    });
   </script>
   <form action="/bitrix/components/restoran/comments_add/ajax.php" method="post" novalidate="novalidate">
       <div class="left">
           <textarea class="add_review" id="review" name="review" req="req" style="width:470px"></textarea>
           <input type="hidden" value="" id="input_ratio" name="ratio" />
       </div>
       <div class="right">
           <p>Ваша оценка:</p>
           <div class="active_rating">
                <?for ($i=1;$i<=5;$i++):?>
                    <div class="star" alt="<?=$i?>"></div>
                <?endfor;?>
            </div>
            <div class="clear"></div>          
            <Br />
           <div class="figure_link" style="margin-left:0px;">
               <input type="submit" value="+ Комментировать">
               <!--<a href="#">+ Добавить отзыв</a>-->
           </div>
            <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
           <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
           <input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
           <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />
       </div>
        <div id="rating_overlay">
            <div class="close"></div>                           
        </div>
       <div class="clear"></div>
    </form>
   <br />
   <div class="grey">
       <p>Дорогие друзья! Помните, что администрация сайта будет удалять:</p>
    <p>1. Комментарии с грубой и ненормативной лексикой<br />
    2. Прямые или косвенные оскорбления героя поста или читателей<br />
    3. Короткие оценочные комментарии («ужасно», «класс«, «отстой»)<Br />
    4. Комментарии, разжигающие национальную и социальную рознь<br />
    <p><a href="#">Полная версия правил Restoran.ru</a></p>
   </div>