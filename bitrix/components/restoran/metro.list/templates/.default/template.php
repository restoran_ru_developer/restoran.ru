<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="scrollPane" style=" width: 700px;height: 275px;overflow: auto;">
    <div id="pann">
        <script>
            $(document).ready(function(){
                $(".metro_list a").toggle(function(){
                    var val = $(this).attr("value");
                    $(this).addClass("active");
                    $("#metro_values").append("<input name='arrFilter_pf[subway][]' type='hidden' id='m"+val+"' value='"+val+"' name='' />");
                },
                function(){
                    var val = $(this).attr("value");
                    $(this).removeClass("active");
                    $("#m"+val).remove();
                });
                $('#metro_select').click(function(){                                
                    $(this).parent().parent().fadeOut(300);
                });  
                $('#pann').jScrollPane({scrollbarWidth:18, showArrows:true});
            });
        </script>
        <div class="white_dotted" style="margin-top:0px; margin-bottom:55px;"></div>
        <div align="right"><input class="filter_button" id="metro_select" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
        <br /><br />
        <?
        $section = "";
        $c = ceil(count($arResult["ITEMS"])/4);
        foreach ($arResult["ITEMS"] as $i=>$ar):       
        ?>
            <?if (($i>=$c&&$i%$c==0)||$i==0):?>
                <div class="left" >
                    <ul class="metro_list">
            <?endif;?>         
            <?if ($arParams["q"] == "thread"):?>
                <?if ($section!=$ar["SECTION_NAME"]):?>
                            <li class="line"><?=$ar["SECTION_NAME"]?></li>                
                <?
                $section = $ar["SECTION_NAME"];
                endif;?>
            <?endif;?>

                        <li style="background:url(<?=CFile::GetPath($ar["PICTURE"])?>) left center no-repeat"><a href="javascript:void(0)" value="<?=$ar["ID"]?>"><span><?=substr($ar["NAME"], 0, 1);?></span><?=substr($ar["NAME"], 1);?></a></li>

            <?if ($i%$c==($c-1)&&$i>0):?>
                    </ul>
                </div>
            <?endif;      
            if (end($arResult["ITEMS"])==$ar&&$i%$c!=($c-1)):?>
                    </ul>
                </div>
            <?endif;?>
        <?endforeach;?>
        <div class="clear"></div>
        <div id="metro_values"></div>
    </div>
</div>