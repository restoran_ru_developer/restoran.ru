<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$arMetroIB = getArIblock("metro", CITY_ID);
$arSelect = Array("ID", "NAME", "PROPERTY_style", "PROPERTY_eng_name");
$arFilter = Array("IBLOCK_ID" => $arMetroIB["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while ($ob = $res->GetNextElement()) {
    $stations[] = $ob->GetFields();
}
//v_dump(LANGUAGE_ID);
?>
<script>
    $(document).ready(function(){
        //alert(1);
        $('.subway_station_extended_filter').click(function(e){
            if($('.hidden-checkboxes').find('.station'+$(this).attr('station_id')).attr('checked') !== 'checked'){
                $(this).addClass('selected');
                $('.hidden-checkboxes').find('.station'+$(this).attr('station_id')).attr('checked', 'checked');
                $('.selected-station-container ul').append('<li class="fil" id="selected_station_'+$(this).attr('station_id')+'" val="'+$(this).attr('station_id')+'">'+$(this).attr('val')+'<a style="margin-top:6px;" href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png"></a></li>');
                $('.selected-station-container').show();
            } else {
                $('.hidden-checkboxes').find('.station'+$(this).attr('station_id')).removeAttr('checked');
                $(this).removeClass('selected');
                $('#selected_station_'+$(this).attr('station_id')).remove();
                if( typeof $('.selected-station-container ul li')[0] == 'undefined' ){
                    $('.selected-station-container').hide();
                }
            }
        });

        $('.selected-station-container a').live('click', function(e){
            $('.hidden-checkboxes').find('.station'+$(this).parent().attr('val')).removeAttr('checked');
            $('[station_id='+$(this).parent().attr('val')+']').removeClass('selected');
            $(this).parent().remove();
            if( typeof $('.selected-station-container ul li')[0] == 'undefined' ){
                $('.selected-station-container').hide();
            }
        });

    });
</script>
<style>
    .selected-station-container img {
        margin: 0;
    }

    .selected-station-container
    {
        background:#f5f5f5;
        height:auto;
        padding:0px 10px;
        line-height: 30px;
        display:none;
        margin-bottom: 1px;
    }
    .selected-station-container ul
    {
        margin:0px;
        padding:0px;
        display:none;
    }
    .selected-station-container ul li.end
    {
        margin:0px;
        height:30px;
        width:15px;
        background: url(/tpl/images/filter_border.png) left center no-repeat;
    }
    .selected-station-container ul li
    {
        float:left;
        padding-right: 15px;
        margin-right:5px;
        list-style-type: none;
    }
    .selected-station-container ul li a
    {
        margin-left:10px;
    }
    .selected-station-container ul li img
    {
        margin:0px;
    }
    .subway-map
    {
        position:relative;
    }

    .selected
    {
        display: block;
        position: absolute;
        z-index: 9999;
        background: #24A6CF !important;
        opacity:.6;
        -pie-background:rgba(36,166,207,0.6);
        behavior:url(/tpl/PIE.php);
    }
    #filter .filter_popup a
    {
        color:#FFF;
        font-size:12px;
        line-height:24px;
        text-decoration:none;    
        font-family: Georgia;
        display:block;
    }
    .your_choose
    {
        margin-right:15px;
        margin-left:5px; 
        font-family: Arial; 
        letter-spacing: 0px; 
        font-weight:bold; 
        line-height:30px; 
        text-transform: uppercase;
    }
</style>

<div class="sort_metro">
    <? if ($arParams["q"] != "abc"): ?>
        <a class="another" href="<?= $this->__component->__path ?>/ajax.php?q=abc&template=checkbox&<?= bitrix_sessid_get() ?>" onclick="$(this).parent().parent().load($(this).attr('href'));return false"><?= GetMessage("ABC") ?></a>  
    <? else: ?>
        <?= GetMessage("ABC") ?> 
    <? endif; ?>
    / 
    <? if ($arParams["q"] != "thread"): ?>
        <a class="another" href="<?= $this->__component->__path ?>/ajax.php?q=thread&template=checkbox&<?= bitrix_sessid_get() ?>" onclick="$(this).parent().parent().load($(this).attr('href'));return false"><?= GetMessage("LINES") ?></a> 
    <? else: ?>
        <?= GetMessage("LINES") ?>
    <? endif; ?>     
    / 
    <? if ($arParams["q"] != "shema"): ?>
        <a class="another" href="<?= $this->__component->__path ?>/ajax.php?q=shema&template=shema&<?= bitrix_sessid_get() ?>" onclick="$(this).parent().parent().load($(this).attr('href'));return false"><?= GetMessage("SHEMA") ?></a> 
    <? else: ?>
        <?= GetMessage("SHEMA") ?>
    <? endif; ?>    
    <? /* if ($arParams["q"]!="near"):?>
      <a class="another" href="javascript:void(0)" onclick="get_location_a()">ближайшие к вам</a>
      <?else:?>
      ближайшие к вам
      <?endif; */ ?>
</div>
<?
$section = "";
$c = ceil(count($arResult["ITEMS"]) / 4);
foreach ($arResult["ITEMS"] as $i => $ar):
    //v_dump($ar);
    ?>

<? endforeach; ?>
<div class="filter_block left" id="filter<?= $prop_id ?>">  
    <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?= GetMessage("MORE_" . $arProp["CODE"]) ?></a></div>
    <div class="subway-shema">    
        <? if (LANGUAGE_ID == "en"): ?>
            <img src="/tpl/images/subway/<?= CITY_ID ?>_en.gif" />
        <? else: ?>
            <img src="/tpl/images/subway/<?= CITY_ID ?>.gif" />
        <? endif; ?>
        <?
        //v_dump(LANGUAGE_ID);
        foreach ($stations as $key => $value) {
            if (LANGUAGE_ID == 'ru') {
                $name = $value['NAME'];
            } else {
                $name = $value['PROPERTY_ENG_NAME_VALUE'];
            }
            ?>

            <a style="<?= $value['PROPERTY_STYLE_VALUE'] ?>" val="<?= $name ?>" station_id="<?= $value['ID'] ?>" class="subway_station subway_station_extended_filter" href="javascript:void(0)"> </a>
            <? } ?>
        <div class="hidden-checkboxes" style="display:none;">
            <? foreach ($stations as $key => $value) { ?>
                <input type="checkbox" name="arrFilter_pf[subway][]" value="<?= $value['ID'] ?>" class="station<?= $value['ID'] ?>">
<? } ?>
        </div>
        <div class="selected-station-container" style="">
            <div class="new_filter_results" style="display: block;">
                <div class="left your_choose">Вы выбрали:</div>
                <ul style="display: block;">

                </ul>
                <div class="left">
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <? foreach ($arProp["VALUE_LIST"] as $key => $val): ?>
            <? if ($val["STYLE"]): ?>
                <a href="javascript:void(0)" class="subway_station <?= (in_array($key, $_REQUEST[$arParams["FILTER_NAME"] . "_pf"][$arProp["CODE"]])) ? "selected" : "" ?>" id="<?= $key ?>" val="<?= $val["NAME"] ?>" style="<?= $val["STYLE"] ?>"> </a>
            <? endif; ?>
<? endforeach; ?>
    </div>
    <div class="clear"></div>

