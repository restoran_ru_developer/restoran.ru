<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<select multiple name="arrFilter_pf[subway][]">
    <? foreach ($arResult["ITEMS"] as $i => $ar): ?>
        <option value="<?= $ar["ID"] ?>"><?= $ar["NAME"] ?></option>
    <? endforeach; ?>
</select>
<div class="clear"></div>