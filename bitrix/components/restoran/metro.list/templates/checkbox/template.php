<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<div class="sort_metro">
    <? if ($arParams["q"] != "abc"): ?>
        <a class="another" href="<?= $this->__component->__path ?>/ajax.php?q=abc&template=checkbox&<?= bitrix_sessid_get() ?>" onclick="$(this).parent().parent().load($(this).attr('href'));return false"><?= GetMessage("ABC") ?></a>  
    <? else: ?>
        <?= GetMessage("ABC") ?> 
    <? endif; ?>
    / 
    <? if ($arParams["q"] != "thread"): ?>
        <a class="another" href="<?= $this->__component->__path ?>/ajax.php?q=thread&template=checkbox&<?= bitrix_sessid_get() ?>" onclick="$(this).parent().parent().load($(this).attr('href'));return false"><?= GetMessage("LINES") ?></a> 
    <? else: ?>
        <?= GetMessage("LINES") ?>
    <? endif; ?>  
    / 
    <? if ($arParams["q"] != "shema"): ?>
        <a class="another" href="<?= $this->__component->__path ?>/ajax.php?q=shema&template=shema&<?= bitrix_sessid_get() ?>" onclick="$(this).parent().parent().load($(this).attr('href'));return false"><?= GetMessage("SHEMA") ?></a> 
    <? else: ?>
        <?= GetMessage("SHEMA") ?>
    <? endif; ?>  
    <? /* if ($arParams["q"]!="near"):?>
      <a class="another" href="javascript:void(0)" onclick="get_location_a()">ближайшие к вам</a>
      <?else:?>
      ближайшие к вам
      <?endif; */ ?>
</div>
<?
$section = "";
$c = ceil(count($arResult["ITEMS"]) / 4);
foreach ($arResult["ITEMS"] as $i => $ar):
    ?>
    <? if (($i >= $c && $i % $c == 0) || $i == 0): ?>
        <div class="left" style="margin-right: <?= ($i >= $c * 3) ? "0px" : "10px" ?>"><table cellpadding="0" cellspacing="0">
                <!--<ul class="metro_list">-->
            <? endif; ?>         
            <? /* if ($arParams["q"] == "thread"):?>
              <?if ($section!=$ar["SECTION_NAME"]):?>
              <tr class="line"><?=$ar["SECTION_NAME"]?></li>
              <?
              $section = $ar["SECTION_NAME"];
              endif;?>
              <?endif; */ ?>    
            <? if ($i == 0): ?>
                <!--<tr class="select_all">
                    <td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="" name="" id="" /></span></td>
                    <td style=""> </td>
                    <td class="option"><label class="niceCheckLabel uppercase"><?= GetMessage("SELECT_ALL") ?></label></td>                                       
                </tr>-->
            <? endif; ?>
            <tr>
                <td style="padding: 5px 0px;padding-right:5px;"><span class="niceCheck"><input type="checkbox" value="<?= $ar["ID"] ?>" name="arrFilter_pf[subway][]" /></span></td>
                <td style="background:url('<?= CFile::GetPath($ar["PICTURE"]) ?>') left center no-repeat; padding-left:15px;"> </td>
                <td class="option"><label class="niceCheckLabel"><?= $ar["NAME"] ?></label></td>
            </tr>
            <? if ($i % $c == ($c - 1) && $i > 0): ?>
            </table></div>
    <? endif;
    if (end($arResult["ITEMS"]) == $ar && $i % $c != ($c - 1)):
        ?>
        </table></div>
    <? endif; ?>
<? endforeach; ?>
<div class="clear"></div>