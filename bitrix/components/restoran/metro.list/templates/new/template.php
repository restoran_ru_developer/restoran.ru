<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script src="<?=$templateFolder?>/script.js"></script>
<script>
    $(document).ready(function(){
        $(".filter_popup_<?=$arResult["ID"]?>").popup({"obj":"filter4","css":{"right":"70px","top":"-5px", "width":"355px"}});
        var params = {
            changedEl: "#multi4",
            scrollArrows: true,
            visRows:15
        }
        cuSelMulti(params);
    })
</script>
    <select multiple="multiple" class="asd" id="multi4" name="arrFilter_pf[subway][]" size="15">
        <?foreach($arResult["ITEMS"] as $key=>$val):?>
            <option style="background:url(<?=CFile::GetPath($val["PICTURE"])?>) left center no-repeat; padding-left:15px;" value="<?=$val["ID"]?>" <?=(in_array($val["ID"],$_REQUEST["arrFilter_pf"]["subway"]))?"selected":""?>><?=$val["NAME"]?></option>
        <?endforeach?>
    </select>
    <br /><br />
    <div align="center"><input class="filter_button" filID="multi4" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>