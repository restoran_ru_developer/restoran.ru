<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (CITY_ID &&$q && check_bitrix_sessid()):
	$APPLICATION->IncludeComponent(
		"restoran:metro.list",
		(!$_REQUEST["template"])?"":$_REQUEST["template"],
		Array(
			"q" => trim($_REQUEST["q"]),
			"lat" => (double)$_REQUEST["lat"],
			"lon" => (double)$_REQUEST["lon"],
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "360000"			
		),
		false
	);
endif;
?>