<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600;
}
global $USER;
if($this->StartResultCache(false, array($arParams,$USER->GetGroups(),CITY_ID)))
{
    $arMetroIB = getArIblock("metro", CITY_ID);	
    if (LANGUAGE_ID=="en")
    {
        if (CITY_ID=="spb")
            $arMetroIB["ID"] = 89;
        elseif (CITY_ID=="msk")
            $arMetroIB["ID"] = 30;
        elseif (CITY_ID=="urm")
            $arMetroIB["ID"] = 3587;
        elseif (CITY_ID=="rga")
            $arMetroIB["ID"] = 3586;

    }    
    //if (CITY_ID=="")
    global $DB;
    $iblock_id = $DB->ForSql($arMetroIB["ID"]);
    if ($arParams["q"]=="near")
    {
        if ($arParams["lat"]&&$arParams["lon"])
        {
            $lat1 = $DB->ForSql((double)$arParams["lat"]-0.01);
            $lat2 = $DB->ForSql((double)$arParams["lat"]+0.01);
            $lon1 = $DB->ForSql((double)$arParams["lon"]-0.01);
            $lon2 = $DB->ForSql((double)$arParams["lon"]+0.01);

            $sql = "SELECT BE.NAME as NAME,BE.ID as ID, BE.IBLOCK_ID as IBLOCK_ID,BE.IBLOCK_SECTION_ID as IBLOCK_SECTION_ID, BS.PICTURE as PICTURE
            FROM b_iblock B 
            INNER JOIN b_iblock_element BE ON BE.IBLOCK_ID = B.ID 
            INNER JOIN b_iblock_property FP0 ON FP0.IBLOCK_ID = B.ID AND FP0.CODE='LAT' 
            INNER JOIN b_iblock_property FP1 ON FP1.IBLOCK_ID = B.ID AND FP1.CODE='LON' 
            INNER JOIN b_iblock_element_property FPV0 ON FPV0.IBLOCK_PROPERTY_ID = FP0.ID AND FPV0.IBLOCK_ELEMENT_ID = BE.ID 
            INNER JOIN b_iblock_element_property FPV1 ON FPV1.IBLOCK_PROPERTY_ID = FP1.ID AND FPV1.IBLOCK_ELEMENT_ID = BE.ID 
            INNER JOIN b_iblock_section BS ON BE.IBLOCK_SECTION_ID = BS.ID
            WHERE 1=1 AND ( (BE.IBLOCK_ID = '".$iblock_id."') AND (FPV0.VALUE_NUM BETWEEN '".$lat1."' AND '".$lat2."') AND (FPV1.VALUE_NUM BETWEEN '".$lon1."' AND '".$lon2."') ) 
            AND (BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL) 
            ORDER BY BE.NAME asc";
        }
    }
    elseif ($arParams["q"]=="thread")
    {
        $sql = "SELECT BE.NAME as NAME,BE.ID as ID, BE.IBLOCK_ID as IBLOCK_ID,BE.IBLOCK_SECTION_ID as IBLOCK_SECTION_ID, BS.PICTURE as PICTURE, BS.NAME as SECTION_NAME
        FROM b_iblock B 
        INNER JOIN b_iblock_element BE ON BE.IBLOCK_ID = B.ID 
        INNER JOIN b_iblock_section BS ON BE.IBLOCK_SECTION_ID = BS.ID
        WHERE 1=1 AND (BE.IBLOCK_ID = '".$iblock_id."') AND (BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL) 
        ORDER BY BS.NAME asc, BE.NAME asc";
    }
    else
    {
        $sql = "SELECT BE.NAME as NAME,BE.ID as ID, BE.IBLOCK_ID as IBLOCK_ID,BE.IBLOCK_SECTION_ID as IBLOCK_SECTION_ID, BS.PICTURE as PICTURE
        FROM b_iblock B 
        INNER JOIN b_iblock_element BE ON BE.IBLOCK_ID = B.ID 
        INNER JOIN b_iblock_section BS ON BE.IBLOCK_SECTION_ID = BS.ID
        WHERE 1=1 AND (BE.IBLOCK_ID = '".$iblock_id."') AND (BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL) 
        ORDER BY BE.NAME asc, BE.NAME asc";
    }
    
    $arResult = array();
    $res = $DB->Query($sql);
    while($ar = $res->GetNext())
    {		        
        $ar["NAME"] = explode("(",$ar["NAME"]);
        $ar["NAME"] = $ar["NAME"][0];
        $arResult["ITEMS"][] = $ar;
        //echo "<img src='".CFile::GetPath($ar["PICTURE"])."' align='left' /> ".$ar["NAME"]."<br />";
    }
        //v_dump($arResult["ITEMS"]);
    $this->SetResultCacheKeys(array(
            "ITEMS",
    ));

    $this->IncludeComponentTemplate();
}
?>