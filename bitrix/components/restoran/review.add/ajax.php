<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$APPLICATION->IncludeComponent(
        "restoran:review.add",
        ".default",
        Array(
                "RATIO" => (int)$_REQUEST["ratio"],
                "REVIEW" => trim($_REQUEST["review"]),
                "ACTION" => "save"
        )
);
require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>