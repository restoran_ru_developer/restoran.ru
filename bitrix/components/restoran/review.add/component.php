<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arParams["IBLOCK_TYPE"] = ($arParams["IBLOCK_TYPE"])?trim($arParams["IBLOCK_TYPE"]):$_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"];
$arParams["IBLOCK_ID"] = ($arParams["IBLOCK_ID"])?intval($arParams["IBLOCK_ID"]):$_SESSION["ADD_REVIEW"]["IBLOCK_ID"];
$arParams["SECTION_ID"] = ($arParams["SECTION_ID"])?intval($arParams["SECTION_ID"]):$_SESSION["ADD_REVIEW"]["SECTION_ID"];
$arParams["ELEMENT_ID"] = ($arParams["ELEMENT_ID"])?intval($arParams["ELEMENT_ID"]):$_SESSION["ADD_REVIEW"]["ELEMENT_ID"];
if (!$_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"])
{
    $_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"] = $arParams["IBLOCK_TYPE"];
    $_SESSION["ADD_REVIEW"]["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
    $_SESSION["ADD_REVIEW"]["SECTION_ID"] = $arParams["SECTION_ID"];
    $_SESSION["ADD_REVIEW"]["ELEMENT_ID"] = $arParams["ELEMENT_ID"];
}
$error = array();
$arr = array();
if(!CModule::IncludeModule("iblock"))
{
    $this->AbortResultCache();
    $error[] = GetMessage("IBLOCK_MODULE_NOT_INSTALLED");
    return;
}
if (!$USER->IsAuthorized())
    $error[] = GetMessage("ERROR_USER");
if ($arParams["IBLOCK_ID"]<=0)
    $error[] = GetMessage("ERROR_IBLOCK_ID");
if ($arParams["SECTION_ID"]<=0)
    $error[] = GetMessage("ERROR_ID");
if (count($error)>0)
{
    $arr["STATUS"] = 0;
    $arr["ERROR"] = implode("<br />",$error);
    //echo $error;    
    return;
}
if ($arParams["ACTION"]=="save")
{    
    if (!$arParams["RATIO"])
        $error[] = GetMessage("RATIO_ERROR");
    if (!$arParams["REVIEW"])
        $error[] = GetMessage("REVIEW_ERROR");        
    if (count($error)>0)
    {
        echo $arr["ERROR"] = implode("<br />",$error);
        return;
    }
    else
    {
        $el = new CIBlockElement;
        $PROP = array();
        $PROP["ratio"] = (int)$_REQUEST["ratio"];  
        $arLoadProductArray = Array(  "MODIFIED_BY"    => $USER->GetID(), 
                                      "IBLOCK_SECTION_ID" => $arParams["SECTION_ID"],
                                      "IBLOCK_ID"      => $arParams["IBLOCK_ID"],  
                                      "PROPERTY_VALUES"=> $PROP,  
                                      "NAME"  => ($USER->GetFirstName())?$USER->GetFirstName():$USER->GetLogin(),  
                                      "ACTIVE" => "Y",
                                      "PREVIEW_TEXT"   => $arParams["REVIEW"]);
        if($PRODUCT_ID = $el->Add($arLoadProductArray))  
        {
            $dbr = CIBlockElement::GetList(array(), array("=ID"=>$arParams["ELEMENT_ID"]), false, false, array("ID", "IBLOCK_ID","PROPERTY_ratio"));
            if ($dbr_arr = $dbr->Fetch())
            {
                $IBLOCK_ID = $dbr_arr["IBLOCK_ID"];
                if ($dbr_arr["PROPERTY_RATIO_VALUE"])
                    $new_ratio = ($dbr_arr["PROPERTY_RATIO_VALUE"]+$arParams["RATIO"])/2;
                else
                    $new_ratio = $arParams["RATIO"];
                $s = CIBlockElement::SetPropertyValues($arParams["ELEMENT_ID"], $IBLOCK_ID, $new_ratio, "ratio");
            }

            echo "Спасибо, Ваш голос учтен";
            unset($_SESSION["ADD_REVIEW"]);
            return;
        }
        else  
        {
            $error = $el->LAST_ERROR;
            echo $arr["ERROR"] = implode("<br />",$error);
            return;
        }
    }   
}
$this->IncludeComponentTemplate();    
?>