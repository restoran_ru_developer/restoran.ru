function showRegpromptFB() {
    /*$(document).ready(function() {
        // show prompt_vk for additional data
        $('#prompt_fb').overlay({
            top: 260,
            onBeforeLoad: function(){
                $('#prompt_fb > p:eq(0)').show();
                $('#prompt_fb > p:eq(1)').hide();
            },
            onBeforeClose: function() {
                $('div.error').hide();
            },
            mask: {
                color: '#fff',
                loadSpeed: 200,
                closeSpeed:0,
                opacity: 0.5
            },
            closeOnClick: false,
            closeOnEsc: false,
            api: true
        }).load();
    });*/
    if (!$("#auth_modal").size())
    {
        $("<div class='popup popup_modal' id='auth_modal' style='width:400px;'></div>").appendTo("body");
        setCenter($("#auth_modal"));
        $("#auth_modal").html($('#prompt_fb').html());
    }
    showOverflow();
    $("#auth_modal").fadeIn("300");
 
    $("#auth_modal > form").submit(function(e) {
    	var form = $(this);
    	// client-side validation OK.
    	if (!e.isDefaultPrevented()) {
    		// submit with AJAX
            $.ajax({
                type: "POST",
                url: "/bitrix/components/restoran/user.facebook_auth/templates/.default/authorize.php",
                data: form.serialize(),
                 beforeSend: function(data)
                {
                    /*if (!$("#vk_email").attr("value"))
                    {
                        alert("Введите E-mail");
                        return false;
                    }
                    if (!$("#vk_phone").attr("value"))
                    {
                        alert("Введите номер телефона");
                        return false;
                    }*/
                    
                    return true;
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);
                    // if result OK show confirm code field
                    if (obj.ERROR)
                    {
                        $("#auth_modal > #facebook_response").html(obj.ERROR);
                    }
                    if(obj.TYPE == "AUTH") {
                        location.href = "/";
                    } else if(obj.TYPE == "REQ_CONFIRM_CODE") {
                        $('#auth_modal > .center > p:eq(0)').hide();
                        $('#auth_modal > .center > p:eq(1)').show();
                        $('#auth_modal > form > div:eq(0)').html(obj.HTML);
                        // reinitialize validator
                       // $("#auth_modal > form").validator({ lang: 'ru' });
                    } else if(obj.TYPE == "ADD") {
                        location.href = "/";
                    } else if(obj.ERROR) {
                        form.data("validator").invalidate(obj.ERROR);
                    }
                }
            });
    		// prevent default form submission logic
    		e.preventDefault();
    	}
        return false;
    });
}

$(document).ready(function() {
    // phone validator
    /*$.tools.validator.fn("[type=phone]", function(el, value) {
        return isNaN(value) ? "Неверный формат номера (пример 79XXXXX...)" : true;
    });

    // validator locale
    $.tools.validator.localize("ru", {
        '*'	: 'Поле обязательно для заполнения',
    	'[required]'	: 'Поле обязательно для заполнения'
    });

    // validate form
    $("#prompt_fb > form").validator({ lang: 'ru' }).submit(function(e) {
        var form = $(this);
        // client-side validation OK.
        if (!e.isDefaultPrevented()) {
            // submit with AJAX
            $.ajax({
                type: "POST",
                url: "/bitrix/components/restoran/user.facebook_auth/templates/.default/authorize.php",
                data: form.serialize(),
                success: function(data) {
                    var obj = jQuery.parseJSON(data);
                    // if result OK show confirm code field
                    if(obj.TYPE == "AUTH") {
                        location.href = "/";
                    } else if(obj.TYPE == "REQ_CONFIRM_CODE") {
                        $('#prompt_fb > p:eq(0)').hide();
                        $('#prompt_fb > p:eq(1)').show();
                        $('#prompt_fb > form > div:eq(0)').html(obj.HTML);
                        // reinitialize validator
                        $("#prompt_fb > form").validator({ lang: 'ru' });
                    } else if(obj.TYPE == "ADD") {
                        location.href = "/";
                    } else if(obj.ERROR) {
                        form.data("validator").invalidate(obj.ERROR);
                    }
                }
            });
            // prevent default form submission logic
            e.preventDefault();
        }
    });*/
});