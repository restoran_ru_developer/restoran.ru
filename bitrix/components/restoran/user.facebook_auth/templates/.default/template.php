<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div id="fb-root"></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({
            appId  : '297181676964377',
            status : true, // check login status
            cookie : true, // enable cookies to allow the server to access the session
            xfbml  : true,  // parse XFBML
            oauth : true //enables OAuth 2.0
        });
    };

    (function() {
        var e = document.createElement('script');
        e.src = document.location.protocol + '//connect.facebook.net/ru_RU/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }());

    function showFBAuth() {
        FB.login(function(response) {
            if (response.authResponse) {
               // showExpose();
                //FB.api('/me/picture', function(response) {
                //    console.log(response);
                //});
                FB.api('/me?fields=birthday,last_name,email,username,name,gender,first_name,location,picture.height(480)', function(response) {
                    var pic = "";
                    console.log(response);
                    // add or authorize user
                    /*FB.api('/me/picture', function(response) {
                        pic = response;
                        //в адресе заменять q на b
                    });
                    console.log(pic);*/
                    $.ajax({
                        type: "POST",
                        url: "<?=$templateFolder?>/authorize.php",
                        data: response,
                        success: function(data) {
                            var obj = jQuery.parseJSON(data);
                            if(obj.TYPE == "AUTH") {
                                location.href = "/";
                            } else {
                                if (obj.ERROR)
                                {
                                    $('#fb_user_id').val(response.id);
                                    //showRegpromptFB();
                                }
                                else
                                {
                                    location.href = "/";
                                }
                            }
                        }
                    });
                });
            } else {
               // closeExpose();
            }
        }, {scope: 'email'/*,user_birthday,user_photos,user_about_me*/});
    }
</script>

<div onclick="showFBAuth()"><img class="pointer" src="<?=SITE_TEMPLATE_PATH?>/images/f_login.png" /></div>
<div class="modal_fb" id="prompt_fb">
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <div id="facebook_response"></div>   
 </div>