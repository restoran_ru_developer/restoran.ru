<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$APPLICATION->IncludeComponent(
        "restoran:main.comments_add",
        ".default",
        Array(
                "REVIEW" => trim($_REQUEST["review"]),
                "ACTION" => "save",
                "IBLOCK_ID" => (int)$_REQUEST["IBLOCK_ID"],
                "SECTION_ID" => (int)$_REQUEST["SECTION_ID"],
                "RATIO" => (int)$_REQUEST["ratio"]
        )
);
require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>