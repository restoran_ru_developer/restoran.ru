<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER;


if($this->StartResultCache(false, array(($USER->GetID()),$arParams["ID"])))
{

    CModule::IncludeModule("iblock");
    if ($arParams["ID"])
    {        
        $res = CIBlockElement::GetList(Array(),Array("ACTIVE"=>"Y" ,"ID"=>$arParams["ID"]),false,false,Array("ID","CREATED_BY","IBLOCK_ID","NAME","ACTIVE_FROM"));
        if ($ar_res = $res->GetNext())
        {
            if ($ar_res["CREATED_BY"]==$USER->GetID())
            {
                $res1 = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], "sort", "asc", array("CODE" => "USERS"));
                while ($ob = $res1->GetNext())
                {
                    $ar_res["USERS"][] = $ob['VALUE'];
                }

                $res1 = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], "sort", "asc", array("CODE" => "NA_USERS"));
                while ($ob = $res1->GetNext())
                {
                    $ar_res["NA_USERS"][] = $ob['VALUE'];
                }

                $res1 = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], "sort", "asc", array("CODE" => "RESTORAN"));
                if ($ob = $res1->GetNext())
                {
                    $r = CIBlockElement::GetByID($ob["VALUE"]);
                    if ($a = $r->Fetch())
                    {
                        $ar_res["RESTORAN"]["NAME"] = $a["NAME"];
                        $ar_res["RESTORAN"]["ID"] = $ob['VALUE'];
                    }

                }
                
                $temp = explode(" ",$ar_res["ACTIVE_FROM"]);
                $ar_res["DATE"] = $temp[0];
                $ar_res["TIME"] = substr($temp[1],0,5);
                $arResult["INVITE"] = $ar_res;
            }
        }
    }
    $rsUsers = CUser::GetList(($by="last_name"), ($order="asc"), Array("ACTIVE"=>"Y","GROUPS_ID"=>Array("1","5"),"!LAST_NAME"=>false));
    while($ar = $rsUsers->Fetch()):
            $arResult["USERS"][] = $ar;
    endwhile;
    $res = CUser::GetById($_REQUEST["USER_ID"]);
    if ($ar = $res->Fetch())
        $arResult["USER"] = $ar;
}

if ($arParams["AJAX"]=="Y"&&check_bitrix_sessid()&&$_REQUEST["web_form_apply"])
{
    $el = new CIBlockElement;
    $_REQUEST["time"] = trim(str_replace("   ", ":", $_REQUEST["time"]));
    $PROP = array();
    $PROP["RESTORAN"] = (int)$_REQUEST["RESTORAN"]; 
    if ($_REQUEST["RESTORAN"]&&$_REQUEST["time"]&&$_REQUEST["date"]&&($_REQUEST["users"][0]||$_REQUEST["friend_emails"]))
    {
        if ($_REQUEST["users"][0])
        {
            foreach($_REQUEST["users"] as $key=>$user)
            {
                $users[] = $user;
                $PROP["USERS"][] = Array("VALUE"=>$user,"DESCRIPTION"=>"");
            }
        }    
        if ($_REQUEST["friend_emails"])
        {
            $temp = explode(",",$_REQUEST["friend_emails"]);   
            foreach($temp as $t)
            {
                $rsUsers = CUser::GetList(($by="name"), ($order="desc"), Array("EMAIL"=>trim($t)));
                if ($arUs = $rsUsers->Fetch())
                {
                    $r = 0; 
                    foreach($_REQUEST["users"] as $key=>$user)
                    {
                        if ($user==$arUs["ID"])
                        {
                            $r = 1;
                            continue;
                        }
                    }
                    if (!$r)
                    {
                        $users[] = $arUs["ID"];
                        $PROP["USERS"][] = Array("VALUE"=>$arUs["ID"],"DESCRIPTION"=>"");
                    }
                }
                else    
                    $PROP["NA_USERS"][] = trim($t);
            }
        }    

        $arInvIB = getArIblock("invites", CITY_ID);
        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => $arInvIB["ID"],
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => (($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail())." ".$_REQUEST["date"]." ".$_REQUEST['form_text_32'],
            "ACTIVE"         => "Y",
            "ACTIVE_FROM"   => $_REQUEST["date"]." ".$_REQUEST["time"]
        );
        //Если передан ID то делаем UPDATE
        if (!$_REQUEST["ID"]):
            if($PRODUCT_ID = $el->Add($arLoadProductArray))
            {
                $arResult["STATUS"] = 1;     
                $arResult["RELOAD"] = 2;      
                CModule::IncludeModule("iblock");
                $res = CIBlockElement::GetByID($PROP["RESTORAN"]);
                if ($ar = $res->GetNext())
                {
                    $rest_url = $ar["DETAIL_PAGE_URL"];
                    $rest_name = $ar["NAME"];
                    $obParser = new CTextParser;
                    $ar["PREVIEW_TEXT"] = $obParser->html_cut($ar["DETAIL_TEXT"], 300);
                    $rest_anons = $ar["PREVIEW_TEXT"];
                    if ($ar["PREVIEW_PICTURE"])
                    {
                        $watermark = Array(
                        Array( 'name' => 'watermark',
                            'position' => 'br',
                            'size'=>'medium',
                            'type'=>'image',
                            'alpha_level'=>'40',
                            'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
                            ),
                        );
                        $p = 0;
                        $rest_image = CFile::ResizeImageGet($ar["PREVIEW_PICTURE"],Array("width"=>392,"height"=>266),BX_RESIZE_IMAGE_EXACT,true,$watermark);
                    }
                    else
                        $rest_image["src"] = "/tpl/images/noname/rest_nnm.png";
                }
                foreach ($users as $user)
                {
                    $rs = CUser::GetByID($user);
                    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($_REQUEST["date"], CSite::GetDateFormat()));
                    $arUser = $rs->Fetch();
                    {
                        $arEventFields = array();
                        $arEventFields = array(
                            "ID"=>$user,
                            "REST_LINK" => $rest_url,
                            "REST_NAME" => $rest_name,
                            "REST_IMAGE" => $rest_image["src"],
                            "REST_ANONS" => $rest_anons,
                            "DATE" => $arTmpDate,
                            "TIME" => $_REQUEST["time"],
                            "EMAIL" => $arUser["EMAIL"],
                            "NAME" =>  $arUser["LAST_NAME"]." ".$arUser["NAME"],
                            "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),
                            "THEME" => "Вас пригласили в ресторан"
                        );  
                        CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",94);
                    }
                }
                if ($_REQUEST["friend_emails"])
                {
                    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($_REQUEST["date"], CSite::GetDateFormat()));
                    foreach ($PROP["NA_USERS"] as $user)
                    {

                            $arEventFields = array();
                            $arEventFields = array(
                                "ID"=>$user,
                                "REST_LINK" => $rest_url,
                                "REST_NAME" => $rest_name,
                                "REST_IMAGE" => $rest_image["src"],
                                "REST_ANONS" => $rest_anons,
                                "DATE" => $arTmpDate,
                                "TIME" => $_REQUEST["time"],
                                "EMAIL" => $user,
                                "NAME" =>  "Дорогой друг",
                                "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),
                                "THEME" => "Вас пригласили в ресторан"
                            );  
                            CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",153);//96
                    }
                }  
                $arEventFields = array(
                    "RESTORAN_URL" => $rest_url,
                    "RESTORAN_NAME" => $rest_name,                        
                    "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),
                    "IBLOCK_ID" =>$arInvIB["ID"],
                    "ID" => $PRODUCT_ID
                );
                CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",95);
            }
            else
            {
                $arResult["STATUS"] = 0;
                $arResult["ERROR"] = $el->LAST_ERROR;                
            }
        else:
            $us_compl = array();
            $arInvIB = getArIblock("invites", CITY_ID);
            $db_props = CIBlockElement::GetProperty($arInvIB["ID"], $_REQUEST["ID"], "sort", "asc", Array("CODE"=>"USERS"));
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["DESCRIPTION"]=="Y")
                {
                    $us_compl[] = $ar_props["VALUE"];
                    foreach($PROP["USERS"] as $k=>$us)
                    {
                        if ($us["VALUE"]==$ar_props["VALUE"])
                            $PROP["USERS"][$k]["DESCRIPTION"] = "Y";
                    }                    
                }                        
            }             
            $arLoadProductArray["PROPERTY_VALUES"] = $PROP;            
            //$res = CIBlockElement::GetProperty($arInvIB["ID"],$_REQUEST["id"],)
            if($result = $el->Update((int)$_REQUEST["ID"],$arLoadProductArray))
            {
                $arResult["STATUS"] = 1;        
                $arResult["RELOAD"] = 1;        
                CModule::IncludeModule("iblock");
                $res = CIBlockElement::GetByID($PROP["RESTORAN"]);
                if ($ar = $res->GetNext())
                {
                    $rest_url = $ar["DETAIL_PAGE_URL"];
                    $rest_name = $ar["NAME"];
                    $obParser = new CTextParser;
                    $ar["PREVIEW_TEXT"] = $obParser->html_cut($ar["DETAIL_TEXT"], 300);
                    $rest_anons = $ar["PREVIEW_TEXT"];
                    if ($ar["PREVIEW_PICTURE"])
                    {
                        $watermark = Array(
                        Array( 'name' => 'watermark',
                            'position' => 'br',
                            'size'=>'small',
                            'type'=>'image',
                            'alpha_level'=>'40',
                            'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
                            ),
                        );
                        $p = 0;
                        $rest_image = CFile::ResizeImageGet($ar["PREVIEW_PICTURE"],Array("width"=>392,"height"=>266),BX_RESIZE_IMAGE_EXACT ,true,$watermark);
                    }
                    else
                        $rest_image["src"] = "/tpl/images/noname/rest_nnm.png";
                }
                foreach ($users as $user)
                {
                    $rs = CUser::GetByID($user);
                    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($_REQUEST["date"], CSite::GetDateFormat()));
                    $arUser = $rs->Fetch();
                    {
                        $arEventFields = array();
                        $arEventFields = array(
                            "ID"=>$user,
                            "REST_LINK" => $rest_url,
                            "REST_NAME" => $rest_name,
                            "REST_IMAGE" => $rest_image["src"],
                            "REST_ANONS" =>$rest_anons,
                            "DATE" => $arTmpDate,
                            "TIME" => $_REQUEST["time"],
                            "EMAIL" => $arUser["EMAIL"],
                            "NAME" =>  $arUser["LAST_NAME"]." ".$arUser["NAME"],
                            "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),
                            "THEME" => "Изменения в приглашении"
                        );  
                        CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",94);
                    }
                }
                if ($_REQUEST["friend_emails"])
                {
                    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($_REQUEST["date"], CSite::GetDateFormat()));
                    foreach ($PROP["NA_USERS"] as $user)
                    {
                        $arEventFields = array();
                        $arEventFields = array(
                            "ID"=>$user,
                            "REST_LINK" => $rest_url,
                            "REST_NAME" => $rest_name,
                            "REST_IMAGE" => $rest_image["src"],
                            "REST_ANONS" =>$rest_anons,
                            "DATE" => $arTmpDate,
                            "TIME" => $_REQUEST["time"],
                            "EMAIL" => $user,
                            "NAME" =>  "Дорогой друг",
                            "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),
                            "THEME" => "Изменения в приглашении"
                        );  
                        CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",96);
                    }
                }  
                /*$arEventFields = array(
                    "RESTORAN_URL" => $rest_url,
                    "RESTORAN_NAME" => $rest_name,                        
                    "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),
                    "IBLOCK_ID" =>$arInvIB["ID"],
                    "ID" => $PRODUCT_ID
                );
                CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",95);*/
            }
            else
            {
                $arResult["STATUS"] = 0;
                $arResult["ERROR"] = $result->LAST_ERROR;
            }   
        endif;
    }
    else
    {
        if (!$_REQUEST["RESTORAN"])
            $er[] = GetMessage("NO_REST");
        if (!$_REQUEST["time"])
            $er[] = GetMessage("NO_TIME");
        if (!$_REQUEST["date"])
            $er[] = GetMessage("NO_DATE");
        if (!$_REQUEST["users"][0])
            $er[] = GetMessage("NO_USERS");
        if (count($er)>0)
        {
            $arResult["ERROR"] = implode("<br />",$er);
            $arResult["STATUS"] = 0;
        }
    }
}
$this->IncludeComponentTemplate();
?>