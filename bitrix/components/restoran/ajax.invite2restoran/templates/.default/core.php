<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
</div>
<?
if ($_REQUEST["ACTION"]=="delete"&&check_bitrix_sessid()&&$_REQUEST["ID"])
{
    global $USER;
    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($ar = $res->Fetch())
    {
        if ($USER->GetID()==$ar["CREATED_BY"])
        {
            $arLoadProductArray = Array(
                "ACTIVE" => "N",
            );
            $el = new CIblockElement;
            $PRODUCT_ID = (int)$_REQUEST["ID"];
            if($res = $el->Update($PRODUCT_ID, $arLoadProductArray))
            {                
                echo "Приглашение отмененно";
                $el_res = CIBlockElement::GetByID($PRODUCT_ID);
                if ($ar_res = $el_res->Fetch())
                {
                    $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE"=>"RESTORAN"));
                    if($ar_props = $db_props->Fetch())
                        $RESTORAN = IntVal($ar_props["VALUE"]);                    
                    $res = CIBlockElement::GetByID($RESTORAN);
                    if ($ar = $res->GetNext())
                    {
                        $rest_url = $ar["DETAIL_PAGE_URL"];
                        $rest_name = $ar["NAME"];
                    }
                    
                    
                    $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE"=>"USERS"));
                    while($ar_props = $db_props->Fetch())
                        $users[] = IntVal($ar_props["VALUE"]);                    
                    
                    $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE"=>"NA_USERS"));
                    while($ar_props = $db_props->Fetch())
                        $na_users[] = IntVal($ar_props["VALUE"]);                    
                    $temp = explode(" ",$ar_res["ACTIVE_FROM"]);
                    $date = $temp[0];
                    $time = substr($temp[1],0,5);
                    foreach ($users as $user)
                    {
                        $rs = CUser::GetByID($user);
                        $arUser = $rs->Fetch();
                        {
                            $arEventFields = array();
                            $arEventFields = array(
                                "ID"=>$user,
                                "RESTORAN_URL" => $rest_url,
                                "RESTORAN_NAME" => $rest_name,
                                "DATE" => $date,
                                "TIME" => $time,
                                "EMAIL" => $arUser["EMAIL"], 
                                "NAME" =>  $arUser["LAST_NAME"]." ".$arUser["NAME"],
                                "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail()
                            );  
                            CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",97);
                        }
                    }
                    foreach ($na_users as $user)
                    {
                            $arEventFields = array();
                            $arEventFields = array(
                                "RESTORAN_URL" => $rest_url,
                                "RESTORAN_NAME" => $rest_name,
                                "DATE" => $date,
                                "TIME" => $time,
                                "EMAIL" => $user,
                                "NAME" =>  "",
                                "SECOND_NAME" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail()
                            );  
                            CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",97);
                    }                                        
                }                
            }
            else
            {
                echo $res->LAST_ERROR;
            }            
        }
        else
             echo "Нет доступа";
    }
}

if ($_REQUEST["ACTION"]=="confirm"&&check_bitrix_sessid()&&$_REQUEST["ID"]&&$_REQUEST["ELEMENT"])
{
    global $DB;
    $sql = "UPDATE b_iblock_element_property SET DESCRIPTION='Y' WHERE ID=".$DB->ForSql($_REQUEST["ID"]);    
    if ($DB->Query($sql))
        echo "Сохранено";
    else
        echo "Произошло ошибка, попробуйте позже";
    
    //отправляем письмо приглашающему
    CModule::IncludeModule("iblock");    
    $res = CIBlockElement::GetByID((int)$_REQUEST["ELEMENT"]);
    if($ar_fields = $res->GetNext())
    {   
        $db_props = CIBlockElement::GetProperty($ar_fields["IBLOCK_ID"], $ar_fields["ID"], array("sort" => "asc"), Array("CODE"=>"RESTORAN"));
        if($ar_props = $db_props->Fetch())
            $RESTORAN = IntVal($ar_props["VALUE"]);                    
        $res = CIBlockElement::GetByID($RESTORAN);
        if ($ar = $res->Fetch())            
            $rest_name = $ar["NAME"];
        
        $rs = CUser::GetByID($ar_fields["CREATED_BY"]);
        $us = $rs->Fetch();
        
        $arEventFields = array();
        $arEventFields = array(            
            "RESTORAN" => $rest_name,            
            "EMAIL" => $us["EMAIL"],
            "ID" => $us["ID"],            
            "USER" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail()
        );  
        CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",128);
    }
}

if ($_REQUEST["ACTION"]=="declain"&&check_bitrix_sessid()&&$_REQUEST["ID"]&&$_REQUEST["ELEMENT"])
{
    global $DB;
    global $USER;
    $sql = "UPDATE b_iblock_element_property SET DESCRIPTION='N' WHERE ID=".$DB->ForSql($_REQUEST["ID"]);    
    if ($DB->Query($sql))
        echo "Сохранено";
    else
        echo "Произошло ошибка, попробуйте позже";
    
    if ($_REQUEST["ac"]!="del"):
        CModule::IncludeModule("iblock");    
        $res = CIBlockElement::GetByID((int)$_REQUEST["ELEMENT"]);
        if($ar_fields = $res->GetNext())
        {
            $res1 = CIBlockElement::GetProperty($ar_fields["IBLOCK_ID"], $ar_fields["ID"], "sort", "asc", array("CODE" => "DECLAIN"));
            while ($ob = $res1->GetNext())
            {
                $ar_fields["USERS"][] = $ob['VALUE'];
            }
            $ar_fields["USERS"][] = $USER->GetID();
            CIBlockElement::SetPropertyValuesEx($ar_fields["ID"], false, array("DECLAIN" => $ar_fields["USERS"]));

            //отправляем письмо приглашающему

            $db_props = CIBlockElement::GetProperty($ar_fields["IBLOCK_ID"], $ar_fields["ID"], array("sort" => "asc"), Array("CODE"=>"RESTORAN"));
            if($ar_props = $db_props->Fetch())
                $RESTORAN = IntVal($ar_props["VALUE"]);                    
            $res = CIBlockElement::GetByID($RESTORAN);
            if ($ar = $res->Fetch())            
                $rest_name = $ar["NAME"];

            $rs = CUser::GetByID($ar_fields["CREATED_BY"]);
            $us = $rs->Fetch();

            $arEventFields = array();
            $arEventFields = array(            
                "RESTORAN" => $rest_name,            
                "EMAIL" => $us["EMAIL"],
                "ID" => $us["ID"],            
                "USER" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail()
            );  
            CEvent::Send("INVITE_TO_RESTORAN", "s1", $arEventFields,"Y",127);
        }
    endif;
}

if ($_REQUEST["ACTION"]=="del_user"&&check_bitrix_sessid()&&$_REQUEST["ID"]&&$_REQUEST["ELEMENT"])
{
    global $DB;
    global $USER;
    $sql = "DELETE FROM b_iblock_element_property WHERE ID=".$DB->ForSql($_REQUEST["ID"])." LIMIT 1";    
    if ($DB->Query($sql))
        echo "Сохранено";
    else
        echo "Произошло ошибка, попробуйте позже";    
}

require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>