<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["AJAX"]=="Y"):?>    
        <?
        $APPLICATION->RestartBuffer();
        if ($arResult["ERROR"]):
            $ar["RELOAD"] = $arResult["RELOAD"];
            if ($ar["RELOAD"]==2)
                $ar["USER"] = $USER->GetID();
            $ar["MESSAGE"] = $arResult["ERROR"];
            $ar["STATUS"] = 0;
            echo json_encode($ar);
        else:
            $ar["RELOAD"] = $arResult["RELOAD"];
            if ($ar["RELOAD"]==2)
                $ar["USER"] = $USER->GetID();        
            $ar["MESSAGE"] = GetMessage("SAVE_COMMENT");
            $ar["STATUS"] = 1;
            echo json_encode($ar);
        endif;?>
<?else:?>
    <link href="<?=$templateFolder?>/style.css"  type="text/css" rel="stylesheet" />
    <link href="<?=$templateFolder?>/chosen.css"  type="text/css" rel="stylesheet" />
    <script src="<?=$templateFolder?>/script.js"></script>
    <script src="<?=$templateFolder?>/chosen.jquery.js"></script>
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <div id="order_online">
        <div class="ajax_form">
            <form action="<?=$templateFolder?>/ajax.php" method="POST" some_param="">
                <?=bitrix_sessid_post()?>
                <input type="hidden" name="ID" value="<?=$arResult["INVITE"]["ID"]?>" />
                <div class="ok"><?=$arResult["FORM_NOTE"]?></div>
                <h1 style="text-align:center;color:#FFF; margin-bottom:0px;"><?=GetMessage("I_WANT")?></h1>
                <script>
                    $(document).ready(function(){
                        $("#choose_user").data("placeholder","Выберите пользователя...").chosen();
                    });
                </script>
                <div class="question">
                    <?=GetMessage("IN_RESTORAN")?>  
                    <?
                        $_REQUEST["name"] = $arResult["INVITE"]["RESTORAN"]["NAME"];
                        $_REQUEST["id"] = $arResult["INVITE"]["RESTORAN"]["ID"];
                    ?>
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "invite",
                            Array(
                                "NAME" => "RESTORAN"
                            ),
                        false
                    );?>
                </div> 
                <div class="question">
                    <input type="text" class="datew" value="<?=($arResult["INVITE"]["DATE"])?$arResult["INVITE"]["DATE"]:$_REQUEST["date"]?>" name="date"/>
                    <?=GetMessage("AT")?>
                    <input type="text" class="time" value="<?=($arResult["INVITE"]["TIME"])?$arResult["INVITE"]["TIME"]:$_REQUEST["time"]?>" name="time"/>
                </div>          
                <div class="question">
                    <h3><?=GetMessage("LIST_USERS")?></h3>

                    <select id="choose_user" style="width:300px;" name="users[]" class="chzn-select" multiple>
                        <?if($arResult["INVITE"]["USERS"]):?>
                            <?foreach ($arResult["USERS"] as $key=>$val):?>
                                <option value="<?=$val["ID"]?>" <?=(in_array($val["ID"], $arResult["INVITE"]["USERS"]))?"selected":""?>><?if(!empty($val["LAST_NAME"]) || !empty($val["NAME"])):?><?=$val["LAST_NAME"]." ".$val["NAME"]?><?else:?><?=$val['PERSONAL_PROFESSION']?><?endif?></option>
                            <?endforeach;?>
                        <?else:?>
                            <?foreach ($arResult["USERS"] as $key=>$val):?>
                                <option value="<?=$val["ID"]?>" <?=($val["ID"]==$_REQUEST["USER_ID"])?"selected":""?>><?if(!empty($val["LAST_NAME"]) || !empty($val["NAME"])):?><?=$val["LAST_NAME"]." ".$val["NAME"]?><?else:?><?=$val['PERSONAL_PROFESSION']?><?endif?></option>
                            <?endforeach;?>
                        <?endif;?>
                    </select>
                    <div class="clear"></div>
                </div>               
                <div class="question">
                    Вы можете пригласить незарегистрированных на нашем сайте друзей, для этого, введите через запятую их email
                    <input type="text" class="search_rest" name="friend_emails" style="text-align:left;" value="<?=($arResult["INVITE"]["NA_USERS"][0])? implode(", ", $arResult["INVITE"]["NA_USERS"]):$_REQUEST["friend_emails"]?>"> <br />                       
                </div>
                <?if($arResult["isUseCaptcha"] == "Y"): ?>
                    <div class="question">
                        <?=GetMessage("CAPTCHA")?>
                    </div>
                    <div class="question">
                        <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" align="left" />
                        <?//=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?//=$arResult["REQUIRED_SIGN"];?>
                        <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
                    </div>
                <?endif; ?>
                <Br />
                <input type="hidden" name="web_form_apply" value="Y" />
                <div align="right">
                    <input class="light_button" id="ivite_button" type="submit" name="web_form_submit" value="<?=strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"];?>" />
                </div>
            </form>
        </div>
    </div>
<?endif;?>