$(document).ready(function(){
    $.tools.validator.localize("ru", {
        '*'	: '',
    	'[req]'	: ''
    });
    $.tools.validator.fn("[req=req]", "", function(input, value) { 
        if (!value)
            return false;
        return true;
    });
    $(".phone").maski("(999) 999-99-99");
    $(".ajax_form > form").validator({lang: 'ru',messageClass: 'error_text_message',singleError: "true"}).submit(function(e) {
        var form = $(this);
        if (!e.isDefaultPrevented()) {
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                dataType: "json",
                data: form.serialize(),
                success: function(data) {
                    //var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
                    //var html2 = '</div>';
                    //data = eval('('+data+')');                            
                    $('.ok').html(data.MESSAGE);
                    $('.ok').fadeIn(500);
                    if (data.STATUS==1)
                        $("#ivite_button").attr("disabled",true);
                    if (data.RELOAD && data.RELOAD=="2")
                    {
                        location.reload();
                        //location.href = "/users/id"+data.USER+"/invites/#my_invite";
                    }
                    if (data.RELOAD && data.RELOAD=="1")
                        location.reload();
                    //setTimeout("location.reload()","2000");
                    //setTimeout("$('.ok').fadeOut(500)",10000);
                }
            });
            e.preventDefault();
        }
    });
    $(".ajax_form > form").bind("onFail", function (e, errors) {
        if (e.originalEvent.type == 'submit') {
            $.each(errors, function () {
                var input = this.input;
            });
        }
    });
    var params = {
        changedEl:"#what_question"
        /*visRows:12,
        scrollArrows: false*/
    }
    cuSel(params);
    $.tools.dateinput.localize("ru",  {months: 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря', 
                            shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
                            days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
                            shortDays:     'вс,пн,вт,ср,чт,пт,сб'});
    //$(".datew").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});




    $.maski.definitions['~']='[0-2]';                               
    $.maski.definitions['!']='[0-5]';                                  
    $(".time").maski("~9   !9",{placeholder:" "});
    //$(".phone1").maski("999    999   99   99",{placeholder:" "});
});                                                    