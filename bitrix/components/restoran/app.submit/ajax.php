<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

function check_phone_format($PHONE){
	if(substr($PHONE, 0,1)=="8"){
		$PHONE = substr_replace($PHONE, "+7", 0, 1);
	}
	return $PHONE;
}


function submit(){
	$EXIT=array();
	    
    $EMAIL=$_REQUEST["email"];
	$PHONE=$_REQUEST["tel"];
	
	$module_id = 'unisender.integration';
	
	$LIST_ID=7342138;
	
	require_once $_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/unisender.integration/include.php";
	$API_KEY = COption::GetOptionString($module_id, "UNISENDER_API_KEY");
	
	$API = new UniAPI($API_KEY);
	
	$PHONE=check_phone_format($PHONE);
	
	$params = array();
	$params['double_optin']=1;
	
	$i=0;
	if($EMAIL!="") {$params['field_names['.$i.']'] = 'email';$i++;}
	
	$params['field_names['.$i.']'] ='Name';$i++;
	$params['field_names['.$i.']'] = "email_list_ids";$i++;
	
	if($PHONE!=""){
		$params['field_names['.$i.']'] = "phone_list_ids";$i++;
		$params['field_names['.$i.']'] = "phone";$i++;
	}
	
	$data = array();
	$i=0;
	if($EMAIL!="") {$data["data[0][".$i."]"] = $EMAIL;$i++;}
	
	$data["data[0][".$i."]"] = "";$i++;
	$data["data[0][".$i."]"] = $LIST_ID;$i++;
	
	if($PHONE!=""){
		$data["data[0][".$i."]"] = $LIST_ID;$i++;
		$data["data[0][".$i."]"] = $PHONE;
	}
	
	
	$params = array_merge($params, $data);
	
	//$params['data'][] = $data;
	
	
	
	//var_dump($params);
	
	if (($res = $API->importContacts($params))!==false){
		unset($params['data']);
		$params['data'] = array();
		$EXIT["STATUS"]="ok";
		if($PHONE!="") {
			$EXIT["TEXT"]="Ссылка на приложение отправлена на номер ".$PHONE."<br/>До встречи в приложении! Restoran.ru";
			
			if (CModule::IncludeModule("sozdavatel.sms")){  
				$message = "Скачайте бесплатное приложение Restoran.ru и получайте деньги за бронирования https://appsto.re/ru/yYEdfb.i";  
				CSMS::Send($message, $PHONE, "UTF-8");  
			} 
		
		}elseif($EMAIL!="") {
			$EXIT["TEXT"]="Ссылка на приложение отправлена на e-mail ".$EMAIL."<br/>До встречи в приложении! Restoran.ru"; 
			
		   $arEventFields = array(
		   		"EMAIL" => $EMAIL,
		   	);  
			CEvent::Send("APP_LINK", "s1", $arEventFields,"Y",159);
			
		}
	}else {
		$EXIT["STATUS"]="error";
		$EXIT["TEXT"]="Ошибка: ".$API->showError();
		
		
		
	}

    echo json_encode($EXIT);
    
    
};


if($_REQUEST["action"]!="") $_REQUEST["action"]();

?>    