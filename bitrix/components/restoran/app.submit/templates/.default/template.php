<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="clear"></div>
 <script>
        var app_submit_core = "<?=$component->__path?>/ajax.php";
    </script>

<div id="app_anons">
            <div class="cent">
                <div class="ttl"><span>Приложение Restoran.ru</span></div>
                <div class="soon">в вашем iPhone</div>

                <div class="txt">Станьте первыми, кто получит ссылку на скачивание<br/>
                Укажите Ваш <a href="#app_email" class="active">email</a> или <a href="#app_tel">номер телефона</a></div>

                <div class="forms">
                    <form action="#" method="post" id="app_email" style="display: block;">
                    	<input type="hidden" name="action" value="submit"/>
                        <input type="text" name="email" class="pole ob" placeholder="Ваша электронная почта"/>
                        <input type="submit" name="s" value="отправить" class="but"/>
                        <div class="otvet"></div>
                    </form>

                    <form action="#" method="post" id="app_tel">
                    	<input type="hidden" name="action" value="submit"/>
                        <input type="text" name="tel" class="pole ob" placeholder="+7 (___) ___-__-__"/>
                        <input type="submit" name="s" value="отправить" class="but"/>
                        <div class="otvet"></div>
                    </form>
                </div>

                <div class="icn_blocks">

                    <div class="bl map">Удобный доступ<br/>
                ко всем ресторанам<br/>
                <b>вашего города</b></div>

                    <div class="bl tbl">Бронируйте столики<br/>
                и получайте <b>деньги<br/>
                на телефон</b></div>
                    <div class="clear"></div>

                    <div class="bl plate">Реальные <b>отзывы<br/>
                и фотографии</b><br/>
                пользователей</div>

                    <div class="bl app_icon"><img src="<?=$templateFolder?>/img/app_icon.png"/></div>

                </div>

            </div>
        </div>
