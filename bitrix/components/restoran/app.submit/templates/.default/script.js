$(document).ready(function() {
	
	$("BODY").on("click", "#app_anons .txt a:not(.active)",function(){
		
		var blid = $(this).attr("href");
		
		$("#app_anons .txt a").removeClass("active");
		$(this).addClass("active");
		
		$("#app_anons .forms form").hide();
		$(""+blid+"").show();
		
		return false;
	});
	
	
	$("#app_anons .forms form").submit(function(){
		var form = $(this);
		var val = form.find(".pole").val();
		if(val==""){
			form.find(".pole").addClass("error");
			
		}else{
			form.find(".pole").removeClass("error");
			
			
			$.post(app_submit_core, form.serialize(), function(otvet){
				
				if(otvet.STATUS=="ok"){
					form.find(".otvet").html(otvet.TEXT);
				}else{
					form.find(".otvet").html(otvet.TEXT);
				}
				
			},"json");
			
		}
		
		
		return false;
	});
	
	  $("#app_anons form input[name=tel]").maski("+7 (999) 999-99-99",{placeholder:"_"});
	
});
