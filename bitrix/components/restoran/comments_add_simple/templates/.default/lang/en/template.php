<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Thanks, Your comment has been saved.";
$MESS ["NEW_USER"] = "<br />You have successfully logged in to the site. We sent a email, with which you can complete your registration online.";
$MESS ["ONLY_AUTHORIZED"] = "Only authorized users can leave comments.";
$MESS ["AUTHORIZE"] = "Please, log in";
?>