<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$obCache = new CPHPCache;

$KEY="11123sfs22";
$life_time = 60*60*2; 
$cache_id = $arParams["Q"].$arParams["CITY_ID"].$KEY; 


if($obCache->InitCache($life_time, $cache_id, "search_redirect")){
	$vars = $obCache->GetVars();
    $LINK = $vars["LINK"];
}else{
	CModule::IncludeModule("iblock");        
	if ($arParams["Q"])
        {
            $arFilter = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>2467, "IBLOCK_TYPE"=>"system", "NAME"=>$arParams["Q"], "SECTION_CODE"=>$arParams["CITY_ID"]);
            $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter,false,false, array("DETAIL_TEXT", "NAME","ID", "SORT"));
            if($arResult = $res->GetNext()){

                    $LINK = trim($arResult["~DETAIL_TEXT"]);


            }
            else $LINK="";
        }
	else $LINK="";
	if($obCache->StartDataCache()){
		$obCache->EndDataCache(array("LINK"=>$LINK)); 
	}
}
//var_dump($LINK);

if($LINK!="") LocalRedirect($LINK, true, "301 Moved permanently");
?>
