<script src="<?=$templateFolder?>/chosen.jquery.js"></script>
<link href="<?=$templateFolder?>/chosen.css" type="text/css" rel="stylesheet" />
<link href="<?=$templateFolder?>/style.css" type="text/css" rel="stylesheet" />
<div align="right">
    <a class="modal_close white" href="javascript:void(0)"></a>
</div>
<div style="margin-top:-24px;margin-right:30px;">
    <h4 style="color:#FFF;">Добавление заведения</h4>
</div>
<div class="ajax_form" id="add_rest_box">
    <form action="" id="add_rest12">   
        <input type="hidden" name="prefix" value="<?=$arResult["PREFIX"]?>" />
        <input type="hidden" name="type" value="<?=$arParams["IBLOCK_TYPE"]?>" />
        <div class="question">
            Выберите город<br />
            <select name="<?=$arResult["PREFIX"]?>[city]" id="city123" class="chzn-select" style="width:300px;">
                <?foreach($arResult["CITIES"] as $city):?>
                    <option <?=($city["CODE"]==CITY_ID)?"selected":""?> value="<?=$city["ID"]?>"><?=$city["NAME"]?></option>
                <?endforeach;?>
            </select>                    
        </div>
        <div class="question">
            Название фирмы<br />
            <input name="<?=$arResult["PREFIX"]?>[rest]" type="text" value="" class="inputtext" size="60" />
        </div>
        <div class="question">
            Телефон фирмы<br />
            <input name="<?=$arResult["PREFIX"]?>[phone]" type="text" value="" id="phone123" class="inputtext" size="60"  />
        </div>                                    
        <div class="clear"></div>
        <!--<p style="font-style:italic">Внимательно заполните все поля формы. Администраторы сайта свяжутся с Вами для подтверждения введенных данных, после чего Вы сможете редактировать информацию о своем ресторане.</p>-->
        <div align="center">
            <input type="submit" id="add_rest_button"  value="Добавить" class="light_button" />
        </div>
    </form>  
</div>
<script>
        $("#city123").chosen();
        $("#phone123").maski("(999) 999-99-99");
        $("#contact_phone").maski("(999) 999-99-99");
</script>