<script src="<?=$templateFolder?>/script.js"></script>
<script src="<?=$templateFolder?>/chosen.jquery.js"></script>
<link href="<?=$templateFolder?>/chosen.css" type="text/css" rel="stylesheet" />
<link href="<?=$templateFolder?>/style.css" type="text/css" rel="stylesheet" />
<div align="right">
    <a class="modal_close white" href="javascript:void(0)"></a>
</div>
<div style="margin-top:-24px;margin-right:30px;">
    <h4 style="color:#FFF;">Добавление заведения</h4>
</div>
<div class="ajax_form" id="add_rest_box">
    <form action="" id="add_rest12">
        <input type="hidden" name="prefix" value="<?=$arResult["PREFIX"]?>" />
        <input type="hidden" name="type" value="<?=$arParams["IBLOCK_TYPE"]?>" />
        <div class="left">
            <h3>Данные ресторана</h3>
            <div class="question">
                Выберите город<br />
                <select name="<?=$arResult["PREFIX"]?>[city]" id="city123" class="chzn-select" style="width:300px;">
                    <?foreach($arResult["CITIES"] as $city):?>
                        <option <?=($city["CODE"]==CITY_ID)?"selected":""?> value="<?=$city["ID"]?>"><?=$city["NAME"]?></option>
                    <?endforeach;?>
                </select>                    
            </div>
            <div class="question">
                Название ресторана<br />
                <input name="<?=$arResult["PREFIX"]?>[rest]" type="text" value="" id="add_rest_name" class="inputtext" size="60" />
            </div>
<!--            <div class="question">
                Телефон ресторана<br />
                <input name="<?=$arResult["PREFIX"]?>[phone]" type="text" value="" id="phone123" class="inputtext" size="60"  />
            </div>                    -->
            <div class="question">
                Адрес ресторана<br />
                <input name="<?=$arResult["PREFIX"]?>[address]" type="text" value="" id="adress" class="inputtext" style="width:240px;"  />
                <script>
                    $ar = Array("обл ","р-н ","пл ","г ","пос ","ш ","ул ","пр ","пер ","наб ");
    $ar2 = Array("область ","район ","площадь ","город ","поселок ","шоссе ","улица ","проспект ","переулок ","набережная ");
                    $(document).ready(function(){
                       $("#adress").autocomplete("/search/map_suggest.php", {
                            delay:600,
                            limit: 5,
                            minChars: 3,
                            cacheLength:5,
                            width:970,
                            selectOnly:true,
                            formatItem: function(data, i, n, value) {                                            
                                var string = value.split("###")[0].replace("Россия","");
                                /*string = string.replace("площадь","пл.");
                                string = string.replace("шоссе","ш.");
                                string = string.replace("улица","ул.");
                                string = string.replace("проспект","пр.");
                                string = string.replace("переулок","пер.");
                                string = string.replace("набережная","наб.");*/
                                return "<a class='suggest_res_url' href='javascript:void(0)'> " + string + "</a>";
                            },
                            onItemSelect: function(value,a,b,c) {
                                var a = value.split("###")[1];                                
                            },
                            formatResult: function(data, value) { 
                                var string = value.split("###")[0].replace("Россия","");
                                string = string.replace("площадь","пл.");
                                string = string.replace("шоссе","ш.");
                                string = string.replace("улица","ул.");
                                string = string.replace("проспект","пр.");
                                string = string.replace("переулок","пер.");
                                string = string.replace("набережная","наб.");
                                return string;
                            },
                            extraParams: {
                                search_in: function() {return "adres"}          
                            }
                        }); 
                    });
                </script>                
                дом <input class="inputtext" name="<?=$arResult["PREFIX"]?>[house]" size="10" style="width:30px;" />
            </div>
        </div>
        <div class="right">
            <h3>Контактные данные пользователя</h3>
            <div class="question">
                Контактное лицо<br />
                <input name="<?=$arResult["PREFIX"]?>[fio]" type="text" value="<?=$arResult["USER"]["LAST_NAME"]?> <?=$arResult["USER"]["NAME"]?>" class="inputtext" size="60" />
            </div>
            <div class="question">
                Телефон<br />
                <input name="<?=$arResult["PREFIX"]?>[contact_phone]" type="text" value="<?=$arResult["USER"]["PERSONAL_PHONE"]?>" id="contact_phone" class="inputtext" size="60" />
            </div>
            <div class="question">
                Email<br />
                <input name="<?=$arResult["PREFIX"]?>[email]" type="text" value="<?=$arResult["USER"]["EMAIL"]?>" class="inputtext" size="60" />
            </div>        
        </div>
        <div class="clear"></div>
        <p style="font-style:italic">Внимательно заполните все поля формы. Администраторы сайта свяжутся с Вами для подтверждения введенных данных, после чего Вы сможете редактировать информацию о своем ресторане.</p>
        <div align="center">
            <input type="submit" id="add_rest_button"  value="Отправить" class="light_button" />
        </div>
    </form>  
</div>
<script>
        $("#city123").chosen();
        $("#phone123").maski("(999) 999-99-99");
        $("#contact_phone").maski("(999) 999-99-99");
</script>