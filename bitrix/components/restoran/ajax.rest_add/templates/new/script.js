$(document).ready(function(){                
    $("#add_rest12").submit(function(){
        var params = $("#add_rest12").serialize();
        console.log(params);
        if ($("#add_rest_name").val())
        {
            $.ajax({
                type: "POST",
                data: params,
                dataType: "html",
                url: "/bitrix/components/restoran/ajax.rest_add/ajax.php",
                success: function(data){
                    $("#add_rest_box").html(data);
                }            
            });
        }
        else
        {
            alert("Введите название ресторана");
        }
        return false;
    });
});
