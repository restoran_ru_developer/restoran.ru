<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Thanks, Your comment has been saved.";
$MESS ["NEW_USER"] = "<br />You have successfully logged in to the site. We sent a email, with which you can complete your registration online.";
$MESS ["ONLY_AUTHORIZED"] = "Only authorized users can leave comments.";
$MESS ["AUTHORIZE"] = "Please, log in";

$MESS ["AJAX_REST_ADD_ADD_REST_TITLE"] = "Adding places";
$MESS ["AJAX_REST_ADD_NAME_TITLE"] = "Restaurant name";
$MESS ["AJAX_REST_ADD_REST_ADDRESS_TITLE"] = "Restaurant address";
$MESS ["AJAX_REST_ADD_HOUSE_TITLE"] = "house";
$MESS ["AJAX_REST_ADD_TITLE"] = "Add";
?>