<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Спасибо, Ваш комментарий сохранен.";
$MESS ["NEW_USER"] = "<br />Вы успешно авторизованы на сайте. На указанный email выслано письмо, с помощью которого вы можете завершить регистрацию на сайте.";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут оставлять комментарии.";
$MESS ["AUTHORIZE"] = "Пожалуйста, авторизуйтесь на сайте.";


$MESS ["AJAX_REST_ADD_ADD_REST_TITLE"] = "Добавление заведения";
$MESS ["AJAX_REST_ADD_NAME_TITLE"] = "Название ресторана";
$MESS ["AJAX_REST_ADD_REST_ADDRESS_TITLE"] = "Адрес ресторана";
$MESS ["AJAX_REST_ADD_HOUSE_TITLE"] = "дом";
$MESS ["AJAX_REST_ADD_TITLE"] = "Добавить";


?>