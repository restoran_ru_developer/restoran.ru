<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arParams["IBLOCK_TYPE"] = ($arParams["IBLOCK_TYPE"])?trim($arParams["IBLOCK_TYPE"]):$_SESSION["ADD_REVIEW"]["IBLOCK_TYPE"];
$arParams["ELEMENT_ID"] = ($arParams["ELEMENT_ID"])?intval($arParams["ELEMENT_ID"]):$_SESSION["ADD_REVIEW"]["ELEMENT_ID"];
$arParams["IBLOCK_ID"] = ($arParams["IBLOCK_ID"])?intval($arParams["IBLOCK_ID"]):$_SESSION["ADD_REVIEW"]["IBLOCK_ID"];
$error = array();
$arr = array();
if(!CModule::IncludeModule("iblock"))
{
    $this->AbortResultCache();
    echo GetMessage("IBLOCK_MODULE_NOT_INSTALLED");
    die;
}
if (!$USER->IsAuthorized())
{
    $this->AbortResultCache();
    echo GetMessage("USER_NOT_AUTHORIZED");
    die;
}
if (count($error)>0)
{
    $arResult["STATUS"] = 0;
    $arResult["ERROR"] = implode("<br />",$error);
}
if ($arParams["AJAX"]=="Y")
{    
    if (count($_POST[$arParams["PREFIX"]])>0)
    {
        $arParams["IBLOCK_ID"] = (int)$_POST[$arParams["PREFIX"]]["city"];
        $re = CIBlock::GetByID($arParams["IBLOCK_ID"]);
        if ($c = $re->Fetch())
        {
            $arParams["CITY"] = $c["NAME"];
        }
        $arParams["NAME"] = $_POST[$arParams["PREFIX"]]["rest"];
        $arParams["ADDRESS"] = $_POST[$arParams["PREFIX"]]["address"].", д.".$_POST[$arParams["PREFIX"]]["house"];
        $arParams["ADDRESS2"] = $_POST[$arParams["PREFIX"]]["address"].", д. ".$_POST[$arParams["PREFIX"]]["house"];
        //find rest
        
        $filter = Array(
            "IBLOCK_ID"=> $arParams["IBLOCK_ID"],
            Array("LOGIC"=>"OR",
                Array("NAME"=>$arParams["NAME"]),
                Array("TAGS"=>$arParams["NAME"]),
            ),
            "?PROPERTY_address" => $arParams["ADDRESS"]. "||" .$arParams["ADDRESS2"]
        );
        $r = CIBlockElement::GetList(Array(),$filter,false, array("nTopCount"=>1), Array("ID","NAME","PROPERTY_user_bind"));
        if ($ar = $r->GetNext())
        {
            $arResult["REST_FIND"] = 1;
            
//            #USER_ID#"
//            #USER_NAME#
//            #REST_NAME#
//            #RESTORATOR_ID#
//            #RESTORATOR#
            if ($ar["PROPERTY_USER_BIND_VALUE"])
            {
                $e = CUser::GetByID($par_el["CREATED_BY"]);
                if ($u = $e->Fetch())
                {
                    $restorator_id = $u["ID"];
                    $restorator = $u["LAST_NAME"]." ".$u["NAME"];
                }
            }
            $arEventFields = array(
                "USER_ID" => $USER->GetID(),
                "USER_NAME" => $USER->GetFullName(),
                "REST_NAME" => $arParams["NAME"], 
                "RESTORATOR_ID" => $restorator_id,
                "RESTORATOR" => $restorator,
                "CITY"=>$arParams["CITY"]
            );
            if ($arParams["IBLOCK_TYPE"]=="firms")
                CEvent::Send("ADD_FIRM", "s1", $arEventFields,false,121);
            else
                CEvent::Send("ADD_REST", "s1", $arEventFields,false,113);                
        }
        else
        {       
            $el = new CIBlockElement;
            $arResult["REST_FIND"] = 0;
            $PROP = array();
            $PROP["phone"] = $arParams["PHONE"];
            $PROP["user_bind"] = $USER->GetID();
            $PROP["address"] = $arParams["ADDRESS2"];
            $arLoadProductArray = Array(
                "MODIFIED_BY"    => $USER->GetID(),
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID"      => $arParams["IBLOCK_ID"],
                "PROPERTY_VALUES"=> $PROP,
                "NAME"           => $arParams["NAME"],
                "ACTIVE"         => "N",                
            );
            //if (CSite::InGroup(Array(14,15)))
            {
                $arLoadProductArray["ACTIVE_FROM"] = date("d.m.Y");
                $arLoadProductArray["ACTIVE_TO"] = date("d.m.Y",  strtotime("+180 days"));;
            }
            if($PRODUCT_ID = $el->Add($arLoadProductArray))
            {
                $arEventFields = array(
                    "USER_ID" => $USER->GetID(),
                    "USER_NAME" => $USER->GetFullName(),
                    "REST_NAME" => $arParams["NAME"], 
                    "REST_ID" => $PRODUCT_ID,                    
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ADDRESS" => $arParams["ADDRESS"],
                    "CITY"=>$arParams["CITY"]
                );
                 if ($arParams["IBLOCK_TYPE"]=="firms")
                    CEvent::Send("ADD_FIRM", "s1", $arEventFields,false,120);
                else
                    CEvent::Send("ADD_REST", "s1", $arEventFields,false,114);                                
            }
            else
            {
                echo $el->LAST_ERROR;
                exit;
            }
              //  $arResult["ERROR"] = "Error: ".$el->LAST_ERROR;
        }
    }
    /*if (!$arParams["REVIEW"])
        $error[] = GetMessage("REVIEW_ERROR");        
    if (count($error)>0)
    {
        $arResult["ERROR"] = implode("<br />",$error);
    }
    else
    {        
        $el = new CIBlockElement;
        $PROP = array();
        $PROP["ELEMENT"] = $arParams["ELEMENT_ID"];  
        if ($PROP["ELEMENT"])
        {
            $PROP["CITY_ID"] = CITY_ID;
        }
        if ($arParams["PARENT"])
        {
            $PROP["PARENT"] = (int)$arParams["PARENT"];  
            $rrr = CIBlockElement::GetByID((int)$arParams["PARENT"]);
            if ($par_el = $rrr->Fetch())
            {
                $e = CUser::GetByID($par_el["CREATED_BY"]);
                if ($u = $e->Fetch())
                {
                    $arEventFields = array(
                        "EMAIL" => $u["EMAIL"],
                        "URL" => $_SERVER["HTTP_REFERER"],
                        "NAME" =>  ($u["PERSONAL_PROFESSION"])?$u["PERSONAL_PROFESSION"]:$USER->GetLogin(), 
                        "COMMENT" => $par_el["PREVIEW_TEXT"],
                        "COMMENT2" => $arParams["REVIEW"],
                    );
                    CEvent::Send("NEW_COMMENT", "s1", $arEventFields,false,99);
                }
            }
        }
        if ($arParams["plus"])
            $PROP["plus"] = $arParams["plus"];  
            //$PROP["plus"] = Array("VALUE"=>array("TYPE"=>"text","TEXT"=>$arParams["plus"]));  
        if ($arParams["minus"])
            $PROP["minus"] = $arParams["minus"];  
            //$PROP["minus"] = Array("VALUE"=>array("TYPE"=>"text","TEXT"=>$arParams["minus"]));  
        
        if ($arParams["video_youtube"])
            $PROP["video_youtube"] = $arParams["video_youtube"];
        if ($arParams["photos"])
        {
            if(is_array($arParams["photos"]["name"])){
                $RET=array();
                foreach($arParams["photos"] as $N=>$V){
                        for($i=0;$i<count($V);$i++){
                                $RET[$i][$N]=$V[$i];
                        }
                }
            }
            $PROP["photos"] = $RET;
        }
        if ($arParams["video"])
            $PROP["video"] = $arParams["video"];
        $PROP["IS_SECTION"] = $arParams["IS_SECTION"];  
        $e = CUser::GetByID($USER->GetID());
        if ($u = $e->Fetch())
        {
            
        
            $arLoadProductArray = Array(  "MODIFIED_BY"    => $USER->GetID(), 
                                      "IBLOCK_ID"      => $arParams["IBLOCK_ID"],  
                                      "PROPERTY_VALUES"=> $PROP,  
                                      "ACTIVE_FROM"=> date("d.m.Y H:i:s"),  
                                      "NAME"  => ($u["PERSONAL_PROFESSION"])?$u["PERSONAL_PROFESSION"]:$USER->GetLogin(),  
                                      "ACTIVE" => "Y",
                                      "PREVIEW_TEXT"   => $arParams["REVIEW"]);
        }
        if ($arParams["RATIO"])
            $arLoadProductArray["DETAIL_TEXT"] = $arParams["RATIO"];
        if($PRODUCT_ID = $el->Add($arLoadProductArray))  
        {
            //echo "Спасибо, Ваш голос учтен";
            unset($_SESSION["ADD_REVIEW"]);
        }
        else  
        {
            $error = $el->LAST_ERROR;
            $arResult["ERROR"] = implode("<br />",$error);
            unset($_SESSION["ADD_REVIEW"]);
        }
    }   */
}
else
{
    if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), CITY_ID, $arParams["IBLOCK_TYPE"])))
    {
        $res = CIBlock::GetList(Array("SORT"=>"ASC"),Array("TYPE"=>$arParams["IBLOCK_TYPE"],"SITE_ID"=>SITE_ID));
        while ($ar = $res->Fetch())
        {
            $arResult["CITIES"][] = Array("ID"=>$ar["ID"],"CODE"=>$ar["CODE"],"NAME"=>$ar["NAME"]);
        }
        $usr = CUser::GetByID($USER->GetID());
        if ($ar_usr = $usr->Fetch())
        {
            $arResult["USER"] = $ar_usr;
        }
        $arResult["PREFIX"] = $arParams["PREFIX"];
        $this->SetResultCacheKeys(array(
                "CITIES",
                "USER",
                "PREFIX",
        ));
    }
    
}
$this->IncludeComponentTemplate();    
?>