<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$APPLICATION->IncludeComponent(
        "restoran:ajax.rest_add",
        "ajax",
        Array(
                "AJAX" => "Y",
                "PREFIX" => $_REQUEST["prefix"],
                "IBLOCK_TYPE" => $_REQUEST["type"],
                "CACHE_TYPE" => "N"
        ),false
    );
?>
<?require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>