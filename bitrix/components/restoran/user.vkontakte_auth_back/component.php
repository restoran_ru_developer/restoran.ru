<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// TODO add mail send about user add

$arParams["APP_ID"] = COption::GetOptionString("socialservices", "vkontakte_appid");

if(!CModule::IncludeModule("socialservices")) {
	ShowError(GetMessage("SOCIALSERVICE_MODULE_NOT_INSTALLED"));
	return;
}

if($arParams["APP_ID"] <= 0){
    ShowError(GetMessage("SET_APP_ID"));
    return;
}


$this->IncludeComponentTemplate();
?>