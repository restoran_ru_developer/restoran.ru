<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/tpl/js/jquery.checkbox.js"></script>
<script>
    $(document).ready(function(){
        $(".select_all").toggle(
            function(){
                $(this).parents(".extended_filter").find(".niceCheck").each(function(){
                    setCheck($(this));
                });                
                $(this).find("a").html("<?=GetMessage("UNSELECT_ALL")?>");
                
            },
            function(){
                //unsetCheck($(this).parents(".extended_filter").find(".niceCheck"));
                $(this).parents(".extended_filter").find(".niceCheck").each(function(){
                    unsetCheck($(this));
                });                
                $(this).find("a").html("<?=GetMessage("SELECT_ALL")?>");              
                
            });    
        });

    /*function success_a(position) {
        link = "/bitrix/components/restoran/metro.list/ajax.php?q=near&template=checkbox&lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&<?=bitrix_sessid_get()?>";    
        $("#metro_list").load(link);                                                    
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success_a, error_a);
        } else {
            alert("Ваш браузер не поддерживает\nопределение местоположения");
            return false;
        }                            
    }*/
</script>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/all/" method="GET">
    <input type="hidden" name="IBLOCK_SECTION_ID" value="32<?=(int)$arParams['SECTION']?>" />
    <input type="hidden" name="page" value="1" />
    <?
    foreach($arResult["arrProp"] as $prop_id => $arProp)
    {
            $res = "";
            $arResult["arrInputNames"][$FILTER_NAME."_pf"]=true;
            if ($arProp["CODE"]!="wi_fi"&&$arProp["CODE"]!='subway'&&$arProp["CODE"]!='subway_dostavka'&&$arProp["CODE"]!="hrs_24")
            {
                if ($arProp["PROPERTY_TYPE"]=="L" || $arProp["PROPERTY_TYPE"]=="E")
                {
                    $name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
                            $value = $arrPFV[$arProp["CODE"]];
                            $res = "";
                            if ($arProp["MULTIPLE"]=="Y")
                            {
                                    if (is_array($value) && count($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
                            else
                            {
                                    if (!is_array($value) && strlen($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
            ?>
                            <h2><?=GetMessage("PROPERTY_NAME_".$arProp["CODE"])?></h2>
                            <div class="extended_filter">                                    
                                <?
                                $prop_count = ceil(count($arProp["VALUE_LIST"])/4);
                                $prop_i = 0;
                                foreach($arProp["VALUE_LIST"] as $key=>$val):
                                    if (($arProp["MULTIPLE"] == "Y") && is_array($value))
                                    {
                                            if(in_array($key, $value))
                                                    $res = ' checked';
                                    }
                                    else
                                    {
                                            if($key == $value)
                                                    $res = ' checked';
                                    }
                                    if (($prop_i >= $prop_count && $prop_i%$prop_count == 0) || $prop_i == 0)
                                    {
                                        $aaa = ($prop_i>=($prop_count*3))?"0px":"10px";
                                        echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
                                    }?>
                                    <?if ($prop_i==0):?>
                                    <!--<tr class="select_all">
                                        <td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="" name="" id="" /></span></td>
                                        <td class="option2"><label class="niceCheckLabel uppercase"><?=GetMessage("SELECT_ALL")?></label></td>                                       
                                    </tr>-->
                                    <?$prop_id++;?>
                                    <?endif;?>
                                    <?echo '<tr><td style="padding: 5px 0px;padding-right:5px">';
                                    echo '<span class="niceCheck"><input type="checkbox" '.$res.' value="'.htmlspecialchars($key).'" name="'.$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]" id="type'.htmlspecialchars($key).'" /></span>';
                                    echo '</td><td class="option2"><label class="niceCheckLabel">'.$val.'</label></td>';
                                    if ($prop_i%$prop_count == ($prop_count-1) && $prop_i>0) 
                                        echo '</table></div>';    
                                    if (end($arProp["VALUE_LIST"]) == $val && $prop_i%$prop_count != ($prop_count-1)):    
                                        echo '</table></div>';
                                    endif;
                                    $prop_i++;
                                endforeach    
                                ?>
                            </div>
                            <div class="clear"></div>
                            <div class="dotted"></div>
                            
            <?             
                    
                }
            }
            elseif ($arProp["CODE"]=="subway")
            {                
            ?>
                <h2><?=GetMessage("PROPERTY_NAME_".$arProp["CODE"])?></h2>
                <div class="extended_filter">    
                    <!--<div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>-->
                    <div id="metro_list">
                    <?$APPLICATION->IncludeComponent(
                                "restoran:metro.list",
                                "checkbox",
                                Array(
                                        "q" => 'abc',
                                        "lat" => (double)$_REQUEST["lat"],
                                        "lon" => (double)$_REQUEST["lon"],
                                        "CACHE_TYPE" => "N",
                                        "CACHE_TIME" => "360000"			
                                ),
                                false
                        );?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="dotted"></div>
            <?
            }
            elseif ($arProp["CODE"]=="subway_dostavka")
            {                
            ?>
                <h2><?=str_replace(" - Доставка","",$arProp["NAME"])?></h2>
                <div class="extended_filter">    
                    <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
                    <div id="metro_list">
                    <?$APPLICATION->IncludeComponent(
                                "restoran:metro.list",
                                "checkbox",
                                Array(
                                        "q" => 'abc',
                                        "lat" => (double)$_REQUEST["lat"],
                                        "lon" => (double)$_REQUEST["lon"],
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "360000"			
                                ),
                                false
                        );?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="dotted"></div>
            <?
            }
            elseif ($arProp["CODE"]=="wi_fi"||$arProp["CODE"]=="hrs_24")
            {
            ?>
                <div class="right">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck"><input type="checkbox" value="y" name="arrFilter_pf[wi_fi]" /></span>
                            </td>
                            <td class="option2 font14" width="100"><label><?=$arProp["NAME"]?></label></td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
            <?
            }
            switch ($arProp["PROPERTY_TYPE"])
            {
                    
                    case "N":
                            /*$value = $arrPFV[$arProp["CODE"]];
                            $name = $FILTER_NAME."_pf[".$arProp["CODE"]."][LEFT]";
                            if(is_array($value) && isset($value["LEFT"]))
                                    $value_left = $value["LEFT"];
                            else
                                    $value_left = "";
                            $res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value_left).'" />&nbsp;'.GetMessage("CC_BCF_TILL").'&nbsp;';

                            if (strlen($value_left) > 0)
                                    ${$FILTER_NAME}["PROPERTY"][">=".$arProp["CODE"]] = doubleval($value_left);

                            $name = $FILTER_NAME."_pf[".$arProp["CODE"]."][RIGHT]";
                            if(is_array($value) && isset($value["RIGHT"]))
                                    $value_right = $value["RIGHT"];
                            else
                                    $value_right = "";
                            $res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value_right).'" />';

                            if (strlen($value_right) > 0)
                                    ${$FILTER_NAME}["PROPERTY"]["<=".$arProp["CODE"]] = doubleval($value_right);

                            break;*/
                    case "S":
                    case "G":
                            /*$name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
                            $value = $arrPFV[$arProp["CODE"]];
                            if(!is_array($value))
                            {
                                    $res .= '<input type="text" name="'.$name.'" size="'.$arParams["TEXT_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

                                    if (strlen($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"]["?".$arProp["CODE"]] = $value;
                            }*/
                            break;
            }
            /*if($res)
                $arResult["ITEMS"][] = array(
                        "NAME" => htmlspecialchars($arProp["NAME"]),
                        "INPUT" => $res,
                        "INPUT_NAME" => $name,
                        "INPUT_VALUE" => is_array($value)? array_map("htmlspecialchars", $value): htmlspecialchars($value),
                        "~INPUT_VALUE" => $value,
                );*/
    }
?>
	<?foreach($arResult["ITEMS"] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
			echo $arItem["INPUT"];
		endif;
	endforeach;?>
                <br />
        <div class="button right">
            <input style="width:168px" class="light_button" type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />    
            <input type="hidden" name="set_filter" value="Y" />          
        </div> 
        <div class="clear"></div>
</form>
