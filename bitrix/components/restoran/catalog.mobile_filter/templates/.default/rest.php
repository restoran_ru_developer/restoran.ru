<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$arRestIB = getArIblock("catalog", CITY_ID);
/*if (CITY_ID=="msk")
    $prop = array("type", "subway", "out_city", "administrative_distr", "average_bill", "kitchen", "credit_cards","menu","children", "proposals", "features","entertainment","ideal_place_for","music","parking","wi_fi","hrs_24");
else
    $prop = array("type", "subway", "out_city", "area", "average_bill", "kitchen", "credit_cards","menu","children", "proposals", "features","entertainment","ideal_place_for","music","parking","wi_fi","hrs_24");*/
if (CITY_ID=="msk")
    $prop = array("type", "subway", "out_city", "administrative_distr", "average_bill", "kitchen","menu","children", "proposals", "features","entertainment","music","ideal_place_for","wi_fi","hrs_24");
else
    $prop = array("type", "subway", "out_city", "area", "average_bill", "kitchen","menu","children", "proposals", "features","entertainment","music","ideal_place_for","wi_fi","hrs_24");
$_REQUEST["CATALOG_ID"]="restaurants";
$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => $prop,
		"PRICE_CODE" => array(),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000",
		"CACHE_GROUPS" => "N",
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"SAVE_IN_SESSION" => "N",
        "SECTION" => 32
	)
);?>
