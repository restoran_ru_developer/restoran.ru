<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script>
$(document).ready(function(){
    jQuery(".niceCheck").click(function() {
            changeCheck(jQuery(this));
        });

        jQuery(".niceCheckLabel").click(function() {
            changeCheck(jQuery(this).parent().parent().find(".niceCheck"));
        }); 
})    
</script>
   <?
$arRestIB = getArIblock("catalog", CITY_ID);
$_REQUEST["CATALOG_ID"]="banket";
if (CITY_ID=="msk")
    $prop = array("type", "subway", "out_city", "administrative_distr", 'kolichestvochelovek',"kitchen","menu","children", "features","entertainment","ideal_place_for","wi_fi","hrs_24",'BANKET_MENU_SUM','ALLOWED_ALCOHOL','BANKET_SPECIAL_EQUIPMENT','BANKET_ADDITIONAL_OPTION');
else
    $prop = array("type", "subway", "out_city", "area", 'kolichestvochelovek',"kitchen","menu","children", "features","entertainment","ideal_place_for","wi_fi","hrs_24",'BANKET_MENU_SUM','ALLOWED_ALCOHOL','BANKET_SPECIAL_EQUIPMENT','BANKET_ADDITIONAL_OPTION');

   $APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(),
                "PROPERTY_CODE" => $prop,
		"PRICE_CODE" => array(),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000",
		"CACHE_GROUPS" => "Y",
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"SAVE_IN_SESSION" => "N",
        "SECTION" => 33
	)
);?>
