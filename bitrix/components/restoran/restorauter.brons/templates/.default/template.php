<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.form.js"></script>

<script>
    $(function () {
        $("ul.tabs").each(function () {
            if ($(this).attr("ajax") == "ajax") {
                if ($(this).attr("history") == "true")
                    var history = true; else
                    var history = false;
                var url = "";
                if ($(this).attr("ajax_url"))
                    url = $(this).attr("ajax_url");
                $(this).tabs("div.panes > .pane", {
                    history: history, onBeforeClick: function (event, i) {
                        var pane = this.getPanes().eq(i);
                        if (pane.is(":empty")) {
                            pane.load(url + this.getTabs().eq(i).attr("href"));
                        }
                    }
                });
            }
            else {
                if ($(this).attr("history") == "true")
                    var history = true; else
                    var history = false;
                $(this).tabs("div.panes  .pane", {
                    history: history, onClick: function (event, i) {
                        $(".bx-yandex-map").each(function () {
                            var i = $(this).attr("id");
                            i = i.split('BX_YMAP_');
                            m = window.GLOBAL_arMapObjects[i[1]];
                            if (m)m.container.fitToViewport();
                        });
                    }
                });
            }
        });
    })
</script>
<style>
    .panes {min-height:150px;}
    .panes div.pane { display:none; min-height:150px; padding:15px 0px;  margin-top:-3px; width:728px; line-height: 20px}
    .panes div span.date_size{ font-size: 1.6em}
    .panes div img{ margin-right:10px;margin-bottom:10px;}
    .poster_panes div img{ margin-right:10px;margin-bottom:10px;}
    .panes div a { color:#24a6cf; text-decoration: underline;}
    .panes div a:hover { color:#000; text-decoration: none;}
    .panes div a.uppercase { color:#1a1a1a; letter-spacing: 1px; text-decoration: none; }
    .panes div a.uppercase:hover { color:#24a6cf; text-decoration: none; }

    ul.tabs { margin:0 !important; padding:0; height:41px; width:728px;border-bottom:1px solid #000;}
    ul.tabs li { float:left; padding:0; margin:0; list-style-type:none;}
    ul.tabs li a {
        padding: 8px 36px;
        font-size: 16px;
        font-family: 'futurafuturiscregular';
        -webkit-text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
        -moz-text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
        text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
        margin-right: 2px;
        line-height: 1.42857143;
        border: 1px solid transparent;
        background: #f3f6f7;
        position: relative;
        display: block;
    }
    ul.tabs a.current {
        color: #000;
        background-color: #ffffff;
        border: 1px solid #000;
        border-top-width: 3px;
        border-bottom-color: transparent;
        border-bottom: 0px;
        cursor: default;
    }
    .more_cities {
        padding:0px 10px;
    }
</style>


<div class="wrap-div">

        <ul class="tabs" style="width: 100%;  margin-bottom: 28px !important;">
            <?foreach($arResult["RESTAURANTS"] as $rest):?>
            <li>
                <a href="<?=$rest["CODE"]?>" rel="<?=$rest["ID"]?>">
                    <div class="left tab_left"></div>
                    <div class="left name" id="rest_tab_<?=$rest["CODE"]?>"><?=$rest["NAME"]?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <?endforeach?>
        </ul>
        
    
        
        <div class="panes">
            <?foreach($arResult["RESTAURANTS"] as $rest):?>
            	
            <div class="pane" style="width: 100%;">
            	
           		
           
                <?
                global $arrFilter;
                $arrFilter["PROPERTY_rest"]=$rest["ID"];
                $APPLICATION->IncludeComponent(
	"bitrix:catalog.section_notact",
	"bron_stat",
	Array(
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "booking_service",
		"IBLOCK_ID" => "105",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => "",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"PAGE_ELEMENT_COUNT" => "20",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => array("cat"),
		"PRICE_CODE" => "",
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => "",
		"USE_PRODUCT_QUANTITY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Публикации",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?>
            </div>
            <?endforeach?>
        </div>
</div>