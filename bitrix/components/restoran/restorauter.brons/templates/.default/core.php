<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_rest(){
	global $USER;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
		
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $_REQUEST["IBLOCK_SECTION_ID"],          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"NAME"           => $_REQUEST["NAME"],
  		"ACTIVE"         => "Y",            
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"CODE"=>translitIt($_REQUEST["NAME"])
  	);
	
	//Основная фотка
	if(isset($_FILES["PREVIEW_PICTURE"])){
		$arLoadProductArray["PREVIEW_PICTURE"]=$_FILES["PREVIEW_PICTURE"];
	}	
	
	//var_dump($_REQUEST["PROP"]);
	if(!isset($_REQUEST["PROP"]["photos"])) $_REQUEST["PROP"]["photos"][]="";

	foreach($_REQUEST["PROP"] as $CODE=>$PR){
		if($CODE=="photos"){
			if($_FILES["NEW_PHOTO"]){
				$fid = CFile::SaveFile($_FILES["NEW_PHOTO"]);
				$PR[]=$fid;
			}
			
			$PR2=array();
				
			foreach($PR as $fid) $PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>"");
			$PR=$PR2;
		}
		CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array($CODE => $PR));
	}	

	//Проверяем права на запись
	if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");
	
	
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию	
	
		unset($arLoadProductArray["IBLOCK_ID"]);
		//var_dump($arLoadProductArray);
		$el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray);
	
	}else{
		//создаем новую публикацию
		
		
		
		if($ELEMENT_ID = $el->Add($arLoadProductArray))
  			echo $ELEMENT_ID;
		else
  			echo "Error: ".$el->LAST_ERROR;
	}
	
}




/***********ФОРМИРУЕМ МАССИВ ФАЙЛА**************/
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($_FILES);
	foreach($_FILES["block"] as $A=>$C){		
		$VAL="";
		if(is_array($C[$BL_NAME])){
			foreach($C[$BL_NAME] as $V) $VAL=$V;
		}
		$FAR[$A]=$V;
	}
	
	if(is_array($FAR["name"])){
		$RET=array();
		foreach($FAR as $N=>$V){
			for($i=0;$i<count($V);$i++){
				$RET[$i][$N]=$V[$i];
			}
		}
		//var_dump($RET);
		return $RET;
	}else return $FAR;
}

function save_dots(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	$D = explode(",", $_REQUEST["pts"]);
	$_REQUEST["pts"]=$D[1].",".$D[0];
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("map" => $_REQUEST["pts"]));
}



function add_new_rest(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	global $USER;
	
	$arRestIB = getArIblock("catalog", CITY_ID);

	$el = new CIBlockElement;

	$PROP = array();
	$PROP["user_bind"] = $USER->GetID();
	
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $arRestIB["ID"],
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => "Новый ресторан",
  		"ACTIVE"         => "N",            // активен
  	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray))
  		echo "ok";
	else
  		echo "Error: ".$el->LAST_ERROR;
	

}


if($_REQUEST["act"]=="edit_rest") edit_rest();
if($_REQUEST["act"]=="save_dots") save_dots();
if($_REQUEST["act"]=="add_new_rest") add_new_rest();

?>
