<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(!CModule::IncludeModule("iblock"))
    return;

// get restaurants iblock info
$arRestIB = getArIblock("catalog", CITY_ID);

if(!$arParams["USER_ID"])
    $arParams["USER_ID"] = $USER->GetID();


$rsRests = CIBlockElement::GetList(
    Array("SORT"=>"ASC"),
    Array(
        "IBLOCK_ID" => $arRestIB["ID"],
        "PROPERTY_user_bind" => $arParams["USER_ID"],
    ),
   	false,
    false,
    array_merge(Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "CODE", "NAME"), $arPropValueCode)
);



while($arRests = $rsRests->GetNext()) {
    $arTmpRest = Array();
 
    $arTmpRest["ID"] = $arRests["ID"];
    $arTmpRest["IBLOCK_ID"] = $arRests["IBLOCK_ID"];
    $arTmpRest["IBLOCK_SECTION_ID"] = $arRests["IBLOCK_SECTION_ID"];
    $arTmpRest["NAME"] = $arRests["NAME"];
    $arTmpRest["CODE"] = $arRests["CODE"];


    $arResult["RESTAURANTS"][] = $arTmpRest;
}


$this->IncludeComponentTemplate();
?>