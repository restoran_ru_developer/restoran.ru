<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("USER_FRIENDS_ADD_TITLE"),
	"DESCRIPTION" => GetMessage("USER_FRIENDS_ADD_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "users_restaurant",
        "NAME" => GetMessage("USERS_RESTAURANT"),
	),
	"COMPLEX" => "N",
);
?>