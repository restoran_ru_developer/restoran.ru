<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("socialnetwork")) {
    ShowError(GetMessage("SOCIALNETWORK_MODULE_NOT_INSTALL"));
    return;
}

$arParams["AJAX_CALL"] = $arParams["AJAX_CALL"] == "Y" ? "Y" : "N";
$arParams["RESULT_CONTAINER_ID"] = trim($arParams["RESULT_CONTAINER_ID"]);
$arParams["FIRST_USER_ID"] = intval($arParams["FIRST_USER_ID"]);
$arParams["SECOND_USER_ID"] = intval($arParams["SECOND_USER_ID"]);
$arParams["ACTION"] = trim($arParams["ACTION"]);

if(!$arParams["FIRST_USER_ID"] || !$arParams["SECOND_USER_ID"]) {
    ShowError(GetMessage("USER_ID_NOT_SETUP"));
    return;
}

$socRel = new CSocNetUserRelations;

// get user info
$rsUser = CUser::GetByID($arParams["SECOND_USER_ID"]);
$arUser = $rsUser->Fetch();

// resize photo
$arUser["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL, true);

// check user restorator stat
$arGroups = CUser::GetUserGroup($arParams["SECOND_USER_ID"]);
if(in_array(RESTORATOR_GROUP, $arGroups))
    $arUser["IS_RESTORATOR"] = true;

$arResult["USER_INFO"] = $arUser;

if($arParams["ACTION"] == "add_friend") {
    $arFieldsUsers = Array(
        "FIRST_USER_ID" => $arParams["FIRST_USER_ID"],
        "SECOND_USER_ID" => $arParams["SECOND_USER_ID"],
        "RELATION" => SONET_RELATIONS_REQUEST,//SONET_RELATIONS_FRIEND,
        "MESSAGE" => $arParams["GREETING_MSG"],
        "DATE_CREATE" => date("d.m.Y H:i:s"),
        "INITIATED_BY" => "F"
    );
    $arResult["REL_ID"] = $socRel->Add($arFieldsUsers);
    
    $ar_1 = CUser::GetByID($arParams["SECOND_USER_ID"]);
    if ($res=$ar_1->Fetch())
    {
        $email = $res["EMAIL"];
        $name = $res["NAME"];
    }
    
    $arEventFields = array(
        "EMAIL"=> $email,        
        "USER_NAME" => $name,
        "USER_FROM" => ($USER->GetFullName())?$USER->GetFullName():$USER->GetEmail(),        
    );  
    CEvent::Send("ADD_FRIEND", "s1", $arEventFields,"Y",126);
}
$arTmpParams = array(
    "RESULT_CONTAINER_ID" => $arParams["RESULT_CONTAINER_ID"],
    "FIRST_USER_ID" => $arParams["FIRST_USER_ID"],
    "SECOND_USER_ID" => $arParams["SECOND_USER_ID"],
);
$relation = CSocNetUserRelations::GetRelation($arParams["FIRST_USER_ID"], $arParams["SECOND_USER_ID"]);
$arResult["ALLREADY_FRIENDS"] = $relation;
$arResult["JS_PARAMS"] = CUtil::PhpToJsObject($arTmpParams);

$this->IncludeComponentTemplate();

if ($arParams["AJAX_CALL"] != "Y") {
    IncludeAJAX();
    $template =& $this->GetTemplate();
    $APPLICATION->AddHeadScript($template->GetFolder().'/proceed.js');
}
?>