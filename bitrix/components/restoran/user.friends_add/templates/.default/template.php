<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["ALLREADY_FRIENDS"]):?>
    <?$APPLICATION->IncludeComponent(
        "restoran:user.friends_delete",
        "",
        Array(
            "FIRST_USER_ID" => $arParams["FIRST_USER_ID"],
            "SECOND_USER_ID" => $arParams["SECOND_USER_ID"],
            "RESULT_CONTAINER_ID" => $arParams["RESULT_CONTAINER_ID"],
        ),
        false
    );?>
<?endif?>

<?if ($arParams["AJAX_CALL"] != "Y" && !$arResult["ALLREADY_FRIENDS"]):?>
    <div id="<?=$arParams["RESULT_CONTAINER_ID"]?>">
        <input class="light" type="button" onclick="addUser(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" value="<?=GetMessage("USER_ADD_FRIEND_BUTTON")?>" />
    </div>
<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["REL_ID"]):?>
    <h1><?=GetMessage("USER_ADD_FRIEND")?></h1>
<?endif?>