
<?

if ($arParams["PREVIEW_TRUNCATE_LEN"]) {
    $arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
    $obParser = new CTextParser;
}

$items = $arResult["ITEMS"];
$allUrl = $arParams["ALL_URL"];
$allTitle = $arParams["ALL_TITLE"];
$city = $arParams["CITY"];
$fileName = $arParams["FILE_NAME"];
$title = $arParams["BLOCK_TITLE"];
//v_dump($items);
for ($n = 0; $n < 3; $n++) {

    if ($arParams["PREVIEW_TRUNCATE_LEN"] > 0) {
        $items[$n]["PREVIEW_TEXT"] = $obParser->html_cut($items[$n]["PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
    }
    if (!$items[$n]["DATE_ACTIVE_FROM"]):
//        $date[$n] = $DB->FormatDate($items[$n]["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate[$n] = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($items[$n]["DATE_CREATE"]));//, CSite::GetDateFormat())
    else:
//        $date[$n] = $DB->FormatDate($items[$n]["DATE_ACTIVE_FROM"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate[$n] = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($items[$n]["DATE_ACTIVE_FROM"]));//, CSite::GetDateFormat();
    endif;
    $arTmpDate[$n] = explode(" ", $arTmpDate[$n]);
    $items[$n]["CREATED_DATE_FORMATED_3"] = $arTmpDate[$n][0] . " " . $arTmpDate[$n][1] . " " . $arTmpDate[$n][2];

    $m = Array("January","February","March","April","May","June","July","August","September","October","November","December");
    $m2 = Array("Января","Февраля","Марта","Апреля","Мая","Июня","Июля","Августа","Сентября","Октября","Ноября","Декабря");
    $items[$n]["CREATED_DATE_FORMATED_3"] = str_replace($m, $m2, $items[$n]["CREATED_DATE_FORMATED_3"]);

    if ($items[$n]["DETAIL_PICTURE"]):
        $file[$n] = CFile::ResizeImageGet($items[$n]["DETAIL_PICTURE"], array('width' => 200, 'height' => 120), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    endif;

}
?>

<?

$templateToWrite = '<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="'.$allUrl.'"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">'.$title.'</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru' . $items[0]["DETAIL_PAGE_URL"] . '" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru' . $file[0]["src"] . '">
                    </a>
                    <a href="http://www.restoran.ru' . $items[0]["DETAIL_PAGE_URL"] . '" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">' . $items[0]["NAME"] . '</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        ' . $items[0]["PREVIEW_TEXT"] . '
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">' . $items[0]["CREATED_DATE_FORMATED_3"] . '</span>
                    <a href="http://www.restoran.ru' . $items[0]["DETAIL_PAGE_URL"] . '" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru' . $items[1]["DETAIL_PAGE_URL"] . '" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru' . $file[1]["src"] . '">
                    </a>
                    <a href="http://www.restoran.ru' . $items[1]["DETAIL_PAGE_URL"] . '" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">' . $items[1]["NAME"] . '</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        ' . $items[1]["PREVIEW_TEXT"] . '
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">' . $items[1]["CREATED_DATE_FORMATED_3"] . '</span>
                    <a href="http://www.restoran.ru' . $items[1]["DETAIL_PAGE_URL"] . '" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru' . $items[2]["DETAIL_PAGE_URL"] . '" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru' . $file[2]["src"] . '">
                    </a>
                    <a href="http://www.restoran.ru' . $items[2]["DETAIL_PAGE_URL"] . '" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">' . $items[2]["NAME"] . '</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        ' . $items[2]["PREVIEW_TEXT"] . '
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">' . $items[2]["CREATED_DATE_FORMATED_3"] . '</span>
                    <a href="http://www.restoran.ru' . $items[2]["DETAIL_PAGE_URL"] . '" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="'.$allUrl.'" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">'.$allTitle.'</a>
                </td>
            </tr>
        </table>
    </td>
</tr>';

//echo $templateToWrite;
//echo $city;
file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/".$fileName."_".$city.".php", $templateToWrite);
?>