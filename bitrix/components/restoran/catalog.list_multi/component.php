<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CPageOption::SetOptionString("main", "nav_page_in_session", "N");
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
//$arParams["IBLOCK_LID"] = trim($arParams["IBLOCK_LID"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
 	$arParams["IBLOCK_TYPE"] = "news";
if (!is_array($arParams["IBLOCK_ID"]))
    $arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
else
    $arParams["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";

$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
	$arParams["SORT_BY1"] = "ACTIVE_FROM";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
	 $arParams["SORT_ORDER1"]="DESC";

if(strlen($arParams["SORT_BY2"])<=0)
	$arParams["SORT_BY2"] = "SORT";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
	 $arParams["SORT_ORDER2"]="ASC";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}
//FirePHP::getInstance()->info($arrFilter);
$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

if(!is_array($arParams["FIELD_CODE"]))
	$arParams["FIELD_CODE"] = array();
/*foreach($arParams["FIELD_CODE"] as $key=>$val)
	if(!$val)
		unset($arParams["FIELD_CODE"][$key]);
*/
if(!is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $key=>$val)
	if($val==="")
		unset($arParams["PROPERTY_CODE"][$key]);

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"]<=0)
	$arParams["NEWS_COUNT"] = 20;

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
	$arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
$arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
$arParams["HIDE_LINK_WHEN_NO_DETAIL"] = $arParams["HIDE_LINK_WHEN_NO_DETAIL"]=="Y";

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["NEWS_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["NEWS_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
	$arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
	$arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
	foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
	{
		if(in_array($PERM, $arUserGroupArray))
		{
			$bUSER_HAVE_ACCESS = true;
			break;
		}
	}
}
if ($_REQUEST["arrFilter_pf"])
{
    foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
    {
        if ($key=="type")
            $arrFilter["?PROPERTY_".$key] = implode(" || ",$ar);
        else
            $arrFilter["PROPERTY_".$key] = $ar;
    }
}
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $_REQUEST["pageSort"], $_REQUEST["by"],$bUSER_HAVE_ACCESS, $arNavigation, CITY_ID,$arrFilter, $arParams["IBLOCK_TYPE"], $arParams["IBLOCK_ID"],$arParams["PARENT_SECTION"],$arParams["PARENT_SECTION_CODE"])))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
        if(!$arParams["IBLOCK_ID"])
	{
		$this->AbortResultCache();
		ShowError(GetMessage("T_NEWS_NEWS_NA"));
		return;
	}
        
	
        $arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
        //SELECT
        $arSelect = array_merge($arParams["FIELD_CODE"], array(
                "ID",
                "IBLOCK_ID",
                "IBLOCK_SECTION_ID",
                "NAME",
                "ACTIVE_FROM",			
                "DETAIL_PAGE_URL",
                "DETAIL_TEXT",
                "DETAIL_TEXT_TYPE",
                "PREVIEW_TEXT",
                "PREVIEW_TEXT_TYPE",
                "PREVIEW_PICTURE",
        ));
        $bGetProperty = count($arParams["PROPERTY_CODE"])>0;
        if($bGetProperty)
                $arSelect[]="PROPERTY_*";
        //WHERE
        $arFilter = array (
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "IBLOCK_LID" => ($arParams["LID1"])?$arParams["LID1"]:SITE_ID,
                "ACTIVE" => "Y",
//                "CHECK_PERMISSIONS" => "Y",
        );

        if($arParams["CHECK_DATES"])
                $arFilter["ACTIVE_DATE"] = "Y";

        $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
                $arParams["PARENT_SECTION"],
                $arParams["PARENT_SECTION_CODE"],
                array(
                        "GLOBAL_ACTIVE" => "Y",
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                )
        );

        if($arParams["PARENT_SECTION"]>0)
        {
                $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
                if($arParams["INCLUDE_SUBSECTIONS"])
                        $arFilter["INCLUDE_SUBSECTIONS"] = "Y";

                $arResult["SECTION"]= array("PATH" => array());
                $rsPath = GetIBlockSectionPath($arResult["ID"], $arParams["PARENT_SECTION"]);
                $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
                while($arPath=$rsPath->GetNext())
                {
                        $arResult["SECTION"]["PATH"][] = $arPath;
                }
        }
        else
        {
                $arResult["SECTION"]= false;
                //$arFilter["SECTION_ID"] = 1;
        }
        //ORDER BY
        $arSort = array(
                $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
                $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
                "ID" => "DESC"
        );

        $arResult["ITEMS"] = array();
        $arResult["ELEMENTS"] = array();

        $arSelect = array(
                "ID",
                "NAME",
                "CODE",
                "ACTIVE",
                "ACTIVE_FROM",
                "ACTIVE_TO",
                "DATE_CREATE",
                "CREATED_BY",
                "IBLOCK_ID",
                "IBLOCK_SECTION_ID",
                "DETAIL_PAGE_URL",
                "LIST_PAGE_URL",
                "SECTION_PAGE_URL",
                "DETAIL_TEXT",
                "DETAIL_TEXT_TYPE",
                "DETAIL_PICTURE",
                "PREVIEW_TEXT",
                "PREVIEW_TEXT_TYPE",
                "PREVIEW_PICTURE",
                'SHOW_COUNTER'
                /*"PROPERTY_*",*/
        );
//    FirePHP::getInstance()->info(array_merge($arFilter, $arrFilter));
        $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, $arSelect);
        $rsElement->SetUrlTemplates($arParams["DETAIL_URL"], $arResult["SECTION_PAGE_URL"], $arParams["IBLOCK_URL"]);
        while($arItem = $rsElement->GetNext())
        {
                /*$arButtons = CIBlock::GetPanelButtons(
                        $arItem["IBLOCK_ID"],
                        $arItem["ID"],
                        0,
                        array("SECTION_BUTTONS"=>false, "SESSID"=>false)
                );
                $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
                $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];*/

                if($arParams["PREVIEW_TRUNCATE_LEN"]>0)
                {
                        $arItem["PREVIEW_TEXT"] = strip_tags($arItem["PREVIEW_TEXT"]);
                        $end_pos = $arParams["PREVIEW_TRUNCATE_LEN"];
                        while(substr($arItem["PREVIEW_TEXT"],$end_pos,1)!=" " && $end_pos<strlen($arItem["PREVIEW_TEXT"]))
                                $end_pos++;
                        if($end_pos<strlen($arItem["PREVIEW_TEXT"]))
                                $arItem["PREVIEW_TEXT"] = substr($arItem["PREVIEW_TEXT"], 0, $end_pos)."...";
                }                        
                if(strlen($arItem["ACTIVE_FROM"])>0)
                        $arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
                else
                        $arItem["DISPLAY_ACTIVE_FROM"] = "";

                foreach($arParams["PROPERTY_CODE"] as $pid)
                {
                    $values = array();
                    $pr_val = array();
                    $desc = array();
                    $value2 = array();
                    $db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>$pid));
                    while($ar_props = $db_props->Fetch())	
                    {
                        if ($ar_props["PROPERTY_TYPE"]=="E")
                        {
                            if (!is_array($ar_props["VALUE"]))
                            {
                                $pr_res = CIBlockElement::GetByID($ar_props["VALUE"]);
                                if($pr_res = $pr_res->GetNext())  
                                {
                                    $values[] = $pr_res["NAME"];
                                    $value2[] = $pr_res["ID"];
                                }
                            }
                            else
                            {
                                $pr_res = CIBlockElement::GetByID($ar_props["VALUE"][0]);
                                if($pr_res = $pr_res->GetNext())  
                                {
                                    $values[] = $pr_res["NAME"];
                                    $value2[] = $pr_res["ID"];
                                }
                            }
                        }
                        elseif(($ar_props["PROPERTY_TYPE"]=="N"||$ar_props["PROPERTY_TYPE"]=="S")&&$ar_props["MULTIPLE"]=="N")
                        {                            
                            $values = $ar_props["VALUE"];                            
                            $desc = $ar_props["DESCRIPTION"];
                            $pr_val = $ar_props["PROPERTY_VALUE_ID"];
                        }
                        else
                        {
                                $values[] = $ar_props["VALUE"];
                                $desc[] = $ar_props["DESCRIPTION"];
                                $pr_val[] = $ar_props["PROPERTY_VALUE_ID"];
                        }
                        //Тут надо допили если свойство типа список
                        $arItem["PROPERTIES"][$pid] = $values;
                        $arItem["PROPERTIES2"][$pid] = $value2;
                        $arItem["DESCRIPTION"][$pid] = $desc;
                        $arItem["PROPERTY_VALUE_ID"][$pid] = $pr_val;
                        
                    }
                }

                $arResult["ITEMS"][] = $arItem;
                $arResult["ELEMENTS"][] = $arItem["ID"];
        }
        if ($arParams["SET_TITLE"])
        {
            $arIBType = CIBlockType::GetByIDLang($arResult["IBLOCK_TYPE_ID"], LANG);
            $arResult["IBLOCK_TYPE"] = $arIBType["NAME"];
        }
        $arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
        $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
        $arResult["NAV_RESULT"] = $rsElement;
        $this->SetResultCacheKeys(array(
                "ID",
                "IBLOCK_TYPE_ID",
                "LIST_PAGE_URL",
                "NAV_CACHED_DATA",
                "NAME",
                "SECTION",
                "ELEMENTS",
                "IBLOCK_TYPE",
                "ITEMS"
        ));
        $this->IncludeComponentTemplate();
}

if(isset($arResult["ID"]))
{
	$arTitleOptions = null;	
	$this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);

	/*if($arParams["SET_TITLE"])
	{
		$APPLICATION->SetTitle($arResult["IBLOCK_TYPE"], $arTitleOptions);
	}*/

	if($arParams["INCLUDE_IBLOCK_INTO_CHAIN"] && isset($arResult["NAME"]))
	{
		if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
			$APPLICATION->AddChainItem(
				$arResult["NAME"]
				,strlen($arParams["IBLOCK_URL"]) > 0? $arParams["IBLOCK_URL"]: $arResult["LIST_PAGE_URL"]
			);
		else
			$APPLICATION->AddChainItem($arResult["NAME"]);
	}

	if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
	{
		foreach($arResult["SECTION"]["PATH"] as $arPath)
		{
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}

	return $arResult["ELEMENTS"];
}

?>