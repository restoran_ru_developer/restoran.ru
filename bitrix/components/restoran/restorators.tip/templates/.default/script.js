$(document).ready(function() {
    $('.razmes_a').on('click', function() {
        // scroll to
        var pos = $('#plans_div').offset().top-10;
        $('html,body').animate({scrollTop: pos}, "300");

        // set rest id
        var restID = $(this).attr('restID');
        //var action = $(this).attr('action');
        $('.REST_ID').val(restID);
    });

    // delete rest
    $('.razmes_del').on('click', function() {
        if(confirm("Удалить размещение ?")) {
            var restID = $(this).attr('restID');
            $.ajax({
                url: "/bitrix/components/restoran/user.restoraunts_pay_list/templates/.default/del_rest.php",
                cache: false,
                type: 'post',
                data: {restID : restID},
                success: function(data) {
                    // some action
                    location.reload();
                }
            });
        }
    });
});

function order_type(id)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/type_order.php",
        data: "id="+id,
        success: function(data) {
            if (!$("#cbron_form").size())
            {
                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
            }
            $('#cbron_form').html(data);
            showOverflow();
            setCenter($("#cbron_form"));
            $("#cbron_form").css("display","block"); 
        }
    });
}

function order_priority(id)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/priority_order.php",
        data: "id="+id,
        success: function(data) {
            if (!$("#cbron_form").size())
            {
                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
            }
            $('#cbron_form').html(data);
            showOverflow();
            setCenter($("#cbron_form"));
            $("#cbron_form").css("display","block"); 
        }
    });
}