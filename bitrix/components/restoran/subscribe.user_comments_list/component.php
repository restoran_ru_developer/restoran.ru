<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600;
}

if($arParams["IBLOCK_ID"] < 1) {
	ShowError("IBLOCK_ID IS NOT DEFINED");
	return false;
}

if(!isset($arParams["ITEMS_LIMIT"])) {
	$arParams["ITEMS_LIMIT"] = 3;
}

if(!isset($arParams["USER_ID"])) {
    $arParams["USER_ID"] = $USER->GetID();
}

if($this->StartResultCache(false, array($arNavigation, $arParams["IBLOCK_ID"], $arParams["ITEMS_LIMIT"], $arParams["USER_ID"])))
{
	if(!CModule::IncludeModule("iblock")) {
		$this->AbortResultCache();
		ShowError("IBLOCK_MODULE_NOT_INSTALLED");
		return false;
	}

    $arTmpComm = Array();

    // get iblocks list
    $rsIblock = CIBlock::GetList(
        Array("SORT" => "ASC"),
        Array(
            "ACTIVE" => "Y",
            "ID" => $arParams["IBLOCK_ID"],
        ),
        false
    );
    while($arIblock = $rsIblock->GetNext()) {
        // get comments and reviews section
        $rsSubSec = CIBlockSection::GetList(
            Array("SORT" => "ASC"),
            Array(
                "ACTIVE"             => "Y",
                "GLOBAL_ACTIVE"      => "Y",
                "IBLOCK_ID"          => $arIblock["ID"],
                "UF_USER_SUBSCRIBED" => $USER->GetID(),
                "ELEMENT_SUBSECTIONS" => "Y",
                "CNT_ACTIVE" => "Y",
            ),
            true
        );
        while($arSubSec = $rsSubSec->GetNext()) {
            // get comments
            $rsSubComm = CIBlockElement::GetList(
                Array("ID" => "DESC"),
                Array(
                    "ACTIVE"     => "Y",
                    "IBLOCK_ID"  => $arIblock["ID"],
                    "SECTION_ID" => $arSubSec["ID"],
                ),
                false,
                Array("nPageSize" => $arParams["ITEMS_LIMIT"]),
                Array("ID", "NAME", "PREVIEW_TEXT", "DATE_CREATE")
            );
            while($arSubComm = $rsSubComm->Fetch()) {
                $arIBType = CIBlockType::GetByIDLang($arIblock["IBLOCK_TYPE_ID"], LANG);
                $arSubComm["SECTION_NAME"] = $arSubSec["NAME"];
                $arSubComm["COMMENTS_CNT"] = $arSubSec["ELEMENT_CNT"];
                $arSubComm["IBLOCK_TYPE_NAME"] = $arIBType["NAME"];
                $arSubComm["DATE_CREATE_FORMATED"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arSubComm["DATE_CREATE"], CSite::GetDateFormat()));

                $arResult["COMMENTS"][] = $arSubComm;
            }
        }
    }

    aarsort($arResult["COMMENTS"], "ID");
    array_splice($arResult["COMMENTS"], $arParams["ITEMS_LIMIT"]);

	$this->IncludeComponentTemplate();
}

?>