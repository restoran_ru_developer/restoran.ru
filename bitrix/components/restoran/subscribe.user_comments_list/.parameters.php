<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(

		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_ID"),
			"TYPE" => "STRING",
            "MULTIPLE" => "Y"
		),
		"ITEMS_LIMIT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("ITEMS_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => "3",
		),
		"USER_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("USER_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),

		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),

	),
);
?>