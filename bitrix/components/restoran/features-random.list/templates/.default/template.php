<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="">
    <ul class="feature-tag-list">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <li><a href="<?=$arItem['URL']?>" ><?=$arItem['NAME']?></a><?if($arItem!=end($arResult["ITEMS"])):?>, <?endif?></li>
        <?endforeach;?>
    </ul>
</div>