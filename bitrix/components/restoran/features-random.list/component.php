<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"]<=0)
    $arParams["NEWS_COUNT"] = 9;

if($_REQUEST['CATALOG_ID']=='banket'){
    $catalog_type = 'Банкетные залы';
    $feature_types = array(
        //'kitchen' => array("metro","area"),

        "features" =>array("metro","area"),
        "entertainment" =>array("metro","area"),
        "proposals" =>array("metro","area"),
        "children" =>array("metro","area"),
        "parking" =>array("metro","area"),
        "FEATURES_B_H" =>array("metro","area"),
        "ALLOWED_ALCOHOL" =>array("metro","area"),
        'BANKET_SPECIAL_EQUIPMENT' =>array("metro","area"),

        'breakfast' =>array("metro","area"),
        'd_tours' =>array("metro","area"),

        'subway' =>array('features','entertainment','proposals','children','parking','features_b_h','my_alcohol','BANKET_SPECIAL_EQUIPMENT'),
        'area' =>array('features','entertainment','proposals','children','parking','features_b_h','my_alcohol','BANKET_SPECIAL_EQUIPMENT'),
        //'out_city' =>array('type',"kitchen",'features','entertainment','proposals','children','parking','features_b_h','my_alcohol','BANKET_SPECIAL_EQUIPMENT'),
    );
}
else {
    $catalog_type = 'Рестораны';
    $feature_types = array(
        "type" => array("metro","area",'system_suburb'),
        'kitchen' => array("metro","area",'system_suburb'),

        "features" =>array("metro","area",'system_suburb'),
        "entertainment" =>array("metro","area",'system_suburb'),
        "proposals" =>array("metro","area",'system_suburb'),
        "children" =>array("metro","area",'system_suburb'),
        "parking" =>array("metro","area",'system_suburb'),

        'breakfast' =>array("metro","area",'system_suburb'),
        'd_tours' =>array("metro","area",'system_suburb'),

        'subway' =>array('type',"kitchen",'features','entertainment','proposals','children','parking'),
        'area' =>array('type',"kitchen",'features','entertainment','proposals','children','parking'),
        'out_city' =>array('type',"kitchen",'features','entertainment','proposals','children','parking'),
    );
}

$mirror_prop_key_arr = array(
    'entertainment'=>'razvlecheniya',
    'features'=>'osobennosti',
    'proposals'=>'predlozheniya',
    'children'=>'detyam',
    'FEATURES_B_H'=>'osobennostibanket',
    'ALLOWED_ALCOHOL'=>'razreshenospirtnoe',
    'BANKET_SPECIAL_EQUIPMENT'=>'specoborudovanie',
    'subway'=>'metro',
    'area'=>'rajon',
    'out_city'=>'prigorody',
);


if(!$feature_types[$_REQUEST['PROPERTY']]){
    return;
}



if($_REQUEST['PROPERTY']=='breakfast'||$_REQUEST['PROPERTY']=='d_tours'){
    $CUR_RESOURCE_OBJ = $_REQUEST['PROPERTY']=='breakfast'?array('IBLOCK_TYPE_ID' => 'system', 'IBLOCK_CODE' => 'breakfast', 'CODE' => 'y', 'NAME' => 'Завтрак','PREVIEW_TEXT'=>'с завтраком'):array('IBLOCK_TYPE_ID' => 'system', 'IBLOCK_CODE' => 'd_tours', 'CODE' => '2', 'NAME' => 'С 3D туром','PREVIEW_TEXT'=>'с 3D туром');
}
else {
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']][0]);
    $CUR_RESOURCE_OBJ = $res->Fetch();
}


if ($this->StartResultCache(false,array($_REQUEST['CATALOG_ID'],$_REQUEST['CITY_ID'],$feature_types[$_REQUEST['PROPERTY']],$_REQUEST['PROPERTY_VALUE'])))//
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    $arSelect = array_merge($arParams["FIELD_CODE"], array(
        "NAME",
        "ID",
        'CODE'
    ));

    $need_city_iblock = array('metro','area','system_suburb');
    foreach($feature_types[$_REQUEST['PROPERTY']] as $feature_code){
        if(in_array($feature_code,$need_city_iblock)){
            $temp_ib_inf = getArIblock($feature_code, CITY_ID);
            $arIB_features[] = $temp_ib_inf['ID'];
        }
        else {
            $temp_ib_inf = getArIblock('',$feature_code);
            $arIB_features[] = $temp_ib_inf['ID'];
        }
    }

    //FirePHP::getInstance()->info($arIB_features,'$arIB_features');
    $arFilter = array (
        "IBLOCK_ID" => $arIB_features,
        "IBLOCK_LID" => SITE_ID,
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "N",
    );


    $rsElement = CIBlockElement::GetList(array('rand'=>'asc'), array_merge($arFilter, $arrFilter), false, array('nTopCount'=>$arParams["NEWS_COUNT"]), $arSelect);

    $cur_key = 0;
    while ($obElement = $rsElement->GetNextElement()) {

        $arItem = $obElement->GetFields();

        $propsSiteUrlArr = array(
            'entertainment'=>'razvlecheniya',
            'features'=>'osobennosti',
            'proposals'=>'predlozheniya',
            'children'=>'detyam',
            'features_b_h'=>'osobennostibanket',
            'my_alcohol'=>'razreshenospirtnoe',
            'BANKET_SPECIAL_EQUIPMENT'=>'specoborudovanie',
            'system_suburb'=>'prigorody',
            'area'=>'rajon',
        );

//        $nonSystemSiteUrlArr = array(
//            'metro'=>'',
//        );

        $arResult["ITEMS"][] = $arItem;
    }

    if($_REQUEST['PROPERTY']=='subway'||$_REQUEST['PROPERTY']=='area'||$_REQUEST['PROPERTY']=='out_city') {
        $arResult["ITEMS"][] = array('IBLOCK_TYPE_ID' => 'system', 'IBLOCK_CODE' => 'breakfast', 'CODE' => 'y', 'NAME' => 'Завтрак','PREVIEW_TEXT'=>'с завтраки');
        $arResult["ITEMS"][] = array('IBLOCK_TYPE_ID' => 'system', 'IBLOCK_CODE' => 'd_tours', 'CODE' => '2', 'NAME' => 'С 3D туром','PREVIEW_TEXT'=>'с 3D туром');
    }

//    FirePHP::getInstance()->info($arResult,'$arResult');
    $rand_keys = array_rand($arResult["ITEMS"],$arParams["NEWS_COUNT"]-1);
    $result=array_diff_key($arResult["ITEMS"],$rand_keys);
    foreach($result as $key=>$result_val){
        unset($arResult["ITEMS"][$key]);
    }

    //Рестораны итальянской кухни метро Маяковская
    foreach($arResult["ITEMS"] as &$arItem){

        if($_REQUEST['PROPERTY']=='subway'||$_REQUEST['PROPERTY']=='area'||$_REQUEST['PROPERTY']=='out_city'){

            $type_str = $arItem['PREVIEW_TEXT']?explode('/',$arItem['PREVIEW_TEXT']):$arItem['NAME'];


            if($_REQUEST['PROPERTY']=='subway'){
                if($arItem['IBLOCK_CODE']=='type'){
                    $arItem['NAME'] = (is_array($type_str)?$type_str[0]:$type_str)." у метро ".$CUR_RESOURCE_OBJ['NAME'];
                }
                elseif($arItem['IBLOCK_CODE']=='kitchen'){
                    $arItem['NAME'] = "$catalog_type ".strtolower($arItem['PREVIEW_TEXT'])." кухни у метро ".$CUR_RESOURCE_OBJ['NAME'];
                }
                else {
                    $prop_str_with = strtolower($arItem['PREVIEW_TEXT']);
                    $arItem['NAME'] = "$catalog_type $prop_str_with у метро ".$CUR_RESOURCE_OBJ['NAME'];
                }
            }
            elseif($_REQUEST['PROPERTY']=='area'){
                $region_str = $CUR_RESOURCE_OBJ['PREVIEW_TEXT']?$CUR_RESOURCE_OBJ['PREVIEW_TEXT']." районе":"районе ".$CUR_RESOURCE_OBJ['NAME'];


                if($arItem['IBLOCK_CODE']=='type'){
                    $arItem['NAME'] = (is_array($type_str)?$type_str[0]:$type_str)." в $region_str";
                }
                elseif($arItem['IBLOCK_CODE']=='kitchen'){
                    $arItem['NAME'] = "$catalog_type ".strtolower($arItem['PREVIEW_TEXT'])." кухни в ".$region_str;
                }
                else {
                    $prop_str_with = strtolower($arItem['PREVIEW_TEXT']);
                    $arItem['NAME'] = "$catalog_type ".strtolower($arItem['PREVIEW_TEXT'])." в $region_str";
                }
            }
            elseif($_REQUEST['PROPERTY']=='out_city'){
                $region_str = $CUR_RESOURCE_OBJ['PREVIEW_TEXT']?$CUR_RESOURCE_OBJ['PREVIEW_TEXT']:$CUR_RESOURCE_OBJ['NAME'];

                if($arItem['IBLOCK_CODE']=='type'){
                    $arItem['NAME'] = (is_array($type_str)?$type_str[0]:$type_str)." в $region_str";
                }
                elseif($arItem['IBLOCK_CODE']=='kitchen'){
                    $arItem['NAME'] = "$catalog_type ".strtolower($arItem['PREVIEW_TEXT'])." кухни в ".$region_str;
                }
                else {
                    $prop_str_with = strtolower($arItem['PREVIEW_TEXT']);
                    $arItem['NAME'] = "$catalog_type ".strtolower($arItem['PREVIEW_TEXT'])." в $region_str";
                }
            }

            //        ,'breakfast','d_tours'
            if($arItem['IBLOCK_TYPE_ID']=='system'){
                $arItem['URL'] = '/'.CITY_ID.'/catalog/'.$_REQUEST['CATALOG_ID'].'/'.($propsSiteUrlArr[$arItem['IBLOCK_CODE']]?$propsSiteUrlArr[$arItem['IBLOCK_CODE']]:$arItem['IBLOCK_CODE']).'/'.$arItem['CODE'].'/'.($mirror_prop_key_arr[$_REQUEST['PROPERTY']]?$mirror_prop_key_arr[$_REQUEST['PROPERTY']]:$_REQUEST['PROPERTY']).'/'.$_REQUEST['PROPERTY_VALUE'].'/';
            }
            else {
                $arItem['URL'] = '/'.CITY_ID.'/catalog/'.$_REQUEST['CATALOG_ID'].'/'.($propsSiteUrlArr[$arItem['IBLOCK_TYPE_ID']]?$propsSiteUrlArr[$arItem['IBLOCK_TYPE_ID']]:$arItem['IBLOCK_TYPE_ID']).'/'.$arItem['CODE'].'/'.($mirror_prop_key_arr[$_REQUEST['PROPERTY']]?$mirror_prop_key_arr[$_REQUEST['PROPERTY']]:$_REQUEST['PROPERTY']).'/'.$_REQUEST['PROPERTY_VALUE'].'/';
            }
            //FirePHP::getInstance()->info($arItem['URL'],$arItem['NAME']);
        }
        else {
            $type_str = $CUR_RESOURCE_OBJ['PREVIEW_TEXT']?explode('/',$CUR_RESOURCE_OBJ['PREVIEW_TEXT']):$CUR_RESOURCE_OBJ['NAME'];

            if($arItem['IBLOCK_TYPE_ID']=='metro'){
                $prop_str_with = strtolower($CUR_RESOURCE_OBJ['PREVIEW_TEXT']);
                $region_str = $arItem['PREVIEW_TEXT']?$arItem['PREVIEW_TEXT']:$arItem['NAME'];

                if($CUR_RESOURCE_OBJ['IBLOCK_CODE']=='type'){
                    $arItem['NAME'] = (is_array($type_str)?$type_str[0]:$type_str)." у метро $region_str";
                }
                elseif($CUR_RESOURCE_OBJ['IBLOCK_CODE']=='kitchen'){
                    $arItem['NAME'] = "$catalog_type ".strtolower($CUR_RESOURCE_OBJ['PREVIEW_TEXT'])." кухни у метро ".$region_str;
                }
                else {
                    $arItem['NAME'] = "$catalog_type $prop_str_with у метро ".$arItem['NAME'];
                }
            }
            elseif($arItem['IBLOCK_TYPE_ID']=='area'){
                $region_str = $arItem['PREVIEW_TEXT']?$arItem['PREVIEW_TEXT']." районе":"районе ".$arItem['NAME'];

                $prop_str_with = strtolower($arItem['PREVIEW_TEXT']);

                if($CUR_RESOURCE_OBJ['IBLOCK_CODE']=='type'){
                    $arItem['NAME'] = (is_array($type_str)?$type_str[0]:$type_str)." в $region_str";
                }
                elseif($CUR_RESOURCE_OBJ['IBLOCK_CODE']=='kitchen'){
                    $arItem['NAME'] = "$catalog_type ".strtolower($CUR_RESOURCE_OBJ['PREVIEW_TEXT'])." кухни в ".$region_str;
                }
                else {
                    $arItem['NAME'] = "$catalog_type ".strtolower($CUR_RESOURCE_OBJ['PREVIEW_TEXT'])." в $region_str";
                }
            }
            elseif($arItem['IBLOCK_TYPE_ID']=='system_suburb'){
                $region_str = $arItem['PREVIEW_TEXT']?$arItem['PREVIEW_TEXT']:$arItem['NAME'];

                if($CUR_RESOURCE_OBJ['IBLOCK_CODE']=='type'){
                    $arItem['NAME'] = (is_array($type_str)?$type_str[0]:$type_str)." в $region_str";
                }
                elseif($CUR_RESOURCE_OBJ['IBLOCK_CODE']=='kitchen'){
                    $arItem['NAME'] = "$catalog_type ".strtolower($CUR_RESOURCE_OBJ['PREVIEW_TEXT'])." кухни в ".$region_str;
                }
                else {
                    $arItem['NAME'] = "$catalog_type ".strtolower($CUR_RESOURCE_OBJ['PREVIEW_TEXT'])." в $region_str";
                }
            }


            if($arItem['IBLOCK_TYPE_ID']=='system'){
                $arItem['URL'] = '/'.CITY_ID.'/catalog/'.$_REQUEST['CATALOG_ID'].'/'.($mirror_prop_key_arr[$_REQUEST['PROPERTY']]?$mirror_prop_key_arr[$_REQUEST['PROPERTY']]:$_REQUEST['PROPERTY']).'/'.$_REQUEST['PROPERTY_VALUE'].'/'.($propsSiteUrlArr[$arItem['IBLOCK_CODE']]?$propsSiteUrlArr[$arItem['IBLOCK_CODE']]:$arItem['IBLOCK_CODE']).'/'.$arItem['CODE'].'/';
            }
            else {
                $arItem['URL'] = '/'.CITY_ID.'/catalog/'.$_REQUEST['CATALOG_ID'].'/'.($mirror_prop_key_arr[$_REQUEST['PROPERTY']]?$mirror_prop_key_arr[$_REQUEST['PROPERTY']]:$_REQUEST['PROPERTY']).'/'.$_REQUEST['PROPERTY_VALUE'].'/'.($propsSiteUrlArr[$arItem['IBLOCK_TYPE_ID']]?$propsSiteUrlArr[$arItem['IBLOCK_TYPE_ID']]:$arItem['IBLOCK_TYPE_ID']).'/'.$arItem['CODE'].'/';
            }
        }


    }


    //$this->EndResultCache();
    $this->IncludeComponentTemplate();
}

?>