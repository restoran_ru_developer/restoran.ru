<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}

function check_code($CODE, $IB, $id = false){
	CModule::IncludeModule("iblock");
	
	$arSelect = Array("ID");
        if ($id)
            $arFilter = Array("IBLOCK_ID"=>$IB, "CODE"=>$CODE, "!ID"=>$id);
        else
            $arFilter = Array("IBLOCK_ID"=>$IB, "CODE"=>$CODE);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->GetNext()){
  		return $CODE."_".rand(1, 100);
	}else return $CODE;
}

function edit_rest(){
	global $USER;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
		
        $new_code = ($_REQUEST["CODE"])?$_REQUEST["CODE"]:translitIt($_REQUEST["NAME"]);
        if ($_REQUEST["ELEMENT_ID"])
            $new_code = check_code($new_code,$_REQUEST["IBLOCK_ID"],$_REQUEST["ELEMENT_ID"]);
        else
            $new_code = check_code($new_code,$_REQUEST["IBLOCK_ID"]);
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION" => $_REQUEST["IBLOCK_SECTION_ID"],          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"NAME"           => $_REQUEST["NAME"],
  		"ACTIVE"         => "Y",            
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"TAGS"           => $_REQUEST["TAGS"],
                "CODE"          => $new_code,
  	);
            
//	CODE"=>translitIt($_REQUEST["NAME"])
	//Основная фотка
	if(isset($_FILES["DETAIL_PICTURE"])){
		
		$arLoadProductArray["DETAIL_PICTURE"] = $_FILES["DETAIL_PICTURE"];
		/*
    	$DF = CFile::SaveFile($FAR, "restorans");
		$MF = CFile::CopyFile($DF);
	
		$arNewFile = CIBlock::ResizePicture(CFile::MakeFileArray($DF), array("WIDTH" => 392,"HEIGHT" => 260,"METHOD" => "resample", "COMPRESSION"=>95));
		$arLoadProductArray["PREVIEW_PICTURE"] = $arNewFile;
		
		$arLoadProductArray["DETAIL_PICTURE"]=$MF;
		*/
		
	}
	
	
		
	
	//var_dump($_REQUEST["PROP"]);
	if(!isset($_REQUEST["PROP"]["photos"])) $_REQUEST["PROP"]["photos"][]="";
	if(!isset($_REQUEST["PROP"]["google_photo"])) $_REQUEST["PROP"]["google_photo"][]="";
	if(!isset($_REQUEST["PROP"]["videopanoramy"])) $_REQUEST["PROP"]["videopanoramy"][]="";
	
	$COMs = $_REQUEST["PROP"]["videopanoramy_radio"];
	
	$COMs_ph = $_REQUEST["PROP"]["photos_comment"];
	
	
	//var_dump($COMs);
	foreach($_REQUEST["PROP"] as $CODE=>$PR){
		if($CODE=="photos"){
			$FL=form_file_array("NEW_PHOTO");
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"restorans");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"restorans");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
			$l=0;	
			foreach($PR as $fid) {
				$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$COMs_ph[$l]);
				$l++;
			}
			$PR=$PR2;
		}
		
		if($CODE=="google_photo"){
			$FL=form_file_array("GGL");
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"restorans");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"restorans");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
				
			foreach($PR as $fid) {$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>"");
			$PR=$PR2;
			}
		}
		
		
		
		
		if($CODE=="videopanoramy"){
			$FL=form_file_array("PANORAMS");
			//var_dump($_FILES);
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"restorans");
					$PR[]=$fid;
					$i++;
				}
			}else{
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"restorans");
					$PR[]=$fid;
				}
			}
			
			$PR2=array();
			$l=0;
			//var_dump($PR);
			foreach($PR as $fid){
				$PR2[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$COMs[$l]);
				$l++;
			} 
			$PR=$PR2;
			
			
		}
		
		if($CODE=="add_props"){
			$PR=array("VALUE"=>array('TYPE'=>'HTML', 'TEXT'=>$PR));
			//var_dump($PR);
		}
		
                if ($CODE=="subway")
                {
                    foreach ($PR as $key=>$P)
                    {
                        foreach ($P as $Pp)
                            $PR2[] = array("VALUE"=>$Pp, "DESCRIPTION"=>$key);
                    }
                    $PR = $PR2;
                }
		//var_dump($CODE);
		//
		
		CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array($CODE => $PR));
	}	

	//Проверяем права на запись
	if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");
	
	
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию	
	
		unset($arLoadProductArray["IBLOCK_ID"]);
		//v_dump($arLoadProductArray);
		if($el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray, false, false,true)) echo "ok";
		
		//переиндексация
		CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);
	}else{
		//создаем новую публикацию
		
		
		
		if($ELEMENT_ID = $el->Add($arLoadProductArray))
  			echo $ELEMENT_ID;
		else
  			echo "Error: ".$el->LAST_ERROR;
	}
	
}




/***********ФОРМИРУЕМ МАССИВ ФАЙЛА**************/
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($BL_NAME);
	//var_dump($_FILES);
	
	foreach($_FILES[$BL_NAME] as $A=>$C){
		if(is_array($C)){
			foreach($C as $index=>$V){
				$FAR[$index][$A]=$V;
			}
		}
	}
	return $FAR;
}



function save_dots(){
	global $DB;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	//var_dump($_REQUEST["pts"]);
//	$D = explode(",", $_REQUEST["pts"]);
//	$_REQUEST["pts"]=$D[0].",".$D[1];
	foreach($_REQUEST["pts"] as $D){
		$PTS[]=$D[0].",".$D[1];
		
		$LON[]=$D[1];
		$LAT[]=$D[0];
	}
	
	//var_dump($PTS);
	$DB->StartUsingMasterOnly();

	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("map" => $PTS));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("lat" => $LAT));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], $_REQUEST["IBLOCK_ID"], array("lon" => $LON));
	
	$DB->StopUsingMasterOnly();
}



function add_new_rest(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	global $USER;
	
	$arRestIB = getArIblock("catalog", CITY_ID);

	$el = new CIBlockElement;

	$PROP = array();
	$PROP["user_bind"] = $USER->GetID();
	
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $arRestIB["ID"],
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => "Новый ресторан",
  		"ACTIVE"         => "N",            // активен
  	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray))
  		echo "ok";
	else
  		echo "Error: ".$el->LAST_ERROR;
}
/*
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($_FILES);
	foreach($_FILES["block"] as $A=>$C){		
		$VAL="";
		if(is_array($C[$BL_NAME])){
			foreach($C[$BL_NAME] as $V) $VAL=$V;
		}
		$FAR[$A]=$V;
	}
	
	if(is_array($FAR["name"])){
		$RET=array();
		foreach($FAR as $N=>$V){
			for($i=0;$i<count($V);$i++){
				$RET[$i][$N]=$V[$i];
			}
		}
		//var_dump($RET);
		return $RET;
	}else return $FAR;
}
*/

if($_REQUEST["act"]=="edit_rest") edit_rest();
if($_REQUEST["act"]=="save_dots") save_dots();
if($_REQUEST["act"]=="add_new_rest") add_new_rest();

?>
