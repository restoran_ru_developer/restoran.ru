$(document).ready(function() {
    // transform inputs
    $(".chzn-select").chosen();

    // on change name
    $('.inpwrap').on('keyup', '.rest_name', function() {
        var restCode = $(this).attr('code');
        $('#rest_tab_' + restCode).text($(this).val());
    });

    // add more inputs
    $('.add_more').on('click', function() {
        var propID = $(this).parent().prev().find('p.inpwrap:last > input').attr('id');
        
        if($(this).parent().prev().find('p.inpwrap:last > select').length>0){
	        var n = $(this).parent().prev().find('p.inpwrap:last > select').attr('name');
        
    	    var arr1 = n.split('[');
        	var pnm = arr1[1].split(']');
	        pnm = pnm[0];
        
    	    var pnum = arr1[2].split(']');
       		pnum = parseInt(pnum[0])+1;
        }
        
        
        var obj = $(this).parent().prev().find('p.inpwrap:last')
            .clone()
            .appendTo($(this).parent().prev());
            
            //alert(obj.html());
            
           
           if(pnum)	obj.find("select").attr("name", "PROP["+pnm+"]["+pnum+"][]");
            
            
            obj.find(".chzn-done").removeClass("chzn-done").attr("id","");
            obj.find("option:selected").removeAttr("selected");
            obj.before('<strong></strong>');
            
            obj.find(".chzn-container").remove();
            obj.children('input').val('').attr({
                id: propID,
                name: propID + '[]'
            });
            
            $(".chzn-select").chosen();
            
            
    });
	
	$(".del_pho").click(function(){
		var obj = $(this);
		if(confirm("Удалить фотографию?")) {
			obj.parent("li").hide("fast", function(){
				$(this).remove();
			});
		}
		return false;
	});
	
	//проверка формы
	function check_editor_form(a,f,o){
		var ret=true;
		o.dataType = "html";
		$("#error_li").html();
		
		
		var error_string="Вы не заполнили следующие поля: ";
		var z =0;
		var co_errors=0;
		f.find("input[type=text]").each(function(){
			if($(this).hasClass("ob") && $(this).val()==""){
				ret = false;
				var name = $(this).parent("p").parent("li").find("strong").html();	
				var name=name.replace(":","");		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		f.find("textarea").each(function(){
			if($(this).hasClass("ob") && $(this).val()==""){
				ret = false;
				var name = $(this).parent("div").parent("p").parent("li").find("strong").html();	
				var name=name.replace(":","");		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		f.find("select").each(function(){
			//alert($(this).val());
			if($(this).hasClass("ob") && ($(this).val()=="" || $(this).val()==null)){
				var nnm = $(this).attr("name");
				
				var ttl = $('select[name="'+nnm+'"]:first').parent("p").parent("li").find("strong").html();	
				var osh=true;
				$('select[name="'+nnm+'"]').each(function(){
					if($(this).val()!=""){
						osh=false;
					}
				});
				
				if(osh){
				ret = false;
				var name = $('select[name="'+nnm+'"]:first').parent("p").parent("li").find("strong").html();		
				var name=name.replace(":","");		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
				}
			}
		});
		
		
		
		error_string+=".";
		
		if(co_errors>0){
			$("#error_li").html(error_string);
		}
		
		if(ajax_load) ret = false;
		
		return ret;
	}
	
	var go_to= "";
	var act = "";
	
	$(".detail_page").click(function(){
		go_to = $(this).attr("href");
		act = "predprosmotr";
	});
	
	$('.my_rest').ajaxForm({
		beforeSubmit: check_editor_form,
		success: function(data) {
			console.log(data);
			
			
			if(act=="predprosmotr"){
				window.location.replace(go_to);
			}else {
				if (!$("#promo_modal").size()){
					$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
				}
			
       		   	$('#promo_modal').html("<div align=\"right\"><a class=\"modal_close uppercase white\" href=\"javascript:void(0)\"></a></div>Изменения сохранены");
				showOverflow();
				setCenter($("#promo_modal"));
				$("#promo_modal").fadeIn("300");
				
				window.location.reload();
				
				
			}
		
			
		}
	});

    //подменю
    
    $(".add_new_rest_li").click(function(){
    	var lnk = $(this).attr("href");
    	
    	$.post(lnk, function(html){
    		 window.location.reload();
    	});
    	return false;
    });
    


});