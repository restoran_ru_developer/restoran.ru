<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;

?>



<script type="text/javascript" src="<?=$templateFolder?>/jq_redactor/redactor.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/uploaderObject.js"></script>
<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.form.js"></script>



<?/*<script src="http://api-maps.yandex.ru/0.8/index.xml?key=ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==" type="text/javascript"></script>*/?>

<div class="wrap-div">

    <div>
        
        
        <?
        $PHBL=0;
        ?>
        
            <?foreach($arResult["RESTAURANTS"] as $rest):?>
            	<?
            	//запрашиваем id секции с меню
            	if($_REQUEST["PARENT_SECTION_ID"]==""){
            		$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>$rest["ID"]);
  					$db_list = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, true);
					if(!$ar_result = $db_list->GetNext()){
    					//если меню не создано, то создаем его
    					
    					$bs = new CIBlockSection;
						$arFields = Array(
  							"ACTIVE" => "Y",
  							"IBLOCK_SECTION_ID" => false,
  							"IBLOCK_ID" => 151,
  							"NAME" => $rest["NAME"],
  							'UF_RESTORAN'=>$rest["ID"]
  						);

  						$ID = $bs->Add($arFields);	
  					}            	
  				}
  				?>
            
            	
            	<ul class="menu-horizontal-sub" >
					<li><a href="/users/id<?=$USER->GetID()?>/restoran_edit/menu/<?=$rest["ID"]?>/">редактировать меню ресторана</a></li>
				</ul>
				
			
                <h2>Редактирование ресторана «<?=$rest["NAME"]?>»</h2>
                <form class="my_rest" action="<?=$templateFolder?>/core.php" method="post" name="rest_edit" id="rest_<?=$rest["ID"]?>">
                    <?=bitrix_sessid_post()?>
                    
                    <input type="hidden" name="ELEMENT_ID" value="<?=$rest["ID"]?>" />
                    <input type="hidden" name="IBLOCK_ID" value="<?=$rest["IBLOCK_ID"]?>" />
                 
                    <input type="hidden" name="act" value="edit_rest" />
                    
                    <ul>
                        <li class="item-row">
                            <strong>Название:</strong>
                            <i class="star">*</i>
                            <p class="inpwrap">
                                <input type="text" class="text rest_name ob" code="<?=$rest["CODE"]?>" name="NAME" value="<?=$rest["NAME"]?>" />
                            </p>
                        </li>
                        
                        <li class="item-row">
                            <strong>Символьный код:</strong>
                            <p class="inpwrap">
                                <input type="text" class="text rest_code" code="<?=$rest["CODE"]?>" name="CODE" value="<?=$rest["CODE"]?>" />
                            </p>
                        </li>
                        
                         <li class="item-row">
                            <strong>Синонимы:</strong>
                            <p class="inpwrap">
                            	<textarea class="text rest_tags" code="TAGS" name="TAGS"  ><?=$rest["TAGS"]?></textarea>
                            </p>
                        </li>
                        
                       	<li class="item-row">
							<strong>Раздел:</strong>
							<i class="star">*</i>
							<p class="inpwrap">
							<?
							//var_dump($rest["IBLOCK_SECTION_ID"]);
							
						//	in_array($keyRestType, $rest["IBLOCK_SECTION_ID"])
							?>
							<select multiple data-placeholder="Выберите раздел" class="chzn-select ob" style="width:350px;" name="IBLOCK_SECTION_ID[]">
							<?foreach($arResult["SECTIONS"] as $keyRestType=>$restType):?>
								<option value="<?=$keyRestType?>"<? if(in_array($keyRestType, $rest["IBLOCK_SECTION_ID"])) echo  "selected='selected'"?>><?=$restType?></option>
							<?endforeach?>
							</select>
							</p>
						</li>

                        
                         <li class="item-row">
                            <strong>Описание:</strong>
                           
                            <p class="inpwrap" style="float:left;width:811px;">
                            	<textarea name="DETAIL_TEXT" id="detail_text" style="height:200px;" class=""><?=$rest["DETAIL_TEXT"]?></textarea>
                            </p>
                        </li>
                        
                        
                          <li class="item-row">
                            <strong>Дополнительное описание:</strong>
                            <p class="inpwrap" style="float:left;width:811px;">
                            	<textarea name="PROP[add_props]"  id="PROP_add_props_<?=$rest["ID"]?>" style="height:200px;"><?=$rest["SELECTED_PROPERTIES"][$rest["ID"]]["add_props"]["VALUE"][0]["TEXT"]?></textarea>
                            </p>
                        </li>
                        
                        <script type="text/javascript">
                        	$("#detail_text").redactor({ 
			path: '<?=$templateFolder?>/jq_redactor', 
			buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|','link'], 
			css: 'redactor.css', 
			lang: 'ru',
			focus: false,
			fileUpload: '<?=$templateFolder?>/file_upload.php',
			autoresize: false
		});
		
		
                        	$("#PROP_add_props_<?=$rest["ID"]?>").redactor({ 
			path: '<?=$templateFolder?>/jq_redactor', 
			buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|','link'], 
			css: 'redactor.css', 
			lang: 'ru',
			focus: false,
			fileUpload: '<?=$templateFolder?>/file_upload.php',
			autoresize: false
		});
                        	
                        </script>
                        <?foreach($rest["PROPERTIES"] as $keyProp=>$prop)://v_dump($rest["SELECTED_PROPERTIES"][$keyProp]);?>
							<?
							if(in_array(strtoupper($prop["CODE"]), $arParams["HIDDEN_PROPERTIES"])) continue;
							if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && in_array(strtoupper($prop["CODE"]), $arParams["ADMIN_ONLY"])) continue;
							?>
                            <?switch($prop["PROPERTY_TYPE"]):
                                case "E":
                            ?>
                                <li class="item-row">
                                    <strong><?=$prop["NAME"]?>:</strong>
                                    <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
									<?
									$m=1;
									$NK=0;	
									if ($prop["CODE"]=="subway"){
										//var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]);
									
									
										//var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]);
										
									
										//var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"]);
										
										for($c=0;$c<count($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"]);$c++){
											if($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"][$c]=="") 
												$rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"][$c]=$c+1;
										}
										/*
										foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"] as $KEY => &$P){
											if($P=="") $P=$KEY;
										}
										*/
										//var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"]);
										?>
									
									
									<?foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"] as $KEY => $DSel){?>	
											
                                    	<?if($DSel=="") $DSel="1";?>
										<?if($NK!==$DSel){?>
											
											<?if($m>1){?>
                                    			</li><li class="item-row"><strong></strong>
                                    		<?}?>
											
											<?
											$NK=$DSel;
											
											$INSELECT=array();
											//var_dump($DSel);
											foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["DESCRIPTION"] as $KEY2 => $DSel2){
												if($DSel2===$DSel) {
													//var_dump($DSel2);
													$INSELECT[]=$rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"][$KEY2];
												}
											}
											
											//var_dump($INSELECT);
											?>
                                    		<p class="inpwrap"><?=($KEY+1)?>.
                                    		<select multiple data-placeholder="Выберите вариант" class="chzn-select <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[".$DSel."][]";?>">
                                        	<?foreach($prop["VALUE"] as $keyRestType=>$restType):?>    
												<option value="<?=$keyRestType?>"<? if(in_array($keyRestType,$INSELECT)) echo  "selected='selected'"?>><?=$restType?></option>
                                            <?endforeach?>
                                       	 	</select>
                                    		</p>
										<?}?>
                                    	
                                    	<?
                                    	$m++;
                                    	?>
                                    <?}?>
                                    
                                    <?}else{?>
									
									
									<?
									// var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]);
									?>
									<?foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"] as $Sel){?>	
											
                                    	<?if($m>1){?>
                                    		</li><li class="item-row"><strong></strong>
                                    	<?}?>
                                    		
										<?//if (array_key_exists($Sel, $prop["VALUE"]) || !intval($Sel)):?>
                                    		<p class="inpwrap">
                                    		<select multiple data-placeholder="Выберите вариант" class="chzn-select <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" style="width:350px;" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";?>">
                                        	<?foreach($prop["VALUE"] as $keyRestType=>$restType):?>    
												<option value="<?=$keyRestType?>"<? if($keyRestType==$Sel) echo  "selected='selected'"?>><?=$restType?></option>
                                            <?endforeach?>
                                       	 	</select>
                                    		</p>
										<?//endif;?>
                                    	
                                    	<?
                                    	$m++;
                                    	?>
                                    <?}
                                    }?>
                                </li>
                                
                                
                                <?if($prop["MULTIPLE"] == "Y"):?>
                                        <li class="item-row">
                                            <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                                        </li>
                                <?endif?>
                                <?
                                break;
                                case "L":
                                ?>
                                    <li class="item-row">
                                        <strong><?=$prop["NAME"]?>:</strong>
                                         <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                        <p>
                                            <?if($prop["LIST_TYPE"] == "C" && $prop["MULTIPLE"] == "N"):?>
                                                <span class="niceCheck">
                                                    <input type="checkbox" class="checkbox" name="PROP[<?=$prop["CODE"]?>][<?=$selPropKey?>]"<?if(in_array($prop["VALUE"], $rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"])) echo  "checked='checked'"?> value="<?=$prop["VALUE"]?>" /><br/>
                                                </span>
                                            <?endif?>
                                        </p>
                                    </li>
                                <?
                                break;
                                case "S":
                                ?>
                                    <li class="item-row"><?//v_dump($prop)?>
                                        <strong><?=$prop["NAME"]?>:</strong>
                                        	
                                         <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo '<i class="star">*</i>';?>
                                        <?
                                        $selPropCnt = 0;
                                        foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"] as $selPropKey=>$selPropVal):?>
                                            <?if($selPropCnt >= 1):?>
                                                <strong></strong>
                                            <?endif?>
                                            <p class="inpwrap"><?if($prop["MULTIPLE"] == "Y"):?><?=($selPropKey+1)?>. <?endif;?>
                                                <input type="text" class="text <?if(in_array(strtoupper($prop["CODE"]), $arParams["REQUIRED_PROPERTIES"])) echo 'ob';?>" id="PROP[<?=$prop["CODE"]?>]" name="PROP[<?=$prop["CODE"]?>]<? if($prop["MULTIPLE"]=="Y") echo "[]";//[<?=$selPropKey]?>" value="<?=$selPropVal?>" />
                                            </p>
                                        <?
                                        $selPropCnt++;
                                        endforeach?>
                                    </li>
                                    <?if($prop["MULTIPLE"] == "Y"):?>
                                        <li class="item-row">
                                            <input type="button" class="submit add_more" name="add_more" value="+Добавить" />
                                        </li>
                                    <?endif?>
                                <?
                                break;
                                case "F":
                                ?>
                                
                                <?
                                if($PHBL==0){
                           		    $APPLICATION->IncludeFile(
									$templateFolder."/photos.php",
									Array(
										"BLOCK_TITTLE"=>"Фотогалерея",
        	    						"BLOCK_VALUE" =>array($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]),
        	    						"DETAIL_PIC"=>($rest["DETAIL_PICTURE"]=="") ? $rest["PREVIEW_PICTURE"] : $rest["DETAIL_PICTURE"],
        	    						"BLOCK_NAME"=>"bl".time().$k,
        	    						"CODE"=>$prop["CODE"]
									),
									Array("MODE"=>"php")
        						);
        						}else{
        							if($PHBL==1){
        								$APPLICATION->IncludeFile(
										$templateFolder."/photos2.php",
										Array(
											"BLOCK_TITTLE"=>$prop["NAME"],
        	    							"BLOCK_VALUE" =>array($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]),
        	    							"BLOCK_NAME"=>"bl".time().$k,
        	    							"CODE"=>$prop["CODE"]
										),
										Array("MODE"=>"php")
        								);
        							}else{
        								
        								
        								$APPLICATION->IncludeFile(
										$templateFolder."/panorams.php",
										Array(
											"BLOCK_TITTLE"=>$prop["NAME"],
        	    							"BLOCK_VALUE" =>array($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]),
        	    							"BLOCK_NAME"=>"bl".time().$k,
        	    							"CODE"=>$prop["CODE"]
										),
										Array("MODE"=>"php")
        								);
        								//var_dump($rest["SELECTED_PROPERTIES"][$rest["ID"]][$keyProp]["VALUE"]);
        							}
        						}
        						$PHBL++;
        						
                                        $k++;
                                      ?> 
                                <?break;?>
                            <?endswitch?>
                        <?endforeach?>
                      	
                      	 <li class="item-row">
                            <div class="zg krt">Карта:</div>
                            <div class="inpwrap">
                            <?
                            $md = explode(",", $rest["SELECTED_PROPERTIES"][$rest["ID"]]["map"]["VALUE"][0]);
                            $mas=array(
                            	"yandex_lat"=>$md[1],
                            	"yandex_lon"=>$md[0],
                            	"yandex_scale"=>14
                            );
                            
                            if($md[1]==""){
                            	if(CITY_ID=="spb") $DTS = COption::GetOptionString("main", "spb_center");
                                elseif(CITY_ID=="tmn") $DTS = COption::GetOptionString("main", "tmn_center");
                                elseif(CITY_ID=="kld") $DTS = COption::GetOptionString("main", "kld_center");
                            	else  $DTS = COption::GetOptionString("main", "msk_center");
                            	$DTS = explode(",", $DTS);
                            	
                            	unset($mas);
                         		$mas=array(
                            		"yandex_lat"=>$DTS[1],
                            		"yandex_lon"=>$DTS[0],
                            		"yandex_scale"=>14
                            	);
                         	}
                         	
                            foreach($rest["SELECTED_PROPERTIES"][$rest["ID"]]["map"]["VALUE"] as $D){
                            	$md = explode(",", $D);
                            	$mas["PLACEMARKS"][]=array(
                            			"LON"=>$md[0],
                            			"LAT"=>$md[1],
                            			"TEXT"=>""
                            		);
                            }
                            ?>
                          
                            <?$APPLICATION->IncludeComponent(
								"bitrix:map.yandex.view",
								"restoran_edit_or",
								Array(
									"KEY" => "ACqpn08BAAAAZv_ZPQMAo7RJtEXOlJAaBe_th8Aqu1ozylcAAAAAAAAAAAACW78YiKkXH1UI9mXqxcpDpJ28Fg==",
									"INIT_MAP_TYPE" => "MAP",
									"MAP_DATA" => serialize($mas),
									"MAP_WIDTH" => "975",
									"MAP_HEIGHT" => "500",
									"CONTROLS" => array("ZOOM"),
									"OPTIONS" => array("ENABLE_SCROLL_ZOOM","DISABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING"),
									"MAP_ID" => "rest_".$rest["ID"],
                                                                        'LOCALE' => "ru",
								)
							);?>
							</div>      
						</li>
                    
                        <li class="item-row">
                            <input type="submit" class="submit-blue detail_page" name="submit" value="сохранить" href="<?=$rest["DETAIL_PAGE_URL"]?>"/>
                            <input type="submit" class="submit-blue" name="submit" value="применить"/>
                        </li>
                        
                        <li class="item-row error_li" id="error_li"></li>
                        
                    </ul>
                </form>
                
                
                
    
            <?endforeach?>
       
    </div>
</div>

<?

//var_dump($rest);
?>