<?$file = CFile::ResizeImageGet($DETAIL_PIC, array('width'=>230, 'height'=>125), BX_RESIZE_IMAGE_EXACT, true);  ?>
<li id="rest_photos">
	<div class="main_photo">
		<div class="zg">Основная фотография<br/>ресторана</div>
		<?if($file["src"]!=""){?>
		<div class="ph"><img src="<?=$file["src"]?>" ></div>
		<?}?>
		<div class="loader">
		<input type="file" name="PREVIEW_PICTURE" value="" id="file-field_detail_<?=$BLOCK_NAME?>" class="file-field" />
		</div>
	</div>
	
	<div class="other_photos">
		<div class="zg">Фотографии ресторана</div>
        <div class="loader">
        	
        	<div id="imu_content_<?=$BLOCK_NAME?>" class="imu_content">
				<div class="upl-description">Перетащите фотографии в это поле <br />или<br />
				<p>
				<input type="file" name="NEW_PHOTO" value="" id="file-field_<?=$BLOCK_NAME?>" multiple="true" class="file-field" /><br/>
				<em>Выберите файлы</em>
				</p>
				</div>
			</div>
        
        </div>
        
        <ul class="photos" id="move_pho">
        	<?$i=0;?>
			<?foreach($BLOCK_VALUE[0] as $VALUE){?>
				<?if($VALUE[0]=="") continue;?>
        		<?$file = CFile::ResizeImageGet($VALUE[0], array('width'=>230, 'height'=>125), BX_RESIZE_IMAGE_EXACT, true);  ?>
        		<li class="pho">
        			<a href="#" class="del_pho"></a>
        			<img src="<?=$file["src"]?>" >
        			<input type="hidden" name="PROP[photos][]" value="<?=$VALUE[0]?>" />
        		</li>
        	<?}?>
        </ul>
                      			
	</div>
</li>



			


<script type="text/javascript">
// Стандарный input для файлов
var fileInput = $('#file-field_<?=$BLOCK_NAME?>');
var fileInput2 = $('#file-field_detail_<?=$BLOCK_NAME?>');
    
fileInput2.customFileInput();    
    
// ul-список, содержащий миниатюрки выбранных файлов
var imgList = $('ul#img-list_<?=$BLOCK_NAME?>');
    
// Контейнер, куда можно помещать файлы методом drag and drop
var dropBox = $('#imu_content_<?=$BLOCK_NAME?>');

    					
// Проверка поддержки File API в браузере
if(window.FileReader == null) {
 	 
 	
    //fileInput.next().remove();
    dropBox.replaceWith(fileInput);
	fileInput.customFileInput();
	
}else{    

	

    // Отображение выбраных файлов и создание миниатюр
    function displayFiles_<?=$BLOCK_NAME?>(files) {
        var imageType = /image.*/;
        var num = 0;
        
        $.each(files, function(i, file) {
            
            // Отсеиваем не картинки
            if (!file.type.match(imageType)) {
                alert("Ошибка");
                return true;
            }

            num++;
            
            // Создаем элемент li и помещаем в него название, миниатюру и progress bar,
            // а также создаем ему свойство file, куда помещаем объект File (при загрузке понадобится)
            var li = $('<li/>').appendTo(imgList);
            var em = $('<em/>').appendTo(li);
            var img = $('<img/>').appendTo(em);
//            var txt = $('<textarea/>').text('Добавьте описание').appendTo(li);
            var txtarea = $('<textarea name="block[<?=$BLOCK_NAME?>][descr_ja][]"/>').appendTo(li);
			txtarea.val('Добавьте описание');

            
			

            li.get(0).file = file;


            // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
            // инфу обо всех файлах
            var reader = new FileReader();
            reader.onload = (function(aImg) {
                return function(e) {
                    aImg.attr('src', e.target.result);
                    aImg.attr('width', 100);
                };
            })(img);
            
            reader.readAsDataURL(file);
        });
    }
    
    
    ////////////////////////////////////////////////////////////////////////////


    // Обработка события выбора файлов через стандартный input
    // (при вызове обработчика в свойстве files элемента input содержится объект FileList,
    //  содержащий выбранные файлы)
    fileInput.bind({
        change: function() {
          //  log(this.files.length+" файл(ов) выбрано через поле выбора");
            displayFiles_<?=$BLOCK_NAME?>(this.files);
        }
    });
          

    // Обработка событий drag and drop при перетаскивании файлов на элемент dropBox
    // (когда файлы бросят на принимающий элемент событию drop передается объект Event,
    //  который содержит информацию о файлах в свойстве dataTransfer.files. В jQuery "оригинал"
    //  объекта-события передается в св-ве originalEvent)
    dropBox.bind({
        dragenter: function() {
            $(this).addClass('highlighted');
            return false;
        },
        dragover: function() {
            return false;
        },
        dragleave: function() {
            $(this).removeClass('highlighted');
            return false;
        },
        drop: function(e) {
            var dt = e.originalEvent.dataTransfer;
            displayFiles_<?=$BLOCK_NAME?>(dt.files);
            return false;
        }
    });

}


$('p.dela a').live("click", function() {
	$(this).parent("p").parent("li").hide("fast", function(){
		$(this).remove();
	});
	return false;
});
			
</script>			