<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "USER_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("USER_ID"),
            "TYPE" => "STRING",
            "VALUES" => $USER->GetID(),
        ),
	),
);
?>