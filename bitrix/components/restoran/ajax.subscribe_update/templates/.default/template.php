<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<script>
    $(document).ready(function(){
        $(".modal_close").click(function(){
           $(this).parent().parent().fadeOut(300); 
           hideOverflow();
        });
    });
</script>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
</div>
<?
if ($arResult["ERROR"])
{
    echo "<div class='center'>";
    if (GetMessage($arResult["ERROR"]))
        echo GetMessage($arResult["ERROR"]);
    else
        echo $arResult["ERROR"];
    echo "</div>";
}
else {
    echo "<div class='center'>";
    echo GetMessage("SU_OK");
    echo "<Br />";
    echo "</div>";
    echo "<ul>";
    foreach ($arResult["RUBRIC"] as $rubr)
    {
        echo "<li>".$rubr.'</li>';
    }
    echo "</ul>";
}
?>