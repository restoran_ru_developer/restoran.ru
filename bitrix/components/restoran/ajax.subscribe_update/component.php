<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["EMAIL"] = trim($_REQUEST["email"]);
$arParams["RUBRIC"] = (is_array($_REQUEST["rubric"]))?$_REQUEST["rubric"]:Array();
global $USER;

if (check_bitrix_sessid()&&check_email($arParams["EMAIL"])&&$arParams["RUBRIC"]&&$USER->IsAuthorized())
{
    CModule::IncludeModule("subscribe");
    $subscr = CSubscription::GetList(
        array("ID"=>"ASC"),
        array("USER_ID"=>$USER->GetID(),"EMAIL"=>$_REQUEST["email"])
    );
    $error = "";
    if($subscr_arr = $subscr->Fetch())
    {
        $arFields = Array(
            "USER_ID" => $USER->GetID(),
            "FORMAT" => "html",
            "EMAIL" => $_REQUEST["email"],
            "ACTIVE" => "Y",
            "RUB_ID" => $_REQUEST["rubric"],
            "SEND_CONFIRM" =>"Y"
        );
        $subscr = new CSubscription;
        $subscr->Update($subscr_arr["ID"],$arFields,SITE_ID);
        //$strWarning .= "Error adding subscription: ".$subscr->LAST_ERROR."<br>";
    }
    else
    {
        $arFields = Array(
            "USER_ID" => $USER->GetID(),
            "FORMAT" => "html",
            "EMAIL" => $_REQUEST["email"],
            "ACTIVE" => "Y",
            "RUB_ID" => $_REQUEST["rubric"],
            "SEND_CONFIRM" =>"Y"
        );
        $subscr = new CSubscription;
        $ID = $subscr->Add($arFields);
        if($ID>0)
            CSubscription::Authorize($ID);
        else
            $error .= "Error adding subscription: ".$subscr->LAST_ERROR."<br>";
    }
    if ($error)
        $arResult["ERROR"] = $error;
    else
    {
        foreach($arParams["RUBRIC"] as $rubric):
            $rubric = CRubric::GetByID($rubric);
            if($rub = $rubric->Fetch())
                $arResult["RUBRIC"][] = $rub["NAME"];
        endforeach;
    }
}
else
{
    if (!check_email($arParams["EMAIL"]))
        $arResult["ERROR"] .= GetMessage("NOT_VALID_EMAIL")."<Br />";
    if(!$USER->IsAuthorized())
        $arResult["ERROR"] .= GetMessage("AUTH_ERROR")."<Br />";
    if(!count($arParams["RUBRIC"]))
        $arResult["ERROR"] .= GetMessage("RUBRIC_ERROR")."<Br />";
}
$this->IncludeComponentTemplate();
?>