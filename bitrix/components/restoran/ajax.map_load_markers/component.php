<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

if($this->StartResultCache(false, array()))
{    
    if (check_bitrix_sessid()&&$arParams["xl"]&&$arParams["xr"]&&$arParams["yl"]&&$arParams["yr"]):
        if (CModule::IncludeModule("iblock")):
            $arRestIB = getArIblock("catalog", CITY_ID);
            if ($_REQUEST["arrFilter_pf"])
            {
                foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
                {
                    //if ($key=="type")
                        //$arrFilter["?PROPERTY_".$key] = implode(" || ",$ar);
                    //else
                        $arrFilter["PROPERTY_".$key] = $ar;
                }
            }
            $arFilter = Array("ACTIVE"=>"Y","IBLOCK_ID"=>$arRestIB["ID"],"!PROPERTY_sleeping_rest_VALUE"=>"Да","!PROPERTY_lat"=>false,"!PROPERTY_lon" => false,"><PROPERTY_lat"=>Array((double)$arParams["xl"],(double)$arParams["xr"]),"><PROPERTY_lon"=>Array((double)$arParams["yl"],(double)$arParams["yr"]));
            if (is_array($arrFilter))
            {
                $arFilter = array_merge($arFilter,$arrFilter);
            }
            $res = CIBlockElement::GetList(Array(),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","DETAIL_PAGE_URL","CODE"));
            //$res->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
            while($ar = $res->GetNext())
            {
                $lat = array();
                $lon = array();
                $adr = array();
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lat"));
                while($ar_props = $db_props->Fetch())
                    $lat[] = $ar_props["VALUE"];
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lon"));
                while($ar_props = $db_props->Fetch())
                    $lon[] = $ar_props["VALUE"];
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
                while($ar_props = $db_props->Fetch())
                    $adr[] = $ar_props["VALUE"];

                foreach($lat as $key=>$l)
                {
                    if ($lat[$key]>=(double)$arParams["xl"]&&$lat[$key]<=(double)$_REQUEST["xr"]&&$lon[$key]>=(double)$_REQUEST["yl"]&&$lon[$key]<=(double)$arParams["yr"])
                    {
                        /*if ($arParams["mobile"]=="Y")
                            $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$lat[$key],"lon"=>$lon[$key],"adres"=>stripslashes($adr[$key]),"url"=> "/mobile/detail.php?RESTOURANT=".$ar["CODE"]);
                        else*/
                            $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$lat[$key],"lon"=>$lon[$key],"adres"=>stripslashes($adr[$key]),"url"=> $ar["DETAIL_PAGE_URL"]);
                    }
                }
            }
            if (count($arResult)>0)
            {   
                echo json_encode($arResult);
            }
        endif;
    endif;
}
?>