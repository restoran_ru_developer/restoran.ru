<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <h2><?=$arItem["IBLOCK_TYPE_NAME"]?>: <?=$arItem["NAME"]?> (<?=$arItem["SECTION_NAME"]?>)</h2>
    <table border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td><?=GetMessage("ACTIVE_FROM")?>:</td>
            <td><?=$arItem["ACTIVE_FROM"]?></td>
        </tr>
        <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
            <?if($arProperty["DISPLAY_VALUE"]):?>
                <tr>
                    <td><?=$arProperty["NAME"]?>:</td>
                    <td><?=$arProperty["DISPLAY_VALUE"];?></td>
                </tr>
            <?endif?>
        <?endforeach;?>
    </table>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>