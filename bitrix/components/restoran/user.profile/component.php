<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if (CSite::InGroup(Array(1,20,14)))
    $arResult["ID"]=$_REQUEST["USER_ID"];
else
    $arResult["ID"]=intval($USER->GetID());
$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy($arResult["ID"]);

$arParams['SEND_INFO'] = $arParams['SEND_INFO'] == 'Y' ? 'Y' : 'N';
$arParams['CHECK_RIGHTS'] = $arParams['CHECK_RIGHTS'] == 'Y' ? 'Y' : 'N';

if(!($arParams['CHECK_RIGHTS'] == 'N' || $USER->CanDoOperation('edit_own_profile')) || $arResult["ID"]<=0)
{
	$APPLICATION->ShowAuthForm(GetMessage("ACCESS_DENIED"));
	return;
}


$strError = '';
$arResult["ID"] = IntVal($arResult["ID"]);

if($_SERVER["REQUEST_METHOD"]=="POST" && (strlen($_REQUEST["save"])>0 || strlen($_REQUEST["apply"])>0) && check_bitrix_sessid())
{

	if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
	{
		//possible encrypted user password
		$sec = new CRsaSecurity();
		if(($arKeys = $sec->LoadKeys()))
		{
			$sec->SetKeys($arKeys);
			$errno = $sec->AcceptFromForm(array('NEW_PASSWORD', 'NEW_PASSWORD_CONFIRM'));
			if($errno == CRsaSecurity::ERROR_SESS_CHECK)
				$strError .= GetMessage("main_profile_sess_expired").'<br />';
			elseif($errno < 0)
				$strError .= GetMessage("main_profile_decode_err", array("#ERRCODE#"=>$errno)).'<br />';
		}
	}

	if($strError == '')
	{
		$bOk = false;
		$obUser = new CUser;
	
		$arPERSONAL_PHOTO = $_FILES["PERSONAL_PHOTO"];
		$arWORK_LOGO = $_FILES["WORK_LOGO"];
	
		$rsUser = CUser::GetByID($arResult["ID"]);
		$arUser = $rsUser->Fetch();
		if($arUser)
		{
			$arPERSONAL_PHOTO["old_file"] = $arUser["PERSONAL_PHOTO"];
			$arPERSONAL_PHOTO["del"] = $_REQUEST["PERSONAL_PHOTO_del"];
	
			$arWORK_LOGO["old_file"] = $arUser["WORK_LOGO"];
			$arWORK_LOGO["del"] = $_REQUEST["WORK_LOGO_del"];
		}
                if ($arResult["ID"]==1)
                    $_REQUEST["LOGIN"] = "admin";
                //$temp = explode(" ",$_REQUEST["NAME"]);                
		$arFields = Array(
			"NAME"					=> $_REQUEST["NAME"],
			"LAST_NAME"				=> $_REQUEST["LAST_NAME"],
			"SECOND_NAME"			=> $_REQUEST["SECOND_NAME"],
			"EMAIL"					=> $_REQUEST["EMAIL"],
			"LOGIN"					=> $_REQUEST["LOGIN"],
			"PERSONAL_GENDER"		=> $_REQUEST["PERSONAL_GENDER"],
			"PERSONAL_BIRTHDAY"		=> $_REQUEST["PERSONAL_BIRTHDAY"],
			"PERSONAL_PHOTO"		=> $arPERSONAL_PHOTO,
			"PERSONAL_PHONE"		=> $_REQUEST["PERSONAL_PHONE"],
			"PERSONAL_PROFESSION"	=> $_REQUEST["PERSONAL_PROFESSION"],
			"PERSONAL_CITY"			=> $_REQUEST["PERSONAL_CITY"],
			"AUTO_TIME_ZONE"		=> ($_REQUEST["AUTO_TIME_ZONE"] == "Y" || $_REQUEST["AUTO_TIME_ZONE"] == "N"? $_REQUEST["AUTO_TIME_ZONE"] : ""),
		);
	
		if(isset($_REQUEST["TIME_ZONE"]))
			$arFields["TIME_ZONE"] = $_REQUEST["TIME_ZONE"];
	
		if($arUser)
		{
			if(strlen($arUser['EXTERNAL_AUTH_ID']) > 0)
			{
				$arFields['EXTERNAL_AUTH_ID'] = $arUser['EXTERNAL_AUTH_ID'];
			}
		}
	
		//if($arResult["MAIN_RIGHT"]=="W" && is_set($_POST, 'EXTERNAL_AUTH_ID')) $arFields['EXTERNAL_AUTH_ID'] = $_POST["EXTERNAL_AUTH_ID"];
		if($USER->IsAdmin())
		{
			$arFields["ADMIN_NOTES"]=$_REQUEST["ADMIN_NOTES"];
		}
		if(strlen($_REQUEST["NEW_PASSWORD"])>0)
		{
			$arFields["PASSWORD"]=$_REQUEST["NEW_PASSWORD"];
			$arFields["CONFIRM_PASSWORD"]=$_REQUEST["NEW_PASSWORD_CONFIRM"];
		}
		$GLOBALS["USER_FIELD_MANAGER"]->EditFormAddFields("USER", $arFields);


		if(!$obUser->Update($arResult["ID"], $arFields))
			echo $strError .= $obUser->LAST_ERROR.'<br />';
        if ($_REQUEST["SUBSCRIBE"]==1)
        {
            $arGroups = CUser::GetUserGroup($USER->GetID());
            $arGroups[] = 31;
            CUser::SetUserGroup($USER->GetID(), $arGroups);
        }

//        FirePHP::getInstance()->info($strError);

	}

	if($strError == '')
	{
		if($arParams['SEND_INFO'] == 'Y')
			$obUser->SendUserInfo($arResult["ID"], SITE_ID, GetMessage("ACCOUNT_UPDATE"), true);

		$bOk = true;
	}
}

$rsUser = CUser::GetByID($arResult["ID"]);
if(!$arResult["arUser"] = $rsUser->GetNext(false))
{
	$arResult["ID"]=0;
	//$arResult["arUser"]["ACTIVE"]="Y";


}
else
{
    //v_dump($arResult);
	//$arResult["arUser"]["GROUP_ID"] = CUser::GetUserGroup($arResult["ID"]);
}

if($strError <> '')
{
	foreach($_POST as $k=>$val)
	{
		if(!is_array($val))
		{
			$arResult["arUser"][$k] = htmlspecialcharsex($val);
			$arResult["arForumUser"][$k] = htmlspecialcharsex($val);
		}
		else
		{
			$arResult["arUser"][$k] = $val;
			$arResult["arForumUser"][$k] = $val;
		}
	}

	$arResult["arUser"]["GROUP_ID"] = $GROUP_ID;
}

$arResult["FORM_TARGET"] = $APPLICATION->GetCurPage();

$arResult["arUser"]["PERSONAL_PHOTO_INPUT"] = CFile::InputFile("PERSONAL_PHOTO", 20, $arResult["arUser"]["PERSONAL_PHOTO"], false, 0, "IMAGE");

if (strlen($arResult["arUser"]["PERSONAL_PHOTO"])>0)
	$arResult["arUser"]["PERSONAL_PHOTO_HTML"] = CFile::ShowImage($arResult["arUser"]["PERSONAL_PHOTO"], 150, 150, "border=0", "", true);

$arResult["IS_ADMIN"] = $USER->IsAdmin();

$arResult["strProfileError"] = $strError;
$arResult["BX_SESSION_CHECK"] = bitrix_sessid_post();

$arResult["DATE_FORMAT"] = CLang::GetDateFormat("SHORT");

$arResult["COOKIE_PREFIX"] = COption::GetOptionString("main", "cookie_name", "BITRIX_SM");
if (strlen($arResult["COOKIE_PREFIX"]) <= 0) 
	$arResult["COOKIE_PREFIX"] = "BX";

// ********************* User properties ***************************************************
$arResult["USER_PROPERTIES"] = array("SHOW" => "N");
if (!empty($arParams["USER_PROPERTY"]))
{
	$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", $arResult["ID"], LANGUAGE_ID);
	if (count($arParams["USER_PROPERTY"]) > 0)
	{
		foreach ($arUserFields as $FIELD_NAME => $arUserField)
		{
			if (!in_array($FIELD_NAME, $arParams["USER_PROPERTY"]))
				continue;
			$arUserField["EDIT_FORM_LABEL"] = strLen($arUserField["EDIT_FORM_LABEL"]) > 0 ? $arUserField["EDIT_FORM_LABEL"] : $arUserField["FIELD_NAME"];
			$arUserField["EDIT_FORM_LABEL"] = htmlspecialcharsEx($arUserField["EDIT_FORM_LABEL"]);
			$arUserField["~EDIT_FORM_LABEL"] = $arUserField["EDIT_FORM_LABEL"];
			$arResult["USER_PROPERTIES"]["DATA"][$FIELD_NAME] = $arUserField;
		}
	}
	if (!empty($arResult["USER_PROPERTIES"]["DATA"]))
		$arResult["USER_PROPERTIES"]["SHOW"] = "Y";
	$arResult["bVarsFromForm"] = ($strError == ''? false : true);
}
// ******************** /User properties ***************************************************

if($arParams["SET_TITLE"] == "Y")
	$APPLICATION->SetTitle(GetMessage("PROFILE_DEFAULT_TITLE"));

if($bOk) 
	$arResult['DATA_SAVED'] = 'Y';

//time zones
$arResult["TIME_ZONE_ENABLED"] = CTimeZone::Enabled();
if($arResult["TIME_ZONE_ENABLED"])
	$arResult["TIME_ZONE_LIST"] = CTimeZone::GetZones();

//secure authorization
$arResult["SECURE_AUTH"] = false;
if(!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
{
	$sec = new CRsaSecurity();
	if(($arKeys = $sec->LoadKeys()))
	{
		$sec->SetKeys($arKeys);
		$sec->AddToForm('form1', array('NEW_PASSWORD', 'NEW_PASSWORD_CONFIRM'));
		$arResult["SECURE_AUTH"] = true;
	}
}

$this->IncludeComponentTemplate();
?>
