<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<style>
    .social {
    background:url(/tpl/images/social-sprites.png) no-repeat;  
    height:16px; 
    line-height:12px;        
    margin-right: 16px;
    width:16px;
    }
    .starrequired {color:red}
    #social1 {background-position: 0 -112px;}
    #social2 {background-position: -16px -96px;}
    #social3 {background-position: -32px -80px;}
    #social4 {background-position: -47px -64px;}
    #social5 {background-position: -64px -48px;}
    #social6 {background-position: -80px -32px;}
    #social7 {background-position: -96px -16px;}
    #social8 {background-position: -112px 0px; }
</style>
<div class="bx-auth-profile">

<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y') {
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
    LocalRedirect($APPLICATION->GetCurDir());
}
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>
<h1><?=GetMessage("EDIT_PROFILE")?></h1>
<!--<form id="user_profile" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">-->
<form id="user_profile" class="my_rest" method="post" name="form1" action="<?if(CSite::InGroup(Array(9)) && $_REQUEST["USER_ID"]==$USER->GetID()):?>/restorator/profile.php<?else:?>/users/id<?=$arResult["ID"]?>/<?endif;?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<div class="profile-block-<?=strpos($arResult["opened"], "reg") === false ? "hidden" : "shown"?>" id="user_div_reg">
    <div>
        <table width="100%" cellspacing="0" cellpadding="5">
            <tr>
                <td width="255" class="question"><?=GetMessage('NAME')?><span class="starrequired">*</span></td>
                <td><input type="text" class="inputtext" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" style="width:805px" /></td>
            </tr>
            <tr>
                <td width="255" class="question"><?=GetMessage('LAST_NAME')?><span class="starrequired">*</span></td>
                <td><input type="text" class="inputtext" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" style="width:805px" /></td>
            </tr>
            <tr>
                <td class="question"><?=GetMessage('NIK_NAME')?><span class="starrequired">*</span></td>
                <td><input type="text" class="inputtext" name="PERSONAL_PROFESSION" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PROFESSION"]?>" style="width:805px" /></td>
            </tr>
            <tr>
                <td class="question"><?=GetMessage('USER_GENDER')?></td>
                <td>
                    <script type="text/javascript">
                            $(document).ready(function(){
                                $("#sb_PERSONAL_GENDER").chosen();
                                $("#multi5").css("width","305px");
                                $("#multi5").attr("data-placeholder"," ");
                                $("#multi5").chosen({no_results_text: "<?=GetMessage("NO_RESULTS")?>"});
                            });
                    </script>
                    <select  data-placeholder="<?=$SELECT_TITLE?>" class="chzn-select" tabindex="2" name="PERSONAL_GENDER" id="sb_PERSONAL_GENDER" style="width:305px;">
                            <option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
                            <option value="M"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_MALE")?></option>
                            <option value="F"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="question"><?=GetMessage("USER_BIRTHDAY_DT")?></td>
                <td><input type="text" class="inputtext" id="PERSONAL_BIRTHDAY" name="PERSONAL_BIRTHDAY" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>"  style="width:805px" ></td>
            </tr>
            <tr>
                <td class="question"><?=GetMessage('USER_CITY')?></td>
                <td><input type="text" class="inputtext" name="PERSONAL_CITY" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>"  style="width:805px"  /></td>
            </tr>
            <tr>
                <td class="question"><?=GetMessage('USER_PHONE')?></td>
                <td>
                    <?$arResult["arUser"]["PERSONAL_PHONE"] = str_replace("+7", "", $arResult["arUser"]["PERSONAL_PHONE"])?>
                    <input type="text" class="inputtext telephone" name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>"  style="width:805px"  />
                </td>
            </tr>            
                <tr>
                    <td class="question">Я хочу получать информационную рассылку с этого сайта:</td>
                    <td>                        
                        <input type="checkbox" value="1" name="SUBSCRIBE" <?=(CSite::InGroup(31)||$_SESSION["GET_SUBSCRIBE"])?"checked":""?>></div>                        
                    </td>
                </tr>            
            <tr>
                <td class="question"><?=GetMessage('USER_HIDE_CONTACTS')?><?//v_dump($arResult["arUser"]["UF_HIDE_CONTACTS"])?></td>
                <td>
                <?// ********************* User properties ***************************************************?>
                <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                    <?//$first = true;?>
                    <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                        <?if($FIELD_NAME == "UF_HIDE_CONTACTS"):?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));
                            ?>
                        <?endif?>
                    <?endforeach;?>
                <?endif;?>
                <?// ******************** /User properties ***************************************************?>
                </td>
            </tr>
            <tr>
                <td class="question"><?=GetMessage('EMAIL')?><span class="starrequired">*</span></td>
                <td>
                    <input type="text" class="inputtext" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>"  style="width:805px"  />
                    <input type="hidden" name="LOGIN" maxlength="50" value="<?=(CSite::InGroup(array(16)))?$arResult["arUser"]["LOGIN"]:$arResult["arUser"]["EMAIL"]?>" />
                </td>
            </tr>        
            <tr>
                <td class="question"><?=GetMessage('NEW_PASSWORD_REQ')?></td>
                <td><input type="password" class="inputtext" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" style="width:805px"  /></td>
            </tr>        
            <!--<tr>
                <td class="question"><?=GetMessage('USER_NOTES')?></td>
                <td><textarea class="inputtext" name="PERSONAL_NOTES" style="width:805px" rows="10"><?=$arResult["arUser"]["PERSONAL_NOTES"]?></textarea></td>
            </tr>        -->
            <tr>
                <td class="question"><?=GetMessage('USER_WORK_INFO')?></td>
                <td><textarea class="inputtext" name="WORK_NOTES" style="width:805px"  rows="10"><?=$arResult["arUser"]["WORK_NOTES"]?></textarea></td>
            </tr>        	
	<?// ********************* User properties ***************************************************?>
	<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
		<?$first = true;?>
		<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
		<?if($FIELD_NAME != "UF_HIDE_CONTACTS"):?>
		<tr><td class="field-name">
			<?if ($arUserField["MANDATORY"]=="Y"):?>
				<span class="starrequired">*</span>
			<?endif;?>
			<?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td class="field-value">
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
        <?endif?>
		<?endforeach;?>
	<?endif;?>
	<?// ******************** /User properties ***************************************************?>
        </table> 
        <table>
            <tr>
                        <td class="question" style="padding-right:35px;"><?=GetMessage("USER_PHOTO")?></td>
                        <td>
                        <?
                        if (strlen($arResult["arUser"]["PERSONAL_PHOTO"])>0)
                        {
                        ?>
                        <br />
                            <?=$arResult["arUser"]["PERSONAL_PHOTO_HTML"]?>
                        <?
                        }
                        ?></td>
                        <td>
                        <?=$arResult["arUser"]["PERSONAL_PHOTO_INPUT"]?>
                        </td>                    
                </tr>
        </table>
        <h2 style="margin-top:0px;"><?=GetMessage("ANOTHER_ACCOUNTS")?></h2>
        <table width="100%" cellspacing="0" cellpadding="5">
            <tr>
                <td class="question"><div class="social" id="social1"></div></td>
                <td><input type="text" class="inputtext" name="WORK_WWW" value="<?=$arResult["arUser"]["WORK_WWW"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social2"></div></td>
                <td><input type="text" class="inputtext" name="WORK_PHONE" value="<?=$arResult["arUser"]["WORK_PHONE"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social3"></div></td>
                <td><input type="text" class="inputtext" name="WORK_FAX" value="<?=$arResult["arUser"]["WORK_FAX"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social4"></div></td>
                <td><input type="text" class="inputtext" name="WORK_PAGER" value="<?=$arResult["arUser"]["WORK_PAGER"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social5"></div></td>
                <td><input type="text" class="inputtext" name="WORK_STATE" value="<?=$arResult["arUser"]["WORK_STATE"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social6"></div></td>
                <td><input type="text" class="inputtext" name="WORK_ZIP" value="<?=$arResult["arUser"]["WORK_ZIP"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social7"></div></td>
                <td><input type="text" class="inputtext" name="WORK_CITY" value="<?=$arResult["arUser"]["WORK_CITY"]?>" style="width:905px" ></td>
            </tr>
            <tr>
                <td class="question"><div class="social" id="social8"></div></td>
                <td><input type="text" class="inputtext" name="WORK_MAILBOX" value="<?=$arResult["arUser"]["WORK_MAILBOX"]?>" style="width:905px" ></td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
</div>
	<!--<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>-->
        <input type="hidden" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
        <br />
	<p align="right"><input class="light_button" type="submit"  value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>"></p>
	<!--<p align="center"><input onclick="sumbit_form(this.form);return false;" class="light_button" type="submit"  value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>"></p>-->
</form>

</div>
<br />