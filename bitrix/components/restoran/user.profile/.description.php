<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("COMP_MAIN_PROFILE_TITLE"),
	"DESCRIPTION" => GetMessage("COMP_MAIN_PROFILE_DESCR"),
	"ICON" => "/images/user_profile.gif",
        "CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "users_restaurant", // for example "my_project"
        "NAME" => GetMessage("USERS_RESTAURANT"),
	),
);
?>