$(document).ready(function() {
    $('.razmes_a').on('click', function() {
        // scroll to
        var pos = $('#plans_div').offset().top-10;
        $('html,body').animate({scrollTop: pos}, "300");

        // set rest id
        var restID = $(this).attr('restID');
        //var action = $(this).attr('action');
        $('.REST_ID').val(restID);
    });

    // delete rest
    $('.razmes_del').on('click', function() {
        if(confirm("Удалить размещение ?")) {
            var restID = $(this).attr('restID');
            $.ajax({
                url: "/bitrix/components/restoran/user.restoraunts_pay_list/templates/.default/del_rest.php",
                cache: false,
                type: 'post',
                data: {restID : restID},
                success: function(data) {
                    // some action
                    location.reload();
                }
            });
        }
    });
});