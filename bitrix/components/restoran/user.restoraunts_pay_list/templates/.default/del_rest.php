<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!CModule::IncludeModule("iblock"))
    return;

if(CIBlockElement::SetPropertyValuesEx($_REQUEST["restID"], false, array("account_bind" => '')))
    return true;
?>