<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<table class="profile-activity w100">
    <tr>
        <th class="first"><?=GetMessage("TH_1")?></th>
        <th><?=GetMessage("TH_2")?></th>
        <th><?=GetMessage("TH_3")?></th>
        <th><?=GetMessage("TH_4")?></th>
        <th><?=GetMessage("TH_5")?></th>
    </tr>
    <?
    $l=1;
    
    ?>
    <?foreach($arResult["RESTS"] as $rest):?>
    <tr class="<? if($l==count($arResult["RESTS"])) echo 'last';?>">
        <td class="first">
            <img src="<?=$rest["PICTURE"]["src"]?>" width="<?=$rest["PICTURE"]["width"]?>" height="<?=$rest["PICTURE"]["height"]?>" />
            <p class="name"><a href="#"><?=$rest["NAME"]?></a></p>
            <p class="profile-activity-rating">
            <div class="stars_div">
                <?for($i = 0; $i < 5; $i++):?>
                    <div class="small_star<?if($i < $rest["RATIO"]):?>_a<?endif?>"></div>
                <?endfor?>
            </div>
            </p>
            <p class="upcase"><?=$rest["SECTION_NAME"]?></p>
        </td>
        <td>
            <p<?if($rest["ACCOUNT_NAME"]):?> class="upcase-premium"<?endif?>><?=($rest["ACCOUNT_NAME"] ? $rest["ACCOUNT_NAME"] : GetMessage("NOT_AVAILABLE"))?></p>
        </td>
        <td>
            <?foreach($rest["PRIORITIES"] as $prior):?>
                <p><?=$prior["NAME"]?></p>
                <p style="white-space: nowrap;"> до <?=$prior["ACCOUNT_VALIDITY"]?></p>
                <p><em><a href="order.php?ID=<?=$prior["PRIORITY_EXTEND_ID"]?>&resto_bind=<?=$rest["ID"]?>&ext_order_id=<?=$prior["EXTEND_ORDER_ID"]?>&action=extend_priority">Продлить</a></em></p>
            <?endforeach?>
            <?if(!$rest["PRIORITIES"]):?>
                <p>Нет</p>
            <?endif?>
            <p><em><a href="order.php?resto_bind=<?=$rest["ID"]?>&action=buy_priority">Приобрести</a></em></p>
        </td>
        <td>
            <?=$rest["ACCOUNT_DESCRIPTION"]?>
            <?if(!$rest["ACCOUNT_DESCRIPTION"]):?>
                Для размещения ресторана на портале
                необходимо приобрести <a class="razmes_a" restID="<?=$rest["ID"]?>" href="javascript:void(null)">тип размещения</a>
            <?endif?>
        </td>
        <td>
            <div class="activity-actions">
                <?if($rest["ACCOUNT_NAME"]):?>
                    <p class="activity-date"><?=$rest["ACCOUNT_VALIDITY"]?></p>
                    <div>
                        <a class="icon-continue" href="order.php?ID=<?=$rest["ACCOUNT_EXTEND_ID"]?>&resto_bind=<?=$rest["ID"]?>&ext_order_id=<?=$rest["EXTEND_ORDER_ID"]?>&action=extend_account">Продлить</a>
                        <a class="icon-delete razmes_del" restID="<?=$rest["ID"]?>" href="javascript:void(null)">Удалить</a>
                    </div>
                <?else:?>
                    <p class="activity-date">-</p>
                    <div>
                        <a class="razmes razmes_a" restID="<?=$rest["ID"]?>" href="javascript:void(null)">Разместить</a>
                    </div>
                <?endif?>
            </div>
        </td>
    </tr>
    <?
    $l++;
    ?>
    <?endforeach?>
</table>