<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
    return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arComponentParameters = array(
"GROUPS" => array(
    ),
        "PARAMETERS" => array(
            "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => "IBLOCK_TYPE",
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "USER_ID" => array(
            "PARENT" => "BASE",
            "NAME" => 'USER_ID',
            "TYPE" => "STRING",
            "DEFAULT" => $USER->GetID(),
        ),
    ),
);
?>