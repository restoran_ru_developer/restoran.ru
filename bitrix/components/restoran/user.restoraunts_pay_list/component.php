<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("sale"))
    return;

if(!$USER->IsAuthorized())
    return;

global $USER;

if(!$arParams["IBLOCK_TYPE"])
    $arParams["IBLOCK_TYPE"] = "catalog";

if(!$arParams["USER_ID"])
    $arParams["USER_ID"] = $USER->GetID();

// get iblock list
$rsCityIblock = CIBlock::GetList(
    Array(),
    Array(
        "ACTIVE" => "Y",
        "SITE_ID" => SITE_ID,
        "TYPE" =>  $arParams["IBLOCK_TYPE"]
    ),
    false
);
while($arCityIblock = $rsCityIblock->Fetch()) {
    // get rest list
    $rsRests = CIBlockElement::GetList(
        Array(),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $arCityIblock["ID"],
            "PROPERTY_user_bind" => $arParams["USER_ID"]
        ),
        false,
        false,
        Array("ID", "IBLOCK_SECTION_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_ratio", "PROPERTY_account_bind", "PROPERTY_priority_bind")
    );
    $arTmpRest = Array();
    while($arRest = $rsRests->Fetch()) {
        $arTmpRest = Array();
        $priorKey = 0;

        $arTmpRest["ID"] = $arRest["ID"];
        $arTmpRest["NAME"] = $arRest["NAME"];
        $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
        $arTmpRest["PICTURE"] = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width'=>73, 'height'=>60), BX_RESIZE_IMAGE_PROPORTIONAL, true);

        // get section name
        $rsRestSec = CIBlockSection::GetByID($arRest["IBLOCK_SECTION_ID"]);
        if($arRestSec = $rsRestSec->Fetch())
            $arTmpRest["SECTION_NAME"] = $arRestSec["NAME"];

        /* get account purchase info */
        if($arRest["PROPERTY_ACCOUNT_BIND_VALUE"]) {
            // get name
            $rsBasket = CSaleBasket::GetList(
                array("ID" => "DESC"),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID"  => $arRest["PROPERTY_ACCOUNT_BIND_VALUE"]
                ),
                false,
                false,
                array()
            );
            $arBasket = $rsBasket->Fetch();

            $arTmpRest["ACCOUNT_NAME"] = $arBasket["NAME"];
            $arTmpRest["ACCOUNT_EXTEND_ID"] = $arBasket["PRODUCT_ID"];
            $arTmpRest["EXTEND_ORDER_ID"] = $arRest["PROPERTY_ACCOUNT_BIND_VALUE"];
            // get account validity
            $rsOrderProps = CSaleOrderPropsValue::GetList(
                array(),
                array(
                    "ORDER_ID" => $arRest["PROPERTY_ACCOUNT_BIND_VALUE"],
                    "CODE" => 'validity',
                ),
                false,
                false,
                array()
            );
            $arOrderProps = $rsOrderProps->Fetch();
            $arTmpRest["ACCOUNT_VALIDITY"] = $arOrderProps["VALUE"];
            // get account desc
            $rsAccInfo = CIBlockElement::GetByID($arBasket["PRODUCT_ID"]);
            $arAccInfo = $rsAccInfo->Fetch();
            $arTmpRest["ACCOUNT_DESCRIPTION"] = $arAccInfo["PREVIEW_TEXT"];
        }

        /* get priority purchase info */
        foreach($arRest["PROPERTY_PRIORITY_BIND_VALUE"] as $priorKey=>$priorityOrderID) {
            // get name
            $rsBasket = CSaleBasket::GetList(
                array("ID" => "DESC"),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID"  => $priorityOrderID
                ),
                false,
                false,
                array()
            );
            $arBasket = $rsBasket->Fetch();
            $arTmpRest["PRIORITIES"][$priorKey]["NAME"] = $arBasket["NAME"];
            $arTmpRest["PRIORITIES"][$priorKey]["PRIORITY_EXTEND_ID"] = $arBasket["PRODUCT_ID"];
            $arTmpRest["PRIORITIES"][$priorKey]["EXTEND_ORDER_ID"] = $priorityOrderID;
            // get account validity
            $rsOrderProps = CSaleOrderPropsValue::GetList(
                array(),
                array(
                    "ORDER_ID" => $priorityOrderID,
                    "CODE" => 'validity',
                ),
                false,
                false,
                array()
            );
            $arOrderProps = $rsOrderProps->Fetch();
            $arTmpRest["PRIORITIES"][$priorKey]["ACCOUNT_VALIDITY"] = $arOrderProps["VALUE"];
        }

        $arResult["RESTS"][] = $arTmpRest;
    }
}

$this->IncludeComponentTemplate();
?>