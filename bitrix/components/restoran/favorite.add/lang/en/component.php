<?
$MESS ['IBLOCK_MODULE_NOT_INSTALLED'] = "Information blocks module is not installed";
$MESS ['ERROR_IBLOCK'] = "Unknown Favorites iblock";
$MESS ['ERROR_SECTION'] = "Unknown Favorites section";
$MESS ['ERROR_ID'] = "Unknown element id";
$MESS ['ERROR_NAME'] = "Unknown element name";
$MESS ['ERROR_USER'] = "Authorization error";
?>