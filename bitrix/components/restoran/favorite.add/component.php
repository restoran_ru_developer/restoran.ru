<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arParams["ELEMENT_ID"] = intval($arParams["ELEMENT_ID"]);
$arParams["ELEMENT_NAME"] = trim(rawurldecode($arParams["ELEMENT_NAME"]));
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION"] = intval($arParams["SECTION1"]);
$error = array();
$arr = array();
if(!CModule::IncludeModule("iblock"))
{
    $this->AbortResultCache();
    $error[] = GetMessage("IBLOCK_MODULE_NOT_INSTALLED");
    return;
}
if (!$USER->IsAuthorized())
    $error[] = GetMessage("ERROR_USER");
if ($arParams["IBLOCK_ID"]<=0)
    $error[] = GetMessage("ERROR_IBLOCK_ID");
if ($arParams["ELEMENT_ID"]<=0)
    $error[] = GetMessage("ERROR_ID");
if ($arParams["SECTION_ID"]<=0)
{
    if (count($error)<=0)
    {        
        if ($arParams["SECTION"]==1) //это для рецептов
        {
            $res = CIBlockSection::GetByID($arParams["ELEMENT_ID"]);
            if($ar_res = $res->GetNext())  
            {
                //$section_id = $ar_res["IBLOCK_CODE"];
                $name = $ar_res["NAME"];
            }
        }
        else
        {
            $res = CIBlockElement::GetByID($arParams["ELEMENT_ID"]);
            if($ar_res = $res->GetNext())  
            {
                //v_dump($ar_res);
                $section_id = $ar_res["IBLOCK_SECTION_ID"];
                $name = $ar_res["NAME"];
            }
        }
        /*$res = CIBlockSection::GetById($section_id);
        if($ar_res = $res->GetNext())   
            $section_code = $ar_res["CODE"];*/
        $res = CIBlockSection::GetList(Array(),Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"],'CODE'=>$ar_res["IBLOCK_TYPE_ID"]));
        if($ar_res = $res->GetNext())   
            $arParams["SECTION_ID"] = $ar_res["ID"];
    }
    if (!$arParams["SECTION_ID"])
        $error[] = GetMessage("ERROR_SECTION_ID");
}
if (count($error)>0)
{
    $arr["STATUS"] = 0;
    $arr["ERROR"] = implode("<br />",$error);
    $arLoadProductArray = Array(  
        "CREATED_BY"    => $USER->GetID(), 
        "MODIFIED_BY"    => $USER->GetID(), 
        "IBLOCK_SECTION_ID" => $arParams["SECTION_ID"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],  
        "NAME" => $name,  
        "ACTIVE" => "Y",            
        "PREVIEW_TEXT" => $arParams["ELEMENT_ID"]
    );
    //v_dump($arLoadProductArray);
    echo json_encode($arr);
    return;
}
$r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"CREATED_BY"=>$USER->GetID(),"PREVIEW_TEXT"=>$arParams["ELEMENT_ID"]));
if ($ar = $r->Fetch())
{
    $arr["STATUS"] = 0;
    $arr["ERROR"] = implode("<br />",$error);
    $arr["MESSAGE"] = "Элемент уже в избранном";
    echo json_encode($arr);
}
else
{
    $el = new CIBlockElement();
    $arLoadProductArray = Array(  
            "CREATED_BY"    => $USER->GetID(), 
            "MODIFIED_BY"    => $USER->GetID(), 
            "IBLOCK_SECTION_ID" => $arParams["SECTION_ID"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],  
            "NAME" => $name,  
            "ACTIVE" => "Y",            
            "PREVIEW_TEXT" => $arParams["ELEMENT_ID"]
        );
        //v_dump($arLoadProductArray);
    if($PRODUCT_ID = $el->Add($arLoadProductArray))  
    {
        $arr["STATUS"] = 1;
        $arr["ERROR"] = implode("<br />",$error);
        $arr["MESSAGE"] = "Элемент добавлен в избранное";
        echo json_encode($arr);
    }
    else  
    {
        $error[] = $el->LAST_ERROR;
        $arr["STATUS"] = 0;
        $arr["ERROR"] = implode("<br />",$error);    
        echo json_encode($arr);
    }
}
?>