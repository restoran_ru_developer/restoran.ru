<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["PAGE"] = ($_REQUEST["PAGEN_1"])?$_REQUEST["PAGEN_1"]:1;
$arParams["NAME"] = trim($_REQUEST["user_name"]);
$arParams["EMAIL"] = trim($_REQUEST["email"]);
$arParams["GENDER"] = trim($_REQUEST["gender"]);
$arParams["PAGE_COUNT"] = ($arParams["PAGE_COUNT"])?$arParams["PAGE_COUNT"]:"20";
if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 24*30*60;
}
global $USER;
if (!$USER->IsAuthorized())
{
    ShowError("NO_AUTHORIZED");
    die;
}
if (!$arParams["USER_ID"])
{
    ShowError("NO_AUTHORIZED");
    die;
}
if($this->StartResultCache(false, array($arParams,$USER->GetGroups())))
{
    CModule::IncludeModule("sale");
    $res = CSaleUserTransact::GetList(Array("TIMESTAMP_X"=>"DESC"),Array("USER_ID"=>$arParams["USER_ID"]));    
    while($ar = $res->GetNext()) :       
        $arResult["ITEMS"][] = $ar;
    endwhile;
    //$arResult["NAV_STRING"] = $rsUsers->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
    //$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
    //$arResult["NAV_RESULT"] = $rsUsers;
    $this->SetResultCacheKeys(array(
            "ITEMS",
    ));
    $this->IncludeComponentTemplate();
}
?>