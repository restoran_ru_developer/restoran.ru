<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <!--<h2><?=GetMessage("FRIENDS_TITLE")?></h2>-->
<?if ($arParams["AJAX"]=="Y"):?>
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <link href="<?=$templateFolder?>/style.css?<?=rand(0,199)?>" type="text/css" rel="stylesheet" />
<?endif;?>
<input type="button" id="add_transaction" class="light_button" user_id="<?=$arParams["USER_ID"]?>" value="+Добавить транзакцию" />
<div style="display:none">
    <form id='add_trransaction_form'>
        <input type='hidden' name='act' value='add_transaction'>
        <input type='hidden' id='user_id' name='id' value='<?=$arParams["USER_ID"]?>'>
        <table>
            <tr>
                <td>Сумма:</td>
                <td><input type=text class=inputtext name=price> <span class="grey"><i>Например:+100</i></span></td>
            </tr>
            <tr>
                <td>Описание:</td>
                <td><input type='text' class=inputtext size=50 name='description'> <input class='dark_button' type='submit' value='Добавить' /></td>
            </tr>
        </table>
        <hr />
    </form>
</div>
<?if (count($arResult["ITEMS"][0])>0):?>
    <div class="account_details_box">
        <table class="account_details" cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <th>Дата транзакции</th>
                <th width="80">Сумма</th>
                <th>Комментарий</th>
            </tr>
            <?foreach ($arResult["ITEMS"] as $cell=>$arItem):?>
                <tr>
                    <td><?=$arItem["TRANSACT_DATE"]?></td>
                    <td align="center"><?=(($arItem["DEBIT"]=="Y")?"+":"-").sprintf("%01.0f",$arItem["AMOUNT"])?></td>
                    <td><?=$arItem["DESCRIPTION"]?></td>
                </tr>
            <?endforeach;?>
        </table>
    </div>
<?else:?>
    <tr><td><div class="errortext">
        <?if ($arParams["ID"]==$USER->GetID()):?>
            <?=GetMessage("NO_YOU_FRIENDS")?>
        <?else:?>
            <?=GetMessage("NO_FRIENDS")?>
        <?endif;?>
    </div></td></tr>
<?endif;?>