<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();
//global $BestIblockUpdateNote,$RestListExceptionFirst20Ids;//,$First20ElementCnt,$First20SectionId;
$BestIblockUpdateNote = '';
$RestListExceptionFirst20Ids = array();
if ($this->StartResultCache(false,$_REQUEST["PROPERTY"].$_REQUEST["PROPERTY_VALUE"]))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    if($_REQUEST['PROPERTY']){

        $PROPERTY_CODE = $_REQUEST["PROPERTY"];

        $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y','CODE'=>$PROPERTY_CODE,'ELEMENT_SUBSECTIONS'=>'N','CNT_ACTIVE'=>"Y",'DEPTH_LEVEL'=>1);
        $db_list = CIBlockSection::GetList(Array(), $arFilter, true);
        if($ar_result = $db_list->Fetch())
        {
            $First20ElementCnt = $ar_result['ELEMENT_CNT'];
            $First20SectionId = $ar_result['ID'];
        }

        if($_REQUEST["PROPERTY_VALUE"]!='y'){
            $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y','CODE'=>$_REQUEST["PROPERTY_VALUE"],'SECTION_ID'=>$ar_result['ID'],'ELEMENT_SUBSECTIONS'=>'N','CNT_ACTIVE'=>"Y");
            $db_list = CIBlockSection::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), $arFilter, true);
            if($ar_result = $db_list->Fetch())
            {
                $First20ElementCnt = $ar_result['ELEMENT_CNT'];
                $First20SectionId = $ar_result['ID'];
            }
        }

        if ($First20ElementCnt > 0 && $First20SectionId) {
            $arResult['First20SectionId'] = $First20SectionId;
        }

        if($First20SectionId){
            $rsElement = CIBlockElement::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), array('IBLOCK_ID'=>$arParams['IBLOCK_ID'],'SECTION_ID'=>$First20SectionId,'ACTIVE'=>'Y'), false, array('nTopCount'=>20), array('PROPERTY_RESTAURANT','ID'));
            while($obElement = $rsElement->Fetch())
            {
                $arResult['BestIblockUpdateNote'] .= $obElement['ID'];
                $arResult['RestListExceptionFirst20Ids']['!ID'][] = $obElement['PROPERTY_RESTAURANT_VALUE'];
            }
        }
    }
    else {  //    catalog index page

        $rsElement = CIBlockElement::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), array('IBLOCK_ID'=>$arParams['IBLOCK_ID'],'SECTION_ID'=>false,'ACTIVE'=>'Y'), false, array('nTopCount'=>20), array('PROPERTY_RESTAURANT','ID'));
        while($obElement = $rsElement->Fetch())
        {
            $arResult['BestIblockUpdateNote'] .= $obElement['ID'];
            $arResult['RestListExceptionFirst20Ids']['!ID'][] = $obElement['PROPERTY_RESTAURANT_VALUE'];
            $arResult['First20SectionId'] = true;
        }
    }

    $this->SetResultCacheKeys(array(
        "BestIblockUpdateNote",
        "RestListExceptionFirst20Ids",
        "First20SectionId"
    ));
    $this->IncludeComponentTemplate();
}
?>