<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["NO_COMMENTS"] = "Be the first to review";
$MESS ["ALL_REVIEWS"] = "ALL REVIEWS";
$MESS ["ONLY_AUTHORIZED"] = "Only authorized users can write reviews";
$MESS ["CLICK_TO_RATE"] = "Click to rate restaurant";
?>