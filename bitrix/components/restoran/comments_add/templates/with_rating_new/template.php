<br /><div class="black_2hr"></div>
<h2>Ваш отзыв</h2>
   <script>
       $(document).ready(function(){
           $('#file-field').customFileInput();
           $('#video-field').customFileInput();
            $("#add_photo").click(function(){
                var count = $(".file-field").length;
                $(this).prev().after('<input type="file" name="file[]" value="" id="file-field'+count+'" class="file-field" />');
                $('#file-field'+count).customFileInput();
            });
            /*$.tools.validator.localize("ru", {
                '*'	: 'Поле обязательно для заполнения',
                '[req]'	: 'Поле обязательно для заполнения'
            });
            $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
                if (!value)
                    return false;
                return true;
            });*/
            /*$("#comments > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize()+"&"+"<?=bitrix_sessid_get()?>",
                        success: function(data) {
                            if (!$("#comment_modal").size())
                            {
                                $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");                                                               
                            }
                            $('#comment_modal').html(data);
                                showOverflow();
                                setCenter($("#comment_modal"));
                                $("#comment_modal").fadeIn("300"); 
                            $("#review").html("");
                            location.reload();
                        }
                    });
                    e.preventDefault();
                }
            });
            $("#comments > form").bind("onFail", function(e, errors)  {
                if (e.originalEvent.type == 'submit') {
                        $.each(errors, function()  {
                                var input = this.input;	                        
                        });
                }
            });*/
        $('#comment_form').ajaxForm({
		beforeSubmit: check_editor_form,
		success: function(data) {
                    if(data)
                    {
                        data = eval('('+data+')');                        
                        if (!$("#comment_modal").size())
                        {
                            $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");
                        }
                        var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
                        var html2 = '</div>';                    
                        $('#comment_modal').html(html+data.MESSAGE+html2);
                        showOverflow();
                        setCenter($("#comment_modal"));
                        $("#comment_modal").fadeIn("300");
                        if (data.ERROR=="1")
                        {
                            $("#add_commm").attr("disabled",false);
                        }
                        if (data.STATUS=="1")
                            setTimeout("location.reload()","3500");
                    }
                    return false;
		}                
	});
        function check_editor_form()
        {            
            if (ajax_load)
                        return false;
            if (!$("#input_ratio").val()) {
                alert("Не выбран рейтинг");
                return false;
            }
            if (!$("#review").val() && parseInt($("#input_ratio").val()) > 0) {
                alert("Заполните поле комментарий");
                return false;
            }
            $("#add_commm").attr("disabled","disabled");
        }
    });
   </script>
<?if (!$USER->IsAuthorized()):?>
    <?$APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "no_auth",
            Array(
                    "REGISTER_URL" => "",
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "",
                    "SHOW_ERRORS" => "N"
            ),
    false
    );?> 
    <br /><br />
<?endif;?>
   <form id="comment_form" action="/bitrix/components/restoran/comments_add/ajax.php" method="post" novalidate="novalidate">
       <?=bitrix_sessid_post()?>
        <div class="active_rating">
            <?for ($i=1;$i<=5;$i++):?>
                <div class="star" alt="<?=$i?>"></div>
            <?endfor;?>
        </div>
       <div class="grey left" style="margin-left:10px;"><i>&ndash; <?=GetMessage("CLICK_TO_RATE")?></i></div>
        <div class="clear"></div> 
        <br />
        <?if (!$USER->IsAuthorized()):?>
            <div class="grey_block" style="margin-bottom:10px">
                <div class="uppercase">E-mail:</div>
                <input class="inputtext-with_border font14" value="" name="email" req="req" style="width:665px" /><br />           
            </div>
        <?endif?>
        <div class="grey_block" style="margin-bottom:10px;">
            <div class="left">
                <div class="uppercase">ДОСТОИНСТВА</div>
                <textarea class="add_review plus" name="plus" req="req" style="width:315px"></textarea>
            </div>
            <div class="right">
                <div class="uppercase">НЕДОСТАТКИ</div>
                <textarea class="add_review" name="minus" req="req" style="width:315px"></textarea>
            </div>
            <div class="clear"></div>
        </div>
        <div class="grey_block" style="margin-bottom:10px;">
            <div class="uppercase">КОММЕНТАРИЙ</div>
            <textarea class="add_review" id="review" name="review" req="req" style="width:665px"></textarea>
            <input type="hidden" value="" id="input_ratio" name="ratio" />
        </div>
        <div class="grey_block" style="margin-bottom:10px;">
            <div class="uppercase">Добавить фотографии</div>
            <input type="file" name="file[]" value="" id="file-field" class="file-field" />
            <a class="uppercase" id="add_photo" style="color:#1a1a1a; border-bottom:1px dotted #1a1a1a">ЕЩЕ</a>
        </div>
        <div class="grey_block">
            <div class="uppercase">Добавить видео</div>
            <input type="file" name="video" value="" id="video-field" class="file-field" />
            <div class="uppercase">или ссылка с Youtube</div>
            <input type="text" name="video_youtube" value="" class="inputtext-with_border font14" style="width:665px"  />
            <br />
            <div style="padding-left:5px; padding-top:5px;">
            <?if (!$USER->IsAuthorized()):?>
                <div class="QapTcha"></div>
                <link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
                <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
                <script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
                <script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
                <script type="text/javascript">
                        $(document).ready(function(){
                                $('.QapTcha').QapTcha({
                                        txtLock : 'Сдвиньте слайдер вправо.',
                                        txtUnlock : 'Готово. Теперь можно отправлять',
                                        disabledSubmit : true,
                                        autoRevert : true,
                                        PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
                                        autoSubmit : false});
                        });
                </script>
            <?endif;?>
                <div class="right" style="margin-top:5px;">
                    <input type="submit" id="add_commm" class="light_button" value="+ Опубликовать">       
                </div>
                <div class="clear"></div>
            </div>
        </div>
       <div>                    
            <Br />            
           <!--<div class="figure_link" style="margin-left:0px;">
               <input type="submit" value="+ Комментировать">
           </div>-->
            <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
           <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
           <input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
           <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />
       </div>
        <div id="rating_overlay">
            <div class="close"></div>                           
        </div>
       <div class="clear"></div>
   <div class="grey left">
        <p>Дорогие друзья! Помните, что администрация сайта будет удалять:</p>
        <p>1. Комментарии с грубой и ненормативной лексикой<br />
        2. Прямые или косвенные оскорбления героя поста или читателей<br />
        3. Короткие оценочные комментарии («ужасно», «класс«, «отстой»)<Br />
        4. Комментарии, разжигающие национальную и социальную рознь<br />
        <p><a href="#">Полная версия правил Restoran.ru</a></p>
   </div>
   <!--<div class="right">
       <input type="submit" id="add_commm" class="light_button" value="+ Опубликовать">
   </div>-->
   <div class="clear"></div>
</form>
