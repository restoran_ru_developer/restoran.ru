<script>
       $(document).ready(function(){
            $.tools.validator.localize("ru", {
                '*'	: 'Поле обязательно для заполнения',
                '[req]'	: 'Поле обязательно для заполнения'
            });
            $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
                if (!value)
                    return false;
                return true;
            });
            $(".add_commm").attr("disabled",false);
            $(".submit_comment_form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $(".add_commm").attr("disabled","disabled");
                    if (ajax_load)
                        return false;
                    $.ajax({
                        type: "POST",
                        //dataType: "json",
                        url: form.attr("action"),
                        data: form.serialize()+"&"+"<?=bitrix_sessid_get()?>",
                        success: function(data) {
                            /*$("#rating_overlay").html(data);
                            //$(".error").hide();
                             $('#rating_overlay').overlay({
                                top: 260,                    
                                closeOnClick: false,
                                closeOnEsc: false,
                                api: true
                            }).load();*/
                            if (!$("#comment_modal").size())
                            {
                                $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");                                                               
                            }
                            var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
                            var html2 = '</div>';
                            data = eval('('+data+')');                            
                            $('#comment_modal').html(html+data.MESSAGE+html2);
                            showOverflow();
                            setCenter($("#comment_modal"));
                            $("#comment_modal").fadeIn("300");
                            if (data.ERROR=="1")
                            {
                                $(".add_commm").attr("disabled",false);
                            }
                            if (data.STATUS=="1")
                                setTimeout("location.reload()","3500");
                        }
                    });
                    e.preventDefault();
                }
            });
            $(".submit_comment_form").bind("onFail", function(e, errors)  {
                if (e.originalEvent.type == 'submit') {
                        $.each(errors, function()  {
                                var input = this.input;	                        
                        });
                }
            });
            $("#wr_cm").click(function(){
                $(this).hide();
                $("#write_comment").show("500");                
            });            
    });
   </script>
   <?if ($arParams["OPEN"]=="Y"):?>
    <div align="right">
            <input type="button" id="wr_cm" class="light_button" value="<?=GetMessage("LEAVE_COMMENT")?>"/>        
        </div>
   <?endif;?>
   <div id="write_comment" <?=($arParams["OPEN"]=="Y")?'style="display:none"':''?>> 
        <?if (!$USER->IsAuthorized()):?>
            <br /><Br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form",
                    "no_auth",
                    Array(
                            "REGISTER_URL" => "",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "",
                            "SHOW_ERRORS" => "N"
                    ),
            false
            );?> 
            <br /><br />
        <?endif;?>
        <h2><?=GetMessage("YOUR_COMMENT")?></h2>
        <form action="/bitrix/components/restoran/comments_add/ajax.php" class="submit_comment_form" method="post" novalidate="novalidate">
            <?=bitrix_sessid_post()?>
            <?if (!$USER->IsAuthorized()):?>
                <div class="uppercase font10 ls1" style="padding-left:6px;">E-mail:</div>
                <input class="inputtext-with_border font14" value="" name="email" req="req" style="width:270px" /><br />
                <div class="uppercase font10 ls1" style="padding-left:6px;padding-top:5px;"><?=GetMessage("R_COMMENT")?>:<input id="ch" name="ch" type="checkbox" value="1" /></div>        
            <?endif?>
            <textarea class="add_review" id="review" name="review" req="req" style="width:698px; margin-bottom:0px;"></textarea>            
            <div class="grey_block" style="padding:10px;">                
                <?if (!$USER->IsAuthorized()):?>
                    <div class="QapTcha"></div>                    
                <?endif;?>
                    <div class="right" style="margin-top:5px;">
                        <input type="submit" disabled="disabled"  class="add_commm light_button" value="<?=GetMessage("R_COMMENT_ADD")?>"/>        
                    </div>
                    <div class="clear"></div>
            </div>                        
            <br />
            <!--<div align="right">
                <input type="submit" id="add_commm" class="light_button" value="Комментировать"/>        
            </div>            -->
                <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
                <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
                <input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
                <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />
                <input type="hidden" name="PARENT" value="<?=$arParams["PARENT"]?>" />
                <input type="hidden" name="cpt" value="<?=$arParams["MY_CAPTCHA"]?>" />
                <div class="clear"></div>
        </form>
   </div>