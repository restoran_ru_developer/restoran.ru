<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["SAVE_COMMENT"] = "Спасибо, Ваш комментарий сохранен.";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут оставлять комментарии.";
$MESS ["LEAVE_COMMENT"] = "Написать комментарий";
$MESS ["YOUR_COMMENT"] = "Ваш комментарий";
$MESS ["R_COMMENT"] = "Комментарий";
$MESS ["R_COMMENT_ADD"] = "Комментировать";
?>