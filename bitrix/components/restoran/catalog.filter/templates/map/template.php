<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<script>
    var map_fil = "";
</script>
<div id="new_filter">    
        <div class="left fil">
            <form action="/<?=CITY_ID?>/catalog/restaurants/all" id="map_fil" method="get" name="rest_filter_form">
                <input type="hidden" name="page" value="1">
                <?
                $p=0;
                $ar_cusel = Array(1,12,4,2,3);
                foreach($arResult["arrProp"] as $prop_id => $arProp)
                {
                ?>
                    <div class="filter_block left" id="filter<?=$prop_id?>">                        
                        <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                        <script>
                            $(document).ready(function(){
                                $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=($ar_cusel[$p]==4)?"784px":(($ar_cusel[$p]==12)?"306px":"355px")?>"}});
                                var params = {
                                    changedEl: "#multi<?=$ar_cusel[$p]?>",
                                    scrollArrows: true,
                                    visRows:15
                                }
                                cuSelMulti(params);
                            })
                        </script>
                        <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                            <!--<div class="title"><?=$arProp["NAME"]?></div>-->
                            <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="15">
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                    <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=($arProp["CODE"]=="subway")?$val["NAME"]:$val?></option>
                                <?endforeach?>
                            </select>
                            <br /><br />
                            <div align="center"><input class="filter_button" filCODE="<?=$arProp["CODE"]?>" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                            <?if($arProp["CODE"]=="subway"):?>
                                <script>
                                    function getMetroCoords(filter)
                                    {
                                        jQuery.get ("/tpl/ajax/get_metro_coords.php?<?=bitrix_sessid_get()?>", filter, function ( data ) {
                                            console.log(data[0]);
                                            if (data)
                                            {
                                                map.panTo(new YMaps.GeoPoint(data[1],data[0]), {flying: 1});
                                                UpdateMarkers();
                                            }
                                        },"json");
                                    }
                                </script>
                            <?endif;?>
                        </div>
                    </div>        
                <?             
                        $p++;
                }
                ?>  
                <div class="left">
                    <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
                </div>
            </form>
        </div>
        <div class="right links_and_search">                
                <div class="left" style="line-height:28px;">
                    <div align="center" class="left" style="margin-right:10px;padding-right:10px;">
                            <a href="/<?=CITY_ID?>/map/near/" class="another white"><i><?=GetMessage("NEAR_YOU")?></i></a> 
                    </div>
                    <div align="center" class="left" style="margin-right:10px;">
                            <a href="/<?=CITY_ID?>/filter/" class="another  white"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>                             
                    </div>

                </div> 
                <div class="right">
                <?/*$APPLICATION->IncludeComponent(
                                "bitrix:search.title",
                                (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_suggest":"main_new",
                                Array(),
                            false
                        );*/?>
                </div>
                <div class="clear"></div>
        </div>
    <div class="clear"></div>
</div>
<div id="new_filter_results">
    <ul>
        
    </ul>
    <div class="clear"></div>
</div>
<br />