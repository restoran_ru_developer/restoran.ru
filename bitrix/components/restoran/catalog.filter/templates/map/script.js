$(document).ready(function() {
    setTimeout("getFilter()",10);        
    // filter_button event click
    $('.filter_button').on('click', function() {
        var id = $(this).attr('filid');
        var filID = 'cuselMultiple-scroll-' + id;
        $('#' + filID + ' > span.cuselMultipleActive').each(function(index) {
            if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");
                
            if (!$("#new_filter_results #val"+$(this).attr("value")).attr("id"))
                $('<li id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).text() + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').appendTo('#new_filter_results ul');
        });
        $('.filter_popup').hide();
        map_fil = $("#map_fil").serialize();
        if ($(this).attr("filCODE")=="subway")
        {
            getMetroCoords(map_fil);
        }
        else
            UpdateMarkers();
    });   
    
    $('#new_filter_results').on('click', 'a',function() {
        var value = $(this).parent().attr("val");
        $(this).parent().remove();
        if ($("#new_filter_results ul li").length==0)
            $("#new_filter_results").css("display","none");
        $("#new_filter .cuselMultipleInputsWrap input[value='"+value+"']").remove();
        $("#new_filter .cuselMultipleActive[value='"+value+"']").removeClass("cuselMultipleActive cuselMultipleCur");
        map_fil = $("#map_fil").serialize();
        UpdateMarkers();
    });
    
    $("#new_filter .filter_block").hover(function(){
        $(this).addClass("filter_block_active");
    },function(){
        $(this).removeClass("filter_block_active");
    });


    
});
function getFilter()
{
    $('.cuselMultipleActive').each(function() {
            if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");                
            if (!$("#new_filter_results #val"+$(this).attr("value")).attr("id"))
                $('<li id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).text() + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').appendTo('#new_filter_results ul');
    });
}

function go_adres(x,y,a)
{
    if (a)
        map.panTo(new YMaps.GeoPoint(x,y), {flying: 1});
    else
        SetMapCenter(y,x,17);
    UpdateMarkers();
}