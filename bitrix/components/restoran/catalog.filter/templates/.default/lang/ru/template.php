<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Фильтр";
$MESS ['IBLOCK_SET_FILTER'] = "Найти";
$MESS ['IBLOCK_DEL_FILTER'] = "Сбросить";
$MESS ["SELECT_ALL"] = "Выбрать все";
$MESS ["UNSELECT_ALL"] = "Убрать отметки";

$MESS ['PROPERTY_NAME_ideal_place_for'] = "Идеальное место для";
$MESS ['PROPERTY_NAME_out_city'] = "Пригороды";
$MESS ['PROPERTY_NAME_type'] = "Тип";
$MESS ['PROPERTY_NAME_average_bill'] = "Средний счет";
$MESS ['PROPERTY_NAME_area'] = "Район";
$MESS ['PROPERTY_NAME_entertainment'] = "Развлечения";
$MESS ['PROPERTY_NAME_features'] = "Особенности";
$MESS ['PROPERTY_NAME_proposals'] = "Предложения";
$MESS ['PROPERTY_NAME_children'] = "Детям";
$MESS ['PROPERTY_NAME_kitchen'] = "Кухня";
$MESS ['PROPERTY_NAME_subway'] = "Метро";
$MESS ['PROPERTY_NAME_music'] = "Музыка";
$MESS ['PROPERTY_NAME_parking'] = "Парковка";


$MESS ['PROPERTY_NAME_BANKET_MENU_SUM'] = "Сумма банкетного меню";
$MESS ['PROPERTY_NAME_ALLOWED_ALCOHOL'] = "Разрешено свое спиртное";
$MESS ['PROPERTY_NAME_BANKET_SPECIAL_EQUIPMENT'] = "Специальное оборудование";
$MESS ['PROPERTY_NAME_BANKET_ADDITIONAL_OPTION'] = "Дополнительные опции";
$MESS ['PROPERTY_NAME_kolichestvochelovek'] = "Количество человек";

?>