<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Filter";
$MESS ['IBLOCK_SET_FILTER'] = "Filter";
$MESS ['IBLOCK_DEL_FILTER'] = "Reset";

$MESS ['PROPERTY_NAME_ideal_place_for'] = "Best suitable for";
$MESS ['PROPERTY_NAME_out_city'] = "Neighborhood";
$MESS ['PROPERTY_NAME_type'] = "Type";
$MESS ['PROPERTY_NAME_average_bill'] = "Average check";
$MESS ['PROPERTY_NAME_area'] = "Administrative district";
$MESS ['PROPERTY_NAME_entertainment'] = "Entertainment";
$MESS ['PROPERTY_NAME_features'] = "Specialties";
$MESS ['PROPERTY_NAME_proposals'] = "Offers";
$MESS ['PROPERTY_NAME_children'] = "Children";
$MESS ['PROPERTY_NAME_kitchen'] = "Cuisine";
$MESS ['PROPERTY_NAME_subway'] = "Subway";
$MESS ['PROPERTY_NAME_music'] = "Music";
$MESS ['PROPERTY_NAME_parking'] = "Parking";

$MESS ['PROPERTY_NAME_BANKET_MENU_SUM'] = "The amount of the banquet menu";
$MESS ['PROPERTY_NAME_ALLOWED_ALCOHOL'] = "Allowed their liquor";
$MESS ['PROPERTY_NAME_BANKET_SPECIAL_EQUIPMENT'] = "Special equipment";
$MESS ['PROPERTY_NAME_BANKET_ADDITIONAL_OPTION'] = "Additional options";
$MESS ['PROPERTY_NAME_kolichestvochelovek'] = "Number of persons";
?>