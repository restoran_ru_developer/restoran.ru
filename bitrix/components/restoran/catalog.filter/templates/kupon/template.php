<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form action="/content/kupon/" method="get" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search left">
            <div class="filter_box">
            <?
            $p=0;
            $ar_cusel = Array(2,3,1);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
                if ($arProp["CODE"]!='subway')
                {
            ?>
                <div class="filter_block" id="filter<?=$prop_id?>">
                    <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                    <div class="title"><?=$arProp["NAME"]?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi<?=$ar_cusel[$p]?>">
                        <?$i=0;?>
                        <?if ($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]):?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <?if (in_array($key, $_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]])):?>
                                    <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                <?endif;?>
                                <?$i++;
                                if ($i>3) break;
                                ?>
                            <?endforeach;?>
                        <?else:?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                <?$i++;
                                if ($i>2) break;
                                ?>
                            <?endforeach;?>
                        <?endif;?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-10px","top":"2px", "width":"355px"}});
                            var params = {
                                changedEl: "#multi<?=$ar_cusel[$p]?>",
                                scrollArrows: true,
                                visRows:10
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                        <div class="title"><?=$arProp["NAME"]?></div>
                        <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="10">
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>        
            <?             
                    $p++;
                }
                elseif ($arProp["CODE"]=="subway")
                {                
                ?>
                <div class="filter_block" style="width:120px" id="filter3">
                        <div class="title"><?=$arProp["NAME"]?></div>              
                        <script>
                            var link = "";
                            function load_metro(obj)
                            {
                                if (link != $(obj).attr('href'))
                                {
                                    link = $(obj).attr('href');
                                    $(".filter_popup_3").css({'padding':'55px','padding-top':'0px','left':'-50px','top':'164px', 'width':'962px','position':'absolute','z-index':10000});
                                    $(".filter_popup_3").load(link, function(){$(".filter_popup_3").fadeIn(300);});                                                      
                                }
                                else
                                {
                                    $(".filter_popup_3").fadeIn(300);
                                }
                            }
                            $(document).ready(function(){
                                $(".filter_popup_3").click(function(e){                                
                                    e.stopPropagation();
                                    //return false;
                                });
                                $(".filter_category_block").find("a").click(function(e){
                                    e.stopPropagation();
                                    //return false;
                                });
                                $('html,body').click(function() {
                                    $(".filter_popup_3").fadeOut(300);
                                });
                                $(window).keyup(function(event) {
                                if (event.keyCode == '27') {
                                    $(".filter_popup_3").fadeOut(300);
                                }
                                });
                            });
                        </script>
                            <ul class="filter_category_block">
                                <li><a href="/tpl/ajax/metro.php?q=abc&<?=bitrix_sessid_get()?>" class="filter_arrow js another" onclick="load_metro(this); return false;"><?=GetMessage("METRO_ABC")?></a></li>
                                <li><a href="/tpl/ajax/metro.php?q=thread&<?=bitrix_sessid_get()?>" class="filter_arrow js another" onclick="load_metro(this); return false;"><?=GetMessage("METRO_VETKA")?></a></li>
                                <!--<li><a id="near_metro" href="javascript:void(0)" class="filter_arrow js another" onclick="get_location();">Ближайшие к вам</a></li>-->
                            </ul>                            
                        <div class="filter_popup filter_popup_3 popup"></div>                    
                        <?/*foreach($arResult["SUBWAY"] as $subway):?>
                            <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                        <?endforeach*/?>
                    </div>
                <?
                }
            }
?>      
        </div>
    </div>
    <div class="button right" align="center">            
            <div style="max-height:54px;margin-bottom:5px;" class="buttons">
            <?/*if (count($arResult["SPEC_PROJECT"])>0):?>
             <ul style="max-height:54px;">
                <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
                    <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
                <?endforeach;?>
            </ul>
            <?endif;*/?>
                <div class="add_rest2"><a href="/users/id<?=$USER->GetID()?>/kupons_list/">+ Добавить купон</a></div>
            </div>
            <input style="width:168px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
            <!--<div align="center" style="line-height:20px;">
                    <a href="/<?=CITY_ID?>/map/" class="another"><i><?=GetMessage("MAP_SEARCH")?></i></a><br />
                    <a href="/<?=CITY_ID?>/map/near/" class="another"><i><?=GetMessage("NEAR_YOU")?></i></a><br />
                    <a href="/content/search/" class="another"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>
            </div>-->
    </div> 
    <div class="clear"></div>
</form>
