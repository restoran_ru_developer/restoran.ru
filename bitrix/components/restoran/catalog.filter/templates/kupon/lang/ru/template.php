<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Фильтр";
$MESS ['IBLOCK_SET_FILTER'] = "Найти";
$MESS ['IBLOCK_DEL_FILTER'] = "Сбросить";
$MESS ["SELECT_ALL"] = "Выбрать все";
$MESS ["UNSELECT_ALL"] = "Убрать отметки";
$MESS ["CHOOSE_BUTTON"] = "ВЫБРАТЬ";
$MESS ["EXTENDED_FILTER"] = "Расширенный фильтр";
$MESS ["MAP_SEARCH"] = "Поиск по карте";
$MESS ["NEAR_YOU"] = "Ближайшие к Вам";
$MESS ["METRO_ABC"] = "По алфавиту";
$MESS ["METRO_VETKA"] = "По веткам";
$MESS ["MORE_category"] = "все категории";
$MESS ["MORE_sale_value"] = "все скидки";
$MESS ["MORE_price"] = "больше";
?>