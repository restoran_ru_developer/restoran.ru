<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<form action="/content/cookery/search.php" method="get" name="rest_filter_form">
<div id="new_filter">    
        <div class="left fil" style="width:100%">
            
                <input type="hidden" name="page" value="1">
                <?
                $p=0;
                if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                    $ar_cusel = Array(1,2,3,6,21,7);
                else
                    $ar_cusel = Array(1,12,4,2,3);
                foreach($arResult["arrProp"] as $prop_id => $arProp)
                {
                ?>
                    <div class="filter_block left" id="filter<?=$prop_id?>">                        
                        <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                        <script>
                            $(document).ready(function(){
                                $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"<?=($ar_cusel[$p]==7)?"-110px":"0px"?>","top":"30px", "width":"<?=($ar_cusel[$p]==4)?"784px":(($ar_cusel[$p]==12)?"306px":"355px")?>"}});
                                var params = {
                                    changedEl: "#multi<?=$ar_cusel[$p]?>",
                                    scrollArrows: true,
                                    visRows:15
                                }
                                cuSelMulti(params);
                            })
                        </script>
                        <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                            <!--<div class="title"><?=$arProp["NAME"]?></div>-->
                            <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="15">
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                    <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                                <?endforeach?>
                            </select>
                            <!--<br /><br />-->
                            <div align="center" style="display:none"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                        </div>
                    </div>        
                <?             
                        $p++;
                }
                ?>  
                <!--<div class="right">
                    <input class="light_button" type="submit" name="search_submit" value="<?//=GetMessage("IBLOCK_SET_FILTER");?>" />
                </div>-->
            
        </div>
    <div class="clear"></div>
</div>
<div id="new_filter_results">
    <?foreach($ar_cusel as $ac):?>
        <ul id="multi<?=$ac?>"><li class="end"></ul>
    <?endforeach;?>
    <div class="left">
        <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" style="margin-top:4px;" />
    </div>
    <div class="clear"></div>
</div>
</form>
    