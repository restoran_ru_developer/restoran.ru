$(document).ready(function() {
    setTimeout("getFilter()",10);        
    // filter_button event click
    $('.filter_button').on('click', function() {
        var id = $(this).attr('filid');
        var filID = 'cuselMultiple-scroll-' + id;
        $('#new_filter_results ul#'+id).find(".fil").remove();
        $('#' + filID + ' > span.cuselMultipleActive').each(function(index) {
            if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");
                
            if (!$("#new_filter_results #val"+$(this).attr("value")).attr("id"))
            {
                $('<li class="fil" id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).text() + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#new_filter_results ul#'+id);
                $('ul#'+id).show();
            }
        });
        $('.filter_popup').hide();
    });   
    $('.filter_popup').on('click',"span",function(){
       var id = $(this).parents(".filter_popup").find(".filter_button").attr('filid');
        var filID = 'cuselMultiple-scroll-' + id;
        $('#new_filter_results ul#'+id).find(".fil").remove();
        $('#' + filID + ' > span.cuselMultipleActive').each(function(index) {
            if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");
                
            if (!$("#new_filter_results #val"+$(this).attr("value")).attr("id"))
            {
                $('<li class="fil" id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).text() + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#new_filter_results ul#'+id);
                $('ul#'+id).show();
            }
        });
        $('.filter_popup').hide();
    });
    $('#new_filter_results').on('click', 'a',function() {
        var value = $(this).parent().attr("val");
        console.log($(this).parent().parent().find(".val"));
        if ($(this).parent().parent().find(".fil").length==1)
        {
            //$("#new_filter_results").css("display","none");
            $(this).parent().parent().hide();
        }
        $(this).parent().remove();        
        $("#new_filter .cuselMultipleInputsWrap input[value='"+value+"']").remove();
        $("#new_filter .cuselMultipleActive[value='"+value+"']").removeClass("cuselMultipleActive cuselMultipleCur");
    });
    
    $("#new_filter .filter_block").hover(function(){
        $(this).addClass("filter_block_active");
    },function(){
        $(this).removeClass("filter_block_active");
    });
});
/*function getFilter()
{
    $('#filter span.cuselMultipleActive').each(function() {
        var id = $(this).parent().parent().parent().parent().find(".cuselMultipleInputsWrap").attr("id");
        if (!id)
            id = $(this).parent().parent().parent().parent().find(".cuselMultipleInputsWrap").attr("id");
            if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");                
            if (!$("#new_filter_results #val"+$(this).attr("value")).attr("id"))
            {
                $('<li class="fil" id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).text() + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#new_filter_results ul#'+id);
                $('ul#'+id).show();
            }
    });
}*/
function getFilter()
{
    $('#filter span.cuselMultipleActive').each(function() {
        var id = $(this).parent().parent().parent().parent().parent().find(".cuselMultipleInputsWrap").attr("id");
        if (!id)
            id = $(this).parent().parent().parent().parent().find(".cuselMultipleInputsWrap").attr("id");
            if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");                
            if (!$("#new_filter_results #val"+$(this).attr("value")).attr("id"))
            {
                $('<li class="fil" id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).text() + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#new_filter_results ul#'+id);
                $('ul#'+id).show();
            }
    });
}