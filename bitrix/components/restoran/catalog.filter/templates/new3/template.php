<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<?if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
    ?>
<form action="/<?=CITY_ID?>/catalog/<?=$cat?>/all/" method="get" name="rest_filter_form">
    <div id="new_filter">    
            <div class="left by_params">
                <?=GetMessage("FIND_BY_PARAMS")?>
            </div>
            <div class="left fil">
                    <input type="hidden" name="page" value="1">
                    <?
                    $p=0;
                    if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                        $ar_cusel = Array(1,2,3,6,21,7);
                    elseif($_REQUEST["CATALOG_ID"]=="banket")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    elseif($_REQUEST["CATALOG_ID"]=="dostavka")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    else
                        $ar_cusel = Array(1,12,4,2,3);
                    foreach($arResult["arrProp"] as $prop_id => $arProp)
                    {
                        if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
                    ?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">            
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <?
                                $s = 0; $c = 0;
                                 $e = 8;
                                if ((count($arProp["VALUE_LIST"])-$e)>=8)
                                    $e = 10;
                                if ((count($arProp["VALUE_LIST"])-$e)>=30)
                                    $e = 10;
                                if ((count($arProp["VALUE_LIST"])-$e)>=50)
                                    $e = 28;
                                if ((count($arProp["VALUE_LIST"])-$e)<8)
                                    $e = 5;
                                $s = ceil(count($arProp["VALUE_LIST"])/$e);
                                $c = 140*$s;
                                if ($s==7)
                                    $c = 960;
                                if ($c<460)
                                {
                                    $c = 460;
                                    if(count($arProp["VALUE_LIST"])<6)
                                        $e = 2;
                                    else
                                    {
                                        if (count($arProp["VALUE_LIST"])>=19)
                                            $e = 8;
                                        else
                                            $e = 6;
                                    }
                                }
                                ?>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=$c?>px"}});                                    
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none">                                    
                                        <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="<?=GetMessage("NF_OK")?>"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>                                    
                                    <?$pp=0;?>
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?//=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>
                                        <?if ($pp%$e==0):?>
                                            <div class="left" style="width:130px; margin-right:10px; overflow:hidden">
                                        <?endif;?>

                                        <a val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>

                                        <?if ($pp%$e==($e-1)):?>
                                            </div>                                        
                                        <?endif;?>
                                        <?if (end($arProp["VALUE_LIST"])==$val):?>
                                            <?if ($arProp["CODE"]=="out_city"):?>
                                                <a class="unbind" href="/<?=CITY_ID?>/catalog/restaurants/out_city/all/"><?=GetMessage("ALL_OUT_CITY")?></a>
                                            <?endif;?>
                                        <?endif;?>
                                        <?if (end($arProp["VALUE_LIST"])==$val&&$pp%$e!=($e-1)):?>                                            
                                            </div>
                                        <?endif;?>                                                                    
                                        <?$pp++;?>
                                    <?endforeach;?>                                          
                                    <div class="clear"></div>
                                    <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                        <input type="hidden" val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                                    <?endforeach;?>                                        
<!--                                    <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>-->
                                </div>                                
                            </div>        
                    <?    
                        elseif($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">  
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <script>
                                        $(document).ready(function(){
                                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-85px","top":"30px", "width":"auto"}});                                           
                                        })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="subway" style="display:none">                                                                        
                                        <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="<?=GetMessage("NF_OK")?>"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>                                    
                                    <div class="subway-map">    
                                        <?if (LANGUAGE_ID=="en"):?>
                                            <img src="/tpl/images/subway/<?=CITY_ID?>_en.gif" />
                                        <?else:?>
                                            <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                        <?endif;?>
                                        <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                            <?if ($val["STYLE"]):?>
                                                <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"> </a>
                                            <?endif;?>
                                        <?endforeach;?>
                                        <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                                <input type="hidden" name="arrFilter_pf[subway][]" val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                        <?endforeach;?>
                                    </div>
<!--                                    <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>-->
                                </div>
                            </div>
                   <?   endif;
                        $p++;
                    }
                    if (LANGUAGE_ID!="en")
                    {
                    if ($_REQUEST["CATALOG_ID"]=="restaurants"||!$_REQUEST["CATALOG_ID"]):
                        $prop_id = 1;
                    ?>  
                    <div class="filter_block left" id="filter<?=$prop_id?>">            
                            <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("FEATURES")?></a></div>
                            <script>
                                $(document).ready(function(){
                                    $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-107px","top":"30px", "width":"460px"}});                                    
                                })
                            </script>
                            <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none;">                                
                                        <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_FEATURES")?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="<?=GetMessage("NF_OK")?>"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>                                    
                                <div class="left" style="width:200px; margin-right:10px; overflow:hidden">                                        
                                    <a code="features" val="Панорамный вид" id="6766" class="<?=(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Панорамный вид</a>
                                    
                                    <a code="children" val="Детская комната" id="184520" class="<?=(in_array(184520,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"]))?"selected":""?>">Детская комната</a>
                                    
                                    <a code="children" val="Детские программы" id="185511" class="<?=(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"]))?"selected":""?>">Детские программы</a>
                                    <a code="proposals" val="Кальяны" id="444604" class="<?=(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"]))?"selected":""?>">Кальяны</a>
                                    <a code="breakfast"  val="Завтраки" id="Y" class="<?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"]=="Y")?"selected":""?>">Завтраки</a>
                                    <a code="entertainment" style="width:190px" val="Спорт на большом экране" id="2013" class="<?=(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Спорт на большом экране</a>                                    
                                    <a code="entertainment" val="Караоке" id="1290" class="<?=(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Караоке</a>
                                    <a code="features" val="Действующий камин" id="151655" class="<?=(in_array(151655,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Действующий камин</a>                                    
                                </div>
                                <div class="left" style="width:200px; margin-right:10px; overflow:hidden">                                        
                                    <a code="type" val="Пивной ресторан" id="432588" class="<?=(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Пивной ресторан</a>
                                    <a code="features" val="Отдельный кабинет" id="1059243" class="<?=(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Отдельный кабинет</a>
                                    <a href="/<?=CITY_ID?>/articles/wedding/" val="Свадьбы" id="1" class="<?=(in_array(1,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["wedding"]))?"selected":""?>">Свадьбы</a>                                                                        
                                    <?if (CITY_ID!="msk"):?>
                                        <a code="d_tours" val="С 3D туром" id="2" class="<?=(in_array(2,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["d_tours"]))?"selected":""?>">С 3D туром</a>
                                    <?endif;?>
                                    <a code="features" val="На воде" id="6784" class="<?=(in_array(6732,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">На воде</a>
                                    <a code="features" val="Танцпол" id="6628" class="<?=(in_array(6628,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Танцпол</a>
                                    <a code="features" val="Разрешено с питомцами" id="6797" class="<?=(in_array(6797,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Разрешено с питомцами</a>                                                                       
                                </div>                                        
                                <div class="clear"></div>
                                <?if(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Панорамный вид"  name="arrFilter_pf[features][]" id="hid6766" value="6766" />
                                <?endif;?>
                                <?if(in_array(184520,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"])):?>
                                    <input type="hidden" val="Детская комната"  name="arrFilter_pf[children][]" id="hid184520" value="184520" />
                                <?endif;?>
                                <?if(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"])):?>
                                    <input type="hidden" val="Детские программы"  name="arrFilter_pf[children][]" id="hid185511" value="185511" />
                                <?endif;?>
                                <?if(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"])):?>
                                    <input type="hidden" val="Кальяны"  name="arrFilter_pf[proposals][]" id="hid444604" value="444604" />
                                <?endif;?>
                                <?if(in_array("Y",$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"])):?>
                                    <input type="hidden" val="Завтраки"  name="arrFilter_pf[breakfast][]" id="hidY" value="Y" />
                                <?endif;?>                                    
                                <?if(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                                    <input type="hidden" val="Спорт на большом экране"  name="arrFilter_pf[entertainment][]" id="hid2013" value="2013" />
                                <?endif;?>
                                <?if(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                                    <input type="hidden" val="Караоке"  name="arrFilter_pf[entertainment][]" id="hid1290" value="1290" />
                                <?endif;?>                                
                                <?if(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                                    <input type="hidden" val="Пивной ресторан"  name="arrFilter_pf[type][]" id="hid432588" value="432588" />
                                <?endif;?>
                                <?if(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Отдельный кабинет"  name="arrFilter_pf[type][]" id="hid1059243" value="1059243" />
                                <?endif;?>
                                <?if(in_array(6784,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="На воде"  name="arrFilter_pf[features][]" id="hid6784" value="6784" />
                                <?endif;?>
                                <?if(in_array(151655,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Действующий камин"  name="arrFilter_pf[features][]" id="hid151655" value="151655" />
                                <?endif;?>
                                <?if(in_array(6628,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Танцпол"  name="arrFilter_pf[features][]" id="hid6628" value="6628" />
                                <?endif;?>
                                <?if(in_array(6797,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Разрешено с питомцами"  name="arrFilter_pf[features][]" id="hid6797" value="6797" />
                                <?endif;?>
                                <?if(in_array(151655,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Действующий камин"  name="arrFilter_pf[type][]" id="hid151655" value="151655" />
                                <?endif;?>
                                <?if(in_array(2,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["d_tours"])):?>
                                    <input type="hidden" val="С 3D туром"  name="arrFilter_pf[d_tours]" id="hid2" value="2" />
                                <?endif;?>
<!--                                    <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>-->
                            </div>
                        </div>
                    <!--<div class="left">
                        <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
                    </div>-->
                    <div class="clear"></div>
                <?endif;?>
                    <?}?>
            </div>            
            <?/*if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                <div class="left" style="position:relative;"><a href="/<?=CITY_ID?>/catalog/restaurants/all/?page=1&arrFilter_pf%5Bsale10%5D=Да" style="display:block;position: absolute;top: -4px;height: 38px;width: 120px;background: url(/tpl/images/filter_svyaznoy.png) left top no-repeat;left: -10px;" onclick=""></a></div>            
            <?endif;*/?>
            <?if (!substr_count($APPLICATION->GetCurDir(), "cookery")):?>
            <div class="right links_and_search">                
                    <div class="left" style="line-height:28px;">                        
                        <div align="center" class="left" style="margin-right:10px;">
                                <a href="/<?=CITY_ID?>/filter/" class="another  white"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>                             
                        </div>

                    </div> 
                    <div class="right">
                    <?/*$APPLICATION->IncludeComponent(
                                    "bitrix:search.title",
                                    (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_suggest":"main_new",
                                    Array(),
                                false
                            );*/?>
                    </div>
                    <div class="clear"></div>
            </div>
            <?endif;?>
        <div class="clear"></div>
    </div>
    <?if ($cat=="restaurants"):
        $mess = GetMessage("IBLOCK_SET_FILTER");
    else:
        $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
    endif;
?>
    <div id="new_filter_results">
        <div class="left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
        <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
            <ul id="multi<?=$prop_id?>"><li class="end"></ul>
        <?endforeach;?>
            <ul id="multi1"><li class="end"></ul>
        <div class="left">
            <input class="light_button" type="submit" value="<?=$mess?>" style="margin-top:4px;" />
        </div>
        <div class="clear"></div>
    </div>
</form>