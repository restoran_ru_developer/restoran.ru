<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="/content/cookery/search.php" method="get" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search left">
            <div class="filter_box">
            <?
            $p=0;
            //$ar_cusel = Array(2,3,1);
            $ar_cusel = Array(1,2,3,6,21,7);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
                if ($arProp["CODE"]!='subway'&&$arProp["CODE"]!='subway_dostavka')
                {
            ?>
            
                <div class="filter_block" id="filter<?=$prop_id?>">
                    <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                    <div class="title" <?=(strlen($arProp["NAME"])>15)?"style='margin-bottom:15px'":""?>><?=str_replace(" ","<br />",$arProp["NAME"])?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi<?=$ar_cusel[$p]?>">
                        <?$i=0;?>
                        <?if ($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]):?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <?if (in_array($key, $_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]])):?>
                                    <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                    <?$i++;
                                    if (strlen($arProp["NAME"])>15&&$i>2||$i>3) break;
                                    ?>
                                <?endif;?>
                            <?endforeach;?>
                        <?else:?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                <?$i++;
                                if (strlen($arProp["NAME"])>15&&$i>2||$i>3) break;
                                ?>
                            <?endforeach;?>
                        <?endif;?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-10px","top":"2px", "width":"355px"}});
                            var params = {
                                changedEl: "#multi<?=$ar_cusel[$p]?>",
                                scrollArrows: true,
                                visRows:10
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                        <div class="title"><?=$arProp["NAME"]?></div>
                        <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="10">
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>        
            <?             
                    $p++;
                }
            }
?>      
        </div>
    </div>
    <div class="button right" align="center">            
            <div style="max-height:54px;margin-bottom:5px;">
            <?if (count($arResult["SPEC_PROJECT"])>0):?>
             <ul style="max-height:54px;min-height:54px;">
                <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
                    <li align="center"><a href="/content/cookery/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
                <?endforeach;?>
            </ul>
            <?endif;?>
            </div>
                <?/*$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include_areas/top_search_links.php",
                    Array(),
                    Array("MODE"=>"html")
                );*/?>
            <!--<div align="center" style="line-height:20px;">
                    <a href="/<?=CITY_ID?>/map/" class="another"><i><?=GetMessage("MAP_SEARCH")?></i></a><br />
                    <a href="/<?=CITY_ID?>/map/near/" class="another"><i><?=GetMessage("NEAR_YOU")?></i></a><br />
                    <a href="/content/search/" class="another"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>
            </div>-->
            <input style="width:168px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
    </div> 
    <div class="clear"></div>
</form>
