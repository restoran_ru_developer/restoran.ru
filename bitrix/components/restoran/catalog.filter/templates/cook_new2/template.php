<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<?if ($_REQUEST["SECTION_CODE"]=="mcfromchif"):?>
    <form action="/<?=$_REQUEST["CITY_ID"]?>/news/mcfromchif/" method="get" name="rest_filter_form">
<?else:?>
    <form action="/content/cookery/search.php" method="get" name="rest_filter_form">
<?endif;?>
<div id="new_filter">    
        <div class="left by_params">
                <?=GetMessage("FIND_BY_PARAMS")?>
        </div>
        <div class="left fil">
            
                <input type="hidden" name="page" value="1">
                <?
                $p=0;
                if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                    $ar_cusel = Array(1,2,3,6,21,7);
                else
                    $ar_cusel = Array(1,12,4,2,3);
                foreach($arResult["arrProp"] as $prop_id => $arProp)
                {
                ?>
                    <div class="filter_block left" id="filter<?=$prop_id?>">            
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <?
                                $s = 0; $c = 0;
                                 $e = 12;
                                if ((count($arProp["VALUE_LIST"])-$e)>=8)
                                    $e = 12;
                                if ((count($arProp["VALUE_LIST"])-$e)>=30)
                                    $e = 17;
                                if ((count($arProp["VALUE_LIST"])-$e)>=50)
                                    $e = 28;
                                if ((count($arProp["VALUE_LIST"])-$e)<8)
                                    $e = 6;
                                $s = ceil(count($arProp["VALUE_LIST"])/$e);
                                $c = 134*$s;
                                if ($s==7)
                                    $c = 960;
                                if ($c<580)
                                {
                                    $c = 580;
                                    if(count($arProp["VALUE_LIST"])<7)
                                        $e = 2;
                                    else
                                    {
                                        if (count($arProp["VALUE_LIST"])>=19)
                                        {
                                            if($arProp["CODE"]=="osn_ingr")
                                                $e=8;
                                                else
                                            $e = 6;
                                        }
                                        else
                                            $e = 3;
                                    }
                                }
                                ?>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"<?=($arProp["CODE"]=="cook"||$arProp["CODE"]=="povod"||$arProp["CODE"]=="prig"||$arProp["CODE"]=="prig_time")?"right":"left"?>":"<?=($arProp["CODE"]=="cook"||$arProp["CODE"]=="povod"||$arProp["CODE"]=="prig"||$arProp["CODE"]=="prig_time")?"0px":"0px"?>","top":"30px", "width":"<?=$c?>px"}});                                    
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none">
                                    <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="Готово"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>
                                    <?$pp=0;?>
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?//=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>
                                        <?if ($pp%$e==0):?>
                                            <div class="left" style="width:124px; margin-right:10px; overflow:hidden">
                                        <?endif;?>

                                        <a val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>

                                        <?if ($pp%$e==($e-1)):?>
                                            </div>                                        
                                        <?endif;?>
                                        <?if (end($arProp["VALUE_LIST"])==$val&&$pp%$e!=($e-1)):?>
                                            </div>
                                        <?endif;?>                            
                                        <?$pp++;?>
                                    <?endforeach;?>                                
                                    <div class="clear"></div>
                                    <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                        <input type="hidden" val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                                    <?endforeach;?>
                                </div>
                            </div>       
                <?             
                        $p++;
                }
                ?>  
                <!--<div class="right">
                    <input class="light_button" type="submit" name="search_submit" value="<?//=GetMessage("IBLOCK_SET_FILTER");?>" />
                </div>-->
            
        </div>
    <div class="clear"></div>
</div>
<div id="new_filter_results">
    <div class="left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
        <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
            <ul id="multi<?=$prop_id?>"><li class="end"></ul>
        <?endforeach;?>
            <ul id="multi1"><li class="end"></ul>
        <div class="left">
            <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" style="margin-top:4px;" />
        </div>
        <div class="clear"></div>
    </div>
</form>
    