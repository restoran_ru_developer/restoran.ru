<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<?if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
    ?>
<form action="/<?=CITY_ID?>/catalog/<?=$cat?>/all/" method="get" name="rest_filter_form">
    <div id="new_filter">    
            <div class="left fil">
                    <input type="hidden" name="page" value="1">
                    <?
                    $p=0;
                    if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                        $ar_cusel = Array(1,2,3,6,21,7);
                    elseif($_REQUEST["CATALOG_ID"]=="banket")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    elseif($_REQUEST["CATALOG_ID"]=="dostavka")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    else
                        $ar_cusel = Array(1,12,4,2,3);
                    foreach($arResult["arrProp"] as $prop_id => $arProp)
                    {
                        //if ($arProp["CODE"]!="subway"):
                    ?>
                        <div class="filter_block left" id="filter<?=$prop_id?>">            
                            <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                            <script>
                                $(document).ready(function(){
                                    $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=($ar_cusel[$p]==4)?"784px":(($ar_cusel[$p]==12)?"306px":"355px")?>"}});
                                    var params = {
                                        changedEl: "#multi<?=$ar_cusel[$p]?>",
                                        scrollArrows: true,
                                        visRows:15
                                    }
                                    cuSelMulti(params);
                                })
                            </script>
                            <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                                <!--<div class="title"><?=$arProp["NAME"]?></div>-->
                                <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="15">
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?if($arProp["CODE"]=="subway"):?>
                                            <option style='background:url("<?=CFile::GetPath($val["PICTURE"])?>") left center no-repeat; padding-left:15px;' value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val["NAME"]?></option>
                                        <?else:?>
                                            <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                                        <?endif;?>
                                    <?endforeach?>
                                </select>
                                <!--<br /><br />-->
                                <div align="center" style="display:none"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                            </div>
                        </div>        
                    <?    
                        /*elseif($arProp["CODE"]=="subway"&&$USER->IsAdmin()):?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">  
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <script>
                                        $(document).ready(function(){
                                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-40px","top":"30px", "width":"auto"}});                                           
                                        })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                                    <div class="subway-map" filID="multi<?=$ar_cusel[$p]?>" style="display: block; ">    
                                        <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                        <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                            <?if ($val["STYLE"]):?>
                                                <a href="javascript:void(0)" class="subway_station" id="<?=$key?>" val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"></a>
                                            <?endif;?>
                                        <?endforeach;?>
                                        <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                                <input type="hidden" id="h<?=$s?>" value="<?=$s?>" />
                                        <?endforeach;?>
                                    </div>
                                </div>
                            </div>
                   <?   endif;*/
                        $p++;
                    }
                    ?>  
                    <!--<div class="left">
                        <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
                    </div>-->
                    <div class="clear"></div>

            </div>
            <?if (!substr_count($APPLICATION->GetCurDir(), "cookery")):?>
            <div class="right links_and_search">                
                    <div class="left" style="line-height:28px;">
                        <div align="center" class="left" style="margin-right:10px;padding-right:10px;">
                                <a href="/<?=CITY_ID?>/map/near/" class="another no_border"><input type="button" class="light_button" value="<?=GetMessage("NEAR_YOU")?>"></a> 
                        </div>
                        <div align="center" class="left" style="margin-right:10px;">
                                <a href="/content/search/" class="another  white"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>                             
                        </div>

                    </div> 
                    <div class="right">
                    <?/*$APPLICATION->IncludeComponent(
                                    "bitrix:search.title",
                                    (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_suggest":"main_new",
                                    Array(),
                                false
                            );*/?>
                    </div>
                    <div class="clear"></div>
            </div>
            <?endif;?>
        <div class="clear"></div>
    </div>
    <?if ($cat=="restaurants"):
        $mess = GetMessage("IBLOCK_SET_FILTER");
    else:
        $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
    endif;
?>
    <div id="new_filter_results">
        <?foreach($ar_cusel as $ac):?>
            <ul id="multi<?=$ac?>"><li class="end"></ul>
        <?endforeach;?>
        <div class="left">
            <input class="light_button" type="submit" name="search_submit" value="<?=$mess?>" style="margin-top:4px;" />
        </div>
        <div class="clear"></div>
    </div>
</form>