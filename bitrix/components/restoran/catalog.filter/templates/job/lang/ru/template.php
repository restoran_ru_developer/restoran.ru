<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Фильтр";
$MESS ['IBLOCK_SET_FILTER'] = "Найти вакансии";
$MESS ['IBLOCK_SET_FILTER_BANKET'] = "Найти резюме";
$MESS ['IBLOCK_DEL_FILTER'] = "Сбросить";
$MESS ["SELECT_ALL"] = "Выбрать все";
$MESS ["UNSELECT_ALL"] = "Убрать отметки";
$MESS ["CHOOSE_BUTTON"] = "ВЫБРАТЬ";
$MESS ["EXTENDED_FILTER"] = "Расширенный поиск";
$MESS ["MAP_SEARCH"] = "Поиск по карте";
$MESS ["NEAR_YOU"] = "Рядом с вами";
$MESS ["METRO_ABC"] = "По алфавиту";
$MESS ["METRO_VETKA"] = "По веткам";
$MESS ["MORE_kolichestvochelovek"] = "Количество мест";
$MESS ["MORE_area"] = "Район";
$MESS ["MORE_average_bill"] = "Счет";
$MESS ["MORE_kitchen"] = "Кухня";
$MESS ["MORE_preference"] = "еще предпочтения";
$MESS ["MORE_kuhnyadostavki"] = "еще кухни";
$MESS ["MORE_type"] = "Тип заведения";
$MESS ["MORE_subway"] = "Метро";
$MESS ["MORE_out_city"] = "Пригороды";
$MESS ["MORE_cat"] = "Категория";
$MESS ["MORE_osn_ingr"] = "Основной ингредиент";
$MESS ["MORE_prig_time"] = "Время приготовления";
$MESS ["MORE_cook"] = "Кухня";
$MESS ["MORE_povod"] = "Повод";
$MESS ["MORE_prig"] = "Приготовление";
$MESS ["MORE_EXPERIENCE"] = "Опыт работы";
$MESS ["MORE_SHEDULE"] = "График работы";
$MESS ["MORE_SUGG_WORK_GRAFIK"] = "График работы";
$MESS ["MORE_POSITION"] = "Должность";
$MESS ["MORE_WAGES_OF"] = "Заработная плата";
$MESS ["MORE_SUGG_WORK_ZP_FROM"] = "Заработная плата";
$MESS ["FIND_BY_PARAMS"] = "Что ищем:";
$MESS ["FEATURES"] = "Особенности";

$MESS ["MORE_category"] = "Категория";
$MESS ["MORE_price"] = "Стоимость";
$MESS ["MORE_sale_value"] = "Скидка";

$MESS ["YOU_CHOOSE"] = "Вы выбрали:";
$MESS ["ALL_OUT_CITY"] = "Все загородные";
?>