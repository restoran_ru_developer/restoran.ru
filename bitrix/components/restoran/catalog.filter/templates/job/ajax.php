<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$arRestIB = getArIblock($_REQUEST["type"], CITY_ID);?>
<?$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"job",
	Array(
		"IBLOCK_TYPE" => $_REQUEST["type"],
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => array("POSITION","EXPERIENCE", "SHEDULE","EXPERIENCE", "SUGG_WORK_GRAFIK","WAGES_OF","SUGG_WORK_ZP_FROM"),
		"PRICE_CODE" => array(),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"SAVE_IN_SESSION" => "N",
                "AJAX" => "Y"
	)
);?>
