<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["AJAX"]=="Y"):?>
<script src="<?=$templateFolder?>/script.js"></script>
<?endif;?>
<form action="/<?=CITY_ID?>/joblist/<?=$arParams["~IBLOCK_TYPE"]?>/" method="get" name="rest_filter_form">
    <div id="new_filter">    
            <div class="left by_params">
                <div class="left" style="margin-right:10px;">
                    <?=GetMessage("FIND_BY_PARAMS")?>                    
                </div>
<!--                <div class="left find_what <?=($arParams["~IBLOCK_TYPE"]=="vacancy")?"active":""?>" <?=($arParams["~IBLOCK_TYPE"]!="vacancy")?"onclick='get_fil(\"vacancy\")'":""?>>
                    вакансии
                </div>
                <div class="left find_what <?=($arParams["~IBLOCK_TYPE"]=="resume")?"active":""?>" <?=($arParams["~IBLOCK_TYPE"]!="resume")?"onclick='get_fil(\"resume\")'":""?>>
                    резюме
                </div>-->
                <div class="left find_what <?=($arParams["~IBLOCK_TYPE"]=="vacancy")?"active":""?>" onclick="location.href='/<?=CITY_ID?>/joblist/vacancy/'">
                    вакансии
                </div>
                <div class="left find_what <?=($arParams["~IBLOCK_TYPE"]=="resume")?"active":""?>" onclick="location.href='/<?=CITY_ID?>/joblist/resume/'">
                    резюме
                </div>
                <div class="clear"></div>
            </div>
            <div class="left fil">
                    <input type="hidden" name="page" value="1">
                    <?
                    $p=0;
                    if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                        $ar_cusel = Array(1,2,3,6,21,7);
                    elseif($_REQUEST["CATALOG_ID"]=="banket")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    elseif($_REQUEST["CATALOG_ID"]=="dostavka")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    else
                        $ar_cusel = Array(1,12,4,2,3);
                    foreach($arResult["arrProp"] as $prop_id => $arProp)
                    {
                        if ($arProp["CODE"]=="WAGES_OF"||$arProp["CODE"]=="SUGG_WORK_ZP_FROM")
                            $arProp["VALUE_LIST"] = Array(
                                "0_30000"=>"до 30 000 р.",
                                "30000_50000"=>"30 000 - 50 000 р.",
                                "70000_100000"=>"70 000 - 100 000 р.",
                                "100000_150000"=>"100 000 - 150 000 р.",
                                "150000_99999999"=>"свыше 150 000 р.",
                            );
                        if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
                    ?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">            
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <?
                                $s = 0; $c = 0;
                                 $e = 8;
                                if ((count($arProp["VALUE_LIST"])-$e)>=8)
                                    $e = 8;
                                if ((count($arProp["VALUE_LIST"])-$e)>=30)
                                    $e = 17;
                                if ((count($arProp["VALUE_LIST"])-$e)>=50)
                                    $e = 28;
                                if ((count($arProp["VALUE_LIST"])-$e)<8)
                                    $e = 5;
                                $s = ceil(count($arProp["VALUE_LIST"])/$e);
                                $c = 134*$s;
                                if ($s==7)
                                    $c = 960;
                                ?>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=$c?>px"}});                                    
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none">
                                    <?$pp=0;?>
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?//=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>
                                        <?if ($pp%$e==0):?>
                                            <div class="left" style="width:124px; margin-right:10px; overflow:hidden">
                                        <?endif;?>

                                        <a val="<?=$val?>" id="<?=$key?>" class="link_filter <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>

                                        <?if ($pp%$e==($e-1)):?>
                                            </div>                                        
                                        <?endif;?>
                                        <?if (end($arProp["VALUE_LIST"])==$val&&$pp%$e!=($e-1)):?>
                                            </div>
                                        <?endif;?>                            
                                        <?$pp++;?>
                                    <?endforeach;?>      
                                    <?if ($arProp["CODE"]=="out_city"):?>
                                        <a href="/<?=CITY_ID?>/catalog/restaurants/out_city/all/"><?=GetMessage("ALL_OUT_CITY")?></a>
                                    <?endif;?>
                                    <div class="clear"></div>
                                    <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                        <input type="hidden" val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                                    <?endforeach;?>
                                </div>
                            </div>        
                    <?                                                        
                       endif;
                        $p++;
                    }?>                    
            </div>                                    
        <div class="clear"></div>
    </div>
    <?if ($arParams["~IBLOCK_TYPE"]=="vacancy"):
        $mess = GetMessage("IBLOCK_SET_FILTER");
    else:
        $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
    endif;
?>
    <div id="new_filter_results">
        <div class="left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
        <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
            <ul id="multi<?=$prop_id?>"><li class="end"></ul>
        <?endforeach;?>
            <ul id="multi1"><li class="end"></ul>
        <div class="left">
            <input class="light_button" type="submit" value="<?=$mess?>" style="margin-top:4px;" />
        </div>
        <div class="clear"></div>
    </div>
</form>