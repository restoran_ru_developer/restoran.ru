<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$item = array();
foreach($arResult["arrProp"] as $prop_id => $arProp)
{
    if ($arProp["CODE"]!="subway")
        $item[$arProp["CODE"]] = $arProp["VALUE_LIST"];
    else
    {
        foreach ($arProp["VALUE_LIST"] as $subw)
        {
            $item[$arProp["CODE"]][$subw["ID"]] = $subw["NAME"];
        }
    }
}
echo json_encode($item);
?>