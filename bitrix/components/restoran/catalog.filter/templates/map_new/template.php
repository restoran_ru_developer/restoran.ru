<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<?if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
    ?>
<script>
    var map_fil = "";
</script>
<form action="/<?=CITY_ID?>/catalog/<?=$cat?>/all/" id="map_fil" method="get" name="rest_filter_form">
    <div id="new_filter">    
            <div class="left by_params">
                Найти по параметрам:
            </div>
            <div class="left fil">
                    <input type="hidden" name="page" value="1">
                    <?
                    $p=0;
                    if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                        $ar_cusel = Array(1,2,3,6,21,7);
                    elseif($_REQUEST["CATALOG_ID"]=="banket")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    elseif($_REQUEST["CATALOG_ID"]=="dostavka")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    else
                        $ar_cusel = Array(1,12,4,2,3);
                    foreach($arResult["arrProp"] as $prop_id => $arProp)
                    {
                        if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
                    ?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">            
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <?
                                $s = 0; $c = 0;
                                 $e = 8;
                                if ((count($arProp["VALUE_LIST"])-$e)>=8)
                                    $e = 10;
                                if ((count($arProp["VALUE_LIST"])-$e)>=30)
                                    $e = 10;
                                if ((count($arProp["VALUE_LIST"])-$e)>=50)
                                    $e = 28;
                                if ((count($arProp["VALUE_LIST"])-$e)<8)
                                    $e = 5;
                                $s = ceil(count($arProp["VALUE_LIST"])/$e);
                                $c = 140*$s;
                                if ($s==7)
                                    $c = 960;
                                if ($c<460)
                                {
                                    $c = 460;
                                    if(count($arProp["VALUE_LIST"])<6)
                                        $e = 2;
                                    else
                                    {
                                        if (count($arProp["VALUE_LIST"])>=19)
                                            $e = 8;
                                        else
                                            $e = 6;
                                    }
                                }
                                ?>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=$c?>px"}});                                    
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none">
                                    <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="Готово"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>
                                    <?$pp=0;?>
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?//=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>
                                        <?if ($pp%$e==0):?>
                                            <div class="left" style="width:124px; margin-right:10px; overflow:hidden">
                                        <?endif;?>

                                        <a val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>

                                        <?if ($pp%$e==($e-1)):?>
                                            </div>                                        
                                        <?endif;?>
                                        <?if (end($arProp["VALUE_LIST"])==$val&&$pp%$e!=($e-1)):?>
                                            </div>
                                        <?endif;?>                            
                                        <?$pp++;?>
                                    <?endforeach;?>                                
                                    <div class="clear"></div>
                                    <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                        <input type="hidden" val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                                    <?endforeach;?>
                                </div>
                            </div>        
                    <?    
                        elseif($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">  
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <script>
                                        $(document).ready(function(){
                                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-85px","top":"30px", "width":"auto"}});                                           
                                        });
                                        function getMetroCoords(filter)
                                        {
                                            jQuery.get ("/tpl/ajax/get_metro_coords.php?<?=bitrix_sessid_get()?>", filter, function ( data ) {
                                                console.log(data[0]);
                                                if (data)
                                                {
                                                    map.panTo(new YMaps.GeoPoint(data[1],data[0]), {flying: 1});
                                                    UpdateMarkers();
                                                }
                                            },"json");
                                        }
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="subway" style="display:none"> 
                                    <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="Готово"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>
                                    <div class="subway-map">    
                                        <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                        <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                            <?if ($val["STYLE"]):?>
                                                <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"></a>
                                            <?endif;?>
                                        <?endforeach;?>
                                        <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                                <input type="hidden" name="arrFilter_pf[subway][]" val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                        <?endforeach;?>
                                    </div>
                                </div>
                            </div>
                   <?   endif;
                        $p++;
                    }
                    if ($_REQUEST["CATALOG_ID"]=="restaurants"||!$_REQUEST["CATALOG_ID"]):
                        $prop_id = 1;
                    ?>  
                    <div class="filter_block left" id="filter<?=$prop_id?>">            
                            <div class="name"><a href="javascript:void(0)" class="filter_arrow white">Особенности</a></div>
                            <script>
                                $(document).ready(function(){
                                    $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"250px"}});                                    
                                })
                            </script>
                            <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none;">
                                <div class="title_box">
                                            <div class="left">
                                                <?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_FEATURES")?>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:void(0)" filter="filter_popup_<?=$prop_id?>" filID="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="fil_button button_blue" filID="multi<?=$ar_cusel[$p]?>" value="Готово"/> 
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="title_box_brd"></div>
                                <div class="left" style="width:200px; margin-right:10px; overflow:hidden">                                        
                                    <a code="features" val="Панорамный вид" id="6766" class="<?=(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Панорамный вид</a>
                                    <a code="children" val="Детские программы" id="185511" class="<?=(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"]))?"selected":""?>">Детские программы</a>
                                    <a code="proposals" val="Кальяны" id="444604" class="<?=(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"]))?"selected":""?>">Кальяны</a>
                                    <a code="breakfast"  val="Завтраки" id="Y" class="<?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"]=="Y")?"selected":""?>">Завтраки</a>
                                    <a code="entertainment" style="width:190px" val="Спорт на большом экране" id="2013" class="<?=(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Спорт на большом экране</a>
                                    <a code="entertainment" val="Караоке" id="1290" class="<?=(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Караоке</a>
                                    <a code="type" val="Пивной ресторан" id="432588" class="<?=(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Пивной ресторан</a>
                                    <a href="/<?=CITY_ID?>/articles/wedding/" val="Свадьбы" id="1" class="<?=(in_array(1,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["wedding"]))?"selected":""?>">Свадьбы</a>                                    
                                </div>                                        
                                <div class="clear"></div>
                                <?if(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                                    <input type="hidden" val="Панорамный вид"  name="arrFilter_pf[features][]" id="hid6766" value="6766" />
                                <?endif;?>
                                <?if(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"])):?>
                                    <input type="hidden" val="Детские программы"  name="arrFilter_pf[children][]" id="hid185511" value="185511" />
                                <?endif;?>
                                <?if(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"])):?>
                                    <input type="hidden" val="Кальяны"  name="arrFilter_pf[proposals][]" id="hid444604" value="444604" />
                                <?endif;?>
                                <?if(in_array("Y",$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"])):?>
                                    <input type="hidden" val="Завтраки"  name="arrFilter_pf[breakfast][]" id="hidY" value="Y" />
                                <?endif;?>                                    
                                <?if(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                                    <input type="hidden" val="Спорт на большом экране"  name="arrFilter_pf[entertainment][]" id="hid2013" value="2013" />
                                <?endif;?>
                                <?if(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                                    <input type="hidden" val="Караоке"  name="arrFilter_pf[entertainment][]" id="hid1290" value="1290" />
                                <?endif;?>                                
                                <?if(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                                    <input type="hidden" val="Пивной ресторан"  name="arrFilter_pf[type][]" id="hid432588" value="432588" />
                                <?endif;?>
                            </div>
                        </div>
                    <!--<div class="left">
                        <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
                    </div>-->
                    <div class="clear"></div>
                <?endif;?>
            </div>
            <?if (!substr_count($APPLICATION->GetCurDir(), "cookery")):?>
            <div class="right links_and_search">                
                    <div class="left" style="line-height:28px;">                        
                        <div align="center" class="left" style="margin-right:10px;">
                                <a href="/<?=CITY_ID?>/filter/" class="another  white"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>                             
                        </div>

                    </div> 
                    <div class="right">
                    <?/*$APPLICATION->IncludeComponent(
                                    "bitrix:search.title",
                                    (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_suggest":"main_new",
                                    Array(),
                                false
                            );*/?>
                    </div>
                    <div class="clear"></div>
            </div>
            <?endif;?>
        <div class="clear"></div>
    </div>
    <?if ($cat=="restaurants"):
        $mess = GetMessage("IBLOCK_SET_FILTER");
    else:
        $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
    endif;
?>
    <div id="new_filter_results">
        <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
            <ul id="multi<?=$prop_id?>"><li class="end"></ul>
        <?endforeach;?>
            <ul id="multi1"><li class="end"></ul>
        <div class="left">
            <input class="light_button" type="submit" value="<?=$mess?>" style="margin-top:4px;" />
        </div>
        <div class="clear"></div>
    </div>
</form>