$(document).ready(function() {
    setTimeout("getFilter()",10);           
    $('.filter_popup a').click(function(){        
        var id = $(this).parents('.filter_popup').attr('val');
        var code = $(this).parents('.filter_popup').attr('code');
        if ($(this).attr("code"))
            code = $(this).attr("code");
        if ($(this).hasClass("selected"))
        {
            $(this).removeClass("selected");
            $("#hid"+$(this).attr("id")).remove();
            $("#val"+$(this).attr("id")).remove();
            if ($("#new_filter_results").find(".fil").length==0)
            {
                $("#multi"+id).hide();
                $("#new_filter_results").css("visibility","hidden");
            }
        }
        else
        {
            $("html,body").animate({scrollTop:"300px"}, 300);
            $(this).addClass("selected");
            if (code=="breakfast")
                $(this).parents('.filter_popup').append("<input type='hidden' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"]' value='"+$(this).attr("id")+"' />");
            else
                $(this).parents('.filter_popup').append("<input type='hidden' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"][]' value='"+$(this).attr("id")+"' />");
            if ($("#new_filter_results").css("visibility")=="hidden")
                $("#new_filter_results").css("visibility","visible");

            if (!$("#new_filter_results #val"+$(this).attr("id")).attr("id"))
            {
                $('<li class="fil" id="val'+$(this).attr("id")+'" val="'+$(this).attr("id")+'">' + $(this).attr("val") + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#multi'+id);
                $('#multi'+id).show();
            }
        }    
        //if(code!="subway")
            //$(".filter_popup").hide();
        map_fil = $("#map_fil").serialize();
        if (code=="subway")
        {
            getMetroCoords(map_fil);
        }
        else
            UpdateMarkers();
    });     
    $( ".title_box a").unbind( "click" );
    $( ".filter_popup a.unbind").unbind( "click" );
    $('.title_box a').click(function(e){                
        $("."+$(this).attr("filter")+" input[type='hidden']").remove();
        $("."+$(this).attr("filter")+" a").removeClass("selected");
        $("ul#"+$(this).attr("filID")).find("li").remove();        
        $("ul#"+$(this).attr("filID")).hide();                
        if ($("#new_filter_results").find(".fil").length==0)
            $("#new_filter_results").css("display","none");
        $(".filter_popup").hide();
    });
    $(".fil_button").click(function(){
        $(".filter_popup").hide();
    });
    $('#new_filter_results').on('click', 'a',function() {
        var value = $(this).parent().attr("val");
        if ($(this).parent().parent().find(".fil").length==1)
            $(this).parent().parent().hide();
        if ($(this).parent().parent().parent().find(".fil").length==1)
            $("#new_filter_results").css("visibility","hidden");
        $(this).parent().remove();       
        $("#"+value).removeClass("selected");
        $("#hid"+value).remove();
        map_fil = $("#map_fil").serialize();
        UpdateMarkers();
    });
    
    $("#new_filter .filter_block").hover(function(){
        $(this).addClass("filter_block_active");
    },function(){
        $(this).removeClass("filter_block_active");
    });   
});
function getFilter()
{  
    $('.filter_popup input[type=hidden]').each(function(){
        var id = $(this).parents('.filter_popup').attr('val');        
        if ($("#new_filter_results").css("visibility")=="hidden")
                $("#new_filter_results").css("visibility","visible");  
        $('<li class="fil" id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).attr("val") + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#new_filter_results ul#multi'+id);
        $('ul#multi'+id).show();
    });
}

function go_adres(x,y,a)
{
    if (a)
        map.panTo(new YMaps.GeoPoint(x,y), {flying: 1});
    else
        SetMapCenter(y,x,17);
    UpdateMarkers();
}