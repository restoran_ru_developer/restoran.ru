<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<div id="new_filter">    
        <div class="left fil">
            <form action="/<?=CITY_ID?>/catalog/restaurants/all" method="get" name="rest_filter_form">
                <input type="hidden" name="page" value="1">
                <?
                $p=0;
                if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                    $ar_cusel = Array(1,2,3,6,21,7);
                else
                    $ar_cusel = Array(1,12,4,2,3);
                foreach($arResult["arrProp"] as $prop_id => $arProp)
                {
                ?>
                    <div class="filter_block left" id="filter<?=$prop_id?>">                        
                        <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                        <script>
                            $(document).ready(function(){
                                $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=($ar_cusel[$p]==4)?"784px":(($ar_cusel[$p]==12)?"306px":"355px")?>"}});
                                var params = {
                                    changedEl: "#multi<?=$ar_cusel[$p]?>",
                                    scrollArrows: true,
                                    visRows:15
                                }
                                cuSelMulti(params);
                            })
                        </script>
                        <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                            <!--<div class="title"><?=$arProp["NAME"]?></div>-->
                            <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="15">
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                    <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                                <?endforeach?>
                            </select>
                            <br /><br />
                            <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                        </div>
                    </div>        
                <?             
                        $p++;
                }
                ?>  
                <div class="left">
                    <input class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
                </div>
            </form>
        </div>
        <?if (!substr_count($APPLICATION->GetCurDir(), "cookery")):?>
        <div class="right links_and_search">                
            <?global $current?>
                <div class="left actual">
                    <div class="left" style="padding-right:15px">
                        <span><?=$current?></span>     
                    </div>
                    <div class="right">
                        <?=pluralForm($curent, "акция", "акции", "акций")?> сегодня
                    </div>
                </div>
                <div class="clear"></div>
        </div>
        <?endif;?>
    <div class="clear"></div>
</div>
<div id="new_filter_results">
    <ul>
        
    </ul>
    <div class="clear"></div>
</div>