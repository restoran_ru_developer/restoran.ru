<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="title_main left" style="width:180px">
    <?=GetMessage("WHAT_YOU_SEARCH_TEXT")?><Br />
    <div id="how_much" align="center"></div>
</div> 
<script>
    var map_fil = "";
</script>
<form action="/<?=CITY_ID?>/catalog/restaurants/all" method="get" id="map_fil" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search left">
            <div class="filter_box">
            <?
            $p=0;
            $ar_cusel = Array(2,4,3,1);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
            ?>
                
                <div class="filter_block" id="filter<?=$prop_id?>">
                    <div class="title"><?=$arProp["NAME"]?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi<?=$ar_cusel[$p]?>">
                        <?$i=0;?>
                        <?if ($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]):?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <?if (in_array($key, $_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]])):?>
                                    <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                    <?$i++;
                                    if ($i>2) break;
                                    ?>
                                <?endif;?>
                            <?endforeach;?>
                        <?else:?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                <?$i++;
                                if ($i>2) break;
                                ?>
                            <?endforeach;?> 
                        <?endif;?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another">больше</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"<?=($arProp["CODE"]=="subway")?"-150px":"-10px"?>","top":"2px", "width":"<?=($arProp["CODE"]=="subway")?"830px":"315px"?>"}});
                            var params = {
                                changedEl: "#multi<?=$ar_cusel[$p]?>",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                            
                        })
                    </script>
                    <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                        <div class="title"><?=$arProp["NAME"]?></div>
                        <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="10">
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filCODE="<?=$arProp["CODE"]?>" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                        <?if($arProp["CODE"]=="subway"):?>
                            <script>
                                function getMetroCoords(filter)
                                {
                                    jQuery.get ("/tpl/ajax/get_metro_coords.php?<?=bitrix_sessid_get()?>", filter, function ( data ) {
                                        console.log(data[0]);
                                        if (data)
                                        {
                                            map.panTo(new YMaps.GeoPoint(data[1],data[0]), {flying: 1});
                                            UpdateMarkers();
                                        }
                                    },"json");
                                }
                            </script>
                        <?endif;?>
                    </div>
                </div>        
            <?             
                    $p++;
            }
?>      
        </div>
    </div>
    <div class="button right" align="center">
                <?/*$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include_areas/top_search_links.php",
                    Array(),
                    Array("MODE"=>"html")
                );*/?>
            <div align="center" style="line-height:20px; margin-bottom:20px;">
                    <a href="/<?=CITY_ID?>/map/" class="another"><i><?=GetMessage("MAP_SEARCH")?></i></a><br />
                    <a href="/<?=CITY_ID?>/map/near/" class="another"><i><?=GetMessage("NEAR_YOU")?></i></a><br />
                    <a href="/content/search/" class="another"><i><?=GetMessage("EXTENDED_FILTER")?></i></a>
            </div>
        <input style="width:168px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
    </div> 
    <div class="clear"></div>
</form>
