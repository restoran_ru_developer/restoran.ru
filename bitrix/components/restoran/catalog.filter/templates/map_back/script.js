$(document).ready(function() {
    /* search input */
    // focus
    $(".search_block .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
    });
    // blur
    $(".search_block .placeholder").on("blur", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val().length <= 0)
            $(this).val(defVal);
    });

    // filter_button event click
    $('.filter_button').on('click', function() {
        var id = $(this).attr('filid');
        var code = $(this).attr('filid');
        var filID = 'cuselMultiple-scroll-' + id;
        $('#' + filID + ' > span.cuselMultipleActive').each(function(index) {
            if($(this).text() && index == 0)
                $('.' + filID).empty();
            if(index <= 2)
                $('<li><a href="javascript:void(0)" class="white">' + $(this).text() + '</a></li>').appendTo('.' + filID);
        });
        $('.filter_popup').hide();
        map_fil = $("#map_fil").serialize();
        
        if ($(this).attr("filCODE")=="subway")
        {
            getMetroCoords(map_fil);
        }
        else
            UpdateMarkers();
    });
    
    

    // XXX stupid hook
    // get restaurants type on search button
    $('form[name=rest_filter_form]').submit(function() {
/*        $('#cuselMultiple-scroll-multi2 > span.cuselMultipleActive').each(function(index) {
            var inpName = $('#multi2_sel_cont').attr('name');
            if($(this).attr('value').length > 0)
                $('<input type="hidden" name="' + inpName + '[]" value="' + $(this).attr('value') + '" />').appendTo('#multi2_sel_cont');
        });*/

        // check def search value
        var defVal = $(".search_block .placeholder").attr('defvalue');
        if($('input[name=q]').val() == defVal)
            $('input[name=q]').val('');
    });

    // send suggest
    $("#top_search_input").autocomplete("/search/map_suggest.php", {
        limit: 5,
        minChars: 3,
        formatItem: function(data, i, n, value) {
            
            var a = value.split("###")[1];
            var x = a.split(" ")[0];
            var y = a.split(" ")[1];
            return "<a class='suggest_res_url' href='javascript:void(0)' onclick='go_adres("+x+","+y+")'> " + value.split("###")[0] + "</a>";
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        },
        extraParams: {
            search_in: function() {return $('#search_in').val();}          
        }
   	});

    // go to suggest result
    $("#top_search_input").result(function(event, data, formatted) {
        if (data) {
            
            
            //location.href = value.split("###")[1];
        }
   	});
        
    $("#map_adres").submit(function(){
        var a = $("#map_adres").serialize();      
        $.ajax({
            type: "POST",
            url: "/search/map_suggest.php",
            data: 'q='+$("#top_search_input").val(),
            success:function(data)
            {
                var a = data.split("###")[1];
                var x = a.split(" ")[0];
                var y = a.split(" ")[1];
                go_adres(x,y);
            }
        });
        return false;
    })

});

function go_adres(x,y)
{
    map.panTo(new YMaps.GeoPoint(x,y), {flying: 1});
    //UpdateMarkers();
}

