<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
{
	ShowError(GetMessage("CC_BCF_MODULE_NOT_INSTALLED"));
	return;
}
/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["SPEC_IBLOCK"] = (int)$arParams["SPEC_IBLOCK"];

unset($arParams["IBLOCK_TYPE"]); //was used only for IBLOCK_ID setup with Editor
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

if(!is_array($arParams["FIELD_CODE"]))
	$arParams["FIELD_CODE"] = array();
foreach($arParams["FIELD_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["FIELD_CODE"][$k]);

if(!is_array($arParams["PROPERTY_CODE"]))
	$arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["PROPERTY_CODE"][$k]);

if(!is_array($arParams["PRICE_CODE"]))
	$arParams["PRICE_CODE"] = array();

if(!is_array($arParams["OFFERS_FIELD_CODE"]))
	$arParams["OFFERS_FIELD_CODE"] = array();
foreach($arParams["OFFERS_FIELD_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["OFFERS_FIELD_CODE"][$k]);

if(!is_array($arParams["OFFERS_PROPERTY_CODE"]))
	$arParams["OFFERS_PROPERTY_CODE"] = array();
foreach($arParams["OFFERS_PROPERTY_CODE"] as $k=>$v)
	if($v==="")
		unset($arParams["OFFERS_PROPERTY_CODE"][$k]);

$arParams["SAVE_IN_SESSION"] = $arParams["SAVE_IN_SESSION"]=="Y";

if(strlen($arParams["FILTER_NAME"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
	$arParams["FILTER_NAME"] = "arrFilter";
$FILTER_NAME = $arParams["FILTER_NAME"];


global $$FILTER_NAME;
$$FILTER_NAME = array();

$arParams["NUMBER_WIDTH"] = intval($arParams["NUMBER_WIDTH"]);
if($arParams["NUMBER_WIDTH"]<=0)
	$arParams["NUMBER_WIDTH"]=5;
$arParams["TEXT_WIDTH"] = intval($arParams["TEXT_WIDTH"]);
if($arParams["TEXT_WIDTH"]<=0)
	$arParams["TEXT_WIDTH"]=20;
$arParams["LIST_HEIGHT"] = intval($arParams["LIST_HEIGHT"]);
if($arParams["LIST_HEIGHT"]<=0)
	$arParams["LIST_HEIGHT"]=5;


if ($_REQUEST["PROPERTY"]&&$_REQUEST["PROPERTY_VALUE"])
{
    //$arRestIB = getArIblock("catalog", CITY_ID);

    if($_REQUEST["CATALOG_ID"]=='banket'){
        if ($_REQUEST["PROPERTY"]=="osobennostibanket")
            $_REQUEST["PROPERTY"] = "FEATURES_B_H";

        if ($_REQUEST["PROPERTY"]=="specoborudovanie")
            $_REQUEST["PROPERTY"] = "BANKET_SPECIAL_EQUIPMENT";

        if ($_REQUEST["PROPERTY"]=="razreshenospirtnoe")
            $_REQUEST["PROPERTY"] = "ALLOWED_ALCOHOL";
    }


    if ($_REQUEST["PROPERTY"]=="street")
        $_REQUEST["PROPERTY"] = "STREET";
    elseif ($_REQUEST["PROPERTY"]=="metro")
        $_REQUEST["PROPERTY"] = "subway";
    elseif($_REQUEST["PROPERTY"]=="offers")
        $_REQUEST["PROPERTY"] = "proposals";
    elseif ($_REQUEST["PROPERTY"] == "okrug")
        $_REQUEST["PROPERTY"] = "administrative_distr";
    elseif ($_REQUEST["PROPERTY"] == "okrug")
        $_REQUEST["PROPERTY"] = "administrative_distr";
    elseif ($_REQUEST["PROPERTY"] == "suburb")
        $_REQUEST["PROPERTY"] = "out_city";
    elseif ($_REQUEST["PROPERTY"] == "prigorody")
        $_REQUEST["PROPERTY"] = "out_city";
		
	elseif ($_REQUEST["PROPERTY"] == "rajon")
        $_REQUEST["PROPERTY"] = "area";
	elseif ($_REQUEST["PROPERTY"] == "srednij_schet")
        $_REQUEST["PROPERTY"] = "average_bill";
	elseif ($_REQUEST["PROPERTY"] == "detyam")
        $_REQUEST["PROPERTY"] = "children";
	elseif ($_REQUEST["PROPERTY"] == "predlozheniya")
        $_REQUEST["PROPERTY"] = "proposals";
	elseif ($_REQUEST["PROPERTY"] == "osobennosti")
        $_REQUEST["PROPERTY"] = "features";
	elseif ($_REQUEST["PROPERTY"] == "razvlecheniya")
        $_REQUEST["PROPERTY"] = "entertainment";
	elseif ($_REQUEST["PROPERTY"] == "idealnoe_mesto_dlya")
        $_REQUEST["PROPERTY"] = "ideal_place_for";
		
    elseif ($_REQUEST["PROPERTY"] == "idealfor")
        $_REQUEST["PROPERTY"] = "ideal_place_for";
    elseif ($_REQUEST["PROPERTY"] == "entertainments")
        $_REQUEST["PROPERTY"] = "entertainment";
    elseif ($_REQUEST["PROPERTY"] == "rubrics")
        $_REQUEST["PROPERTY"] = "closed_rubrics";
    elseif ($_REQUEST["PROPERTY"] == "group")
        $_REQUEST["PROPERTY"] = "rest_group";
//    elseif ()
//        $_REQUEST["PROPERTY"] = "AROUND_THE_CLOCK";

    //ресторанные группы
//    elseif ($_REQUEST["PROPERTY"] == "group")
//        $_REQUEST["PROPERTY"] = "rest_group";



    
    if ($_REQUEST["PROPERTY2"]=="metro")
        $_REQUEST["PROPERTY2"] = "subway";
    elseif($_REQUEST["PROPERTY2"]=="offers")
        $_REQUEST["PROPERTY2"] = "proposals";
    elseif ($_REQUEST["PROPERTY2"] == "okrug")
        $_REQUEST["PROPERTY2"] = "administrative_distr";
    elseif ($_REQUEST["PROPERTY2"] == "okrug")
        $_REQUEST["PROPERTY2"] = "administrative_distr";
    elseif ($_REQUEST["PROPERTY2"] == "suburb")
        $_REQUEST["PROPERTY2"] = "out_city";
    elseif ($_REQUEST["PROPERTY2"] == "prigorody")
        $_REQUEST["PROPERTY2"] = "out_city";
		
	elseif ($_REQUEST["PROPERTY2"] == "rajon")
        $_REQUEST["PROPERTY2"] = "area";
	elseif ($_REQUEST["PROPERTY2"] == "srednij_schet")
        $_REQUEST["PROPERTY2"] = "average_bill";
	elseif ($_REQUEST["PROPERTY2"] == "detyam")
        $_REQUEST["PROPERTY2"] = "children";
	elseif ($_REQUEST["PROPERTY2"] == "predlozheniya")
        $_REQUEST["PROPERTY2"] = "proposals";
	elseif ($_REQUEST["PROPERTY2"] == "osobennosti")
        $_REQUEST["PROPERTY2"] = "features";
	elseif ($_REQUEST["PROPERTY2"] == "razvlecheniya")
        $_REQUEST["PROPERTY2"] = "entertainment";
	elseif ($_REQUEST["PROPERTY2"] == "idealnoe_mesto_dlya")
        $_REQUEST["PROPERTY2"] = "ideal_place_for";
		
    elseif ($_REQUEST["PROPERTY2"] == "idealfor")
        $_REQUEST["PROPERTY2"] = "ideal_place_for";
    elseif ($_REQUEST["PROPERTY2"] == "entertainments")
        $_REQUEST["PROPERTY2"] = "entertainment";
    elseif ($_REQUEST["PROPERTY2"] == "entertainments")
        $_REQUEST["PROPERTY2"] = "entertainment";
    elseif ($_REQUEST["PROPERTY2"] == "rubrics")
        $_REQUEST["PROPERTY2"] = "closed_rubrics";
    elseif ($_REQUEST["PROPERTY2"] == "group")
        $_REQUEST["PROPERTY2"] = "rest_group";

    if($_REQUEST["PROPERTY"] == "24hours" && $_REQUEST["PROPERTY_VALUE"]){
        $_REQUEST["arrFilter_pf"]['features'][0]=2392461;
    }

    $properties = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>$_REQUEST["PROPERTY"]));
    if ($prop_fields = $properties->GetNext())
    {
        if ($prop_fields["LINK_IBLOCK_ID"])
        {
            if ($_REQUEST["PROPERTY_VALUE"]=="all")
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"),false,false,Array("ID"));
                while($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]][] = $ar["ID"];
                    $_REQUEST["arrFilter_pf"][$prop_fields["CODE"]][]=$ar["ID"];
                }
            }
            else
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y","CODE"=>$_REQUEST["PROPERTY_VALUE"]),false,false,Array("ID"));
                if($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]] = $ar["ID"];
                    $_REQUEST["arrFilter_pf"][$prop_fields["CODE"]][0]=$ar["ID"];
                }
            }

            if($_REQUEST["PROPERTY"]=='d_tours'){
                $_REQUEST["arrFilter_pf"][$_REQUEST["PROPERTY"]][0]=$_REQUEST["PROPERTY_VALUE"];
            }
        }
        else {
            $_REQUEST["arrFilter_pf"][$prop_fields["CODE"]] = "Y";
        }
    }

    $properties = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>$_REQUEST["PROPERTY2"]));
    if ($prop_fields = $properties->GetNext())
    {
        if ($prop_fields["LINK_IBLOCK_ID"])
        {
            if ($_REQUEST["PROPERTY_VALUE2"]=="all")
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"),false,false,Array("ID"));
                while($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]][] = $ar["ID"];
                    $_REQUEST["arrFilter_pf"][$prop_fields["CODE"]][]=$ar["ID"];
                }
            }
            else
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y","CODE"=>$_REQUEST["PROPERTY_VALUE2"]),false,false,Array("ID"));
                if($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]] = $ar["ID"];
                    $_REQUEST["arrFilter_pf"][$prop_fields["CODE"]][0]=$ar["ID"];
                }
            }

            if($_REQUEST["PROPERTY2"]=='d_tours'){
                $_REQUEST["arrFilter_pf"][$_REQUEST["PROPERTY2"]][0]=$_REQUEST["PROPERTY_VALUE"];
            }

        }
        else
            $_REQUEST["arrFilter_pf"][$prop_fields["CODE"]] = "Y";
    }

    if($_REQUEST['PROPERTY']){  //  подборки - не включать спящие
        $_REQUEST["arrFilter_pf"]['sleeping_rest'] = 'Да';
    }
}



/*************************************************************************
		Processing the  "Filter" and "Reset" button actions
*************************************************************************/
$arDateFields = array(
	"ACTIVE_DATE" => array(
		"from" => "_ACTIVE_DATE_1",
		"to" => "_ACTIVE_DATE_2",
		"days_to_back" => "_ACTIVE_DATE_1_DAYS_TO_BACK",
		"filter_from" => ">=DATE_ACTIVE_FROM",
		"filter_to" => "<=DATE_ACTIVE_TO",
	),
	"DATE_ACTIVE_FROM" => array(
		"from" => "_DATE_ACTIVE_FROM_1",
		"to" => "_DATE_ACTIVE_FROM_2",
		"days_to_back" => "_DATE_ACTIVE_FROM_1_DAYS_TO_BACK",
		"filter_from" => ">=DATE_ACTIVE_FROM",
		"filter_to" => "<=DATE_ACTIVE_FROM",
	),
	"DATE_ACTIVE_TO" => array(
		"from" => "_DATE_ACTIVE_TO_1",
		"to" => "_DATE_ACTIVE_TO_2",
		"days_to_back" => "_DATE_ACTIVE_TO_1_DAYS_TO_BACK",
		"filter_from" => ">=DATE_ACTIVE_TO",
		"filter_to" => "<=DATE_ACTIVE_TO",
	),
	"DATE_CREATE" => array(
		"from" => "_DATE_CREATE_1",
		"to" => "_DATE_CREATE_2",
		"days_to_back" => "_DATE_CREATE_1_DAYS_TO_BACK",
		"filter_from" => ">=DATE_CREATE",
		"filter_to" => "<=DATE_CREATE",
	),
);

/*Init filter values*/
$arrPFV = array();
$arrCFV = array();
$arrFFV = array();//Element fields value
$arrDFV = array();//Element date fields
$arrOFV = array();//Offer fields values
$arrODFV = array();//Offer date fields
$arrOPFV = array();//Offer properties fields
foreach($arDateFields as $id => $arField)
{
	$arField["from"] = array(
		"name" => $FILTER_NAME.$arField["from"],
		"value" => "",
	);
	$arField["to"] = array(
		"name" => $FILTER_NAME.$arField["to"],
		"value" => "",
	);
	$arField["days_to_back"] = array(
		"name" => $FILTER_NAME.$arField["days_to_back"],
		"value" => "",
	);
	$arrDFV[$id] = $arField;

	$arField["from"]["name"] = "OF_".$arField["from"]["name"];
	$arField["to"]["name"] = "OF_".$arField["to"]["name"];
	$arField["days_to_back"]["name"] = "OF_".$arField["days_to_back"]["name"];
	$arrODFV[$id] = $arField;
}

/*Leave filter values empty*/
if(strlen($_REQUEST["del_filter"]) > 0)
{
	foreach($arrDFV as $id => $arField)
		$GLOBALS[$arField["days_to_back"]["name"]] = "";

	foreach($arrODFV as $id => $arField)
		$GLOBALS[$arField["days_to_back"]["name"]] = "";
}
/*Read filter values from request*/
elseif(strlen($_REQUEST["set_filter"]) > 0)
{
	if(isset($_REQUEST[$FILTER_NAME."_pf"]))
		$arrPFV = $_REQUEST[$FILTER_NAME."_pf"];
	if(isset($_REQUEST[$FILTER_NAME."_cf"]))
		$arrCFV = $_REQUEST[$FILTER_NAME."_cf"];
	if(isset($_REQUEST[$FILTER_NAME."_ff"]))
		$arrFFV = $_REQUEST[$FILTER_NAME."_ff"];
	if(isset($_REQUEST[$FILTER_NAME."_of"]))
		$arrOFV = $_REQUEST[$FILTER_NAME."_of"];
	if(isset($_REQUEST[$FILTER_NAME."_op"]))
		$arrOPFV = $_REQUEST[$FILTER_NAME."_op"];

	$now = time();
	foreach($arrDFV as $id => $arField)
	{
		$name = $arField["from"]["name"];
		if(isset($_REQUEST[$name]))
			$arrDFV[$id]["from"]["value"] = $_REQUEST[$name];

		$name = $arField["to"]["name"];
		if(isset($_REQUEST[$name]))
			$arrDFV[$id]["to"]["value"] = $_REQUEST[$name];

		$name = $arField["days_to_back"]["name"];
		if(isset($_REQUEST[$name]))
		{
			$value = $arrDFV[$id]["days_to_back"]["value"] = $_REQUEST[$name];
			if(strlen($value) > 0)
				$arrDFV[$id]["from"]["value"] = GetTime($now - 86400*intval($value));
		}
	}

	foreach($arrODFV as $id => $arField)
	{
		$name = $arField["from"]["name"];
		if(isset($_REQUEST[$name]))
			$arrODFV[$id]["from"]["value"] = $_REQUEST[$name];

		$name = $arField["to"]["name"];
		if(isset($_REQUEST[$name]))
			$arrODFV[$id]["to"]["value"] = $_REQUEST[$name];

		$name = $arField["days_to_back"]["name"];
		if(isset($_REQUEST[$name]))
		{
			$value = $arrODFV[$id]["days_to_back"]["value"] = $_REQUEST[$name];
			if(strlen($value) > 0)
				$arrODFV[$id]["from"]["value"] = GetTime($now - 86400*intval($value));
		}
	}
}
/*No action specified, so read from the session (if parameter is set)*/
elseif($arParams["SAVE_IN_SESSION"])
{
	if(isset($_SESSION[$FILTER_NAME."arrPFV"]))
		$arrPFV = $_SESSION[$FILTER_NAME."arrPFV"];
	if(isset($_SESSION[$FILTER_NAME."arrCFV"]))
		$arrCFV = $_SESSION[$FILTER_NAME."arrCFV"];
	if(isset($_SESSION[$FILTER_NAME."arrFFV"]))
		$arrFFV = $_SESSION[$FILTER_NAME."arrFFV"];
	if(isset($_SESSION[$FILTER_NAME."arrOFV"]))
		$arrOFV = $_SESSION[$FILTER_NAME."arrOFV"];
	if(isset($_SESSION[$FILTER_NAME."arrOPFV"]))
		$arrOPFV = $_SESSION[$FILTER_NAME."arrOPFV"];
	if(isset($_SESSION[$FILTER_NAME."arrDFV"]) && is_array($_SESSION[$FILTER_NAME."arrDFV"]))
	{
		foreach($_SESSION[$FILTER_NAME."arrDFV"] as $id => $arField)
		{
			$arrDFV[$id]["from"]["value"] = $arField["from"]["value"];
			$arrDFV[$id]["to"]["value"] = $arField["to"]["value"];
			$arrDFV[$id]["days_to_back"]["value"] = $arField["days_to_back"]["value"];
		}
	}
	if(isset($_SESSION[$FILTER_NAME."arrODFV"]) && is_array($_SESSION[$FILTER_NAME."arrODFV"]))
	{
		foreach($_SESSION[$FILTER_NAME."arrODFV"] as $id => $arField)
		{
			$arrODFV[$id]["from"]["value"] = $arField["from"]["value"];
			$arrODFV[$id]["to"]["value"] = $arField["to"]["value"];
			$arrODFV[$id]["days_to_back"]["value"] = $arField["days_to_back"]["value"];
		}
	}
}

/*Save filter values to the session*/
if($arParams["SAVE_IN_SESSION"])
{
	$_SESSION[$FILTER_NAME."arrPFV"] = $arrPFV;
	$_SESSION[$FILTER_NAME."arrCFV"] = $arrCFV;
	$_SESSION[$FILTER_NAME."arrFFV"] = $arrFFV;
	$_SESSION[$FILTER_NAME."arrOFV"] = $arrOFV;
	$_SESSION[$FILTER_NAME."arrDFV"] = $arrDFV;
	$_SESSION[$FILTER_NAME."arrODFV"] = $arrODFV;
	$_SESSION[$FILTER_NAME."arrOPFV"] = $arrOPFV;
}

if($_REQUEST['AJAX_REQUEST']!='Y'):
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()),$arParams["CACHE_NOTES"],CITY_ID,20)))
{
	$arResult["arrProp"] = array();
	$arResult["arrPrice"] = array();
	$arResult["arrSection"] = array();
	$arResult["arrOfferProp"] = array();

	/*if (in_array("SECTION_ID", $arParams["FIELD_CODE"]))
	{
		$arResult["arrSection"][0] = GetMessage("CC_BCF_TOP_LEVEL");
		$rsSection = CIBlockSection::GetList(
			Array("left_margin"=>"asc"),
			Array(
				"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
				"ACTIVE"=>"Y",
			),
			false,
			Array("ID", "DEPTH_LEVEL", "NAME")
		);
		while($arSection = $rsSection->Fetch())
		{
			$arResult["arrSection"][$arSection["ID"]] = str_repeat(" . ", $arSection["DEPTH_LEVEL"]).$arSection["NAME"];
		}
	}*/

	// prices
	/*if(CModule::IncludeModule("catalog"))
	{
		$rsPrice = CCatalogGroup::GetList($v1, $v2);
		while($arPrice = $rsPrice->Fetch())
		{
			if(($arPrice["CAN_ACCESS"] == "Y" || $arPrice["CAN_BUY"] == "Y") && in_array($arPrice["NAME"],$arParams["PRICE_CODE"]))
				$arResult["arrPrice"][$arPrice["NAME"]] = array("ID"=>$arPrice["ID"], "TITLE"=>$arPrice["NAME_LANG"]);
		}
	}
	else
	{
		$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
		while($arProp = $rsProp->Fetch())
		{
			if(in_array($arProp["CODE"],$arParams["PRICE_CODE"]) && in_array($arProp["PROPERTY_TYPE"], array("N")))
				$arResult["arrPrice"][$arProp["CODE"]] = array("ID"=>$arProp["ID"], "TITLE"=>$arProp["NAME"]);
		}
	}*/
        global $DB;
	// properties
        /*if (is_array($arParams["PROPERTY_CODE"]))
        {
            v_dump($arParams["PROPERTY_CODE"]);
            $rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ID"=>$arParams["PROPERTY_CODE"],"ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
            while ($arProp = $rsProp->Fetch())
            {
                    if($arProp["PROPERTY_TYPE"] != "F")
                    {
                            $arTemp = array(
                                    "CODE" => $arProp["CODE"],
                                    "NAME" => $arProp["NAME"],
                                    "PROPERTY_TYPE" => $arProp["PROPERTY_TYPE"],
                                    "MULTIPLE" => $arProp["MULTIPLE"],
                            );
                            if ($arProp["PROPERTY_TYPE"]=="L")
                            {
                                $arrEnum = array();
                                $rsEnum = CIBlockProperty::GetPropertyEnum($arProp["ID"]);
                                while($arEnum = $rsEnum->Fetch())
                                {
                                    $arrEnum[$arEnum["ID"]] = $arEnum["VALUE"];
                                }
                                $arTemp["VALUE_LIST"] = $arrEnum;
                            }
                            if ($arProp["PROPERTY_TYPE"]=="E" && $arProp["CODE"]!="subway")
                            {
                                $arrEnum = array();
                                $arTemp["LINK_IBLOCK_ID"] = $arProp["LINK_IBLOCK_ID"];
                                $rsEnum = CIBlockElement::GetList(Array("SORT"=>"ASC","NAME"=>"ASC"),Array("IBLOCK_ID"=>$arProp["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"));
                                while($arEnum = $rsEnum->GetNext())
                                {
                                    $arrEnum[$arEnum["ID"]] = $arEnum["NAME"];
                                }
                                $arTemp["VALUE_LIST"] = $arrEnum;

                            }
                            if ($arProp["PROPERTY_TYPE"]=="G" && $arProp["CODE"]!="menu")
                            {
                                $arrEnum = array();
                                $arTemp["LINK_IBLOCK_ID"] = $arProp["LINK_IBLOCK_ID"];
                                $sql = "SELECT * FROM b_iblock_section WHERE 1=1 AND IBLOCK_ID = ".$DB->ForSql($arProp["LINK_IBLOCK_ID"])." AND ACTIVE='Y' GROUP BY NAME ORDER BY NAME ASC";
                                $rsEnum = $DB->Query($sql);
                                //$rsEnum = CIBlockSection::GetList(Array("SORT"=>"ASC","NAME"=>"ASC"),Array("IBLOCK_ID"=>$arProp["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"),false);
                                while($arEnum = $rsEnum->GetNext())
                                {
                                    $arrEnum[$arEnum["ID"]] = $arEnum["NAME"];
                                }
                                $arTemp["VALUE_LIST"] = $arrEnum;

                            }
                            $arResult["arrProp"][$arProp["ID"]] = $arTemp;
                    }
            }
        }*/
        
	$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
	while ($arProp = $rsProp->Fetch())
	{
        //v_dump($arProp);
		if(in_array($arProp["CODE"],$arParams["PROPERTY_CODE"]) && $arProp["PROPERTY_TYPE"] != "F")
		{
			$arTemp = array(
                "ID" => $arProp["ID"],
				"CODE" => $arProp["CODE"],
				"NAME" => $arProp["NAME"],
				"PROPERTY_TYPE" => $arProp["PROPERTY_TYPE"],
				"MULTIPLE" => $arProp["MULTIPLE"],
			);
			if ($arProp["PROPERTY_TYPE"]=="L" && $arProp["CODE"]!="wi_fi" && $arProp["CODE"]!="hrs_24")
			{
                            $arrEnum = array();
                            $rsEnum = CIBlockProperty::GetPropertyEnum($arProp["ID"]);
                            while($arEnum = $rsEnum->Fetch())
                            {
                                $arrEnum[$arEnum["ID"]] = $arEnum["VALUE"];
                            }
                            $arTemp["VALUE_LIST"] = $arrEnum;
			}
                        if ($arProp["PROPERTY_TYPE"]=="E" /*&& $arProp["CODE"]!="subway"*/)
                        {
                            if ($arProp["CODE"]=="subway")
                            {
                                global $DB;
                                $arrEnum = array();                                
                                $sql = "SELECT BE.NAME as NAME,BE.CODE as CODE,BE.ID as ID, BE.IBLOCK_ID as IBLOCK_ID,BE.IBLOCK_SECTION_ID as IBLOCK_SECTION_ID, BS.PICTURE as PICTURE, BS.NAME as SECTION_NAME
                                        FROM b_iblock B 
                                        INNER JOIN b_iblock_element BE ON BE.IBLOCK_ID = B.ID 
                                        INNER JOIN b_iblock_section BS ON BE.IBLOCK_SECTION_ID = BS.ID
                                        WHERE 1=1 AND (BE.IBLOCK_ID = '".$arProp["LINK_IBLOCK_ID"]."') AND (BE.WF_STATUS_ID=1 AND BE.WF_PARENT_ELEMENT_ID IS NULL) 
                                        ORDER BY BE.NAME asc, BS.NAME asc";
                                $rsEnum = $DB->Query($sql);                                
                                while($arEnum = $rsEnum->Fetch())
                                {                                   
                                    if (LANGUAGE_ID=="en"&&$arEnum["CODE"])
                                        $arEnum["NAME"] = $arEnum["CODE"];
                                    else
                                    {
                                        $arEnum["NAME"] = explode("(",$arEnum["NAME"]);
                                        $arEnum["NAME"] = $arEnum["NAME"][0];
                                    }
                                    
                                    $db_props = CIBlockElement::GetProperty($arEnum["IBLOCK_ID"],$arEnum["ID"], array("sort" => "asc"), Array("CODE"=>"STYLE"));
                                    if($ar_props = $db_props->Fetch())
                                            $arEnum["STYLE"] = $ar_props["VALUE"];
                                    else
                                            $arEnum["STYLE"] = false;

                                    $db_props = CIBlockElement::GetProperty($arEnum["IBLOCK_ID"],$arEnum["ID"], array("sort" => "asc"), Array("CODE"=>"STYLE_NEW"));
                                    if($ar_props = $db_props->Fetch())
                                            $arEnum["STYLE_NEW"] = $ar_props["VALUE"];
                                    else
                                            $arEnum["STYLE_NEW"] = false;

                                    $arrEnum[$arEnum["ID"]] = $arEnum;
                                }
                                $arTemp["VALUE_LIST"] = $arrEnum;
                            }
                            else 
                            {
                                $arrEnum = array();
                                $arTemp["LINK_IBLOCK_ID"] = $arProp["LINK_IBLOCK_ID"];
                                //v_dump($arProp["LINK_IBLOCK_ID"]);
                                $filter = Array(
                                    "IBLOCK_ID"=>$arProp["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"
                                );
                                if($arProp["CODE"]=="kitchen")
                                    $filter["PROPERTY_".CITY_ID."_VALUE"] = "Да";
                                if($arProp["CODE"]=="type")
                                    $filter["PROPERTY_".CITY_ID."_VALUE"] = "Да";
                                $rsEnum = CIBlockElement::GetList(Array("SORT"=>"ASC","NAME"=>"ASC"),$filter,false, false, Array("ID","NAME","PROPERTY_eng_name",'CODE'));
                                while($arEnum = $rsEnum->Fetch())
                                {
                                    if(($arProp["CODE"]=='kitchen'||$arProp["CODE"]=='area'||$arProp["CODE"]=='out_city'||$arProp["CODE"]=='average_bill')&&$this->__templateName=='filter_2014'){//||$arProp["CODE"]=='BANKET_MENU_SUM'||$arProp["CODE"]=='kolichestvochelovek'
                                        if (LANGUAGE_ID=="en"&&$arEnum["PROPERTY_ENG_NAME_VALUE"])
                                            $arrEnum[$arEnum["ID"]]['NAME'] = $arEnum["PROPERTY_ENG_NAME_VALUE"];
                                        else
                                            $arrEnum[$arEnum["ID"]]['NAME'] = $arEnum["NAME"];

                                        $arrEnum[$arEnum["ID"]]['CODE'] = $arEnum["CODE"];
                                    }
                                    else {
                                        if (LANGUAGE_ID=="en"&&$arEnum["PROPERTY_ENG_NAME_VALUE"])
                                            $arrEnum[$arEnum["ID"]] = $arEnum["PROPERTY_ENG_NAME_VALUE"];
                                        else
                                            $arrEnum[$arEnum["ID"]] = $arEnum["NAME"];
                                    }

                                }
                                $arTemp["VALUE_LIST"] = $arrEnum;
                            }
                        }
                        if ($arProp["PROPERTY_TYPE"]=="G" && $arProp["CODE"]!="menu")
                        {
                            $arrEnum = array();
                            $arTemp["LINK_IBLOCK_ID"] = $arProp["LINK_IBLOCK_ID"];
                            $sql = "SELECT * FROM b_iblock_section WHERE 1=1 AND IBLOCK_ID = ".$DB->ForSql($arProp["LINK_IBLOCK_ID"])." AND ACTIVE='Y' GROUP BY NAME ORDER BY NAME ASC";
                            $rsEnum = $DB->Query($sql);
                            //$rsEnum = CIBlockSection::GetList(Array("SORT"=>"ASC","NAME"=>"ASC"),Array("IBLOCK_ID"=>$arProp["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"),false);
                            while($arEnum = $rsEnum->GetNext())
                            {
                                $arrEnum[$arEnum["ID"]] = $arEnum["NAME"];
                            }
                            $arTemp["VALUE_LIST"] = $arrEnum;
                            
                        }
			$arTest[$arTemp["CODE"]] = $arTemp;
		}
	}
        foreach ($arParams["PROPERTY_CODE"] as $code)
        {
            $art[$code] = $arTest[$code];
            $arResult["arrProp"][$art[$code]["ID"]] = $art[$code];
        }
        
        /*if ($arParams["SPEC_IBLOCK"])
        {
            $rsSpec = CIBlockElement::GetList(Array("sort"=>"ASC","name"=>"asc"),Array("IBLOCK_ID"=>$arParams["SPEC_IBLOCK"],"ACTIVE"=>"Y","GLOBAL_ACTIVE"=>"Y"));
            while($arSpec = $rsSpec->GetNext())
            {
                $arResult["SPEC_PROJECT"][] = $arSpec;
            }
        }*/
    $arResult["FORM_ACTION"] = isset($_SERVER['REQUEST_URI'])? htmlspecialchars($_SERVER['REQUEST_URI']): "";
    $arResult["FILTER_NAME"] = $FILTER_NAME;

	$this->EndResultCache();
//    $this->IncludeComponentTemplate();
}
endif;

/*************************************************************************
		Adding the titles and input fields
*************************************************************************/

$arResult["arrInputNames"] = array(); // array of the input field names; is being used in the function $APPLICATION->GetCurPageParam

// simple fields
$arResult["ITEMS"] = array();
//v_dump($arResult["arrProp"]);
/*foreach($arResult["arrProp"] as $prop_id => $arProp)
{
	$res = "";
	$arResult["arrInputNames"][$FILTER_NAME."_pf"]=true;
	switch ($arProp["PROPERTY_TYPE"])
	{
		case "L":
			$name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
			$value = $arrPFV[$arProp["CODE"]];
			if ($arProp["MULTIPLE"]=="Y")
				$res .= '<select multiple name="'.$name.'[]" size="'.$arParams["LIST_HEIGHT"].'">';
			else
				$res .= '<select name="'.$name.'">';
			$res .= '<option value="">'.GetMessage("CC_BCF_ALL").'</option>';
			foreach($arProp["VALUE_LIST"] as $key=>$val)
			{
				$res .= '<option';

				if (($arProp["MULTIPLE"] == "Y") && is_array($value))
				{
					if(in_array($key, $value))
						$res .= ' selected';
				}
				else
				{
					if($key == $value)
						$res .= ' selected';
				}

				$res .= ' value="'.htmlspecialchars($key).'">'.htmlspecialchars($val).'</option>';
			}
			$res .= '</select>';

			if ($arProp["MULTIPLE"]=="Y")
			{
				if (is_array($value) && count($value) > 0)
					${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
			}
			else
			{
				if (!is_array($value) && strlen($value) > 0)
					${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
			}
			break;
		case "N":
			$value = $arrPFV[$arProp["CODE"]];
			$name = $FILTER_NAME."_pf[".$arProp["CODE"]."][LEFT]";
			if(is_array($value) && isset($value["LEFT"]))
				$value_left = $value["LEFT"];
			else
				$value_left = "";
			$res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value_left).'" />&nbsp;'.GetMessage("CC_BCF_TILL").'&nbsp;';

			if (strlen($value_left) > 0)
				${$FILTER_NAME}["PROPERTY"][">=".$arProp["CODE"]] = doubleval($value_left);

			$name = $FILTER_NAME."_pf[".$arProp["CODE"]."][RIGHT]";
			if(is_array($value) && isset($value["RIGHT"]))
				$value_right = $value["RIGHT"];
			else
				$value_right = "";
			$res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value_right).'" />';

			if (strlen($value_right) > 0)
				${$FILTER_NAME}["PROPERTY"]["<=".$arProp["CODE"]] = doubleval($value_right);

			break;
		case "S":
		case "E":
		case "G":
			$name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
			$value = $arrPFV[$arProp["CODE"]];
			if(!is_array($value))
			{
				$res .= '<input type="text" name="'.$name.'" size="'.$arParams["TEXT_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

				if (strlen($value) > 0)
					${$FILTER_NAME}["PROPERTY"]["?".$arProp["CODE"]] = $value;
			}
			break;
	}
	if($res)
		$arResult["ITEMS"][] = array(
			"NAME" => htmlspecialchars($arProp["NAME"]),
			"INPUT" => $res,
			"INPUT_NAME" => $name,
			"INPUT_VALUE" => is_array($value)? array_map("htmlspecialchars", $value): htmlspecialchars($value),
			"~INPUT_VALUE" => $value,
 		);
}*/

$bHasOffersFilter = false;
/*foreach($arParams["OFFERS_FIELD_CODE"] as $field_code)
{
	$field_res = "";
	$arResult["arrInputNames"][$FILTER_NAME."_of"]=true;
	$name = $FILTER_NAME."_of[".$field_code."]";
	$value = $arrOFV[$field_code];
	switch ($field_code)
	{
		case "CODE":
		case "XML_ID":
		case "NAME":
		case "PREVIEW_TEXT":
		case "DETAIL_TEXT":
		case "IBLOCK_TYPE_ID":
		case "IBLOCK_ID":
		case "IBLOCK_CODE":
		case "IBLOCK_NAME":
		case "IBLOCK_EXTERNAL_ID":
		case "SEARCHABLE_CONTENT":
			$field_res = '<input type="text" name="'.$name.'" size="'.$arParams["TEXT_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

			if (strlen($value)>0)
				${$FILTER_NAME}["OFFERS"]["?".$field_code] = $value;

			break;
		case "ID":
		case "SORT":
		case "SHOW_COUNTER":
			$name = $FILTER_NAME."_of[".$field_code."][LEFT]";
			$value = $arrOFV[$field_code]["LEFT"];
			$field_res = '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value).'" />&nbsp;'.GetMessage("CC_BCF_TILL").'&nbsp;';

			if(strlen($value)>0)
				${$FILTER_NAME}["OFFERS"][">=".$field_code] = intval($value);

			$name = $FILTER_NAME."_of[".$field_code."][RIGHT]";
			$value = $arrOFV[$field_code]["RIGHT"];
			$field_res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

			if(strlen($value)>0)
				${$FILTER_NAME}["OFFERS"]["<=".$field_code] = intval($value);

			break;

		case "ACTIVE_DATE":
		case "DATE_ACTIVE_FROM":
		case "DATE_ACTIVE_TO":
		case "DATE_CREATE":
			$arDateField = $arrODFV[$field_code];
			$arResult["arrInputNames"][$arDateField["from"]["name"]]=true;
			$arResult["arrInputNames"][$arDateField["to"]["name"]]=true;
			$arResult["arrInputNames"][$arDateField["days_to_back"]["name"]]=true;

			$field_res = CalendarPeriod(
				$arDateField["from"]["name"], $arDateField["from"]["value"],
				$arDateField["to"]["name"], $arDateField["to"]["value"],
				$FILTER_NAME."_form", "Y", "class=\"inputselect\"", "class=\"inputfield\""
			);

			if(strlen($arDateField["from"]["value"]) > 0)
				${$FILTER_NAME}["OFFERS"][$arDateField["filter_from"]] = $arDateField["from"]["value"];

			if(strlen($arDateField["to"]["value"]) > 0)
				${$FILTER_NAME}["OFFERS"][$arDateField["filter_to"]] = $arDateField["to"]["value"];
			break;
	}
	if($field_res)
	{
		$bHasOffersFilter = true;
		$arResult["ITEMS"][] = array(
			"NAME" => htmlspecialchars(GetMessage("IBLOCK_FIELD_".$field_code)),
			"INPUT" => $field_res,
			"INPUT_NAME" => $name,
			"INPUT_VALUE" => htmlspecialchars($value),
			"~INPUT_VALUE" => $value,
		);
	}
}

foreach($arResult["arrOfferProp"] as $prop_id => $arProp)
{
	$res = "";
	$arResult["arrInputNames"][$FILTER_NAME."_op"]=true;
	switch ($arProp["PROPERTY_TYPE"])
	{
		case "L":

			$name = $FILTER_NAME."_op[".$arProp["CODE"]."]";
			$value = $arrOPFV[$arProp["CODE"]];
			if ($arProp["MULTIPLE"]=="Y")
				$res .= '<select multiple name="'.$name.'[]" size="'.$arParams["LIST_HEIGHT"].'">';
			else
				$res .= '<select name="'.$name.'">';
			$res .= '<option value="">'.GetMessage("CC_BCF_ALL").'</option>';
			foreach($arProp["VALUE_LIST"] as $key=>$val)
			{
				$res .= '<option';

				if (($arProp["MULTIPLE"] == "Y") && is_array($value))
				{
					if(in_array($key, $value))
						$res .= ' selected';
				}
				else
				{
					if($key == $value)
						$res .= ' selected';
				}

				$res .= ' value="'.htmlspecialchars($key).'">'.htmlspecialchars($val).'</option>';
			}
			$res .= '</select>';

			if ($arProp["MULTIPLE"]=="Y")
			{
				if (is_array($value) && count($value)>0)
					${$FILTER_NAME}["OFFERS"]["PROPERTY"][$arProp["CODE"]] = $value;
			}
			else
			{
				if (strlen($value)>0)
					${$FILTER_NAME}["OFFERS"]["PROPERTY"][$arProp["CODE"]] = $value;
			}
			break;

		case "N":

			$name = $FILTER_NAME."_op[".$arProp["CODE"]."][LEFT]";
			$value = $arrOPFV[$arProp["CODE"]]["LEFT"];
			$res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value).'" />&nbsp;'.GetMessage("CC_BCF_TILL").'&nbsp;';

			if (strlen($value)>0)
				${$FILTER_NAME}["OFFERS"]["PROPERTY"][">=".$arProp["CODE"]] = intval($value);

			$name = $FILTER_NAME."_op[".$arProp["CODE"]."][RIGHT]";
			$value = $arrOPFV[$arProp["CODE"]]["RIGHT"];
			$res .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

			if (strlen($value)>0)
				${$FILTER_NAME}["OFFERS"]["PROPERTY"]["<=".$arProp["CODE"]] = doubleval($value);

			break;

		case "S":
		case "E":
		case "G":

			$name = $FILTER_NAME."_op[".$arProp["CODE"]."]";
			$value = $arrOPFV[$arProp["CODE"]];
			$res .= '<input type="text" name="'.$name.'" size="'.$arParams["TEXT_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

			if (strlen($value)>0)
				${$FILTER_NAME}["OFFERS"]["PROPERTY"]["?".$arProp["CODE"]] = $value;

			break;
	}
	if($res)
	{
		$bHasOffersFilter = true;
		$arResult["ITEMS"][] = array(
			"NAME" => htmlspecialchars($arProp["NAME"]),
			"INPUT" => $res,
			"INPUT_NAME" => $name,
			"INPUT_VALUE" => htmlspecialchars($value),
			"~INPUT_VALUE" => $value,
 		);
	}
}

if($bHasOffersFilter)
{
	//This will force to use catalog.section offers price filter
	if(!isset(${$FILTER_NAME}["OFFERS"]))
		${$FILTER_NAME}["OFFERS"] = array();
}

foreach($arResult["arrPrice"] as $price_code => $arPrice)
{
	$res_price = "";
	$arResult["arrInputNames"][$FILTER_NAME."_cf"]=true;

	$name = $FILTER_NAME."_cf[".$arPrice["ID"]."][LEFT]";
	$value = $arrCFV[$arPrice["ID"]]["LEFT"];

	if (strlen($value)>0)
	{
		if(CModule::IncludeModule("catalog"))
			${$FILTER_NAME}[">=CATALOG_PRICE_".$arPrice["ID"]] = $value;
		else
			${$FILTER_NAME}[">=PROPERTY_".$arPrice["ID"]] = $value;
	}

	$res_price .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value).'" />&nbsp;'.GetMessage("CC_BCF_TILL").'&nbsp;';

	$name = $FILTER_NAME."_cf[".$arPrice["ID"]."][RIGHT]";
	$value = $arrCFV[$arPrice["ID"]]["RIGHT"];

	if (strlen($value)>0)
	{
		if(CModule::IncludeModule("catalog"))
			${$FILTER_NAME}["<=CATALOG_PRICE_".$arPrice["ID"]] = $value;
		else
			${$FILTER_NAME}["<=PROPERTY_".$arPrice["ID"]] = $value;
	}

	$res_price .= '<input type="text" name="'.$name.'" size="'.$arParams["NUMBER_WIDTH"].'" value="'.htmlspecialchars($value).'" />';

	$arResult["ITEMS"][] = array("NAME" => htmlspecialchars($arPrice["TITLE"]), "INPUT" => $res_price);

}*/

$arResult["arrInputNames"]["set_filter"]=true;
$arResult["arrInputNames"]["del_filter"]=true;

$arSkip = array(
	"AUTH_FORM" => true,
	"TYPE" => true,
	"USER_LOGIN" => true,
	"USER_CHECKWORD" => true,
	"USER_PASSWORD" => true,
	"USER_CONFIRM_PASSWORD" => true,
	"USER_EMAIL" => true,
	"captcha_word" => true,
	"captcha_sid" => true,
	"login" => true,
	"Login" => true,
	"backurl" => true,
);

foreach(array_merge($_GET, $_POST) as $key=>$value)
{
	if(
		!array_key_exists($key, $arResult["arrInputNames"])
		&& !array_key_exists($key, $arSkip)
	)
	{
		$arResult["ITEMS"][] = array(
			"HIDDEN" => true,
			"INPUT" => '<input type="hidden" name="'.htmlspecialchars($key).'" value="'.htmlspecialchars($value).'" />',
		);
	}
}

if($_REQUEST['AJAX_REQUEST']!='Y'):
$this->IncludeComponentTemplate();
endif;
?>
