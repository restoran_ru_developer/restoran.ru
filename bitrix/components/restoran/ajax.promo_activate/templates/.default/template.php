<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?if ($arParams["AJAX"]!="Y"):?>
    <form action="/bitrix/components/restoran/ajax.promo_activate/templates/.default/ajax.php" id="promo_form">
        <script src='/bitrix/components/restoran/user.profile/templates/.default/chosen.jquery.js'></script>
        <link href="/bitrix/components/restoran/ajax.promo_activate/templates/.default/chosen.css"  type="text/css" rel="stylesheet" />
        <script>
            $(document).ready(function(){
                $("#promo_submit").click(function(){
                    $.ajax({
                        url: $("#promo_form").attr("action"),
                        type: 'post',      
                        data: $("#promo_form").serialize(),
                        success: function(data)
                        {
                            $("#add_promo_response").text(data);
                        }
                    });
                    return false;
                });
            });
        </script>
        <div align="right">
            <a class="modal_close uppercase white" href="javascript:void(0)"></a>
            <Br />
        </div>
        <div id="add_promo_response" align="center"></div>
        <div class="title" style="text-align: center; border:none;">Активация промо-кода</div>
        <div class="question">
            Введите промо-код:<br />
            <input type="text" class="inputtext" name="promo" style="width:94%"/>
        </div>
        <div class="question" id="rest_block">
            Выберите ресторан:<br />
            <script type="text/javascript">
                    $(document).ready(function(){
                        $("#rest").chosen();         
                    });
            </script>
            <select id="rest" data-placeholder="Выберите ресторан" class="chzn-select" tabindex="2" name="restoran" id="rest" style="width:300px;">
                <option value="0">Добавить новый ресторан</option>
                <?foreach($arResult["RESTORANS"] as $rest):?>
                    <option value="<?=$rest["ID"]?>"><?=$rest["NAME"]?></option>
                <?endforeach;?>
            </select>
        </div>
        <br /><br />
        <div class="question" style="text-align:center">
            <input id="promo_submit" type="submit" class="light_button" value="Активировать"/>
        </div>
    </form>
<?else:?>
    <?if ($arResult["ERROR"]):
        echo $arResult["ERROR"];
    else:
        echo "ПОЗДРАВЛЯЕМ! ВЫ УСПЕШНО АКТИВИРОВАЛИ ПРОМО-КОД";
    endif;?>
<? endif; ?>
