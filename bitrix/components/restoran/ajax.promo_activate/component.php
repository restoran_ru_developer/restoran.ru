<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["REST"] = intval($arParams["REST"]);
$arParams["PROMO"] = trim($arParams["PROMO"]);
global $USER;

if ($arParams["AJAX"]!="Y")
{
    if($this->StartResultCache(false, array(($USER->GetID()))))
    {
        CModule::IncludeModule("iblock");
        $arRestIB = getArIblock("catalog", CITY_ID);
        $res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"catalog", "IBLOCK_ID"=>$arRestIB["ID"], "!PROPERTY_user_bind"=>false, "PROPERTY_user_bind"=>Array($USER->GetID())),false,false,Array("ID","NAME","CODE"));
        while ($ar_res = $res->GetNext())
        {
            $arResult["RESTORANS"][] = $ar_res;
        }
    }
}
else
{
    if (!$arParams["PROMO"])
        $error[] = GetMessage("NO_PROMO");
    else
    {
        CModule::IncludeModule("catalog");
        CModule::IncludeModule("sale");
        if ($arParams["REST"])
        {
            $res = CCatalogDiscountCoupon::GetList(Array(),Array("COUPON"=>$arParams["PROMO"]));
            if ($ar_res = $res->GetNext())
            {
                CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID(), False);		
                Add2BasketByProductID($arParams["ELEMENT_ID"], 1);

                $arFields = array(
                    "LID" => "s1",
                    "PERSON_TYPE_ID" => 3,
                    "PAYED" => "Y",
                    "CANCELED" => "N",
                    "STATUS_ID" => "N",
                    "PRICE" => 0.0,
                    "CURRENCY" => "RUB",
                    "USER_ID" => IntVal($USER->GetID()),
                    "PAY_SYSTEM_ID" => 7,
                    "PRICE_DELIVERY" => 0,
                    "DELIVERY_ID" => "",
                    "DISCOUNT_VALUE" => 0,
                    "TAX_VALUE" => 0.0,
                    "USER_DESCRIPTION" => ""
                );

                $ORDER_ID = CSaleOrder::Add($arFields);
                $ORDER_ID = IntVal($ORDER_ID);
                CSaleBasket::OrderBasket($ORDER_ID, $_SESSION["SALE_USER_ID"], SITE_ID);

                $dateMonthsAdded = strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "+3 month");

                // set validity date
                $dateMonthsAdded = CIBlockFormatProperties::DateFormat('d.m.Y', $dateMonthsAdded);
                $arOrderFields = array(
                    "ORDER_ID" => $ORDER_ID,
                    "ORDER_PROPS_ID" => 19,
                    "NAME" => "Срок действия",
                    "CODE" => "validity",
                    "VALUE" => $dateMonthsAdded
                );
                CSaleOrderPropsValue::Add($arOrderFields);

                $arOrderFields = array(
                    "ORDER_ID" => $ORDER_ID,
                    "ORDER_PROPS_ID" => 18,
                    "NAME" => "Привязка к ресторану",
                    "CODE" => "resto_bind",
                    "VALUE" => $arParams["REST"]
                );
                CSaleOrderPropsValue::Add($arOrderFields);
                $arRestIB = getArIblock("catalog", CITY_ID);
                CIBlockElement::SetPropertyValuesEx($arParams["REST"], $arRestIB["ID"], array("account_bind" => $ORDER_ID));
            }
            else
            {
                $error[] = GetMessage("WRONG_PROMO");
            }
        }
    }
    if (count($error)>0)
        $arResult["ERROR"] = implode(",<br /> ",$error);
}
$this->IncludeComponentTemplate();
?>