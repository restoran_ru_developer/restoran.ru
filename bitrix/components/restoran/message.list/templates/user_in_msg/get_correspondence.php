<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!CModule::IncludeModule("socialnetwork"))
    return false;

global $USER;
$curUserID = $USER->GetID();
$secUserID = $_REQUEST["secUserID"];
$eventID = $_REQUEST["eventID"];

$APPLICATION->IncludeComponent(
    "bitrix:socialnetwork.messages_users_messages",
    "user_correspondence",
    Array(
        "EVENT_ID" => $eventID,
        "SET_NAVCHAIN" => "N",
        "MESSAGE_VAR" => "",
        "PATH_TO_USER" => "",
        "PATH_TO_MESSAGE_FORM" => "",
        "PATH_TO_MESSAGE_FORM_MESS" => "",
        "PATH_TO_MESSAGES_CHAT" => "",
        "PATH_TO_MESSAGES_USERS" => "",
        "PATH_TO_MESSAGES_USERS_MESSAGES" => "",
        "PAGE_VAR" => "",
        "USER_VAR" => "",
        "USER_ID" => $secUserID,
        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
        "ITEMS_COUNT" => "20"
    ),
    false
);
?>