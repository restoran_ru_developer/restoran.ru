<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!CModule::IncludeModule("socialnetwork")&&!CModule::IncludeModule("iblock"))
{
	ShowError(GetMessage("SONET_MODULE_NOT_INSTALL"));
	return;
}
if (!$USER->IsAuthorized())
{
	ShowError(GetMessage("NO_AUTORIZED"));
	return;    
}
$arParams["USER_ID"] = $USER->GetID();
$arParams["SECOND_USER_ID"] = (int)$arParams["SECOND_USER_ID"];
$arParams["MESSAGE_LENGTH"] = ($arParams["MESSAGE_LENGTH"])?$arParams["MESSAGE_LENGTH"]:150;
$arParams["PATH_TO_USER"] = trim($arParams["PATH_TO_USER"]);
$arParams["PATH_TO_USER_MESSAGES"] = trim($arParams["PATH_TO_USER_MESSAGES"]);

$arParams["ITEMS_COUNT"] = IntVal($arParams["ITEMS_COUNT"]);
if ($arParams["ITEMS_COUNT"] <= 0)
	$arParams["ITEMS_COUNT"] = 30;

$arParams["PATH_TO_SMILE"] = trim($arParams["PATH_TO_SMILE"]);


if ($_REQUEST["action"] == "delete" && check_bitrix_sessid() && IntVal($_REQUEST["eventID"]) > 0)
{
        $errorMessage = "";

        if (!CSocNetMessages::DeleteMessage(IntVal($_REQUEST["eventID"]), $GLOBALS["USER"]->GetID()))
        {
                if ($e = $APPLICATION->GetException())
                        $errorMessage .= $e->GetString();
        }

        if (strlen($errorMessage) > 0)
                $arResult["ErrorMessage"] = $errorMessage;
}


global $DB;
if ($arParams["SECOND_USER_ID"])
    $sql = "SELECT * FROM b_sonet_messages WHERE (TO_USER_ID = ".$arParams["USER_ID"]." AND FROM_USER_ID=".$arParams["SECOND_USER_ID"]." AND TO_DELETED!='Y') OR (FROM_USER_ID = ".$arParams["USER_ID"]." AND TO_USER_ID=".$arParams["SECOND_USER_ID"]." AND FROM_DELETED!='Y') ORDER BY DATE_CREATE DESC LIMIT 0 , 30";
else        
    $sql = "SELECT * FROM b_sonet_messages WHERE TO_USER_ID = ".$arParams["USER_ID"]." OR FROM_USER_ID=".$arParams['USER_ID']." ORDER BY DATE_CREATE DESC LIMIT 0 , 30";
$rsUsers = $DB->Query($sql);
    while($arUser = $rsUsers->GetNext()) :
        if ($arUser["TO_USER_ID"]==$arParams["USER_ID"])
        {
            $r = CUser::GetByID($arUser["FROM_USER_ID"]);
            if ($a = $r->Fetch())
            {
                $arUser["USER_ID"] = $a["ID"]; 
                $arUser["USER_NAME"] = $a["NAME"];
                $title = $a["NAME"]." ".$a["LAST_NAME"];                
                $arUser["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($a["PERSONAL_PHOTO"], array('width'=>64, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true, Array());
                if ($a["PERSONAL_PHOTO"])
                    $arUser["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($a["PERSONAL_PHOTO"], array('width'=>64, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true, Array());
                else
                {
                    switch ($a["PERSONAL_GENDER"])
                    {
                            case "M":
                                    $arUser["USER_PERSONAL_PHOTO_FILE"]["src"] = "/tpl/images/noname/man_nnm.png";
                                    break;
                            case "F":
                                   $arUser["USER_PERSONAL_PHOTO_FILE"]["src"] = "/tpl/images/noname/woman_nnm.png";
                                    break;
                            default:
                                    $arUser["USER_PERSONAL_PHOTO_FILE"]["src"] = "/tpl/images/noname/unisx_nnm.png";
                    }                    
                }
            }            
        }
        if ($arUser["FROM_USER_ID"]==$arParams["USER_ID"])
        {
            $r = CUser::GetByID($arUser["FROM_USER_ID"]);
            if ($a = $r->Fetch())
            {
                $arUser["USER_ID"] = $a["ID"]; 
                $arUser["USER_NAME"] = $a["NAME"];
                if ($a["PERSONAL_PHOTO"])
                    $arUser["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($a["PERSONAL_PHOTO"], array('width'=>64, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true, Array());
                else
                {
                    switch ($a["PERSONAL_GENDER"])
                    {
                            case "M":
                                    $arUser["USER_PERSONAL_PHOTO_FILE"]["src"] = "/tpl/images/noname/man_nnm.png";
                                    break;
                            case "F":
                                   $arUser["USER_PERSONAL_PHOTO_FILE"]["src"] = "/tpl/images/noname/woman_nnm.png";
                                    break;
                            default:
                                    $arUser["USER_PERSONAL_PHOTO_FILE"]["src"] = "/tpl/images/noname/unisx_nnm.png";
                    }                    
                }
            }            
        }
        $date = $DB->FormatDate($arUser["DATE_CREATE"], 'YYYY-MM-DD HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arUser["DATE_CREATE_FORMATED"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($date, CSite::GetDateFormat()));
        $tmpDate = explode(' ', $arUser["DATE_CREATE_FORMATED"]);
        $arUser["DATE_CREATE_FORMATED_1"] = $tmpDate[0];
        $arUser["DATE_CREATE_FORMATED_2"] = strtolower($tmpDate[1])." ".$tmpDate[2];
        $arUser["DATE_CREATE_FORMATED_3"] = CIBlockFormatProperties::DateFormat("G:i", MakeTimeStamp($date, CSite::GetDateFormat()));
        if ($arUser["DATE_VIEW"])
            $arUser["IS_READ"] = true;        
        
        if(strlen($arUser["MESSAGE"]) > $arParams["MESSAGE_LENGTH"]) {
            $obParser = new CTextParser;
            $arUser["TRUNCATED_MESSAGE"] = $obParser->html_cut($arUser["MESSAGE"], $arParams["MESSAGE_LENGTH"]);
        }
        $arUser["DELETE_LINK"] = htmlspecialchars($APPLICATION->GetCurUri("eventID=".$arUser["ID"]."&action=delete&".bitrix_sessid_get().""));
        $arResult["ITEMS"][] = $arUser;
    endwhile;
    $arResult["NAV_STRING"] = $rsUsers->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
    $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
    $arResult["NAV_RESULT"] = $rsUsers;
    $this->SetResultCacheKeys(array(
            "ITEMS",
    ));
    $APPLICATION->SetTitle("Обмен сообщениями с ".$title);
$this->IncludeComponentTemplate();
?>