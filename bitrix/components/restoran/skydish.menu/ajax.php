<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


function load_section(){
	global $APPLICATION;
	
	$dir = parse_url($_REQUEST["dir"]);
	
	$APPLICATION->IncludeComponent(
		"restoran:skydish.menu",
		"load_section",
		Array("DIR"=>$dir["path"], "ACTIVE"=>$_REQUEST["section_id"],"REST_ID"=>$_REQUEST["rest_id"]),
		false
	);	
	
	
}

if($_REQUEST["action"]=="load_section") load_section();

?>    