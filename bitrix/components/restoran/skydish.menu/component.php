<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arParams["REST_ID"]=="") return false;


//кешируем json на час чтобы не сильо напрягать skydish

$cache_lifetime =3600; 
$cache_id = md5("rest".trim($arParams["REST_ID"]));
$cache_dir = "/skydish_menu";

$obCache = new CPHPCache;

if($obCache->InitCache($cache_lifetime, $cache_id, $cache_dir)){
    $cres = $obCache->GetVars();
	$JSON=$cres["JSON"];
}elseif($obCache->StartDataCache()) {
	
	$JSON_URL="https://skydish.ru/api/v1/menu/".trim($arParams["REST_ID"])."/json";


	$string = file_get_contents($JSON_URL);
	$JSON = json_decode($string, true);

    $obCache->EndDataCache(array("JSON"=>$JSON));
}
        

foreach($JSON["data"]["menuGroups"] as $menuGroup){
	$arResult["GROUPS"][$menuGroup["id"]]=$menuGroup["name"];
	if(!$arParams["ACTIVE"] || $arParams["ACTIVE"]=="") $arParams["ACTIVE"]=$menuGroup["id"];
	
	foreach($menuGroup["items"] as $menuitem){
		$arResult["ITEMS"][$menuGroup["id"]][]=$menuitem;
	}
	
}
/*
if($USER->isAdmin()){
	echo '<pre>';
	var_dump($JSON_URL);
	echo '</pre>';
}*/
$this->IncludeComponentTemplate();
?>