$(document).ready(function() {
	
	$(".menu_menu li a").click(function(){
		var section_id = $(this).data("section-id");
		var rest_id = $(this).data("rest-id");
		var url = $(this).attr("href");
		
		$(".menu_menu li").removeClass("active");
		$(this).parent("li").addClass("active");
			
		$.get(skydish_menu_core, {"action":"load_section","section_id":section_id,"rest_id":rest_id},function(resp){
			$(".right-side").html(resp);			
			
			if (history && history.pushState) {
				history.pushState({foo: 'bar'}, '', url);
			}else{
		
			}
			
		},"html");
		return false;
	});
	
	
});
