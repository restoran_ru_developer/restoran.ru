<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Отправить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка №";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['I_WANT'] = "Приглашение";
$MESS ['INVITE'] = "пригласить";
$MESS ['IN_RESTORAN'] = "в ресторан";
$MESS ['NA'] = "на";
$MESS ['AT'] = "в";
$MESS ['OUR_COMPANY'] = "Наша компания состоит из";
$MESS ['PEOPLE'] = "человек";
$MESS ['HOW_MUCH_BUY'] = "и мы планируем потратить";
$MESS ['BY_PERSON'] = "руб. на человека";
$MESS ['MY_NAME'] = "Меня зовут";
$MESS ['FOR_FEEDBACK'] = "Для обратной связи я оставлю ";
$MESS ['MY_PHONE'] = "свой телефон:";
$MESS ['MY_EMAIL'] = "или e-mail:";   
$MESS ['SAVE_COMMENT'] = "Приглашение успешно отправленно. ";   
$MESS ['CAPTCHA'] = "Подтверждаю, я не робот, на картинке написано:";   
$MESS ['LIST_USERS'] = "Список приглашенных пользователей:";   
?>
