<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
	var skydish_menu_core = "<?=$component->__path?>/ajax.php";
</script>


<div class="pull-left" style="width:240px;">
<div class="grey">
    <i style="color: #f03054;font-weight: 900;">Изменено: <?=FormatDate('j F Y', time())?></i>
</div>

<br>
<ul class="menu_menu">
	<?foreach($arResult["GROUPS"] as $gid=>$G){?>
    <li id="sec<?=$gid?>" <?if($arParams["ACTIVE"]==$gid) echo 'class="active"';?>><a href="<?=$arParams["DIR"]?>?SECTION_ID=<?=$gid?>" data-section-id="<?=$gid?>" data-rest-id="<?=$arParams["REST_ID"]?>"><?=$G?></a>&nbsp;<sup><?=count($arResult["ITEMS"][$gid])?></sup></li>
    <?}?>
</ul>

</div>



<div class="right-side" style="width:700px">
	<div class="menu_links">   
		<div class="left"><a href="?all=Y" onclick="">Смотреть все меню</a></div>
        <div class="right"><noindex>Распечатать: &nbsp; <a href="<?=$arParams["DIR"]?>?SECTION_ID=<?=$arParams["ACTIVE"]?>&amp;print=Y" rel="nofollow">Страницу меню</a> / <a href="<?=$arParams["DIR"]?>?all=Y&amp;print=Y" rel="nofollow">Все меню</a></noindex>
        </div>
        <div class="clear"></div>
    </div>
    
    <h1 class="menu_name"><?=$arResult["GROUPS"][$arParams["ACTIVE"]]?></h1>
    
    <table id="dostavka_table" cellpadding="0" cellspacing="0">        
        <tr class="end">            
            <td colspan="8"><h3 style="font18"></h3></td>
        </tr>
		<?foreach($arResult["ITEMS"][$arParams["ACTIVE"]] as $menuitem){?>
		<tr>                    
			<td colspan="<?if($menuitem["description"]!="") echo 2; else echo 3;?>" class="name"><?=$menuitem["name"]?></td>
			<?if($menuitem["description"]!=""){?>
				<td class="preview_text "><?=$menuitem["description"]?></td>
			<?}?>
			<td class="price end">
				<table width="100%" cellpadding="5">
					<tr>
						<td colspan="3" nowrap=""><?=$menuitem["price"]?> <span class="rouble font16">e</span></td>                                                        
					</tr>
				</table>
			</td>
		</tr>
		<?}?>
	</table>
	
	<div class="clear"></div>
	<br>
</div>