function addUserSubscribe(arParams) {
    function __handlerAddUserSubscribe(data) {
        var obContainer = document.getElementById("comm_sub_div");
        if (obContainer) {
            obContainer.innerHTML = data;
        }
    }

    $.ajax({
        type: 'POST',
        url: '/bitrix/components/restoran/subscribe.comments_sub_add/templates/.default/ajax.php',
        data: arParams,
        success: __handlerAddUserSubscribe
    });
}
