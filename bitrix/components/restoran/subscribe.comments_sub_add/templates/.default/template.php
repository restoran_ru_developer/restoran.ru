<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if($arParams["AJAX_CALL"] != "Y"):?>
    <div id="comm_sub_div">
        <?if(!$arResult["ALLREADY_SUBSCRIBED"]):?>
            <input type="button" onclick="addUserSubscribe(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" value="<?=GetMessage("SUBSCRIBE_BUTTON")?>" />
        <?else:?>
            <?$APPLICATION->IncludeComponent(
                "restoran:subscribe.comments_sub_delete",
                "",
                Array(
                    "IBLOCK_TYPE_ARTICLE" => "",
                    "IBLOCK_ID_ARTICLE" => $arResult["IBLOCK_ID_ARTICLE"],
                    "IBLOCK_TYPE_COMMENTS" => "",
                    "IBLOCK_ID_COMMENTS" => $arResult["IBLOCK_ID_COMMENTS"],
                    "ARTICLE_SECTION_ID" => $arResult["ARTICLE_SECTION_ID"], // задается ID раздела, если статьи в разделах
                    "ARTICLE_ELEMENT_ID" => $arResult["ARTICLE_ELEMENT_ID"], // задается ID элемента, если статьи в элементах (афиша и т.д.)
                    "USER_ID" => $USER->GetID()
                ),
                false
            );?>
        <?endif?>
    </div>
<?else:?>
    <?v_dump($arResult);?>
<?endif?>