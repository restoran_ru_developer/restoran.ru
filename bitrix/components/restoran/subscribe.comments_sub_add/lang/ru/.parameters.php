<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS["IBLOCK_TYPE_ARTICLE"] = "Тип инфо-блока (статьи)";
$MESS["IBLOCK_ID_ARTICLE"] = "Инфо-блок (статьи)";
$MESS["IBLOCK_TYPE_COMMENTS"] = "Тип инфо-блока (комментарии)";
$MESS["IBLOCK_ID_COMMENTS"] = "Инфо-блок (комментарии)";
$MESS["USER_ID"] = "ID пользователя";
$MESS["ARTICLE_SECTION_ID"] = "ID раздела статьи";
$MESS["ARTICLE_ELEMENT_ID"] = "ID элемента статьи";
?>