<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["AJAX_CALL"] = $arParams["AJAX_CALL"] == "Y" ? "Y" : "N";

if(!$arParams["IBLOCK_ID_ARTICLE"] || !$arParams["IBLOCK_ID_COMMENTS"]) {
	ShowError("IBLOCK_ID IS NOT DEFINED");
	return false;
}

if(!$arParams["USER_ID"])
    $arParams["USER_ID"] = $USER->GetID();

if(!$arParams["ARTICLE_SECTION_ID"] && !$arParams["ARTICLE_ELEMENT_ID"]) {
    ShowError("ARTICLE ID IS NOT DEFINED");
    return false;
}

if($arParams["AJAX_CALL"] != "Y" && $arParams["USER_ID"] && ($arParams["ARTICLE_SECTION_ID"] || $arParams["ARTICLE_ELEMENT_ID"])) {
    if($arParams["ARTICLE_SECTION_ID"]) {
        // get article info, if section
        $rsArticleSec = CIBlockSection::GetList(
            Array("SORT" => "ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams["IBLOCK_ID_ARTICLE"],
                "ID" => $arParams["ARTICLE_SECTION_ID"]
            ),
            false,
            Array("ID", "UF_SECTION_BIND")
        );
        if($arArticleSec = $rsArticleSec->Fetch()) {
            // get bined users
            $rsCommSec = CIBlockSection::GetList(
                Array("SORT" => "ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_ID" => $arParams["IBLOCK_ID_COMMENTS"],
                    "ID" => $arArticleSec["UF_SECTION_BIND"]
                ),
                false,
                Array("ID", "UF_USER_SUBSCRIBED")
            );
            $arCommSec = $rsCommSec->Fetch();

            // clear 0 users
            $arSubUsers = remove_item_by_value($arCommSec["UF_USER_SUBSCRIBED"], 0);

            // set subscribe section id
            $subSecID = $arArticleSec["UF_SECTION_BIND"];
        }
    } elseif($arParams["ARTICLE_ELEMENT_ID"]) {
        // get article info, if element
        $rsArticleEl = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams["IBLOCK_ID_ARTICLE"],
                "ID" => $arParams["ARTICLE_ELEMENT_ID"]
            ),
            false,
            false,
            Array("ID", "PROPERTY_COMMENTS")
        );
        $arArticleEl = $rsArticleEl->Fetch();

        // get bined users
        $rsCommSec = CIBlockSection::GetList(
            Array("SORT" => "ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams["IBLOCK_ID_COMMENTS"],
                "ID" => $arArticleEl["PROPERTY_COMMENTS_VALUE"]
            ),
            false,
            Array("ID", "UF_USER_SUBSCRIBED")
        );
        $arCommSec = $rsCommSec->Fetch();

        // clear 0 users
        $arSubUsers = remove_item_by_value($arCommSec["UF_USER_SUBSCRIBED"], 0);

        // set subscribe section id
        $subSecID = $arArticleEl["PROPERTY_COMMENTS_VALUE"];
    }

    // check user already subscribed
    if(in_array($arParams["USER_ID"], $arSubUsers)) {
        $arResult["ALLREADY_SUBSCRIBED"] = true;
    }

    // set result
    $arResult["IBLOCK_ID_ARTICLE"] = $arParams["IBLOCK_ID_ARTICLE"];
    $arResult["IBLOCK_ID_COMMENTS"] = $arParams["IBLOCK_ID_COMMENTS"];
    $arResult["USER_ID"] = $arParams["USER_ID"];
    $arResult["ARTICLE_SECTION_ID"] = $arParams["ARTICLE_SECTION_ID"];
    $arResult["ARTICLE_ELEMENT_ID"] = $arParams["ARTICLE_ELEMENT_ID"];
} elseif(!in_array($arParams["USER_ID"], $arSubUsers) && $arParams["AJAX_CALL"] == "Y" && $arParams["USER_ID"] && ($arParams["ARTICLE_SECTION_ID"] || $arParams["ARTICLE_ELEMENT_ID"])) {
    $bs = new CIBlockSection;

    // add cur user
    $arParams["SUBSCRIBED_USER_ARRAY"][] = $arParams["USER_ID"];

    // set comments subscribe
    $arFieldsSecUp = Array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID_COMMENTS"],
        "UF_USER_SUBSCRIBED" => $arParams["SUBSCRIBED_USER_ARRAY"]
    );
    $arResult["RESULT"] = $bs->Update($arParams["SUBSCRIBE_SECTION_ID"], $arFieldsSecUp);
}

$arTmpParams = array(
    "IBLOCK_ID_ARTICLE" => $arParams["IBLOCK_ID_ARTICLE"],
    "IBLOCK_ID_COMMENTS" => $arParams["IBLOCK_ID_COMMENTS"],
    "USER_ID" => $arParams["USER_ID"],
    "ARTICLE_SECTION_ID" => $arParams["ARTICLE_SECTION_ID"],
    "ARTICLE_ELEMENT_ID" => $arParams["ARTICLE_ELEMENT_ID"],
    "SUBSCRIBED_USER_ARRAY" => $arSubUsers,
    "SUBSCRIBE_SECTION_ID" => $subSecID,
);

$arResult["JS_PARAMS"] = CUtil::PhpToJsObject($arTmpParams);

$this->IncludeComponentTemplate();

if ($arParams["AJAX_CALL"] != "Y") {
    IncludeAJAX();
    $template =& $this->GetTemplate();
    $APPLICATION->AddHeadScript($template->GetFolder().'/proceed.js');
}
?>