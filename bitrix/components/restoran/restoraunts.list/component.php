<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

//if($_SESSION["CONTEXT"]=="Y") $_REQUEST["CONTEXT"]="Y";	
if($APPLICATION->get_cookie("CONTEXT")=="Y") $_REQUEST["CONTEXT"]="Y";

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
    $arParams["IBLOCK_TYPE"] = "news";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";

// set sort value
switch($_REQUEST["pageRestSort"]) {
    case "new":
        $arParams["SORT_BY1"] = "ID";
        break;
    case "price":
        $arParams["SORT_BY1"] = "property_average_bill";
        break;
    case "ratio":
        $arParams["SORT_BY1"] = "property_ratio";
        break;
    case "alphabet":
        $arParams["SORT_BY1"] = "NAME";
        break;
    case "popular":
        $arParams["SORT_BY1"] = "PROPERTY_rating_date";
        $arParams["SORT_ORDER1"] = "DESC";
        $arParams["SORT_BY2"] = "PROPERTY_stat_day";
        $arParams["SORT_ORDER2"] = "DESC";
        break;
    default:
        $arParams["SORT_BY1"] = $arParams["SORT_BY1"];
        break;
}
$arParams["SORT_ORDER1"] = ($_REQUEST["by"])?$_REQUEST["by"]:$arParams["SORT_ORDER1"];
$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
    $arParams["SORT_BY1"] = "ACTIVE_FROM";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
    $arParams["SORT_ORDER1"]="DESC";

if(strlen($arParams["SORT_BY2"])<=0)
    $arParams["SORT_BY2"] = "SORT";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
    $arParams["SORT_ORDER2"]="ASC";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

if(!is_array($arParams["FIELD_CODE"]))
    $arParams["FIELD_CODE"] = array();

if(!is_array($arParams["PROPERTY_CODE"]))
    $arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $key=>$val)
    if($val==="")
        unset($arParams["PROPERTY_CODE"][$key]);

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"]<=0)
    $arParams["NEWS_COUNT"] = 20;

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
    $arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
    $arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
$arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
$arParams["HIDE_LINK_WHEN_NO_DETAIL"] = $arParams["HIDE_LINK_WHEN_NO_DETAIL"]=="Y";

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
    $arNavParams = array(
        "nPageSize" => $arParams["NEWS_COUNT"],
        "bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
        "bShowAll" => $arParams["PAGER_SHOW_ALL"],
    );

    //  для фильтров с наборами first_20
    if($_REQUEST['CUSTOM_PAGEN_01']){
        $arNavParams['iNumPage']=$_REQUEST['CUSTOM_PAGEN_01'];
    }

    $arNavigation = CDBResult::GetNavParams($arNavParams);
    if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
        $arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
    $arNavParams = array(
        "nTopCount" => $arParams["NEWS_COUNT"],
        "bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
    );
    $arNavigation = false;
}

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
    $arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
    $arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
    foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
    {
        if(in_array($PERM, $arUserGroupArray))
        {
            $bUSER_HAVE_ACCESS = true;
            break;
        }
    }
}

/* get filter val from get */

/*$arrFilter["NAME"] = "%".$_GET["q"]."%";
$arrFilter["PROPERTY_kitchen"] = $_GET["cuisine"];
$arrFilter["PROPERTY_average_bill"] = $_GET["average_bill"];
$arrFilter["PROPERTY_subway"] = $_GET["subway"];
$arrFilter["PROPERTY_type"] = $_GET["rest_type"];*/




if ($_REQUEST["arrFilter_pf"])
{
    /*if ($USER->IsAdmin())
    {
        $arrFilter[0] =  Array("LOGIC"=>"OR",
            Array(
                Array("PROPERTY_kitchen"=>Array($_REQUEST["arrFilter_pf"]["kitchen"][0])),
                Array("PROPERTY_kitchen"=>Array($_REQUEST["arrFilter_pf"]["kitchen"][0],$_REQUEST["arrFilter_pf"]["kitchen"][1])),
            )
        );                                
        v_dump($arrFilter);
    }
    else
    {*/
    if ($_REQUEST["arrFilter_pf"]["subway"])
    {
        foreach ($_REQUEST["arrFilter_pf"]["subway"] as $subway)
        {
            $ids_s = RestIBlock::get_sm_subway($subway);
            foreach ($ids_s as $s_id)
            {
                if (!in_array($s_id, $_REQUEST["arrFilter_pf"]["subway"]))
                    $_REQUEST["arrFilter_pf"]["subway"][] = $s_id;
            }
        }
    }
    foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
    {
        /*if ($USER->IsAdmin()):
            if ($key=="kitchen")
            {

                $arrFilter[0] =  Array("LOGIC"=>"OR",
                    Array(
                $arrFilter[0] =        Array("LOGIC"=>"AND",
                            Array("PROPERTY_kitchen" => "184605"),
                            Array("PROPERTY_kitchen" => "184668"),
                        )
                    ),
                    Array("PROPERTY_kitchen"=>Array(184605,184668)),
                );
                //$arrFilter["?PROPERTY_".$key] = implode(" && ",$ar);
                unset($arrFilter["PROPERTY_kitchen"]);
                v_dump($arrFilter);
            }
            else
                $arrFilter["PROPERTY_".$key] = $ar;
        else:*/
        if ($key=="breakfast")
            $arrFilter["PROPERTY_".$key."_VALUE"] = $ar;
        elseif ($key=="wi_fi"&&$ar)
            $arrFilter["PROPERTY_".$key."_VALUE"] = "Да";
        elseif ($key=="sale10")
            $arrFilter["PROPERTY_".$key."_VALUE"] = $ar;
        elseif ($key=="d_tours"&&$ar)
            $arrFilter["!PROPERTY_".$key] = false;
        elseif ($key=="sleeping_rest"&&$ar)
            $arrFilter["!PROPERTY_".$key."_VALUE"] = 'Да';
        else
            $arrFilter["PROPERTY_".$key] = $ar;
        //endif;
    }
    /*if ($USER->IsAdmin())
    {          
        $ar_add = array();  
        $pro = $arrFilter;
        unset($pro["PROPERTY_kitchen"]);
        $ar_add["LOGIC"] = "OR";
        foreach ($arrFilter["PROPERTY_kitchen"] as $key=>$kit)
        {            
            $ar_k = array();            
             for ($i=0;$i<=$key;$i++)
             {
                 $ar_k[] = $arrFilter["PROPERTY_kitchen"][$key];
             }             
             $pro["PROPERTY_kitchen"] = $ar_k;
             $ar_add[$key] = $pro;   
        }
        //unset($arrFilter);        
        unset($arrFilter);
        $arrFilter[0] =  $ar_add;                   
    }*/
    //}
}
if ($_REQUEST["letter"]&&$arrFilter["NAME"])
{
    $ar = Array("LOGIC"=>"OR",
        Array("NAME"=>$arrFilter["NAME"]),
        Array("TAGS"=>$arrFilter["NAME"]),
    );
    //$ar = array('NAME'=>$arrFilter["NAME"]);
    unset($arrFilter["NAME"]);
    $arrFilter[0] = $ar;
}

if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N" ? false: $USER->GetGroups()),943677,$arParams["CACHE_NOTES"],$_SESSION["lat"],$_SESSION["lon"],$_REQUEST["lat"],$_REQUEST["lon"],$_REQUEST["arrFilter_pf"],$_REQUEST["page"], $_REQUEST["PAGEN_1"],$_REQUEST["CONTEXT"],$arrFilter, $_REQUEST["letter"], $arParams["SORT_BY1"])))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $rsIBlock = CIBlock::GetList(array(), array(
        "TYPE" =>$arParams["IBLOCK_TYPE"],
        "ACTIVE" => "Y",
        "CODE" => $arParams["IBLOCK_ID"],
        "SITE_ID" => SITE_ID,
        'ID'=>$arParams['IBLOCK_INT_ID']
    ));
    if($arResult = $rsIBlock->GetNext()) {
        $arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
        //SELECT
        $arSelect = array_merge($arParams["FIELD_CODE"], array(
            "ID",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "ACTIVE_FROM",
            "DETAIL_PAGE_URL",
            "DETAIL_TEXT",
            "DETAIL_TEXT_TYPE",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "PREVIEW_PICTURE",
            "DETAIL_PICTURE",
            "TAGS"
        ));

//        if(!$USER->IsAdmin()){
            $bGetProperty = count($arParams["PROPERTY_CODE"])>0;
            if($bGetProperty)
                $arSelect[]="PROPERTY_*";
//        }


        //WHERE
        $arFilter = array (
            "IBLOCK_ID" => $arResult["ID"],
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE" => "Y",
            //"CHECK_PERMISSIONS" => "Y",
            //"!PROPERTY_NETWORK_REST_VALUE" => "Да"
        );

        //$arFilter['!PROPERTY_NETWORK_REST_VALUE'] = 'Да';

        if($arParams["CHECK_DATES"])
            $arFilter["ACTIVE_DATE"] = "Y";

        $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
            $arParams["PARENT_SECTION"],
            $arParams["PARENT_SECTION_CODE"],
            array(
                "GLOBAL_ACTIVE" => "Y",
                "IBLOCK_ID" => $arResult["ID"],
            )
        );

        if($arParams["PARENT_SECTION"] > 0) {
            $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
            if($arParams["INCLUDE_SUBSECTIONS"])
                $arFilter["INCLUDE_SUBSECTIONS"] = "Y";

            $arResult["SECTION"]= array("PATH" => array());
            $rsPath = GetIBlockSectionPath($arResult["ID"], $arParams["PARENT_SECTION"]);
            $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
            while($arPath=$rsPath->GetNext())
            {
                $arResult["SECTION"]["PATH"][] = $arPath;
            }
        }
        else
        {
            $arResult["SECTION"]= false;
        }
        //v_dump($arParams["PARENT_SECTION"]);
        //ORDER BY

        if ($_REQUEST["pageRestSort"]=="popular")
        {
            $arSort = array(
                $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
                $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
                "NAME" => "ASC"
            );
        }
        elseif ($_REQUEST["pageRestSort"]=="popular2")
        {
            $arSort = array(
                "PROPERTY_restoran_ratio"=>"asc,nulls",
                "SORT"=>"ASC",
                //"NAME" => "ASC"
            );
        }
        elseif ($_REQUEST["pageRestSort"]=="popular-mobile")
        {
            $arSort = array(
                $arParams["SORT_BY1"] => $arParams["SORT_ORDER1"],
                $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"],
                $arParams["SORT_BY3"] => $arParams["SORT_ORDER3"],
                "ID" => "DESC"
            );
        }
        else
        {
            $arSort = array(
                $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
                //$arParams["SORT_BY9"]=>$arParams["SORT_ORDER9"],
            );
            if($arParams["SORT_BY9"]){
                $arSort[$arParams["SORT_BY9"]] = $arParams["SORT_ORDER9"];
            }
        }
        //if(!array_key_exists("ID", $arSort))
        //	$arSort["ID"] = "DESC";

        $arResult["ITEMS"] = array();
        $arResult["ELEMENTS"] = array();

        //v_dump(array_merge($arFilter, $arrFilter));
        //v_dump($arNavParams);
        if (!$arrFilter["PROPERTY_sale10_VALUE"]&&!$arrFilter["ID"]&&(!$arrFilter["PROPERTY_rest_group"]||$arParams["NO_SLEEP"]=="Y")&&$_REQUEST['PROPERTY']!='STREET')
            $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';


        if (($arSort["distance"]=="asc"||$arSort["distance"]=="ASC")&&(($_SESSION["lat"]&&$_SESSION["lon"])||($_REQUEST["lat"]&&$_REQUEST["lon"])))
        {
            $arS = Array("PROPERTY_LAT","PROPERTY_LON");
            $a = array_search("PROPERTY_*", $arSelect);
            unset($arSelect[$a]);

            if ($arParams["NO_SLEEP_TO_NEAR"]!="Y")
                unset($arrFilter["!PROPERTY_sleeping_rest_VALUE"]);
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/classes/cakeiblockelement.php");
            FirePHP::getInstance()->info(array_merge($arFilter, $arrFilter),'filter cake');
            //FirePHP::getInstance()->info($arSort,'$arSort');
            $rsElement = CCakeIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, array_merge($arSelect,$arS), $arParams['DISTANCE_VALUE']);
        }
        else {
            //FirePHP::getInstance()->info(array_merge($arFilter, $arrFilter),'filter2');
            //FirePHP::getInstance()->info($arSort,'$arSort');
            $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, $arSelect);
        }


        $rsElement->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
        while($obElement = $rsElement->GetNextElement())
        {
            $arItem = $obElement->GetFields();
            /*$arButtons = CIBlock::GetPanelButtons(
                $arItem["IBLOCK_ID"],
                $arItem["ID"],
                0,
                array("SECTION_BUTTONS"=>false, "SESSID"=>false)
            );
            $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
            $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];*/
            $arItem["DETAIL_PAGE_URL"] = str_replace("#CATALOG_ID#",$arParams["PARENT_SECTION_CODE"],$arItem["DETAIL_PAGE_URL"]);
            if($arParams["PREVIEW_TRUNCATE_LEN"]>0 && $arItem["PREVIEW_TEXT_TYPE"]!="html")
            {
                $end_pos = $arParams["PREVIEW_TRUNCATE_LEN"];
                while(substr($arItem["PREVIEW_TEXT"],$end_pos,1)!=" " && $end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $end_pos++;
                if($end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $arItem["PREVIEW_TEXT"] = substr($arItem["PREVIEW_TEXT"], 0, $end_pos)."...";
            }

            if(strlen($arItem["ACTIVE_FROM"])>0)
                $arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
            else
                $arItem["DISPLAY_ACTIVE_FROM"] = "";

            if($arItem["PREVIEW_PICTURE"]) {
                $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
                //$arPhoto = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => $arParams["PREVIEW_PICTURE_MAX_WIDTH"], 'height' => $arParams["PREVIEW_PICTURE_MAX_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
            }
            /*else {
                // get photo from prop
                $rsPhotoProp = CIBlockElement::GetProperty($arResult["ID"], $arItem["ID"], array("id" => "asc"), Array("CODE" => $arParams["PREVIEW_PICTURE_ADDITIONAL_PROP_CODE"]));
                $arPhotoProp = $rsPhotoProp->Fetch();
                $arPhoto = CFile::ResizeImageGet($arPhotoProp["VALUE"], array('width' => $arParams["PREVIEW_PICTURE_MAX_WIDTH"], 'height' => $arParams["PREVIEW_PICTURE_MAX_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
            }
            unset($arItem["PREVIEW_PICTURE"]);
            $arItem["PREVIEW_PICTURE"]["SRC"] = $arPhoto["src"];
            $arItem["PREVIEW_PICTURE"]["WIDTH"] = $arPhoto["width"];
            $arItem["PREVIEW_PICTURE"]["HEIGHT"] = $arPhoto["height"];*/

            //if(array_key_exists("DETAIL_PICTURE", $arItem))
            //	$arItem["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);

            $arItem["FIELDS"] = array();
            foreach($arParams["FIELD_CODE"] as $code)
                if(array_key_exists($code, $arItem))
                    $arItem["FIELDS"][$code] = $arItem[$code];

//            if(!$USER->IsAdmin()){
                if($bGetProperty)
                    $arItem["PROPERTIES"] = $obElement->GetProperties();
//            }
//            else {
//                $PROPERTY_CODE_LIST = $arParams['PROPERTY_CODE'];
//                foreach ($PROPERTY_CODE_LIST as $prop_name) {
//                    $arItem["PROPERTIES"][$prop_name] = $ob->GetProperty($prop_name);
//                }
//            }



            $arItem["DISPLAY_PROPERTIES"]=array();
            foreach($arParams["PROPERTY_CODE"] as $pid)
            {
                $prop = &$arItem["PROPERTIES"][$pid];
                if((is_array($prop["VALUE"]) && count($prop["VALUE"])>0) ||
                    (!is_array($prop["VALUE"]) && strlen($prop["VALUE"])>0))
                {
                    $arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
                }
            }

            $arResult["ITEMS"][] = $arItem;
            $arResult["ELEMENTS"][] = $arItem["ID"];
        }

        //  для наборов first_20
        if($_REQUEST['CUSTOM_PAGEN_01']) {
            $rsElement->NavPageCount++;
            if($_REQUEST['page']!=1){
                $rsElement->NavPageNomer++;
            }
        }


//        elseif($arParams['PLUS_PAGE']){
//            $rsElement->NavPageCount++;
//        }

        if($arParams['SHOW_REST_COUNT']=='Y'){
            $arResult['REQUEST_OBJECT_COUNT'] = $rsElement->SelectedRowsCount();
//            FirePHP::getInstance()->info($arResult['REQUEST_OBJECT_COUNT'],'REQUEST_OBJECT_COUNT');
        }

        $arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
        $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
        $arResult["NAV_RESULT"] = $rsElement;
        $this->SetResultCacheKeys(array(
            "ID",
            "IBLOCK_TYPE_ID",
            "LIST_PAGE_URL",
            "NAV_CACHED_DATA",
            "NAME",
            "SECTION",
            "ELEMENTS",
            'REQUEST_OBJECT_COUNT'
        ));
        $this->IncludeComponentTemplate();
    }
    else
    {
        $this->AbortResultCache();
        ShowError(GetMessage("T_NEWS_NEWS_NA"));
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
    }
}

if(isset($arResult["ID"]))
{
    /*$arTitleOptions = null;
    if($USER->IsAuthorized())
    {
        if(
            $APPLICATION->GetShowIncludeAreas()
            || (is_object($GLOBALS["INTRANET_TOOLBAR"]) && $arParams["INTRANET_TOOLBAR"]!=="N")
            || $arParams["SET_TITLE"]
        )
        {
            if(CModule::IncludeModule("iblock"))
            {
                $arButtons = CIBlock::GetPanelButtons(
                    $arResult["ID"],
                    0,
                    $arParams["PARENT_SECTION"],
                    array("SECTION_BUTTONS"=>false)
                );

                if($APPLICATION->GetShowIncludeAreas())
                    $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

                if(
                    is_array($arButtons["intranet"])
                    && is_object($GLOBALS["INTRANET_TOOLBAR"])
                    && $arParams["INTRANET_TOOLBAR"]!=="N"
                )
                {
                    foreach($arButtons["intranet"] as $arButton)
                        $GLOBALS["INTRANET_TOOLBAR"]->AddButton($arButton);
                }

                if($arParams["SET_TITLE"])
                {
                    $arTitleOptions = array(
                        'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
                        'PUBLIC_EDIT_LINK' => "",
                        'COMPONENT_NAME' => $this->GetName(),
                    );
                }
            }
        }
    }

    $this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);
*/
    if($arParams["SET_TITLE"])
    {
        $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
    }

    /*if($arParams["INCLUDE_IBLOCK_INTO_CHAIN"] && isset($arResult["NAME"]))
    {
        if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
            $APPLICATION->AddChainItem(
                $arResult["NAME"]
                ,strlen($arParams["IBLOCK_URL"]) > 0? $arParams["IBLOCK_URL"]: $arResult["LIST_PAGE_URL"]
            );
        else
            $APPLICATION->AddChainItem($arResult["NAME"]);
    }

    if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
    {
        foreach($arResult["SECTION"]["PATH"] as $arPath)
        {
            $APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
        }
    }

    return $arResult["ELEMENTS"];*/
}
//v_dump($_REQUEST);
?>