<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Фильтр по ресторанам",
	"DESCRIPTION" => "Позволяет отфильтровать рестораны",
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content_restoran", // for example "my_project"
		"CHILD" => array(
            "ID" => "restoraunt",
            "NAME" => GetMessage("CAT_REST"),
            "SORT" => 10,
		),
	),
	"COMPLEX" => "N",
);

?>