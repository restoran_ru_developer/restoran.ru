<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypes = CIBlockParameters::GetIBlockTypes();

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
{
    if ($i==0)
        $iblock_id = $arRes["ID"];
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];
    $i++;
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"ITEMS_LIMIT" => Array(
			"PARENT" => "BASE",
			"NAME" => "ITEMS_LIMIT",
			"TYPE" => "STRING",
			"DEFAULT" => "4",
		),
        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypes,
            "DEFAULT" => "system",
            "REFRESH" => "Y",
        ),
        "AVERAGE_BILL_IB_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_AVERAGE_BILL_IB_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "REST_TYPE_IB_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_REST_TYPE_IB_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),

		"CACHE_TIME"  =>  Array("DEFAULT"=>360000),

	),
);
?>