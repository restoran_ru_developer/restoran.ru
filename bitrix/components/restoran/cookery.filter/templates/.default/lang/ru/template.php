<?
$MESS ['BSF_T_SEARCH_BUTTON'] = "НАЙТИ";
$MESS ['BSF_T_WHERE_SEARCH_REST'] = "по ресторанам";
$MESS ['BSF_T_WHERE_SEARCH_NEWS'] = "по новостям";
$MESS ['BSF_T_WHERE_SEARCH_RECIPE'] = "по рецептам";
$MESS ['BSF_T_WHERE_SEARCH_BLOG'] = "по блогам";
$MESS ['BSF_T_WHERE_SEARCH_OVERVIEWS'] = "по обзорам";
$MESS ['BSF_T_WHERE_SEARCH_INTERVIEWS'] = "по интервью";
$MESS ['BSF_T_WHERE_SEARCH_MASTER_CLASS'] = "по мастер-классам";
$MESS ['BSF_T_SEARCH_EXMP_STRING'] = "Например, Чиз-кейк";
$MESS ['CATEGORY_TITLE'] = "Категории";
$MESS ['INGRIDIENT_TITLE'] = "Основной ингридиент";
$MESS ['TIME_TITLE'] = "Время приготовления";
$MESS ['DIFICULTY_TITLE'] = "Сложноть";
$MESS ['TECHNOLOGY_TITLE'] = "Технология";
$MESS ['PREPORATION_TITLE'] = "Приготовление";
$MESS ['CHOOSE_BUTTON'] = "ВЫБРАТЬ";
?>