<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>    
        <?if($APPLICATION->GetCurDir() == "/content/cookery/"):?>  
		<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">		
		<div class="search left">
			
            <div class="filter_box">
                <div class="left filter_block" id="filter1">
                    <div class="title"><?=GetMessage("CATEGORY_TITLE")?></div>
					<input type="hidden" value="recipe" name="search_in" />
                    <ul class="filter_category_block cuselMultiple-scroll-multi3">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["CATEGORY"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow another js">все категории</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_1").popup({"obj":"filter1","css":{"left":"-10px","top":"0px", "width":"345px"}});
                            var params = {
                                changedEl: "#multi3",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_1" style="display:none">
                        <div class="title"><?=GetMessage("CATEGORY_TITLE")?></div>
                        <!--<div id="multi3_sel_cont" name="cuisine"></div>-->
                        <select multiple="multiple" class="asd" id="multi3" name="category[]" size="10">
                            <?foreach($arResult["CATEGORY"] as $cuisine):?>
                                <option value="<?=$cuisine["ID"]?>"><?=$cuisine["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi3" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="left filter_block" id="filter2">
                    <div class="title"><?=GetMessage("INGRIDIENT_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi1">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["INGRIDIENT"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another">все игридиенты</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_2").popup({"obj":"filter2","css":{"left":"-10px","top":"0px", "width":"345px"}});
                            var params = {
                                changedEl: "#multi1",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_2" style="display:none">
                        <div class="title"><?=GetMessage("INGRIDIENT_TITLE")?></div>
                        <!--<div id="multi1_sel_cont" name="average_bill"></div>-->
                        <select multiple="multiple" class="asd" id="multi1" name="ingridient[]" size="10">
                            <?foreach($arResult["INGRIDIENT"] as $avBill):?>
                                <option value="<?=$avBill["ID"]?>"><?=$avBill["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="left filter_block" id="filter3">
                    <div class="title"><?=GetMessage("TIME_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi11">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]-1; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["TIME"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow another js">дольше</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_11").popup({"obj":"filter3","css":{"left":"-10px","top":"0px", "width":"345px"}});
                            var params = {
                                changedEl: "#multi11",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_11" style="display:none">
                        <div class="title"><?=GetMessage("TIME_TITLE")?></div>
                        <!--<div id="multi4_sel_cont" name="subway"></div>-->
                        <select multiple="multiple" class="asd" id="multi11" name="subway[]" size="10">
                            <?foreach($arResult["TIME"] as $subway):?>
                                <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi11" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="left filter_block" id="filter4">
                    <div class="title">Кухня</div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi4">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["KITCHEN"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow another js">все кухни</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_4").popup({"obj":"filter4","css":{"left":"-230px","top":"0px", "width":"830px"}});
                            var params = {
                                changedEl: "#multi4",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_4" style="display:none">
                        <div class="title">Кухня</div>
                        <!--<div id="multi4_sel_cont" name="subway"></div>-->
                        <select multiple="multiple" class="asd" id="multi4" name="kitchen[]" size="15">
                            <?foreach($arResult["KITCHEN"] as $subway):?>
                                <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi4" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
				<div class="left filter_block " id="filter21">
                    <div class="title">Повод</div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi21">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["CAUSE"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another">все поводы</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_21").popup({"obj":"filter21","css":{"left":"-10px","top":"0px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi21",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_21"  style="display:none">
                        <div class="title">Повод</div>
                        <select multiple="multiple" class="asd" id="multi21" name="cause[]" size="10">
                            <?foreach($arResult["CAUSE"] as $restType):?>
                                <option value="<?=$restType["ID"]?>"><?=$restType["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi21" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                
				<!--<div class="left filter_block " id="filter5">
                    <div class="title"><?=GetMessage("TECHNOLOGY_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi1">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["TECHNOLOGY"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow"></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_4").popup({"obj":"filter4","css":{"left":"-10px","top":"0px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi2",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_1">
                        <div class="title"><?=GetMessage("TECHNOLOGY_TITLE")?></div>
                        <select multiple="multiple" class="asd" id="multi1" name="technology[]" size="10">
                            <?foreach($arResult["TECHNOLOGY"] as $restType):?>
                                <option value="<?=$restType["ID"]?>"><?=$restType["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>-->
                <div class="left filter_block end" id="filter6">
                    <div class="title"><?=GetMessage("PREPORATION_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi2">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["PREPORATION"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another">все способы</a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_16").popup({"obj":"filter6","css":{"left":"-10px","top":"0px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi2",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_16"  style="display:none">
                        <div class="title"><?=GetMessage("PREPORATION_TITLE")?></div>
                        <select multiple="multiple" class="asd" id="multi2" name="preporation[]" size="10">
                            <?foreach($arResult["PREPORATION"] as $restType):?>
                                <option value="<?=$restType["ID"]?>"><?=$restType["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi2" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
			
		</div>	
		<div class="button right">
				<input style="width:168px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
				<?if ($APPLICATION->GetCurDir()=="/content/cookery/"):?>
					<?$APPLICATION->IncludeFile(
						SITE_TEMPLATE_PATH."/include_areas/top_search_links.php",
						Array(),
						Array("MODE"=>"html")
					);?>
				<?endif;?>
		</div> 
		</form>		
    <?endif;?>              
    <div class="clear"></div>
	<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
    <div class="search_block left">
	
        <div class="left" id="title-search">
            <?/*$APPLICATION->IncludeComponent(
                    "bitrix:search.title",
                    "main_search_suggest",
                    Array(
                            "SHOW_INPUT" => "N",
                            "INPUT_ID" => "title-search-input",
                            "CONTAINER_ID" => "title-search",
                            "PAGE" => "#SITE_DIR#search/index.php?search_in=rest",
                            "NUM_CATEGORIES" => "1",
                            "TOP_COUNT" => "5",
                            "ORDER" => "rank",
                            "USE_LANGUAGE_GUESS" => "Y",
                            "CHECK_DATES" => "N",
                            "SHOW_OTHERS" => "N",
                            "CATEGORY_0_TITLE" => "",
                            "CATEGORY_0" => array("iblock_catalog"),
                            "CATEGORY_0_iblock_catalog" => array("11")
                    ),
            false
            );*/?>
            <input <?/*?>id="title-search-input"<?*/?> id="top_search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : GetMessage("BSF_T_SEARCH_EXMP_STRING"))?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
        </div>
        <div class="right">
            <select id="search_in" name="search_in" class="search_in">
                <option<?if($_REQUEST["search_in"] == 'rest'):?> selected="selected"<?endif?> value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></option>
                <option<?if($_REQUEST["search_in"] == 'news'):?> selected="selected"<?endif?> value="news"><?=GetMessage("BSF_T_WHERE_SEARCH_NEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'recipe' || !$_REQUEST["search_in"]):?> selected="selected"<?endif?> value="recipe"><?=GetMessage("BSF_T_WHERE_SEARCH_RECIPE")?></option>
                <option<?if($_REQUEST["search_in"] == 'blog'):?> selected="selected"<?endif?> value="blog"><?=GetMessage("BSF_T_WHERE_SEARCH_BLOG")?></option>
                <option<?if($_REQUEST["search_in"] == 'overviews'):?> selected="selected"<?endif?> value="overviews"><?=GetMessage("BSF_T_WHERE_SEARCH_OVERVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'interview'):?> selected="selected"<?endif?> value="interview"><?=GetMessage("BSF_T_WHERE_SEARCH_INTERVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'master_classes'):?> selected="selected"<?endif?> value="master_classes"><?=GetMessage("BSF_T_WHERE_SEARCH_MASTER_CLASS")?></option>
            </select>
        </div>
        <div class="clear"></div>
    </div>
	
	<div class="right search_button">
		<input type="submit" class="dark_button" value="Поиск" />
	</div>
	</form>
    <div class="clear"></div>
