<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600;
}

if($arParams["ITEMS_LIMIT"] < 1)
    $arParams["ITEMS_LIMIT"] = 4;

$props = Array(84,85,82,81,83,87);

$CACHE_ID = CITY_ID."|".SITE_ID."|".$APPLICATION->GetCurPage()."|".$USER->GetGroups()."|".$arParams["ITEMS_LIMIT"]."|".$_REQUEST["search_in"]."|".$_REQUEST["q"];

if($this->StartResultCache(false, $CACHE_ID)) {

	if(!CModule::IncludeModule("iblock")) {
		$this->AbortResultCache();
		ShowError("IBLOCK_MODULE_NOT_INSTALLED");
		return false;
	}	

	$arSort = array("SORT" => "ASC", "NAME" => "ASC", "ID" => "DESC");
    $arSelect = array("ID", "NAME");

    // get cuisine list
	$arFilterCuisine = array("IBLOCK_ID" => $props[0], "ACTIVE" => "Y");
        $rsCuisines = CIBlockElement::GetList($arSort, $arFilterCuisine, false, false, $arSelect);
        while($arCuisines = $rsCuisines->GetNext())
            $arResult["CATEGORY"][] = $arCuisines;

    // get average bill list
    $arFilterAvBill = array("IBLOCK_ID" => $props[1], "ACTIVE" => "Y");
        $rsAvBills = CIBlockElement::GetList($arSort, $arFilterAvBill, false, false, $arSelect);
        while($arAvBills = $rsAvBills->GetNext())
            $arResult["INGRIDIENT"][] = $arAvBills;

    // get subway list
	$arFilterSubway = array("IBLOCK_ID" => $props[2], "ACTIVE" => "Y");
        $rsSubway = CIBlockElement::GetList($arSort, $arFilterSubway, false, false, $arSelect);
        while($arSubway = $rsSubway->GetNext())
            $arResult["TIME"][] = $arSubway;

    // get restaurants type list
    $arFilterRestType = array("IBLOCK_ID" => $props[3], "ACTIVE" => "Y");
    if($arParams["REST_TYPE_IB_ID"]) {
        $rsRestType = CIBlockElement::GetList($arSort, $arFilterRestType, false, false, $arSelect);
        while($arRestType = $rsRestType->GetNext())
            $arResult["DIFICULTY"][] = $arRestType;
    }

	// get restaurants type list
    $arFilterRestType1 = array("IBLOCK_ID" => $props[4], "ACTIVE" => "Y");
        $rsRestType = CIBlockElement::GetList($arSort, $arFilterRestType1, false, false, $arSelect);
        while($arRestType1 = $rsRestType->GetNext())
            $arResult["TECHNOLOGY"][] = $arRestType1;

	// get restaurants type list
    $arFilterRestType2 = array("IBLOCK_ID" => $props[5], "ACTIVE" => "Y");
        $rsRestType = CIBlockElement::GetList($arSort, $arFilterRestType2, false, false, $arSelect);
        while($arRestType2 = $rsRestType->GetNext())
            $arResult["PREPORATION"][] = $arRestType2;
	
	// get restaurants type list
    $arFilterRestType3 = array("IBLOCK_ID" => 120, "ACTIVE" => "Y");
        $rsRestType = CIBlockElement::GetList($arSort, $arFilterRestType3, false, false, $arSelect);
        while($arRestType3 = $rsRestType->GetNext())
            $arResult["KITCHEN"][] = $arRestType3;
			
			// get restaurants type list
    $arFilterRestType4 = array("IBLOCK_ID" => 80, "ACTIVE" => "Y");
        $rsRestType = CIBlockElement::GetList($arSort, $arFilterRestType4, false, false, $arSelect);
        while($arRestType4 = $rsRestType->GetNext())
            $arResult["CAUSE"][] = $arRestType4;
    //v_dump($arResult["RESTAURANTS_TYPE"]);

	$this->SetResultCacheKeys(array(
		"ID",
		"IBLOCK_ID",
		"NAME",
        "CUISINES",
        "AVERAGE_BILL",
        "SUBWAY",
        "RESTAURANTS_TYPE"
	));

	$this->IncludeComponentTemplate();
}
?>