<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;




if($this->StartResultCache(false, array($arNavigation, $arParams["ELEMENT_ID"], $arParams["IBLOCK_ID"],$arParams["PARENT_SECTION"])))
{
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	//редактируем уже существующий элемент
	if($arParams["ELEMENT_ID"]>0){
		$res = CIBlockElement::GetByID($arParams["ELEMENT_ID"]);
		if($ob = $res->GetNextElement()){
			$arResult = $ob->GetFields();
			$arResult["PROPERTIES"] = $ob->GetProperties();

			
		}
	
	
	
	
	}
	
	//Нужно будет закэшировать эти штуки	
	//Получаем список разделов в секции для выбора родительского раздела
	$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$arParams["PARENT_SECTION"]);
  	$db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), $arFilter, true);
	while($ar_result = $db_list->GetNext()){
    	$arResult["SECTIONS"][]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"]);
  	}
		
		
		
	//теперь смотрим, если в нфоблоке есть свойства типа список или привязка к элементам инфоблока, то забираем их значения
	//Привязка к элементам
	$properties = CIBlockProperty::GetList(Array("name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"],"PROPERTY_TYPE"=>"E"));
	while ($prop_fields = $properties->GetNext()){
  		$VALS=array();
                $ar_result = array();
                $VALUES = array();
  		//Теперь нужно выбрать все элементы из инфоблока
  		$arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"]);
		$res_prl = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_prl);
		while($ar_result = $res_prl->GetNext()){
                    $iblock_type_as = $ar_result["IBLOCK_TYPE_ID"];
                    $ar_result["NAME"] = str_replace("`","",$ar_result["NAME"]);
                    $ar_result["NAME"] = str_replace("'","",$ar_result["NAME"]);
                    $ar_result["NAME"] = str_replace('"',"",$ar_result["NAME"]);
                    $sec = "";
                    $res = CIBlockSection::GetByID($ar_result["IBLOCK_SECTION_ID"]);
                    if($ar_res = $res->GetNext())
                        $sec = " [".$ar_res['NAME']."]";
                    $VALUES[]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"].$sec);
		}
//                  if ($iblock_type_as=="catalog")
//                  {
//                    //если привязка к ресторану, то нужно запросить еще и фирмы
//                    $arIIIB = getArIblock("firms", CITY_ID);
//
//                    $arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>$arIIIB["ID"]);
//                    $res_prl = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_prl, false);
//                    while($ob_prl = $res_prl->GetNext()){
//                            $VALUES[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"]." [Фирмы]");
//                    }
//
//                }
  		$arResult["PROPS"][$prop_fields["CODE"]]=array("ID"=>$prop_fields["ID"], "NAME"=>$prop_fields["NAME"], "LIST"=>$VALUES);	
	} 
	
	
	$properties = CIBlockProperty::GetList(Array("name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"],"PROPERTY_TYPE"=>"L"));
	while ($prop_fields = $properties->GetNext()){
  		$VALS=array();
  		//var_dump($prop_fields);
  		
  		$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>$prop_fields["CODE"]));
		while($ob_prl = $property_enums->GetNext()){
			$VALS[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["VALUE"]);
		}
  		$arResult["PROPS"][$prop_fields["CODE"]]=array("ID"=>$prop_fields["ID"], "NAME"=>$prop_fields["NAME"], "LIST"=>$VALS);	
	} 
	
	
	
	$arResult["PROPS"]["ACTIVE"]=array("LIST"=>array(
		array("ID"=>"Y", "NAME"=>"Да"), 
		array("ID"=>"N", "NAME"=>"Нет") 
	));
	
	
	//var_dump($arResult);
		

	$this->SetResultCacheKeys(array(
		"ID",
		"IBLOCK_ID",
		"NAV_CACHED_DATA",
		"NAME",
		"IBLOCK_SECTION_ID",
		"IBLOCK",
		"LIST_PAGE_URL", 
		"~LIST_PAGE_URL",
		"SECTION",
		"PROPERTIES",
		"SORT"
	));

	$this->IncludeComponentTemplate();

}
//var_dump($arParams);
?>