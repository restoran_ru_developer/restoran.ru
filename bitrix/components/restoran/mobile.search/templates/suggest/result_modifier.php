<?
CModule::IncludeModule("iblock");
if (is_array($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            //$el["PROPERTIES"] = $obElement->GetProperties();    
            if ($el["IBLOCK_TYPE_ID"]=="rest_group")
            {
                $el["SECTION_NAME"] = "Ресторанная группа";
                $el["DETAIL_PAGE_URL"] = "/".CITY_ID."/catalog/group/".$el["CODE"]."/";
            }
            elseif ($el["IBLOCK_TYPE_ID"]=="closed_rubrics_rest")
            {
                $el["SECTION_NAME"] = "Рубрика";
                $el["DETAIL_PAGE_URL"] = "/".CITY_ID."/catalog/restaurants/rubrics/".$el["CODE"]."/";
            }
            else
            {
                $res = CIBlockSection::GetByID($el["IBLOCK_SECTION_ID"]);
                if ($ar = $res->Fetch())
                    $el["SECTION_NAME"] = $ar["NAME"];
            }
            $arItem["ELEMENT"] = $el;
         }
    endforeach;?>
<?endif;?>
