<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
        $arParams["CACHE_TYPE"] = "Y";
	$arParams["CACHE_TIME"] = 0;
}
global $USER;
$q = $_REQUEST["q"]; 
$page = $_REQUEST["page"];
if (!$page||$page==1)
    $page = 0;
$n = $page*$arParams["COUNT"];

if (!$arParams["IBLOCK_ID"])
    $arRestIB = getArIblock("catalog", CITY_ID);
else
    $arRestIB["ID"] = (int)$arParams["IBLOCK_ID"];

if ($arParams["SECTION"])
{
    $obCache = new CPHPCache;
    $life_time = 60*60*24;
    $cache_id = $arParams['SECTION']."1".CITY_ID.$arRestIB["ID"];
    if($obCache->InitCache($life_time, $cache_id, "/")) {
        $vars = $obCache->GetVars();
        $id = $vars["ID"];
    } else {
        global $DB;                
        $sql = "SELECT ID FROM b_iblock_section            
                WHERE CODE='".$DB->ForSql($arParams['SECTION'])."' AND IBLOCK_ID=".$DB->ForSql($arRestIB["ID"]);
        $res = $DB->Query($sql);
        $r = $res->Fetch();
        $id = $r["ID"];
    }
    if($obCache->StartDataCache()) {
        $obCache->EndDataCache(array(
            "ID"    => $id
        ));
    }
    $arResult["SECTION_ID"] = (int)$id;
}

if($this->StartResultCache(false, array($page,$q,CITY_ID)))
{
    
    CModule::IncludeModule("iblock");
    if ($q)
    {
        $q = ToLower($q);        
        $arSelect = Array("ID","NAME","DETAIL_PAGE_URL");
        $iblock = $DB->ForSQL($arRestIB["ID"]);        
        $q = $DB->ForSQL($q);
        $count = $DB->ForSQL($arParams["COUNT"]);
        $rest_sql = "";
        if ($arResult["SECTION_ID"])
            $sect = " AND IBLOCK_SECTION_ID = ".$arResult["SECTION_ID"]." ";
        else
            $sect = "";
                 
        $res = $DB->Query('(SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' '.$sect.'  AND NAME LIKE "'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' '.$sect.' AND NAME LIKE "%'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' '.$sect.' AND TAGS LIKE "%'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.') 
                        UNION (SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' '.$sect.' AND TAGS LIKE "'.$q.'%" ORDER BY NAME ASC LIMIT '.$count.')                         
                            LIMIT '.$count);

    }
    elseif ($arParams["MOB"]=="Y")
    {
        $iblock = $DB->ForSQL($arRestIB["ID"]);
        $count = $DB->ForSQL($arParams["COUNT"]);
        $res = $DB->Query('SELECT ID,NAME,IBLOCK_ID FROM b_iblock_element WHERE ACTIVE = "Y" AND IBLOCK_ID='.$iblock.' ORDER BY NAME ASC LIMIT '.$n.','.$count);                
    }
    if ($res)
    {
        while($ar = $res->Fetch())
        {	                    
            if ($arParams["NO_SLEEP"]=="Y")
            {
                $sleep = "";
                //Проверяем спящего
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array(), Array("CODE"=>"sleeping_rest"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sleep = "Да";
                    }
                    /*$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$ar["IBLOCK_ID"], "ID"=>$ar_props["VALUE"]));
                    if($enum_fields = $property_enums->GetNext())
                    {
                        $sleep = $enum_fields["VALUE"];
                    } */           
                }
                if ($sleep!="Да")
                    $arResult["ITEMS"][] = $ar;
            }
            elseif($arParams["NO_MOBILE"]=="Y"){
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array(), Array("CODE"=>"no_mobile"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE_ENUM"]=='Да'){
                        continue;
                    }
                    else {
                        $arResult["ITEMS"][] = $ar;
                    }
                }
                else {
                    $arResult["ITEMS"][] = $ar;
                }
            }
            else
            {
                $arResult["ITEMS"][] = $ar;
            }
        }    
    }
    $this->SetResultCacheKeys(array(
            "ITEMS",
    ));    
    $this->IncludeComponentTemplate();
}
?>