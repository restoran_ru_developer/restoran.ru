<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//if (count($arResult["ITEMS"])>0)
$items = array();
$page = $arResult["NAV_RESULT"]->PAGEN;
$all = $arResult["NAV_RESULT"]->NavPageCount;
if ($page<=$all)
{
    foreach ($arResult["ITEMS"] as &$item)
    {

        $item["PREVIEW_PICTURE"] = $item["PREVIEW_PICTURE"]["src"];    
        unset($item["PROPERTIES2"]);
        unset($item["DETAIL_TEXT"]);
        unset($item["~DETAIL_TEXT"]);
        unset($item["~SEARCHABLE_CONTENT"]);
        unset($item["SEARCHABLE_CONTENT"]);
        unset($item["IBLOCK_EXTERNAL_ID"]);
        unset($item["~IBLOCK_EXTERNAL_ID"]);
        unset($item["~ID"]);
        unset($item["TIMESTAMP_X"]);
        unset($item["~TIMESTAMP_X"]);
        unset($item["TIMESTAMP_X_UNIX"]);
        unset($item["~TIMESTAMP_X_UNIX"]);
        unset($item["~MODIFIED_BY"]);
        unset($item["MODIFIED_BY"]);
        unset($item["~DATE_CREATE_UNIX"]);
        unset($item["DATE_CREATE_UNIX"]);
        unset($item["~CREATED_BY"]);
        unset($item["CREATED_BY"]);
        unset($item["~IBLOCK_ID"]);
        unset($item["~IBLOCK_SECTION_ID"]);
        unset($item["~ACTIVE"]);
        unset($item["~ACTIVE_FROM"]);
        unset($item["~ACTIVE_TO"]);
        unset($item["~ACTIVE_TO"]);
        unset($item["~DATE_ACTIVE_FROM"]);
        unset($item["~DATE_ACTIVE_TO"]);
        unset($item["~SORT"]);
        unset($item["~NAME"]);
        unset($item["~PREVIEW_PICTURE"]);
        unset($item["~PREVIEW_TEXT"]);
        unset($item["~DETAIL_PICTURE"]);
        unset($item["~DETAIL_TEXT_TYPE"]);
        unset($item["DETAIL_TEXT_TYPE"]);
        unset($item["WF_STATUS_ID"]);
        unset($item["~WF_STATUS_ID"]);
        unset($item["WF_PARENT_ELEMENT_ID"]);
        unset($item["~WF_PARENT_ELEMENT_ID"]);
        unset($item["WF_LAST_HISTORY_ID"]);
        unset($item["~WF_LAST_HISTORY_ID"]);

        unset($item["WF_NEW"]);
        unset($item["~WF_NEW"]);
        unset($item["LOCK_STATUS"]);
        unset($item["~LOCK_STATUS"]);
        unset($item["WF_LOCKED_BY"]);
        unset($item["~WF_LOCKED_BY"]);
        unset($item["WF_DATE_LOCK"]);
        unset($item["~WF_DATE_LOCK"]);
        unset($item["WF_COMMENTS"]);
        unset($item["~WF_COMMENTS"]);
        unset($item["IN_SECTIONS"]);
        unset($item["~IN_SECTIONS"]);
        unset($item["SHOW_COUNTER"]);
        unset($item["~SHOW_COUNTER"]);
        unset($item["SHOW_COUNTER_START"]);
        unset($item["~SHOW_COUNTER_START"]);
        unset($item["SHOW_COUNTER_START"]);
        unset($item["~SHOW_COUNTER_START"]);
        unset($item["~CODE"]);
        unset($item["~TAGS"]);
        unset($item["XML_ID"]);
        unset($item["~XML_ID"]);
        unset($item["EXTERNAL_ID"]);
        unset($item["~EXTERNAL_ID"]);
        unset($item["TMP_ID"]);
        unset($item["~TMP_ID"]);
        unset($item["USER_NAME"]);
        unset($item["~USER_NAME"]);
        unset($item["LOCKED_USER_NAME"]);
        unset($item["~LOCKED_USER_NAME"]);
        unset($item["CREATED_USER_NAME"]);
        unset($item["~CREATED_USER_NAME"]);
        unset($item["LANG_DIR"]);
        unset($item["~LANG_DIR"]);
        unset($item["LID"]);
        unset($item["~LID"]);            
        unset($item["~IBLOCK_NAME"]);            
        unset($item["~PREVIEW_TEXT_TYPE"]);            
        unset($item["PREVIEW_TEXT_TYPE"]);            
        unset($item["~DETAIL_PAGE_URL"]);            
        unset($item["~LIST_PAGE_URL"]);            
        unset($item["~DATE_CREATE"]);            
        unset($item["~CREATED_DATE"]);            
        unset($item["~BP_PUBLISHED"]);            
        unset($item["BP_PUBLISHED"]);            
        unset($item["~IBLOCK_TYPE_ID"]);            
        unset($item["~IBLOCK_TYPE_ID"]);            
        unset($item["~IBLOCK_CODE"]);            
        @$item["ORDER_PHONE"] = "+7".str_replace(" ","",trim(strip_tags(file_get_contents("http://restoran.ru/bitrix/templates/main/include_areas/top_phones_".$_REQUEST["CITY_ID"].".php")))); 
        $items[] = $item;    
    }
}
echo json_encode($items);
?>