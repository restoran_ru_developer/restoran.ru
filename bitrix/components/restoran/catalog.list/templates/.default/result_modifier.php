<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) 
{  
    if ($key==0)
    {
        if ($arItem["PREVIEW_PICTURE"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>370, 'height'=>240), BX_RESIZE_IMAGE_EXACT, true);
        elseif($arItem["PROPERTIES"]["photos"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PROPERTIES"]["photos"][0], array('width'=>370, 'height'=>240), BX_RESIZE_IMAGE_EXACT, true);
    }
    else
    {
        if ($arItem["PREVIEW_PICTURE"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>232, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);
        elseif($arItem["PROPERTIES"]["photos"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PROPERTIES"]["photos"][0], array('width'=>232, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);
    }
    if (!$arItem["PREVIEW_PICTURE"]["src"])
        $arItem["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
}
/*$arReviewsIB = getArIblock("reviews", CITY_ID);
foreach($arResult["ITEMS"] as $key=>$arItem) {  
    if ($arItem["PROPERTIES"]["reviews_bind"]["VALUE"])
        $arResult["ITEMS"][$key]["REVIEWS"] = getReviewsRatio($arReviewsIB["ID"],$arItem["PROPERTIES"]["reviews_bind"]["VALUE"]);
 }*/
 
?>