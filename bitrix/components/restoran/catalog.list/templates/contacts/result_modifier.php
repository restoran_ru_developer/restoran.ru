<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) 
{  
    if ($arItem["PROPERTIES"]["users"])
    {
        foreach ($arItem["PROPERTIES"]["users"] as $user)
        {
            $res = CUser::GetByID($user);
            while ($ar =$res->GetNext())
            {
                if ($ar["PERSONAL_PHOTO"])
                    $ar["PERSONAL_PHOTO"] = CFile::ResizeImageGet($ar["PERSONAL_PHOTO"], array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_EXACT, false, Array());
                elseif (!$ar["PERSONAL_PHOTO"]&&$ar["GENDER"]=="M")
                    $ar["PERSONAL_PHOTO"]["src"] = '/tpl/images/noname/man_nnm.png';
                elseif (!$ar["PERSONAL_PHOTO"]&&$ar["GENDER"]=="F")
                    $ar["PERSONAL_PHOTO"]["src"] = '/tpl/images/noname/woman_nnm.png';
                else
                    $ar["PERSONAL_PHOTO"]["src"] = '/tpl/images/noname/unisx_nnm.png';
                if (!substr_count($ar["PERSONAL_PHONE"],"+7")&&$ar["PERSONAL_PHONE"])
                {
                    $ar["PERSONAL_PHONE"] = "+7 (".substr($ar["PERSONAL_PHONE"], 0,3).") ".substr($ar["PERSONAL_PHONE"], 3,3)."-".substr($ar["PERSONAL_PHONE"], 6,2)."-".substr($ar["PERSONAL_PHONE"], 8,2);
                }
                $arItem["USERS"][] = $ar;
            }
        }
        
    }
}
/*$arReviewsIB = getArIblock("reviews", CITY_ID);
foreach($arResult["ITEMS"] as $key=>$arItem) {  
    if ($arItem["PROPERTIES"]["reviews_bind"]["VALUE"])
        $arResult["ITEMS"][$key]["REVIEWS"] = getReviewsRatio($arReviewsIB["ID"],$arItem["PROPERTIES"]["reviews_bind"]["VALUE"]);
 }*/
?>