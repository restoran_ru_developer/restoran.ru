<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script>
    $(document).ready(function(){        
        $("#comments tr").hover(function(){            
            $(this).find(".answer").css("visibility","visible");
        }, function(){
            $(this).find(".answer").css("visibility","hidden");
        } );
    });
    function answer_me(a)
    {
        var b = a.length+2;
        $("#review").html(a+", ");
        $("#review").focus();
        document.getElementById("review").setSelectionRange(b, b);
    }
</script>
<div id="comments" >    
    <div style="margin-left:85px;">
        <div class="dotted" style="margin:20px 0px"></div>
        <?if (sizeof($arResult["COMMENTS"])>0):?>
            <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="80" valign="top">
                            <div class="ava">
                                <?if($commVal["AVATAR"]):?>
                                    <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" />
                                <?endif?>
                            </div>
                        </td>
                        <td width="150"  valign="top">
                            <span class="name"><a class="another" href="/users/id<?=$commVal["CREATED_BY"]?>/"><?=$commVal["NAME"]?></a></span>
                            <span class="comment_date"><?=$commVal["FORMATED_DATE_1"]?></span> <?=$commVal["FORMATED_DATE_2"]?>
                        </td>
                        <td  valign="top" class="hover_answer">
                            <i><?=$commVal["PREVIEW_TEXT"]?></i>
                            <div class="answer" style="visibility: hidden"><br /><a href="#review" class="another anchor" onclick="answer_me('<?=$commVal["NAME"]?>'); return false;">Ответить</a></div>
                        </td>
                    </tr>
                </table>                
                <?if ($commVal!=end($arResult["COMMENTS"])):?>
                <div class="dotted" style="margin:20px 0px"></div>
                <?endif;?>
            <?endforeach?>
        <?else:?>
            <?=GetMessage("NO_COMMENTS")?>
        <?endif;?>
    </div>
    <?
    $APPLICATION->IncludeComponent(
                "restoran:comments_add",
                ($arParams["ADD_COMMENT_TEMPLATE"])?$arParams["ADD_COMMENT_TEMPLATE"]:"",
                Array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                        "IS_SECTION" => $arParams["IS_SECTION"],                        
                ),false
    );?>
</div>