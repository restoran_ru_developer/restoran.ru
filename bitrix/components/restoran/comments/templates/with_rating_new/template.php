<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?if(count($arResult["COMMENTS"])):?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<script>
    function change_big_modal2_next2()
    {
            $(".big_modal2 img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().next().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().next().find("img").attr("alt");                                            
                    $(".big_modal2 img").attr("alt",$("#"+$(this).attr("alt")).parent().next().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("alt");                                            
                    $(".big_modal2 img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("id"));                    
                }
                $(".big_modal2 img").attr("src",img.src);
                showOverflow();
                //$(".big_modal2 img").css({"width":"0px","height":"0px"});
                $(".big_modal2 img").animate({"width":+img.width,"height":+img.height},100,function(){
                    $(".big_modal2").animate({"width":+img.width,"height":+img.height,"top":"="+eval($("window").height()/2-img.height/2),"left":"="+eval(($("window").width()/2)-(img.width/2))},300);
                });                                            
                $(".big_modal2 img").fadeIn(300); 

            });                                            

    }
    function change_big_modal2_prev2()
    {
            $(".big_modal2 img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().prev().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().prev().find("img").attr("alt");                                            
                    $(".big_modal2 img").attr("alt",$("#"+$(this).attr("alt")).parent().prev().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("alt");                                            
                    $(".big_modal2 img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("id"));                    
                }
                $(".big_modal2 img").attr("src",img.src);
                showOverflow();
                //$(".big_modal2 img").css({"width":"0px","height":"0px"});
                $(".big_modal2 img").animate({"width":+img.width,"height":+img.height},100,function(){
                    $(".big_modal2").animate({"width":+img.width,"height":+img.height,"top":"="+eval($("window").height()/2-img.height/2),"left":"="+eval(($("window").width()/2)-(img.width/2))},300);
                });                                            
                $(".big_modal2 img").fadeIn(300); 

            });                                            

    }
    $(document).ready(function(){
        //$("#photogalery").galery({});
        $("body").append("<div class=big_modal2><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        setCenter($(".big_modal2"));
        $(".big_modal2 .slider_left").on("click",function(){
            change_big_modal2_prev2();
        });
        $(".big_modal2 .slider_right").on("click",function(){
            change_big_modal2_next2();
        });
        $(".big_modal2").hover(function(){
            $(".big_modal2 .slider_left").fadeIn(300);
            $(".big_modal2 .slider_right").fadeIn(300);
        },function(){
            $(".big_modal2 .slider_left").fadeOut(300);
            $(".big_modal2 .slider_right").fadeOut(300);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_modal2 img").attr("src",img.src);
            $(".big_modal2 img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_modal2 img").css({"width":"0px","height":"0px"});
            $(".big_modal2 img").animate({"width":+img.width,"height":+img.height},300);
            $(".big_modal2").animate({"width":+img.width,"height":+img.height,"top":"-="+eval(img.height/2),"left":"-="+eval(img.width/2)},300);
                $(".big_modal2").fadeIn(300);
        });
        $("#system_overflow").click(function(){
            $(".big_modal2").fadeOut();
            hideOverflow();
            $(".big_modal2").css("width","0px");
            $(".big_modal2").css("height","0px");
            setCenter($(".big_modal2"));
            $(".big_modal2 img").css({"width":"0px","height":"0px"});
        });
    });
</script>
<div id="comments">
    <?foreach($arResult["COMMENTS"] as $key => $arItem):?>
        <div class="left left_block">            
            <div class="left rest_img">
                <div class="ava">
                    <img src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" width="64" />
                </div>
            </div>
            <div class="left" style="line-height:24px;">
                <span class="name"><a class="another" href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["NAME"]?></a></span><br />
                <span class="comment_date"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <br />
                <div class="small_rating">
                    <?for($i=1;$i<=$arItem["DETAIL_TEXT"];$i++):?>
                        <div class="small_star_a" alt="<?=$i?>"></div>
                    <?endfor;?>
                    <?for($i;$i<=5;$i++):?>
                        <div class="small_star" alt="<?=$i?>"></div>
                    <?endfor;?>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right right_block">
            <p>
                <span class="preview_text<?=$arItem["ID"]?>">
                    <?=$arItem["PREVIEW_TEXT2"]?>
                    <?if (strlen($arItem["PREVIEW_TEXT2"])!=strlen($arItem["PREVIEW_TEXT"])):?>
                        <a onclick="$('.preview_text<?=$arItem["ID"]?>').toggle()" class="jsanother">еще</a>
                    <?endif;?>
                </span>
                <?if (strlen($arItem["PREVIEW_TEXT2"])!=strlen($arItem["PREVIEW_TEXT"])):?>
                    <span class="preview_text<?=$arItem["ID"]?>" style="display:none;">
                        <?=$arItem["PREVIEW_TEXT"]?> <a onclick="$('.preview_text<?=$arItem["ID"]?>').toggle()" class="jsanother">скрыть</a>
                    </span>
                <?endif;?>
            </p>
            <?if ($arItem["PROPERTIES"]["pluss"]["TEXT"]):?>
                <b>ДОСТОИНСТВА:</b><br />
                <p>
                    <span class="plus<?=$arItem["ID"]?>">
                        <?=$arItem["PROPERTIES"]["pluss"]["TEXT2"]?>
                        <?if (strlen($arItem["PROPERTIES"]["pluss"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["pluss"]["TEXT"])):?>
                            <a onclick="$('.plus<?=$arItem["ID"]?>').toggle()" class="jsanother">еще</a>
                        <?endif;?>
                    </span>
                    <?if (strlen($arItem["PROPERTIES"]["pluss"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["pluss"]["TEXT"])):?>
                        <span class="plus<?=$arItem["ID"]?>" style="display:none;">
                            <?=$arItem["PROPERTIES"]["pluss"]["TEXT"]?> <a onclick="$('.plus<?=$arItem["ID"]?>').toggle()" class="jsanother">скрыть</a>
                        </span>
                    <?endif;?>
                </p>                
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["minuss"]["TEXT"]):?>
                <b>НЕДОСТАТКИ:</b><br />
                <p>
                    <span class="minus<?=$arItem["ID"]?>">
                        <?=$arItem["PROPERTIES"]["minuss"]["TEXT2"]?>
                        <?if (strlen($arItem["PROPERTIES"]["minuss"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["minuss"]["TEXT"])):?>
                            <a onclick="$('.minus<?=$arItem["ID"]?>').toggle()" class="jsanother">еще</a>
                        <?endif;?>
                    </span>
                    <?if (strlen($arItem["PROPERTIES"]["minuss"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["minuss"]["TEXT"])):?>
                        <span class="minus<?=$arItem["ID"]?>" style="display:none;">
                            <?=$arItem["PROPERTIES"]["minuss"]["TEXT"]?> <a onclick="$('.minus<?=$arItem["ID"]?>').toggle()" class="jsanother">скрыть</a>
                        </span>
                    <?endif;?>
                </p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["video"][0]):?>
                <div class="grey45" style="margin-bottom:10px;">
                    <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.CFile::GetPath($arItem["PROPERTIES"]["video"][0])?>"></a> 
                    <script>
                        flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                            clip: {
                                    autoPlay: false, 
                                    autoBuffering: true
                            }
                        });
                    </script>
                </div>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                <div class="grey45 photog">
                    <?foreach($arItem["PROPERTIES"]["photos"] as $key=>$photo):?>
                        <div class="left <?=($key%3==2)?"end":""?>"><img id="photo<?=$photo["ID"]?>" src="<?=$photo["SRC"]?>" alt="<?=$photo["ORIGINAL_SRC"]?>" width="137" /></div>
                    <?endforeach;?>
                    <div class="clear"></div>
                </div>                
            <?endif;?>
        </div>
        <div class="clear"></div>
        <div class="more">
            <div class="clear"></div>
            <div class="left"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Ответить</a></div>
            <div class="left">Комментарии:(<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=($arItem["PROPERTIES"]["COMMENTS"])?$arItem["PROPERTIES"]["COMMENTS"]:"0"?></a>)</div>            
            <div class="clear"></div>
        </div>
        <?if (end($arResult["COMMENTS"])!=$arItem):?>
            <div class="black_hr"></div>            
        <?endif;?>
    <?endforeach?> 
            <Br /><Br />
    <?if ($arParams["URL"]):?>
            <div align="right"><a class="uppercase" href="<?=$arParams["URL"]?>"><?=GetMessage("ALL_REVIEWS")?></a></div>
    <?endif;?>
<?else:?>
            <br />
       <?=GetMessage("NO_COMMENTS")?>     
            <br />
<?endif;?>
    <?
    $APPLICATION->IncludeComponent(
                "restoran:comments_add",
                ($arParams["ADD_COMMENT_TEMPLATE"])?$arParams["ADD_COMMENT_TEMPLATE"]:"",
                Array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                        "IS_SECTION" => $arParams["IS_SECTION"],                        
                ),false
    );?>
</div>
<!--<div id="comments">
    <?if(count($arResult["COMMENTS"])):?>
        <script>
            $(document).ready(function(){        
                $("#comments tr").hover(function(){            
                    $(this).find(".answer").css("visibility","visible");
                }, function(){
                    $(this).find(".answer").css("visibility","hidden");
                } );
            });
            function answer_me(a)
            {
                var b = a.length+2;
                $("#review").html(a+", ");
                $("#review").focus();
                document.getElementById("review").setSelectionRange(b, b);
            }
        </script>
        <ul>
            <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
            <li>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="80" valign="top">
                            <div class="ava">
                                <?if($commVal["AVATAR"]):?>
                                    <img src="<?=$commVal["AVATAR"]["src"]?>" width="64" alt="<?=$commVal["NAME"]?>" />
                                <?endif?>
                            </div>
                        </td>
                        <td width="150"  valign="top">
                            <span class="name"><?=$commVal["NAME"]?></span>
                            <span class="comment_date"><?=$commVal["FORMATED_DATE_1"]?></span> <?=$commVal["FORMATED_DATE_2"]?>
                        </td>
                        <td  valign="top" class="hover_answer">
                            <div class="rating left" style="padding-bottom:0px;">
                                <?for($p=0;$p<$commVal["DETAIL_TEXT"];$p++):?>
                                    <div class="small_star_a"></div>                                        
                                <?endfor?>
                                <?for($p;$p<5;$p++):?>
                                    <div class="small_star"></div>
                                <?endfor;?>
                            </div>                        
                            <div class="clear"></div>
                            <i><?=$commVal["PREVIEW_TEXT"]?></i>                        
                            <div class="answer" style="visibility: hidden"><br /><a href="#review" class="another anchor" onclick="answer_me('<?=$commVal["NAME"]?>')">Ответить</a></div>
                        </td>
                    </tr>
                </table>            
            </li>
            <?endforeach?>
        </ul>
        <?if ($arParams["URL"]):?>
            <div align="right"><a class="uppercase" href="<?=$arParams["URL"]?>"><?=GetMessage("ALL_REVIEWS")?></a></div>
        <?endif;?>
        <?else:?>
            <?=GetMessage("NO_COMMENTS")?>
    <?endif;?>            
    
</div>-->