<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
global $child;
global $params;
$params = $arParams;
$child = $arResult["CHILDS"];
function getChilds($id)
{
    global $child;
    global $params;
    global $APPLICATION;
    global $USER;
    foreach ($child[$id] as $commKey=>$commVal):            
?>
        <div class="commentt">
            <p><?=$commVal["PREVIEW_TEXT"]?></p>
            <div class="left" style="width:50%">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="36" valign="top">
                            <div class="comment_ava">
                                <?if($commVal["AVATAR"]):?>
                                    <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" width="36" />
                                <?endif?>
                            </div>
                        </td>
                        <td valign="top">
                            <a class="another" href="/users/id<?=$commVal["CREATED_BY"]?>/"><?=($commVal["NICKNAME"])?$commVal["NICKNAME"]:$commVal["USER_NAME"]?></a><br />
                            <?=$commVal["FORMATED_DATE_1"]?> <?=$commVal["FORMATED_DATE_2"]?>
                        </td>
                    </tr>
                </table> 
            </div>       
            <div class="right">
                <input type="button" class="grey_button" value="Ответить" onclick="show_close_textarea(<?=$commVal["ID"]?>)"/>
            </div>
            <div class="clear"></div>
            <div class="review_comment_add review_comment_add<?=$commVal["ID"]?>" style="display:none;">
                <?
                    $APPLICATION->IncludeComponent(
                                "restoran:comments_add",
                                "review_comment",
                                Array(
                                        "IBLOCK_TYPE" => $params["IBLOCK_TYPE"],
                                        "IBLOCK_ID" => $params["IBLOCK_ID"],
                                        "ELEMENT_ID" => $params["ELEMENT_ID"],
                                        "IS_SECTION" => "N", 
                                        "PARENT" => $commVal["ID"]
                                ),false
                );?>
            </div>   
            <?if ($commVal!=end($arResult["COMMENTS"])):?>
                    <div class="dotted" style="margin:15px 0px 10px 0px"></div>
                <?endif;?>   
            <?if ($child[$commVal["ID"]])
                    getChilds($commVal["ID"]);
            ?>
        </div>
<?
    endforeach;
}
?>
<div id="comments" >    
    <?if (sizeof($arResult["COMMENTS"])>0):?>
        <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
            <div class="commentt">
                <p><?=HtmlToTxt($commVal["PREVIEW_TEXT"])?></p>
                <div class="left" style="width:50%">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="36" valign="top">
                                <div class="comment_ava">
                                    <?if($commVal["AVATAR"]):?>
                                        <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" width="36" />
                                    <?endif?>
                                </div>
                            </td>
                            <td valign="top">
                                <a class="another" href="/users/id<?=$commVal["CREATED_BY"]?>/"><?=($commVal["NICKNAME"])?$commVal["NICKNAME"]:$commVal["USER_NAME"]?></a><br />
                                <?=$commVal["FORMATED_DATE_1"]?> <?=$commVal["FORMATED_DATE_2"]?>
                            </td>
                        </tr>
                    </table> 
                </div>     
                <div class="right">
                    <input type="button" class="grey_button" value="Ответить" onclick="show_close_textarea(<?=$commVal["ID"]?>)"/>
                </div>
                <div class="clear"></div>
                <div class="review_comment_add review_comment_add<?=$commVal["ID"]?>" style="display:none;">
                    <?
                        $APPLICATION->IncludeComponent(
                                    "restoran:comments_add",
                                    "review_comment",
                                    Array(
                                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                                            "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                                            "IS_SECTION" => "N", 
                                            "PARENT" => $commVal["ID"]
                                    ),false
                    );?>
                </div>
                <?//if ($commVal!=end($arResult["COMMENTS"])):?>
                    <div class="dotted" style="margin:15px 0px 10px 0px"></div>
                <?//endif;?>   
                <?
                if ($arResult["CHILDS"][$commVal["ID"]])
                    getChilds($commVal["ID"])
                ?>                                 
            </div>
        <?endforeach?>
    <?else:?>
        <?=GetMessage("NO_COMMENTS")?>
    <?endif;?>
</div>