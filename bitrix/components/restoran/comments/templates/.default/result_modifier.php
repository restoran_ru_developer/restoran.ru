<?
foreach($arResult["COMMENTS"] as $key=>$arItem) {
    // explode date
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arResult["COMMENTS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    $arUser = $rsUser->Fetch();
    $arResult["COMMENTS"][$key]["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 64, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);    
    if (!$arResult["COMMENTS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="M")
        $arResult["COMMENTS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/man_nnm.png";
    elseif (!$arResult["COMMENTS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="F")
        $arResult["COMMENTS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/woman_nnm.png";
    elseif (!$arResult["COMMENTS"][$key]["AVATAR"]["src"])
        $arResult["COMMENTS"][$key]["AVATAR"]['src'] = "/tpl/images/noname/unisx_nnm.png";       
}
?>