<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script>
    $(document).ready(function(){        
        $("#comments tr").hover(function(){            
            $(this).find(".answer").show();
        }, function(){
            $(this).find(".answer").hide();
        } );
    });
    function answer_me(a)
    {
        var b = a.length+2;
        $("#review").html(a+", ");
        $("#review").focus();
        document.getElementById("review").setSelectionRange(b, b);
    }
</script>
<div id="comments">
    <?if (sizeof($arResult["COMMENTS"])>0):?>
    <ul>
        <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
        <li>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td width="80" valign="top">
                        <div class="ava">
                            <?if($commVal["AVATAR"]):?>
                                <img src="<?=$commVal["AVATAR"]["src"]?>" width="64" alt="<?=$commVal["NAME"]?>" />
                            <?endif?>
                        </div>
                    </td>
                    <td width="150"  valign="top">
                        <span class="name"><a class="another" href="/users/id<?=$commVal["CREATED_BY"]?>/"><?=$commVal["NAME"]?></a></span>
                        <span class="comment_date"><?=$commVal["FORMATED_DATE_1"]?></span> <?=$commVal["FORMATED_DATE_2"]?>
                    </td>
                    <td  valign="top" class="hover_answer">
                        <i><?=$commVal["PREVIEW_TEXT"]?></i>
                        <div class="answer" style="display: none"><br /><a href="#review" class="another anchor" onclick="answer_me('<?=$commVal["NAME"]?>'); return false;">Ответить</a></div>
                    </td>
                </tr>
            </table>
            <?/*if($commVal["CHILD_COMMENTS"]):?>
                <ul>
                    <?foreach($commVal["CHILD_COMMENTS"] as $childComm):?>
                    <li>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80"   valign="top">
                                    <div class="ava">
                                        <?if($commVal["AVATAR"]):?>
                                            <img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" />
                                        <?endif?>
                                    </div>
                                </td>
                                <td width="150"  valign="top">
                                    <span class="name"><?=$childComm["NAME"]?></span>
                                    <span class="comment_date"><?=$childComm["FORMATED_DATE_1"]?></span> <?=$childComm["FORMATED_DATE_2"]?>
                                </td>
                                <td  valign="top" class="hover_answer">
                                    <i><?=$childComm["PREVIEW_TEXT"]?></i>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <?endforeach?>
                </ul>
            <?endif*/?>
            <?if ($commVal!=end($arResult["COMMENTS"])):?>
            <div class="dotted"></div>
            <?endif;?>
        </li>
        <?endforeach?>
    </ul>
    <?else:?>
        <?=GetMessage("NO_COMMENTS")?>
    <?endif;?>
    <br />
    <?
    $APPLICATION->IncludeComponent(
                "restoran:comments_add",
                ($arParams["ADD_COMMENT_TEMPLATE"])?$arParams["ADD_COMMENT_TEMPLATE"]:"",
                Array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                        "IS_SECTION" => $arParams["IS_SECTION"],                        
                ),false
    );?>
</div>