<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div id="comments">
    <?if(count($arResult["COMMENTS"])):?>
        <script>
            $(document).ready(function(){        
                $("#comments tr").hover(function(){            
                    $(this).find(".answer").css("visibility","visible");
                }, function(){
                    $(this).find(".answer").css("visibility","hidden");
                } );
            });
            function answer_me(a)
            {
                var b = a.length+2;
                $("#review").html(a+", ");
                $("#review").focus();
                document.getElementById("review").setSelectionRange(b, b);
            }
        </script>
        <ul>
            <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
            <li>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="80" valign="top">
                            <div class="ava">
                                <?if($commVal["AVATAR"]):?>
                                    <img src="<?=$commVal["AVATAR"]["src"]?>" width="64" alt="<?=$commVal["NAME"]?>" />
                                <?endif?>
                            </div>
                        </td>
                        <td width="150"  valign="top">
                            <span class="name"><?=$commVal["NAME"]?></span>
                            <span class="comment_date"><?=$commVal["FORMATED_DATE_1"]?></span> <?=$commVal["FORMATED_DATE_2"]?>
                        </td>
                        <td  valign="top" class="hover_answer">
                            <div class="rating left" style="padding-bottom:0px;">
                                <?for($p=0;$p<$commVal["DETAIL_TEXT"];$p++):?>
                                    <div class="small_star_a"></div>                                        
                                <?endfor?>
                                <?for($p;$p<5;$p++):?>
                                    <div class="small_star"></div>
                                <?endfor;?>
                            </div>                        
                            <div class="clear"></div>
                            <i><?=$commVal["PREVIEW_TEXT"]?></i>                        
                            <div class="answer" style="visibility: hidden"><br /><a href="#review" class="another anchor" onclick="answer_me('<?=$commVal["NAME"]?>')">Ответить</a></div>
                        </td>
                    </tr>
                </table>            
            </li>
            <?endforeach?>
        </ul>
        <?if ($arParams["URL"]):?>
            <div align="right"><a class="uppercase" href="<?=$arParams["URL"]?>"><?=GetMessage("ALL_REVIEWS")?></a></div>
        <?endif;?>
        <?else:?>
            <?=GetMessage("NO_COMMENTS")?>
    <?endif;?>
    <?
    $APPLICATION->IncludeComponent(
                "restoran:comments_add",
                ($arParams["ADD_COMMENT_TEMPLATE"])?$arParams["ADD_COMMENT_TEMPLATE"]:"",
                Array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "ELEMENT_ID" => $arParams["ELEMENT_ID"],
                        "IS_SECTION" => $arParams["IS_SECTION"],                        
                ),false
    );?>
</div>