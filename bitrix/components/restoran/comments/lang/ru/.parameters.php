<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_ID"] = "ID инфоблока";
$MESS["SECTION_ID"] = "ID раздела с комментариями";
$MESS["ITEMS_LIMIT"] = "Кол-во выводимых элементов";
$MESS["IBLOCK_FIELD"] = "Выводимые поля";
$MESS["IBLOCK_PROPERTY"] = "Выводимые свойства";
$MESS["PROPERTY_PARENT_BIND_CODE"] = "Код свойства родительского элемента";
$MESS["PROPERTY_USER_BIND_CODE"] = "Код свойства привязки к пользователю";
?>