<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600;
}

if(!isset($arParams["PROPERTY_PARENT_BIND_CODE"])) {
	$arParams["PROPERTY_PARENT_BIND_CODE"] = 'PARENT_ID';
}

if(!isset($arParams["PROPERTY_USER_BIND_CODE"])) {
	$arParams["PROPERTY_USER_BIND_CODE"] = 'USER_BIND';
}
$arNavParams = array(
        "nPageSize" => $arParams["COUNT"],
);
$arNavigation = CDBResult::GetNavParams($arNavParams);
if(count($arParams["FIELD_CODE"]) <= 0)
    $arParams["FIELD_CODE"] = array(
        "ID",
        "IBLOCK_ID",
        "IBLOCK_SECTION_ID",
        "NAME",
        "ACTIVE_FROM",
        "DATE_CREATE",
        "DETAIL_PAGE_URL",
        "DETAIL_TEXT",
        "DETAIL_TEXT_TYPE",
        "PREVIEW_TEXT",
        "PREVIEW_TEXT_TYPE",
        "PREVIEW_PICTURE",
        "CREATED_BY",
        "PROPERTY_PARENT"
    );

if(count($arParams["PROPERTY_CODE"]) <= 0)
    $arParams["PROPERTY_CODE"] = Array();

if (!function_exists('get_url_mime_type')):
function get_url_mime_type($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_exec($ch);
        return curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
}
endif;     
if($this->StartResultCache(false, array($arParams["CACHE_NOTES"], $USER->GetGroups(), $arParams["ELEMENT_ID"], $arParams["IBLOCK_ID"], $arParams["SECTION_ID"])))
{
    if(!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError("IBLOCK_MODULE_NOT_INSTALLED");
        return false;
    }
    if ($arParams["IBLOCK_TYPE"]=="comment")
        $arSort = Array("created_date"=>"ASC");
    else
        $arSort = Array("created_date"=>"DESC");
    if($arParams["ELEMENT_ID"]):
        // get comments list
        $arSelectFields = array_merge($arParams["FIELD_CODE"], $arParams["PROPERTY_CODE"]);
        if ($arParams["IBLOCK_TYPE"]=="comment")
            $arIB["ID"] = (SITE_ID=="s1")?2438:2641;
        else                
            $arIB = getArIblock($arParams["IBLOCK_TYPE"], CITY_ID);
        //$arIB = getArIblock2($arParams["IBLOCK_TYPE"]);
        $rsComments = CIBlockElement::GetList(
            $arSort,
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arIB["ID"],
                "PROPERTY_ELEMENT" => $arParams["ELEMENT_ID"],
            ),
            false,
            $arNavParams,
            $arSelectFields
        );
        while($arComments = $rsComments->GetNext()) {
            // format date            
            $arComments["PREVIEW_TEXT"] = strip_tags($arComments["PREVIEW_TEXT"]);
            $text = $arComments["PREVIEW_TEXT"];
            preg_match_all("#(https?|ftp)://\S+[^\s.,>)\];'\"!?]#", $text, $links);
            

            foreach ($links[0] as $value) {
                $type[] = get_url_mime_type($value);
            }

            for ($i = 0; $i < count($type); $i++) {
                $imageType = preg_match("#(image/)#", $type[$i], $pregResult);
                $textType = preg_match("#(text/)#", $type[$i], $pregResult);
                if($imageType == 1){
                    $types[$i] = 'image';
                }
                if($textType == 1){
                    $videoType = preg_match("#(youtube.com|vimeo.com|video.yandex.ru|video.google.com|ru.youtube.com|rutube.ru|video.mail.ru|vkadre.ru|vision.rambler.ru|dailymotion.com|smotri.com|flickr.com)#", $links[0][$i], $pregResult);
                    if($videoType == 1){
                        $types[$i] = 'video';
                    } else {
                        $types[$i] = 'text';
                    }
                }
            }

            for ($i = 0; $i < count($types); $i++) {
                $result[$i] = array('link' => $links[0][$i], 'type' => $types[$i]);
                if ($types[$i]=="video")
                {
                    if (substr_count($links[0][$i], "youtube"))
                    {                    
                        $temp = explode("v=",$links[0][$i]);
                        if (!$temp[1])
                            $temp = explode("/",$links[0][$i]);
                        $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i],"<div align='center'><iframe width='400' height='300' src='http://www.youtube.com/embed/".$temp[count($temp)-1]."' frameborder='0' allowfullscreen></iframe></div>",$arComments["PREVIEW_TEXT"]);                                                                
                    }
                    if (substr_count($links[0][$i], "smotri"))
                    {        
                        //$links[0][$i] = str_replace('#', '', $links[0][$i]);
                        $temp = explode("id=",$links[0][$i]);
                        if (!$temp[1]){
                            $temp = explode("/",$links[0][$i]);
                        }
                            
                        $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i],"<div align='center'><iframe width='400' height='300' src='http://pics.smotri.com/player.swf?file=".str_replace('#', '', $temp[count($temp)-1])."' frameborder='0' allowfullscreen></iframe></div>",$arComments["PREVIEW_TEXT"]);                                                                
                    }
                    if (substr_count($links[0][$i], "vimeo"))
                    {                                            
                        $temp = explode("/",$links[0][$i]);
                        $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i],"<div align='center'><iframe width='400' height='300' src='http://player.vimeo.com/video/".$temp[count($temp)-1]."' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>",$arComments["PREVIEW_TEXT"]);                                                                
                    }
                    if ($USER->IsAdmin()){
                        
                        //echo $links[0][$i];
                    }
                    
                    if (substr_count($links[0][$i], "rutube"))
                    {                    
                        $temp = explode("/",$links[0][$i]);
                        if (!$temp[1])
                            $temp = explode("/",$links[0][$i]);
                        $rutubeObject = json_decode(file_get_contents('http://rutube.ru/api/oembed/?url=['.$links[0][$i].']&format=json'));
                        $rutubeArray = get_object_vars($rutubeObject);
                        //echo $rutubeArray['html'];
                        $rutubeArray['html'] = str_replace('iframe', 'iframe style="width:400px; height:300px; margin-left: 164px;"', $rutubeArray['html']);
                        $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i], $rutubeArray['html'] ,$arComments["PREVIEW_TEXT"]);                                                                
                    }    
                    /*if (substr_count($links[0][$i], "yandex"))
                    {                    
                        $temp = explode("/",$links[0][$i]);
                        if (!$temp[1])
                            $temp = explode("/",$links[0][$i]);
                        $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i],"<div align='center'><iframe width='400' height='300' src='http://rutube.ru/video/".$temp[count($temp)-2]."' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe></div>",$arComments["PREVIEW_TEXT"]);                                                                
                    } */
                }
                $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i],"<a class='another' href='".$links[0][$i]."' target='_blank'>".substr($links[0][$i],0,45)."...</a>",$arComments["PREVIEW_TEXT"]);
            }            
            $arComments["PREVIEW_TEXT"] = str_replace("\n", "<br />", $arComments["PREVIEW_TEXT"]);
            $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($arComments["ACTIVE_FROM"], CSite::GetDateFormat()));
            $arTmpDate = explode(" ", $arTmpDate);
            $arComments["FORMATED_DATE_1"] = $arTmpDate[0];
            $arComments["FORMATED_DATE_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

            // get user avatar
            if($arComments["CREATED_BY"]) {
                $rsUser = CUser::GetByID($arComments["CREATED_BY"]);
                $arUser = $rsUser->Fetch();
                $arComments["USER_NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"];
                $arComments["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT, true);
                $arComments["NICKNAME"] = $arUser["PERSONAL_PROFESSION"];
                //$arComments["AVATAR"] = CFile::GetPath($arUser["PERSONAL_PHOTO"]);
                if (!$arComments["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="M")
                    $arComments["AVATAR"]["src"] = "/tpl/images/noname/man_nnm.png";
                elseif (!$arComments["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="F")
                    $arComments["AVATAR"]["src"] = "/tpl/images/noname/woman_nnm.png";
                elseif (!$arComments["AVATAR"]["src"])
                    $arComments["AVATAR"]["src"] = "/tpl/images/noname/unisx_nnm.png";                
            }
            foreach($arParams["PROPERTY_CODE"] as $pid)
            {
                $values = array();
                $db_props = CIBlockElement::GetProperty($arComments["IBLOCK_ID"], $arComments["ID"], array("sort" => "asc"), Array("CODE"=>$pid));
                while($ar_props = $db_props->Fetch())	
                {
                    if ($ar_props["PROPERTY_TYPE"]=="E")
                    {
                        if (!is_array($ar_props["VALUE"]))
                        {
                            $pr_res = CIBlockElement::GetByID($ar_props["VALUE"]);
                            if($pr_res = $pr_res->GetNext())  
                            {
                                $values[] = $pr_res["NAME"];
                            }
                        }
                        else
                        {
                            $pr_res = CIBlockElement::GetByID($ar_props["VALUE"][0]);
                            if($pr_res = $pr_res->GetNext())  
                            {
                                $values[] = $pr_res["NAME"];
                            }
                        }
                    }
                    elseif(($ar_props["PROPERTY_TYPE"]=="N"||$ar_props["PROPERTY_TYPE"]=="S")&&$ar_props["MULTIPLE"]=="N")
                    {
                        $values = $ar_props["VALUE"];
                    }
                    else
                    {
                        if ($ar_props["CODE"]=="photos")
                        {
                            if($ar_props["VALUE"])
                                $values[] = Array("ID"=>$ar_props["VALUE"],"SRC"=>CFile::GetPath($ar_props["VALUE"]));
                        }
                        elseif ($ar_props["CODE"]=="video")
                        {
                            if($ar_props["VALUE"])
                                $values[] = Array("ID"=>$ar_props["VALUE"],"SRC"=>CFile::GetPath($ar_props["VALUE"]));
                        }
                        else
                            $values[] = $ar_props["VALUE"];
                    }                    
                    //Тут надо допили если свойство типа список                    
                }
                $arComments["PROPERTIES"][$pid] = $values;
            }            
            // set comment info
            if(!$arComments["PROPERTY_PARENT_VALUE"]) {
                $arResult["COMMENTS"][$arComments["ID"]] = $arComments;
            } else {
                // set child comments
                $arResult["CHILDS"][$arComments["PROPERTY_PARENT_VALUE"]][] = $arComments;
            }            
        }
        $arResult["IBLOCK_ID"] = $arIB["ID"];
        //v_dump($arResult["COMMENTS"]);

            $this->SetResultCacheKeys(array(
                    "ID",
                    "IBLOCK_ID",
                    "NAME",
                    "IBLOCK_SECTION_ID",
                    "IBLOCK",
                    "LIST_PAGE_URL", 
                    "~LIST_PAGE_URL",
                    "SECTION",
                    "PROPERTIES",
            ));
    endif;
    $this->IncludeComponentTemplate();

}

?>