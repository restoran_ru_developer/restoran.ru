<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if(($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"]) && $arParams["NEWS_COUNT"])
{
    $arNavParams = array(
        "nPageSize" => $arParams["NEWS_COUNT"],
    );
    if($_REQUEST['PAGEN']){
        $arNavParams['iNumPage']=$_REQUEST['PAGEN'];
    }
}
elseif($arParams["NEWS_COUNT"])
{
    $arNavParams = array(
        "nTopCount" => $arParams["NEWS_COUNT"],
    );
}
else {
    $arNavParams = false;
}


if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
    $arParams["SORT_ORDER1"]="ASC";


if ($this->StartResultCache(false,array($_REQUEST["lat"], $_REQUEST["lon"], $arNavParams)))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    $arSelect = array_merge($arParams["FIELD_CODE"], array(
        "ID",
        "IBLOCK_ID",
        "NAME",
    ));
    $arFilter = array (
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "IBLOCK_LID" => SITE_ID,
        "ACTIVE" => "Y",
    );


    $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
        $arParams["PARENT_SECTION"],
        $arParams["PARENT_SECTION_CODE"],
        array(
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        )
    );

    if($arParams["PARENT_SECTION"] > 0) {
        $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
        if($arParams["INCLUDE_SUBSECTIONS"])
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
    }
    else
    {
        $arResult["SECTION"]= false;
    }


    $arSort = array(
        $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"]
    );

    if (($arSort["distance"]=="asc"||$arSort["distance"]=="ASC")&&$_REQUEST["lat"]&&$_REQUEST["lon"])
    {
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/classes/cakeiblockelement.php");
        $arS = Array("PROPERTY_LAT","PROPERTY_LON");
        $rsElement = CCakeIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, array_merge($arSelect,$arS), $arParams['DISTANCE_VALUE']);
    }
    else {
        $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, $arSelect);
    }

    while ($obElement = $rsElement->GetNextElement()) {
        $arItem = array();
//        $arItem = $obElement->GetFields();

        $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;
        if ($bGetProperty) {
            foreach ($arParams['PROPERTY_CODE'] as $prop_name) {
                $arItem["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
            }
        }

        $arResult['RestListIds'][] = $arItem['PROPERTIES']['RESTORAN']['VALUE'][0];
    }

    $arResult['NavPageCount'] = $rsElement->NavPageCount;

    $this->SetResultCacheKeys(array(
        "RestListIds",
        "NavPageCount",
    ));
    $this->IncludeComponentTemplate();
}
?>