<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!isset($arParams["CACHE_TIME"])) {
        $arParams["CACHE_TYPE"] = "Y";
	$arParams["CACHE_TIME"] = 0;
}

$q = $arParams["q"];
if (!$page||$page==1)
    $page = 0;

if($this->StartResultCache(false, $page.$q.CITY_ID.'dsd'))
{
    $arRestIB = getArIblock("catalog", $arParams["IBLOCK_ID"]);


    CModule::IncludeModule("iblock");
    if ($q)
    {
        $q = ToLower($q);
        $lang = get_lang($q);
        $arSelect = Array("ID","NAME");
        $iblock = $DB->ForSQL($arRestIB["ID"]);        
        $q = $DB->ForSQL($q);
        $count = $DB->ForSQL($arParams["COUNT"]);
        $rest_sql = "";

        $network_prop_compare_arr = array(
            12 => 4534,
            2636 => 4540,
            11 => 4536,
            3567 => 4542,
            3566 => 4538,
            13 => 4544,
            16 => 4546,
            2591 => 4548,
            2624 => 4550,
            2674 => 4552,
            2821 => 4554,
            3418 => 4556,
            3624 => 4622,
            3625 => 4624,
            15 => 4558,
            2820 => 4560,
            2719 => 4562,
            2586 => 4626,
            2587 => 4628
        );

//        FirePHP::getInstance()->info($arRestIB);

        if ($lang=="ru")
        {
            $q2 = decode2anotherlang_ru($q);
            $q2 = $DB->ForSQL($q2);

            $res = $DB->Query('(SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')
                            UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')
                            UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')
                            UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')'.'LIMIT '.$count);
        }
        elseif ($lang=="en")
        {
            $q2 = decode2anotherlang_en($q);
            $q2 = $DB->ForSQL($q2);

            $res = $DB->Query('(SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "'.$q.'%" OR NAME LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')
                            UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.NAME LIKE "%'.$q.'%" OR NAME LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')
                            UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "%'.$q.'%" OR TAGS LIKE "%'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')
                            UNION (SELECT b_iblock_element.ID,b_iblock_element.NAME,b_iblock_element.IBLOCK_ID FROM b_iblock_element
                LEFT JOIN b_iblock_element_property
                ON b_iblock_element.ID=b_iblock_element_property.IBLOCK_ELEMENT_ID AND b_iblock_element_property.IBLOCK_PROPERTY_ID='.$network_prop_compare_arr[$iblock].'
                WHERE b_iblock_element.IBLOCK_ID='.$iblock.' AND (b_iblock_element.TAGS LIKE "'.$q.'%" OR TAGS LIKE "'.$q2.'%") AND b_iblock_element_property.IBLOCK_ELEMENT_ID IS NULL AND b_iblock_element.ACTIVE="Y"
                ORDER BY b_iblock_element.NAME ASC
                LIMIT '.$count.')'.$rest_sql.'LIMIT '.$count);
        }
    }

    if ($res)
    {
        $sleeping = $no_sleeping = array();
        while($ar = $res->Fetch())
        {
            if ($arParams["NO_SLEEP"]=="Y")
            {
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array(), Array("CODE"=>"sleeping_rest"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sleeping[] = $ar;
                    }
                    else {
                        $no_sleeping[] = $ar;
                    }
                }
                else {
                    $no_sleeping[] = $ar;
                }
            }
        }

        if ($arParams["NO_SLEEP"]=="Y") {
            $arResult["TOP_RESTORAN_ITEMS"] = $no_sleeping;
        }
        else {
            $arResult["TOP_RESTORAN_ITEMS"] = array_merge($no_sleeping,$sleeping);
        }

        $arParams["PROPERTY_CODE"] = array('s_place_restaurants','subway','address','lat','lon','average_bill','kitchen','opening_hours','opening_hours_google','phone');

        foreach($arResult["TOP_RESTORAN_ITEMS"] as $cur_key=>$arItem){

            $res = CIBlockElement::GetByID($arItem['ID']);
            if($obElement = $res->GetNextElement()){
                $arItem = $obElement->GetFields();

                if ($arItem["PREVIEW_PICTURE"]) {
                    $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
                    //$arPhoto = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => $arParams["PREVIEW_PICTURE_MAX_WIDTH"], 'height' => $arParams["PREVIEW_PICTURE_MAX_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
                }


                $arItem["FIELDS"] = array();
                foreach ($arParams["FIELD_CODE"] as $code)
                    if (array_key_exists($code, $arItem))
                        $arItem["FIELDS"][$code] = $arItem[$code];


                $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;
                if ($bGetProperty) {
                    foreach ($arParams['PROPERTY_CODE'] as $prop_name) {
//                    FirePHP::getInstance()->info($prop_name);
                        $arItem["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
                    }
                }


                $arItem["DISPLAY_PROPERTIES"] = array();
                foreach ($arParams["PROPERTY_CODE"] as $pid) {
                    $prop = &$arItem["PROPERTIES"][$pid];
                    if ((is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0) ||
                        (!is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0)
                    ) {
                        $arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
                    }
                }

                //  is ADVERT
                if ($arItem['PROPERTY_S_PLACE_RESTAURANTS_VALUE']) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['IS_ADVERT'] = true;
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['IS_ADVERT'] = false;
                }

                $arResult["RESTORAN_ITEMS"][$cur_key]['ID'] = $arItem['ID'];
                $arResult["RESTORAN_ITEMS"][$cur_key]['NAME'] = html_entity_decode($arItem['NAME']);
                $arResult["RESTORAN_ITEMS"][$cur_key]['PREVIEW_PICTURE'] = 'http://www.restoran.ru' . $arItem['PREVIEW_PICTURE']['SRC'];


                //number_of_bookings
                //  book counter
                $arResult["RESTORAN_ITEMS"][$cur_key]['BOOK_COUNTER_ALL'] = 0;
                $arSelect = Array("PROPERTY_guest");
                $arFilter = Array("IBLOCK_ID" => 105, "ACTIVE" => "Y", 'PROPERTY_rest' => $arItem['ID'], '!PROPERTY_status' => 1418);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while ($ob = $res->Fetch()) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['BOOK_COUNTER_ALL'] += $ob['PROPERTY_GUEST_VALUE'];
                }


                $arResult["RESTORAN_ITEMS"][$cur_key]['BOOK_COUNTER_TODAY'] = 0;
                $arSelect = Array("PROPERTY_guest");
                $arFilter = Array("IBLOCK_ID" => 105, "ACTIVE" => "Y", 'PROPERTY_rest' => $arItem['ID'], '>=DATE_ACTIVE_FROM' => ConvertTimeStamp(strtotime(date('d.m.Y'))), '>=DATE_CREATE' => ConvertTimeStamp(strtotime(date('d.m.Y'))), '<=DATE_CREATE' => ConvertTimeStamp(strtotime(date('d.m.Y', strtotime('+1 day')))), '!PROPERTY_status' => 1418);//'<=DATE_ACTIVE_FROM'=>ConvertTimeStamp(strtotime(date('d.m.Y',strtotime('+1 day'))))
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while ($ob = $res->Fetch()) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['BOOK_COUNTER_TODAY'] += $ob['PROPERTY_GUEST_VALUE'];
                }


                if (is_array($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['subway'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['subway'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['address'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['address'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['lat'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['lat'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['lon'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['lon'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['average_bill'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['average_bill'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['kitchen'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['kitchen'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours_google'][] = strip_tags($prop_val);
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours_google'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE']);
                }

                if (is_array($arItem["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE'])) {
                    foreach ($arItem["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_ITEMS"][$cur_key]['phone'][] = clearUserPhoneMobileApp(strip_tags($prop_val));
                    }
                }
                else {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['phone'] = clearUserPhoneMobileApp(strip_tags($arItem["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE']));
                }
            }
        }
    }
    $this->SetResultCacheKeys(array(
        "RESTORAN_ITEMS",
    ));
}

$result_array['RESTORAN_ITEMS'] = $arResult["RESTORAN_ITEMS"];
return $result_array;
?>