<?
CModule::IncludeModule("iblock");
if (is_array($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            //$el["PROPERTIES"] = $obElement->GetProperties();    
            if ($el["IBLOCK_TYPE_ID"]=="rest_group")
            {
                $el["SECTION_NAME"] = "Ресторанная группа";
                $el["DETAIL_PAGE_URL"] = "/".CITY_ID."/catalog/restaurants/group/".$el["CODE"]."/";
            }
            elseif ($el["IBLOCK_TYPE_ID"]=="closed_rubrics_rest")
            {
                $el["SECTION_NAME"] = "Рубрика";
                $el["DETAIL_PAGE_URL"] = "/".CITY_ID."/catalog/restaurants/rubrics/".$el["CODE"]."/";
            }
            else
            {
                $res = CIBlockSection::GetByID($el["IBLOCK_SECTION_ID"]);
                if ($ar = $res->Fetch())
                    $el["SECTION_NAME"] = $ar["NAME"];
            }
            $arItem["ELEMENT"] = $el;

            $arFileTmp = CFile::ResizeImageGet(
                $el['PREVIEW_PICTURE'],
                array("width" => 40, "height" => 40),
                BX_RESIZE_IMAGE_EXACT,
                false
            );

            $db_props = CIBlockElement::GetProperty($el['IBLOCK_ID'], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
//                AddMessage2Log($ar_props);
                $arItem["ELEMENT"]['address'] = $ar_props['VALUE'];
            }

            $arItem["ELEMENT"]['SEARCH_PREVIEW_PIC']['SRC'] = $arFileTmp["src"];
         }
    endforeach;?>
<?endif;?>
