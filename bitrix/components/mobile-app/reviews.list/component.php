<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(!$arParams["NEWS_COUNT"]){
    $arNavParams = false;
}
else {
    $arNavParams = array(
        "nPageSize" => $arParams["NEWS_COUNT"],
        "bDescPageNumbering" => '',
        "bShowAll" => false
    );
}

if($_REQUEST['PAGEN'] && $arParams["NEWS_COUNT"]){
    $arNavParams['iNumPage']=$_REQUEST['PAGEN'];
}
//$arNavigation = CDBResult::GetNavParams($arNavParams);

//FirePHP::getInstance()->info($arNavParams,'$arNavParams');
//FirePHP::getInstance()->info($arNavigation,'$arNavigation');

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

if ($this->StartResultCache(false,array($arParams["ID"],$arNavParams,$arrFilter))) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        return;
    }

    $res = CIBlockElement::GetByID(intval($arParams['ID']));
    if ($ar_res = $res->Fetch()) {
        $arReviewsIB = getArIblock("reviews", $ar_res['IBLOCK_CODE']);

        if($arParams['REVIEW_PHOTOS']=='Y'){
            $arSelect = Array("ID",'IBLOCK_ID','NAME','DATE_CREATE');
            $arFilter = Array("IBLOCK_ID" => $arReviewsIB['ID'], "ACTIVE" => "Y", 'PROPERTY_ELEMENT' => $ar_res['ID'],'!PROPERTY_photos'=>false);
            $resReviews = CIBlockElement::GetList(Array('created' => 'DESC'), $arFilter, false, $arNavParams, $arSelect);
            $review_key = 0;
            $photos_key = 0;

            while ($obElement = $resReviews->GetNextElement()) {

                $arItem = $obElement->GetFields();

                $THIS_PHOTOS_PROP_VALUE = $obElement->GetProperty('photos');

                $THIS_PHOTOS_DISPLAY_VALUE = CIBlockFormatProperties::GetDisplayValue($arItem, $THIS_PHOTOS_PROP_VALUE, "news_out");

//                if (count($THIS_PHOTOS_DISPLAY_VALUE['VALUE'])>1) {
//                    foreach ($THIS_PHOTOS_DISPLAY_VALUE['FILE_VALUE'] as $prop_val) {
//                        $arResult["RESTORAN_REVIEWS"][$review_key]['PHOTOS'][] = $prop_val['SRC'];
//                    }
//                }
//                else {
//                    $arResult["RESTORAN_REVIEWS"][$review_key]['PHOTOS'] = $THIS_PHOTOS_DISPLAY_VALUE['FILE_VALUE']['SRC'];
//                }

                if (count($THIS_PHOTOS_DISPLAY_VALUE['VALUE'])>1) {
                    foreach ($THIS_PHOTOS_DISPLAY_VALUE['FILE_VALUE'] as $prop_val) {
                        $arResult["RESTORAN_REVIEWS"]['PHOTOS'][$photos_key]['NICKNAME'] = $arItem['NAME'];
                        $arResult["RESTORAN_REVIEWS"]['PHOTOS'][$photos_key]['DATE_CREATE'] = strtotime($arItem['DATE_CREATE']);
                        $arResult["RESTORAN_REVIEWS"]['PHOTOS'][$photos_key]['URL'] = $prop_val['SRC'];
                        $photos_key++;
                    }
                }
                else {
                    $arResult["RESTORAN_REVIEWS"]['PHOTOS'][$photos_key]['NICKNAME'] = $arItem['NAME'];
                    $arResult["RESTORAN_REVIEWS"]['PHOTOS'][$photos_key]['DATE_CREATE'] = strtotime($arItem['DATE_CREATE']);
                    $arResult["RESTORAN_REVIEWS"]['PHOTOS'][$photos_key]['URL'] = $THIS_PHOTOS_DISPLAY_VALUE['FILE_VALUE']['SRC'];
                    $photos_key++;
                }

                $review_key++;
            }
        }
        else {
            $arSelect = Array("ID", 'CREATED_BY', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'DATE_CREATE', 'NAME');
            $arFilter = Array("IBLOCK_ID" => $arReviewsIB['ID'], "ACTIVE" => "Y", 'PROPERTY_ELEMENT' => $ar_res['ID']);
            $resReviews = CIBlockElement::GetList(Array('created' => 'DESC'), $arFilter, false, $arNavParams, $arSelect);
            $review_key = 0;
            while ($ob = $resReviews->Fetch()) {
//                    FirePHP::getInstance()->info($ob);
                $res = CUser::GetByID($ob["CREATED_BY"]);
                if ($ar = $res->Fetch()) {
                    $arResult['RESTORAN_REVIEWS'][$review_key]['username'] = $ar["PERSONAL_PROFESSION"];
                    if (!$arResult['RESTORAN_REVIEWS'][$review_key]['username']) {
                        $temp = explode("@", $ar["EMAIL"]);
                        $arResult['RESTORAN_REVIEWS'][$review_key]['username'] = $temp[0];
                    }
                } else {
                    $temp = explode("@", $ob["NAME"]);
                    $arResult['RESTORAN_REVIEWS'][$review_key]['username'] = $temp[0];
                }

                $arResult['RESTORAN_REVIEWS'][$review_key]['date'] = strtotime($ob['DATE_CREATE']);
                $arResult['RESTORAN_REVIEWS'][$review_key]['comment'] = html_entity_decode($ob['PREVIEW_TEXT']);
                $arResult['RESTORAN_REVIEWS'][$review_key]['rating'] = $ob['DETAIL_TEXT'];

                $review_key++;
            }
        }


        $arResult['NavPageCount'] = $resReviews->NavPageCount;
//
//        FirePHP::getInstance()->info($arResult);
//        $this->SetResultCacheKeys(array(
//            "RESTORAN_ITEMS",
//        ));
    }
    else
    {
        $this->AbortResultCache();
        ShowError(GetMessage("T_NEWS_NEWS_NA"));
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
    }

}

if(isset($arResult['RESTORAN_REVIEWS']))
{
    $result_array['NavPageCount'] = $arResult['NavPageCount'];
    $result_array['RESTORAN_REVIEWS'] = $arResult["RESTORAN_REVIEWS"];
    return $result_array;
}
?>