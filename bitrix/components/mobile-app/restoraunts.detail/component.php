<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//FirePHP::getInstance()->info(3212312);
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

//if($_SESSION["CONTEXT"]=="Y") $_REQUEST["CONTEXT"]="Y";	
if($APPLICATION->get_cookie("CONTEXT")=="Y") $_REQUEST["CONTEXT"]="Y";

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
    $arParams["IBLOCK_TYPE"] = "news";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";

// set sort value
$arParams["SORT_ORDER1"] = ($_REQUEST["by"])?$_REQUEST["by"]:$arParams["SORT_ORDER1"];
$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
    $arParams["SORT_BY1"] = "ACTIVE_FROM";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
    $arParams["SORT_ORDER1"]="DESC";

if(strlen($arParams["SORT_BY2"])<=0)
    $arParams["SORT_BY2"] = "SORT";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
    $arParams["SORT_ORDER2"]="ASC";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

if(!is_array($arParams["FIELD_CODE"]))
    $arParams["FIELD_CODE"] = array();

if(!is_array($arParams["PROPERTY_CODE"]))
    $arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $key=>$val)
    if($val==="")
        unset($arParams["PROPERTY_CODE"][$key]);

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"]<=0)
    $arParams["NEWS_COUNT"] = 20;

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
    $arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
    $arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
$arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
$arParams["HIDE_LINK_WHEN_NO_DETAIL"] = $arParams["HIDE_LINK_WHEN_NO_DETAIL"]=="Y";

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
    $arNavParams = array(
        "nPageSize" => $arParams["NEWS_COUNT"],
        "bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
        "bShowAll" => $arParams["PAGER_SHOW_ALL"],
    );

    //  для фильтров с наборами first_20
    if($_REQUEST['PAGEN']){
        $arNavParams['iNumPage']=$_REQUEST['PAGEN'];
    }

    $arNavigation = CDBResult::GetNavParams($arNavParams);
    if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
        $arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
    $arNavParams = array(
        "nTopCount" => $arParams["NEWS_COUNT"],
        "bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
    );
    $arNavigation = false;
}

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
    $arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
    $arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
    foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
    {
        if(in_array($PERM, $arUserGroupArray))
        {
            $bUSER_HAVE_ACCESS = true;
            break;
        }
    }
}

if($this->StartResultCache(false))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
    //SELECT
    $arSelect = array_merge($arParams["FIELD_CODE"], array(
        "ID",
        "IBLOCK_ID",
        "IBLOCK_CODE",
        "NAME",
        "DETAIL_TEXT",
        "PREVIEW_PICTURE",
        "DETAIL_PICTURE",
        "DETAIL_PAGE_URL",
        'MOBILE_APP'
    ));

//        if($bGetProperty)
//            $arSelect[]="PROPERTY_*";

    $arFilter = array(
        "IBLOCK_LID" => SITE_ID,
        "IBLOCK_ACTIVE" => "Y",
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "N",
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    );

    if($arParams["CHECK_DATES"])
        $arFilter["ACTIVE_DATE"] = "Y";

    //ORDER BY
    $arSort = array(
        $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
        $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
    );

    $arResult["ITEMS"] = array();
    $arResult["ELEMENTS"] = array();

    $arFilter["ID"] = $arParams["ELEMENT_ID"];

    $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, array('nTopCount'=>1), $arSelect);

    if ($obElement = $rsElement->GetNextElement()) {

//            FirePHP::getInstance()->info($obElement);

        $arResult = $obElement->GetFields();
//            FirePHP::getInstance()->info($arResult,'$arResult');//


        if ($arResult["PREVIEW_PICTURE"]) {
            $arResult["PREVIEW_PICTURE"] = CFile::GetFileArray($arResult["PREVIEW_PICTURE"]);
            //$arPhoto = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"], array('width' => $arParams["PREVIEW_PICTURE_MAX_WIDTH"], 'height' => $arParams["PREVIEW_PICTURE_MAX_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
//            FirePHP::getInstance()->info($arResult["PREVIEW_PICTURE"]);
        }
        elseif($arResult['DETAIL_PICTURE']){
            $arResult["PREVIEW_PICTURE"] = CFile::GetFileArray($arResult["DETAIL_PICTURE"]);
        }


        $arResult["FIELDS"] = array();
        foreach ($arParams["FIELD_CODE"] as $code)
            if (array_key_exists($code, $arResult))
                $arResult["FIELDS"][$code] = $arResult[$code];


        $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;
        if ($bGetProperty) {
            foreach ($arParams['PROPERTY_CODE'] as $prop_name) {
//                    FirePHP::getInstance()->info($prop_name);
                $arResult["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
            }
        }


        $arResult["DISPLAY_PROPERTIES"] = array();
        foreach ($arParams["PROPERTY_CODE"] as $pid) {
            $prop = &$arResult["PROPERTIES"][$pid];
            if ((is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0) ||
                (!is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0)
            ) {
                $arResult["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arResult, $prop, "news_out");
            }
        }

//        FirePHP::getInstance()->info($arResult["DISPLAY_PROPERTIES"]);

//        $arResult["ITEMS"] = $arResult;


        //  is ADVERT
        if ($arResult['PROPERTY_S_PLACE_RESTAURANTS_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['IS_ADVERT'] = true;
        }
        else {
            $arResult["RESTORAN_ITEMS"]['IS_ADVERT'] = false;
        }

        $arResult["RESTORAN_ITEMS"]['DETAIL_PAGE_URL'] = $arResult['DETAIL_PAGE_URL'];

        $arResult["RESTORAN_ITEMS"]['ID'] = $arResult['ID'];
        $arResult["RESTORAN_ITEMS"]['NAME'] = html_entity_decode($arResult['NAME']);
        if($arResult['PREVIEW_PICTURE']['SRC']){
            $arResult["RESTORAN_ITEMS"]['PREVIEW_PICTURE'] = $arResult['PREVIEW_PICTURE']['SRC'];
        }



        /****COMMON_PROPS****/
        if($arResult["DISPLAY_PROPERTIES"]['RATIO']['DISPLAY_VALUE']){
            $arResult["RESTORAN_ITEMS"]['rating'] = $arResult["DISPLAY_PROPERTIES"]['RATIO']['DISPLAY_VALUE'];
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['subway'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['subway'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['address'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['address'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['lat'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['lat'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['lon'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['lon'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['average_bill'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['average_bill'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['kitchen'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['kitchen'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['opening_hours_google'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['opening_hours_google'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['opening_hours'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['opening_hours'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['phone'][] = clearUserPhoneMobileApp(strip_tags($prop_val));
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['phone'] = clearUserPhoneMobileApp(strip_tags($arResult["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE']));
        }

//        FirePHP::getInstance()->info(json_encode($arResult['DETAIL_TEXT']),'DETAIL_TEXT');


        $arResult["RESTORAN_ITEMS"]['description'] = html_entity_decode(str_replace(array("\r", "\n"), ' ', strip_tags($arResult['DETAIL_TEXT'])),ENT_COMPAT,'UTF-8');//html_entity_decode(
        /****COMMON_PROPS****/

        /****PHOTOS****/
        $photos_count = 0;
        if (is_array($arResult["DISPLAY_PROPERTIES"]['photos']['FILE_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['photos']['FILE_VALUE'] as $prop_val) {
//                if($photos_count>2)break;
                $arResult["RESTORAN_ITEMS"]['photos'][] = $prop_val['SRC'];
                $photos_count++;
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['photos'] = $arResult["DISPLAY_PROPERTIES"]['photos']['FILE_VALUE']['SRC'];
        }

        //  колличество фото пользователей
        $arReviewsIB = getArIblock("reviews", $arResult['IBLOCK_CODE']);
        $arFilter = Array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>$arResult['ID'],'!PROPERTY_photos'=>false);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
        if($ob = $res->SelectedRowsCount())
        {
            $arResult["RESTORAN_ITEMS"]['gallery_photos_count'] = $ob;
        }
        else {
            $arResult["RESTORAN_ITEMS"]['gallery_photos_count'] = 0;
        }
//
        /****PHOTOS****/





        /****FEATURES****/
//        FirePHP::getInstance()->info($arResult["DISPLAY_PROPERTIES"]['type'],'type');

        if (is_array($arResult["DISPLAY_PROPERTIES"]['type']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['type']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['type']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['type']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['type']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['type']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['type']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['type']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['type']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['type']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['music']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['music']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['music']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['music']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['music']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['music']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['music']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['music']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['music']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['music']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['parking']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['parking']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['parking']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['parking']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['parking']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['parking']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['parking']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['parking']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['parking']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['parking']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['features']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['features']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['features']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['features']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['features']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['features']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['features']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['features']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['features']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['features']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['children']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['children']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['children']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['children']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['children']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['children']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['children']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['children']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['children']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['children']['DISPLAY_VALUE']);
        }


        if (is_array($arResult["DISPLAY_PROPERTIES"]['proposals']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['proposals']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['proposals']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['proposals']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['proposals']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['proposals']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['proposals']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['proposals']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['proposals']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['proposals']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['breakfast']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['breakfast']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['breakfast']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['breakfast']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['breakfast']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['breakfast']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['breakfast']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['breakfast']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['breakfast']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['breakfast']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['entertainment']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['entertainment']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['entertainment']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['entertainment']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['entertainment']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['entertainment']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['entertainment']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['entertainment']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['entertainment']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['entertainment']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['d_tours']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['d_tours']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['d_tours']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['d_tours']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['d_tours']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['d_tours']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['d_tours']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['ALLOWED_ALCOHOL']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['DISPLAY_VALUE'])) {
            $arResult["RESTORAN_ITEMS"]['features']['ALLOWED_ALCOHOL']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['NAME'];
            foreach ($arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['features']['ALLOWED_ALCOHOL']['VALUE'][] = strip_tags($prop_val);
            }
        }
        elseif($arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['DISPLAY_VALUE']) {
            $arResult["RESTORAN_ITEMS"]['features']['ALLOWED_ALCOHOL']['NAME'] = $arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['NAME'];
            $arResult["RESTORAN_ITEMS"]['features']['ALLOWED_ALCOHOL']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['ALLOWED_ALCOHOL']['DISPLAY_VALUE']);
        }
        /****FEATURES****/

        /****REGION****/
        if (is_array($arResult["DISPLAY_PROPERTIES"]['area']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['area']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['district'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['district'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['area']['DISPLAY_VALUE']);
        }

        if (is_array($arResult["DISPLAY_PROPERTIES"]['out_city']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['out_city']['DISPLAY_VALUE'] as $prop_val) {
                $arResult["RESTORAN_ITEMS"]['out_of_city'][] = strip_tags($prop_val);
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['out_of_city'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['out_city']['DISPLAY_VALUE']);
        }
        /****REGION****/


        /****FEATURES_IN_PICS****/
        if (is_array($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['DISPLAY_VALUE'])) {
            foreach ($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['DISPLAY_VALUE'] as $porp_arr_key=>$prop_val) {
                $arResult["RESTORAN_ITEMS"]['FEATURES_IN_PICS'][$porp_arr_key]['VALUE'] = strip_tags($prop_val);
                if($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['VALUE'][$porp_arr_key]]['PREVIEW_PICTURE']){
                    $arFile = CFile::GetFileArray($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['VALUE'][$porp_arr_key]]['PREVIEW_PICTURE']);
                    $arResult["RESTORAN_ITEMS"]['FEATURES_IN_PICS'][$porp_arr_key]['PREVIEW_PICTURE'] = $arFile['SRC'];
                }
            }
        }
        else {
            $arResult["RESTORAN_ITEMS"]['FEATURES_IN_PICS']['VALUE'] = strip_tags($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['DISPLAY_VALUE']);
            if($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['VALUE']]['PREVIEW_PICTURE']){
                $arFile = CFile::GetFileArray($arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]['FEATURES_IN_PICS']['VALUE']]['PREVIEW_PICTURE']);
                $arResult["RESTORAN_ITEMS"]['FEATURES_IN_PICS']['PREVIEW_PICTURE'] = $arFile['SRC'];
            }
        }
        /****FEATURES_IN_PICS****/







        /****IN_COLLECTION****/
        if($arResult['IBLOCK_CODE']=='msk'){
            $PROP_IBLOCK_ID = 4364;
        }
        elseif($arResult['IBLOCK_CODE']=='spb') {
            $PROP_IBLOCK_ID = 4363;
        }
        elseif($arResult['IBLOCK_CODE']=='rga') {
            $PROP_IBLOCK_ID = 4365;
        }

        $arFilter = Array('IBLOCK_ID'=>$PROP_IBLOCK_ID, 'ACTIVE'=>'Y','CNT_ACTIVE'=>'Y','ELEMENT_SUBSECTIONS'=>'N','SECTION_ID'=>false);//'DEPTH_LEVEL'=>1
        $db_list = CIBlockSection::GetList(Array('name'=>'asc'), $arFilter, true, array('ID'),false);//,'IBLOCK_SECTION_ID'
        while ($ar = $db_list->Fetch()) {
            $FIRST_20_SECTION_IDS[] = $ar['ID'];
        }

        $res = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>$PROP_IBLOCK_ID,'PROPERTY_RESTAURANT'=>$arParams["ELEMENT_ID"], 'SECTION_ID'=>$FIRST_20_SECTION_IDS,'INCLUDE_SUBSECTIONS'=>'N'), false, false, array('IBLOCK_SECTION_ID'));
        $collection_key = 0;
        while($ob = $res->Fetch())
        {
//            $res = CIBlockSection::GetByID($ob['IBLOCK_SECTION_ID']);
            if($ob['IBLOCK_SECTION_ID']){
                $db_list = CIBlockSection::GetList(Array('ID'=>'ASC'), array('ID'=>$ob['IBLOCK_SECTION_ID']), true, array('ID','NAME','PICTURE'));
                if($ar_res = $db_list->Fetch()){
                    if($ar_res['PICTURE']){
                        $resize_img = CFile::ResizeImageGet($ar_res['PICTURE'], array('width'=>1024, 'height'=>1024), BX_RESIZE_IMAGE_EXACT, true, Array());
                        $arResult["RESTORAN_ITEMS"]['COLLECTION_INF'][$collection_key]['PICTURE'] = $resize_img['src'];
                    }
                    $arResult["RESTORAN_ITEMS"]['COLLECTION_INF'][$collection_key]['ID'] = $ar_res['ID'];
                    $arResult["RESTORAN_ITEMS"]['COLLECTION_INF'][$collection_key]['NAME'] = $ar_res['NAME'];
                    $arResult["RESTORAN_ITEMS"]['COLLECTION_INF'][$collection_key]['ELEMENT_CNT'] = $ar_res['ELEMENT_CNT'];
                }
                $collection_key++;
            }
        }


        /****IN_COLLECTION****/

        /****MENU****/
        global $DB;
        $sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a
        JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=".$DB->ForSql($arResult["ID"])." ";
        $db_list = $DB->Query($sql);
        if($ar_result = $db_list->Fetch())
        {
//            FirePHP::getInstance()->info($ar_result,'$ar_result$ar_result$ar_result$ar_result$ar_result$ar_result');
            if ($ar_result["ELEMENT_CNT"]>0){
                $MENU_ID = $ar_result['ID'];

                //  получение id фото раздела
                $menu_key = 0;
//                FirePHP::getInstance()->info();

                $db_list = CIBlockSection::getList(array(),array('IBLOCK_ID'=>$MENU_ID, 'CODE'=>'foto'),false, array('ID'));
                if($ar_result = $db_list->Fetch()){
                    $photoSectionId = $ar_result['ID'];
                    $db_list = CIBlockElement::GetList(array('ID'=>'DESC'),array('IBLOCK_ID'=>$MENU_ID, '!SECTION_ID'=>$photoSectionId, 'INCLUDE_SUBSECTIONS'=>'Y','ACTIVE'=>'Y'), false, array('nPageSize'=>2,'iNumPage'=>1), array('ID','NAME','PREVIEW_PICTURE'));
                }
                else {
                    $db_list = CIBlockElement::GetList(array('ID'=>'DESC'),array('IBLOCK_ID'=>$MENU_ID, 'INCLUDE_SUBSECTIONS'=>'Y','ACTIVE'=>'Y'), false, array('nPageSize'=>2,'iNumPage'=>1), array('ID','NAME','PREVIEW_PICTURE'));//'>CATALOG_PRICE_1'=>0
                }
                CModule::IncludeModule("sale");
                CModule::IncludeModule("catalog");
                while($ar_result = $db_list->Fetch()){
                    $arResult["RESTORAN_ITEMS"]['menu'][$menu_key]['name'] = $ar_result['NAME'];
                    if($ar_result['PREVIEW_PICTURE']){
                        $arFile = CFile::GetFileArray($ar_result['PREVIEW_PICTURE']);
                        if($arFile){
                            $arResult["RESTORAN_ITEMS"]['menu'][$menu_key]['image_url'] = $arFile['SRC'];
                        }
                    }



                    $db_res = CPrice::GetList(
                        array(),
                        array(
                            "PRODUCT_ID" => $ar_result["ID"],
                            "CATALOG_GROUP_ID" => 1,
                        )
                    );
                    if($price = $db_res->Fetch()){
                        $this_price = $price;
                    }

                    if ($this_price["CURRENCY"]=="EUR")
                        $arResult["RESTORAN_ITEMS"]['menu'][$menu_key]['price'] = sprintf("%01.2f", $this_price["PRICE"]);
                    else
                        $arResult["RESTORAN_ITEMS"]['menu'][$menu_key]['price'] = sprintf("%01.0f", $this_price["PRICE"]);

                    $menu_key++;
                }

                $arResult["RESTORAN_ITEMS"]['dishes_number'] = $db_list->NavRecordCount;
            }
        }
        /****MENU****/

        $this->SetResultCacheKeys(array(
            "RESTORAN_ITEMS",
//            'IBLOCK_CODE',
//            'ID'
        ));
    }
    else
    {
        $this->AbortResultCache();
        ShowError(GetMessage("T_NEWS_NEWS_NA"));
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
    }
}

if(isset($arResult["ID"]))
{



    /****BOOK COUNTER****/
    $arResult["RESTORAN_ITEMS"]['BOOK_COUNTER_ALL'] = 0;
    $arSelect = Array("PROPERTY_guest");
    $arFilter = Array("IBLOCK_ID" => 105, "ACTIVE" => "Y", 'PROPERTY_rest' => $arResult['ID'], '!PROPERTY_status' => 1418);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->Fetch()) {
        $arResult["RESTORAN_ITEMS"]['BOOK_COUNTER_ALL'] += $ob['PROPERTY_GUEST_VALUE'];
    }


    $arResult["RESTORAN_ITEMS"]['BOOK_COUNTER_TODAY'] = 0;
    $arSelect = Array("PROPERTY_guest");
    $arFilter = Array("IBLOCK_ID" => 105, "ACTIVE" => "Y", 'PROPERTY_rest' => $arResult['ID'], '>=DATE_ACTIVE_FROM' => ConvertTimeStamp(strtotime(date('d.m.Y'))), '>=DATE_CREATE' => ConvertTimeStamp(strtotime(date('d.m.Y'))), '<=DATE_CREATE' => ConvertTimeStamp(strtotime(date('d.m.Y', strtotime('+1 day')))), '!PROPERTY_status' => 1418);//'<=DATE_ACTIVE_FROM'=>ConvertTimeStamp(strtotime(date('d.m.Y',strtotime('+1 day'))))
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->Fetch()) {
        $arResult["RESTORAN_ITEMS"]['BOOK_COUNTER_TODAY'] += $ob['PROPERTY_GUEST_VALUE'];
    }
    /****BOOK COUNTER****/

//        { : GMT date,  : 'username', comment : 'текст комментария', rating : 5ти бальная оценка}
    /****LAST_COMMENT****/
    $arReviewsIB = getArIblock("reviews", $arResult['IBLOCK_CODE']);
    $arSelect = Array("ID",'CREATED_BY','PREVIEW_TEXT','DETAIL_TEXT','TIMESTAMP_X','NAME','PROPERTY_photos');
    $arFilter = Array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>$arResult['ID']);
    $res = CIBlockElement::GetList(Array('timestamp_x'=>'DESC'), $arFilter, false, array('nTopCount'=>2), $arSelect);
    $reviews_count = 0;
    while($ob = $res->Fetch())
    {
        $res_user = CUser::GetByID($ob["CREATED_BY"]);
        if ($ar = $res_user->Fetch())
        {
            $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['username'] = $ar["PERSONAL_PROFESSION"];
            if (!$arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['username'])
            {
                $temp = explode("@",$ar["EMAIL"]);
                $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['username'] = $temp[0];
            }

            $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"] = $arUser["PERSONAL_PHOTO"];
            if (!$arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"]["SRC"]&&$arUser["PERSONAL_GENDER"]=="M")
                $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"]["SRC"] = "/tpl/images/noname/man_nnm.png";
            elseif (!$arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"]["SRC"]&&$arUser["PERSONAL_GENDER"]=="F")
                $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"]["SRC"] = "/tpl/images/noname/woman_nnm.png";
            elseif (!$arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"]["SRC"])
                $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]["avatar"]['SRC'] = "/tpl/images/noname/unisx_nnm.png";
        }
        else
        {
            $temp = explode("@",$ob["NAME"]);
            $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['username'] = $temp[0];
        }

        $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['date'] = strtotime($ob['TIMESTAMP_X']);
        $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['comment'] = html_entity_decode($ob['PREVIEW_TEXT']);
        $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['rating'] = $ob['DETAIL_TEXT'];

        if($ob['PROPERTY_photos_VALUE']){
            $arFile = CFile::GetFileArray($ob['PROPERTY_photos_VALUE']);
            if($arFile)
                $arResult["RESTORAN_ITEMS"]['last_comment'][$reviews_count]['photos'] = $arFile['SRC'];
        }


        $reviews_count++;
    }

    $res = CIBlockElement::GetList(Array('timestamp_x'=>'DESC'), $arFilter, false, false, $arSelect);
    if($review_full_count = $res->SelectedRowsCount())
    {
        $arResult["RESTORAN_ITEMS"]['reviewCount'] = $review_full_count;
    }
    /****LAST_COMMENT****/

    /****P_N_COMMENTS_COUNT****/
    $arSelect = Array("ID");
    $arFilter = Array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>$arResult['ID'],'>DETAIL_TEXT'=>3);
    $res = CIBlockElement::GetList(Array('ID'=>'DESC'), $arFilter, false, false, $arSelect);
    $arResult["RESTORAN_ITEMS"]['POSITIVE_COMMENTS_COUNT'] = $res->SelectedRowsCount();

    $arFilter = Array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>$arResult['ID'],'<=DETAIL_TEXT'=>3);
    $res = CIBlockElement::GetList(Array('ID'=>'DESC'), $arFilter, false, false, $arSelect);
    $arResult["RESTORAN_ITEMS"]['NEGATIVE_COMMENTS_COUNT'] = $res->SelectedRowsCount();
    /****P_N_COMMENTS_COUNT****/




    return $arResult["RESTORAN_ITEMS"];
}
?>