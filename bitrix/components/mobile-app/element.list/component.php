<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult = array();

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

if ($this->StartResultCache(false))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        return;
    }

    $arSelect = array_merge($arParams["FIELD_CODE"], array(
        "NAME",
        "PREVIEW_PICTURE",
    ));

    $arFilter = array (
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "IBLOCK_LID" => SITE_ID,
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "N",
    );

    $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
        $arParams["PARENT_SECTION"],
        $arParams["PARENT_SECTION_CODE"],
        array(
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        )
    );

    if($arParams["PARENT_SECTION"] > 0) {
        $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
        if($arParams["INCLUDE_SUBSECTIONS"])
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
    }
    else
    {
        $arResult["SECTION"]= false;
    }

    $rsElement = CIBlockElement::GetList(false, array_merge($arFilter, $arrFilter), false, false, $arSelect);

    $cur_key = 0;
    while ($obElement = $rsElement->GetNextElement()) {

        $arItem = $obElement->GetFields();

        $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;
        if ($bGetProperty) {
            foreach ($arParams['PROPERTY_CODE'] as $prop_name) {
                $arItem["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
            }
        }

//        if($arItem['PROPERTIES']['ICON']['VALUE']){
//            $arFile = CFile::GetFileArray($arItem['PROPERTIES']['ICON']['VALUE']);
//            $arItem['PICTURE'] = $arFile['SRC'];
//        }
        if($arItem['PREVIEW_PICTURE']){
            $arFile = CFile::GetFileArray($arItem['PREVIEW_PICTURE']);
            $arItem['PICTURE'] = $arFile['SRC'];
        }

        $FEATURE_CODE = '';
        if($arItem['PROPERTIES']['FEATURES']['VALUE']&&$arItem['PROPERTIES']['FEATURES']['VALUE']!='Y'){
            $res = CIBlockElement::GetByID($arItem['PROPERTIES']['FEATURES']['VALUE']);
            if($ar_res = $res->Fetch()){
                if($ar_res['IBLOCK_TYPE_ID']=='system'){
                    $FEATURE_CODE = $ar_res['IBLOCK_CODE'];
                }
                else {
                    $FEATURE_CODE = $ar_res['IBLOCK_TYPE_ID'];
                }
            }
//            print_r($ar_res);
        }
        elseif($arItem['PROPERTIES']['FEATURES']['VALUE']=='Y'){
            $FEATURE_CODE = $arItem['CODE'];
        }
//        FirePHP::getInstance()->info($FEATURE_CODE);
        $propsArr = array(
            'kitchen'=>'kitchen',
            'average_bill'=>'average_bill',
            'features'=>'features',
            'proposals'=>'proposals',
            'rest_group'=>'rest_group',
            'children'=>'children',
            'entertainment'=>'entertainment',
            'parking'=>'parking',
        );


        $temp_item = array('NAME'=>$arItem['NAME'],'PROP_CODE'=>$propsArr[$FEATURE_CODE]?$propsArr[$FEATURE_CODE]:$FEATURE_CODE,'PROP_VALUE'=>$arItem['PROPERTIES']['FEATURES']['VALUE'], 'COLLECTION_VALUE'=>$arItem['PROPERTIES']['FIRST_20']['VALUE'],'PICTURE'=>$arItem['PICTURE']);
        $arResult['RESTORAN_ITEMS'][$cur_key] = $temp_item;

        $cur_key++;
    }
//    FirePHP::getInstance()->info($arResult['RESTORAN_ITEMS']);


    $this->SetResultCacheKeys(array(
        "RESTORAN_ITEMS",
    ));

    $this->IncludeComponentTemplate();
}

$result_array['RESTORAN_ITEMS'] = $arResult["RESTORAN_ITEMS"];
return $result_array;
?>