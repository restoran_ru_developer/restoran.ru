<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//FirePHP::getInstance()->info(3212312);
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
    $arParams["IBLOCK_TYPE"] = "news";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";

// set sort value
$arParams["SORT_ORDER1"] = ($_REQUEST["by"])?$_REQUEST["by"]:$arParams["SORT_ORDER1"];
$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
    $arParams["SORT_BY1"] = "ACTIVE_FROM";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
    $arParams["SORT_ORDER1"]="DESC";

if(strlen($arParams["SORT_BY2"])<=0)
    $arParams["SORT_BY2"] = "SORT";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
    $arParams["SORT_ORDER2"]="ASC";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

if(!is_array($arParams["FIELD_CODE"]))
    $arParams["FIELD_CODE"] = array();

if(!is_array($arParams["PROPERTY_CODE"]))
    $arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $key=>$val)
    if($val==="")
        unset($arParams["PROPERTY_CODE"][$key]);

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"]<=0)
    $arParams["NEWS_COUNT"] = 20;

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
    $arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
    $arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
$arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
$arParams["HIDE_LINK_WHEN_NO_DETAIL"] = $arParams["HIDE_LINK_WHEN_NO_DETAIL"]=="Y";

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";


$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
    $arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
    $arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
    foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
    {
        if(in_array($PERM, $arUserGroupArray))
        {
            $bUSER_HAVE_ACCESS = true;
            break;
        }
    }
}

if($this->StartResultCache(false, array($_REQUEST["lat"], $_REQUEST["lon"], $arrFilter, $_REQUEST["page"], $_REQUEST["PAGEN_1"])))
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $rsIBlock = CIBlock::GetList(array(), array(
        "TYPE" =>$arParams["IBLOCK_TYPE"],
        "ACTIVE" => "Y",
        "CODE" => $arParams["IBLOCK_ID"],
        "SITE_ID" => SITE_ID,
        'ID'=>$arParams['IBLOCK_INT_ID']
    ));
    if($arResult = $rsIBlock->GetNext()) {
        $arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
        //SELECT
        $arSelect = array_merge($arParams["FIELD_CODE"], array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PREVIEW_PICTURE",
            "DETAIL_PICTURE",
        ));


        $arFilter = array (
            "IBLOCK_ID" => $arResult["ID"],
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE" => "Y",
            //"CHECK_PERMISSIONS" => "Y",
        );

        if($arParams["CHECK_DATES"])
            $arFilter["ACTIVE_DATE"] = "Y";

        $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
            $arParams["PARENT_SECTION"],
            $arParams["PARENT_SECTION_CODE"],
            array(
                "GLOBAL_ACTIVE" => "Y",
                "IBLOCK_ID" => $arResult["ID"],
            )
        );

        if($arParams["PARENT_SECTION"] > 0) {
            $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
            if($arParams["INCLUDE_SUBSECTIONS"])
                $arFilter["INCLUDE_SUBSECTIONS"] = "Y";

            $arResult["SECTION"]= array("PATH" => array());
            $rsPath = GetIBlockSectionPath($arResult["ID"], $arParams["PARENT_SECTION"]);
            $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
            while($arPath=$rsPath->GetNext())
            {
                $arResult["SECTION"]["PATH"][] = $arPath;
            }
        }
        else
        {
            $arResult["SECTION"]= false;
        }

        //ORDER BY
        $arSort = array(
            $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"]
        );


        $arResult["ITEMS"] = array();
        $arResult["ELEMENTS"] = array();


        if ($arSort["IDS"]=="ASC")
        {
            $rsElement = CIBlockElement::GetList(false, array_merge($arFilter, $arrFilter), false, false, $arSelect);
        }
        else {
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/classes/cakeiblockelement.php");
            $arS = Array("PROPERTY_LAT","PROPERTY_LON");
            $rsElement = CCakeIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, array_merge($arSelect,$arS),$arParams['DISTANCE_VALUE']);
        }




        $cur_key = 0;
        while ($obElement = $rsElement->GetNextElement()) {

            $arItem = $obElement->GetFields();

            if ($arItem["PREVIEW_PICTURE"]) {
                $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
            }
            elseif($arItem['DETAIL_PICTURE']){
                $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
            }

            $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;
            if ($bGetProperty) {
                foreach ($arParams['PROPERTY_CODE'] as $prop_name) {
                    $arItem["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
                }
            }


            $arItem["DISPLAY_PROPERTIES"] = array();
            foreach ($arParams["PROPERTY_CODE"] as $pid) {
                $prop = &$arItem["PROPERTIES"][$pid];
                if ((is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0) ||
                    (!is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0)
                ) {
                    $arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
                }
            }


            $arResult["RESTORAN_ITEMS"][$cur_key]['ID'] = $arItem['ID'];
            $arResult["RESTORAN_ITEMS"][$cur_key]['NAME'] = html_entity_decode($arItem['NAME']);
            if($arItem['PREVIEW_PICTURE']['SRC']){
                $arResult["RESTORAN_ITEMS"][$cur_key]['PREVIEW_PICTURE'] = 'http://www.restoran.ru' . $arItem['PREVIEW_PICTURE']['SRC'];
            }



            /**COMMON_LIST_PROPS**/
            if (is_array($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['subway'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['subway'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['address'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['address'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['address']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['lat'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['lat'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['lat']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['lon'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['lon'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['lon']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['average_bill'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['average_bill'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['kitchen'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['kitchen'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['kitchen']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours_google'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours_google'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['opening_hours_google']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours'][] = strip_tags($prop_val);
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['opening_hours'] = strip_tags($arItem["DISPLAY_PROPERTIES"]['opening_hours']['DISPLAY_VALUE']);
            }

            if (is_array($arItem["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE'])) {
                foreach ($arItem["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE'] as $prop_val) {
                    $arResult["RESTORAN_ITEMS"][$cur_key]['phone'][] = clearUserPhoneMobileApp(strip_tags($prop_val));
                }
            }
            else {
                $arResult["RESTORAN_ITEMS"][$cur_key]['phone'] = clearUserPhoneMobileApp(strip_tags($arItem["DISPLAY_PROPERTIES"]['phone']['DISPLAY_VALUE']));
            }
            /**COMMON_LIST_PROPS**/

            $cur_key++;
        }

        /***COLLECTIONS SORT***/
        if($arrFilter['ID']&&$arParams["SORT_BY1"]=='IDS'){
            foreach($arrFilter['ID'] as $parent_news_id){
                foreach($arResult["RESTORAN_ITEMS"] as  $key=> $one_item){
                    if($one_item['ID']==$parent_news_id)
                        $new_items_arr['ITEMS'][] = $one_item;
                }
            }
            $arResult["RESTORAN_ITEMS"] = $new_items_arr['ITEMS'];
        }

        $this->SetResultCacheKeys(array(
            "RESTORAN_ITEMS",
        ));
    }
    else
    {
        $this->AbortResultCache();
        ShowError(GetMessage("T_NEWS_NEWS_NA"));
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
    }
}

if(isset($arResult["ID"]))
{
    $result_array['RESTORAN_ITEMS'] = $arResult["RESTORAN_ITEMS"];
    return $result_array;
}
?>