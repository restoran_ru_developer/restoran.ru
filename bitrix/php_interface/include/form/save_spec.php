<?php
if ($_REQUEST["block"]&&($_REQUEST["apply"]=="Применить"||$_REQUEST["save"]=="Сохранить"))
{                
    $_REQUEST["DETAIL_TEXT"] = make_detail_text();
    $_POST["DETAIL_TEXT"] = make_detail_text();    
    unset($_REQUEST["block"]);    
}
if (($_REQUEST["apply"]=="Применить"||$_REQUEST["save"]=="Сохранить"))
{           
    $res = CIBlock::GetByID((int)$_REQUEST["IBLOCK_ID"]);
    if($ar_res = $res->GetNext())
      $iblock_code = $ar_res['CODE'];
    //Берем ID свойств спецпроекта
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"RESTORAN"));
    if($prop_fields = $properties->GetNext())
    {
      $property_restoran_id = $prop_fields["ID"];
      $property_restoran_iblock_id = $prop_fields["LINK_IBLOCK_ID"];      
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"subway"));
    if($prop_fields = $properties->GetNext())
    {
      $property_subway_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"bill"));
    if($prop_fields = $properties->GetNext())
    {
      $property_bill_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"area"));
    if($prop_fields = $properties->GetNext())
    {
      $property_area_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"lat"));
    if($prop_fields = $properties->GetNext())
    {
      $property_lat_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"lon"));
    if($prop_fields = $properties->GetNext())
    {
      $property_lon_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "NAME"=>"Загородные"));
    if($prop_fields = $properties->GetNext())
    {
      $property_out_city_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "NAME"=>"Пригороды"));
    if($prop_fields = $properties->GetNext())
    {
      $property_out_city_id = $prop_fields["ID"];
    }       
    if ($property_restoran_id&&$property_restoran_iblock_id)
    {        
        foreach($_POST["PROP"][$property_restoran_id] as $rest_vals)
        {
            if ($rest_vals)
            {
                if (is_array($rest_vals)&&count($rest_vals["VALUE"])>0)
                {
                    $rest_id = $rest_vals["VALUE"];
                }
                else                    
                    $rest_id = $rest_vals;
            }
            
        }        
        if ($rest_id)
        {
            
            if ($property_subway_id)
            {
                //Забираем у ресторана метро
                $subway = array();
                $subway_name = array();
                $subway[] = ""; 
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "subway"));
                while ($ob = $res->GetNext())
                {
                    $subway[] = $ob['VALUE'];
                    $r = CIBlockElement::GetByID($ob['VALUE']);
                    if ($ar = $r->Fetch())
                    {
                        $subway_name[] = $ar["NAME"]." [".$ar["ID"]."]";
                    }
                }                                           
                $_POST["PROP"][$property_subway_id] = $subway;                
                $_REQUEST["PROP"][$property_subway_id] = $subway;                
                $_POST["visual_PROPx".$property_subway_id."x"] = implode("\n\r",$subway_name);                
                $_REQUEST["visual_PROPx".$property_subway_id."x"] = implode("\n\r",$subway_name);                
            }
            if ($property_bill_id && !substr_count($iblock_code, "new_year"))
            {
                //Забираем у ресторана средний счет
                $bill = array();                
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "average_bill"));
                while ($ob = $res->GetNext())
                {
                    $bill[] = $ob['VALUE'];
                }                
                $_POST["PROP"][(int)$property_bill_id] = $bill;
            }
            if ($property_area_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "area"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_area_id] = $area;
            }
            if ($property_out_city_id)
            {
                //Забираем у ресторана район
                $out_city = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "out_city"));
                while ($ob = $res->GetNext())
                {
                    $out_city[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_out_city_id] = $out_city;
            }
            if ($property_lat_id)
            {
                //Забираем у ресторана район
                $lat = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "lat"));
                while ($ob = $res->GetNext())
                {
                    $lat[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_lat_id] = $lat;
            }
            if ($property_lon_id)
            {
                //Забираем у ресторана район
                $lat = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "lon"));
                while ($ob = $res->GetNext())
                {
                    $lon[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_lon_id] = $lon;
            }
        }
    }
    $PROP = $_POST["PROP"];    
}
function make_detail_text(){
	$EXIT = "";
	$SHAG=1;
	
	//var_dump($_REQUEST["block"]);
	//var_dump($_FILES);
	$RESTORANS_IDs=array();
	foreach($_REQUEST["block"] as $BL_NAME=>$BL){
		
		//ШАГ
		if($BL["type"]=="add_step"){
			
			$FL=form_file_array($BL_NAME);
			if($FL["name"]!=""){
				$fid = CFile::SaveFile($FL);
			}else $fid=$BL["fid"];
			
			
			$EXIT.='<div class="recept_text shag bl"><h2>Шаг '.$SHAG.'</h2><div class="left photos123" style="position:relative; width:238px;margin-bottom: 10px;"><img id="pic'.$fid.'" src="#FID_'.$fid.'#" class="pic" width="232"/><img id="pic'.$fid.'big" style="display:none" src="#FID_'.$fid.'#" width="728"/></div><div style="" class="txt">'.$BL["txt"].'</div><div class="clear"></div></div>';
 			
 			$SHAG++;
		}
		
		//ПРЯМАЯ РЕЧЬ
		if($BL["type"]=="add_pr"){
			//формируем массив файла
			
			$FL=form_file_array($BL_NAME);
			if($FL["name"]!=""){
				$fid = CFile::SaveFile($FL);
			}else $fid=$BL["fid"];
			
			
			
			$EXIT.='<br/><table class="direct_speech bl"><tbody><tr> <td width="170" style="border-image: initial; "><div class="author"><img src="#FID_'.$fid.'#"  class="pic" width="230"/> </div></td> <td width="150" style="border-image: initial; "> <span class="uppercase txt1">'.$BL["txt1"].'</span><br /><i class="txt2">'.$BL["txt2"].'</i> </td> <td class="font14" style="border-image: initial; "> <i class="txt3">&laquo;'.$BL["txt3"].'&raquo;</i> </td> </tr></tbody></table>';
		}

		//ВСТАВКА ТЕКСТА
		if($BL["type"]=="add_text"){
			if (!$BL["zag"])
                            $EXIT.='<div class="vis_red bl"><div style="" class="txt">'.$BL["val"].'</div></div>';
                        else
                            $EXIT.='<div class="vis_red bl"><h2>'.$BL["zag"].'</h2><div style="" class="txt">'.$BL["val"].'</div></div>';
		}
		
		//ВСТАВКА РЕСТОРАНА
		if($BL["type"]=="add_rest"){
			
			if(is_array($BL["selected"])){
				for($i=0; $i<count($BL["selected"]);$i++){
					if($BL["selected"][$i]=="") unset($BL["selected"][$i]);
				}
			}
			
			$RESTs="";
			foreach($BL["selected"] as $rid){
				$RESTs.="#REST_".$rid."# ";
			}
			
			$BL["txt"] = str_replace("<p></p>","",$BL["txt"]);
			$RESTORANS_IDs = array_merge($RESTORANS_IDs, $BL["selected"]);
			$EXIT.='<div class="restoran_text bl"><div class="left article_rest">'.$RESTs.'</div><div class="right article_rest_text">'.$BL["txt"].'</div><div class="clear"></div></div>';
		}
		
		//ФОТОГАЛЕРЕЯ
		if($BL["type"]=="add_photos"){
		
			
			//нужно собрать массив
			//сначала добавляем уже загруженные картинки
			$PICS=array();
			if(isset($BL["file"]) && is_array($BL["file"])){
				for($i=0;$i<count($BL["file"]);$i++){
                                    if ($BL["file"][$i])
					$PICS[]=array("FID"=>$BL["file"][$i], "DESCR"=>$BL["descr"][$i]);
				}
			}
                        
                        if(isset($BL["fid_new"]) && is_array($BL["fid_new"])){
				for($i=0;$i<count($BL["fid_new"]);$i++){
					$PICS[]=array("FID"=>$BL["fid_new"][$i], "DESCR"=>$BL["descr_new"][$i]);
				}
			}
                        
			//var_dump($PICS);                        
			//var_dump($BL_NAME);			
			$FL=form_file_array($BL_NAME);
			//var_dump($FL);
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			//	echo 1;
				foreach($FL as $F){
                                    if($F["name"]!=""){
					$fid = CFile::SaveFile($F,"publications");
                                        if ($fid)
                                            $PICS[]=array("FID"=>$fid, "DESCR"=>$BL["descr_ja"][$i]);
                                    }
					$i++;
				}
			}else{
			//	echo 2;
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"publications");
					$PICS[]=array("FID"=>$fid, "DESCR"=>$BL["descr_ja"][$i]);
				}
			}
		
			
			$EXIT.='<div class="articles_photo bl">';
			
				if(count($PICS)>1){
					$EXIT.='<div class="left scroll_arrows">';
					$EXIT.='<a class="prev browse left" ></a><span class="scroll_num">1</span>/'.count($PICS).'<a class="next browse right" ></a>';
					$EXIT.='</div><div class="clear"></div>';
  				}	
  				for($i=0;$i<count($PICS);$i++){
  					if($i==0){
  						//первая картинка
  						$EXIT.='<div class="img"><div class="images"><img src="#FID_'.$PICS[$i]["FID"].'#" height="512" class="first_pic" /></div><p><i>'.$PICS[$i]["DESCR"].'</i></p></div>';
  						if(count($PICS)>1){
  							$EXIT.='<div class="special_scroll"><div class="scroll_container">';
    						$EXIT.='<div class="item active"> <img src="#FID_'.$PICS[$i]["FID"].'#" alt="'.$PICS[$i]["DESCR"].'" align="bottom"  class="pic"/> </div>';	
  						}
  					}else{
  						$EXIT.='<div class="item"> <img src="#FID_'.$PICS[$i]["FID"].'#" alt="'.$PICS[$i]["DESCR"].'" align="bottom"  class="pic"/> </div>';
  					}
  				}
  				
  				if(count($PICS)>1) $EXIT.='</div></div>';
  				
  				$EXIT.='</div>';
		}
		
		
		
		//ВИДЕО
		if($BL["type"]=="add_video"){
			
			$FL=form_file_array($BL_NAME);
			if($FL["name"]!=""){
				$fid = CFile::SaveFile($FL);
			}else $fid=$BL["fid"];
			
			if($fid!="") $EXIT.='<div class="video bl"><a class="myPlayer" href="#FID_'.$fid.'#"></a></div><br/>';
 			
 			$SHAG++;
		}
		
		
		//ВИДЕО
		if($BL["type"]=="add_video_code"){
			
			$EXIT.='<div class="video_code bl">'.$BL["val"].'</div><br/>';
 			
		}
	
	}

	
	

	return $EXIT;
}
function form_file_array($BL_NAME){
	$FAR=array();
	//var_dump($_FILES);
	foreach($_FILES["block"] as $A=>$C){		
		$VAL="";
		if(is_array($C[$BL_NAME])){
			foreach($C[$BL_NAME] as $V) $VAL=$V;
		}
		$FAR[$A]=$V;
	}
	
	if(is_array($FAR["name"])){
		$RET=array();
		foreach($FAR as $N=>$V){
			for($i=0;$i<count($V);$i++){
				$RET[$i][$N]=$V[$i];
			}
		}
		//var_dump($RET);
		return $RET;
	}else return $FAR;
}
?>
