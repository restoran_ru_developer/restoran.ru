<?
class WebFormHandler {
    function onAfterResultStatusChangeHandler($WEB_FORM_ID, $RESULT_ID, $NEW_STATUS_ID, $CHECK_RIGHTS) {
        $newBookingStatID = 3;
        $tableBookedStatID = 5;

        // бронирование
        if($WEB_FORM_ID == 3) {
            // send sms
            if (CModule::IncludeModule("sozdavatel.sms")) {
                switch($NEW_STATUS_ID) {
                    case $newBookingStatID:
                        $message = "Заявка на бронь принята. Номер брони №".$RESULT_ID;
                    break;
                    case $tableBookedStatID:
                        $message = "Столик забронирован. Номер брони №".$RESULT_ID;
                    break;
                }
                // get phone number
                $arResult = Array();
                $arAnswer2 = Array();
                $arAnswer = CFormResult::GetDataByID(
                    $RESULT_ID,
                    array("SIMPLE_QUESTION_482"),
                    $arResult,
                    $arAnswer2
                );
                CSMS::Send($message, $arAnswer["SIMPLE_QUESTION_482"][0]["USER_TEXT"], "UTF-8");
            }

            AddMessage2Log('форма брони 3');
        }
    }
}
?>