<?
/*
 * Class for manual redirect form link to link
 */
class RedirectURIClass {
    // get list of redirect links
    private function getRedirectURI() {
        if(CModule::IncludeModule("iblock")) {
            $obCache = new CPHPCache;
            //$life_time = 60*60*2;
            $life_time = 1;
            $cache_id = date("m.d.y")."redirect112222333";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $arRedirectLinks = $vars["REDIRECT_LINKS"];
            } else {
                $rsLinks = CIBlockElement::GetList(
                    Array("SORT"=>"ASC"),
                    Array(
                        "ACTIVE" => "Y",
                        "IBLOCK_ID" => "229", // iblock with links
                    ),
                    false,
                    false,
                    Array("ID", "NAME", "PROPERTY_FROM_URL", "PROPERTY_REDIRECT")
                );
                while($arLinks = $rsLinks->GetNext()) {
                    $arRedirectLinks[$arLinks["PROPERTY_FROM_URL_VALUE"]] = $arLinks["PROPERTY_REDIRECT_VALUE"];
                }
                if($obCache->StartDataCache()) {
                    $obCache->EndDataCache(array(
                        "REDIRECT_LINKS" => $arRedirectLinks
                    ));
                }
            }

            return $arRedirectLinks;
        }
    }

    // redirect
    public function redirectURI($curURI) {
        $arLinks = $this->getRedirectURI();        
        if(is_array($arLinks) && $arLinks[$curURI])
            LocalRedirect($arLinks[$curURI], false, "301 Moved permanently");
    }
}
?>