<?
class RestUsers {
    function OnAfterUserAddHandler(&$arFields) {
        // send email to user
        if($arFields["UF_VK_USER_ID"] || $arFields["UF_FB_USER_ID"] || $arFields["PERSONAL_MAILBOX"]) {
            CUser::SendUserInfo($arFields["ID"], SITE_ID, "");
        }

        // subscribe user
//        if(CModule::IncludeModule("subscribe")) {
//            $arFieldsSub = Array(
//                "USER_ID" => $arFields["ID"],
//                "FORMAT" => "html",
//                "EMAIL" => $arFields["EMAIL"],
//                "ACTIVE" => "Y",
//                "RUB_ID" => Array(4),
//                "CONFIRMED" => "Y",
//                "SEND_CONFIRM" => "N"
//            );
//            $subscr = new CSubscription;
//
//            //can add without authorization
//            $ID = $subscr->Add($arFieldsSub);
//            if($ID)
//                $subscr->Authorize($ID);
//        }
//        
        /*CModule::IncludeModule("sale");			
            CSaleUserAccount::UpdateAccount(
                $arFields["ID"],
                300,
                "RUB",
                "Регистрация пользователя" 
            );*/        
        CModule::IncludeModule("sale");			
        if(!CSaleUserAccount::GetByUserID($arFields["ID"], "RUB")){ //Если у текущего юзера нет аккаунта
            $arFields2 = Array("USER_ID" => $arFields["ID"], "CURRENCY" => "RUB", "CURRENT_BUDGET" => 0);
            CSaleUserAccount::Add($arFields2);  //создаем его
         }                 
//         if ((int)$arFields["PERSONAL_PAGER"])
//         {
//             CSaleUserAccount::UpdateAccount(
//                    $arFields["PERSONAL_PAGER"],
//                    50,
//                    "RUB",
//                    "Приглашение пользователя +50"
//            );
//         }
//         if ($arFields["UF_VK_USER_ID"] || $arFields["UF_FB_USER_ID"])
//         {
//            if ($arFields["PERSONAL_PHOTO"])
//            {
//                CSaleUserAccount::UpdateAccount(
//                    $arFields["ID"],
//                    350,
//                    "RUB",
//                    "Новый пользователь с аватаркой +350"
//                );
//            }
//            else
//            {
//                CSaleUserAccount::UpdateAccount(
//                    $arFields["ID"],
//                    300,
//                    "RUB",
//                    "Новый пользователь +300"
//                );
//            }
//         }

    }
    
    function OnAfterUserRegisterHandler(&$arFields)
    {
        CModule::IncludeModule("sale");		
        if(!CSaleUserAccount::GetByUserID($arFields["USER_ID"], "RUB")){ //Если у текущего юзера нет аккаунта
            $arFields = Array("USER_ID" => $arFields["USER_ID"], "CURRENCY" => "RUB", "CURRENT_BUDGET" => 0);
            CSaleUserAccount::Add($arFields);  //создаем его
         }
        		
//        CSaleUserAccount::UpdateAccount(
//            $arFields["USER_ID"],
//            300,
//            "RUB",
//            "Новый пользователь +300"
//        );
           
        //если такой пользователь есть в приглашениях
        CModule::IncludeModule("iblock");
        $arFilter = Array(
            "IBLOCK_TYPE"=>"invites", 
            "ACTIVE"=>"Y", 
            "PROPERTY_NA_USERS"=>$arFields["EMAIL"],
            "!PROPERTY_NA_USERS"=>false
        );
        $res = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter);
        while($ar_fields = $res->GetNext())
        {
            $ar[] = $ar_fields;
            $res1 = CIBlockElement::GetProperty($ar_fields["IBLOCK_ID"], $ar_fields["ID"], "sort", "asc", array("CODE" => "NA_USERS"));
            while ($ob = $res1->GetNext())
            {
                if ($ob["VALUE"]!=$arFields["EMAIL"])
                    $ar_fields["NA_USERS"][] = $ob['VALUE'];
            }
            $res1 = CIBlockElement::GetProperty($ar_fields["IBLOCK_ID"], $ar_fields["ID"], "sort", "asc", array("CODE" => "USERS"));
            while ($ob = $res1->GetNext())
            {
                $ar_fields["USERS"][] = $ob['VALUE'];
            }
            $ar_fields["USERS"][] = $arFields["USER_ID"];
            CIBlockElement::SetPropertyValuesEx($ar_fields["ID"], false, array("NA_USERS" => $ar_fields["NA_USERS"]));
            CIBlockElement::SetPropertyValuesEx($ar_fields["ID"], false, array("USERS" => $ar_fields["USERS"]));
            $created_by = $ar_fields["CREATED_BY"];            
        }
//        CSaleUserAccount::UpdateAccount(
//            $created_by,
//            50,
//            "RUB",
//            "Приглашение друга +50"
//            );
    }
    
    function OnBeforeUserAddHandler (&$arFields)
    {
        $arFields["UF_HIDE_CONTACTS"] = 1;
    }
    
    function OnBeforeUserRegisterHandler(&$arFields) {
        global $APPLICATION;
        
        // set login from email
        $arFields["LOGIN"] = $arFields["EMAIL"];
        if (!$arFields["PERSONAL_PROFESSION"])
        {
            $temp = explode("@",$arFields["EMAIL"]);
            $arFields["PERSONAL_PROFESSION"] = $temp[0];
        }
        /*$temp = explode(" ",$arFields["NAME"]);
        $arFields["NAME"] = $temp[1];
        $arFields["LAST_NAME"] = $temp[0];*/
        $arFields["UF_HIDE_CONTACTS"] = 1;
        // get city by ip
        
        //$arFields["PERSONAL_CITY"] = getCityByIP();

        /*if(!$arFields["UF_USER_AGREEMENT"]) {
            $APPLICATION->ThrowException("Вы должны принять пользовательское соглашение!");
            return false;
        }*/

        // send user confirm sms
        /*if (CModule::IncludeModule("sozdavatel.sms")) {
            // set confirm code and user e-mail to session
            session_start();
            $_SESSION["REGISTER_CHECKWORD"] = $arFields["CONFIRM_CODE"];
            $_SESSION["REGISTER_USER_EMAIL"] = $arFields["LOGIN"];

            $message = "Код для подтверждения регистрации: ".$arFields["CONFIRM_CODE"];
            CSMS::Send($message, $arFields["PERSONAL_PHONE"], "UTF-8");
        }*/        

    }
    
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $arFields["PERSONAL_PHONE"] = str_replace("+7", "", $arFields["PERSONAL_PHONE"]);
        $arFields["PERSONAL_PHONE"] = preg_replace("/\D/", "", $arFields["PERSONAL_PHONE"]);
    }
    
    function getPersonalLink()
    {
        global $USER;
        //if (!$_SESSION["USER_TYPE"])
        {
            $groups = $USER->GetUserGroupArray();
            $groups_code = array();
            foreach($groups as $group)
            {
                $rsGroup = CGroup::GetByID($group);
                $arGroup = $rsGroup->Fetch();
                if ($arGroup["STRING_ID"])
                    $groups_code[] = preg_replace("/_([A-Za-z]+)/","",$arGroup["STRING_ID"]);
                //$groups_code[] = str_replace("_".CITY_ID,"",$arGroup["STRING_ID"]);
                
            }
            if (in_array("editor", $groups_code))
            {
                $_SESSION["USER_TYPE"] = "redactor";
                return "redactor";
            }
            elseif(in_array("restorator", $groups_code))
            {
                $_SESSION["USER_TYPE"] = "restorator";
                return "restorator";
            }
            else
            {
                $_SESSION["USER_TYPE"] = "user";
                return "user";
            }
        }
       // else
         //   return $_SESSION["USER_TYPE"];
    }
    
    function GetCountFriends($id)
    {
        global $DB;
        $id = $DB->ForSql($id);
        $sql = "SELECT COUNT(ID) as COUNT FROM b_sonet_user_relations WHERE (FIRST_USER_ID=".$id." OR SECOND_USER_ID=".$id.") AND RELATION='F'";
        $res = $DB->Query($sql);
        $count = $res->Fetch();
        $count = $count["COUNT"];
        return $count;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	* --------------------------------
	* Функция addUser2UserSubscribtion
	* --------------------------------
	* Подписывает одного пользователя на другого. При включённом toggle - отписывает при повторном обращении
	*
	* @$userToAdd — пользователь, на которого подписываем
	* @$userCurrent — пользователь которого подписать на $userToAdd. По умолчанию - текущий пользователь
	* @$toggle — режим "переключателя": отписываем пользователя, если подписан; подписываем пользователя, если отписан. По умолчанию false — только подписывать.
	* @$fieldName — имя пользотвательского поля, в котором хранится список пользователей, на которых подписали данного. По умолчанию "UF_USER_SUBSCRIBE"
	*/
	
	function addUser2UserSubscribtion($userToAdd, $userCurrent = false,  $toggle = false, $fieldName = "UF_USER_SUBSCRIBE")
	{
		//Подготавливаем сведения
		$userToAdd = (int)$userToAdd;
		if($userToAdd < 1) return false;
		if(empty($userCurrent))
		{
			global $USER;
			$userCurrent = $USER->GetID();
		}
		
		//Получение списка текущих подписок
		$arFilter = array(
			"ID" => $userCurrent
		);
		$arSelect = array(
			"SELECT" => array(
				$fieldName 
			)
		);
		$user = CUser::GetList( ($by="name"), ($order="asc"), $arFilter, $arSelect );
		$userArr = $user->Fetch();
		$subscribeList = $userArr["UF_USER_SUBSCRIBE"];

		
		
		//Подписка
		if(!$toggle)
		{
			if(in_array($userToAdd, $subscribeList))
			{
				//Если пользователь уже подписан, возвращем true
				return true;
			}
			else
			{
				//Пользователь не подписан, подписываем
				$subscribeList[] = $userToAdd;
			}
		}
		else
		{
			if(in_array($userToAdd, $subscribeList))
			{
				//Если пользователь подписан, удаляем его из массива
				foreach($subscribeList as $k => $v)
				{
					if($v == $userToAdd)
					{
						unset($subscribeList[$k]);
						break;
					}
				}
				$toret = 2;
			}
			else
			{
				$subscribeList[] = $userToAdd;
				$toret = 1;
			}
			
			
		}
		
		//Непосредственно подписываем
		$oUser = new CUser;
		$result = $oUser->Update($userCurrent, array( $fieldName  => $subscribeList));
		
		//Если обновилось и режим "переключателя" - уточняем, что было совершено
		if($result && $toggle)
		{
			$result = $toret;
		}
		return $result;
	}
	
	
	
	

	
	
	/**
	* --------------------------------------
	* Функция deleteUserFromUserSubscribtion
	* --------------------------------------
	* @$userToDelete — пользователь, от которого надо отписать
	* @$userCurrent — пользователь которого отписываем от $userToDelete. По умолчанию - текущий пользователь
	* @$fieldName — имя пользотвательского поля, в котором хранится список пользователей, на которых подписали данного. По умолчанию "UF_USER_SUBSCRIBE"
	*/
	
	function deleteUserFromUserSubscribtion($userToDelete, $userCurrent = false, $fieldName = "UF_USER_SUBSCRIBE")
	{
		//Подготавливаем сведения
		$userToDelete = (int)$userToDelete;
		if($userToDelete < 1) return false;
		if(empty($userCurrent))
		{
			global $USER;
			$userCurrent = $USER->GetID();
		}
		
		
		//Получение списка текущих подписок
		$arFilter = array(
			"ID" => $userCurrent
		);
		$arSelect = array(
			"SELECT" => array(
				$fieldName 
			)
		);
		$user = CUser::GetList( ($by="name"), ($order="asc"), $arFilter, $arSelect );
		$userArr = $user->Fetch();
		$subscribeList = $userArr["UF_USER_SUBSCRIBE"];
		
		
		//Удаляем пользователя из подписки
		foreach($subscribeList as $k => $v)
		{
			if($v == $userToDelete)
			{
				unset($subscribeList[$k]);
				break;
			}
		}
		
		//Непосредственно обновляем
		$oUser = new CUser;
		$result = $oUser->Update($userCurrent, array( $fieldName  => $subscribeList));
		return $result;
	}
	
	
	
	
	
	
	
	
	
	/**
	* --------------------------------------
	* Функция checkUser2UserSubscribtion
	* --------------------------------------
	* Проверяет, подписан ли $userCurrent на пользователя $userToCheck  
	*
	* @$userToCheck — пользователь, от которого надо отписать
	* @$userCurrent — пользователь которого отписываем от $userToCheck. По умолчанию - текущий пользователь
	* @$fieldName — имя пользотвательского поля, в котором хранится список пользователей, на которых подписали данного. По умолчанию "UF_USER_SUBSCRIBE"
	*/
	
	function checkUser2UserSubscribtion($userToCheck, $userCurrent = false, $fieldName = "UF_USER_SUBSCRIBE")
	{
		//Подготавливаем сведения
		$userToCheck = (int)$userToCheck;
		if($userToCheck < 1) return false;
		if(empty($userCurrent))
		{
			global $USER;
			$userCurrent = $USER->GetID();
		}
		
		
		//Получение списка текущих подписок
		$arFilter = array(
			"ID" => $userCurrent
		);
		$arSelect = array(
			"SELECT" => array(
				$fieldName 
			)
		);
		$user = CUser::GetList( ($by="name"), ($order="asc"), $arFilter, $arSelect );
		$userArr = $user->Fetch();
		$subscribeList = $userArr["UF_USER_SUBSCRIBE"];
		
		//Возвращаем
		return in_array($userToCheck, $subscribeList);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
?>