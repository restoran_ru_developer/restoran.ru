<?
class RestBlogPost {
    function OnBeforePostAddHandler(&$arFields) {
        global $APPLICATION;

        if(CModule::IncludeModule("blog")) {
            // check unique post code
            $rsBlogPost = CBlogPost::GetList(
                Array("ID"=>"DESC"),
                Array(
                    "CODE" => $arFields["CODE"]
                ),
                false,
                false,
                Array()
            );
            if($arBlogPost = $rsBlogPost->Fetch()) {
                $APPLICATION->ThrowException('Адрес сообщения должен быть уникален!');
                return false;
            }
        }
    }
}
?>