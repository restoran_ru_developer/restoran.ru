<?
class RestSearch {
    
   function BeforeIndexHandler($arFields)
   {

      if(CModule::IncludeModule("iblock")):
          global $USER;
            global $DB;       
         $arFields["BODY"] = str_replace("\r","",$arFields["BODY"]);    
         $arFields["BODY"] = str_replace("ул.","улица",$arFields["BODY"]);    
         $arFields["BODY"] = str_replace("пр.","проспект",$arFields["BODY"]);
        if($arFields["MODULE_ID"] == "iblock" && ($arFields["PARAM2"] == 12 || $arFields["PARAM2"] == 11 || $arFields["PARAM2"] == 2820 || $arFields["PARAM2"] == 2636 || $arFields["PARAM2"] == 2821 ) && substr($arFields["ITEM_ID"], 0, 1) != "S")
        {
//            $arFields["PARAMS"]["iblock_section"] = array();
//            //Получаем разделы привязки элемента (их может быть несколько)
//            $rsSections = CIBlockElement::GetElementGroups($arFields["ITEM_ID"], true);
//            while($arSection = $rsSections->Fetch())
//            {
//                //Сохраняем в поисковый индекс
//                $arFields["PARAMS"]["iblock_section"][] = $arSection["ID"];
//            }
            $res = CIBlockElement::GetProperty($arFields["PARAM2"], $arFields["ITEM_ID"], "sort", "asc", array("CODE" => "sleeping_rest"));
            if ($ob = $res->GetNext())
            {                
                //Сохраняем в поисковый индекс
                if ($ob["VALUE_ENUM"]=="Да")                
                    $arFields["PARAMS"]["sleeping"] = "Y";
                else
                    $arFields["PARAMS"]["sleeping"] = "N";                
            }

            // ресторан сети
            $res = CIBlockElement::GetProperty($arFields["PARAM2"], $arFields["ITEM_ID"], "sort", "asc", array("CODE" => "NETWORK_REST"));
            if ($ob = $res->GetNext())
            {
                if ($ob["VALUE_ENUM"]=="Да")
                    $arFields["PARAMS"]["NETWORK_REST"] = "Y";
                else
                    $arFields["PARAMS"]["NETWORK_REST"] = "N";
            }


            //Добавляем ресторанную группу 
            //
            $VALUES = array();
                $db_props = array();
                $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"rest_group","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                    {
                        $VALUES[] = $ar_re["NAME"];
                        $VALUES[] = str_replace(",","",$ar_re["PREVIEW_TEXT"]);
                    }
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                //Добавляем Особенности
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"features","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"];                      
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                //Добавляем Метро
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"subway","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = "метро ".$ar_re["NAME"];  
                    }
                }
                $v = implode(", ",$VALUES);
                $arFields["BODY"] .= " \n".$v;    
                
                //Добавляем Средний счет
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"average_bill","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"]; 
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v;  
                
                //Добавляем Кухню
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"kitchen","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"]." кухня";
                    }
                }
                $v = implode(", ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем Количество человек
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"kolichestvochelovek","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"];                    
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем Загородные
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"out_city","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"];  
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем Метро доставки
//                $VALUES = array();
//                $db_props = array();
//                $ar_props = array();
//                $db_props = CIBlockElement::GetProperty(                        
//                                        $arFields["PARAM2"],         
//                                        $arFields["ITEM_ID"],        
//                                        array("sort" => "asc"),      
//                                        Array("CODE"=>"subway_dostavka","EMPTY"=>"N")); 
//                while($ar_props = $db_props->Fetch())
//                {
//                    if ($ar_props["VALUE"])
//                    {
//                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
//                        $re = $DB->Query($sql);
//                        if ($ar_re = $re->Fetch())
//                            $VALUES[] = $ar_re["NAME"]; 
//                    }
//                }
//                $v = implode(" ",$VALUES);
//                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем Тип
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"type","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"];  
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем Кредитные карты
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"credit_cards","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"];  
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v;
                
                //Добавляем Идеальное место для
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"ideal_place_for","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = "Идеальное место для ".$ar_re["NAME"];                          
                    }
                }
                $v = implode(", ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем Парковка
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"parking","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"]." парковка";     
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                
                //Добавляем район
                $VALUES = array();
                $db_props = array();
                $ar_props = array();
                $db_props = CIBlockElement::GetProperty(                        
                                        $arFields["PARAM2"],         
                                        $arFields["ITEM_ID"],        
                                        array("sort" => "asc"),      
                                        Array("CODE"=>"area","EMPTY"=>"N")); 
                while($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                    {
                        $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                        $re = $DB->Query($sql);
                        if ($ar_re = $re->Fetch())
                            $VALUES[] = $ar_re["NAME"]." район";     
                    }
                }
                $v = implode(" ",$VALUES);
                $arFields["BODY"] .= " \n".$v; 
                $arFields["TITLE"] .= " ".str_replace(",", "", $arFields["TAGS"]);
        }
        if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 139 && substr($arFields["ITEM_ID"], 0, 1) != "S") //Рецепты
        {
            //Добавляем категорию
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"cat","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
            
            //Добавляем основной ингредиент
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"osn_ingr","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
            
            //Добавляем время приготовления
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"prig_time","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
            
            //Добавляем кухню
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"cook","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"]." кухня";     
                }
            }
            $v = implode(", ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
            
            //Добавляем повод
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"povod","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
            
            //Добавляем предпочтение
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"predp","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v; 
            
            //Добавляем приготовление
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"prig","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v;
            
            //Добавляем сложность
            $VALUES = array();
            $db_props = array();
            $ar_props = array();
            $db_props = CIBlockElement::GetProperty(                        
                                    $arFields["PARAM2"],         
                                    $arFields["ITEM_ID"],        
                                    array("sort" => "asc"),      
                                    Array("CODE"=>"slognost","EMPTY"=>"N")); 
            while($ar_props = $db_props->Fetch())
            {
                if ($ar_props["VALUE"])
                {
                    $sql = "SELECT * FROM b_iblock_element WHERE ID=".$DB->ForSQL($ar_props["VALUE"])." LIMIT 1";
                    $re = $DB->Query($sql);
                    if ($ar_re = $re->Fetch())
                        $VALUES[] = $ar_re["NAME"];     
                }
            }
            $v = implode(" ",$VALUES);
            $arFields["BODY"] .= " \n".$v;
        }
      endif;
      //Всегда возвращаем arFields
//      if ($USER->IsAdmin())
//            {                   
//               
//                v_dump($arFields);
//                exit;
//            }
      return $arFields;
   }
    function BeforeIndexUpdateHandler($ID, $arFields){
//        AddMessage2Log($ID,'ID BeforeIndexUpdateHandler');
//        AddMessage2Log($arFields,'BeforeIndexUpdateHandler');
    }
}
?>
