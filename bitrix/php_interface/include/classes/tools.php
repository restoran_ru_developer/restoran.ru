<?
// dump data
function v_dump($ar) {
    echo '<pre>';
    var_dump($ar);
    echo '</pre>';
}
// clear html
function clearStrHTML($txt) {
    $search = array (/*"'<script***91;^>***93;*?>.*?</script>'si",*/    // Вырезает javaScript
        /*"'<***91;\/\!***93;*?***91;^<>***93;*?>'si",*/                // Вырезает HTML-теги
        /*"'(***91;\r\n***93;)***91;\s***93;+'",*/                      // Вырезает пробельные символы
        "'&(quot|#34);'i",                                          // Заменяет HTML-сущности
        "'&(amp|#38);'i",
        "'&(amp|#249);'i",
        "'&(amp|#34);'i",
        "'&(lt|#60);'i",
        "'&(gt|#62);'i",
        "'&(nbsp|#160);'i",
        "'&(iexcl|#161);'i",
        "'&(cent|#162);'i",
        "'&(pound|#163);'i",
        "'&(copy|#169);'i",
        /*/"'&#(\d+);'e"*/                                             // интерпретировать как php-код
    );

    $replace = array ("",
        //"",
        //"\\1",
        //"\"",
        "&",
        "u",
        '"',
        "<",
        ">",
        " ",
        chr(161),
        chr(162),
        chr(163),
        chr(169),
        //"chr(\\1)"
    );

    $str = preg_replace($search, $replace, $txt);
    return $str;
}

function get_lang($text)
{
    $a = Array("а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","э","ы","ю","я","ь","ъ");
    foreach ($a as $letter)
    {
        if (substr_count($text, $letter))
            return "ru";
    }
    $b = Array("f","d","u","l","t","p","b","q","r","k","v","y","j","g","h","c","n","e","a","w","x","i","o","s","z","m");
    foreach ($b as $letter)
    {
        if (substr_count($text, $letter))
            return "en";
    }    
    return false;
}
function decode2anotherlang_ru($text)
{
    $a = Array("а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","э","ы","ю","я","ь","ъ");
    $b = Array("f",",","d","u","l","t",";","p","b","q","r","k","v","y","j","g","h","c","n","e","a","[","w","x","i","o","'","s",".","z","m","]");
    return str_replace($a, $b, $text);
}
function decode2anotherlang_en($text)
{
    $a = Array("а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","э","ы","ю","я","ь","ъ");
    $b = Array("f",",","d","u","l","t",";","p","b","q","r","k","v","y","j","g","h","c","n","e","a","[","w","x","i","o","'","s",".","z","m","]");
    return str_replace($b, $a, $text);
}

function change_quotes($text)
{
    $text = stripslashes($text);
    $text = htmlspecialchars_decode($text);
    //$text = html_entity_decode($text);
    $text = str_replace("&quot;","'",$text);
    $text = str_replace('"',"'",$text);
    //var_dump($text);
    $text = str_replace("&#40;", "(", $text);
    $text = str_replace("&#41;", ")", $text);
    
    $text = preg_replace("/[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯабвгдеёжзийклмнопрстуфхцчшщэыюяьъA-Za-z0-9 -_]/ium", "", $text);
    $text = preg_replace("/'([АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯабвгдеёжзийклмнопрстуфхцчшщэыюяьъ a-zA-Z0-9+=-@#$%\^\!&*()]+)'/ium", "«$1»", $text);
    return $text;
}
// resort array by value
function aarsort (&$array, $key) {
    $sorter = array();
    $ret = array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii] = $va[$key];
    }
    arsort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii] = $array[$ii];
    }
    $array = $ret;
}

// remove item by array
function remove_item_by_value($array, $val = '', $preserve_keys = true) {
    if (empty($array) || !is_array($array)) return false;
    if (!in_array($val, $array)) return $array;

    foreach($array as $key => $value) {
        if ($value == $val) unset($array[$key]);
    }

    return ($preserve_keys === true) ? $array : array_values($array);
}

// get plural form
function pluralForm($n, $form1, $form2, $form5){
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}

// translit it
function translitIt($str) {
    $str = preg_replace("/[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯабвгдеёжзийклмнопрстуфхцчшщэыюяьъA-Za-z0-9 -_]/im", "", $str);
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        ":"=>"", " "=>"_", "\""=>"","/"=>"", "\/"=>"", "."=>"", ","=>"","'"=>"","!"=>"", "?"=>"", ";"=>"","%"=>"","$"=>"", "#"=>"", "@"=>"", "("=>"", ")"=>"", "&"=>"", "+"=>"", "="=>"", "’"=>"", "`"=>"",
    );
    $str = strtr($str, $tr);
    $str = strtolower($str);
    
	$str = str_replace("'", "", $str);
    
    return $str;
}

function translateFilename($str) {
    $str = preg_replace("/[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЭЮЯабвгдеёжзийклмнопрстуфхцчшщэыюяьъA-Za-z0-9 -_]/im", "", $str);
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        ":"=>"", " "=>"_", "\""=>"","/"=>"", "\/"=>"", "."=>".", ","=>"","'"=>"","!"=>"", "?"=>"", ";"=>"","%"=>"","$"=>"", "#"=>"", "@"=>"", "("=>"", ")"=>"", "&"=>"", "+"=>"", "="=>"", "’"=>"", "`"=>"",
    );
    $str = strtr($str, $tr);
    $str = strtolower($str);
    
	$str = str_replace("'", "", $str);
    
    return $str;
}
// get city by user ip
function getUserIP(){
//    if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
//        $user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
//    } else {
        $user_ip = $_SERVER['REMOTE_ADDR'];
//    }
    $temp = explode(",",$user_ip);    
    return trim($temp[0]);
}

// get city by ip (ip geo base)
function getCityByIP($ip = '', $reset_cache = false) {
    if(!$_COOKIE["geolocation"] or $reset_cache) {
        if (!$ip) {
            $ip = getUserIP();            
        }
        $ctx = stream_context_create(array( 
            'http' => array( 
                'timeout' => 1 
                ) 
            ) 
        );

//        $urm_array = array(77,38);
//        $search_in = explode('.',$ip);
//        if($search_in[0]==$urm_array[0] && $search_in[1]==$urm_array[1]){
//            $city = 'Юрмала';
//        }
//        else {
        $data = file_get_contents('http://ipgeobase.ru:7020/geo?ip='.$ip,0,$ctx);
        $city = ( $xml = simplexml_load_string($data) ) ? (array)$xml->ip->city : false;
        $city = $city[0];
//        }


//        if ($_SERVER["REMOTE_ADDR"]=="31.28.10.18")
//        {
//            v_dump($city);
//            exit;
//            
//        }
//        $data = file_get_contents('http://api.sypexgeo.net/json/'.$ip,0,$ctx);                
//        $data = json_decode($data, true);       
//        if ($_SERVER["REMOTE_ADDR"]=="31.28.10.18")
//        {
//            v_dump($data);
//            exit;
//            
//        }
//        $city = $data["city"]["name_ru"];   
//        if ($data["region"]["name_ru"]=="Московская область")
//            $city = "Москва";
//        if ($data["region"]["name_ru"]=="Ленинградская область")
//            $city = "Санкт-Петербург";
        if ($city) {
            $city_enc = base64_encode($city);
            setcookie("geolocation", $city_enc, time()+60*5); //set cookie for 1 week
        }
    } else {
        $city = base64_decode($_COOKIE["geolocation"]);
    }
    return $city;
}

// get city code by city name
function getCityCode($cityName = '') {
    global $APPLICATION;
    global $CITY_FROM_COOKIE;
    if(!$_REQUEST["CITY_ID"]) {
        if (!$APPLICATION->get_cookie("CITY_ID"))
        {
            if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/googlebot|slurp|yahoo|slurp|msnbot|msnbot|teoma|scooter|ia_archiver|lycos|yandex|stackrambler|mail|aport|webalta/i', $_SERVER['HTTP_USER_AGENT'])) {
                /** попадаем сюда с restoran.ru / m.restoran.ru / en.restoran.ru **/
                return "msk";
            }
            else {
                $CITY_FROM_COOKIE = true;

                if(!$cityName)
                    $cityName = getCityByIP();

                if (!$cityName)      //  открываем выбор города    || $_REQUEST['no-city']=='Y'
                {
                    $_COOKIE["NEED_SELECT_CITY"] = "Y";
                    $APPLICATION->set_cookie("CITY_ID", "msk", time()+60*60*1, "/",".restoran.ru",false,true);   //  лишнее, нужен только return
                    return "msk";
                }
                elseif(CModule::IncludeModule("iblock")) {// && !$cityCode
                    $rsCityIB = CIBlock::GetList(
                        Array("SORT"=>"ASC"),
                        Array(
                            "ACTIVE" => "Y",
                            "TYPE" => "catalog",
                            "NAME" => "%".$cityName."%"
                        ),
                        false
                    );
                    if($arCityIB = $rsCityIB->Fetch()){
                        $cityCode = $arCityIB["CODE"];
                        if($cityCode=='urm'){
                            $cityCode = 'rga';
                        }
                        // set cookie
                        $APPLICATION->set_cookie("CITY_ID", $cityCode, time()+60*60*1, "/",".restoran.ru",false,true);   //  лишнее, нужен только return
                        return $cityCode;
                    }
                    else {      //  открываем выбор города
                        $_COOKIE["NEED_SELECT_CITY"] = "Y";
                        $APPLICATION->set_cookie("CITY_ID", "msk", time()+60*60*1, "/",".restoran.ru",false,true);   //  лишнее, нужен только return
                        return "msk";
                    }
                }
            }
        }
        else
        {
            //  set var set cookie
            $CITY_FROM_COOKIE = true;
            return $APPLICATION->get_cookie("CITY_ID");
        }
    }
    else {
        $tmpCityCode = explode("?", $_REQUEST["CITY_ID"]);
        $cityCode = $tmpCityCode[0];

        if($cityCode=='urm'){
            $cityCode = 'rga';
        }
        if(CModule::IncludeModule("iblock") && $cityCode) {
            $rsCityIB = CIBlock::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "TYPE" => "catalog",
                    "CODE" => "%".$cityCode."%"
                ),
                false
            );
             // set cookie
            if ($arCityIB = $rsCityIB->Fetch())
            {
                $APPLICATION->set_cookie("CITY_ID", $cityCode, time()+60*60*1, "/",".restoran.ru",false,true);   //  лишнее, нужен только return
            }
            else {
                $CITY_FROM_COOKIE = true;
                $cityCode = $APPLICATION->get_cookie("CITY_ID");
            }
        }
    }

    return (strlen($cityCode) > 0 ? $cityCode : "msk");
}

function getCityCodeByIp($comparedCityCode) {
    $cityName = getCityByIP();

    if(CModule::IncludeModule("iblock") && $cityName) {
        $rsCityIB = CIBlock::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "TYPE" => "catalog",
                "NAME" => "%".$cityName."%"
            ),
            false
        );
        if($arCityIB = $rsCityIB->Fetch()){
            $cityCode = $arCityIB["CODE"];
            // set cookie
            if($cityCode!=$comparedCityCode){
                $_COOKIE["NEED_SELECT_CITY"] = "Y";
            }
        }
        else {      //  открываем выбор города
            $_COOKIE["NEED_SELECT_CITY"] = "Y";
        }
    }
    elseif(!$cityName){
        $_COOKIE["NEED_SELECT_CITY"] = "Y";
    }
}

// Get iblock array
function getArIblock($ibType = "", $code, $prefix = false) {
    if(CModule::IncludeModule("iblock") && $code) {
        global $USER;

        $obCache = new CPHPCache;
        $life_time = 60*60*48;
        $cache_id = $ibType.SITE_ID.$prefix.$code.$USER->GetUserGroupString()."citycode12323642338";
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arIB = $vars["IBLOCK"];
        } else {
            if ($prefix)
                $code = $prefix.$code;
            $rsIB = CIBlock::GetList(
                        Array("SORT" => "ASC"),
                        Array("TYPE" => $ibType, "CODE" => $code, "SITE_ID"=>SITE_ID),
                        false
            );
            $arIB = $rsIB->GetNext();
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "IBLOCK"    => $arIB
            ));
        }
        return $arIB;
    }
}

function getArIblock_EN_RU($ibType = "", $code, $prefix = false) {
    if(CModule::IncludeModule("iblock") && $code) {
        global $USER;

        $obCache = new CPHPCache;
        $life_time = 60*60*48;
        $cache_id = $ibType.SITE_ID.$prefix.$code.$USER->GetUserGroupString()."citycode12323642333_en_ru_2";
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arIB = $vars["IBLOCK"];
        } else {
            if ($prefix)
                $code = $prefix.$code;
            $rsIB = CIBlock::GetList(
                Array("SORT" => "ASC"),
                Array("TYPE" => $ibType, "CODE" => $code),
                false
            );
            while($arIB_obj = $rsIB->GetNext()){
                $arIB[] = $arIB_obj;
            }
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "IBLOCK"    => $arIB
            ));
        }
        return $arIB;
    }
}

// Get iblock array
function getArIblock2($ibType = "") {
    if(CModule::IncludeModule("iblock")) {
        global $USER;

        $obCache = new CPHPCache;
        $life_time = 0;//60*60*48;
        $cache_id = $ibType."citycode2";
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arIB = $vars["IBLOCK"];
        } else {
            $rsIB = CIBlock::GetList(
                        Array("SORT" => "ASC"),
                        Array("TYPE" => $ibType,"SITE_ID"=>SITE_ID),
                        false
            );
            while($ar = $rsIB->GetNext())
                $arIB["ID"][] = $ar["ID"];
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "IBLOCK"    => $arIB
            ));
        }
        return $arIB;
    }
}

// Get iblock element array
function getArElement($ibType = "", $ibID = "", $code = "", $id = "") {
    if(CModule::IncludeModule("iblock") && ($code || $id)) {
        global $USER;

        $obCache = new CPHPCache;
        $life_time = 60*60*24;
        $cache_id = $ibType.$ibID.$code.$id.$USER->GetUserGroupString();
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arEl = $vars["ELEMENT"];
        } else {
            $arFilter = Array();
            $arFilter["TYPE"] = $ibType;
            if($code)
                $arFilter["CODE"] = $code;
            if($id)
                $arFilter["ID"] = $id;
            $rsEl = CIBlockElement::GetList(
                Array("SORT" => "ASC"),
                $arFilter,
                false,
                false,
                Array()
            );
            $arEl = $rsEl->GetNext();
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "ELEMENT"    => $arEl
            ));
        }
        return $arEl;
    }
}

// Get iblock section array
function getArSection($id = "") {
    if(CModule::IncludeModule("iblock") && $id) {
        global $USER;
 
        $obCache = new CPHPCache;
        $life_time = 60*60*24;
        $cache_id = $code.$id.$USER->GetUserGroupString();
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arEl = $vars["SECTION"];
        } else {
            $arFilter = Array();
            if($id)
                $arFilter["ID"] = $id;
            $rsEl = CIBlockSection::GetList(
                Array("SORT" => "ASC"),
                $arFilter);
            $arEl = $rsEl->GetNext();
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "SECTION"    => $arEl
            ));
        }
        return $arEl;
    }
}

// get blog array by post id
function getBlogAr($postID) {
    $arBlog = Array();
    if(CModule::IncludeModule("blog")) {
        global $USER;

        $obCache = new CPHPCache;
        $life_time = 60*60*24;
        $cache_id = $postID.$USER->GetUserGroupString();
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arBlog = $vars["BLOG_ARRAY"];
        } else {
            $rsBlogPost = CBlogPost::GetList(
                Array("ID"=>"DESC"),
                Array(
                    "CODE" => $postID
                ),
                false,
                false,
                Array()
            );
            if($arBlogPost = $rsBlogPost->Fetch()) {
                $arBlog = CBlog::GetByID($arBlogPost["BLOG_ID"]);
            }
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "BLOG_ARRAY"    => $arBlog
            ));
        }

        return $arBlog;
    }
}

// get blog array by post id
function getBlogArByCityID($cityID) {
    if(CModule::IncludeModule("iblock") && $cityID) {
        global $USER;

        $obCache = new CPHPCache;
        $life_time = 60*60*24;
        $cache_id = $cityID.$USER->GetUserGroupString();
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $arBlogID = $vars["BLOG_ID_ARRAY"];
        } else {
            // get rest iblock info
            $arRestIB = getArIblock("catalog", CITY_ID);

            $arFilter = Array(
                "ACTIVE" => "Y",
                "TYPE" => "catalog",
                "IBLOCK_ID" => $arRestIB["ID"],
                "!PROPERTY_blog_bind" => false
            );

            $rsEl = CIBlockElement::GetList(
                Array("SORT" => "ASC"),
                $arFilter,
                false,
                false,
                Array("ID", "PROPERTY_blog_bind")
            );
            while($arEl = $rsEl->GetNext())
                $arBlogID[] = $arEl["PROPERTY_BLOG_BIND_VALUE"];
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "BLOG_ID_ARRAY"    => $arBlogID
            ));
        }

        return $arBlogID;
    }
}

// Get Section Navigation
function getFAQSectionNavigation($id, $ibid, $page)
{
    if(CModule::IncludeModule("iblock") && CITY_ID) 
    {   
        global $USER;
        $obCache = new CPHPCache;
        $life_time = 60*60;
        $cache_id = CITY_ID.$USER->GetUserGroupString().$page.$id;
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $next = $vars["NEXT"];
            $prev = $vars["PREV"];
			$isCached = true;
        } else {
           $isCached = false;
		   
			   
			$resSectionNext = CIBlockSection::GetList(Array("SORT"=>"ASC"), array("IBLOCK_ID" => $ibid, ">ID" => $id));
			$arSection = $resSectionNext->Fetch();
			//var_dump($arSection);
			$next = '';
			$prev = '';
			if(!empty($arSection["CODE"]))
			{
				$urlNext = str_replace($id, $arSection["ID"],$page);
				$next = '<a href="'.$urlNext.'" class="faq-next">Следующий раздел</a>';
			}
			
			$resSectionPrev = CIBlockSection::GetList(Array("ID"=>"DESC"), array("IBLOCK_ID" => $ibid, "<ID" => $id));
			$arSection = $resSectionPrev->Fetch();		
			if(!empty($arSection["CODE"]))
			{
				$urlPrev = str_replace($id, $arSection["ID"],$page);
				$prev = '<a href="'.$urlPrev.'" class="faq-prev">Предыдущий раздел</a>';
			}
			
		   
		   
		   
		   
		   
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "NEXT"    => $next,
                "PREV"    => $prev
            ));
        }
        
        return array("NEXT" => $next, "PREV" => $prev, "CACHED" => $isCached);
    }    
}




// get kupon count by filter
function getKuponsCount($iblock_id,$filter)
{
    if(CModule::IncludeModule("iblock") && CITY_ID) 
    {   
        global $USER;
        $obCache = new CPHPCache;
        $life_time = 60*60;
        $cache_id = CITY_ID.$USER->GetUserGroupString().serialize($filter).$iblock_id;
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $count = $vars["COUNT"];
        } else {
            $arFilter["IBLOCK_ID"] = (int)$iblock_id;
            $rsElement = CIBlockElement::GetList(Array(), array_merge($arFilter, $filter), false, false, Array());        
            $count = $rsElement->SelectedRowsCount();
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "COUNT"    => $count
            ));
        }
        
        return $count;
    }    
}
//Set user Fields
function SetUserField ($entity_id, $value_id, $uf_id, $uf_value) 
{ 
   return $GLOBALS["USER_FIELD_MANAGER"]->Update ($entity_id, $value_id, 
      Array ($uf_id => $uf_value)); 
}
function GetUserField ($entity_id, $value_id, $property_id) 
{ 
    $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields($entity_id,$value_id);
    return $arUF[$property_id]; 
}
function getCookeryPost($type, $iblock_type_id, $iblock_id, $section_id, $id )
{
    /*v_dump($type);
    v_dump($iblock_type_id);
    v_dump($iblock_id);
    v_dump($section_id);
    v_dump($id);*/
    
    if(CModule::IncludeModule("iblock")) 
    {
        global $USER;
        $obCache = new CPHPCache;
        $life_time = 36000*60;
        $cache_id = CITY_ID.$USER->GetUserGroupString().serialize($filter).$iblock_id;
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $count = $vars["COUNT"];
        } else {
            $arFilter = Array();
            if (!$_SESSION["SECTION_SORT_".$iblock_type_id]["ORDER"])
            {
                $_SESSION["SECTION_SORT_".$iblock_type_id]["ORDER"] = "new";
                $_SESSION["SECTION_SORT_".$iblock_type_id]["BY"] = "DESC";
            }       
            if ($_SESSION["SECTION_SORT_".$iblock_type_id]["ORDER"]=="popular")
            {
                if ($type=="next") {
                    if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                    {
                        $arFilter["<=UF_COUNT"] = $arr["UF_COUNT"];
                        $arFilter["!UF_COUNT"] = $arr["UF_COUNT"];
                    }
                    elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                        $arFilter[">UF_COUNT"] = $arr["UF_COUNT"];
                }
                else {
                    if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                        $arFilter[">UF_COUNT"] = $arr["UF_COUNT"];
                    elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                    {
                        $arFilter["<=UF_COUNT"] = $arr["UF_COUNT"];                
                        $arFilter["!UF_COUNT"] = $arr["UF_COUNT"];                
                    }
                }
                $arFilter["IBLOCK_ID"] = $iblock_id;
                $arFilter["SECTION_ID"] = $section_id;                    
                $res = CIBlockSection::GetList(Array("UF_COUNT"=>$_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]), $arFilter, false, Array("ID","CODE"));
                $res->NavStart(1);
                if ($ar_res = $res->Fetch())
                    $return = $ar_res;                                    
            }
            else
            {
                if ($type=="next") {
                    if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                    {
                        $arFilter["<=ID"] = $id;
                        $arFilter["!ID"] = $id;
                    }
                    elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                        $arFilter[">ID"] = $id;        
                }
                else {
                    if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                        $arFilter[">ID"] = $id;
                    elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                    {
                        $arFilter['<=ID'] = $id;
                        $arFilter['!ID'] = $id;
                    }                    
                }
                $arFilter["IBLOCK_ID"] = $iblock_id;
                $arFilter["SECTION_ID"] = $section_id;                                
                $res = CIBlockSection::GetList(Array("ID"=>$_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]), $arFilter, false, Array("ID","CODE"));
                $res->NavStart(1);
                if ($ar_res = $res->Fetch())
                    $return = $ar_res;
            }
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "RES"    => $return
            ));
        }
        return $return;
    }
    return false;
}

function getPost($type = "next", $iblock_type_id, $iblock_id, $id, $section_id = false, $depth = false)
{
    if(CModule::IncludeModule("iblock")) 
    {   
        global $USER;
        $obCache = new CPHPCache;
        $life_time = 36000*60;
        $cache_id = CITY_ID.$USER->GetUserGroupString().serialize($filter).$iblock_id;
        if($obCache->InitCache($life_time, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $count = $vars["COUNT"];
        } else {
            $arFilter = Array();
            if (!$_SESSION["SECTION_SORT_".$iblock_type_id]["ORDER"])
            {
                $_SESSION["SECTION_SORT_".$iblock_type_id]["ORDER"] = "new";
                $_SESSION["SECTION_SORT_".$iblock_type_id]["BY"] = "DESC";
            }       
            if ($_SESSION["SECTION_SORT_".$iblock_type_id]["ORDER"]=="popular")
            {
                $arFilter = Array();
                $arFilter["ID"] = $id;
                $rr = CIBlockSection::GetList(Array(),$arFilter,false,Array("ID","UF_COUNT"));
                if ($arr = $rr->Fetch())
                {
                    if ($type=="prev") {
                        if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                            $arFilter["<UF_COUNT"] = $arr["UF_COUNT"];
                        elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                            $arFilter[">UF_COUNT"] = $arr["UF_COUNT"];
                    }
                    else {
                        if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                            $arFilter[">UF_COUNT"] = $arr["UF_COUNT"];
                        elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                            $arFilter["<UF_COUNT"] = $arr["UF_COUNT"];                
                    }
                    $arFilter["IBLOCK_ID"] = $iblock_id;
                   // if ($section_id&&$depth<3) {
                        $arFilter["IBLOCK_SECTION_ID"] = $section_id;
                    //}
                    /*elseif ($section_id&&$depth>3) {
                        $r = CIBlockSection::GetById($section_id);
                        if($ar = $r->Fetch()) {
                            $arFilter[">LEFT_BORDER"] = $ar["LEFT_MARGIN"];
                            $arFilter["<RIGHT_BORDER"] = $ar["RIGHT_MARGIN"];
                        }
                    }*/
                    $res = CIBlockSection::GetList(Array("UF_COUNT"=>$_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]), $arFilter, false, Array("ID","CODE"));
                    $res->NavStart(1);
                    if ($ar_res = $res->Fetch())
                        $return = $ar_res;
                }                
            }
            else//if sort = new
            {        
                if ($type=="prev") {
                    if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                        $arFilter["<ID"] = $id;
                    elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                        $arFilter[">ID"] = $id;        
                }
                else {
                    if ($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="ASC")
                        $arFilter[">ID"] = $id;
                    elseif($_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]=="DESC")
                        $arFilter["<ID"] = $id;
                }
                $arFilter["IBLOCK_ID"] = $iblock_id;
                //if ($section_id&&$depth<3) {
                    $arFilter["IBLOCK_SECTION_ID"] = $section_id;
                //}
                /*elseif ($section_id&&$depth>3) {
                    $r = CIBlockSection::GetById($section_id);
                    if($ar = $r->Fetch()) {
                        $arFilter[">LEFT_BORDER"] = $ar["LEFT_MARGIN"];
                        $arFilter["<RIGHT_BORDER"] = $ar["RIGHT_MARGIN"];
                    }
                }*/
                    //v_dump($arFilter);
                $res = CIBlockSection::GetList(Array("ID"=>$_SESSION["SECTION_SORT_".$iblock_type_id]["BY"]), $arFilter, false, Array("ID","CODE"));
                $res->NavStart(1);
                if ($ar_res = $res->Fetch())
                    $return = $ar_res;
            }    
        }
        if($obCache->StartDataCache()) {
            $obCache->EndDataCache(array(
                "RES"    => $return
            ));
        }
        return $return;
    }
    return false;
}

function getReviewsRatio($iblock_id, $section_id)
{
    global $DB;
    global $USER;
    $obCache = new CPHPCache;
    $life_time = 36000*60;
    $iblock_section = $DB->ForSql($section_id);
    $iblock = $DB->ForSql($iblock_id);    
    $where = "ACTIVE = 'Y' AND WF_PARENT_ELEMENT_ID IS NULL AND IBLOCK_ID = ".$iblock_id." AND IBLOCK_SECTION_ID = ".$iblock_section;
    $sql = "SELECT COUNT(ID) AS count FROM b_iblock_element WHERE ".$where; 
    $c = $DB->Query($sql);
    if ($a_count = $c->Fetch())
    {
        $count = $a_count["count"];
    }
    $cache_id = $USER->GetUserGroupString().$iblock_id.$section_id.$count.SITE_ID;
    if($obCache->InitCache($life_time, $cache_id, "/")) 
    {
        $vars = $obCache->GetVars();
        $result = $vars["RESULT"];
    } 
    else 
    {
        $sql = "SELECT SUM(DETAIL_TEXT) as ratio,COUNT(ID) AS count FROM b_iblock_element WHERE ".$where; 
        $r = $DB->Query($sql);
        if ($ar1 = $r->Fetch())
        {
            $ratio = $ar1["ratio"];
            $count = $ar1["count"];
        }
        $result["COUNT"] = $count;
        $result["RATIO"] = round($ratio/$count,1);
    }
    if($obCache->StartDataCache()) {
        $obCache->EndDataCache(array(
            "RESULT"    => $result
        ));
    }
    return $result;
}


//функция возвращает баллы за действие для пользователя
function getUserActionBalls($action_code, $uid){
	$balls = false;
	if(CModule::IncludeModule("iblock")) {
		$arGroups = CUser::GetUserGroup($uid);
		
		$arSelect = Array("ID", "NAME", "PROPERTY_balls_p","PROPERTY_balls_r");
		$arFilter = Array("IBLOCK_ID"=>150, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "CODE"=>$action_code);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($arFields = $res->GetNext()){ 
			//ресторатор
 			if(in_array(9, $arGroups)) 
                            $balls=$arFields["PROPERTY_BALLS_R_VALUE"];
                        else
                            $balls=$arFields["PROPERTY_BALLS_P_VALUE"];
 			//обычный пользователь
 			//if(in_array(5, $arGroups)) $balls=$arFields["PROPERTY_BALLS_P_VALUE"];
 				
		}
	}
	
	return $balls;
}

function change_search_adr($q)
{
    $q  = strtolower($q);
    $ar = Array("обл ","р-н ","пл ","г ","пос ","ш ","ул ","пр ","пер ","наб ");
    $ar2 = Array("область ","район ","площадь ","город ","поселок ","шоссе ","улица ","проспект ","переулок ","набережная ");
    $q2 = str_replace($ar2, $ar, $q);
    $q = str_replace($ar, $ar2, $q);    
    if ($q2!=$q)
        return $q." || ".$q2;  
    else
        return $q;  
}

function keywordsShort($txt) {
	$txt = strip_tags($txt);
	$txt = htmlspecialchars($txt);
	//$txt = preg_replace("/((\w{0,2}+))/uis","",$txt);// что-то не сросклось - хотел союзы повырезать
	$txt = preg_replace("/(\t+)/is","",$txt);// знаки табуляции вырезать
	$txt = preg_replace("/(\s+){2,}/is"," ",$txt);//двойные пробелы заменить одним
	$txt = preg_replace("/(\r\n)+/i", "", $txt);// переводы строки вырезать
	$txt = trim($txt);
	$txt = preg_replace("/(\s+)/is",", ",$txt);// расставить запятые между словами
	$txt = preg_replace("/(\,+){2,}/is",",",$txt);// двойные запятые заменить одинарными
	return $txt;
}

function SetSeo()
{
    global $APPLICATION;
    //$APPLICATION->AddViewContent("preview_seo_text","test");
    if (!CModule::IncludeModule("iblock"))
        return false;
		
		

		// для страниц поиска по ОДНОМУ параметру костыль, автоматически формирующий титлы, дескрипшны, кейвордз и заголовки страниц
		// если ниже из инфоблока СЕО будут выбраны записи, то титлы перепишутся
		global $USER;
		//if ($USER->IsAdmin()) {
		
		if (!empty($_REQUEST["CITY_ID"])) $CITY_ID = $_REQUEST["CITY_ID"];
		if (!empty($_REQUEST["CATALOG_ID"])) $CATALOG_ID = $_REQUEST["CATALOG_ID"];
		if (!empty($_REQUEST["PROPERTY"])) $PROPERTY = $_REQUEST["PROPERTY"];
		if (!empty($_REQUEST["PROPERTY_VALUE"])) $PROPERTY_VALUE = $_REQUEST["PROPERTY_VALUE"];
		//echo $CATALOG_ID
		//echo $ar1.'ok111';
		//echo 'ok111';
		//echo '<pre>';
		//print_r($_REQUEST);
		
		$CITY_ID_ARRAY = Array(
			'spb' => 'Санкт-Петербурга',
			'msk' => 'Москвы'
		);
		
		$CATALOG_ID_ARRAY = Array(
			'restaurants' => 'Все рестораны'
		);
		$CATALOG_ID_SHORT_ARRAY = Array(
			'restaurants' => 'Рестораны'
		);
		
		$PROPERTY_ARRAY = Array(
//			'subway' => 'метро',
//			'metro' => 'метро',
			'kitchen' => 'кухня',
			'type' => '',
//			'area' => 'район',
//			'out_city' => 'пригород',
			'average_bill' => 'средний счёт',
//			'children' => 'детям',
//			'proposals' => 'предложения',
//			'features' => 'особенности',
//			'entertainment' => 'развлечения',
			'ideal_place_for' => 'идеально для'
		);
		
		$PROPERTY_SHORT_ARRAY = Array(
			'subway' => 'м.',
			'kitchen' => 'кухня',
			'type' => 'тип',
			'area' => 'р-н',
			'out_city' => '',
			'average_bill' => '',//ср.счёт
			'children' => '',
			'proposals' => '',
			'features' => '',
			'entertainment' => '',
			'ideal_place_for' => 'идеально для'
		);
		
		if (!empty($CITY_ID) && !empty($CATALOG_ID) && !empty($PROPERTY) && !empty($PROPERTY_VALUE)) {
            if (array_key_exists($PROPERTY,$PROPERTY_ARRAY) && array_key_exists($CITY_ID,$CITY_ID_ARRAY) && array_key_exists($CATALOG_ID,$CATALOG_ID_ARRAY)) {
                if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"][$PROPERTY])==1&&$_REQUEST["arrFilter_pf"][$PROPERTY][0])
                {

                    $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"][$PROPERTY][0]);
                    $ar1 = $r->Fetch();

                    if (!empty($ar1['NAME'])) {
                        //$title = $CATALOG_ID_ARRAY[$CATALOG_ID].' '.$CITY_ID_ARRAY[$CITY_ID].', '.$PROPERTY_ARRAY[$PROPERTY].': '.$ar1['NAME'];
                        if (!empty($PROPERTY_ARRAY[$PROPERTY])) $title = $CITY_ID_ARRAY[$CITY_ID].', '.$PROPERTY_ARRAY[$PROPERTY].': '.$ar1['NAME'];
                        else $title = $CITY_ID_ARRAY[$CITY_ID].': '.$ar1['NAME'];
                        $title_short = $CATALOG_ID_SHORT_ARRAY[$CATALOG_ID].' '.$title;//
                        $title = $CATALOG_ID_ARRAY[$CATALOG_ID].' '.$title;//
                        $description = $title.' '.', меню, фотографии, отзывы';
                        $APPLICATION->SetPageProperty("title",  $title);
                        $APPLICATION->SetPageProperty("description",  $description);
                        $APPLICATION->SetPageProperty("h1",  $title_short);

                        $keywords = keywordsShort($title).' меню, фотографии, отзывы';// подкастрировать keywords
                        $APPLICATION->SetPageProperty("keywords",  $keywords);
                    }

                    //echo $CITY_ID_ARRAY[$CITY_ID].'<pre>';
                    //print_r($ar1['NAME']);
                //LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/subway/".$ar1["CODE"]);
                }
            }
		}
		//}
		// END блока формирования титлов и заголовков
		
		
		
    $url = $APPLICATION->GetCurPageParam("",array("clear_cache", "sphrase_id","CITY_ID","CATALOG_ID","RESTORAN","IBLOCK_TYPE_ID","SECTION_ID","RESTOURANT","CODE","PAGEN_1","USER_ID","ID","KUPON","blog","SECTION_CODE","logout"));

    if(preg_match('/\/metro\//',$url)&&($CITY_ID=='msk'||$CITY_ID=='spb')){
        return false;
    }

    if (($_SERVER["HTTP_HOST"]=="spb.restoran.ru"||$_SERVER["HTTP_HOST"]=="www.spb.restoran.ru"||$_SERVER["HTTP_HOST"]=="spb.restoran.ru:443")&&$url=="/")
            $url = "/spb/";
    if (($_SERVER["HTTP_HOST"]=="urm.restoran.ru"||$_SERVER["HTTP_HOST"]=="www.urm.restoran.ru")&&$url=="/")
        $url = "/urm/";
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"),
            Array("IBLOCK_CODE"=>"seo","LID"=>SITE_ID, "NAME" =>$url, "IBLOCK_TYPE"=>"system","ACTIVE"=>"Y"),
            false, 
            Array('nTopCount'=>9),
            Array("ID","NAME","PREVIEW_TEXT","PROPERTY_title","PROPERTY_h1","PROPERTY_keywords","PROPERTY_description")
    );
    if ($ar=$res->Fetch())
    {
//        if (substr_count($url, "article")&&$_REQUEST["PAGEN_1"])
//            $APPLICATION->SetPageProperty("title",  $ar["PROPERTY_TITLE_VALUE"]." Страница: ".$_REQUEST["PAGEN_1"]);
//        else
        if($_REQUEST["PAGEN_1"]||$_REQUEST['page']){
            $page_num = $_REQUEST["PAGEN_1"]?$_REQUEST["PAGEN_1"]:$_REQUEST['page'];
            if($page_num==1){
                $APPLICATION->SetPageProperty("description", $ar["PROPERTY_DESCRIPTION_VALUE"]);
                $APPLICATION->SetPageProperty("keywords",  $ar["PROPERTY_KEYWORDS_VALUE"]);
                $APPLICATION->SetPageProperty("title",  $ar["PROPERTY_TITLE_VALUE"]);
            }
            else {
                $APPLICATION->SetPageProperty("description", $ar["PROPERTY_DESCRIPTION_VALUE"].(LANGUAGE_ID!='en'?" Страница: ":" Page: ").$page_num);
                $APPLICATION->SetPageProperty("keywords",  $ar["PROPERTY_KEYWORDS_VALUE"]);
                $APPLICATION->SetPageProperty("title",  $ar["PROPERTY_TITLE_VALUE"].(LANGUAGE_ID!='en'?" Страница: ":" Page: ").$page_num);
            }
        }
        else {
            $APPLICATION->SetPageProperty("description", $ar["PROPERTY_DESCRIPTION_VALUE"]);
            $APPLICATION->SetPageProperty("keywords",  $ar["PROPERTY_KEYWORDS_VALUE"]);
            $APPLICATION->SetPageProperty("title",  $ar["PROPERTY_TITLE_VALUE"]);
        }


        if ($ar["PROPERTY_H1_VALUE"])
            $APPLICATION->SetPageProperty("h1",  $ar["PROPERTY_H1_VALUE"]);
        else
            $APPLICATION->SetPageProperty("h1",  $ar["PROPERTY_TITLE_VALUE"]);
        if ($ar["PREVIEW_TEXT"])
            $preview_text = $ar["PREVIEW_TEXT"];
        if ($preview_text)
        {
            $APPLICATION->SetPageProperty("catalog_prop_description", "");
            $preview_text = stripslashes($preview_text);
            $str = "";

            if($_REQUEST['arrFilter_pf']){
                $str .= '<span class="preview_seo">'.$preview_text;
                $str .= '</span>';
            }
            else {

                if(CUR_DIR=='banquet-service'){
                    $str .= '<p class="preview_seo">';
                    $str .= $preview_text;
                    $str .= '</p>';
                }
                else {
                    $preview_text2 = TruncateText($preview_text, 400);

                    $str .= '<span class="preview_seo">'.$preview_text2;
                    if (strlen($preview_text2)!=strlen($preview_text)):
                        $str .= ' <a onclick=$(".preview_seo").toggle() class="jsanother">еще</a>';
                    endif;
                    $str .= "</span>";
                    if (strlen($preview_text2)!=strlen($preview_text)):
                        $str .= '<span class="preview_seo" style="display:none;">';
                        $str .= $preview_text." <a onclick=$('.preview_seo').toggle() class='jsanother'>скрыть</a>";
                        $str .= '</span>';
                    endif;
                }
            }

            $APPLICATION->AddViewContent("preview_seo_text",$str);
        }
        return true;
    }
    else
    {
        $url = $APPLICATION->GetCurPage();    
        if (($_SERVER["HTTP_HOST"]=="spb.restoran.ru"||$_SERVER["HTTP_HOST"]=="www.spb.restoran.ru"||$_SERVER["HTTP_HOST"]=="spb.restoran.ru:443")&&$url=="/")
            $url = "/spb/";
        if (($_SERVER["HTTP_HOST"]=="urm.restoran.ru"||$_SERVER["HTTP_HOST"]=="www.urm.restoran.ru")&&$url=="/")
            $url = "/urm/";
        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"),
                Array("IBLOCK_CODE"=>"seo","LID"=>SITE_ID, "NAME" =>$url, "IBLOCK_TYPE"=>"system","ACTIVE"=>"Y"),
                false, 
                Array('nTopCount'=>9),
                Array("ID","NAME","PREVIEW_TEXT","PROPERTY_title","PROPERTY_h1","PROPERTY_keywords","PROPERTY_description")
        );
        if ($ar=$res->Fetch())
        {
		// если в инфоблоке сео что-то есть
            if($_REQUEST["PAGEN_1"]||$_REQUEST['page']){
                $page_num = $_REQUEST["PAGEN_1"]?$_REQUEST["PAGEN_1"]:$_REQUEST['page'];
                if($page_num==1){
                    $APPLICATION->SetPageProperty("description", $ar["PROPERTY_DESCRIPTION_VALUE"]);
                    $APPLICATION->SetPageProperty("keywords",  $ar["PROPERTY_KEYWORDS_VALUE"]);
                    $APPLICATION->SetPageProperty("title",  $ar["PROPERTY_TITLE_VALUE"]);
                }
                else {
                    $APPLICATION->SetPageProperty("description", $ar["PROPERTY_DESCRIPTION_VALUE"].(LANGUAGE_ID!='en'?" Страница: ":" Page: ").$page_num);
                    $APPLICATION->SetPageProperty("keywords",  $ar["PROPERTY_KEYWORDS_VALUE"]);
                    $APPLICATION->SetPageProperty("title",  $ar["PROPERTY_TITLE_VALUE"].(LANGUAGE_ID!='en'?" Страница: ":" Page: ").$page_num);
                }
            }
            else {
                $APPLICATION->SetPageProperty("description", $ar["PROPERTY_DESCRIPTION_VALUE"]);
                $APPLICATION->SetPageProperty("keywords", $ar["PROPERTY_KEYWORDS_VALUE"]);
                $APPLICATION->SetPageProperty("title", $ar["PROPERTY_TITLE_VALUE"]);
            }
            if ($ar["PROPERTY_H1_VALUE"])
                $APPLICATION->SetPageProperty("h1",  $ar["PROPERTY_H1_VALUE"]);
            else
                $APPLICATION->SetPageProperty("h1",  $ar["PROPERTY_TITLE_VALUE"]);
            if ($ar["PREVIEW_TEXT"])
                $preview_text = $ar["PREVIEW_TEXT"];
            if ($preview_text)
            {
                $APPLICATION->SetPageProperty("catalog_prop_description", "");

                $preview_text = stripslashes($preview_text);
                $str = "";

                if($_REQUEST['arrFilter_pf']){
                    $str .= '<span class="preview_seo">'.$preview_text;
                    $str .= '</span>';
                }
                else {
                    $preview_text2 = TruncateText($preview_text, 320);

                    $str .= '<span class="preview_seo">'.$preview_text2;
                    if (strlen($preview_text2)!=strlen($preview_text)):
                        $str .= ' <a onclick=$(".preview_seo").toggle() class="jsanother">еще</a>';
                    endif;
                    $str .= "</span>";
                    if (strlen($preview_text2)!=strlen($preview_text)):
                        $str .= '<span class="preview_seo" style="display:none;">';
                        $str .= $preview_text." <a onclick=$('.preview_seo').toggle() class='jsanother'>скрыть</a>";
                        $str .= '</span>';
                    endif;
                }

                $APPLICATION->AddViewContent("preview_seo_text",$str);
            }
            return true;
        }
        else {
            //if ($USER->IsAdmin())
            {
                $title = $APPLICATION->GetTitle(false);
                $title = explode(" ",$title);
                $keywords = array();
                foreach ($title as $t)
                {
                    if (strlen($t)>3)
                    {
                        $t=trim($t);
                        $t = str_replace(Array("«","!","-","_","@","#","$","%","^","*","(",")","»","'",'"'), "", $t);
                        $keywords[] = $t;
                    }
                }         
                $title = str_replace(Array("«","!","-","_","@","#","$","%","^","*","(",")","»","'",'"'), "", $APPLICATION->GetTitle(false));

                if($_REQUEST["PAGEN_1"]||$_REQUEST['page']){
                    $page_num = $_REQUEST["PAGEN_1"]?$_REQUEST["PAGEN_1"]:$_REQUEST['page'];
                    if($page_num==1){
                        if(!$APPLICATION->GetPageProperty("keywords"))
                        {
                            $APPLICATION->SetPageProperty("keywords",  implode(", ",$keywords));
                        }
                        if(!$APPLICATION->GetPageProperty("description"))
                        {
                            $APPLICATION->SetPageProperty("description",  $title);
                        }
                    }
                    else {
                        $APPLICATION->SetPageProperty("title",  $APPLICATION->GetTitle(false).(LANGUAGE_ID!='en'?" Страница: ":" Page: ").$page_num);
                        if(!$APPLICATION->GetPageProperty("keywords"))
                        {
                            $APPLICATION->SetPageProperty("keywords",  implode(", ",$keywords));
                        }
                        if(!$APPLICATION->GetPageProperty("description"))
                        {
                            $APPLICATION->SetPageProperty("description",  $title.(LANGUAGE_ID!='en'?" Страница: ":" Page: ").$page_num);
                        }
                    }
                }
                else {
                    if(!$APPLICATION->GetPageProperty("keywords"))
                    {
                        $APPLICATION->SetPageProperty("keywords",  implode(", ",$keywords));
                    }
                    if(!$APPLICATION->GetPageProperty("description"))
                    {
                        $APPLICATION->SetPageProperty("description",  $title);
                    }
                }

            }
        }
    }    
            
    if (!$APPLICATION->GetPageProperty("keywords"))
    {   
        $APPLICATION->SetPageProperty("keywords",  "");
    }
    if (!$APPLICATION->GetPageProperty("description"))
    {
        $APPLICATION->SetPageProperty("description",  "");
    }

    return false;
}

function mobile_detect()
{
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $ipad = strpos($user_agent,"iPad");
    $ipod = strpos($user_agent,"iPod");
    $iphone = strpos($user_agent,"iPhone");
    $android = strpos($user_agent,"Android");
    $symb = strpos($user_agent,"Symbian");
    $winphone = strpos($user_agent,"WindowsPhone");
    $wp7 = strpos($user_agent,"WP7");
    $wp8 = strpos($user_agent,"WP8");
    $operam = strpos($user_agent,"Opera M");
    $palm = strpos($user_agent,"webOS");
    $berry = strpos($user_agent,"BlackBerry");
    $mobile = strpos($user_agent,"Mobile");
    $htc = strpos($user_agent,"HTC_");
    $fennec = strpos($user_agent,"Fennec/");

    if ($ipod || $iphone || $android || $symb || $winphone || $wp7 || $wp8 || $operam || $palm || $berry || $mobile || $htc || $fennec) 
    {
        return true; 
    } 
    else
    {
        return false; 
    }
}
function check_megafon($tel)
{
//    $megafon_code = Array("7926","7925","7921","7931","7920","7922","7923","7924","7927","7928","7937","7929","7930","7932","7938","7933","7939");
//    foreach ($megafon_code as $b)
//    {
//        if (substr_count("7".$tel, $b)>0)
//                $megafon = "1";
//    }
    //if ($megafon)
    {
//        if (CITY_ID == "msk")
//            return "84959882656";
//        elseif(CITY_ID == "spb")
//            return "88127401820";
    }
    return false;
}

function send_sms_rga_urm($phone,$message){
    $TEL=preg_replace("/\D/", "", $phone);

    $sms=array();
    $sms['login']='37126322177'; //Логин к личному кабинету
    $sms['password']='6aS9js72'; //Пароль к личному кабинету
    $sms['from']='Restoran.ru';
    $sms['to']=$TEL;
    $sms['text']=$message;
    //AddMessage2Log($sms,'sms BRON-URM-RGA change status');

    $ch = curl_init(); //Работаем через CURL библиотеку
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //Получить ответ
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms));
    curl_setopt($ch, CURLOPT_URL, "https://api.bmi.io/sms/v1/send");
    $result = curl_exec($ch); //Отправляем данные
//    AddMessage2Log($result,'sms BRON-URM-RGA change status');
//    AddMessage2Log($message,'sms BRON-URM-RGA change message');
    curl_close($ch);
}
function TimeEncodeZeroZone($iTime)
{
    $iTime = $iTime-3*60*60;
//    $iTZ = date("Z", $iTime);
//    $iTZHour = intval(abs($iTZ)/3600);
//    $iTZMinutes = intval((abs($iTZ)-$iTZHour*3600)/60);
//    $strTZ = ($iTZ<0? "-": "+").sprintf("%02d:%02d", $iTZHour, $iTZMinutes);
    $strTZ = '+00:00';
    return date("Y-m-d",$iTime)."T".date("H:i:s",$iTime).$strTZ;
}

function clearUserPhone($phone_str){
    return preg_replace('/^\(/','+7 (',preg_replace('/^8 |^8/','+7 ',trim(preg_replace('/[^-+0-9() ]+/i','',$phone_str))));
}
function clearUserPhoneMobileApp($phone_str){
//    global $USER;
//    if($USER->IsAdmin())
        return preg_replace('/8 \(|8\(/','+7 (',preg_replace('/-/',' ',preg_replace('/^8 |^8/','+7 ',trim(preg_replace('/[^-+0-9() ,;]+/i','',$phone_str)))));
//    else
//        return preg_replace('/-/',' ',preg_replace('/^8 |^8/','+7 ',trim(preg_replace('/[^-+0-9() ,;]+/i','',$phone_str))));
}
function calculateDistanceToRest($LAT1, $LON1, $LAT2, $LON2) {

    // перевести координаты в радианы
    $lat1 = $LAT1 * M_PI / 180;
    $lat2 = $LAT2 * M_PI / 180;
    $long1 = $LON1 * M_PI / 180;
    $long2 = $LON2 * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

    $ad = atan2($y, $x);
    $dist = $ad * 6371000;

    return $dist;
}


function TruncateIdeallyText($strText, $intLen)
{
    if(strlen($strText) > $intLen)
        return rtrim(substr($strText, 0, $intLen), ".")."<span class='more-props-from-list'>...</span>";
    else
        return $strText;
}
?>