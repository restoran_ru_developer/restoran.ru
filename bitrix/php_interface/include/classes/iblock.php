<?
class RestIBlock {
    function OnAfterIBlockSectionAddHandler(&$arFields) {
        /*global $APPLICATION;  
        $iblock_types = Array("photoreports","interview","master_classes","overviews");
        if(CModule::IncludeModule("iblock")):
            $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
            if($ar_res = $res->GetNext())
            {
                if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types))
                {
                    //Для статейных разделов
                    $arCommentsIB = getArIblock("comments", CITY_ID);
                    $arSection["ID"] = RestIBlock::hasParentClassArticles($ar_res["IBLOCK_TYPE_ID"]);                    
                    //Создаем раздел куда буду складываться новые комментарии
                    $bs = new CIBlockSection;
                    $arNew = Array(  "ACTIVE" => "Y",  
                                        "IBLOCK_ID" => $arCommentsIB["ID"],  
                                        "IBLOCK_SECTION_ID" => $arSection["ID"],
                                        "NAME" => $arFields["NAME"]
                                     );
                    $ID = $bs->Add($arNew);  
                    $res = ($ID>0);
                    if(!$res)  
                    {
                        $APPLICATION->ThrowException($bs->LAST_ERROR);
                        return false;
                    }
                    unset($bs);
                    unset($res);
                    unset($arNew);
                    //А теперь привязываем к созданный раздел к добавляемой статье
                    $bs = new CIBlockSection;
                    $arNew = $arFields;
                    $arNew["UF_SECTION_BIND"] = $ID;
                    $res = $bs->Update($arFields["ID"], $arNew);                    
                    if(!$res)  
                    {
                        $APPLICATION->ThrowException($bs->LAST_ERROR);
                        return false;
                    }
                    return true;
                }
                elseif ($ar_res["IBLOCK_TYPE_ID"] == "cookery")
                {
                    $arCommentsIB = 88;
                    $bs = new CIBlockSection;
                    $arNew = Array(  "ACTIVE" => "Y",  
                                        "IBLOCK_ID" => $arCommentsIB,                                          
                                        "NAME" => $arFields["NAME"]
                                     );
                    $ID = $bs->Add($arNew);  
                    $res = ($ID>0);
                    if(!$res)  
                    {
                        $APPLICATION->ThrowException($bs->LAST_ERROR);
                        return false;
                    }
                    unset($bs);
                    unset($res);
                    unset($arNew);
                    //А теперь привязываем к созданный раздел к добавляемой статье
                    $bs = new CIBlockSection;
                    $arNew = $arFields;
                    $arNew["UF_COMMENTS"] = $ID;
                    $res = $bs->Update($arFields["ID"], $arNew);                    
                    if(!$res)  
                    {
                        $APPLICATION->ThrowException($bs->LAST_ERROR);
                        return false;
                    }
                    return true;
                    
                }
                return true;
            }
            $APPLICATION->ThrowException("Что-то пошло не так");
            return false;
        endif;*/
    }
    
    function OnBeforeIBlockSectionDeleteHandler($ID)
    {
        /*global $APPLICATION;        
        $iblock_types = Array("photoreports","interview","master_classes","overviews");
        if(CModule::IncludeModule("iblock")):
            $res = CIBlockSection::GetList(Array("SORT"=>"ASC"),Array("ID"=>$ID),false,Array("IBLOCK_ID"));
            if($ar_res = $res->GetNext())
            {
                $res = CIBlockSection::GetList(Array("SORT"=>"ASC"),Array("ID"=>$ID,"IBLOCK_ID"=>$ar_res["IBLOCK_ID"]),false,Array("UF_*"));
                if($ar_res = $res->GetNext())
                {
                    if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types))
                    {
                        //Для статейных разделов
                        $res_ibs = CIBlockSection::GetByID($ar_res["UF_SECTION_BIND"]);
                        if ($ar_res_ibs = $res_ibs->GetNext())
                        {
                            global $DB;
                            $DB->StartTransaction();	
                            if(!CIBlockSection::Delete($ar_res_ibs["ID"]))
                            {	
                                $strWarning .= 'Error.';		                            
                                $DB->Rollback();	
                                $APPLICATION->ThrowException($strWarning);
                                return false;
                            }	
                            else		
                                $DB->Commit();
                            return true;
                        }                        
                    }
                    elseif ($ar_res["IBLOCK_TYPE_ID"]=="cookery")
                    {
                        //Для статейных разделов
                        $res_ibs = CIBlockSection::GetByID($ar_res["UF_COMMENTS"]);
                        if ($ar_res_ibs = $res_ibs->GetNext())
                        {
                            global $DB;
                            $DB->StartTransaction();	
                            if(!CIBlockSection::Delete($ar_res_ibs["ID"]))
                            {	
                                $strWarning .= 'Error.';		                            
                                $DB->Rollback();	
                                $APPLICATION->ThrowException($strWarning);
                                return false;
                            }	
                            else		
                                $DB->Commit();
                            return true;
                        }                        
                    }
                    return true;
                }
            }
            $APPLICATION->ThrowException("Что-то пошло не так");
            return false;
        endif;*/
    }
    
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        global $APPLICATION;          
        $iblock_types_afisha = Array("afisha");
        $iblock_types_catalog = Array("catalog");
        if(CModule::IncludeModule("iblock")):
            
        endif;
        $APPLICATION->ThrowException("Что-то пошло не так");
        return false;
    }
    function OnBeforeIBlockElementAddHandlerForComments(&$arFields) //  uses for all elements
    {
        global $APPLICATION;
        if($arFields['PROPERTY_VALUES']['ELEMENT']=='2333500'||$arFields['PROPERTY_VALUES']['ELEMENT']=='1958209'){//
            $APPLICATION->ThrowException("Что-то пошло не так");
            return false;
        }
        //AddMessage2Log($arFields,'before add');
        $exept_ib_ids_arr = array(4514);
        if(!in_array($arFields['IBLOCK_ID'],$exept_ib_ids_arr)){
            if($arFields['CODE']){
//            if(CModule::IncludeModule("iblock")) {
//                $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
//                if ($ar_res = $res->Fetch()) {
                //$ar_res["IBLOCK_TYPE_ID"] == 'catalog'
                $arFields['CODE'] = preg_replace('/_/','-',$arFields['CODE']);
//                }
//            }
            }
        }
    }
   
    function OnBeforeIBlockElementDeleteHandler($ID)
    {        
        $iblock_types_comment = Array("comment","reviews");
        $iblock_types_for_account = Array("comment","reviews","blogs","cookery");        
        if(CModule::IncludeModule("iblock")):
            if (!$arFields["WF_PARENT_ELEMENT_ID"])
            {
                $res = CIBlockElement::GetList(Array(),Array("ID"=>$ID),false,false,Array("ID","IBLOCK_ID","DATE_CREATE","CREATED_BY","IBLOCK_TYPE_ID","DETAIL_TEXT","PROPERTY_ELEMENT"));
                if($ar_res = $res->GetNext())
                {                     
                    if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types_comment))
                    {                                    
                            /*$element = $ar_res['PROPERTY_ELEMENT_VALUE'];                            
                            $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$ar_res["IBLOCK_ID"],"PROPERTY_ELEMENT"=>$element));
                            while($ar_res2 = $res->Fetch())
                            {
                                $id[] = $ar_res2["ID"];                                    
                            }

                            $ids = implode(", ",$id);
                            if ($ids)
                            {
                                global $DB;
                                $ids = $DB->ForSql($ids);
                                $sql = "SELECT SUM(DETAIL_TEXT) as RATIO FROM b_iblock_element WHERE ID IN (".$ids.")";
                                $a = $DB->Query($sql);
                                if ($r = $a->Fetch())
                                    $ratio = $r["RATIO"];
                            }
                            $count = count($id)-1;
                            if ($count>0)
                                $ratio = ($ratio-(int)$ar_res["DETAIL_TEXT"])/$count;
                            else
                                $ratio = 0;
                            CIBlockElement::SetPropertyValuesEx($element, false, array("COMMENTS" => $count));
                            CIBlockElement::SetPropertyValuesEx($element, false, array("RATIO" => $ratio));*/
                            $id = array();
                            $ii = array();
                            
                            $element = $ar_res['PROPERTY_ELEMENT_VALUE'];
                            $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arFields["IBLOCK_ID"],"PROPERTY_ELEMENT"=>$element,"!ID"=>$ID));
                            while($ar_res2 = $res->Fetch())
                            {
                                if ($ar_res2["ACTIVE"]=="Y")
                                    $id[] = $ar_res2["ID"];         
                                if ($ar_res2["DETAIL_TEXT"]>0){
                                    $ii[] = $ar_res2["ID"];
                                }
                            }

                            $ids = implode(", ",$ii);
                            if ($ids)
                            {
                                global $DB;
                                $ids = $DB->ForSql($ids);
                                $sql = "SELECT SUM(DETAIL_TEXT) as RATIO FROM b_iblock_element WHERE ID IN (".$ids.")";
                                $a = $DB->Query($sql);
                                if ($r = $a->Fetch())
                                    $ratio = $r["RATIO"];
                            }
                            $count = count($id);
                            $ratio = $ratio/count($ii);
                            CIBlockElement::SetPropertyValuesEx($element, false, array("COMMENTS" => $count));
                            CIBlockElement::SetPropertyValuesEx($element, false, array("RATIO" => $ratio));
                            $ress = CIBlockElement::GetByID($element);
                            if ($arrr = $ress->Fetch())
                                $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_' . $arrr["IBLOCK_ID"]);
                    }
                    if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types_for_account))
                    {
                        CModule::IncludeModule("sale");	
                        if ($ar_res["IBLOCK_TYPE_ID"]=="reviews")
                            $uda = "отзыва";
                        if ($ar_res["IBLOCK_TYPE_ID"]=="blogs")
                            $uda = "поста";
                        if ($ar_res["IBLOCK_TYPE_ID"]=="cookery")
                            $uda = "рецепта";
                        if ($ar_res["IBLOCK_TYPE_ID"]=="comment")
                            $uda = "комментария";
//                        $uda_date2 = date("d.m.Y H:i:s",strtotime($ar_res["DATE_CREATE"]." + 2 second"));
//                        $dbAccountCurrency = CSaleUserTransact::GetList(
//                            array(),
//                            array("USER_ID" => $ar_res["CREATED_BY"],">=TRANSACT_DATE"=>$ar_res["DATE_CREATE"],"<=TRANSACT_DATE"=>$uda_date2),
//                            false,
//                            false
//                        );
//                        if ($arAccountCurrency = $dbAccountCurrency->Fetch())
//                        {
//                            CSaleUserAccount::UpdateAccount(
//                                $ar_res["CREATED_BY"],
//                                ($arAccountCurrency["AMOUNT"]*-1),
//                                "RUB",
//                                "Удаление ".$uda
//                            );
//                        }
                    }
                }
            }
        endif;
        return true;
    }
    
    /*
     * Checking parent section for comments (afisha, maste_classes etc.)
     * $code - iblock type of element
     * return int (SECTION_ID)
     */
    private function hasParentClassArticles($code)
    {
        $arCommentsIB = getArIblock("comments", CITY_ID);
        $dbFilter = Array( 
                            "IBLOCK_TYPE"=>"comments",
                            "IBLOCK_ID"=>$arCommentsIB["ID"],
                            "CODE"=>trim($code)
                        );                
        //Проверяем существует ли родительский раздел для комментариев (Интервью, Мастер-классы и т.д.)
        $db_list = CIBlockSection::GetList(Array(), $dbFilter, false, Array("ID"));
        if (!$arSection = $db_list->GetNext())
        {
            //нет? добавляем! 
            $parent_section_name = CIBlockType::GetByIDLang(trim($code), LANG);
            $bs = new CIBlockSection;
            $arNew = Array(  "ACTIVE" => "Y",  
                                "IBLOCK_ID" => $arCommentsIB["ID"],  
                                "NAME" => $parent_section_name["NAME"],
                                "CODE" => trim($code)
                             );
            $ID = $bs->Add($arNew);  
            $res = ($ID>0);
            if(!$res)  
            {
                $APPLICATION->ThrowException($bs->LAST_ERROR);
                return false;
            }
            $arSection["ID"] = $ID;
        }        
        return $arSection["ID"];
    }
    
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {

        //  установка улицы - яндекс геокодирование
        if($arFields['IBLOCK_ID']==11||$arFields['IBLOCK_ID']==12){
            $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
            if($ar_res = $res->GetNext())
            {
                if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                    global $APPLICATION;
                    // city_id не установлен then:
                    if ($APPLICATION->GetCurDir() != '/bitrix/admin/') {
                        $street_ib_id = getArIblock("streets", $ar_res['CODE']);
                        self::getSetStreet($arFields['IBLOCK_ID'], $street_ib_id['ID'], $arFields['ID']);
                    }
                }
            }
        }





        $iblock_types_catalog = Array("catalog");
        $iblock_types_comment = Array("comment");
        
        if(CModule::IncludeModule("iblock")):
            if (!$arFields["WF_PARENT_ELEMENT_ID"])
            {
                $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
                if($ar_res = $res->Fetch())
                {
                    if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types_catalog))
                    {
                        //  change CODE from _ to - for new rests
                        if($arFields['ID']>2472961&&$arFields['CODE']){
                            $arFields['CODE'] = preg_replace('/_/','-',$arFields['CODE']);
                        }

                        if(is_array($arFields["PROPERTY_VALUES"]))
                        {
                            $res = CIBlockProperty::GetByID("map", $arFields["IBLOCK_ID"], "catalog");
                            if($ar_res_map = $res->GetNext())
                            {
                                $map = $ar_res_map["ID"];
                                foreach ($arFields['PROPERTY_VALUES'][$map] as $val)
                                {
                                    if($val["VALUE"]){
                                        $map_values = $val["VALUE"];
                                    }
                                }

                                if($map_values){
                                    $temp = explode(',',$map_values);
                                    $lat = floatval($temp[0]);
                                    $lon = floatval($temp[1]);
                                    $arFields["PROPERTY_VALUES"]["lat"] = $lat;
                                    $arFields["PROPERTY_VALUES"]["lon"] = $lon;
                                }
                            }
                        }
                    }
                    else {
                        //  change CODE from _ to - for all resources
                        $exept_ib_ids_arr = array(4514);
                        if(!in_array($arFields['IBLOCK_ID'],$exept_ib_ids_arr)) {
                            if ($arFields['ID'] > 2484635 && $arFields['CODE']) {
                                $arFields['CODE'] = preg_replace('/_/', '-', $arFields['CODE']);
                            }
                        }
                    }



                    if ($ar_res["IBLOCK_TYPE_ID"]=="rest_group")
                    {
                        $r = CIBlock::GetByID($arFields["IBLOCK_ID"]);
                        if ($a = $r->Fetch())
                        {
                            $arIB = getArIblock("catalog", $a["CODE"]);
                            $els_o = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arIB["ID"],"PROPERTY_rest_group"=>$arFields["ID"]),false,false,Array("ID","NAME"));
                            while ($els = $els_o->Fetch())
                            {                                
                                CIBlockElement::UpdateSearch($els["ID"], true);
                            }                            
                        }                        
                    }
                }                
            }
        endif;


        global $APPLICATION;

        //  установка координат по привязке ресторана в другие блоки
        if(((int)$arFields["IBLOCK_ID"]==223||(int)$arFields["IBLOCK_ID"]==222)&&$APPLICATION->GetCurDir()=='/bitrix/admin/') {
//            AddMessage2Log($arFields);
            $properties = CIBlockProperty::GetList(Array("sort" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => (int)$arFields["IBLOCK_ID"], "CODE" => "RESTORAN"));
            if ($prop_fields = $properties->GetNext()) {
                $property_restoran_id = $prop_fields["ID"];
                $property_restoran_iblock_id = $prop_fields["LINK_IBLOCK_ID"];
            }

            $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$arFields["IBLOCK_ID"], "CODE"=>"lat"));
            if($prop_fields = $properties->GetNext())
            {
                $property_lat_id = $prop_fields["ID"];
            }
            $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$arFields["IBLOCK_ID"], "CODE"=>"lon"));
            if($prop_fields = $properties->GetNext())
            {
                $property_lon_id = $prop_fields["ID"];
            }

            if ($property_restoran_id&&$property_restoran_iblock_id)
            {
                foreach($arFields["PROPERTY_VALUES"][$property_restoran_id] as $rest_vals)
                {
                    if ($rest_vals)
                    {
                        if (is_array($rest_vals)&&count($rest_vals["VALUE"])>0)
                        {
                            $rest_id = $rest_vals["VALUE"];
                        }
                        else
                            $rest_id = $rest_vals;
                    }
                }

                if ($rest_id)
                {
                    if ($property_lat_id)
                    {
                        $lat = array();
                        $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "lat"));
                        while ($ob = $res->GetNext())
                        {
                            $lat[] = $ob['VALUE'];
                        }
                        $arFields["PROPERTY_VALUES"][$property_lat_id] = $lat;
//                        $arFields["PROPERTY_VALUES"] = array_merge($arFields["PROPERTY_VALUES"],array($property_lat_id=>$lat));
                    }

                    if ($property_lon_id)
                    {
                        $lon = array();
                        $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "lon"));
                        while ($ob = $res->GetNext())
                        {
                            $lon[] = $ob['VALUE'];
                        }
                        $arFields["PROPERTY_VALUES"][$property_lon_id] = $lon;
//                        $arFields["PROPERTY_VALUES"] = array_merge($arFields["PROPERTY_VALUES"],array($property_lon_id=>$lon));
                    }
                }
                //AddMessage2Log($arFields);
            }
        }

        return true;
    }
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        CModule::IncludeModule("iblock");

        // синхронизация англ русс ресторанов
        $en_ru_synchronize = array(11,12,3566,3567);
        $en_ru_synchronize_compare = array(11 => 2586, 12 => 2587, 3566 => 3624, 3567 => 3625);     // сопоставление id инфоблоков
        $en_ru_compare_sleeping_rest = array(1419 => 2021, 1468 => 2040, 2573 => 2724, 2556 => 2707);   // сопоставление id свойства sleeping_rest

        if(in_array($arFields['IBLOCK_ID'],$en_ru_synchronize)){
            $res = CIBlockElement::GetByID($arFields['ID']);
            if($ar_res = $res->Fetch()){
                $res = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields["ID"], "sort", "asc", array("CODE" => "sleeping_rest")); // редактируемого элемента
                if ($ob_with_prop = $res->Fetch())
                {
                    $get_en_obj = CIBlockElement::GetList(Array(), array('CODE'=>$ar_res['CODE'],'IBLOCK_ID'=>$en_ru_synchronize_compare[$arFields['IBLOCK_ID']]), false, Array('nTopCount'=>1), array('ID'));
                    if($ob = $get_en_obj->Fetch())
                    {
                        CIBlockElement::SetPropertyValuesEx($ob['ID'], $en_ru_synchronize_compare[$arFields['IBLOCK_ID']], array('sleeping_rest' =>$en_ru_compare_sleeping_rest[$ob_with_prop['VALUE']]));
                    }
                }
            }
        }

        $res = CIBlockElement::GetByID($arFields["ID"]);
        if($ar_res = $res->GetNext())
        {
            $iblock_type_id = $ar_res["IBLOCK_TYPE_ID"];

            if ($iblock_type_id=="comment")
            {
                if($ar_res["ACTIVE"]=="N")
                {
                    $res22 = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arFields["ID"], "sort", "asc", array("CODE" => "ELEMENT"));
                    if ($ob = $res22->GetNext())
                    {
                        $element = $ob['VALUE'];
                    }
//                $res22 = CIBlockProperty::GetByID("ELEMENT", $arFields["IBLOCK_ID"], "comment");
//                if($ar_res22 = $res22->GetNext())
//                {
//                    foreach ($arFields['PROPERTY_VALUES'][$ar_res22["ID"]] as $el)
//                        $element = $el["VALUE"];
//                }



                    $iblock_type = $ar_res["IBLOCK_TYPE_ID"];
                    $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$ar_res["IBLOCK_ID"],"PROPERTY_ELEMENT"=>$element));
                    while($ar_res = $res->Fetch())
                    {
                        if($ar_res["ACTIVE"]=="Y")
                            $id[] = $ar_res["ID"];
                    }
                    $count = count($id);

                    CIBlockElement::SetPropertyValuesEx($element, false, array("COMMENTS" => $count));
                    $ress = CIBlockElement::GetByID($element);
                    if ($arrr = $ress->Fetch())
                        $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_'.$arrr["IBLOCK_ID"]);

                }
            }

            if ($iblock_type_id=="catalog"&&$arFields["CODE"])
            {
                $db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields["ID"], array("sort" => "asc"), Array("CODE"=>"D_N_C_LIKE_CODE_FIELD"));
                if($ar_props = $db_props->Fetch()){
                    if(!$ar_props['VALUE']){
                        $correct_code = preg_replace('/_/','-',$arFields["CODE"]);
                        CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array('LIKE_CODE_FIELD' => $correct_code));
                    }
                }
            }
        }

        
//        $up = 0;
//        CModule::IncludeModule("sale");
//        $uda_date2 = date("d.m.Y H:i:s",strtotime($ar_res["DATE_CREATE"]." + 2 second"));
//        $dbAccountCurrency = CSaleUserTransact::GetList(
//                    array(),
//                    array("USER_ID" => $ar_res["CREATED_BY"],">=TRANSACT_DATE"=>$ar_res["DATE_CREATE"],"<=TRANSACT_DATE"=>$uda_date2),
//                    false,
//                    false
//                );
//        if (!$arAccountCurrency = $dbAccountCurrency->Fetch())
//        {
//
//            if ($iblock_type_id=='comment')
//            {
//                if ($ar_res["ACTIVE"]=="Y")
//                {
//                    $up = 1;
////                    Restics::UpdateUserAccount($ar_res["CREATED_BY"],10,"Добавление комментария +10");
////                    CSaleUserAccount::UpdateAccount(
////                        $ar_res["CREATED_BY"],
////                        10,
////                        "RUB",
////                        "Добавление комментария +10"
////                    );
//                }
//            }
//            elseif($iblock_type_id=="blogs")
//            {
//                    if (($ar_res["ACTIVE"]=="Y"))
//                    {
//                        $up = 1;
////                        Restics::UpdateUserAccount($ar_res["CREATED_BY"],50,"Добавление поста в блог +50");
////                        CSaleUserAccount::UpdateAccount(
////                            $ar_res["CREATED_BY"],
////                            50,
////                            "RUB",
////                            "Добавление поста в блог +50"
////                        );
//                    }
//            }
//            elseif ($iblock_type_id=="cookery" && $arFields["IBLOCK_ID"]==139)
//            {
//                    if (substr_count($ar_res["DETAIL_TEXT"], "#FID_")>0)
//                    {
//                        if ($ar_res["ACTIVE"]=="Y")
//                        {
//                            $up = 1;
////                            Restics::UpdateUserAccount($ar_res["CREATED_BY"],100,"Добавление рецепта c шагами +100");
////                            CSaleUserAccount::UpdateAccount(
////                                $ar_res["CREATED_BY"],
////                                100,
////                                "RUB",
////                                "Добавление рецепта c шагами +100"
////                            );
//                        }
//                    }
//                    else
//                    {
//                        if ($ar_res["ACTIVE"]=="Y")
//                        {
//                            $up = 1;
////                            Restics::UpdateUserAccount($ar_res["CREATED_BY"],50,"Добавление рецепта +50");
////                            CSaleUserAccount::UpdateAccount(
////                                $ar_res["CREATED_BY"],
////                                50,
////                                "RUB",
////                                "Добавление рецепта +50"
////                            );
//                        }
//                    }
//                    //mail('eka@cakelabs.ru', 'Добавлен новый рецепт', '');
//            }
//            else
//            {
//                //$photos = $arFields['PROPERTY_VALUES']["photos"];
//                if ($arFields['PROPERTY_VALUES']["photos"][0] && $ar_res["ACTIVE"]=="Y")
//                {
//                    $up = 1;
////                    Restics::UpdateUserAccount($ar_res["CREATED_BY"],100,"Добавление отзыва с фото +100");
////                    CSaleUserAccount::UpdateAccount(
////                        $ar_res["CREATED_BY"],
////                        100,
////                        "RUB",
////                        "Добавление отзыва с фото +100"
////                    );
//                }
//                elseif ($ar_res["ACTIVE"]=="Y")
//                {
//                    $up = 1;
////                    Restics::UpdateUserAccount($ar_res["CREATED_BY"],50,"Добавление отзыва +50");
////                    CSaleUserAccount::UpdateAccount(
////                        $ar_res["CREATED_BY"],
////                        50,
////                        "RUB",
////                        "Добавление отзыва +50"
////                    );
//                }
//            }
//            if ($up==1)
//            {
////                $dbAccountCurrency = CSaleUserTransact::GetList(
////                    array("TRANSACT_DATE"=>"DESC"),
////                    array("USER_ID" => $ar_res["CREATED_BY"]),
////                    false,
////                    Array("nTopCount"=>1)
////                );
////                if ($arAccountCurrency = $dbAccountCurrency->Fetch())
////                {
////                    CSaleUserTransact::Update(
////                        $arAccountCurrency["ID"],
////                        Array("TRANSACT_DATE"=>$ar_res["DATE_CREATE"])
////                    );
////                }
//            }
//        }
        /*$iblock_types_comment = Array("comment");
        
        if(CModule::IncludeModule("iblock")):
            if (!$arFields["WF_PARENT_ELEMENT_ID"])
            {
                $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
                if($ar_res = $res->GetNext())
                { 
                    if ($arFields["ACTIVE"]=="Y"):
                        if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types_comment))
                        {                        
                            $res = CIBlockProperty::GetByID("ELEMENT", $arFields["IBLOCK_ID"], "comment");
                            if($ar_res = $res->GetNext())
                            {
                                foreach ($arFields['PROPERTY_VALUES'][$ar_res["ID"]] as $el)
                                    $element = $el["VALUE"];
                                $res = CIBlockProperty::GetByID("IS_SECTION", $arFields["IBLOCK_ID"], "comment");
                                if($ar_res = $res->GetNext())
                                {
                                    foreach ($arFields['PROPERTY_VALUES'][$ar_res["ID"]] as $el2)
                                    $section = $el2["VALUE"];
                                }
                                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arFields["IBLOCK_ID"],"PROPERTY_ELEMENT"=>$element));
                                while($ar_res = $res->Fetch())
                                {
                                    $id[] = $ar_res["ID"];                                    
                                }
                                
                                $ids = implode(", ",$id);
                                if ($ids)
                                {
                                    global $DB;
                                    $ids = $DB->ForSql($ids);
                                    $sql = "SELECT SUM(DETAIL_TEXT) as RATIO FROM b_iblock_element WHERE ID IN (".$ids.")";
                                    $a = $DB->Query($sql);
                                    if ($r = $a->Fetch())
                                        $ratio = $r["RATIO"];
                                }
                                $count = count($id);
                                $ratio = $ratio/$count;
                                
                                if ($section=="Y")
                                {
                                    $bs = new CIBlockSection;
                                    if ($ratio)
                                        $arFields = Array (
                                            "ACTIVE" => "Y",
                                            "UF_COMMENTS" => $count,
                                            "UF_RATIO" => $ratio
                                        );
                                    else
                                        $arFields = Array (
                                            "ACTIVE" => "Y",
                                            "UF_COMMENTS" => $count
                                        );
                                    $res = $bs->Update($element, $arFields);
                                }
                                else
                                {
                                    $PROP = array();
                                    $PROP["RATIO"] = $ratio;
                                    $PROP["COMMENTS"] = $count;
                                    
                                    CIBlockElement::SetPropertyValuesEx($element, false, array("COMMENTS" => $count));
                                    CIBlockElement::SetPropertyValuesEx($element, false, array("RATIO" => $ratio));
                                    //CIBlockElement::SetPropertyValueCode($element, "RATIO", $ratio);                                  
                                    //$rrr = CIBlockElement::SetPropertyValuesEx($element, 117, array("COMMENTS" => $count));
                                    
                                }
                            }
                        }
                    endif;
                }
            }
        endif;
        return true;*/


    }

    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
        if($ar_res = $res->GetNext())
        {
            if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                global $APPLICATION;
                // city_id не установлен then:
                if ($APPLICATION->GetCurDir() != '/bitrix/admin/') {
                    $street_ib_id = getArIblock("streets", $ar_res['CODE']);
                    self::getSetStreet($arFields['IBLOCK_ID'], $street_ib_id['ID'], $arFields['ID']);
                }

                if($arFields["CODE"]){
                    $correct_code = preg_replace('/_/','-',$arFields["CODE"]);
                    CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array('LIKE_CODE_FIELD' => $correct_code));
                }

            }
        }

        $iblock_types_comment = Array("comment","reviews");

        if(CModule::IncludeModule("iblock")):
            if (!$arFields["WF_PARENT_ELEMENT_ID"])
            {
                //$res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
                if($ar_res)
                {
                    $iblock_type = $ar_res["IBLOCK_TYPE_ID"];                    
                    if ($arFields["ACTIVE"]=="Y"):
                        if (in_array($ar_res["IBLOCK_TYPE_ID"],$iblock_types_comment))
                        {
                            $iblock_type = $ar_res["IBLOCK_TYPE_ID"];
                                $element = $arFields['PROPERTY_VALUES']["ELEMENT"];
                                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arFields["IBLOCK_ID"],"PROPERTY_ELEMENT"=>$element));
                                while($ar_res = $res->Fetch())
                                {
                                    if($ar_res["ACTIVE"]=="Y")
                                        $id[] = $ar_res["ID"];         
                                    if ($ar_res["DETAIL_TEXT"]>0){
                                        $ii[] = $ar_res["ID"];
                                    }
                                }
                                
                                $ids = implode(", ",$ii);
                                if ($ids)
                                {
                                    global $DB;
                                    $ids = $DB->ForSql($ids);
                                    $sql = "SELECT SUM(DETAIL_TEXT) as RATIO FROM b_iblock_element WHERE ID IN (".$ids.")";
                                    $a = $DB->Query($sql);
                                    if ($r = $a->Fetch())
                                        $ratio = $r["RATIO"];
                                }
                                $count = count($id);
                                $ratio = $ratio/count($ii);

                                CIBlockElement::SetPropertyValuesEx($element, false, array("COMMENTS" => $count));
                                CIBlockElement::SetPropertyValuesEx($element, false, array("RATIO" => $ratio));
                                $ress = CIBlockElement::GetByID($element);
                                if ($arrr = $ress->Fetch())
                                    $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_'.$arrr["IBLOCK_ID"]);
                                                                                                
                        }
                    endif;                    
                    if ($iblock_type=='comment')
                    {

                        $re = CIBlockElement::GetByID($element);
                        if ($a = $re->Fetch())
                        {
                            if ($a["IBLOCK_TYPE_ID"]=="reviews")
                            {
                                $db_props = CIBlockElement::GetProperty($a["IBLOCK_ID"], $a["ID"], array("sort" => "asc"), Array("CODE"=>"ELEMENT"));
                                if($ar_props = $db_props->Fetch())
                                {
                                        $element2 = IntVal($ar_props["VALUE"]);                                                    
                                        $resr = CIBlockElement::GetByID($element2);
                                        if ($a2 = $resr->Fetch())
                                            $restoran_name = $a2["NAME"];
                                }
                                else
                                    $restoran_name = " ";
                            }
                        }
                        else
                            $restoran_name = "";
                        $arEventFields = array(
                            "COMMENT" => $arFields["PREVIEW_TEXT"],
                            "RATIO" => $arFields["DETAIL_TEXT"],
                            "NAME" => $arFields["NAME"],
                            "IBLOCK_TYPE_ID" => $iblock_type,
                            "IBLOCK_ID" => $arFields["IBLOCK_ID"],
                            "ID" => $arFields["ID"],
                            "URL" => $_SERVER["HTTP_REFERER"],
                            "RESTORAN" => $restoran_name,
                            "IP" => $_SERVER["REMOTE_ADDR"]
                        );
                        $res = CEvent::Send("NEW_COMMENT", "s1", $arEventFields);

                        CModule::IncludeModule("sale");		
                        if ($arFields["ACTIVE"]=="Y")
                        {
                            Restics::UpdateUserAccount($arFields["CREATED_BY"],10,"Добавление комментария +10");
//                            CSaleUserAccount::UpdateAccount(
//                                $arFields["CREATED_BY"],
//                                10,
//                                "RUB",
//                                "Добавление комментария +10" 
//                            );
                        }
                    }
                    elseif($iblock_type=="blogs")
                    {         
                            if (($arFields["ACTIVE"]=="Y"))
                            {
                                CModule::IncludeModule("sale");	
                                Restics::UpdateUserAccount($arFields["CREATED_BY"],50,"Добавление поста в блог +50");
//                                CSaleUserAccount::UpdateAccount(
//                                                $arFields["CREATED_BY"],
//                                                50,
//                                                "RUB",
//                                                "Добавление поста в блог +50" 
//                                );
                            }
                            $arEventFields = array(
                                        "POST" => $arFields["PREVIEW_TEXT"],
                                        "NAME" => $arFields["NAME"],
                                        "IBLOCK_TYPE_ID" => $ar_res["IBLOCK_TYPE_ID"],
                                        "IBLOCK_ID" => $arFields["IBLOCK_ID"],                                        
                                        "ID" => $arFields["ID"],
                            );
                            $res = CEvent::Send("NEW_POST", "s1", $arEventFields);
                    }
                    elseif ($iblock_type=="cookery" && $arFields["IBLOCK_ID"]==139)
                    {         
                            CModule::IncludeModule("sale");	
                            if (substr_count($arFields["DETAIL_TEXT"], "#FID_")>0)
                            {
                                if ($arFields["ACTIVE"]=="Y")
                                {
                                    Restics::UpdateUserAccount($arFields["CREATED_BY"],100,"Добавление рецепта c шагами +100");
//                                    CSaleUserAccount::UpdateAccount(
//                                        $arFields["CREATED_BY"],
//                                        100,
//                                        "RUB",
//                                        "Добавление рецепта c шагами +100" 
//                                    );                                
                                }
                            }
                            else
                            {
                                if ($arFields["ACTIVE"]=="Y")
                                {
                                    Restics::UpdateUserAccount($arFields["CREATED_BY"],50,"Добавление рецепта +50");
//                                    CSaleUserAccount::UpdateAccount(
//                                        $arFields["CREATED_BY"],
//                                        50,
//                                        "RUB",
//                                        "Добавление рецепта +50" 
//                                    );
                                }
                            }
                    }                    
                    elseif($iblock_type=="resume")
                    {                              
                        global $USER;
                            $arEventFields = array(                                    
                                    "NAME" => $USER->GetFullName(),
                                    "IBLOCK_TYPE_ID" => $ar_res["IBLOCK_TYPE_ID"],
                                    "IBLOCK_ID" => $arFields["IBLOCK_ID"],                                        
                                    "ID" => $arFields["ID"],
                            );                            
                            $res = CEvent::Send("NEW_RESUME", "s1", $arEventFields);
                    }
                    elseif($iblock_type=="vacancy")
                    {                      
                        global $USER;
                            $arEventFields = array(                                    
                                    "NAME" => $USER->GetFullName(),
                                    "IBLOCK_TYPE_ID" => $ar_res["IBLOCK_TYPE_ID"],
                                    "IBLOCK_ID" => $arFields["IBLOCK_ID"],                                        
                                    "ID" => $arFields["ID"],
                            );
                            $res = CEvent::Send("NEW_VACANCY", "s1", $arEventFields);
                    }
                    elseif ($iblock_type=="reviews")
                    {   
                        $re = CIBlockElement::GetByID($element);
                        if ($a = $re->Fetch())
                        {
                            $restoran_name = $a["NAME"];
                        }
                        else
                            $restoran_name = "";
                        $arEventFields = array(
                            "COMMENT" => $arFields["PREVIEW_TEXT"]."\n".$arFields['PROPERTY_VALUES']["plus"]."\n".$arFields['PROPERTY_VALUES']["minus"],
                            "RATIO" => $arFields["DETAIL_TEXT"],
                            "NAME" => $arFields["NAME"],
                            "IBLOCK_TYPE_ID" => $iblock_type,
                            "IBLOCK_ID" => $arFields["IBLOCK_ID"],
                            "ID" => $arFields["ID"],
                            "URL" => $_SERVER["HTTP_REFERER"],
                            "RESTORAN" => $restoran_name,
                            "IP" => $_SERVER["REMOTE_ADDR"]
                        );
                        $res = CEvent::Send("NEW_COMMENT", "s1", $arEventFields);

                        // send msg if video added video
                        $rsVideoProp = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"], array("sort" => "asc"), Array("CODE"=>"video"));
                        $arVideoProp = $rsVideoProp->Fetch();
                        if($arVideoProp["VALUE"]) {
                            $arEventFieldsVideo = Array(
                                "REVIEW_ID"         => $arFields["ID"],
                                "IBLOCK_ID"         => $arFields["IBLOCK_ID"],
                                "LINK_TO_VIDEO"     => CFile::GetPath($arVideoProp["VALUE"])
                            );
                            CEvent::Send("NEW_COMMENT_ADD_VIDEO", "s1", $arEventFieldsVideo);
                        }                                   
                        //$photos = $arFields['PROPERTY_VALUES']["photos"];                        
                        if (is_array($arFields['PROPERTY_VALUES']["photos"]) && $arFields["ACTIVE"]=="Y")
                        {
//                            Restics::UpdateUserAccount($arFields["CREATED_BY"],100,"Добавление отзыва с фото +100");
//                            CModule::IncludeModule("sale");
//                            CSaleUserAccount::UpdateAccount(
//                                $arFields["CREATED_BY"],
//                                100,
//                                "RUB",
//                                "Добавление отзыва с фото +100" 
//                            );
                        }
                        elseif ($arFields["ACTIVE"]=="Y")
                        {
//                            CModule::IncludeModule("sale");
//                            Restics::UpdateUserAccount($arFields["CREATED_BY"],50,"Добавление отзыва +50");

//                            CSaleUserAccount::UpdateAccount(
//                                $arFields["CREATED_BY"],
//                                50,
//                                "RUB",
//                                "Добавление отзыва +50" 
//                            );
                        }
                    }                        
                }
            }
        endif;
        return true;
    }
    function IsSleeping($id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*2;
            $cache_id = $ibType.$ibID.$code.CITY_ID.$id."sleeping1";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $is = $vars["ELEMENT"];
            } else {
                global $DB;
                $arRestIB = getArIblock("catalog", CITY_ID);
                $id = $DB->ForSql($id);
                $sql = "SELECT ID FROM b_iblock_element WHERE CODE='".$id."' AND IBLOCK_ID=".$arRestIB["ID"];
                $res = $DB->Query($sql);
                $r = $res->Fetch();
                $db_props = CIBlockElement::GetProperty((int)$arRestIB["ID"], (int)$r["ID"], array(), Array("CODE"=>"sleeping_rest"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                        $is = true;
                }
                else
                        $is = false;
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $is
                ));
            }
            return $is;
        endif;
    }
    function IsClosed($id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*2;
            $cache_id = $ibType.$ibID.$code.CITY_ID.$id."closed";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $is = $vars["ELEMENT"];
            } else {
                global $DB;
                $arRestIB = getArIblock("catalog", CITY_ID);
                $id = $DB->ForSql($id);
                $sql = "SELECT ID FROM b_iblock_element WHERE CODE='".$id."' AND IBLOCK_ID=".$arRestIB["ID"];
                $res = $DB->Query($sql);
                $r = $res->Fetch();
                $db_props = CIBlockElement::GetProperty((int)$arRestIB["ID"], (int)$r["ID"], array(), Array("CODE"=>"no_mobile"));
                if($ar_props = $db_props->Fetch())
                {
                    if ($ar_props["VALUE"])
                        $is = true;
                }
                else
                        $is = false;
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $is
                ));
            }
            return $is;
        endif;
    }
    function IsOldNetwork($id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*2;
            $cache_id = $ibType.$ibID.$code.CITY_ID.$id."OldNetwork_1";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $is = $vars["ELEMENT"];
            } else {
                global $DB;
                $arRestIB = getArIblock("catalog", CITY_ID);
                $id = $DB->ForSql($id);
                $sql = "SELECT ID FROM b_iblock_element WHERE CODE='".$id."' AND IBLOCK_ID=".$arRestIB["ID"];
                $res = $DB->Query($sql);
                $r = $res->Fetch();
                $db_props = CIBlockElement::GetProperty((int)$arRestIB["ID"], (int)$r["ID"], array(), Array("CODE"=>"REST_NETWORK"));
                if($ar_props = $db_props->Fetch())
                {
                    if($ar_props["VALUE"]){
                        $is = false;
                    }
                }

                if($is!==false){
                    $db_props = CIBlockElement::GetProperty((int)$arRestIB["ID"], (int)$r["ID"], array(), Array("CODE"=>"address"));
                    $address_count = 0;
                    while($ar_props = $db_props->Fetch())
                    {
                        if ($ar_props["VALUE"]){
                            $address_count++;
                        }
                        if($address_count>1){
                            $is = true;
                            break;
                        }
                        else {
                            $is = false;
                        }
                    }
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $is
                ));
            }
            return $is;
        endif;
    }
    //  todo is network

    function IsNetwork($id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*24*30;
            $cache_id = CITY_ID.$id."NetworkNew0";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $is = $vars["IS_NETWORK"];
            }
            else {
                global $DB;
                $arRestIB = getArIblock("catalog", CITY_ID);
                $id = $DB->ForSql($id);
                $sql = "SELECT ID FROM b_iblock_element WHERE CODE='".$id."' AND IBLOCK_ID=".$arRestIB["ID"];
                $res = $DB->Query($sql);
                $r = $res->Fetch();
                $db_props = CIBlockElement::GetProperty((int)$arRestIB["ID"], (int)$r["ID"], array(), Array("CODE"=>"REST_NETWORK"));
                if($ar_props = $db_props->Fetch())
                {
                    if($ar_props["VALUE"]){
                        $is = true;
                    }
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "IS_NETWORK" => $is
                ));
            }
            return $is;
        endif;
    }


    /*function GetArticleNext($iblock_id, $id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*24;
            $cache_id = $iblock_id.$id;
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $next = $vars["ELEMENT"];
            } else {
                $arFilter = Array(
                    "IBLOCK_ID"=>IntVal($iblock_id), 
                    "ACTIVE"=>"Y", 
                    ">ID"=> IntVal($id),
                );
                $res = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, false, Array("ID","CODE","SECTION_PAGE_URL"), Array("nTopCount"=>1));
                while($ar_fields = $res->GetNext())
                {
                    $next = $ar_fields["SECTION_PAGE_URL"];
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $next
                ));
            }
            return $next;
        endif;
    }
    
    function GetArticlePrev($iblock_id, $id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*24;
            $cache_id = $iblock_id.$id;
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $prev = $vars["ELEMENT"];
            } else {
                $arFilter = Array(
                    "IBLOCK_ID"=>IntVal($iblock_id), 
                    "ACTIVE"=>"Y", 
                    "<ID"=> IntVal($id),
                );
                $res = CIBlockSection::GetList(Array("ID"=>"DESC"), $arFilter, false, Array("ID","CODE","SECTION_PAGE_URL"), Array("nTopCount"=>1));
                while($ar_fields = $res->GetNext())
                {
                    $prev = $ar_fields["SECTION_PAGE_URL"];
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $prev
                ));
            }
            return $prev;
        endif;
    }*/
    
    function GetArticlePrev($iblock_id, $id, $section_id = false)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 14400;
            $cache_id = $iblock_id.$id."prev";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $prev = $vars["ELEMENT"];
            } else {
                $arFilter = Array(
                    "IBLOCK_ID"=>IntVal($iblock_id), 
                    "ACTIVE"=>"Y", 
                    "<ID"=> IntVal($id),
                );
                if ($section_id)
                    $arFilter["SECTION_ID"] = $section_id; 
                                
                $res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array("nTopCount"=>1), Array("ID","CODE","DETAIL_PAGE_URL"));
                while($ar_fields = $res->GetNext())
                {
                    $prev = $ar_fields["DETAIL_PAGE_URL"];
                    //$prev = str_replace("content", CITY_ID, $prev);
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $prev
                ));
            }
            return $prev;
        endif;
    }
    
    function GetArticleNext($iblock_id, $id, $section_id = false)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 14400;
            $cache_id = $iblock_id.$id."next";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $prev = $vars["ELEMENT"];
            } else {
                $arFilter = Array(
                    "IBLOCK_ID"=>IntVal($iblock_id), 
                    "ACTIVE"=>"Y", 
                    ">ID"=> IntVal($id),
                );
                if ($section_id)
                    $arFilter["SECTION_ID"] = $section_id; 

                $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), Array("ID","CODE","DETAIL_PAGE_URL"));
                while($ar_fields = $res->GetNext())
                {
                    $prev = $ar_fields["DETAIL_PAGE_URL"];
                    //$prev = str_replace("content", CITY_ID, $prev);
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ELEMENT"    => $prev
                ));
            }
            return $prev;
        endif;
    }
    
    function getCookerySection($section)
    {
        global $DB;
        $id_section = $DB->ForSql($section);
        /*$sql = "SELECT b.CODE as CODE FROM b_iblock_section a
                JOIN b_iblock_section b ON b.ID = a.IBLOCK_SECTION_ID
                WHERE a.ID = ".$id_section." LIMIT 1";*/
        $sql = "SELECT CODE as CODE FROM b_iblock_section             
                WHERE ID = ".$id_section." LIMIT 1";
        $r = $DB->Query($sql);
        if ($ar = $r->Fetch())
        {
            $code = $ar["CODE"];
        }
        if ($code)
            return $code;
        return false;
    }
    
    function GetRestIDByCode($code)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 0;//60*60*24;
            $cache_id = $iblock_id.$id.$code."idcode";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $id = $vars["ID"];
            } else {
                $arRestIB = getArIblock("catalog", CITY_ID);
                $arFilter = Array(
                    "IBLOCK_ID"=>IntVal($arRestIB["ID"]), 
                    "ACTIVE"=>"Y", 
                    "CODE"=> $code,
                );
                $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), Array("ID"));
                if($ar_fields = $res->GetNext())
                {
                    //v_dump($ar_fields);
                    $id = $ar_fields["ID"];
                    
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ID"    => $id
                ));
            }
            return $id;
        endif;
    }
    
    function GetTypeBySectionCode($code)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*24;
            $cache_id = $code."typecode1".CITY_ID;
            if ($CITY_ID=="nsk")
                $cache_id.=rand (1, 99999);
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $id = $vars["ID"];
            } else {
                global $DB;                
                $sql = "SELECT BI.IBLOCK_TYPE_ID FROM b_iblock_section AS BIS
                        JOIN b_iblock AS BI ON BI.ID = BIS.IBLOCK_ID
                        WHERE BIS.CODE='".$DB->ForSql($code)."' AND BI.CODE='".$DB->ForSql(CITY_ID)."'";
                $res = $DB->Query($sql);
                $r = $res->Fetch();
                $id = $r["IBLOCK_TYPE_ID"];
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ID"    => $id
                ));
            }
            return $id;
        endif;
    }
    
    function GetLinkedIds($iblock_id,$code)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60*24;
            $cache_id = $iblock_id.$code."linkedtypecode";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $id = $vars["ID"];
            } else {
                CModule::IncludeModule("iblock");
                $res = CIblockElement::GetList(Array(),Array("IBLOCK_ID"=>$iblock_id,"CODE"=>$code),false,false,Array("ID","PROPERTY_ELEMENTS"));
                while($r = $res->Fetch())
                {
                    $id[] = $r["PROPERTY_ELEMENTS_VALUE"];
                }
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ID"    => $id
                ));
            }
            return $id;
        endif;
    }
    
    function getOldOpinionID($id)
    {        
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 0;//60*60*24;
            $cache_id = $id."oldopinionid";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $id = $vars["ID"];
            } else {
                $arRestIB = getArIblock("reviews", CITY_ID);
                $res = CIblockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","PROPERTY_OLD_OPINION_ID"=>$id,"NAME"=>"old.restoran.ru"),false,false,Array("ID","CREATED_BY"));
                if($r = $res->Fetch())
                {
                    if ($r["CREATED_BY"]==130)
                        $id = $r["ID"];
                    else
                        $id = false;
                }
                else
                    $id = false;
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ID"    => $id
                ));
            }
            return $id;
        endif;
    }
    
    function get_sm_subway($id)
    {
        if(CModule::IncludeModule("iblock")):
            $obCache = new CPHPCache;
            $life_time = 60*60;
            $cache_id = $id."";
            if($obCache->InitCache($life_time, $cache_id, "/")) {
                $vars = $obCache->GetVars();
                $ids = $vars["ID"];
            } else {
                $arRestIB = getArIblock("metro", CITY_ID);
                $res = CIblockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"ACTIVE"=>"Y","PROPERTY_near"=>$id),false,false,Array("ID","NAME"));
                while($r = $res->Fetch())
                {                    
                    $ids[] = $r["ID"];
                }                
            }
            if($obCache->StartDataCache()) {
                $obCache->EndDataCache(array(
                    "ID"    => $ids
                ));
            }
            return $ids;
        endif;
    }

    function OnAfterIBlockElementDeleteHandler($arFields)
    {
        $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
        if($ar_res = $res->GetNext()) {
            if (in_array($ar_res["IBLOCK_TYPE_ID"],array('catalog'))) {
                self::ObjectSiteMapJson($arFields['ID'], $arFields['IBLOCK_ID'],$ar_res['CODE'],'Y');
            }
        }
    }

    //  TODO поиск по улицам

    function getSetStreet($catalog_iblock_id,$street_iblock_id,$catalog_element_id){

        $db_props = CIBlockElement::GetProperty($catalog_iblock_id, $catalog_element_id, array("sort" => "asc"), Array("CODE"=>"lat"));
        if($ar_props = $db_props->Fetch())
            $obj_lat_prop_val = $ar_props["VALUE"];
        $db_props = CIBlockElement::GetProperty($catalog_iblock_id, $catalog_element_id, array("sort" => "asc"), Array("CODE"=>"lon"));
        if($ar_props = $db_props->Fetch())
            $obj_lon_prop_val = $ar_props["VALUE"];
        $db_props = CIBlockElement::GetProperty($catalog_iblock_id, $catalog_element_id, array("sort" => "asc"), Array("CODE"=>"address"));
        if($ar_props = $db_props->Fetch())
            $obj_address_prop_val = $ar_props["VALUE"];

        //FirePHP::getInstance()->info($obj_address_prop_val,'$obj_address_prop_val');
        $street_name = self::yaGeocode($obj_lon_prop_val, $obj_lat_prop_val, $obj_address_prop_val);
        //AddMessage2Log($street_name,'$street_name START');
        if ($street_name) {
            //AddMessage2Log($street_name,'$street_name');
            $arParams = array("replace_space"=>"-","replace_other"=>"-");
            $street_translit = Cutil::translit($street_name,"ru",$arParams);
            $street_res = CIBlockElement::GetList(Array('NAME' => 'ASC'), array('IBLOCK_ID' => $street_iblock_id, array("LOGIC" => "OR","NAME"=>$street_name,'CODE'=>$street_translit)), false, Array("nTopCount" => 1), array('ID'));
            if ($street_ob = $street_res->Fetch()) {    // если улица уже существует

                //  id групп инфоблока улиц
                $db_list = CIBlockSection::GetList(array(),array('IBLOCK_ID'=>$street_iblock_id),false,array('CODE','ID'));
                while($ar_result = $db_list->Fetch())
                {
                    $street_section_ids[$ar_result['CODE']] = $ar_result['ID'];
                }

                //  собираем группы элемента
                $catalog_section_obj = CIBlockElement::GetElementGroups($catalog_element_id, false, array('CODE'));
                while($ar_group = $catalog_section_obj->Fetch()){
                    $catalog_element_group_code[] = $ar_group['CODE'];
                }

                // собираем id групп улицы
                $db_old_groups = CIBlockElement::GetElementGroups($street_ob['ID'], false, array('CODE', 'ID'));
                while ($ar_group = $db_old_groups->Fetch()) {
                    $street_group_code['CODE'][] = $ar_group['CODE'];
                    $street_group_code['ID'][] = $ar_group['ID'];
                }
                //  идем по группам элемента каталога(ресторана) и дописы в массив групп улицы



                foreach($catalog_element_group_code as $catalog_element_section_code){
                    if (!in_array($catalog_element_section_code,$street_group_code['CODE'])) {
                        $street_group_code['ID'][] = $street_section_ids[$catalog_element_section_code];
                    }
                }
                CIBlockElement::SetElementSection($street_ob['ID'], $street_group_code['ID']);

                //  установить улицу элементу каталога
                CIBlockElement::SetPropertyValuesEx($catalog_element_id, $catalog_iblock_id, array('STREET' => $street_ob['ID']));
            }
            else {  //  если улица не найдена
                $el = new CIBlockElement;
                //  добавить и установить элементу каталога

                $db_old_groups = CIBlockElement::GetElementGroups($catalog_element_id, false, array('CODE'));
                while($ar_group = $db_old_groups->Fetch()){
                    $this_group_code['CODE'][] = $ar_group['CODE'];
                }


                foreach($this_group_code['CODE'] as $section_code){
                    $arFilter = Array('IBLOCK_ID'=>$street_iblock_id, 'GLOBAL_ACTIVE'=>'Y', 'CODE'=>$section_code);
                    $db_list = CIBlockSection::GetList(Array(), $arFilter, false, array('ID'));
                    if($ar_result = $db_list->Fetch())
                    {
                        $this_group_code['ID'][] = $ar_result['ID'];
                    }
                }

                $arLoadProductArray = Array(
                    "IBLOCK_SECTION" => $this_group_code['ID'],
                    "IBLOCK_ID" => $street_iblock_id,
                    "NAME" => "$street_name",
                    'CODE' => Cutil::translit($street_name, "ru", $arParams),
                    "ACTIVE" => "Y"
                );

                //AddMessage2Log($street_name,'$street_name');

                if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                    CIBlockElement::SetPropertyValuesEx($catalog_element_id, $catalog_iblock_id, array('STREET' => $PRODUCT_ID));
                }
            }
        }
        //AddMessage2Log($street_name, '$street_name end');
        //AddMessage2Log('end offff');
    }
    static function yaGeocode($lat,$lon,$street_name=''){
        if($street_name!=''){//!$lat||!$lon &&
            $answer = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&results=1&kind=street&geocode='.$street_name),true);
        }
        elseif($lat && $lon) {
            $answer = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&results=1&kind=street&geocode='.$lat.','.$lon),true);       //  обратное геокодирование
        }
        //AddMessage2Log($answer['response'],'response');

        if($answer['response']['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found']){
            if($answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName']){
                return $answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName'];
            }
            elseif($answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName']) {
                return $answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName'];
            }
            elseif($answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['Thoroughfare']['ThoroughfareName']) {
                return $answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['Thoroughfare']['ThoroughfareName'];
            }
        }
        else {
            return false;
        }
    }


    //  TODO custom sitemap
    const ABS_DOC_ROOT = '/home/bitrix/www';

    static function ObjectSiteMapJson($objId,$iblock_id,$IBLOCK_CODE, $delete_record='N'){

        $jsonFile = self::ABS_DOC_ROOT.'/sitemap/'.$IBLOCK_CODE.'.json';
        $sitemap_json_arr = self::json_decod_function($jsonFile);

        if($delete_record!='Y'){
            if($IBLOCK_CODE=='spb'){
                $host_str = 'https://spb.restoran.ru/';
            }
            elseif($IBLOCK_CODE=='urm'){
                $host_str = 'https://urm.restoran.ru/';
            }
            else {
                $host_str = 'https://www.restoran.ru/';
            }
            unset($sitemap_json_arr['restaurants'][$objId]);
            unset($sitemap_json_arr['banket'][$objId]);
            $db_old_groups = CIBlockElement::GetElementGroups($objId, true, array('CODE','ID'));
            while($ar_group = $db_old_groups->Fetch()){
                $arSelect = Array("ID", 'CODE','TIMESTAMP_X_UNIX');
                $arFilter = Array("IBLOCK_ID" => $iblock_id, 'ID'=>$objId, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
                $res = CIBlockElement::GetList(Array('TIMESTAMP_X_UNIX' => 'ASC'), $arFilter, false, false, $arSelect);

                while ($ob = $res->Fetch()) {
                    $ob['DETAIL_PAGE_URL'] = $host_str.$IBLOCK_CODE.'/detailed/'.$ar_group['CODE'].'/'.$ob['CODE'].'/';
                    $sitemap_json_arr[$ar_group['CODE']][$objId] = $ob;
                }
            }
        }
        else {
            unset($sitemap_json_arr['restaurants'][$objId]);
            unset($sitemap_json_arr['banket'][$objId]);
        }


        $bodyjson = json_encode($sitemap_json_arr);
        file_put_contents($jsonFile, $bodyjson);

        self::MakeCitySiteMap($iblock_id,$IBLOCK_CODE);
    }

    static function GlueAllCities() {
        CModule::IncludeModule("search");

        $f=fopen(self::ABS_DOC_ROOT."/custom_main_sitemap.xml", "w");
        $strBegin="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<sitemapindex xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
        $city_array = array('spb','msk','rga');//,'urm','tmn','kld','nsk','krd','sch','ufa','ast'
//        $city_array = array('spb');
        fwrite($f,$strBegin);
        foreach ($city_array as $city_code) {
//            $arIB = getArIblock("catalog", $city_code);
            $strTime = CSiteMap::TimeEncode(filemtime(self::ABS_DOC_ROOT."/custom_sitemap_".$city_code.".xml"));
            fwrite($f,"\t<sitemap>\n\t\t<loc>http://www.restoran.ru/custom_sitemap_".$city_code.".xml</loc>\n\t\t<lastmod>".$strTime."</lastmod>\n\t</sitemap>\n");
        }
        fwrite($f,'</sitemapindex>');
        fclose($f);
        //echo '!_finished_!';
    }

    static function MakeCitySiteMap($iblock_id,$IBLOCK_CODE){
        CModule::IncludeModule("search");
        $jsonFile = self::ABS_DOC_ROOT.'/sitemap/'.$IBLOCK_CODE.'.json';
        $sitemap_json_arr = self::json_decod_function($jsonFile);

        $strToWrite = '';

        $f=fopen(self::ABS_DOC_ROOT."/custom_sitemap_".$IBLOCK_CODE.".xml", "w");
        $strBegin="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
        fwrite($f, $strBegin);

//        $f=fopen(self::ABS_DOC_ROOT."/custom_sitemap_".$iblock_id.".xml", "a");
        $week_ago = time()-3600*7;
        foreach($sitemap_json_arr['restaurants'] as $one_city_obj){
            $strToWrite.="\t<url>\n\t\t<loc>".$one_city_obj['DETAIL_PAGE_URL']."</loc>\n\t\t<lastmod>".CSiteMap::TimeEncode($one_city_obj['TIMESTAMP_X_UNIX'])."</lastmod>\n\t\t<changefreq>weekly</changefreq>\n\t\t<priority>".($one_city_obj['TIMESTAMP_X_UNIX']>=$week_ago?"1":"0.5")."</priority>\n\t</url>\n";
        }
        foreach($sitemap_json_arr['banket'] as $one_city_obj){
            $strToWrite.="\t<url>\n\t\t<loc>".$one_city_obj['DETAIL_PAGE_URL']."</loc>\n\t\t<lastmod>".CSiteMap::TimeEncode($one_city_obj['TIMESTAMP_X_UNIX'])."</lastmod>\n\t\t<changefreq>weekly</changefreq>\n\t\t<priority>".($one_city_obj['TIMESTAMP_X_UNIX']>=$week_ago?"1":"0.5")."</priority>\n\t</url>\n";
        }

        fwrite($f,$strToWrite);


        fwrite($f,"</urlset>\n");
        fclose($f);
        //echo '!_finished_!';
    }

    static function MakeCitySiteMapJson($iblock_id,$section_code,$IBLOCK_CODE,$NEW_FILE='N'){

        $jsonFile = self::ABS_DOC_ROOT.'/sitemap/'.$IBLOCK_CODE.'.json';
        if($IBLOCK_CODE=='spb'){
            $host_str = 'https://spb.restoran.ru/';
        }
        elseif($IBLOCK_CODE=='urm'){
            $host_str = 'https://urm.restoran.ru/';
        }
        else {
            $host_str = 'https://www.restoran.ru/';
        }

        $sitemap_json_arr = array();
        if($NEW_FILE!='Y'){ //  создать новый док.(удалить все старые данные)
            $sitemap_json_arr = self::json_decod_function($jsonFile);
        }

        if(!$_REQUEST['iNumPage']){
            $iNumPage = 1;
        }
        else {
            $iNumPage = $_REQUEST['iNumPage'];
        }


        $arSelect = Array("ID", 'CODE','TIMESTAMP_X_UNIX');
        $arFilter = Array("IBLOCK_ID" => $iblock_id, 'SECTION_CODE' => $section_code, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array('TIMESTAMP_X_UNIX' => 'ASC'), $arFilter, false, Array("nPageSize" => 1000, 'iNumPage' => $iNumPage), $arSelect);
        while ($ob = $res->Fetch()) {
            $ob['DETAIL_PAGE_URL'] = $host_str.$IBLOCK_CODE.'/detailed/'.$section_code.'/'.$ob['CODE'].'/';
            $sitemap_json_arr[$section_code][$ob['ID']] = $ob;
        }

        if ($res->NavPageCount!=$iNumPage && $iNumPage<25 && $res->NavPageCount!=0) {    // не последняя

            $bodyjson = json_encode($sitemap_json_arr);
            if(file_put_contents($jsonFile, $bodyjson)){
                LocalRedirect('/sitemap-test.php?iNumPage='.++$iNumPage, true);
            }
            else {
                echo 'coudnt put file (';
            }
        }
        else {
            $bodyjson = json_encode($sitemap_json_arr);
            file_put_contents($jsonFile, $bodyjson);

            //echo '!_finished_!';
        }
    }

    static function json_decod_function($local_address){
        $src = file_get_contents($local_address);
        $obj = json_decode($src,TRUE);
        return $obj;
    }


    //  TODO 2gis

    static function GisOrganizationJson($iblock_id,$section_code,$IBLOCK_CODE,$CITY_NAME,$NEW_FILE='N'){

        $jsonFile = self::ABS_DOC_ROOT.'/2gis/2gisOrganization.json';

        $sitemap_json_arr = array();
        if($NEW_FILE!='Y'){ //  создать новый док.(удалить все старые данные)
            $sitemap_json_arr = self::json_decod_function($jsonFile);
        }

        if(!$_REQUEST['iNumPage']){
            $iNumPage = 1;
        }
        else {
            $iNumPage = $_REQUEST['iNumPage'];
        }

        echo $iblock_id.' - $iblock_id';

        $arSelect = Array("ID", 'NAME','CODE');
//,'PROPERTY_working_VALUE'=>'Да'
        $arFilter = Array("IBLOCK_ID" => $iblock_id, 'SECTION_CODE' => $section_code, "ACTIVE" => "Y", '!PROPERTY_sleeping_rest_VALUE'=>'Да');
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 500, 'iNumPage' => $iNumPage), $arSelect);

        $leftBorderCnt = $res->SelectedRowsCount();

        if($IBLOCK_CODE=='spb'){
            $host = 'http://spb.restoran.ru/';
        }
        elseif($IBLOCK_CODE=='urm'){
            $host = 'http://urm.restoran.ru/';
        }
        else {
            $host = 'http://www.restoran.ru/';
        }

        while ($ob = $res->Fetch()) {

            $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
                $sitemap_json_arr['offers'][$ob['ID']]['address'] = $ar_props['VALUE'];
            }
            $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"lat"));
            if($ar_props = $db_props->Fetch()){
                $sitemap_json_arr['offers'][$ob['ID']]['coordinates']['lat'] = $ar_props['VALUE'];
            }
            $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"lon"));
            if($ar_props = $db_props->Fetch()){
                $sitemap_json_arr['offers'][$ob['ID']]['coordinates']['lon'] = $ar_props['VALUE'];
            }
            $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"phone"));
            if($ar_props = $db_props->Fetch()){
                $sitemap_json_arr['offers'][$ob['ID']]['contacts']['phones'][0]['phone'] = $ar_props['VALUE'];
            }
            $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"working"));
            if($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE_ENUM']=='Да'){
                    $reward_value = 100;
                }
                else {
                    $reward_value = 0;
                }
            }

            $sitemap_json_arr['offers'][$ob['ID']]['id'] = $ob['ID'];
            $sitemap_json_arr['offers'][$ob['ID']]['locale'] = 'ru_RU';
            $sitemap_json_arr['offers'][$ob['ID']]['city'] = $CITY_NAME;
            $sitemap_json_arr['offers'][$ob['ID']]['name'] = $ob['NAME'];
            $sitemap_json_arr['offers'][$ob['ID']]['order_url'] = 'http://www.restoran.ru/2gis-widget-form.php?id='.$ob['ID'].'&name='.$ob['NAME'].($_REQUEST['CITY_CODE']?'&CITY_ID='.$_REQUEST['CITY_CODE']:'');
            $sitemap_json_arr['offers'][$ob['ID']]['reward']['metric'] = 'roubles';
            $sitemap_json_arr['offers'][$ob['ID']]['reward']['value'] = $reward_value;

            $sitemap_json_arr['offers'][$ob['ID']]['contacts']['url'] = $host.$IBLOCK_CODE.'/detailed/restaurants/'.$ob['CODE'].'/';
        }

        usleep(1000);

        echo $iNumPage.' $iNumPage';
        echo $res->NavPageCount.' $res->NavPageCount';

        if ($res->NavPageCount!=$iNumPage && $iNumPage<25 && $leftBorderCnt!=0) {    // не последняя

            $bodyjson = json_encode($sitemap_json_arr);
            if(file_put_contents($jsonFile, $bodyjson)){
                LocalRedirect('/2gis/generate-organization-json.php?start=Y&CITY_CODE='.$IBLOCK_CODE.'&iNumPage='.++$iNumPage, true);
            }
            else {
                echo 'coudnt put file (';
            }
        }
        else {
            $bodyjson = json_encode($sitemap_json_arr);
            file_put_contents($jsonFile, $bodyjson);

            echo '!_finished_!';
            LocalRedirect('/2gis/generate-organization-json.php?start=Y&CITY_CODE='.$IBLOCK_CODE.'&LAST_PAGE=Y', true);
        }
    }

    static function GisExternalPhotosJson($iblock_id,$section_code,$IBLOCK_CODE,$NEW_FILE='N'){

        $jsonFile = self::ABS_DOC_ROOT.'/2gis/2gisExternalPhotos.json';
        $watermark = Array(
            Array( 'name' => 'watermark',
                'position' => 'br',
                'size'=>'medium',
                'type'=>'image',
                'alpha_level'=>'40',
                'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark1.png',
            ),
        );

        $sitemap_json_arr = array();
        if($NEW_FILE!='Y'){ //  создать новый док.(удалить все старые данные)
            $sitemap_json_arr = self::json_decod_function($jsonFile);
        }

        if(!$_REQUEST['iNumPage']){
            $iNumPage = 1;
        }
        else {
            $iNumPage = $_REQUEST['iNumPage'];
        }

        $arSelect = Array("ID");
//        ,'PROPERTY_working_VALUE'=>'Да'
        $arFilter = Array("IBLOCK_ID" => $iblock_id, 'SECTION_CODE' => $section_code, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", '!PROPERTY_sleeping_rest_VALUE'=>'Да');
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 500, 'iNumPage' => $iNumPage), $arSelect);

        $leftBorderCnt = $res->SelectedRowsCount();


        while ($ob = $res->Fetch()) {
//            $sitemap_json_arr['type'] = 'branch';
//            $sitemap_json_arr['album_code'] = 'common';
            $sitemap_json_arr['objects'][$ob['ID']]['id'] = $ob['ID'];
            unset($sitemap_json_arr['objects'][$ob['ID']]['photos']);

            $db_props = CIBlockElement::GetProperty($iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"photos"));

            $photo_num = 0;

            while($ar_props = $db_props->Fetch()){
                if($photo_num>9){
                    break;
                }
                $file = CFile::GetFileArray($ar_props['VALUE']);
                $time_creation = $file['TIMESTAMP_X'];
                $creationTime = date('Y-m-d\TH:i:sP',strtotime($time_creation));

                $resize_img = CFile::ResizeImageGet($file,Array(),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);

                $sitemap_json_arr['objects'][$ob['ID']]['photos'][$photo_num]['url'] = 'http://www.restoran.ru'.$resize_img['src'];
                $sitemap_json_arr['objects'][$ob['ID']]['photos'][$photo_num]['creation_time'] = $creationTime;
                if($file['DESCRIPTION']!=''){
                    $sitemap_json_arr['objects'][$ob['ID']]['photos'][$photo_num]['description'] = $file['DESCRIPTION'];
                }

                $photo_num++;
            }
            usleep(100);
        }
        usleep(1000);

        if ($res->NavPageCount!=$iNumPage && $iNumPage<500 && $leftBorderCnt!=0) {    // не последняя
            $bodyjson = json_encode($sitemap_json_arr);
            if(file_put_contents($jsonFile, $bodyjson)){
                LocalRedirect('/2gis/generate-photos-json.php?start=Y&CITY_CODE='.$IBLOCK_CODE.'&iNumPage='.++$iNumPage, true);
            }
            else {
                echo 'coudnt put file (';
            }
        }
        else {
            $bodyjson = json_encode($sitemap_json_arr);
            file_put_contents($jsonFile, $bodyjson);
            LocalRedirect('/2gis/generate-photos-json.php?CITY_CODE='.$IBLOCK_CODE.'&LAST_PAGE=Y&start=Y', true);
        }
    }

    static function GlueGisOrganizationJson(){
        $city_array = array('spb','msk','rga','urm','tmn', 'nsk', 'kld');//
        // TODO поставить удаление json файла

        if(!$_REQUEST['CITY_CODE']){
            $jsonFile = self::ABS_DOC_ROOT.'/2gis/2gisOrganization.json';
            $bodyjson = json_encode(array());
            file_put_contents($jsonFile, $bodyjson);
        }

        if($_REQUEST['CITY_CODE'] && $_REQUEST['LAST_PAGE']=='Y'){
            $key = array_search($_REQUEST['CITY_CODE'],$city_array);
            if($key!==''){
                if(array_key_exists($key+1,$city_array)){
                    $arIB = getArIblock("catalog", $city_array[$key+1]);
                    self::GisOrganizationJson($arIB['ID'],'restaurants',$city_array[$key+1],$arIB['NAME']);
                }
                else {
                    echo '!_FINISH_!';
                }
            }
        }
        elseif($_REQUEST['CITY_CODE']) {
            $arIB = getArIblock("catalog", $_REQUEST['CITY_CODE']);
            self::GisOrganizationJson($arIB['ID'],'restaurants',$_REQUEST['CITY_CODE'],$arIB['NAME']);
        }
        else {
            $arIB = getArIblock("catalog", $city_array[0]);
            self::GisOrganizationJson($arIB['ID'],'restaurants',$city_array[0],$arIB['NAME']);
        }
    }

    static function GlueGisExternalPhotosJson(){
        $city_array = array('spb','msk','rga','urm','tmn', 'nsk', 'kld');//
        if($_REQUEST['CITY_CODE'] && $_REQUEST['LAST_PAGE']=='Y'){
            $key = array_search($_REQUEST['CITY_CODE'],$city_array);
            if($key!==''){
                if(array_key_exists($key+1,$city_array)){
                    $arIB = getArIblock("catalog", $city_array[$key+1]);
                    self::GisExternalPhotosJson($arIB['ID'],'restaurants',$city_array[$key+1]);
                }
                else {
                    echo '!_FINISH_!';
                }
            }
        }
        elseif($_REQUEST['CITY_CODE']) {
            $arIB = getArIblock("catalog", $_REQUEST['CITY_CODE']);
            self::GisExternalPhotosJson($arIB['ID'],'restaurants',$_REQUEST['CITY_CODE']);
        }
        else {
            $arIB = getArIblock("catalog", $city_array[0]);
            self::GisExternalPhotosJson($arIB['ID'],'restaurants',$city_array[0]);
        }
    }


    //  TODO custom_rank у спящих

    static function SleepingCustomRank(){
        if($_REQUEST["PROP"]['sleeping_rest'] && $_REQUEST["ELEMENT_ID"]){
            CModule::IncludeModule("search");
            $cCustomRank = new CSearchCustomRank;

            $res = CSearchCustomRank::GetByID($_REQUEST["ELEMENT_ID"]);
            if(!$obj = $res->Fetch()){
                $arFields = array(
                    'ID'=>$_REQUEST["ELEMENT_ID"],
                    "SITE_ID"=>"s1",
                    "MODULE_ID"=>"iblock",
                    "PARAM1"=>"catalog",
                    "PARAM2"=>$_REQUEST['IBLOCK_ID'],
                    "ITEM_ID"=>$_REQUEST["ELEMENT_ID"],
                    'RANK'=>'-1',
                    'APPLIED'=>'Y'
                );
                $cCustomRank->Add($arFields);
            }
            $SLEEPING_TIMESTAMP = false;
        }
        elseif(!$_REQUEST["PROP"]['sleeping_rest']&&$_REQUEST["ELEMENT_ID"]) {
            CModule::IncludeModule("search");
            $cCustomRank = new CSearchCustomRank;
            $res = CSearchCustomRank::GetByID($_REQUEST["ELEMENT_ID"]);
            if($obj = $res->Fetch()){
                $cCustomRank->Delete($_REQUEST["ELEMENT_ID"]);
            }
            $SLEEPING_TIMESTAMP = true;
        }

    }


    static function yaDirectJson($IBLOCK_CODE,$no_check_dates=false){
        /**
        stamp: <timestamp> момента окончания формирования выдачи
        city: <varchar:3> буквенный код по заранее утвержденной схеме, региональная принадлежность выдачи (msk, spb etc)
        data: {
        id: <int:5> заведения по базе restoran.ru (уникальное)
        title: <varchar:255> наименование заведения без признаков типа заведения
        types: [type1, type_2, etc] типы этого заведения в текстовом представлении, например ("Ресторан", "Кафе" etc), где первое значение является приоритетным, varchar:255
        metro: [metro1, metro2, etc] текстовое значение названий привязанных станций метро, varchar:255
        street: <varchar:255> чистое название улицы размещения заведения в именительном падеже, без указания типа улицы (ул., пер., etc). (Если нет возможности вырезать из адреса название улицы - сообщите об этом заказчику. Мы можем реализовать этот функционал на нашей стороне. В таком случае поле должно будет содержать полный адрес заведения.)
        url: постоянный url страницы ресторана desktop версии с "context" вместо detailed varchar:255
        url_mobile: постоянный url страницы ресторана mobile версии varchar:255
        }
         **/


        //  TODO зачищаем файл с
        $arIB = getArIblock("catalog", $IBLOCK_CODE);

        if(!$_REQUEST['iNumPage']){
            $iNumPage = 1;
        }
        else {
            $iNumPage = $_REQUEST['iNumPage'];
        }

        $jsonFile = self::ABS_DOC_ROOT.'/yadirect/yaDirect_'.$IBLOCK_CODE.'.json';

        if(!$no_check_dates){
            if($iNumPage==1){
                $file_change_time = filemtime($jsonFile);

                $arFilter = Array("IBLOCK_ID" => $arIB['ID'], "ACTIVE" => "Y", '!PROPERTY_sleeping_rest_VALUE'=>'Да');//
                $res_for_time = CIBlockElement::GetList(Array('TIMESTAMP_X'=>'DESC'), $arFilter, false, Array("nTopCount" => 1), array('ID'));//
                if ($ob_time = $res_for_time->Fetch()) {
                    $IBLOCK_CHANGE_TIME = strtotime($ob_time['TIMESTAMP_X']);
                }

                if($res_for_time < $IBLOCK_CHANGE_TIME){
                    echo 'json newer then rests';
                    usleep(1000000);
                    LocalRedirect('/yadirect/index.php?start=Y&CITY_CODE='.$IBLOCK_CODE.'&LAST_PAGE_OF_CITY=Y', true);
                }
            }
        }



        if($iNumPage==1){
            $json_output_arr = array();
        }
        else {
            $json_output_arr = self::json_decod_function($jsonFile);
        }


        $key = count($json_output_arr['data'])? count($json_output_arr['data']) : 0;


        $arSelect = Array("ID", 'NAME', 'CODE');
        $arFilter = Array("IBLOCK_ID" => $arIB['ID'], "ACTIVE" => "Y", '!PROPERTY_sleeping_rest_VALUE'=>'Да');//


        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50, 'iNumPage' => $iNumPage), $arSelect);//

        $leftBorderCnt = $res->SelectedRowsCount();


        if($IBLOCK_CODE=='spb'){
            $host = 'http://spb.restoran.ru/';
        }
        elseif($IBLOCK_CODE=='urm'){
            $host = 'http://urm.restoran.ru/';
        }
        else {
            $host = 'http://www.restoran.ru/';
        }


        while ($ob = $res->Fetch()) {

            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"type"));
            while($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $res_inner_value = CIBlockElement::GetList(Array(), array('ID'=>$ar_props['VALUE']), false, false, array('NAME'));//, 'iNumPage' => $iNumPage
                    if($ar_res = $res_inner_value->Fetch()){
                        $json_output_arr['data'][$key]['types'][] = $ar_res['NAME'];
                    }
                }
            }
            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"subway"));
            while($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $res_inner_value = CIBlockElement::GetList(Array(), array('ID'=>$ar_props['VALUE']), false, false, array('NAME'));//, 'iNumPage' => $iNumPage
                    if($ar_res = $res_inner_value->Fetch()){
                        $json_output_arr['data'][$key]['metro'][] = $ar_res['NAME'];
                    }
                }
            }
            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
                $json_output_arr['data'][$key]['street'] = $ar_props['VALUE'];
            }

            $json_output_arr['data'][$key]['id'] = $ob['ID'];
            $json_output_arr['data'][$key]['title'] = $ob['NAME'];


            $json_output_arr['data'][$key]['url'] = $host.$IBLOCK_CODE.'/context/restaurants/'.$ob['CODE'].'/';
            $json_output_arr['data'][$key]['url_mobile'] = 'http://m.restoran.ru/detail.php?ID='.$ob['ID'].'&CITY_ID='.$IBLOCK_CODE;

            $key++;
        }


        echo $iNumPage.' $iNumPage';
        echo $res->NavPageCount.' $res->NavPageCount';
        CModule::IncludeModule("search");
        $json_output_arr['stamp'] = CSiteMap::TimeEncode(time());
        $json_output_arr['city'] = $IBLOCK_CODE;

        usleep(2500000);

//        echo json_encode($json_output_arr);

        if ($res->NavPageCount!=$iNumPage && $iNumPage<100 && $leftBorderCnt!=0) {    // не последняя

            $bodyjson = json_encode($json_output_arr);
            if(file_put_contents($jsonFile, $bodyjson)){
                LocalRedirect('/yadirect/index.php?start=Y&iNumPage='.++$iNumPage.'&CITY_CODE='.$IBLOCK_CODE, true);
            }
            else {
                echo 'coudnt put file (';
            }
        }
        else {
            $bodyjson = json_encode($json_output_arr);
            file_put_contents($jsonFile, $bodyjson);

            echo '!_finished_with_city_!';
            LocalRedirect('/yadirect/index.php?start=Y&CITY_CODE='.$IBLOCK_CODE.'&LAST_PAGE_OF_CITY=Y', true);
        }
    }

    static function yaDirectJsonNow($IBLOCK_CODE,$no_check_dates=false){
        $arIB = getArIblock("catalog", $IBLOCK_CODE);

//        $jsonFile = self::ABS_DOC_ROOT.'/yadirect/yaDirect_'.$IBLOCK_CODE.'.json';

//        $json_output_arr = self::json_decod_function($jsonFile);
        $json_output_arr = array();

        $key = count($json_output_arr['data'])? count($json_output_arr['data']) : 0;

        $arSelect = Array("ID", 'NAME', 'CODE');
        $arFilter = Array("IBLOCK_ID" => $arIB['ID'], "ACTIVE" => "Y", '!PROPERTY_sleeping_rest_VALUE'=>'Да','!SECTION_ID'=>false);//
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 5000), $arSelect);//

//        $leftBorderCnt = $res->SelectedRowsCount();



        if($IBLOCK_CODE=='spb'){
            $host = 'http://spb.restoran.ru/';
        }
        elseif($IBLOCK_CODE=='urm'){
            $host = 'http://urm.restoran.ru/';
        }
        else {
            $host = 'http://www.restoran.ru/';
        }


        while ($ob = $res->Fetch()) {

            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"type"));
            while($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $res_inner_value = CIBlockElement::GetList(Array(), array('ID'=>$ar_props['VALUE']), false, false, array('NAME'));//, 'iNumPage' => $iNumPage
                    if($ar_res = $res_inner_value->Fetch()){
                        $json_output_arr['data'][$key]['types'][] = $ar_res['NAME'];
                    }
                }
            }
            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"subway"));
            while($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $res_inner_value = CIBlockElement::GetList(Array(), array('ID'=>$ar_props['VALUE']), false, false, array('NAME'));//, 'iNumPage' => $iNumPage
                    if($ar_res = $res_inner_value->Fetch()){
                        $json_output_arr['data'][$key]['metro'][] = $ar_res['NAME'];
                    }
                }
            }
            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
                $json_output_arr['data'][$key]['street'] = $ar_props['VALUE'];
            }

            //  код без _
            $CODE = '';
            $db_props = CIBlockElement::GetProperty($arIB['ID'], $ob['ID'], array("sort" => "asc"), Array("CODE"=>"LIKE_CODE_FIELD"));
            if($ar_props = $db_props->Fetch()){
                $CODE = $ar_props['VALUE'];
            }

            $json_output_arr['data'][$key]['id'] = $ob['ID'];
            $json_output_arr['data'][$key]['title'] = $ob['NAME'];


//            $json_output_arr['data'][$key]['url'] = $host.$IBLOCK_CODE.'/context/restaurants/'.$ob['CODE'].'/';
            $json_output_arr['data'][$key]['url'] = 'http://'.$CODE.'.restoran.ru';
            $json_output_arr['data'][$key]['url_mobile'] = 'http://m.restoran.ru/detail.php?ID='.$ob['ID'].'&CITY_ID='.$IBLOCK_CODE;

            $key++;
        }


//        CModule::IncludeModule("search");
        $json_output_arr['stamp'] = time()+60;
//        FirePHP::getInstance()->info(date('d.m.Y H:i:s',time()+60));
        $json_output_arr['city'] = $IBLOCK_CODE;
        $json_output_arr['count'] = count($json_output_arr['data']);

//        echo json_encode($json_output_arr);
        $bodyjson = json_encode($json_output_arr);
//        FirePHP::getInstance()->info(date('d.m.Y H:i'));
//        FirePHP::getInstance()->info(time());
        echo $bodyjson;
    }

    static function newsXMLNow($IBLOCK_CODE,$no_check_dates=false){
        CModule::IncludeModule("search");
        $arIB = getArIblock("news", $IBLOCK_CODE);

        $arSelect = Array("ID", 'NAME', 'CODE','TIMESTAMP_X','DETAIL_PAGE_URL');
        $arFilter = Array("IBLOCK_ID" => $arIB['ID'], "ACTIVE" => "Y", '>DATE_CREATE'=>'01.01.2015 00:00:00');//
        $res = CIBlockElement::GetList(Array('timestamp_x'=>'DESC'), $arFilter, false, false, $arSelect);//

        if($IBLOCK_CODE=='spb'){
            $host = 'https://spb.restoran.ru';
        }
        else {
            $host = 'https://www.restoran.ru';
        }

        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

        $week_ago = time()-3600*7;

        while ($ob = $res->GetNext()) {
            echo "\t<url>\n\t\t<loc>".$host.$ob['DETAIL_PAGE_URL']."</loc>\n\t\t<lastmod>".CSiteMap::TimeEncode(strtotime($ob['TIMESTAMP_X']))."</lastmod>\n\t\t<changefreq>weekly</changefreq>\n\t\t<priority>".(strtotime($ob['TIMESTAMP_X'])>=$week_ago?"1":"0.5")."</priority>\n\t</url>\n";
        }

        echo '</urlset>';
    }

}
?>