<?
############################################
# Work with labor exchange
#
# Файл создан: 02.02.2012
# Автор: Kilin Denis
# E-mail: dk@cakelabs.ru
############################################

/*
 * class for work with resume and vacancy
 */
class LaborExchange {
    /*
     * set responded users
     * $arFields = Array(
     *     "IBLOCK_ID" - id resume or vacansy iblock
     *     "ELEMENT_ID" - id resume or vacansy
     *     "USER_ID" - respond user id (if authorized)
     *     "USER_EMAIL" - respond user email (if not authorized)
     *     "USER_FIO" - respond user FIO (if not authorized)
     *     "RES_RES_PROPERTY_CODE" - iblock property code for respond user set (if authorized)
     * );
     */
    function respondSet($arFields) {
        if(!$arFields["IBLOCK_ID"] || !$arFields["ELEMENT_ID"]) {
            $arResult["ERROR"] = "You must set params!";
            return $arResult;
        }

        if(!CModule::IncludeModule("iblock")) {
            $arResult["ERROR"] = "You must set iblock module!";
            return $arResult;
        }

        if(!$arFields["REG_RES_PROPERTY_CODE"])
            $arFields["REG_RES_PROPERTY_CODE"] = "RESPOND_USERS_REG";

        if(!$arFields["UNREG_USERS_EMAIL_PROPERTY_CODE"])
            $arFields["UNREG_USERS_EMAIL_PROPERTY_CODE"] = "RES_USERS_UNR_EMAIL";

        if(!$arFields["UNREG_USERS_FIO_PROPERTY_CODE"])
            $arFields["UNREG_USERS_FIO_PROPERTY_CODE"] = "RES_USERS_UNR_FIO";

        // get cur reg respond users
        $rsResUsers = CIBlockElement::GetProperty(
            $arFields["IBLOCK_ID"],
            $arFields["ELEMENT_ID"],
            Array(),
            Array("CODE" => $arFields["REG_RES_PROPERTY_CODE"])
        );
        while($arResUsers = $rsResUsers->Fetch()) {
            $arUs[] = $arResUsers["VALUE"];
        }

        // if user is authorized
        if($arFields["USER_ID"]) {
            // get cur reg respond users
            $rsResUsers = CIBlockElement::GetProperty(
                $arFields["IBLOCK_ID"],
                $arFields["ELEMENT_ID"],
                Array(),
                Array("CODE" => $arFields["REG_RES_PROPERTY_CODE"])
            );
            while($arResUsers = $rsResUsers->Fetch()) {
                $arUs[] = $arResUsers["VALUE"];
            }

            if(!in_array($arFields["USER_ID"], $arUs)){
                $arUs[] = $arFields["USER_ID"];
                CIBlockElement::SetPropertyValuesEx($arFields["ELEMENT_ID"], $arFields["IBLOCK_ID"], Array($arFields["REG_RES_PROPERTY_CODE"] => $arUs));
            }
        // if user is not authorized
        } else {
            $arUsEmail = Array();
            $arUsFIO = Array();

            // get cur unreg respond users email
            $rsResUsersEmail = CIBlockElement::GetProperty(
                $arFields["IBLOCK_ID"],
                $arFields["ELEMENT_ID"],
                Array(),
                Array("CODE" => $arFields["UNREG_USERS_EMAIL_PROPERTY_CODE"])
            );
            while($arResUsersEmail = $rsResUsersEmail->Fetch()) {
                $arUsEmail[] = $arResUsersEmail["VALUE"];
            }
            if(!in_array($arFields["USER_EMAIL"], $arUsEmail)){
                $arUsEmail[] = $arFields["USER_EMAIL"];
                CIBlockElement::SetPropertyValuesEx($arFields["ELEMENT_ID"], $arFields["IBLOCK_ID"], Array($arFields["UNREG_USERS_EMAIL_PROPERTY_CODE"] => $arUsEmail));

                // get cur unreg respond users FIO
                $rsResUsersFIO = CIBlockElement::GetProperty(
                    $arFields["IBLOCK_ID"],
                    $arFields["ELEMENT_ID"],
                    Array(),
                    Array("CODE" => $arFields["UNREG_USERS_FIO_PROPERTY_CODE"])
                );
                while($arResUsersFIO = $rsResUsersFIO->Fetch()) {
                    $arUsFIO[] = $arResUsersFIO["VALUE"];
                }
                $arUsFIO[] = $arFields["USER_FIO"];
                CIBlockElement::SetPropertyValuesEx($arFields["ELEMENT_ID"], $arFields["IBLOCK_ID"], Array($arFields["UNREG_USERS_FIO_PROPERTY_CODE"] => $arUsFIO));
            }
        }

        return true;
    }
}
?>