<?
class RestOrder {
    function OnOrderAddHandler($ID, $arOrder) {
        global $APPLICATION;
        global $DB;

        if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("iblock"))
            return;

        /* set accounts and priorities */

        // get product id
        $rsBasket = CSaleBasket::GetList(
            array(),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID"  => $ID
            ),
            false,
            false,
            array()
        );
        $arBasket = $rsBasket->Fetch();

        // get purchase element info
        $rsEl = CIBlockElement::GetByID($arBasket["PRODUCT_ID"]);
        if($arEl = $rsEl->Fetch()) {
            // get validity date
            $rsPVProp = CIBlockElement::GetProperty(
                $arEl["IBLOCK_ID"],
                $arBasket["PRODUCT_ID"],
                Array(),
                Array(
                    "ACTIVE" => "Y",
                    "CODE" => "period_of_validity",
                )
            );
            $arPVProp = $rsPVProp->Fetch();

            // get order props
            $rsOrderProps = CSaleOrderPropsValue::GetList(
                array(),
                array(
                    "ORDER_ID" => $ID,
                    "CODE" => "resto_bind",
                ),
                false,
                false,
                array()
            );
            $arOrderProps = $rsOrderProps->Fetch();

            // get restoran iblock id
            $rsRestoEl = CIBlockElement::GetByID($arOrderProps["VALUE"]);
            $arRestoEl = $rsRestoEl->Fetch();

            // default validity
            $todayDate = date("Y-m-d");
            $dateMonthsAdded = strtotime(date("Y-m-d", strtotime($todayDate)) . "+{$arPVProp["VALUE"]} month");

            // if extend
            if($_REQUEST["action"] == "extend_account" || $_REQUEST["action"] == "extend_priority") {
                // get validity
                $rsOldOrderProps = CSaleOrderPropsValue::GetList(
                    array(),
                    array(
                        "ORDER_ID" => $_REQUEST["ext_order_id"],
                        "CODE" => 'validity',
                    ),
                    false,
                    false,
                    array()
                );
                $arOldOrderProps = $rsOldOrderProps->Fetch();
                $oldDate = ConvertDateTime($arOldOrderProps["VALUE"], "Y-m-d");
                $dateMonthsAdded = strtotime(date("Y-m-d", strtotime($oldDate)) . "+{$arPVProp["VALUE"]} month");
            }

            // set validity date
            $dateMonthsAdded = CIBlockFormatProperties::DateFormat('d.m.Y', $dateMonthsAdded);
            $arOrderFields = array(
                "ORDER_ID" => $ID,
                "ORDER_PROPS_ID" => 19,
                "NAME" => "Срок действия",
                "CODE" => "validity",
                "VALUE" => $dateMonthsAdded
            );
            CSaleOrderPropsValue::Add($arOrderFields);

            switch($arEl["IBLOCK_ID"]) {
                case SELL_ACCOUNT_IBLOCK_ID:
                    CIBlockElement::SetPropertyValuesEx($arOrderProps["VALUE"], $arRestoEl["IBLOCK_ID"], array("{$arEl["IBLOCK_CODE"]}" => $ID));
                break;
                case SELL_PRIORITY_IBLOCK_ID:
                    $rsPriorityProp = CIBlockElement::GetProperty(
                        $arRestoEl["IBLOCK_ID"],
                        $arOrderProps["VALUE"],
                        Array(),
                        Array(
                            "ACTIVE" => "Y",
                            "CODE" => $arEl["IBLOCK_CODE"],
                        )
                    );
                    while($arPriorityProp = $rsPriorityProp->Fetch()) {
                        if($_REQUEST["action"] == "extend_priority" && $arPriorityProp["VALUE"] != $_REQUEST["ext_order_id"])
                            $arPriorityPropTmp[] = $arPriorityProp["VALUE"];
                    }
                    if(!in_array($arBasket["PRODUCT_ID"], $arPriorityPropTmp)) {
                        $arPriorityPropTmp[] = $ID;
                        CIBlockElement::SetPropertyValuesEx($arOrderProps["VALUE"], $arRestoEl["IBLOCK_ID"], array("{$arEl["IBLOCK_CODE"]}" => $arPriorityPropTmp));
                    }

                    //$APPLICATION->ThrowException(v_dump($arPriorityPropTmp));
                    //return false;
                break;
            }
        }
    }
}
?>