#!/usr/bin/php -q
<?
set_time_limit(0);
$_SERVER["DOCUMENT_ROOT"] = "/var/www/restoran.cakelabs.ru";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
if (!CModule::IncludeModule("iblock"))
    return;
$rsSites = CSite::GetList($by="sort", $order="desc");
while ($arSite = $rsSites->Fetch())
{
    $ar_fields = false;
    
    $arFilter = Array( 
                       "IBLOCK_TYPE" => "catalog",
                       "SITE_ID" => $arSite["LID"],
                       "<=DATE_ACTIVE_TO" => (ConvertTimeStamp(time()+86400, "SHORT","s1")),  
                       ">=DATE_ACTIVE_TO"   => (ConvertTimeStamp(time(), "SHORT","s1"))
                    );
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC", "PROPERTY_PRIORITY"=>"ASC"), $arFilter, false, false, Array("DATE_ACTIVE_FROM","DATE_ACTIVE_TO","NAME","PROPERTY_user_bind"));
    while($ar_fields = $res->GetNext())
    {  
        $arUser = false;
        $manager = false;
        $arEventFields = array();
        if ($ar_fields["PROPERTY_USER_BIND_VALUE"])
        {
            $us = CUser::GetByID($ar_fields["PROPERTY_USER_BIND_VALUE"]);
            if ($arUser = $us->Fetch())
            {
                $arEventFields = array(
                    "USER_NAME"         => $arUser["SURNAME"]." ".$arUser["NAME"],
                    "NAME"              => $ar_fields["NAME"],
                    "DATE_ACTIVE_TO"    => $ar_fields["DATE_ACTIVE_TO"],
                    "DATE_ACTIVE_FROM"  => $ar_fields["DATE_ACTIVE_FROM"],
                    "EMAIL"             => $arUser["EMAIL"],
                    );
                if ($arUser["WORK_PAGER"])
                {
                    $us2 = CUser::GetByID($arUser["WORK_PAGER"]);
                    if ($manager = $us2->Fetch())
                        $arEventFields["MANAGER_EMAIL"] = $manager["EMAIL"];
                }                
                CEvent::Send("ONE_MONTH_RESTORATOR_REMINDER", $arSite["LID"], $arEventFields);
                if (CModule::IncludeModule("sozdavatel.sms")&&$arUser["PERSONAL_PHONE"]&&$arSite["LID"]=="s1")
                {
                    //CSMS::Send($message, $arUser["PERSONAL_PHONE"], "UTF-8");
                }
            }
        }
    }
}
?>
