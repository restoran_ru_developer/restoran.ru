<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://restoran.ru/spb/news/restoransnewsspb/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">НОВОСТИ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/deshevyiy-steyk--eto-prestuplenie-intervyu-s-medsom-refslundom/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/b41/207_117_2/mads_refslund_1.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/deshevyiy-steyk--eto-prestuplenie-intervyu-s-medsom-refslundom/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">«Дешёвый стейк – это преступление»: интервью с Мэдсом Рефслундом</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Рефслунд – фигура знаменательная в кулинарном деле. Сначала с его подачи несколько лет подряд ресторан NOMA становился лучшим в мире, потом его собственное детище – MR в Копенгагене – с лёгкостью стало обладателем всеми вожделенной звезды Мишлен. 
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">10 Февраля 2017</span>
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/deshevyiy-steyk--eto-prestuplenie-intervyu-s-medsom-refslundom/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/kritika---eto-vzglyad-ispyitatelya-samoleta-intervyu-s-kritikom-borisom/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/ea4/207_117_2/glavnaya.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/kritika---eto-vzglyad-ispyitatelya-samoleta-intervyu-s-kritikom-borisom/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">«Критика - это взгляд испытателя самолета»: интервью с критиком Борисом</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Борис - это имя знают все, кто так или иначе сопричастен ресторанному рынку. И его имя - это единственное, что достоверно о нём известно.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">3 Февраля 2017</span>
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/kritika---eto-vzglyad-ispyitatelya-samoleta-intervyu-s-kritikom-borisom/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/novoe-menyu-v-restorane-circus/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/136/207_117_2/tsirkus.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/novoe-menyu-v-restorane-circus/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Новое меню в ресторане Circus</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Ресторан Circus на проспекте Маршала Жукова уже успел зарекомендовать себя самым необычным ресторанным проект Санкт-Петербурга.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">30 Января 2017</span>
                    <a href="http://www.restoran.ru/spb/news/restoransnewsspb/novoe-menyu-v-restorane-circus/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://restoran.ru/spb/news/restoransnewsspb/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ НОВОСТИ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>