<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://www.restoran.ru/msk/news/restvew/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">ОБЗОРЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/news/restvew/podarki-na-den-svyatogo-valentina-restoranyi-obyyasnyayutsya-v-lyubvi/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/218/207_117_2/petercafe.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/news/restvew/podarki-na-den-svyatogo-valentina-restoranyi-obyyasnyayutsya-v-lyubvi/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Подарки на День святого Валентина: рестораны объясняются в любви</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Бутылка вина в подарок совершенно бесплатно, игристое и закуски в качестве комплимента, специальное меню от звёздных пар – в этом году рестораны расстарались максимально, чтобы порадовать гостей на 14 февраля.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">9 Февраля 2017</span>
                    <a href="http://www.restoran.ru/msk/news/restvew/podarki-na-den-svyatogo-valentina-restoranyi-obyyasnyayutsya-v-lyubvi/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/news/restvew/vnutri-vkusnee-subproduktyi-v-restoranah/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/1ad/207_117_2/le_restoran.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/news/restvew/vnutri-vkusnee-subproduktyi-v-restoranah/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Внутри вкуснее: субпродукты в ресторанах</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Печень, почки, сердце, хвосты, языки - московские повара экспериментируют со всеми возможными частями вкуснейших туш и тушек.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">30 Января 2017</span>
                    <a href="http://www.restoran.ru/msk/news/restvew/vnutri-vkusnee-subproduktyi-v-restoranah/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/news/restvew/pech-s-pyilu-s-jaru/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/0f8/207_117_2/toplenoe_moloko_2.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/news/restvew/pech-s-pyilu-s-jaru/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Печь: с пылу с жару</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Современная ресторанная индустрия, как и древнерусская кулинария, пляшет от печки и берет взятки с грядки. Мы выбрали рестораны, в которых повара запекают, тушат и пекут в дровяных печах, что для зимнего времени особенно актуально, потому что сытно.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">24 Января 2017</span>
                    <a href="http://www.restoran.ru/msk/news/restvew/pech-s-pyilu-s-jaru/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://www.restoran.ru/msk/news/restvew/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ ОБЗОРЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>