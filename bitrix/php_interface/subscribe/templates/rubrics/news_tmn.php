<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://www.restoran.ru/tmn/news/restoransnewssch/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">НОВОСТИ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/nadejda_veselova_legkost_byitiya/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/5c5/207_117_2/dsc6164_web.jpg.jpg">
                    </a>
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/nadejda_veselova_legkost_byitiya/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Надежда Веселова: Лёгкость бытия</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Интервью с шеф-кондитером ресторана Базилик Надеждой Веселовой
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">15 Июня 2015</span>
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/nadejda_veselova_legkost_byitiya/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/dostavka_v_restorane_byistrogo_pitaniya_giros/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/e74/207_117_2/fzne9yk9ek1czxx_movhd_wh4m3oona_0iyqdoazu6y.jpg">
                    </a>
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/dostavka_v_restorane_byistrogo_pitaniya_giros/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Доставка в ресторане быстрого питания Гирос</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Отличная новость друзья!<br />
Теперь в ресторане быстрого питания ГИРОС любимые блюда доставят быстро и оперативно!
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">5 Июня 2015</span>
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/dostavka_v_restorane_byistrogo_pitaniya_giros/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/vovlechenie_klienta_cherez_oschuscheniya/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/7c8/207_117_2/dsc5825.jpg">
                    </a>
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/vovlechenie_klienta_cherez_oschuscheniya/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">&quot;Вовлечение клиента через ощущения&quot;</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        В четверг, 4 июня, в ресторане &quot;Александр&quot; состоится полезный бизнес-ланч для руководителей, маркетологов и организаторов мероприятий.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">2 Июня 2015</span>
                    <a href="http://www.restoran.ru/tmn/news/novostitmn/vovlechenie_klienta_cherez_oschuscheniya/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://www.restoran.ru/tmn/news/restoransnewssch/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ НОВОСТИ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>