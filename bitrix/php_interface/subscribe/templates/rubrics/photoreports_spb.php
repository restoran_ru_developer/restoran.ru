<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://spb.restoran.ru/spb/photos/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">ФОТООТЧЕТЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/photos/godji-moscow-party/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/8aa/207_117_2/img2079.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/photos/godji-moscow-party/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Godji Moscow Party</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        На одну ночь Петербург озарился московским блеском. 16 декабря огнями Невы в ресторане Godji наслаждались специальные гости из столицы: телеведущие Аврора, Дарья Субботина, Артём Королев, Оксана Фёдорова, Дмитрий Нагиев и модельер Маша Цигаль.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">19 Декабря 2016</span>
                    <a href="http://www.restoran.ru/spb/photos/godji-moscow-party/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/photos/gala-ujin-golden-triangle/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/e8f/207_117_2/david-emmerle-_four-seasons_-i-shefy-avtory-uzhina_-igor-grishechkin_-evgeniy-vikentev_-dmitriy-blinov.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/photos/gala-ujin-golden-triangle/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Гала-ужин Golden Triangle</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        10 октября в ресторане Quadrum отеля Four Seasons Moscow в рамках Московского гастрономического фестиваля прошел ужин Golden Triangle, на котором самые талантливые шеф-повара представляют единое меню.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">13 Октября 2016</span>
                    <a href="http://www.restoran.ru/spb/photos/gala-ujin-golden-triangle/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/photos/otkryitie_letney_verandyi_restorana_puri/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/d48/207_117_2/glavnaya.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/photos/otkryitie_letney_verandyi_restorana_puri/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Открытие летней веранды ресторана «Пури»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        27 июля состоялось открытие летней веранды ресторана «Пури».
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">18 Августа 2016</span>
                    <a href="http://www.restoran.ru/spb/photos/otkryitie_letney_verandyi_restorana_puri/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://spb.restoran.ru/spb/photos/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ ФОТООТЧЕТЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>