<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://restoran.ru/msk/news/restoransnewsmsk/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">НОВОСТИ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/friksheyki-v-restorane-voronej/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/df5/207_117_2/villi-vonka_small.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/friksheyki-v-restorane-voronej/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Фрикшейки в ресторане «Воронеж»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        В закусочной «Воронеж» на Большой Дмитровке появились модные фрикшейки.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">14 Февраля 2017</span>
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/friksheyki-v-restorane-voronej/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/portal-restoranru-i-restoran-kollektsiya-food--chillout-obyyavlyayut-konkurs/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/9a2/207_117_2/fzckzac7jxc.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/portal-restoranru-i-restoran-kollektsiya-food--chillout-obyyavlyayut-konkurs/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Портал «Ресторан.ру» и ресторан «КОЛЛЕКЦИЯ food & chillout» объявляют конкурс!</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Друзья, портал «Ресторан.ру» и ресторан «КОЛЛЕКЦИЯ food & chillout» объявляют конкурс! Приз – сертификат на бесплатный ужин на сумму 3000 руб. в ресторане «КОЛЛЕКЦИЯ food & chillout». 
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">13 Февраля 2017</span>
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/portal-restoranru-i-restoran-kollektsiya-food--chillout-obyyavlyayut-konkurs/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/ohota-na-kraba-v-restorane-market/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/279/207_117_2/market-_-falanga-kraba_-zapechennaya-v-slivochnom-souse-pod-syrom-parmezan.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/ohota-na-kraba-v-restorane-market/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Охота на краба в ресторане «Маркет»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        В рыбном ресторане «Маркет» предлагают блюда из специального меню, главным героем которого стал камчатский краб.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">8 Февраля 2017</span>
                    <a href="http://www.restoran.ru/msk/news/restoransnewsmsk/ohota-na-kraba-v-restorane-market/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://restoran.ru/msk/news/restoransnewsmsk/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ НОВОСТИ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>