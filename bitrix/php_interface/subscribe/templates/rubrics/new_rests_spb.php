<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://spb.restoran.ru/spb/news/newplacespb/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">НОВЫЕ РЕСТОРАНЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/newplacespb/art-proekt-eatart/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/b2a/207_117_2/9rebkquyxuy.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/newplacespb/art-proekt-eatart/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Арт-проект Eat&Art</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Хорошие художники копируют, великие художники воруют. /Banksy/
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">14 Февраля 2017</span>
                    <a href="http://www.restoran.ru/spb/news/newplacespb/art-proekt-eatart/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/newplacespb/obnovlennyiy--olius/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/d92/207_117_2/5.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/newplacespb/obnovlennyiy--olius/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Обновлённый  «Олиус» на новом месте</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Банкетный ресторан «Олиус» сменил дислокацию: теперь прекрасные банкетные залы находятся по адресу Лермонтовский пр. 22-24.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">13 Февраля 2017</span>
                    <a href="http://www.restoran.ru/spb/news/newplacespb/obnovlennyiy--olius/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/newplacespb/paradnaya/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/1ed/207_117_2/6.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/newplacespb/paradnaya/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">«Парадная» рядом с шумными соседями</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Совсем недавно на  Конюшенной площади открылся бар-кальянная с истинно петербургским названием «Парадная».
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">9 Февраля 2017</span>
                    <a href="http://www.restoran.ru/spb/news/newplacespb/paradnaya/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://spb.restoran.ru/spb/news/newplacespb/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ НОВЫЕ РЕСТОРАНЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>