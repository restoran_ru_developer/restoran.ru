<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://www.restoran.ru/tmn/news/restvew/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">ОБЗОРЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/tmn/news/restvew/tyumenskoe_gostepriimstvo_konferentsiya_professionalov/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/85c/207_117_2/pp6vzs8rjsm.jpg">
                    </a>
                    <a href="http://www.restoran.ru/tmn/news/restvew/tyumenskoe_gostepriimstvo_konferentsiya_professionalov/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Тюменское гостеприимство: конференция профессионалов</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        В Тюмени прошла конференция профессионалов сферы гостеприимства.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">5 Мая 2015</span>
                    <a href="http://www.restoran.ru/tmn/news/restvew/tyumenskoe_gostepriimstvo_konferentsiya_professionalov/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/tmn/news/restvew/vkusnaya_tyumen_restoran_bazilik/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/cc9/207_117_2/dsc3756.jpg">
                    </a>
                    <a href="http://www.restoran.ru/tmn/news/restvew/vkusnaya_tyumen_restoran_bazilik/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Вкусная Тюмень. Ресторан Базилик</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Влюбиться в «Базилик» с первого взгляда
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">1 Апреля 2015</span>
                    <a href="http://www.restoran.ru/tmn/news/restvew/vkusnaya_tyumen_restoran_bazilik/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/tmn/news/restvew/zavtraki_tyumeni_bazilik/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/ff9/207_117_2/dsc3897.jpg">
                    </a>
                    <a href="http://www.restoran.ru/tmn/news/restvew/zavtraki_tyumeni_bazilik/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Завтраки Тюмени. Базилик</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Если Ваше утро начинается далеко не утром, а позавтракать хочется вне дома – ресторан «Базилик» примет Вас на завтрак в любой день и в любое время. Здесь даже лучше побыть в одиночестве – настроиться на предстоящий день, насладиться уютом, приятной атмосферой и, конечно, вкусной едой.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">26 Марта 2015</span>
                    <a href="http://www.restoran.ru/tmn/news/restvew/zavtraki_tyumeni_bazilik/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://www.restoran.ru/tmn/news/restvew/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ ОБЗОРЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>