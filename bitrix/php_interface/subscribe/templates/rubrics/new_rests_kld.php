<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://www.restoran.ru/kld/news/novye_restorany/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">НОВЫЕ РЕСТОРАНЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/villa_chento_gorodskoe_kafe/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/dad/207_117_2/6.jpg">
                    </a>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/villa_chento_gorodskoe_kafe/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Villa Ченто Городское кафе</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Открылось новое городское кафе &quot;Villa Ченто&quot;.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">6 Июня 2014</span>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/villa_chento_gorodskoe_kafe/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/restoran_portovino/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/244/207_117_2/p0.jpg">
                    </a>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/restoran_portovino/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Ресторан Портовино</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Ресторан Портофино – это кухня итальянского курорта Лигурийской Ривьеры.<br />
Шеф-повар «Портофино» Вальтери Шаини предлагает отведать блюда истинной итальянской кухни:
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">6 Июня 2014</span>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/restoran_portovino/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/venskoe_kafe/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/396/207_117_2/9b540359869a6cb8c6af33264d0185b2.jpg">
                    </a>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/venskoe_kafe/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Венское Кафе</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Старинная австрийская поговорка гласит: «Скажи, какое твое кафе любимое, и я скажу, кто ты». «Венское кафе» — это не только чашечка ароматного кофе, восхитительная выпечка и элегантное пирожное, но и атмосфера элегантной и аристократической Европы, способная удовлетворить взыскательного гостя. Именно здесь вы почувствуете себя жителем великой австрийской столицы, не покидая Калининграда.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">6 Июня 2014</span>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/venskoe_kafe/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://www.restoran.ru/kld/news/novye_restorany/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ НОВЫЕ РЕСТОРАНЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>