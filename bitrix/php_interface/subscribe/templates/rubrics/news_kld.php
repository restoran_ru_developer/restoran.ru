<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://www.restoran.ru/kld/news/novosti_kld/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">НОВОСТИ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/udachnyiy_vyibor_ot_restorana_kaiser_wurst/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/e43/207_117_2/kayzer.jpg">
                    </a>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/udachnyiy_vyibor_ot_restorana_kaiser_wurst/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Удачный выбор от ресторана «Kaiser Wurst»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Ресторан «Kaiser Wurst» предлагает удачный набор «Кёрри Вурст» всего за 390 рублей.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">11 Февраля 2015</span>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/udachnyiy_vyibor_ot_restorana_kaiser_wurst/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/den_svyatogo_valentina_v_restorane_dolce_vita/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/93f/207_117_2/spa.jpg">
                    </a>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/den_svyatogo_valentina_v_restorane_dolce_vita/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">День Святого Валентина в ресторане «Dolce Vita»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        В этот вечер для вас: Love-сет от шеф-повара ресторана, спецпредложение по карте бара, живая музыка, и&nbsp;&nbsp;конечно, приятные фотографии на память и хорошее настроение.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">11 Февраля 2015</span>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/den_svyatogo_valentina_v_restorane_dolce_vita/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/spetsialnyie_predlojeniya_ot_restorana_tetka_fisher/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/0ab/207_117_2/kofe.jpg">
                    </a>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/spetsialnyie_predlojeniya_ot_restorana_tetka_fisher/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Специальные предложения от ресторана «Тётка Фишер»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Начните день с любимого, бодрящего кофе и горячего пончика «Берлинер» от ресторана немецкой кухни «Тетка Фишер».
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">11 Февраля 2015</span>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/spetsialnyie_predlojeniya_ot_restorana_tetka_fisher/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://www.restoran.ru/kld/news/novosti_kld/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ НОВОСТИ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>