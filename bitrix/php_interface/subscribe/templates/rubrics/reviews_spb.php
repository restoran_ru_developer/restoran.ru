<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://spb.restoran.ru/spb/news/restvew/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">ОБЗОРЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/afrodiziaki-blyuda-obostryayuschie-chuvstva-ko-dnyu-svyatogo-valentina/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/043/207_117_2/blok_belyy_i_chernyy_ravioli_s_rakovymi_sheykami.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/afrodiziaki-blyuda-obostryayuschie-chuvstva-ko-dnyu-svyatogo-valentina/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Афродизиаки: блюда, обостряющие чувства, ко Дню святого Валентина</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Он уже совсем скоро, он буквально наступает нам на пятки – праздник, о природе и о социальном значении которого можно бесконечно спорить.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">13 Февраля 2017</span>
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/afrodiziaki-blyuda-obostryayuschie-chuvstva-ko-dnyu-svyatogo-valentina/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/restorannaya-geografiya-ulitsa-rubinshteyna/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/5c2/207_117_2/1461750112.32.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/restorannaya-geografiya-ulitsa-rubinshteyna/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Ресторанная география. Улица Рубинштейна</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Улица Рубинштейна ничем особым не выделяется среди своих сестёр по Центральному району. Да, здесь есть театр, но в нашей культурной столице театр есть почти на каждой улице центра &#40;конечно, не додинский, но какой-то всё равно найдется&#41;. 
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">29 Декабря 2016</span>
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/restorannaya-geografiya-ulitsa-rubinshteyna/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/mnogo-vrednogo--sladko/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/7ef/207_117_2/schaste_mandarinovoe_pirozhnoe.jpg">
                    </a>
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/mnogo-vrednogo--sladko/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Много вредного – сладко!</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        Спокойствие, только спокойствие, как говорил Карлсон, большой любитель этого самого сладкого во всех его видах и подвидах: мы, конечно, шутим.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">15 Декабря 2016</span>
                    <a href="http://www.restoran.ru/spb/news/obzor_spb/mnogo-vrednogo--sladko/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://spb.restoran.ru/spb/news/restvew/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ ОБЗОРЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>