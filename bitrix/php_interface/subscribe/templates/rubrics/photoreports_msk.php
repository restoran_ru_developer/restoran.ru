<tr>
    <td style="background:#FFFFFF; padding:30px 37px; padding-bottom:0px">
        <div style="margin-left: 182px; line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <div class="rubrika" style="margin: 0 25px; line-height: 30px; float: left; width: 220px; color:#0097D6; letter-spacing: 0.2em; text-align:center; ">
            <a href="http://restoran.ru/msk/photos/"  style="text-decoration: none; color:#0097D6; font-family: Georgia; font-size: 16px; text-align:center;">ФОТООТЧЕТЫ</a>
        </div>
        <div style="line-height: 30px; float: left; width: 12px; margin-bottom: 30px; text-align: center; font-size:16px; color:#000000 !important; font-family: Georgia;">&ndash;</div>
        <table width="100%" style=" margin:0px; border-bottom:1px solid #2c323c;">
            <tr>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/photos/staryiy-novyiy-god-v-restorane-christian/" style="width:207px; height:117px;">
                        <img style="float:left; position: relative;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/e28/207_117_2/christian-_6_.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/photos/staryiy-novyiy-god-v-restorane-christian/" style="float: left; display:block; text-decoration: none; margin: 13px 0;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Старый Новый год в ресторане Christian</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        13 января ресторан CHRISTIAN пригласил друзей отпраздновать старый Новый год.
                    </p>
                    <span style="clear:both; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">25 Января 2017</span>
                    <a href="http://www.restoran.ru/msk/photos/staryiy-novyiy-god-v-restorane-christian/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/photos/russkie-shefyi-na-festivale-far-from-moscow-v-los-andjelese/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117"src="http://www.restoran.ru/upload/resize_cache/iblock/3c8/207_117_2/img_8200.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/photos/russkie-shefyi-na-festivale-far-from-moscow-v-los-andjelese/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">Русские шефы на фестивале Far From Moscow в Лос-Анджелесе</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        В Лос-Анджелесе прошел первый фестиваль современной российской культуры — Far From Moscow.
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">20 Декабря 2016</span>
                    <a href="http://www.restoran.ru/msk/photos/russkie-shefyi-na-festivale-far-from-moscow-v-los-andjelese/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                </td>
                <td style="width: 220px; padding-bottom: 20px;" valign="top">
                    <a href="http://www.restoran.ru/msk/photos/den-rojdeniya-v-restorane-subbotitsa/" style="width:207px; height:117px;">
                        <img style="float:left; margin-left: 10px;" width="207" height="117" src="http://www.restoran.ru/upload/resize_cache/iblock/330/207_117_2/dsc_3179.jpg">
                    </a>
                    <a href="http://www.restoran.ru/msk/photos/den-rojdeniya-v-restorane-subbotitsa/" style="float: left; display:block; text-decoration: none; margin: 13px 0 13px 10px;text-align: left; line-height: 20px; color:#0097D6; font-size:16px; font-family: Georgia; ">День рождения в ресторане «Субботица»</a>
                    <p style="margin-top:0px; margin-bottom:8px; float:left; margin-left: 10px; font-size:12px; color:#000000 !important; line-height: 22px; font-family: Georgia; ">
                        11 декабря ресторан «Субботица» отметил 2-ой день рождения!
                    </p>
                    <span style="clear:both; margin-left: 10px; float:left; font-size:12px; color:#000000 !important; font-style:italic; font-family: Georgia; width:120px;">15 Декабря 2016</span>
                    <a href="http://www.restoran.ru/msk/photos/den-rojdeniya-v-restorane-subbotitsa/" style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; float:right; display:block; color:#0097D6; font-size:12px; font-family: Georgia; ">Далее</a>
                    <a href="http://restoran.ru/msk/photos/" style="text-decoration: none; margin-top: 35px; margin-bottom: 25px; float:right; font-size:12px; color: #000000 !important; font-family: Georgia; ">ВСЕ ФОТООТЧЕТЫ</a>
                </td>
            </tr>
        </table>
    </td>
</tr>