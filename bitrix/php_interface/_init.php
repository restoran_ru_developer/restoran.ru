<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/FirePHP.class.php');
$firephp = FirePHP::getInstance(true);
$options = array(
    'maxObjectDepth' => 15,
    'maxArrayDepth' => 15,
    'maxDepth' => 15,
    'useNativeJsonEncode' => true,
    'includeLineNumbers' => true
);
$firephp->setOptions($options);

//if($APPLICATION->GetCurPage()=='/home/bitrix/www/2gis/generate-organization-json.php'){
//    define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/2gis-log.txt");
//}
//else {
//    define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
//}
//    AddMessage2Log($_REQUEST['CITY_ID'],'CITY_ID');


include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/const.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/restics.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/tools.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/users.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/blog.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/iblock.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/search.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/labor_exchange.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/order.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/form.php");
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/redirect.php");
$BX_DOC_ROOT = $_SERVER["DOCUMENT_ROOT"];
$CUR_DIR = $APPLICATION->GetCurDir();

//$APPLICATION->set_cookie("CITY_ID", "msk", time() - 36000000, "/", "www.restoran.ru", false, true);
//$APPLICATION->set_cookie("CITY_ID", "spb", time() - 36000000, "/", "spb.restoran.ru", false, true);
$APPLICATION->set_cookie("SUBSCR_EMAIL", "", time() - 36000000, "/", "www.restoran.ru", false, true);
$APPLICATION->set_cookie("SUBSCR_EMAIL", "", time() - 36000000, "/", ".restoran.ru", false, true);

if($_SERVER['REMOTE_ADDR']=='79.175.45.127'){
//    AddMessage2Log('my_log_for_host='.$_SERVER['HTTP_HOST'].'_cur_dir='.$CUR_DIR);
}

if(substr_count($_SERVER['HTTP_HOST'],'.')>2){ //  для m.yujnoch.restoran.ru/
    LocalRedirect("http://restoran.ru" . $APPLICATION->GetCurPage(), true, "301 Moved permanently");
}

$rest = preg_replace("/(www\.)?([\w\d-]+)\.(.*)/is", "\\2", $_SERVER["HTTP_HOST"]);
if ($_REQUEST["from_mobile"] == "Y" || $_COOKIE["BITRIX_SM_FROM_MOBILE"] == "Y"){
    include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/Mobile_Detect.php");
    $detect = new Mobile_Detect();
    if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad")&&!$detect->isTablet()):
        $APPLICATION->set_cookie("FROM_MOBILE", "Y", time() + 360, "/", ".restoran.ru", false, true);
    else:
        $APPLICATION->set_cookie("FROM_MOBILE", "Y", time() + 43200, "/", ".restoran.ru", false, true);
    endif;
}


//if(!$APPLICATION->get_cookie("CITY_ID")&&$_SERVER['HTTP_HOST']=='www.restoran.ru'&&$CUR_DIR=='/'){
//    LocalRedirect("http://restoran.ru/", true, "301 Moved permanently");
//}


//  избавляемся от корневого адреса города с разделом переводя на хост
if($rest!='en'&&$rest!='m'&&($CUR_DIR=='/msk/'||$CUR_DIR=='/spb/')){
    if($CUR_DIR=='/spb/'){
        LocalRedirect("http://spb.restoran.ru/", true, "301 Moved permanently");
    }
//    elseif($CUR_DIR=='/urm/'){
//        LocalRedirect("http://urm.restoran.ru/", true, "301 Moved permanently");
//    }
    elseif($CUR_DIR=='/msk/'){
        $APPLICATION->set_cookie("CITY_ID", "msk", time() + 60 * 60 * 24, "/", ".restoran.ru", false, true);
        LocalRedirect("http://www.restoran.ru/", true, "301 Moved permanently");
    }
}
// избавляемся от двойного редиректа на / отправляя сразу на хост
if($rest!='en'&&$rest!='m'){
    $CUR_URI = $APPLICATION->GetCurUri();
    if($CUR_URI=='/spb'){
        LocalRedirect("http://spb.restoran.ru/", true, "301 Moved permanently");
    }
    if($CUR_URI=='/msk'){
        LocalRedirect("http://www.restoran.ru/", true, "301 Moved permanently");
    }
//    if($CUR_URI=='/urm'){
//        LocalRedirect("http://urm.restoran.ru/", true, "301 Moved permanently");
//    }
}

// закрыть тюмень

$closedCitiesArr = array('tmn','kld','nsk','krd','sch','ufa','ast');
if(in_array($_REQUEST["CITY_ID"],$closedCitiesArr)){
    LocalRedirect("http://www.restoran.ru/", true, "301 Moved permanently");
}

if($rest=='urm'||preg_match('/urm\//',$APPLICATION->GetCurPage())){
    if(preg_match('/urm\//',$APPLICATION->GetCurPage())){
        $urm_url_str = preg_replace('/urm\//','rga/',$APPLICATION->GetCurPage());
    }
    else {
        $urm_url_str = '/rga/';
    }
    LocalRedirect("http://www.restoran.ru" . $urm_url_str, true, "301 Moved permanently");
}


//  перевод на мобильную версию
if ($rest != "m" && $_COOKIE["BITRIX_SM_FROM_MOBILE"] != "Y" && $_REQUEST["from_mobile"] != "Y") {

//    $_SESSION['lat'] = $_SESSION['lon'] = '';


    include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/Mobile_Detect.php");
    $detect = new Mobile_Detect();
    if (($detect->isMobile() || $detect->isTablet()) && !substr_count($_SERVER["HTTP_USER_AGENT"], "iPad")) {
        //if ($detect->isMobile()) {
        if ($APPLICATION->GetCurPage() == '/')
        {
            if ($_REQUEST["CITY_ID"]!="tmn" && CITY_ID!="tmn" && $_REQUEST["CITY_ID"]!="kld" && CITY_ID!="kld")
            {
                if ($_REQUEST["CONTEXT"]!="Y")
                    LocalRedirect("http://m.restoran.ru/", true); //header("Location: http://m.restoran.ru/"); // переадресация на "мобильную весию" сайта
                else
                    LocalRedirect("http://m.restoran.ru/?CONTEXT=Y", true);
            }
        }
        elseif ($_REQUEST["RESTOURANT"])
        {
            CModule::IncludeModule("iblock");
            $res = CIBlockElement::GetList(Array(), Array("IBLOCK_TYPE" => "catalog", "CODE" => $_REQUEST["RESTOURANT"], "ACTIVE" => "Y"), false, Array("nTopCount" => 1), Array("ID", "NAME", "DETAIL_PAGE_URL"));
            if ($ar = $res->Fetch()) {
                $r = CIBlock::GetByID($ar["IBLOCK_ID"]);
                if ($a = $r->Fetch())
                {
                    if ($_REQUEST["CONTEXT"]!="Y")
                        LocalRedirect("http://m.restoran.ru/detail.php?CITY_ID=".$a["CODE"]."&ID=" . $ar["ID"], true);
                    else
                        LocalRedirect("http://m.restoran.ru/detail.php?CITY_ID=".$a["CODE"]."&ID=" . $ar["ID"]."&CONTEXT=Y", true);
                }
            }
            else {
                LocalRedirect("http://m.restoran.ru/", true);
            }
        }
    }
}



//  перевод с кривых адресов на детальную ресторана
if ($rest != "test-spb" && $rest != "test-www" && $rest != "tln" && $rest != "en" && $rest != "ennew" && $rest != "amt" && $rest != "msk" && $rest != "ast" && $rest != "spb" && $rest != "new" && $rest != "ru" && $rest != "en" && $rest != "fr" && $rest != "de" && $rest != "sp" && $rest != "it" && $rest != "restoran" && $rest != "www" && $rest != "" && $rest != "m" && $rest != "kld" && $rest != "ufa" && $rest != "nsk"&& $rest != "rga"&& $rest != "urm") {

    CModule::IncludeModule("iblock");
    if (substr_count($APPLICATION->GetCurPage(), "detailed")) {
        LocalRedirect("http://www.restoran.ru" . $APPLICATION->GetCurPage(), true, "301 Moved permanently");
    }
    else {
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_TYPE" => "catalog", "CODE" => $rest, "ACTIVE" => "Y"), false, Array("nTopCount" => 1), Array("ID", "NAME", "DETAIL_PAGE_URL"));
        if ($ar = $res->GetNext()) {
//            LocalRedirect("http://www.restoran.ru" . $ar["DETAIL_PAGE_URL"], true, "301 Moved permanently");
            LocalRedirect("http://www.restoran.ru" . preg_replace('/detailed/','context',$ar["DETAIL_PAGE_URL"]), true, "301 Moved permanently");
        }
        else {
            @define("ERROR_404", "Y");
            CHTTP::SetStatus("404 Not Found");
        }
    }
}



//if ($rest != "" && $_SERVER['HTTP_HOST']=='restoran.ru' && $rest != 'm' && $rest != 'en' && $rest != 'spb' && $rest != 'urm' && ERROR_404!='Y' && $CUR_DIR=='/') {
//    //  todo добавить проверку куки на город и отправлять на правильный хост
//    //  TODO решить Ригу
//    if($APPLICATION->get_cookie("CITY_ID")=='urm'||$APPLICATION->get_cookie("CITY_ID")=='spb'){
//        LocalRedirect("http://".$APPLICATION->get_cookie("CITY_ID").".restoran.ru".$APPLICATION->GetCurPage(), true, "301 Moved permanently");
//    }
//    elseif($APPLICATION->get_cookie("CITY_ID")=='rga'){
//        LocalRedirect("http://www.restoran.ru/rga".$APPLICATION->GetCurPage(), true, "301 Moved permanently");
//    }
//    else {
//        LocalRedirect("http://www.restoran.ru".$APPLICATION->GetCurPage(), true, "301 Moved permanently");
//    }
//}

//  перевод на детальную
if ($_REQUEST["CITY_ID"] && $_REQUEST["CITY_ID"] != "amt" && $_REQUEST["CITY_ID"] != "nsk"&& $_REQUEST["CITY_ID"] != "rga"&& $_REQUEST["CITY_ID"] != "urm" && $_REQUEST["CITY_ID"] != "kld" && $_REQUEST["CITY_ID"] != "ufa" && $_REQUEST["CITY_ID"] != "tln" && $_REQUEST["CITY_ID"] != "msk" && $_REQUEST["CITY_ID"] != "spb" && $_REQUEST["CITY_ID"] != "ast" && $_REQUEST["CITY_ID"] != "anp" && $_REQUEST["CITY_ID"] != "krd" && $_REQUEST["CITY_ID"] != "sch" && $_REQUEST["CITY_ID"] != "all" && $_REQUEST["CITY_ID"] != "content" && ERROR_404!='Y') {
    CModule::IncludeModule("iblock");
    if ($_REQUEST["CATALOG_ID"] && $_REQUEST["RESTOURANT"]) {
        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_TYPE" => "catalog", "CODE" => $_REQUEST["RESTOURANT"], "ACTIVE" => "Y"), false, Array("nTopCount" => 1), Array("ID", "NAME", "DETAIL_PAGE_URL"));
        if ($ar = $res->GetNext()) {
            LocalRedirect("http://www.restoran.ru" . $ar["DETAIL_PAGE_URL"], true, "301 Moved permanently");
        }
    }
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    LocalRedirect("http://www.restoran.ru/404.php", true);
}


//  404 с хоста с кривым запросом города
// define user city code
if ($_SERVER["HTTP_HOST"] == "spb.restoran.ru" || $_SERVER["HTTP_HOST"] == "spb.restoran.ru:80") {
    if(!empty($_REQUEST['CITY_ID'])){
        if($_REQUEST['CITY_ID']!='spb' && $APPLICATION->GetCurDir()!='/' && !preg_match('/\/content\//',$APPLICATION->GetCurDir())){
            LocalRedirect("http://www.restoran.ru" . $APPLICATION->GetCurPage(), true, "301 Moved permanently");
//            @define("ERROR_404", "Y");
//            CHTTP::SetStatus("404 Not Found");
        }
    }
    define("CITY_ID", "spb");
    $APPLICATION->set_cookie("CITY_ID", "spb", time() + 60 * 60 * 24, "/", ".restoran.ru", false, true);
}
else {
    $cityCode = getCityCode();
    define("CITY_ID", $cityCode);

    $APPLICATION->set_cookie("CITY_ID", $cityCode, time()+60*60*24, "/", ".restoran.ru",false,true);
}

if ($rest == "ennew")
{
    define("SITE_ID","s2");
    define("LANGUAGE_ID","en");
    //  LocalRedirect("http://en.restoran.ru" . $url, true, "301 Moved permanently");

}

//  перевод на хост по городу (можно ли сюда попасть??! если хост приоритетней чем город)
if ($rest != "ennew" && $rest != "en" && $rest != "m" && !$_SERVER["HTTPS"] && ERROR_404!='Y') {
    if ($cityCode == "spb" && $_SERVER["HTTP_HOST"] != "spb.restoran.ru" && $_SERVER["HTTP_HOST"] != "spb.restoran.ru:80"&& $_SERVER["HTTP_HOST"]!="spb.restoran.ru:443") {
        $url = $APPLICATION->GetCurPage();
        if (!substr_count($url, "tpl") && !substr_count($url, "bitrix") && !substr_count($url, "suggest") && !substr_count($url, "php") && !substr_count($url, "auth") && !substr_count($url, "bs"))
        {
            if ($_SERVER["HTTPS"])
            {
                $url = $APPLICATION->GetCurPageParam();
                LocalRedirect("https://spb.restoran.ru" . $url, true, "301 Moved permanently");
            }
            else
                LocalRedirect("http://spb.restoran.ru" . $url, true, "301 Moved permanently");
        }
    }
    if ($cityCode == "msk" && $_SERVER["HTTP_HOST"] != "www.restoran.ru" && $_SERVER["HTTP_HOST"] != "www.restoran.ru:80"&& $_SERVER["HTTP_HOST"]!="www.restoran.ru:443") {
        $url = $APPLICATION->GetCurPage();
        if (!substr_count($url, "tpl") && !substr_count($url, "bitrix") && !substr_count($url, "suggest") && !substr_count($url, "php") && !substr_count($url, "auth") && !substr_count($url, "bs"))
        {
            if ($_SERVER["HTTPS"])
            {
                $url = $APPLICATION->GetCurPageParam();
                LocalRedirect("https://www.restoran.ru" . $url, true, "301 Moved permanently");
            }
            else
                LocalRedirect("http://www.restoran.ru" . $url, true, "301 Moved permanently");
        }
    }
    if ($cityCode == "rga" && $CUR_DIR=='/') {
        $url = $APPLICATION->GetCurPage();
        if (!substr_count($url, "tpl") && !substr_count($url, "bitrix") && !substr_count($url, "suggest") && !substr_count($url, "php") && !substr_count($url, "auth") && !substr_count($url, "bs"))
        {
            LocalRedirect("http://www.restoran.ru/rga/", true, "301 Moved permanently");
        }
    }
    if ($cityCode == "rga" && $_SERVER["HTTP_HOST"] != "www.restoran.ru" && $_SERVER["HTTP_HOST"] != "www.restoran.ru:80"&& $_SERVER["HTTP_HOST"]!="www.restoran.ru:443") {
        $url = $APPLICATION->GetCurPage();
        if (!substr_count($url, "tpl") && !substr_count($url, "bitrix") && !substr_count($url, "suggest") && !substr_count($url, "php") && !substr_count($url, "auth") && !substr_count($url, "bs"))
        {
            LocalRedirect("http://www.restoran.ru" . $url, true, "301 Moved permanently");
        }
    }
}


/**  EN  **/
if (CITY_ID != "msk" && CITY_ID != "spb"&& CITY_ID != "rga"&& CITY_ID != "urm" && $rest == "en" && ERROR_404!='Y') {
    LocalRedirect("http://restoran.ru" . $APPLICATION->GetCurPage(), true, "301 Moved permanently");
}
//if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/index.php")
//    LocalRedirect("/".CITY_ID."/");
// check redirect links
if (!substr_count($APPLICATION->GetCurDir(), "bitrix") && !substr_count($APPLICATION->GetCurDir(), "bs") && $APPLICATION->GetCurPage() != "/index.php" && $APPLICATION->GetCurDir() != "/" && ERROR_404!='Y') {
    $redir = new RedirectURIClass();
    global $APPLICATION;
    $redir->redirectURI($APPLICATION->GetCurDir());
}


//if(($_SERVER["HTTP_HOST"] == "www.restoran.ru" || $_SERVER["HTTP_HOST"] == "www.restoran.ru:80") && !$_SERVER["HTTPS"]){
//    LocalRedirect("https://www.restoran.ru" . $APPLICATION->GetCurPageParam(), true, "301 Moved permanently");
//}

// hook for define site language id
if (SITE_ID == 's2')
    $lang = 'en';
else
    $lang = 'ru';
define("SITE_LANGUAGE_ID", $lang);


AddEventHandler("main", "OnBeforeUserUpdate", Array("RestUsers", "OnBeforeUserUpdateHandler"));
// after user add event
AddEventHandler("main", "OnAfterUserAdd", Array("RestUsers", "OnAfterUserAddHandler"));
AddEventHandler("main", "OnBeforeUserAdd", Array("RestUsers", "OnBeforeUserAddHandler"));
// before add post to blog
AddEventHandler("blog", "OnBeforePostAdd", Array("RestBlogPost", "OnBeforePostAddHandler"));
// before user register
AddEventHandler("main", "OnBeforeUserRegister", Array("RestUsers", "OnBeforeUserRegisterHandler"));
AddEventHandler("main", "OnAfterUserRegister", Array("RestUsers", "OnAfterUserRegisterHandler"));
//after section add
//AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("RestIBlock", "OnAfterIBlockSectionAddHandler"));
//before section delete
//AddEventHandler("iblock", "OnBeforeIBlockSectionDelete", Array("RestIBlock", "OnBeforeIBlockSectionDeleteHandler"));
//after element add
//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("RestIBlock", "OnBeforeIBlockElementAddHandler"));
//before element delete
//AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("MyClass", "OnBeforeIBlockElementDeleteHandler"));
AddEventHandler("search", "BeforeIndex", Array("RestSearch", "BeforeIndexHandler"));
AddEventHandler("search", "OnBeforeIndexUpdate", Array("RestSearch", "BeforeIndexUpdateHandler"));
// after sale.order.ajax complete
AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", Array("RestOrder", "OnOrderAddHandler"));
// after status change webform
AddEventHandler('form', 'onAfterResultStatusChange', Array("WebFormHandler", "onAfterResultStatusChangeHandler"));

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("RestIBlock", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("RestIBlock", "OnAfterIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("RestIBlock", "OnAfterIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("RestIBlock", "OnBeforeIBlockElementDeleteHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementDelete", Array("RestIBlock", "OnAfterIBlockElementDeleteHandler"));
/* if ($_REQUEST["a"])
  {
  $fp = fopen($_SERVER["DOCUMENT_ROOT"].'/upload/file_upload_csv/session.txt', 'w');
  fwrite($fp, bitrix_sessid_get()."/n");
  fclose($fp);
  } */
$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
$mobile = strpos($_SERVER['HTTP_USER_AGENT'], "Mobile");
$symb = strpos($_SERVER['HTTP_USER_AGENT'], "Symbian");
$operam = strpos($_SERVER['HTTP_USER_AGENT'], "Opera M");
$htc = strpos($_SERVER['HTTP_USER_AGENT'], "HTC_");
$fennec = strpos($_SERVER['HTTP_USER_AGENT'], "Fennec/");
$winphone = strpos($_SERVER['HTTP_USER_AGENT'], "WindowsPhone");
$wp7 = strpos($_SERVER['HTTP_USER_AGENT'], "WP7");
$wp8 = strpos($_SERVER['HTTP_USER_AGENT'], "WP8");

if (($iphone || $android || $palmpre || $ipod || $berry || $mobile || $symb || $operam || $htc || $fennec || $winphone || $wp7 || $wp8 === true)) {
    define("MOBILE", "mobilec");
} else {
    define("MOBILE", "");
}


AddEventHandler("subscribe", "BeforePostingSendMail", Array("SubscribeByCategory", "BeforePostingSendMailHandler"));

class SubscribeByCategory {

    function BeforePostingSendMailHandler($arFields) {
        CModule::IncludeModule("subscribe");
        CModule::IncludeModule("iblock");

        $post_rub = CPosting::GetRubricList($arFields['POSTING_ID']);
        if($post_rub_arr = $post_rub->Fetch())
        {
            if ($post_rub_arr["ID"] == '26' || $post_rub_arr["ID"] == '27' || $post_rub_arr["ID"] == '28') {
                $rsUser = CUser::GetByID($arFields["EMAIL_EX"]["USER_ID"]);
                $arUser = $rsUser->Fetch();
                $podpiska = unserialize($arUser["WORK_STREET"]);
                $selectedRubrics = array();

                $city = $podpiska['CITY_ID'];
                foreach ($podpiska["rubriki"] as $key => $value) {
                    $selectedRubrics[$value] = $value;
                }


                $body = '';
                $body .= '<table cellpadding="0" cellspacing="0" width="100%" style="background:#2c323c;  padding:10px 40px; padding-top:0px;"><tr><td style="padding-bottom:0px;"><table align="center" cellpadding="0" cellspacing="0"  width="730" height="167" style="position:relative; overflow:hidden; background-image:url(\'http://restoran.ru/tpl/images/mail/rassilka/rassilka_head.jpg\')"><tr><td height="167" style="color:#FFF; text-align:center; font-size:14px; font-family: Georgia; "><a href="http://www.restoran.ru/" style="width:100%; height: 167px; display:block"></a></td></tr></table><table align="center" cellpadding="0" cellspacing="0"  width="730" style="position:relative;">';
                $bodyToSave = '';
                $bodyToSave .= '<table align="center" cellpadding="0" cellspacing="0"  width="730" style="position:relative;">';

                if (isset($selectedRubrics['news'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/news_" . $city . ".php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/news_" . $city . ".php");
                }
                if (isset($selectedRubrics['receipts'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/receipts_" . $city . "_msk.php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/receipts_" . $city . "_msk.php");
                }
                if (isset($selectedRubrics['new_rests'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/new_rests_" . $city . ".php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/new_rests_" . $city . ".php");
                }
                if (isset($selectedRubrics['reviews'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/reviews_" . $city . ".php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/reviews_" . $city . ".php");
                }
                if (isset($selectedRubrics['photoreports'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/photoreports_" . $city . ".php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/photoreports_" . $city . ".php");
                }
                if (isset($selectedRubrics['blogs'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/blogs_" . $city . ".php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/blogs_" . $city . ".php");
                }
                if (isset($selectedRubrics['afisha'])) {
                    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/afisha_" . $city . ".php");
                    $bodyToSave .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/afisha_" . $city . ".php");
                }
                $body .= '<tr><td style="background:#FFFFFF; padding:30px 37px; padding-bottom:30px; text-align: center;">Управление рассылками доступно только зарегистированным пользователям в личном кабинете. Пожалуйста, пройдите процедуру регистрации <a style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; color:#0097D6; font-size:12px; font-family: Georgia;" href="http://www.restoran.ru/auth/?forgot_password=yes">здесь</a></td></tr>';
                $body .= '</table></td></tr></table>';
                $bodyToSave .= '</table>';
                $body = str_replace("é", "e", $body);
                $body = str_replace("è", "e", $body);
                $body = str_replace("É", "E", $body);
                $body = str_replace("È", "E", $body);
                $body = str_replace("À", "A", $body);
                $body = str_replace("à", "a", $body);
                $body = str_replace("á", "a", $body);
                //$body = str_replace("'", "`", $body);
                //$bodyToSave = $body;
                $body = iconv("utf-8", "windows-1251//IGNORE", $body);

                if (in_array(2, $podpiska["where"])) {
                    $el = new CIBlockElement;
                    $PROP = array();
                    $PROP["SUBSCRIBE_USER_ID"] = $arFields["EMAIL_EX"]["USER_ID"];
                    $arLoadProductArray = Array(
                        "MODIFIED_BY" => $arFields["EMAIL_EX"]["USER_ID"], // элемент изменен текущим пользователем
                        "IBLOCK_ID" => 2868,
                        "NAME" => "Рассылка по категориям от " . date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("SHORT")), time()),
                        "PROPERTY_VALUES" => $PROP,
                        "NAME" => "Элемент",
                        "ACTIVE" => "Y", // активен
                        "PREVIEW_TEXT" => "Рассылка по категориям от " . date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("SHORT")), time()),
                        "DETAIL_TEXT" => $bodyToSave,
                        'DETAIL_TEXT_TYPE' => 'html',
                        "DATE_ACTIVE_FROM" => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("SHORT")), time()),
                        "DATE_ACTIVE_TO" => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("SHORT")), time() + 3600 * 24 * 30),
                        //"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
                    );
                    if ($PRODUCT_ID = $el->Add($arLoadProductArray))
                        echo "New ID: " . $PRODUCT_ID;
                    else
                        echo "Error: " . $el->LAST_ERROR;
                }

                if (in_array(1, $podpiska["where"])) {
                    if (!trim($body))
                        return false;
                    $arFields["BODY"] = str_replace("mailbody", $body, $arFields["BODY"]);
                    return $arFields;
                }
                return false;
            }
        }
        return $arFields;
    }

}

?>