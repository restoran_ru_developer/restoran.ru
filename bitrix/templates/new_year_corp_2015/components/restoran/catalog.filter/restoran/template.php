<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$i=0;
foreach($arResult["arrProp"] as $prop_id => $arProp):?>
    <div class="form-group">
        <label for="name" ><?=$arProp["NAME"]?></label>
        <?if ($arProp["PROPERTY_TYPE"]!="L"):?>
            <select multiple class="multiselect-<?=$arProp["CODE"]?> form-control" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]">                            
                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                    <?if ($arProp["CODE"]!="subway"):?>
                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>                
                    <?else:?>
                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val["NAME"]?></option>                
                    <?endif;?>
                <?endforeach?>
            </select> 
            <script type="text/javascript">                                        
                $(function() {
                    $('.multiselect-<?=$arProp["CODE"]?>').multiselect({
                        numberDisplayed:1,
                        nonSelectedText: '<?=GetMessage('multiselect-'.$arProp["CODE"])?>',
                        nSelectedText: "Выбрано",   
                        maxHeight: 250,
                        buttonWidth: "142px"
                    });
                });
            </script>
        <?else:?>
            <div class="checkbox-box">
                <div class="yes <?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]=="Да")?"active":""?>">Да</div>
                <div class="no <?=(isset($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]])&&$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]!="Да")?"active":""?>">Нет</div>
                <input type="hidden" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>]" value="<?=$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]?>" />
            </div>
            <script type="text/javascript">                                        
                $(function() {
                    $(".checkbox-box div").click(function(){
                        $(this).parents(".checkbox-box").find("div").removeClass("active");
                        $(this).addClass("active");
                        if ($(this).text()=="Да")
                            $(this).parents(".checkbox-box").find("input").val($(this).text());
                        else
                            $(this).parents(".checkbox-box").find("input").val("");
                    });
                });
            </script>
        <?endif;?>   
        <?$i++?>
    </div>        
<?endforeach;?>        
<div class="form-group">
    <label>&nbsp;</label>
    <input type="submit" class="form-control btn-light" value="Далее">
</div>