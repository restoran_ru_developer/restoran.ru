jQuery.fn.galery = function(options){
    // настройки по умолчанию
    var options = jQuery.extend({
        num: 5,
        speed:500,
        count: true
    },options);
    

    var _this = this;
    return this.each(function() {    
        var num = "";
        var container = "";
        var item = "";
        this.counter = function(a,b) {
            var obj = container.parent().find(".scroll_num");
            var obj_val = obj.html()*1;
            if (!b)
            {
                if (a) 
                {
                    if (obj_val==num)
                        obj.html(1);
                    else
                        obj.html(++obj_val);
                }
                else
                {
                    if (obj_val==1)
                        obj.html(num);
                    else
                        obj.html(--obj_val);
                }
            }
            else
            {
                obj.html(b.attr("num"));
            }
        };
    
        this.next = function(obj) {
            var url, text;        
            if (container.find(".active").next().hasClass("item"))
            {                        
                if (typeof(obj)=="undefined")
                {
                    url = container.find(".active").removeClass("active").next().addClass("active").find("img").attr("src");            
                    text = container.find(".active").find("img").attr("alt");
                }
                else
                {
                    container.find(".active").removeClass("active");
                    url = obj.addClass("active").find("img").attr("src");
                    text = obj.find(".active").find("img").attr("alt");
                }
                if (options.count) this.counter(1,obj);
            }
            else
            {            
                if (typeof(obj)=="undefined")
                {
                    url = container.find(".active").removeClass("active").end().find(".item:first").addClass("active").find("img").attr("src");            
                    text = container.find(".active").find("img").attr("alt");
                }
                else
                {
                    container.find(".active").removeClass("active");
                    url = obj.addClass("active").find("img").attr("src");                
                    text = obj.find(".active").find("img").attr("alt");
                }
                if (options.count) this.counter(1,obj);
                container.find(".scroll_container").animate({"marginLeft":0},options.speed,"linear");                
            }
            var wrap = container.parent().find(".img");
            var video = "";
            if (container.find(".active").find("img").attr("video"))
                video = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="266" border="0"><param name="movie" value="/tpl/js/panorama.swf?img='+url+'&amp;type=0"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"><embed src="/tpl/js/panorama.swf?img='+url+'&amp;type=0" width="397" height="300" border="0" type="application/x-shockwave-flash"></object>';
            if (container.find(".active").find("img").attr("dtour"))
            {
                if (!text||text=="undefined")
                    text = container.find(".active").find("img").attr("alt");
                video = '<object type="application/x-shockwave-flash" data="'+text+'" width="397" height="266" allowFullScreen="true"><param name="movie" value="'+text+'" /></object>';
            }
            if (container.find(".active").find("img").attr("lookon"))
            {
                if (!text||text=="undefined")
                    text = container.find(".active").find("img").attr("alt");
                video = '<a class="lookon_iframe" target="_blank" href="'+container.find(".active").find("img").attr("alt_iframe")+'" style="width:395px;height:264px; position:absolute;display:block; z-index:10000;"></a>\n\
<embed type="application/x-shockwave-flash" src="'+container.find(".active").find("img").attr("alt")+'" width="395" height="264" style="undefined" id="krpanoSWFObject" name="krpanoSWFObject" bgcolor="#000000" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="xml='+container.find(".active").find("img").attr("alt_xml")+'">';
            }
            if (video)
            {
                if (wrap) 
                    wrap.fadeTo(0, 0.2,function(){ $(this).html(video);}).fadeTo(0, 1);
            }
            else
            {
                if (wrap) 
                    wrap.fadeTo(0, 0.2,function(){ $(this).find("img").attr("src",url); $(this).find("p").html(text); $(this).find("img").attr("alt",text);}).fadeTo(0, 1);                
            }
            if (container.find(".active").attr("num")>=options.num)
            {                
                var left = item.width()*1+item.css("margin-right").replace("px","")*1;
                $(this).find(".scroll_container").animate({"marginLeft":"-="+left},options.speed,"linear");                
            }
        };
    
        this.prev = function(obj) {
            var url, text;
            if (container.find(".active").prev().hasClass("item"))
            {                
                if (!obj)
                {
                    url = container.find(".active").removeClass("active").prev().addClass("active").find("img").attr("src");            
                    text = container.find(".active").find("img").attr("alt");
                }
                else
                {
                    container.find(".active").removeClass("active");
                    url = obj.addClass("active").find("img").attr("src");
                    text = obj.find(".active").find("img").attr("alt");            
                }
                if (options.count) this.counter(0,obj);
                if (container.find(".active").attr("num")>=options.num)
                {                
                    var left = item.width()*1+item.css("margin-right").replace("px","")*1;
                    container.find(".scroll_container").animate({"marginLeft":"-="+-left},options.speed,"linear");                                    
                }
                if (obj&&obj.attr("num")*1<=options.num*1)
                    container.find(".scroll_container").animate({"marginLeft":"0px"},options.speed,"linear");                

            }
            else
            {                
                if (!obj) 
                {
                    url =  container.find(".active").removeClass("active").end().find(".item:last").addClass("active").find("img").attr("src");            
                    text = container.find(".active").find("img").attr("alt");
                }
                else
                {
                    container.find(".active").removeClass("active");
                    url = obj.addClass("active").find("img").attr("src");
                    text = obj.find(".active").find("img").attr("alt");            
                }
                if (options.count) this.counter(0,obj);
                var mleft = item.width()*1+item.css("margin-right").replace("px","")*1;
                var left = mleft*num-mleft*options.num;
                container.find(".scroll_container").animate({"marginLeft":-left},options.speed,"linear");                
            }
            var wrap = container.parent().find(".img");
            var video = "";
            if (container.find(".active").find("img").attr("video"))
                video = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="266" border="0"><param name="movie" value="/tpl/js/panorama.swf?img='+url+'&amp;type=0"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"><embed src="/tpl/js/panorama.swf?img='+url+'&amp;type=0" width="397" height="266" border="0" type="application/x-shockwave-flash"></object>';
            if (container.find(".active").find("img").attr("dtour"))
            {
                if (!text||text=="undefined")
                    text = container.find(".active").find("img").attr("alt");
                video = '<object type="application/x-shockwave-flash" data="'+text+'" width="397" height="266" allowFullScreen="true"><param name="movie" value="'+text+'"></object>';
            }
            if (container.find(".active").find("img").attr("lookon"))
            {
                if (!text||text=="undefined")
                    text = container.find(".active").find("img").attr("alt");
                video = '<a target="_blank" class="lookon_iframe" href="'+container.find(".active").find("img").attr("alt_iframe")+'" style="width:395px;height:264px; position:absolute;display:block; z-index:10000;"></a>\n\
<embed type="application/x-shockwave-flash" src="'+container.find(".active").find("img").attr("alt")+'" width="395" height="264" style="undefined" id="krpanoSWFObject" name="krpanoSWFObject" bgcolor="#000000" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="xml='+container.find(".active").find("img").attr("alt_xml")+'">';
            }
            if (video)
            {
                if (wrap) 
                    wrap.fadeTo(0, 0.2,function(){ $(this).html(video);}).fadeTo(0, 1);
            }
            else
            {
                if (wrap) 
                    wrap.fadeTo(0, 0.2,function(){ $(this).find("img").attr("src",url); $(this).find("p").html(text);  $(this).find("img").attr("alt",text); }).fadeTo(0, 1);                
            }
            //if (wrap) wrap.fadeTo("medium", 0.2,function(){ $(this).find("img").attr("src",url);}).fadeTo("fast", 1); 
        };
    
        num = $(this).find(".scroll_container div").length;
        container = $(this).find(".special_scroll");
        item = container.find(".item");
        var sthis = this;
        container.find(".item").click(function(){            
            var current = container.find(".active").attr("num")*1;
            var i=1;
            if ($(this).attr("num")*1>=options.num&&$(this).attr("num")*1>current)
            {
                sthis.next($(this));                     
            }
            else if ($(this).attr("num")*1<options.num&&$(this).attr("num")*1>current)
            {
                sthis.next($(this));                     
            }
            else if ($(this).attr("num")*1>=options.num&&$(this).attr("num")*1<current)
            {
                sthis.prev($(this));
            }
            else
                sthis.prev($(this));
        });

        $(this).find(".scroll_arrows").find(".next").click(function(){        
            sthis.next();
        });
        
        $(this).find(".scroll_arrows").find(".prev").click(function(){                    
           sthis.prev();
        }); 
    });
 //   return this.init();
}