<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js')?>
    <?//$APPLICATION->AddHeadScript('http://cdn.jquerytools.org/1.2.6/all/jquery.tools.min.js')?>    
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true)?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js')?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/styles.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/bitrix/templates/main/css/cusel.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js')?>            
    <?$APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js')?>             
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js')?>    
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js')?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/style.css";  type="text/css" rel="stylesheet" />', true)?>
</head>
<body>
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '297181676964377', // App ID
                channelURL : '//<?=SITE_SERVER_NAME?>/channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                oauth      : true, // enable OAuth 2.0
                xfbml      : true  // parse XFBML
            });
        };

        (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
        }(document));
    </script>

    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="container">
        <div id="wrapp">
            <div id="header">
                <div class="baner">
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "top_main_page_".CITY_ID,
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "0"
                            ),
                    false
                    );?>
                </div> 
                <div id="logo" class="left"><a href="/<?=CITY_ID?>/"><img src="/tpl/images/logo.png" /></a></div>                
                <div class="city_select left" style="margin-top:20px; margin-right:10px;width:140px">  
                    <div style="margin-right:15px; margin-bottom:16px;">
                        <?$APPLICATION->IncludeComponent(
                            "restoran:city.selector",
                            "city_select",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_URL" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000000",
                                "CACHE_NOTES" => "new3212",
                                "CACHE_GROUPS" => "Y"
                            ),
                        false
                        );?>      
                        <div class="clear"></div>
                    </div>                    
                    <div style="margin-right:20px">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.site.selector",
                            "lang_selector",
                            Array(
                                "SITE_LIST" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "360000000000",
                                "CACHE_NOTES" => ""
                            ),
                        false
                        );?>                    
                        <div class="clear"></div>
                    </div>                    
                </div>
                <div class="left" style="margin-top:20px; text-align: center; margin-right:12px;">
                        <div class="phones" style="line-height:22px;margin-bottom:12px;">
                            <?
                            $APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                                Array(),
                                Array("MODE"=>"html")
                            );
                            ?>                            
                        </div>                        
                        <div>
                            <?if (CITY_ID=="ast"||CITY_ID=="krd"||CITY_ID=="tmn"):?>
                                <script>
                                function cities_bron(params)
                                {
                                    $.ajax({
                                        type: "POST",
                                        url: "/tpl/ajax/bron_<?=CITY_ID?>.php",
                                        data: params,
                                        success: function(data) {
                                            if (!$("#cbron_form").size())
                                            {
                                                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
                                            }
                                            $('#cbron_form').html(data);
                                            showOverflow();
                                            setCenter($("#cbron_form"));
                                            $("#cbron_form").css("display","block"); 
                                        }
                                    });
                                }
                                </script>
                                <a href="javascript:void(0)" onclick="cities_bron('<?=bitrix_sessid_get()?>')" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                            <?else:?>
                                <?if ($USER->IsAdmin()||CSite::InGroup(Array(15,16))):?>
                                    <a href="javascript:void(0)" id="add_rest" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?elseif ($USER->IsAuthorized()):?>
                                    <a href="/users/id<?=$USER->GetID()?>/rest_edit/" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?else:?>
                                    <a href="/auth/" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?endif;?>
                            <?endif;?>
                        </div>
                </div>
                <div class="left" style="margin-top:22px;">
                    <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient2"  onclick="ajax_bron2('<?=bitrix_sessid_get()?>&banket=Y',1)"><?=GetMessage("BANKET_BOOK")?></a>
                        </div>
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient2"  onclick="ajax_bron('<?=bitrix_sessid_get()?>')"><?=GetMessage("TABLE_BOOK")?></a>
                        </div>
                    <?else:?>                        
                    <?endif;?>
                </div>
                <div class="right" style="margin-top:20px; width:150px; overflow:hidden; position:relative">   
                    <div class="top_middle_menu" style="margin-right:0px;">
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:system.auth.form",
                                "user",
                                Array(
                                        "REGISTER_URL" => "",
                                        "FORGOT_PASSWORD_URL" => "",
                                        "PROFILE_URL" => "",
                                        "SHOW_ERRORS" => "N"
                                ),
                        false
                        );?> 
                        <div class="clear"></div>
                    </div>                                                                             
                </div>                            
                <?/*else:?>
                    <div class="phone_block right" onclick="ajax_bron('<?=bitrix_sessid_get()?>')"  style="margin-right:20px;">
                        <?=GetMessage("ORDER_BUBBLE_TEXT")?>
                    </div>
                <?endif;*/?>
                <div class="clear"></div>            
            </div>        
            <?
            $kuponIB = getArIblock("kupons", CITY_ID);
            global $current;
            $current = getKuponsCount($kuponIB["ID"],Array(">DATE_ACTIVE_TO"=>date("d.m.Y H:i:s")));
            //$last = getKuponsCount($kuponIB["ID"],Array("<=DATE_ACTIVE_TO"=>date("d.m.Y H:i:s")));
            ?>
<!--            <div id="main_menu">
                <ul>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top_kupon_1",
                        Array(
                            "ROOT_MENU_TYPE" => "top_kupon",
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(CITY_ID)
                        ),
                    false
                    );?>
                </ul>
                <div class="clear"></div>
            </div>
            <div id="block960">
                <div class="left toptop_menu">
                        <ul>
                            <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "kupons",
                                    Array(
                                            "ROOT_MENU_TYPE" => "kupon",
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "",
                                            "USE_EXT" => "Y",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_TIME" => "3600",
                                            "MENU_CACHE_USE_GROUPS" => "Y",
                                            "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                    ),
                            false
                            );?> 
                        </ul>
                </div>

                <div class="right">
                    <?if ($USER->IsAuthorized()):?>
                        <a href="/users/id<?=$USER->GetID()?>/rest_edit/" class="add_block_blue"><?=GetMessage("ADD_KUPON")?></a>
                    <?else:?>
                        <a href="/auth/" class="add_block_blue"><?=GetMessage("ADD_KUPON")?></a>
                    <?endif;?>
                </div>
                <div class="clear"></div>
            </div>        -->
                <div id="block960">
                    <?if (SITE_ID=="s1"):?>
                    <div class="left" style="width:820px">     
                    <?endif;?>
                        <div id="main_menu" <?if (SITE_ID=="s1"):?>style="width:820px;"<?endif;?>>
                                <ul>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top_1",
                                        Array(
                                            "ROOT_MENU_TYPE" => "top_kupon",
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "top_1",
                                            "USE_EXT" => "N",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_TIME" => "3600000000",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                            "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                        ),
                                    false
                                    );?>
                                </ul>
                        </div>
                        <div id="toptop_menu_new" style="margin:0px;width:820px;<?if (SITE_ID!="s1"):?>margin-bottom:10px<?endif;?>">
                            <ul>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top_small_2_new_2_11",
                                        Array(
                                                "ROOT_MENU_TYPE" => "art_".CITY_ID,
                                                "MAX_LEVEL" => "1",
                                                "CHILD_MENU_TYPE" => "",
                                                "USE_EXT" => "N",
                                                "DELAY" => "N",
                                                "ALLOW_MULTI_SELECT" => "N",
                                                "MENU_CACHE_TYPE" => "A",
                                                "MENU_CACHE_TIME" => "3600000000",
                                                "MENU_CACHE_USE_GROUPS" => "N",
                                                "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                        ),
                                false
                                );?> 
                            </ul>
                        </div>
                    <?if (SITE_ID=="s1"):?>
                    </div>
                    <div class="right"  style="width:152px">
                        <?if (CITY_ID!="tmn"):?>
                            <div style="width:112px; height:90px; margin-top: -20px;">
                                    <a onclick="ajax_bron('<?=bitrix_sessid_get()?>')" id="bron_round" href="javascript:void(0)" style="position:relative; text-decoration:none;">    
                                        <div id="bron_round_phone"></div>
                                        <?if (CITY_ID=="spb"):?>
                                            <div id="rounded2" class="bron_round_blue"></div>
                                        <?else:?>
                                            <div id="rounded2" class="bron_round_red"></div>
                                        <?endif;?>                                    
                                        <div id="rounded1" class="bron_round_orange"></div>                                    
                                        <div id="rounded_first" class="appetite appetite_bron_15">Хочу заказать столик<?//=GetMessage("NEW_YEAR")?></div>                                   
                                        <?if (CITY_ID=="spb"):?>
                                            <div id="rounded_second" class="appetite appetite_bron_15_2">(812)<br />740-1820<?//=GetMessage("NEW_YEAR")?></div>                                                                           
                                        <?else:?>
                                            <div id="rounded_second" class="appetite appetite_bron_15_2">(495)<br />988-2656<?//=GetMessage("NEW_YEAR")?></div>
                                        <?endif;?>
                                    </a>   
                                    <?/*                             
                                    <!--<a href="/<?=CITY_ID?>/articles/8marta/"><img src="<?=SITE_TEMPLATE_PATH?>/images/1362071058_bouquet.png" /></a>-->
                                    <div style="position: absolute; z-index: 100; margin: 0px auto;">
                                        <a href="/<?=CITY_ID?>/articles/8marta/"><img src="/bitrix/images/1.gif" width="157" height="116" border="0" alt=""></a>                                
                                    </div>
                                    <object classid="clsid:D27CDB6E-AE6D-11CF-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="157" height="116">
                                        <param name="movie" value="<?=SITE_TEMPLATE_PATH?>/images/8mart3.swf">
                                        <param name="quality" value="high">
                                        <param name="bgcolor" value="#FFFFFF">
                                        <param name="wmode" value="opacue">
                                        <embed src="<?=SITE_TEMPLATE_PATH?>/images/8mart3.swf" quality="high" bgcolor="#FFFFFF" wmode="opacue" width="157" height="116" name="banner" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">
                                    </object>  
                                    <a href="/<?=CITY_ID?>/articles/8marta/"><img src="<?=SITE_TEMPLATE_PATH?>/images/8m.png" /></a>
                                    <!--<a href="/<?=CITY_ID?>/articles/valentine/"><img src="/tpl/images/valentine.png" /></a>-->*/?>
                            </div>
                        <?else:?>
                            <div style="width:112px; height:90px; margin-top: -30px;">                                    
                                <a href="/<?=CITY_ID?>/articles/reklama/"><img src="/tpl/images/tmn_povar.png" /></a>
                            </div>
                        <?endif;?>
                        <!--<div style="width:112px; height:90px; margin-top: -20px;">
                            <a id="ny_ded" href="javascript:void(0)" style="position:relative; text-decoration:none;cursor:default">    
                                <div class="ny_round_64"></div>
                                <div class="cur_s"></div>
                                <div class="appetite appetite_ny_15"><?=GetMessage("NEW_YEAR")?></div>
                                <div id="ny_dm_92"></div>
                            </a>
                        </div>-->
                    </div>
                    <div class="clear"></div>
                    <?endif;?>
                </div>
                
                <div id="filter">
                    <div class="filter">                    
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:search.title",
                                "main_search_suggest_new",
                                Array(),
                            false
                        );?>                    
                        <?$arRestIB = getArIblock("kupons", CITY_ID);?>                                       
                        <noindex>
                        <?    $APPLICATION->IncludeComponent(
                                    "restoran:catalog.filter",
                                    "kupons",
                                    Array(
                                            "IBLOCK_TYPE" => "catalog",
                                            "IBLOCK_ID" => $arRestIB["ID"],                                        
                                            "FILTER_NAME" => "arrFilter",
                                            "FIELD_CODE" => array(),
                                            "PROPERTY_CODE" => array("category", "subway","price", "sale_value"),
                                            "PRICE_CODE" => array(),
                                            "CACHE_TYPE" => "Y",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_GROUPS" => "N",
                                            "LIST_HEIGHT" => "5",
                                            "TEXT_WIDTH" => "20",
                                            "NUMBER_WIDTH" => "5",
                                            "SAVE_IN_SESSION" => "N"
                                    )
                            );?>
                        </noindex>                                                                                                                                  
                        <div class="clear"></div>
                    </div>
                </div>
        <!--<div class="top_baner">
            <div class="top_baner_block">
                <div class="left">
                    <?$APPLICATION->IncludeComponent(
					"bitrix:advertising.banner",
					"",
					Array(
						"TYPE" => "top_content_main_page",
						"NOINDEX" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0"
					),
				false
				);?>
                </div>
                <?
                $new = getKuponsCount($kuponIB["ID"],Array("PROPERTY_new_VALUE"=>"Да",">DATE_ACTIVE_TO"=>date("d.m.Y H:i:s")));
                $popular = getKuponsCount($kuponIB["ID"],Array("PROPERTY_popular_VALUE"=>"Да",">DATE_ACTIVE_TO"=>date("d.m.Y H:i:s")));
                ?>
                <div class="right" style="margin-right: 40px;margin-top:20px">
                    <a class="title" href="/content/kupon/?new=Y">Новинки</a> <sup><?=$new?></sup><br />
                    <a class="title" href="/content/kupon/?popular=Y">Популярное</a> <sup><?=$popular?></sup>
                </div>
                <div class="clear"></div>
            </div>            
        </div>-->
        <div id="content">