<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>   
    <div class="kupons left<?if($key%3==2):?> end<?endif?>">
        <div class="dates_metro">
            <div class="left">
                <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]." ".$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?>
                <?if ($arItem["ACTIVE_TO"]):?>
                    <?="&ndash; ".$arItem["DISPLAY_ACTIVE_TO_DAY"]." ".$arItem["DISPLAY_ACTIVE_TO_MONTH"]?>
                <?endif;?>
            </div>
            <div class="right metro_<?=CITY_ID?>"><?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]?></div>
            <div class="clear"></div>
        </div>
        <div class="favorites">
            <?
              $params = array();
              $params["id"] = $arItem["ID"];
              $params = addslashes(json_encode($params));            
            ?>
            <a class="another" href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a>
            <!--<div class="rating">
                <?for($i = 1; $i <= 5; $i++):?>
                    <div class="star<?if($i <= round($arItem["DISPLAY_PROPERTIES"]["ratio"]["DISPLAY_VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                <?endfor?>
                <div class="clear"></div>
            </div>-->
        </div>
        <div class="kupon_name">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </div>
        <div class="price">
            <?=GetMessage("R_SALE")?><span> <?=$arItem["PROPERTIES"]["sale"]["VALUE"]?>%</span> <?=GetMessage("R_ZA")?> <span><?=$arItem["PRICE"]["PRICE"]?></span> <span class="rouble">e</span>
        </div>
        <?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
        <div class="kupon_image_block">
            <div class="kupon_image">
                <?if ($arItem["PROPERTIES"]["kupon_dnya"]["VALUE"]=="Да"):?>
                    <div class="kupon_of_day">Скидка дня!</div>                
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["new"]["VALUE"]=="Да"):?>
                    <div class="new">новинка!</div>
                <?elseif ($arItem["PROPERTIES"]["popular"]["VALUE"]=="Да"):?>
                    <div class="new">популярное!</div>
                <?endif;?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" /></a>
            </div>
        </div>
        <?endif;?>        
        <div>
            Коментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)
        </div>
        <div class="grey" align="center">
            Купили <span><?=($arItem["PROPERTIES"]["quantity"]["VALUE"]+$arItem["PROPERTIES"]["delta"]["VALUE"])?></span> скидок. 
        </div>
    </div>
    <?if($key%3==2):?>
        <div class="clear"></div>   
    <?endif;?>
    <?if ($key == round(count($arResult["ITEMS"])/2)):?>
        <div class="kupon_long_baner">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "kupon_long",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0",
                            "CACHE_NOTES" => ""
                    ),
            false
            );?> 
        </div>
    <?elseif($key%3==2&&$key<(count($arResult["ITEMS"])-3)):?>
        <div class="border_line"></div>        
    <?endif;?>
<?endforeach;?>
<div class="clear"></div>        
<hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageRestCnt"] == 20 || !$_REQUEST["pageRestCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageRestCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=40'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageRestCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=60'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">60</a>')?>
        </div>
        <div class="right">
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            	<?=$arResult["NAV_STRING"]?>
            <?endif;?>
        </div>
    <div class="clear"></div>
    <br /><br />