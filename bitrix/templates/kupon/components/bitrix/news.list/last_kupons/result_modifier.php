<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!CModule::IncludeModule("catalog"))
    return false;
if (!CModule::IncludeModule("sale"))
    return false;
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arTmpDate = array();    
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arTmpDate = array();
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ITEMS"][$key]["ACTIVE_TO"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO_YEAR"] = $arTmpDate[2];
    // truncate name
    $arResult["ITEMS"][$key]["NAME"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["NAME"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //
    $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
    
    //price
    $arResult["ITEMS"][$key]["PRICE"] = CPrice::GetBasePrice($arItem["ID"]);
    $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]);    
    //date difference    
    $month = $days = $year = 0;
    $month = substr($arItem["ACTIVE_TO"],3,2); 
    $days = substr($arItem["ACTIVE_TO"],0,2); 
    $year = substr($arItem["ACTIVE_TO"],6,4);;     
    $arResult["ITEMS"][$key]["LAST_DAYS"] = ceil((mktime(0, 0, 0, $month, $days, $year) - time())/86400);
    // count restaurant reviews
    /*$rsPostComment = CBlogComment::GetList(
        Array("ID"=>"DESC"),
        Array(
            "BLOG_ID" => 3,
            "POST_ID" => $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["reviews"]["DISPLAY_VALUE"]
        ),
        false,
        false,
        Array()
    );
    $arResult["ITEMS"][$key]["REVIEWS_CNT"] = $rsPostComment->SelectedRowsCount();*/
}
?>