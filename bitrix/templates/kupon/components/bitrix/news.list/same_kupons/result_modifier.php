<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!CModule::IncludeModule("catalog"))
    return false;
if (!CModule::IncludeModule("sale"))
    return false;
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // truncate name
    $arResult["ITEMS"][$key]["NAME"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["NAME"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //
    $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
    
    //price
    $arResult["ITEMS"][$key]["PRICE"] = CPrice::GetBasePrice($arItem["ID"]);
    $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]);    
    if ($arResult["ITEMS"][$key]["PROPERTIES"]["COMMENTS_BIND"]["VALUE"])            
        $arResult["ITEMS"][$key]["RATIO"] = getReviewsRatio(57, $arResult["ITEMS"][$key]["PROPERTIES"]["COMMENTS_BIND"]["VALUE"]);

}
?>