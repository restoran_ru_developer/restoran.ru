<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>   
    <div class="similar_kupons">        
        <div class="kupon_name" style="height:auto;overflow: visible;margin-bottom:10px; ">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </div>
        <!--<div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="small_star<?if($i <= round($arItem["RATIO"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
        </div>        -->
        <div class="price">
            <?=GetMessage("R_SALE")?><span> <?=$arItem["PROPERTIES"]["sale"]["VALUE"]?>%</span> <?=GetMessage("R_ZA")?> <span><?=$arItem["PRICE"]["PRICE"]?></span> <span class="rouble">e</span>
        </div>
        <?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
        <div class="kupon_image_block">
            <div class="kupon_image">
                <?if ($arItem["PROPERTIES"]["kupon_dnya"]["VALUE"]=="Да"):?>
                    <div class="new">Скидка дня!</div>                
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["new"]["VALUE"]=="Да"):?>
                    <div class="new">новинка!</div>
                <?elseif ($arItem["PROPERTIES"]["popular"]["VALUE"]=="Да"):?>
                    <div class="new">популярное!</div>
                <?endif;?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img width="230" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" /></a>
            </div>
        </div>
        <?endif;?>        
        <div class="left">
            Коментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)
        </div>
        <div class="right">
            <a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>       
    <?if ($arItem!=end($arResult["ITEMS"])):?>
    <div class="dotted"></div>        
    <?endif;?>
<?endforeach;?>
<div class="clear"></div>        