<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?echo ShowError($arResult["ERROR_MESSAGE"]);?>
<h2><?=GetMessage("STB_ORDER_PROMT"); ?></h2>
<script>
$(document).ready(function(){
    $("#quantity").keyup(function(){
        $("#summ").html(<?=$arResult["ITEMS"]["AnDelCanBuy"][0]["PRICE"]?>*$("#quantity").val());
    });
    $("#quantity").change(function(){
        $.ajax({
          url: "/tpl/ajax/add2basket_kupon.php",
          type: "post",
          data: "q="+$("#quantity").val(),
          success: function(){
            
          }
        });
    });
});
</script>
<table width="100%" cellpadding="5">
	<?
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{
		?>
		<tr>
                    <td align="center"><input class="inputtext font18" id="quantity" maxlength="3" type="text" name="QUANTITY_<?=$arBasketItems["ID"] ?>" value="<?=$arBasketItems["QUANTITY"]?>" size="1" ></td>
                    <td class="another"> x </td>
                    <td nowrap class="font18"><?=$arBasketItems["PRICE"]?> <span class="rouble font18">e</span></td>
                    <td class="another"> = </td>
                    <td nowrap>
                        <span class="font30" id="summ"><?=$arResult["allSum"]?></span> <span class="rouble font30">e</span>                            
                    </td>
                    <td width="20"></td>
                    <td width="500">
                        <i><?=GetMessage("STB_ORDER_PROMT_PRICE"); ?></i>
                    </td>
		</tr>
		<?
		$i++;
	}
	?>
</table>