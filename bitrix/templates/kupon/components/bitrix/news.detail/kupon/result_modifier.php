<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $DB;
if (!CModule::IncludeModule("catalog"))
    return false;
if (!CModule::IncludeModule("sale"))
    return false;
if ($arResult["PROPERTIES"]["COMMENTS_BIND"]["VALUE"])            
    $arResult["RATIO"] = getReviewsRatio(57,$arResult["PROPERTIES"]["COMMENTS_BIND"]["VALUE"]);
$arResult["NEXT_POST"] = getCookeryPost("next",$arParams["IBLOCK_TYPE"],$arParams["IBLOCK_ID"], $ar_sec["IBLOCK_SECTION_ID"], $ar_sec["ID"]);                
$arResult["PREV_POST"] = getCookeryPost("prev",$arParams["IBLOCK_TYPE"],$arParams["IBLOCK_ID"], $ar_sec["IBLOCK_SECTION_ID"],$ar_sec["ID"]);                

if($arIBType = CIBlockType::GetByIDLang($arResult["IBLOCK_TYPE_ID"], LANG))   
    $arResult["IBLOCK_TYPE_NAME"] = htmlspecialcharsex($arIBType["NAME"]);

$rsUser = CUser::GetByID($arResult["SECTION"]["PATH"][0]["CREATED_BY"]);
if($arUser = $rsUser->GetNext())
    $arResult["AUTHOR_NAME"] = $arUser["NAME"];
// format date
$date = $DB->FormatDate($arResult["SECTION"]["PATH"][0]["DATE_CREATE"], 'YYYY-MM-DD HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
$arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
$arTmpDate = explode(" ", $arTmpDate);
$arResult["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
$arResult["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

$arTmpDate = array();
$arTmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_FROM"]);
$arResult["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
$arResult["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
$arResult["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
$arTmpDate = array();
$arTmpDateO = explode(".", $arResult["ACTIVE_TO"]);
$arResult["ACTIVE_TO_DAY"] = $arTmpDateO[0];
$arResult["ACTIVE_TO_MONTH"] = $arTmpDateO[1];
$arResult["ACTIVE_TO_YEAR"] = $arTmpDateO[2];


$arResult["DISPLAY_ACTIVE_TO"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ACTIVE_TO"], CSite::GetDateFormat()));
$arTmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_TO"]);
$arResult["DISPLAY_ACTIVE_TO_DAY"] = $arTmpDate[0];
$arResult["DISPLAY_ACTIVE_TO_MONTH"] = $arTmpDate[1];
$arResult["DISPLAY_ACTIVE_TO_YEAR"] = $arTmpDate[2];

$arResult["PRICE"] = CPrice::GetBasePrice($arResult["ID"]);
$arResult["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["PRICE"]["PRICE"]);        

$month = $days = $year = 0;
$month = substr($arResult["ACTIVE_TO"],3,2); 
$days = substr($arResult["ACTIVE_TO"],0,2); 
$year = substr($arResult["ACTIVE_TO"],6,4);;     
$arResult["LAST_DAYS"] = ceil((mktime(0, 0, 0, $month, $days, $year) - time())/86400);
$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);

$arRestIB = getArIblock("catalog", CITY_ID);
// get rest info
if(is_array($arResult["PROPERTIES"]["REST_BIND"]["VALUE"])) {
    foreach($arResult["PROPERTIES"]["REST_BIND"]["VALUE"] as $restBind) {
        $rsRest = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arRestIB["ID"],
                "ID" => $restBind,
            ),
            false,
            false,
            Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE")
        );
        while($arRest = $rsRest->GetNext()) {
            // get rest city name
            $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
            $arRestCity = $rsRestCity->GetNext();

            // get rest picture
            if(!$arRest["PREVIEW_PICTURE"]) {
                $restPic = CFile::ResizeImageGet($arRest["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
            } else {
                $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
            }

            // set rest info
            $arTmpRest["NAME"] = $arRest["NAME"];
            $arTmpRest["REST_PICTURE"] = $restPic;
            $arResult["REST_BIND_ARRAY"][$arRest["ID"]] = $arTmpRest;
        }
    }
} 
elseif($arResult["PROPERTIES"]["REST_BIND"]["VALUE"])
{
     $rsRest = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                /*"ACTIVE" => "Y",*/
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arRestIB["ID"],
                "ID" => $arResult["PROPERTIES"]["REST_BIND"]["VALUE"],
            ),
            false,
            false,
            Array("ID", "IBLOCK_ID", "NAME", "CODE", "PREVIEW_PICTURE")
        );
        while($arRest = $rsRest->GetNext()) {
            // get rest city name
            $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
            $arRestCity = $rsRestCity->GetNext();

            // get rest picture
            if(!$arRest["PREVIEW_PICTURE"]) {
                $restPic = CFile::ResizeImageGet($arRest["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
            } else {
                $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
            }

            // set rest info
            $arTmpRest["NAME"] = $arRest["NAME"];
            $arTmpRest["CODE"] = $arRest["CODE"];
            $arTmpRest["CITY"] = $arRestCity["NAME"];            
            $arTmpRest["REST_PICTURE"] = $restPic;
            $arResult["REST_BIND_ARRAY"][$arRest["ID"]] = $arTmpRest;
        }
}
/*if ($block["TAGS"])
    $arResult["TAGS"] = $block["TAGS"];*/
// get current date
$arResult["TODAY_DATE"] = CIBlockFormatProperties::DateFormat('j F', MakeTimeStamp(date('d.m.Y'), CSite::GetDateFormat()));

function get_file($matches)
{
    $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'bc',
        'size'=>'small',
        'type'=>'image',
        'alpha_level'=>'30',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
    );
    $photo = array();
    $photo = CFile::ResizeImageGet($matches[1],Array("height"=>512),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
    //return CFile::GetPath($matches[1]);
    return $photo["src"];
    //return "http://".SITE_SERVER_NAME."".CFile::GetPath($matches[1]);
    //return CFile::GetPath($matches[1]);
}
function get_file_wo_w($matches)
{    
    $photo = array();
    $photo = CFile::ResizeImageGet($matches[1],Array(),BX_RESIZE_IMAGE_PROPORTIONAL,true,Array());
    //return CFile::GetPath($matches[1]);
    return $photo["src"];
    //return "http://".SITE_SERVER_NAME."".CFile::GetPath($matches[1]);
    //return CFile::GetPath($matches[1]);
}

function get_file_av($matches)
{
    $photo = array();
    $photo = CFile::ResizeImageGet($matches[1],Array("width"=>150,"height"=>150),BX_RESIZE_IMAGE_EXACT,true,Array());
    return $photo["src"];
}
function get_rest($matches)
{
    global $resto;
    $arRestIB = getArIblock("catalog", CITY_ID);
    $rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            //"ACTIVE" => "Y",
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arRestIB["ID"],
            "ID" => $matches[1],
        ),
        false,
        false,
        Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","DETAIL_PICTURE", "DETAIL_PAGE_URL","PROPERTY_RATIO")
    );
    if($arRest = $rsRest->GetNext()) {
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"kitchen"));
        while($ar_props = $db_props->Fetch())
        {
            $kitchen["NAME"] = $ar_props["NAME"];
            $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
            $arCuisine = $rsCuisine->Fetch();
            $kitchen["VALUE"][] = $arCuisine["NAME"];
        }
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"average_bill"));
        if($ar_props = $db_props->Fetch())
        {
            $avb["NAME"] = $ar_props["NAME"];
            $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
            $arCuisine = $rsCuisine->Fetch();
            $avb["VALUE"][] = $arCuisine["NAME"];
        }
        //"PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill"
        //v_dump($arRest);
        // get rest city name
        $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
        $arRestCity = $rsRestCity->Fetch();

        // get rest picture
        if ($arRest["PREVIEW_PICTURE"])
             $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 148, 'height' => 98), BX_RESIZE_IMAGE_EXACT, true, Array());
        elseif ($arRest["DETAIL_PICTURE"])
             $restPic = CFile::ResizeImageGet($arRest["DETAIL_PICTURE"], array('width' => 148, 'height' => 98), BX_RESIZE_IMAGE_EXACT, true, Array());
        else
            $restPic["src"] = "/tpl/images/noname/rest_nnm.png"; 
        // get cuisine name
        $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
        $arCuisine = $rsCuisine->Fetch();

        // set rest info
        $arTmpRest["NAME"] = $arRest["NAME"];
        $arTmpRest["CITY"] = $arResult["IBLOCK"]["NAME"];
        $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
        $arTmpRest["REST_PICTURE"] = $restPic;
        $arTmpRest["URL"] = $arRest["DETAIL_PAGE_URL"];
        $arTmpRest["CUISINE"] = $kitchen;
        $arTmpRest["AVERAGE_BILL"] = $avb;
        $res = '<a href="'.$arTmpRest["URL"].'"><img class="indent" src="'.$arTmpRest["REST_PICTURE"]["src"].'" width="148" /></a><br />
                <a class="font14" href="'.$arTmpRest["URL"].'">'.$arTmpRest["NAME"].'</a>,<br />'.$arTmpRest["CITY"].'
                <p><div class="rating">';
        for($i = 0; $i < 5; $i++):
            if ($i < $arTmpRest["RATIO"]):
                $res .= '<div class="small_star_a" alt="'.$i.'"></div>';               
            else:
                $res .= '<div class="small_star" alt="'.$i.'"></div>';
            endif;
        endfor;
        $res .= '<div class="clear"></div></div>';
        $res .= '<b>'.$arTmpRest["CUISINE"]["NAME"].':</b> ';
        $res .= implode(", ",$arTmpRest["CUISINE"]["VALUE"]);
        $res .= '<br /><b>'.$arTmpRest["AVERAGE_BILL"]["NAME"].':</b> ';
        $res .= implode(", ",$arTmpRest["AVERAGE_BILL"]["VALUE"]);
        $res .= '<br /></p>';
        if(end($block["REST_BIND_ARRAY"]) != $rest):
            $res .= '<hr class="dotted" />';
        endif;
        $resto[] = Array("ID"=>$arRest["ID"],"NAME"=>$arRest["NAME"]);
    }
    else
    {
        $rsRest = CIBlockElement::GetByID($matches[1]);
        if($arRest = $rsRest->GetNext()) {
           
            //"PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill"
            //v_dump($arRest);
            // get rest city name
            $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
            $arRestCity = $rsRestCity->Fetch();
            
            $rsRestCity = CIBlockSection::GetByID($arRest["IBLOCK_SECTION_ID"]);
            $arSECCity = $rsRestCity->Fetch();

            
            $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"RATIO"));
            if($ar_props = $db_props->Fetch())
            {
                $arTmpRest["RATIO"] = $ar_props["VALUE"];
            }
            // get rest picture
            if ($arRest["PREVIEW_PICTURE"])
                $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 148, 'height' => 98), BX_RESIZE_IMAGE_EXACT, true, Array());
            elseif ($arRest["DETAIL_PICTURE"])
                $restPic = CFile::ResizeImageGet($arRest["DETAIL_PICTURE"], array('width' => 148, 'height' => 98), BX_RESIZE_IMAGE_EXACT, true, Array());
            else
                $restPic["src"] = "/tpl/images/noname/rest_nnm.png"; 

            // set rest info
            $arTmpRest["NAME"] = $arRest["NAME"];
            $arTmpRest["CITY"] = $arRestCity["NAME"];
            $arTmpRest["TYPE"] = $arSECCity["NAME"];
            $arTmpRest["REST_PICTURE"] = $restPic;
            $arTmpRest["URL"] = $arRest["DETAIL_PAGE_URL"];
            $res = '<a href="'.$arTmpRest["URL"].'"><img class="indent" src="'.$arTmpRest["REST_PICTURE"]["src"].'" width="148" /></a><br />
                    <a class="font14" href="'.$arTmpRest["URL"].'">'.$arTmpRest["NAME"].'</a>,<br />'.$arTmpRest["CITY"].'
                    <p><div class="rating">';
            for($i = 0; $i < 5; $i++):
                if ($i < $arTmpRest["RATIO"]):
                    $res .= '<div class="small_star_a" alt="'.$i.'"></div>';               
                else:
                    $res .= '<div class="small_star" alt="'.$i.'"></div>';
                endif;
            endfor;
            $res .= '<div class="clear"></div></div>';
            $res .= $arTmpRest["TYPE"];
            $res .= '<br /></p>';
            if(end($block["REST_BIND_ARRAY"]) != $rest):
                $res .= '<hr class="dotted" />';
            endif;
            //$resto[] = Array("ID"=>$arRest["ID"],"NAME"=>$arRest["NAME"]);
        }
    }
    return $res;
}
if ($arResult["IBLOCK_TYPE_ID"]=="afisha")
    $arResult["DETAIL_TEXT"] = preg_replace_callback("/#FID_([0-9]+)#/","get_file_wo_w",$arResult["DETAIL_TEXT"]);
else
    $arResult["DETAIL_TEXT"] = preg_replace_callback("/#FID_([0-9]+)#/","get_file",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = preg_replace_callback("/#FID_PR([0-9]+)#/","get_file_av",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = preg_replace_callback("/#REST_([0-9]+)#/","get_rest",$arResult["DETAIL_TEXT"]);
if (!$arResult["PROPERTIES"]["COMMENTS"]["VALUE"])
    $arResult["PROPERTIES"]["COMMENTS"]["VALUE"] = 0;

$pic = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array('width' => 720, 'height' => 480), BX_RESIZE_IMAGE_EXACT, true, Array());
$arResult["DETAIL_PICTURE"]["SRC"] = $pic["src"];
?>