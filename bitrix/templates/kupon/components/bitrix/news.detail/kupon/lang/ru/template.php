<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["PREV_POST"] = "Предыдущая скидка";
$MESS["NEXT_POST"] = "Следующая скидка";
$MESS["CUISINE"] = "Кухня";
$MESS["AVERAGE_BILL"] = "Средний счет";
$MESS["TAGS_TITLE"] = "Теги";
$MESS["R_ADD2FAVORITES"] = "В книгу рецептов";
$MESS["R_PRINT"] = "Распечатать";
$MESS["R_COMMENTS"] = "Комментарии";
$MESS["SUBSCRIBE"] = "подписаться";
$MESS["BYU"] = "купить";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "день";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "дня";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "дней";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Далее";
$MESS["R_ADD2FAVORITES"] = "В избранное";
$MESS["R_SALE"] = "Скидка";
$MESS["R_ZA"] = "за";
$MESS["SHOW_CNT_TITLE"] = "Выводить по";
?>