<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
$(document).ready(function(){    
    $("#minus_plus_buts").on("click",".plus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
  			if(parseInt(otvet)){
                                $(".all_plus_minus").html($(".all_plus_minus").html()*1+1);
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				
  			}
  		});
		return false;
	});
	
	$("#minus_plus_buts").on("click",".minus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet){
  			if(parseInt(otvet)){                            
                                $(".all_plus_minus").html($(".all_plus_minus").html()*1-1);
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				}
  			
  		});
		return false;
	});
  	
  	$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
  		$("#minus_plus_buts").html(otvet);
  	});  
    
    
    $(".articles_photo").galery(); 
})
</script>
<div id="content">
    <div class="left" style="width:720px;">
        <div class="statya_section_name">
            <h6><?=$arResult["IBLOCK_TYPE_NAME"]?></h6>
            <div class="statya_nav">
                <div class="left">
                    <?if ($arResult["RECEPT"]["PREV_POST"]):?>
                        <a class="statya_left_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["PREV_POST"]["CODE"]?>/"><?=GetMessage("PREV_POST")?></a>
                    <?else:?>
                        <a class="statya_left_arrow no_border" href="javascript:void(0)" title="Предыдущих рецептов нет"><?=GetMessage("PREV_POST")?></a>
                    <?endif;?>                    
                </div>
                <div class="right">
                    <?if ($arResult["RECEPT"]["NEXT_POST"]):?>
                        <a class="statya_right_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["NEXT_POST"]["CODE"]?>/"><?=GetMessage("NEXT_POST")?></a>
                    <?else:?>
                        <a class="statya_right_arrow no_border" href="javascript:void(0)" title="Следующих рецептов нет"><?=GetMessage("NEXT_POST")?></a>                         
                    <?endif;?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="black_hr margin15"></div>
        <h1><?=$arResult["NAME"]?></h1>
        <!--<div class="rating left" style="margin-bottom:10px; margin-right:20px;">
            <?for($i==0;$i<$arResult["RATIO"]["RATIO"];$i++):?>
                <div class="star_a"></div>
            <?endfor?>
            <?for($i;$i<5;$i++):?>
                <div class="star"></div>
            <?endfor;?>
        </div>        
        <div class="left">
            Комментарии: (<a href="#comments" class="anchor another"><?=($arResult["RATIO"]["COUNT"])?$arResult["RATIO"]["COUNT"]:"0"?></a>)
        </div>
        <div class="clear"></div>-->
        <a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br />
        <i><?=$arResult["PREVIEW_TEXT"]?></i>
        <br /><br />
        <div class="left recept_prop_block1 font14">
            <?=$arResult["DISPLAY_ACTIVE_FROM_DAY"]." ".$arResult["DISPLAY_ACTIVE_FROM_MONTH"]?>
                <?if ($arResult["ACTIVE_TO"]):?>
                    <?="&ndash; ".$arResult["DISPLAY_ACTIVE_TO_DAY"]." ".$arResult["DISPLAY_ACTIVE_TO_MONTH"]?>
            <?endif;?>                        
            <br /><br />
            <?if ($arResult["ACTIVE_TO"]):?>
                До конца продаж: <span><?=$arResult["LAST_DAYS"]?></span> <?=pluralForm(intval($arResult["LAST_DAYS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?>,
                <script>
                    var rightNow = new Date(); //берем текущую дату
                    //вставляем дату например 31.12 12:00
                    var date1 = new Date(rightNow.getFullYear(), <?=$arResult["ACTIVE_TO_YEAR"]?>, <?=$arResult["ACTIVE_TO_DAY"]?>, <?=$arResult["ACTIVE_TO_MONTH"]?>, 0, 0, 0);
                    var d = Math.floor((date1-rightNow)/ (1000 * 60 * 60 * 24));
                    var h = Math.floor((date1-rightNow-d*(1000*60*60*24))/ (1000 * 60 * 60)); 
                    
                    var m = Math.floor((date1-rightNow-d*(1000*60*60*24)-h*(1000 * 60 * 60))/ (1000 * 60));
                    
                    var s1 = Math.ceil((date1-rightNow-d*(1000*60*60*24)-h*(1000*60*60)-m*(1000*60))/1000);
                    function tick()
                    {
                        var h_d = 0;
                        var m_d = 0;
                        var s_d = 0;
                        if (s1==0) s1=59;
                        if (m==0) m=59;
                        s1--;
                        if (s1==0) m--;
                        s_d = s1;
                        m_d = m;
                        h_d = h;
                        if (s_d<10) s_d = "0"+s1;
                        if (m<10) m_d = "0"+m;
                        if (h<10) h_d = "0"+h;                        
                        $("#hours").html(h_d);
                        $("#minutes").html(m_d);
                        $("#seconds").html(s_d);
                        setTimeout("tick()",1000);
                    }
                    $(document).ready(function(){
                        setTimeout("tick()",1000);
                    });
                </script>
                 <span id="hours"></span> : <span id="minutes"></span> : <span id="seconds"></span>
            <?endif;?>
            <br /><br />
            Уже купили <span><?=($arResult["PROPERTIES"]["quantity"]["VALUE"]+$arResult["PROPERTIES"]["delta"]["VALUE"])?></span> скидок. 
            <?
              $params = array();
              $params["id"] = $arResult["SECTION"]["PATH"][1]["ID"];
              $params = addslashes(json_encode($params));            
            ?>
            <!--<div class="recept_book">
                <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a>
            </div>
            <div class="print">
                <a href="?print=Y" target="_blank"><?=GetMessage("R_PRINT")?></a>
            </div>-->
        </div>
        <div class="right recept_prop_block2 font14" style="color:#FFF">
            <?=GetMessage("R_SALE")?><span class="font30"> <?=$arResult["PROPERTIES"]["sale"]["VALUE"]?>%</span> &nbsp;<?=GetMessage("R_ZA")?> &nbsp;<span class="font30"><?=$arResult["PRICE"]["PRICE"]?></span> <span class="rouble font30">e</span>
            <br /><br />
            <a href="/spb/kupon/order.php?ID=<?=$arResult["ID"]?>" class="no_border"><input type="button" value="Купить" class="light_button"/></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/spb/kupon/order.php?ID=<?=$arResult["ID"]?>&present=Y" style="color:#FFF" >Купить в подарок</a>
            <br /><Br />
            <?
              $params = array();
              $params["id"] = $arResult["ID"];
              $params = addslashes(json_encode($params));            
            ?>            
            <a class="another" href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a>            
        </div>
        <div class="clear"></div>
        <br />        
        <div class="detail_kupon_image">
            <?if ($arResult["PROPERTIES"]["kupon_dnya"]["VALUE"]=="Да"):?>
                <div class="kupon_of_day">Скидка дня!</div>                
            <?endif;?>
            <?if ($arResult["PROPERTIES"]["new"]["VALUE"]=="Да"):?>
                <div class="new">новинка!</div>
            <?elseif ($arResult["PROPERTIES"]["popular"]["VALUE"]=="Да"):?>
                <div class="new">популярное!</div>
            <?endif;?>
            <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" />
        </div>        
        <br />
        <?if ($arResult["DETAIL_TEXT"]):?>
        <h2>Условия акции</h2>        
        <div class="kupon_conditions">
            <?=$arResult["DETAIL_TEXT"]?>
        </div>
        <br />
        <?endif;?>
        <?if(is_array($arResult["REST_BIND_ARRAY"])):?>
            <h2>Подробнее о предложении и ресторане</h2>
            <div class="left" style="width:170px;">
                <?foreach ($arResult["REST_BIND_ARRAY"] as $rest):?>
                    <img src="<?=$rest["REST_PICTURE"]["src"]?>" /><Br /><Br />
                    <a href="/<?=CITY_ID?>/detailed/restaurants/<?=$rest["CODE"]?>"><?=$rest["NAME"]?></a>,<Br />
                    <?=$rest["CITY"]?><Br /><Br />
                <?endforeach;?>
                <div class="metro_<?=CITY_ID?>"><?=$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]?></div>
            </div>
            <div class="right kupon_rest" style="width:550px;">
                <?=$arResult["DISPLAY_PROPERTIES"]["RESTORAN_PREVIEW"]["DISPLAY_VALUE"]?>
            </div>        
            <div class="clear"></div>
        <?endif;?>
        <?if ($arResult["PROPERTIES"]["PHOTOS"]["VALUE"][0]):?>   
            <div class="left kupon_photo">
                <?if(count($arResult["PROPERTIES"]["PHOTOS"]["VALUE"]) == 1):?>
                    <div class="img">
                        <img src="<?=CFile::GetPath($arResult["PROPERTIES"]["PHOTOS"]["VALUE"][0])?>" width="715" />
                        <p><i><?=$arResult["PROPERTIES"]["PHOTOS"]["DESCRIPTION"][0]?></i></p>
                    </div>
                <?else:?>
                    <script type="text/javascript">
                        $(document).ready(function(){
                           $(".kupon_photo").galery();
                        });
                    </script>
                    <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["PHOTOS"]["VALUE"])?><a class="next browse right"></a></div>
                    <div class="clear"></div>
                    <div class="img">
                        <img src="<?=CFile::GetPath($arResult["PROPERTIES"]["PHOTOS"]["VALUE"][0])?>" width="720" />
                        <p><i><?=$arResult["PROPERTIES"]["PHOTOS"]["DESCRIPTION"][0]?></i></p>
                    </div>
                    <div class="special_scroll">
                        <div class="scroll_container">
                            <?foreach($arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $keyPic=>$pic):?>
                                <div class="item<?if($keyPic == 0):?> active<?endif?>">
                                    <img src="<?=CFile::GetPath($pic)?>" alt="<?=$pic["description"]?>" align="bottom" />
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                <?endif?>
            </div>
            <div class="clear"></div>    
        <?endif;?>
        <?
			if( strlen(trim($arResult["TAGS"])) > 0 )
			{  
				$arrTags = explode(',', $arResult["TAGS"]);
				$count = count($arrTags);
				$i = 0;
				$t="";

			
				foreach($arrTags as $value):
				   $i++;
				   $value = trim($value);
				   $t .= '<a class="another" href="/search/?tags='.str_replace(' ', '+', $value).'&search_in=kupons">'.$value.'</a>';
				   if($i != $count) $t .= ', ';
				endforeach;
        ?>
        <p><?=GetMessage("TAGS_TITLE")?>: <?=$t?></p>
		<? 
			}
		?>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#comments" class="anchor another"><?=($arResult["COMMENTS"]["COUNT"])?$arResult["COMMENTS"]["COUNT"]:"0"?></a>)</div>       
            
            <!--<div class="right" style="padding-right:15px;"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <!--<div class="right" style="padding-right:15px;"><input type="button" class="button" value="<?=GetMessage("SUBSCRIBE")?>" /></div>-->
            <!--<div class="right" style="padding-right:15px;"><a href="/content/kupon/order.php?ID=<?=$arResult["ID"]?>" class="no_border"><input type="button" class="light_button" value="<?=GetMessage("BYU")?>" /></a></div>-->
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
            <div id="minus_plus_buts"></div>
            <div class="clear"></div>
        </div>
        <br /><Br />
        
                   <?$APPLICATION->IncludeComponent(
                   	"restoran:comments",
                   	"with_rating",
                   	Array(
                            "IBLOCK_TYPE" => "reviews",
                            "ELEMENT_ID" => $arResult["ID"],
                            "IS_SECTION" => "N",
                            "ADD_COMMENT_TEMPLATE" => "with_rating"
                   	),
                   false
                   );?>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2881483, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <div class="black_hr margin15"></div>
        <div class="statya_section_name">
            <h6><?=$arResult["IBLOCK_TYPE_NAME"]?></h6>
            <div class="statya_nav">
                <div class="left">
                    <?if ($arResult["RECEPT"]["PREV_POST"]):?>
                        <a class="statya_left_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["PREV_POST"]["CODE"]?>/"><?=GetMessage("PREV_POST")?></a>
                    <?else:?>
                        <a class="statya_left_arrow no_border" href="javascript:void(0)" title="Предыдущих рецептов нет"><?=GetMessage("PREV_POST")?></a>
                    <?endif;?>                    
                </div>
                <div class="right">
                    <?if ($arResult["RECEPT"]["NEXT_POST"]):?>
                        <a class="statya_right_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["NEXT_POST"]["CODE"]?>/"><?=GetMessage("NEXT_POST")?></a>
                    <?else:?>
                        <a class="statya_right_arrow no_border" href="javascript:void(0)" title="Следующих рецептов нет"><?=GetMessage("NEXT_POST")?></a>                         
                    <?endif;?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "content_article_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
    <div class="right" style="width:240px">        
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
        <br /><br />
        <div class="top_block">Похожие скидки</div>
        <?
        global $arrFilterSim;
        $arrFilterSim["!ID"] = $arResult["ID"];
        ?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "same_kupons",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "kupons",
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => 5,
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilterSim",
                    "FIELD_CODE" => Array("ACTIVE_TO"),
                    "PROPERTY_CODE" => array("COMMENTS","ratio", "reviews", "subway","kupon_dnya","new","popular"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Скидки",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "kupon_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "PRICE_CODE" => "BASE"
            ),
        false
        );?>
        <div class="top_block">Рекомендуем</div>
        <?
        global $arrFilterRec;
        $arrFilterRec["PROPERTY_RECOMENDED_VALUE"] = "Да";
        $arrFilterRec["!ID"] = $arResult["ID"];
        ?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "same_kupons",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "kupons",
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => 3,
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilterRec",
                    "FIELD_CODE" => Array("ACTIVE_TO"),
                    "PROPERTY_CODE" => array("COMMENTS","ratio", "reviews", "subway","kupon_dnya","new","popular"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Скидки",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "kupon_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "PRICE_CODE" => "BASE"
            ),
        false
        );?>
        <?/*$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "interview",
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "NEWS_COUNT" => 6,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrOtherInterview",
                        "FIELD_CODE" => Array("ACTIVE_TO"),
                        "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "kupon_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "PRICE_CODE" => "BASE"
                ),
            false
        );*/?>
        <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
    <div class="clear"></div>
</div>