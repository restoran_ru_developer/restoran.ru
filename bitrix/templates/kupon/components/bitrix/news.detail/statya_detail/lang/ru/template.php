<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["NEXT_POST"] = "Предыдущий пост";
$MESS["PREV_POST"] = "Следующий пост";
$MESS["CUISINE"] = "Кухня";
$MESS["AVERAGE_BILL"] = "Средний счет";
$MESS["TAGS_TITLE"] = "Теги";
$MESS["R_ADD2FAVORITES"] = "В избранное";
$MESS["R_COMMENTS"] = "Комментарии";
$MESS["SUBSCRIBE"] = "подписаться";
$MESS["UNSUBSCRIBE"] = "отписаться";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_VK"] = "Вконтакте";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["READ_A"] = "Читайте также";

$MESS["R_BOOK_TABLE2"] = "Забронировать<br />столик";
$MESS["R_ORDER_BANKET2"] = "Заказать<br />банкет";
$MESS["R_BOOK_TABLE"] = "Забронировать столик";
$MESS["R_ORDER_BANKET"] = "Заказать банкет";
$MESS["R_BUFFET"] = "Заказать фуршет";
$MESS["R_IN"] = "в";
$MESS["R_AT"] = "в";
$MESS["R_ON"] = "на";
$MESS["R_FOR"] = "на";
$MESS["R_PERSONS"] = "персоны";
$MESS["R_BOOK"] = "ЗАБРОНИРОВАТЬ";

?>