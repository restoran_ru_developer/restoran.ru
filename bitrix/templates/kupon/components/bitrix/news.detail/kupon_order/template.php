<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<h2><?= GetMessage("YOUR_KUPON") ?></h2>
<div class="left">    
    <div class="detail_kupon_image" style="height: 118px; overflow: hidden;">
        <? if ($arResult["PROPERTIES"]["kupon_dnya"]["VALUE"] == "Да"): ?>
            <div class="new">Скидка дня!</div>                
        <? endif; ?>
        <? if ($arResult["PROPERTIES"]["new"]["VALUE"] == "Да"): ?>
            <div class="new">новинка!</div>
        <? elseif ($arResult["PROPERTIES"]["popular"]["VALUE"] == "Да"): ?>
            <div class="new">популярное!</div>
        <? endif; ?>
        <img src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" width="200" />
    </div>        
</div>
<div class="right" style="width:465px">
    <p><?= $arResult["NAME"] ?></p>    
    <p><?= $arResult["DISPLAY_ACTIVE_FROM_DAY"] . " " . $arResult["DISPLAY_ACTIVE_FROM_MONTH"] ?>
        <? if ($arResult["ACTIVE_TO"]): ?>
            <?= "&ndash; " . $arResult["DISPLAY_ACTIVE_TO_DAY"] . " " . $arResult["DISPLAY_ACTIVE_TO_MONTH"] . " " . $arResult["DISPLAY_ACTIVE_TO_YEAR"] ?>
        <? endif; ?>                           
    </p>
    <br />
    <a href="#" class="another buy-for-present" style="height: 40px; float: left; display: block; position: relative;">Купить в подарок</a>    
</div>
<div class="clear"></div>

<div class="present-email-container" style="float: left <? if($_REQUEST['present'] !== 'Y'){ echo ';display: none ';}?> ;width: 100%">
    <div class="dotted"></div>
    <div style="width: 100%;">Введите E-Mail друга:</div>
    <input  class="inputtext font18" id="ORDER_PROP_22_COPY" type="text" value="" name="ORDER_PROP_22_COPY" style="width: 660px; margin-bottom: 8px;" onkeyup="$('#ORDER_PROP_22').val($(this).val());">
</div>
<div class="clear"></div>