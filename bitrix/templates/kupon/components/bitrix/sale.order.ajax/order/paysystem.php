<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h2><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></h2>
<br />
<table width="100%" cellpadding="0" style="display:none">
	<?
	if ($arResult["PAY_FROM_ACCOUNT"]=="Y")
	{
		?>
		<tr>
		<td colspan="2">
		<input id="PAY_CURRENT_ACCOUNT" type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">
		<input type="checkbox" name="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT" value="Y"<?if($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y") echo " checked=\"checked\"";?> onChange="submitForm()"> <label for="PAY_CURRENT_ACCOUNT"><b><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT")?></b></label><br />
		<?=GetMessage("SOA_TEMPL_PAY_ACCOUNT1")?> <b><?=$arResult["CURRENT_BUDGET_FORMATED"]?></b>, <?=GetMessage("SOA_TEMPL_PAY_ACCOUNT2")?>
		<br /><br />
		</td></tr>
		<?
	}
	?>
	<?
	foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
	{
            if(count($arResult["PAY_SYSTEM"]) == 1)
            {
                ?>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
                        <b><?=$arPaySystem["NAME"];?></b>
                    </td>
                </tr>
                <?
            }
	}
	?>
</table>
<script>
    $(document).ready(function(){
       $(".payment .left").click(function(){
           $(".payment .left").removeClass("active");
           $(this).addClass("active");
           $("#payment_type").val($(this).attr("pay"));
           if ($("#payment_type").val($(this).attr("pay"))==5)
               $("#PAY_CURRENT_ACCOUNT").val("Y");
       });
    });
</script>
<div class="payment">
    <div class="left" pay="1">   
        <div class="el">
            Webmoney<br />
            Яндекс. Деньги<Br />
            Деньги@Mail.ru
        </div>
    </div>
    <div class="left" pay="2">
        <div class="sms">СМС</div>
    </div>        
    <div class="left" pay="3">
        <div class="bk">Банковская карта</div>
    </div>       
    <div class="left" pay="4">
        <div class="alfa">Альфа-Клик</div>
    </div>
    <div class="left"  pay="5">
        <div class="ball">С баланса Restoran.ru</div>
    </div>    
</div>
<div class="clear"></div>
<input type="hidden" name="payment_type" id="payment_type" value="0" />