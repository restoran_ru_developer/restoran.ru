<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>
    $(document).ready(function(){
        $('#PERSON_TYPE_1').click();
        
        $(".payment .left").live('click', function() {
            $(".payment .left").removeClass("active");
            $(this).addClass("active");
            $("#ORDER_PROP_21").val($(this).attr("pay"));
        });
        
        $(".ball").live('click', function() {
            if ($('#PAY_CURRENT_ACCOUNT').attr('checked') !== 'checked'){
                $('#PAY_CURRENT_ACCOUNT').click();
                $('#ID_PAY_SYSTEM_ID_9').removeAttr('checked');
            }
        });
        
        $(".el").live('click', function() {
            if ($('#ID_PAY_SYSTEM_ID_9').attr('checked') !== 'checked'){
                $('#ID_PAY_SYSTEM_ID_9').click();
                $('#PAY_CURRENT_ACCOUNT').removeAttr('checked');
            }
        });
        
        $(".sms").live('click', function() {
            if ($('#ID_PAY_SYSTEM_ID_9').attr('checked') !== 'checked'){
                $('#ID_PAY_SYSTEM_ID_9').click();
                $('#PAY_CURRENT_ACCOUNT').removeAttr('checked');
            }
        });
        
        $(".bk").live('click', function() {
            if ($('#ID_PAY_SYSTEM_ID_9').attr('checked') !== 'checked'){
                $('#ID_PAY_SYSTEM_ID_9').click();
                $('#PAY_CURRENT_ACCOUNT').removeAttr('checked');
            }
        });
        
        $(".alfa").live('click', function() {
            if ($('#ID_PAY_SYSTEM_ID_9').attr('checked') !== 'checked'){
                $('#ID_PAY_SYSTEM_ID_9').click();
                $('#PAY_CURRENT_ACCOUNT').removeAttr('checked');
            }
        });
        
        $(".buy-for-present").live('click', function() {
            $('.present-email-container').toggle();
            $('#ORDER_PROP_22_COPY').val('');
            $('#ORDER_PROP_22').val('');
            return false;
        });
        
    });
</script>
<a name="order_fform"></a>
<div id="order_form_div" class="order-checkout">
    <NOSCRIPT>
    <div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
    </NOSCRIPT>
    <?
    //v_dump($arResult);
    if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N") {
        if (!empty($arResult["ERROR"])) {
            foreach ($arResult["ERROR"] as $v)
                echo ShowError($v);
        } elseif (!empty($arResult["OK_MESSAGE"])) {
            foreach ($arResult["OK_MESSAGE"] as $v)
                echo ShowNote($v);
        }

        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
    } else {
        if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
            if (strlen($arResult["REDIRECT_URL"]) > 0) {
                ?>
                <script>
                    <!--
                    window.top.location.href='<?= CUtil::JSEscape($arResult["REDIRECT_URL"]) ?>';
                    //-->
                </script>
                <?
                die();
            } else {
                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
            }
        } else {
            ?>
            <script>
                <!--
                function submitForm(val)
                {
                    if(val != 'Y') 
                        BX('confirmorder').value = 'N';
        			
                    var orderForm = BX('ORDER_FORM');
        			
                    BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
                    BX.submit(orderForm);

                    return true;
                }
                function SetContact(profileId)
                {
                    BX("profile_change").value = "Y";
                    submitForm();
                }
                //-->
            </script>
        <?
        if ($_POST["is_ajax_post"] != "Y") {
            ?><form action="" method="POST" name="ORDER_FORM" id="ORDER_FORM">
            <?= bitrix_sessid_post() ?>
                    <div id="order_form_content">
            <?
        } else {
            $APPLICATION->RestartBuffer();
        }
        if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
            foreach ($arResult["ERROR"] as $v)
                echo ShowError($v);
            ?>
                        <script>
                            top.BX.scrollToNode(top.BX('ORDER_FORM'));
                        </script>
                        <?
                    }

                    if (count($arResult["PERSON_TYPE"]) > 1) {
                        ?>
                        <span style="display: none;">
                            <b><?= GetMessage("SOA_TEMPL_PERSON_TYPE") ?></b>
                            <table class="sale_order_full_table">
                                <tr>
                                    <td>
                        <?
                        foreach ($arResult["PERSON_TYPE"] as $v) {
                            ?><input type="radio" id="PERSON_TYPE_<?= $v["ID"] ?>" name="PERSON_TYPE" value="<?= $v["ID"] ?>"<? if ($v["CHECKED"] == "Y") echo " checked=\"checked\""; ?> onClick="submitForm()"> <label for="PERSON_TYPE_<?= $v["ID"] ?>"><?= $v["NAME"] ?></label><br /><?
            }
            ?>
                                        <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>">
                                    </td></tr></table>
                            <br /><br />
                        </span>
                        <?
                    }
                    else {
                        if (IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0) {
                            ?>
                            <input type="hidden" name="PERSON_TYPE" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>">
                            <input type="hidden" name="PERSON_TYPE_OLD" value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>">
                                            <?
                                        } else {
                                            foreach ($arResult["PERSON_TYPE"] as $v) {
                                                ?>
                                <input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?= $v["ID"] ?>">
                                <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $v["ID"] ?>">
                                                <?
                                            }
                                        }
                                    }

                                    //include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
                                    ?>			
                    <?
                    if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d") {
                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
                    } else {
                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
                    }
                    ?>			
                    <br /><br />
                    <?
                    //include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");

                    if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
                        echo $arResult["PREPAY_ADIT_FIELDS"];
                    ?>
                    <?
                    if ($_POST["is_ajax_post"] != "Y") {
                        ?>
                    </div>
                    <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                    <input type="hidden" name="profile_change" id="profile_change" value="N">
                    <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                    <input type="hidden" name="ORDER_PROP_21" id="ORDER_PROP_21" value="">
                    <input type="hidden" name="ORDER_PROP_22" id="ORDER_PROP_22" value="">
                    <br /><br />
                    <div align="right">
                        <input type="button" class="light_button" name="submitbutton" onClick="submitForm('Y');" value="<?=GetMessage("SOA_TEMPL_BUTTON")?>" style="padding-left:60px; padding-right:60px;">
                    </div>
                </form>
                        <? if ($arParams["DELIVERY_NO_AJAX"] == "N"): ?>
                    <script language="JavaScript" src="/bitrix/js/main/cphttprequest.js"></script>
                    <script language="JavaScript" src="/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js"></script>
                        <? endif; ?>
                        <?
                    }
                    else {
                        ?>
                <script>
                    top.BX('confirmorder').value = 'Y';
                    top.BX('profile_change').value = 'N';
                </script>
                        <?
                        die();
                    }
                }
            }
            ?>
</div>