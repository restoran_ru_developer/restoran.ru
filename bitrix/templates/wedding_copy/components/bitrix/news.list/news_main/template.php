<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="spec_news">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="galery_news" style="<?=($key==0)?"display:block":""?>">
            <div class="date_p left">
                <span><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span><br />
                <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?><br />
                <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
            </div>
            <div class="left" style="width:415px;">
                <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?=$arItem['NAME']?></a></div>
                <p><?=$arItem['PREVIEW_TEXT']?></p>  
            </div>
            <div class="clear"></div>
        </div>
    <?endforeach;?>
    <div class="scroll_arrows"><a class="prev browse left" ></a><span class="scroll_num">1</span>/<span class="all_num"><?=count($arResult["ITEMS"])?></span><a class="next browse right" ></a></div>
</div>