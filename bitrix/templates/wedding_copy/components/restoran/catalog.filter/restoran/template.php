<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="/<?=CITY_ID?>/articles/letnie_verandy/" method="get" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search">
            <div class="filter_box">
            <?
            $p=0;
            $ar_cusel = Array(2,3,1);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
                if ($arProp["CODE"]!='subway'&&$arProp["CODE"]!='subway_dostavka'&&$arProp["CODE"]!='out_city')
                {
            ?>
            
                <div class="filter_block" id="filter<?=$prop_id?>">
                    <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                    <div class="title"><?=$arProp["NAME"]?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi<?=$ar_cusel[$p]?>">
                        <?$i=0;?>
                        <?if ($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]):?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <?if (in_array($key, $_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]])):?>
                                    <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                    <?$i++;
                                    if ($i>2) break;
                                    ?>
                                <?endif;?>
                            <?endforeach;?>
                        <?else:?>
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <li><a href="javascript:void(0)" class="white"><?=$val?></a></li>
                                <?$i++;
                                if ($i>2) break;
                                ?>
                            <?endforeach;?>
                        <?endif;?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow js another"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"right":"70px","top":"2px", "width":"355px"}});
                            var params = {
                                changedEl: "#multi<?=$ar_cusel[$p]?>",
                                scrollArrows: true,
                                visRows:10
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                        <div class="title" align="right"><?=$arProp["NAME"]?></div>
                        <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="10">
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>        
            <?             
                    $p++;
                }
                elseif ($arProp["CODE"]=="subway")
                {                
                ?>
                <div class="filter_block" id="filter3" style="margin-top:10px;position: relative">
                    <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                        <div class="title"><?=$arProp["NAME"]?></div>              
                        <script>
                            var link = "";                            
                            function load_metro(obj)
                            {
                                if (link != $(obj).attr('href'))
                                {
                                    link = $(obj).attr('href');
                                    $(".filter_popup_4").css({'padding':'25px','padding-right':'0px','right':'70px','top':'-5px', 'width':'815px','position':'absolute','z-index':10000,'background': 'url(/tpl/images/popup_bg.png)'});
                                    $(".filter_popup_4").load(link, function(){$(".filter_popup_4").fadeIn(300);});                                          
                                }
                                else
                                {
                                    $(".filter_popup_4").fadeIn(300);
                                }
                            }
                            $(document).ready(function(){
                                $(".filter_popup_4").click(function(e){                                
                                    e.stopPropagation();
                                    //return false;
                                });
                                $("#fil321").find("a").click(function(e){
                                    e.stopPropagation();
                                    //return false;
                                });
                                $('html,body').click(function() {
                                    $(".filter_popup_4").fadeOut(300);
                                });
                                $(window).keyup(function(event) {
                                if (event.keyCode == '27') {
                                    $(".filter_popup_4").fadeOut(300);
                                }
                                });
                            });
                        </script>
                            <ul class="filter_category_block" id="fil321">
                                <li><a href="/tpl/ajax/metro.php?q=abc&<?=bitrix_sessid_get()?>" class="filter_arrow js another" onclick="load_metro(this); return false;"><?=GetMessage("METRO_ABC")?></a></li>
                                <li><a href="/tpl/ajax/metro.php?q=thread&<?=bitrix_sessid_get()?>" class="filter_arrow js another" onclick="load_metro(this); return false;"><?=GetMessage("METRO_VETKA")?></a></li>
                                <!--<li><a id="near_metro" href="javascript:void(0)" class="filter_arrow js another" onclick="get_location();">Ближайшие к вам</a></li>-->
                            </ul>                            
                        <!--<div class="filter_popup filter_popup_3 popup" style="display:none"></div>                    -->
                        <div class="filter_popup filter_popup_4" style="display:none"></div>
                        <?/*foreach($arResult["SUBWAY"] as $subway):?>
                            <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                        <?endforeach*/?>
                        <?if ($arResult["OUT_CITY"]):?>
                            <div id="filter<?=$arResult["OUT_CITY"]["ID"]?>" style="position:relative">
                                <div class="title" style="margin-top:18px; line-height:24px;"><a href="javascript:void(0)" class="filter_arrow js another"><?=GetMessage("MORE_".$arResult["OUT_CITY"]["CODE"])?></a></div>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$arResult["OUT_CITY"]["ID"]?>").popup({"obj":"filter<?=$arResult["OUT_CITY"]["ID"]?>","css":{"right":"70px","top":"-5px", "width":"355px"}});
                                        var params = {
                                            changedEl: "#multi1",
                                            scrollArrows: true,
                                            visRows:10
                                        }
                                        cuSelMulti(params);
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$arResult["OUT_CITY"]["ID"]?>" style="display:none">
                                    <div class="title" align="right"><?=GetMessage("MORE_".$arResult["OUT_CITY"]["CODE"])?></div>
                                    <select multiple="multiple" class="asd" id="multi1" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arResult["OUT_CITY"]["CODE"]?>][]" size="10">
                                        <?foreach($arResult["OUT_CITY"]["VALUE_LIST"] as $key=>$val):?>
                                            <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arResult["OUT_CITY"]["CODE"]]))?"selected":""?>><?=$val?></option>
                                        <?endforeach?>
                                    </select>
                                    <br /><br />
                                    <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                                </div>
                            </div>
                        <?endif;?>
                            <?$p++;?>
                    </div>
                <?
                }                
            }
?>      
                <br /><Br />
                <input style="width:128px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
        </div>
    </div>
    <div class="clear"></div>
</form>
