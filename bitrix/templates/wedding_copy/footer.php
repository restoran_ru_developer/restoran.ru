<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
            <div class="clear"></div>            
        </div>       
    </div>
    </div>
    <div id="footer">            
        <div class="footer_content">
            <div class="to_top"><?=GetMessage("GO_TOP")?></div>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom_menu",
                    Array(
                            "ROOT_MENU_TYPE" => "bottom_menu_".CITY_ID,
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array()
                    ),
            false
            );?>
            <div class="clear"></div>
            <br /><br />
            <div class="left">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_copyright_1.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="left">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_copyright_2.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="right">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_dev_company.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
<script type="text/javascript">

 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-33504724-2']);
 _gaq.push(['_trackPageview']);

 (function() {
   var ga = document.createElement('script'); ga.type =
'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' :
'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ga, s);
 })();

</script>
</body>
</html>