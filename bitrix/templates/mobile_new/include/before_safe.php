<?php
if (($_REQUEST["apply"]=="Применить"||$_REQUEST["save"]=="Сохранить"))
{
    //Берем ID свойств
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"RESTOURANT"));
    if($prop_fields = $properties->GetNext())
    {
        $property_restoran_id = $prop_fields["ID"];
        $property_restoran_iblock_id = $prop_fields["LINK_IBLOCK_ID"];
    }
//

    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"lat"));
    if($prop_fields = $properties->GetNext())
    {
        $property_lat_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"lon"));
    if($prop_fields = $properties->GetNext())
    {
        $property_lon_id = $prop_fields["ID"];
    }


    if ($property_restoran_id&&$property_restoran_iblock_id)
    {
        foreach($_POST["PROP"][$property_restoran_id] as $rest_vals)
        {
            if ($rest_vals)
            {
                if (is_array($rest_vals)&&count($rest_vals["VALUE"])>0)
                {
                    $rest_id = $rest_vals["VALUE"];
                }
                else
                    $rest_id = $rest_vals;
            }
        }
        if ($rest_id)
        {
            if ($property_lat_id)
            {
                //Забираем у ресторана район
                $lat = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "lat"));
                while ($ob = $res->GetNext())
                {
                    $lat[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_lat_id] = $lat;
            }

            if ($property_lon_id)
            {
                //Забираем у ресторана район
                $lon = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "lon"));
                while ($ob = $res->GetNext())
                {
                    $lon[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_lon_id] = $lon;
            }
//
        }
    }
    $PROP = $_POST["PROP"];
}
?>
