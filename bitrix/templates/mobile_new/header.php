<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET;?>" />
        <meta name="viewport" content="width=device-width, target-densityDpi=device-dpi, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="HandheldFriendly" content="true"/>       
        
        <title><?$APPLICATION->ShowTitle();?></title>
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="user-scalable" content="no">


        <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/images/touch/favicon.ico?360000" type="image/x-icon" />
        <!-- Apple Touch Icons -->
        <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon.png?360000" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-57x57.png?360000" />
        <link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-72x72.png?360000" />
        <link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-114x114.png?360000" />
        <link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-144x144.png?360000" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-60x60.png?360000" />
        <link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-120x120.png?360000" />
        <link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-76x76.png?360000" />
        <link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-152x152.png?360000" />
        <!-- Windows 8 Tile Icons -->
        <meta name="msapplication-square70x70logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/smalltile.png?360000" />
        <meta name="msapplication-square150x150logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/mediumtile.png?360000" />
        <meta name="msapplication-wide310x150logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/widetile.png?360000" />
        <meta name="msapplication-square310x310logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/largetile.png?360000" />
        
        <!-- Chrome Add to Homescreen -->
        <link rel="shortcut icon" sizes="196x196" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-152x152.png?360000">

        <!-- For iOS web apps. Delete if not needed. https://github.com/h5bp/mobile-boilerplate/issues/94 -->
        <!--
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="Restoran.ru">
        -->

<!--        <script src="//code.jquery.com/jquery-1.10.2.js"></script>-->

        <!--        -1-->

        <?
        $APPLICATION->ShowCSS();

        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/h5bp.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/components/components.css');

        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/main.css?1');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/ratio.css?1');

        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/swiper.css?1');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.animation.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.icons.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.scroller.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.widget.css');
        ?>

        <!--        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>-->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js" async></script>
        <script>
            setTimeout(function(){
                WebFont.load({
                    google: {
                        families: ['Roboto+Condensed:400,300,700:latin']
                    }
                });
            },200);
        </script>

        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery-2.1.3.min.js?360000" ></script>
<!--        <script src="--><?//=SITE_TEMPLATE_PATH?><!--/scripts/jquery-1.11.1.min.js?360000" ></script>-->

        <?$APPLICATION->ShowHeadStrings()?>

        <?$APPLICATION->ShowHeadScripts()?>

        <?$APPLICATION->ShowMeta("keywords")?>
        <?//$APPLICATION->ShowMeta("description")?>
        <meta name="description" content="Мобильная версия Ресторан.ру">



        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/touch.js?360000" async ></script>

        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.zepto.js?360000"  ></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.core.js?360000"  ></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.js?360000"  ></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.select.js?360000"  ></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.scroller.js?360000"  ></script>

        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/markerclusterer.js?360000" async></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.cookie.js?360000" async></script>

        <!--    перенесено из футера    -->
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/pushstate.js?360002" async></script>
        <!--    перенесено из футера    -->

        <?
        include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/Mobile_Detect.php");
        $detect = new Mobile_Detect();
        ?>
        <?if (substr_count($_SERVER["HTTP_USER_AGENT"],"Version")&&substr_count($_SERVER["HTTP_USER_AGENT"],"Android")&&!substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")&&!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
            <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/android.css?1');?>        
            <script>
                var android_fail = 1;
            </script>
        <?else:?>
            <script>
                var android_fail = 0;
            </script>
        <?endif;?>

        <?//if((substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")  || $USER->IsAdmin())):?>
            <script async >
                $(function(){
                    <?if(!$USER->IsAdmin())://  исчезающий input при фокусе?>

                        <?if((!substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone"))):?>
                            var winScrollTop = 0;
                            $('main').on('focus', 'input#restoran_input_field', function(){
                                //$(window).scrollTop(0);
                                //, top:'0'
    //                            $(window).scrollTop(0);

                                $(this).parents('.popup.filter').css({position:'absolute'});
                                $('body').height($(window).height());

                                //$('body').css('overflow','hidden');
                                var winScrollTop = $(window).scrollTop();
                                console.log(winScrollTop,'winScrollTop');

                                $(window).bind('scroll',scroll_to_top);
    //                            $('.popup.filter').offset({top:scroll_to_top+100, left:0})
    //                            $("#overflow").show();
                            });
                            $('main').on('blur', 'input#restoran_input_field',function(){
                                //,top:'auto'
                                $(this).parents('.popup.filter').css({position:'fixed'});
                                $('body').css('height','auto');
                                //$('body').css('overflow','auto');
                                var winScrollTop = $(window).scrollTop();
                                $(window).unbind('scroll',scroll_to_top); //Выключить отмену прокрутки
                            });

                            function scroll_to_top() {
                                $(window).scrollTop(winScrollTop);
                            }
                        <?endif?>
                    <?else:?>
                        var winScrollTop = 0;
                        $('main').on('focus', 'input#restoran_input_field', function(){
                            //$(window).scrollTop(0);
                            //, top:'0'
                            $(this).parents('.popup.filter').css({position:'absolute'});
                            $('body').height($(window).height());

                            $(window).scrollTop(0);
                            //$('body').css('overflow','hidden');
                            var winScrollTop = $(window).scrollTop();
                            $(window).bind('scroll',top_fixed_position);
                            $("#overflow").show();
                        });
                        $('main').on('blur', 'input#restoran_input_field',function(){
                            //,top:'auto'
                            $(this).parents('.popup.filter').css({position:'fixed'});
                            $('body').css('height','auto');
                            //$('body').css('overflow','auto');
                            var winScrollTop = $(window).scrollTop();
                            $(window).unbind('scroll',top_fixed_position); //Выключить отмену прокрутки
                        });


                        function top_fixed_position() {
                            $(window).scrollTop(winScrollTop);
                        }
                    <?endif;?>
                });
            </script>
        <?//endif?>

</head>
<body>
        <div class='panel'><?$APPLICATION->ShowPanel();?></div>

        <?
        require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH . "/functions.php");?>

        <?if(substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")&&$APPLICATION->get_cookie('HIDE_APP_BANNER')!='Y'):?>
            <div class="floating-banner-to-app-store">
                <a href="https://appsto.re/ru/yYEdfb.i" target="_blank" class="floating-link-to-install-app">установить</a>
                <span class="icon-close"></span>
            </div>
        <?endif?>
        <header class="app-bar promote-layer <?=$APPLICATION->get_cookie('HIDE_APP_BANNER')!='Y'&&substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")?'app-banner-exist':''?>">
            <div class="app-bar-container myhead">
                <button class="menu"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu.png"></button>
                <a href="/" class="ajax logo"><img src='<?=SITE_TEMPLATE_PATH?>/images/logo.png'></a>
                <section class="app-bar-actions">
                    <?//if($USER->IsAdmin()):?><a href="/search/" class="right_btn search-top-trigger">Поиск</a><?//endif?>
                    <a href="/map/" class="right_btn ajax">Карта</a>
                </section>
            </div>
        </header>

        <nav class="navdrawer-container promote-layer">
            <div class="scroll-menu-block">
                <div class="city-drop-down-wrapper">
                    <ul>
                        <?
                        $APPLICATION->IncludeComponent(
                            "restoran:city.selector",
                            "active_city",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_URL" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "360000000001",
                                "CACHE_NOTES" => "new3212236624228",
                                "CACHE_GROUPS" => "N"
                            ),
                            false
                        );
                        ?>
                    </ul>
                </div>

                <ul>
                    <li><a class="search-menu-link" href="/search/" >Поиск ресторана</a></li>
                    <li><a class="ajax icon-arrow-right2" href="/">Рядом со мной</a></li>

                    <?if(CITY_ID=="spb" || CITY_ID=="msk"):?>
                    <li><a class="ajax icon-arrow-right2" id="popular_link" href="/popular/" >Популярные</a></li>
<!--                    --><?//if(CITY_ID=='msk'||CITY_ID=='spb'):?><!--<li><a class="ajax icon-arrow-right2" href="/letnie-verandy/">Летние веранды рядом</a></li>--><?//endif;?>
<!--                    --><?//if(CITY_ID=='msk'||CITY_ID=='spb'):?><!--<li><a class="ajax icon-arrow-right2" href="/new-year-corp/">Новогодние банкеты</a></li>--><?//endif;?>
<!--                    --><?//if(CITY_ID=='msk'||CITY_ID=='spb'):?><!--<li><a class="ajax icon-arrow-right2" href="/new-year-night/">Новогодняя ночь</a></li>--><?//endif;?>
                    <li><a class="ajax icon-arrow-right2" href="/index.php?page=1&by=asc&arrFilter_pf%5Bfeatures%5D%5B%5D_flag=2392461&set_filter=Y">Круглосуточно</a></li>
<!--                        <li><a class="ajax icon-arrow-right2" href="/post/">Великий пост</a></li>-->
                    <?endif;?>

                    <li><a href="/bookinghistory/" class="icon-arrow-right2">Избранное</a></li>

                    <?if(CITY_ID=="spb" || CITY_ID=="msk" || CITY_ID=="rga" || CITY_ID=="urm"):?>
                        <li><a class="ajax icon-arrow-right2" href="/booking/">Бронировать</a></li>
                    <?endif?>
<!--                    <li><a href="/bookinghistory/">Мои бронирования</a></li>-->
                    <?
//                    if($USER->IsAdmin()):?><!--<li><a href="/top100/" class="icon-arrow-right2 ajax">Top-100</a></li>--><?//endif?>
                    <li ><a class="icon-arrow-right2" href="http://www.restoran.ru/?from_mobile=Y">Полная версия сайта</a></li>
                </ul>
            <?if (CITY_ID=="spb"):?>
                <a href="tel:+78127401820" class="left-slide-menu-phone-link">+7 812 740 18 20</a>
            <?elseif(CITY_ID=="msk"):?>
                <a href="tel:+74959882656" class="left-slide-menu-phone-link">+7 495 988 26 56</a>
            <?elseif(CITY_ID=="rga" || CITY_ID=="urm"):?>
                <a href="tel:+37166103106" class="left-slide-menu-phone-link">+371 661 031 06</a>
            <?endif;?>
<!--                <a class="full-version-link" href="http://www.restoran.ru/?from_mobile=Y">Полная версия сайта</a>-->
            </div>
        </nav>

        <div id="mm">
            <ul>
                    <li><a class="ajax" href="/search/">Поиск ресторана</a></li>
                <?if(CITY_ID=="spb" || CITY_ID=="msk"):?>
                    <li><a class="ajax" href="/popular/">Популярные</a></li>
                <?endif?>
                    <li><a class="ajax" href="/">Рядом со мной</a></li>
                    <li><a class="ajax" href="/booking/">Бронировать</a></li>
                    <li><a href="/bookinghistory/">Избранное</a></li>
            </ul>
        </div>

        <main id="main"
            <?
            $app_class = $APPLICATION->get_cookie('HIDE_APP_BANNER')!='Y'&&substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")?'app-banner-padding':'';
            ?>
            <?if($APPLICATION->GetCurDir()=="/map/"):
                echo "class='map $app_class' id='map'";
            elseif(trim($APPLICATION->GetCurDir())=="/auth/"):
                echo "class='grad'";
            else:
                echo "class='$app_class'";
            endif;?>   >

            <?
            if($APPLICATION->get_cookie('HIDE_APP_SLIDER')!='Y'&&substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")&&!preg_match('/detail\.php/',$APPLICATION->GetCurPage())&&!preg_match('/\/booking\//',$APPLICATION->GetCurPage())&&!preg_match('/\/map\//',$APPLICATION->GetCurPage())&&!preg_match('/\/city_select\//',$APPLICATION->GetCurPage())):?>
                <div class="mobile-banner">
                    <a class="install-link-to-app-store" href="https://appsto.re/ru/yYEdfb.i" target="_blank">установить</a>
                    <div class="skip-this-banner">Пропустить</div>
                </div>
            <?endif?>


            <?if ($_REQUEST["ajax"]):?>
                <div id="ajax">
                    <?if($APPLICATION->get_cookie('HIDE_APP_SLIDER')!='Y'&&substr_count($_SERVER["HTTP_USER_AGENT"],"iPhone")&&!preg_match('/detail\.php/',$APPLICATION->GetCurPage())&&!preg_match('/\/booking\//',$APPLICATION->GetCurPage())&&!preg_match('/\/map\//',$APPLICATION->GetCurPage())&&!preg_match('/\/city_select\//',$APPLICATION->GetCurPage())):?>
                        <div class="mobile-banner">
                            <a class="install-link-to-app-store" href="https://appsto.re/ru/yYEdfb.i" target="_blank">установить</a>
                            <div class="skip-this-banner">Пропустить</div>
                        </div>
                    <?endif?>
            <? endif; ?>
                <?if(!preg_match('/detail\.php/',$APPLICATION->GetCurPage())):?>
                    <div class="detail-container"></div>
                    <div class="another-container">
                <?endif?>
<?
FirePHP::getInstance()->info($APPLICATION->get_cookie("CONTEXT"),'cook context');
if($_REQUEST["CONTEXT"]=="Y")
    if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
if($APPLICATION->get_cookie("CONTEXT")=="Y") $_REQUEST["CONTEXT"]="Y";
?>