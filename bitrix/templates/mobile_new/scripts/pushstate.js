var old_doc_content, list_scroll_position;
window.addEventListener("load", function(e) {
    setTimeout(function() {
        window.addEventListener('popstate', function(e){
            var href =  location.pathname+location.search;

            console.log(e);

            if (history && history.pushState&&!android_fail) {
                $("body").addClass("historypushed");

                //$("#overflow").show();
                hide_show_overflow(true);
                $(".bubblingG").show();
                $("main").addClass("blur");
                if (href.indexOf("detail")>=0)
                    $("#bubbling_text").text("Загружаем анкету ресторана...");
                else if (href=="/")
                    $("#bubbling_text").text("Загружаем ближайшие рестораны...");
                else
                    $("#bubbling_text").text("Загружаем страницу...");


                if(href.indexOf("detail")==-1&&$('.detail-container:empty').length==0&&$('.another-container').length>0){
                    $('#main .another-container').show();
                    $('#main .detail-container').hide();

                    hide_show_overflow();
                    $(".bubblingG").hide();
                    $("main").removeClass("blur");
                    if (href.indexOf("detail") >= 0)
                        history.replaceState(null, null, href);
                    else
                        history.pushState(null, null, href);
                    $("title").html(old_doc_content.find("#title").val());
                    if (href.indexOf("detail") >= 0) {
                    }
                    else {
                        $(".myhead").html(old_doc_content.find(".myhead").html())
                        menuclick();
                    }
                    if (href == "/map/") {
                        $("main").addClass("map");
                        $(".myhead .right_btn:not(.search-top-trigger)").html("Список").attr("href", "/").addClass("ajax");
                    }
                    else {
                        $("main").removeClass("map");
                    }
                    $(window).scroll(function(){
                        scroll_load();
                    });
                    setTimeout(function(){
                        $('body').scrollTop(list_scroll_position);
                    },100)

                    if($('.mobile-banner').length){
                        $('.mobile-banner').show();
                    }
                }
                else {
                    $.get(href, {ajax: "Y"}, function (data) {
                        if (href != "/")
                            $(window).unbind("scroll");

                        $("main").html($(data).find("#ajax"));



                        //$("#overflow").hide();
                        hide_show_overflow();
                        $(".bubblingG").hide();
                        $("main").removeClass("blur");
                        if (href.indexOf("detail") >= 0)
                            history.replaceState(null, null, href);
                        else
                            history.pushState(null, null, href);
                        $("title").html($(data).find("#title").val());
                        if (href.indexOf("detail") >= 0) {
                        }
                        else {
                            $(".myhead").html($(data).find(".myhead").html())
                            menuclick();
                        }
                        if (href == "/map/") {
                            $("main").addClass("map");
                            $(".myhead .right_btn:not(.search-top-trigger)").html("Список").attr("href", "/").addClass("ajax");
                        }
                        else {
                            $("main").removeClass("map");
                        }

                        if($('.mobile-banner').length){
                            $('.mobile-banner').height($(window).height()).css({'margin-top':'-'+$('#main').css('padding-top'),'margin-bottom':$('#main').css('padding-top')});
                            $('.skip-this-banner').on('click',function(){
                                $("html, body").animate({scrollTop: $(window).height() }); //крутим враппер
                            })
                        }
                    });

                }

            }
            else {

            }
        }, false);
    }, 0);
}, false);

$(function(){
    setOverflowD();

    $("body").on("click","a.ajax",function(e){
        var _this = $(this);
        var href = $(this).attr("href");
        setOverflowD();
        //if (href!="/map/")
        //{

        if(window.location.pathname!=href && href!=""){
            if (history && history.pushState&&!android_fail) {
                e.preventDefault();
                $("body").addClass("historypushed");
                //$("#overflow").show();
                hide_show_overflow(true);
                $(".bubblingG").show();
                $("main").addClass("blur");
                if (href.indexOf("detail")>=0)
                    $("#bubbling_text").text("Загружаем анкету ресторана...");
                if (href.indexOf("map/detail")>=0)
                    $("#bubbling_text").text("Загружаем карту...");
                if (href=="/")
                    $("#bubbling_text").text("Загружаем ближайшие рестораны...");


                $.get( href,{ajax:"Y"}, function( data ) {

                    list_scroll_position = $("body").scrollTop();
                    if (href != "/")
                        $(window).unbind("scroll");
                    $("body").scrollTop(0);


                    if(href.indexOf("detail")>=0&&href.indexOf("map")==-1){
                        old_doc_content = $('html').clone();
                        $(".detail-container").html($(data).find("#ajax"));
                        $(".detail-container").show();
                        $('#main .another-container').hide();
                        $('.mobile-banner').hide();
                    }
                    else {
                        $("main").html($(data).find("#ajax"));

                        if($('.mobile-banner').length){
                            $('.mobile-banner').show();
                            $('.mobile-banner').height($(window).height()).css({'margin-top':'-'+$('#main').css('padding-top'),'margin-bottom':$('#main').css('padding-top')});
                            $('.skip-this-banner').on('click',function(){
                                $("html, body").animate({scrollTop: $(window).height() }); //крутим враппер
                            })
                        }
                    }


                    $('.ok').hide();
                    //$("#overflow").hide();
                    hide_show_overflow();
                    $(".bubblingG").hide();
                    $("main").removeClass("blur");
                    history.pushState(null, null, href);
                    $("title").html($(data).find("#title").val());
                    if (href.indexOf("detail") >= 0 || href.indexOf("booking") >= 0) {

                    }
                    else {
                        $(".myhead").html($(data).find(".myhead"));
                        menuclick();
                    }
                    if (href == "/map/") {
                        $("main").addClass("map");
                        $(".myhead .right_btn:not(.search-top-trigger)").html("Список").attr("href", "/").addClass("ajax");
                    }
                    else {
                        $("main").removeClass("map");
                    }
                    if (href != "/auth/") {
                        $("main").removeClass("grad");
                    }
                    else {
                        $("main").addClass("grad");
                    }
                    $(".need_select_city").show();
                });
//                            $( "#main" ).load(href +" #ajax", {ajax:"Y"},function (data){
//                                if (href!="/")
//                                    $(window).unbind("scroll");
//                                else
//                                    $(window).scroll(function(){
//                                        scroll_load();
//                                    });
//                                $("#overflow").hide();
//                                $(".bubblingG").hide();
//                                $("main").removeClass("blur");
//                                history.pushState({foo: 'bar'}, $("#title").val(), href);                                                        
//                                $("title").html($(data).find("#title").val());                                
//                            });
            }else{

            }
        }
        //}
    });

    $("main").on("submit","#search_form",function(e){
        var _this = $(this);
        var params = $(this).serialize()+"&ajax=Y";
        var href = $(this).attr("action");

        if (href != "") {
            if (history && history.pushState && !android_fail) {
                e.preventDefault();
                $("body").addClass("historypushed");
                //$("#overflow").show();
                hide_show_overflow(true);
                $("#bubbling_text").text("Загружаем результаты поиска...");
                $(".bubblingG").show();
                $.get( href+'?'+params,{ajax:"Y"}, function( data ) {
                    //$("#overflow").hide();
                    hide_show_overflow();
                    $(".bubblingG").hide();
                    $("body").scrollTop(0);
                    $("main").html($(data).find("#ajax"));
                    $("main").removeClass("blur");
                    history.pushState({foo: 'bar'}, $("#title").val(), href + "?" + params);
                    $("title").html($(data).find("#title").val());
                });

            } else {

            }
        }
        return false;
    });

    $("main").on("submit","#booking",function(e){
        if (null!=$("#booking").length)
        {
            if ($("#phone").val().replace(/\D/g,'').length<1)
            {
                //alert("Телефон должен содержать 10 цифр (без 8).\nНапример: 9217774565");
                alert("Заполните поле Телефон");
                return false;
            }
            //if (!$("#name").val())
            //{
            //    alert("Заполните поле имя,\n чтобы мы знали как к Вам обратиться");
            //    return false;
            //}
            if (!$("#count").val()||$("#count").val().replace(/\D/g,'')==0)
            {
                alert("Введите кол-во человек");
                return false;
            }
            expr = /(.+) (.+)/;
            if (!$("#name").val()||!expr.test($("#name").val()))
            {
                alert("Введите свое имя и фамилию,\n чтобы мы знали как к Вам обратиться");
                return false;
            }
            if (!$("#count").val())
            {
                alert("Введите кол-во человек");
                return false;
            }
            if (!$("#restoran-id").val())
            {
                alert("Выберите ресторан");
                return false;
            }

            $(this).find('input[type="submit"]').attr('disabled',true);

        }
    });


    $("main").on('focus',$("#restoran"),function(){
        if (null!=$(".restoran_suggest ul"))
        {
            $(".restoran_suggest").show();
        }
    });

    $("main").on('click',"a.suggest-item",function(){
        $( ".restoran_suggest" ).hide();
        $("#restoran").val($(this).text());
        $("#restoran-name").val($(this).text());
        $("#restoran-id").val($(this).data("value"));
        $('input[name="rest_title"]').val($(this).text());
    });
});

function setOverflowD()
{
    //$(document).height()
    $("#overflow").css("height",'100%');//.css("width",$(window).width())
}