//    how to get
var directionsDisplay;
var directionsService;
var map;
var scrollPosition;
var ajax_load;

function hide_show_overflow(show){
    // how it works: http://stackoverflow.com/questions/10238084/ios-safari-how-to-disable-overscroll-but-allow-scrollable-divs-to-scroll-norma/14244680#14244680
    if(show == undefined||show == false||show == ''){
        $("#overflow").hide();
        $(document).unbind("scroll touchmove");//touchstart  mousewheel
    }
    else {
        $("#overflow").show();

        $(document).bind("scroll touchmove", stop_scroll);//scroll touchstart touchmove mousewheel

        //$('body').on('touchstart','.scroll-menu-block', function(e){//
        //    if (e.currentTarget.scrollTop === 0) {
        //        e.currentTarget.scrollTop = 1;
        //    } else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
        //        e.currentTarget.scrollTop -= 1;
        //    }
        //});

        $('body').on('touchmove', '.scroll-menu-block, .questions-overflow-wrap', function(e){//
            //console.log(e.changedTouches[0].clientY,'clientY')
            //console.log(e.changedTouches[0].pageY,'pageY')
            //console.log(e.changedTouches[0].screenY,'screenY')

            //console.log($(this)[0].scrollTop,'scrollTop');
            //console.log($(this)[0].scrollHeight-$(this)[0].offsetHeight,'minus');
            if($(this)[0].scrollHeight > $(this).innerHeight()) {   //  для блоков, где это актуально
            //if(($(this)[0].scrollTop<($(this)[0].scrollHeight-$(this)[0].offsetHeight))) {//$(this)[0].scrollTop==0||
            //    console.log('stop it');
                //e.preventDefault();
                e.stopPropagation();
            }

            //else {
            //    console.log('let it go');
            //    e.stopPropagation();
            //}
        });

        $('#overflow, .scroll-menu-block').on('swipeleft',function(event){
            event.stopImmediatePropagation();
            closeMenu();
            $('.popup.filter').hide();
            console.log('swipe left');
        })
    }
}
function stop_scroll(event) {
    event.preventDefault();
    //if($(event.target).attr('id')=='overflow'){
    //    event.preventDefault();
    //}
    //else {
    //    $(document).scrollTop(scrollPosition[1]);
    //}
}

function initialize_map_how_get(sessionLat,sessionLon) {
    //if(!sessionLat||!sessionLon){
    //    sessionLat = coord[1];
    //    sessionLon = coord[0];
    //}
    directionsDisplay = new google.maps.DirectionsRenderer();
    var mapSenter = new google.maps.LatLng(sessionLat, sessionLon);
    var mapOptions = {
        zoom:9,
        center: mapSenter
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    directionsDisplay.setMap(map);
    console.log('init map');
}

function calcRoute(endLat,endLon,selectedMode) {
    $(".bubblingG").show();
    $("#bubbling_text").text("Строим маршрут...");

    var start = new google.maps.LatLng(sessionLat, sessionLon);
    var end = new google.maps.LatLng(endLat, endLon);
    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.TravelMode[selectedMode]
    };
    directionsService = new google.maps.DirectionsService();
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            $(".bubblingG").hide();
            $("#bubbling_text").text("");
        }
        else {
            $(".bubblingG").hide();
            $("#bubbling_text").text("");
        }
    });
}
//    how to get

function show_filter(e){
    e.preventDefault();
    e.stopPropagation();
    $('.popup.filter').show();
    //$("#overflow").show();
    hide_show_overflow(true);
}

$(function(){

    $("main").on("click",".how-to-get-button",function() {
        $('.how-to-get-icon-wrapper.active').removeClass('active');
        $(this).find('.how-to-get-icon-wrapper').toggleClass('active');
        return false;
    });
    $("main").on("click",".how-to-get-icon-wrapper li > span",function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.map-canvas-wrapper').show();

        hide_show_overflow(true);
        //$("#overflow").show();

        google.maps.event.trigger(map, "resize");
        map.setCenter(new google.maps.LatLng(sessionLat, sessionLon));

        calcRoute($(this).attr('this_lat'),$(this).attr('this_lon'),$(this).attr('this_mode'));
        return false;
    });

    $("main").on("click",".close-map-canvas",function() {
        //$("#overflow").hide();
        hide_show_overflow();
        $(".popup").hide();
        $(".bubblingG").hide();
        $("#bubbling_text").text("");
    });

    $(".need_select_city .close").on("click",function() {
        //$("#overflow").hide();
        hide_show_overflow();
        $(".popup").hide();
        $(".bubblingG").hide();
        $("#bubbling_text").text("");
    });

    $("main").on("click",".stars_new li",function() {
//        if(!$(this).parents(".popup").hasClass("popup"))
//            $("#review").focus();
        var count = $(this).index()+1;
        var i = 1;
        $("#input_ratio").attr("value",count);
        $(this).parent().find("li").each(function(){
            if (i<=count)
                $(this).attr("class","active");
            else
                $(this).attr("class","");
            i++;
        })
    });
    $("#main").on('click',".btn.filter",function(e){
        show_filter(e);
    });
    //$("#main").on('click',".filter-trigger-wrapper",function(e){
    //    show_filter(e);
    //});



    $(".filter *").click(function(e){
        var clicked = $(e.target);
        if(clicked.parent().hasClass('btn')){
            show_filter(e);
        }
        else {
            e.stopPropagation();
        }
    });

    $("main").click(function(){
        $(".rlist").removeClass("blur");
        $('.popup.filter').hide();
        $('.restoran_suggest').hide();
        $('.how-to-get-icon-wrapper.active').removeClass('active');
    });
    $("main").on("click",".close",function(e){
        $('.items-detail').removeClass('blur');
        $('header').removeClass('blur');
        $('.popup.review').hide();
    });



    $("#overflow").click(function(){
        $(".popup").hide();
    });

    $(document).on('swiperight',function(event){
        event.stopImmediatePropagation();
        toggleMenu();
        console.log('swipe');
    })

    $('.mobile-banner').height($(window).height()).css({'margin-top':'-'+$('#main').css('padding-top'),'margin-bottom':$('#main').css('padding-top')});

    $('.skip-this-banner').on('click',function(){
        $.get( "/ajax/hideAppBanner.php",{appSliderHide:"Y"}, function( data ) {
            $("html, body").animate({scrollTop: $(window).height() }); //крутим враппер
        });

    })

    $('.icon-close').on('click',function(){
        $.get( "/ajax/hideAppBanner.php",{appBannerHide:"Y"}, function( data ) {
            $('.floating-banner-to-app-store').animate({'top':'-'+$('.floating-banner-to-app-store').height()},1000);
            $('.app-banner-exist').animate({'top':0},1000);
            $('.app-banner-padding').animate({'padding-top':47},1000);
            $('.mobile-banner').animate({'margin-top':-47,'margin-bottom':47},1000);
        });
    })

    $('.question').on('click',function(){
        $(this).find('label').click();
    })

});


$(function(){
    'use strict';
    
    var navdrawerContainer = document.querySelector('.navdrawer-container');
    var appbarElement = document.querySelector('.app-bar');
    var menuBtn = document.querySelector('.menu');   

    var main = document.querySelector('main');
    function closeMenu() {
        $(".app-bar").removeClass("open");
         $(".navdrawer-container").removeClass("open");
//        appbarElement.classList.remove('open');
//        navdrawerContainer.classList.remove('open');
//        $("#overflow").hide();
        hide_show_overflow();
        $("html,body").removeClass("overflow");        
        return false;
    }

    function toggleMenu() {
        $("#overflow").toggle();
        if($("#overflow").is(":visible")){
            hide_show_overflow(true);
        }
        else {
            hide_show_overflow();
        }

        $(".app-bar").toggleClass('open');
        $(".navdrawer-container").toggleClass("open");
//        appbarElement.classList.toggle('open');
//        navdrawerContainer.classList.toggle('open');
        $("html,body").toggleClass("overflow");

        if($('.all-wrap').length>0){
            $('.navdrawer-container.promote-layer.open').height()<$(window).height()?$('.navdrawer-container.promote-layer.open').height($(window).height()):'';
            $('.all-wrap').height($('.navdrawer-container.promote-layer.open').height());
            $('.all-wrap').toggleClass('open');
        }

        return false;
    }


    //main.addEventListener('ontouchstart', closeMenu);
    //main.addEventListener('click', closeMenu);
    menuBtn.addEventListener('click', toggleMenu);
    navdrawerContainer.addEventListener('click', closeMenu);    
    $("#overflow").click(function(){
        closeMenu();
    });
    navdrawerContainer.addEventListener('ontouchstart', closeMenu);
//    navdrawerContainer.addEventListener('click', function (event) {
//        if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
//            closeMenu();
//        }
//    });

   //$('a.dropdown-trigger').on('click', function(){
   //    $(this).parent().toggleClass('open');
   //    return false;
   //});

    $(document).on('click',".search-menu-link",function(e){
        closeMenu();
        setTimeout(function(){
            show_filter(e);
            //setTimeout(function(){
            //    $('#restoran_input_field').focus()
            //},200)
        },800)
        e.preventDefault();
    });
    $(document).on('click',".search-top-trigger",function(e){
        var now_href =  location.pathname+location.search;
        if (now_href.indexOf("detail")==-1&&$('.btn.filter').length>0){
            show_filter(e);
            e.preventDefault();
        }
    });
});
$(document).on('click', '[data-toggle=favorite]', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var _this = $(this);
    if (!$(this).data("restoran"))
    {
        alert("Произошла ошибка, попробуйте позже")
        return false;
    }

    console.log($(this).data("restoran"));


    var params = {"restoran":$(this).data("restoran"), 'name':'MY_MOBILE_BRONS'};
    var request = $.ajax({
        type: "POST",
        url: $(this).attr("href"),
        data: params,
        dataType: 'json'
    })
    .done(function(data) {
            console.log(data);
        if(!data.error){
            alert(data.message);
        }
        else {
            alert(data.message);
        }
    });
    request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
    });

});
function menuclick()
{
    'use strict';

    var navdrawerContainer = document.querySelector('.navdrawer-container');
    var appbarElement = document.querySelector('.app-bar');
    var menuBtn = document.querySelector('.menu');

    var main = document.querySelector('main');

    $("#overflow").click(function(){
        closeMenu();
    });    
    //main.addEventListener('click', closeMenu);
    menuBtn.addEventListener('click', toggleMenu);
    navdrawerContainer.addEventListener('click', closeMenu);
    navdrawerContainer.addEventListener('ontouchstart', closeMenu);
//    navdrawerContainer.addEventListener('click', function (event) {
//        if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
//            closeMenu();
//        }
//    });
}
function closeMenu() {
    $(".app-bar").removeClass("open");
    $(".navdrawer-container").removeClass("open");
    //$("#overflow").hide();
    hide_show_overflow();
    $("html,body").removeClass("overflow");
    return false;
}

function toggleMenu() {
    $(".app-bar").toggleClass('open');
    $(".navdrawer-container").toggleClass("open");
    $("#overflow").toggle();
    if($("#overflow").is(":visible")){
        hide_show_overflow(true);
    }
    else {
        hide_show_overflow();
    }

    $("html,body").toggleClass("overflow");
    return false;
}

function scroll_load() {
    //if (($(".item").last().offset().top-$(".item").last().height()*5)<$(window).scrollTop()&&!ajax_load)
    if (($(document).height()-$(".item").last().height()*4)<$(window).scrollTop()&&!ajax_load&&max_page>page&&$("#overflow:visible").length==0)//
    {
        $(".bubblingG").show();
        $("#overflow").show();
        //hide_show_overflow(true);
        $(".bubblingG #bubbling_text").text("Загрузка следующей страницы ресторанов...");        
        ajax_load = 1;        
        $.ajax({
            type: "POST",
            url: "/ajax/list.php"+location.search,
            data: {
                page:++page,
                PAGEN_1:page,
                pageRestSort:$("#sort").val()
            },
            success: function(data) {
                $(".bubblingG").hide();
                $(".bubblingG #bubbling_text").text("");
                $("#overflow").hide();
                //hide_show_overflow();
                $(".item").last().after(data);   
                ajax_load = 0;
                setOverflowD();
            }
        });
    } 
}
function popular_scroll_load() {
    //if (($(".item").last().offset().top-$(".item").last().height()*5)<$(window).scrollTop()&&!ajax_load)
    if (($(document).height()-$(".item").last().height()*4)<$(window).scrollTop()&&!ajax_load&&max_page>page&&$("#overflow:visible").length==0)//
    {
        $(".bubblingG").show();
        $("#overflow").show();
        //hide_show_overflow(true);
        $(".bubblingG #bubbling_text").text("Загрузка следующей страницы ресторанов...");
        ajax_load = 1;
        $.ajax({
            type: "POST",
            url: "/ajax/popular-list.php"+location.search,
            data: {
                page:++page,
                PAGEN_1:page,
                pageRestSort:$("#sort").val()
            },
            success: function(data) {
                $(".bubblingG").hide();
                $(".bubblingG #bubbling_text").text("");
                $("#overflow").hide();
                //hide_show_overflow();
                $(".item").last().after(data);
                ajax_load = 0;
                setOverflowD();
            }
        });
    }
}
function scroll_load_top100() {
    //if (($(".item").last().offset().top-$(".item").last().height()*5)<$(window).scrollTop()&&!ajax_load)
    if (($(document).height()-$(".item").last().height()*4)<$(window).scrollTop()&&!ajax_load&&max_page>page&&$("#overflow:visible").length==0)
    {
        $(".bubblingG").show();
        $("#overflow").show();
        //hide_show_overflow(true);
        $(".bubblingG #bubbling_text").text("Загрузка следующей страницы ресторанов...");
        ajax_load = 1;
        $.ajax({
            type: "POST",
            url: "/ajax/top100-list.php"+location.search,
            data: {
                page:++page,
                PAGEN_1:page,
                pageRestSort:$("#sort").val()
            },
            success: function(data) {
                $(".bubblingG").hide();
                $(".bubblingG #bubbling_text").text("");
                $("#overflow").hide();
                //hide_show_overflow();
                $(".item").last().after(data);
                ajax_load = 0;
                setOverflowD();
            }
        });
    }
}

function setOverflowD()
{
    //$(document).height()
    $("#overflow").css("height",'100%');//.css("width",$(window).width())
}

//$(document).on('pageinit', function() {
//    $('body').on('swiperight',function(){
//        toggleMenu();
//        console.log('swipe');
//    })
//    $('body').on('swipeleft',function(){
//        closeMenu();
//        console.log('swipe to clode');
//    })
//})

