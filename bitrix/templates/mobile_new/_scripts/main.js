//    how to get
var directionsDisplay;
var directionsService;
var map;


function initialize_map_how_get(sessionLat,sessionLon) {
    //if(!sessionLat||!sessionLon){
    //    sessionLat = coord[1];
    //    sessionLon = coord[0];
    //}
    directionsDisplay = new google.maps.DirectionsRenderer();
    var mapSenter = new google.maps.LatLng(sessionLat, sessionLon);
    var mapOptions = {
        zoom:9,
        center: mapSenter
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    directionsDisplay.setMap(map);
}

function calcRoute(endLat,endLon,selectedMode) {
    $(".bubblingG").show();
    $("#bubbling_text").text("Строим маршрут...");

    var start = new google.maps.LatLng(sessionLat, sessionLon);
    var end = new google.maps.LatLng(endLat, endLon);
    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.TravelMode[selectedMode]
    };
    directionsService = new google.maps.DirectionsService();
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            $(".bubblingG").hide();
            $("#bubbling_text").text("");
        }
        else {
            $(".bubblingG").hide();
            $("#bubbling_text").text("");
        }
    });
}
//    how to get
$(function(){

    function show_filter(e){
        e.preventDefault();
        e.stopPropagation();
        $('.popup.filter').show();
        $("#overflow").show();
    }

    $("main").on("click",".how-to-get-button",function() {
        $('.how-to-get-icon-wrapper.active').removeClass('active');
        $(this).find('.how-to-get-icon-wrapper').toggleClass('active');
        return false;
    });
    $("main").on("click",".how-to-get-icon-wrapper li > span",function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.map-canvas-wrapper').show();

        $("#overflow").show();

        google.maps.event.trigger(map, "resize");
        map.setCenter(new google.maps.LatLng(sessionLat, sessionLon));

        calcRoute($(this).attr('this_lat'),$(this).attr('this_lon'),$(this).attr('this_mode'));
        return false;
    });

    $("main").on("click",".close-map-canvas",function() {
        $("#overflow").hide();
        $(".popup").hide();
        $(".bubblingG").hide();
        $("#bubbling_text").text("");
    });

    $("main").on("click",".stars_new li",function() {
//        if(!$(this).parents(".popup").hasClass("popup"))
//            $("#review").focus();
        var count = $(this).index()+1;
        var i = 1;
        $("#input_ratio").attr("value",count);
        $(this).parent().find("li").each(function(){
            if (i<=count)
                $(this).attr("class","active");
            else
                $(this).attr("class","");
            i++;
        })
    });
    $("#main").on('click',".btn.filter",function(e){
        show_filter(e);
    });
    $(".filter *").click(function(e){
        var clicked = $(e.target);
        if(clicked.parent().hasClass('btn')){
            show_filter(e);
        }
        else {
            e.stopPropagation();
        }
    });

    $("main").click(function(){
        $(".rlist").removeClass("blur");
        $('.popup.filter').hide();
        $('.restoran_suggest').hide();
        $('.how-to-get-icon-wrapper.active').removeClass('active');
    });
    $("main").on("click",".close",function(e){
        $('.items-detail').removeClass('blur');
        $('header').removeClass('blur');
        $('.popup.review').hide();
    });



    $("#overflow").click(function(){
        $(".popup").hide();
    });
});
$(function(){
    'use strict';
    
    var navdrawerContainer = document.querySelector('.navdrawer-container');
    var appbarElement = document.querySelector('.app-bar');
    var menuBtn = document.querySelector('.menu');   

    var main = document.querySelector('main');
    function closeMenu() {
        $(".app-bar").removeClass("open");
         $(".navdrawer-container").removeClass("open");
//        appbarElement.classList.remove('open');
//        navdrawerContainer.classList.remove('open');
        $("#overflow").hide();
        $("html,body").removeClass("overflow");        
        return false;
    }

    function toggleMenu() {               
        $(".app-bar").toggleClass('open');
        $(".navdrawer-container").toggleClass("open");
//        appbarElement.classList.toggle('open');
//        navdrawerContainer.classList.toggle('open');
        $("#overflow").toggle();
        $("html,body").toggleClass("overflow");                 
        return false;        
    }

    //main.addEventListener('ontouchstart', closeMenu);
    //main.addEventListener('click', closeMenu);
    menuBtn.addEventListener('click', toggleMenu);
    navdrawerContainer.addEventListener('click', closeMenu);    
    $("#overflow").click(function(){
        closeMenu();
    });    
    navdrawerContainer.addEventListener('ontouchstart', closeMenu);
//    navdrawerContainer.addEventListener('click', function (event) {
//        if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
//            closeMenu();
//        }
//    });

   //$('a.dropdown-trigger').on('click', function(){
   //    $(this).parent().toggleClass('open');
   //    return false;
   //});

});
$(document).on('click', '[data-toggle=favorite]', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var _this = $(this);
    if (!$(this).data("restoran"))
    {
        alert("Произошла ошибка, попробуйте позже")
        return false;
    }

    console.log($(this).data("restoran"));


    var params = {"restoran":$(this).data("restoran"), 'name':'MY_MOBILE_BRONS'};
    var request = $.ajax({
        type: "POST",
        url: $(this).attr("href"),
        data: params,
        dataType: 'json'
    })
    .done(function(data) {
            console.log(data);
        if(!data.error){
            alert(data.message);
        }
        else {
            alert(data.message);
        }
    });
    request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
    });

});
function menuclick()
{
    'use strict';

    var navdrawerContainer = document.querySelector('.navdrawer-container');
    var appbarElement = document.querySelector('.app-bar');
    var menuBtn = document.querySelector('.menu');

    var main = document.querySelector('main');
    function closeMenu() {
        $(".app-bar").removeClass("open");
         $(".navdrawer-container").removeClass("open");
        $("#overflow").hide();
        $("html,body").removeClass("overflow");        
        return false;
    }

    function toggleMenu() {
        $(".app-bar").toggleClass('open');
        $(".navdrawer-container").toggleClass("open");
        $("#overflow").toggle();
        $("html,body").toggleClass("overflow");           
        return false;
    }
    $("#overflow").click(function(){
        closeMenu();
    });    
    //main.addEventListener('click', closeMenu);
    menuBtn.addEventListener('click', toggleMenu);
    navdrawerContainer.addEventListener('click', closeMenu);
    navdrawerContainer.addEventListener('ontouchstart', closeMenu);
//    navdrawerContainer.addEventListener('click', function (event) {
//        if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
//            closeMenu();
//        }
//    });
}


function scroll_load() {
    //if (($(".item").last().offset().top-$(".item").last().height()*5)<$(window).scrollTop()&&!ajax_load)        
    if (($(document).height()-$(".item").last().height()*4)<$(window).scrollTop()&&!ajax_load&&max_page>page)
    {
        $(".bubblingG").show();
        $("#overflow").show();
        $(".bubblingG #bubbling_text").text("Загрузка следующей страницы ресторанов...");        
        ajax_load = 1;        
        $.ajax({
            type: "POST",
            url: "/ajax/list.php"+location.search,
            data: {
                page:++page,
                PAGEN_1:page,
                pageRestSort:$("#sort").val()
            },
            success: function(data) {
                $(".bubblingG").hide();
                $(".bubblingG #bubbling_text").text("");
                $("#overflow").hide();                
                $(".item").last().after(data);   
                ajax_load = 0;
                setOverflowD();
            }
        });
    } 
}

function setOverflowD()
{
    //$(document).height()
    $("#overflow").css("height",'100%').css("width",$(window).width());
}