$(function(){
    //alert('ios 8');
    $('main').on('focus', 'input#restoran_input_field', function(){
        //$(window).scrollTop(0);
        //, top:'0'
        $(this).parents('.popup.filter').css({position:'absolute'});
        $('body').height($(window).height());

        $(window).scrollTop(0);
        //$('body').css('overflow','hidden');
        OffScroll();  //Запустили отмену прокрутки
    });
    $('main').on('blur', 'input#restoran_input_field',function(){
        //,top:'auto'
        $(this).parents('.popup.filter').css({position:'fixed'});
        $('body').css('height','auto');
        //$('body').css('overflow','auto');
        $(window).unbind('scroll'); //Выключить отмену прокрутки
    });

    function OffScroll () {
        var winScrollTop = $(window).scrollTop();
        $(window).bind('scroll',function () {
            $(window).scrollTop(winScrollTop);
        });
    }
});