<?
global $USER;
$res = CUser::GetByID($USER->GetID());
if ($ar = $res->Fetch())
{
     if ($ar['PERSONAL_PHOTO'])
    {
        $file = CFile::ResizeImageGet($ar['PERSONAL_PHOTO'], array('width'=>48, 'height'=>48), BX_RESIZE_IMAGE_EXACT  , true);                        
        //$file["src"] = CFile::GetPath($uInfo['PERSONAL_PHOTO']);                        
        $arResult['USER_PHOTO'] = $file;
    }
    else
    {        
        if ($ar['PERSONAL_GENDER']=="M")
            $arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/man_nnm_42.png";
        elseif($ar['PERSONAL_GENDER']=="M")
            $arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/woman_nnm_42.png";
        else
            $arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/unisx_nnm_42.png";    
    }
}
?>