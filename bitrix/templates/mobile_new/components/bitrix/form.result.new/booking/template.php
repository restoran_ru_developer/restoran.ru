<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class='booking' <?=(!$arResult["RESTORAN_NAME"])?"style='margin-top:10px;'":""?>>
        <?if ($arResult["RESTORAN_NAME"]):?>
            <div class="rest" style='background-image: url(<?=$arResult["RESTORAN_PIC"]["src"]?>);'>
                <div class="info">
                    <h2><?=$arResult["RESTORAN_NAME"]?></h2>
                    <h3><?=$arResult["RESTORAN_ADRES"]?></h3>
                </div>
            </div>
        <?endif;?>
        <form id="booking" name="SIMPLE_FORM_24" action="/booking/?rest_title=<?=$arResult['REST_PAGE_URL']?>" data-ajax="false" method="POST" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="WEB_FORM_ID" value="24">     
            <?if ($arResult["FORM_NOTE"]):?>
            <div class="ok" style="color:#000; text-align:center">
                <?= $arResult["FORM_NOTE"] ?><br /><br />                
            </div>
            <?endif;?>
            <div class="font12" id="err">
                <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
            </div>                    
            <?
            if ($_REQUEST["form_text_164"])
                $_REQUEST["id"] = $_REQUEST["form_text_164"];
            if ($_REQUEST["form_text_155"])
                $arResult["REST"] = $_REQUEST["form_text_155"];
            ?>       
            <?if (!$arResult["REST"]):?>
            <div class="line restoran_suggest_box">
                <label for="restoran">Ресторан</label>                
                <input id='restoran' autocomplete="off" type="text" value="<?=$arResult["REST"]?>">                
                <div class="restoran_suggest"></div>                
            </div>        
            <?endif;?>
            <input type="hidden" value="<?=$arResult["REST"]?>" name="form_text_155" id="restoran-name">
            <input type="hidden" name="rest_title" value="<?=$arResult["REST"]?>" />
            <input type="hidden" value="<?=$_REQUEST["id"]?>" name="form_text_164" id="restoran-id">
            <?
            foreach ($arResult["QUESTIONS"] as $key => $question):
                $ar = array();
                $ar["CODE"] = $key;
                $questions[] = array_merge($question, $ar);
            endforeach;
            ?>
            

            <div class="line">
                <label for="date">Дата брони</label>                                
                <input type="date" id="date" value="<?=($_REQUEST["form_".$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[2]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[2]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[2]["STRUCTURE"][0]["ID"]]:date("Y-m-d")?>" name="form_<?= $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[2]["STRUCTURE"][0]["ID"] ?>"/>
            </div>
            <div class="line">
                <label for="time">Время</label>
                <input type="time" id="time" placeholder="19:00" value="<?=($_REQUEST["form_".$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[3]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_".$questions[3]["STRUCTURE"][0]["FIELD_TYPE"]."_".$questions[3]["STRUCTURE"][0]["ID"]]:"19:00"?>" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>"/>                      
            </div>    
            <div class="line">            
                <label for="count">Кол-во человек</label>
                <input id="count" type="number" class="phone" style="text-align:left" value="<?=($_REQUEST["form_text_158"])?$_REQUEST["form_text_158"]:"0"?>" placeholder="" name="form_text_158"/>
            </div>
            <div class="line">
                <label for="name">Имя</label>
                <input placeholder="Имя Фамилия" id="name" type="text" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />                       
            </div>       
            <?
            $rsUser = CUser::GetByID($USER->GetID());
            $ar = $rsUser->Fetch();
            $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("+371", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = preg_replace("/\D/", "", $ar["PERSONAL_PHONE"]);
            ?>        
            <div class="line">
                <label for="phone">Телефон</label>                
                <input type="text" id="phone" class="rga-urm-phone-input" value="<?=($_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]]:$ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" placeholder=""  />
                <select name="country_code_phone" class="booking-country-code-select">
                    <option value="371" selected >+371</option>
                    <option value="7" >+7</option>
                </select>
            </div>        
            <? //var_dump($arResult["isUseCaptcha"])?>
            <? if ($arResult["isUseCaptcha"] == "Y"): ?>
                <div class="question">
                    <?= GetMessage("CAPTCHA") ?>
                </div>
                <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') 370px 18px no-repeat;">
                    <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
                    <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")  ?><? //=$arResult["REQUIRED_SIGN"];  ?>
                    <input type="text" data-theme="b" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
                </div>
            <? endif; ?>        
            <? if ($_REQUEST["invite"]): ?>
                <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
            <? endif; ?>            
            <? if ($_REQUEST["CITY"]): ?>
                <input type="hidden" name="CITY" value="<?= $_REQUEST["CITY"]?>" />
            <? else: ?>        
                <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
            <? endif; ?>

            <input type="hidden" name="web_form_apply" value="Y" />
            <div class="line">            
                <input id="form_button" class="light_button"  data-theme="b" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?= strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]; ?>" />        
            </div>         
        <?=$arResult["FORM_FOOTER"] ?>               
        </form>
</div>
<script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.mask.js"></script>
<script>
    $(function(){
//        $("#phone").focusin(function(){
//            $("#phone").caret.begin;
//        });

//        $("#phone").mask("+000 000-000-00");
        $("#restoran").keyup(function(){        
            if ($("#restoran").length&&$("#restoran").val().length>2)
            {
                if (null!=$(".restoran_suggest"))
                {
                    var a = "";
                    if (location.pathname.indexOf("/search/")>=0)
                        a = "search";
                    else
                        a = "";
                    $( ".restoran_suggest" ).load("/ajax/suggest.php", {"q":$("#restoran").val(),"sessid":$("#sessid").val(),"page":a}, function (data) {
                        if (data) {
                            $(".restoran_suggest").show();
                        }
                    });
                }
            }
        });
    });
</script>