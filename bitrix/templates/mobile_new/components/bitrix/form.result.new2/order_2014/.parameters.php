<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arTemplateParameters = array(
	'SHOW_THIS_FORM_TITLE' => array(
		'NAME' => GetMessage('SHOW_THIS_FORM_TITLE'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	),
);

?>