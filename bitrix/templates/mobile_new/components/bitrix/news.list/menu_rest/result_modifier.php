<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 240, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    CModule::IncludeModule("sale");
    CModule::IncludeModule("catalog");

    $db_res = CPrice::GetList(
        array(),
        array(
            "PRODUCT_ID" => $arItem["ID"],
            "CATALOG_GROUP_ID" => 1
        )
    );
    if($price = $db_res->Fetch()){
        $arResult["ITEMS"][$key]["PRICE"] = $price;
    }

//    $arResult["ITEMS"][$key]["PRICE"] = CPrice::GetBasePrice($arItem["ID"]);
    if ($arResult["ITEMS"][$key]["PRICE"]["CURRENCY"]=="EUR")
        $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.2f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]); 
    else
        $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]); 
}
?>