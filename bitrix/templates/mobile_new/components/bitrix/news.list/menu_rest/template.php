<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div style="clear:both"></div>
<h2 align="center">Меню</h2>
    
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="menu_item <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">
        <?/*if ($arItem["PREVIEW_PICTURE"]["src"]):?>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /><br /><br />
        <?endif;*/?>        
        <span class="menu_name"><?=$arItem["NAME"]?></span>
        <?if ($arItem["PRICE"]["PRICE"]>0):?>
            <?if (CITY_ID=="ast"||CITY_ID=="amt"):?>
                <span class="menu_price"><?=$arItem["PRICE"]["PRICE"]?> <span class="tenge font18">h</span></span>
            <?else:?>
                <?if ($arItem["PRICE"]["CURRENCY"]=="EUR"):?>
                    <span class="menu_price">€ <?=$arItem["PRICE"]["PRICE"]?></span>
                <?else:?>
                    <span class="menu_price"><?=$arItem["PRICE"]["PRICE"]?> <span class="rouble font18">q</span></span>
                <?endif;?>                
            <?endif;?>
        <?endif;?>     
                    <div class="clear"></div>
    </div>
<?endforeach;?>
