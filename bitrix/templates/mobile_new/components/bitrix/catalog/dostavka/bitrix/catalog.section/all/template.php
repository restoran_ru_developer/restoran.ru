<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1 class="menu_name"><?=$arResult["NAME"]?></h1>
<?if (count($arResult["ITEMS"])>0):?><table width="100%">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
        <?
        $cols = 1;
        if (!is_array($arElement["PREVIEW_PICTURE"]))
            $cols++;
        if (!trim($arElement["PREVIEW_TEXT"]))
            $cols++;
        if (!$arElement["PRICES"]["BASE"]["VALUE"]&&!$arElement["CAN_BUY"]&&!substr_count($_SERVER["HTTP_REFERER"],"dostavka"))
        {
            $preview = "end";
        }
        elseif ($arElement["PRICES"]["BASE"]["VALUE"]&&!$arElement["CAN_BUY"]&&!substr_count($_SERVER["HTTP_REFERER"],"dostavka"))
        {
            $price = "end";
        }
        else
        {
            $buy = "end";
        }
        ?>
		<tr class="<?=(end($arResult["ITEMS"])==$arElement)?"end":""?>">                    
                        <td>
                            <div class="name"><?=$arElement["~NAME"]?></div>
                            <?if (trim($arElement["PREVIEW_TEXT"])):?>                        
                                <Br /><i><?=$arElement["PREVIEW_TEXT"]?></i>
                            <?endif;?>
                        </td>                    
                    <?if ($arElement["PRICES"]["BASE"]["VALUE"]):?>
                        <td class="price <?=$price?>" width="100" align="right">                            
                            <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                <?if($arPrice["CAN_ACCESS"]&&$arPrice["VALUE"]>0):?>
                                    <?if ($arPrice["CURRENCY"]=="EUR"):?>
                                        <span class="menu_price" colspan="3" nowrap="">€ <?=$arPrice["VALUE"]?></span>
                                    <?else:?>
                                        <span class="menu_price" colspan="3" nowrap=""><?=$arPrice["VALUE"]?> руб.</span>                                                        
                                    <?endif;?>                                        
                                <?endif;?>
                            <?endforeach;?>                            
                        </td>
                    <?endif;?>                    
                </tr>
<?endforeach;?>
    </table>
<?endif;?>