<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>


<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="ajax_form" style="">
    <? if ($arResult["AUTH_SERVICES"]): ?>
        <h3 style="margin-top:0px; margin-bottom:10px;"><? echo GetMessage("AUTH_TITLE") ?></h3>
        <div class="clear"></div>
    <? endif ?>
    <div style="float: left; margin-bottom:5px">
        <?
        $APPLICATION->IncludeComponent("restoran:user.vkontakte_auth", ".default", array(
                ), false
        );
        ?>
    </div>
    <div style="float: right;margin-bottom:5px">
        <?
        $APPLICATION->IncludeComponent("restoran:user.facebook_auth", ".default", array(
                ), false
        );
        ?>
    </div>
    <div class="clear"></div>
            <!--<div><?= GetMessage("AUTH_PLEASE_AUTH") ?></div>-->
    <form name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="AUTH" />
        <? if (strlen($arResult["BACKURL"]) > 0): ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
        <? endif ?>
        <? foreach ($arResult["POST"] as $key => $value): ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
        <? endforeach ?>
        <div class="question">
            <?= GetMessage("AUTH_LOGIN") ?><br />
            <input class="inputtext" data-theme="b" type="text" name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>" size="39" />
        </div>
        <div class="question">
            <?= GetMessage("AUTH_PASSWORD") ?><br />
            <input class="inputtext" data-theme="b" type="password" name="USER_PASSWORD" maxlength="255"  size="39" />
        </div>
        <div class="">
            <table>
                <tr>
                    <td><input data-theme="c" data-mini="true" type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" /></td>
                    <td><label for="USER_REMEMBER">&nbsp;<?= GetMessage("AUTH_REMEMBER_ME") ?></label></td>
                </tr>
            </table>                
        </div>       
        <noindex>
            <p align="center">
                <a class="another font14" style="margin:0px;font-size:12px;" href="/auth/register.php" rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a>
            </p>
        </noindex>
        <? if ($arParams["NOT_SHOW_LINKS"] != "Y"): ?>
            <noindex>
                <p align="center">
                    <a class="another font14" style="margin:0px;font-size:12px;" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>
                </p>
            </noindex>
        <? endif ?>
        <div align="center">
            <input type="submit" data-theme="e" class="light_button" name="Login" value="<?= GetMessage("AUTH_AUTHORIZE") ?>" />
        </div>
        <? /* if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
          <noindex>
          <p>
          <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a><br />
          <?=GetMessage("AUTH_FIRST_ONE")?>
          </p>
          </noindex>
          <?endif */ ?>
    </form>
</div>
<script type="text/javascript">
<? if (strlen($arResult["LAST_LOGIN"]) > 0): ?>
        try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<? else: ?>
        try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<? endif ?>
</script>

