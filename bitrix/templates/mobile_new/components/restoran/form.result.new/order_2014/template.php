<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<script>
$(document).ready(function(){
    $('#date').on('change',function(){
        $('.today-tomorrow-select option').attr('selected',false);
        if($('.today-tomorrow-select option[value="'+$(this).val()+'"]').length>0){
            $('.today-tomorrow-select option[value="'+$(this).val()+'"]').attr('selected',true);
        }
        else {
            $('.in-select-for-date').text($(this).val()).attr('selected',true).show();
            $('.in-select-for-date').val($(this).val());
        }
    })
    $('.today-tomorrow-select').on('change',function(event){
        console.log('change');
        this_selected_opt = $(this).find('option:selected');
        $('#date').val(this_selected_opt[0].value);
        $('#date').attr('data-date',this_selected_opt[0].value);
        $('.in-select-for-date').hide();
    })

    $('.wishes-list-wrapper li').on('click touchstart', function(){
        $('.wishes-input').val($(this).text());
        $('.wishes-list-wrapper').removeClass('active');
        $('.wishes-input').focus();
    });

    $('.wish-list-new-trigger').hover(
        function(){
            $('.wishes-list-wrapper').addClass('active');
        },
        function(){
            $('.wishes-list-wrapper').removeClass('active');
        }
    )
    $('.wishes-list-wrapper').hover(
        function(){
            $('.wishes-list-wrapper').addClass('active');
        },
        function(){
            $('.wishes-list-wrapper').removeClass('active');
        }
    )


       //    $(".datew").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});                                                                                                                   
//    $(".phone1").maski("(999) 999-99-99",{placeholder:""});
    $(".up").click(function(){
       $("#company").val($("#company").val()*1+1); 
    });
    $(".down").click(function(){
        if ($("#company").val()*1>1)
            $("#company").val($("#company").val()*1-1); 
    });



    $("#order_online form").submit(function(){


        var err = "";
        if (!$("#top_search_rest").val())
        {
            err = "Выберите ресторан<br />";
        }
        if (!$("#time").val())
        {
            err = err+"Выберите время<br/>";
        }
        if (!$("#company").val()||$("#company").val().replace(/\D/g,'')==0)
        {
            err = err+"Введите кол-во человек<br/>";
        }
        expr = /(.+) (.+)/;
        if (!$("#name").val()||!expr.test($("#name").val()))
        {
            err = err+"Введите свое имя и фамилию<br/>";
        }
        if ($(".phone1").val().replace(/\D/g,'').length<1)
        {
            err = err+"Введите свой телефон";
        }
        if (err)
        {
            $(".errors").html(err+"<br /><Br />");
            return false;
        }
        $('.btn.btn-info.btn-nb').attr('disabled',true);
    });
});     
</script>
<? 
//if ($arResult["isFormNote"] != "Y") {     
?><?/*=bitrix_sessid();*/?>
<?$datehash = date("Ymd-His");?>
<?
if ($_REQUEST["name"] )
    $_REQUEST["name"] = rawurldecode($_REQUEST["name"]);
if (!$_REQUEST["person"])
    $_REQUEST["person"] = 2;
if ($_REQUEST["banket"]=="Y")
    $_REQUEST["person"] = 10;
if (!$_REQUEST["time"])
    $_REQUEST["time"] = date("H   i");
?>
<div class='booking' <?=(!$arResult["RESTORAN_NAME"])?"style='margin-top:10px;'":""?>>
    <?if ($arResult["RESTORAN_NAME"]):?>
    <?$_REQUEST['name'] = $arResult["RESTORAN_NAME"];?>
    <div class="rest" style='background-image: url(<?=$arResult["RESTORAN_PIC"]["src"]?>);'>
        <div class="info">
            <h2><?=$arResult["RESTORAN_NAME"]?></h2>
            <h3><?=$arResult["RESTORAN_ADRES"]?></h3>
        </div>
    </div>
    <?endif;?>
</div>
<div class="modal-dialog gis-widget">
    <div class="modal-content">

        <div class="modal-body">

<link href="<?= $templateFolder ?>/style.css?<?=md5($datehash)?>"  type="text/css" rel="stylesheet" />
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>     
    <div id="order_online" >
<? endif; ?>
        <?if($arParams['SHOW_THIS_FORM_TITLE']!='N'):?>
            <div class="title"><?=GetMessage("RESERVE_TABLE")?></div>
        <?endif;?>
            <?= $arResult["FORM_HEADER"] ?>
            <div class="errors"></div>
            <label for="top_search_input1"><?=GetMessage("REST_NAME")?></label>
            <div class="service_free"><?=GetMessage("SERVICE_FREE")?></div>            
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>                    
                    <td class="left_column">
                        <?foreach ($arResult["QUESTIONS"] as $key => $question):
                                $ar = array();
                                $ar["CODE"] = $key;
                                $questions[] = array_merge($question, $ar);                    
                        endforeach;?>                                                                                          
                            <?//if (!$_REQUEST["name"]&&!$_REQUEST["form_text_32"]):?>
                                <div class="form-group">                                                                               
                                        <?
                                        $APPLICATION->IncludeComponent(
                                                "bitrix:search.title", "bron_2014", Array(
                                            "NAME" => "form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]
                                                ), false
                                        );
                                        ?>
                                </div>
                             <? if (LANGUAGE_ID == 'ru'): ?>            			                        
                                <?
                                    $s = $arResult["arrVALUES"]["form_dropdown_SIMPLE_QUESTION_547"];
                                    if (!$s)
                                        $s = $_REQUEST["form_dropdown_SIMPLE_QUESTION_547"];


                                    ?>
                                <?if (!$s):?>
                                    <input type="hidden" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29">
                                <?else:?>                                    
                                    <input type="hidden" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="<?=$s?>">
                                <?endif;?>                                 
                            <? endif; ?>
                            <? if (LANGUAGE_ID == 'en'): ?>
                                <input type="hidden" id="what_question_" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29" />            
                            <? endif; ?>   
                        <div class="form-group pull-left today-tomorrow-wrapper">
                            <label for="date"><?=GetMessage("NA")?></label>
                            <?
                            if (!$_REQUEST["date"]){
                                $_REQUEST["date"] = substr_count($_SERVER["HTTP_USER_AGENT"],"iPad") ? date("Y-m-d") : date("d.m.Y");
                            }
                            ?>
                            <input <? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'type="text"'; ?> <? if (substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'readonly="true" type="date" '; ?>
                                class="<? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'datepicker'; ?> form-control"
                                id="date"
                                data-date="<?= substr_count($_SERVER["HTTP_USER_AGENT"], "iPad") ? date("Y-m-d") : date("d.m.Y") ?>"
                                value="<? echo ($_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]] : $_REQUEST["date"] ?>"
                                name="form_<?= $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[2]["STRUCTURE"][0]["ID"] ?>"/>

                            <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/tpl/include_areas/order-form-today-tomorrow.php",
                            "EDIT_TEMPLATE" => ""
                            ),
                            false
                            ); ?>

                                <!--<div class="today-tomorrow-trigger"></div>-->


                        </div>
                        <div class="form-group pull-right" style="position:relative">
                            <label for="company"><?=GetMessage("OUR_COMPANY")?></label>

                            <?if(!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
                                <div class="up-down"><a href="javascript:void(0)" class="up"></a><a href="javascript:void(0)" class="down"></a></div>
                                <input type="text" class="form-control" id="company"
                                       value="<?= ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"] ?>"
                                       size="4" maxlength="3"
                                       name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>"/>
                            <?else:?>
                                <select class="form-control company-select" id="company" name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>">
                                    <?$selected_val = ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"]?>
                                    <?for($i=0;$i<21;$i++){?>
                                        <option value="<?=$i?>" <?if($selected_val==$i)echo 'selected';?>><?=$i?></option>
                                    <?}?>
                                </select>
                            <?endif?>
                        </div>                                                
                        <div class="clearfix"></div>                        
                        <div class="form-group" style="margin-top:10px;">                                       
                            <input type="text" class="form-control wishes-input" placeholder="<?= GetMessage("MY_WISH") ?>" name="form_<?= $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[11]["STRUCTURE"][0]["ID"] ?>" <?= trim($_REQUEST["form_" . $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[11]["STRUCTURE"][0]["ID"]]) ?>/>

                            <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/tpl/include_areas/order-form-wishes-list.php",
                            "EDIT_TEMPLATE" => ""
                            ),
                            false
                            ); ?>

                        </div>
                        <div class="form-group pull-left end"> 
                            <label for="name"><?=(LANGUAGE_ID=="en")?"Name":"Имя"?></label>                            
                            <input type="text" placeholder="<?=(LANGUAGE_ID=="en")?"Name":"Фамилия Имя"?>" class="form-control" id="name" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />                                        
                        </div>
                        <div class="form-group pull-right end"> 
                            <label for="phone"><?=GetMessage("MY_PHONE")?></label>  <br />                            
                                    <?
                                    $rsUser = CUser::GetByID($USER->GetID());
                                    $ar = $rsUser->Fetch();
                                    ?>
                                    <div class="pull-left phone-code-wrapper no-checkbox" style="margin-top: 10px; margin-right: 10px; font-size: 14px;">+7</div>
                                    <div class="pull-right">
                                        <input type="text" class="phone1 form-control" placeholder="(800) 123-45-67" style="text-align:left" value="<?= ($_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]] : $ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" />
                                        </div>
                            <input type="hidden" class="form-control" id="email" value="<?= ($_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]] : $USER->GetEmail() ?>"
                                   name="form_<?= $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[8]["STRUCTURE"][0]["ID"] ?>" />
                                    
                        </div>                           
                        <div class="clearfix"></div>                                                                                                                        
                    </td>
                    <td class="right_column">
                        <div class="times">                    
                            <div class="more2">
                            <?                   
                            for ($i=12;$i<=26; $i++):?>
                                <?if ($i>=24) $ii="0".($i-24); else $ii = $i;?>
                                <div class="time <?=($i>23)?"end":""?> <?=($arResult["arrVALUES"]["form_text_34"]==$ii.":00")?"active":""?>"><?=$ii?>:00</div>
                                <?if ($i==27):?>
                                    </div>
                                    <div class="moretime" onclick="$('.more2').toggle()">…</div>                        
                                    <div class="more2" style="display:none">                                                            
                                <?endif;?>                            
                                <?if ($t==2) break;?>
                                <div class="time <?=($i>23)?"end":""?>  <?=($arResult["arrVALUES"]["form_text_34"]==$ii.":30")?"active":""?>"><?=$ii?>:30</div>                                                
                            <?endfor;?>
                                        </div>
                                    <input type="hidden" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>" id="time" value="<?=$arResult["arrVALUES"]["form_text_34"]?>" />
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>      
        <div class="form-group">            
           <input style="width:0px;height:0px;padding:0px;margin:0px;" class="btn btn-info btn-nb" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?=($bbb=="b")?GetMessage("RESERVE_BANKET"):GetMessage("RESERVE_TABLE")?>" />
           <a style="width:100%;" class="btn btn-info btn-nb" onclick="$('#order_online form').submit()"><?=($bbb=="b")?GetMessage("RESERVE_BANKET"):GetMessage("RESERVE_TABLE")?></a>
        </div>
        <? /* if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
          <?if ($arResult["isFormTitle"]):?>
          <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
          <?endif;?>
          <?endif; */ ?>        
        <div class="ok"><?if ($arResult["isFormNote"] == "Y") {?><?= $arResult["FORM_NOTE"] ?>
		<?}?></div>
        <div class="font12" id="err">
            <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
        </div>		
        <script>            
            $(document).ready(function(){
                <?if ($_REQUEST["banket"]=="Y"):?>
                            if (!$("#top_search_rest").val())
                                $("#top_search_rest").val(1);
                    <?endif;?>
                $("#top_search_input1").blur(function(){
                    if ($("#top_search_adr").val()&&$("#top_search_name").val())
                    {
                        $("#chek .rest_name").html($("#top_search_name").val());
                        $("#chek .rest_adress").html($("#top_search_adr").val()+"<hr />");                       
                    }
                });                               
                $(".time").click(function(){
                    $(".time").removeClass("active");
                    $(this).addClass("active");
                    $("#time").val($(this).text());                    
                    $("#chek .time2").html( $("#date-picker").val()+"<br /><?=GetMessage("CHEK_TIME")?>: "+$("#time").val()+"<br /><?=GetMessage("CHEK_PERSONS")?>: "+$(".updowninp").val()); 
                });                                                                    	                  	
            });
        </script>                                           
        <? if ($arResult["isUseCaptcha"] == "Y"): ?>
            <div class="question">
                <?= GetMessage("CAPTCHA") ?>
            </div>
            <div>
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
                <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")   ?><? //=$arResult["REQUIRED_SIGN"];   ?>
                <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
            </div>
        <? endif; ?>
        
        <div class="inv_cph">
            <input type="checkbox" name="lst_nm" value="1" />
            <input type="checkbox" name="scd_nm" value="1" checked="checked" />
        </div>
        <? if ($_REQUEST["invite"]): ?>
            <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
        <? endif; ?>
        <? if ($_REQUEST["CITY"]): ?>
            <input type="hidden" name="CITY" value="<?= $_REQUEST["CITY"] ?>" />
        <? else: ?>        
            <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
        <? endif; ?>
        <input type="hidden" name="web_form_apply" value="Y" />
        <div class="clear"></div>        

        <!--div class="left">
            <p class="another_color font12" style="line-height:24px;">* — <?= GetMessage("FORM_REQUIRED_FIELDS") ?></p>
        </div-->

        
		
        <div class="clear"></div>
        
        <?
//} //endif (isFormNote)
        ?>
        <?= $arResult["FORM_FOOTER"] ?>
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    </div>
<? endif; ?>
<script src="/bitrix/templates/mobile_new/scripts/jquery.mask.js"></script>
<script>
    $(function(){
        $(".phone1").mask("(000) 000-0000");
        $("#restoran").keyup(function(){
            if ($("#restoran").length&&$("#restoran").val().length>2)
            {
                if (null!=$(".restoran_suggest"))
                {
                    var a = "";
                    if (location.pathname.indexOf("/search/")>=0)
                        a = "search";
                    else
                        a = "";
                    $( ".restoran_suggest" ).load("/ajax/suggest.php", {"q":$("#restoran").val(),"sessid":$("#sessid").val(),"page":a}, function (data) {
                        if (data) {
                            $(".restoran_suggest").show();
                        }
                    });
                }
            }
        });
    });
</script>
        </div>
    </div>
</div>