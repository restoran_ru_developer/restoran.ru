<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
<div class="review">
    <div class="review-head">
        <span class="name"><?= $arItem["NAME"] ?></span>
        <div style='background-image:url("<?=$arItem['AVATAR']['src']?>");' alt="" class="user"></div>
        <span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM_DAY"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_MONTH"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_YEAR"] ?></span>
    </div>
    <div class="review-body">

<?
$obParser = new CTextParser;
 $arItem["PREVIEW_TEXT2"] = $obParser->html_cut($arItem["PREVIEW_TEXT2"], 100);
?>
        <p class="text"> <?=$arItem["PREVIEW_TEXT2"] ?></p>
        <a href="" class="link">Читать дальше</a>
    </div>
</div>
<?endforeach;?>