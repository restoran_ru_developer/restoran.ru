<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div id="comments">
    <? if ($arResult["ITEMS"][0]["ID"]): ?>
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        
            <div class="one-opinion">
                <div class="left review_text" >
                    <div class="preview_text<?= $arItem["ID"] ?>">
                        <?=$arItem["PREVIEW_TEXT2"] ?>
                    </div>
                    <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                        <div class="preview_text<?= $arItem["ID"] ?>" style="display:none;">
                            <?=$arItem["PREVIEW_TEXT"] ?>
                        </div>
                    <? endif; ?>
                </div>

                <div class="image" style="background-image=url(<?=$arItem['PICTURE']['SRC']?>)"></div>
                <span class="name"><?= $arItem["NAME"] ?> </span>
                <div class="comment_date"><?= $arItem["DISPLAY_ACTIVE_FROM_DAY"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_MONTH"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_YEAR"] ?>, <?= $arItem["DISPLAY_ACTIVE_FROM_TIME"] ?></div>

                <div class="clear"></div>
            </div>
        <? endforeach; ?>
    <? else: ?>
        <p style="font-style:italic"><?= GetMessage("NO_REVIEWS") ?></p>
    <? endif; ?>
</div>
