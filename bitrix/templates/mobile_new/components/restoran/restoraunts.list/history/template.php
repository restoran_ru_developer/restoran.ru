<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["aja"]!="Y"):?>
<div class="items rlist">
<?endif;?>        
<? if (count($arResult["ITEMS"]) > 0): ?>            
    <?
    foreach ($arResult["ITEMS"] as $cell => $arItem):  	                      
        $lat1 = (float) $arItem["PROPERTIES"]['lat']["VALUE"][count($arItem["PROPERTIES"]['lat']["VALUE"])-1];
        $lon1 = (float) $arItem["PROPERTIES"]['lon']["VALUE"][count($arItem["PROPERTIES"]['lon']["VALUE"])-1];
        $lat2 = (float) $_SESSION['lat'];
        $lon2 = (float) $_SESSION['lon'];

        if ($_REQUEST["pageRestSort"]=="distance")
            $dist = $arItem["DISTANCE"];
        else
            $dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
        if ($dist < 1000)
            $distance = round($dist,0) . ' м';
        else
            $distance = round($dist/1000,2).' км';

        $arIB = getArIblock("catalog", CITY_ID);
        $arItem['PROPERTIES']['address']['VALUE']['0'] = str_replace($arIB["NAME"].", ", "", $arItem['PROPERTIES']['address']['VALUE']['0']);
        ?>
        <div class="item sl">
            <div class="pic" style='background:url(<?= $arItem['PREVIEW_PICTURE']['src'] ?>) no-repeat;'>
                <div class="info">
                    <a class="ajax" href="/detail.php?ID=<?= $arItem["ID"] ?>">
                        <div class='left'>
                            <h2><?= (strlen($arItem['NAME']) > 23 ? substr($arItem['NAME'], 0, 23) . '...' : $arItem['NAME']); ?></h2>

                            <h3><?= (strlen($arItem['PROPERTIES']['address']['VALUE']['0']) > 30 ? substr($arItem['PROPERTIES']['address']['VALUE']['0'], 0, 30) . '...' : $arItem['PROPERTIES']['address']['VALUE']['0']); ?></h3>

                            <h3 class='subway'>м.
                                <?if (is_array($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])):
                                    echo strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'][0]);
                                else:
                                    echo strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
                                endif;?>
                            </h3>
                        </div>
                        <div class='right'>
                            <span class='time'><?= $distance ?></span>
                            <?if (is_array($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])):
                                preg_match_all("/\d+/", strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'][0]), $r);
                            else:
                                preg_match_all("/\d+/", strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']), $r);
                            endif;?>
                            <? if (end($r)): ?>
                                <span class="average_bill"><?= end($r[0]) ?></span>
                            <? endif; ?>
                        </div>
                    </a>
                </div>
                <div class="rest-buttons">
                    <div>
                        <?if (CITY_ID=="spb"):?>
                            <a class='button call-trigger' href='tel:+78127401820'><span>+7 812 740 18 20</span></a>
                        <?elseif(CITY_ID=="msk"):?>
                            <a class='button call-trigger' href='tel:+74959882656'><span>+7 495 988 26 56</span></a>
                        <?elseif(CITY_ID=="rga" || CITY_ID=="urm"):?>
                            <a class='button call-trigger' href="tel:+37166103106" ><span>+371 661 031 06</span></a>
                        <?endif;?>

                        <?if (CITY_ID=="msk" || CITY_ID=="spb" || CITY_ID=="rga" || CITY_ID=="urm"):?>
                            <a class="button boo ajax reserve-trigger-link" href="/booking/?id=<?= $arItem['ID'] ?>&CITY_ID=<?=CITY_ID?>"><span>Забронировать</span></a>
                        <?endif;?>

                        <a class="button add-too-favorite-on-list <?if (CITY_ID!="msk" && CITY_ID!="spb" && CITY_ID!="rga" && CITY_ID!="urm"):?>full-button<?endif;?>" href="/reviews/?id=<?=$arItem['ID']?>&CITY_ID=<?=CITY_ID?>">Оставить отзыв</a>
                    </div>
                </div>
            </div>
<!--            <div class="item_back">-->
<!--                --><?//if (CITY_ID=="spb"):?>
<!--                    <a href='tel:+78127401820'>Позвонить</a>-->
<!--                --><?//elseif(CITY_ID=="msk"):?>
<!--                    <a href='tel:+74959882656'>Позвонить</a>                               -->
<!--                --><?//endif;?><!--                     -->
<!--                <a class="ajax" href="/booking/?id=--><?//=$arItem['ID']?><!--">Забронировать столик</a>-->
<!--                <a class="" href="/reviews/?CITY_ID=--><?//=CITY_ID?><!--&id=--><?//=$arItem['ID']?><!--">Оставить отзыв</a>                -->
<!--            </div>-->
        </div>
    <?endforeach;?>        
<?else:?>
    <h3>К сожалению, ничего не найдено</h3>
<?endif?>
<?if ($arParams["aja"]!="Y"):?>
</div>
<?endif;?>
<script>
    $(function(){                
        $(".item").on('click', function(e) {
            if ( $(this).find(".pic").css("margin-left")=="0px")
            {
                var a = ($(this).find(".item_back").width())*(-1);
                $(this).find(".pic").css("margin-left",a+"px");
                $(this).find(".item_back").show();
            }
            else
            {
                var a = $(this).find(".item_back").width();
                $(this).find(".pic").css("margin-left","0px");
            }
        });         
//        $(".item .info a").click(function(e){
//            e.stopPropagation();            
//        })
//        $(".item").on('swiperight', function(e) {
//            var a = $(this).find(".item_back").width();
//            $(this).find(".pic").css("margin-left","0px");
//        });        
    });
</script>