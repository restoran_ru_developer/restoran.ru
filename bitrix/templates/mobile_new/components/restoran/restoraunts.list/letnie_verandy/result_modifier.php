<? //
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "address", CITY_ID=='urm'||CITY_ID=='rga'?"area":'','photos'
);

$arIB = getArIblock("catalog", CITY_ID);

foreach($arResult["ITEMS"] as $cell=>&$arItem) {
//    FirePHP::getInstance()->info($arItem['DISPLAY_PROPERTIES']['RESTORAN']['DISPLAY_VALUE'],$arItem['DISPLAY_PROPERTIES']['RESTORAN']['VALUE'][0]);

//    $temp_name_str = is_array($arItem['DISPLAY_PROPERTIES']['RESTORAN']['DISPLAY_VALUE'])?$arItem['DISPLAY_PROPERTIES']['RESTORAN']['DISPLAY_VALUE'][0]:$arItem['DISPLAY_PROPERTIES']['RESTORAN']['DISPLAY_VALUE'];
//    $arItem['NAME'] = strip_tags($arItem['~NAME']);
    $arItem['NAME'] = $arItem['~NAME'];
    $arItem['ID'] = $arItem['DISPLAY_PROPERTIES']['RESTORAN']['VALUE'][0];
    foreach($arFormatProps as $pid=>$arProperty) {

        $db_props = CIBlockElement::GetProperty($arIB['ID'], $arItem['DISPLAY_PROPERTIES']['RESTORAN']['VALUE'][0], "sort", "asc", Array("CODE"=>$arProperty));
        if($ar_props = $db_props->Fetch()){
//            FirePHP::getInstance()->info($ar_props);
            if($ar_props['PROPERTY_TYPE']=='E'){
                $res = CIBlockElement::GetById($ar_props['VALUE']);
                if($ar_res = $res->Fetch()){
                    $arItem['DISPLAY_PROPERTIES'][$arProperty]['DISPLAY_VALUE'] = $ar_res['NAME'];
                }
            }
            else {
                $arItem['DISPLAY_PROPERTIES'][$arProperty]['DISPLAY_VALUE'] = $ar_props['VALUE'];
            }

            if($arProperty=='photos'){
                if($ar_props["VALUE"])
                {
                    $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($ar_props["VALUE"], array('width' => 848, 'height' => 420), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
                }
                else {
                    $res = CIBlockElement::GetById($arItem['DISPLAY_PROPERTIES']['RESTORAN']['VALUE']);
                    if($ar_res = $res->Fetch()){
                        if($ar_res["DETAIL_PICTURE"])
                        {
                            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($ar_res['DETAIL_PICTURE'], array('width'=>848, 'height'=>420), BX_RESIZE_IMAGE_PROP, true, Array());
                        }
                        else
                            $arItem["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
                    }
                }
            }
        }
    }
}
$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult['COUNT'] = $arResult["NAV_RESULT"]->NavPageCount;	
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
        $cp->SetResultCacheKeys(array('COUNT'));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
	if (!isset($arResult['COUNT']))
	{
		$arResult['COUNT'] = $cp->arResult['COUNT'];		
	}
}
?>