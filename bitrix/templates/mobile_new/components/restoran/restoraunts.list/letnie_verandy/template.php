<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["aja"]!="Y"):?>
<div class="items rlist">
<?endif;?>    
<? if (count($arResult["ITEMS"]) > 0): ?>            
    <?
    foreach ($arResult["ITEMS"] as $cell => $arItem):  	                      
        $lat1 = (float) $arItem["DISPLAY_PROPERTIES"]['lat']["DISPLAY_VALUE"];
        $lon1 = (float) $arItem["DISPLAY_PROPERTIES"]['lon']["DISPLAY_VALUE"];
        $lat2 = (float) $_SESSION['lat'];
        $lon2 = (float) $_SESSION['lon'];

        if ($_REQUEST["pageRestSort"]=="distance")
            $dist = $arItem["DISTANCE"];
        else
            $dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
        if ($dist < 1000)
            $distance = round($dist,0) . ' м';
        else
            $distance = round($dist/1000).' км';

        $arIB = getArIblock("catalog", CITY_ID);
        $arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'] = str_replace($arIB["NAME"].", ", "", $arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE']);
        $arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'] = str_replace("г.", "", $arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE']);
        ?>
        <div class="item sl <?=($cell==0)?"first":""?>" id="in-list-id-<?=$arItem['ID']?>">
           <a class="ajax jj" href="/detail.php?ID=<?= $arItem["ID"] ?>">
            <div class="pic" style='background:url(<?=$arItem['PREVIEW_PICTURE']['src']?>) no-repeat;'>
                <div class="info">
                    
                    <div class='left'>
                        <h2><?=(strlen($arItem['NAME'])>23?substr($arItem['NAME'],0,23).'...':$arItem['NAME']);?></h2>
                        <h3><?=(strlen($arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])>30?substr($arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'],0,30).'...':$arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE']);?></h3>
                            <?if($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']):?>
                                <h3 class='subway'>м.
                                    <?if (is_array($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])):
                                        echo strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'][0]);
                                    else:
                                        echo strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
                                    endif;?>
                                </h3>
                            <?else:
                                echo "<h3 class='subway ".(CITY_ID=='rga'||CITY_ID=='urm'?'no-item':'')."'>".(CITY_ID=='rga'||CITY_ID=='urm'?'Р-н: '.$arItem["DISPLAY_PROPERTIES"]['area']["DISPLAY_VALUE"].' ':'')."(и еще ".(count($arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])-1)." ".  pluralForm((count($arItem['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])-1), "адрес", "адреса", "адресов").")</h3>";
                            endif;?>
                    </div>
                    <div class='right'>
                        <span class='time'><?=$distance ?></span>
                        <?if (is_array($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])):
                            preg_match_all("/\d+/", strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'][0]), $r);
                        else:
                            preg_match_all("/\d+/", strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']), $r);
                        endif;?>
                        <? if (end($r)): ?>
                            <span
                                class="average_bill <?= (CITY_ID == "rga" || CITY_ID == "urm") ? "euro" : "" ?>"><?= end($r[0]) ?></span>
                        <? endif; ?>
			        </div>

                </div>
                <a href="/tpl/ajax/get_favorite_mobile_cookie.php" data-toggle="favorite"
                   data-restoran="<?= $arItem['ID'] ?>" class="button add-too-favorite-on-list-in-content"><span>В избранное</span>
                </a>
                <div class="rest-buttons">
                    <div>
                        <?if (CITY_ID=="spb"):?>
                            <a class='button call-trigger' href='tel:+78127401820'><span>+7 812 740 18 20</span></a>
                        <?elseif(CITY_ID=="msk"):?>
                            <a class='button call-trigger' href='tel:+74959882656'><span>+7 495 988 26 56</span></a>
                        <?elseif(CITY_ID=="rga" || CITY_ID=="urm"):?>
                            <a class='button call-trigger' href="tel:+37166103106" ><span>+371 661 031 06</span></a>
                        <?endif;?>

                        <?if (CITY_ID=="msk" || CITY_ID=="spb" || CITY_ID=="rga" || CITY_ID=="urm"):?>
                            <a class="button boo ajax reserve-trigger-link" href="/booking/?id=<?= $arItem['ID'] ?>&CITY_ID=<?=CITY_ID?>"><span>Забронировать</span></a>
                        <?endif;?>

<!--                        <a class="button add-too-favorite-on-list --><?//if (CITY_ID!="msk" && CITY_ID!="spb" && CITY_ID!="rga" && CITY_ID!="urm"):?><!--full-button--><?//endif;?><!--" href="/reviews/?id=--><?//=$arItem['ID']?><!--&CITY_ID=--><?//=CITY_ID?><!--">Оставить отзыв</a>-->
                        <div class="button add-too-favorite-on-list how-to-get-button <? if (CITY_ID != "msk" && CITY_ID != "spb" && CITY_ID != "rga" && CITY_ID != "urm"):?>full-button<? endif;?>"
                           >
                            <div class="how-to-get-icon-wrapper">
                                <ul>
                                    <li><span this_lat="<?=$lat1?>" this_lon="<?=$lon1?>" this_mode="DRIVING">Личный транспорт</span></li>
                                    <li><span this_lat="<?=$lat1?>" this_lon="<?=$lon1?>" this_mode="TRANSIT">Общественный транспорт</span></li>
                                    <li><span this_lat="<?=$lat1?>" this_lon="<?=$lon1?>" this_mode="WALKING">Пешком</span></li>
                                </ul>
                            </div>
                            <span>Как добраться</span>
                        </div>
                    </div>
                </div>
            </div>
               </a>
        </div>
    <?endforeach;?>        
<?else:?>
    <h3>К сожалению, ничего не найдено</h3>
<?endif;?>
<?if ($arParams["aja"]!="Y"):?>
</div>
<?endif;?>
<?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
<script>
    $(function(){       
          $('.item').unbind("movestart");
          $('.item').unbind("swipeleft");
          $('.item').unbind("swiperight");
          $('.item').on('movestart', function(e) {  
            if ((e.distX > e.distY && e.distX < -e.distY) ||
                (e.distX < e.distY && e.distX > -e.distY)) {
              e.preventDefault();              
            }
          });
        
        $(".item").on('swipeleft.click', function(e) {
            e.stopPropagation();
            var a = ($(this).find(".item_back").width())*(-1);
            $(this).find(".pic").css("margin-left",a+"px");            
//            if ($(this).hasClass("first"))
//            {                
//                $(this).find(".pic .left h2").css("transform","translate("+(-1)*a+"px)");
//            }
            $(this).find(".item_back").show();
            return false;
        });
         $(".jj").on('click', function(e) {
            return true;
        });
        $(".item").on('swiperight', function(e) {
            var a = $(this).find(".item_back").width();
            $(this).find(".pic").css("margin-left","0px");
        });

    });
</script>
<?endif;?>

<?//FirePHP::getInstance()->info($_SERVER['HTTP_REFERER'],'____')?>