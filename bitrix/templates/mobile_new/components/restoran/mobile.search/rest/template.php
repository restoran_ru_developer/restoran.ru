<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="items search-page rlist">
    <input type="hidden" id="q" value="<?= $_REQUEST["q"] ?>">
    <input type="hidden" id="processing" value="0">
    <input type="hidden" id="maxpage" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
    <?
    //if (count($arResult["ITEMS"]) == 1)
        //LocalRedirect('/detail.php?ID=' . $arResult["ITEMS"][0]["ELEMENT"]["ID"].'&ajax=Y');    
    ?>
    <? if (count($arResult["ITEMS"]) > 0): ?>            
            <?
            foreach ($arResult["ITEMS"] as $cell => $arItem):                  
                $lat1 = (float) $arItem["ELEMENT"]["PROPERTIES"]['lat']["VALUE"][0];
                $lon1 = (float) $arItem["ELEMENT"]["PROPERTIES"]['lon']["VALUE"][0];
                $lat2 = (float) $_SESSION['lat'];
                $lon2 = (float) $_SESSION['lon'];
                $dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
                if ($dist < 1000) {
                    $distance = $dist . ' м';
                } elseif (($dist > 1000 && $dist < 10000)) {
                    $distance = substr($dist, 0, 1) . '.' . substr($dist, 1, 1) . ' км';
                } elseif (($dist > 10000 && $dist < 100000)) {
                    $distance = substr($dist, 0, 2) . '.' . substr($dist, 2, 1) . ' км';
                }            
                $address = explode(', ', $arItem["ELEMENT"]["PROPERTIES"]['address']['VALUE'][0]);
                unset($address[0]);
                $address = implode(', ', $address);
                ?>
                <div class="item sl">
                    <a class="ajax jj" href="/detail.php?ID=<?= $arItem["ELEMENT"]["ID"] ?>">
                    <div class="pic" style='background:url(<?=$arItem["ELEMENT"]['PREVIEW_PICTURE']['src']?>) no-repeat;'>
                        <div class="info">

                                <div class='left'>
                                        <h2><?=(strlen($arItem["ELEMENT"]['NAME'])>20?substr($arItem["ELEMENT"]['NAME'],0,20).'...':$arItem["ELEMENT"]['NAME']);?></h2>
                                        <h3><?=(strlen($address)>30?substr($address,0,30).'...':$address);?></h3>
                                        <h3 class='subway'>м. 
                                            <?if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])):
                                                echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'][0]);
                                            else:
                                                echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
                                            endif;?>                                                                                    
                                        </h3>
                                </div>
                                <div class='right'>
                                        <span class='time'><?= $distance ?></span>
                                        <?if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])):
                                                preg_match_all("/\d+/", strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'][0]),$r);            
                                            else:
                                                preg_match_all("/\d+/", strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']),$r);            
                                            endif;?>                         
                                        <?if (end($r[0])):?>
                                        <span class="average_bill"><?=end($r[0])?></span>
                                        <?endif;?>
                                </div>
                        </div>
                        <div class="rest-buttons">
                            <div>
                                <?if (CITY_ID=="spb"):?>
                                    <a class='button call-trigger' href='tel:+78127401820'><span>+7 812 740 18 20</span></a>
                                <?elseif(CITY_ID=="msk"):?>
                                    <a class='button call-trigger' href='tel:+74959882656'><span>+7 495 988 26 56</span></a>
                                <?elseif(CITY_ID=="rga" || CITY_ID=="urm"):?>
                                    <a class='button call-trigger' href="tel:+37166103106" ><span>+371 661 031 06</span></a>
                                <?endif;?>

                                <?if (CITY_ID=="msk" || CITY_ID=="spb" || CITY_ID=="rga" || CITY_ID=="urm"):?>
                                    <a class="button boo ajax reserve-trigger-link" href="/booking/?id=<?= $arItem['ID'] ?>&CITY_ID=<?=CITY_ID?>"><span>Забронировать</span></a>
                                <?endif;?>

                                <a class="button add-too-favorite-on-list <?if (CITY_ID!="msk" && CITY_ID!="spb" && CITY_ID!="rga" && CITY_ID!="urm"):?>full-button<?endif;?>" href="/reviews/?id=<?=$arItem['ID']?>&CITY_ID=<?=CITY_ID?>">Оставить отзыв</a>
                            </div>
                        </div>
                    </div>
                    </a>

                    <!--                    <div class="item_back">-->
<!--                        <a href="#">Позвонить</a>-->
<!--                        <a class="ajax" href="/booking/?id=--><?//=$arItem["ELEMENT"]['ID']?><!--">Забронировать столик</a>-->
<!--                        <a class="" href="/reviews/?CITY_ID=--><?//=CITY_ID?><!--&id=--><?//=$arItem["ELEMENT"]['ID']?><!--">Оставить отзыв</a>                -->
<!--                    </div>-->
                </div>
            <?endforeach;?>        
    <?elseif ($_REQUEST["q"]):?>    
        <h2>К сожалению, по вашему запросу ничего не найдено.</h2>
    <?endif?>
</div>
<script>
    $(function(){       
          $('.item').unbind("movestart");
          $('.item').unbind("swipeleft");
          $('.item').unbind("swiperight");
          $('.item').on('movestart', function(e) {  
            if ((e.distX > e.distY && e.distX < -e.distY) ||
                (e.distX < e.distY && e.distX > -e.distY)) {
              e.preventDefault();              
            }
          });
        
        $(".item").on('swipeleft', function(e) {
            var a = ($(this).find(".item_back").width())*(-1);
            $(this).find(".pic").css("margin-left",a+"px");
            $(this).find(".item_back").show();
        });
        $(".item").on('swiperight', function(e) {
            var a = $(this).find(".item_back").width();
            $(this).find(".pic").css("margin-left","0px");
        });

    });
</script>