<?  
CModule::IncludeModule("iblock");
$res = CIBlockElement::GetByID($arParams["ELEMENT_ID"]);
if ($ar = $res->Fetch())
{
     $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'mr',
        'size'=>'small',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/images/watermark1.png', 
        ),
    );        
    $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"photos"));
    if($ar_props = $db_props->Fetch())
        $photo = IntVal($ar_props["VALUE"]);
    else
        $photo = false;         
             
    if (!$photo)
        $arResult["RESTORAN_PIC"] = CFile::ResizeImageGet($ar['DETAIL_PICTURE'], array('width'=>848, 'height'=>420), BX_RESIZE_IMAGE_EXACT, true, $watermark);
    else
        $arResult["RESTORAN_PIC"] = CFile::ResizeImageGet($photo, array('width'=>848, 'height'=>420), BX_RESIZE_IMAGE_EXACT, true, $watermark);
    $arResult["RESTORAN_ID"] = $ar["ID"];
    $arResult["RESTORAN_NAME"] = $ar["NAME"];
    $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
    if($ar_props = $db_props->Fetch())
    {
        $arResult["RESTORAN_ADRES"] = str_replace("Москва,","",$ar_props["VALUE"]);
        $arResult["RESTORAN_ADRES"] = str_replace("Санкт-Петербург,","",$ar_props["VALUE"]);
    }
    $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"without_reviews"));
    if($ar_props = $db_props->Fetch())
    {
        $arResult["RESTORAN_WR"] = $ar_props["VALUE"];        
    }
}
?>