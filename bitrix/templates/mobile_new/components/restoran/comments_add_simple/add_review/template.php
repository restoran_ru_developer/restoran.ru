<?
$APPLICATION->AddHeadString('<link href="' . $templateFolder . '/fineuploader.css" rel="stylesheet" type="text/css"/>');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<div class='booking review'>
    <?if ($arResult["RESTORAN_NAME"]):?>
        <div class="rest" style='background-image: url(<?=$arResult["RESTORAN_PIC"]["src"]?>);'>
            <div class="info">
                <h2><?=$arResult["RESTORAN_NAME"]?></h2>
                <h3><?=$arResult["RESTORAN_ADRES"]?></h3>
            </div>
        </div>
    <?endif;?>
    <?if (!$arResult["RESTORAN_WR"]):?>
    <form name="attach" action="/ajax/ajax_comment.php" method="post" id="comment_form" enctype="multipart/form-data">
        <?= bitrix_sessid_post() ?>
        <input type="text" name="password" value="" class="password-field">
        <?
        if ($arResult["RESTORAN_ID"])
            echo "<input type='hidden' id='ELEMENT_ID' name='ELEMENT_ID' value='".$arResult["RESTORAN_ID"]."' />";
        ?>
        <div class="line">
                <label ><?= GetMessage("CLICK_TO_RATE") ?></label>
                <ul class="stars_new">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>                
        </div>
        <? if (!$USER->IsAuthorized()): ?>
            <div class="line">               
                <input name='email' req="req"  placeholder='E-mail' type="email" style="font-size:16px;padding-left:20px;padding-right:20px;">
            </div>
        <?endif;?>        
        <?/* if (!$USER->IsAuthorized()): ?>    
            <?
                $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form", "no_auth", Array(
                        "REGISTER_URL" => "",
                        "FORGOT_PASSWORD_URL" => "", 
                        "PROFILE_URL" => "",
                        "SHOW_ERRORS" => "N"
                    ), false
                );
            ?>         
        <? endif; */?>        
        <div class="line text" style="height:201px;">
           <textarea class="add_review" id="review" placeholder='Отзыв' name="review" req="req"></textarea>           
           <input type="hidden" value="" id="input_ratio" name="ratio" />                               
           <div id="attach_photo"></div>
           <div class="file_upload">Прикрепить фото</div>
           <div class="clear"></div>
           <div class="img-container" id="img-container">
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
<!--        --><?//if (!$USER->IsAuthorized()): ?>
<!--            <div class="line" style="position: relative; margin-top: 20px;">                        -->
<!--                <div class="QapTcha"></div>-->
<!--                <link rel="stylesheet" href="/tpl/js/quaptcha_mobile/QapTcha.jquery.css" type="text/css" />-->
<!--                <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>-->
<!--                <script type="text/javascript" src="/tpl/js/quaptcha_mobile/jquery.ui.touch.js"></script>-->
<!--                <script type="text/javascript" src="/tpl/js/quaptcha_mobile/QapTcha.jquery.js"></script>-->
                <script type="text/javascript">
//                    $(document).ready(function(){
//                        $('.QapTcha').QapTcha({
//                            txtLock : '<?//= GetMessage("MOVE_SLIDER") ?>//',
//                            txtUnlock : '<?//= GetMessage("MOVE_SLIDER_DONE") ?>//',
//                            disabledSubmit : true,
//                            autoRevert : true,
//                            PHPfile : '/tpl/js/quaptcha_mobile/Qaptcha.jquery.php',
//                            autoSubmit : false});
//                    });
                </script>
<!--            </div>-->
<!--         --><?//else:?>
<!--            <div class="line" style="min-height:0px;"></div>-->
<!--        --><?//endif;?>
        <div class="line">
            <input style="width: 100%;" <?// if (!$USER->IsAuthorized()){ echo 'disabled="disabled"';} ?> type="submit" id="add_commm" class="light_button add-comm-mobile" value="<?= GetMessage("REVIEWS_PUBLISH") ?>">       
        </div>
        <input type="hidden" name="IBLOCK_TYPE" value="<?= $arParams["IBLOCK_TYPE"] ?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
        <input type="hidden" name="IS_SECTION" value="<?= $arParams["IS_SECTION"] ?>" />

        <div class="line small">
            <?= GetMessage("R_TERMS") ?> 
        </div>
        <br /><br />
        <div class="popup review add_comment">
            <div class="inner">
                <div class="head">
                    <h2 ><?= GetMessage("CLICK_TO_RATE") ?></h2>
                </div>
                <div class="body">            
                    <ul class="stars_new">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <input style="width: 80%; margin-bottom:40px;" type="submit" class="light_button add-comm-mobile" value="Продолжить">       
                </div>
            </div>
            <div class="close"></div>       
        </div>
    </form>
    <?else:?>
        <?//if ($arParams["ELEMENT_ID"]!=976505):?>
            <h3>По просьбе ресторана возможность оставлять отзывы о работе заведения на данной странице заблокирована</h3>
        <?//endif;?>
    <?endif;?>
</div>
<?if (!$arResult["RESTORAN_WR"]):?>
    <script>
        $(document).ready(function(){        

            var files = new Array();
            var errorHandler = function(event, id, fileName, reason) {
                qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
            };
            $('#attach_photo').fineUploader({
                text: {
                    uploadButton: "",
                    cancelButton: "",
                    waitingForResponse: ""
                },
                multiple: false,
                disableCancelForFormUploads: true,
                request: {
                    endpoint: "/ajax/file_upload.php",
                    params: {"generateError": true,"f":"images"}
                },
                validation:{
                    allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
                },
                failedUploadTextDisplay: {                
                    maxChars: 5
                },
                messages: {
                    typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: jpg, gif, png"
                }
            })
            .on('upload', function(id, fileName){
                $(".qq-upload-list").show();
            })
            .on('error', errorHandler)
            .on('complete', function(event, id, fileName, response) {
                if (response.success)
                {
                    files.push(response.resized);            
                    $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                    $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                    $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
                    $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));                        
                    $(".qq-upload-list").hide();
                    $(".qq-upload-success").remove();
                }
            }); 

            $("#wr_cm").click(function(){
                $(this).hide();
                $("#write_comment").show("500");                
            });                 
            $(".img-container").on("click",".remove-link",function(){
                var file = $(this).next().attr("src");
                $(this).parents(".image-block").remove();                
                return false;
            });

            $("#comment_form").keypress(function(e){
                e = e || window.event;    
                //for chrome & safari
                if (e.ctrlKey) {
                    if(e.keyCode == 10){
                        $("#comment_form").submit();
                        return false;
                    }
                };
                //for firefox
                if (e.keyCode == 13 && e.ctrlKey) {
                    $("#comment_form").submit();
                    return false;
                };
            });
            $("#comment_form").submit(function(){
                if (!$(this).find("#review").val())
                {
                    alert("Введите комментарий!");
                    return false;
                }
                if (!$(this).find("#input_ratio").val())
                {
                    $(".popup").show();
                    //alert("Оцените ресторан!");
                    return false;
                }
//                $("#add_commm").attr("disabled",true);
                $(".add-comm-mobile").attr("disabled",true);
                var params = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: params,
                    success: function(data) {
                        if(data.trim() !== '')
                        {
                            data = eval('('+data+')');
                            if (data.ERROR=="1")
                            {
                                alert(data.MESSAGE);
//                                $("#add_commm").attr("disabled",false);
                                $(".add-comm-mobile").attr("disabled",false);
                            }
                            if (data.STATUS=="0")
                            {
                                alert(data.MESSAGE);
//                                $("#add_commm").attr("disabled",false);
                                $(".add-comm-mobile").attr("disabled",false);
                            }
                            if (data.STATUS=="1"){
                                alert('Спасибо! Ваш комментарий добавлен.')
                                history.back();
                                var url = "/detail.php?ID="+$("#ELEMENT_ID").val();
                                location.href = url;
                                //setTimeout("location.reload()","100");
                            }
                        }
                    }
                });
                return false; 
            });

            $(".menu").unbind("click");
            $(".logo").html("<h1>Отзыв</h1><h4>").attr("href","javascript:void(0)").removeClass("ajax");
            $(".logo h1").css("line-height",$('header').height()-14+"px");
            $(".app-bar-actions").remove();
            $(".menu").after($(".menu").clone().removeClass("menu").addClass("arrow_r"));//.removeClass("menu").addClass("arrow_r")));                
            $(".arrow_r").find("img").attr("src",$(".arrow_r img").attr("src").replace("menu.png","arrow_r.png"));
            $(".menu").hide();
            //$(".menu").addClass("arrow_r").removeClass("menu");
            $(".arrow_r,.logo").click(function(){
                if (history.length>2)
                    history.back();
                else
                    location.href = '/detail.php?ID=<?=$_REQUEST["id"]?>';
            });

        });

    </script>
<?endif;?>