<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

if($arResult['SHOW_COUNTER_WORK']&&$arResult['SHOW_COUNTER']){
    $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), Array("CODE"=>"SHOW_COUNTER"));
    if($ar_props = $db_props->Fetch()){
        $sleeping_val_id = array(
            'spb'=>1468,
            'msk'=>1419,
            'urm'=>2573,
            'rga'=>2556
        );

        if($ar_props["VALUE"]!=''&&$arResult['SHOW_COUNTER']!=$ar_props['DESCRIPTION']){    //  в DESCRIPTION - записываeтся SHOW_COUNTER
            if($ar_props["VALUE"]<1){
                CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arResult['IBLOCK_ID'], array('sleeping_rest' => $sleeping_val_id[CITY_ID]));
                CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arResult['IBLOCK_ID'], array('SHOW_COUNTER' => ''));

                $el = new CIBlockElement;
                $el->Update($arResult["ID"], Array(), false, true, true);

//                //  отправка письма

                $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $element_id, array("sort" => "asc"), Array("CODE"=>"user_bind"));
                while($ar_props = $db_props->Fetch()){
                    $restorator_id = $ar_props['VALUE'];
                    if($restorator_id){
                        $rs = CUser::GetByID($restorator_id);
                        if($arUser = $rs->Fetch()){
                            if($arUser["EMAIL"]){
                                $mail_message = "у ресторана ".$RESTORAN_NAME." закончилось количество показов, ресторан переводится в спящий режим.";
                                $arMessage = Array(
                                    "MESSAGE"=> $mail_message,
                                    'EMAIL_TO'=>$arUser["EMAIL"]
                                );
                                $res = CEvent::Send("SHOW_COUNTER_END", "s1", $arMessage);
                            }
                        }
                    }
                }




            }
            else {
                $deduct_count = $arResult['SHOW_COUNTER']-$ar_props['DESCRIPTION'];
                $prop = $ar_props["VALUE"] - $deduct_count;

                $prop = array("VALUE"=>$prop,'DESCRIPTION'=>$arResult['SHOW_COUNTER']);

                CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arResult['IBLOCK_ID'], array('SHOW_COUNTER' => $prop));


                //  добавление статистики
                global $DB;

                $sql = "SELECT * FROM show_counter_statistic WHERE rest_id=".$arResult['ID']." AND stat_date='".date('Y-m-d')."'";
                $db_list = $DB->Query($sql);
                if($ar_result = $db_list->Fetch())
                {
                    //update

                    $show_counter_day_count_val =  $deduct_count+$ar_result['count_val'];
                    $sql = "UPDATE show_counter_statistic SET count_val=".$show_counter_day_count_val." WHERE rest_id=".$arResult['ID']." AND stat_date='".date('Y-m-d')."'";
                    $db_list = $DB->Query($sql);
                    $db_list->Fetch();
                }
                else {
                    $sql = "INSERT INTO show_counter_statistic (rest_id, stat_date, count_val) VALUES(".$arResult['ID'].",'".date('Y-m-d')."',".$deduct_count.")";
                    $db_list = $DB->Query($sql);
                    $db_list->Fetch();
                }


                if($prop['VALUE']==50){
                    //  отправка письма
                    $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $element_id, array("sort" => "asc"), Array("CODE"=>"user_bind"));
                    while($ar_props = $db_props->Fetch()){
                        $restorator_id = $ar_props['VALUE'];
                        if($restorator_id){
                            $rs = CUser::GetByID($restorator_id);
                            if($arUser = $rs->Fetch()){
                                if($arUser["EMAIL"]){
                                    $mail_message = "у ресторана ".$RESTORAN_NAME." заканчивается количество показов, через 50 показов ресторан будет переведен в спящий режим.";
                                    $arMessage = Array(
                                        "MESSAGE"=> $mail_message,
                                        'EMAIL_TO'=>$arUser["EMAIL"]
                                    );
                                    $res = CEvent::Send("SHOW_COUNTER_END", "s1", $arMessage);
                                }
                            }
                        }
                    }

                }

            }

        }
    }
}