<div id="order">
    <div>
        <script>
            $(document).ready(function(){
                var params = {
                    changedEl: "#order_what"
                }
                cuSel(params);                                                             
                $(".whose_date").dateinput({lang: '<?= LANGUAGE_ID ?>', firstDay: 1 , format:"dd.mm.yyyy"});                                                                        
                $.maski.definitions['~']='[0-2]';                               
                $.maski.definitions['!']='[0-5]';                                  
                $(".time").maski("~9   !9",{placeholder:" "});

                $(".small_bron").click(function(){
                    var _this = $(this);
                    $.ajax({
                        type: "POST",
                        url: "/tpl/ajax/online_order_rest.php",
                        data: "what="+$("#order_what").val()+"&date="+$(".whose_date").val()+"&name=<?= $arResult["NAME"] ?>&id=<?= $arResult["ID"] ?>&time="+$(".time").val()+"&person="+$("#person").val(),
                        success: function(data) {
                            if (!$("#bron_modal").size())
                                $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                            $('#bron_modal').html(data);
                            showOverflow();
                            setCenter($("#bron_modal"));
                            $("#bron_modal").fadeIn("300"); 
                        }
                    });
                    return false;
                });

                $(".small_ord").click(function(){
                    var _this = $(this);
                    $.ajax({
                        type: "POST",
                        url: "/tpl/ajax/online_order_rest.php",
                        data: "what=2&date="+$(".whose_date").val()+"&name=<?= $arResult["NAME"] ?>&id=<?= $arResult["ID"] ?>&time="+$(".time").val()+"&person="+$("#person").val(),
                        success: function(data) {
                            if (!$("#bron_modal").size())
                                $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                            $('#bron_modal').html(data);
                            showOverflow();
                            setCenter($("#bron_modal"));
                            $("#bron_modal").fadeIn("300"); 
                        }
                    });
                    return false;
                });                                                                        
            });
        </script>
        <?/*if ($arResult["PROPERTIES"]["sale10"]["VALUE"]=="Да"):?>
            <div class="left" style="width:212px; height:122px; background:url(/tpl/images/order_form_svyaznoy.png) -10px -10px no-repeat;">
                
            </div>
        <?endif;*/?>
        <div class="left" <?//if ($arResult["PROPERTIES"]["sale10"]["VALUE"]=="Да") echo 'style="width:480px"'?>>            
            <? if (LANGUAGE_ID == "ru"): ?>
                <div class="left" style="<?= (LANGUAGE_ID == "en") ? "width:160px" : "width:215px;" ?>">
                    <select id="order_what" name="order_what" style="<?= (LANGUAGE_ID == "en") ? "width:150px" : "width:206px;" ?>">
                        <option selected="selected" value="0"><?= GetMessage("R_BOOK_TABLE") ?></option>
                        <option  value="1"><?= GetMessage("R_ORDER_BANKET") ?></option>
                        <option value="2"><?= GetMessage("R_BUFFET") ?></option>
                    </select>
                </div> <?= GetMessage("R_IN") ?> &nbsp;<span style="color:#24A6CF" class="font18"><?= $arResult["NAME"] ?></span><br />                            
            <? endif; ?>
            <? if (LANGUAGE_ID == "en"): ?>
                <input type="hidden" value="0" name="order_what">
                Make a reservation at &nbsp;<span style="color:#24A6CF" class="font18"><?= $arResult["NAME"] ?></span><br />                            
            <? endif; ?>
            <?= GetMessage("R_ON") ?> 
            <span style="position: relative; display: inline">
                <input class="whose_date" value="<?= date("d.m.Y") ?>" type="date" />
            </span>
            <?= GetMessage("R_AT") ?> <input type="text" class="time" value="" name="time"> <?= GetMessage("R_FOR") ?> 
            <input type="text" class="text" value="" size="2" maxlength="2" id="person" name="person">
            <?= GetMessage("R_PERSONS") ?>
            <div class="phone left">
                <? if (LANGUAGE_ID == "ru"): ?>
                    <?= $arResult["CONTACT_DESCRIPTION"] ?>                      
                <? endif; ?>
                <? if (LANGUAGE_ID == "en"): ?>
                <? //var_dump($arResult["CONTACT_DESCRIPTION"]);
                $arResult["CONTACT_DESCRIPTION"] = '<p>Banquet organization and tables reservation +7 (812) 740-18-20</p> 
                    <p>Make restaurant reservations - we speak English.</p>'?>
                    <? //echo str_replace('Организация банкетов и заказ столиков', 'Banquet organization and tables reservation.<br> Make restaurant reservations - we speak English.', $arResult["CONTACT_DESCRIPTION"]) ?> 
                <? echo $arResult["CONTACT_DESCRIPTION"]; ?> 
                <? endif; ?>
            </div>
            <div class="right" style="font-size:12px; margin-top:25px;width:134px;<? if (LANGUAGE_ID == "en"){ echo 'width:100px;';} ?>" align="right">
                <input type="button" class="small_bron light_button" value="<?= GetMessage("R_BOOK") ?>" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <!--<div class="right">

        <div class="coupon">
            -50% <span>скидка</span><br />
            <a href="#">Купить купон</a>
        </div>

    </div>-->
</div>