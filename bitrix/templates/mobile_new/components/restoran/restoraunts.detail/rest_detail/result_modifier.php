<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek","opening_hours","children","credit_cards"
);


foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $old_val = $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"];
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = array();
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][0] = strip_tags($old_val);
        }
    }
}
if (LANGUAGE_ID=="en")
{
    foreach ($arResult["DISPLAY_PROPERTIES"] as &$properties)
    {        
            if (is_array($properties["DISPLAY_VALUE"]))
            {                
                if (is_array($properties["VALUE"]))
                {                    
                    foreach($properties["VALUE"] as $key=>$val)
                    {
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {                                                
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);                        
                        }            
                    }
                }
                else
                {
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {           
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                    } 
                }
            }
            else
            {
                
                $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                if ($ar = $r->Fetch())
                {   
                    if ($ar["PROPERTY_ENG_NAME_VALUE"])
                        $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);

                }   
            }  
            if ($properties["CODE"]=="wi_fi")
                    $properties["DISPLAY_VALUE"] = "Yes";
            
    }
}

$old_val = "";
if ($arResult["PROPERTIES"]["network"]["VALUE"])
{
    $i=1;
    $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"],"PROPERTY_network"=>$arResult["PROPERTIES"]["network"]["VALUE"],"!ID"=>$arResult["ID"]),false,false,Array("ID", "DETAIL_PAGE_URL","PROPERTY_address","PROPERTY_phone","PROPERTY_subway"));
    while ($ar = $res->GetNext())
    {
        //v_dump($ar);
        $arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][$i] = $ar["PROPERTY_ADDRESS_VALUE"];
        ?>
        <?$arResult["DISPLAY_PROPERTIES"]["network_url"][$i] = $ar["DETAIL_PAGE_URL"];
        $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][$i] = $ar["PROPERTY_PHONE_VALUE"];
        
        $rrr = CIBlockElement::GetByID($ar["PROPERTY_SUBWAY_VALUE"]);
        if ($arr = $rrr->Fetch())
            $arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][$i] = $arr["NAME"];
        $i++;
    }  
}

$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut($arResult["DETAIL_TEXT"], $truncate_len);

 $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'medium',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark1.png', 
        ),
    );
 if ($arResult["PROPERTIES"]["photos"]["VALUE"][0])
 {
    $p=0;
    foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
    {
        //$file = array();
        //$file = CFile::GetFileArray($photo);
        //if ($file["WIDTH"]>600||$file["HEIGHT"]>600)
        if ($key<5)
        {       
            $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($photo,Array("width"=>864,"height"=>420),BX_RESIZE_IMAGE_EXACT,true,$watermark);
            
        }
        else
        {
            $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = $photo;
        }
        $p++;
    }
 }
 else {
     $arResult["PROPERTIES"]["photos"]["VALUE2"][0] = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"],Array("width"=>864,"height"=>420),BX_RESIZE_IMAGE_EXACT,true,$watermark);
 }

if ($arResult["PROPERTIES"]["sleeping_rest"]["VALUE"]!="Да")
{
    global $DB;
    $sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a 
            JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=".$DB->ForSql($arResult["ID"])." ";
    $db_list = $DB->Query($sql);
    if($ar_result = $db_list->GetNext())
    {    
        if ($ar_result["ELEMENT_CNT"]>0){
            $arResult["MENU_ID"] = $ar_result['ID'];


        }
    }
}



//$res = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>101,"CODE"=>CITY_ID));
//if ($ars = $res->GetNext())
//{
//    if($ars["DESCRIPTION"])
//    {
//        $ars["DESCRIPTION"] = explode("<i>",$ars["DESCRIPTION"]);
//        $arResult["CONTACT_DESCRIPTION"] = $ars["DESCRIPTION"][0];
//
//        }
//}
//$APPLICATION;
//$cp = $this->__component; // объект компонента
//if (is_object($cp))
//{
//	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
//	$cp->arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]["SRC"];
//	$cp->arResult['TEXT'] = strip_tags($arResult["PREVIEW_TEXT"]);
//	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
//        $cp->SetResultCacheKeys(array('IMAGE','TEXT'));
//	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
//	if (!isset($arResult['IMAGE']))
//	{
//		$arResult['IMAGE'] = $cp->arResult['IMAGE'];
//		$arResult['TEXT'] = $cp->arResult['TEXT'];
//	}
//}

//if($arResult["PREVIEW_PICTURE"])
//    $arResult["PREVIEW_PICTURE"]['AVATAR'] = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"],Array("width"=>96,"height"=>96),BX_RESIZE_IMAGE_EXACT,true);

$cp = $this->__component; // объект компонента
if (is_object($cp)) {
    if ($arResult['PROPERTIES']['SHOW_COUNTER']['VALUE'] != '') {
        $arResult["SHOW_COUNTER_WORK"] = true;
        $cp->arResult['SHOW_COUNTER_WORK'] = $arResult["SHOW_COUNTER_WORK"];

        $cp->SetResultCacheKeys(array('SHOW_COUNTER_WORK'));
    }
}
?>