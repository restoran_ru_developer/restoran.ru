<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<link rel="stylesheet" href="--><?//=$templateFolder?><!--/style.css" />-->

<div class="filter-page">
    <script>
        $(function(){
//            для строки поиска
            $("#restoran_input_field").keyup(function(){
                if ($("#restoran_input_field").length&&$("#restoran_input_field").val().length>2)
                {
                    if (null!=$(".restoran_suggest"))
                    {
                        var a = "search";
//                    if (location.pathname.indexOf("/search/")>=0)
//                        a = "search";
//                    else
//                        a = "";
                        $( ".restoran_suggest" ).load("/ajax/suggest.php", {"q":$("#restoran_input_field").val(),"sessid":$("#sessid").val(),"page":a}, function (data) {
                            if (data) {
                                $(".restoran_suggest").show();
                            }
                        });
                    }
                }
            });
        });
    </script>
    <div class="question search-field-wrapper">
        <div class='search'>
            <form id="search_form" action="/search/">
                <input id="restoran_input_field" class="filter-top-search-str" autocomplete="off" placeholder='ПОИСК ПО НАЗВАНИЮ' name='q' value="<?=$_REQUEST["q"]?>" type="text" />
                <input type="submit" class="top-search filter-top-search-str" value='Найти' />
                <div class="restoran_suggest"></div>
            </form>
        </div>
    </div>

    <?if ($arParams["NO_NAME"]!="Y"):?>
        <div class="new font18 center">Подобрать ресторан</div>
        <Br />
    <?endif;?>
    <?if ($APPLICATION->get_cookie("COORDINATES")=="Y"):?>
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" data-ajax="true" action="/index.php" method="GET">
    <?else:?>
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" data-ajax="true" action="/popular/" method="GET">
    <?endif;?>
            <input type="hidden" name="page" value="1" />

            <input type="hidden" name="by" value="asc" />

            <div class="questions-overflow-wrap">

                <?if(CITY_ID=="spb" || CITY_ID=="msk"):?>
                    <div class="question">
                        <div class="input only-link"><a class="ajax" href="/popular/">Популярные</a></div>
                    </div>
<!--                    <div class="question">-->
<!--                        <div class="input only-link"><a class="ajax" href="/letnie-verandy/">Летние веранды рядом</a></div>-->
<!--                    </div>-->
                <?endif?>

                <div class="question">
                    <div class="input only-link"><a class="ajax" href="/index.php?page=1&by=asc&arrFilter_pf%5Bfeatures%5D%5B%5D_flag=2392461&set_filter=Y">Круглосуточно</a></div>
                </div>

                <?
                foreach($arResult["arrProp"] as $prop_id => $arProp)
                {
                    $res = "";
                    $arResult["arrInputNames"][$FILTER_NAME."_pf"]=true;
                    if ($arProp["CODE"]!="wi_fi"&&$arProp["CODE"]!='subway_dostavka'&&$arProp["CODE"]!="hrs_24")
                    {
                        if ($arProp["PROPERTY_TYPE"]=="L" || $arProp["PROPERTY_TYPE"]=="E")
                        {
                            $name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
                            $value = $arrPFV[$arProp["CODE"]];
                            $res = "";
                            if ($arProp["MULTIPLE"]=="Y")
                            {
                                if (is_array($value) && count($value) > 0)
                                    ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
                            else
                            {
                                if (!is_array($value) && strlen($value) > 0)
                                    ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
                            $url = $APPLICATION->GetCurPage();
                            ?>

                            <div class="question">
                                <label for='<?=$arProp["CODE"]?>'><?=$arProp["NAME"]?></label>
                                <div class="input">
                                    <?if ($arProp["CODE"]=="subway"):?>
                                        <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" data-theme="d" multiple="multiple" id="<?=$arProp["CODE"]?>" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                                <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val["NAME"]?> <?if($arResult['CATEGORY_ELEMENT_COUNT'][$arProp["CODE"]][$key]>0):?>(<?=$arResult['CATEGORY_ELEMENT_COUNT'][$arProp["CODE"]][$key]?>)<?endif;?></option>
                                            <? endforeach; ?>
                                        </select>
                                    <?elseif ($arProp["CODE"]=="features"):?>
                                        <?
                                        ?>
                                        <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" data-theme="d" multiple="multiple"  id="<?=$arProp["CODE"]?>" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                            <?foreach($arProp["VALUE_LIST"] as $val):?>
                                                <option <?=(in_array($val['VALUE'], $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$val["CODE"]]))?"selected":""?> value="<?=$val["VALUE"]?>" to-change-name="<?=$val['CODE']?>"><?=$val["NAME"]?></option>
                                            <? endforeach; ?>
                                        </select>
                                    <?else:?>
                                        <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" data-theme="d" multiple="multiple" id="<?=$arProp["CODE"]?>" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                                <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?=htmlspecialchars($key) ?>"><?=$val?><?if($arProp["CODE"]=='out_city' || $arProp["CODE"]=='kitchen'):?> <?if($arResult['CATEGORY_ELEMENT_COUNT'][$arProp["CODE"]][$key]>0):?>(<?=$arResult['CATEGORY_ELEMENT_COUNT'][$arProp["CODE"]][$key]?>)<?endif;?><?endif?></option>
                                            <? endforeach; ?>
                                        </select>
                                    <?endif;?>
                                    <input type="hidden" id="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>_flag" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>_flag" value="0" />
                                </div>
                            </div>

                        <?
                        }
                    }
                }
                ?>
            </div>

            <div class="line2"></div>
            <?
            $mskSelected = '';
            $spbSelected = '';
            if (isset($_REQUEST['CITY_ID'])){
                if ($_REQUEST['CITY_ID'] == 'msk'){
                    $mskSelected = 'selected="selected"';
                    $spbSelected = '';
                } else {
                    $mskSelected = '';
                    $spbSelected = 'selected="selected"';
                }
            } else {
                if (CITY_ID == 'msk'){
                    $mskSelected = 'selected="selected"';
                    $spbSelected = '';
                } else {
                    $mskSelected = '';
                    $spbSelected = 'selected="selected"';
                }
            }
            ?>

            <input type="hidden" name="set_filter" value="Y" />
        </form>
</div>
