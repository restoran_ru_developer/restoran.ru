<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>
<?
global $USER;
$vkUserID = $_REQUEST["vk_user_id"];

$arUserFilter = Array(
    "ACTIVE" => "Y",
    "UF_VK_USER_ID" => $vkUserID
);
$rsUser = CUser::GetList(($by="personal_country"), ($order="desc"), $arUserFilter);
$arUser = $rsUser->Fetch();

if($arUser) {
    $USER->Authorize($arUser["ID"]);
    $arResult["TYPE"] = "AUTH";
} else {
    $arResult["TYPE"] = "NO";
}
echo json_encode($arResult);
?>