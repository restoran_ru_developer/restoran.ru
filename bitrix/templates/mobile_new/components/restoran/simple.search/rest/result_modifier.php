<?
CModule::IncludeModule("iblock");
if (is_array($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            foreach ($el["PROPERTIES"] as $key=>$prop)
            {
                //if (in_array($prop["CODE"], Array("type","kitchen","average_bill","kuhnya_dostavki","subway","address")))
                if (in_array($prop["CODE"], Array("kitchen","average_bill","subway","address")))
                    $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            }
            $arItem["ELEMENT"] = $el;                        
            if ($el["PROPERTIES"]["photos"]["VALUE"][0])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 848, 'height' => 420), BX_RESIZE_IMAGE_PROP, true);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 848, 'height' => 420), BX_RESIZE_IMAGE_PROP, true);
            if(!$arItem["ELEMENT"]["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm_new.png";
         }
    endforeach;?>
<?endif;?>
