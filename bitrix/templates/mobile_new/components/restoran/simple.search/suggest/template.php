<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?if (count($arResult["ITEMS"])>0):?>
<ul>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <li><a class="suggest-item" data-value="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?> [<?=$arItem["SECTION_NAME"]?>]</a></li>
    <?endforeach; ?>
</ul>
<?endif;?>

