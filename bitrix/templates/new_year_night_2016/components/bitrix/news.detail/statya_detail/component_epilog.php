<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$arResult["NAME"] = preg_replace("/&lt;br[ \/]+&gt;[0-9A-zА-ярР .]+/"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br/>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br />"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br /&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br/&gt;"," ",$arResult["NAME"]);

$APPLICATION->SetTitle('Новогодняя ночь в ресторане '.$arResult["NAME"]);
$APPLICATION->SetPageProperty("description", 'Новогодняя ночь от ресторана '.$arResult["NAME"]);
$APPLICATION->SetPageProperty("keywords",  'Новогодняя, ночь, ресторан, '.$arResult["NAME"]);

global $REST_ID;
$REST_ID = $arResult['REST_ID'];

?>
<script>
    $(function(){
        var sessionLat = '<?= $_SESSION['lat'] ?>';
        var sessionLon = '<?= $_SESSION['lon'] ?>';
        var options_for_get_position = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';

        setTimeout(get_location_a, 1000);

        function get_location_a()
        {
            if (navigatorOn != 'Y') {
                console.log('navigatorOff');
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(success_a, error_a, options_for_get_position);
                } else {
                    console.log("Ваш браузер не поддерживает\nопределение местоположения");
                    return false;
                }
            }
            else {
                var lat_lon_list = new Object();
                $('.need-distance').each(function(indx, element){
                    lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                })
//                    console.log(lat_lon_list);
                $.ajax({
                    type: "POST",
                    url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                    data: {
                        lat_lon_list: lat_lon_list
//                            who: '321'
                    },
                    dataType:'json',
                    success: function(data) {
                        $('.need-distance').each(function(indx, element){
                            $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                        })
                        $('.distance-place').show();
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });


                var lat_lon_list = new Object();
                $('.pull-left').each(function(indx, element){
                    lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                })
                console.log(lat_lon_list);
                $.ajax({
                    type: "POST",
                    url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                    data: {
                        lat_lon_list: lat_lon_list
//                            who: '321'
                    },
                    dataType:'json',
                    success: function(data) {
                        $('.pull-left').each(function(indx, element){
                            $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                        })
                        $('.distance-place').show();
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
        }
        function success_a(position) {
            sessionLat = position.coords.latitude;
            sessionLon = position.coords.longitude;

//                console.log(sessionLat,'sessionLat');
//                console.log(sessionLon,'sessionLon');
            $.ajax({
                type: "POST",
                url: "<?=SITE_TEMPLATE_PATH?>/ajax/ajax-coordinates.php",
                data: {
                    lat:sessionLat,
                    lon:sessionLon
                },
                success: function(data) {
                    var lat_lon_list = new Object();
                    $('.pull-left').each(function(indx, element){
                        lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                    })
                    console.log(lat_lon_list);
                    $.ajax({
                        type: "POST",
                        url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                        data: {
                            lat_lon_list: lat_lon_list
//                            who: '321'
                        },
                        dataType:'json',
                        success: function(data) {
                            $('.pull-left').each(function(indx, element){
                                $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                            })
                            $('.distance-place').show();
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });

                }
            });
        }
        function error_a(error)
        {
            console.log(error);
            $('.detail-restaurant-distance-block').hide();
            $('.right-props').addClass('full-string');
            return false;
        }
    })
</script>
