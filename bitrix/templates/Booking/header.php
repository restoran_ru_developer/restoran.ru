<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<script type="text/javascript">
		google.load("jquery", "1");
		google.load("jqueryui", "1");
	</script>
	
	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle()?></title>

	<? 
	if($USER->IsAuthorized()){
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.maskedinput-1.3.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.ui.datepicker-ru.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jQuery.fileinput.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.form.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/cookies.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.typewatch.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/java.js?189898');

        if(CSite::InGroup(array(36))){
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/lookon.js');
        }

		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w1.js?1');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w2.js?1');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w3.js?1');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w4.js?1');
		
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/telephone.js');

		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/inputs.js');
		
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.simplemodal.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/bonus.js');
	}else{
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/auth.js');
	}
	
	

	
	?>
	
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
<div id="conteiner">
	
	<?
//		$code = $_GET['code'];
//
//		if($code!=""){
//			require_once(dirname(__FILE__) . '/yam_src/consts.php');
//			require_once(dirname(__FILE__) . '/yam_src/lib/YandexMoney.php');
//
//			$ym = new YandexMoney(CLIENT_ID, './ym.log');
//			$receiveTokenResp = $ym->receiveOAuthToken($code, REDIRECT_URI, CLIENT_SECRET);
//
//			if ($receiveTokenResp->isSuccess()) {
//				$token = $receiveTokenResp->getAccessToken();
//
//				//пишем токен в файл
//				$fp = fopen(dirname(__FILE__) . '/yam_src/token', "w+");
//				fwrite($fp, $token);
//				fclose($fp);
//
//				//$_SESSION["ya_token"]=$token;
//			} else {
//				print "Error: " . $receiveTokenResp->getError();
//			}
//
//
//		}
		
	?>	
	
	<div id="ajax_loader"></div>
	<?if ($USER->IsAuthorized()){?>
		<div id="incoming_call">
			<div class="client_name">Иван Иванов</div>
			<div class="client_phone">+7 (123) 456-78-90</div>
		</div>
		
		<div class="center" id="content">
		<input type="hidden" id="OPERATOR_ID" value="<?=$USER->GetID()?>"/>
		
		<input type="hidden" id="OPERATOR_NUMBER" value="<?=$_SESSION["OPERATOR_NUMBER"]?>"/>
		<?if(!$USER->IsAdmin()){?><audio src="<?=SITE_TEMPLATE_PATH?>/ring.mp3" id="sound"></audio><?}?>
		
		<div class="logo"></div>
		<div class="user">Здравствуйте, <?=$USER->GetFullName()?>!   <a href="/bs/?logout=yes">Выйти</a></div>
		<div id="position">
			<a href="#" class="clear">Сбросить расположение окон</a>
		</div>
		
		<?
		$CITYar=array(
			"msk"=>"Москва",
			"spb"=>"Санкт-Петербург",
			"jurmala"=>"Юрмала",
			"nsk"=>"Новосибирск"
		);
		
		global $USER;
		$rsUser = CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
			
		$arGroups = CUser::GetUserGroup($USER->GetID());
		
		$arUser["UF_CITY"] = strtolower($arUser["UF_CITY"]);
		$arUser["UF_CITY"] = explode(",", $arUser["UF_CITY"]);
		
//		if(!in_array($_SESSION["CITY"], $arUser["UF_CITY"]) && !$USER->IsAdmin()) unset($_SESSION["CITY"]);
		
			
		
		if((in_array(19, $arGroups)==0 || $USER->GetID()==17459) && !$USER->IsAdmin() && count($arUser["UF_CITY"])>0) { // count($arUser["UF_CITY"])>1
			if(is_array($arUser["UF_CITY"])){
				if($_SESSION["CITY"]=="") $_SESSION["CITY"]=$arUser["UF_CITY"][0];
				
				if($_REQUEST["new_city"]!="" && $_SESSION["CITY"] != $_REQUEST["new_city"]) $_SESSION["CITY"] = $_REQUEST["new_city"];
				?>
				<form id="city_pos" action="/bs/" method="post">
					<input type="hidden" name="city" value="" />
					<div class="select select_off" id="city" style="z-index: 800;">
						<div class="selected_element"><?=$CITYar[$_SESSION["CITY"]]?></div>
						<div class="all_variants">
							<?foreach($CITYar as $Scode=>$Sname){?>
								<?if(in_array($Scode, $arUser["UF_CITY"])){?><a href="<?=$Scode?>" rel="nofollow" class=""><?=$Sname?></a><?}?>
							<?}?>
						</div>
					</div>
				</form>
				<?								
			}
            else {  //  нерабочее выражение
                $_SESSION["CITY"]=$arUser["UF_CITY"];
            }
		}
		
		if($_SESSION["CITY"]==""){
			if(is_array($arUser["UF_CITY"])) $_SESSION["CITY"]=$arUser["UF_CITY"][0];
			else $_SESSION["CITY"]=$arUser["UF_CITY"];


			if($_SESSION["CITY"]=="") $_SESSION["CITY"]="msk";
		}


		if((in_array(19, $arGroups)>0 && $USER->GetID()!=17459) || $USER->IsAdmin() || in_array(36, $arGroups)>0){
			if($_REQUEST["new_city"]!="" && $_SESSION["CITY"] != $_REQUEST["new_city"]) $_SESSION["CITY"] = $_REQUEST["new_city"];
		?>
			<form id="city_pos" action="/bs/" method="post">
				<input type="hidden" name="city" value="" />
				<div class="select select_off" id="city" style="z-index: 800;">
					<div class="selected_element"><?=$CITYar[$_SESSION["CITY"]]?></div>
					<div class="all_variants">
						<?foreach($CITYar as $Scode=>$Sname){?>
							<a href="<?=$Scode?>" rel="nofollow" class=""><?=$Sname?></a>
						<?}?>
					</div>
				</div>
			</form>
		<?}?>
		<div id="dialog-confirm" title="">
			
		</div>
	<?}?>
           <div id="cur_city" data-city="<?=$_SESSION["CITY"]?>" style="display: none;"></div>
	
	