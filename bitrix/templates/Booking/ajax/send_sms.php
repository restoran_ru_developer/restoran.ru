<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("sozdavatel.sms");
$answer = array();
$answer['error'] = false;

$phone = intval($_REQUEST['phone']);
if($phone){
    $message = $_REQUEST['textmessage'];
    if(preg_match('/^371/',$phone)) {
        $answer['sms_service'] = send_sms_rga_urm($phone,$message);
    }
    elseif(preg_match('/^7/',$phone)){
        $answer['sms_service'] = CSMS::Send($message, $phone, "UTF-8");
    }
    else {
        $answer['sms_service']=CSMS::Send($message, "7".$phone, "UTF-8");
    }

    if($answer['sms_service']){
        $answer['message'] = 'sms отправлена';
    }
//    FirePHP::getInstance()->info($answer);
}
else {
    $answer['error'] = true;
    $answer['message'] = 'отсутствует телефон клиента';
}
echo json_encode($answer);
?>