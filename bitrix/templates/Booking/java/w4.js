$(document).ready(function() {
	/******ЗАПРАШИВАЕМ ИНФУ ПО ЗАКАЗУ*****/
	$("#w4 .tbl tr:not(.hd)").live("click", function(){
		
		var to_send = {};
		to_send["act"] = "get_order_info";
		to_send["id"] = $(this).attr("id");
		clear_window("w3");
		
		$.post(core, to_send, function(html){
			var order = jQuery.parseJSON(html);
			
			
			$("#w3 input[name=rastoran_id]").val(order.RESTORAN_ID);
			$("#w3 input[name=order_id]").val(order.ID);
			$("#w3 .menu").html("Заказ №"+order.ID);
			$("#w3 input[name=rastoran]").val(order.RESTORAN);
			$("#w3 .rinfo").html(order.RESTORAN_INFO);
			
			$("#w3 input[name=date]").val(order.DATE);
			$("#w3 input[name=time]").val(order.TIME);
			$("#w3 input[name=guest]").val(order.GUEST);
			$("#w3 textarea[name=prim_order]").val(order.PRIM.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&#40;/g, '(').replace(/&#41;/g, ')').replace(/&#39;/g, "'"));
			
			//alert(decodeURI(order.PRIM));
			
			$("#w3 input[name=prinyal]").val(order.PRINYAL);
			$("#w3 input[name=podtv2]").val(order.PODTV2);
			$("#w3 input[name=podtv]").val(order.PODTV);
			$("#w3 input[name=summ_sch]").val(order.SUMM_SCH);
			$("#w3 input[name=stol]").val(order.STOL);
			
			$("#w3 input[name=procent]").val(order.PROCENT);
			
			$("#sozdan").html(order.DATE_CREATE);
			
			$("#operator").html(order.OPERATOR);
			$("#operator").parent("div").show();
			$("#w3 #status .selected_element").html(order.STATUS);
			$("#w3 #source .selected_element").html(order.SOURCE);
			
			
			if(order.TYPE_ID==1552){
				$("#format_m").show();
				
			}else{
				$("#format_m").hide();
			}
			
			$("#w3 input[name=format]").val(order.FORMAT);
			
			
			
			
			$("#w3 #type .selected_element").html(order.TYPE);
			
			if(order.DENGI=="Y") $("#w3 input[name=dengi]").attr("checked", true);
			else $("#w3 input[name=dengi]").attr("checked", false);
			
			//if(client.STATUS_COLOR!="" && client.STATUS!="") 
			$("#w3 #status .selected_element").html(order.STATUS).attr("class","selected_element "+order.STATUS_COLOR);
						
			$("#w3 input[name=status]").val(order.STATUS_ID);
			$("#w3 input[name=source]").val(order.SOURCE_ID);
			$("#w3 input[name=type]").val(order.TYPE_ID);
			
			$("#w3 input[name=control_call]").val(order.CONTROL_CALL);
			
			$("#w3").css("height", "auto");
		});
			
	});
	


});
