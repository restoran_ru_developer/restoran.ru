$(document).ready(function() {
	if($("#time").length>0){
		$.mask.definitions['~']='[0-2]';
		$.mask.definitions['h']='[0-9]';                               
		$.mask.definitions['!']='[0-5]';                                  
		$("#time").mask("~h:!9",{placeholder:"_"});

	} 
	
	$("#w3 input[name=prinyal]").capitalize();
	$("#w3 input[name=podtv2]").capitalize();
	$("#w3 input[name=podtv]").capitalize();
	
	
	//Считаем процент
	$("#w3 input[name=summ_sch]").keyup(function(){
		if($("#cur_city").data("city")=="jurmala"){
			var v = $(this).val();
			if(v>10){
				var procent = parseInt($("#rest_procent").html());
				if(!procent) procent =10;
				var p = (v/100*procent).toFixed(1);
				
				$("#w3 input[name=procent]").val(p);
			}else{
				$("#w3 input[name=procent]").val("");
			}
		}else{
			var v = parseInt($(this).val());
			if(v>10){
				var procent = parseInt($("#rest_procent").html());
				if(!procent) procent =10;
				var p = parseInt(v/100*procent);
				
				$("#w3 input[name=procent]").val(p);
			}else{
				$("#w3 input[name=procent]").val("");
			}
		}
	}).change(function(){
		
	});
	
	$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
	$( "#order_date" ).datepicker( $.datepicker.regional[ "ru" ] );
	$( "#DATE_CREATE" ).datepicker( $.datepicker.regional[ "ru" ] );
	$( "#control_call" ).datepicker( $.datepicker.regional[ "ru" ] );
	
	/********ПОИСК РЕСТОРАНА********/
	$("#w3 input[name=rastoran]").capitalize();
	$("#w3 input[name=rastoran]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_restoran";
			to_send["name"] = encodeURIComponent($("#w3 input[name=rastoran]").val());
			$.post(core, to_send, function(otvet){
				if(otvet!=""){
					if($("#w3 .variants").is(":hidden")){
						$("#w3 .variants").html(otvet).slideDown("fast");
					}else{
						$("#w3 .variants").html(otvet);
					}
				}else{
					$("#w3 .variants").slideUp("fast");
					$("#w3 input[name=rastoran_id]").val("");
					$("#w3 .rinfo").html("");
					
				}
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
	
	
	
	/*******ВЫБОР РЕСТОРАНА*******/
	$("#w3 .variants a").live("click",function(){
		var id = $(this).attr("href");
		$("#w3 input[name=rastoran]").val($(this).html());
		
		var to_send = {};
		to_send["act"] = "get_restoran_info";
		to_send["restoran_id"] = id;
		$("#w3 .variants").slideUp("fast");
		
		$.post(core, to_send, function(html){
			var restoran = eval('(' + html + ')');	
			
			$("#w3 input[name=rastoran_id]").val(restoran.ID);
			$("#w3 .rinfo").html(restoran.RESTORAN_INFO);
		});	
		return false;
	});
	/*******СКРЫВАЕМ/ПОКАЗЫВАЕМ ИНФУ******/
	$("#w3 .dcontrol a").click(function(){
		$("#w3").css("height","");
		if($(this).hasClass("select_on")){
			$(this).removeClass("select_on");
			$(this).html("<i>Скрыть описание</i>");
			$("#w3 .dcontrol").next().slideDown("fast", function(){
				save_win_pos();
			});
			
		}else{
			$(this).addClass("select_on");
			$(this).html("<i>Показать описание</i>");
			$("#w3 .dcontrol").next().slideUp("fast",function(){
				save_win_pos();
			});
		}
		return false;
	});
	
});
