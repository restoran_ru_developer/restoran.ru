function check_for_chenges(){
	if($("#client_changed").val()=="Y" || $("#order_changed").val()=="Y"){
		
		$("#dialog:ui-dialog").dialog( "destroy" );
				
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование',
					buttons: {
						"Нет": function() {
							$( this ).dialog( "close" );
						},
						"Да": function() {
							$(this).dialog("close");
							
							var to_send = {};
		$("#w1 input, #w1 textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();		
		});
		
		$("#w3 input, #w3 textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();		
		});
		
		to_send["act"] = "update_client";
		

							$.post(core, to_send, function(html){
								var otvet = eval('(' + html + ')');
								
								$("#client_changed").val("N");
								$("#order_changed").val("N");
								
								//ошибка доступа к заказу
			if(otvet.message=="permission denied"){	
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование заказа',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Доступ запрещен!');
				$("#dialog-confirm").css("padding-left", 70);
			}
			
			//Новый клиент добавлен
			if(otvet.message=="client_edded"){
				$("#dialog:ui-dialog").dialog( "destroy" );
				
				$("#w1 input[name=client_id]").val(otvet.client);
				$("#w3 input[name=order_id]").val(otvet.ORDER_ID);
				
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Новый клиент',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Новый клиент добавлен!');
				$("#dialog-confirm").css("padding-left", 70);
			}
			
			
			//Заказ изменен
			if(otvet.message=="order_saved"){
				$("#dialog:ui-dialog").dialog( "destroy" );
		
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование заказа',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Заказ изменен!');
				$("#dialog-confirm").css("padding-left", 70);
			}
			
			if(otvet.message=="client_updated"){
				$("#dialog:ui-dialog").dialog( "destroy" );
				
				$("#w3 input[name=order_id]").val(otvet.ORDER_ID);
				
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование клиента',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Изменения сохранены!');
				$("#dialog-confirm").css("padding-left", 70);
			}

							});
						}
					}
			}).html('<span class="alert_icon"></span>Изменения не были сохранены. Сохранить?');
			$("#dialog-confirm").css("padding-left", 70);
		
		
	}
}


$(document).ready(function() {
	
	
	
	if($("#phone").length>0 && $("#cur_city").data("city")=="jurmala") $("#phone").mask("99999999999");
	else $("#phone").mask("9999999999");
	
	
	/*********СПИСОК НОВЫХ ЗАКАЗОВ**********/
	$("#new_order").live("click", function(){
		check_for_chenges();
			
		$("#w2 .menu a").removeClass("active");
		$("#w2 .menu a:eq(1)").addClass("active");
			
		var to_send = {};
		to_send["act"] = "change_razdel";
		to_send["razdel"] = "#new_orders";

		$.post(core, to_send, function(otvet){
			$("#w2 .content").html(otvet);
				
			$("#w4 .head .menu").html("");
			clear_window("w4");
			$("#w4").show();
				
			rest_list();
	
		});
		return false;
	});

	/*******СОХРАНЯЕМ ИНФУ КЛИЕНТА*****/
	$("#w1 .save_but").click(function(){
		if(form_checked()){
			
			$("#client_changed").val("N");
			$("#order_changed").val("N");
		
		var to_send = {};
		$("#w1 input, #w1 textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();		
		});
		
		$("#w3 input, #w3 textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();		
		});
		
		to_send["act"] = "update_client";

            console.log(to_send,'to_send');


        if(to_send.rastoran_id.replace(/\D/g,'')>0){

            if(to_send.source==''||to_send.source==undefined){
                $("#dialog-confirm").dialog({
                    resizable: false,
                    draggable: false,
                    height:140,
                    modal: true,
                    title: 'Редактирование заказа',
                    buttons: {
                        "Ok": function() {
                            $( this ).dialog( "close" );
                        }
                    }
                }).html('<span class="alert_icon"></span>Не указан ИСТОЧНИК заказа!');
                $("#dialog-confirm").css("padding-left", 70);
                return false;
            }
        }


		$.post(core, to_send, function(html){
			var otvet = eval('(' + html + ')');
			
			//ошибка доступа к заказу
			if(otvet.message=="permission denied"){	
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование заказа',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Доступ запрещен!');
				$("#dialog-confirm").css("padding-left", 70);
			}
			
			//Новый клиент добавлен
			if(otvet.message=="client_edded"){
				$("#dialog:ui-dialog").dialog( "destroy" );
				
				$("#w1 input[name=client_id]").val(otvet.client);
				$("#w3 input[name=order_id]").val(otvet.ORDER_ID);
				
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Новый клиент',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Новый клиент добавлен!');
				$("#dialog-confirm").css("padding-left", 70);
			}
			
			//Такой клиент уже есть в базе
			if(otvet.message=="client_find"){
				$("#dialog:ui-dialog").dialog( "destroy" );
				
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Новый клиент',
					buttons: {
						"Нет": function() {
							$( this ).dialog( "close" );
						},
						"Да": function() {
							$(this).dialog("close");
							to_send["no_alert"] = "Y";
							$.post(core, to_send, function(html){
								var otvet = eval('(' + html + ')');
								
								$("#w1 input[name=client_id]").val(otvet.client);
								$("#w3 input[name=order_id]").val(otvet.ORDER_ID);
								
								$("#dialog-confirm").dialog({
									resizable: false,
									draggable: false,
									height:140,
									modal: true,
									title: 'Новый клиент',
									buttons: {
										"Ok": function() {
											$( this ).dialog( "close" );
										}
									}
								}).html('<span class="alert_icon"></span>Новый клиент добавлен!');
							});	
						}
					}
				}).html(otvet.message2);
				$("#dialog-confirm").css("padding-left", 70);
			}	
		
		
			//Заказ изменен
			if(otvet.message=="order_saved"){
				$("#dialog:ui-dialog").dialog( "destroy" );
		
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование заказа',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Заказ изменен!');
				$("#dialog-confirm").css("padding-left", 70);
			}
			
			if(otvet.message=="client_updated"){
				$("#dialog:ui-dialog").dialog( "destroy" );
				
				$("#w3 input[name=order_id]").val(otvet.ORDER_ID);
				
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Редактирование клиента',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Изменения сохранены!');
				$("#dialog-confirm").css("padding-left", 70);
			}

            if(otvet.message=="error"){
                $("#dialog:ui-dialog").dialog( "destroy" );

                $("#w3 input[name=order_id]").val(otvet.ORDER_ID);

                $("#dialog-confirm").dialog({
                    resizable: false,
                    draggable: false,
                    height:140,
                    modal: true,
                    title: 'Редактирование клиента',
                    buttons: {
                        "Ok": function() {
                            $( this ).dialog( "close" );
                        }
                    }
                }).html('<span class="alert_icon"></span>'+otvet.message_text);
                $("#dialog-confirm").css("padding-left", 70);
            }
		
		});
		
		
		}	
	
		return false;
	});
	
	/*******ПРОВЕРКА ЗАПОЛНЕНИЯ*******/
	function form_checked(){
		$("#w1 .alert").remove();
		$("#w3 .alert").remove();
		
		var res = true;
		
		/*
		if($("#w1 input[name=surname]").val().length<=1){
			var div="<a class=\"alert\" title=\"Укажите фамилию!\"></a>";
			$("#w1 input[name=surname]").before(div);		
			res = false;
		}
		*/
	
		return res;
	}
	
	/*********ПОИСК ПО ФАМИЛИИ**********/
	$("#w1 input[name=surname]").capitalize();	
	$("#w1 input[name=surname]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_client_by_surname";
			to_send["surname"] = $("#w1 input[name=surname]").val();
			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
	
	/*********ПОИСК ПО ИМЕНИ**********/
	$("#w1 input[name=name]").capitalize();	
	$("#w1 input[name=name]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_client_by_name";
			to_send["name"] = $("#w1 input[name=name]").val();
			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
	
	/*********ПОИСК ПО ТЕЛЕФОНУ**********/
	$("#w1 input[name=telephone]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_client_by_telephone";
			to_send["telephone"] = $("#w1 input[name=telephone]").val();
			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
	
	
	/*********ПОИСК ПО МЫЛУ**********/
	$("#w1 input[name=email]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_client_by_email";
			to_send["email"] = $("#w1 input[name=email]").val();
			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
	
	
	/**********НОВЫЙ КЛИЕНТ*************/
	$("#w1 .new_user").click(function(){
		clear_window("w1");
		clear_window("w3");
		
		$("#w2 .menu a").removeClass("active");
		$("#w2 .menu a:eq(0)").addClass("active");
		
		var to_send = {};
			to_send["act"] = "change_razdel";
			to_send["razdel"] = $("#w2 .menu a:eq(0)").attr("href");

			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
				
				$("#w4 .head .menu").html("");
				clear_window("w4");
				$("#w4").show();
				
				rest_list();
				
				
	
			});
		
		$("#w3 .menu").html("Заказ");
		return false;
	});


});
