var core2 = "/bitrix/templates/Booking/telephone.php";
var ltop = 0; 
var show_ajax_runer=1;
var ajax_runer =0;
var auto_refresh = true;
var showing = false;
var notification = false;
$(document).ready(function() {
	var call_window = $("#incoming_call");
	
	/*********ВХОДЯЩИЕ ЗВОНКИ***********/
	/*
	(function poll(){
		show_ajax_runer=0;
    	$.ajax({ url: core2,data: { act: "check_call"}, success: function(data){
        	//Update your dashboard gauge
        	//salesGauge.setValue(data.value);
        	console.log(data);
        	show_ajax_runer=1;
    	}, dataType: "json", complete: poll, timeout: 30000 });
	})();

	*/
	
	function check_calls(){
		var to_send = {};
		to_send["act"] = "check_call";
		to_send["operator_id"] = $("#OPERATOR_NUMBER").val();
		show_ajax_runer=0;
		if(to_send["operator_id"]>0){
            ajax_load=false;
            $.post(core2, to_send, function(html){
                if(html!=""){
                    var otvet = eval('(' + html + ')');
                    //Показываем попап
                    if(otvet.client!="" && !showing){
                        showing =  true;
                        show_call_window(otvet);
                    }

                    show_ajax_runer=1;
                }
                ajax_load=true;
            });
		}
	}
		
	setInterval(check_calls, 1000);
	
	function show_call_window(client){
		if(call_window.is(":hidden")){
			//console.log(client);
			call_window.find(".client_name").html(client.name);
			call_window.find(".client_phone").html(client.telephone);
			
			var not_txt="";
			
			if(client.client=="NEW") not_txt="Новый клиент, "+client.telephone;
			else not_txt=client.name+", "+client.telephone;
				
			if(window.webkitNotifications) {
				notification = window.webkitNotifications.createNotification("http://spb.restoran.ru/bitrix/templates/Booking/images/notification_icon.png", "Входящий звонок", not_txt);
				notification.show();
			}
			if(client.client=="NEW") call_window.find(".client_name").html("Новый клиент");
			
			call_window.show();
			
			setTimeout(close_call_window, 9000);
			showing = false;
			
			
			//если клиент не новый и в данный момент не идет заполение полей, то подгружаем карточку звонящего клиента
			if(client.client!="NEW" && $("#w1 input[name=user]").val()=="" && $("#w1 input[name=surname]").val()=="" && $("#w1 input[name=email]").val()=="") show_client_info(client.client);
				
			
		}
	}
	
	function close_call_window(){
		call_window.hide();
		if(notification) notification.cancel();
		showing = false;
	}
	
	
	
	function show_client_info(client_id){
		clear_window("w3");
		clear_window("w1");
		
		var to_send = {};
		to_send["act"] = "get_client_info";
		to_send["id"] = client_id;	
		
		$.post(core, to_send, function(html){
			var client = eval('(' + html + ')');
			
			$("#w4 .head .menu").html("Заказы клиента");
						
			$("#w1 input[name=user]").val(client.USER_ID);
			$("#w1 input[name=client_id]").val(client.CLIENT_ID);
			$("#w1 input[name=name]").val(client.NAME);
			
			if(client.SURNAME) $("#w1 input[name=surname]").val(client.SURNAME.replace(/&quot;/g, '"'));
			else $("#w1 input[name=surname]").val("");
			
			$("#w1 input[name=email]").val(client.EMAIL);
			$("#w1 input[name=user]").val(client.USERID);
			
			if(client.USERID>0){
				$("#link_to_user").attr("href", client.USRLNK);
				$("#link_to_user").show();
				$("#w1").css("height", "auto");
			}else{
				$("#link_to_user").attr("href", client.USRLNK);
				$("#link_to_user").hide();
				$("#w1").css("height", "auto");
			}
			
			$("#w1 input[name=telephone]").val(client.TELEPHONE);
			$("#w1 textarea[name=prim]").val(client.PRIM.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&#40;/g, '(').replace(/&#41;/g, ')').replace(/&#39;/g, "'").replace(/<br \/>/g, "").replace(/&nbsp;/g, " "));
			
			
			$("#w4 .content").html(client.ORDERS);
			$("#w4").css("height", "auto"); 
			$("#w3").css("height", "auto");
		});
	
	
	}
	
		
});
