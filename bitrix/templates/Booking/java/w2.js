keyup = false;
function rest_list(){
	
	$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
	$( "#date_s" ).datepicker( $.datepicker.regional[ "ru" ] );
	$( "#date_po" ).datepicker( $.datepicker.regional[ "ru" ] );
	
	$( "#date_s2" ).datepicker( $.datepicker.regional[ "ru" ] );
	$( "#date_po2" ).datepicker( $.datepicker.regional[ "ru" ] );
	
	//$( "#control_call" ).datepicker( $.datepicker.regional[ "ru" ] );
	$( "#control_call2" ).datepicker( $.datepicker.regional[ "ru" ] );
	
	
	$("#w2 input[name=restoran]").capitalize();
	$("#w2 input[name=restoran]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_restoran";
			to_send["name"] = $("#w2 input[name=restoran]").val();
			$.post(core, to_send, function(otvet){
				if(otvet!=""){
					if($("#w2 .variants").is(":hidden")){
						$("#w2 .variants").html(otvet).slideDown("fast");
					}else{
						$("#w2 .variants").html(otvet);
					}
				}else{
					$("#w2 .variants").slideUp("fast");
				}
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
		
	/***********ПОИСК РЕСТОРАНОВ ПО НАЗВАНИЮ**************/
	
	$("#restorans_filter input[name=rest_name]").capitalize();
	$("#restorans_filter input[name=rest_name]").typeWatch( {
    	callback: function(){ 
    		var to_send = {};
			to_send["act"] = "search_rest_by_name";
			to_send["name"] = $("#restorans_filter input[name=rest_name]").val();
			$.post(core, to_send, function(otvet){
				$("#w2 .paginator").remove();
				if($("#w2 .tbl").length>0) $("#w2 .tbl").replaceWith(otvet);
				else $("#restorans_filter").after(otvet);
			});
    	},
    	wait: 600,
    	highlight: true,
    	captureLength: 2
	});
	
	
	/*******ВЫБОР РЕСТОРАНА*******/
	$("#w2 .variants a").live("click",function(){
		var id = $(this).attr("href");
		$("#w2 input[name=restoran]").val($(this).html());
		
		var to_send = {};
		to_send["act"] = "get_restoran_info";
		to_send["restoran_id"] = id;
		$("#w2 .variants").slideUp("fast");
		
		$.post(core, to_send, function(html){
			var restoran = eval('(' + html + ')');	
			
			$("#w2 input[name=restoran_id]").val(restoran.ID);
			$("#w2 .rinfo").html(restoran.RESTORAN_INFO);
		});	
		return false;
	});
	
	
		$("#file_uploader .files").customFileInput();
	
		$("#file_uploader .files").bind({
			change: function() {
    		    if($("#file_uploader .files .customfile-feedback").html()!=""){
    		    	$("#file_uploader .but input").attr("class", "on"); 
    		    }else{
    		    	$("#file_uploader .but input").attr("class", "off"); 
    		    }
		
			}
		});
		
		function check_form(a,f,o){
			var ret=true;
			o.dataType = "html";
			
			if($("#file_uploader .customfile-feedback").html()==""){
				var div="<a class=\"alert\" title=\"Выберите файл!\"></a>";
				$("#file_uploader .customfile-feedback").before(div);		
				ret = false;
			}
			
			return ret;
		}
		
		$('#file_uploader').ajaxForm({
			beforeSubmit: check_form,
			success: function(data) {
				var otvet = eval('(' + data + ')');
				
				$("#dialog:ui-dialog").dialog( "destroy" );
		
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Загрузка файла',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html(otvet.message);
			}
		});
		
		
		
		
	}

$(document).ready(function() {
	
	/*******ФИЛЬТРУЕМ ЗАКАЗЫ ************/
	$("#orders_filter .but a").live("click", function(){
		
		var to_send = {};
		to_send["act"] = "filter_orders";
		to_send["razdel"] = $("#w2 .menu .active").attr("href");
			
		$("#orders_filter input, #orders_filter textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();		
		});
		
		$.post(core, to_send, function(otvet){
			$("#w4").show();
			
			
			if($("#orders_filter").length>0){
				$("#w2 .paginator").remove();
				if($("#w2 .tbl").length>0) $("#w2 .tbl").replaceWith(otvet);
				else $("#orders_filter").after(otvet);
			}else $("#w2 .content").html(otvet);
		});	
		
		return false;
	});


	/*******Сбрасываем фильтр**********/
	$("#clear_rest_filter").live("click",function(){
		
		
		var to_send = {};
		to_send["act"] = "clear_rest_filter";	
		$.post(core, to_send, function(html){
			if($("#restorans_filter a").hasClass("active")){
				$("#restorans_filter .fltrs").slideUp("fast");
				$("#restorans_filter a").removeClass("active");
				$("#w2").animate({"height":$("#w2").height()-300},300);
				$("#w4").show();
			}
			
			if($("#restorans_filter").length>0){
				$("#w2 .paginator").remove();
				if($("#w2 .tbl").length>0) $("#w2 .tbl").replaceWith(html);
				else $("#restorans_filter").after(html);
			}else $("#w2 .content").html(html);
			
		});	
		return false;
	});
	
	
	$("#clear_orders_filter").live("click",function(){

		var to_send = {};
		to_send["act"] = "clear_orders_filter";	
		$.post(core, to_send, function(html){
			
			$("#w2 .content").html(html);
			
			$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
			$( "#date_s" ).datepicker( $.datepicker.regional[ "ru" ] );
			$( "#date_po" ).datepicker( $.datepicker.regional[ "ru" ] );
	
			$( "#date_s2" ).datepicker( $.datepicker.regional[ "ru" ] );
			$( "#date_po2" ).datepicker( $.datepicker.regional[ "ru" ] );
			
			$( "#control_call2" ).datepicker( $.datepicker.regional[ "ru" ] );
			
			rest_list();
		});	
		return false;
	});



	
	/*************РЕДАКТИРОВАНИЕ РЕСТОРАНА***************/
	$("a.pen").live("click", function(){
		var to_send = {};
		to_send["act"] = "edite_rest_form";
		to_send["id"]= $(this).attr("href");
		$.post(core, to_send, function(html){
			$("#dialog:ui-dialog").dialog( "destroy" );
			
			$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
				
					modal: true,
					title: 'Редактирование ресторана',
					buttons: {
						"Сохранить": function() {
							//$( this ).dialog( "close" );
							var to_send = {};
							to_send["act"] = "edite_rest";
							$("#add_rest_form input[type=text]").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							$("#add_rest_form input[type=hidden]").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							
							
							$("#add_rest_form input[type=checkbox]:checked").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							$("#add_rest_form textarea").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							
							$("#add_rest_form select").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
								
							$.post(core, to_send, function(html){
										
								$("#dialog-confirm").dialog( "close" );
								
							});
						
						}
					}
				}).html(html).css("height", "auto");
				$(".ui-dialog").css("top", ($(window).height()-$("#dialog-confirm").height())/2);
				$("#dialog-confirm").css("padding-left", 0);
		});
		return false;
	});
	
	
	/*************ДОБАВЛЕНИЕ РЕСТОРАНА******************/
	$("a.add_new_rest").live("click", function(){
		
		var to_send = {};
		to_send["act"] = "add_rest_form";
		$.post(core, to_send, function(html){
			$("#dialog:ui-dialog").dialog( "destroy" );
			
			$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
				
					modal: true,
					title: 'Добавление ресторана',
					buttons: {
						"Сохранить": function() {
							//$( this ).dialog( "close" );
							var to_send = {};
							to_send["act"] = "add_new_rest";
							$("#add_rest_form input").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							$("#add_rest_form textarea").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							
							$("#add_rest_form select").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
								
							$.post(core, to_send, function(html){
								
								
								var to_send = {};
								to_send["act"] = "search_rest_by_name";
								to_send["name"] = $("#add_rest_form input[name=name]").val();
								$.post(core, to_send, function(otvet){
									$("#w2 .paginator").remove();
									if($("#w2 .tbl").length>0) $("#w2 .tbl").replaceWith(otvet);
									else $("#restorans_filter").after(otvet);
								});
								
								$("#dialog-confirm").dialog( "close" );
								
							});
						
						}
					}
				}).html(html).css("height", "auto");
				$(".ui-dialog").css("top", ($(window).height()-$("#dialog-confirm").height())/2);
				$("#dialog-confirm").css("padding-left", 0);
			
		});	
		
		return false;
	});
	
	
	$("#w2 .dater").live("click", function(){
		var d = $(this).find("a").attr("rel");
	
		if($(this).hasClass("open")){
			$(this).removeClass("open");
			$(this).addClass("closed");
			
			$(".orders .dt"+d).hide();
			
			$.cookie("c_"+$(this).attr("id"),'closed');
			
		}else{
			$(this).addClass("open");
			$(this).removeClass("closed");
			$(".orders .dt"+d).show();
			
			$.cookie("c_"+$(this).attr("id"),'open');
		}
		
		return false;
	});
	
	/*****ФИЛЬТР РЕСТОРАНОВ*****/
	$("#restorans_filter a:not(.but_search, .add_new_rest, .clear_rest_filter, .clear_orders_filter )").live("click", function(){
		if($(this).hasClass("active")){
			$("#restorans_filter .fltrs").slideUp("fast");
			$("#restorans_filter a").removeClass("active");
			$("#w2").animate({"height":$("#w2").height()-300},300);
			$("#w4").show();
		}else{
			$("#restorans_filter a").removeClass("active");
			$(this).addClass("active");
			var ttl = $(this).find("span").html();
		
			var to_send = {};
			to_send["act"] = "load_rest_filter";
			to_send["fltr"] = $(this).attr("href");
			
			
			$.post(core, to_send, function(otvet){
				if($("#restorans_filter .fltrs").is(":hidden"))$("#w2").animate({"height":$("#w2").height()+300},300);
				$("#restorans_filter .fltrs").html(otvet).slideDown("fast");
				$("#w4").hide();
				
			});
		}
		return false;
	});
	
	
	$("#restorans_filter a.but_search").live("click", function(){
		var to_send = {};
		to_send["act"] = "filtr_rest";
		to_send["rzdl"] = $("#restorans_filter a.active").attr("href");
		
		$("#restorans_filter a").removeClass("active");
		
		$("#restorans_filter input:checked").each(function(){
			to_send[$(this).attr("name")] = $(this).val();
		});
		
		
		
		
		$.post(core, to_send, function(otvet){
			$("#w2").animate({"height":$("#w2").height()-300},300);
			$("#restorans_filter .fltrs").slideUp("fast");
			$("#w4").show();
			
			
			if($("#restorans_filter").length>0){
				$("#w2 .paginator").remove();
				if($("#w2 .tbl").length>0) $("#w2 .tbl").replaceWith(otvet);
				else $("#restorans_filter").after(otvet);
			}else $("#w2 .content").html(otvet);
			
				
		});
			
	
		return false;
	});
	
	


	/*****ПЕРЕКЛЮЧЕНИЕ РАЗДЕЛОВ*************/
	$("#w2 .menu a:not(.dirotch)").live("click", function(){
		if(!$(this).hasClass("active")){
			check_for_chenges();
			
			$("#w2 .menu a").removeClass("active");
			$(this).addClass("active");
			
			var to_send = {};
			to_send["act"] = "change_razdel";
			to_send["razdel"] = $(this).attr("href");

			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
				
				$("#w4 .head .menu").html("");
				clear_window("w4");
				$("#w4").show();
				
				rest_list();
				
				if(to_send["razdel"]="#orders"){
					$("#w2").css("height", "auto"); 
					
					cook_opener();
				}
				

	
			});
		}
		return false;
	});

	

	/*****ПЕРЕКЛЮЧЕНИЕ И СОРТИРОВКА******/
	//сортировка
	$(".paginator .sort a").live("click", function(){
		if(!$(this).hasClass("active")){
			var to_send = {};
			to_send["order"] = $(this).attr("href");
			to_send["act"] = "change_razdel";
			to_send["razdel"] = $("#w2 .menu .active").attr("href");
			
			$.post(core, to_send, function(otvet){
				$("#w2 .content").html(otvet);
				
				rest_list();
				
			});
		}
		return false;
	});
	//постраничка
	$(".paginator .pager a").live("click", function(){
		var to_send = {};
		to_send["PAGEN_1"] = parseInt($(this).html());
		to_send["act"] = "change_razdel";
		to_send["razdel"] = $("#w2 .menu .active").attr("href");
		
		$.post(core, to_send, function(otvet){
			if($("#restorans_filter").length>0){
				$("#w2 .paginator").remove();
				$("#w2 .tbl").replaceWith(otvet);
			}else $("#w2 .content").html(otvet);
			
			rest_list();
			cook_opener();
		});
		return false;
	});
	
	//запрашиваем инфу по клиенту или по заказу
	$("#w2 .sms tr").live("click", function(){
		
		var to_send = {};
		to_send["act"] = "get_client_by_sms";
		to_send["id"] = $(this).attr("id");
		clear_window("w3");
		$.post(core, to_send, function(html){
			var client = eval('(' + html + ')');
			
			$("#w4 .head .menu").html("Заказы клиента");
			
			$("#w1 input[name=client_id]").val(client.CLIENT_ID);
			$("#w1 input[name=name]").val(client.NAME);
			$("#w1 input[name=surname]").val(client.SURNAME);
			$("#w1 input[name=email]").val(client.EMAIL);
			$("#w1 input[name=telephone]").val(client.TELEPHONE);
			$("#w1 textarea[name=prim]").val(client.PRIM);
			
			
			$("#w4 .content").html(client.ORDERS);
			$("#w4").css("height", "auto"); 
			$("#w3").css("height", "auto");
			
			
		});
	});


	var get_user = parseInt(window.location.search.replace("?user_info=", ""));
	if(get_user>0){
		var to_send = {};
		to_send["act"] = "get_client_info";
		to_send["id"] = get_user;
		
		$.post(core, to_send, function(html){
			var client = eval('(' + html + ')');
			
			$("#w4 .head .menu").html("Заказы клиента");
			
			$("#w1 input[name=user]").val(client.USER_ID);
			$("#w1 input[name=client_id]").val(client.CLIENT_ID);
			$("#w1 input[name=name]").val(client.NAME);
			
			if(client.SURNAME) $("#w1 input[name=surname]").val(client.SURNAME.replace(/&quot;/g, '"'));
			else $("#w1 input[name=surname]").val("");
			
			$("#w1 input[name=email]").val(client.EMAIL);
			
			$("#w1 input[name=user]").val(client.USER_ID);
			
			if(client.USERID>0 || client.USER_ID>0){
				$("#link_to_user").attr("href", client.USRLNK);
				$("#link_to_user").show();
				$("#w1").css("height", "auto");
			}else{
				$("#link_to_user").attr("href", client.USRLNK);
				$("#link_to_user").hide();
				$("#w1").css("height", "auto");
			}
			
			$("#w1 input[name=telephone]").val(client.TELEPHONE);
			$("#w1 textarea[name=prim]").val(client.PRIM.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&#40;/g, '(').replace(/&#41;/g, ')').replace(/&#39;/g, "'").replace(/<br \/>/g, "").replace(/&nbsp;/g, " "));
			
			
			$("#w4 .content").html(client.ORDERS);
			$("#w4").css("height", "auto"); 
			$("#w3").css("height", "auto");
		});
		
		
	}


	//запрашиваем инфу по клиенту или по заказу
    if(typeof(LOOKON) == "undefined"){
        $("#w2 .tbl:not(.reports, .rests, .sms) tr:not(.dater,.headd, .spacer)").live("click", function(){

            var to_send = {};
            if($("#w2 .tbl").hasClass("clients")) to_send["act"] = "get_client_info";
            if($("#w2 .tbl").hasClass("orders")) to_send["act"] = "get_client_info_by_order";
            if($("#w2 .tbl").hasClass("orders_all")) to_send["act"] = "get_client_info_by_order";


            to_send["id"] = $(this).attr("id");
            clear_window("w3");
            $.post(core, to_send, function(html){
                var client = eval('(' + html + ')');

                $("#w4 .head .menu").html("Заказы клиента");

                $("#w3 input[name=rastoran_id]").val(client.RESTORAN_ID);
                $("#w3 input[name=order_id]").val(client.ID);
                $("#w3 input[name=rastoran]").val(client.RESTORAN);
                $("#w3 .rinfo").html(client.RESTORAN_INFO);

                $("#sozdan").html(client.DATE_CREATE);

                $("#w3 input[name=date]").val(client.DATE);
                $("#w3 input[name=time]").val(client.TIME);
                $("#w3 input[name=guest]").attr("value",client.GUEST);

                $("#w3 input[name=DATE_CREATE]").val(client.DATE_CREATE);

                if(client.PRIM_ORDER)
                    $("#w3 textarea[name=prim_order]").val(client.PRIM_ORDER.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&#40;/g, '(').replace(/&#41;/g, ')').replace(/&#39;/g, "'").replace(/<br \/>/g, "").replace(/&nbsp;/g, " "));

                $("#w3 input[name=prinyal]").val(client.PRINYAL);



                $("#w3 input[name=podtv2]").val(client.PODTV2);
                $("#w3 input[name=podtv]").val(client.PODTV);
                $("#w3 input[name=summ_sch]").val(client.SUMM_SCH);
                $("#w3 input[name=stol]").val(client.STOL);
                $("#w3 input[name=procent]").val(client.PROCENT);

                $("#w3 input[name=control_call]").val(client.CONTROL_CALL);

                if(client.ID>0) $("#w3 .menu").html("Заказ №"+client.ID);

                if(client.OPERATOR!="" && client.OPERATOR){
                    $("#w3 #operator").html(client.OPERATOR);
                    $("#w3 #operator").parent("div").show();
                }
                //if(client.STATUS_COLOR!="" && client.STATUS!="")
                if(to_send["act"]=="get_client_info_by_order") $("#w3 #status .selected_element").html(client.STATUS).attr("class","selected_element "+client.STATUS_COLOR);


                $("#w3 #source .selected_element").html(client.SOURCE);

                $("#w3 input[name=status]").val(client.STATUS_ID);
                $("#w3 input[name=source]").val(client.SOURCE_ID);

                $("#w3 #type .selected_element").html(client.TYPE);
                $("#w3 input[name=type]").val(client.TYPE_ID);

                if(client.TYPE_ID==1552){
                    $("#format_m").show();

                }else{
                    $("#format_m").hide();
                }

                $("#w3 input[name=format]").val(client.FORMAT);

                $("#w1 input[name=user]").val(client.USER_ID);
                $("#w1 input[name=client_id]").val(client.CLIENT_ID);
                $("#w1 input[name=name]").val(client.NAME);

                if(client.SURNAME) $("#w1 input[name=surname]").val(client.SURNAME.replace(/&quot;/g, '"'));
                else $("#w1 input[name=surname]").val("");

                $("#w1 input[name=email]").val(client.EMAIL);

                $("#w1 input[name=user]").val(client.USER_ID);

                if(client.USERID>0 || client.USER_ID>0){
                    $("#link_to_user").attr("href", client.USRLNK);
                    $("#link_to_user").show();
                    $("#w1").css("height", "auto");
                }else{
                    $("#link_to_user").attr("href", client.USRLNK);
                    $("#link_to_user").hide();
                    $("#w1").css("height", "auto");
                }

                $("#w1 input[name=telephone]").val(client.TELEPHONE);
                $("#w1 textarea[name=prim]").val(client.PRIM.replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&#40;/g, '(').replace(/&#41;/g, ')').replace(/&#39;/g, "'").replace(/<br \/>/g, "").replace(/&nbsp;/g, " "));


                $("#w4 .content").html(client.ORDERS);
                $("#w4").css("height", "auto");
                $("#w3").css("height", "auto");
            });

        });
    }

	
	
	//запрашиваем детальную инфу по ресторану
	$("#w2 .rests tr").live("click", function(){
		
		var to_send = {};
		
		to_send["act"] = "get_rest_detailed_info";	
		to_send["id"] = $(this).attr("id");
	
		$.post(core, to_send, function(html){
			var rest = eval('(' + html + ')');
			
			
			$("#w4").css("height", "auto");
			$("#w4 .content").html(rest.RESTORAN_INFO);
			$("#w4 .head .menu").html(rest.NAME);
			
		});
			
	});
	
	
	
	
	/***********НАВИГАЦИЯ СТРЕЛКАМИ****************/
	function DownArrowPressed(){
		if($("#w2 .tbl .tr_selected").length>0){
			var i = $('#w2 .tbl tr').index($("#w2 .tbl .tr_selected"));
			i=i+1;
			if(i==$('#w2 .tbl tr').length) i=0;
			$("#w2 .tbl tr").removeClass("tr_selected");
			$("#w2 .tbl tr:eq("+i+")").addClass("tr_selected");
			console.log(i+" "+$('#w2 .tbl tr').length);
		}
	}
	
	function UpArrowPressed(){
		if($("#w2 .tbl .tr_selected").length>0){
			var i = $('#w2 .tbl tr').index($("#w2 .tbl .tr_selected"));
			i=i-1;
			if(i<0) i=$('#w2 .tbl tr').length-1;
			$("#w2 .tbl tr").removeClass("tr_selected");
			$("#w2 .tbl tr:eq("+i+")").addClass("tr_selected");
		}
		return false;
	}
	
	document.onkeydown = function(evt) {
    	evt = evt || window.event;
    	switch (evt.keyCode) {
       		case 38:
            UpArrowPressed();
            if($(".tbl .tr_selected").length>0) evt.returnValue = false;
            break;
        	case 40:
            DownArrowPressed();
            if($(".tbl .tr_selected").length>0) evt.returnValue = false;
            break;
    	}
    	
	};

/**********************************************************************/
	 /***************************************/
	/*******НОВЫЕ ОТЧЕТЫ********************/
   /***************************************/
	
	/*******СОЗДАЕМ НОВЫЙ ОТЧЕТ ************/
	$("#reports_filter .but a").live("click", function(){
		var to_send = {};
		to_send["act"] = "create_report";
		to_send["razdel"] = $("#w2 .menu .active").attr("href");
			
		$("#reports_filter input, #reports_filter textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();		
		});
		
		
		$.post(core, to_send, function(otvet){
			
			if(otvet=="not_orders"){
				$("#dialog:ui-dialog").dialog("destroy");
		
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Отчеты',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Вы выбранном периоде нет заказов!');
			}else{
				$("#w2 .content").html(otvet);

				
				$("#w4 .head .menu").html("");
				clear_window("w4");
				$("#w4").show();
				
				rest_list();
			}

		});	
		return false;
	});
	
	//ПЕЧАТАЕМ ОТМЕЧЕННЫЕ ЗАКАЗЫ
	$(".print_orders, .xls_orders").live("click", function(){
		var lnk = $(this).attr("href");
		
		$("#orders_list_form").attr("action", lnk);
		
		var co=$(".rep_tbl table tr td input[type=checkbox]:checked").length;
				
		if(co==0){
			$("#dialog-confirm").dialog({
				resizable: false,
				draggable: false,
				height:140,
				modal: true,
				title: 'Печать заказов',
				buttons: {
					"Ok": function() {
						$( this ).dialog( "close" );
					}
				}
			}).html('<span class="alert_icon"></span>Выберите заказы для печати');
			
		}else $("#orders_list_form").submit();
		
		/*
		var str="?";
		var co=0;
		$(".rep_tbl table tr td input[type=checkbox]:checked").each(function(){
			str+="ids[]="+$(this).val()+"&";
			co++;
		});
		//alert(co);
		
		$(this).attr("href","print_orders.php"+str);
		if(co==0){
			$("#dialog-confirm").dialog({
				resizable: false,
				draggable: false,
				height:140,
				modal: true,
				title: 'Печать заказов',
				buttons: {
					"Ok": function() {
						$( this ).dialog( "close" );
					}
				}
			}).html('<span class="alert_icon"></span>Выберите заказы для печати');
			
		}
		*/
		return false;
	});
	
	
	
	//В ЭКСЕЛЬ
	/*
	$(".xls_orders").live("click", function(){
		var str="?";
		var co=0;
		$(".rep_tbl table tr td input[type=checkbox]:checked").each(function(){
			str+="ids[]="+$(this).val()+"&";
			co++;
		});
		$(this).attr("href","get_orders_xls.php"+str);
		if(co==0){
			$("#dialog-confirm").dialog({
				resizable: false,
				draggable: false,
				height:140,
				modal: true,
				title: 'Экспорт в Excel',
				buttons: {
					"Ok": function() {
						$( this ).dialog( "close" );
					}
				}
			}).html('<span class="alert_icon"></span>Выберите заказы для экспорта в *.xls');
			return false;
		}
	});
	*/
	
	
	//ОПЛАЧИВАЕМ ЗАКАЗЫ
	$(".pay_orders").live("click", function(){
		var str="?";
		$(".rep_tbl table tr td input[type=checkbox]:checked").each(function(){
			str+="ids[]="+$(this).val()+"&";
			if($("#prim_"+$(this).val()).val()!="") str+="prim_"+$(this).val()+"="+$("#prim_"+$(this).val()).val();
			
			$(this).parent("td").parent("tr").next().remove();
			$(this).parent("td").parent("tr").remove();
		});
		
		var to_send = {};
		to_send["act"] = "pay_orders";
		$.post(core+str, to_send, function(html){
			var otvet = eval('(' + html + ')');
			$("#dialog-confirm").dialog({
				resizable: false,
				draggable: false,
				height:140,
				modal: true,
				title: 'Оплата заказов',
				buttons: {
					"Ok": function() {
						$( this ).dialog( "close" );
					}
				}
			}).html('<span class="alert_icon"></span>'+otvet.message);
				
		});
		return false;
	});
	
	
	//ОТПРАВЛЯЕМ ЗАКАЗЫ В АРХИВ
	$(".arch_orders").live("click", function(){
		var str="?";
		$(".rep_tbl table tr td input[type=checkbox]:checked").each(function(){
			str+="ids[]="+$(this).val()+"&";
			if($("#prim_"+$(this).val()).val()!="") str+="prim_"+$(this).val()+"="+$("#prim_"+$(this).val()).val();
			
			$(this).parent("td").parent("tr").next().remove();
			$(this).parent("td").parent("tr").remove();
		});
		if (confirm("Отправить выбранные заказы в архив?")) {
			
			var to_send = {};
			to_send["act"] = "arch_orders";
			$.post(core+str, to_send, function(html){
				var otvet = eval('(' + html + ')');
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Оплата заказов',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>'+otvet.message);
					
			});	
			
		}
		return false;
	});
	
	
	

	/***********ПРОСТАВЛЯЕМ ГАЛОЧКИ*************/
	$(".rep_tbl input[name=check_all]").live("click", function(event){
		event.stopPropagation();
		if($(this).is(":checked")){
			$(".rep_tbl table tr td input[type=checkbox]").attr("checked","checked");
		}else{
			$(".rep_tbl table tr td input[type=checkbox]").removeAttr("checked");
		}
		
	});

	
});
