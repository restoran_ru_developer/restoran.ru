$(document).ready(function() {
	$("#bonus .print_bonus").click(function(){
		var lnk = $(this).attr("href");
		
		$("#bonus").attr("action", lnk).submit();
		
		return false;
	});
	
	$("#bonus .manage a").click(function(){
		$("#bonus .manage a").removeClass("active");
		$(this).addClass("active");
			
		var cl = $(this).attr("href").replace("#","");
		if(cl==""){
                    $("#bonus table .cltr").hide();
			$("#bonus table tr:not(.head)").show();
			$("#bonus table .nums").show();                        
		}else{
			$("#bonus table tr:not(.head)").hide();
                        $("#bonus table .nums").hide();
                        $("#bonus table .cltr").hide();
			//$("#bonus table tr."+cl).show();			
			$("#bonus table ."+cl).show();			
			
		}
		
		
		return false;	
	});
	
	
	$("#bonus .show_pays").click(function(){
		var pays = $(this).parent("td").find(".pays");
		if(pays.is(":hidden")){
			pays.slideDown(200);
		}else{
			pays.slideUp(200);	
		}
		
		return false;
	});
	
	
	$("#bonus .dopay").click(function(e){
		e.preventDefault();
		var tel = $(this).parent("td").parent("tr").find(".c2").html().replace("_","");
		var id = $(this).parent("td").parent("tr").attr("id").replace("client","");
		var rest = $(this).parent("td").parent("tr").attr("rest_data");
		var order = $(this).parent("td").parent("tr").attr("order_data");
		var btn = $(this);
		var summ = $(this).attr("href").replace("#","");
		var line = $(this).parent("td").parent("tr");
		
		confirm("Отправить "+summ+" рублей на телефон "+tel+" ?", function () {
            $.post(core, {act: "send_bonus", summ: summ, client_id: id, tel: tel ,rest: rest, order:order }, function(html){
                var otvet = eval('(' + html + ')');

                console.log(otvet);

                if(otvet.ACT=="redirect") window.location.replace(otvet.URL);

                if(otvet.STATUS=="ok"){
                    btn.remove();
                    $("#modal").modal({closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>"});
                    $("#simplemodal-container .message").html(otvet.TEXT).removeClass("red");
                    if(summ==100) line.remove();

                    console.log(otvet.PAYMEN_ID);
                }

                if(otvet.STATUS=="error"){
                    $("#modal").modal({closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>"});
                    $("#simplemodal-container .message").html(otvet.TEXT).addClass("red");
                    //$.modal(otvet.TEXT);
                    console.log(otvet.PAYMEN_ID);
                }
            });
		});

		
		return false;
	});
	
	
	
	function confirm(message, callback) {
		$('#confirm').modal({
			closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
			position: ["20%",],
			overlayId: 'confirm-overlay',
			containerId: 'confirm-container', 
			onShow: function (dialog) {
				var modal = this;
	
				$('.message', dialog.data[0]).append(message);
				$('.yes', dialog.data[0]).click(function () {
					// call the callback
					if ($.isFunction(callback)) {
						callback.apply();
					}
					// close the dialog
					modal.close(); // or $.modal.close();
				});
			}
		});
	}
	
				
});
