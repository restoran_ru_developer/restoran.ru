var core = "/bitrix/templates/Booking/core.php";
var ltop = 0; 
var show_ajax_runer=1;
var ajax_runer =0;
var auto_refresh = true;
var ajax_load=true;

$(function(){
    $(document).bind("ajaxSend",function(){
        if(ajax_load) {
            $("#system_loading").show();
        }
    }).bind("ajaxComplete",function(){
        $("#system_loading").hide();
    });

    $('.submit-client-sms').on('click', function(){

        var params = {"phone":$('#phone').val(),"textmessage":$('textarea[name="sms_client_text"]').val()};
        $.ajax({
            type: "POST",
            url: '/bitrix/templates/Booking/ajax/send_sms.php',
            data: params,
            dataType: 'json'
        })
        .done(function(data) {
            //$('#booking_form .modal-body').html(data);
                alert(data.message);
        });
    })
})
function cook_opener(){
		$(".orders .dater").each(function(){
			var id=$(this).attr("id");
			var d = $(this).find("a").attr("rel");
			
			if($.cookie("c_"+id)=='open'){
				$(this).addClass("open");
				$(this).removeClass("closed");
				$(".orders .dt"+d).show();
			}
			
			if($.cookie("c_"+id)=='closed'){
				$(this).removeClass("open");
				$(this).addClass("closed");
				$(".orders .dt"+d).hide();
			}
		
		});
	}


jQuery.fn.capitalize = function() {
    $(this[0]).keyup(function(event) {
        var box = event.target;
        var txt = $(this).val();
        var start = box.selectionStart;
        var end = box.selectionEnd;
        $(this).val(txt.replace(/^(.)|(\-)(.)/g, function($1) {
            return $1.toUpperCase();
        }));
       // box.setSelectionRange(start, end);
    });

   return this;
}


function clear_window(w){
	$("#"+w+" input, #"+w+" textarea").each(function(){
		$(this).val("");		
	});
	$("#"+w+" .select").each(function(){
		$(this).find(".selected_element").html("&nbsp;");		
	});
	
	if(w=="w3"){
		$("#w3 .rinfo").html("");
		$("#status .selected_element").attr("class", "selected_element");
	} 
	
	if(w=="w4") $("#w4 .content").html("");
}


 function myFocus(element) {
     if (element.value == element.defaultValue) {
       element.value = '';
     }
   }
   function myBlur(element) {
     if (element.value == '') {
     	$("#w2 input[name=restoran_id]").val("");
       	element.value = element.defaultValue;
     }
   }
   

function save_win_pos(event, ui){
			if(ui) var id = ui.helper.attr("id");
			var to_send = {};
			
			$(".window").each(function(){
				var w = {};
				w["top"] = parseInt($(this).css("top"));
				w["left"] = parseInt($(this).css("left"));
				w["width"] = parseInt($(this).width());
				w["height"] = parseInt($(this).height());
				
				to_send[$(this).attr("id")] = w;
			});
			to_send["act"] = "save_win_pos";
			to_send["w_id"] = id;
			to_send["razdel"] = $("#w2 .menu .active").attr("href");
			if($("#w3 .rinfo").is(":hidden")) to_send["rest_info"]="hide";
			else to_send["rest_info"]="show";
			
			$.post(core, to_send, function(otvet){
				if(id=="w2"){
					if($("#restorans_filter").length>0){
						$("#w2 .paginator").remove();
						$("#w2 .tbl").replaceWith(otvet);
					}else $("#w2 .content").html(otvet);
			
					rest_list();
				}
				
			});
			
			
		
		}

$(document).ready(function() {
		
		$("#w1 input, #w1 textarea").keydown(function(){
			$("#client_changed").val("Y");
		
		});
		
		$("#w3 input, #w3 textarea").keydown(function(){
			$("#order_changed").val("Y");
		});
	



		/*********ПРОВЕРЯЕМ НОВЫЕ ЗАЯВКИ***********/
		function check_new(){
			var to_send = {};
			to_send["act"] = "check_new";
			show_ajax_runer=0;
            ajax_load = false;
			$.post(core, to_send, function(html){
				var arr = eval('(' + html + ')');
				if(arr.SMS>0) $("#new_sms").show().find("i").html(arr.SMS); 
				else $("#new_sms").hide();
			
				if(arr.ORDERS>0){
					$("#new_order").show().find("i").html(arr.ORDERS); 
					if($("#sound").length>0) $("#sound")[0].play();
				} else $("#new_order").hide();
				
				show_ajax_runer=1;
                ajax_load = true;
			});
		
		}
		
		
		
		/*********ПРОВЕРЯЕМ НОВЫЕ бонусы***********/
		function check_new_bonus(){
			var to_send = {};
			to_send["act"] = "check_new_bonus";
			show_ajax_runer=0;
            ajax_load = false;
			$.post(core, to_send, function(html){
				var arr = eval('(' + html + ')');
				
			
				if(arr.BONUS>0){
					$("#new_bonus").show().find("i").html(arr.BONUS); 
					if($("#sound").length>0) $("#sound")[0].play();
				} else $("#new_bonus").hide();
				
				show_ajax_runer=1;
                ajax_load = true;
			});
		
		}
		
		$("#new_bonus").click(function(){
			window.location.replace("/bs/bonus/");
		});
		
		/*****************ОБНОВЛЯЕМ 2Е ОКНО КАЖДЫЕ 30 СЕКУНД**********************/
		function refresh_win(){
			var to_send = {};
			
			show_ajax_runer=0;
			/*
			if($("#w2 .menu .active").attr("href")=="#clients" && auto_refresh){
				var to_send = {};
				to_send["order"] = $(this).attr("href");
				to_send["act"] = "change_razdel";
				to_send["razdel"] = $("#w2 .menu .active").attr("href");
				to_send["PAGEN_1"] = $("#w2 .pager span:not(.not_pager)").html();
				
				$.post(core, to_send, function(otvet){
					$("#w2 .content").html(otvet);
					rest_list();
					show_ajax_runer=1;
				});
			}
			*/
			
			if($("#w2 .menu .active").attr("href")=="#orders" && auto_refresh){
				var to_send = {};
				to_send["order"] = $(this).attr("href");
				to_send["act"] = "change_razdel";
				to_send["razdel"] = $("#w2 .menu .active").attr("href");
				to_send["PAGEN_1"] = $("#w2 .pager span:not(.not_pager)").html();
				
				$.post(core, to_send, function(otvet){
					$("#w2 .content").html(otvet);
					rest_list();
					show_ajax_runer=1;
					cook_opener();
				});
			}
			
		
		
		}
		
		
		//setInterval(refresh_win, 30000);
		if($("#w1").length>0) {
			setInterval(check_new, 30000);
			setInterval(check_new_bonus, 600000);
		}

        if(typeof(LOOKON) == "undefined"){
            check_new_bonus();
        }

		
		$(".window").resizable({ handles: 'se', minHeight: 96, minWidth: 330, stop: save_win_pos});
		$( ".pos2" ).resizable( "option", "minWidth", 560);
		
		$( ".pos3" ).resizable( "option", "minWidth", 381);
		
		$( ".pos1" ).resizable( "option", "minHeight", $( ".pos1" ).height());
		$( ".pos1" ).resizable( "option", "maxHeight", $( ".pos1" ).height());
		
		//$( ".pos3" ).css("height", 645);
		$( ".pos3" ).resizable( "option", "minHeight", $( ".pos3" ).height());
		$( ".pos3" ).resizable( "option", "maxHeight", $( ".pos3" ).height());
		
		
		$(".window").draggable({ handle: ".head", stop: save_win_pos});

		if($(".pos4" ).css("top")=="" || $(".pos4" ).css("top")=="auto") $(".pos4" ).css("top",$(".pos2").height()+$(".pos2").offset().top);
		
		if($(".pos1").length>0){
			var mh = 0;
			if($(".pos1" ).height()+$(".pos3").height()>$(".pos2").height()+$(".pos4").height()) mh =$(".pos1" ).height()+$(".pos3").height()+100;
			else mh = $(".pos2").height()+$(".pos3").height()+100;
			
			$("#content").height(mh);
		}
		
		$(".tbl:not(.reports,.rep_tbl) tr:not(.dater)").live("mouseenter", function(){
			$(this).addClass("tr_hover");
		}).live("mouseleave", function(){
			$(this).removeClass("tr_hover");
		});

		
		$(".rep_tbl tr:not(.spacer)").live("mouseenter", function(){
			$(this).addClass("tr_hover");
		}).live("mouseleave", function(){
			$(this).removeClass("tr_hover");
		});
		
		
		$(".tbl:not(.reports,.rep_tbl) tr:not(.dater)").live("click", function(){
			if($(this).hasClass("tr_selected")) $(this).removeClass("tr_selected");
			else {
				$(".tbl tr").removeClass("tr_selected");
				$(this).addClass("tr_selected");
			}
		});
		
		
		
		
		$("#position a.clear").mouseenter(function(){
			$(this).animate({opacity:1}, 200);
		}).mouseleave(function(){
			$(this).animate({opacity:0.2}, 200);
		}).click(function(){
			var to_send = {};
			to_send["act"] = "clear_pos";
			$.post(core, to_send, function(otvet){
				if(otvet=="ok") window.location.reload();
			});
		});
		
		
		$("#city_pos").mouseenter(function(){
			$(this).animate({opacity:1}, 200);
		}).mouseleave(function(){
			$(this).animate({opacity:0.2}, 200);
		});
		
		
	////Аякс лоадер
	$("#ajax_loader").ajaxStart(function(){
		ajax_runer=1;
		if(show_ajax_runer==1){
   			$(this).show();
   		}
	}).ajaxStop(function(){
   		$(this).hide();
   		ajax_runer=0;
   		//show_ajax_runer=0;
	});
	
	$("#conteiner").mousemove(function(e){
		
			$("#ajax_loader").css("left",e.pageX+15);
			$("#ajax_loader").css("top",e.pageY+0);

	});











});
