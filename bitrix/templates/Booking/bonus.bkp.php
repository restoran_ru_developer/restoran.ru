<?


function get_token(){
	require_once(dirname(__FILE__) . '/yam_src/consts.php');
	require_once(dirname(__FILE__) . '/yam_src/lib/YandexMoney.php');

	$scope = 'account-info operation-history operation-details payment.to-pattern("phone-topup")'; // this is scope of permissions
	$authUri = YandexMoney::authorizeUri(CLIENT_ID, REDIRECT_URI, $scope);
	return $authUri;
}


function check_tocken(){
	$EXIT=array();
	//берем токен из файлика
	$file_array = file(dirname(__FILE__) . '/yam_src/token');
	$token=$file_array[0];
	
	if($token==""){
		$EXIT["URL"]=get_token();
		$EXIT["ACT"]="redirect";
		echo json_encode($EXIT);
		die();
	}else return $token;
}


function send_bonus(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	
	$res = CIBlockElement::GetByID($_REQUEST["client_id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		
		//var_dump($arFields);
		//die();
		if($arFields["IBLOCK_SECTION_ID"]==83823) $CITY="spb";
		else $CITY="msk";
		
		
		$arProps["TELEPHONE"]["VALUE"] = trim(str_replace("_", "", $arProps["TELEPHONE"]["VALUE"]));
		
		$arProps["TELEPHONE"]["VALUE"] = str_replace("+", "", $arProps["TELEPHONE"]["VALUE"]);
		
		$FL=$arProps["TELEPHONE"]["VALUE"]{0};
		
		if($FL!="7") $arProps["TELEPHONE"]["VALUE"]="7".$arProps["TELEPHONE"]["VALUE"];
		
		
		$token = check_tocken();
		
		require_once(dirname(__FILE__) . '/yam_src/consts.php');
		require_once(dirname(__FILE__) . '/yam_src/lib/YandexMoney.php');
	
		$ym = new YandexMoney(YOUR_APP_CLIENT_ID, dirname(__FILE__) . '/yalog.log');
		
		
		
		$params["phone-number"] = $arProps["TELEPHONE"]["VALUE"]; 
		$params["amount"] = $_REQUEST["summ"];
		$params["pattern_id"] = "phone-topup"; 
		
		
		$rest = CIBlockElement::GetByID($_REQUEST["rest"]);
		if($arFields_rest = $rest->GetNext()){
			$REST_NAME=$arFields_rest["NAME"];
		}
		
		
		
		
		
		//потом удалить
		/*
		$params["phone-number"] = "79213054450";
		$params["amount"] = "1";
		 
		 
		$SMSTXT="Спасибо, что пользуетесь сервисом Restoran.ru! Вам комплимент - ".$_REQUEST["summ"]."р. на счет.";
		$SMSTXT.="Звоните нам чаще! ";
		
		if($CITY=="spb") $SMSTXT.="http://www.restoran.ru/spb/news/sms/ ";
		else $SMSTXT.="http://www.restoran.ru/msk/news/sms/ ";
		
		if($CITY=="spb") $SMSTXT.="+78127401820";
		else $SMSTXT.="+74959882656";
		
		
		var_dump($SMSTXT);
		CModule::IncludeModule("sozdavatel.sms");
		CSMS::Send($SMSTXT, "+".$params["phone-number"], "UTF-8");
		die();
		*/
		
		//
		
		//var_dump($params);
		//die();
		
		$VARDUMP=array();
		$VARDUMP["params"]= var_export($params, true);
		
		$resp = $ym->requestPaymentShop($token, $params);
		$VARDUMP["resp"]= var_export($resp, true);

		$requestId = $resp->getRequestId();
		$VARDUMP["requestId"]= var_export($requestId, true);
		
		$resp = $ym->processPaymentByWallet($token, $requestId); 
		$VARDUMP["resp2"]= var_export($resp, true);
		if ($resp->isSuccess()){
			
			$PaymentId = $resp->getPaymentId();
		
			$EXIT["STATUS"]="ok";
			$EXIT["ACT"]="alert";
			$EXIT["TEXT"]="Операция выполнена успешно!";
		
			
			
			$el = new CIBlockElement;
		
			$PROP = array();
			$PROP["client"] = $arFields["ID"];  
			$PROP["summ"] = $_REQUEST["summ"];       
		
			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
				"IBLOCK_ID"      => 2964,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => $arFields["ID"]."_".$_REQUEST["summ"],
				"ACTIVE"         => "Y",            // активен
				"PREVIEW_TEXT"	=> $PaymentId            
			);
		
			$BONUS_ID = $el->Add($arLoadProductArray);
			
			echo json_encode($EXIT);
			
			if($PROP["summ"]==100 && $REST_NAME!=""){
				/*
				Спасибо за обращение в службу брони Restoran.ru и посещение
ресторана "имяресторана".Вам бонус 100р на ваш телефон.Наш телефон: +78127401820"
				*/
				$SMSTXT="Спасибо за обращение в службу брони Restoran.ru и посещение ресторана «".$REST_NAME."».";
				$SMSTXT.="Вам бонус ".$_REQUEST["summ"]."р. на ваш телефон.";
				$SMSTXT.="Наш телефон: +78127401820";
				
				
				$order = CIBlockElement::GetByID($_REQUEST["order"]);
				if($arFields_order = $order->GetNext()){
					CIBlockElement::SetPropertyValuesEx($_REQUEST["order"], false, array("bonus100" => 2448));
				}
				/*Спасибо за обращение в службу брони Ресторан.ру и посещение ресторана «имярек». Вам бонус 100р. Наш телефон: +78127401820*/
				
			}else{
				/*Спасибо за обращение в службу брони Restoran.ru.Вам бонус на ваш телефон.Наш телефон:
+74959882656/+78127401820"*/		
				$SMSTXT="Спасибо за обращение в службу брони Restoran.ru! Вам бонус на ваш телефон.";
				$SMSTXT.="Наш телефон: ";
				
				if($CITY=="spb") $SMSTXT.="+78127401820";
				else $SMSTXT.="+74959882656";
			}
			
			
			CModule::IncludeModule("sozdavatel.sms");
			CSMS::Send($SMSTXT, "+".$params["phone-number"], "UTF-8");
			
		}else{
			///Оплата не прошла
			$EXIT["STATUS"]="error";
			$EXIT["ACT"]="alert";
			$EXIT["TEXT"]=$resp->getError();
			$EXIT["PARAMS"]=serialize($params);
$EXIT["VDUMP"]=serialize($VARDUMP);
			
			$VARDUMP = var_export($VARDUMP, true);
			
			$TO_SEND=array("VARDUMP"=>$VARDUMP);
			CEvent::Send('ERROR_LOG', SITE_ID, $TO_SEND, "Y", 145);
			
			
			if($EXIT["TEXT"]=="not_enough_funds") $EXIT["TEXT"]="Ошибка: Недостаточно средств";
			if($EXIT["TEXT"]=="limit_exceeded") $EXIT["TEXT"]="Ошибка: Превышение лимита";
			if($EXIT["TEXT"]=="contract_not_found") $EXIT["TEXT"]="Ошибка: Неправильный номер телефона";
			
			echo json_encode($EXIT);
		    die();
		}		
	}
}	
	
	
if($_REQUEST["act"]=="send_bonus") send_bonus(); 	
	
	
	
	
?>