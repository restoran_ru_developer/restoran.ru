<?
define("NO_KEEP_STATISTIC", true);

define("SITE_TEMPLATE_PATH", "/bitrix_personal/templates/Booking");
define("SITE_TEMPLATE_ID", "Booking");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/*Добавить проверку на авторизацию и все такое*/
/*нужно сделать дву универсальные функциидля запроса инфы по заказу и по клиенту*/

//FirePHP::getInstance()->info($_REQUEST["act"],'in core');

include 'bonus.php';

if($_SESSION["CITY"]==""){
    $rsUser = CUser::GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();


    $arGroups = CUser::GetUserGroup($USER->GetID());

    $arUser["UF_CITY"] = strtolower($arUser["UF_CITY"]);
    $arUser["UF_CITY"] = explode(",", $arUser["UF_CITY"]);

	if(!in_array($_SESSION["CITY"], $arUser["UF_CITY"])) unset($_SESSION["CITY"]);

    if(in_array(19, $arGroups)==0 && !$USER->IsAdmin()) {
        if(is_array($arUser["UF_CITY"])){
            if($_SESSION["CITY"]=="") $_SESSION["CITY"]=$arUser["UF_CITY"][0];
        }elseif($_SESSION["CITY"]=="") $_SESSION["CITY"]=$arUser["UF_CITY"];

    }
    
    if($_SESSION["CITY"]==""){
		if(is_array($arUser["UF_CITY"])) $_SESSION["CITY"]=$arUser["UF_CITY"][0];
		else $_SESSION["CITY"]=$arUser["UF_CITY"];
			
			
		if($_SESSION["CITY"]=="") $_SESSION["CITY"]="msk";	
	}
}


function user_auth(){
    global $USER;
    if (!is_object($USER)) $USER = new CUser;
    $arAuthResult = $USER->Login($_REQUEST["USER_LOGIN"], $_REQUEST["USER_PASSWORD"], "N");

    if($arAuthResult["TYPE"]=="ERROR"){
        echo $arAuthResult["MESSAGE"];
    }else{
        echo "ok";
        $_SESSION["OPERATOR_NUMBER"] = $_REQUEST["OPERATOR_NUMBER"];

    }
}

function save_win_pos(){
    global $USER;
    $W_ID = $_REQUEST["w_id"];
    $RAZDEL = $_REQUEST["razdel"];

    unset($_REQUEST["act"]);
    unset($_REQUEST["w_id"]);
    unset($_REQUEST["razdel"]);
    //v_dump($_REQUEST);

    $USER->Update($USER->GetID(), Array("UF_WIN_POS"=> serialize($_REQUEST)));

    if($W_ID=="w2"){
        $_REQUEST["razdel"]=$RAZDEL;
        //Пересчитываем высоту окна
        $C_HEIGHT = $_REQUEST["w2"]["height"] - 30 - 20-10 -22;
        $_SESSION["CLIENTS_PAGE_CO"]=round($C_HEIGHT/28);

        change_razdel("Y");

    }

}

function clear_pos(){
    global $USER;
    $USER->Update($USER->GetID(), Array("UF_WIN_POS"=> ""));
    echo "ok";
}


function resort_clients(){
    global $APPLICATION;

    if($_REQUEST["order"]=="#ID" || !isset($_SESSION["CLIENTS_ORDER"])) {
        $_SESSION["CLIENTS_ORDER"]="ID";
        $_SESSION["CLIENTS_BY"] = "DESC";
    }

    if($_REQUEST["order"]=="#SURNAME") {
        $_SESSION["CLIENTS_ORDER"]="PROPERTY_SURNAME";
        $_SESSION["CLIENTS_BY"] = "ASC";
    }

    if($_REQUEST["order"]=="#NAME") {
        $_SESSION["CLIENTS_ORDER"]="PROPERTY_NAME";
        $_SESSION["CLIENTS_BY"] = "ASC";
    }


    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/clients.php"),
        Array(),
        Array("MODE"=>"php")
    );

}

function get_client_info(){

    global $APPLICATION;
    $_REQUEST["id"] = str_replace("client_", "", $_REQUEST["id"]);

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $EXIT = array();
    $res = CIBlockElement::GetByID($_REQUEST["id"]);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        $EXIT["CLIENT_ID"] = $arFields["ID"];

        $EXIT["NAME"] = $arProps["NAME"]["VALUE"];
        $EXIT["SURNAME"] = $arProps["SURNAME"]["VALUE"];
        $EXIT["EMAIL"] = $arProps["EMAIL"]["VALUE"];
        $EXIT["TELEPHONE"] = $arProps["TELEPHONE"]["VALUE"];
        $EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];

        $EXIT["PRIM"] = str_replace("<br />", "", $EXIT["PRIM"]);

        $EXIT["USERID"] = $arProps["user"]["VALUE"];
        $EXIT["USER_ID"] = $arProps["user"]["VALUE"];

        if($arProps["user"]["VALUE"]!="") $EXIT["USRLNK"]="/users/id".$EXIT["USERID"]."/";
        //$EXIT["ORDERS"]

        $EXIT["ORDERS"]='<div class="tbl">';
        $EXIT["ORDERS"].='<table cellpadding="0" cellspacing="0">';
        $EXIT["ORDERS"].='<tr class="hd">';
        $EXIT["ORDERS"].='<th class="c11">ресторан</th>';
        $EXIT["ORDERS"].='<th class="c12">гостей</th>';
        $EXIT["ORDERS"].='<th class="c13">визит</th>';
        $EXIT["ORDERS"].='<th class="c14">статус</th>';
        $EXIT["ORDERS"].='</tr>';

        //Теперь нужно запросить все заказы для этого пользователя
        //Эту часть нужно будет закэшировать
        $HO=false;
        $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_client"=>$arFields["ID"]);
        $res_zak = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter, false, false, false);
        while($ob_zak= $res_zak->GetNextElement()){
            $arFields_zak = $ob_zak->GetFields();
            $arProps_zak = $ob_zak->GetProperties();

//            $arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", MakeTimeStamp($arFields_zak["ACTIVE_FROM"], CSite::GetDateFormat()));
            $arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", strtotime($arFields_zak["ACTIVE_FROM"]));

            $resr = CIBlockElement::GetByID($arProps_zak["rest"]["VALUE"]);
            if($arFields_rest = $resr->Fetch()){
                $REST_NAME = $arFields_rest["NAME"];

            }else $REST_NAME="Подбор";



            $EXIT["ORDERS"].='<tr id="order_'.$arFields_zak["ID"].'">';
            $EXIT["ORDERS"].='<td class="c11"><div>&nbsp;'.$REST_NAME.'</div></td>';
            $EXIT["ORDERS"].='<td class="c12"><div>&nbsp;'.$arProps_zak["guest"]["VALUE"].'</div></td>';
            $EXIT["ORDERS"].='<td class="c13"><div>&nbsp;'.$arFields_zak["ACTIVE_FROM"].'</div></td>';
            $EXIT["ORDERS"].='<td class="c14"><div>&nbsp;'.$arProps_zak["status"]["VALUE"].'</div></td>';
            $EXIT["ORDERS"].='</tr>';

            $HO=true;

        }

        $EXIT["ORDERS"].='</table>';
        $EXIT["ORDERS"].='</div>';

        if(!$HO) $EXIT["ORDERS"]="";

    }

    echo json_encode($EXIT);
}


function change_razdel($T1){
    global $APPLICATION;



    if($_REQUEST["razdel"]=="#clients"){

        if($_REQUEST["order"]=="#ID" || !isset($_SESSION["CLIENTS_ORDER"])) {
            $_SESSION["CLIENTS_ORDER"]="ID";
            $_SESSION["CLIENTS_BY"] = "DESC";
        }

        if($_REQUEST["order"]=="#SURNAME") {
            $_SESSION["CLIENTS_ORDER"]="PROPERTY_SURNAME";
            $_SESSION["CLIENTS_BY"] = "ASC";
        }

        if($_REQUEST["order"]=="#NAME") {
            $_SESSION["CLIENTS_ORDER"]="PROPERTY_NAME";
            $_SESSION["CLIENTS_BY"] = "ASC";
        }


        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/clients.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }

    if($_REQUEST["razdel"]=="#orders"){
        if($_REQUEST["order"]=="#ACTIVE_FROM" || !isset($_SESSION["ORDERS_ORDER"])) {
            $_SESSION["ORDERS_ORDER"]="ACTIVE_FROM";
            $_SESSION["ORDERS_BY"] = "ASC";
        }

        if($_REQUEST["order"]=="#ID") {
            $_SESSION["ORDERS_ORDER"]="ID";
            $_SESSION["ORDERS_BY"] = "DESC";
        }

        global $arrFilter;
        //$arrFilter["PROPERTY_status_VALUE"]=array("заказ принят", "забронирован", "банкет в работе", "внесена предоплата");
        $arrFilter["PROPERTY_status"]=array(1418, 1467, 1503, 1504);

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/orders.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }


    if($_REQUEST["razdel"]=="#all_orders"){

        if($_REQUEST["order"]=="#ACTIVE_FROM" || !isset($_SESSION["ORDERS_ORDER"])) {
            $_SESSION["ORDERS_ORDER"]="ACTIVE_FROM";
            $_SESSION["ORDERS_BY"] = "ASC";
        }

        if($_REQUEST["order"]=="#ID") {
            $_SESSION["ORDERS_ORDER"]="ID";
            $_SESSION["ORDERS_BY"] = "DESC";
        }

        global $arrFilter;

        if($_SESSION["ALL_ORDERS_FLTR"]) $arrFilter=$_SESSION["ALL_ORDERS_FLTR"];

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/all_orders.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }


    if($_REQUEST["razdel"]=="#new_orders"){
        global $arrFilter;
        $arrFilter["PROPERTY_new_VALUE"]="Y";

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/all_orders.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }






    if($_REQUEST["razdel"]=="#reports"){


        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/reports.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }


    if($_REQUEST["razdel"]=="#restorans"){
        if(isset($_REQUEST["PAGEN_1"]) || $T1=="Y") $SHOW_FLTR="N";
        else $SHOW_FLTR="Y";

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/restorans.php"),
            Array("SHOW_FLTR"=>$SHOW_FLTR),
            Array("MODE"=>"php")
        );
    }else{
        unset($_SESSION["REST_FLTR"]);
    }

    if($_REQUEST["razdel"]=="#sms"){
        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/sms.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }

}

//нужно проверить права на изменение
//нужно вернуть строку с новыми данными и заменяем старую в w2
function update_client(){
    global $APPLICATION;
    global $USER;

    $arGroups = CUser::GetUserGroup($USER->GetID());

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }


    $EXIT2 =$EXIT2 =array();

    //сначала смотрим, апдэйтим ли мы клиента, или создаем нового
    if($_REQUEST["client_id"]>0){
        //апдейтим клиента
        $el = new CIBlockElement;

        $PROP = array();
        $PROP["NAME"] = $_REQUEST["name"];
        $PROP["SURNAME"] = $_REQUEST["surname"];
        $PROP["TELEPHONE"] = $_REQUEST["telephone"];
        $PROP["EMAIL"] = $_REQUEST["email"];
        $PROP["user"] = $_REQUEST["user"];

        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"],
            "ACTIVE"         => "Y",            // активен
            "PREVIEW_TEXT"   => $_REQUEST["prim"],
        );

        $res = $el->Update($_REQUEST["client_id"], $arLoadProductArray);
        $EXIT["message"]="client_updated";

        $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
    }else{
        //если это не пользователь с сайта
        if($_REQUEST["user"]==""){
            //добавляем нового
            $EXIT = add_new_client();

            $_REQUEST["client_id"]=$EXIT["client"];

            $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
        }
    }



    if($_REQUEST["rastoran_id"]=="") $_REQUEST["rastoran_id"]=0;


    //далее смотрим, на бронь
    if($_REQUEST["order_id"]>0){

        @unlink($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders/".$_REQUEST["id"]);

        //апдэйтим существующую бронь
        //Нужно получить текущий статус заказа и сумму счета
        $ro = CIBlockElement::GetByID($_REQUEST["order_id"]);
        if($ar_res_ro = $ro->GetNextElement()){
            $orPrps = $ar_res_ro->GetProperties();
            $orFlds = $ar_res_ro->GetFields();
        }

        //если заказ меняет оператор и у заказа статус отчет
        if(!in_array(17, $arGroups) && $orPrps["status"]["VALUE"]=="отчет"){
            $EXIT["message"] ="permission denied";
        }else{

            /*****ПИШЕМ В ЛОГ*****/
            //При изминении статуса или суммы счета
            //$orPrps["status"]["VALUE"]!=$_REQUEST["status"] ||
            if($orPrps["summ_sch"]["VALUE"]!=$_REQUEST["summ_sch"] && $orPrps["summ_sch"]["VALUE"]!="" && $orPrps["summ_sch"]["VALUE"]>0){
                $LOG="";
                if($orPrps["summ_sch"]["VALUE"]=="") $S=0;
                else $S=$orPrps["summ_sch"]["VALUE"];

                if($_REQUEST["summ_sch"]=="") $NA=0;
                else $NA=$_REQUEST["summ_sch"];

                //if($orPrps["status"]["VALUE"]!=$_REQUEST["status"]) $LOG.="Изменен статус заказа ()";
                if($orPrps["summ_sch"]["VALUE"]!=$_REQUEST["summ_sch"]) $LOG.="Изменена сумма счета с ".$S." на ".$NA;

                $el2 = new CIBlockElement;

                $arLoadProductArray = Array(
                    "MODIFIED_BY"    => $USER->GetID(),
                    "IBLOCK_ID"      => 2810,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => $_REQUEST["order_id"],
                    "ACTIVE"         => "Y",
                    "PREVIEW_TEXT"   => $LOG
                );

                $el2->Add($arLoadProductArray);
            }
            /*******************/

            $el = new CIBlockElement;

            $PROP2 = array();
            $PROP2["rest"] = $_REQUEST["rastoran_id"];
            $PROP2["guest"] = $_REQUEST["guest"];

            $PROP2["client"] = $_REQUEST["client_id"];
            $PROP2["prinyal"] = $_REQUEST["prinyal"];
            $PROP2["source"] = $_REQUEST["source"];
            $PROP2["podtv"] = $_REQUEST["podtv"];
            $PROP2["summ_sch"] = $_REQUEST["summ_sch"];
            $PROP2["podtv"] = $_REQUEST["podtv"];

            $PROP2["user"] = $_REQUEST["user"];

            $PROP2["stol"] = $_REQUEST["stol"];
            $PROP2["podtv2"] = $_REQUEST["podtv2"];
            $PROP2["type"] = $_REQUEST["type"];

            $PROP2["procent"] = $_REQUEST["procent"];

            $PROP2["format"] = $_REQUEST["format"];

            $PROP2["status"] = $_REQUEST["status"];

            $PROP2["control_call"] = $_REQUEST["control_call"];
            $PROP2["yandexid"] = $orPrps["yandexid"]["VALUE"];


            $s_st = "";
            if ($_REQUEST["status"]=="1467")
                $s_st = "APPROVED";
            elseif ($_REQUEST["status"]=="1809")
                $s_st = "CANCELLED_BY_ORGANIZATION";
            elseif($_REQUEST["status"]=="1499")
                $s_st = "COME";
            elseif($_REQUEST["status"]=="1500")
                $s_st = "DID_NOT_COME";
            elseif($_REQUEST["status"]=="1501")
                $s_st = "CANCELLED_BY_USER";
            elseif ($_REQUEST["status"]=="1502")
                $s_st = "CANCELLED_BY_ORGANIZATION";
            else
                $s_st = "";
//                if ($s_st)
//                {
//                    include($_SERVER["DOCUMENT_ROOT"].'/yandex/jsonrpc.php');
//                    $client = new JsonRpc( 'http://restoranru123:2f14e0303b4ae367196ef67abb1356dc344d95b5@api.booking.mfront01ht.yandex.ru/api ' );
//                    $ya_result = $client->updateBookStatus( $orPrps["yandexid"]["VALUE"], $s_st );
//                    v_dump($ya_result);
//                }

            //if ($_REQUEST["user"]&& in_array(15, CUser::GetUserGroup($_REQUEST["user"])))
            if ($PROP["TELEPHONE"])
            {

                if ($_REQUEST["status"]==1467 && $orPrps["status"]["VALUE"]!="забронирован")
                {
                    CModule::IncludeModule("sozdavatel.sms");
                    $rrest = CIBlockElement::GetByID($orPrps["rest"]["VALUE"]);
                    if ($ar_rest = $rrest->Fetch())
                    {

                        if($ar_rest['IBLOCK_ID']==3624||$ar_rest['IBLOCK_ID']==3625||$ar_rest['IBLOCK_ID']==2586||$ar_rest['IBLOCK_ID']==2587){
                            $from_en_host = true;
                        }

                        $db_props = CIBlockElement::GetProperty($ar_rest["IBLOCK_ID"], $ar_rest["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
                        while($ar_props = $db_props->Fetch())
                        {
                            $address[] = $ar_props["VALUE"];
                        }
                        if (count($address)>1)
                            $address = "";
                        else
                            $address = $address[0];

                        if ($ar_rest["IBLOCK_ID"] == 3566 || $ar_rest["IBLOCK_ID"] == 3567) {
                            $this_is_latvia = true;
                        }

                        if(!$this_is_latvia && !$from_en_host) {
                            $message = "Ресторан.ру забронировал для Вас столик в ресторане ".$ar_rest["NAME"]." на ".substr($orFlds["ACTIVE_FROM"], 0, 16).", на ".$orPrps["guest"]["VALUE"]." ".pluralForm($orPrps["guest"]["VALUE"],"человека","человека","человек").".";
                        }
                        elseif($from_en_host){
                            $message = "Restoran.ru has booked a table for you at the ".$ar_rest["NAME"]." Restaurant for ".substr($orFlds["ACTIVE_FROM"], 0, 16)." for ".$orPrps["guest"]["VALUE"]." people.";
                        }

                        if ($address && !$this_is_latvia && !$from_en_host)
                        {
                            $message .= " Адрес: ".$address." ";


                            $show = false;
                            $rsData = CBXShortUri::GetList(Array(), Array());
                            while($arRes = $rsData->Fetch()) {
                                if ($arRes["URI"] == "/content/catalog/map.php?ID=".$ar_rest["ID"]) {
                                    $str_SHORT_URI = $arRes["SHORT_URI"];
                                    $show = true;
                                }
                            }
                            if ($show):
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            else:
                                $str_SHORT_URI = CBXShortUri::GenerateShortUri();
                                $arFields = Array(
                                    "URI" => "/content/catalog/map.php?ID=".$ar_rest["ID"],
                                    "SHORT_URI" => $str_SHORT_URI,
                                    "STATUS" => "301",
                                );
                                $ID = CBXShortUri::Add($arFields);
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            endif;
                            $message .= $map_url;
                            $message .= " .";
                        }

                        if($address&&$from_en_host){
                            $message .= " The address is: ".$address." ";


                            $show = false;
                            $rsData = CBXShortUri::GetList(Array(), Array());
                            while($arRes = $rsData->Fetch()) {
                                if ($arRes["URI"] == "/content/catalog/map.php?ID=".$ar_rest["ID"]) {
                                    $str_SHORT_URI = $arRes["SHORT_URI"];
                                    $show = true;
                                }
                            }
                            if ($show):
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            else:
                                $str_SHORT_URI = CBXShortUri::GenerateShortUri();
                                $arFields = Array(
                                    "URI" => "/content/catalog/map.php?ID=".$ar_rest["ID"],
                                    "SHORT_URI" => $str_SHORT_URI,
                                    "STATUS" => "301",
                                );
                                $ID = CBXShortUri::Add($arFields);
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            endif;
                            $message .= $map_url;
                            $message .= " .";
                        }


                        if ($this_is_latvia&&!$from_en_host) {
                            if ($address)
                            {
                                $str_address = " Adrese: ".$address." ";

                                $show = false;
                                $rsData = CBXShortUri::GetList(Array(), Array());
                                while($arRes = $rsData->Fetch()) {
                                    if ($arRes["URI"] == "/content/catalog/map.php?ID=".$ar_rest["ID"]) {
                                        $str_SHORT_URI = $arRes["SHORT_URI"];
                                        $show = true;
                                    }
                                }
                                if ($show):
                                    $map_url = "http://restoran.ru/".$str_SHORT_URI;
                                else:
                                    $str_SHORT_URI = CBXShortUri::GenerateShortUri();
                                    $arFields = Array(
                                        "URI" => "/content/catalog/map.php?ID=".$ar_rest["ID"],
                                        "SHORT_URI" => $str_SHORT_URI,
                                        "STATUS" => "301",
                                    );
                                    $ID = CBXShortUri::Add($arFields);
                                    $map_url = "http://restoran.ru/".$str_SHORT_URI;
                                endif;
                                $str_address .= $map_url;
                                $str_address .= " .";
                            }
                            $message = "Restoran.ru ir rezervejis Jums galdinu restorana ".$ar_rest["NAME"]." uz Jusu vardu uz ".substr($orFlds["ACTIVE_FROM"], 0, 16)." ".$orPrps["guest"]["VALUE"]." personam. ".$str_address."/Restoran.ru зарезервировал столик в ресторане ".$ar_rest["NAME"]." на Ваше имя на ".substr($orFlds["ACTIVE_FROM"], 0, 16).", на ".$orPrps["guest"]["VALUE"]." ".pluralForm($orPrps["guest"]["VALUE"],"человека","человека","человек").".";
                        }


                        $sixty_text = '';
                        if($ar_rest["ID"] == 1905056){
                            $sixty_text = ' Важно! В ресторане действует вечерний  Dress Code. Запрещено посещение в спортивной и пляжной одежде и обуви. В случае несоблюдения Dress Code ресторан вправе отказать вам в посещении. ';
                        }
                        if($ar_rest["ID"] == 2180168){
                            $sixty_text = ' Важно! Бронь столика сохраняется в течении 15 мин. ';
                        }
                        if($ar_rest["ID"]==2169012||$ar_rest["ID"]==1556934||$ar_rest["ID"]==2182898){
                            $sixty_text = ' Важно! К оплате принимают карты American Express, UnionPay или наличный расчет. ';
                        }

                        if(!$this_is_latvia&&!$from_en_host){

                            if(!$from_en_host){
                                $message .= $sixty_text."Если вы опаздываете или передумали, позвоните: ";
                            }
                            else {
                                $message .= "If you happen to be late or change your mind, please call us:";
                            }

                            if ($ar_rest["IBLOCK_ID"] == 12) $message .= "8-812-740-18-20";
                            if ($ar_rest["IBLOCK_ID"] == 11) $message .= "8-495-988-26-56";

//                            if ($ar_rest["IBLOCK_ID"] == 3566) $message .= "+371 661 031 06";
//                            if ($ar_rest["IBLOCK_ID"] == 3567) $message .= "+371 661 031 06";
                        }

                        $message .= " www.restoran.ru";

                        $megafon = check_megafon($PROP["TELEPHONE"]);
                        // TODO: убрать отчистку, уже чистые данные
                        $send_phone = preg_replace('/\D/','',$PROP["TELEPHONE"]);
                        if(preg_match('/^371/',$send_phone)){
                            send_sms_rga_urm($send_phone,$message);
                        }
                        elseif(preg_match('/^7/',$send_phone)){
                            CSMS::Send($message, $send_phone, "UTF-8");
                        }
                        else {
                            if ($megafon)
                                CSMS::Send($message, "7".$send_phone, "UTF-8",$megafon);
                            else
                                CSMS::Send($message, "7".$send_phone, "UTF-8");
                        }

                        /** Напомание о брони **/
                        if(($ar_rest["IBLOCK_ID"] == 12) && date('d.m.Y',strtotime($orFlds["ACTIVE_FROM"])) != date('d.m.Y')) {// || $ar_rest["IBLOCK_ID"] == 11
                            if ($ar_rest["IBLOCK_ID"] == 12) $site_phone = "8-812-740-18-20";
                            if ($ar_rest["IBLOCK_ID"] == 11) $site_phone = "8-495-988-26-56";

                            $new_message = 'Здравствуйте. Restoran.Ru напоминает, что у Вас сегодня забронирован столик в ресторан '.$ar_rest["NAME"].' на '.substr($orFlds["ACTIVE_FROM"], 0, 16).'. Хорошего Вам отдыха! Изменились планы? Звоните '.$site_phone.' .';
                            if (preg_match('/^7/', $send_phone)) {
                                $strTime = TimeEncodeZeroZone(strtotime($orFlds["ACTIVE_FROM"]) - 60 * 60 * 3);
                                CSMS::Send($new_message, $send_phone, "UTF-8", false, $strTime);
                            } else {
//                            $strTime = null;
                                $strTime = TimeEncodeZeroZone(strtotime($orFlds["ACTIVE_FROM"]) - 60 * 60 * 3);

                                $megafon = check_megafon($PROP["TELEPHONE"]);
                                if ($megafon)
                                    CSMS::Send($new_message, "7" . $send_phone, "UTF-8", $megafon, $strTime);
                                else
                                    CSMS::Send($new_message, "7" . $send_phone, "UTF-8", false, $strTime);
                            }
                        }




                    }
                }
                if ($_REQUEST["status"]==1809 && $orPrps["status"]["VALUE"]!="нет мест")
                {
                    CModule::IncludeModule("sozdavatel.sms");
                    $rrest = CIBlockElement::GetByID($orPrps["rest"]["VALUE"]);
                    if ($ar_rest = $rrest->Fetch())
                    {
                        if($ar_rest['IBLOCK_ID']==3624||$ar_rest['IBLOCK_ID']==3625||$ar_rest['IBLOCK_ID']==2586||$ar_rest['IBLOCK_ID']==2587){
                            $from_en_host = true;
                        }
                        if ($ar_rest["IBLOCK_ID"] == 3566 || $ar_rest["IBLOCK_ID"] == 3567){
                            $this_is_latvia = true;
                        }

                        if($this_is_latvia&&!$from_en_host){
                            $message = "Diemzel restorana nav vietu. Mes atradisim Jums citu restoranu. Zvaniet +371 661-031-06 Rezervesana Restoran.ru/ К сожалению в ресторане мест нет. Мы найдем Вам другой ресторан. Звоните +371 661-031-06.";
                        }
                        else {
                            if(!$from_en_host){
                                $message = "Сожалеем, в ресторане нет мест. Мы найдем другой ресторан для вас. Звоните ";
                            }
                            else {
                                $message = "We are sorry to inform you that there are no free seats at the restaurant left. We can find the other restaurant for you. Feel free to call to the Restoran.ru booking service ";
                            }

                            if ($ar_rest["IBLOCK_ID"] == 12) $message .= "8-812-740-18-20";
                            if ($ar_rest["IBLOCK_ID"] == 11) $message .= "8-495-988-26-56";

                            if(!$from_en_host) {
                                $message .= " , служба брони Ресторан.ру";
                            }
                        }

                        $message .= " www.restoran.ru";

                        $megafon = check_megafon($PROP["TELEPHONE"]);
                        // TODO: убрать отчистку, уже чистые данные
                        $send_phone = preg_replace('/\D/','',$PROP["TELEPHONE"]);
                        if(preg_match('/^371/',$send_phone)){
                            send_sms_rga_urm($send_phone,$message);
                        }
                        elseif(preg_match('/^7/',$send_phone)){
                            CSMS::Send($message, $send_phone, "UTF-8");
                        }
                        else {
                            if ($megafon)
                                CSMS::Send($message, "7" . $send_phone, "UTF-8", $megafon);
                            else
                                CSMS::Send($message, "7" . $send_phone, "UTF-8");
                        }
                    }
                }
                if ($_REQUEST["status"]==1499 && $orPrps["status"]["VALUE"]!="гости пришли")
                {

                    CModule::IncludeModule("sozdavatel.sms");
                    $rrest = CIBlockElement::GetByID($orPrps["rest"]["VALUE"]);
                    if ($ar_rest = $rrest->Fetch()) {

                        if ($ar_rest["IBLOCK_ID"] != 3566 && $ar_rest["IBLOCK_ID"] != 3567) {// без латвии

                            if($ar_rest['IBLOCK_ID']==2586||$ar_rest['IBLOCK_ID']==2587||$ar_rest['IBLOCK_ID']==3624||$ar_rest['IBLOCK_ID']==3625){
                                $from_en_host = true;
                            }


                            if(!$from_en_host){
                                if ($ar_rest["IBLOCK_ID"] == 12) $message = "Благодарим вас за посещение ресторана " . $ar_rest["NAME"] . " " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Вам понравилось? Оставьте отзыв на сайте http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=spb С уважением, Restoran.ru ";// С нас - бонус!
                                if ($ar_rest["IBLOCK_ID"] == 11) $message = "Благодарим вас за посещение ресторана " . $ar_rest["NAME"] . " " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Вам понравилось? Оставьте отзыв на сайте http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=msk С уважением, Restoran.ru "; // С нас - бонус!
                                if ($ar_rest["IBLOCK_ID"] == 3418) $message = "Благодарим вас за посещение ресторана " . $ar_rest["NAME"] . " " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Вам понравилось? Оставьте отзыв на сайте http://m.restoran.ru/add_review.php?RESTOURANT=" . $ar_rest["CODE"] . "&CITY_ID=nsk С уважением, Restoran.ru ";//С нас - бонус!
                            }
                            else {
                                if ($ar_rest["IBLOCK_ID"] == 2587) $message = "Thank you for visiting the restaurant " . $ar_rest["NAME"] . " on " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Did you like it? Please leave your comments on the website http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=spb";
                                if ($ar_rest["IBLOCK_ID"] == 2586) $message = "Thank you for visiting the restaurant " . $ar_rest["NAME"] . " on " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Did you like it? Please leave your comments on the website http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=msk";
//                                if ($ar_rest["IBLOCK_ID"] == 3418) $message = "Thank you for visiting the restaurant " . $ar_rest["NAME"] . " on " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Did you like it? Please leave your comments on the website http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=nsk";
                                if ($ar_rest['IBLOCK_ID']==3624) $message = "Thank you for visiting the restaurant " . $ar_rest["NAME"] . " on " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Did you like it? Please leave your comments on the website http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=rga";
                                if ($ar_rest['IBLOCK_ID']==3625) $message = "Thank you for visiting the restaurant " . $ar_rest["NAME"] . " on " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Did you like it? Please leave your comments on the website http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=urm";
                            }


//                            if ($ar_rest["IBLOCK_ID"] == 3566) $message = "Благодарим вас за посещение ресторана " . $ar_rest["NAME"] . " " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Вам понравилось? Оставьте отзыв на сайте http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=rga . С уважением, Restoran.ru ";
//                            if ($ar_rest["IBLOCK_ID"] == 3567) $message = "Благодарим вас за посещение ресторана " . $ar_rest["NAME"] . " " . substr($orFlds["ACTIVE_FROM"], 0, 16) . ". Вам понравилось? Оставьте отзыв на сайте http://m.restoran.ru/reviews/?id=" . $ar_rest["ID"] . "&CITY_ID=urm . С уважением, Restoran.ru ";


                            //$message = "Сожалеем, ресторан отказал в брони. Мы найдем другой ресторан для вас. Звоните ";
                            //$message = "Благодарим вас за посещение ресторана ".$ar_rest["NAME"]." ".substr($orFlds["ACTIVE_FROM"], 0, 16).". Вам понравилось? Оставьте отзыв на сайте http://m.restoran.ru/opinions.php?RESTOURANT=".$ar_rest["CODE"]." С нас - бонус! С уважением, Restoran.ru ";

                            $message .= " www.restoran.ru";

                            // TODO: убрать отчистку, уже чистые данные
                            $send_phone = preg_replace('/\D/', '', $PROP["TELEPHONE"]);
                            if (preg_match('/^371/', $send_phone)) {
                                send_sms_rga_urm($send_phone, $message);
                            } elseif (preg_match('/^7/', $send_phone)) {
                                $strTime = TimeEncodeZeroZone(time() + 60 * 60 * 2);
                                CSMS::Send($message, $send_phone, "UTF-8", false, $strTime);
                            } else {
//                            $strTime = null;
                                $strTime = TimeEncodeZeroZone(time() + 60 * 60 * 2);

                                $megafon = check_megafon($PROP["TELEPHONE"]);
                                if ($megafon)
                                    CSMS::Send($message, "7" . $send_phone, "UTF-8", $megafon, $strTime);
                                else
                                    CSMS::Send($message, "7" . $send_phone, "UTF-8", false, $strTime);
                            }
                        }
                    }
                }
            }

            //Переводим в отчет
            //Начисляем 100 очков посетителю при
            if($_REQUEST["status"]==1808 && $orPrps["status"]["VALUE"]!="отчет"){
                CModule::IncludeModule("sale");

                //за банкет 500
//                if($_REQUEST["type"]==1498) Restics::UpdateUserAccount($_REQUEST["user"],100,"Бронирование столика +100");
//                else Restics::UpdateUserAccount($_REQUEST["user"],500,"Бронирование банкета +500");
//                //CSaleUserAccount::UpdateAccount($_REQUEST["user"],100,"RUB","Бронирование столика +100");
//
//
//                $arIB = getArIblock2("invites");
//                $arFilter_in = Array("ACTIVE"=>"Y","IBLOCK_TYPE"=>"invites", "PROPERTY_BRON"=>$_REQUEST["order_id"], "IBLOCK_ID"=>$arIB["ID"]);
//                //var_dump($arFilter_in);
//                $res_in = CIBlockElement::GetList(Array(), $arFilter_in);
//                if($ob_in = $res_in->GetNext()){
//                    //CSaleUserAccount::UpdateAccount($_REQUEST["user"],50,"RUB","Бронирование столика через приглашение +50");
//                    Restics::UpdateUserAccount($_REQUEST["user"],50,"Бронирование столика через приглашение +50");
//                }

                //Записываем ресторану долг
                dolg2rest($_REQUEST["procent"], $_REQUEST["rastoran_id"]);
            }

//            if(!$_REQUEST["date"]||!$_REQUEST["time"]){
//                $EXIT["message"]='error';
//                $EXIT["message_text"]='Не установленна дата/время заказа';
//            }
//            else {
                $DT="";
                if($_REQUEST["date"]!=""){
                    $DT=$_REQUEST["date"];
                }

                if($_REQUEST["time"]!="" && $DT!=""){
                    $tr = explode(":", $_REQUEST["time"]);
                    if($tr[0]<24 && $tr[1]<61){
                        $DT.=" ".$_REQUEST["time"].":00";
                    }
                }

                $arLoadProductArray2 = Array(
                    "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
                    "PROPERTY_VALUES"=> $PROP2,
                    "NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"]." ".$_REQUEST["rastoran_id"],
                    "ACTIVE"         => "Y",            // активен
                    "ACTIVE_FROM"=>$DT,
                    "PREVIEW_TEXT"   => htmlspecialchars_decode($_REQUEST["prim_order"], ENT_NOQUOTES),
                );

                //var_dump($orFlds);
                if($orFlds["CREATED_BY"]=="" || $orFlds["CREATED_BY"]==0){
                    $arLoadProductArray2["CREATED_BY"] = $USER->GetID();
                }

                if($_REQUEST["DATE_CREATE"]!=""){
                    $arLoadProductArray2["DATE_CREATE"]=$_REQUEST["DATE_CREATE"];
                }

                //htmlspecialchars_decode(string string [, int quote_style])

                //var_dump($arLoadProductArray2);
                if($_REQUEST["client_id"]>0 || $_REQUEST["user"]>0) {
                    if($res = $el->Update($_REQUEST["order_id"], $arLoadProductArray2)) $EXIT["message"]="order_saved";
                    else {
                        $EXIT["message"]='error';
                        $EXIT["message_text"]=$el->LAST_ERROR;
                    }
                }


                $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_105');
//            }
        }
    }else{
        //создаем новую бронь
        //if($_REQUEST["rastoran_id"]>0 || $_REQUEST["rastoran"]!="")
        //$_REQUEST["type"]!="" &&

        if($_REQUEST["client_id"]!="")
        {
            $EXIT2 = add_new_order($_REQUEST["client_id"]);
            if ($_REQUEST["telephone"])
            {
                if ($_REQUEST["status"]==1467&&$_REQUEST["rastoran_id"]&&$_REQUEST["type"]==1498)
                {
                    CModule::IncludeModule("sozdavatel.sms");
                    $rrest = CIBlockElement::GetByID($_REQUEST["rastoran_id"]);
                    if ($ar_rest = $rrest->Fetch())
                    {
                        if($ar_rest['IBLOCK_ID']==3624||$ar_rest['IBLOCK_ID']==3625||$ar_rest['IBLOCK_ID']==2586||$ar_rest['IBLOCK_ID']==2587){
                            $from_en_host = true;
                        }
                        if ($ar_rest["IBLOCK_ID"] == 3566 || $ar_rest["IBLOCK_ID"] == 3567) {
                            $this_is_latvia = true;
                        }

                        $db_props = CIBlockElement::GetProperty($ar_rest["IBLOCK_ID"], $ar_rest["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
                        while($ar_props = $db_props->Fetch())
                        {
                            $address[] = $ar_props["VALUE"];
                        }
                        if (count($address)>1)
                            $address = "";
                        else
                            $address = $address[0];



                        if(!$this_is_latvia && !$from_en_host) {
                            $message = "Ресторан.ру забронировал для Вас столик в ресторане ".$ar_rest["NAME"]." на ".substr($_REQUEST["date"]." ".$_REQUEST["time"], 0, 16).", на ".$_REQUEST["guest"]." ".pluralForm($_REQUEST["guest"],"человека","человека","человек").".";
                        }
                        elseif($from_en_host){
                            $message = "Restoran.ru has booked a table for you at the ".$ar_rest["NAME"]." Restaurant for ".substr($_REQUEST["date"]." ".$_REQUEST["time"], 0, 16)." for ".$_REQUEST["guest"]." people.";
                        }


                        if ($address && !$this_is_latvia && !$from_en_host)
                        {
                            $message .= " Адрес: ".$address." ";


                            $show = false;
                            $rsData = CBXShortUri::GetList(Array(), Array());
                            while($arRes = $rsData->Fetch()) {
                                if ($arRes["URI"] == "/content/catalog/map.php?ID=".$ar_rest["ID"]) {
                                    $str_SHORT_URI = $arRes["SHORT_URI"];
                                    $show = true;
                                }
                            }
                            if ($show):
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            else:
                                $str_SHORT_URI = CBXShortUri::GenerateShortUri();
                                $arFields = Array(
                                    "URI" => "/content/catalog/map.php?ID=".$ar_rest["ID"],
                                    "SHORT_URI" => $str_SHORT_URI,
                                    "STATUS" => "301",
                                );
                                $ID = CBXShortUri::Add($arFields);
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            endif;
                            $message .= $map_url;
                            $message .= " .";
                        }

                        if($address&&$from_en_host){
                            $message .= " The address is: ".$address." ";


                            $show = false;
                            $rsData = CBXShortUri::GetList(Array(), Array());
                            while($arRes = $rsData->Fetch()) {
                                if ($arRes["URI"] == "/content/catalog/map.php?ID=".$ar_rest["ID"]) {
                                    $str_SHORT_URI = $arRes["SHORT_URI"];
                                    $show = true;
                                }
                            }
                            if ($show):
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            else:
                                $str_SHORT_URI = CBXShortUri::GenerateShortUri();
                                $arFields = Array(
                                    "URI" => "/content/catalog/map.php?ID=".$ar_rest["ID"],
                                    "SHORT_URI" => $str_SHORT_URI,
                                    "STATUS" => "301",
                                );
                                $ID = CBXShortUri::Add($arFields);
                                $map_url = "http://restoran.ru/".$str_SHORT_URI;
                            endif;
                            $message .= $map_url;
                            $message .= " .";
                        }


                        if ($this_is_latvia&&!$from_en_host){
                            if ($address)
                            {
                                $str_address = " Adrese: ".$address." ";

                                $show = false;
                                $rsData = CBXShortUri::GetList(Array(), Array());
                                while($arRes = $rsData->Fetch()) {
                                    if ($arRes["URI"] == "/content/catalog/map.php?ID=".$ar_rest["ID"]) {
                                        $str_SHORT_URI = $arRes["SHORT_URI"];
                                        $show = true;
                                    }
                                }
                                if ($show):
                                    $map_url = "http://restoran.ru/".$str_SHORT_URI;
                                else:
                                    $str_SHORT_URI = CBXShortUri::GenerateShortUri();
                                    $arFields = Array(
                                        "URI" => "/content/catalog/map.php?ID=".$ar_rest["ID"],
                                        "SHORT_URI" => $str_SHORT_URI,
                                        "STATUS" => "301",
                                    );
                                    $ID = CBXShortUri::Add($arFields);
                                    $map_url = "http://restoran.ru/".$str_SHORT_URI;
                                endif;
                                $str_address .= $map_url;
                                $str_address .= " .";
                            }
                            $message = "Restoran.ru ir rezervejis Jums galdinu restorana ".$ar_rest["NAME"]." uz Jusu vardu uz ".substr($_REQUEST["date"]." ".$_REQUEST["time"], 0, 16)." ".$_REQUEST["guest"]." personam. ".$str_address."/Restoran.ru зарезервировал столик в ресторане ".$ar_rest["NAME"]." на Ваше имя на ".substr($_REQUEST["date"]." ".$_REQUEST["time"], 0, 16).", на ".$_REQUEST["guest"]." ".pluralForm($_REQUEST["guest"],"человека","человека","человек").".";
                        }

                        $sixty_text = '';
                        if($ar_rest["ID"] == 1905056){
                            $sixty_text = ' Важно! В ресторане действует вечерний  Dress Code. Запрещено посещение в спортивной и пляжной одежде и обуви. В случае несоблюдения Dress Code ресторан вправе отказать вам в посещении. ';
                        }



                        if(!$this_is_latvia&&!$from_en_host){

                            if(!$from_en_host){
                                $message .= $sixty_text."Если вы опаздываете или передумали, позвоните: ";
                            }
                            else {
                                $message .= "If you happen to be late or change your mind, please call us:";
                            }

                            if ($ar_rest["IBLOCK_ID"] == 12) $message .= "8-812-740-18-20";
                            if ($ar_rest["IBLOCK_ID"] == 11) $message .= "8-495-988-26-56";

//                            if ($ar_rest["IBLOCK_ID"] == 3566) $message .= "+371 661 031 06";
//                            if ($ar_rest["IBLOCK_ID"] == 3567) $message .= "+371 661 031 06";
                        }

                        $message .= " www.restoran.ru";

                        if($ar_rest["ID"] == 385676) {
                            $message = "Ресторан.ру забронировал для Вас столик в ресторане Бричмула на ".substr($_REQUEST["date"]." ".$_REQUEST["time"], 0, 16).", на ".$_REQUEST["guest"]." ".pluralForm($_REQUEST["guest"],"человека","человека","человек").". Адрес: Комендантский пр., д. 13 ".$map_url.". Важно! Если Вы хотите забронировать конкретный стол (например, у окна, с диванами, на террасе и т.д.), необходимо заранее подъехать в ресторан для оформления предзаказа. Если Вы опаздываете или изменились планы, позвоните 8-812-740-18-20 www.restoran.ru";
                        }

                        // TODO: убрать отчистку, уже чистые данные
                        $send_phone = preg_replace('/\D/','',$_REQUEST["telephone"]);
                        if(preg_match('/^371/',$send_phone)){
                            send_sms_rga_urm($send_phone,$message);
                        }
                        elseif(preg_match('/^7/',$send_phone)){
                            CSMS::Send($message, $send_phone, "UTF-8");
                        }
                        else {
                            if ($megafon)
                                CSMS::Send($message, "7" . $send_phone, "UTF-8", $megafon);
                            else
                                CSMS::Send($message, "7" . $send_phone, "UTF-8");
                        }
                    }
                }
            }
        }
        $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_105');

    }

    //var_dump($EXIT2);
    echo json_encode(array_merge($EXIT,$EXIT2));

}


function dolg2rest($SUMMA, $RESTID){
    global $APPLICATION;
    global $USER;


    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    //смотрим сначала, есть ли запись для этого ресторана
    $arFilter = Array("IBLOCK_ID"=>2541, "SECTION_CODE"=>$_SESSION["CITY"], "PROPERTY_rest"=>$RESTID);
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        $DOLG = $arProps["dolg"]["VALUE"] + $SUMMA;

        CIBlockElement::SetPropertyValuesEx($arFields["ID"], 2541, array("dolg" => $DOLG));
    }else{
        $el = new CIBlockElement;

        //возьмем название ресторана
        $res = CIBlockElement::GetByID($RESTID);
        if($ar_res = $res->GetNext()){
            $RESTNAME = $ar_res['NAME'];
        }

        if($_SESSION["CITY"]=="spb") $SECID=185097;
        if($_SESSION["CITY"]=="msk") $SECID=185096;
        if($_SESSION["CITY"]=="jurmala") $SECID=274886;

        if($_SESSION["CITY"]=="nsk") $SECID=283286;

        $PROP = array();
        $PROP["dolg"] = $SUMMA;
        $PROP["rest"] = $RESTID;

        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => $SECID,          // элемент лежит в корне раздела
            "IBLOCK_ID"      => 2541,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $RESTNAME,
            "ACTIVE"         => "Y",            // активен
        );

        $el->Add($arLoadProductArray);

    }


}


/**
 * @param $CLIENT_ID
 * @return array
 */
function add_new_order($CLIENT_ID){
    global $APPLICATION;
    global $USER;

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if($_REQUEST["status"]=="") $_REQUEST["status"]=1418;

    $EXIT=array();

    $el = new CIBlockElement;

    $PROP2 = array();
    $PROP2["rest"] = $_REQUEST["rastoran_id"];
    $PROP2["guest"] = $_REQUEST["guest"];
    $PROP2["status"] = $_REQUEST["status"];
    $PROP2["client"] = $CLIENT_ID;
    $PROP2["prinyal"] = $_REQUEST["prinyal"];
    $PROP2["source"] = $_REQUEST["source"];
    $PROP2["podtv"] = $_REQUEST["podtv"];
    $PROP2["summ_sch"] = $_REQUEST["summ_sch"];
    $PROP2["podtv"] = $_REQUEST["podtv"];
    $PROP2["summ_sch"] = $_REQUEST["summ_sch"];
    $PROP2["stol"] = $_REQUEST["stol"];
    $PROP2["podtv2"] = $_REQUEST["podtv2"];
    $PROP2["user"] = $_REQUEST["user"];
    $PROP2["type"] = $_REQUEST["type"];
    $PROP2["procent"] = $_REQUEST["procent"];
    $PROP2["control_call"] = $_REQUEST["control_call"];

    if($_SESSION["CITY"]=="spb") $SECTION=83660;
    if($_SESSION["CITY"]=="msk") $SECTION=83659;
    if($_SESSION["CITY"]=="jurmala") $SECTION=274887;

    if($_SESSION["CITY"]=="nsk") $SECTION=283287;

    //Переводим в отчет
    //Начисляем 100 очков посетителю при
    if($_REQUEST["status"]==1808){
        CModule::IncludeModule("sale");

//        //за банкет 500
//        if($_REQUEST["type"]==1498) Restics::UpdateUserAccount($_REQUEST["user"],100,"Бронирование столика +100");
//        else Restics::UpdateUserAccount($_REQUEST["user"],500,"Бронирование банкета +500");
//
//        /*
//        if($_REQUEST["type"]==1498) CSaleUserAccount::UpdateAccount($_REQUEST["user"],100,"RUB","Бронирование столика +100");
//        else CSaleUserAccount::UpdateAccount($_REQUEST["user"],500,"RUB","Бронирование банкета +500");
//        */
//        $arIB = getArIblock2("invites");
//        $arFilter_in = Array("ACTIVE"=>"Y","IBLOCK_TYPE"=>"invites", "PROPERTY_BRON"=>$_REQUEST["order_id"], "IBLOCK_ID"=>$arIB["ID"]);
//
//        //var_dump($arFilter_in);
//        $res_in = CIBlockElement::GetList(Array(), $arFilter_in);
//        if($ob_in = $res_in->GetNext()){
//            Restics::UpdateUserAccount($_REQUEST["user"],50,"Бронирование столика через приглашение +50");
//            //CSaleUserAccount::UpdateAccount($_REQUEST["user"],50,"RUB","Бронирование столика через приглашение +50");
//        }

        //Записываем ресторану долг
        dolg2rest($_REQUEST["procent"], $_REQUEST["rastoran_id"]);
    }



    $DT="";
    if($_REQUEST["date"]!=""){
        $DT=$_REQUEST["date"];
    }

    if($_REQUEST["time"]!="" && $DT!=""){
        $tr = explode(":", $_REQUEST["time"]);
        if($tr[0]<24 && $tr[1]<61){
            $DT.=" ".$_REQUEST["time"].":00";
        }
    }

    $arLoadProductArray2 = Array(
        "IBLOCK_SECTION_ID" => $SECTION,
        "IBLOCK_ID" => 105,
        "PROPERTY_VALUES"=> $PROP2,
        "NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"]." ".$_REQUEST["rastoran_id"],
        "ACTIVE"         => "Y",
        "ACTIVE_FROM"=>$DT,
        "PREVIEW_TEXT"   => $_REQUEST["prim_order"],
    );

    if($_REQUEST["DATE_CREATE"]!=""){
        $arLoadProductArray2["DATE_CREATE"]=$_REQUEST["DATE_CREATE"];
    }

    if($ORID = $el->Add($arLoadProductArray2)) $EXIT["ORDER_ID"]=$ORID;
    else echo "Error: ".$el->LAST_ERROR;

    return $EXIT;
}

function search_client_by_surname(){
    global $APPLICATION;
    global $USER;
    global $arrFilter;
    if($_REQUEST["surname"]!="") $arrFilter["PROPERTY_surname"]="%".$_REQUEST["surname"]."%";

    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/clients.php"),
        Array("arrFilter"=>$arrFilter),
        Array("MODE"=>"php")
    );
}


function search_client_by_name(){
    global $APPLICATION;
    global $USER;
    global $arrFilter;
    if($_REQUEST["name"]!="") $arrFilter["PROPERTY_name"]="%".$_REQUEST["name"]."%";

    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/clients.php"),
        Array("arrFilter"=>$arrFilter),
        Array("MODE"=>"php")
    );
}


function search_client_by_telephone(){
    global $APPLICATION;
    global $USER;
    global $arrFilter;

    $_REQUEST["telephone"] = str_replace("_", "", $_REQUEST["telephone"]);

    if($_REQUEST["telephone"]!=""){
        $first =substr($_REQUEST["telephone"],0,3);
        if($first==812){

            $arrFilter["PROPERTY_telephone"]=array("%".$_REQUEST["telephone"]."%", "%".substr($_REQUEST["telephone"],3));
        }else{
            $arrFilter["PROPERTY_telephone"]="%".$_REQUEST["telephone"]."%";
        }

    }
    //var_dump($arrFilter["PROPERTY_telephone"]);
    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/clients.php"),
        Array("arrFilter"=>$arrFilter),
        Array("MODE"=>"php")
    );
}

function search_client_by_email(){
    global $APPLICATION;
    global $USER;
    global $arrFilter;
    if($_REQUEST["email"]!="") $arrFilter["PROPERTY_email"]="%".$_REQUEST["email"]."%";

    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/clients.php"),
        Array("arrFilter"=>$arrFilter),
        Array("MODE"=>"php")
    );
}


function add_new_client(){
    global $APPLICATION;
    global $USER;

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if($_SESSION["CITY"]=="spb") $SECTION=83823;
    if($_SESSION["CITY"]=="msk")  $SECTION=83822;
    if($_SESSION["CITY"]=="jurmala")  $SECTION=274888;

    if($_SESSION["CITY"]=="nsk")  $SECTION=283288;

    $EXIT=array();
    //сначала посмотрим, есть ли в базе клиент с таким телефон или emailом или фамилией
    //если клиент есть, то покажем алерт
    $_REQUEST["telephone"] = str_replace("_", "", $_REQUEST["telephone"]);
    $arrFilter=array(
        array(
            "LOGIC"=>"OR",
            "PROPERTY_EMAIL"=>$_REQUEST["email"],
            "PROPERTY_TELEPHONE"=>$_REQUEST["telephone"],
            "PROPERTY_SURNAME"=>$_REQUEST["surname"]
        ),
        "IBLOCK_SECTION"=>$SECTION,
        "IBLOCK_ID"=>104
    );

    if($_REQUEST["email"]=="") unset($arrFilter[0]["PROPERTY_EMAIL"]);

    //var_dump($arrFilter);
    $res = CIBlockElement::GetList(Array(), $arrFilter, false, false, false);
    if($ob = $res->GetNextElement()){
        $arProps = $ob->GetProperties();

        //такой клиент есть
        if($_REQUEST["no_alert"]=="Y"){
            //все равно добавляем
            $el = new CIBlockElement;

            $PROP = array();

            if($_REQUEST["name"]=="" && $_REQUEST["surname"]=="") $_REQUEST["surname"]="Аноним";

            $PROP["NAME"] = $_REQUEST["name"];
            $PROP["SURNAME"] = $_REQUEST["surname"];
            $PROP["TELEPHONE"] = $_REQUEST["telephone"];
            $PROP["EMAIL"] = $_REQUEST["email"];

            $arLoadProductArray = Array(
                "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
                "IBLOCK_ID"=>104,
                "IBLOCK_SECTION" => $SECTION,          // элемент лежит в корне раздела
                "PROPERTY_VALUES"=> $PROP,
                "NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"],
                "ACTIVE"         => "Y",            // активен
                "PREVIEW_TEXT"   => $_REQUEST["prim"],
            );

            if($CLIENT_ID = $el->Add($arLoadProductArray)){
                $EXIT["message"]="client_edded";
                $EXIT["client"]=$CLIENT_ID;

                $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
            }else
                $EXIT["message"]=$el->LAST_ERROR;


        }else{
            $EXIT["message"]="client_find";

            if($arProps["SURNAME"]["VALUE"]==$_REQUEST["surname"] && $_REQUEST["surname"]!="")
                $EXIT["message2"]='<span class="quest_icon"></span>Клиент с такой фамилией уже есть. Продолжить?';

            if($arProps["TELEPHONE"]["VALUE"]==$_REQUEST["telephone"] && $_REQUEST["telephone"]!="")
                $EXIT["message2"]='<span class="quest_icon"></span>Клиент с таким телефоном уже есть. Продолжить?';

            if($arProps["EMAIL"]["VALUE"]==$_REQUEST["email"] && $_REQUEST["email"]!="")
                $EXIT["message2"]='<span class="quest_icon"></span>Клиент с таким E-mailом уже есть. Продолжить?';

            if($EXIT["message2"]=="") $EXIT["message2"]='<span class="quest_icon"></span>Вы не заполнили некоторые поля. Продолжить?';
        }
    }else{
        //такого клиента нет, смело добавляем

        $el = new CIBlockElement;

        $PROP = array();
        $PROP["NAME"] = $_REQUEST["name"];
        $PROP["SURNAME"] = $_REQUEST["surname"];
        $PROP["TELEPHONE"] = $_REQUEST["telephone"];
        $PROP["EMAIL"] = $_REQUEST["email"];

        if($_SESSION["CITY"]=="spb") $SECTION=83823;
        if($_SESSION["CITY"]=="msk") $SECTION=83822;
        if($_SESSION["CITY"]=="jurmala") $SECTION=274888;
        if($_SESSION["CITY"]=="nsk") $SECTION=283288;

        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_ID"=>104,       // элемент лежит в корне раздела
            "PROPERTY_VALUES"=> $PROP,
            "IBLOCK_SECTION" => $SECTION,
            "NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"],
            "ACTIVE"         => "Y",            // активен
            "PREVIEW_TEXT"   => $_REQUEST["prim"],
        );

        if($CLIENT_ID = $el->Add($arLoadProductArray)){
            $EXIT["message"]="client_edded";
            $EXIT["client"]=$CLIENT_ID;

            $GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
        }else
            $EXIT["message"]=$el->LAST_ERROR;
    }

    return $EXIT;

}


function search_restoran(){
    global $APPLICATION;
    global $USER;
    if($_REQUEST["name"]=="") die("");

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if($_SESSION["CITY"]=="") $_SESSION["CITY"]="msk";
    if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
    if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
    if($_SESSION["CITY"]=="jurmala") $IBLOCK_ID=array(3567,3566); //юрмала и рига

    if($_SESSION["CITY"]=="nsk") $IBLOCK_ID=3418;

    //var_dump(urldecode($_REQUEST["name"]));

    //urldecode(string str)

    $TRANS = translitIt(urldecode($_REQUEST["name"]));

    $arrFilter=array(
        array(
            "LOGIC"=>"OR",
            "TAGS"=>"%".urldecode($_REQUEST["name"])."%",
            "?NAME"=>"%".urldecode($_REQUEST["name"])."% || %".$TRANS."%"
        ),
        "IBLOCK_ID"=>$IBLOCK_ID,
//        'ACTIVE'=>'Y',//    CLEAR_ACTIVE
        '!ID'=>'1139761',
        '!PROPERTY_NOT_SHOW_IN_BS_VALUE'=>'Y'
    );


    //var_dump($arrFilter);

    //$arrFilter["ACTIVE"]="Y";
    //$arrFilter["SECTION_CODE"]="restaurants";

    $now_rest="";
    $res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arrFilter, false, false, false);
    while($ob= $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps_rest = $ob->GetProperties();

        //if(search_restoran

        $ADDR="";
        foreach($arProps_rest["address"]["VALUE"] as $T){
            $ADDR.=$T;
            if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
            $i++;
        }

        $ADDR = str_replace("Москва, ","", $ADDR);
        $ADDR = str_replace("Санкт-Петербург, ","", $ADDR);

        if($arFields["IBLOCK_SECTION_ID"]==103 || $arFields["IBLOCK_SECTION_ID"]==32 || $arFields["IBLOCK_SECTION_ID"]==247659 || $arFields["IBLOCK_SECTION_ID"]==247660) $T="[Р]";
        elseif($arFields["IBLOCK_SECTION_ID"]==104 || $arFields["IBLOCK_SECTION_ID"]==33 || $arFields["IBLOCK_SECTION_ID"]==247664 || $arFields["IBLOCK_SECTION_ID"]==247671) $T="[Б]";


        //var_dump($arFields);
        echo '<a href="#'.$arFields["ID"].'">'.$T.$arFields["NAME"].($arFields['ACTIVE']=='N'?" - НЕАКТИВЕН НА САЙТЕ":"");
        echo ' ('.$ADDR.')';
        echo '</a>';
        $now_rest=$arFields["NAME"];
    }
}

function get_restoran_info(){
    global $APPLICATION;
    global $USER;
    if($_REQUEST["restoran_id"]=="") die("");
    $_REQUEST["restoran_id"] = str_replace("#", "", $_REQUEST["restoran_id"]);
    $_REQUEST["restoran_id"] = str_replace("http://www.restoran.ru/bs/", "", $_REQUEST["restoran_id"]);

    //$_REQUEST["restoran_id"] =intval($_REQUEST["restoran_id"]);
    //var_dump($_REQUEST["restoran_id"]);

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $EXIT=array();
    $res = CIBlockElement::GetByID($_REQUEST["restoran_id"]);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps_rest = $ob->GetProperties();

        $EXIT["ID"]=$arFields["ID"];
        $EXIT["ADDRESS"]=$arProps_rest["address"]["VALUE"];

        foreach($arProps_rest["phone"]["VALUE"] as $T){
            $TEL.=$T;
            if($arProps_rest["phone"]["VALUE"][$i+1]!="") $TEL.=", ";
            $i++;
        }

        $i=0;
        foreach($arProps_rest["address"]["VALUE"] as $T){
            $ADDR.=$T;
            if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
            $i++;
        }

        $i=0;
        foreach($arProps_rest["opening_hours"]["VALUE"] as $T){
            $OPEN.=$T;
            if($arProps_rest["opening_hours"]["VALUE"][$i+1]!="") $OPEN.=", ";
            $i++;
        }

        $i=0;
        foreach($arProps_rest["subway"]["VALUE"] as $T){
            $res_metro = CIBlockElement::GetByID($T);
            if($ob_metro = $res_metro->GetNext()){
                $tar=explode("(", $ob_metro["NAME"]);
                $METRO.=trim($tar[0]);
            }
            if($arProps_rest["subway"]["VALUE"][$i+1]!="") $METRO.=", ";
            $i++;
        }

        if($arProps_rest["rabotaem"]["VALUE"]=="Y") $arProps_rest["rabotaem"]["VALUE"]="Работаем";
        else $arProps_rest["rabotaem"]["VALUE"]="НЕ Работаем";

        if($arProps_rest["procent"]["VALUE"]=="") $arProps_rest["procent"]["VALUE"]=10;

        $EXIT["RESTORAN_INFO"]='';
        $EXIT["RESTORAN_INFO"].='<div class="il"><b>Телефон:</b> '.$TEL.'</div>';
        if($arProps_rest["real_tel"]["VALUE"]!="") $EXIT["RESTORAN_INFO"].='<div class="il"><b>Реальный телефон:</b> '.$arProps_rest["real_tel"]["VALUE"].'</div>';
        $EXIT["RESTORAN_INFO"].='<div class="il"><b>Адрес:</b> '.$ADDR.' </div>';
        $EXIT["RESTORAN_INFO"].='<div class="il"><b>Метро:</b> '.$METRO.'</div>';
        $EXIT["RESTORAN_INFO"].='<div class="il spec_color"><b>Сотрудничество:</b> '.$arProps_rest["rabotaem"]["VALUE"].'</div>';
        $EXIT["RESTORAN_INFO"].='<div class="il"><b>Время работы:</b> '.$OPEN.'</div>';
        $EXIT["RESTORAN_INFO"].='<div class="il2"><b>Пометки:</b><br/> '.$arProps_rest["pometki"]["VALUE"].'</div>';
        $EXIT["RESTORAN_INFO"].='<div class="il"><b>Процент:</b> <span id="rest_procent">'.$arProps_rest["procent"]["VALUE"].'</span>%</div>';
    }

    echo json_encode($EXIT);

}

function get_rest_detailed_info(){
    global $APPLICATION;
    global $USER;
    if($_REQUEST["id"]=="") die("");
    $_REQUEST["restoran_id"] = str_replace("rest_", "", $_REQUEST["id"]);

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }


    $res = CIBlockElement::GetByID($_REQUEST["restoran_id"]);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arFields["PROPERTIES"] = $ob->GetProperties();

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/restoran_detailed_info.php"),
            Array("REST"=>$arFields),
            Array("MODE"=>"php")
        );

    }


}

function get_order_info(){
    global $APPLICATION;
    global $USER;
    $_REQUEST["id"] = str_replace("order_", "", $_REQUEST["id"]);

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $EXIT = array();
    $res = CIBlockElement::GetByID($_REQUEST["id"]);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();


        $EXIT["DATE_CREATE"] = $arFields["DATE_CREATE"];

        $EXIT["ID"] = $arFields["ID"];
        //$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];

        $EXIT["PRIM"] = addslashes(html_entity_decode($arFields["PREVIEW_TEXT"]));


        //addslashes()
        //$EXIT["PRIM"] =rawurlencode($EXIT["PRIM"]);

        //var_dump($EXIT["PRIM"]);

        //htmlspecialchars_decode(string string [, int quote_style])

        $tar=explode(" ", $arFields["ACTIVE_FROM"]);
        $tar2=explode(":", $tar[1]);
        $EXIT["DATE"] = $tar[0];
        $EXIT["TIME"] = $tar2[0].":".$tar2[1];

        if($EXIT["TIME"]==":") $EXIT["TIME"]="";

        if($arProps["status"]["VALUE"]=="") $arProps["status"]["VALUE"]="&nbsp;";
        if($arProps["source"]["VALUE"]=="") $arProps["source"]["VALUE"]="&nbsp;";



        $EXIT["GUEST"] = $arProps["guest"]["VALUE"];
        $EXIT["STATUS"] = $arProps["status"]["VALUE"];
        $EXIT["STATUS_ID"] = $arProps["status"]["VALUE_ENUM_ID"];


        if($arProps["status"]["VALUE"]=="заказ принят") $EXIT["STATUS_COLOR"] = "yello";
        if($arProps["status"]["VALUE"]=="забронирован") $EXIT["STATUS_COLOR"]  = "sv_zel";
        if($arProps["status"]["VALUE"]=="гости пришли") $EXIT["STATUS_COLOR"]  = "zel";
        if($arProps["status"]["VALUE"]=="гости отменили заказ") $EXIT["STATUS_COLOR"]  = "red";
        if($arProps["status"]["VALUE"]=="ошибочный заказ") $EXIT["STATUS_COLOR"]  = "black";
        if($arProps["status"]["VALUE"]=="банкет в работе") $EXIT["STATUS_COLOR"]  = "yello";
        if($arProps["status"]["VALUE"]=="внесена предоплата") $EXIT["STATUS_COLOR"]  = "yello";
        if($arProps["status"]["VALUE"]=="отчет") $EXIT["STATUS_COLOR"]  = "blue";
        if($arProps["status"]["VALUE"]=="заказ оплачен") $EXIT["STATUS_COLOR"]  = "blue";


        CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], 105, array("new" => false));
        //удаляем файл
        @unlink($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders/".$_REQUEST["id"]);



        $EXIT["CONTROL_CALL"] = $arProps["control_call"]["VALUE"];

        $EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];

        $EXIT["FORMAT"] = $arProps["format"]["VALUE"];

        $EXIT["TYPE"] = $arProps["type"]["VALUE"];
        $EXIT["TYPE_ID"] = $arProps["type"]["VALUE_ENUM_ID"];
        $EXIT["DENGI"] = $arProps["dengi"]["VALUE"];

        $EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
        $EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];
        $EXIT["SUMM_SCH"] = $arProps["summ_sch"]["VALUE"];
        $EXIT["PROCENT"] = $arProps["procent"]["VALUE"];

        $EXIT["STOL"] = $arProps["stol"]["VALUE"];
        $EXIT["PODTV2"] = $arProps["podtv2"]["VALUE"];
        $EXIT["PODTV"] = $arProps["podtv"]["VALUE"];


        $EXIT["SOURCE"] = $arProps["source"]["VALUE"];
        $EXIT["SOURCE_ID"] = $arProps["source"]["VALUE_ENUM_ID"];


        $rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        $arUser = $rsUser->Fetch();

        $EXIT["OPERATOR"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];

        //теперь нужно получить инфу по ресторану
        $res_rest = CIBlockElement::GetByID($arProps["rest"]["VALUE"]);
        if($ob_rest = $res_rest->GetNextElement()){
            $arFields_rest = $ob_rest->GetFields();
            $arProps_rest = $ob_rest->GetProperties();

            $EXIT["RESTORAN"]=$arFields_rest["NAME"];
            $EXIT["RESTORAN_ID"]=$arFields_rest["ID"];


            $i=0;
            foreach($arProps_rest["phone"]["VALUE"] as $T){
                $TEL.=$T;
                if($arProps_rest["phone"]["VALUE"][$i+1]!="") $TEL.=", ";
                $i++;
            }

            $i=0;
            foreach($arProps_rest["address"]["VALUE"] as $T){
                $ADDR.=$T;
                if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
                $i++;
            }

            $i=0;
            foreach($arProps_rest["opening_hours"]["VALUE"] as $T){
                $OPEN.=$T;
                if($arProps_rest["opening_hours"]["VALUE"][$i+1]!="") $OPEN.=", ";
                $i++;
            }

            $i=0;
            foreach($arProps_rest["subway"]["VALUE"] as $T){
                $res_metro = CIBlockElement::GetByID($T);
                if($ob_metro = $res_metro->GetNext()){
                    $tar=explode("(", $ob_metro["NAME"]);
                    $METRO.=trim($tar[0]);
                }
                if($arProps_rest["subway"]["VALUE"][$i+1]!="") $METRO.=", ";
                $i++;
            }





            $EXIT["RESTORAN_INFO"]='';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Телефон:</b> '.$TEL.'</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Адрес:</b> '.$ADDR.' </div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Метро:</b> '.$METRO.'</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il cpec_color"><b>Сотрудничество:</b> ';

            if($arProps_rest["rabotaem"]["VALUE"]=="Y") $EXIT["RESTORAN_INFO"].='Работаем';
            else  $EXIT["RESTORAN_INFO"].='НЕ Работаем';

            $EXIT["RESTORAN_INFO"].='</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Время работы:</b> '.$OPEN.'</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Процент:</b> <span id="rest_procent">'.$arProps_rest["procent"]["VALUE"].'</span>%</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il2"><b>Пометки:</b><br/> '.$arProps_rest["operator_comment"]["VALUE"].'</div>';


        }else{
            $EXIT["RESTORAN"]="Подбор";
        }


    }


    echo json_encode($EXIT);


}

function get_client_info_by_order(){
    global $APPLICATION;
    $_REQUEST["id"] = str_replace("order_", "", $_REQUEST["id"]);

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $EXIT = array();

    //сначала нужно получить инфу по заказу
    $res = CIBlockElement::GetByID(intval($_REQUEST["id"]));
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        $EXIT["DATE_CREATE"] = $arFields["DATE_CREATE"];
        //substr_count($arFields["DATE_CREATE"], "00:00")

        $EXIT["DATE_CREATE"] = str_replace("00:00:00", "", $EXIT["DATE_CREATE"]);

        $EXIT["ID"] = $arFields["ID"];
        $EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];


        $EXIT["PRIM"]=str_replace("<br />", "", $EXIT["PRIM"]);

        $tar=explode(" ", $arFields["ACTIVE_FROM"]);
        $tar2=explode(":", $tar[1]);
        $EXIT["DATE"] = $tar[0];
        $EXIT["TIME"] = $tar2[0].":".$tar2[1];

        if($EXIT["TIME"]==":") $EXIT["TIME"]="";

        if($arProps["status"]["VALUE"]=="") $arProps["status"]["VALUE"]="&nbsp;";
        if($arProps["source"]["VALUE"]=="") $arProps["source"]["VALUE"]="&nbsp;";




        if($arProps["status"]["VALUE"]=="заказ принят") $EXIT["STATUS_COLOR"] = "yello";
        if($arProps["status"]["VALUE"]=="забронирован") $EXIT["STATUS_COLOR"]  = "sv_zel";
        if($arProps["status"]["VALUE"]=="гости пришли") $EXIT["STATUS_COLOR"]  = "zel";
        if($arProps["status"]["VALUE"]=="гости отменили заказ") $EXIT["STATUS_COLOR"]  = "red";
        if($arProps["status"]["VALUE"]=="ошибочный заказ") $EXIT["STATUS_COLOR"]  = "black";
        if($arProps["status"]["VALUE"]=="банкет в работе") $EXIT["STATUS_COLOR"]  = "yello";
        if($arProps["status"]["VALUE"]=="внесена предоплата") $EXIT["STATUS_COLOR"]  = "yello";
        if($arProps["status"]["VALUE"]=="отчет") $EXIT["STATUS_COLOR"]  = "blue";
        if($arProps["status"]["VALUE"]=="заказ оплачен") $EXIT["STATUS_COLOR"]  = "blue";

        $EXIT["CONTROL_CALL"] = $arProps["control_call"]["VALUE"];


        $EXIT["GUEST"] = $arProps["guest"]["VALUE"];
        $EXIT["STATUS"] = $arProps["status"]["VALUE"];
        $EXIT["STATUS_ID"] = $arProps["status"]["VALUE_ENUM_ID"];
        $EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];

        $EXIT["PRIM_ORDER"] = $arFields["PREVIEW_TEXT"];
        $EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];
        $EXIT["SUMM_SCH"] = $arProps["summ_sch"]["VALUE"];
        $EXIT["STOL"] = $arProps["stol"]["VALUE"];
        $EXIT["PROCENT"] = $arProps["procent"]["VALUE"];






        $EXIT["PODTV2"] = $arProps["podtv2"]["VALUE"];
        $EXIT["PODTV"] = $arProps["podtv"]["VALUE"];


        $EXIT["USER_ID"] = $arProps["user"]["VALUE"];

        $EXIT["SOURCE"] = $arProps["source"]["VALUE"];
        $EXIT["SOURCE_ID"] = $arProps["source"]["VALUE_ENUM_ID"];

        $EXIT["TYPE"] = $arProps["type"]["VALUE"];
        $EXIT["TYPE_ID"] = $arProps["type"]["VALUE_ENUM_ID"];


        $EXIT["FORMAT"] = $arProps["format"]["VALUE"];

        $rsUser = CUser::GetByID($arFields["CREATED_BY"]);
        $arUser = $rsUser->Fetch();

        $EXIT["OPERATOR"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];

        CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], 105, array("new" => false));
        //удаляем файл
        @unlink($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders/".$_REQUEST["id"]);


        //теперь нужно получить инфу по ресторану
        $res_rest = CIBlockElement::GetByID($arProps["rest"]["VALUE"]);
        if($ob_rest = $res_rest->GetNextElement()){
            $arFields_rest = $ob_rest->GetFields();
            $arProps_rest = $ob_rest->GetProperties();

            //if($arProps_rest["procent"]["VALUE"]!="")

            $EXIT["RESTORAN"]=$arFields_rest["NAME"].($arFields_rest['ACTIVE']=='N'?" - НЕАКТИВЕН НА САЙТЕ":"");
            $EXIT["RESTORAN_ID"]=$arFields_rest["ID"];

            $i=0;
            foreach($arProps_rest["phone"]["VALUE"] as $T){
                $TEL.=$T;
                if($arProps_rest["phone"]["VALUE"][$i+1]!="") $TEL.=", ";
                $i++;
            }

            $i=0;
            foreach($arProps_rest["address"]["VALUE"] as $T){
                $ADDR.=$T;
                if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
                $i++;
            }

            $i=0;
            foreach($arProps_rest["opening_hours"]["VALUE"] as $T){
                $OPEN.=$T;
                if($arProps_rest["opening_hours"]["VALUE"][$i+1]!="") $OPEN.=", ";
                $i++;
            }

            $i=0;
            foreach($arProps_rest["subway"]["VALUE"] as $T){
                $res_metro = CIBlockElement::GetByID($T);
                if($ob_metro = $res_metro->GetNext()){
                    $tar=explode("(", $ob_metro["NAME"]);
                    $METRO.=trim($tar[0]);
                }
                if($arProps_rest["subway"]["VALUE"][$i+1]!="") $METRO.=", ";
                $i++;
            }


            $EXIT["RESTORAN_INFO"]='';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Телефон:</b> '.$TEL.'</div>';
            if($arProps_rest["real_tel"]["VALUE"]!="") $EXIT["RESTORAN_INFO"].='<div class="il"><b>Реальный телефон:</b> '.$arProps_rest["real_tel"]["VALUE"].'</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Адрес:</b> '.$ADDR.' </div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Метро:</b> '.$METRO.'</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il spec_color"><b>Сотрудничество:</b> ';

            if($arProps_rest["rabotaem"]["VALUE"]=="Y") $EXIT["RESTORAN_INFO"].='Работаем';
            else  $EXIT["RESTORAN_INFO"].='НЕ Работаем';

            $EXIT["RESTORAN_INFO"].='</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Время работы:</b> '.$OPEN.'</div>';
            $EXIT["RESTORAN_INFO"].='<div class="il"><b>Процент:</b> <span id="rest_procent">'.$arProps_rest["procent"]["VALUE"].'</span></div>';
            $EXIT["RESTORAN_INFO"].='<div class="il2"><b>Пометки:</b><br/> '.$arProps_rest["operator_comment"]["VALUE"].'</div>';
        }

        //теперь берем инфу по клиенту
        if($arProps["client"]["VALUE"]>0){
            $res = CIBlockElement::GetByID($arProps["client"]["VALUE"]);
            if($ob = $res->GetNextElement()){
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();

                $EXIT["CLIENT_ID"] = $arFields["ID"];
                //var_dump($arProps);
                $EXIT["NAME"] = $arProps["NAME"]["VALUE"];
                $EXIT["SURNAME"] = $arProps["SURNAME"]["VALUE"];
                $EXIT["EMAIL"] = $arProps["EMAIL"]["VALUE"];
                $EXIT["TELEPHONE"] = $arProps["TELEPHONE"]["VALUE"];
                $EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];

                $EXIT["PRIM"]=str_replace("<br />", "", $EXIT["PRIM"]);

                $EXIT["ORDERS"]='<div class="tbl">';
                $EXIT["ORDERS"].='<table cellpadding="0" cellspacing="0">';
                $EXIT["ORDERS"].='<tr class="hd">';
                $EXIT["ORDERS"].='<th class="c11">ресторан</th>';
                $EXIT["ORDERS"].='<th class="c12">гостей</th>';
                $EXIT["ORDERS"].='<th class="c13">визит</th>';
                $EXIT["ORDERS"].='<th class="c14">статус</th>';
                $EXIT["ORDERS"].='</tr>';

                //Теперь нужно запросить все заказы для этого пользователя
                //Эту часть нужно будет закэшировать
                $HO=false;
                $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_client"=>$arFields["ID"], "SECTION_CODE" => $_SESSION["CITY"], "!PROPERTY_rest" => 1139761);
                $res_zak = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter, false, false, false);
                while($ob_zak= $res_zak->GetNextElement()){
                    $arFields_zak = $ob_zak->GetFields();
                    $arProps_zak = $ob_zak->GetProperties();

                    $arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", MakeTimeStamp($arFields_zak["ACTIVE_FROM"], CSite::GetDateFormat()));

                    $resr = CIBlockElement::GetByID($arProps_zak["rest"]["VALUE"]);
                    if($arFields_rest = $resr->GetNext()){
                        $REST_NAME = $arFields_rest["NAME"];

                    }else $REST_NAME="Подбор";



                    $EXIT["ORDERS"].='<tr id="order_'.$arFields_zak["ID"].'">';
                    $EXIT["ORDERS"].='<td class="c11"><div>&nbsp;'.$REST_NAME.'</div></td>';
                    $EXIT["ORDERS"].='<td class="c12"><div>&nbsp;'.$arProps_zak["guest"]["VALUE"].'</div></td>';
                    $EXIT["ORDERS"].='<td class="c13"><div>&nbsp;'.$arFields_zak["ACTIVE_FROM"].'</div></td>';
                    $EXIT["ORDERS"].='<td class="c14"><div>&nbsp;'.$arProps_zak["status"]["VALUE"].'</div></td>';
                    $EXIT["ORDERS"].='</tr>';

                    $HO=true;

                }

                $EXIT["ORDERS"].='</table>';
                $EXIT["ORDERS"].='</div>';

                if(!$HO) $EXIT["ORDERS"]="";

                if($arProps["user"]["VALUE"]>0){
                    $rsUser = CUser::GetByID($arProps["user"]["VALUE"]);
                    $arUser = $rsUser->Fetch();


                    $EXIT["USER_ID"] = $arProps["user"]["VALUE"];
                    if($EXIT["USER_ID"]>0) $EXIT["USRLNK"]="/users/id".$EXIT["USER_ID"]."/";

                    $EXIT["NAME"] = $arUser["NAME"];
                    $EXIT["SURNAME"] = $arUser["LAST_NAME"];
                    $EXIT["EMAIL"] = $arUser["EMAIL"];
                    if (!$EXIT["TELEPHONE"])
                        $EXIT["TELEPHONE"] = $arUser["PERSONAL_PHONE"];
                    $EXIT["PRIM"] = "Пользователь сайта. Зарегистрирован ".$arUser["DATE_REGISTER"].". \n".$arUser["WORK_NOTES"];
                    $EXIT["PRIM"] = str_replace("<br />", "", $EXIT["PRIM"]);

                }


            }
        }else{
            //Посмотрим, может быть это пользователь с сайта
            if($arProps["user"]["VALUE"]>0){
                $rsUser = CUser::GetByID($arProps["user"]["VALUE"]);
                $arUser = $rsUser->Fetch();



                $EXIT["CLIENT_ID"] = "";
                $EXIT["USER_ID"] = $arProps["user"]["VALUE"];
                //var_dump($arUser);
                if($EXIT["USER_ID"]>0) $EXIT["USRLNK"]="/users/id".$EXIT["USER_ID"]."/";


                $EXIT["NAME"] = $arUser["NAME"];
                $EXIT["SURNAME"] = $arUser["LAST_NAME"];
                $EXIT["EMAIL"] = $arUser["EMAIL"];
                $EXIT["TELEPHONE"] = $arUser["PERSONAL_PHONE"];
                $EXIT["PRIM"] = "Пользователь сайта. Зарегистрирован ".$arUser["DATE_REGISTER"].". \n".$arUser["WORK_NOTES"];
                $EXIT["PRIM"] = str_replace("<br />", "", $EXIT["PRIM"]);
                $EXIT["ORDERS"]="";
            }

        }

    }



    echo json_encode($EXIT);
}

function load_rest_filter(){
    global $APPLICATION;

    if($_REQUEST["fltr"]=="#type"){

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/rest_fltr_type.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }

    if($_REQUEST["fltr"]=="#props"){

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/rest_fltr_props.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }

    if($_REQUEST["fltr"]=="#location"){

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/rest_fltr_location.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }

    if($_REQUEST["fltr"]=="#intertaim"){

        $APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/rest_fltr_intertaim.php"),
            Array(),
            Array("MODE"=>"php")
        );
    }
}

//нужно сделать проверку на разрешение удаления отчета
function remove_report(){
    global $APPLICATION;
    $ELEMENT_ID = str_replace("#", "", $_REQUEST["report_id"]);

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if(CIBlockElement::Delete($ELEMENT_ID)) echo "ok";


}


function search_rest_by_name(){
    global $APPLICATION;
    global $arrFilter;

//    $arrFilter["ACTIVE"]="Y";

    $TRANS = translitIt(urldecode($_REQUEST["name"]));

    $arrFilter=array(
        array(
            "LOGIC"=>"OR",
            "TAGS"=>"%".urldecode($_REQUEST["name"])."%",
            "?NAME"=>"%".urldecode($_REQUEST["name"])."% || %".$TRANS."%"
        ),
//        'ACTIVE'=>'Y',
        '!ID'=>1139761,
        '!PROPERTY_NOT_SHOW_IN_BS_VALUE'=>'Y'
    );

    if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
    if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
    if($_SESSION["CITY"]=="jurmala") $IBLOCK_ID=array(3567,3566); //юрмала и рига

    if($_SESSION["CITY"]=="nsk") $IBLOCK_ID=3418;

    //$arrFilter["NAME"]="%".urldecode($_REQUEST["name"])."%";
    //$arrFilter["IBLOCK_CODE"]=$_SESSION["CITY"];
    //$arrFilter["SECTION_CODE"]="restaurants";

    //var_dump($arrFilter);

    $APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "restorans_list", array(
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => $IBLOCK_ID,
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => "ID",
        "ELEMENT_SORT_ORDER" => "DESC",
        "FILTER_NAME" => "arrFilter",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-2,
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "phone",
        ),
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "Y",//a
        "CACHE_TIME" => "86400",//36000001
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "N",
        "PRICE_CODE" => array(
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "USE_PRODUCT_QUANTITY" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "win_pager",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
        false
    );

}



function filtr_rest(){
    global $arrFilter;
    global $APPLICATION;

    $arrFilter=array();

    foreach($_REQUEST as $CODE=>$V){
        //Фильтр по метро
        if(substr_count($CODE, "metro")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_subway"][]=$t[1];
        }

        if(substr_count($CODE, "type")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_type"][]=$t[1];
        }

        if(substr_count($CODE, "ideal")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_ideal_place_for"][]=$t[1];
        }

        if(substr_count($CODE, "music")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_music"][]=$t[1];
        }

        if(substr_count($CODE, "parking")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_parking"][]=$t[1];
        }


        if(substr_count($CODE, "kitchen")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_kitchen"][]=$t[1];
        }

        if(substr_count($CODE, "average_bill")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_average_bill"][]=$t[1];
        }


        if(substr_count($CODE, "children")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_children"][]=$t[1];
        }


        if(substr_count($CODE, "features")){
            $t = explode("_", $CODE);
            $arrFilter["PROPERTY_features"][]=$t[1];
        }

    }



    //$arrFilter["IBLOCK_CODE"] = $_SESSION["CITY"];
    //$arrFilter["PROPERTY_SUBWAY"] = 184378;

    if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
    if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
    if($_SESSION["CITY"]=="jurmala") $IBLOCK_ID=array(3567,3566); //юрмала и рига

    if($_SESSION["CITY"]=="nsk") $IBLOCK_ID=3418;


    if($_REQUEST["rzdl"]=="#location"){
        $_SESSION["REST_FLTR"]["PROPERTY_subway"] = $arrFilter["PROPERTY_subway"];
    }

    if($_REQUEST["rzdl"]=="#type"){
        $_SESSION["REST_FLTR"]["PROPERTY_type"] = $arrFilter["PROPERTY_type"];
        $_SESSION["REST_FLTR"]["PROPERTY_ideal_place_for"] = $arrFilter["PROPERTY_ideal_place_for"];
        $_SESSION["REST_FLTR"]["PROPERTY_music"] = $arrFilter["PROPERTY_music"];
        $_SESSION["REST_FLTR"]["PROPERTY_parking"] = $arrFilter["PROPERTY_parking"];
    }

    if($_REQUEST["rzdl"]=="#props"){
        $_SESSION["REST_FLTR"]["PROPERTY_kitchen"] = $arrFilter["PROPERTY_kitchen"];
        $_SESSION["REST_FLTR"]["PROPERTY_average_bill"] = $arrFilter["PROPERTY_average_bill"];
    }

    if($_REQUEST["rzdl"]=="#intertaim"){
        $_SESSION["REST_FLTR"]["PROPERTY_features"] = $arrFilter["PROPERTY_features"];
        $_SESSION["REST_FLTR"]["PROPERTY_children"] = $arrFilter["PROPERTY_children"];
    }

    foreach($_SESSION["REST_FLTR"] as $M=>$V) if($V=="") unset($_SESSION["REST_FLTR"][$M]);


    $arrFilter=$_SESSION["REST_FLTR"];
    $arrFilter["SECTION_CODE"]="restaurants";


    $arrFilter["ACTIVE"]="Y";
    //var_dump($arrFilter);

    $APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "restorans_list", array(
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => $IBLOCK_ID,
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => "ID",
        "ELEMENT_SORT_ORDER" => "DESC",
        "FILTER_NAME" => "arrFilter",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-2,
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "phone",
            1=> "subway"
        ),
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "N",
        "CACHE_FILTER" => "Y",
        "PRICE_CODE" => array(
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "USE_PRODUCT_QUANTITY" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "win_pager",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
        false
    );


}

function check_new(){
    //$_SERVER["DOCUMENT_ROOT"];


    $EXIT=array();
    $EXIT["SMS"]=0;
    $EXIT["ORDERS"]=0;


    $files = scandir($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders");    //сканируем (получаем массив файлов)
    array_shift($files); // удаляем из массива '.'
    array_shift($files);


    // var_dump($_SESSION["CITY"]);

    $CIT=$_SESSION["CITY"];

    if($CIT=="jurmala") $CIT="urm";

    //if($_SESSION["CITY"]=="spb")
    $CO_NEW=0;
    foreach($files as $f){
        $file_array = file($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders/".$f);
        if($file_array[0]==$CIT || ($CIT=="urm" && $file_array[0]=="rga")){
            $CO_NEW++;
        }
//        var_dump($file_array[0]);
//        FirePHP::getInstance()->info($file_array[0],'$file_array- '.$f);
    }

    $EXIT["ORDERS"]=$CO_NEW;

    echo json_encode($EXIT);

}

function check_new_bonus(){
    global $USER;
    $EXIT=array();
    $EXIT["BONUS"]=0;
    global $DB;
    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if($_SESSION["CITY"]=="jurmala") {
        echo json_encode($EXIT);
        die();
    }
    //if($USER->IsAdmin()){
    ///1111111

    //выбираем заказы и группируем по пользователям
    $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_status"=>array(1808,1506,1810), "SECTION_CODE"=>$_SESSION["CITY"],'!PROPERTY_rest'=>1139761);

//    if($_SESSION["CITY"]=="spb") $arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,3,2,2014));
    $arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,12,1,2015));

    $res = CIBlockElement::GetList(Array("CNT"=>"DESC"), $arFilter, array("PROPERTY_client"), false, array("ID"));
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        if($arFields["CNT"]>1){
            $CLIDs[]=$arFields["PROPERTY_CLIENT_VALUE"];
            $CL[$arFields["PROPERTY_CLIENT_VALUE"]]=$arFields["CNT"];
        }
    }

    //Выбираем истории выплат по пользователям

    $arFilter = Array("IBLOCK_ID"=>2964, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,12,1,2015));
    $res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, false);
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProperties = $ob->GetProperties();

        $PAYS[$arProperties["client"]["VALUE"]][]=array(
            "ID"=>$arFields["ID"],
            "DATE_CREATE"=>$arFields["DATE_CREATE"],
            "SUMM"=>$arProperties["summ"]["VALUE"]
        );
        $CLIENTS[$arProperties["client"]["VALUE"]]["ALLSUMM"]+=$arProperties["summ"]["VALUE"];
    }

    $PAYSco=0;
    foreach($CL as $clid=>$cnt){
        $NEED_PAY=false;
        $SUM=0;

        $FIRST_BONUS=200;
        $SECOND_BONUS=300;
        $THIRD_BONUS=500;


//        if($cnt>=3){
//            if(count($PAYS[$clid])==0){
//                $PAYSco++;
//                $EXIT["CLIENTS"][]=$clid;
//            }else{
//                if($cnt%10==0 || $cnt>10 || ($cnt>=5 && $cnt<=10)){
//                    $v=intval($cnt/10);
//                    if($CLIENTS[$clid]["ALLSUMM"]<$v*$SECOND_BONUS+$FIRST_BONUS+$SECOND_BONUS){
//                        $PAYSco++;
//                        $EXIT["CLIENTS"][]=$clid;
//                    }
//                }
//            }
//        }

        if($cnt>=2){
            if(count($PAYS[$clid])==0){ //  если оплат не было
                $PAYSco++;
                $EXIT["CLIENTS"][]=$clid;
            }
            elseif($cnt>=5 && $cnt<10 && count($PAYS[$clid])==1){
                $PAYSco++;
                $EXIT["CLIENTS"][]=$clid;
            }
            else {
                if($cnt%5==0 || $cnt>5){//   todo почему просто не >=5   || ($cnt>=5 && $cnt<=10)
                    $v=intval($cnt/5);//$SECOND_BONUS за 5 заказ
                    if($CLIENTS[$clid]["ALLSUMM"]<$v*$THIRD_BONUS+$FIRST_BONUS+$SECOND_BONUS){
                        $PAYSco++;
                        $EXIT["CLIENTS"][]=$clid;
                    }
                }
            }
        }


    }

    $EXIT["BONUS"]=$PAYSco;
    //}

    echo json_encode($EXIT);
}


function add_rest_form(){
    global $APPLICATION;
    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/add_rest_form.php"),
        Array(),
        Array("MODE"=>"php")
    );
}


function edite_rest_form(){
    global $APPLICATION;
    global $USER;

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }


    $ELEMENT_ID = str_replace("#", "", $_REQUEST["id"]);

    $res = CIBlockElement::GetByID($ELEMENT_ID);
    if($ob = $res->GetNextElement()){
        $arProps = $ob->GetProperties();
        //var_dump($arProps["rabotaem"]);
    }

    global $APPLICATION;
    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/edite_rest_form.php"),
        Array("VNINFO"=>$arProps["vn_info"]["VALUE"], "POMETKI"=>$arProps["pometki"]["VALUE"], "ID"=>$ELEMENT_ID, "RABOTAEM"=>$arProps["rabotaem"]["VALUE"],"REAL_TEL"=>$arProps["real_tel"]["VALUE"],"PROCENT"=>$arProps["procent"]["VALUE"]),
        Array("MODE"=>"php")
    );
}


function edite_rest(){
    global $APPLICATION;
    global $USER;

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("pometki" => $_REQUEST["pometki"]));
    CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("vn_info" => $_REQUEST["vninfo"]));
    CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("real_tel" => $_REQUEST["real_tel"]));
    CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("procent" => $_REQUEST["procent"]));

    if($_REQUEST["rabotaem"]!=""){
        if($_SESSION["CITY"]=="msk") $PROP["rabotaem"] = 1547;
        if($_SESSION["CITY"]=="spb") $PROP["rabotaem"] = 1557;

        CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("rabotaem" => $PROP["rabotaem"]));
    }else
        CIBlockElement::SetPropertyValueCode($_REQUEST["id"], "rabotaem", false);
}

/************ДОБАВЛЯЕМ НОВЫЙ РЕСТОРАН***************/
function add_new_rest(){
    global $APPLICATION;
    global $USER;

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    //нужно получить id инфоблока с городом
    $arrFilter["IBLOCK_CODE"]=$_SESSION["CITY"];
    if(isset($_REQUEST["city4add"]) && $_REQUEST["city4add"]!="") $arrFilter["IBLOCK_CODE"]=$_REQUEST["city4add"];

    if($arrFilter["IBLOCK_CODE"]=="jurmala")  $arrFilter["IBLOCK_CODE"]="urm";

    $res = CIBlock::GetList(Array(), Array('TYPE'=>'catalog', 'SITE_ID'=>SITE_ID, 'ACTIVE'=>'Y', "CODE"=>$arrFilter["IBLOCK_CODE"]), true);
    if($REST_IB = $res->Fetch()){

    }
    $PROP = array();

    if($arrFilter["IBLOCK_CODE"]=="msk") $PROP["rabotaem"] = 1547;
    if($arrFilter["IBLOCK_CODE"]=="spb") $PROP["rabotaem"] = 1557;


    if($arrFilter["IBLOCK_CODE"]=="msk") $PROP["operator_add"] = 1924;
    if($arrFilter["IBLOCK_CODE"]=="spb") $PROP["operator_add"] = 1923;


    if($arrFilter["IBLOCK_CODE"]=="msk") $SID = 32;
    if($arrFilter["IBLOCK_CODE"]=="spb") $SID = 103;

    if($arrFilter["IBLOCK_CODE"]=="urm") $SID = 247660;
    if($arrFilter["IBLOCK_CODE"]=="rga") $SID = 247659;

    if($arrFilter["IBLOCK_CODE"]=="nsk") $SID = 239716;

    $el = new CIBlockElement;


    $PROP["address"] = $_REQUEST["address"];
    $PROP["phone"] = $_REQUEST["telephone"];
    $PROP["subway"] = $_REQUEST["metro"];
    $PROP["kitchen"] = $_REQUEST["cook"];
    $PROP["opening_hours"] = $_REQUEST["time"];
    $PROP["pometki"] = $_REQUEST["pometki"];
    $PROP["vn_info"] = $_REQUEST["vninfo"];


    //$arrFilter["SECTION_CODE"]="restaurants";

    $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(),
        "IBLOCK_ID"=>$REST_IB["ID"],
        "IBLOCK_SECTION_ID" => $SID,
        "PROPERTY_VALUES"=> $PROP,
        "NAME"           => $_REQUEST["name"],
        "ACTIVE"         => "N",            // активен
        "CODE"         => translitIt($_REQUEST["name"])."_operator",
    );


    //var_dump($arLoadProductArray);


    if($REST_ID = $el->Add($arLoadProductArray))
    {
        echo "ok";
        $arEventFields = array(
            "USER_ID" => $USER->GetID(),
            "USER_NAME" => $USER->GetFullName(),
            "REST_NAME" => $_REQUEST["name"],
            "REST_ID" => $REST_ID,
            "IBLOCK_ID" => $REST_IB["ID"],
            "CITY"=>$_SESSION["CITY"]
        );
        CEvent::Send("ADD_REST", "s1", $arEventFields,false,146);
    }
    else
        echo "Error: ".$el->LAST_ERROR;


}


function sms_otvet_form(){
    global $APPLICATION;
    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/sms_otvet_form.php"),
        Array("ID"=>$_REQUEST["id"]),
        Array("MODE"=>"php")
    );
}


function send_sms_otvet(){
    global $APPLICATION;

    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $res = CIBlockElement::GetByID($_REQUEST["id"]);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();


        $TXT='<?xml version="1.0" encoding="UTF-8" ?>
		<data>
		<authorization>
			<login>restoran_1223</login>
			<password>d3Csa23w94</password>
		</authorization>
		<type>send</type>
		<request>
			<transaction_id>'.$arProps["transaction_id"]["VALUE"].'</transaction_id>
			<message>'.$_REQUEST["otvet_text"].'</message>
		</request>
		</data>';

        //var_dump($TXT);

        //инициализируем сеанс
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://gate.ias.su/baseapi/');
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $TXT);
        $res = curl_exec($curl);

        var_dump($res);


        CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], 149, array("new" => ""));
        //удаляем файл
        @unlink($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/sms/".$ID);

    }
}


function get_client_by_sms(){
    global $APPLICATION;
    $_REQUEST["id"] = str_replace("sms_", "", $_REQUEST["id"]);
    $res = CIBlockElement::GetByID($_REQUEST["id"]);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $SMSn = trim(str_replace("+7", "", $arFields["NAME"]));
    }




    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }
    //var_dump($SMSn);

    $EXIT = array();
    $arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y", "PROPERTY_telephone"=>"%".$SMSn."%");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();

        $EXIT["CLIENT_ID"] = $arFields["ID"];

        $EXIT["NAME"] = $arProps["NAME"]["VALUE"];
        $EXIT["SURNAME"] = $arProps["SURNAME"]["VALUE"];
        $EXIT["EMAIL"] = $arProps["EMAIL"]["VALUE"];
        $EXIT["TELEPHONE"] = $arProps["TELEPHONE"]["VALUE"];
        $EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];

        $EXIT["ORDERS"]='<div class="tbl">';
        $EXIT["ORDERS"].='<table cellpadding="0" cellspacing="0">';
        $EXIT["ORDERS"].='<tr class="hd">';
        $EXIT["ORDERS"].='<th class="c11">ресторан</th>';
        $EXIT["ORDERS"].='<th class="c12">гостей</th>';
        $EXIT["ORDERS"].='<th class="c13">визит</th>';
        $EXIT["ORDERS"].='<th class="c14">статус</th>';
        $EXIT["ORDERS"].='</tr>';

        //Теперь нужно запросить все заказы для этого пользователя
        //Эту часть нужно будет закэшировать
        $HO=false;
        $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_client"=>$arFields["ID"],'!PROPERTY_rest'=>1139761);
        $res_zak = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter, false, false, false);
        while($ob_zak= $res_zak->GetNextElement()){
            $arFields_zak = $ob_zak->GetFields();
            $arProps_zak = $ob_zak->GetProperties();

            $arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", MakeTimeStamp($arFields_zak["ACTIVE_FROM"], CSite::GetDateFormat()));

            $resr = CIBlockElement::GetByID($arProps_zak["rest"]["VALUE"]);
            if($arFields_rest = $resr->GetNext()){
                $REST_NAME = $arFields_rest["NAME"];

            }else $REST_NAME="&nbsp;";



            $EXIT["ORDERS"].='<tr id="order_'.$arFields_zak["ID"].'">';
            $EXIT["ORDERS"].='<td class="c11"><div>&nbsp;'.$REST_NAME.'</div></td>';
            $EXIT["ORDERS"].='<td class="c12"><div>&nbsp;'.$arProps_zak["guest"]["VALUE"].'</div></td>';
            $EXIT["ORDERS"].='<td class="c13"><div>&nbsp;'.$arFields_zak["ACTIVE_FROM"].'</div></td>';
            $EXIT["ORDERS"].='<td class="c14"><div>&nbsp;'.$arProps_zak["status"]["VALUE"].'</div></td>';
            $EXIT["ORDERS"].='</tr>';

            $HO=true;

        }

        $EXIT["ORDERS"].='</table>';
        $EXIT["ORDERS"].='</div>';

        if(!$HO) $EXIT["ORDERS"]="";

    }

    echo json_encode($EXIT);



}


function clear_rest_filter(){
    global $APPLICATION;
    unset($_SESSION["REST_FLTR"]);

    if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
    if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
    if($_SESSION["CITY"]=="jurmala") $IBLOCK_ID=array(3567,3566); //юрмала и рига

    if($_SESSION["CITY"]=="nsk") $IBLOCK_ID=3418;

    $APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "restorans_list", array(
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => $IBLOCK_ID,
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => "ID",
        "ELEMENT_SORT_ORDER" => "DESC",
        "FILTER_NAME" => "arrFilter",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-2,
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "phone",
            1=> "subway"
        ),
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "N",
        "CACHE_FILTER" => "Y",
        "PRICE_CODE" => array(
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "USE_PRODUCT_QUANTITY" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "win_pager",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
        false
    );


}



function clear_orders_filter(){
    global $APPLICATION;

    unset($_SESSION["ALL_ORDERS_FLTR"]);

    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/all_orders.php"),
        Array(),
        Array("MODE"=>"php")
    );

}

function filter_orders(){

    global $arrFilter;
    global $APPLICATION;

    $arrFilter=array();

    //unset($_SESSION["ALL_ORDERS_FLTR"]);
    if($_REQUEST["date_po"]!="") $_REQUEST["date_po"] = date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po"])));
    if($_REQUEST["date_po2"]!="") $_REQUEST["date_po2"] = date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po2"])));

    if($_REQUEST["restoran_id"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"] && $_REQUEST["restoran_id"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]=$_REQUEST["restoran_id"];
    if($_REQUEST["source"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_source"] && $_REQUEST["source"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_source"]=$_REQUEST["source"];

    if($_REQUEST["status"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"] && $_REQUEST["status"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]=$_REQUEST["status"];
    if($_REQUEST["operator"]!=$_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"] && $_REQUEST["operator"]!="") $_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]=$_REQUEST["operator"];

    if($_REQUEST["type"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_type"] && $_REQUEST["type"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_type"]=$_REQUEST["type"];

    if($_REQUEST["date_s"]!=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_ACTIVE_FROM"] && $_REQUEST["date_s"]!="") $_SESSION["ALL_ORDERS_FLTR"][">=DATE_ACTIVE_FROM"]=$_REQUEST["date_s"];
    if($_REQUEST["date_po"]!=$_SESSION["ALL_ORDERS_FLTR"]["<DATE_ACTIVE_FROM"] && $_REQUEST["date_po"]!="") $_SESSION["ALL_ORDERS_FLTR"]["<DATE_ACTIVE_FROM"]=$_REQUEST["date_po"];

    if($_REQUEST["date_s2"]!=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"] && $_REQUEST["date_s2"]!="") $_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]=$_REQUEST["date_s2"];
    if($_REQUEST["date_po2"]!=$_SESSION["ALL_ORDERS_FLTR"]["<DATE_CREATE"] && $_REQUEST["date_po2"]!="") $_SESSION["ALL_ORDERS_FLTR"]["<DATE_CREATE"]=$_REQUEST["date_po2"];

    if($_REQUEST["control_call"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_control_call"] && $_REQUEST["control_call"]!=""){
        $_SESSION["ALL_ORDERS_FLTR"][">=PROPERTY_control_call"]=ConvertDateTime($_REQUEST["control_call"], "YYYY-MM-DD")." 00:00:00";
        $_SESSION["ALL_ORDERS_FLTR"]["<=PROPERTY_control_call"]=ConvertDateTime($_REQUEST["control_call"], "YYYY-MM-DD")." 23:59:59";
    }


    //print_r($_SESSION["ALL_ORDERS_FLTR"]);
    //$newdate = strtotime ('-1 day', strtotime($_REQUEST["date_po"])) ;

    //$newdate = date( 'd.m.Y' ,strtotime('-1 day', strtotime($_REQUEST["date_po"])));

    foreach($_SESSION["ALL_ORDERS_FLTR"] as $M=>$V) if($V=="") unset($_SESSION["ALL_ORDERS_FLTR"][$M]);

    if($_SESSION["ALL_ORDERS_FLTR"]) $arrFilter=$_SESSION["ALL_ORDERS_FLTR"];

    //var_dump($arrFilter);
//    FirePHP::getInstance()->info($arrFilter);

    global $USER;
    $APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "orders_list_all", array(
        "IBLOCK_TYPE" => "booking_service",
        "IBLOCK_ID" => "105",
        "SECTION_ID" => "",
        "SECTION_CODE" => $_SESSION["CITY"],
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => $_SESSION["ORDERS_ORDER"],
        "ELEMENT_SORT_ORDER" => $_SESSION["ORDERS_BY"],
        "FILTER_NAME" => "arrFilter",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-4,
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "guest",
            1 => "rest",
        ),
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => $USER->IsAdmin()?'N':"A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "N",
        "CACHE_FILTER" => "Y",
        "PRICE_CODE" => array(
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "USE_PRODUCT_QUANTITY" => "N",
        "DISPLAY_TOP_PAGER" => "Y",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "win_pager",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CBY_F"=>"Y"
    ),
        false
    );

}



function create_report(){
    global $APPLICATION;

    global $arFilter;

    //Формируем фильтр для заказов
    if($_REQUEST["date_s"]!=""){
        $arFilter[">=DATE_ACTIVE_FROM"]=$_REQUEST["date_s"];
    }

    if($_REQUEST["date_po"]!=""){
        $arFilter["<=DATE_ACTIVE_FROM"]=date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po"])));
    }

    if($_REQUEST["date_s2"]!=""){
        $arFilter[">=DATE_CREATE"]=$_REQUEST["date_s2"];
    }

    if($_REQUEST["date_po2"]!=""){
        $arFilter["<DATE_CREATE"]=date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po2"])));
    }

    //ИСТОЧНИК
    if($_REQUEST["source"]>0){
        $arFilter["PROPERTY_source"]=$_REQUEST["source"];
    }

    //ТИП
    if($_REQUEST["type"]>0){
        $arFilter["PROPERTY_type"]=$_REQUEST["type"];
    }

    //СТАТУС
    if($_REQUEST["status"]>0){
        $arFilter["PROPERTY_status"]=$_REQUEST["status"];
    }


    //РЕСТОРАН
    if($_REQUEST["restoran_id"]>0){
        $arFilter["PROPERTY_rest"]=$_REQUEST["restoran_id"];
    }

    //ОПЕРАТОР
    if($_REQUEST["operator"]>0){
        $arFilter["CREATED_BY"]=$_REQUEST["operator"];
        $rsUser = CUser::GetByID($_REQUEST["operator"]);
        $OPERATOR = $rsUser->Fetch();

        $ONAME.=$OPERATOR["LAST_NAME"]." ".$OPERATOR["NAME"].",";
    }

    $OGRAn=5000;
    if(count($arFilter)==0) $OGRAn=1000;


    $arFilter["IBLOCK_ID"]=105;
    $arFilter["ACTIVE"]="Y";

    $arFilter["SECTION_CODE"] = $_SESSION["CITY"];


    $APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/reports_filter.php"),
        Array("FLTR"=>$arFilter),
        Array("MODE"=>"php")
    );

    //var_dump($arFilter);



    $APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "orders_for_report", array(
        "IBLOCK_TYPE" => "booking_service",
        "IBLOCK_ID" => "105",
        "SECTION_ID" => "",
        "SECTION_CODE" => $_SESSION["CITY"],
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => "ID",
        "ELEMENT_SORT_ORDER" => "DESC",
        "FILTER_NAME" => "arFilter",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "PAGE_ELEMENT_COUNT" => 50000,
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "summ_sch",

        ),
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "N",
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "ADD_SECTIONS_CHAIN" => "N",
        "DISPLAY_COMPARE" => "N",
        "SET_TITLE" => "Y",
        "SET_STATUS_404" => "N",
        "CACHE_FILTER" => "Y",
        "PRICE_CODE" => array(
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "USE_PRODUCT_QUANTITY" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "win_pager",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "NO_COUNT"=>"Y"
    ),
        false
    );



}



function pay_orders(){
    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if(count($_REQUEST["ids"])>0){
        $arFilter = Array("IBLOCK_ID"=>105,"ID"=>$_REQUEST["ids"]);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("PROPERTY_status","ID"));
        while($arFields = $res->GetNext()){
            if($arFields["PROPERTY_STATUS_VALUE"]=="отчет") $STATS[]=$arFields["ID"];
        }

        //var_dump($STATS);
        //die();
        foreach($_REQUEST["ids"] as $key=>$val){
            //у отмеченных заказов проставляем статус "заказ оплачен"
            if(!in_array($val, $STATS)) continue;

            CIBlockElement::SetPropertyValuesEx($val, 105, array("status" => 1506));

            //заносим комментарии
            if($_REQUEST["prim_".$val]!="") CIBlockElement::SetPropertyValuesEx($val, 105, array("prim_oplata" => $_REQUEST["prim_".$val]));
        }


        $EXIT["message"]="Отмеченные заказы оплачены.";
        echo json_encode($EXIT);
    }else{
        $EXIT["message"]="Не выбраны заказы.";
        echo json_encode($EXIT);
    }
}


function arch_orders(){
    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    foreach($_REQUEST["ids"] as $key=>$val){
        //у отмеченных заказов проставляем статус "Архив"
        CIBlockElement::SetPropertyValuesEx($val, 105, array("status" => 1810));

        //заносим комментарии
        if($_REQUEST["prim_".$val]!="") CIBlockElement::SetPropertyValuesEx($val, 105, array("prim_oplata" => $_REQUEST["prim_".$val]));
    }


    $EXIT["message"]="Заказы отправлены в архив.";
    echo json_encode($EXIT);
}



if($_REQUEST["act"]=="pay_orders") pay_orders();
if($_REQUEST["act"]=="arch_orders") arch_orders();





if($_REQUEST["act"]=="clear_orders_filter") clear_orders_filter();
if($_REQUEST["act"]=="filter_orders") filter_orders();
if($_REQUEST["act"]=="clear_rest_filter")  clear_rest_filter();
if($_REQUEST["act"]=="get_client_by_sms") get_client_by_sms();
if($_REQUEST["act"]=="send_sms_otvet") send_sms_otvet();
if($_REQUEST["act"]=="sms_otvet_form") sms_otvet_form();

if($_REQUEST["act"]=="check_new") check_new();

if($_REQUEST["act"]=="get_rest_detailed_info") get_rest_detailed_info();
if($_REQUEST["act"]=="load_rest_filter") load_rest_filter();

if($_REQUEST["act"]=="get_client_info_by_order") get_client_info_by_order();
if($_REQUEST["act"]=="get_order_info") get_order_info();
if($_REQUEST["act"]=="get_restoran_info") get_restoran_info();
if($_REQUEST["act"]=="search_restoran") search_restoran();
if($_REQUEST["act"]=="add_new_client") add_new_client();
if($_REQUEST["act"]=="search_client_by_email") search_client_by_email();
if($_REQUEST["act"]=="search_client_by_telephone") search_client_by_telephone();
if($_REQUEST["act"]=="search_client_by_name") search_client_by_name();
if($_REQUEST["act"]=="search_client_by_surname") search_client_by_surname();
if($_REQUEST["act"]=="update_client") update_client();
if($_REQUEST["act"]=="change_razdel") change_razdel();
if($_REQUEST["act"]=="get_client_info") get_client_info();
if($_REQUEST["act"]=="change_page") change_razdel();
if($_REQUEST["act"]=="resort_clients") change_razdel();
if($_REQUEST["act"]=="clear_pos") clear_pos();
if($_REQUEST["act"]=="save_win_pos") save_win_pos();
if($_REQUEST["act"]=="user_auth") user_auth();

if($_REQUEST["act"]=="remove_report") remove_report();
if($_REQUEST["act"]=="search_rest_by_name") search_rest_by_name();
if($_REQUEST["act"]=="filtr_rest") filtr_rest();


if($_REQUEST["act"]=="add_new_rest") add_new_rest();
if($_REQUEST["act"]=="add_rest_form") add_rest_form();


if($_REQUEST["act"]=="edite_rest_form") edite_rest_form();
if($_REQUEST["act"]=="edite_rest") edite_rest();



if($_REQUEST["act"]=="create_report") create_report();



if($_REQUEST["act"]=="check_new_bonus") check_new_bonus();
?>