<?
if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
if(!function_exists("build_list")){
    function build_list($code, $z_index, $selected, $COLORS)
    {
        $ELS = array();
        $property_enums = CIBlockPropertyEnum::GetList(Array("SORT" => "ASC", "ID" => "ASC"), Array("IBLOCK_ID" => 105, "CODE" => $code));
        while ($enum_fields = $property_enums->Fetch()) {
            $ELS[] = array("ID" => $enum_fields["ID"], "VALUE" => $enum_fields["VALUE"]);
        }

        if ($selected == "") {
            $selected = $ELS[0]["VALUE"];
            $sid = $ELS[0]["ID"];
        }

        ?>
        <input type="hidden" name="<?= $code ?>" value="<?= $sid ?>"/>
        <div class="select select_off" id="<?= $code ?>" style="z-index: <?= $z_index ?>;">
            <div class="selected_element <?= $COLORS[0] ?>"><?= $selected ?></div>
            <div class="all_variants">
                <?$c = 0;?>
                <?foreach ($ELS as $E) {
                    ?>
                    <a href="<?= $E["ID"] ?>" rel="nofollow" class="<?= $COLORS[$c] ?>"><?= $E["VALUE"] ?></a>
                    <?
                    $c++;
                    ?>
                <?
                }?>
            </div>
        </div>
    <?
    }
}?>



<form action="#" id="reports_filter">
	<div class="param mr">
		<input type="hidden"  name="restoran_id" value="<?=$FLTR["PROPERTY_rest"]?>"/>
		<input type="hidden"  name="this_is_report" value="true"/>
		<?
		if($FLTR["PROPERTY_rest"]>0){
			$res = CIBlockElement::GetByID($FLTR["PROPERTY_rest"]);
			if($ar_res = $res->Fetch()){
  				$REST_NAME = $ar_res['NAME'];
			}
		}
		if($REST_NAME=="") $REST_NAME="Ресторан";
		?>
		<input type="text" class="pole ugolok" name="restoran" value="<?=$REST_NAME?>"  onfocus="myFocus(this);" onblur="myBlur(this);"/>
		<div class="variants"></div>
	</div>

	<div class="param mr">
		<? build_list("source", 8000, "Источник",array(),$FLTR["PROPERTY_source"]);?>
	</div>
	
	<div class="param mr">
		<? build_list("status", 7999, "Статус", array("yello","sv_zel", "zel", "red","blue","yello","yello","yello","yello","black","black","red" ,"blue", "grey"));?>
		<?//build_list("status", 7999, "Статус", array("","bld","bld","bld"),$FLTR["PROPERTY_status"]);?>
	</div>

	
	<div class="param mr">
		<? build_list("type",7000, "Тип",array(),$FLTR["PROPERTY_type"]);?>
	</div>

	<div class="param pwz mr">
		<div class="zg">с</div>
		<input type="text" name="date_s" value="<?=$FLTR[">=DATE_ACTIVE_FROM"]?>" class="pole" id="date_s"/>
	</div>
	
	<div class="param pwz mr">
		<div class="zg">по</div>
		<input type="text" name="date_po" value="<? if($FLTR["<=DATE_ACTIVE_FROM"]!="") echo date( 'd.m.Y' ,strtotime('-1 day', strtotime($FLTR["<=DATE_ACTIVE_FROM"])))?>" class="pole" id="date_po"/>
	</div>
	
	
	
	<div class="param pwz2 mr">
		<div class="zg"><b>создан</b> с</div>
		<input type="text" name="date_s2" value="<?=$FLTR[">=DATE_CREATE"]?>" class="pole" id="date_s2"/>
	</div>
	
	<div class="param pwz mr">
		<div class="zg">по</div>
		<input type="text" name="date_po2" value="<? if($FLTR["<DATE_CREATE"]!="") echo date( 'd.m.Y' ,strtotime('-1 day', strtotime($FLTR["<DATE_CREATE"])))?>" class="pole" id="date_po2"/>
	</div>
	
	
	<?
	$filter=array(
		"GROUPS_ID"=>array(16),
		"!GROUPS_ID"=>array(19,17)
	);
	$rsUsers = CUser::GetList(($by="last_name"), ($order="asc"), $filter); // выбираем пользователей
	while($U = $rsUsers->NavNext()){
		$OPERATORS[]=$U;
		if($FLTR["CREATED_BY"]==$U["ID"]){
			$SEL_OP=$U["LAST_NAME"]." ".$U["NAME"];
		}
	
	}	
	
	if($SEL_OP=="") $SEL_OP="Оператор";	
	?>
	
	<div class="param mr">
		<input type="hidden" name="operator" value="<?=$FLTR["CREATED_BY"]?>"/>
		<div class="select select_off" id="operator" style="z-index: 6998;">
		<div class="selected_element"><?=$SEL_OP?></div>
		<div class="all_variants">
			<?if($FLTR["CREATED_BY"]!=""){?><a href="" rel="nofollow">Не важно</a><?}?>
			<?
			$filter=array(
				"GROUPS_ID"=>array(16),
				"!GROUPS_ID"=>array(19,17)
			);
			$rsUsers = CUser::GetList(($by="last_name"), ($order="asc"), $filter); // выбираем пользователей
			foreach($OPERATORS as $U){
				?>
				<a href="<?=$U["ID"]?>" rel="nofollow"><?=$U["LAST_NAME"]?> <?=$U["NAME"]?></a>
				<?
			}
			?>
		</div>
		</div>
	</div>
	
	
	<div class="param but">
		<a href="#" id="make_report" >Сформировать</a>
	</div>
	
	<div class="clear"></div>
	
</form>