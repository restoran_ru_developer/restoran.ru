<form action="#" method="post" id="add_rest_form">

	<?if($_SESSION["CITY"]=="jurmala"){?>
        <div class="ln">
            <div class="zg zg2">Город</div>
            <div class="inpc">
                <select name="city4add" style="height:22px;">
                    <option value=""></option>
                    <option value="urm">Юрмала</option>
                    <option value="rga">Рига</option>
                </select>
            </div>
        </div>
    <?}?>

	<div class="ln">
		<div class="zg">Название</div>
		<div class="inpc"><input type="text" class="pole" name="name"  /></div>
	</div>

	<div class="ln">
		<div class="zg">Адрес</div>
		<div class="inpc"><input type="text" class="pole" name="address" /></div>
	</div>

	<div class="ln">
		<div class="zg">Телефон</div>
		<div class="inpc"><input type="text" class="pole" name="telephone" /></div>
	</div>
	
	<div class="ln" style="height:80px;">
		<div class="zg">Метро</div>
		<div class="inpc">
		<select name="metro[]" size="1" multiple>
		<?
		if($_SESSION["CITY"]=="spb") $IBL=89;
		if($_SESSION["CITY"]=="msk") $IBL=30;
		
		$arFilter = Array("IBLOCK_ID"=>$IBL,  "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"metro", "!PROPERTY_LAT"=>false);
		$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false);
		while($ar_fields = $res->Fetch()){
			
			?>
			<option value="<?=$ar_fields["ID"]?>"><?=$ar_fields["NAME"]?></option>
			<?
		}
		?>
		</select>
		</div>
	</div>

	<div class="ln" style="height:80px;">
		<div class="zg">Кухня</div>
		<div class="inpc">
		<select name="cook[]" size="1" multiple>
		<?
		$arFilter = Array("IBLOCK_ID"=>114,  "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false);
		while($ar_fields = $res->Fetch()){
			
			?>
			<option value="<?=$ar_fields["ID"]?>"><?=$ar_fields["NAME"]?></option>
			<?
		}
		?>
		</select>
		</div>
	</div>

	<div class="ln">
		<div class="zg">Время работы</div>
		<div class="inpc"><input type="text" class="pole" name="time" /></div>
	</div>
	
	<div class="ln">
		<div class="zg">Работаем</div>
		<div class="inpc"><input type="checkbox" name="working" value="Y" /></div>
	</div>
	
	<div class="ln" style="height:80px;">
		<div class="zg">Пометки</div>
		<div class="inpc"><textarea name="pometki" class="pole"></textarea></div>
	</div>
	
	
	<div class="ln" style="height:80px;">
		<div class="zg zg2">Внутренняя информация</div>
		<div class="inpc"><textarea name="vninfo" class="pole"></textarea></div>
	</div>

</form>
