<?

if(!CModule::IncludeModule("iblock")){
    $this->AbortResultCache();
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
}
//FirePHP::getInstance()->info($_SESSION["CITY"]);
/*
	
отчет
заказ оплачен
архив
*/

//выбираем заказы и группируем по пользователям	
$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_status"=>array(1808,1506,1810), "SECTION_CODE"=>$_SESSION["CITY"]);//отчет,заказ оплачен,архив
$arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,12,1,2015));
//if($_SESSION["CITY"]=="spb") $arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,12,22,2015));

//FirePHP::getInstance()->info($arFilter,'$arFilter');
$res = CIBlockElement::GetList(Array("CNT"=>"DESC"), $arFilter, array("PROPERTY_client"), false, array("ID"));
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    if($arFields["CNT"]>1){// || ($_SESSION["CITY"]=="spb" && $arFields["CNT"]==1)
        $CLIDs[]=$arFields["PROPERTY_CLIENT_VALUE"];
        $CL[$arFields["PROPERTY_CLIENT_VALUE"]]=$arFields["CNT"];
    }
}
//var_dump($arFilter);
if(count($CLIDs)==0) die();

//выбираем инфу по пользователям
$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$CLIDs);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, false);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProperties = $ob->GetProperties();

    $CLIENTS[$arFields["ID"]]=array(
        "ID"=>$arFields["ID"],
        "NAME"=>trim($arProperties["NAME"]["VALUE"]." ".$arProperties["SURNAME"]["VALUE"]),
        "TELEPHONE"=>$arProperties["TELEPHONE"]["VALUE"]
    );
}

//Выбираем истории выплат по пользователям
$arFilter = Array("IBLOCK_ID"=>2964, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,12,1,2015));
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, false);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProperties = $ob->GetProperties();

    $PAYS[$arProperties["client"]["VALUE"]][]=array(
        "ID"=>$arFields["ID"],
        "DATE_CREATE"=>$arFields["DATE_CREATE"],
        "SUMM"=>$arProperties["summ"]["VALUE"]
    );
    $CLIENTS[$arProperties["client"]["VALUE"]]["ALLSUMM"]+=$arProperties["summ"]["VALUE"];
}




//FirePHP::getInstance()->info($CL[1381881],'$CL');
//FirePHP::getInstance()->info($PAYS[1381881],'$PAYS');
//FirePHP::getInstance()->info($CLIENTS[1381881],'$CLIENTS');
?>

<div id='confirm'>
    <div class='header'><span>Отправка бонуса</span></div>
    <div class='message'></div>
    <div class='buttons'>
        <div class='no simplemodal-close'>Отмена</div><div class='yes'>Продолжить</div>
        <div class="clear"></div>
    </div>
</div>


<div id='modal'>
    <div class='header'><span>Отправка бонуса</span></div>
    <div class='message'></div>
    <div class='buttons'>
        <div class='no simplemodal-close'>Ok</div>
        <div class="clear"></div>
    </div>
</div>

<form id="bonus" action="#" method="post" target="_blank">
    <div class="manage">
        <a href="#" class="active">Все</a><a href="#need_pay">Нужны выплаты</a><a href="#orders2">2 заказа</a><a href="#orders3">3 заказа</a><a href="#orders4">4 заказа и более</a>
    </div>
	<?
		
	$n=0;	
	?>
    <table cellpadding="0" cellspacing="0">
        <tr class="head">
            <th class="c0"></th>
            <th class="c1">ФИО</th>
            <th class="c2">Телефон</th>
            <th class="c3">Заказов</th>
            <th class="c4">Выплаты</th>
        </tr>
        <?foreach($CL as $clid=>$cnt){?>
            <?
	        $n++;
            if($CLIENTS[$clid]["ID"]=="") continue;
            $NEED_PAY=false;
            $SUM=0;

//            $FIRST_BONUS=300;
//            $SECOND_BONUS=500;
            $FIRST_BONUS=200;
            $SECOND_BONUS=300;
            $THIRD_BONUS=500;



            $Class="cltr";
            $Class.=" orders".$cnt;
            if($cnt>4) $Class.=" orders4";


            $CITY_CODES=array(812,495,499); //  коды телефонов, которые не подходят
            $TCODE = substr($CLIENTS[$clid]["TELEPHONE"], 0, 3);

            if(($cnt>=2) && $CLIENTS[$clid]["TELEPHONE"]!="" && !in_array($TCODE, $CITY_CODES)){
                if(count($PAYS[$clid])==0){ //  если оплат не было
                    $NEED_PAY=true;
                    $SUM=$FIRST_BONUS;
                    $Class.=" need_pay";
                }
                elseif($cnt>=5 && $cnt<10 && count($PAYS[$clid])==1){
                    $NEED_PAY=true;
                    $SUM=$SECOND_BONUS;
                    $Class.=" need_pay";
                }
                else {
                    if($cnt%5==0 && $cnt>9){//   todo почему просто не >=5   || ($cnt>=5 && $cnt<=10)     ///if($cnt%5==0 || $cnt>5)
                        $v=intval($cnt/5)-1;//$SECOND_BONUS за 5 заказ
                        if($CLIENTS[$clid]["ALLSUMM"]<$v*$THIRD_BONUS+$FIRST_BONUS+$SECOND_BONUS){
                            $NEED_PAY=true;
                            $SUM=$THIRD_BONUS;
                            $Class.=" need_pay";
                        }
                    }

                }
            }

            ?>
            <tr class="<?=$Class?>" id="client<?=$CLIENTS[$clid]["ID"]?>">
                <td class="c0"><?=$n?></td>
                <td class="c1">
	                <?if($NEED_PAY){?>
						<input type="checkbox" name="clients_ids[]" value="<?=$clid?>"> 
					<?}?>
                <a href="/bs/?user_info=<?=$CLIENTS[$clid]["ID"]?>" target="_blank"><?=$CLIENTS[$clid]["NAME"]?></a></td>
                
                <td class="c2"><?=$CLIENTS[$clid]["TELEPHONE"]?></td>
                <td class="c3"><?=$cnt?></td>
                <td class="c4">
                    <?if($NEED_PAY){?><a href="#<?=$SUM?>" class="dopay">Отправить бонус</a><?}?>
                    <?if(count($PAYS[$clid])>0){?>
                        <span>Всего: <?=$CLIENTS[$clid]["ALLSUMM"]?> рублей</span>
                        <a href="#" class="show_pays">История выплат</a>
                        <div class="clear"></div>
                        <div class="pays">
                            <?foreach($PAYS[$clid] as $P){?>
                                <div class="pay" id="pid<?=$P["ID"]?>"><?=$P["DATE_CREATE"]?> - <?=$P["SUMM"]?> рублей</div>
                            <?}?>
                        </div>
                    <?}?>
                </td>
            </tr>
        <?}?>
    </table>

	<a href="print_bonus.php" target="_blank" class="print_bonus">Распечатать отмеченые</a>

	<div class="clear"></div>

</form>


<?
/*

        <?foreach($CL2 as $clid=>$CL){?>
            <?
            $NEED_PAY=false;
            $SUM=0;



            $SPEC_BONUS=100;

            $CLIENTS[$clid]["TELEPHONE"] = str_replace("_", "", $CLIENTS[$clid]["TELEPHONE"]);

            $Class="cltr";
            $Class.=" now_in_rest";


            $CITY_CODES=array(812,495,499);
            $TCODE = substr($CLIENTS[$clid]["TELEPHONE"], 0, 3);

            if($CLIENTS[$clid]["TELEPHONE"]!="" && !in_array($TCODE, $CITY_CODES) && count($PAYS[$clid])<5){
                $NEED_PAY=true;
                $SUM=$SPEC_BONUS;
                $Class.=" need_pay";
            }

            ?>
            <tr class="<?=$Class?>" id="client<?=$CLIENTS[$clid]["ID"]?>" rest_data="<?=$CL["REST"]?>" order_data="<?=$CL["ID"]?>">
                <td class="c1"><a href="/bs/?user_info=<?=$CLIENTS[$clid]["ID"]?>" target="_blank"><?=$CLIENTS[$clid]["NAME"]?></a></td>
                <td class="c2"><?=$CLIENTS[$clid]["TELEPHONE"]?></td>
                <td class="c3">1</td>
                <td class="c4">
                    <?if($NEED_PAY){?><a href="#<?=$SUM?>" class="dopay">Отправить бонус</a><?}?>
                    <?if(count($PAYS[$clid])>0){?>
                        <span>Всего: <?=$CLIENTS[$clid]["ALLSUMM"]?> рублей</span>
                        <a href="#" class="show_pays">История выплат</a>
                        <div class="clear"></div>
                        <div class="pays">
                            <?foreach($PAYS[$clid] as $P){?>
                                <div class="pay" id="pid<?=$P["ID"]?>"><?=$P["DATE_CREATE"]?> - <?=$P["SUMM"]?> рублей</div>
                            <?}?>
                        </div>
                    <?}?>
                </td>
            </tr>
        <?}?>
	
*/	
?>