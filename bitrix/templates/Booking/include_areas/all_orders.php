<?
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/orders_filter.php"),
		Array(),
		Array("MODE"=>"php")
	);


	if(!isset($_SESSION["CLIENTS_ORDER"])) {
		$_SESSION["ORDERS_ORDER"]="ACTIVE_FROM";
		$_SESSION["ORDERS_BY"] = "ASC";
	}

if(CSite::InGroup(array(36))){
    global $arrFilter;
}


if($_SESSION["ALL_ORDERS_FLTR"]) $arrFilter=$_SESSION["ALL_ORDERS_FLTR"];

//var_dump($arrFilter);
//FirePHP::getInstance()->info($arrFilter,'only orders');

?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "orders_list_all", array(
	"IBLOCK_TYPE" => "booking_service",
	"IBLOCK_ID" => "105",
	"SECTION_ID" => "",
	"SECTION_CODE" => $_SESSION["CITY"],
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $_SESSION["ORDERS_ORDER"],
	"ELEMENT_SORT_ORDER" => $_SESSION["ORDERS_BY"],
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "N",
	"SHOW_ALL_WO_SECTION" => "N",
	"PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-5,
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "guest",
		1 => "rest",
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => 'A',//
	"CACHE_TIME" => "36000002",
	"CACHE_GROUPS" => "N",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "Y",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "win_pager",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => "",
	"CBY_F"=>"Y",
    'LOOKON_USERS' => CSite::InGroup(array(36))?"Y":'N'
	),
	false
);?>