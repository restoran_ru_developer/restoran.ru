<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if(count($arResult["ITEMS"])>0){?>	
<form id="orders_list_form" method="post" name="orders_otch" target="_blank">			
<div class="tbl orders_all rep_tbl" style="height:500px;overflow:auto;">
<table cellpadding="0" cellspacing="0" width="100%">
	<tr class="headd">
		<th>ID</th>
		<th>Статус</th>
		<th>Дата</th>
		<th>Ресторан</th>
		<th>Сумма счета</th>
		<th>Сумма выплат</th>
		<th>Тип</th>
		<th>Примечание</th>
		<th><input type="checkbox" name="check_all" value="Y"/></td>
	</tr>
<?
$dn=1;
?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?
	$CLIENT_NAME="";
	$CLIENT_SURNAME="";
	$DATE="";
	$TIME="";
	
	$tar=explode(" ", $arElement["ACTIVE_FROM"]);
 	$tar2=explode(":", $tar[1]);
 	$DATE = $tar[0];
 	$TIME = $tar2[0].":".$tar2[1];
	
	if($TIME==":") $TIME="";
	
	
	//Берем инфу по клиенту
	$CLIENT_NAME="";
	$CLIENT_SURNAME="";
	$res = CIBlockElement::GetByID($arElement["PROPERTIES"]["client"]["VALUE"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
// 		$arProps = $ob->GetProperties();
        $PROPERTY_CODE_LIST = array('NAME','SURNAME');
        foreach ($PROPERTY_CODE_LIST as $prop_name) {
            $arProps[$prop_name] = $ob->GetProperty($prop_name);
        }
 		$CLIENT_NAME=$arProps["NAME"]["VALUE"];
 		$CLIENT_SURNAME=$arProps["SURNAME"]["VALUE"];
 	}
 	
 	//Берем инфу по ресторану
	if($arElement["PROPERTIES"]["rest"]["VALUE"]==0) $REST_NAME="подбор";
	else{
		$res = CIBlockElement::GetByID($arElement["PROPERTIES"]["rest"]["VALUE"]);
		if($arFields = $res->Fetch()){
 			$REST_NAME=$arFields["NAME"];
 		}
	}
	
	
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="заказ принят") $COLOR = "yello";
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="забронирован") $COLOR = "sv_zel";
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="гости пришли") $COLOR = "zel";
	
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="гости отменили заказ") $COLOR = "red";
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="ошибочный заказ") $COLOR = "black";
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="банкет в работе") $COLOR = "yello";
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="внесена предоплата") $COLOR = "yello";
	
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="отчет") $COLOR = "blue";
	if($arElement["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен") $COLOR = "blue";
	
	//var_dump($arElement["PROPERTIES"]["summ_sch"]);
	?>
	<tr id="order_<?=$arElement["ID"]?>" class="<?=$COLOR?>" >
		<td class="cr0"><?=$arElement["ID"]?></td>
		<td class="cr1"><?=$arElement["PROPERTIES"]["status"]["VALUE"]?></td>
		<td class="cr2"><?=$DATE?></td>
		<td class="cr3"><?=$REST_NAME?></td>
		<td class="cr4"><?=$arElement["PROPERTIES"]["summ_sch"]["VALUE"]?></td>
		<td class="cr5"><?=$arElement["PROPERTIES"]["procent"]["VALUE"]?></td>
		<td class="cr6"><?=$arElement["PROPERTIES"]["type"]["VALUE"]?></td>
		<td class="cr7"><input class="pole prim" name="prim" type="text" id="prim_<?=$arElement["ID"]?>" value="<?=$arElement["PROPERTIES"]["prim_oplata"]["VALUE"]?>"></td>
		<td class="cr8"><input type="checkbox" name="ids[]" value="<?=$arElement["ID"]?>"/></td>
	</tr>
	<tr class="spacer"><td colspan="9">&nbsp;</td></tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
</table>					
</div>
<?}?>
<div class="paginator">
	<a href="print_orders.php" target="_blank" class="print_orders">Распечатать</a>
	<a href="#" target="_blank" class="pay_orders">Оплатить</a>
	<a href="#" target="_blank" class="arch_orders">В архив</a>
	<a href="get_orders_xls.php" target="_blank" class="xls_orders">*.xls</a>
</div>
</form>