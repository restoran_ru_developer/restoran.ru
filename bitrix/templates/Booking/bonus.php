<?


function get_token(){
	require_once(dirname(__FILE__) . '/yam_src30/consts.php');
	require_once(dirname(__FILE__) . '/yam_src30/lib/api.php');

//	$scope = 'account-info operation-history operation-details payment.to-pattern("phone-topup")'; // this is scope of permissions
    $scope = array('account-info','operation-history','operation-details','payment.to-pattern("phone-topup")');
	$authUri = \YandexMoney\API::buildObtainTokenUrl(CLIENT_ID, REDIRECT_URI, $scope);
    FirePHP::getInstance()->info($authUri,'$authUri');
	return $authUri;
}

function get_really_token(){
//    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	require_once(dirname(__FILE__) . '/yam_src30/consts.php');
	require_once(dirname(__FILE__) . '/yam_src30/lib/api.php');


//    echo CLIENT_ID;
//    echo REDIRECT_URI;
	$really_token = \YandexMoney\API::getAccessToken(CLIENT_ID, $_REQUEST['code'], REDIRECT_URI,CLIENT_SECRET);
    FirePHP::getInstance()->info($really_token,'$really_token');
//    echo $really_token;
//	return $really_token;
}


function check_tocken(){
	$EXIT=array();
	//берем токен из файлика
	$file_array = file(dirname(__FILE__) . '/yam_src30/token');
	$token=$file_array[0];
//    $token = '';
	if($token==""){
		$EXIT["URL"]=get_token();
		$EXIT["ACT"]="redirect";
		echo json_encode($EXIT);
		die();
	}
    else
        return $token;
}


function send_bonus(){

	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	
	$res = CIBlockElement::GetByID($_REQUEST["client_id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();

		if($arFields["IBLOCK_SECTION_ID"]==83823) $CITY="spb";
		else $CITY="msk";
		
		
		$arProps["TELEPHONE"]["VALUE"] = preg_replace("/\D/", "", $arProps["TELEPHONE"]["VALUE"]);
		
		$FL=$arProps["TELEPHONE"]["VALUE"]{0};
		
		if($FL!="7") $arProps["TELEPHONE"]["VALUE"] = "7".$arProps["TELEPHONE"]["VALUE"];

        if(!preg_match('/^79/',$arProps["TELEPHONE"]["VALUE"])){
            $EXIT["STATUS"]="error";
            $EXIT["ACT"]="alert";
            $EXIT["TEXT"]='Неправильный телефон клиента: '.$arProps["TELEPHONE"]["VALUE"];
            echo json_encode($EXIT);
            die();
        }

		$token = check_tocken();
//        if($USER->IsAdmin()){
        FirePHP::getInstance()->info($token);
//        }

		require_once(dirname(__FILE__) . '/yam_src30/consts.php');
		require_once(dirname(__FILE__) . '/yam_src30/lib/api.php');
	
		
		$ym = new \YandexMoney\API($token);
//496412170705110020


//        FirePHP::getInstance()->info($ym,'$ym');


		$params["phone-number"] = $arProps["TELEPHONE"]["VALUE"];
//        $params["phone-number"] = '79523969381';
//        $_REQUEST["summ"] = 1;

        $params["amount"] = intval($_REQUEST["summ"]);
		$params["pattern_id"] = "phone-topup";
//		$params["identifier_type"] = "phone";
//        $params["test_payment"] = false;


//        if($USER->IsAdmin()){
////            $operationHistory = $ym->operationHistory();
//          $acount_info = $ym->accountInfo();
//            FirePHP::getInstance()->info($acount_info);
////
//            $EXIT["STATUS"]="error";
//            $EXIT["ACT"]="alert";
//            $EXIT["TEXT"]="Операция на тестировании!";
//            echo json_encode($EXIT);
//            die;
//        }

//        $acount_info = $ym->accountInfo();
        FirePHP::getInstance()->info($params,'$params');


		$request_payment = $ym->requestPayment($params);
        //AddMessage2Log($request_payment,'$request_payment');


        FirePHP::getInstance()->info($request_payment,'0 payment');
        $PaymentId = $request_payment->request_id;

        FirePHP::getInstance()->info($PaymentId,'$PaymentId');
        if ($request_payment->status == "success"){
            $process_payment = $ym->processPayment(array(
                "request_id" => $request_payment->request_id,
                'test_payment' => false
            ));
            FirePHP::getInstance()->info($process_payment,'$process_payment - 2');
            if ($process_payment->status == "success"){

                $PaymentId = $process_payment->payment_id;

                $rest = CIBlockElement::GetByID($_REQUEST["rest"]);
                if($arFields_rest = $rest->Fetch()){
                    $REST_NAME=$arFields_rest["NAME"];
                }

                $EXIT["STATUS"]="ok";
                $EXIT["ACT"]="alert";
                $EXIT["TEXT"]="Операция выполнена успешно!";
                $EXIT["PAYMEN_ID"] = $PaymentId;
//
                $el = new CIBlockElement;

                $PROP = array();
                $PROP["client"] = $arFields["ID"];
                $PROP["summ"] = $_REQUEST["summ"];

                $arLoadProductArray = Array(
                    "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
                    "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                    "IBLOCK_ID"      => 2964,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => $arFields["ID"]."_".$_REQUEST["summ"],
                    "ACTIVE"         => "Y",            // активен
                    "PREVIEW_TEXT"	=> $PaymentId
                );

                $BONUS_ID = $el->Add($arLoadProductArray);

                echo json_encode($EXIT);

                if($PROP["summ"]==100 && $REST_NAME!=""){
                    /*
                    Спасибо за обращение в службу брони Restoran.ru и посещение
                        ресторана "имяресторана".Вам бонус 100р на ваш телефон.Наш телефон: +78127401820"
                    */
                    $SMSTXT="Спасибо за обращение в службу брони Restoran.ru и посещение ресторана «".$REST_NAME."».";
                    $SMSTXT.="Вам бонус ".$_REQUEST["summ"]." рублей на ваш телефон. Ждите его от вашего мобильного оператора в ближайшее время.";
                    $SMSTXT.="Наш телефон: +78127401820";
                    $SMSTXT.=" www.restoran.ru";

                    $order = CIBlockElement::GetByID($_REQUEST["order"]);
                    if($arFields_order = $order->GetNext()){
                        CIBlockElement::SetPropertyValuesEx($_REQUEST["order"], false, array("bonus100" => 2448));
                    }
                    /*Спасибо за обращение в службу брони Ресторан.ру и посещение ресторана «имярек». Вам бонус 100р. Наш телефон: +78127401820*/

                }
                else {
                    /*Спасибо за обращение в службу брони Restoran.ru.Вам бонус на ваш телефон.Наш телефон:
                        +74959882656/+78127401820"*/
                    $SMSTXT="Спасибо за обращение в службу бронирования столиков Restoran.ru! Вам бонус ".$_REQUEST["summ"]." рублей на ваш телефон. Ждите его от вашего мобильного оператора в ближайшее время.";
                    $SMSTXT.="Наш телефон: ";

                    if($CITY=="spb") $SMSTXT.="+78127401820";
                    else $SMSTXT.="+74959882656";

                    $SMSTXT.=" www.restoran.ru";
                }


                CModule::IncludeModule("sozdavatel.sms");
                CSMS::Send($SMSTXT, "+".$params["phone-number"], "UTF-8");

                //  todo добавить лог отправки бонуса письмом


            }
            else{
                ///Оплата не прошла
                $EXIT["STATUS"]="error";
                $EXIT["ACT"]="alert";
                $EXIT["TEXT"]=$process_payment->error.': '.$process_payment->error_description;
                $EXIT["PAYMEN_ID"] = $PaymentId;


                $VARDUMP=array();
                $VARDUMP["params"]= var_export($params, true);
                $VARDUMP["datetime"] = date("d.m.Y H:i:s");
                $VARDUMP["payment_id"] = $EXIT["PAYMEN_ID"];
                $VARDUMP["provider_text"] = $EXIT["TEXT"];
                $EXIT["VDUMP"]=serialize($VARDUMP);

                $VARDUMP = var_export($VARDUMP, true);

                $TO_SEND=array("VARDUMP"=>$VARDUMP);
                CEvent::Send('ERROR_LOG', SITE_ID, $TO_SEND, "Y", 145);


                echo json_encode($EXIT);
                die();
            }
        }
        else {
            $EXIT["STATUS"]="error";
            $EXIT["ACT"]="alert";
            $EXIT["TEXT"]=$request_payment->error.': '.$request_payment->error_description;
            $EXIT["PAYMEN_ID"] = $PaymentId;


            $VARDUMP=array();
            $VARDUMP["params"]= var_export($params, true);
            $VARDUMP["datetime"] = date("d.m.Y H:i:s");
            $VARDUMP["payment_id"] = $EXIT["PAYMEN_ID"];
            $VARDUMP["provider_text"] = $EXIT["TEXT"];
            $EXIT["VDUMP"]=serialize($VARDUMP);

            $VARDUMP = var_export($VARDUMP, true);

            $TO_SEND=array("VARDUMP"=>$VARDUMP);
            CEvent::Send('ERROR_LOG', SITE_ID, $TO_SEND, "Y", 145);

            $EXIT["TEXT"]=$request_payment->error;

            if($EXIT["TEXT"]=="technical_error") $EXIT["TEXT"]="Ошибка: technical_error";
            elseif($EXIT["TEXT"]=="not_enough_funds") $EXIT["TEXT"]="Ошибка: Недостаточно средств";
            elseif($EXIT["TEXT"]=="limit_exceeded") $EXIT["TEXT"]="Ошибка: Превышение лимита";
            elseif($EXIT["TEXT"]=="contract_not_found") $EXIT["TEXT"]="Ошибка: Неправильный номер телефона";
            elseif($EXIT["TEXT"]=="payment_refused") $EXIT["TEXT"]="Ошибка: Абонента с таким номером не существует";
            else $EXIT["TEXT"]=$request_payment->error.': '.$request_payment->error_description;

            echo json_encode($EXIT);
            die();
        }


	}
}	
	
	
if($_REQUEST["act"]=="send_bonus") send_bonus();

if($_REQUEST["code"]) {//&&$_REQUEST['get_really_token']=='Y'
    get_really_token();
}


?>