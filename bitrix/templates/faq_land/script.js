function navigateTo(x, y)
{
    x = x * -1;
    y = y * -1;
    //alert(1);
    $("#slider-div").stop(true,false).animate(
        {
            left: x+'px',
            top: y+'px'
        },
        1000
    );
}


/**
 * Получает смещение относительно левого верхнего угла
 */
function calculateCorrection()
{
    correctionX = $("#slider-data").width() / 2;
    correctionY = $("#slider-data").height() / 2;
}


$(document).ready(function(){

    var tX, tY;

    // Получение коррекции
    calculateCorrection()

    $(".nv").click(function(){

        var id = $(this).attr('id');
        ids = id.substr(3);
        coords = ids.split('-');
        coords[0] = parseInt(coords[0]);
        coords[1] = parseInt(coords[1]);

        navigateTo(coords[0],coords[1]);

    });
});