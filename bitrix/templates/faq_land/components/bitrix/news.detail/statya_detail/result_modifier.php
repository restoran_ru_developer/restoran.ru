<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $DB;
if($arIBType = CIBlockType::GetByIDLang($arResult["IBLOCK_TYPE_ID"], LANG))   
    $arResult["IBLOCK_TYPE_NAME"] = htmlspecialcharsex($arIBType["NAME"]);
//if ($USER->IsAdmin())
//    v_dump($arResult);
// get rest iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
// get author name
$rsUser = CUser::GetByID($arResult["CREATED_BY"]);
if($arUser = $rsUser->GetNext())
    $arResult["AUTHOR_NAME"] = $arUser["NAME"];

// format date

if (!$arResult["DISPLAY_ACTIVE_FROM"]):
    $date = $DB->FormatDate($arResult["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
else:
    $arTmpDate = $arResult["DISPLAY_ACTIVE_FROM"];
endif;
$arTmpDate = explode(" ", $arTmpDate);
$arResult["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
$arResult["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

function get_file($matches)
{
    //return "http://".SITE_SERVER_NAME."".CFile::GetPath($matches[1]);
    CFile::GetPath($matches[1]);
}
function get_rest($matches)
{
    $rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            //"ACTIVE" => "Y",
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arRestIB["ID"],
            "ID" => $matches[1],
        ),
        false,
        false,
        Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL","PROPERTY_RATIO")
    );
    if($arRest = $rsRest->GetNext()) {
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"kitchen"));
        while($ar_props = $db_props->GetNext())
        {
            $kitchen["NAME"] = $ar_props["NAME"];
            $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
            $arCuisine = $rsCuisine->GetNext();
            $kitchen["VALUE"][] = $arCuisine["NAME"];
        }
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"average_bill"));
        if($ar_props = $db_props->GetNext())
        {
            $avb["NAME"] = $ar_props["NAME"];
            $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
            $arCuisine = $rsCuisine->GetNext();
            $avb["VALUE"][] = $arCuisine["NAME"];
        }
        //"PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill"
        //v_dump($arRest);
        // get rest city name
        $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
        $arRestCity = $rsRestCity->GetNext();

        // get rest picture
        if ($arRest["DETAIL_PICTURE"])
             $restPic = CFile::ResizeImageGet($arRest["DETAIL_PICTURE"], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_PROP, true);
        else
            $restPic["src"] = "/tpl/images/noname/rest_nnm.png"; 
        // get cuisine name
        $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
        $arCuisine = $rsCuisine->GetNext();

        // set rest info
        $arTmpRest["NAME"] = $arRest["NAME"];
        $arTmpRest["CITY"] = $arResult["IBLOCK"]["NAME"];
        $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
        $arTmpRest["REST_PICTURE"] = $restPic;
        $arTmpRest["URL"] = $arRest["DETAIL_PAGE_URL"];
        $arTmpRest["CUISINE"] = $kitchen;
        $arTmpRest["AVERAGE_BILL"] = $avb;
        $res = '<a href="'.$arTmpRest["URL"].'"><img class="indent" src="'.$arTmpRest["REST_PICTURE"]["src"].'" width="148" /></a><br />
                <a class="font14" href="'.$arTmpRest["URL"].'">'.$arTmpRest["NAME"].'</a>,<br />'.$arTmpRest["CITY"].'
                <p><div class="rating">';
        for($i = 0; $i < 5; $i++):
            if ($i < $arTmpRest["RATIO"]):
                $res .= '<div class="small_star_a" alt="'.$i.'"></div>';               
            else:
                $res .= '<div class="small_star" alt="'.$i.'"></div>';
            endif;
        endfor;
        $res .= '<div class="clear"></div></div>';
        $res .= '<b>'.$arTmpRest["CUISINE"]["NAME"].': </b>';
        $res .= implode(", ",$arTmpRest["CUISINE"]["VALUE"]);
        $res .= '<br /><b>'.$arTmpRest["AVERAGE_BILL"]["NAME"].':</b>';
        $res .= implode(", ",$arTmpRest["AVERAGE_BILL"]["VALUE"]);
        $res .= '<br /></p>';
        if(end($block["REST_BIND_ARRAY"]) != $rest):
            $res .= '<hr class="dotted" />';
        endif;
        $resto[] = Array("ID"=>$arRest["ID"],"NAME"=>$arRest["NAME"]);
    }
    return $res;
}

$arResult["DETAIL_TEXT"] = preg_replace_callback("/#FID_([0-9]+)#/","get_file",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = str_replace("#FID_#","/tpl/images/user.jpg",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = preg_replace_callback("/#REST_([0-9]+)#/","get_rest",$arResult["DETAIL_TEXT"]);
if (!$arResult["PROPERTIES"]["COMMENTS"]["VALUE"])
    $arResult["PROPERTIES"]["COMMENTS"]["VALUE"] = 0;
//$arResult["DETAIL_TEXT"] = preg_replace("|(#FID_[0-9]+#)|","",$arResult["DETAIL_TEXT"]);

// get rest info
foreach($arResult["ITEMS"] as $key=>$block) {
    if(is_array($block["PROPERTIES"]["REST_BIND"]["VALUE"])) {
        foreach($block["PROPERTIES"]["REST_BIND"]["VALUE"] as $restBind) {
            $rsRest = CIBlockElement::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "ID" => $restBind,
                ),
                false,
                false,
                Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_photos", "PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill")
            );
            while($arRest = $rsRest->GetNext()) {
                // get rest city name
                $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
                $arRestCity = $rsRestCity->GetNext();

                // get rest picture
                if(!$arRest["PREVIEW_PICTURE"]) {
                    $restPic = CFile::ResizeImageGet($arRest["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
                } else {
                    $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
                }

                // get cuisine name
                $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
                $arCuisine = $rsCuisine->GetNext();

                // set rest info
                $arTmpRest["NAME"] = $arRest["NAME"];
                $arTmpRest["CITY"] = $arRestCity["NAME"];
                $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
                $arTmpRest["REST_PICTURE"] = $restPic;
                if(!in_array($arCuisine["NAME"], $arTmpRest["CUISINE"]))
                    $arTmpRest["CUISINE"][] = $arCuisine["NAME"];
                $arTmpRest["AVERAGE_BILL"] = $arRest["PROPERTY_AVERAGE_BILL_VALUE"];

                $arResult["ITEMS"][$key]["REST_BIND_ARRAY"][$arRest["ID"]] = $arTmpRest;
            }
            $arTmpRest["CUISINE"] = Array();
        }
    }

    // get respondent data
    if($block["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"] && $block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]) {
        $arResult["ITEMS"][$key]["RESPONDENT"]["PHOTO"] = CFile::ResizeImageGet($block["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"], array('width' => 168, 'height' => 168), BX_RESIZE_IMAGE_EXACT, true);
        $arResult["ITEMS"][$key]["RESPONDENT"]["NAME"] = $block["PROPERTIES"]["RESPONDENT_NAME"]["VALUE"];
        $arResult["ITEMS"][$key]["RESPONDENT"]["POST"] = $block["PROPERTIES"]["RESPONDENT_POST"]["VALUE"];
        if($block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"])
            $arResult["ITEMS"][$key]["RESPONDENT"]["SPEECH"] = $block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"];
    }

    // get photos
    foreach($block["PROPERTIES"]["PICTURES"]["VALUE"] as $picKey=>$pic) {
        $photoPic = CFile::ResizeImageGet($pic, array('width' => 394, 'height' => 271), BX_RESIZE_IMAGE_EXACT, true);
        $photoPic["description"] = $block["PROPERTIES"]["PICTURES"]["DESCRIPTION"][$picKey];
        $arResult["ITEMS"][$key]["PICTURES"][] = $photoPic;
        // cnt pictures
        $arResult["ITEMS"][$key]["CNT_PICTURES"] = count($arResult["ITEMS"][$key]["PICTURES"]);
    }
    if ($block["PROPERTIES"]["VIDEO"]["VALUE"])
    {
        $arResult["ITEMS"][$key]["VIDEO"] = $block["PROPERTIES"]["VIDEO"]["VALUE"];
    }
}

// get current date
$arResult["TODAY_DATE"] = CIBlockFormatProperties::DateFormat('j F', MakeTimeStamp(date('d.m.Y'), CSite::GetDateFormat()));

$arResult["NEXT_ARTICLE"] = RestIBlock::GetArticleNext($arResult["IBLOCK_ID"],$arResult["ID"]);
$arResult["PREV_ARTICLE"] = RestIBlock::GetArticlePrev($arResult["IBLOCK_ID"],$arResult["ID"]);

foreach ($arResult["DISPLAY_PROPERTIES"] as &$properties)
{
    if (is_array($properties["DISPLAY_VALUE"]))
       $properties["DISPLAY_VALUE"] = strip_tags(implode(", ",$properties["DISPLAY_VALUE"]));
    else
        $properties["DISPLAY_VALUE"] = strip_tags($properties["DISPLAY_VALUE"]);
}
?>