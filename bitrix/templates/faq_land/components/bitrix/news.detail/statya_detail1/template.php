<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h1><?=$arResult["SECTION"]["PATH"][2]["NAME"]?></h1>
<!--<div class="rating" style="margin-bottom:10px;">
    <?/*for($i==0;$i<$arResult["RECEPT"]["RATIO"]["RATIO"];$i++):?>
        <div class="star_a"></div>
    <?endfor?>
    <?for($i;$i<5;$i++):?>
        <div class="star"></div>
    <?endfor;*/?>
</div>-->
<a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
<br /><br />
<?foreach($arResult["ITEMS"] as $key=>$block):?>
    <?
    // short text
    if($block["PREVIEW_TEXT"]):?>
        <i><?=$block["PREVIEW_TEXT"]?></i>
    <?
    // prev type for layout
    $prevType = 'shortText';
    endif?>
    <?
    // rest bind
    if(is_array($block["REST_BIND_ARRAY"])):?>
        <div class="left" style="width:165px;">
            <?foreach($block["REST_BIND_ARRAY"] as $rest):?>
                <img class="indent" src="<?=$rest["REST_PICTURE"]["src"]?>" width="148" /><br />
                    <a class="font14" href="#"><?=$rest["NAME"]?></a>,<br />
                    <?=$rest["CITY"]?>
                <p>
                    <div class="rating">
                        <?for($i = 0; $i < 5; $i++):?>
                            <div class="small_star<?if($i < $rest["RATIO"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                        <?endfor?>
                        <div class="clear"></div>
                    </div>
                    <b><?=GetMessage("CUISINE")?>:</b>
                    <?foreach($rest["CUISINE"] as $cuisine):?>
                        <?=$cuisine?><?if(end($rest["CUISINE"]) != $cuisine) echo ", ";?>
                    <?endforeach?>
                    <br />
                    <b><?=GetMessage("AVERAGE_BILL")?>:</b> <?=$rest["AVERAGE_BILL"]?><br />
                </p>
                <?if(end($block["REST_BIND_ARRAY"]) != $rest):?>
                    <hr class="dotted" />
                <?endif?>
            <?endforeach?>
        </div>
    <?
    $prevType = 'rest';
    endif?>
    <?
    //  text
    if ($block["DETAIL_PICTURE"]["SRC"]):?>
        <div class="recept_text">                  
            <?if ($block["PROPERTIES"]["SHOW_NAME"]["VALUE"]=="Да"):?>
                <h2><?=$block["NAME"]?></h2>
            <?endif;?>
            <?if ($block["PROPERTIES"]["BIG_PICTURE"]["VALUE"]!="Да"):?>
                <div class="left photos123" style="position:relative; width:238px;margin-bottom: 10px;">                            
                    <img id="pic<?=$block["PREVIEW_PICTURE"]["ID"]?>" src="<?=$block["PREVIEW_PICTURE"]["SRC"]?>" />
                    <img id="pic<?=$block["PREVIEW_PICTURE"]["ID"]?>big" style="display:none" src="<?=$block["DETAIL_PICTURE"]["SRC"]?>" />
                </div>
            <?else:?>
                <!--<div class="left" style="position:relative">
                    <?if ($block["PROPERTIES"]["QUANTITY"]["VALUE"]):?>
                        <div class="portion">
                            <div><?=$block["PROPERTIES"]["QUANTITY"]["VALUE"]?></div>
                            <?=pluralForm($block["PROPERTIES"]["QUANTITY"]["VALUE"],"порция","порции","порций")?>
                        </div>
                    <?endif;?>
                    <img src="<?=$block["DETAIL_PICTURE"]["SRC"]?>" />
                </div>-->
                <br />
            <?endif;?>
            <div style="">
                <?=$block["DETAIL_TEXT"]?>
            </div>
                <div class="clear"></div>
        </div>
        <br /><br />                                            
    <?elseif($block["DETAIL_TEXT"]):?>
        <div class="recept_text">                        
            <?if ($block["PROPERTIES"]["SHOW_NAME"]["VALUE"]=="Да"):?>
                <h2><?=$block["NAME"]?></h2>
            <?endif;?>            
            <?=$block["DETAIL_TEXT"]?>                
        </div>
        <br /><br />                                
    <?endif?>                
    <?            
    // speech
    if($block["RESPONDENT"]):?>
        <div class="bon_appetite">Приятного аппетита!</div>
        <table class="direct_speech">
            <tr>
                <td width="170">
                    <div class="author">
                        <img src="<?=$block["RESPONDENT"]["PHOTO"]["src"]?>" />
                    </div>
                </td>
                <td width="150">
                    <span class="uppercase"><?=$block["RESPONDENT"]["NAME"]?></span><br/>
                    <i><?=$block["RESPONDENT"]["POST"]?></i>
                </td>
                <td>
                    <i>«<?=$block["RESPONDENT"]["SPEECH"]?>»</i>
                </td>
            </tr>
        </table>
        <br />
    <?
    $prevType = 'speech';
    endif?>
    <?
    //  photos
    if(is_array($block["PICTURES"])):?>
        <div class="left articles_photo">
            <?if($block["CNT_PICTURES"] == 1):?>
                <div class="img">
                    <img src="<?=$block["PICTURES"][0]["src"]?>" width="715" />
                    <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                </div>
            <?else:?>
                <script type="text/javascript">
                    $(document).ready(function(){
                       $(".articles_photo").galery();
                    });
                </script>
                <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/<?=$block["CNT_PICTURES"]?><a class="next browse right"></a></div>
                <div class="clear"></div>
                <div class="img">
                    <img src="<?=$block["PICTURES"][0]["src"]?>" width="715" />
                    <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                </div>
                <div class="special_scroll">
                    <div class="scroll_container">
                        <?foreach($block["PICTURES"] as $keyPic=>$pic):?>
                            <div class="item<?if($keyPic == 0):?> active<?endif?>">
                                <img src="<?=$pic["src"]?>" alt="<?=$pic["description"]?>" align="bottom" />
                            </div>
                        <?endforeach?>
                    </div>
                </div>
            <?endif?>
        </div>
        <div class="clear"></div>
    <?
    $prevType = 'photos';
    endif?>
<?endforeach?>        