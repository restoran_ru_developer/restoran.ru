<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
$(document).ready(function(){
    $(".photos123").each(function(){
       var c = new Image;
       c.src = $(this).find("img:visible").attr("src");
       $(this).append("<div class='block'>Увеличить</div>");
       $(this).find(".block").css({"display":"none","position":"absolute","left":c.width/2-50+"px","width":"100px","height":"30px","top":c.height/2-15+"px","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
    });
    $(".photos123").hover(function(){
        var c = new Image;
        c.src = $(this).find("img:visible").attr("src");
        $(this).find(".block").css({"left":c.width/2-50+"px","top":c.height/2-15+"px"}); 
        $(this).find(".block").show();
    },
    function(){
        var c = new Image;
        c.src = $(this).find("img:visible").attr("src");
        $(this).find(".block").css({"left":c.width/2-50+"px","top":c.height/2-15+"px"});
        $(this).find(".block").hide();
    });
    $(".photos123 .block").toggle(function(){
        $(this).parent().find("img:hidden").show().prev().hide();        
        $(this).parent().removeClass('left');        
        $(this).html("Уменьшить");
        var c = new Image;
        c.src = $(this).parent().find("img:visible").attr("src");
        $(this).css({"left":c.width/2-50+"px","top":c.height/2-15+"px"}); 
    },
    function(){
        $(this).parent().find("img:hidden").show().next().hide();
        $(this).parent().addClass('left');        
        $(this).html("Увеличить");
        var c = new Image;
        c.src = $(this).parent().find("img:visible").attr("src");
        $(this).css({"left":c.width/2-50+"px","top":c.height/2-15+"px"}); 
    });    
})
</script>  

<div id="content">
        <h1><?=$arResult["SECTION"]["PATH"][2]["NAME"]?></h1>
        <a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <?
            // short text
            if($arResult["RECEPT"]["DESCRIPTION"]):?>
                <i><?=$arResult["RECEPT"]["DESCRIPTION"]?></i>
                <br /><br />
            <?
            // prev type for layout
            $prevType = 'shortText';
            endif?>
        <?foreach($arResult["ITEMS"] as $key=>$block):?>
            
            <?
            // rest bind
            if(is_array($block["REST_BIND_ARRAY"])):?>
                <div class="left" style="width:165px;">
                    <?foreach($block["REST_BIND_ARRAY"] as $rest):?>
                        <img class="indent" src="<?=$rest["REST_PICTURE"]["src"]?>" width="148" /><br />
                            <a class="font14" href="#"><?=$rest["NAME"]?></a>,<br />
                            <?=$rest["CITY"]?>
                        <p>
                            <div class="rating">
                                <?for($i = 0; $i < 5; $i++):?>
                                    <div class="small_star<?if($i < $rest["RATIO"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                                <?endfor?>
                                <div class="clear"></div>
                            </div>
                            <b><?=GetMessage("CUISINE")?>:</b>
                            <?foreach($rest["CUISINE"] as $cuisine):?>
                                <?=$cuisine?><?if(end($rest["CUISINE"]) != $cuisine) echo ", ";?>
                            <?endforeach?>
                            <br />
                            <b><?=GetMessage("AVERAGE_BILL")?>:</b> <?=$rest["AVERAGE_BILL"]?><br />
                        </p>
                        <?if(end($block["REST_BIND_ARRAY"]) != $rest):?>
                            <hr class="dotted" />
                        <?endif?>
                    <?endforeach?>
                </div>
            <?
            $prevType = 'rest';
            endif?>
            <?if($arResult["RECEPT"]["DETAIL_PICTURE"]&&$key==0):?>
                <div class="left" style="position:relative;margin-bottom:10px ">
                    <?if ($arResult["RECEPT"]["QUANTITY"]):?>
                        <div class="portion">
                            <div><?=$arResult["RECEPT"]["QUANTITY"]?></div>
                            <?=pluralForm($arResult["RECEPT"]["QUANTITY"],"порция","порции","порций")?>
                        </div>
                    <?endif;?>
                    <img src="<?=$arResult["RECEPT"]["DETAIL_PICTURE"]["src"]?>" />
                </div>
                <div class="clear"></div>
            <?endif;?>
            <?
            //  text
            if ($block["DETAIL_PICTURE"]["SRC"]):?>
                <div class="recept_text">                  
                    <?if ($block["PROPERTIES"]["SHOW_NAME"]["VALUE"]=="Да"):?>
                        <h2><?=$block["NAME"]?></h2>
                    <?endif;?>
                    <?if ($block["PROPERTIES"]["BIG_PICTURE"]["VALUE"]!="Да"):?>
                        <div class="left photos123" style="position:relative; width:238px;margin-bottom: 10px;">                            
                            <img id="pic<?=$block["PREVIEW_PICTURE"]["ID"]?>" src="<?=$block["PREVIEW_PICTURE"]["src"]?>" />
                            <img id="pic<?=$block["PREVIEW_PICTURE"]["ID"]?>big" style="display:none" src="<?=$block["DETAIL_PICTURE"]["SRC"]?>" />
                        </div>
                    <?else:?>
                        <div class="left" style="position:relative">
                            <?if ($block["PROPERTIES"]["QUANTITY"]["VALUE"]):?>
                                <div class="portion">
                                    <div><?=$block["PROPERTIES"]["QUANTITY"]["VALUE"]?></div>
                                    <?=pluralForm($block["PROPERTIES"]["QUANTITY"]["VALUE"],"порция","порции","порций")?>
                                </div>
                            <?endif;?>
                            <img src="<?=$block["DETAIL_PICTURE"]["SRC"]?>" />
                        </div>
                    <?endif;?>
                    <div style="">
                        <?=$block["DETAIL_TEXT"]?>
                    </div>
                        <div class="clear"></div>
                </div>
                <br /><br />                                            
            <?elseif($block["DETAIL_TEXT"]):?>
                <div class="recept_text">    
                    <?if ($block["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Визуальный редактор"):?>
                        <h2><?=$block["NAME"]?></h2>
                    <?endif;?>            
                    <?=$block["DETAIL_TEXT"]?>                
                </div>
                <br /><br />                                
            <?endif?>                
            <?            
            // speech
            if($block["RESPONDENT"]):?>
                <div class="bon_appetite">Приятного аппетита!</div>
                <table class="direct_speech">
                    <tr>
                        <td width="170">
                            <div class="author">
                                <?if (!$block["RESPONDENT"]["PHOTO"]["src"]):?>
                                    <img src="/tpl/images/user.jpg" />
                                <?else:?>
                                    <img src="<?=$block["RESPONDENT"]["PHOTO"]["src"]?>" />
                                <?endif;?>
                            </div>
                        </td>
                        <td width="150">
                            <span class="uppercase"><?=$block["RESPONDENT"]["NAME"]?></span><br/>
                            <i><?=$block["RESPONDENT"]["POST"]?></i>
                        </td>
                        <td>
                            <i>«<?=$block["RESPONDENT"]["SPEECH"]?>»</i>
                        </td>
                    </tr>
                </table>
                <br />
            <?
            $prevType = 'speech';
            endif?>
            <?
            //  photos
            if(is_array($block["PICTURES"])):?>
                <div class="left articles_photo">
                    <?if($block["CNT_PICTURES"] == 1):?>
                        <div class="img">
                            <img src="<?=$block["PICTURES"][0]["src"]?>" width="715" />
                            <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                        </div>
                    <?else:?>
                        <script type="text/javascript">
                            $(document).ready(function(){
                               $(".articles_photo").galery();
                            });
                        </script>
                        <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/<?=$block["CNT_PICTURES"]?><a class="next browse right"></a></div>
                        <div class="clear"></div>
                        <div class="img">
                            <img src="<?=$block["PICTURES"][0]["src"]?>" width="715" />
                            <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                        </div>
                        <div class="special_scroll">
                            <div class="scroll_container">
                                <?foreach($block["PICTURES"] as $keyPic=>$pic):?>
                                    <div class="item<?if($keyPic == 0):?> active<?endif?>">
                                        <img src="<?=$pic["src"]?>" alt="<?=$pic["description"]?>" align="bottom" />
                                    </div>
                                <?endforeach?>
                            </div>
                        </div>
                    <?endif?>
                </div>
                <div class="clear"></div>
            <?
            $prevType = 'photos';
            endif?>
        <?endforeach?>
        <?
            $arrTags = explode(',', $arResult["TAGS"]);
            $count = count($arrTags);
            $i = 0;
            $t="";
            foreach($arrTags as $value):
               $i++;
               $value = trim($value);
               $t .= '<a class="another" href="/search/?tags='.str_replace(' ', '+', $value).'&search_in=recipe">'.$value.'</a>';
               if($i != $count) $t .= ', ';
            endforeach;
        ?>
        <p><?=GetMessage("TAGS_TITLE")?>: <?=$t?></p>
    <div class="clear"></div>
</div>