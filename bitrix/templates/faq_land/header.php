<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js')?>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.7.2.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui-1.8.21.custom.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.scrollview.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/script.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){

			$("#slider-data").scrollview();
		
		});
	</script>
</head>
<body>

    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="wrapper">
        