<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?=$APPLICATION->ShowTitle(false);?></title>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js')?>    
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
    <?$APPLICATION->AddHeadScript('/tpl/js/detail_galery.js')?>
</head>
<body id="page">    
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <?if ($APPLICATION->GetCurPage()!="/gastronight/index.php"&&$APPLICATION->GetCurPage()!="/gastronight/"):?>
        <div class="bg"></div>    
        <div class="container">
            <div class="left leftside">
                <div class="gn">
                    <a href="/gastronight/"><img src="<?=SITE_TEMPLATE_PATH?>/images/g_gn.png" /></a>
                </div>
                <div class="log">
                    <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/g_res.png" /></a>
                </div>     
                <div class="menu menu_press">
                    <a href="/gastronight/press.php"><img width="110" src="images/g_press.png" alt="Пресс-релиз" title="Пресс-релиз" /></a>
                </div>
                <div class="menu menu_rest">
                    <a href="/gastronight/rest.php"><img width="143" src="images/g_rest.png" alt="Рестораны" title="Рестораны" /></a>
                </div>
                <div class="menu menu_ex">
                    <a href="#"><img width="99" src="images/g_ex.png" alt="Эксперты" title="Эксперты" /></a>
                </div>
                <div class="menu menu_inst">
                    <a href="/gastronight/instagram.php"><img width="118" src="images/g_inst.png" alt="Instagram конкурс" title="Instagram конкурс" /></a>
                </div>
                <div class="menu menu_photo">
                    <a href="/gastronight/photoreports.php"><img width="97" src="images/g_photo.png" alt="Фотоотчеты" title="Фотоотчеты" /></a>        
                </div>
            </div>
            <div class="left rightside">
                <h1><?=$APPLICATION->ShowTitle(false)?></h1>
    <?endif;?>
        