<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>                                                                                    
                        <div class="new_restoraunt left<? if ($key % $del == $ost): ?> end<? endif ?>">                                                                  
                            <a href="/gastronight/photoreports.php?ID=<?=$arItem["ID"]?>"><img src="<?= $arItem["PREVIEW_PICTURE"] ?>" width="232" /></a>
                            <div class="title">
                                <a href="/gastronight/photoreports.php?ID=<?=$arItem["ID"]?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a>
                            </div>
                            <p><?= $arItem["PREVIEW_TEXT"] ?></p>                                                                    
                        </div>                           
    <? endforeach; ?>    
<? else: ?>    
    <p><font class="notetext"><?= GetMessage("NOTHING_TO_FOUND") ?></font></p>    
<? endif; ?>
<div class="clear"></div>        