<?
$MESS ['BSF_T_SEARCH_BUTTON'] = "НАЙТИ";
$MESS ['BSF_T_WHERE_SEARCH_REST'] = "on restaurants";
$MESS ['BSF_T_WHERE_SEARCH_NEWS'] = "on articles";
$MESS ['BSF_T_WHERE_SEARCH_KUPONS'] = "on sales";
$MESS ['BSF_T_WHERE_SEARCH_AFISHA'] = "on billboard";
$MESS ['BSF_T_WHERE_SEARCH_RECIPE'] = "on recipes";
$MESS ['BSF_T_WHERE_SEARCH_BLOG'] = "on blogs";
$MESS ['BSF_T_WHERE_SEARCH_OVERVIEWS'] = "on overviews";
$MESS ['BSF_T_WHERE_SEARCH_INTERVIEWS'] = "on interview";
$MESS ['BSF_T_WHERE_SEARCH_REVIEWS'] = "on opinions";
$MESS ['BSF_T_WHERE_SEARCH_DOSTAVKA'] = "on delivery";
$MESS ['BSF_T_WHERE_SEARCH_MASTER_CLASS'] = "on master-class";
$MESS ['BSF_T_WHERE_ALL'] = "on all site";
$MESS ['BSF_T_SEARCH_EXMP_STRING'] = "Example, restaurant Moscow";
$MESS ['BSF_T_SEARCH_EXMP_STRING_spb'] = "Example, restaurant Sky Lounge";
$MESS ['CUISINE_TITLE'] = "Кухня";
$MESS ['AVERAGE_BILL_TITLE'] = "Средний счет";
$MESS ['SUBWAY_TITLE'] = "Метро";
$MESS ['REST_TYPE_TITLE'] = "TYPE";
$MESS ['CHOOSE_BUTTON'] = "CHOOSE";
$MESS ['NEAR_YOU'] = "Restaurants near you";
$MESS ['SA_FIND'] = "find";
?>