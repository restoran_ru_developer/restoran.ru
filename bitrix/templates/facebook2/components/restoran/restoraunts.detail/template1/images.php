<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ($_REQUEST["PHOTOS"]):
    $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'medium',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
    );
    $p = 0;
    foreach($_REQUEST["PHOTOS"] as $key=>$photo)
    {
        $photos[] = CFile::ResizeImageGet($photo,Array("width"=>$file["WIDTH"],"height"=>$file["HEIGHT"]),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
    }
?>
    <script>
                                    function change_big_modal()
                                    {
                                            $(".big_modal img").fadeOut(300,function(){
                                               var img = new Image();                                               
                                                img.src = $("#photogalery").find(".img").find("img").attr("src");                                            
                                                img.onload = function(){
                                                    $(".big_modal img").attr("src",img.src);
                                                    showOverflow();
                                                    //$(".big_modal img").css({"width":"0px","height":"0px"});
                                                    $(".big_modal img").animate({"width":+img.width,"height":+img.height},300,function(){
                                                        $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))},300);
                                                    });                                            
                                                    $(".big_modal img").fadeIn(300); 
                                                }
                                            });                                            

                                    }
                                    $(document).ready(function(){
                                        $("#photogalery").galery({});
                                        $("body").append("<div class=big_modal id=detail_galery><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
                                        setCenter($(".big_modal"));
                                        $(".slider_left").on("click",function(){
                                           $("#photogalery").find(".scroll_arrows").find(".prev").click();
                                           setTimeout("change_big_modal()",300);
                                        });
                                        $(".slider_right").on("click",function(){
                                           $("#photogalery").find(".scroll_arrows").find(".next").click();                                           
                                           setTimeout("change_big_modal()",300);
                                        });
                                        $("#detail_galery").hover(function(){
                                            $("#detail_galery .slider_left").fadeIn(300);
                                            $("#detail_galery .slider_right").fadeIn(300);
                                        },function(){
                                            $("#detail_galery .slider_left").fadeOut(300);
                                            $("#detail_galery .slider_right").fadeOut(300);
                                        });
                                        $("#photogalery").find(".img").on("click","img",function(){                                            
                                            var img = new Image();
                                            img.src = $(this).attr("src");
                                            img.onload = function(){
                                                $("#detail_galery img").attr("src",img.src);
                                                showOverflow();
                                                //$("#detail_galery img").css({"width":"0px","height":"0px"});
                                                $("#detail_galery img").animate({"width":+img.width,"height":+img.height},300);
                                                $("#detail_galery").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))},300);
                                                $("#detail_galery").fadeIn(300);
                                            }
                                        });
                                        $("#system_overflow").click(function(){                                            
                                            $("#detail_galery").css("display","none");
                                            //setCenter($(".big_modal"));
                                            //$("#detail_galery img").css({"width":"0px","height":"0px"});
                                            $("#detail_galery").css("width","0px");
                                            $("#detail_galery").css("height","0px");
                                        });
                                    });
                                </script>                                    
                                    <div class="img img4">
                                        <img src="<?=$photos[0]["src"]?>" height="264" />                                        
                                    </div>
                                    <div class="special_scroll">   
                                        <div class="scroll_container">
                                          <?foreach($photos as $key=>$photo):?>
                                              <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                  <img src="<?=$photo["src"]?>" align="bottom" width="75" />
                                              </div>                                          
                                          <?endforeach;?>
                                        </div>
                                    </div>
                                    <div class="left scroll_arrows">
                                        <a class="prev browse left"></a>
                                        <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($photos)?></div>
                                        <a class="next browse right"></a>
                                    </div>
                                    <div class="right" style="width:200px; padding-top:20px;" align="right">
                                        <?if($_REQUEST["video"]):?>
                                        <!--<a href="#" class="another"><?=GetMessage("R_3D")?></a>&nbsp;&nbsp;&nbsp;-->
                                            Фотогалерея&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery2').show();$('#photogalery').hide()" class="another">Видеопрезентации</a>
                                        <?endif;?>
                                    </div> 
<?
endif;
?>
