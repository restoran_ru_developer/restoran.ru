<?
$watermark = Array(
        Array( 
            'name' => 'watermark',
            'position' => 'br',
            'size'=>'medium',
            'type'=>'image',
            'alpha_level'=>'40',
            'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
);
$p = 0;
foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
{           
        $s = CFile::ResizeImageGet($photo,Array("width"=>800,"height"=>600),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true,$watermark);
        $arResult["PHOTOS"][$p] = $s["src"];
        $p++; 
}
?>