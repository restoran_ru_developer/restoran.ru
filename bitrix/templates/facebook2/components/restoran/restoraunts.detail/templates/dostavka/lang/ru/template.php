<?
$MESS["R_OPENING_HOURS"] = "Часы работы";
$MESS["R_PHONE"] = "Телефон";
$MESS["R_MAIN_CONTACT"] = "Контактные данные";
$MESS["R_MAIN_CONTACT_PHONE"] = "Телефон главного офиса";
$MESS["R_ADD2FAVORITES"] = "В избранное";
$MESS["R_MORE_PROPERTIES_1"] = "Развернуть характеристики";
$MESS["R_MORE_PROPERTIES_2"] = "Свернуть характеристики";
$MESS["R_MORE_TEXT_1"] = "Развернуть описание";
$MESS["R_MORE_TEXT_2"] = "Свернуть описание";
$MESS["IN_MENU"] = "В меню";
$MESS["R_3D"] = "3D галерея";
$MESS["R_VIDEOPRESENTATION"] = "Видеопрезентации";
$MESS["R_NEWS"] = "Новости";
$MESS["R_VIDEONEWS"] = "Видеоновости";
$MESS["R_PHOTOREVIEWS"] = "Фотоотчеты";
$MESS["R_KUPONS"] = "Акции и скидки";
$MESS["R_AFISHA"] = "Афиша";
$MESS["R_REVIEWS"] = "Отзывы";
$MESS["R_BLOG"] = "Блог ресторатора";
$MESS["R_PARTNERS"] = "Партнеры";
$MESS["R_SIMILAR"] = "Похожие рестораны";
$MESS["R_PHOTO"] = "Фотогалерея";
$MESS["IN_MENU"] = "Меню доставки";
?>