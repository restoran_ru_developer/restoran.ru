<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek", "kuhnyadostavki"
);

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
        }
    }
}
$res = array();
$arNewsIB = getArIblock("news", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
$arResult["NEWS_COUNT"] = $res->SelectedRowsCount();

$res = array();
$arVideonewsIB = getArIblock("videonews", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","IBLOCK_ID"=>$arVideonewsIB["ID"]),false,Array("nTopCount"=>1));
$arResult["VIDEONEWS_COUNT"] = $res->SelectedRowsCount();

$res = array();
$arPhotoIB = getArIblock("photoreports", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
$arResult["PHOTO_COUNT"] = $res->SelectedRowsCount();

$res = array();
$arKuponIB = getArIblock("kupons", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","IBLOCK_ID"=>$arKuponIB["ID"]),false,Array("nTopCount"=>1));
$arResult["KUPONS_COUNT"] = $res->SelectedRowsCount();

$res = array();
$arAfishaIB = getArIblock("afisha", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_FROM"=>date('d.m.Y'),"IBLOCK_ID"=>$arAfishaIB["ID"]),false,Array("nTopCount"=>1));
$arResult["AFISHA_COUNT"] = $res->SelectedRowsCount();

if ($arResult["PROPERTIES"]["user_bind"]["VALUE"])
{
    $res = array();
    $arBlogsIB = getArIblock("blogs", CITY_ID);
    $res = CIBlockElement::GetList(Array(),Array("CREATED_BY"=>$arResult["PROPERTIES"]["user_bind"]["VALUE"][0],"IBLOCK_ID"=>$arBlogsIB["ID"]));
    $arResult["BLOGS_COUNT"] = $res->SelectedRowsCount();
}
// get preview text
$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut(strip_tags($arResult["DETAIL_TEXT"]), 250);
$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>(int)$arResult["ID"]);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
if($ar_result = $db_list->GetNext())
{
    $arResult["MENU_ID"] = $ar_result['ID'];
}
$p =0;
$watermark = Array(
    Array( 'name' => 'watermark',
      'position' => 'br',
      'size'=>'real',
      'type'=>'image',
      'alpha_level'=>'30',
      'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
      ),
);
foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
{
    $file = array();
    $file = CFile::GetFileArray($photo);
    if ($file["WIDTH"]>600||$file["HEIGHT"]>600)
    {       
        $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($file["ID"],Array("width"=>$file["WIDTH"],"height"=>$file["HEIGHT"]),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
        $p++;
    }
}
?>