<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.min.js')?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.validator.js')?>
<?
global $element_id;
$element_id = $arResult["ID"];
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arReviewsIB = getArIblock("reviews", CITY_ID);
?>
<div class="news-detail">    
         <div class="catalog_detail">
                <div class="left detail" style="position:relative">                    
                    <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
                        <h1><?=$arResult["NAME"]?></h1>
                        <div class="rating left" style="margin-left:30px; padding-top:20px;">
                            <?$ratio = ceil($arResult["DISPLAY_PROPERTIES"]["ratio"]["VALUE"]);?>
                            <?for ($i=1;$i<=$ratio;$i++):?>
                                <div class="star_a" alt="<?=$i?>"></div>
                            <?endfor;?>
                            <?for ($i=($ratio+1);$i<=5;$i++):?>
                                <div class="star" alt="<?=$i?>"></div>
                            <?endfor;?>
                        </div>
                        <?
                          $params = array();
                          $params["id"] = $arResult["ID"];
                          $params = addslashes(json_encode($params));            
                        ?>
                        <div align="right" class="right" style="padding-top:20px;">
                            <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>                            
                        </div>      
                        <?if(CSite::InGroup( array(14,15,1))):?>  
                            <div id="edit_link" style="position:absolute; top:0px; right:10px;"><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>
                        <?endif;?>
                        <div class="clear"></div>
                    <?endif;?>                    
                    <div class="left" style="width:315px">
                        <div class="img">
                            <!--<img src="<?=CFile::GetPath($arResult["PROPERTIES"]["photos"]["VALUE"][0])?>" width="397" />-->
                            <?if ($arResult["PREVIEW_PICTURE"]["SRC"]):?>
                                <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="315" />
                            <?else:?>
                                <img src="/tpl/images/noname/rest_nnm.png" width="315" />
                            <?endif;?>
                        </div>
                        <br /><br />
                        <div class="preview_text">
                            <?=$arResult["PREVIEW_TEXT"]?>
                        </div>
                    </div>
                    <div class="right" style="width:390px;">                         
                        <table class="properties" cellpadding="5" cellspacing="0">
                            <?$i=0;?>
                            <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                                <?if ($i <= 5 && $pid!="map" && $pid!="ratio"):?>
                                    <tr>
                                        <td width="170" valign="top">
                                            <div class="dotted" style="height:12px; margin:0px;">
                                                <span class="name"><?=$arProperty["NAME"]?>:&nbsp; </span>
                                            </div>
                                        </td>
                                        <td>
                                            <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                                <?=implode(", ",$arProperty["DISPLAY_VALUE"])?>
                                            <?else:?>
                                                <?=$arProperty["DISPLAY_VALUE"];?>
                                            <?endif?>
                                        </td>
                                    </tr>                                   
                                <?endif;?>
                                <?$i++;?>
                            <?endforeach;?>
                        </table>                                                  
                        <br /><br />
                        <div align="left">
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:asd.share.buttons",
                                        "likes2",
                                        Array(
                                                "ASD_TITLE" => $_REQUEST["title"],
                                                "ASD_URL" => $_REQUEST["url"],
                                                "ASD_PICTURE" => $_REQUEST["picture"],
                                                "ASD_TEXT" => $_REQUEST["text"],
                                                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE",1=>"VK_LIKE",2=>"TWITTER",),
                                                "LIKE_TYPE" => "LIKE",
                                                "VK_API_ID" => "2881483",
                                                "VK_LIKE_VIEW" => "mini",
                                                "SCRIPT_IN_HEAD" => "N"
                                        )
                                );?>
                            <div class="clear"></div>
                        </div>                        
                    </div>
                    <div class="clear"></div>                     
                </div>
                <div class="right">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc_right_banner",
                                    "AREA_FILE_RECURSIVE" => "Y",
                                    "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    <div class="clear"></div>                       
                </div>
                <div class="clear"></div> 
            </div>
            <br /><br />
            <?if($arResult["MENU_ID"]):?>
            <div class="catalog_detail">
                <h2><?=GetMessage("IN_MENU")?></h2>                
                <div id="menu_dostavka"></div>
                <script>
                    $(document).ready(function(){
                       $("#menu_dostavka").load("<?=$templateFolder."/menu.php?SECTION_ID=".$arResult["MENU_ID"]."&CATALOG_ID=dostavka&PARENT_SECTION_ID=".$arResult["MENU_ID"]?>&<?=bitrix_sessid_get()?>"); 
                    });
                </script>                
            </div>
            <br /><br />           
            <?endif;?>
            <div class="catalog_detail">
                <?if ($arResult["NEWS_COUNT"]||$arResult["VIDEONEWS_COUNT"]||$arResult["PHOTO_COUNT"]||$arResult["KUPONS_COUNT"]):?>
                <div id="tabs_block6" class="tabs">
                    <ul class="tabs big" ajax="ajax">
                            <?if ($arResult["NEWS_COUNT"]>0):?>
                            <li>                                
                                <a href="<?=$templateFolder."/news.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_NEWS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                            <?endif;?>
                            <?if ($arResult["VIDEONEWS_COUNT"]>0):?>
                            <li><a href="<?=$templateFolder."/videonews.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_VIDEONEWS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>                                    
                                </a>
                            </li>
                            <?endif;?>
                            <?if ($arResult["PHOTO_COUNT"]>0):?>
                            <li><a href="<?=$templateFolder."/photoreviews.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_PHOTOREVIEWS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>                                    
                                </a>
                            </li>
                            <?endif;?>
                            <?if ($arResult["KUPONS_COUNT"]>0):?>
                            <li><a href="<?=$templateFolder."/kupons.php?id=".$arResult["ID"]."&".bitrix_sessid_get()?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_KUPONS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>                                    
                                </a>
                            </li>
                            <?endif;?>
                    </ul>
                    <div class="panes">
                        <?if ($arResult["NEWS_COUNT"]>0):?>
                       <div class="pane big" style="display:block"></div>
                       <?endif;?>
                       <?if ($arResult["VIDEONEWS_COUNT"]>0):?>
                        <div class="pane big"></div>
                        <?endif;?>
                        <?if ($arResult["PHOTO_COUNT"]>0):?>
                        <div class="pane big"></div>
                        <?endif;?>
                         <?if ($arResult["KUPONS_COUNT"]>0):?>
                        <div class="pane big"></div>
                        <?endif;?>                        
                    </div>
                </div>
                <?endif;?>
                <div class="left">
                    <?if ($arResult["AFISHA_COUNT"]):?>
                    <div id="tabs_block4">
                            <ul class="tabs">
                                <li>                                
                                    <a href="#">
                                        <div class="left tab_left"></div>
                                        <div class="left name"><?=GetMessage("R_AFISHA")?></div>
                                        <div class="left tab_right"></div>
                                        <div class="clear"></div>
                                    </a>                                                                
                                </li>
                            </ul>
                            <div class="panes">
                               <div class="pane" style="display:block">
                                    <?
                                   global $arAfishaFilter;
                                   $arAfishaFilter["PROPERTY_RESTORAN"] = $arResult["ID"]?>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:news.list",
                                        "afisha_list_main_rest",
                                        Array(
                                                "DISPLAY_DATE" => "N",
                                                "DISPLAY_NAME" => "Y",
                                                "DISPLAY_PICTURE" => "Y",
                                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                                "AJAX_MODE" => "N",
                                                "IBLOCK_TYPE" => "afisha",
                                                "IBLOCK_ID" => $arAfishaIB["ID"],
                                                "NEWS_COUNT" => "5",
                                                "SORT_BY1" => "ACTIVE_FROM",
                                                "SORT_ORDER1" => "DESC",
                                                "SORT_BY2" => "SORT",
                                                "SORT_ORDER2" => "ASC",
                                                "FILTER_NAME" => "arAfishaFilter",
                                                "FIELD_CODE" => array("DETAIL_PICTURE"),
                                                "PROPERTY_CODE" => array("EVENT_TYPE","TIME"),
                                                "CHECK_DATES" => "N",
                                                "DETAIL_URL" => "",
                                                "PREVIEW_TRUNCATE_LEN" => "200",
                                                "ACTIVE_DATE_FORMAT" => "j M Y",
                                                "SET_TITLE" => "N",
                                                "SET_STATUS_404" => "N",
                                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                "ADD_SECTIONS_CHAIN" => "N",
                                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                "PARENT_SECTION" => "",
                                                "PARENT_SECTION_CODE" => "",
                                                "CACHE_TYPE" => "A",
                                                "CACHE_TIME" => "36000000",
                                                "CACHE_FILTER" => "N",
                                                "CACHE_GROUPS" => "Y",
                                                "DISPLAY_TOP_PAGER" => "N",
                                                "DISPLAY_BOTTOM_PAGER" => "N",
                                                "PAGER_TITLE" => "Новости",
                                                "PAGER_SHOW_ALWAYS" => "N",
                                                "PAGER_TEMPLATE" => "",
                                                "PAGER_DESC_NUMBERING" => "N",
                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                "PAGER_SHOW_ALL" => "N",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N"
                                        ),
                                       false
                                    );?>                                  
                               </div>
                            </div>
                    </div>
                    <?endif;?>
                    <div class="left" id="tabs_block7">
                       <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <ul class="tabs" ajax="ajax">
                            <li>                                
                                <a href="<?=$templateFolder."/reviews.php?id=".$arResult["ID"]?>&url=<?=$url?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_REVIEWS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                            <li>
                                <a href="#">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_VK")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_FACEBOOK")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>
                            </li>
                        </ul>
                        <div class="panes">
                           <div class="pane" style="display:block"></div>
                           <div class="pane" style="display:block">
                            <!-- Put this script tag to the <head> of your page -->
                                <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

                                <script type="text/javascript">
                                VK.init({apiId: 2881483, onlyWidgets: true});
                                </script>

                                <!-- Put this div tag to the place, where the Comments block will be -->
                                <div id="vk_comments"></div>
                                <script type="text/javascript">
                                VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                                </script>
                            </div>
                            <div class="pane">
                                    <div class="fb-comments" data-href="http://www.restoran.ru<?=$APPLICATION->GetCurPage()?>" data-num-posts="20" data-width="728"></div>
                            </div>
                        </div>
                    </div>
                    <br /><br />
                    <?if ($arResult["BLOGS_COUNT"]>0):?>
                    <div class="left" id="tabs_block8">
                        <ul class="tabs" ajax="ajax">
                            <li>                                
                                <a href="<?=$templateFolder."/blog.php?user=".$arResult["PROPERTIES"]["user_bind"]["VALUE"][0]?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_BLOG")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                        </ul>
                        <div class="panes">
                           <div class="pane"></div>
                        </div>                        
                    </div>
                    <?endif;?>                    
                    <br /><br />
                    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_content_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                    <br /><br />
                </div>
                <div class="right">                                        
                    <?
                    if (is_array($arResult["PROPERTIES"]["similar_rest"]["VALUE"]))
                        {?>
                           <div class="top_block">
                                <?=GetMessage("R_SIMILAR")?>
                           </div>
                           <? global $arSimilarFilter;
                            $arSimilarFilter = Array("ID"=>$arResult["PROPERTIES"]["similar_rest"]["VALUE"]);
                            $APPLICATION->IncludeComponent("restoran:restoraunts.list", "similar_one", array(
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $_REQUEST["CITY_ID"],
                                    "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                                    "NEWS_COUNT" => "3",
                                    "SORT_BY1" => "rand",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arSimilarFilter",
                                    "PROPERTY_CODE" => $arParams["SIMILAR_PROPERTIES"],
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_SHADOW" => "Y",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "Y",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Рестораны",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => ""
                                    ),
                                    false
                            );
                        } ?>                 
                </div>
                <div class="clear"></div>
                <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
                <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                <br /><br />
            </div>
            