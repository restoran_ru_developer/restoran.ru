<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ($_REQUEST["page"])
    $_REQUEST["PAGEN_1"] = (int)$_REQUEST["page"];
?>
<?
if ($_REQUEST["PARENT_SECTION_ID"] && check_bitrix_sessid())
{ 
    
    $APPLICATION->IncludeComponent(
            "bitrix:catalog",
            "dostavka",
            Array(
                    "AJAX_MODE" => "Y",
                    "SEF_MODE" => "N",
                    "IBLOCK_TYPE" => "menu",
                    "IBLOCK_ID" => "151",
                    "USE_FILTER" => "N",
                    "USE_REVIEW" => "N",
                    "USE_COMPARE" => "N",
                    "SHOW_TOP_ELEMENTS" => "Y",
                    "PAGE_ELEMENT_COUNT" => "12",
                    "LINE_ELEMENT_COUNT" => "3",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "LIST_PROPERTY_CODE" => array("map"),
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LIST_META_KEYWORDS" => "-",
                    "LIST_META_DESCRIPTION" => "-",
                    "LIST_BROWSER_TITLE" => "-",
                    "DETAIL_PROPERTY_CODE" => array("map"),
                    "DETAIL_META_KEYWORDS" => "map",
                    "DETAIL_META_DESCRIPTION" => "map",
                    "DETAIL_BROWSER_TITLE" => "map",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "PRICE_CODE" => array("BASE"),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "LINK_IBLOCK_TYPE" => "",
                    "LINK_IBLOCK_ID" => "",
                    "LINK_PROPERTY_SID" => "",
                    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                    "USE_ALSO_BUY" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "TOP_ELEMENT_COUNT" => "9",
                    "TOP_LINE_ELEMENT_COUNT" => "3",
                    "TOP_ELEMENT_SORT_FIELD" => "sort",
                    "TOP_ELEMENT_SORT_ORDER" => "asc",
                    "TOP_PROPERTY_CODE" => array("map"),
                    "VARIABLE_ALIASES" => Array(
                            "SECTION_ID" => "SECTION_ID",
                            "ELEMENT_ID" => "ELEMENT_ID"
                    ),
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "Y",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
            ),
    false
    );
}
?>