<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek"
);

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
        }
    }
}

// get preview text
$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut(strip_tags($arResult["DETAIL_TEXT"]), 250);
$watermark = Array(
    Array( 'name' => 'watermark',
      'position' => 'br',
      'size'=>'small',
      'type'=>'image',
      'alpha_level'=>'30',
      'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
      ),
);
if ($arResult["DETAIL_PICTURE"]["ID"])
    $arResult["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"],Array("width"=>395,"height"=>298),BX_RESIZE_IMAGE_EXACT,true,$watermark);
else
    $arResult["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["PREVIEW_PICTURE"]["ID"],Array("width"=>395,"height"=>298),BX_RESIZE_IMAGE_EXACT,true,$watermark);
$APPLICATION;
$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]["SRC"];
	$cp->arResult['TEXT'] = strip_tags($arResult["PREVIEW_TEXT"]);
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
        $cp->SetResultCacheKeys(array('IMAGE','TEXT'));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
	if (!isset($arResult['IMAGE']))
	{
		$arResult['IMAGE'] = $cp->arResult['IMAGE'];
		$arResult['TEXT'] = $cp->arResult['TEXT'];
	}
}
if ($arResult["PROPERTIES"]["d_tours"]["VALUE"][0])
{
    $arTourIB = getArIblock("system_lookon", CITY_ID);
    foreach($arResult["PROPERTIES"]["d_tours"]["VALUE"] as $key=>$dtour_id)
    {
        $rrr = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arTourIB["ID"],"ID"=>$dtour_id,"ACTIVE"=>"Y"),false,false,Array("ID","NAME","PREVIEW_PICTURE","DETAIL_PICTURE","PROPERTY_tour","PROPERTY_widgetPath"));
        if ($rr = $rrr->Fetch())
        {            
            $lookon_tour = str_replace("index.html", "start.swf", $rr["PROPERTY_WIDGETPATH_VALUE"]);
            $lookon_xml = str_replace("index.html", "start.xml", $rr["PROPERTY_WIDGETPATH_VALUE"]); 
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key] = $lookon_tour;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][$key] = $lookon_xml;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][$key] = $rr["PROPERTY_TOUR_VALUE"];
            $arResult["PROPERTIES"]["d_tour"]["VALUE"][$key] = $rr["PREVIEW_PICTURE"];            
        }
    }
}
?>