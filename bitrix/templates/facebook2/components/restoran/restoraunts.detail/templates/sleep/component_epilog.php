<?php
$count = count($_COOKIE["RECENTLY_VIEWED"]["catalog"]);
if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["catalog"]))
    setcookie ("RECENTLY_VIEWED[catalog][".$count."]", $arResult["ID"], time() + 3600*12, "/");
global $APPLICATION;
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);
?>
