<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.min.js')?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.validator.js')?>
<?
global $element_id;
$element_id = $arResult["ID"];
?>


 <script>
$(document).ready(function(){  
        //minus_plus_buts 
    
    $("#minus_plus_buts").on("click",".plus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
  			if(parseInt(otvet)){
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				
  			}
  		});
		return false;
	});
	
	$("#minus_plus_buts").on("click",".minus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet){
  			if(parseInt(otvet)){
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				}
  			
  		});
		return false;
	});
  	
  
  	$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
  		$("#minus_plus_buts").html(otvet);
  	});  
    
   
    
    
});
</script> 

<div class="news-detail">    
         <div class="catalog_detail">
                <div class="left detail">                    
                    <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
                         <?if ($arResult["SECTION"]["PATH"][0]["CODE"]=="restaurants"):?>
                            <?$rest_name="Ресторан "?>
                            <h1>Ресторан <?=$arResult["NAME"]?></h1>
                        <?else:?>
                            <?$rest_name="Банкетный зал "?>
                            <h1>Банкетный зал <?=$arResult["NAME"]?></h1>
                        <?endif;?>
                        <div class="clear"></div>
                    <?endif;?>                    
                    <div class="left">
                        <div id="photogalery">
                            <div class="img img2" style="border:0px; text-align: left; margin-bottom:10px;">
                                <!--<img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>" />-->
                                <?if ($arResult["PREVIEW_PICTURE"]["src"]):?>
                                    <img src="<?=$arResult["PREVIEW_PICTURE"]["src"]?>"/>
                                <?else:?>
                                    <img src="/tpl/images/noname/rest_nnm_new.png" width="380" />
                                <?endif;?>
                            </div>
                            <div class="right">
                                <?if($arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>
                                    <?=GetMessage("R_PHOTO")?>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="$('#photogalery3').show();$('#photogalery').hide();" class="another"><?=GetMessage("R_3D")?></a>                                
                                <?endif;?>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?if ($arResult["PROPERTIES"]["d_tour"]["VALUE"][0]):?>                            
                            <div id="photogalery3" style="display:none">
                                <script>
                                        $(document).ready(function(){
                                            $("#photogalery3").galery({});
                                        });
                                    </script>
                                <div class="img img4">
                                    <?if($arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][0]):?>
                                        <a target="_blank" class="lookon_iframe" href="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][0]?>"></a>
                                        <embed type="application/x-shockwave-flash" src="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][0]?>" width="395" height="264" style="undefined" id="krpanoSWFObject" name="krpanoSWFObject" bgcolor="#000000" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="xml=<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][0]?>">    
                                    <?else:?>
                                        <object type="application/x-shockwave-flash" data="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][0]?>" width="397" height="266" allowFullScreen="true"><param name="movie" value="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][0]?>"></object>
                                    <!--<img src="<?=CFile::GetPath($arResult["PROPERTIES"]["d_tour"]["VALUE"][0])?>" alt="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][0]?>" height="264" />-->
                                    <?endif;?>
                                </div>
                                <div class="special_scroll">   
                                    <div class="scroll_container">                                            
                                        <?foreach($arResult["PROPERTIES"]["d_tour"]["VALUE"] as $key=>$photo):?>
                                            <div class="item <?=($key==0)?"active":""?>" num="<?=($key+1)?>">
                                                <?if($arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key]):?>
                                                    <img lookon="lookon" src="<?=CFile::GetPath($photo)?>" alt_iframe="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][$key]?>" alt="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key]?>" alt_xml="<?=$arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][$key]?>" align="bottom" width="75" />
                                                <?else:?>
                                                    <img dtour="dtour" src="<?=CFile::GetPath($photo)?>" alt="<?=$arResult["PROPERTIES"]["d_tour"]["DESCRIPTION"][$key]?>" align="bottom" width="75" />
                                                <?endif;?>
                                            </div>                                          
                                        <?endforeach;?>
                                    </div>
                                </div>
                                <div class="left scroll_arrows">
                                    <a class="prev browse left"></a>
                                    <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arResult["PROPERTIES"]["d_tour"]["VALUE"])?></div>
                                    <a class="next browse right"></a>
                                </div>
                                <div class="right" style="width:255px; padding-top:20px;" align="right">
                                        <a href="javascript:void(0)" onclick="$('#photogalery').show();$('#photogalery3').hide()" class="another"><?=GetMessage("R_PHOTO")?></a>&nbsp;&nbsp;&nbsp;<?=GetMessage("R_3D")?>
                                </div> 
                                <div class="clear"></div>
                            </div>
                        <?endif;?>
                    </div>
                    <div class="right" style="width:325px;">                     
                        <table class="properties" cellpadding="5" cellspacing="0">
                            <?$i=0;?>
                            <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                                <?if($arProperty["DISPLAY_VALUE"]):?>
                                    <?if ($i <= 5 && $pid!="map" && $pid!="ratio" && $pid!="phone" && $pid!="address" && $pid!="site"):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=$arProperty["NAME"]?>:&nbsp; </span>
                                                </div>
                                            </td>
                                            <td>
                                                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                                    <?=strip_tags(implode(", ",$arProperty["DISPLAY_VALUE"]));?>
                                                <?else:?>
                                                    <?=str_replace(";",";<br />",$arProperty["DISPLAY_VALUE"]);?>
                                                <?endif?>
                                            </td>
                                        </tr>    
                                    <?endif;?>
                                    <?/*if ($pid == "phone" && CSite::InGroup(Array(1,16))):?>
                                        <tr>
                                            <td width="135" valign="top">
                                                <div class="dotted" style="height:12px; margin:0px;">
                                                    <span class="name"><?=$arProperty["NAME"]?>:&nbsp; </span>
                                                </div>
                                            </td>
                                            <td>
                                                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                                    <?=strip_tags(implode(", ",$arProperty["DISPLAY_VALUE"]));?>
                                                <?else:?>
                                                    <?=str_replace(";",";<br />",$arProperty["DISPLAY_VALUE"]);?>
                                                <?endif?>
                                            </td>
                                        </tr>  
                                    <?endif;*/?>
                                    <?$i++;?>
                                <?endif;?>
                                        <?$last_pid = $pid?>
                            <?endforeach;?>
                            <?if ($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"]):?>
                                <tr>
                                    <td>
                                        <div class="dotted" style="height:12px; margin:0px;">
                                            <span class="name"><?=$arResult["DISPLAY_PROPERTIES"]["address"]["NAME"]?>:&nbsp; </span>
                                        </div>                                                
                                    </td>
                                    <td>
                                        <?=$arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"][0]?>
                                    </td>
                                </tr>
                            <?endif;?>  
                        </table>                       
                        <br />
                        <?/*
                        <div class="rest_buttons">    
                            <script>
                                $(document).ready(function(){
                                   $(".small_bron").click(function(){
                                            var _this = $(this);
                                            $.ajax({
                                                type: "POST",
                                                url: "/tpl/ajax/online_order_rest.php",
                                                data: "what=0&date=<?=date("d.m.Y")?>&name=<?=$arResult["NAME"]?>&id=<?=$arResult["ID"]?>&<?=bitrix_sessid_get()?>",
                                                success: function(data) {
                                                    if (!$("#mail_modal").size())
                                                    {
                                                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                                                    }
                                                    $('#bron_modal').html(data);
                                                    showOverflow();
                                                    setCenter($("#bron_modal"));
                                                    $("#bron_modal").fadeIn("300"); 
                                                }
                                            });
                                            return false;
                                    });
                                    
                                    $(".small_ord").click(function(){
                                            var _this = $(this);
                                            $.ajax({
                                                type: "POST",
                                                url: "/tpl/ajax/online_order_rest.php",
                                                data: "what=2&date=<?=date("d.m.Y")?>&name=<?=$arResult["NAME"]?>&id=<?=$arResult["ID"]?>&<?=bitrix_sessid_get()?>",
                                                success: function(data) {
                                                    if (!$("#mail_modal").size())
                                                    {
                                                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                                                    }
                                                    $('#bron_modal').html(data);
                                                    showOverflow();
                                                    setCenter($("#bron_modal"));
                                                    $("#bron_modal").fadeIn("300"); 
                                                }
                                            });
                                            return false;
                                    });   
                                });
                            </script>
                            <?if ($arResult["SECTION"]["PATH"][0]["CODE"]!="banket"):?>
                                <input type="button" class="small_bron light_button" value="забронировать столик" />                                
                                <br />
                            <?endif;?>                                
                                <input type="button" class="small_ord dark_button" value="Заказать банкет" />                                                    
                        </div> */?>
                    </div>
                    <div class="clear"></div> 
                    
                    <!--<div class="preview_text" text="1">
                        <?=$arResult["PREVIEW_TEXT"]?>                   
                    </div>-->
                    <Br />
                    
                    
                    
                    
                    
                    
               
					
					 <div class="staya_likes">
            			
            			<div id="minus_plus_buts"></div>
            			
            			<div class="left">
            			
            			
            			<div style="padding-left:45px;"><?
                          $params = array();
                          $params["id"] = $arResult["ID"];
                          $params = addslashes(json_encode($params));            
                        ?>
                       
                            <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>
                       </div> 
            			
            			</div>
            			
            			<div class="right">
            				
            				
            			<?$APPLICATION->IncludeComponent(
                                    "bitrix:asd.share.buttons",
                                    "likes",
                                    Array(
                                        "ASD_TITLE" => $APPLICATION->GetTitle(false),
                                        "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                                        "ASD_PICTURE" => "",
                                        "ASD_TEXT" => ($arResult["PREVIEW_TEXT"] ? $arResult["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                                        "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                                        "LIKE_TYPE" => "LIKE",
                                        "VK_API_ID" => "2881483",
                                        "VK_LIKE_VIEW" => "mini",
                                        "SCRIPT_IN_HEAD" => "N"
                                    )
                                );?> 

            			</div>
                   		<div class="clear"></div>
       				 </div>
                    
                    
                    
                     <br/>  <br/>
                    <h2><?=GetMessage("R_SIMILAR")?></h2>
                    <?if ($arParams["SIMILAR_OUTPUT"]=="Y"):?>                        
                        <?
                        global $arSimilarFilter;
                        foreach ($arParams["SIMILAR_PROPERTIES"] as $similar_property):
                            if ($similar_property)
                            {
                                switch ($arResult["PROPERTIES"][$similar_property]["PROPERTY_TYPE"])
                                {
                                    case "L":
                                        $arSimilarFilter["PROPERTY_".$similar_property."_VALUE"] = $arResult["PROPERTIES"][$similar_property]["VALUE"];
                                    break;
                                    case "S":
                                    case "E":
                                        $arSimilarFilter["PROPERTY_".$similar_property] = $arResult["PROPERTIES"][$similar_property]["VALUE"];
                                    break;
                                }
                                $arSimilarFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
                            }
                        endforeach;
                        $arSimilarFilter["!ID"] = $arResult["ID"];                        
                        ?>        
                        <?$APPLICATION->IncludeComponent("restoran:restoraunts.list", "similar", array(
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => CITY_ID,
                                "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                                "NEWS_COUNT" => "9",
                                "SORT_BY1" => "rand",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arSimilarFilter",
                                "PROPERTY_CODE" => $arParams["SIMILAR_PROPERTIES"],
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_SHADOW" => "Y",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Рестораны",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_OPTION_ADDITIONAL" => ""
                                ),
                                false
                        );?>
                    <?endif;?> 
                    <div id="tabs_block7">
                        <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <ul class="tabs" ajax="ajax">
                            <li>                                
                                <a href="<?=$templateFolder."/reviews.php?id=".$arResult["ID"]?>&url=<?=$url?>">
                                    <div class="left tab_left"></div>
                                    <div class="left name"><?=GetMessage("R_REVIEWS")?></div>
                                    <div class="left tab_right"></div>
                                    <div class="clear"></div>
                                </a>                                                                
                            </li>
                        </ul>
                        <div class="panes">
                           <div class="pane" style="display:block"></div>
                        </div>
                    </div>  
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "bottom_content_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
                </div>
                <div class="right">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc_right_banner",
                                    "AREA_FILE_RECURSIVE" => "Y",
                                    "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    <div class="clear"></div>
                    <br /><br />
                    <div class="top_block">Популярные</div>
                    <? $arRestIB = getArIblock("catalog", CITY_ID);?>
                    <?
                    $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "recomended",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arRestIB["ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "show_counter",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arPFilter",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "A" => "popular"
                        ),
                    false
                    );?>  
                    <br />
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_1_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
                    <Br /><br />
                    <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
                            Array(),
                            Array("MODE"=>"html")
                    );?>
                    <br /><br />
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_3_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
                </div>
                <div class="clear"></div> 
                <br /><br />
                <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                <br /><br />
            </div>
</div>
            