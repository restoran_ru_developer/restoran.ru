<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$arReviewsIB = getArIblock("reviews", CITY_ID);?>
<link href="<?=SITE_TEMPLATE_PATH?>/components/restoran/catalog.list/reviews/style.css"  type="text/css" rel="stylesheet" />
<link href="/bitrix/components/restoran/comments_add/templates/my_new/style.css"  type="text/css" rel="stylesheet" />
<script src="/tpl/js/jQuery.fileinput.js"></script>
<script src="/tpl/js/jquery.form.js"></script>
<div id="reviews">
<?if ($_REQUEST["id"]&&$arReviewsIB["ID"]):
    global $arrFilter;
    $arrFilter = array();
    $arrFilter["PROPERTY_ELEMENT"] = $_REQUEST["id"];
    $APPLICATION->IncludeComponent(
        "restoran:catalog.list",
        "reviews",
        Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "reviews",
                "IBLOCK_ID" => $arReviewsIB["ID"],
                "NEWS_COUNT" => "3",
                "SORT_BY1" => "created_date",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array("DATE_CREATE","CREATED_BY","DETAIL_PAGE_URL"),
                "PROPERTY_CODE" => array("ELEMENT","minus","plus","photos","video","COMMENTS"),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "URL" => $_REQUEST["url"],
        ),
        false
    );
    $APPLICATION->IncludeComponent(
        "restoran:comments_add_new",
        "with_rating_new_on_page_rest",
        Array(
                "IBLOCK_TYPE" => "reviews",
                "IBLOCK_ID" => $arReviewsIB["ID"],
                "ELEMENT_ID" => $_REQUEST["id"],
                "IS_SECTION" => "N",                        
        ),false
    );
endif;
?>
    </div>