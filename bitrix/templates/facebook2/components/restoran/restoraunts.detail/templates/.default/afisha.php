<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<script>
    $(document).ready(function(){
        $("ul#poster_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.poster_panes > .poster_pane", {                
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.poster_panes > .poster_pane",{effect: 'fade'});
    }); 
    $("ul.history_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.panes > .pane", {                
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.panes > .pane",{effect: 'fade'});
    }); 
    });
</script>
<?
if ($_REQUEST["id"]):    
    $arAfishaIB = getArIblock("afisha", CITY_ID);
    global $arrFilter;
    $arrFilter["PROPERTY_RESTORAN"] =(int) $_REQUEST["id"];
    $arrFilter["!PROPERTY_RESTORAN"] = false;
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "afisha_list_main_rest",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "afisha",
                    "IBLOCK_ID" => $arAfishaIB["ID"],
                    "NEWS_COUNT" => "60",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "property_EVENT_DATE",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => array("DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("EVENT_TYPE","TIME","RESTORAN","EVENT_DATE"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "200",
                    "ACTIVE_DATE_FORMAT" => "j M Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );
endif;
?>

