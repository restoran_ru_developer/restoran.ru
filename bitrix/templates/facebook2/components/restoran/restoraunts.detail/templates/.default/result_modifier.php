<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek","opening_hours"
);

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $old_val = $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"];
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = array();
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][0] = strip_tags($old_val);
        }
    }
}
if (LANGUAGE_ID=="en")
{
    foreach ($arResult["DISPLAY_PROPERTIES"] as &$properties)
    {        
            if (is_array($properties["DISPLAY_VALUE"]))
            {                
                if (is_array($properties["VALUE"]))
                {                    
                    foreach($properties["VALUE"] as $key=>$val)
                    {
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {                                                
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);                        
                        }            
                    }
                }
                else
                {
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {           
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                    } 
                }
            }
            else
            {
                
                $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                if ($ar = $r->Fetch())
                {   
                    if ($ar["PROPERTY_ENG_NAME_VALUE"])
                        $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);

                }   
            }  
            if ($properties["CODE"]=="wi_fi")
                    $properties["DISPLAY_VALUE"] = "Yes";
            
    }
}

$old_val = "";
if ($arResult["PROPERTIES"]["network"]["VALUE"])
{
    $i=1;
    $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"],"PROPERTY_network"=>$arResult["PROPERTIES"]["network"]["VALUE"],"!ID"=>$arResult["ID"]),false,false,Array("ID", "DETAIL_PAGE_URL","PROPERTY_address","PROPERTY_phone","PROPERTY_subway"));
    while ($ar = $res->GetNext())
    {
        //v_dump($ar);
        $arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][$i] = $ar["PROPERTY_ADDRESS_VALUE"];
        $arResult["DISPLAY_PROPERTIES"]["network_url"][$i] = $ar["DETAIL_PAGE_URL"];
        $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][$i] = $ar["PROPERTY_PHONE_VALUE"];
        
        $rrr = CIBlockElement::GetByID($ar["PROPERTY_SUBWAY_VALUE"]);
        if ($arr = $rrr->Fetch())
            $arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][$i] = $arr["NAME"];
        $i++;
    }  
}
/*$res = array();
$arNightIB = getArIblock("special_projects", CITY_ID,"new_year_night"."_");
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNightIB["ID"]),false,Array("nTopCount"=>1));
if ($aNight = $res->GetNext())
{
    $arResult["NIGHT_COUNT"] = 1;
    $arResult["NIGHT_LINK"] = $aNight["DETAIL_PAGE_URL"];
}

$res = array();
$arCorpIB = getArIblock("special_projects", CITY_ID,"new_year_corp"."_");
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
if ($aCorp = $res->GetNext())
{
    $arResult["CORP_COUNT"] = 1;
    $arResult["CORP_LINK"] = $aCorp["DETAIL_PAGE_URL"];
}*/


$res = array();
$arNewsIB = getArIblock("news", CITY_ID);
$res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
$arResult["NEWS_COUNT"] = $res->SelectedRowsCount();
$res = array();
if (CITY_ID!="tmn"&&CITY_ID!="ast"&&CITY_ID!="alm"&&CITY_ID!="vrn"&&CITY_ID!="tln"&&CITY_ID!="krd"&&CITY_ID!="sch")
{
    $arVideonewsIB = getArIblock("videonews", CITY_ID);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arVideonewsIB["ID"]),false,Array("nTopCount"=>1));
    $arResult["VIDEONEWS_COUNT"] = $res->SelectedRowsCount();        

    $res = array();
    $arPhotoIB = getArIblock("photoreports", CITY_ID);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
    $arResult["PHOTO_COUNT"] = $res->SelectedRowsCount();
    
    $res = array(); 
    $arPhotoIB = getArIblock("overviews", CITY_ID);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
    $arResult["OVERVIEWS_COUNT"] = $res->SelectedRowsCount();

    if (SITE_ID=="s1")
    {
        $res = array(); 
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>145),false,Array("nTopCount"=>1));
        $arResult["MC_COUNT"] = $res->SelectedRowsCount();
                

        $res = array();
        $arCorpIB = getArIblock("special_projects", CITY_ID,"letnie_verandy"."_");
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
        if ($aCorp = $res->GetNext())
        {
            $arResult["VERANDA_COUNT"] = 1;
            $arResult["VERANDA_LINK"] = $aCorp["DETAIL_PAGE_URL"];
        }
        /*$res = array();
        $arCorpIB = getArIblock("special_projects", CITY_ID,"valentine"."_");
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
        if ($aCorp = $res->GetNext())
        {
            $arResult["VALENTINE_COUNT"] = 1;
            $arResult["VALENTINE_LINK"] = $aCorp["DETAIL_PAGE_URL"];
        }*/
//        $res = array();
//        $arCorpIB = getArIblock("special_projects", CITY_ID,"8marta"."_");
//        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//        if ($aCorp = $res->GetNext())
//        {
//            $arResult["8MARTA_COUNT"] = 1;
//            $arResult["8MARTA_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//        }
         $res = array();
     /*    $arCorpIB = getArIblock("special_projects", CITY_ID,"post"."_");
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
        if ($aCorp = $res->GetNext())
        {
            $arResult["POST_COUNT"] = 1;
            $arResult["POST_LINK"] = $aCorp["DETAIL_PAGE_URL"];
        }*/        
    }
}
/*$res = array();
$arKuponIB = getArIblock("kupons", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","IBLOCK_ID"=>$arKuponIB["ID"]),false,Array("nTopCount"=>1));
$arResult["KUPONS_COUNT"] = $res->SelectedRowsCount();*/

$res = array();
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arResult["AFISHA_IB"] = $arAfishaIB;
if ($arAfishaIB["ID"]) 
{
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y", "ACTIVE_DATE"=>"Y", ">=PROPERTY_EVENT_DATE"=>array(false, date('Y-m-d')),
        Array("LOGIC"=>"OR",
            Array(            
                "PROPERTY_d1"=>"false",
                "PROPERTY_d2"=>"false",
                "PROPERTY_d3"=>"false",
                "PROPERTY_d4"=>"false",
                "PROPERTY_d5"=>"false",
                "PROPERTY_d6"=>"false",
                "PROPERTY_d7"=>"false",
            ),
            Array(
                "!PROPERTY_d1"=>"false",
            ),
            Array(
                "!PROPERTY_d2"=>"false",
            ),
            Array(
                "!PROPERTY_d3"=>"false",
            ),
            Array(
                "!PROPERTY_d4"=>"false",
            ),
            Array(
                "!PROPERTY_d5"=>"false",
            ),
            Array(
                "!PROPERTY_d6"=>"false",
            ),
            Array(
                "!PROPERTY_d7"=>"false",
            ),
        ),
        "IBLOCK_ID"=>$arAfishaIB["ID"]),false,Array("nTopCount"=>1));
    $arResult["AFISHA_COUNT"] = $res->SelectedRowsCount();
}

if ($arResult["PROPERTIES"]["user_bind"]["VALUE"])
{
    $res = array();
    $arBlogsIB = getArIblock("blogs", CITY_ID);
    $res = CIBlockElement::GetList(Array(),Array("CREATED_BY"=>$arResult["PROPERTIES"]["user_bind"]["VALUE"][0],"IBLOCK_ID"=>$arBlogsIB["ID"]));
    $arResult["BLOGS_COUNT"] = $res->SelectedRowsCount();
}

/*$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>(int)$arResult["ID"]);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
if($ar_result = $db_list->GetNext())
{
    if ($ar_result["ELEMENT_CNT"]>0)
        $arResult["MENU_ID"] = $ar_result['ID'];
}*/

global $DB;
$sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a 
        JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=".$DB->ForSql($arResult["ID"])." ";
$db_list = $DB->Query($sql);
if($ar_result = $db_list->GetNext())
{
    if ($ar_result["ELEMENT_CNT"]>0)
        $arResult["MENU_ID"] = $ar_result['ID'];
}
$p =0;

// get preview text
if ($arResult["MENU_ID"])
    $truncate_len = 1000;
else
    $truncate_len = 200;
$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut($arResult["DETAIL_TEXT"], $truncate_len);
//$arResult["PREVIEW_TEXT"] = strip_tags($arResult["PREVIEW_TEXT"],"<p><a><ul><li><ol>");
$arResult["PREVIEW_TEXT_VK"] = $obParser->html_cut($arResult["DETAIL_TEXT"], 100);
 $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'medium',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
    );
foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
{
    //$file = array();
    //$file = CFile::GetFileArray($photo);
    //if ($file["WIDTH"]>600||$file["HEIGHT"]>600)
    {       
        $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($photo,Array("width"=>$file["WIDTH"],"height"=>$file["HEIGHT"]),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
        $p++;
    }
}

$res = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>101,"CODE"=>CITY_ID));
if ($ars = $res->GetNext())
{
    if($ars["DESCRIPTION"])
    {
        $ars["DESCRIPTION"] = explode("<i>",$ars["DESCRIPTION"]);
        $arResult["CONTACT_DESCRIPTION"] = $ars["DESCRIPTION"][0];

        }
}
$APPLICATION;
$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]["SRC"];
	$cp->arResult['TEXT'] = strip_tags($arResult["PREVIEW_TEXT"]);
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
        $cp->SetResultCacheKeys(array('IMAGE','TEXT'));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
	if (!isset($arResult['IMAGE']))
	{
		$arResult['IMAGE'] = $cp->arResult['IMAGE'];
		$arResult['TEXT'] = $cp->arResult['TEXT'];
	}
}
if ($arResult["PROPERTIES"]["d_tours"]["VALUE"][0])
{    
    $arTourIB = getArIblock("system_lookon", CITY_ID);
    foreach($arResult["PROPERTIES"]["d_tours"]["VALUE"] as $key=>$dtour_id)
    {        
        $rrr = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arTourIB["ID"],"ID"=>$dtour_id,"ACTIVE"=>"Y"),false,false,Array("ID","NAME","PREVIEW_PICTURE","DETAIL_PICTURE","PROPERTY_tour","PROPERTY_widgetPath"));
        if ($rr = $rrr->Fetch())
        {
            $lookon_tour = str_replace("index.html", "start.swf", $rr["PROPERTY_WIDGETPATH_VALUE"]);
            $lookon_xml = str_replace("index.html", "start.xml", $rr["PROPERTY_WIDGETPATH_VALUE"]); 
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key] = $lookon_tour;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][$key] = $lookon_xml;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][$key] = $rr["PROPERTY_TOUR_VALUE"];
            $arResult["PROPERTIES"]["d_tour"]["VALUE"][$key] = $rr["PREVIEW_PICTURE"];
        }
    }
}
?>