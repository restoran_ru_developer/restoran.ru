$(document).ready(function(){
    $("#photogalery").galery({});
    $("body").append("<div class=big_modal id=detail_galery_modal><div class='modal_close_galery'></div><div class='slider_left1'><a></a></div><img src='' /><p></p><div class='slider_right1'><a></a></div></div>");
    //setCenter($(".big_modal"));
    $("#detail_galery_modal .slider_left1").on("click",function(){
        $("#photogalery").find(".scroll_arrows").find(".prev").click();
        change_big_modal3();
    });
    $("#detail_galery_modal .slider_right1").on("click",function(){
        $("#photogalery").find(".scroll_arrows").find(".next").click();                                           
        change_big_modal3();
    });
    $("#detail_galery_modal img").on("click",function(){
        $("#photogalery").find(".scroll_arrows").find(".next").click();                                           
        change_big_modal3();
    });
    $("#detail_galery_modal").hover(function(){
        $("#detail_galery_modal .slider_left1").fadeIn(300);
        $("#detail_galery_modal .slider_right1").fadeIn(300);
    },function(){
        $("#detail_galery_modal .slider_left1").fadeOut(300);
        $("#detail_galery_modal .slider_right1").fadeOut(300);
    });
    $("#photogalery .img img").click(function(){                                            
        var img = new Image();
        img.src = $(this).attr("src");
        var alt = '';
        alt = $(this).attr("alt");
        console.log(alt);
        img.onload = new function(){
            $("#detail_galery_modal img").attr("src",img.src);
            $("#detail_galery_modal p").text(alt);
            showOverflow();
            if ($(window).width()<1100||$(window).height()<700)
            {
                img.width = img.width*0.65;
                img.height = img.height*0.65;
            }
            $("#detail_galery_modal img").animate({"width":+img.width,"height":+img.height},300);
            $("#detail_galery_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))},300);
            $("#detail_galery_modal").fadeIn(300);
        }
    });    
    $("#gal2 .item img").click(function(){                                            
        var img = new Image();
        img.src = $(this).attr("src");
        var alt = '';
        alt = $(this).attr("alt");
        console.log(alt);
        img.onload = new function(){
            $("#detail_galery_modal img").attr("src",img.src);
            $("#detail_galery_modal p").text(alt);
            showOverflow();
            if ($(window).width()<1100||$(window).height()<700)
            {
                img.width = img.width*0.65;
                img.height = img.height*0.65;
            }
            $("#detail_galery_modal img").animate({"width":+img.width,"height":+img.height},300);
            $("#detail_galery_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))},300);
            $("#detail_galery_modal").fadeIn(300);
        }
    });
    $(window).resize(function(){
            if (!$(".big_modal").is("hidden"))
            {
                var img = new Image();
                img.src = $("#photogalery .img img").attr("src");
                if ($(window).width()<1100||$(window).height()<700)
                {
                    img.width = img.width*0.65;
                    img.height = img.height*0.65;
                }
                $("#detail_galery_modal img").css({"width":+img.width,"height":+img.height});
                $("#detail_galery_modal").css({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))});
            }
    });                                        
});
function change_big_modal3()
{
    var img = new Image();                                               
        var alt = '';
        img.src = $("#photogalery").find(".img").find("img").attr("src");
        console.log(img.src);
        alt = $("#photogalery").find(".img").find("img").attr("alt");
        img.onload = new function(){
            $("#detail_galery_modal img").attr("src",img.src);
            $("#detail_galery_modal p").text(alt);
            showOverflow();
            if ($(window).width()<1100||$(window).height()<700)
            {
                img.width = img.width*0.65;
                img.height = img.height*0.65;
            }
            $("#detail_galery_modal img").animate({"width":+img.width,"height":+img.height},100);
            $("#detail_galery_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2),"left":""+eval(($(window).width()/2)-(img.width/2))},100);
        }
}