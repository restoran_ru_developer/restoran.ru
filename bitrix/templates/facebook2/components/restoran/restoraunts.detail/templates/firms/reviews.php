<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$arReviewsIB = getArIblock("reviews", CITY_ID);?>
<div id="reviews">
   <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "detailed_reviews_rest",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "afisha",
                    "IBLOCK_ID" => $arReviewsIB["ID"],
                    "NEWS_COUNT" => "2",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array("DATE_CREATE"),
                    "PROPERTY_CODE" => array("EVENT_TYPE"),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "200",
                    "ACTIVE_DATE_FORMAT" => "j M Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => ((int)$_REQUEST["section"])?(int)$_REQUEST["section"]:"987654321654878",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
           false
    );?> 
   <br /><br /><br />
   <?if ($_REQUEST["add"]=="Y"):?>
        <?$APPLICATION->IncludeComponent(
                "restoran:review.add",
                "",
                Array(
                        "IBLOCK_TYPE" => "reviews",
                        "IBLOCK_ID" => $arReviewsIB["ID"],
                        "SECTION_ID" => (int)$_REQUEST["section"],
                        "ELEMENT_ID" => (int)$_REQUEST["element"]
                )
        );?>
    <?endif;?>
   <br /><br />
</div>