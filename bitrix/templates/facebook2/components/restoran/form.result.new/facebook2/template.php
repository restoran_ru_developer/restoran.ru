<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>

    $(document).ready(function(){
        
        if($.browser.safari){
            browser = 'safari';
        }
        if($.browser.opera){
            browser = 'opera';
        }
        if($.browser.chrome){
            browser = 'chrome';
        }
        $(".ui-input-search").find('input').val($('#autocomplete-restaurant-name').val());
        
        $('#dateinput').val('<? echo ConvertDateTime(ConvertTimeStamp(time(), "SHORT", "ru"), "YYYY-MM-DD", "ru"); ?>');

    });
    
    $( document ).on( "mobileinit", function() {
        $.mobile.panel.prototype.options.initSelector = ".mypanel";
    });
</script>

<link href="<?= $templateFolder ?>/style.css?7"  type="text/css" rel="stylesheet" />
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <script>
        $(document).ready(function(){
           $.tools.validator.localize("ru", {
                '*'	: 'Поле обязательно для заполнения',
                '[req]'	: 'Поле обязательно для заполнения'
            });
            $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
                if (!value)
                    return false;
                return true;
            });            
            $(".ajax_form > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize(),
                        success: function(data) {
                            //$('.ok').fadeIn(500);                                
                            $("#form_result").html(data);                                                                                        
                        }
                    });
                    e.preventDefault();
                }
            }); 
        });
        </script>
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
       
        
        
    <div id="order_online" class="order_online_mobile">
        <div class="ajax_form">
            <?endif;?> 
            <? if ($_REQUEST["formresult"]=="addok" && $_REQUEST["RESULT_ID"]): ?>
            <br /><Br /><Br />
            <div class="ok" style="color:#000"><?= $arResult["FORM_NOTE"] ?>
                <script>
                    $(document).ready(function(){
                        $("#order_online").hide();
                    });
                    </script>
            </div>
            <?endif;?>
            <? if ($_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
            <div class="font12" id="err">
                    <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
            </div>
            <?endif;?>
            <? if ( !$_REQUEST["RESULT_ID"]&&!$_REQUEST["web_form_apply"]): ?>
        <form name="SIMPLE_FORM_3" action="/facebook2/order.php?ID=<?=$_REQUEST["ID"]?>" data-ajax="false" method="POST" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="WEB_FORM_ID" value="3">   
        <? /* if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
          <?if ($arResult["isFormTitle"]):?>
          <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
          <?endif;?>
          <?endif; */ ?>                
       
        
<!--        <h4>Ресторан</h4>-->
        <div class="question autocomplete-question">
            <?
            global $rest2;
            //echo "<h4 style='font-size:28px!important;padding-top:10px!important; text-align:center'>".$rest2."</h4>";            
//            $APPLICATION->IncludeComponent(
//                    "bitrix:search.title", "bron", Array(
//              //  "NAME" => "form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]
//                "NAME" => "form_text_44"
//                    ), false
//            );
            ?>
            <input type="hidden" value="<?=$_REQUEST["ID"]?>" name="form_text_44" />
            <input type="hidden" value="<?=$rest2?>" name="form_text_32" />
        </div>
        <?
        foreach ($arResult["QUESTIONS"] as $key => $question):
            $ar = array();
            $ar["CODE"] = $key;
            $questions[] = array_merge($question, $ar);
        endforeach;
        ?>

<!--        <hr>-->
        <br />
        <div id="datepicker"></div>
        <input type ="hidden" id="actualDate" name="form_date_33" value="<?=$_REQUEST["form_date_33"]?>" />
        <div class="order-time">
		        <h4>Время:</h4>
                <input id="ptime" data-theme="b" type="text" placeholder="19:00" class="text order-time-input" value="<?= ($_REQUEST["form_" . $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[3]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[3]["STRUCTURE"][0]["ID"]] : "19:00" ?>" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>"/>
        </div>
        <div class="order-people">
	        <h4>Кол-во персон:</h4>
			<input id="pnum" data-theme="b" type="text" class="text order-people-input" style="text-align:left" value="<?= ($_REQUEST["form_text_35"]) ? $_REQUEST["form_text_35"] : "2" ?>" maxlength="3" placeholder="2" name="form_text_35"/>
        </div>
        <div class="clear"></div>
        <hr>
        <h4 style="padding-top:0px !important;">Представьтесь</h4>
        <input placeholder="Имя Фамилия" type="text" data-theme="b" class="search_rest order-name-input" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />
        
        <?
        $rsUser = CUser::GetByID($USER->GetID());
        $ar = $rsUser->Fetch();
        $ar["PERSONAL_PHONE"] = str_replace(" ", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace("-", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace("(", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace(")", "", $ar["PERSONAL_PHONE"]);
        $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);
        $phone = $ar["PERSONAL_PHONE"];
        $ar["PERSONAL_PHONE"] = substr($phone, 0, 3) . " " . substr($phone, 3, 3) . "  " . substr($phone, 6, 2) . "  " . substr($phone, 8, 2);
        ?>
        <h4>Контактный телефон</h4>
		<input data-theme="b" type="text" class="text order-name-input" id="phone" style="text-align:left" value="<?= ($_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]] : $ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" placeholder="(123) 456-78-90"/>
		<h4>E-mail:</h4>
			<input data-theme="b" type="email" class="text order-name-input" value="<?= ($_REQUEST["form_text_41"]) ? $_REQUEST["form_text_41"] : "" ?>" name="form_text_41"/>
		<hr>
        <? //var_dump($arResult["isUseCaptcha"])?>
        <? if ($arResult["isUseCaptcha"] == "Y"): ?>
            <div class="question">
                <?= GetMessage("CAPTCHA") ?>
            </div>
            <div class="question" style="background:url('<?= $templateFolder ?>/images/s_star.png') 370px 18px no-repeat;">
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
                <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")  ?><? //=$arResult["REQUIRED_SIGN"];  ?>
                <input type="text" data-theme="b" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
            </div>
        <? endif; ?>
<!--		<div class="smoking">
            <input type="radio" name="form_checkbox_smoke" <?=($_REQUEST["form_checkbox_smoke"]==65)?"checked":""?> id="form_checkbox_smokea" value="65" checked data-theme="d"><label class="label_radio" for="form_checkbox_smokea">Некурящий зал</label>
            <input type="radio" name="form_checkbox_smoke" <?=($_REQUEST["form_checkbox_smoke"]==66)?"checked":""?> id="form_checkbox_smokeb" value="66" data-theme="d"><label class="label_radio" for="form_checkbox_smokeb">Курящий зал</label>
            
        </div>-->
        <h4>Комментарий</h4>
				<textarea class="order-textarea" name="form_textarea_59"><?=$_REQUEST["form_textarea_59"]?></textarea>

        <? if ($_REQUEST["invite"]): ?>
            <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
        <? endif; ?>
        <input type="hidden" value="29" name="form_dropdown_SIMPLE_QUESTION_547" id="what_question">
        <? if ($_REQUEST["city"]): ?>
            <input type="hidden" name="CITY" value="<?= $_REQUEST["city"] ?>" />
        <? else: ?>        
            <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
        <? endif; ?>

        <input type="hidden" name="web_form_apply" value="Y" />
        <input type="hidden" name="web_form_submit" value="<?= strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]; ?>"/>
        
        <a href="#"><input class="light_button catalog-order"  data-theme="b" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?= strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]; ?>" /></a>
        <div class="clear"></div>

        <?= $arResult["FORM_FOOTER"] ?>
        <?
//} //endif (isFormNote)
        ?>
        <?endif;?>
        
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>                    
        </div>
    </div>
        <Br /><br />
            <div id="form_result">
            </div>
        <?endif;?>
        