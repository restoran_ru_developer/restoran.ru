<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--<div style="min-height:24px;">
    <?if (count($arResult["SPEC_PROJECT"])>0):?>
        <ul style="max-height:24px;">
        <?foreach ($arResult["SPEC_PROJECT"] as $spec):?>                 
            <li align="center"><a href="/<?=CITY_ID?>/spec/<?=$spec["CODE"]?>/"><?=$spec["NAME"]?></a></li>
        <?endforeach;?>
    </ul>
    <?endif;?>
</div>-->
<?if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
    ?>
<form action="/<?=CITY_ID?>/catalog/<?=$cat?>/all/" method="get" name="rest_filter_form">
    <div id="new_filter">    
            <div class="left by_params">
                <?=GetMessage("FIND_BY_PARAMS")?>
            </div>
            <div class="left fil">
                    <input type="hidden" name="page" value="1">
                    <?
                    $p=0;
                    if (substr_count($APPLICATION->GetCurDir(), "cookery")>0)
                        $ar_cusel = Array(1,2,3,6,21,7);
                    elseif($_REQUEST["CATALOG_ID"]=="banket")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    elseif($_REQUEST["CATALOG_ID"]=="dostavka")
                    {
                        $ar_cusel = Array(1,4,6,2,3);
                    }
                    else
                        $ar_cusel = Array(1,12,4,2,3);
                    foreach($arResult["arrProp"] as $prop_id => $arProp)
                    {
                        if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
                    ?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">            
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <?
                                $s = 0; $c = 0;
                                 $e = 8;
                                if ((count($arProp["VALUE_LIST"])-$e)>=8)
                                    $e = 8;
                                if ((count($arProp["VALUE_LIST"])-$e)>=30)
                                    $e = 17;
                                if ((count($arProp["VALUE_LIST"])-$e)>=50)
                                    $e = 28;
                                if ((count($arProp["VALUE_LIST"])-$e)<8)
                                    $e = 5;
                                $s = ceil(count($arProp["VALUE_LIST"])/$e);
                                $c = 134*$s;
                                if ($s==7)
                                    $c = 960;
                                ?>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"0px","top":"30px", "width":"<?=$c?>px"}});                                    
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="<?=$arProp["CODE"]?>" style="display:none">
                                    <?$pp=0;?>
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?//=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>
                                        <?if ($pp%$e==0):?>
                                            <div class="left" style="width:124px; margin-right:10px; overflow:hidden">
                                        <?endif;?>

                                        <a val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>

                                        <?if ($pp%$e==($e-1)):?>
                                            </div>                                        
                                        <?endif;?>
                                        <?if (end($arProp["VALUE_LIST"])==$val&&$pp%$e!=($e-1)):?>
                                            </div>
                                        <?endif;?>                            
                                        <?$pp++;?>
                                    <?endforeach;?>      
                                    <?if ($arProp["CODE"]=="out_city"):?>
                                        <a href="/<?=CITY_ID?>/catalog/restaurants/out_city/all/"><?=GetMessage("ALL_OUT_CITY")?></a>
                                    <?endif;?>
                                    <div class="clear"></div>
                                    <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                        <input type="hidden" val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                                    <?endforeach;?>
                                </div>
                            </div>        
                    <?    
                        elseif($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                            <div class="filter_block left" id="filter<?=$prop_id?>">  
                                <div class="name"><a href="javascript:void(0)" class="filter_arrow white"><?=GetMessage("MORE_".$arProp["CODE"])?></a></div>
                                <script>
                                        $(document).ready(function(){
                                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"left":"-85px","top":"30px", "width":"auto"}});                                           
                                        })
                                </script>
                                <div class="filter_popup filter_popup_<?=$prop_id?>" val="<?=$prop_id?>" code="subway" style="display:none">                                    
                                    <div class="subway-map">    
                                        <?if (LANGUAGE_ID=="en"):?>
                                            <img src="/tpl/images/subway/<?=CITY_ID?>_en.gif" />
                                        <?else:?>
                                            <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                        <?endif;?>
                                        <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                            <?if ($val["STYLE"]):?>
                                                <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"> </a>
                                            <?endif;?>
                                        <?endforeach;?>
                                        <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                                <input type="hidden" name="arrFilter_pf[subway][]" val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                        <?endforeach;?>
                                    </div>
                                </div>
                            </div>
                   <?   endif;
                        $p++;
                    }
                    ?>
    </div>
            </div>            
            <?/*if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                <div class="left" style="position:relative;"><a href="/<?=CITY_ID?>/catalog/restaurants/all/?page=1&arrFilter_pf%5Bsale10%5D=Да" style="display:block;position: absolute;top: -4px;height: 38px;width: 120px;background: url(/tpl/images/filter_svyaznoy.png) left top no-repeat;left: -10px;" onclick=""></a></div>            
            <?endif;*/?>            
        <div class="clear"></div>
    </div>
    <?if ($cat=="restaurants"):
        $mess = GetMessage("IBLOCK_SET_FILTER");
    else:
        $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
    endif;
?>
    <div id="new_filter_results">
        <div class="left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
        <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
            <ul id="multi<?=$prop_id?>"><li class="end"></ul>
        <?endforeach;?>
            <ul id="multi1"><li class="end"></ul>
        <div class="left">
            <input class="light_button" type="submit" value="<?=$mess?>" style="margin-top:4px;" />
        </div>
        <div class="clear"></div>
    </div>
</form>