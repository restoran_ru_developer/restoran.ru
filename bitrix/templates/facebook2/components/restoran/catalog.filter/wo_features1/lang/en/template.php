<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Filter";
$MESS ['IBLOCK_SET_FILTER'] = "Find";
$MESS ['IBLOCK_DEL_FILTER'] = "Reset";
$MESS ['IBLOCK_SET_FILTER_BANKET'] = "Найти банкетные залы";
$MESS ['IBLOCK_DEL_FILTER'] = "Сбросить";
$MESS ["SELECT_ALL"] = "Выбрать все";
$MESS ["UNSELECT_ALL"] = "Убрать отметки";
$MESS ["CHOOSE_BUTTON"] = "Choose";
$MESS ["EXTENDED_FILTER"] = "Advanced search";
$MESS ["MAP_SEARCH"] = "Поиск по карте";
$MESS ["NEAR_YOU"] = "Near you";
$MESS ["METRO_ABC"] = "По алфавиту";
$MESS ["METRO_VETKA"] = "По веткам";
$MESS ["MORE_kolichestvochelovek"] = "Количество мест";
$MESS ["MORE_area"] = "District";
$MESS ["MORE_average_bill"] = "Bill";
$MESS ["MORE_kitchen"] = "Kitchen";
$MESS ["MORE_type"] = "Type";
$MESS ["MORE_subway"] = "Subway";
$MESS ["MORE_out_city"] = "Suburb";
$MESS ["FIND_BY_PARAMS"] = "Categories:";
$MESS ["FEATURES"] = "Features";
$MESS ["YOU_CHOOSE"] = "You select:";
$MESS ["ALL_OUT_CITY"] = "All suburbs";
?>