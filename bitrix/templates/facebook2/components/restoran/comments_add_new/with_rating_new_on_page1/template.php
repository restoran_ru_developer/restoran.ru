<?
    $APPLICATION->AddHeadString('<link href="'.$templateFolder.'/fineuploader.css" rel="stylesheet" type="text/css"/>');     
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
                        
?>
<script type="text/javascript" src="<?=$templateFolder?>/script.js"></script>


<form name="attach" action="/bitrix/components/restoran/comments_add_new/ajax.php" method="post" id="comment_form" enctype="multipart/form-data">  
       <?=bitrix_sessid_post()?>
       
        <div class="clear"></div> 
        <br />
            <h4>Ресторан</h4>
            <input type="text" id="restoraunt" class="inputtext-with_border opinion-search font14"  req="req"/>
            <input type="hidden" id="restoran222" value="" size="30" name="ELEMENT_ID"/>
        <hr>
        <?if (!$USER->IsAuthorized()):?>
            <h4 style="padding-top:0px !important;">E-mail:</h4>
                <input class="inputtext-with_border opinion-name-input font14" value="" name="email" req="req" />
        <?endif?>
        <hr>
        <div class="smoking">
        <h4 style="float: left; padding-top: 2px !important; padding-right: 10px;">Рейтинг</h4>
        <div class="active_rating">
            <?for ($i=1;$i<=5;$i++):?>
                <div class="star" alt="<?=$i?>"></div>
            <?endfor;?>
        </div>
       <div class="grey left" style="margin-left:10px;"></div>
	    </div>
	    <hr>
        <h4>Текст отзыва</h4>
            <textarea class="add_review opinion-textarea" id="review" name="review" req="req" ></textarea>
            <input type="hidden" value="" id="input_ratio" name="ratio" />

           
            <div class="clear"></div>
        

       
        <div class="grey_block">
            <?if (!$USER->IsAuthorized()):?>
                <div class="QapTcha"></div>
                <link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
                <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
                <script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
                <script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
                <script type="text/javascript">
                        $(document).ready(function(){
                                $('.QapTcha').QapTcha({
                                        txtLock : '<?=GetMessage("MOVE_SLIDER")?>',
                                        txtUnlock : '<?=GetMessage("MOVE_SLIDER_DONE")?>',
                                        disabledSubmit : true,
                                        autoRevert : true,
                                        PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
                                        autoSubmit : false});
                        });
                </script>
            <?endif;?>
            <div class="right" style="margin-top:5px;">
                <input type="submit" id="add_commm" class="light_button opinion-button" value="+ <?=GetMessage("REVIEWS_PUBLISH")?>">       
            </div>
            <div class="clear"></div>
        </div>
       <div>                    
            <Br />            
            <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
           <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
           <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />
       </div>
        <div id="rating_overlay">
            <div class="close"></div>                           
        </div>
       <div class="clear"></div>
   
   <div class="clear"></div>
</form>
<script>
$(document).ready(function(){        
    $("#restoraunt").autocomplete("/tpl/ajax/afisha_suggest.php", {
        limit: 5,
        minChars: 3,
        width: 685,
        formatItem: function(data, i, n, value) {
            return value.split("###")[0];
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        }        
    });  
    $("#restoraunt").result(function(event, data, formatted) {
        if (data) {
            $("#restoran222").val(formatted.split("###")[1]);
        }
    });               
        var files = new Array();
        var errorHandler = function(event, id, fileName, reason) {
          qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
//        $('#attach_photo').fineUploader({
//            text: {
//                uploadButton: "",
//                cancelButton: "",
//                waitingForResponse: ""
//            },
//            multiple: false,
//            disableCancelForFormUploads: true,
//            request: {
//                endpoint: "<?=$templateFolder?>/upload.php",
//                params: {"generateError": true,"f":"images"}
//            },
//            validation:{
//                allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
//            },
//            failedUploadTextDisplay: {                
//                maxChars: 5
//            },
//            messages: {
//               typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: jpg, gif, png"
//            }
//        })
//        .on('upload', function(id, fileName){
//            $(".qq-upload-list").show();
//        })
//        .on('error', errorHandler)
//        .on('complete', function(event, id, fileName, response) {
//            if (response.success)
//            {
//                files.push(response.resized);            
//                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
//                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
//                $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
//                $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));                        
//                $(".qq-upload-list").hide();
//                $(".qq-upload-success").remove();
//            }
//        }); 
//        $('#attach_video').fineUploader({
//            text: {
//                uploadButton: "",
//                cancelButton: "",
//                waitingForResponse: ""
//            },
//            multiple: true,
//            disableCancelForFormUploads : true,
//            request: {
//                endpoint: "<?=$templateFolder?>/upload.php",
//                params: {"generateError": true, "f":"video"}
//            },
//             validation:{
//                allowedExtensions : ["m4v","avi","mov","flv","3gp"]
//            },
//            failedUploadTextDisplay: {                
//                maxChars: 5
//            },
//            messages: {
//               typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: m4v, avi, mov, flv, 3gp"
//            }                
//        })
//        .on('upload', function(id, fileName){
//            $(".qq-upload-list").show();
//        })
//        .on('error', errorHandler)
//        .on('complete', function(event, id, fileName, response) {
//            if (response.success)
//            {
//                files.push("video");                        
//                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
//                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
//                $('<img src="<?=$templateFolder?>/images/video_file.png" height="70">').appendTo($('#img'+files.length));
//                $('<div class="name">'+response.uploadName+'</div>').appendTo($('#img'+files.length));
//                $('<input value="'+response.path+'" type="hidden" name="video[]" />').appendTo($('#img'+files.length));                            
//                $(".qq-upload-list").hide();
//                $(".qq-upload-success").remove();
//            }
//        });
         $("#wr_cm").click(function(){
            $(this).hide();
            $("#write_comment").show("500");                
        });                 
        $(".img-container").on("click",".remove-link",function(){
            var file = $(this).next().attr("src");
            $(this).parents(".image-block").remove();                
            return false;
        });
        
        $("#comment_form").keypress(function(e){
            e = e || window.event;    
            //for chrome & safari
            if (e.ctrlKey) {
                if(e.keyCode == 10){
                    $("#comment_form").submit();
                    return false;
                }
            };
            //for firefox
            if (e.keyCode == 13 && e.ctrlKey) {
                $("#comment_form").submit();
                return false;
            };
        });
        $("#comment_form").submit(function(){
            if (!$(this).find("#review").val())
                {
                    alert("Введите комментарий!");
                    return false;
                }
            if (!$(this).find("#input_ratio").val())
            {
                alert("Выберите рейтинг!");
                return false;
            }
            if (!$(this).find("#restoran222").val())
            {
                alert("Выберите ресторан!");
                return false;
            }
                
            var params = $(this).serialize();
            $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: params,
                    success: function(data) {
                        if(data)
                        {
                            data = eval('('+data+')');                        
//                            if (!$("#comment_modal").size())
//                            {
//                                $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");
//                            }
//                            var html = '<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="center">';
//                            var html2 = '</div>';                    
//                            $('#comment_modal').html(html+data.MESSAGE+html2);
//                            showOverflow();
//                            setCenter($("#comment_modal"));
                            alert(data.MESSAGE);
                            //$("#comment_modal").fadeIn("300");
                            if (data.ERROR=="1")
                            {
                                $("#add_commm").attr("disabled",false);
                            }
                            if (data.STATUS=="1")
                                setTimeout("location.reload()","500");
                        }
                    }
                });
        return false; 
        });
    });
    
</script>
</div>