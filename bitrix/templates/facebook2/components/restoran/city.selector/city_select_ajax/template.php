<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    <br /><br />
</div>
<p class="center"><?=GetMessage("WRONG_CITY")?></p>
<?$cities = "";?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?if (CITY_ID!=$arItem["CODE"]):
        if (substr_count($_SERVER["HTTP_REFERER"],CITY_ID)):
            $arItem["CODE"] = str_replace(CITY_ID,$arItem["CODE"],$_SERVER["HTTP_REFERER"]);
            $cities .= '<li><a href="'.$arItem["CODE"].'">'.$arItem["NAME"].'</a></li>';
        else:
            if ($arItem["CODE"]=="spb")
                $cities .= '<li><a href="http://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
            elseif($arItem["CODE"]=="ast"&&CSite::InGroup(Array(1,23)))
            {
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
            }
            else
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        endif;
    else:
        $city = $arItem["NAME"];
    endif;?>
<?endforeach;?>
<div style="width:175px; margin:0 auto;">
<div class="title" style="text-align: center"><?=$city?></div>
<ul>
    <?=$cities?>
</ul>
</div>