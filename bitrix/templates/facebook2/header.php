<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="HandheldFriendly" content="True" />
        <?$APPLICATION->ShowHead()?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadScript('/facebook2/js/jquery-latest.min.js'); ?>                
        <link rel="stylesheet" href="/facebook2/style.css" type="text/css"   rel="stylesheet"/>
<!--        <link href="<?=SITE_TEMPLATE_PATH?>/popup.css?24"  type="text/css" rel="stylesheet" />-->
 <?// $APPLICATION->AddHeadScript('/facebook2/scripts/filter.js'); ?>  

        <!--<script type="text/javascript" src="/bitrix/templates/main/script_min.js"></script>-->
<!--<script type="text/javascript" src="/bitrix/components/bitrix/search.title/script.js"></script>
<script type="text/javascript" src="/bitrix/templates/mobile_copy/components/bitrix/search.title/main_search_suggest_new1/script.js"></script>
<script type="text/javascript" src="/bitrix/components/restoran/catalog.filter/templates/new3/script.js"></script>
<script type="text/javascript" src="/bitrix/components/restoran/comments_add_new/templates/with_rating_new_on_page/script.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/header.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/util.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/button.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.base.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.form.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.xhr.js"></script>-->

<!--<script type="text/javascript" src="/tpl/js/fu/js/dnd.js"></script>-->        
<script type="text/javascript" src="/tpl/js/jquery.js"></script>
<script type="text/javascript" src="/facebook2/js/jquery-ui.js"></script>
<script type="text/javascript" src="/facebook2/js/jquery.ui.datepicker-ru.js"></script>
<script type="text/javascript" src="/facebook2/js/maskedinput.js"></script>
<script type="text/javascript" src="/bitrix/templates/main/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/main/js/jquery.tools.validator.js"></script>
<!--<script src="/facebook2/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="/facebook2/js/jquery.jscrollpane.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/tpl/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="/tpl/js/jquery_placeholder.js"></script>
<script type="text/javascript" src="/tpl/js/jquery.popup.js"></script>-->
    <script>
  $(function() {
      $.datepicker.formatDate( "dd.mm.yy");
    $( "#datepicker" ).datepicker({altField: "#actualDate", dateFormat: "dd.mm.yy",onSelect: function(a){$("#pdate").html(a)}, option:$.datepicker.regional[ "ru" ]  });
    $( "#datepicker" ).datepicker( "setDate", "<?=$_REQUEST["form_date_33"]?>" );
    <?if ($_REQUEST["form_date_33"]):?>
        $("#actualDate").val("<?=$_REQUEST["form_date_33"]?>");
    <?else:?>
        $("#actualDate").val("<?=date("d.m.Y")?>");
    <?endif;?>
    $("#pnum").keyup(function(e) {
        if (this.value.match(/[^0-9]/gi)) {
            this.value = this.value.replace(/[^0-9]/gi, '');
    }
    });
    $("#phone").maski("(999) 999-99-99");    
    $("#ptime").maski("99:99");    
  });
  </script>

<!--<script>
	$(function()
{
	$('.scroll-pane').jScrollPane();
});
</script>

<script>
	$(function()
{
	$('.scroll-pane-down').jScrollPane();    
});
</script>-->



     
    </head>
    <body>

     <div id="wrap">
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
<div class="clear"></div>
	<section>