<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<?$CUR_DIR = $APPLICATION->GetCurDir();?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?$APPLICATION->ShowTitle()?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Restoran.ru">
        <meta property="fb:app_id" content="297181676964377" />
        <?if (CITY_ID=="tmn"):?>
            <meta property="fb:admins" content="100005128267295,100004709941783" />
        <?else:?>
            <meta property="fb:admins" content="100004709941783" />
        <?endif;?>
        <?if (!$_REQUEST["CODE"]):?>
            <meta property="og:image" content="http://www.restoran.ru/images/logo.png" />
        <?endif;?>
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link href="http://fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->



        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>



        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>


<!--        --><?//if(preg_match('/banquet-service-new/',$CUR_DIR)):?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script-new.js')?>
<!--        --><?//else:?>
<!--            --><?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
<!--        --><?//endif?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>

        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.ru.js')?>

        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>

        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/index.js')?>


        <?$APPLICATION->ShowHead()?>
        <?
         //Строки от Антона
        if(substr_count($_SERVER["REQUEST_URI"], "blog")>0 || substr_count($_SERVER["REQUEST_URI"], "kup")>0 || substr_count($_SERVER["REQUEST_URI"], "restoran_edit")>0 || substr_count($_SERVER["REQUEST_URI"], "restorator")>0 || substr_count($_SERVER["REQUEST_URI"], "businessman")>0 || substr_count($_SERVER["REQUEST_URI"], "rest_edit")>0)
        {
            if (!CSite::InGroup(Array(1,15,16,23)))
            {
                //$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
            }
            else
                $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor/css/redactor.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadScript('/tpl/js/jquery-ui-1.8.17.custom.min.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/redactor_list_script.js');
        }
        $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);

        CModule::IncludeModule("advertising");
        if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
            $page = "others";
        else
            $page = "main";
        CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));

        //unset($_SESSION["CONTEXT"]);
        //unset($_REQUEST["CONTEXT"]);
//        FirePHP::getInstance()->info();
        ?>


        <link href="<?=SITE_TEMPLATE_PATH?>/css/banquet.css"  type="text/css" rel="stylesheet" />


    </head>
<body class="banquet-section <?if(preg_match('/selections/',$CUR_DIR)){echo 'inner-section-page';}?>">

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=297181676964377&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

<div id="panel"><?$APPLICATION->ShowPanel()?></div>

<div class="container">

<div class="banquet-section-header">
    <div class="wood-top-block-wrapper">
        <div class="center-block">

            <div class="right-phone-block">
                <div class="phone-wrapper">
                    <span>812 </span> 338 38 58
                </div>
                <div class="reserve-wrapper-button"><a href="/tpl/ajax/online_order_banquet.php" class="booking"><span>бронировать</span> бесплатно</a></div>
            </div>
            <div class="left-small-menu">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "top-header-left-menu", Array(
	"ROOT_MENU_TYPE" => "banquet_service_menu",	// Тип меню для первого уровня
	"MAX_LEVEL" => "1",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
	"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"DELAY" => "N",	// Откладывать выполнение шаблона меню
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	"MENU_CACHE_TYPE" => "A",	// Тип кеширования
	"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	),
	false
);?>
<!--                <ul>-->
<!--                    <li><a href="#">Вход</a></li>-->
<!--                    <li><a href="#">Регистрация</a></li>-->
<!--                    <li><a href="#">О службе</a></li>-->
<!--                    <li><a href="#">Сотрудничество</a></li>-->
<!--                    <li><a href="#">In English</a></li>-->
<!--                </ul>-->
            </div>

            <div class="main-center-section-banner-wrapper">
                <div class="city-list-wrapper">
                    <div class="right-city-line"></div>
                    <div class="left-city-line"></div>

                    <div id="city_select">
                        <?$APPLICATION->IncludeComponent("restoran:city.selector", "banquet_city_select", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_URL" => "",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600000000",
	"CACHE_GROUPS" => "Y"
	),
	false
);?>
                    </div>
                </div>

                <div class="main-center-section-banner">
                    <a href="/<?=CITY_ID?>/banquet-service/">
                        <span>Банкетная</span>
                        <br>
                        служба
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/little-logo-restoran.png" width="103" height="46" alt="logo">
                    </a>

                    <div class="left-flag flags"><a href="/ <?=CITY_ID?>/catalog/bankets/all/">Банкетные залы</a></div>
                    <div class="right-flag flags"><a href="#">акции ресторанов</a></div>
                </div>
            </div>

            <?if(preg_match('/selections/',$CUR_DIR)):?>
                <div class="filter-title">
                    <?$APPLICATION->ShowTitle(false)?>
                </div>
                <div class="filter-sub-title">

                    <?
//                    if($CUR_DIR==){
//                        $dir_props = $APPLICATION->GetDirPropertyList('/banquet-service/selections/');
////                        print_r($dir_props);
//                        echo $dir_props['THIS_SECTION_DESC_'.strtoupper(CITY_ID)];
//                    }
//                    else {
                    $APPLICATION->ShowViewContent('section_desc');
//                    }
                    if(!empty($_GET['arrFilter_pf'])){

                        $arIB = getArIblock("selection_of_restaurants", CITY_ID);
                        $arSelect = Array("PREVIEW_TEXT");
                        $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], 'CODE'=>'LIST_DESC');
                        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                        if($ob = $res->GetNextElement())
                        {
                            $arFields = $ob->GetFields();
                            echo $arFields['PREVIEW_TEXT'];
                        }


//                        $dir_props = $APPLICATION->GetDirPropertyList('/banquet-service/selections/');
//                        echo $dir_props['THIS_SECTION_DESC_'.strtoupper(CITY_ID)];
                    }
                    ?>
                </div>
            <?endif?>

        </div>
    </div>

    <?if(!preg_match('/selections/',$CUR_DIR)):?>

        <div class="under-wood-top-block-wrapper">
            <div class="center-block">
                <div class="filter-title">
                    Найдем для вас ресторан мечты
                </div>
                <div class="filter-sub-title">Совершенно бесплатно!</div>

                <div class="reserve-block-wrapper">
                    <form action="" method="post" id="reserve-form">
                        <div class="one-reserve-filter-wrapper">
                            <label for="datepicker">выберите дату</label>
                            <div class="field-wrapper datepicker-wrapper">
                                <input type="text" id="datepicker" class="reserve-calendar-input" value="">
                                <span class="calendar-trigger"></span>
                            </div>
                        </div>
                        <div class="one-reserve-filter-wrapper">
                            <label for="datepicker">кол-во гостей</label>
                            <div class="field-wrapper">
                                <input type="text" class="reserve-guest-num-input" name="reserve_guest_num" value="">
                                <div class="more-less-input-trigger">
                                    <div class="more-button"></div>
                                    <div class="less-button"></div>
                                </div>
                            </div>
                        </div>

                        <div class="one-reserve-filter-wrapper">
                            <label for="datepicker">бюджет р./чел.</label>
                            <div class="field-wrapper">
                                <input type="text" class="reserve-budget-input" value="" name="budget_for_person">
                            </div>
                        </div>

                        <div class="one-reserve-filter-wrapper">
                            <label for="reserve-region">район</label>
                            <div class="field-wrapper">
                                <div class="reserve-region-field-wrapper">
                                    <span class="reserve-region-selected-value" id="reserve-region" data-toggle="dropdown">любой</span>
                                    <input type="hidden" value="" name="reserve_region">
                                    <ul class="">
                                        <li select_value="0">любой</li>

                                        <?

                                        $obCache = new CPageCache;
                                        $life_time = 30*24*60*60;
                                        $cache_id = 'area-banquet-reserve-form-'.CITY_ID;
                                        if($obCache->StartDataCache($life_time, $cache_id)):
                                            $arIB = getArIblock("area", CITY_ID);

                                            $arSelect = Array('ID','NAME');
                                            $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y");
                                            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                                            while($ob = $res->GetNextElement())
                                            {
                                                $obj_field = $ob->GetFields();?>
                                                <li select_value="<?=$obj_field['ID']?>"><?=$obj_field['NAME']?></li>
                                            <?}

                                            $obCache->EndDataCache();
                                        endif;


                                        ?>

                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="one-reserve-filter-wrapper">
                            <label for="reserve-region">повод</label>
                            <div class="field-wrapper">
                                <div class="reserve-region-field-wrapper">
                                    <span class="reserve-region-selected-value" id="reserve-reason" data-toggle="dropdown">любой</span>
                                    <input type="hidden" value="" name="reserve_reason">
                                    <ul class="">
                                        <li select_value="0">любой</li>

                                        <?
                                        $obCache = new CPageCache;
                                        $life_time = 30*24*60*60;
                                        $cache_id = 'reason-banquet-reserve-form-'.CITY_ID;
                                        if($obCache->StartDataCache($life_time, $cache_id)):
                                            $arIB = getArIblock("area", CITY_ID);

                                            $arSelect = Array('ID','NAME');
                                            $arFilter = Array("IBLOCK_ID"=>209, "ACTIVE"=>"Y");
                                            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                                            while($ob = $res->GetNextElement())
                                            {
                                                $obj_field = $ob->GetFields();?>
                                                <li select_value="<?=$obj_field['ID']?>"><?=$obj_field['NAME']?></li>
                                            <?}

                                            $obCache->EndDataCache();
                                        endif;
                                        ?>

                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="one-reserve-filter-wrapper reserve-submit-wrapper">
                            <label for="reserve-submit">Контактные данные</label>
                            <div class="field-wrapper">
<!--                                <input type="submit" id="reserve-submit" value="далее" >-->
                                <a href="/tpl/ajax/online_order_banquet.php" class="booking reserve-submit-link">далее</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="blue-top-main-thought-categories-menu-wrapper">
            <div class="center-block">
                <div class="one-thought-wrapper">
                    <div class="this-title">быстро</div>
                    <div class="this-text-about">наши операторы<br>
                        всегда на связи</div>
                </div>
                <div class="thought-right-border"></div>
                <div class="one-thought-wrapper">
                    <div class="this-title">просто</div>
                    <div class="this-text-about">
                        выбери ресторан
                        <br>
                        в два клика
                    </div>
                </div>
                <div class="thought-right-border"></div>
                <div class="one-thought-wrapper">
                    <div class="this-title">бесплатно</div>
                    <div class="this-text-about">
                        никаких комиссий
                        <br>
                        и скрытых платежей
                    </div>
                </div>
                <div class="thought-right-border"></div>
                <div class="one-thought-wrapper">
                    <div class="this-title">онлайн</div>
                    <div class="this-text-about">
                        фото и 3d-туры
                        <br>
                        для вашего выбора
                    </div>
                </div>
            </div>
        </div>
    <?else:?>
        <div class="under-wood-top-block-wrapper">
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);
//            if(preg_match('/banquet-service-new/',$CUR_DIR)):
                $APPLICATION->IncludeComponent(
                    "restoran:catalog.filter",
                    "banquet_filter_new",
                    Array(
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "FILTER_NAME" => "arrFilter_pf",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => array("average_bill", "subway", "area", 'kolichestvochelovek'),
                        "PRICE_CODE" => array(),
                        "CACHE_TYPE" => "N",    //  Y
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "N",
                        "LIST_HEIGHT" => "5",
                        "TEXT_WIDTH" => "20",
                        "NUMBER_WIDTH" => "5",
                        "SAVE_IN_SESSION" => "N"
                    )
                );
//            else:
//                $APPLICATION->IncludeComponent(
//                    "restoran:catalog.filter",
//                    "banquet_filter",
//                    Array(
//                        "IBLOCK_TYPE" => "catalog",
//                        "IBLOCK_ID" => $arRestIB["ID"],
//                        "FILTER_NAME" => "banquetArrFilter",
//                        "FIELD_CODE" => array(),
//                        "PROPERTY_CODE" => array("average_bill", "subway", "area", 'kolichestvochelovek'),
//                        "PRICE_CODE" => array(),
//                        "CACHE_TYPE" => "N",    //  Y
//                        "CACHE_TIME" => "36000000",
//                        "CACHE_GROUPS" => "N",
//                        "LIST_HEIGHT" => "5",
//                        "TEXT_WIDTH" => "20",
//                        "NUMBER_WIDTH" => "5",
//                        "SAVE_IN_SESSION" => "N"
//                    )
//                );
//            endif;
            ?>
        </div>
    <?endif?>


</div>



<div class="content content-indent">

<div class="block">

