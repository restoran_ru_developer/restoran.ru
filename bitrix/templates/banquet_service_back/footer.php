<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
<div class="clearfix"></div>
</div>
<div class="ontop icon-arrow-up"> наверх</div>
</div>
</div>


<!--footer-->
<div id="footer">
    <div class="container">

        <?
        $arSiteMenuIB = getArIblock("site_menu", CITY_ID);
        $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bottom_site_menu", array(
                "IBLOCK_TYPE" => "site_menu",
                "IBLOCK_ID" => $arSiteMenuIB["ID"],
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "Y",
                "TOP_DEPTH" => "2",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000001",
                "CACHE_GROUPS" => "N",
                "ADD_SECTIONS_CHAIN" => "N"
            ),
            false
        );?>


        <div class="pull-right">
            <div class="title">Пользователю</div>
            <?$APPLICATION->IncludeFile(
                "/index_inc.php",
                Array(),
                Array("MODE"=>"html")
            );?>
            <br /><br />


            <?$APPLICATION->IncludeComponent("bitrix:news.list", "links_cloud", Array(
                    "DISPLAY_DATE" => "N",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "IBLOCK_TYPE" => "site_menu",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => "links_cloud",	// Код информационного блока
                    "NEWS_COUNT" => "20",	// Количество новостей на странице
                    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                    "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                    "SORT_BY2" => "",	// Поле для второй сортировки новостей
                    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                    "FILTER_NAME" => "",	// Фильтр
                    "FIELD_CODE" => "",	// Поля
                    "PROPERTY_CODE" => "",	// Свойства
                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "PARENT_SECTION" => "",	// ID раздела
                    "PARENT_SECTION_CODE" => CITY_ID,	// Код раздела
                    "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
                    "CACHE_TYPE" => "A",	// Тип кеширования
                    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",	// Учитывать права доступа
                    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                    "PAGER_TITLE" => "Новости",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                ),
                false
            );?>

        </div>
        <div class="clearfix"></div>
        <div class="comp_count">
            <ul class="companies">
                <li>
                    <a href="#" class="cakelabs">разработка сайта</a>
                </li>
            </ul>
            <div class="counters">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/temp/counters.png" />
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="booking_form" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">╳<!--&times;--></span><span class="sr-only">Close</span></button>                
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<!--    <div class="modal fade" id="information" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              &nbsp;
            </div>
            <div class="modal-body"></div>
          </div>
        </div>
    </div>-->
<?
setSeo();
?>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-33504724-1']);
    _gaq.push(['_setDomainName', 'restoran.ru']);


    _gaq.push (['_addOrganic', 'images.yandex.ru', 'text']);
    _gaq.push (['_addOrganic', 'blogs.yandex.ru', 'text']);
    _gaq.push (['_addOrganic', 'video.yandex.ru', 'text']);
    _gaq.push (['_addOrganic', 'mail.ru', 'q']);
    _gaq.push (['_addOrganic', 'go.mail.ru', 'q']);
    _gaq.push (['_addOrganic', 'google.com.ua', 'q']);
    _gaq.push (['_addOrganic', 'images.google.ru', 'q']);
    _gaq.push (['_addOrganic', 'maps.google.ru', 'q']);
    _gaq.push (['_addOrganic', 'rambler.ru', 'words']);
    _gaq.push (['_addOrganic', 'nova.rambler.ru', 'query']);
    _gaq.push (['_addOrganic', 'nova.rambler.ru', 'words']);
    _gaq.push (['_addOrganic', 'gogo.ru', 'q']);
    _gaq.push (['_addOrganic', 'nigma.ru', 's']);
    _gaq.push (['_addOrganic', 'search.qip.ru', 'query']);
    _gaq.push (['_addOrganic', 'webalta.ru', 'q']);
    _gaq.push (['_addOrganic', 'sm.aport.ru', 'r']);
    _gaq.push (['_addOrganic', 'meta.ua', 'q']);
    _gaq.push (['_addOrganic', 'search.bigmir.net', 'z']);
    _gaq.push (['_addOrganic', 'search.i.ua', 'q']);
    _gaq.push (['_addOrganic', 'index.online.ua', 'q']);
    _gaq.push (['_addOrganic', 'web20.a.ua', 'query']);
    _gaq.push (['_addOrganic', 'search.ukr.net', 'search_query']);
    _gaq.push (['_addOrganic', 'search.com.ua', 'q']);
    _gaq.push (['_addOrganic', 'search.ua', 'q']);
    _gaq.push (['_addOrganic', 'poisk.ru', 'text']);
    _gaq.push (['_addOrganic', 'go.km.ru', 'sq']);
    _gaq.push (['_addOrganic', 'liveinternet.ru', 'ask']);
    _gaq.push (['_addOrganic', 'gde.ru', 'keywords']);
    _gaq.push (['_addOrganic', 'affiliates.quintura.com', 'request']);
    _gaq.push (['_addOrganic', 'akavita.by', 'z']);
    _gaq.push (['_addOrganic', 'search.tut.by', 'query']);
    _gaq.push (['_addOrganic', 'all.by', 'query']);


    _gaq.push(['_trackPageview']);
    setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])",15000);


    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter17073367 = new Ya.Metrika({id:17073367,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?if ($_COOKIE["NEED_SELECT_CITY"]=="Y"):?>
    <script>
        if (!$("#city_modal").size())
        {
            $("<div class='popup popup_modal' id='city_modal' style='width:320px;padding-bottom:30px'></div>").appendTo("body");
            $('#city_modal').load('/tpl/ajax/city_select2.php?<?=bitrix_sessid_get()?>', function(data) {
                setCenter($("#city_modal"));
                showOverflow();
                $("#city_modal").fadeIn("300");
            });
        }
    </script>
    <?unset($_COOKIE["NEED_SELECT_CITY"]);?>
<?endif;?>
<div id="system_loading">Загрузка...</div>
</body>
</html>