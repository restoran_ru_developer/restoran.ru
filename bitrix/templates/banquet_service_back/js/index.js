/**
 * Created by mac on 18.09.14.
 */

$(function() {
    $( "#datepicker" ).datepicker({format: 'dd.mm.yyyy', language:'ru'});
    $('.datepicker-wrapper .calendar-trigger').on('click', function(){
        $( "#datepicker" ).datepicker('show');
    });

    $('.reserve-region-field-wrapper ul li').on('click',function(){
        select_val = $(this).attr('select_value');
        select_name = $(this).text();
        $(this).parents('.reserve-region-field-wrapper').find('input[type="hidden"]').val(select_val);
        $(this).parents('.reserve-region-field-wrapper').find('.reserve-region-selected-value').text(select_name);
    });

    $('.more-button').on('click', function(){
        if(isNaN(parseInt($('input[name="reserve_guest_num"]').val())) || parseInt($('input[name="reserve_guest_num"]').val())<1){
            $('input[name="reserve_guest_num"]').val(1);
        }
        else {
            $('input[name="reserve_guest_num"]').val(parseInt($('input[name="reserve_guest_num"]').val())+1);
        }
    });
    $('.less-button').on('click', function(){
        if(isNaN(parseInt($('input[name="reserve_guest_num"]').val())) || parseInt($('input[name="reserve_guest_num"]').val())<1){
            $('input[name="reserve_guest_num"]').val(0);
        }
        else {
            $('input[name="reserve_guest_num"]').val(parseInt($('input[name="reserve_guest_num"]').val())-1);
        }
    });


});