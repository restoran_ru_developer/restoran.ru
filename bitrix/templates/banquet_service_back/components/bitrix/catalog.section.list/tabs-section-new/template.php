<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */?>
<?//print_r($arResult);
//UF_SHOW_IN_B_ROOT
//UF_ALL_NEWS_TITLE


$first_entry = true;
?>
<ul class="nav nav-tabs">
<?
foreach ($arResult['SECTIONS'] as $key=>&$arSection)
{
    if(!$arSection['UF_SHOW_IN_B_ROOT'])
        continue;
    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
//    print_r($arSection);
?>
    <li class="<?if($first_entry): $first_entry=false;?>active<?endif?>"><a href="#this_tab_id_<?=$arSection['ID']?>"  data-toggle="tab"><?=$arSection['NAME']?></a></li>
<?
}
?>
</ul>

<div class="tab-content">
    <?
    $first_entry = true;
    $arIB = getArIblock("selection_of_restaurants", CITY_ID);
    foreach ($arResult['SECTIONS'] as $key=>&$arSection)
    {
        if(!$arSection['UF_SHOW_IN_B_ROOT'])
            continue;
        ?>
        <div class="tab-pane <?if($first_entry): $first_entry=false;?>active<?endif;?> sm" id="this_tab_id_<?=$arSection['ID']?>">
            <?
            if(intval($arSection['ELEMENT_CNT'])>0):

                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list", "banquet_main_news_new", Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
//                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_TYPE" => "selection_of_restaurants",
                        "IBLOCK_ID" => $arIB["ID"],
                        "NEWS_COUNT" => 9,
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
//                        "SORT_BY2" => "SORT",
//                        "SORT_ORDER2" => "ASC",
//                        "SORT_BY3" => "",
//                        "SORT_ORDER3" => "",
                        "FILTER_NAME" => '',
                        "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("RATIO", "THIS_RESTAURANT"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => $arSection['ID'],
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",//A
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "REST_PROPS" => "Y",
                        'ALL_NEWS_TITLE'=> $arSection['UF_ALL_NEWS_TITLE'] ? $arSection['UF_ALL_NEWS_TITLE'] : 'все новости',
                        'PARENT_NEWS_LIST' => $convert_news,
                        'ALL_NEWS_PARENT_SECTION_ID' => $arSection['IBLOCK_SECTION_ID'] ? $arSection['IBLOCK_SECTION_ID'] : $arSection['ID'],
                        'BANQUET_INDEX_PAGE' => 'Y'

                    ), false
                );
                ?>
            <?endif?>
        </div>
    <?
    }
    ?>

</div>

