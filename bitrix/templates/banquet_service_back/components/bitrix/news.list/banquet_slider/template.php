<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//print_r($arResult);
?>

<div class="slider-wrapper">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
		<li data-target="#carousel-example-generic" data-slide-to="<?=$key?>" class="<?if(reset($arResult["ITEMS"])==$arItem):?>active<?endif;?>"></li>
		<?endforeach;?>
	</ol>
	<!-- Wrapper for slides -->
	<ul>
		<div class="carousel-inner">
			<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>

					<li class="item <?if(reset($arResult["ITEMS"])==$arItem):?>active<?endif;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<img src="<?=$arResult["SLIDER_PIC"][$key]["SRC"]?>" width="728" height="400" alt="<?=$arItem['NAME']?>">
						<div class="slide-text-about-wrapper">
							<div class="action-little-title">акции ресторанов</div>
							<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE'];?>">
								<div class="action-text">
									<?=$arItem['NAME']?>
									<br>
									<?=$arItem['PREVIEW_TEXT']?>
								</div>
							</a>
						</div>
					</li>
			<?endforeach;?>

		</div>
	</ul>
</div>
	<div class="blue-slider-overlay"></div>
</div>