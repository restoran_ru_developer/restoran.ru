<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Filter";
$MESS ['IBLOCK_SET_FILTER'] = "Find";
$MESS ['IBLOCK_DEL_FILTER'] = "Reset";
$MESS ['IBLOCK_SET_FILTER_BANKET'] = "Найти банкетные залы";
$MESS ['IBLOCK_DEL_FILTER'] = "Сбросить";
$MESS ["SELECT_ALL"] = "Выбрать все";
$MESS ["UNSELECT_ALL"] = "Убрать отметки";
$MESS ["CHOOSE_BUTTON"] = "Choose";
$MESS ["EXTENDED_FILTER"] = "Advanced search";
$MESS ["MAP_SEARCH"] = "Поиск по карте";
$MESS ["NEAR_YOU"] = "Near you";
$MESS ["METRO_ABC"] = "По алфавиту";
$MESS ["METRO_VETKA"] = "По веткам";
$MESS ["MORE_kolichestvochelovek"] = "Sitting capacity";
$MESS ["MORE_area"] = "District";
$MESS ["MORE_average_bill"] = "Check";
$MESS ["MORE_kitchen"] = "Cuisine";
$MESS ["MORE_type"] = "Type";
$MESS ["MORE_subway"] = "Subway";
$MESS ["MORE_out_city"] = "Neighborhood";
$MESS ["FIND_BY_PARAMS"] = "Search by params:";
$MESS ["FEATURES"] = "Features";
$MESS ["YOU_CHOOSE"] = "You select:";
$MESS ["ALL_OUT_CITY"] = "All suburbs";

$MESS ["NF_CHOOSE"] = "Choose";
$MESS ["NF_FEATURES"] = "features";
$MESS ["NF_kolichestvochelovek"] = "Sitting capacity";
$MESS ["NF_area"] = "district";
$MESS ["NF_average_bill"] = "check";
$MESS ["NF_kitchen"] = "cuisine";
$MESS ["NF_type"] = "type";
$MESS ["NF_subway"] = "subway";
$MESS ["NF_out_city"] = "neighborhood";
$MESS ["NF_RESET"] = "Reset all";
$MESS ["NF_OK"] = "Done";
?>