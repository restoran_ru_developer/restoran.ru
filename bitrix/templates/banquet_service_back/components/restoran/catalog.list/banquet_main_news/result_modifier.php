<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

//if ($USER->IsAdmin()){
////    print_r($GLOBALS['banquet_filter']);
////    print_r($arParams['PARENT_NEWS_LIST']);
////    print_r($arResult["ITEMS"]);
//}

//  сортировка от фильтрующего массива
foreach($arParams['PARENT_NEWS_LIST'] as $top_key=>$parent_news_id){
    foreach($arResult["ITEMS"] as  $key=> $one_item){
        if($one_item['ID']==$top_key)
            $new_items_arr['ITEMS'][$key] = $one_item;
    }
}
$arResult["ITEMS"] = $new_items_arr['ITEMS'];

//  для кнопок
$res = CIBlockSection::GetByID($arParams['ALL_NEWS_PARENT_SECTION_ID']);
if($ar_res = $res->GetNext()){
    $arResult['THIS_ROOT_SECTION_PAGE_URL'] = $ar_res['SECTION_PAGE_URL'];
    $arResult['THIS_ROOT_SECTION_NAME'] = $ar_res['NAME'];
}


foreach($arResult["ITEMS"] as $key=>$arItem) {
    // resize images
    if(!$arItem["PREVIEW_PICTURE"]) {    
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    } else {    
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    }
    if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
    
    if($arItem["IBLOCK_TYPE_ID"] == "firms_news")
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("msk",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    }
    if (!$arItem["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    else
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //get props if it is a restaurant
    if ($arParams["REST_PROPS"]=="Y"):

        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
        while ($ob = $res->GetNext())
        {
//            $temp_val['kitchen'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
        if ($ob = $res->GetNext())
        {
//            $temp_val['average_bill'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
        if ($ob = $res->GetNext())
        {
//            $temp_val['subway'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["SUBWAY"] = $a['NAME'];
        }

        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "type"));
        if ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["TYPE"] = $a['NAME'];
        }



        //  для свойства идеально для  элементов (разобраться, слишком сложно написано):
        $arIB = getArIblock("selection_of_restaurants", CITY_ID);
        $res = CIBlockElement::GetProperty($arIB['ID'], $arParams['PARENT_NEWS_LIST'][$arResult["ITEMS"][$key]["ID"]], "sort", "asc", array("CODE" => "IDEALLY"));

        while ($ob = $res->GetNext())
        {
//            $temp_val['kitchen'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);

            if ($a = $r->Fetch()){

                if($a['ID']==$arParams['THIS_SECTION_IDEALLY']){    //  если совпадает с разделом
                    $arResult["ITEMS"][$key]["IDEALLY"][0] = $a['NAME'];
                }
                $temp_a[] = $a['NAME'];
            }
        }
        if(empty($arResult["ITEMS"][$key]["IDEALLY"]) && !empty($temp_a)){
            foreach($temp_a as $a_val){
                $arResult["ITEMS"][$key]["IDEALLY"][] = $a_val;
            }
        }

        unset($temp_a);

    endif;
}

?>