<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["SHOW_CNT_TITLE"] = "Выводить по";
$MESS["SORT_NEW_TITLE"] = "по новизне";
$MESS["SORT_PRICE_TITLE"] = "по стоимости";
$MESS["SORT_RATIO_TITLE"] = "по рейтингу";
$MESS["SORT_ALPHABET_TITLE"] = "по алфавиту";
$MESS["R_ADD2FAVORITES"] = "В избранное";
$MESS["BOOK_BUTTON"] = "ЗАБРОНИРОВАТЬ";
$MESS["NOT_FOUND"] = "К сожалению, по выбранным параметрам фильтра ничего не найдено";

$MESS["R_PROPERTY_kitchen"] = "Кухня";
$MESS["R_PROPERTY_kitchens"] = "Кухни";
$MESS["R_PROPERTY_average_bill"] = "Средний счет";
$MESS["R_PROPERTY_subway"] = "Метро";
$MESS["R_PROPERTY_area"] = "Район";
$MESS["R_PROPERTY_phone"] = "Телефон";
$MESS["R_PROPERTY_address"] = "Адрес";
$MESS["R_PROPERTY_number_of_rooms"] = "Кол-во залов";
$MESS["R_PROPERTY_opening_hours"] = "Время работы";

$MESS["R_PROPERTY_kolichestvochelovek"] = "Кол-во человек";
$MESS["R_PROPERTY_out_city"] = "Загородные";
$MESS["R_PROPERTY_credit_cards"] = "Кредитные карты";
$MESS["R_PROPERTY_children"] = "Детям";
$MESS["R_PROPERTY_features"] = "Особенности";
$MESS["R_PROPERTY_entertainment"] = "Развлечения";
$MESS["R_PROPERTY_ideal_place_for"] = "Идеальное мето для";
$MESS["R_PROPERTY_music"] = "Музыка";
$MESS["R_PROPERTY_parking"] = "Парковка";
$MESS["R_PROPERTY_site"] = "Сайт";
$MESS["R_PROPERTY_breakfast"] = "Завтрак";
$MESS["R_PROPERTY_breakfasts"] = "Завтраки";
$MESS["R_PROPERTY_business_lunch"] = "Бизнес ланч";
$MESS["R_PROPERTY_branch"] = "Бранч";
$MESS["R_PROPERTY_rent"] = "Аренда (стоимость от)";
$MESS["R_PROPERTY_my_alcohol"] = "Свой алкоголь";
$MESS["R_PROPERTY_bankets"] = "Банкеты";
$MESS["R_PROPERTY_catering"] = "Кейтеринг";
$MESS["R_PROPERTY_food_delivery"] = "Доставка еды";
$MESS["R_PROPERTY_wi_fi"] = "Wi-fi";
$MESS["R_PROPERTY_administrative_distr"] = "Адм. округ";
$MESS["R_PROPERTY_email"] = "Email";
$MESS["R_PROPERTY_proposals"] = "Предложения";
$MESS["R_PROPERTY_add_props"] = "Дополнительная информация";
$MESS["R_PROPERTY_landmarks"] = "Ориентиры";
$MESS["R_PROPERTY_type"] = "Тип";

$MESS["R_BOOK_TABLE2"] = "Забронировать столик";
$MESS["R_ORDER_BANKET2"] = "Заказать банкет";
?>