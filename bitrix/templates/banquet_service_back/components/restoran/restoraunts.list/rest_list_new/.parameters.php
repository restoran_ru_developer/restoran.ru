<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_NAME" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_NAME"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
);

$arTemplateParameters['PARENT_NEWS_LIST'] = Array(
    "NAME" => GetMessage("T_IBLOCK_DESC_PARENT_NEWS_LIST"),
    "TYPE" => "TEXT",
    "DEFAULT" => '',
);
$arTemplateParameters['THIS_SECTION_IDEALLY'] = Array(
	"NAME" => GetMessage("T_IBLOCK_DESC_THIS_SECTION_IDEALLY"),
	"TYPE" => "TEXT",
	"DEFAULT" => '',
);
?>
