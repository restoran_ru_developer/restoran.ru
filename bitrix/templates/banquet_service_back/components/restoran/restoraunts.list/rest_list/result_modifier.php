<?
                
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "address", 'phone'
);
//print_r($arResult["ITEMS"]);

foreach($arParams['PARENT_NEWS_LIST'] as $top_key=>$parent_news_id){
    foreach($arResult["ITEMS"] as  $key=> $one_item){
        if($one_item['ID']==$top_key)
            $new_items_arr['ITEMS'][$key] = $one_item;
    }
}

$arResult["ITEMS"] = $new_items_arr['ITEMS'];
foreach($arResult["ITEMS"] as $cell=>$arItem) {
    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        // remove links from subway and kitchen name
        if(in_array($pid, $arFormatProps)) {
            if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                }
            } else {
                $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
            }
        }
    }    
    if (LANGUAGE_ID=="en")
    {
        foreach ($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"] as &$properties)
        {        

                if (is_array($properties["DISPLAY_VALUE"]))
                {
                    if (is_array($properties["VALUE"]))
                    {
                        foreach($properties["VALUE"] as $key=>$val)
                        {
                            $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                            if ($ar = $r->Fetch())
                            {                    
                                if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                    $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);                        
                            }            
                        }
                    }
                    else
                    {
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {           
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                        } 
                    }
                }
                else
                {
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {   
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);

                    }   
                }    
        }
    }
    if($arItem["PREVIEW_PICTURE"])
    {
        //$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = $arItem["PREVIEW_PICTURE"]["SRC"];
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>232, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true, Array());
    }
    elseif($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>232, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true, Array());
        
    }    
    elseif($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][0])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 155), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
    }
    else
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm_new.png";
    //v_dump($arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]);    


    $arIB = getArIblock("selection_of_restaurants", CITY_ID);
    $res = CIBlockElement::GetProperty($arIB['ID'], $arParams['PARENT_NEWS_LIST'][$arResult["ITEMS"][$key]["ID"]], "sort", "asc", array("CODE" => "IDEALLY"));

    while ($ob = $res->GetNext())
    {
//            $temp_val['kitchen'][$key][] = $ob['VALUE'];
        $r = CIBlockElement::GetByID($ob['VALUE']);

        if ($a = $r->Fetch()){
            if($a['ID']==$arParams['THIS_SECTION_IDEALLY']){
                $arResult["ITEMS"][$key]["IDEALLY"][0] = $a['NAME'];
            }
            $temp_a[] = $a['NAME'];
        }
    }
    if(empty($arResult["ITEMS"][$key]["IDEALLY"]) && !empty($temp_a)){
        foreach($temp_a as $a_val){
            $arResult["ITEMS"][$key]["IDEALLY"][] = $a_val;
        }
    }
//    print_r($arResult["ITEMS"]);
    unset($temp_a);
//    print_r($arResult["ITEMS"]);
}
?>