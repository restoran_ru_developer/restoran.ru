jQuery.fn.galery=function(options){var options=jQuery.extend({num:5,speed:500,count:true},options);var _this=this;return this.each(function(){var num="";var container="";var item="";this.counter=function(a,b){var obj=container.parent().find(".scroll_num");var obj_val=obj.html()*1;if(!b)
{if(a)
{if(obj_val==num)
obj.html(1);else
obj.html(++obj_val);}
else
{if(obj_val==1)
obj.html(num);else
obj.html(--obj_val);}}
else
{obj.html(b.attr("num"));}};this.next=function(obj){var url,text;if(container.find(".active").next().hasClass("item"))
{if(typeof(obj)=="undefined")
{url=container.find(".active").removeClass("active").next().addClass("active").find("img").attr("src");text=container.find(".active").find("img").attr("alt");}
else
{container.find(".active").removeClass("active");url=obj.addClass("active").find("img").attr("src");text=obj.find(".active").find("img").attr("alt");}
if(options.count)this.counter(1,obj);}
else
{if(typeof(obj)=="undefined")
{url=container.find(".active").removeClass("active").end().find(".item:first").addClass("active").find("img").attr("src");text=container.find(".active").find("img").attr("alt");}
else
{container.find(".active").removeClass("active");url=obj.addClass("active").find("img").attr("src");text=obj.find(".active").find("img").attr("alt");}
if(options.count)this.counter(1,obj);container.find(".scroll_container").animate({"marginLeft":0},options.speed,"linear");}
var wrap=container.parent().find(".img");var video="";if(container.find(".active").find("img").attr("video"))
video='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="266" border="0"><param name="movie" value="/tpl/js/panorama.swf?img='+url+'&amp;type='+container.find(".active").find("img").attr("type")+'"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"><embed src="/tpl/js/panorama.swf?img='+url+'&amp;type='+container.find(".active").find("img").attr("type")+'" width="397" height="300" border="0" type="application/x-shockwave-flash"></object>';if(container.find(".active").find("img").attr("dtour"))
{if(!text||text=="undefined")
text=container.find(".active").find("img").attr("alt");video='<object type="application/x-shockwave-flash" data="'+text+'" width="397" height="266" allowFullScreen="true"><param name="movie" value="'+text+'" /></object>';}
if(container.find(".active").find("img").attr("lookon"))
{if(!text||text=="undefined")
text=container.find(".active").find("img").attr("alt");video='<a class="lookon_iframe" target="_blank" href="'+container.find(".active").find("img").attr("alt_iframe")+'" style="width:395px;height:264px; position:absolute;display:block; z-index:10000;"></a>\n\
<embed type="application/x-shockwave-flash" src="'+container.find(".active").find("img").attr("alt")+'" width="395" height="264" style="undefined" id="krpanoSWFObject" name="krpanoSWFObject" bgcolor="#000000" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="xml='+container.find(".active").find("img").attr("alt_xml")+'">';}
if(video)
{if(wrap)
wrap.fadeTo(0,0.2,function(){$(this).html(video);}).fadeTo(0,1);}
else
{if(wrap)
wrap.fadeTo(0,0.2,function(){$(this).find("img").attr("src",url);$(this).find("p").html(text);$(this).find("img").attr("alt",text);}).fadeTo(0,1);}
if(container.find(".active").attr("num")>=options.num)
{var left=item.width()*1+item.css("margin-right").replace("px","")*1;$(this).find(".scroll_container").animate({"marginLeft":"-="+left},options.speed,"linear");}};this.prev=function(obj){var url,text;if(container.find(".active").prev().hasClass("item"))
{if(!obj)
{url=container.find(".active").removeClass("active").prev().addClass("active").find("img").attr("src");text=container.find(".active").find("img").attr("alt");}
else
{container.find(".active").removeClass("active");url=obj.addClass("active").find("img").attr("src");text=obj.find(".active").find("img").attr("alt");}
if(options.count)this.counter(0,obj);if(container.find(".active").attr("num")>=options.num)
{var left=item.width()*1+item.css("margin-right").replace("px","")*1;container.find(".scroll_container").animate({"marginLeft":"-="+-left},options.speed,"linear");}
if(obj&&obj.attr("num")*1<=options.num*1)
container.find(".scroll_container").animate({"marginLeft":"0px"},options.speed,"linear");}
else
{if(!obj)
{url=container.find(".active").removeClass("active").end().find(".item:last").addClass("active").find("img").attr("src");text=container.find(".active").find("img").attr("alt");}
else
{container.find(".active").removeClass("active");url=obj.addClass("active").find("img").attr("src");text=obj.find(".active").find("img").attr("alt");}
if(options.count)this.counter(0,obj);var mleft=item.width()*1+item.css("margin-right").replace("px","")*1;var left=mleft*num-mleft*options.num;container.find(".scroll_container").animate({"marginLeft":-left},options.speed,"linear");}
var wrap=container.parent().find(".img");var video="";if(container.find(".active").find("img").attr("video"))
video='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="266" border="0"><param name="movie" value="/tpl/js/panorama.swf?img='+url+'&amp;type=0"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"><embed src="/tpl/js/panorama.swf?img='+url+'&amp;type=0" width="397" height="266" border="0" type="application/x-shockwave-flash"></object>';if(container.find(".active").find("img").attr("dtour"))
{if(!text||text=="undefined")
text=container.find(".active").find("img").attr("alt");video='<object type="application/x-shockwave-flash" data="'+text+'" width="397" height="266" allowFullScreen="true"><param name="movie" value="'+text+'"></object>';}
if(container.find(".active").find("img").attr("lookon"))
{if(!text||text=="undefined")
text=container.find(".active").find("img").attr("alt");video='<a target="_blank" class="lookon_iframe" href="'+container.find(".active").find("img").attr("alt_iframe")+'" style="width:395px;height:264px; position:absolute;display:block; z-index:10000;"></a>\n\
<embed type="application/x-shockwave-flash" src="'+container.find(".active").find("img").attr("alt")+'" width="395" height="264" style="undefined" id="krpanoSWFObject" name="krpanoSWFObject" bgcolor="#000000" quality="high" allowfullscreen="true" allowscriptaccess="always" wmode="opaque" flashvars="xml='+container.find(".active").find("img").attr("alt_xml")+'">';}
if(video)
{if(wrap)
wrap.fadeTo(0,0.2,function(){$(this).html(video);}).fadeTo(0,1);}
else
{if(wrap)
wrap.fadeTo(0,0.2,function(){$(this).find("img").attr("src",url);$(this).find("p").html(text);$(this).find("img").attr("alt",text);}).fadeTo(0,1);}};num=$(this).find(".scroll_container div").length;container=$(this).find(".special_scroll");item=container.find(".item");var sthis=this;container.find(".item").click(function(){var current=container.find(".active").attr("num")*1;var i=1;if($(this).attr("num")*1>=options.num&&$(this).attr("num")*1>current)
{sthis.next($(this));}
else if($(this).attr("num")*1<options.num&&$(this).attr("num")*1>current)
{sthis.next($(this));}
else if($(this).attr("num")*1>=options.num&&$(this).attr("num")*1<current)
{sthis.prev($(this));}
else
sthis.prev($(this));});$(this).find(".scroll_arrows").find(".next").click(function(){sthis.next();});$(this).find(".scroll_arrows").find(".prev").click(function(){sthis.prev();});});};var ajax_load=false;jQuery(document).ready(function(){if($.browser.msie){if($.browser.version=="8.0")
$.fx.off=true;}
setInterval("start_rounded()",3000);jQuery('input[placeholder], textarea[placeholder]').placeholder();$('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/144.gif" align="left"> Загрузка...</div>');$('body').append('<div id="system_overflow"></div>');$.ajaxSetup({async:true,cache:true});$.tools.dateinput.localize("ru",{months:'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря',shortMonths:'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',shortDays:'вс,пн,вт,ср,чт,пт,сб'});$(".baner2 div").css("margin","0 auto");$("#invite_friend").hover(function(){$(this).find("div.restik").slideDown("fast");},function(){$(this).find("div.restik").slideUp("fast");});$(".banner_ajax").each(function(){$(this).html($("#baner_inv_block").find("#baner_"+$(this).attr("id")).find("script").remove().end().html());$("#baner_inv_block").find("#baner_"+$(this).attr("id")).html("");});$("#invite_friend").click(function(){var _this=$(this);$.ajax({type:"POST",url:"/tpl/ajax/invite2site.php",data:"",success:function(data){if(!$("#add_invite").size())
{$("<div class='popup popup_modal' id='add_invite'></div>").appendTo("body");}
$('#add_invite').html(data);showOverflow();setCenter($("#add_invite"));$("#add_invite").fadeIn("300");}});return false;});$("#system_loading").bind("ajaxSend",function(){$(this).css("display","block");ajax_load=true;}).bind("ajaxComplete",function(){$(this).css("display","none");ajax_load=false;});var params={changedEl:"#search_in",visRows:12}
cuSel(params);$("#system_overflow").click(function(){$(".big_modal").css("display","none");hideOverflow();$(".popup").css("display","none");});$("ul.tabs").each(function(){if($(this).attr("ajax")=="ajax")
{if($(this).attr("history")=="true")
var history=true;else
var history=false;var url="";if($(this).attr("ajax_url"))
url=$(this).attr("ajax_url");$(this).tabs("div.panes > .pane",{history:history,onBeforeClick:function(event,i){var pane=this.getPanes().eq(i);if(pane.is(":empty")){pane.load(url+this.getTabs().eq(i).attr("href"));}}});}
else
{if($(this).attr("history")=="true")
var history=true;else
var history=false;$(this).tabs("div.panes  .pane",{history:history,onClick:function(event,i){$(".bx-yandex-map").each(function(){var i=$(this).attr("id");i=i.split('BX_YMAP_');m=window.GLOBAL_arMapObjects[i[1]];if(m)m.container.fitToViewport();});}});}});$("#poster_tabs li").click(function(){if($(this).parents(".pane").attr("main")=="main_page")
$('.poster_pane').jScrollPane({verticalGutter:15});});$("ul#poster_tabs").each(function(){if($(this).attr("ajax")=="ajax")
{var url="";if($(this).attr("ajax_url"))
url=$(this).attr("ajax_url");$(this).tabs("div.poster_panes > .poster_pane",{history:'true',onBeforeClick:function(event,i){var pane=this.getPanes().eq(i);if(pane.is(":empty")){pane.load(url+this.getTabs().eq(i).attr("href"),function(){if($(this).parents(".pane").attr("main")=="main_page")
$('.poster_pane').jScrollPane({verticalGutter:15});});}}});}
else
$(this).tabs("div.poster_panes > .poster_pane",{effect:'fade'});});$("ul.history_tabs").each(function(){if($(this).attr("ajax")=="ajax")
{var url="";if($(this).attr("ajax_url"))
url=$(this).attr("ajax_url");$(this).tabs("div.panes > .pane",{history:'true',onBeforeClick:function(event,i){var pane=this.getPanes().eq(i);if(pane.is(":empty")){pane.load(url+this.getTabs().eq(i).attr("href"));}}});}
else
$(this).tabs("div.panes > .pane",{effect:'fade'});});$(".to_top").on('click',function(){$('html,body').animate({scrollTop:"0"},500);});$("a.anchor").click(function(){var pos=$($(this).attr("href")).offset().top-10;$('html,body').animate({scrollTop:pos},"300");});$("input[type^='radio']").each(function(){changeRadioStart($(this));});$(".modal_close").live('click',function(){$(this).parents(".popup_modal").css("display","none");hideOverflow();});$(".modal_close_galery").live('click',function(){$(".big_modal").css("display","none");hideOverflow();});jQuery("body").on('click',".niceCheck2",function(){changeCheck2(jQuery(this));});jQuery("body").on('click','.niceCheck2Label',function(){changeCheck2(jQuery(this).parent().parent().find(".niceCheck2"));});jQuery(".niceCheck2").each(function(){changeCheckStart2(jQuery(this));});$("#add_rest").click(function(){$.ajax({type:"POST",url:"/tpl/ajax/add_rest.php",data:params,success:function(data){if(!$("#add_rest_form").size())
{$("<div class='popup popup_modal' id='add_rest_form' style='width:735px'></div>").appendTo("body");}
$('#add_rest_form').html(data);showOverflow();setCenter($("#add_rest_form"));$("#add_rest_form").css("display","block");}});});});function changeCheck2(el)
{var el=el,input=el.find("input").eq(0);if(!input.attr("checked")){el.css("background-position","0 -19px");input.attr("checked",true)}else{el.css("background-position","0 0");input.attr("checked",false)}
return true;}
function changeCheckStart2(el)
{var el=el,input=el.find("input").eq(0);if(input.attr("checked")){el.css("background-position","0 -19px");}
return true;}
function showExpose(){$(document).mask({color:'#1a1a1a',loadSpeed:200,closeSpeed:0,opacity:0.5,closeOnEsc:false,closeOnClick:false});}
function closeExpose(){$.mask.close();}
function send_ajax(url,dataType,params,successFunc){if(!dataType)
dataType='json';params=eval('('+params+')');$.ajax({url:url,type:'post',dataType:dataType,data:params,statusCode:{404:function(){alert('Page not found');}},success:function(data){if(successFunc)
successFunc(data);}});}
function sumbit_form(form,beforeFunc,successFunc){var params=$(form).serialize();var url=$(form).attr("action");console.log(url);$.ajax({url:url,type:'post',data:params,statusCode:{404:function(){alert('Page not found');}},beforeSend:function(){if(beforeFunc)
beforeFunc(form);},success:function(data){$(form).parent().html(data);if(successFunc)
successFunc(form,data);}});return false;}
function show_popup(obj,options)
{if(!options)
options={"obj":$(obj).parent().attr("id")};else
options.obj=$(obj).parent().attr("id");if(!$(obj).parent().find("div").hasClass("popup"))
$("<div>").addClass("popup").appendTo($(obj).parent()).popup(options);}
function pluralForm(n,form1,form2,form5){var n=Math.abs(n)%100;var n1=n%10;if(n>10&&n<20)return form5;if(n1>1&&n1<5)return form2;if(n1==1)return form1;return form5;}
function createModal(id)
{}
function setCenter(obj)
{var ow=obj.width();var oh=obj.height();var opt=obj.css("padding-top");if(opt)
opt=opt.replace("px","");var opb=obj.css("padding-bottom");if(opb)
opb=opb.replace("px","");var opl=obj.css("padding-left");if(opl)
opl=opl.replace("px","");var opr=obj.css("padding-right");if(opr)
opr=opr.replace("px","");if(opl=="auto")
opl=0;if(opr=="auto")
opr=0;if(opb=="auto")
opb=0;if(opt=="auto")
opt=0;var omt=obj.css("margin-top");if(omt)
omt=omt.replace("px","");var omb=obj.css("margin-bottom");if(omb)
omb=omb.replace("px","");var oml=obj.css("margin-left");if(oml)
oml=oml.replace("px","");var omr=obj.css("margin-right");if(omr)
omr=omr.replace("px","");if(oml=="auto")
oml=0;if(omr=="auto")
omr=0;if(omb=="auto")
omb=0;if(omt=="auto")
omt=0;oh=oh*1+opt*1+opb*1+omt*1+omb*1;ow=ow*1+opl*1+opr*1+oml*1+omr*1;if(obj.attr("class")=="big_modal")
{obj.css("position","absolute");}
obj.css("top",($(window).height()-oh)/2+"px");obj.css("left",($(window).width()-ow)/2+"px");obj.css("z-index",10000);}
function showOverflow()
{$("body").css("overflow","hidden");$("#system_overflow").fadeIn(300);$("#system_overflow").css("height",$(document).height());}
function hideOverflow()
{$("#system_overflow").fadeOut(300);$("body").css("overflow","auto");}
function send_message(id,name)
{var _this=$(this);$.ajax({type:"POST",url:"/tpl/ajax/im.php",data:"USER_NAME="+name+"&USER_ID="+id,success:function(data){if(!$("#mail_modal").size())
{$("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");}
$('#mail_modal').html(data);showOverflow();setCenter($("#mail_modal"));$("#mail_modal").fadeIn("300");}});return false;}
function show_review_form()
{var _this=$(this);$.ajax({type:"POST",url:"/tpl/ajax/review_form.php",data:"",success:function(data){if(!$("#review_modal").size())
{$("<div class='popup popup_modal' id='review_modal'></div>").appendTo("body");}
$('#review_modal').html(data);showOverflow();setCenter($("#review_modal"));$("#review_modal").fadeIn("300");}});return false;}
function ajax_bron(params,a,c)
{var b="400px";if (!c) var c="";$.ajax({type:"POST",url:"/tpl/ajax/online_order_rest"+c+".php",data:params,success:function(data){if(!$("#bron_modal").size())
{$("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");}
$('#bron_modal').html(data);showOverflow();setCenter($("#bron_modal"));$("#bron_modal").css("display","block");}});}
function ajax_bron2(params,a,c)
{var b="400px";if(a)
b="800px";if (!c) var c="";$.ajax({type:"POST",url:"/tpl/ajax/online_order_banket"+c+".php",data:params,success:function(data){if(!$("#bron_modal").size())
{$("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");}
$('#bron_modal').html(data);showOverflow();setCenter($("#bron_modal"));$("#bron_modal").css("display","block");}});}
function call_me(params)
{$.ajax({type:"POST",url:"/tpl/ajax/call_me.php",data:params,success:function(data){if(!$("#call_me_form").size())
{$("<div class='popup popup_modal' id='call_me_form' style='width:335px'></div>").appendTo("body");}
$('#call_me_form').html(data);showOverflow();setCenter($("#call_me_form"));$("#call_me_form").css("display","block");}});}
function invite2rest(params)
{var _this=$(this);$.ajax({type:"POST",url:"/tpl/ajax/invite2rest.php",data:params,success:function(data){if(!$("#invite2rest_modal").size())
{$("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");}
$('#invite2rest_modal').html(data);showOverflow();setCenter($("#invite2rest_modal"));$("#invite2rest_modal").css("top","150px")
$("#invite2rest_modal").fadeIn("300");}});return false;}
function favorite_success(data)
{if(!$("#favorite_form").size())
{$("<div class='popup popup_modal' id='favorite_form' style='width:335px'></div>").appendTo("body");}
$('#favorite_form').html(data);showOverflow();setCenter($("#favorite_form"));$("#favorite_form").fadeIn("300");if(!data.ERROR)
{if(data.MESSAGE)
$("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.MESSAGE);}
else
$("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.ERROR);}
function start_rounded()
{if($("#rounded1").is(":hidden"))
{$("#rounded1").show();$("#rounded_first").show();$("#rounded2").hide();$("#rounded_second").hide();}
else
{$("#rounded1").hide();$("#rounded_first").hide();$("#rounded2").show();$("#rounded_second").show();}};jQuery.fn.popup=function(options){var options=jQuery.extend({url:"",bg_color:"#2d323b",opacity:".97",obj:"",css:{"top":"-20px","right":"-10px"},center:false,speed:500,right:false},options);this.makeCenter=function(){var a=((($(window).height()/2)-((jQuery(this).height())/2)));if(a<0)
a=0;jQuery(this).css({top:a+'px',right:((($(window).width()/2)-((jQuery(this).width())/2)))+'px',position:"fixed"});};this.init=function(){var _this=this;_this.css({"background":"url(/tpl/images/popup_bg.png) left top","opacity":"0.99","display":"none","position":"absolute","z-index":"10000"});_this.css(options.css);_this.parent().css("position","relative");$("#"+options.obj).on('click','a',function(e){$(".popup").fadeOut(options.speed);$(".filter_popup").fadeOut(options.speed);e.stopPropagation();if(options.url)
{if(!$(_this).html())
{if(options.url==true)options.url=$(this).attr("href");console.log(options.url);_this.load(options.url,function(){if(options.center)
_this.makeCenter();_this.fadeIn(options.speed);});}
else
{if(options.center)
_this.makeCenter();_this.fadeIn(options.speed);}
return false;}
else
{if(options.center)
_this.makeCenter();_this.fadeIn(options.speed);}});$(_this).on('click','.popup_close',function(e){_this.fadeOut(options.speed);});_this.click(function(e){e.stopPropagation();});$('html,body').click(function(){_this.fadeOut(options.speed);});$(window).keyup(function(event){if(event.keyCode=='27'){_this.fadeOut(options.speed);}});if(options.center)
{$(window).resize(function(){_this.makeCenter();});}
return _this;};return this.init();};(function(a){var b=(a.browser.msie?"paste":"input")+".maski",c=window.orientation!=undefined;a.maski={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawmaskiFn"},a.fn.extend({caret:function(a,b){if(this.length!=0){if(typeof a=="number"){b=typeof b=="number"?b:a;return this.each(function(){if(this.setSelectionRange)this.setSelectionRange(a,b);else if(this.createTextRange){var c=this.createTextRange();c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",a),c.select()}})}if(this[0].setSelectionRange)a=this[0].selectionStart,b=this[0].selectionEnd;else if(document.selection&&document.selection.createRange){var c=document.selection.createRange();a=0-c.duplicate().moveStart("character",-1e5),b=a+c.text.length}return{begin:a,end:b}}},unmaski:function(){return this.trigger("unmaski")},maski:function(d,e){if(!d&&this.length>0){var f=a(this[0]);return f.data(a.maski.dataName)()}e=a.extend({placeholder:"_",completed:null},e);var g=a.maski.definitions,h=[],i=d.length,j=null,k=d.length;a.each(d.split(""),function(a,b){b=="?"?(k--,i=a):g[b]?(h.push(new RegExp(g[b])),j==null&&(j=h.length-1)):h.push(null)});return this.trigger("unmaski").each(function(){function v(a){var b=f.val(),c=-1;for(var d=0,g=0;d<k;d++)if(h[d]){l[d]=e.placeholder;while(g++<b.length){var m=b.charAt(g-1);if(h[d].test(m)){l[d]=m,c=d;break}}if(g>b.length)break}else l[d]==b.charAt(g)&&d!=i&&(g++,c=d);if(!a&&c+1<i)f.val(""),t(0,k);else if(a||c+1>=i)u(),a||f.val(f.val().substring(0,c+1));return i?d:j}function u(){return f.val(l.join("")).val()}function t(a,b){for(var c=a;c<b&&c<k;c++)h[c]&&(l[c]=e.placeholder)}function s(a){var b=a.which,c=f.caret();if(a.ctrlKey||a.altKey||a.metaKey||b<32)return!0;if(b){c.end-c.begin!=0&&(t(c.begin,c.end),p(c.begin,c.end-1));var d=n(c.begin-1);if(d<k){var g=String.fromCharCode(b);if(h[d].test(g)){q(d),l[d]=g,u();var i=n(d);f.caret(i),e.completed&&i>=k&&e.completed.call(f)}}return!1}}function r(a){var b=a.which;if(b==8||b==46||c&&b==127){var d=f.caret(),e=d.begin,g=d.end;g-e==0&&(e=b!=46?o(e):g=n(e-1),g=b==46?n(g):g),t(e,g),p(e,g-1);return!1}if(b==27){f.val(m),f.caret(0,v());return!1}}function q(a){for(var b=a,c=e.placeholder;b<k;b++)if(h[b]){var d=n(b),f=l[b];l[b]=c;if(d<k&&h[d].test(f))c=f;else break}}function p(a,b){if(!(a<0)){for(var c=a,d=n(b);c<k;c++)if(h[c]){if(d<k&&h[c].test(l[d]))l[c]=l[d],l[d]=e.placeholder;else break;d=n(d)}u(),f.caret(Math.max(j,a))}}function o(a){while(--a>=0&&!h[a]);return a}function n(a){while(++a<=k&&!h[a]);return a}var f=a(this),l=a.map(d.split(""),function(a,b){if(a!="?")return g[a]?e.placeholder:a}),m=f.val();f.data(a.maski.dataName,function(){return a.map(l,function(a,b){return h[b]&&a!=e.placeholder?a:null}).join("")}),f.attr("readonly")||f.one("unmaski",function(){f.unbind(".maski").removeData(a.maski.dataName)}).bind("focus.maski",function(){m=f.val();var b=v();u();var c=function(){b==d.length?f.caret(0,b):f.caret(b)};(a.browser.msie?c:function(){setTimeout(c,0)})()}).bind("blur.maski",function(){v(),f.val()!=m&&f.change()}).bind("keydown.maski",r).bind("keypress.maski",s).bind(b,function(){setTimeout(function(){f.caret(v(!0))},0)}),v()})}})})(jQuery);
/*!
 * jQuery Tools v1.2.6 - The missing UI library for the Web
 * 
 * dateinput/dateinput.js
 * overlay/overlay.js
 * rangeinput/rangeinput.js
 * scrollable/scrollable.js
 * scrollable/scrollable.navigator.js
 * tabs/tabs.js
 * tabs/tabs.slideshow.js
 * toolbox/toolbox.expose.js
 * toolbox/toolbox.history.js
 * toolbox/toolbox.mousewheel.js
 * tooltip/tooltip.js
 * tooltip/tooltip.dynamic.js
 * tooltip/tooltip.slide.js
 * validator/validator.js
 * 
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 * 
 * http://flowplayer.org/tools/
 * 
 * jquery.event.wheel.js - rev 1 
 * Copyright (c) 2008, Three Dub Media (http://threedubmedia.com)
 * Liscensed under the MIT License (MIT-LICENSE.txt)
 * http://www.opensource.org/licenses/mit-license.php
 * Created: 2008-07-01 | Updated: 2008-07-14
 * 
 * -----
 * 
 */
(function(a,b){a.tools=a.tools||{version:"v1.2.7"};var c=[],d={},e,f=[75,76,38,39,74,72,40,37],g={};e=a.tools.dateinput={conf:{format:"mm/dd/yy",formatter:"default",selectors:!1,yearRange:[-5,5],lang:"en",offset:[0,0],speed:0,firstDay:0,min:b,max:b,trigger:0,toggle:0,editable:0,css:{prefix:"cal",input:"date",root:0,head:0,title:0,prev:0,next:0,month:0,year:0,days:0,body:0,weeks:0,today:0,current:0,week:0,off:0,sunday:0,focus:0,disabled:0,trigger:0}},addFormatter:function(a,b){d[a]=b},localize:function(b,c){a.each(c,function(a,b){c[a]=b.split(",")}),g[b]=c}},e.localize("en",{months:"January,February,March,April,May,June,July,August,September,October,November,December",shortMonths:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec",days:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",shortDays:"Sun,Mon,Tue,Wed,Thu,Fri,Sat"});function h(a,b){return(new Date(a,b+1,0)).getDate()}function i(a,b){a=""+a,b=b||2;while(a.length<b)a="0"+a;return a}var j=a("<a/>");function k(a,b,c,e){var f=b.getDate(),h=b.getDay(),k=b.getMonth(),l=b.getFullYear(),m={d:f,dd:i(f),ddd:g[e].shortDays[h],dddd:g[e].days[h],m:k+1,mm:i(k+1),mmm:g[e].shortMonths[k],mmmm:g[e].months[k],yy:String(l).slice(2),yyyy:l},n=d[a](c,b,m,e);return j.html(n).html()}e.addFormatter("default",function(a,b,c,d){return a.replace(/d{1,4}|m{1,4}|yy(?:yy)?|"[^"]*"|'[^']*'/g,function(a){return a in c?c[a]:a})}),e.addFormatter("prefixed",function(a,b,c,d){return a.replace(/%(d{1,4}|m{1,4}|yy(?:yy)?|"[^"]*"|'[^']*')/g,function(a,b){return b in c?c[b]:a})});function l(a){return parseInt(a,10)}function m(a,b){return a.getFullYear()===b.getFullYear()&&a.getMonth()==b.getMonth()&&a.getDate()==b.getDate()}function n(a){if(a!==b){if(a.constructor==Date)return a;if(typeof a=="string"){var c=a.split("-");if(c.length==3)return new Date(l(c[0]),l(c[1])-1,l(c[2]));if(!/^-?\d+$/.test(a))return;a=l(a)}var d=new Date;d.setDate(d.getDate()+a);return d}}function o(d,e){var i=this,j=new Date,o=j.getFullYear(),p=e.css,q=g[e.lang],r=a("#"+p.root),s=r.find("#"+p.title),t,u,v,w,x,y,z=d.attr("data-value")||e.value||d.val(),A=d.attr("min")||e.min,B=d.attr("max")||e.max,C,D;A===0&&(A="0"),z=n(z)||j,A=n(A||new Date(o+e.yearRange[0],1,1)),B=n(B||new Date(o+e.yearRange[1]+1,1,-1));if(!q)throw"Dateinput: invalid language: "+e.lang;if(d.attr("type")=="date"){var D=d.clone(),E=D.wrap("<div/>").parent().html(),F=a(E.replace(/type/i,"type=text data-orig-type"));e.value&&F.val(e.value),d.replaceWith(F),d=F}d.addClass(p.input);var G=d.add(i);if(!r.length){r=a("<div><div><a/><div/><a/></div><div><div/><div/></div></div>").hide().css({position:"absolute"}).attr("id",p.root),r.children().eq(0).attr("id",p.head).end().eq(1).attr("id",p.body).children().eq(0).attr("id",p.days).end().eq(1).attr("id",p.weeks).end().end().end().find("a").eq(0).attr("id",p.prev).end().eq(1).attr("id",p.next),s=r.find("#"+p.head).find("div").attr("id",p.title);if(e.selectors){var H=a("<select/>").attr("id",p.month),I=a("<select/>").attr("id",p.year);s.html(H.add(I))}var J=r.find("#"+p.days);for(var K=0;K<7;K++)J.append(a("<span/>").text(q.shortDays[(K+e.firstDay)%7]));a("body").append(r)}e.trigger&&(t=a("<a/>").attr("href","#").addClass(p.trigger).click(function(a){e.toggle?i.toggle():i.show();return a.preventDefault()}).insertAfter(d));var L=r.find("#"+p.weeks);I=r.find("#"+p.year),H=r.find("#"+p.month);function M(b,c,e){z=b,w=b.getFullYear(),x=b.getMonth(),y=b.getDate(),e||(e=a.Event("api")),e.type=="click"&&!a.browser.msie&&d.focus(),e.type="beforeChange",G.trigger(e,[b]);e.isDefaultPrevented()||(d.val(k(c.formatter,b,c.format,c.lang)),e.type="change",G.trigger(e),d.data("date",b),i.hide(e))}function N(b){b.type="onShow",G.trigger(b),a(document).on("keydown.d",function(b){if(b.ctrlKey)return!0;var c=b.keyCode;if(c==8||c==46){d.val("");return i.hide(b)}if(c==27||c==9)return i.hide(b);if(a(f).index(c)>=0){if(!C){i.show(b);return b.preventDefault()}var e=a("#"+p.weeks+" a"),g=a("."+p.focus),h=e.index(g);g.removeClass(p.focus);if(c==74||c==40)h+=7;else if(c==75||c==38)h-=7;else if(c==76||c==39)h+=1;else if(c==72||c==37)h-=1;h>41?(i.addMonth(),g=a("#"+p.weeks+" a:eq("+(h-42)+")")):h<0?(i.addMonth(-1),g=a("#"+p.weeks+" a:eq("+(h+42)+")")):g=e.eq(h),g.addClass(p.focus);return b.preventDefault()}if(c==34)return i.addMonth();if(c==33)return i.addMonth(-1);if(c==36)return i.today();c==13&&(a(b.target).is("select")||a("."+p.focus).click());return a([16,17,18,9]).index(c)>=0}),a(document).on("click.d",function(b){var c=b.target;!a(c).parents("#"+p.root).length&&c!=d[0]&&(!t||c!=t[0])&&i.hide(b)})}a.extend(i,{show:function(b){if(!(d.attr("readonly")||d.attr("disabled")||C)){b=b||a.Event(),b.type="onBeforeShow",G.trigger(b);if(b.isDefaultPrevented())return;a.each(c,function(){this.hide()}),C=!0,H.off("change").change(function(){i.setValue(l(I.val()),l(a(this).val()))}),I.off("change").change(function(){i.setValue(l(a(this).val()),l(H.val()))}),u=r.find("#"+p.prev).off("click").click(function(a){u.hasClass(p.disabled)||i.addMonth(-1);return!1}),v=r.find("#"+p.next).off("click").click(function(a){v.hasClass(p.disabled)||i.addMonth();return!1}),i.setValue(z);var f=d.offset();/iPad/i.test(navigator.userAgent)&&(f.top-=a(window).scrollTop()),r.css({top:f.top+d.outerHeight({margins:!0})+e.offset[0],left:f.left+e.offset[1]}),e.speed?r.show(e.speed,function(){N(b)}):(r.show(),N(b));return i}},setValue:function(c,d,f){var g=l(d)>=-1?new Date(l(c),l(d),l(f==b||isNaN(f)?1:f)):c||z;g<A?g=A:g>B&&(g=B),typeof c=="string"&&(g=n(c)),c=g.getFullYear(),d=g.getMonth(),f=g.getDate(),d==-1?(d=11,c--):d==12&&(d=0,c++);if(!C){M(g,e);return i}x=d,w=c,y=f;var k=new Date(c,d,1-e.firstDay),o=k.getDay(),r=h(c,d),t=h(c,d-1),D;if(e.selectors){H.empty(),a.each(q.months,function(b,d){A<new Date(c,b+1,1)&&B>new Date(c,b,0)&&H.append(a("<option/>").html(d).attr("value",b))}),I.empty();var E=j.getFullYear();for(var F=E+e.yearRange[0];F<E+e.yearRange[1];F++)A<new Date(F+1,0,1)&&B>new Date(F,0,0)&&I.append(a("<option/>").text(F));H.val(d),I.val(c)}else s.html(q.months[d]+" "+c);L.empty(),u.add(v).removeClass(p.disabled);for(var G=o?0:-7,J,K;G<(o?42:35);G++)J=a("<a/>"),G%7===0&&(D=a("<div/>").addClass(p.week),L.append(D)),G<o?(J.addClass(p.off),K=t-o+G+1,g=new Date(c,d-1,K)):G<o+r?(K=G-o+1,g=new Date(c,d,K),m(z,g)?J.attr("id",p.current).addClass(p.focus):m(j,g)&&J.attr("id",p.today)):(J.addClass(p.off),K=G-r-o+1,g=new Date(c,d+1,K)),A&&g<A&&J.add(u).addClass(p.disabled),B&&g>B&&J.add(v).addClass(p.disabled),J.attr("href","#"+K).text(K).data("date",g),D.append(J);L.find("a").click(function(b){var c=a(this);c.hasClass(p.disabled)||(a("#"+p.current).removeAttr("id"),c.attr("id",p.current),M(c.data("date"),e,b));return!1}),p.sunday&&L.find("."+p.week).each(function(){var b=e.firstDay?7-e.firstDay:0;a(this).children().slice(b,b+1).addClass(p.sunday)});return i},setMin:function(a,b){A=n(a),b&&z<A&&i.setValue(A);return i},setMax:function(a,b){B=n(a),b&&z>B&&i.setValue(B);return i},today:function(){return i.setValue(j)},addDay:function(a){return this.setValue(w,x,y+(a||1))},addMonth:function(a){var b=x+(a||1),c=h(w,b),d=y<=c?y:c;return this.setValue(w,b,d)},addYear:function(a){return this.setValue(w+(a||1),x,y)},destroy:function(){d.add(document).off("click.d keydown.d"),r.add(t).remove(),d.removeData("dateinput").removeClass(p.input),D&&d.replaceWith(D)},hide:function(b){if(C){b=a.Event(),b.type="onHide",G.trigger(b);if(b.isDefaultPrevented())return;a(document).off("click.d keydown.d"),r.hide(),C=!1}return i},toggle:function(){return i.isOpen()?i.hide():i.show()},getConf:function(){return e},getInput:function(){return d},getCalendar:function(){return r},getValue:function(a){return a?k(e.formatter,z,a,e.lang):z},isOpen:function(){return C}}),a.each(["onBeforeShow","onShow","change","onHide"],function(b,c){a.isFunction(e[c])&&a(i).on(c,e[c]),i[c]=function(b){b&&a(i).on(c,b);return i}}),e.editable||d.on("focus.d click.d",i.show).keydown(function(b){var c=b.keyCode;if(C||a(f).index(c)<0)(c==8||c==46)&&d.val("");else{i.show(b);return b.preventDefault()}return b.shiftKey||b.ctrlKey||b.altKey||c==9?!0:b.preventDefault()}),n(d.val())&&M(z,e)}a.expr[":"].date=function(b){var c=b.getAttribute("type");return c&&c=="date"||a(b).data("dateinput")},a.fn.dateinput=function(b){if(this.data("dateinput"))return this;b=a.extend(!0,{},e.conf,b),a.each(b.css,function(a,c){!c&&a!="prefix"&&(b.css[a]=(b.css.prefix||"")+(c||a))});var d;this.each(function(){var e=new o(a(this),b);c.push(e);var f=e.getInput().data("dateinput",e);d=d?d.add(f):f});return d?d:this}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.6"},a.tools.overlay={addEffect:function(a,b,d){c[a]=[b,d]},conf:{close:null,closeOnClick:!0,closeOnEsc:!0,closeSpeed:"fast",effect:"default",fixed:!a.browser.msie||a.browser.version>6,left:"center",load:!1,mask:null,oneInstance:!0,speed:"normal",target:null,top:"10%"}};var b=[],c={};a.tools.overlay.addEffect("default",function(b,c){var d=this.getConf(),e=a(window);d.fixed||(b.top+=e.scrollTop(),b.left+=e.scrollLeft()),b.position=d.fixed?"fixed":"absolute",this.getOverlay().css(b).fadeIn(d.speed,c)},function(a){this.getOverlay().fadeOut(this.getConf().closeSpeed,a)});function d(d,e){var f=this,g=d.add(f),h=a(window),i,j,k,l=a.tools.expose&&(e.mask||e.expose),m=Math.random().toString().slice(10);l&&(typeof l=="string"&&(l={color:l}),l.closeOnClick=l.closeOnEsc=!1);var n=e.target||d.attr("rel");j=n?a(n):null||d;if(!j.length)throw"Could not find Overlay: "+n;d&&d.index(j)==-1&&d.click(function(a){f.load(a);return a.preventDefault()}),a.extend(f,{load:function(d){if(f.isOpened())return f;var i=c[e.effect];if(!i)throw"Overlay: cannot find effect : \""+e.effect+"\"";e.oneInstance&&a.each(b,function(){this.close(d)}),d=d||a.Event(),d.type="onBeforeLoad",g.trigger(d);if(d.isDefaultPrevented())return f;k=!0,l&&a(j).expose(l);var n=e.top,o=e.left,p=j.outerWidth({margin:!0}),q=j.outerHeight({margin:!0});typeof n=="string"&&(n=n=="center"?Math.max((h.height()-q)/2,0):parseInt(n,10)/100*h.height()),o=="center"&&(o=Math.max((h.width()-p)/2,0)),i[0].call(f,{top:n,left:o},function(){k&&(d.type="onLoad",g.trigger(d))}),l&&e.closeOnClick&&a.mask.getMask().one("click",f.close),e.closeOnClick&&a(document).bind("click."+m,function(b){a(b.target).parents(j).length||f.close(b)}),e.closeOnEsc&&a(document).bind("keydown."+m,function(a){a.keyCode==27&&f.close(a)});return f},close:function(b){if(!f.isOpened())return f;b=b||a.Event(),b.type="onBeforeClose",g.trigger(b);if(!b.isDefaultPrevented()){k=!1,c[e.effect][1].call(f,function(){b.type="onClose",g.trigger(b)}),a(document).unbind("click."+m).unbind("keydown."+m),l&&a.mask.close();return f}},getOverlay:function(){return j},getTrigger:function(){return d},getClosers:function(){return i},isOpened:function(){return k},getConf:function(){return e}}),a.each("onBeforeLoad,onStart,onLoad,onBeforeClose,onClose".split(","),function(b,c){a.isFunction(e[c])&&a(f).bind(c,e[c]),f[c]=function(b){b&&a(f).bind(c,b);return f}}),i=j.find(e.close||".close"),!i.length&&!e.close&&(i=a("<a class=\"close\"></a>"),j.prepend(i)),i.click(function(a){f.close(a)}),e.load&&f.load()}a.fn.overlay=function(c){var e=this.data("overlay");if(e)return e;a.isFunction(c)&&(c={onBeforeLoad:c}),c=a.extend(!0,{},a.tools.overlay.conf,c),this.each(function(){e=new d(a(this),c),b.push(e),a(this).data("overlay",e)});return c.api?e:this}})(jQuery);(function(a){a.tools=a.tools||{version:"v1.2.6"};var b;b=a.tools.rangeinput={conf:{min:0,max:100,step:"any",steps:0,value:0,precision:undefined,vertical:0,keyboard:!0,progress:!1,speed:100,css:{input:"range",slider:"slider",progress:"progress",handle:"handle"}}};var c,d;a.fn.drag=function(b){document.ondragstart=function(){return!1},b=a.extend({x:!0,y:!0,drag:!0},b),c=c||a(document).bind("mousedown mouseup",function(e){var f=a(e.target);if(e.type=="mousedown"&&f.data("drag")){var g=f.position(),h=e.pageX-g.left,i=e.pageY-g.top,j=!0;c.bind("mousemove.drag",function(a){var c=a.pageX-h,e=a.pageY-i,g={};b.x&&(g.left=c),b.y&&(g.top=e),j&&(f.trigger("dragStart"),j=!1),b.drag&&f.css(g),f.trigger("drag",[e,c]),d=f}),e.preventDefault()}else try{d&&d.trigger("dragEnd")}finally{c.unbind("mousemove.drag"),d=null}});return this.data("drag",!0)};function e(a,b){var c=Math.pow(10,b);return Math.round(a*c)/c}function f(a,b){var c=parseInt(a.css(b),10);if(c)return c;var d=a[0].currentStyle;return d&&d.width&&parseInt(d.width,10)}function g(a){var b=a.data("events");return b&&b.onSlide}function h(b,c){var d=this,h=c.css,i=a("<div><div/><a href='#'/></div>").data("rangeinput",d),j,k,l,m,n;b.before(i);var o=i.addClass(h.slider).find("a").addClass(h.handle),p=i.find("div").addClass(h.progress);a.each("min,max,step,value".split(","),function(a,d){var e=b.attr(d);parseFloat(e)&&(c[d]=parseFloat(e,10))});var q=c.max-c.min,r=c.step=="any"?0:c.step,s=c.precision;if(s===undefined)try{s=r.toString().split(".")[1].length}catch(t){s=0}if(b.attr("type")=="range"){var u=b.clone().wrap("<div/>").parent().html(),v=a(u.replace(/type/i,"type=text data-orig-type"));v.val(c.value),b.replaceWith(v),b=v}b.addClass(h.input);var w=a(d).add(b),x=!0;function y(a,f,g,h){g===undefined?g=f/m*q:h&&(g-=c.min),r&&(g=Math.round(g/r)*r);if(f===undefined||r)f=g*m/q;if(isNaN(g))return d;f=Math.max(0,Math.min(f,m)),g=f/m*q;if(h||!j)g+=c.min;j&&(h?f=m-f:g=c.max-g),g=e(g,s);var i=a.type=="click";if(x&&k!==undefined&&!i){a.type="onSlide",w.trigger(a,[g,f]);if(a.isDefaultPrevented())return d}var l=i?c.speed:0,t=i?function(){a.type="change",w.trigger(a,[g])}:null;j?(o.animate({top:f},l,t),c.progress&&p.animate({height:m-f+o.height()/2},l)):(o.animate({left:f},l,t),c.progress&&p.animate({width:f+o.width()/2},l)),k=g,n=f,b.val(g);return d}a.extend(d,{getValue:function(){return k},setValue:function(b,c){z();return y(c||a.Event("api"),undefined,b,!0)},getConf:function(){return c},getProgress:function(){return p},getHandle:function(){return o},getInput:function(){return b},step:function(b,e){e=e||a.Event();var f=c.step=="any"?1:c.step;d.setValue(k+f*(b||1),e)},stepUp:function(a){return d.step(a||1)},stepDown:function(a){return d.step(-a||-1)}}),a.each("onSlide,change".split(","),function(b,e){a.isFunction(c[e])&&a(d).bind(e,c[e]),d[e]=function(b){b&&a(d).bind(e,b);return d}}),o.drag({drag:!1}).bind("dragStart",function(){z(),x=g(a(d))||g(b)}).bind("drag",function(a,c,d){if(b.is(":disabled"))return!1;y(a,j?c:d)}).bind("dragEnd",function(a){a.isDefaultPrevented()||(a.type="change",w.trigger(a,[k]))}).click(function(a){return a.preventDefault()}),i.click(function(a){if(b.is(":disabled")||a.target==o[0])return a.preventDefault();z();var c=j?o.height()/2:o.width()/2;y(a,j?m-l-c+a.pageY:a.pageX-l-c)}),c.keyboard&&b.keydown(function(c){if(!b.attr("readonly")){var e=c.keyCode,f=a([75,76,38,33,39]).index(e)!=-1,g=a([74,72,40,34,37]).index(e)!=-1;if((f||g)&&!(c.shiftKey||c.altKey||c.ctrlKey)){f?d.step(e==33?10:1,c):g&&d.step(e==34?-10:-1,c);return c.preventDefault()}}}),b.blur(function(b){var c=a(this).val();c!==k&&d.setValue(c,b)}),a.extend(b[0],{stepUp:d.stepUp,stepDown:d.stepDown});function z(){j=c.vertical||f(i,"height")>f(i,"width"),j?(m=f(i,"height")-f(o,"height"),l=i.offset().top+m):(m=f(i,"width")-f(o,"width"),l=i.offset().left)}function A(){z(),d.setValue(c.value!==undefined?c.value:c.min)}A(),m||a(window).load(A)}a.expr[":"].range=function(b){var c=b.getAttribute("type");return c&&c=="range"||a(b).filter("input").data("rangeinput")},a.fn.rangeinput=function(c){if(this.data("rangeinput"))return this;c=a.extend(!0,{},b.conf,c);var d;this.each(function(){var b=new h(a(this),a.extend(!0,{},c)),e=b.getInput().data("rangeinput",b);d=d?d.add(e):e});return d?d:this}})(jQuery);(function(a){a.tools=a.tools||{version:"v1.2.6"},a.tools.scrollable={conf:{activeClass:"active",circular:!1,clonedClass:"cloned",disabledClass:"disabled",easing:"swing",initialIndex:0,item:"> *",items:".items",keyboard:!0,mousewheel:!1,next:".next",prev:".prev",size:1,speed:400,vertical:!1,touch:!0,wheelSpeed:0}};function b(a,b){var c=parseInt(a.css(b),10);if(c)return c;var d=a[0].currentStyle;return d&&d.width&&parseInt(d.width,10)}function c(b,c){var d=a(c);return d.length<2?d:b.parent().find(c)}var d;function e(b,e){var f=this,g=b.add(f),h=b.children(),i=0,j=e.vertical;d||(d=f),h.length>1&&(h=a(e.items,b)),e.size>1&&(e.circular=!1),a.extend(f,{getConf:function(){return e},getIndex:function(){return i},getSize:function(){return f.getItems().size()},getNaviButtons:function(){return n.add(o)},getRoot:function(){return b},getItemWrap:function(){return h},getItems:function(){return h.find(e.item).not("."+e.clonedClass)},move:function(a,b){return f.seekTo(i+a,b)},next:function(a){return f.move(e.size,a)},prev:function(a){return f.move(-e.size,a)},begin:function(a){return f.seekTo(0,a)},end:function(a){return f.seekTo(f.getSize()-1,a)},focus:function(){d=f;return f},addItem:function(b){b=a(b),e.circular?(h.children().last().before(b),h.children().first().replaceWith(b.clone().addClass(e.clonedClass))):(h.append(b),o.removeClass("disabled")),g.trigger("onAddItem",[b]);return f},seekTo:function(b,c,k){b.jquery||(b*=1);if(e.circular&&b===0&&i==-1&&c!==0)return f;if(!e.circular&&b<0||b>f.getSize()||b<-1)return f;var l=b;b.jquery?b=f.getItems().index(b):l=f.getItems().eq(b);var m=a.Event("onBeforeSeek");if(!k){g.trigger(m,[b,c]);if(m.isDefaultPrevented()||!l.length)return f}var n=j?{top:-l.position().top}:{left:-l.position().left};i=b,d=f,c===undefined&&(c=e.speed),h.animate(n,c,e.easing,k||function(){g.trigger("onSeek",[b])});return f}}),a.each(["onBeforeSeek","onSeek","onAddItem"],function(b,c){a.isFunction(e[c])&&a(f).bind(c,e[c]),f[c]=function(b){b&&a(f).bind(c,b);return f}});if(e.circular){var k=f.getItems().slice(-1).clone().prependTo(h),l=f.getItems().eq(1).clone().appendTo(h);k.add(l).addClass(e.clonedClass),f.onBeforeSeek(function(a,b,c){if(!a.isDefaultPrevented()){if(b==-1){f.seekTo(k,c,function(){f.end(0)});return a.preventDefault()}b==f.getSize()&&f.seekTo(l,c,function(){f.begin(0)})}});var m=b.parents().add(b).filter(function(){if(a(this).css("display")==="none")return!0});m.length?(m.show(),f.seekTo(0,0,function(){}),m.hide()):f.seekTo(0,0,function(){})}var n=c(b,e.prev).click(function(a){a.stopPropagation(),f.prev()}),o=c(b,e.next).click(function(a){a.stopPropagation(),f.next()});e.circular||(f.onBeforeSeek(function(a,b){setTimeout(function(){a.isDefaultPrevented()||(n.toggleClass(e.disabledClass,b<=0),o.toggleClass(e.disabledClass,b>=f.getSize()-1))},1)}),e.initialIndex||n.addClass(e.disabledClass)),f.getSize()<2&&n.add(o).addClass(e.disabledClass),e.mousewheel&&a.fn.mousewheel&&b.mousewheel(function(a,b){if(e.mousewheel){f.move(b<0?1:-1,e.wheelSpeed||50);return!1}});if(e.touch){var p={};h[0].ontouchstart=function(a){var b=a.touches[0];p.x=b.clientX,p.y=b.clientY},h[0].ontouchmove=function(a){if(a.touches.length==1&&!h.is(":animated")){var b=a.touches[0],c=p.x-b.clientX,d=p.y-b.clientY;f[j&&d>0||!j&&c>0?"next":"prev"](),a.preventDefault()}}}e.keyboard&&a(document).bind("keydown.scrollable",function(b){if(!(!e.keyboard||b.altKey||b.ctrlKey||b.metaKey||a(b.target).is(":input"))){if(e.keyboard!="static"&&d!=f)return;var c=b.keyCode;if(j&&(c==38||c==40)){f.move(c==38?-1:1);return b.preventDefault()}if(!j&&(c==37||c==39)){f.move(c==37?-1:1);return b.preventDefault()}}}),e.initialIndex&&f.seekTo(e.initialIndex,0,function(){})}a.fn.scrollable=function(b){var c=this.data("scrollable");if(c)return c;b=a.extend({},a.tools.scrollable.conf,b),this.each(function(){c=new e(a(this),b),a(this).data("scrollable",c)});return b.api?c:this}})(jQuery);(function(a){var b=a.tools.scrollable;b.navigator={conf:{navi:".navi",naviItem:null,activeClass:"active",indexed:!1,idPrefix:null,history:!1}};function c(b,c){var d=a(c);return d.length<2?d:b.parent().find(c)}a.fn.navigator=function(d){typeof d=="string"&&(d={navi:d}),d=a.extend({},b.navigator.conf,d);var e;this.each(function(){var b=a(this).data("scrollable"),f=d.navi.jquery?d.navi:c(b.getRoot(),d.navi),g=b.getNaviButtons(),h=d.activeClass,i=d.history&&history.pushState,j=b.getConf().size;b&&(e=b),b.getNaviButtons=function(){return g.add(f)},i&&(history.pushState({i:0}),a(window).bind("popstate",function(a){var c=a.originalEvent.state;c&&b.seekTo(c.i)}));function k(a,c,d){b.seekTo(c),d.preventDefault(),i&&history.pushState({i:c})}function l(){return f.find(d.naviItem||"> *")}function m(b){var c=a("<"+(d.naviItem||"a")+"/>").click(function(c){k(a(this),b,c)});b===0&&c.addClass(h),d.indexed&&c.text(b+1),d.idPrefix&&c.attr("id",d.idPrefix+b);return c.appendTo(f)}l().length?l().each(function(b){a(this).click(function(c){k(a(this),b,c)})}):a.each(b.getItems(),function(a){a%j==0&&m(a)}),b.onBeforeSeek(function(a,b){setTimeout(function(){if(!a.isDefaultPrevented()){var c=b/j,d=l().eq(c);d.length&&l().removeClass(h).eq(c).addClass(h)}},1)}),b.onAddItem(function(a,c){var d=b.getItems().index(c);d%j==0&&m(d)})});return d.api?e:this}})(jQuery);(function(a){a.tools=a.tools||{version:"v1.2.6"},a.tools.tabs={conf:{tabs:"a",current:"current",onBeforeClick:null,onClick:null,effect:"default",initialIndex:0,event:"click",rotate:!1,slideUpSpeed:400,slideDownSpeed:400,history:!1},addEffect:function(a,c){b[a]=c}};var b={"default":function(a,b){this.getPanes().hide().eq(a).show(),b.call()},fade:function(a,b){var c=this.getConf(),d=c.fadeOutSpeed,e=this.getPanes();d?e.fadeOut(d):e.hide(),e.eq(a).fadeIn(c.fadeInSpeed,b)},slide:function(a,b){var c=this.getConf();this.getPanes().slideUp(c.slideUpSpeed),this.getPanes().eq(a).slideDown(c.slideDownSpeed,b)},ajax:function(a,b){this.getPanes().eq(0).load(this.getTabs().eq(a).attr("href"),b)}},c,d;a.tools.tabs.addEffect("horizontal",function(b,e){if(!c){var f=this.getPanes().eq(b),g=this.getCurrentPane();d||(d=this.getPanes().eq(0).width()),c=!0,f.show(),g.animate({width:0},{step:function(a){f.css("width",d-a)},complete:function(){a(this).hide(),e.call(),c=!1}}),g.length||(e.call(),c=!1)}});function e(c,d,e){var f=this,g=c.add(this),h=c.find(e.tabs),i=d.jquery?d:c.children(d),j;h.length||(h=c.children()),i.length||(i=c.parent().find(d)),i.length||(i=a(d)),a.extend(this,{click:function(c,d){var i=h.eq(c);typeof c=="string"&&c.replace("#","")&&(i=h.filter("[href*="+c.replace("#","")+"]"),c=Math.max(h.index(i),0));if(e.rotate){var k=h.length-1;if(c<0)return f.click(k,d);if(c>k)return f.click(0,d)}if(!i.length){if(j>=0)return f;c=e.initialIndex,i=h.eq(c)}if(c===j)return f;d=d||a.Event(),d.type="onBeforeClick",g.trigger(d,[c]);if(!d.isDefaultPrevented()){b[e.effect].call(f,c,function(){j=c,d.type="onClick",g.trigger(d,[c])}),h.removeClass(e.current),i.addClass(e.current);return f}},getConf:function(){return e},getTabs:function(){return h},getPanes:function(){return i},getCurrentPane:function(){return i.eq(j)},getCurrentTab:function(){return h.eq(j)},getIndex:function(){return j},next:function(){return f.click(j+1)},prev:function(){return f.click(j-1)},destroy:function(){h.unbind(e.event).removeClass(e.current),i.find("a[href^=#]").unbind("click.T");return f}}),a.each("onBeforeClick,onClick".split(","),function(b,c){a.isFunction(e[c])&&a(f).bind(c,e[c]),f[c]=function(b){b&&a(f).bind(c,b);return f}}),e.history&&a.fn.history&&(a.tools.history.init(h),e.event="history"),h.each(function(b){a(this).bind(e.event,function(a){f.click(b,a);return a.preventDefault()})}),i.find("a[href^=#]").bind("click.T",function(b){f.click(a(this).attr("href"),b)}),location.hash&&e.tabs=="a"&&c.find("a[href='"+location.hash+"']").length?f.click(location.hash):(e.initialIndex===0||e.initialIndex>0)&&f.click(e.initialIndex)}a.fn.tabs=function(b,c){var d=this.data("tabs");d&&(d.destroy(),this.removeData("tabs")),a.isFunction(c)&&(c={onBeforeClick:c}),c=a.extend({},a.tools.tabs.conf,c),this.each(function(){d=new e(a(this),b,c),a(this).data("tabs",d)});return c.api?d:this}})(jQuery);(function(a){var b;b=a.tools.tabs.slideshow={conf:{next:".forward",prev:".backward",disabledClass:"disabled",autoplay:!1,autopause:!0,interval:3e3,clickable:!0,api:!1}};function c(b,c){var d=this,e=b.add(this),f=b.data("tabs"),g,h=!0;function i(c){var d=a(c);return d.length<2?d:b.parent().find(c)}var j=i(c.next).click(function(){f.next()}),k=i(c.prev).click(function(){f.prev()});function l(){g=setTimeout(function(){f.next()},c.interval)}a.extend(d,{getTabs:function(){return f},getConf:function(){return c},play:function(){if(g)return d;var b=a.Event("onBeforePlay");e.trigger(b);if(b.isDefaultPrevented())return d;h=!1,e.trigger("onPlay"),e.bind("onClick",l),l();return d},pause:function(){if(!g)return d;var b=a.Event("onBeforePause");e.trigger(b);if(b.isDefaultPrevented())return d;g=clearTimeout(g),e.trigger("onPause"),e.unbind("onClick",l);return d},resume:function(){h||d.play()},stop:function(){d.pause(),h=!0}}),a.each("onBeforePlay,onPlay,onBeforePause,onPause".split(","),function(b,e){a.isFunction(c[e])&&a(d).bind(e,c[e]),d[e]=function(b){return a(d).bind(e,b)}}),c.autopause&&f.getTabs().add(j).add(k).add(f.getPanes()).hover(d.pause,d.resume),c.autoplay&&d.play(),c.clickable&&f.getPanes().click(function(){f.next()});if(!f.getConf().rotate){var m=c.disabledClass;f.getIndex()||k.addClass(m),f.onBeforeClick(function(a,b){k.toggleClass(m,!b),j.toggleClass(m,b==f.getTabs().length-1)})}}a.fn.slideshow=function(d){var e=this.data("slideshow");if(e)return e;d=a.extend({},b.conf,d),this.each(function(){e=new c(a(this),d),a(this).data("slideshow",e)});return d.api?e:this}})(jQuery);(function(a){a.tools=a.tools||{version:"v1.2.6"};var b;b=a.tools.expose={conf:{maskId:"exposeMask",loadSpeed:"slow",closeSpeed:"fast",closeOnClick:!0,closeOnEsc:!0,zIndex:9998,opacity:.8,startOpacity:0,color:"#fff",onLoad:null,onClose:null}};function c(){if(a.browser.msie){var b=a(document).height(),c=a(window).height();return[window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,b-c<20?c:b]}return[a(document).width(),a(document).height()]}function d(b){if(b)return b.call(a.mask)}var e,f,g,h,i;a.mask={load:function(j,k){if(g)return this;typeof j=="string"&&(j={color:j}),j=j||h,h=j=a.extend(a.extend({},b.conf),j),e=a("#"+j.maskId),e.length||(e=a("<div/>").attr("id",j.maskId),a("body").append(e));var l=c();e.css({position:"absolute",top:0,left:0,width:l[0],height:l[1],display:"none",opacity:j.startOpacity,zIndex:j.zIndex}),j.color&&e.css("backgroundColor",j.color);if(d(j.onBeforeLoad)===!1)return this;j.closeOnEsc&&a(document).bind("keydown.mask",function(b){b.keyCode==27&&a.mask.close(b)}),j.closeOnClick&&e.bind("click.mask",function(b){a.mask.close(b)}),a(window).bind("resize.mask",function(){a.mask.fit()}),k&&k.length&&(i=k.eq(0).css("zIndex"),a.each(k,function(){var b=a(this);/relative|absolute|fixed/i.test(b.css("position"))||b.css("position","relative")}),f=k.css({zIndex:Math.max(j.zIndex+1,i=="auto"?0:i)})),e.css({display:"block"}).fadeTo(j.loadSpeed,j.opacity,function(){a.mask.fit(),d(j.onLoad),g="full"}),g=!0;return this},close:function(){if(g){if(d(h.onBeforeClose)===!1)return this;e.fadeOut(h.closeSpeed,function(){d(h.onClose),f&&f.css({zIndex:i}),g=!1}),a(document).unbind("keydown.mask"),e.unbind("click.mask"),a(window).unbind("resize.mask")}return this},fit:function(){if(g){var a=c();e.css({width:a[0],height:a[1]})}},getMask:function(){return e},isLoaded:function(a){return a?g=="full":g},getConf:function(){return h},getExposed:function(){return f}},a.fn.mask=function(b){a.mask.load(b);return this},a.fn.expose=function(b){a.mask.load(b,this);return this}})(jQuery);(function(a){var b,c,d,e;a.tools=a.tools||{version:"v1.2.6"},a.tools.history={init:function(g){e||(a.browser.msie&&a.browser.version<"8"?c||(c=a("<iframe/>").attr("src","javascript:false;").hide().get(0),a("body").prepend(c),setInterval(function(){var d=c.contentWindow.document,e=d.location.hash;b!==e&&a(window).trigger("hash",e)},100),f(location.hash||"#")):setInterval(function(){var c=location.hash;c!==b&&a(window).trigger("hash",c)},100),d=d?d.add(g):g,g.click(function(b){var d=a(this).attr("href");c&&f(d);if(d.slice(0,1)!="#"){location.href="#"+d;return b.preventDefault()}}),e=!0)}};function f(a){if(a){var b=c.contentWindow.document;b.open().close(),b.location.hash=a}}a(window).bind("hash",function(c,e){e?d.filter(function(){var b=a(this).attr("href");return b==e||b==e.replace("#","")}).trigger("history",[e]):d.eq(0).trigger("history",[e]),b=e}),a.fn.history=function(b){a.tools.history.init(this);return this.bind("history",b)}})(jQuery);(function(a){a.fn.mousewheel=function(a){return this[a?"bind":"trigger"]("wheel",a)},a.event.special.wheel={setup:function(){a.event.add(this,b,c,{})},teardown:function(){a.event.remove(this,b,c)}};var b=a.browser.mozilla?"DOMMouseScroll"+(a.browser.version<"1.9"?" mousemove":""):"mousewheel";function c(b){switch(b.type){case"mousemove":return a.extend(b.data,{clientX:b.clientX,clientY:b.clientY,pageX:b.pageX,pageY:b.pageY});case"DOMMouseScroll":a.extend(b,b.data),b.delta=-b.detail/3;break;case"mousewheel":b.delta=b.wheelDelta/120}b.type="wheel";return a.event.handle.call(this,b,b.delta)}})(jQuery);(function(a){a.tools=a.tools||{version:"v1.2.6"},a.tools.tooltip={conf:{effect:"toggle",fadeOutSpeed:"fast",predelay:0,delay:30,opacity:1,tip:0,fadeIE:!1,position:["top","center"],offset:[0,0],relative:!1,cancelDefault:!0,events:{def:"mouseenter,mouseleave",input:"focus,blur",widget:"focus mouseenter,blur mouseleave",tooltip:"mouseenter,mouseleave"},layout:"<div/>",tipClass:"tooltip"},addEffect:function(a,c,d){b[a]=[c,d]}};var b={toggle:[function(a){var b=this.getConf(),c=this.getTip(),d=b.opacity;d<1&&c.css({opacity:d}),c.show(),a.call()},function(a){this.getTip().hide(),a.call()}],fade:[function(b){var c=this.getConf();!a.browser.msie||c.fadeIE?this.getTip().fadeTo(c.fadeInSpeed,c.opacity,b):(this.getTip().show(),b())},function(b){var c=this.getConf();!a.browser.msie||c.fadeIE?this.getTip().fadeOut(c.fadeOutSpeed,b):(this.getTip().hide(),b())}]};function c(b,c,d){var e=d.relative?b.position().top:b.offset().top,f=d.relative?b.position().left:b.offset().left,g=d.position[0];e-=c.outerHeight()-d.offset[0],f+=b.outerWidth()+d.offset[1],/iPad/i.test(navigator.userAgent)&&(e-=a(window).scrollTop());var h=c.outerHeight()+b.outerHeight();g=="center"&&(e+=h/2),g=="bottom"&&(e+=h),g=d.position[1];var i=c.outerWidth()+b.outerWidth();g=="center"&&(f-=i/2),g=="left"&&(f-=i);return{top:e,left:f}}function d(d,e){var f=this,g=d.add(f),h,i=0,j=0,k=d.attr("title"),l=d.attr("data-tooltip"),m=b[e.effect],n,o=d.is(":input"),p=o&&d.is(":checkbox, :radio, select, :button, :submit"),q=d.attr("type"),r=e.events[q]||e.events[o?p?"widget":"input":"def"];if(!m)throw"Nonexistent effect \""+e.effect+"\"";r=r.split(/,\s*/);if(r.length!=2)throw"Tooltip: bad events configuration for "+q;d.bind(r[0],function(a){clearTimeout(i),e.predelay?j=setTimeout(function(){f.show(a)},e.predelay):f.show(a)}).bind(r[1],function(a){clearTimeout(j),e.delay?i=setTimeout(function(){f.hide(a)},e.delay):f.hide(a)}),k&&e.cancelDefault&&(d.removeAttr("title"),d.data("title",k)),a.extend(f,{show:function(b){if(!h){l?h=a(l):e.tip?h=a(e.tip).eq(0):k?h=a(e.layout).addClass(e.tipClass).appendTo(document.body).hide().append(k):(h=d.next(),h.length||(h=d.parent().next()));if(!h.length)throw"Cannot find tooltip for "+d}if(f.isShown())return f;h.stop(!0,!0);var o=c(d,h,e);e.tip&&h.html(d.data("title")),b=a.Event(),b.type="onBeforeShow",g.trigger(b,[o]);if(b.isDefaultPrevented())return f;o=c(d,h,e),h.css({position:"absolute",top:o.top,left:o.left}),n=!0,m[0].call(f,function(){b.type="onShow",n="full",g.trigger(b)});var p=e.events.tooltip.split(/,\s*/);h.data("__set")||(h.unbind(p[0]).bind(p[0],function(){clearTimeout(i),clearTimeout(j)}),p[1]&&!d.is("input:not(:checkbox, :radio), textarea")&&h.unbind(p[1]).bind(p[1],function(a){a.relatedTarget!=d[0]&&d.trigger(r[1].split(" ")[0])}),e.tip||h.data("__set",!0));return f},hide:function(c){if(!h||!f.isShown())return f;c=a.Event(),c.type="onBeforeHide",g.trigger(c);if(!c.isDefaultPrevented()){n=!1,b[e.effect][1].call(f,function(){c.type="onHide",g.trigger(c)});return f}},isShown:function(a){return a?n=="full":n},getConf:function(){return e},getTip:function(){return h},getTrigger:function(){return d}}),a.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","),function(b,c){a.isFunction(e[c])&&a(f).bind(c,e[c]),f[c]=function(b){b&&a(f).bind(c,b);return f}})}a.fn.tooltip=function(b){var c=this.data("tooltip");if(c)return c;b=a.extend(!0,{},a.tools.tooltip.conf,b),typeof b.position=="string"&&(b.position=b.position.split(/,?\s/)),this.each(function(){c=new d(a(this),b),a(this).data("tooltip",c)});return b.api?c:this}})(jQuery);(function(a){var b=a.tools.tooltip;b.dynamic={conf:{classNames:"top right bottom left"}};function c(b){var c=a(window),d=c.width()+c.scrollLeft(),e=c.height()+c.scrollTop();return[b.offset().top<=c.scrollTop(),d<=b.offset().left+b.width(),e<=b.offset().top+b.height(),c.scrollLeft()>=b.offset().left]}function d(a){var b=a.length;while(b--)if(a[b])return!1;return!0}a.fn.dynamic=function(e){typeof e=="number"&&(e={speed:e}),e=a.extend({},b.dynamic.conf,e);var f=a.extend(!0,{},e),g=e.classNames.split(/\s/),h;this.each(function(){var b=a(this).tooltip().onBeforeShow(function(b,e){var i=this.getTip(),j=this.getConf();h||(h=[j.position[0],j.position[1],j.offset[0],j.offset[1],a.extend({},j)]),a.extend(j,h[4]),j.position=[h[0],h[1]],j.offset=[h[2],h[3]],i.css({visibility:"hidden",position:"absolute",top:e.top,left:e.left}).show();var k=a.extend(!0,{},f),l=c(i);if(!d(l)){l[2]&&(a.extend(j,k.top),j.position[0]="top",i.addClass(g[0])),l[3]&&(a.extend(j,k.right),j.position[1]="right",i.addClass(g[1])),l[0]&&(a.extend(j,k.bottom),j.position[0]="bottom",i.addClass(g[2])),l[1]&&(a.extend(j,k.left),j.position[1]="left",i.addClass(g[3]));if(l[0]||l[2])j.offset[0]*=-1;if(l[1]||l[3])j.offset[1]*=-1}i.css({visibility:"visible"}).hide()});b.onBeforeShow(function(){var a=this.getConf(),b=this.getTip();setTimeout(function(){a.position=[h[0],h[1]],a.offset=[h[2],h[3]]},0)}),b.onHide(function(){var a=this.getTip();a.removeClass(e.classNames)}),ret=b});return e.api?ret:this}})(jQuery);(function(a){var b=a.tools.tooltip;a.extend(b.conf,{direction:"up",bounce:!1,slideOffset:10,slideInSpeed:200,slideOutSpeed:200,slideFade:!a.browser.msie});var c={up:["-","top"],down:["+","top"],left:["-","left"],right:["+","left"]};b.addEffect("slide",function(a){var b=this.getConf(),d=this.getTip(),e=b.slideFade?{opacity:b.opacity}:{},f=c[b.direction]||c.up;e[f[1]]=f[0]+"="+b.slideOffset,b.slideFade&&d.css({opacity:0}),d.show().animate(e,b.slideInSpeed,a)},function(b){var d=this.getConf(),e=d.slideOffset,f=d.slideFade?{opacity:0}:{},g=c[d.direction]||c.up,h=""+g[0];d.bounce&&(h=h=="+"?"-":"+"),f[g[1]]=h+"="+e,this.getTip().animate(f,d.slideOutSpeed,function(){a(this).hide(),b.call()})})})(jQuery);(function(a){a.tools=a.tools||{version:"v1.2.6"};var b=/\[type=([a-z]+)\]/,c=/^-?[0-9]*(\.[0-9]+)?$/,d=a.tools.dateinput,e=/^([a-z0-9_\.\-\+]+)@([\da-z\.\-]+)\.([a-z\.]{2,6})$/i,f=/^(https?:\/\/)?[\da-z\.\-]+\.[a-z\.]{2,6}[#&+_\?\/\w \.\-=]*$/i,g;g=a.tools.validator={conf:{grouped:!1,effect:"default",errorClass:"invalid",inputEvent:null,errorInputEvent:"keyup",formEvent:"submit",lang:"en",message:"<div/>",messageAttr:"data-message",messageClass:"error",offset:[0,0],position:"center right",singleError:!1,speed:"normal"},messages:{"*":{en:"Please correct this value"}},localize:function(b,c){a.each(c,function(a,c){g.messages[a]=g.messages[a]||{},g.messages[a][b]=c})},localizeFn:function(b,c){g.messages[b]=g.messages[b]||{},a.extend(g.messages[b],c)},fn:function(c,d,e){a.isFunction(d)?e=d:(typeof d=="string"&&(d={en:d}),this.messages[c.key||c]=d);var f=b.exec(c);f&&(c=i(f[1])),j.push([c,e])},addEffect:function(a,b,c){k[a]=[b,c]}};function h(b,c,d){var e=b.offset().top,f=b.offset().left,g=d.position.split(/,?\s+/),h=g[0],i=g[1];e-=c.outerHeight()-d.offset[0],f+=b.outerWidth()+d.offset[1],/iPad/i.test(navigator.userAgent)&&(e-=a(window).scrollTop());var j=c.outerHeight()+b.outerHeight();h=="center"&&(e+=j/2),h=="bottom"&&(e+=j);var k=b.outerWidth();i=="center"&&(f-=(k+c.outerWidth())/2),i=="left"&&(f-=k);return{top:e,left:f}}function i(a){function b(){return this.getAttribute("type")==a}b.key="[type="+a+"]";return b}var j=[],k={"default":[function(b){var c=this.getConf();a.each(b,function(b,d){var e=d.input;e.addClass(c.errorClass);var f=e.data("msg.el");f||(f=a(c.message).addClass(c.messageClass).appendTo(document.body),e.data("msg.el",f)),f.css({visibility:"hidden"}).find("p").remove(),a.each(d.messages,function(b,c){a("<p/>").html(c).appendTo(f)}),f.outerWidth()==f.parent().width()&&f.add(f.find("p")).css({display:"inline"});var g=h(e,f,c);f.css({visibility:"visible",position:"absolute",top:g.top,left:g.left}).fadeIn(c.speed)})},function(b){var c=this.getConf();b.removeClass(c.errorClass).each(function(){var b=a(this).data("msg.el");b&&b.css({visibility:"hidden"})})}]};a.each("email,url,number".split(","),function(b,c){a.expr[":"][c]=function(a){return a.getAttribute("type")===c}}),a.fn.oninvalid=function(a){return this[a?"bind":"trigger"]("OI",a)},g.fn(":email","Please enter a valid email address",function(a,b){return!b||e.test(b)}),g.fn(":url","Please enter a valid URL",function(a,b){return!b||f.test(b)}),g.fn(":number","Please enter a numeric value.",function(a,b){return c.test(b)}),g.fn("[max]","Please enter a value no larger than $1",function(a,b){if(b===""||d&&a.is(":date"))return!0;var c=a.attr("max");return parseFloat(b)<=parseFloat(c)?!0:[c]}),g.fn("[min]","Please enter a value of at least $1",function(a,b){if(b===""||d&&a.is(":date"))return!0;var c=a.attr("min");return parseFloat(b)>=parseFloat(c)?!0:[c]}),g.fn("[required]","Please complete this mandatory field.",function(a,b){if(a.is(":checkbox"))return a.is(":checked");return b}),g.fn("[pattern]",function(a){var b=new RegExp("^"+a.attr("pattern")+"$");return b.test(a.val())});function l(b,c,e){var f=this,i=c.add(f);b=b.not(":button, :image, :reset, :submit"),c.attr("novalidate","novalidate");function l(b,c,d){if(e.grouped||!b.length){var f;if(d===!1||a.isArray(d)){f=g.messages[c.key||c]||g.messages["*"],f=f[e.lang]||g.messages["*"].en;var h=f.match(/\$\d/g);h&&a.isArray(d)&&a.each(h,function(a){f=f.replace(this,d[a])})}else f=d[e.lang]||d;b.push(f)}}a.extend(f,{getConf:function(){return e},getForm:function(){return c},getInputs:function(){return b},reflow:function(){b.each(function(){var b=a(this),c=b.data("msg.el");if(c){var d=h(b,c,e);c.css({top:d.top,left:d.left})}});return f},invalidate:function(c,d){if(!d){var g=[];a.each(c,function(a,c){var d=b.filter("[name='"+a+"']");d.length&&(d.trigger("OI",[c]),g.push({input:d,messages:[c]}))}),c=g,d=a.Event()}d.type="onFail",i.trigger(d,[c]),d.isDefaultPrevented()||k[e.effect][0].call(f,c,d);return f},reset:function(c){c=c||b,c.removeClass(e.errorClass).each(function(){var b=a(this).data("msg.el");b&&(b.remove(),a(this).data("msg.el",null))}).unbind(e.errorInputEvent||"");return f},destroy:function(){c.unbind(e.formEvent+".V").unbind("reset.V"),b.unbind(e.inputEvent+".V").unbind("change.V");return f.reset()},checkValidity:function(c,g){c=c||b,c=c.not(":disabled");if(!c.length)return!0;g=g||a.Event(),g.type="onBeforeValidate",i.trigger(g,[c]);if(g.isDefaultPrevented())return g.result;var h=[];c.not(":radio:not(:checked)").each(function(){var b=[],c=a(this).data("messages",b),k=d&&c.is(":date")?"onHide.v":e.errorInputEvent+".v";c.unbind(k),a.each(j,function(){var a=this,d=a[0];if(c.filter(d).length){var h=a[1].call(f,c,c.val());if(h!==!0){g.type="onBeforeFail",i.trigger(g,[c,d]);if(g.isDefaultPrevented())return!1;var j=c.attr(e.messageAttr);if(j){b=[j];return!1}l(b,d,h)}}}),b.length&&(h.push({input:c,messages:b}),c.trigger("OI",[b]),e.errorInputEvent&&c.bind(k,function(a){f.checkValidity(c,a)}));if(e.singleError&&h.length)return!1});var m=k[e.effect];if(!m)throw"Validator: cannot find effect \""+e.effect+"\"";if(h.length){f.invalidate(h,g);return!1}m[1].call(f,c,g),g.type="onSuccess",i.trigger(g,[c]),c.unbind(e.errorInputEvent+".v");return!0}}),a.each("onBeforeValidate,onBeforeFail,onFail,onSuccess".split(","),function(b,c){a.isFunction(e[c])&&a(f).bind(c,e[c]),f[c]=function(b){b&&a(f).bind(c,b);return f}}),e.formEvent&&c.bind(e.formEvent+".V",function(a){if(!f.checkValidity(null,a))return a.preventDefault();a.target=c,a.type=e.formEvent}),c.bind("reset.V",function(){f.reset()}),b[0]&&b[0].validity&&b.each(function(){this.oninvalid=function(){return!1}}),c[0]&&(c[0].checkValidity=f.checkValidity),e.inputEvent&&b.bind(e.inputEvent+".V",function(b){f.checkValidity(a(this),b)}),b.filter(":checkbox, select").filter("[required]").bind("change.V",function(b){var c=a(this);(this.checked||c.is("select")&&a(this).val())&&k[e.effect][1].call(f,c,b)});var m=b.filter(":radio").change(function(a){f.checkValidity(m,a)});a(window).resize(function(){f.reflow()})}a.fn.validator=function(b){var c=this.data("validator");c&&(c.destroy(),this.removeData("validator")),b=a.extend(!0,{},g.conf,b);if(this.is("form"))return this.each(function(){var d=a(this);c=new l(d.find(":input"),d,b),d.data("validator",c)});c=new l(this,this.eq(0).closest("form"),b);return this.data("validator",c)}})(jQuery);;
/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function($){var types=['DOMMouseScroll','mousewheel'];if($.event.fixHooks){for(var i=types.length;i;){$.event.fixHooks[types[--i]]=$.event.mouseHooks;}}
$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;i;){this.addEventListener(types[--i],handler,false);}}else{this.onmousewheel=handler;}},teardown:function(){if(this.removeEventListener){for(var i=types.length;i;){this.removeEventListener(types[--i],handler,false);}}else{this.onmousewheel=null;}}};$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel");},unmousewheel:function(fn){return this.unbind("mousewheel",fn);}});function handler(event){var orgEvent=event||window.event,args=[].slice.call(arguments,1),delta=0,returnValue=true,deltaX=0,deltaY=0;event=$.event.fix(orgEvent);event.type="mousewheel";if(orgEvent.wheelDelta){delta=orgEvent.wheelDelta/120;}
if(orgEvent.detail){delta=-orgEvent.detail/3;}
deltaY=delta;if(orgEvent.axis!==undefined&&orgEvent.axis===orgEvent.HORIZONTAL_AXIS){deltaY=0;deltaX=-1*delta;}
if(orgEvent.wheelDeltaY!==undefined){deltaY=orgEvent.wheelDeltaY/120;}
if(orgEvent.wheelDeltaX!==undefined){deltaX=-1*orgEvent.wheelDeltaX/120;}
args.unshift(event,delta,deltaX,deltaY);return($.event.dispatch||$.event.handle).apply(this,args);}})(jQuery);;(function($){var mwheelI={pos:[-260,-260]},minDif=3,doc=document,root=doc.documentElement,body=doc.body,longDelay,shortDelay;function unsetPos(){if(this===mwheelI.elem){mwheelI.pos=[-260,-260];mwheelI.elem=false;minDif=3;}}
$.event.special.mwheelIntent={setup:function(){var jElm=$(this).bind('mousewheel',$.event.special.mwheelIntent.handler);if(this!==doc&&this!==root&&this!==body){jElm.bind('mouseleave',unsetPos);}
jElm=null;return true;},teardown:function(){$(this).unbind('mousewheel',$.event.special.mwheelIntent.handler).unbind('mouseleave',unsetPos);return true;},handler:function(e,d){var pos=[e.clientX,e.clientY];if(this===mwheelI.elem||Math.abs(mwheelI.pos[0]-pos[0])>minDif||Math.abs(mwheelI.pos[1]-pos[1])>minDif){mwheelI.elem=this;mwheelI.pos=pos;minDif=250;clearTimeout(shortDelay);shortDelay=setTimeout(function(){minDif=10;},200);clearTimeout(longDelay);longDelay=setTimeout(function(){minDif=3;},1500);e=$.extend({},e,{type:'mwheelIntent'});return $.event.handle.apply(this,arguments);}}};$.fn.extend({mwheelIntent:function(fn){return fn?this.bind("mwheelIntent",fn):this.trigger("mwheelIntent");},unmwheelIntent:function(fn){return this.unbind("mwheelIntent",fn);}});$(function(){body=doc.body;$(doc).bind('mwheelIntent.mwheelIntentDefault',$.noop);});})(jQuery);;(function($){$.jScrollPaneCusel={active:[]};$.fn.jScrollPaneCusel=function(settings)
{settings=$.extend({},$.fn.jScrollPaneCusel.defaults,settings);var rf=function(){return false;};return this.each(function()
{var $this=$(this);var cuselWid=$(this).width();$this.css('overflow','hidden');var paneEle=this;if($(this).parent().is('.jScrollPaneContainer')){var currentScrollPosition=settings.maintainPosition?$this.position().top:0;var $c=$(this).parent();var paneWidth=cuselWid;var paneHeight=$c.outerHeight();var trackHeight=paneHeight;$('>.jScrollPaneTrack, >.jScrollArrowUp, >.jScrollArrowDown',$c).remove();$this.css({'top':0});}else{var currentScrollPosition=0;this.originalPadding=$this.css('paddingTop')+' '+$this.css('paddingRight')+' '+$this.css('paddingBottom')+' '+$this.css('paddingLeft');this.originalSidePaddingTotal=(parseInt($this.css('paddingLeft'))||0)+(parseInt($this.css('paddingRight'))||0);var paneWidth=cuselWid;var paneHeight=$this.innerHeight();var trackHeight=paneHeight;$this.wrap($('<div></div>').attr({'class':'jScrollPaneContainer'}).css({'height':paneHeight+'px','width':paneWidth+'px'}));if(!window.navigator.userProfile)
{var borderWid=parseInt($(this).parent().css("border-left-width"))+parseInt($(this).parent().css("border-right-width"));paneWidth-=borderWid;$(this).css("width",paneWidth+"px").parent().css("width",paneWidth+"px");}
$(document).bind('emchange',function(e,cur,prev)
{$this.jScrollPaneCusel(settings);});}
if(settings.reinitialiseOnImageLoad){var $imagesToLoad=$.data(paneEle,'jScrollPaneImagesToLoad')||$('img',$this);var loadedImages=[];if($imagesToLoad.length){$imagesToLoad.each(function(i,val){$(this).bind('load',function(){if($.inArray(i,loadedImages)==-1){loadedImages.push(val);$imagesToLoad=$.grep($imagesToLoad,function(n,i){return n!=val;});$.data(paneEle,'jScrollPaneImagesToLoad',$imagesToLoad);settings.reinitialiseOnImageLoad=false;$this.jScrollPaneCusel(settings);}}).each(function(i,val){if(this.complete||this.complete===undefined){this.src=this.src;}});});};}
var p=this.originalSidePaddingTotal;var cssToApply={'height':'auto','width':paneWidth-settings.scrollbarWidth-settings.scrollbarMargin-p+'px'}
if(settings.scrollbarOnLeft){cssToApply.paddingLeft=settings.scrollbarMargin+settings.scrollbarWidth+'px';}else{cssToApply.paddingRight=settings.scrollbarMargin+'px';}
$this.css(cssToApply);var a=$this.find("span").eq(0).height();var contentHeight2=$this.find("span").size()*a;var contentHeight=Math.ceil($this.find("span").size()/(Math.floor(($this.width()/($this.find("span").eq(0).width())))))*a;var percentInView=paneHeight/contentHeight;if(percentInView<.99){var $container=$this.parent();$container.append($('<div></div>').attr({'class':'jScrollPaneTrack'}).css({'width':settings.scrollbarWidth+'px'}).append($('<div></div>').attr({'class':'jScrollPaneDrag'}).css({'width':settings.scrollbarWidth+'px'}).append($('<div></div>').attr({'class':'jScrollPaneDragTop'}).css({'width':settings.scrollbarWidth+'px'}),$('<div></div>').attr({'class':'jScrollPaneDragBottom'}).css({'width':settings.scrollbarWidth+'px'}))));var $track=$('>.jScrollPaneTrack',$container);var $drag=$('>.jScrollPaneTrack .jScrollPaneDrag',$container);if(settings.showArrows){var currentArrowButton;var currentArrowDirection;var currentArrowInterval;var currentArrowInc;var whileArrowButtonDown=function()
{if(currentArrowInc>4||currentArrowInc%4==0){positionDrag(dragPosition+currentArrowDirection*mouseWheelMultiplier);}
currentArrowInc++;};var onArrowMouseUp=function(event)
{$('html').unbind('mouseup',onArrowMouseUp);currentArrowButton.removeClass('jScrollActiveArrowButton');clearInterval(currentArrowInterval);};var onArrowMouseDown=function(){$('html').bind('mouseup',onArrowMouseUp);currentArrowButton.addClass('jScrollActiveArrowButton');currentArrowInc=0;whileArrowButtonDown();currentArrowInterval=setInterval(whileArrowButtonDown,100);};$container.append($('<div></div>').attr({'class':'jScrollArrowUp'}).css({'width':settings.scrollbarWidth+'px'}).bind('mousedown',function()
{currentArrowButton=$(this);currentArrowDirection=-1;onArrowMouseDown();this.blur();return false;}).bind('click',rf),$('<div></div>').attr({'class':'jScrollArrowDown'}).css({'width':settings.scrollbarWidth+'px'}).bind('mousedown',function()
{currentArrowButton=$(this);currentArrowDirection=1;onArrowMouseDown();this.blur();return false;}).bind('click',rf));var $upArrow=$('>.jScrollArrowUp',$container);var $downArrow=$('>.jScrollArrowDown',$container);if(settings.arrowSize){trackHeight=paneHeight-settings.arrowSize-settings.arrowSize;$track.css({'height':trackHeight+'px',top:settings.arrowSize+'px'})}else{var topArrowHeight=$upArrow.height();settings.arrowSize=topArrowHeight;trackHeight=paneHeight-topArrowHeight-$downArrow.height();$track.css({'height':trackHeight+'px',top:topArrowHeight+'px'})}}
var $pane=$(this).css({'position':'absolute','overflow':'visible'});var currentOffset;var maxY;var mouseWheelMultiplier;var dragPosition=0;var dragMiddle=percentInView*paneHeight/2;var getPos=function(event,c){var p=c=='X'?'Left':'Top';return event['page'+c]||(event['client'+c]+(document.documentElement['scroll'+p]||document.body['scroll'+p]))||0;};var ignoreNativeDrag=function(){return false;};var initDrag=function()
{ceaseAnimation();currentOffset=$drag.offset(false);currentOffset.top-=dragPosition;maxY=trackHeight-$drag[0].offsetHeight;mouseWheelMultiplier=2*settings.wheelSpeed*maxY/contentHeight;};var onStartDrag=function(event)
{initDrag();dragMiddle=getPos(event,'Y')-dragPosition-currentOffset.top;$('html').bind('mouseup',onStopDrag).bind('mousemove',updateScroll);if($.browser.msie){$('html').bind('dragstart',ignoreNativeDrag).bind('selectstart',ignoreNativeDrag);}
return false;};var onStopDrag=function()
{$('html').unbind('mouseup',onStopDrag).unbind('mousemove',updateScroll);dragMiddle=percentInView*paneHeight/2;if($.browser.msie){$('html').unbind('dragstart',ignoreNativeDrag).unbind('selectstart',ignoreNativeDrag);}};var positionDrag=function(destY)
{destY=destY<0?0:(destY>maxY?maxY:destY);dragPosition=destY;$drag.css({'top':destY+'px'});var p=destY/maxY;$pane.css({'top':((paneHeight-contentHeight)*p)+'px'});$this.trigger('scroll');if(settings.showArrows){$upArrow[destY==0?'addClass':'removeClass']('disabled');$downArrow[destY==maxY?'addClass':'removeClass']('disabled');}};var updateScroll=function(e)
{positionDrag(getPos(e,'Y')-currentOffset.top-dragMiddle);};var dragH=Math.max(Math.min(percentInView*(paneHeight-settings.arrowSize*2),settings.dragMaxHeight),settings.dragMinHeight);$drag.css({'height':dragH+'px'}).bind('mousedown',onStartDrag);var trackScrollInterval;var trackScrollInc;var trackScrollMousePos;var doTrackScroll=function()
{if(trackScrollInc>8||trackScrollInc%4==0){positionDrag((dragPosition-((dragPosition-trackScrollMousePos)/2)));}
trackScrollInc++;};var onStopTrackClick=function()
{clearInterval(trackScrollInterval);$('html').unbind('mouseup',onStopTrackClick).unbind('mousemove',onTrackMouseMove);};var onTrackMouseMove=function(event)
{trackScrollMousePos=getPos(event,'Y')-currentOffset.top-dragMiddle;};var onTrackClick=function(event)
{initDrag();onTrackMouseMove(event);trackScrollInc=0;$('html').bind('mouseup',onStopTrackClick).bind('mousemove',onTrackMouseMove);trackScrollInterval=setInterval(doTrackScroll,100);doTrackScroll();};$track.bind('mousedown',onTrackClick);$container.bind('mousewheel',function(event,delta){initDrag();ceaseAnimation();var d=dragPosition;positionDrag(dragPosition-delta*mouseWheelMultiplier);var dragOccured=d!=dragPosition;return false;});var _animateToPosition;var _animateToInterval;function animateToPosition()
{var diff=(_animateToPosition-dragPosition)/settings.animateStep;if(diff>1||diff<-1){positionDrag(dragPosition+diff);}else{positionDrag(_animateToPosition);ceaseAnimation();}}
var ceaseAnimation=function()
{if(_animateToInterval){clearInterval(_animateToInterval);delete _animateToPosition;}};var scrollTo=function(pos,preventAni)
{if(typeof pos=="string"){$e=$(pos,$this);if(!$e.length)return;pos=$e.offset().top-$this.offset().top;}
$container.scrollTop(0);ceaseAnimation();var destDragPosition=-pos/(paneHeight-contentHeight)*maxY;if(preventAni||!settings.animateTo){positionDrag(destDragPosition);}else{_animateToPosition=destDragPosition;_animateToInterval=setInterval(animateToPosition,settings.animateInterval);}};$this[0].scrollTo=scrollTo;$this[0].scrollBy=function(delta)
{var currentPos=-parseInt($pane.css('top'))||0;scrollTo(currentPos+delta);};initDrag();scrollTo(-currentScrollPosition,true);$('*',this).bind('focus',function(event)
{var $e=$(this);var eleTop=0;while($e[0]!=$this[0]){eleTop+=$e.position().top;$e=$e.offsetParent();}
var viewportTop=-parseInt($pane.css('top'))||0;var maxVisibleEleTop=viewportTop+paneHeight;var eleInView=eleTop>viewportTop&&eleTop<maxVisibleEleTop;if(!eleInView){var destPos=eleTop-settings.scrollbarMargin;if(eleTop>viewportTop){destPos+=$(this).height()+15+settings.scrollbarMargin-paneHeight;}
scrollTo(destPos);}})
if(location.hash){scrollTo(location.hash);}
$.jScrollPaneCusel.active.push($this[0]);}else{$this.css({'height':paneHeight+'px','width':paneWidth-this.originalSidePaddingTotal+'px','padding':this.originalPadding});$this.parent().unbind('mousewheel');}})};$.fn.jScrollPaneRemoveCusel=function()
{$(this).each(function()
{$this=$(this);var $c=$this.parent();if($c.is('.jScrollPaneContainer')){$this.css({'top':'','height':'','width':'','padding':'','overflow':'','position':''});$this.attr('style',$this.data('originalStyleTag'));$c.after($this).remove();}});}
$.fn.jScrollPaneCusel.defaults={scrollbarWidth:10,scrollbarMargin:5,wheelSpeed:8,showArrows:false,arrowSize:0,animateTo:false,dragMinHeight:1,dragMaxHeight:99999,animateInterval:100,animateStep:3,maintainPosition:true,scrollbarOnLeft:false,reinitialiseOnImageLoad:false};$(window).bind('unload',function(){var els=$.jScrollPaneCusel.active;for(var i=0;i<els.length;i++){els[i].scrollTo=els[i].scrollBy=null;}});})(jQuery);;
/*!
 * jScrollPane - v2.0.0beta11 - 2011-06-11
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2010 Kelvin Luck
 * Dual licensed under the MIT and GPL licenses.
 */
(function($,window,undefined){$.fn.jScrollPane=function(settings)
{function JScrollPane(elem,s)
{var settings,jsp=this,pane,paneWidth,paneHeight,container,contentWidth,contentHeight,percentInViewH,percentInViewV,isScrollableV,isScrollableH,verticalDrag,dragMaxY,verticalDragPosition,horizontalDrag,dragMaxX,horizontalDragPosition,verticalBar,verticalTrack,scrollbarWidth,verticalTrackHeight,verticalDragHeight,arrowUp,arrowDown,horizontalBar,horizontalTrack,horizontalTrackWidth,horizontalDragWidth,arrowLeft,arrowRight,reinitialiseInterval,originalPadding,originalPaddingTotalWidth,previousContentWidth,wasAtTop=true,wasAtLeft=true,wasAtBottom=false,wasAtRight=false,originalElement=elem.clone(false,false).empty(),mwEvent=$.fn.mwheelIntent?'mwheelIntent.jsp':'mousewheel.jsp';originalPadding=elem.css('paddingTop')+' '+
elem.css('paddingRight')+' '+
elem.css('paddingBottom')+' '+
elem.css('paddingLeft');originalPaddingTotalWidth=(parseInt(elem.css('paddingLeft'),10)||0)+
(parseInt(elem.css('paddingRight'),10)||0);function initialise(s)
{var isMaintainingPositon,lastContentX,lastContentY,hasContainingSpaceChanged,originalScrollTop,originalScrollLeft,maintainAtBottom=false,maintainAtRight=false;settings=s;if(pane===undefined){originalScrollTop=elem.scrollTop();originalScrollLeft=elem.scrollLeft();elem.css({overflow:'hidden',padding:0});paneWidth=elem.innerWidth()+originalPaddingTotalWidth;paneHeight=elem.innerHeight();elem.width(paneWidth);pane=$('<div class="jspPane" />').css('padding',originalPadding).append(elem.children());container=$('<div class="jspContainer" />').css({'width':paneWidth+'px','height':paneHeight+'px'}).append(pane).appendTo(elem);}else{elem.css('width','');maintainAtBottom=settings.stickToBottom&&isCloseToBottom();maintainAtRight=settings.stickToRight&&isCloseToRight();hasContainingSpaceChanged=elem.innerWidth()+originalPaddingTotalWidth!=paneWidth||elem.outerHeight()!=paneHeight;if(hasContainingSpaceChanged){paneWidth=elem.innerWidth()+originalPaddingTotalWidth;paneHeight=elem.innerHeight();container.css({width:paneWidth+'px',height:paneHeight+'px'});}
if(!hasContainingSpaceChanged&&previousContentWidth==contentWidth&&pane.outerHeight()==contentHeight){elem.width(paneWidth);return;}
previousContentWidth=contentWidth;pane.css('width','');elem.width(paneWidth);container.find('>.jspVerticalBar,>.jspHorizontalBar').remove().end();}
pane.css('overflow','auto');if(s.contentWidth){contentWidth=s.contentWidth;}else{contentWidth=pane[0].scrollWidth;}
contentHeight=pane[0].scrollHeight;pane.css('overflow','');percentInViewH=contentWidth/paneWidth;percentInViewV=contentHeight/paneHeight;isScrollableV=percentInViewV>1;isScrollableH=percentInViewH>1;if(!(isScrollableH||isScrollableV)){elem.removeClass('jspScrollable');pane.css({top:0,width:container.width()-originalPaddingTotalWidth});removeMousewheel();removeFocusHandler();removeKeyboardNav();removeClickOnTrack();unhijackInternalLinks();}else{elem.addClass('jspScrollable');isMaintainingPositon=settings.maintainPosition&&(verticalDragPosition||horizontalDragPosition);if(isMaintainingPositon){lastContentX=contentPositionX();lastContentY=contentPositionY();}
initialiseVerticalScroll();initialiseHorizontalScroll();resizeScrollbars();if(isMaintainingPositon){scrollToX(maintainAtRight?(contentWidth-paneWidth):lastContentX,false);scrollToY(maintainAtBottom?(contentHeight-paneHeight):lastContentY,false);}
initFocusHandler();initMousewheel();initTouch();if(settings.enableKeyboardNavigation){initKeyboardNav();}
if(settings.clickOnTrack){initClickOnTrack();}
observeHash();if(settings.hijackInternalLinks){hijackInternalLinks();}}
if(settings.autoReinitialise&&!reinitialiseInterval){reinitialiseInterval=setInterval(function()
{initialise(settings);},settings.autoReinitialiseDelay);}else if(!settings.autoReinitialise&&reinitialiseInterval){clearInterval(reinitialiseInterval);}
originalScrollTop&&elem.scrollTop(0)&&scrollToY(originalScrollTop,false);originalScrollLeft&&elem.scrollLeft(0)&&scrollToX(originalScrollLeft,false);elem.trigger('jsp-initialised',[isScrollableH||isScrollableV]);}
function initialiseVerticalScroll()
{if(isScrollableV){container.append($('<div class="jspVerticalBar" />').append($('<div class="jspCap jspCapTop" />'),$('<div class="jspTrack" />').append($('<div class="jspDrag" />').append($('<div class="jspDragTop" />'),$('<div class="jspDragBottom" />'))),$('<div class="jspCap jspCapBottom" />')));verticalBar=container.find('>.jspVerticalBar');verticalTrack=verticalBar.find('>.jspTrack');verticalDrag=verticalTrack.find('>.jspDrag');if(settings.showArrows){arrowUp=$('<a class="jspArrow jspArrowUp" />').bind('mousedown.jsp',getArrowScroll(0,-1)).bind('click.jsp',nil);arrowDown=$('<a class="jspArrow jspArrowDown" />').bind('mousedown.jsp',getArrowScroll(0,1)).bind('click.jsp',nil);if(settings.arrowScrollOnHover){arrowUp.bind('mouseover.jsp',getArrowScroll(0,-1,arrowUp));arrowDown.bind('mouseover.jsp',getArrowScroll(0,1,arrowDown));}
appendArrows(verticalTrack,settings.verticalArrowPositions,arrowUp,arrowDown);}
verticalTrackHeight=paneHeight;container.find('>.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow').each(function()
{verticalTrackHeight-=$(this).outerHeight();});verticalDrag.hover(function()
{verticalDrag.addClass('jspHover');},function()
{verticalDrag.removeClass('jspHover');}).bind('mousedown.jsp',function(e)
{$('html').bind('dragstart.jsp selectstart.jsp',nil);verticalDrag.addClass('jspActive');var startY=e.pageY-verticalDrag.position().top;$('html').bind('mousemove.jsp',function(e)
{positionDragY(e.pageY-startY,false);}).bind('mouseup.jsp mouseleave.jsp',cancelDrag);return false;});sizeVerticalScrollbar();}}
function sizeVerticalScrollbar()
{verticalTrack.height(verticalTrackHeight+'px');verticalDragPosition=0;scrollbarWidth=settings.verticalGutter+verticalTrack.outerWidth();pane.width(paneWidth-scrollbarWidth-originalPaddingTotalWidth);try{if(verticalBar.position().left===0){pane.css('margin-left',scrollbarWidth+'px');}}catch(err){}}
function initialiseHorizontalScroll()
{if(isScrollableH){container.append($('<div class="jspHorizontalBar" />').append($('<div class="jspCap jspCapLeft" />'),$('<div class="jspTrack" />').append($('<div class="jspDrag" />').append($('<div class="jspDragLeft" />'),$('<div class="jspDragRight" />'))),$('<div class="jspCap jspCapRight" />')));horizontalBar=container.find('>.jspHorizontalBar');horizontalTrack=horizontalBar.find('>.jspTrack');horizontalDrag=horizontalTrack.find('>.jspDrag');if(settings.showArrows){arrowLeft=$('<a class="jspArrow jspArrowLeft" />').bind('mousedown.jsp',getArrowScroll(-1,0)).bind('click.jsp',nil);arrowRight=$('<a class="jspArrow jspArrowRight" />').bind('mousedown.jsp',getArrowScroll(1,0)).bind('click.jsp',nil);if(settings.arrowScrollOnHover){arrowLeft.bind('mouseover.jsp',getArrowScroll(-1,0,arrowLeft));arrowRight.bind('mouseover.jsp',getArrowScroll(1,0,arrowRight));}
appendArrows(horizontalTrack,settings.horizontalArrowPositions,arrowLeft,arrowRight);}
horizontalDrag.hover(function()
{horizontalDrag.addClass('jspHover');},function()
{horizontalDrag.removeClass('jspHover');}).bind('mousedown.jsp',function(e)
{$('html').bind('dragstart.jsp selectstart.jsp',nil);horizontalDrag.addClass('jspActive');var startX=e.pageX-horizontalDrag.position().left;$('html').bind('mousemove.jsp',function(e)
{positionDragX(e.pageX-startX,false);}).bind('mouseup.jsp mouseleave.jsp',cancelDrag);return false;});horizontalTrackWidth=container.innerWidth();sizeHorizontalScrollbar();}}
function sizeHorizontalScrollbar()
{container.find('>.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow').each(function()
{horizontalTrackWidth-=$(this).outerWidth();});horizontalTrack.width(horizontalTrackWidth+'px');horizontalDragPosition=0;}
function resizeScrollbars()
{if(isScrollableH&&isScrollableV){var horizontalTrackHeight=horizontalTrack.outerHeight(),verticalTrackWidth=verticalTrack.outerWidth();verticalTrackHeight-=horizontalTrackHeight;$(horizontalBar).find('>.jspCap:visible,>.jspArrow').each(function()
{horizontalTrackWidth+=$(this).outerWidth();});horizontalTrackWidth-=verticalTrackWidth;paneHeight-=verticalTrackWidth;paneWidth-=horizontalTrackHeight;horizontalTrack.parent().append($('<div class="jspCorner" />').css('width',horizontalTrackHeight+'px'));sizeVerticalScrollbar();sizeHorizontalScrollbar();}
if(isScrollableH){pane.width((container.outerWidth()-originalPaddingTotalWidth)+'px');}
contentHeight=pane.outerHeight();percentInViewV=contentHeight/paneHeight;if(isScrollableH){horizontalDragWidth=Math.ceil(1/percentInViewH*horizontalTrackWidth);if(horizontalDragWidth>settings.horizontalDragMaxWidth){horizontalDragWidth=settings.horizontalDragMaxWidth;}else if(horizontalDragWidth<settings.horizontalDragMinWidth){horizontalDragWidth=settings.horizontalDragMinWidth;}
horizontalDrag.width(horizontalDragWidth+'px');dragMaxX=horizontalTrackWidth-horizontalDragWidth;_positionDragX(horizontalDragPosition);}
if(isScrollableV){verticalDragHeight=Math.ceil(1/percentInViewV*verticalTrackHeight);if(verticalDragHeight>settings.verticalDragMaxHeight){verticalDragHeight=settings.verticalDragMaxHeight;}else if(verticalDragHeight<settings.verticalDragMinHeight){verticalDragHeight=settings.verticalDragMinHeight;}
verticalDrag.height(verticalDragHeight+'px');dragMaxY=verticalTrackHeight-verticalDragHeight;_positionDragY(verticalDragPosition);}}
function appendArrows(ele,p,a1,a2)
{var p1="before",p2="after",aTemp;if(p=="os"){p=/Mac/.test(navigator.platform)?"after":"split";}
if(p==p1){p2=p;}else if(p==p2){p1=p;aTemp=a1;a1=a2;a2=aTemp;}
ele[p1](a1)[p2](a2);}
function getArrowScroll(dirX,dirY,ele)
{return function()
{arrowScroll(dirX,dirY,this,ele);this.blur();return false;};}
function arrowScroll(dirX,dirY,arrow,ele)
{arrow=$(arrow).addClass('jspActive');var eve,scrollTimeout,isFirst=true,doScroll=function()
{if(dirX!==0){jsp.scrollByX(dirX*settings.arrowButtonSpeed);}
if(dirY!==0){jsp.scrollByY(dirY*settings.arrowButtonSpeed);}
scrollTimeout=setTimeout(doScroll,isFirst?settings.initialDelay:settings.arrowRepeatFreq);isFirst=false;};doScroll();eve=ele?'mouseout.jsp':'mouseup.jsp';ele=ele||$('html');ele.bind(eve,function()
{arrow.removeClass('jspActive');scrollTimeout&&clearTimeout(scrollTimeout);scrollTimeout=null;ele.unbind(eve);});}
function initClickOnTrack()
{removeClickOnTrack();if(isScrollableV){verticalTrack.bind('mousedown.jsp',function(e)
{if(e.originalTarget===undefined||e.originalTarget==e.currentTarget){var clickedTrack=$(this),offset=clickedTrack.offset(),direction=e.pageY-offset.top-verticalDragPosition,scrollTimeout,isFirst=true,doScroll=function()
{var offset=clickedTrack.offset(),pos=e.pageY-offset.top-verticalDragHeight/2,contentDragY=paneHeight*settings.scrollPagePercent,dragY=dragMaxY*contentDragY/(contentHeight-paneHeight);if(direction<0){if(verticalDragPosition-dragY>pos){jsp.scrollByY(-contentDragY);}else{positionDragY(pos);}}else if(direction>0){if(verticalDragPosition+dragY<pos){jsp.scrollByY(contentDragY);}else{positionDragY(pos);}}else{cancelClick();return;}
scrollTimeout=setTimeout(doScroll,isFirst?settings.initialDelay:settings.trackClickRepeatFreq);isFirst=false;},cancelClick=function()
{scrollTimeout&&clearTimeout(scrollTimeout);scrollTimeout=null;$(document).unbind('mouseup.jsp',cancelClick);};doScroll();$(document).bind('mouseup.jsp',cancelClick);return false;}});}
if(isScrollableH){horizontalTrack.bind('mousedown.jsp',function(e)
{if(e.originalTarget===undefined||e.originalTarget==e.currentTarget){var clickedTrack=$(this),offset=clickedTrack.offset(),direction=e.pageX-offset.left-horizontalDragPosition,scrollTimeout,isFirst=true,doScroll=function()
{var offset=clickedTrack.offset(),pos=e.pageX-offset.left-horizontalDragWidth/2,contentDragX=paneWidth*settings.scrollPagePercent,dragX=dragMaxX*contentDragX/(contentWidth-paneWidth);if(direction<0){if(horizontalDragPosition-dragX>pos){jsp.scrollByX(-contentDragX);}else{positionDragX(pos);}}else if(direction>0){if(horizontalDragPosition+dragX<pos){jsp.scrollByX(contentDragX);}else{positionDragX(pos);}}else{cancelClick();return;}
scrollTimeout=setTimeout(doScroll,isFirst?settings.initialDelay:settings.trackClickRepeatFreq);isFirst=false;},cancelClick=function()
{scrollTimeout&&clearTimeout(scrollTimeout);scrollTimeout=null;$(document).unbind('mouseup.jsp',cancelClick);};doScroll();$(document).bind('mouseup.jsp',cancelClick);return false;}});}}
function removeClickOnTrack()
{if(horizontalTrack){horizontalTrack.unbind('mousedown.jsp');}
if(verticalTrack){verticalTrack.unbind('mousedown.jsp');}}
function cancelDrag()
{$('html').unbind('dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp');if(verticalDrag){verticalDrag.removeClass('jspActive');}
if(horizontalDrag){horizontalDrag.removeClass('jspActive');}}
function positionDragY(destY,animate)
{if(!isScrollableV){return;}
if(destY<0){destY=0;}else if(destY>dragMaxY){destY=dragMaxY;}
if(animate===undefined){animate=settings.animateScroll;}
if(animate){jsp.animate(verticalDrag,'top',destY,_positionDragY);}else{verticalDrag.css('top',destY);_positionDragY(destY);}}
function _positionDragY(destY)
{if(destY===undefined){destY=verticalDrag.position().top;}
container.scrollTop(0);verticalDragPosition=destY;var isAtTop=verticalDragPosition===0,isAtBottom=verticalDragPosition==dragMaxY,percentScrolled=destY/dragMaxY,destTop=-percentScrolled*(contentHeight-paneHeight);if(wasAtTop!=isAtTop||wasAtBottom!=isAtBottom){wasAtTop=isAtTop;wasAtBottom=isAtBottom;elem.trigger('jsp-arrow-change',[wasAtTop,wasAtBottom,wasAtLeft,wasAtRight]);}
updateVerticalArrows(isAtTop,isAtBottom);pane.css('top',destTop);elem.trigger('jsp-scroll-y',[-destTop,isAtTop,isAtBottom]).trigger('scroll');}
function positionDragX(destX,animate)
{if(!isScrollableH){return;}
if(destX<0){destX=0;}else if(destX>dragMaxX){destX=dragMaxX;}
if(animate===undefined){animate=settings.animateScroll;}
if(animate){jsp.animate(horizontalDrag,'left',destX,_positionDragX);}else{horizontalDrag.css('left',destX);_positionDragX(destX);}}
function _positionDragX(destX)
{if(destX===undefined){destX=horizontalDrag.position().left;}
container.scrollTop(0);horizontalDragPosition=destX;var isAtLeft=horizontalDragPosition===0,isAtRight=horizontalDragPosition==dragMaxX,percentScrolled=destX/dragMaxX,destLeft=-percentScrolled*(contentWidth-paneWidth);if(wasAtLeft!=isAtLeft||wasAtRight!=isAtRight){wasAtLeft=isAtLeft;wasAtRight=isAtRight;elem.trigger('jsp-arrow-change',[wasAtTop,wasAtBottom,wasAtLeft,wasAtRight]);}
updateHorizontalArrows(isAtLeft,isAtRight);pane.css('left',destLeft);elem.trigger('jsp-scroll-x',[-destLeft,isAtLeft,isAtRight]).trigger('scroll');}
function updateVerticalArrows(isAtTop,isAtBottom)
{if(settings.showArrows){arrowUp[isAtTop?'addClass':'removeClass']('jspDisabled');arrowDown[isAtBottom?'addClass':'removeClass']('jspDisabled');}}
function updateHorizontalArrows(isAtLeft,isAtRight)
{if(settings.showArrows){arrowLeft[isAtLeft?'addClass':'removeClass']('jspDisabled');arrowRight[isAtRight?'addClass':'removeClass']('jspDisabled');}}
function scrollToY(destY,animate)
{var percentScrolled=destY/(contentHeight-paneHeight);positionDragY(percentScrolled*dragMaxY,animate);}
function scrollToX(destX,animate)
{var percentScrolled=destX/(contentWidth-paneWidth);positionDragX(percentScrolled*dragMaxX,animate);}
function scrollToElement(ele,stickToTop,animate)
{var e,eleHeight,eleWidth,eleTop=0,eleLeft=0,viewportTop,viewportLeft,maxVisibleEleTop,maxVisibleEleLeft,destY,destX;try{e=$(ele);}catch(err){return;}
eleHeight=e.outerHeight();eleWidth=e.outerWidth();container.scrollTop(0);container.scrollLeft(0);while(!e.is('.jspPane')){eleTop+=e.position().top;eleLeft+=e.position().left;e=e.offsetParent();if(/^body|html$/i.test(e[0].nodeName)){return;}}
viewportTop=contentPositionY();maxVisibleEleTop=viewportTop+paneHeight;if(eleTop<viewportTop||stickToTop){destY=eleTop-settings.verticalGutter;}else if(eleTop+eleHeight>maxVisibleEleTop){destY=eleTop-paneHeight+eleHeight+settings.verticalGutter;}
if(destY){scrollToY(destY,animate);}
viewportLeft=contentPositionX();maxVisibleEleLeft=viewportLeft+paneWidth;if(eleLeft<viewportLeft||stickToTop){destX=eleLeft-settings.horizontalGutter;}else if(eleLeft+eleWidth>maxVisibleEleLeft){destX=eleLeft-paneWidth+eleWidth+settings.horizontalGutter;}
if(destX){scrollToX(destX,animate);}}
function contentPositionX()
{return-pane.position().left;}
function contentPositionY()
{return-pane.position().top;}
function isCloseToBottom()
{var scrollableHeight=contentHeight-paneHeight;return(scrollableHeight>20)&&(scrollableHeight-contentPositionY()<10);}
function isCloseToRight()
{var scrollableWidth=contentWidth-paneWidth;return(scrollableWidth>20)&&(scrollableWidth-contentPositionX()<10);}
function initMousewheel()
{container.unbind(mwEvent).bind(mwEvent,function(event,delta,deltaX,deltaY){var dX=horizontalDragPosition,dY=verticalDragPosition;jsp.scrollBy(deltaX*settings.mouseWheelSpeed,-deltaY*settings.mouseWheelSpeed,false);return dX==horizontalDragPosition&&dY==verticalDragPosition;});}
function removeMousewheel()
{container.unbind(mwEvent);}
function nil()
{return false;}
function initFocusHandler()
{pane.find(':input,a').unbind('focus.jsp').bind('focus.jsp',function(e)
{scrollToElement(e.target,false);});}
function removeFocusHandler()
{pane.find(':input,a').unbind('focus.jsp');}
function initKeyboardNav()
{var keyDown,elementHasScrolled,validParents=[];isScrollableH&&validParents.push(horizontalBar[0]);isScrollableV&&validParents.push(verticalBar[0]);pane.focus(function()
{elem.focus();});elem.attr('tabindex',0).unbind('keydown.jsp keypress.jsp').bind('keydown.jsp',function(e)
{if(e.target!==this&&!(validParents.length&&$(e.target).closest(validParents).length)){return;}
var dX=horizontalDragPosition,dY=verticalDragPosition;switch(e.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:keyDown=e.keyCode;keyDownHandler();break;case 35:scrollToY(contentHeight-paneHeight);keyDown=null;break;case 36:scrollToY(0);keyDown=null;break;}
elementHasScrolled=e.keyCode==keyDown&&dX!=horizontalDragPosition||dY!=verticalDragPosition;return!elementHasScrolled;}).bind('keypress.jsp',function(e)
{if(e.keyCode==keyDown){keyDownHandler();}
return!elementHasScrolled;});if(settings.hideFocus){elem.css('outline','none');if('hideFocus'in container[0]){elem.attr('hideFocus',true);}}else{elem.css('outline','');if('hideFocus'in container[0]){elem.attr('hideFocus',false);}}
function keyDownHandler()
{var dX=horizontalDragPosition,dY=verticalDragPosition;switch(keyDown){case 40:jsp.scrollByY(settings.keyboardSpeed,false);break;case 38:jsp.scrollByY(-settings.keyboardSpeed,false);break;case 34:case 32:jsp.scrollByY(paneHeight*settings.scrollPagePercent,false);break;case 33:jsp.scrollByY(-paneHeight*settings.scrollPagePercent,false);break;case 39:jsp.scrollByX(settings.keyboardSpeed,false);break;case 37:jsp.scrollByX(-settings.keyboardSpeed,false);break;}
elementHasScrolled=dX!=horizontalDragPosition||dY!=verticalDragPosition;return elementHasScrolled;}}
function removeKeyboardNav()
{elem.attr('tabindex','-1').removeAttr('tabindex').unbind('keydown.jsp keypress.jsp');}
function observeHash()
{if(location.hash&&location.hash.length>1){var e,retryInt,hash=escape(location.hash);try{e=$(hash);}catch(err){return;}
if(e.length&&pane.find(hash)){if(container.scrollTop()===0){retryInt=setInterval(function()
{if(container.scrollTop()>0){scrollToElement(hash,true);$(document).scrollTop(container.position().top);clearInterval(retryInt);}},50);}else{scrollToElement(hash,true);$(document).scrollTop(container.position().top);}}}}
function unhijackInternalLinks()
{$('a.jspHijack').unbind('click.jsp-hijack').removeClass('jspHijack');}
function hijackInternalLinks()
{unhijackInternalLinks();$('a[href^=#]').addClass('jspHijack').bind('click.jsp-hijack',function()
{var uriParts=this.href.split('#'),hash;if(uriParts.length>1){hash=uriParts[1];if(hash.length>0&&pane.find('#'+hash).length>0){scrollToElement('#'+hash,true);return false;}}});}
function initTouch()
{var startX,startY,touchStartX,touchStartY,moved,moving=false;container.unbind('touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick').bind('touchstart.jsp',function(e)
{var touch=e.originalEvent.touches[0];startX=contentPositionX();startY=contentPositionY();touchStartX=touch.pageX;touchStartY=touch.pageY;moved=false;moving=true;}).bind('touchmove.jsp',function(ev)
{if(!moving){return;}
var touchPos=ev.originalEvent.touches[0],dX=horizontalDragPosition,dY=verticalDragPosition;jsp.scrollTo(startX+touchStartX-touchPos.pageX,startY+touchStartY-touchPos.pageY);moved=moved||Math.abs(touchStartX-touchPos.pageX)>5||Math.abs(touchStartY-touchPos.pageY)>5;return dX==horizontalDragPosition&&dY==verticalDragPosition;}).bind('touchend.jsp',function(e)
{moving=false;}).bind('click.jsp-touchclick',function(e)
{if(moved){moved=false;return false;}});}
function destroy(){var currentY=contentPositionY(),currentX=contentPositionX();elem.removeClass('jspScrollable').unbind('.jsp');elem.replaceWith(originalElement.append(pane.children()));originalElement.scrollTop(currentY);originalElement.scrollLeft(currentX);}
$.extend(jsp,{reinitialise:function(s)
{s=$.extend({},settings,s);initialise(s);},scrollToElement:function(ele,stickToTop,animate)
{scrollToElement(ele,stickToTop,animate);},scrollTo:function(destX,destY,animate)
{scrollToX(destX,animate);scrollToY(destY,animate);},scrollToX:function(destX,animate)
{scrollToX(destX,animate);},scrollToY:function(destY,animate)
{scrollToY(destY,animate);},scrollToPercentX:function(destPercentX,animate)
{scrollToX(destPercentX*(contentWidth-paneWidth),animate);},scrollToPercentY:function(destPercentY,animate)
{scrollToY(destPercentY*(contentHeight-paneHeight),animate);},scrollBy:function(deltaX,deltaY,animate)
{jsp.scrollByX(deltaX,animate);jsp.scrollByY(deltaY,animate);},scrollByX:function(deltaX,animate)
{var destX=contentPositionX()+Math[deltaX<0?'floor':'ceil'](deltaX),percentScrolled=destX/(contentWidth-paneWidth);positionDragX(percentScrolled*dragMaxX,animate);},scrollByY:function(deltaY,animate)
{var destY=contentPositionY()+Math[deltaY<0?'floor':'ceil'](deltaY),percentScrolled=destY/(contentHeight-paneHeight);positionDragY(percentScrolled*dragMaxY,animate);},positionDragX:function(x,animate)
{positionDragX(x,animate);},positionDragY:function(y,animate)
{positionDragY(y,animate);},animate:function(ele,prop,value,stepCallback)
{var params={};params[prop]=value;ele.animate(params,{'duration':settings.animateDuration,'ease':settings.animateEase,'queue':false,'step':stepCallback});},getContentPositionX:function()
{return contentPositionX();},getContentPositionY:function()
{return contentPositionY();},getContentWidth:function()
{return contentWidth;},getContentHeight:function()
{return contentHeight;},getPercentScrolledX:function()
{return contentPositionX()/(contentWidth-paneWidth);},getPercentScrolledY:function()
{return contentPositionY()/(contentHeight-paneHeight);},getIsScrollableH:function()
{return isScrollableH;},getIsScrollableV:function()
{return isScrollableV;},getContentPane:function()
{return pane;},scrollToBottom:function(animate)
{positionDragY(dragMaxY,animate);},hijackInternalLinks:function()
{hijackInternalLinks();},destroy:function()
{destroy();}});initialise(s);}
settings=$.extend({},$.fn.jScrollPane.defaults,settings);$.each(['mouseWheelSpeed','arrowButtonSpeed','trackClickSpeed','keyboardSpeed'],function(){settings[this]=settings[this]||settings.speed;});return this.each(function()
{var elem=$(this),jspApi=elem.data('jsp');if(jspApi){jspApi.reinitialise(settings);}else{jspApi=new JScrollPane(elem,settings);elem.data('jsp',jspApi);}});};$.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,stickToBottom:false,stickToRight:false,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:undefined,animateScroll:false,animateDuration:300,animateEase:'linear',hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:0,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:false,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:'split',horizontalArrowPositions:'split',enableKeyboardNavigation:true,hideFocus:false,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:.8};})(jQuery,this);;function cuSelMulti(params){jQuery(document).ready(function(){jQuery(params.changedEl).each(function(num)
{var chEl=jQuery(this),chElClass=chEl.attr("class"),chElId=chEl.attr("id"),chElName=chEl.attr("name"),disabledSel=chEl.attr("disabled"),scrollArrows=params.scrollArrows,chElOnChange=chEl.attr("onchange"),chElTab=chEl.attr("tabindex"),chElMultiple=chEl.attr("multiple"),selectedOpt=chEl.find("option[selected]"),firstOpt=chEl.find("option").eq(0);if(!chElId)return false;if(!disabledSel)
{classDisCuselText="",classDisCusel="";}
else
{classDisCuselText="classDisCuselLabel";classDisCusel="classDisCusel";}
if(scrollArrows)
{classDisCusel+=" cuselScrollArrows";}
selectedOpt.addClass("cuselMultipleActive");firstOpt.addClass("cuselMultipleCur");var optionStr=chEl.html(),spanStr=optionStr.replace(/option/ig,"span");if(params.checkZIndex)
{num=jQuery(".cusel").length;}
var i,chElVals=selectedOpt.length,chElValsStr="";for(i=0;i<chElVals;i++)
{chElValsStr+='<input type="hidden" name="'+chElName+'" value="'+selectedOpt.eq(i).val()+'" />';}
var cuselFrame='<div class="cuselMultiple '+chElClass+' '+classDisCusel+'"'+' id=cuselFrame-'+chElId+' tabindex="'+chElTab+'"'+'>'+'<div class="cuselMultipleTop"></div><div class="cuselMultipleContent">'+'<div class="cuselMultiple-scroll-wrap"><div class="cuselMultiple-scroll-pane" id="cuselMultiple-scroll-'+chElId+'">'+
spanStr+'<div class="clear"></div></div></div>'+'</div><div class="cuselMultipleBottom"></div>'+'<div name="'+chElName+'"></div>'+'<div class="cuselMultipleInputsWrap" id="'+chElId+'">'+chElValsStr+'</div>'+'</div>';var arrOptSelected=chEl.find("option[selected]"),colOptSelected=arrOptSelected.length;chEl.replaceWith(cuselFrame);if(chElOnChange)jQuery("#"+chElId).bind('change',chElOnChange);if(chEl.attr("size"))
params.visRows=parseInt(chEl.attr("size"));var newSel=jQuery("#cuselFrame-"+chElId),arrSpan=newSel.find("span"),defaultHeight;if(!arrSpan.eq(0).text())
{defaultHeight=arrSpan.eq(1).outerHeight();arrSpan.eq(0).css("height",arrSpan.eq(1).height());}
else
{defaultHeight=arrSpan.eq(0).outerHeight();}
if(params.visRows&&arrSpan.length>params.visRows)
{newSel.find(".cuselMultiple-scroll-pane").eq(0).css({height:defaultHeight*params.visRows/2+defaultHeight+"px"}).jScrollPaneCusel({showArrows:params.scrollArrows});}
var arrAddTags=jQuery("#cusel-scroll-"+chElId).find("span[addTags]"),lenAddTags=arrAddTags.length;for(i=0;i<lenAddTags;i++)arrAddTags.eq(i).append(arrAddTags.eq(i).attr("addTags")).removeAttr("addTags");});jQuery(".cuselMultiple").unbind("click");jQuery(".cuselMultiple").click(function(e)
{var clicked=jQuery(e.target),clickedId=clicked.attr("id"),clickedClass=clicked.attr("class");if(clicked.is("span")&&clicked.parents(".cuselMultiple-scroll-wrap").is("div")&&!clicked.parents(".cuselMultiple").hasClass("classDisCusel"))
{changeVal(clicked);}
else if(clicked.parents(".cuselMultiple-scroll-wrap").is("div"))
{return;}});jQuery(".cuselMultiple").focus(function()
{jQuery(this).addClass("cuselMultipleFocus");});jQuery(".cuselMultiple").blur(function()
{jQuery(this).removeClass("cuselMultipleFocus").find(".cuselMultipleOptHover").removeClass("cuselMultipleOptHover");});jQuery(".cuselMultiple").hover(function()
{jQuery(this).addClass("cuselMultipleFocus").find(".cuselMultipleOptHover").removeClass("cuselMultipleOptHover");},function()
{jQuery(this).removeClass("cuselMultipleFocus");});jQuery(".cuselMultiple").unbind("keydown");jQuery(".cuselMultiple").keydown(function(event)
{var key,keyChar;if(window.event)key=window.event.keyCode;else if(event)key=event.which;if(key==null||key==0||key==9||key==17||key==16)return true;if(jQuery(this).attr("class").indexOf("classDisCusel")!=-1)return false;if(key==40)
{var cuselCur,cuselHover=jQuery(this).find(".cuselMultipleOptHover").eq(0),cuselCurNext;if(cuselHover.hasClass("cuselMultipleOptHover"))cuselCur=cuselHover;else cuselCur=jQuery(this).find(".cuselMultipleCur").eq(0);cuselCurNext=cuselCur.next();if(cuselCurNext.is("span"))
{cuselHover.removeClass("cuselMultipleOptHover");cuselCurNext.addClass("cuselMultipleOptHover");var scrollWrap=jQuery(this).find(".cuselMultiple-scroll-pane").eq(0);if(scrollWrap.parents(".cuselMultiple").find(".jScrollPaneTrack").eq(0).is("div"))
{var hOption=scrollWrap.find("span").eq(0).outerHeight();scrollWrap[0].scrollBy(hOption);}
return false;}
else return false;}
if(key==38)
{var cuselCur,cuselHover=jQuery(this).find(".cuselMultipleOptHover").eq(0),cuselCurPrev;if(cuselHover.hasClass("cuselMultipleOptHover"))cuselCur=cuselHover;else cuselCur=jQuery(this).find(".cuselMultipleCur").eq(0);cuselCurPrev=cuselCur.prev();if(cuselCurPrev.is("span"))
{cuselHover.removeClass("cuselMultipleOptHover");cuselCurPrev.addClass("cuselMultipleOptHover");var scrollWrap=jQuery(this).find(".cuselMultiple-scroll-pane").eq(0);if(scrollWrap.parents(".cuselMultiple").find(".jScrollPaneTrack").eq(0).is("div"))
{var hOption=-parseInt(scrollWrap.find("span").eq(0).outerHeight());scrollWrap[0].scrollBy(hOption);}
return false;}
else return false;}
if(key==13||key==32)
{var clicked=jQuery(this).find(".cuselMultipleOptHover").eq(0);changeVal(clicked);}
return false;});var arr=[];jQuery(".cuselMultiple").keypress(function(event)
{var key,keyChar;if(window.event)key=window.event.keyCode;else if(event)key=event.which;if(key==null||key==0||key==9||key==17||key==16)return true;if(jQuery(this).attr("class").indexOf("classDisCusel")!=-1)return false;var o=this;arr.push(event);clearTimeout(jQuery.data(this,'timer'));var wait=setTimeout(function(){handlingEvent()},300);jQuery(this).data('timer',wait);function handlingEvent()
{var charKey=[];for(var iK in arr)
{if(window.event)charKey[iK]=arr[iK].keyCode;else if(arr[iK])charKey[iK]=arr[iK].which;charKey[iK]=String.fromCharCode(charKey[iK]).toUpperCase();}
var arrOption=jQuery(o).find("span"),colArrOption=arrOption.length,i,letter;for(i=0;i<colArrOption;i++)
{var match=true;for(var iter in arr)
{letter=arrOption.eq(i).text().charAt(iter).toUpperCase();if(letter!=charKey[iter])
{match=false;}}
if(match)
{jQuery(o).find(".cuselMultipleOptHover").removeClass("cuselMultipleOptHover").end().find("span").eq(i).addClass("cuselMultipleOptHover");var scrollWrap=jQuery(o).find(".cuselMultiple-scroll-pane").eq(0);if(scrollWrap.parents(".cuselMultiple").find(".jScrollPaneTrack").eq(0).is("div"))
{var idScrollWrap=scrollWrap.attr("id"),hOption=scrollWrap.find("span").eq(0).outerHeight()-0.2;scrollWrap[0].scrollTo(hOption*i);}
arr=arr.splice;arr=[];break;return true;}}
arr=arr.splice;arr=[];}
if(jQuery.browser.opera&&window.event.keyCode!=9)return false;});jQuery(".cuselMultiple span").mouseover(function()
{jQuery(this).parent().find(".cuselMultipleOptHover").eq(0).removeClass("cuselMultipleOptHover");});});}
function cuSelMultiRefresh(params)
{var arrRefreshEl=params.refreshEl.split(","),lenArr=arrRefreshEl.length,i;for(i=0;i<lenArr;i++)
{var refreshScroll=jQuery(arrRefreshEl[i]).parents(".cuselMultiple").find(".cuselMultiple-scroll-wrap").eq(0);refreshScroll.jScrollPaneRemoveCusel();var arrSpan=refreshScroll.find("span"),defaultHeight=arrSpan.eq(0).outerHeight();if(params.visRows&&arrSpan.length>params.visRows)
{refreshScroll.css({height:defaultHeight*params.visRows+"px"}).children(".cuselMultiple-scroll-pane").css("height",defaultHeight*params.visRows+"px").jScrollPaneCusel({showArrows:params.scrollArrows});}}}
function changeVal(clicked)
{var clickedVal;(clicked.attr("value")==undefined)?clickedVal=clicked.text():clickedVal=clicked.attr("value");var inputsWrap=clicked.parents(".cuselMultiple").find(".cuselMultipleInputsWrap").eq(0);if(clicked.hasClass("cuselMultipleActive"))
{clicked.removeClass("cuselMultipleActive");inputsWrap.find("input[value*='"+clickedVal+"']").remove();}
else
{clicked.addClass("cuselMultipleActive");var inputName=inputsWrap.find("input").eq(0).attr("name");if(!inputName)inputName=inputsWrap.prev().attr("name");inputsWrap.append('<input type="hidden" name="'+inputName+'" value="'+clickedVal+'">');clicked.parents(".cuselMultiple-scroll-wrap").find(".cuselCur").removeClass("cuselMultipleCur");clicked.addClass("cuselMultipleCur");}
clicked.parents(".cuselMultiple").find(".cuselMultipleInputsWrap").eq(0).change();return;}
;/* -------------------------------------

	cusel version 2.3
	смена обычного селект на стильный
	autor: Evgen Ryzhkov
	www.xiper.net
----------------------------------------	
*/
function cuSel(params) {
	
jQuery(document).ready(function(){
							
	jQuery(params.changedEl).each(
	function(num)
	{
	var chEl = jQuery(this),
		chElWid = chEl.outerWidth(), // ширина селекта
		chElClass = chEl.attr("class"), // класс селекта
		chElId = chEl.attr("id"), // id
		chElName = chEl.attr("name"), // имя
		defaultVal = chEl.val(), // начальное значение
		activeOpt = chEl.find("option[value="+defaultVal+"]").eq(0),
		defaultText = activeOpt.text(), // начальный текст
		disabledSel = chEl.attr("disabled"), // заблокирован ли селект
		scrollArrows = params.scrollArrows,
		chElOnChange = chEl.attr("onchange"),
		chElTab = chEl.attr("tabindex"),
		chElMultiple = chEl.attr("multiple");
		
		if(!chElId || chElMultiple)	return false; // не стилизируем селект если не задан id
		
		if(!disabledSel)
		{
			classDisCuselText = "", // для отслеживания клика по задизайбленному селекту
			classDisCusel=""; // для оформления задизейбленного селекта
		}
		else
		{
			classDisCuselText = "classDisCuselLabel";
			classDisCusel="classDisCusel";
		}
		
		if(scrollArrows)
		{
			classDisCusel+=" cuselScrollArrows";
		}
			
		activeOpt.addClass("cuselActive");  // активному оптиону сразу добавляем класс для подсветки
	
	var optionStr = chEl.html(), // список оптионов

		
	/* 
		делаем замену тегов option на span, полностью сохраняя начальную конструкцию
	*/

	spanStr = optionStr.replace(/option/ig,"span");
	
	
	/*
		если нужна проверка z-index, проверяем на наличие уже созданных селектов и задаем z-index новому с учетом имеющихся
	*/
	if(params.checkZIndex)
	{
		num = jQuery(".cusel").length;
	}
	
	
	/* каркас стильного селекта	*/
	var cuselFrame = '<div class="cusel '+chElClass+' '+classDisCusel+'"'+
					' id=cuselFrame-'+chElId+
					' style="width:'+chElWid+'px"'+
					' tabindex="'+chElTab+'"'+
					'>'+
					'<div class="cuselFrameRight"></div>'+
					'<div class="cuselText">'+defaultText+'</div>'+
					'<div class="cusel-scroll-wrap"><div class="cusel-scroll-pane" id="cusel-scroll-'+chElId+'">'+
					spanStr+
					'</div></div>'+
					'<input type="hidden" id="'+chElId+'" name="'+chElName+'" value="'+defaultVal+'" />'+
					'</div>';
					
					
	/* удаляем обычный селект, на его место вставляем стильный */
	chEl.replaceWith(cuselFrame);
	
	/* если был поцеплен onchange - цепляем его полю */
	if(chElOnChange) jQuery("#"+chElId).bind('change',chElOnChange);

	
	/*
		устаналиваем высоту выпадающих списков основываясь на числе видимых позиций и высоты одной позиции
		при чем только тем, у которых число оптионов больше числа заданного числа видимых
	*/
	var newSel = jQuery("#cuselFrame-"+chElId),
		arrSpan = newSel.find("span"),
		defaultHeight;
		
		if(!arrSpan.eq(0).text())
		{
			defaultHeight = arrSpan.eq(1).outerHeight();
			arrSpan.eq(0).css("height", arrSpan.eq(1).height());
		} 
		else
		{
			defaultHeight = arrSpan.eq(0).outerHeight();
		}
		
	
	if(arrSpan.length>params.visRows)
	{
		newSel.find(".cusel-scroll-wrap").eq(0)
			.css({height: defaultHeight*params.visRows+"px", display : "none", visibility: "visible" })
			.children(".cusel-scroll-pane").css("height",defaultHeight*params.visRows+"px");
	}
	else
	{
		newSel.find(".cusel-scroll-wrap").eq(0)
			.css({display : "none", visibility: "visible" });
	}
	
	/* вставляем в оптионы дополнительные теги */
	
	var arrAddTags = jQuery("#cusel-scroll-"+chElId).find("span[addTags]"),
		lenAddTags = arrAddTags.length;
		
		for(i=0;i<lenAddTags;i++) arrAddTags.eq(i)
										.append(arrAddTags.eq(i).attr("addTags"))
										.removeAttr("addTags");
	
	});

/* ---------------------------------------
	привязка событий селектам
------------------------------------------
*/

jQuery(".cusel").unbind("click");
$('html,body').click(function() {
    jQuery(".cusel-scroll-wrap").fadeOut(300);
});
jQuery(".cusel").click(
	function(e)
	{
                e.stopPropagation();
		var clicked = jQuery(e.target),
			clickedId = clicked.attr("id"),
            		clickedClass = clicked.attr("class");                        
					
		/* если кликнули по самому селекту (текст) */
		if(typeof(clickedClass)!="undefined"&&(clickedClass.indexOf("cuselText")!=-1 || clickedClass.indexOf("cuselFrameRight")!=-1) && clicked.parent().attr("class").indexOf("classDisCusel")==-1)
		{
			var cuselWrap = clicked.parent().find(".cusel-scroll-wrap").eq(0);
			
			/* если выпадающее меню скрыто - показываем */
			if(cuselWrap.css("display")=="none")
			{
				jQuery(".cusel-scroll-wrap").css("display","none");
				cuselWrap.css("display","block");
				var cuselArrows = false;
				if(clicked.parents(".cusel").attr("class").indexOf("cuselScrollArrows")!=-1) cuselArrows=true;
				if(!cuselWrap.find(".jScrollPaneContainer").eq(0).is("div"))
				{
					cuselWrap.find("div").eq(0).jScrollPaneCusel({showArrows:cuselArrows});
					/*var scrollContainer = cuselWrap.find(".jScrollPaneContainer").eq(0),
						widScrollContainer = scrollContainer.width();
						
						scrollContainer.css("width",widScrollContainer+"px");
						*/
				}
			}
			else
			{
				cuselWrap.css("display","none");
			}
			/* / если выпадающее меню скрыто - показываем */
		}
		/* если кликнули по самому селекту (контейнер) */
		else if(typeof(clickedClass)!="undefined"&&clickedClass.indexOf("cusel")!=-1 && clickedClass.indexOf("classDisCusel")==-1 && clickedClass.indexOf("cuselActive")==-1)
		{
						
			var cuselWrap = clicked.find(".cusel-scroll-wrap").eq(0);
			
			/* если выпадающее меню скрыто - показываем */
			if(cuselWrap.css("display")=="none")
			{
				jQuery(".cusel-scroll-wrap").css("display","none");
				cuselWrap.css("display","block");
				var cuselArrows = false;
				if(clicked.attr("class").indexOf("cuselScrollArrows")!=-1) cuselArrows=true;
				if(!cuselWrap.find(".jScrollPaneContainer").eq(0).is("div"))
				{
					cuselWrap.find("div").eq(0).jScrollPaneCusel({showArrows:cuselArrows});
				}
			}
			else
			{
				cuselWrap.css("display","none");
			}
			/* / если выпадающее меню скрыто - показываем */
		}
		
		/* если выбрали позицию в списке */
		else if(clicked.is("span") && clicked.parents(".cusel-scroll-wrap").is("div"))
		{
			var clickedVal;
			(clicked.attr("value") == undefined) ? clickedVal=clicked.text() : clickedVal=clicked.attr("value");
			clicked
				.parents(".cusel-scroll-wrap").find(".cuselActive").eq(0).removeClass("cuselActive")
				.end().parents(".cusel-scroll-wrap")
				.next().val(clickedVal)
				.end().prev().text(clicked.text())
				.end().css("display","none");
			clicked.addClass("cuselActive");
			if(typeof(clickedClass)!="undefined"&&clickedClass.indexOf("cuselActive")==-1)	clicked.parents(".cusel").find(".cusel-scroll-wrap").eq(0).next("input").change(); // чтобы срабатывал onchange
		}
		
		else if(clicked.parents(".cusel-scroll-wrap").is("div"))
		{
			return;
		}
		
		/*
			скрываем раскрытые списки, если кликнули вне списка
		*/
		else
		{
			jQuery(".cusel-scroll-wrap").css("display","none");
		}
		

		
	});

/*
	дубляж псевдоклассов css focus и hover для IE6-7
*/
jQuery(".cusel").focus(
function()
{
	jQuery(this).addClass("cuselFocus");
});

jQuery(".cusel").blur(
function()
{
	jQuery(this).removeClass("cuselFocus");
});

jQuery(".cusel").hover(
function()
{
	jQuery(this)
		.addClass("cuselFocus");
},
function()
{
	jQuery(this).removeClass("cuselFocus");
});

jQuery(".cusel").unbind("keydown"); /* чтобы не было двлйного срабатывания события */

jQuery(".cusel").keydown(
function(event)
{
	
	/*
		если селект задизайблин, с не го работает только таб
	*/
	var key, keyChar;
		
	if(window.event) key=window.event.keyCode;
	else if (event) key=event.which;
	
	if(key==null || key==0 || key==9) return true;
	
	if(jQuery(this).attr("class").indexOf("classDisCusel")!=-1) return false;
		
	/*
		если нажали стрелку вниз
	*/
	if(key==40)
	{
		var cuselOptHover = jQuery(this).find(".cuselOptHover").eq(0);
		if(!cuselOptHover.is("span")) var cuselActive = jQuery(this).find(".cuselActive").eq(0);
		else var cuselActive = cuselOptHover;
		var cuselActiveNext = cuselActive.next();
			
		if(cuselActiveNext.is("span"))
		{
			jQuery(this)
				.find(".cuselText").eq(0).text(cuselActiveNext.text());
			cuselActive.removeClass("cuselOptHover");
			cuselActiveNext.addClass("cuselOptHover");
			
			/* делаем прокрутку к выбранному элементу */
			var scrollWrap = jQuery(this).find(".cusel-scroll-pane").eq(0);
				
			if(scrollWrap.parent().find(".jScrollPaneTrack").eq(0).is("div"))
			{
				var idScrollWrap = scrollWrap.attr("id"),
					hOption = scrollWrap.find("span").eq(0).outerHeight();
				jQuery("#"+idScrollWrap)[0].scrollBy(hOption);
			}
			
			return false;
		}
		else return false;
	}
	
	/*
		если нажали стрелку вверх
	*/
	if(key==38)
	{
		var cuselOptHover = jQuery(this).find(".cuselOptHover").eq(0);
		if(!cuselOptHover.is("span")) var cuselActive = jQuery(this).find(".cuselActive").eq(0);
		else var cuselActive = cuselOptHover;
		cuselActivePrev = cuselActive.prev();
			
		if(cuselActivePrev.is("span"))
		{
			jQuery(this)
				.find(".cuselText").eq(0).text(cuselActivePrev.text());
			cuselActive.removeClass("cuselOptHover");
			cuselActivePrev.addClass("cuselOptHover");
			
			/* делаем прокрутку к выбранному элементу */
			var scrollWrap = jQuery(this).find(".cusel-scroll-pane").eq(0);
				
			if(scrollWrap.parent().find(".jScrollPaneTrack").eq(0).is("div"))
			{
				var idScrollWrap = scrollWrap.attr("id"),
					hOption = -parseInt(scrollWrap.find("span").eq(0).outerHeight());
				jQuery("#"+idScrollWrap)[0].scrollBy(hOption);
			}
			
			return false;
		}
		else return false;
	}
	
	/*
		если нажали esc
	*/
	if(key==27)
	{
		var cuselActiveText = jQuery(this).find(".cuselActive").eq(0).text();
		jQuery(this)
			.find(".cusel-scroll-wrap").eq(0).css("display","none")
			.end().find(".cuselOptHover").eq(0).removeClass("cuselOptHover");
		jQuery(this).find(".cuselText").eq(0).text(cuselActiveText);
	}
	
	/*
		если нажали enter
	*/
	if(key==13)
	{
		
		var cuselHover = jQuery(this).find(".cuselOptHover").eq(0);
		if(cuselHover.is("span"))
		{
			jQuery(this).find(".cuselActive").removeClass("cuselActive");
			var cuselHoverVal = cuselHover.attr("value");
			cuselHover.addClass("cuselActive");
		}
		else var cuselHoverVal = jQuery(this).find(".cuselActive").attr("value");
		
		jQuery(this)
			.find(".cusel-scroll-wrap").eq(0).css("display","none")
			.end().find(".cuselOptHover").eq(0).removeClass("cuselOptHover");
		jQuery(this).find("input").eq(0)
			.val(cuselHoverVal)
			.change();
	}
		
	if(jQuery.browser.opera) return false; /* специально для опера, чтоб при нажатиии на клавиши не прокручивалось окно браузера */

});

/*
	функция отбора по нажатым символам (от Alexey Choporov)
	отбор идет пока пауза между нажатиями сиволов не будет больше 0.5 сек
	keypress нужен для отлова символа нажатой клавиш
*/
var arr = [];
jQuery(".cusel").keypress(function(event)
{
	var key, keyChar;
		
	if(window.event) key=window.event.keyCode;
	else if (event) key=event.which;
	
	if(key==null || key==0 || key==9) return true;
	
	if(jQuery(this).attr("class").indexOf("classDisCusel")!=-1) return false;
	
	var o = this;
	arr.push(event);
	clearTimeout(jQuery.data(this, 'timer'));
	var wait = setTimeout(function() { handlingEvent() }, 500);
	jQuery(this).data('timer', wait);
	function handlingEvent()
	{
		var charKey = [];
		for (var iK in arr)
		{
			if(window.event)charKey[iK]=arr[iK].keyCode;
			else if(arr[iK])charKey[iK]=arr[iK].which;
			charKey[iK]=String.fromCharCode(charKey[iK]).toUpperCase();
		}
		var arrOption=jQuery(o).find("span"),colArrOption=arrOption.length,i,letter;
		for(i=0;i<colArrOption;i++)
		{
			var match = true;
			for (var iter in arr)
			{
				letter=arrOption.eq(i).text().charAt(iter).toUpperCase();
				if (letter!=charKey[iter])
				{
					match=false;
				}
			}
			if(match)
			{
				jQuery(o).find(".cuselOptHover").removeClass("cuselOptHover").end().find("span").eq(i).addClass("cuselOptHover").end().end().find(".cuselText").eq(0).text(arrOption.eq(i).text());
				var scrollWrap=jQuery(o).find(".cusel-scroll-pane").eq(0);
				if(scrollWrap.parent().find(".jScrollPaneTrack").eq(0).is("div"))
				{
					var idScrollWrap=scrollWrap.attr("id"),hOption=scrollWrap.find("span").eq(0).outerHeight()-0.2;
					jQuery("#"+idScrollWrap)[0].scrollTo(hOption*i);
				}
			arr = arr.splice;
			arr = [];
			break;
			return true;
			}
		}
		arr = arr.splice;
		arr = [];
	}
	if(jQuery.browser.opera && window.event.keyCode!=9) return false;
});

jQuery(".cusel span").mouseover(
function()
{
	jQuery(this).parent().find(".cuselOptHover").eq(0).removeClass("cuselOptHover");
});
								


/*
	проставляем z-index селектам,
	вынесено отдельно, чтобы избежать проблем с динамически добавленными селектами и при инициализации селектв разными способами
*/
var arrCusel = jQuery(".cusel"),
	colCusel = arrCusel.length-1,
	i;

	for(i=0;i<=colCusel;i++)
	{
		arrCusel.eq(i).css("z-index",colCusel-i);
	}
	
	
								});

	
}

function cuSelRefresh(params)
{
	/*
		устаналиваем высоту выпадающих списков основываясь на числе видимых позиций и высоты одной позиции
		при чем только тем, у которых число оптионов больше числа заданного числа видимых
	*/

	var arrRefreshEl = params.refreshEl.split(","),
		lenArr = arrRefreshEl.length,
		i;
	
	for(i=0;i<lenArr;i++)
	{
		var refreshScroll = jQuery(arrRefreshEl[i]).parents(".cusel").find(".cusel-scroll-wrap").eq(0);
		refreshScroll.find(".cusel-scroll-pane").jScrollPaneRemoveCusel();
		refreshScroll.css({visibility: "hidden", display : "block"});
	
		var	arrSpan = refreshScroll.find("span"),
			defaultHeight = arrSpan.eq(0).outerHeight();
		
		
		
	
		if(arrSpan.length>params.visRows)
		{
			refreshScroll
				.css({height: defaultHeight*params.visRows+"px", display : "none", visibility: "visible" })
				.children(".cusel-scroll-pane").css("height",defaultHeight*params.visRows+"px");
		}
		else
		{
			refreshScroll
				.css({display : "none", visibility: "visible" });
		}
	}
	
	
	
	
}
;jQuery.fn.ontop=function(options){var options=jQuery.extend({text:"наверх"},options);this.init=function(){$(this).append("<div id='ontop'><div class='ontop_arrow'>"+options.text+"</div></div>");var _this=$("#ontop");var a=$(window).height();$(window).scroll(function(){if($(window).scrollTop()>$(window).height()+300)
_this.fadeIn(300);else
_this.fadeOut(300);});_this.click(function(){$('html,body').animate({scrollTop:"0"},500);});return _this;};return this.init();}
$(document).ready(function(){if($(window).width()>1024)
{$("#container").ontop();}});;jQuery.fn.system_message=function(options){this.init=function(){$(this).append('<div id="system_message"></div>');var _this=$("#system_message");$("#system_message").on("click","#sys_mes_close",function(e){e.stopPropagation();$("#system_message").fadeOut(300);});return _this;};return this.init();}
$(document).ready(function(){$("body").system_message();});;function changeRadio(el)
{var el=el,input=el.find("input").eq(0);var nm=input.attr("name");jQuery(".niceRadio input").each(function(){if(jQuery(this).attr("name")==nm)
{jQuery(this).parent().removeClass("radioChecked");}});if(el.attr("class").indexOf("niceRadioDisabled")==-1)
{el.addClass("radioChecked");input.attr("checked",true);}
return true;}
function changeVisualRadio(input)
{var wrapInput=input.parent();var nm=input.attr("name");jQuery(".niceRadio input").each(function(){if(jQuery(this).attr("name")==nm)
{jQuery(this).parent().removeClass("radioChecked");}});if(input.attr("checked"))
{wrapInput.addClass("radioChecked");}}
function changeRadioStart(el)
{try
{var el=el,radioName=el.attr("name"),radioId=el.attr("id"),radioChecked=el.attr("checked"),radioDisabled=el.attr("disabled"),radioTab=el.attr("tabindex");radioValue=el.attr("value");if(radioChecked)
el.after("<span class='niceRadio radioChecked'>"+"<input type='radio'"+"name='"+radioName+"'"+"id='"+radioId+"'"+"checked='"+radioChecked+"'"+"tabindex='"+radioTab+"'"+"value='"+radioValue+"' /></span>");else
el.after("<span class='niceRadio'>"+"<input type='radio'"+"name='"+radioName+"'"+"id='"+radioId+"'"+"tabindex='"+radioTab+"'"+"value='"+radioValue+"' /></span>");if(radioDisabled)
{el.next().addClass("niceRadioDisabled");el.next().find("input").eq(0).attr("disabled","disabled");}
el.next().bind("mousedown",function(e){changeRadio(jQuery(this))});el.next().find("input").eq(0).bind("change",function(e){changeVisualRadio(jQuery(this))});if(jQuery.browser.msie)
{el.next().find("input").eq(0).bind("click",function(e){changeVisualRadio(jQuery(this))});}
el.remove();}
catch(e)
{}
return true;};(function(b){function d(a){this.input=a;a.attr("type")=="password"&&this.handlePassword();b(a[0].form).submit(function(){if(a.hasClass("placeholder")&&a[0].value==a.attr("placeholder"))a[0].value=""})}d.prototype={show:function(a){if(this.input[0].value===""||a&&this.valueIsPlaceholder()){if(this.isPassword)try{this.input[0].setAttribute("type","text")}catch(b){this.input.before(this.fakePassword.show()).hide()}this.input.addClass("placeholder");this.input[0].value=this.input.attr("placeholder")}},hide:function(){if(this.valueIsPlaceholder()&&this.input.hasClass("placeholder")&&(this.input.removeClass("placeholder"),this.input[0].value="",this.isPassword)){try{this.input[0].setAttribute("type","password")}catch(a){}this.input.show();this.input[0].focus()}},valueIsPlaceholder:function(){return this.input[0].value==this.input.attr("placeholder")},handlePassword:function(){var a=this.input;a.attr("realType","password");this.isPassword=!0;if(b.browser.msie&&a[0].outerHTML){var c=b(a[0].outerHTML.replace(/type=(['"])?password\1/gi,"type=$1text$1"));this.fakePassword=c.val(a.attr("placeholder")).addClass("placeholder").focus(function(){a.trigger("focus");b(this).hide()});b(a[0].form).submit(function(){c.remove();a.show()})}}};var e=!!("placeholder"in document.createElement("input"));b.fn.placeholder=function(){return e?this:this.each(function(){var a=b(this),c=new d(a);c.show(!0);a.focus(function(){c.hide()});a.blur(function(){c.show(!1)});b.browser.msie&&(b(window).load(function(){a.val()&&a.removeClass("placeholder");c.show(!0)}),a.focus(function(){if(this.value==""){var a=this.createTextRange();a.collapse(!0);a.moveStart("character",0);a.select()}}))})}})(jQuery);
;/*
 * jQuery Autocomplete plugin 1.1
 *
 * Copyright (c) 2009 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id: jquery.autocomplete.js 15 2009-08-22 10:30:27Z joern.zaefferer $
 */

;(function($) {
	
$.fn.extend({
	autocomplete: function(urlOrData, options) {
		var isUrl = typeof urlOrData == "string";
		options = $.extend({}, $.Autocompleter.defaults, {
			url: isUrl ? urlOrData : null,
			data: isUrl ? null : urlOrData,
			delay: isUrl ? $.Autocompleter.defaults.delay : 10,
			max: options && !options.scroll ? 10 : 150
		}, options);
		
		// if highlight is set to false, replace it with a do-nothing function
		options.highlight = options.highlight || function(value) { return value; };
		
		// if the formatMatch option is not specified, then use formatItem for backwards compatibility
		options.formatMatch = options.formatMatch || options.formatItem;
		
		return this.each(function() {
			new $.Autocompleter(this, options);
		});
	},
	result: function(handler) {
		return this.bind("result", handler);
	},
	search: function(handler) {
		return this.trigger("search", [handler]);
	},
	flushCache: function() {
		return this.trigger("flushCache");
	},
	setOptions: function(options){
		return this.trigger("setOptions", [options]);
	},
	unautocomplete: function() {
		return this.trigger("unautocomplete");
	}
});

$.Autocompleter = function(input, options) {

	var KEY = {
		UP: 38,
		DOWN: 40,
		DEL: 46,
		TAB: 9,
		RETURN: 13,
		ESC: 27,
		COMMA: 188,
		PAGEUP: 33,
		PAGEDOWN: 34,
		BACKSPACE: 8
	};

	// Create $ object for input element
	var $input = $(input).attr("autocomplete", "off").addClass(options.inputClass);

	var timeout;
	var previousValue = "";
	var cache = $.Autocompleter.Cache(options);
	var hasFocus = 0;
	var lastKeyPressCode;
	var config = {
		mouseDownOnSelect: false
	};
	var select = $.Autocompleter.Select(options, input, selectCurrent, config);
	
	var blockSubmit;
	
	// prevent form submit in opera when selecting with return key
	$.browser.opera && $(input.form).bind("submit.autocomplete", function() {
		if (blockSubmit) {
			blockSubmit = false;
			return false;
		}
	});
                
	// only opera doesn't trigger keydown multiple times while pressed, others don't work with keypress at all
	$input.bind(($.browser.opera ? "keypress" : "keydown") + ".autocomplete", function(event) {
		// a keypress means the input has focus
		// avoids issue where input had focus before the autocomplete was applied
		hasFocus = 1;
		// track last key pressed
		lastKeyPressCode = event.keyCode;
		switch(event.keyCode) {
		
			case KEY.UP:
				event.preventDefault();
				if ( select.visible() ) {
					select.prev();
				} else {
					onChange(0, true);
				}
				break;
				
			case KEY.DOWN:
				event.preventDefault();
				if ( select.visible() ) {
					select.next();
				} else {
					onChange(0, true);
				}
				break;
				
			case KEY.PAGEUP:
				event.preventDefault();
				if ( select.visible() ) {
					select.pageUp();
				} else {
					onChange(0, true);
				}
				break;
				
			case KEY.PAGEDOWN:
				event.preventDefault();
				if ( select.visible() ) {
					select.pageDown();
				} else {
					onChange(0, true);
				}
				break;
			
			// matches also semicolon
			case options.multiple && $.trim(options.multipleSeparator) == "," && KEY.COMMA:
			case KEY.TAB:
			case KEY.RETURN:
				if( selectCurrent() ) {
					// stop default to prevent a form submit, Opera needs special handling
					event.preventDefault();
					blockSubmit = true;
					return false;
				}
				break;
				
			case KEY.ESC:
				select.hide();
				break;
				
			default:
				clearTimeout(timeout);
				timeout = setTimeout(onChange, options.delay);
				break;
		}
	}).focus(function(){
		// track whether the field has focus, we shouldn't process any
		// results if the field no longer has focus
		hasFocus++;
	}).blur(function() {
		hasFocus = 0;
		if (!config.mouseDownOnSelect) {
			hideResults();
		}
	}).click(function() {
		// show select when clicking in a focused field
		if ( hasFocus++ > 1 && !select.visible() ) {
			onChange(0, true);
		}
	}).bind("search", function() {
		// TODO why not just specifying both arguments?
		var fn = (arguments.length > 1) ? arguments[1] : null;
		function findValueCallback(q, data) {
			var result;
			if( data && data.length ) {
				for (var i=0; i < data.length; i++) {
					if( data[i].result.toLowerCase() == q.toLowerCase() ) {
						result = data[i];
						break;
					}
				}
			}
			if( typeof fn == "function" ) fn(result);
			else $input.trigger("result", result && [result.data, result.value]);
		}
		$.each(trimWords($input.val()), function(i, value) {
			request(value, findValueCallback, findValueCallback);
		});
	}).bind("flushCache", function() {
		cache.flush();
	}).bind("setOptions", function() {
		$.extend(options, arguments[1]);
		// if we've updated the data, repopulate
		if ( "data" in arguments[1] )
			cache.populate();
	}).bind("unautocomplete", function() {
		select.unbind();
		$input.unbind();
		$(input.form).unbind(".autocomplete");
	});
	
	
	function selectCurrent() {
		var selected = select.selected();                
		if( !selected )
			return false;
		if (options.onItemSelect) setTimeout(function() { options.onItemSelect(selected.value) }, 1);
		var v = selected.result;
		previousValue = v;
		
		if ( options.multiple ) {
			var words = trimWords($input.val());
			if ( words.length > 1 ) {
				var seperator = options.multipleSeparator.length;
				var cursorAt = $(input).selection().start;
				var wordAt, progress = 0;
				$.each(words, function(i, word) {
					progress += word.length;
					if (cursorAt <= progress) {
						wordAt = i;
						return false;
					}
					progress += seperator;
				});
				words[wordAt] = v;
				// TODO this should set the cursor to the right position, but it gets overriden somewhere
				//$.Autocompleter.Selection(input, progress + seperator, progress + seperator);
				v = words.join( options.multipleSeparator );
			}
			v += options.multipleSeparator;
		}
		
		$input.val(v);		
		hideResultsNow();
		$input.trigger("result", [selected.data, selected.value]);
		return true;
	}
	
	function onChange(crap, skipPrevCheck) {
		if( lastKeyPressCode == KEY.DEL ) {
			select.hide();
			return;
		}
		
		var currentValue = $input.val();
		
		if ( !skipPrevCheck && currentValue == previousValue )
			return;
		
		previousValue = currentValue;
		
		currentValue = lastWord(currentValue);
		if ( currentValue.length >= options.minChars) {
			$input.addClass(options.loadingClass);
			if (!options.matchCase)
				currentValue = currentValue.toLowerCase();
			request(currentValue, receiveData, hideResultsNow);
		} else {
			stopLoading();
			select.hide();
		}
	};
	
	function trimWords(value) {
		if (!value)
			return [""];
		if (!options.multiple)
			return [$.trim(value)];
		return $.map(value.split(options.multipleSeparator), function(word) {
			return $.trim(value).length ? $.trim(word) : null;
		});
	}
	
	function lastWord(value) {
		if ( !options.multiple )
			return value;
		var words = trimWords(value);
		if (words.length == 1) 
			return words[0];
		var cursorAt = $(input).selection().start;
		if (cursorAt == value.length) {
			words = trimWords(value)
		} else {
			words = trimWords(value.replace(value.substring(cursorAt), ""));
		}
		return words[words.length - 1];
	}
	
	// fills in the input box w/the first match (assumed to be the best match)
	// q: the term entered
	// sValue: the first matching result
	function autoFill(q, sValue){
		// autofill in the complete box w/the first match as long as the user hasn't entered in more data
		// if the last user key pressed was backspace, don't autofill
		if( options.autoFill && (lastWord($input.val()).toLowerCase() == q.toLowerCase()) && lastKeyPressCode != KEY.BACKSPACE ) {
			// fill in the value (keep the case the user has typed)
			$input.val($input.val() + sValue.substring(lastWord(previousValue).length));
			// select the portion of the value not typed by the user (so the next character will erase)
			$(input).selection(previousValue.length, previousValue.length + sValue.length);
		}
	};

	function hideResults() {
		clearTimeout(timeout);
		timeout = setTimeout(hideResultsNow, 200);
	};

	function hideResultsNow() {
		var wasVisible = select.visible();
		select.hide();
		clearTimeout(timeout);
		stopLoading();
		if (options.mustMatch) {
			// call search and run callback
			$input.search(
				function (result){
					// if no value found, clear the input box
					if( !result ) {
						if (options.multiple) {
							var words = trimWords($input.val()).slice(0, -1);
							$input.val( words.join(options.multipleSeparator) + (words.length ? options.multipleSeparator : "") );
						}
						else {
							$input.val( "" );
							$input.trigger("result", null);
						}
					}
				}
			);
		}
	};

	function receiveData(q, data) {
		if ( data && data.length && hasFocus ) {
			stopLoading();
			select.display(data, q);
			autoFill(q, data[0].value);
			select.show();
		} else {
			hideResultsNow();
		}
	};

	function request(term, success, failure) {
		if (!options.matchCase)
			term = term.toLowerCase();
		var data = cache.load(term);
		// recieve the cached data
		if (data && data.length) {
			success(term, data);
		// if an AJAX url has been supplied, try loading the data now
		} else if( (typeof options.url == "string") && (options.url.length > 0) ){
			
			var extraParams = {
				timestamp: +new Date()
			};
			$.each(options.extraParams, function(key, param) {
				extraParams[key] = typeof param == "function" ? param() : param;
			});
			
			$.ajax({
				// try to leverage ajaxQueue plugin to abort previous requests
				mode: "abort",
				// limit abortion to this input
				port: "autocomplete" + input.name,
				dataType: options.dataType,
				url: options.url,
				data: $.extend({
					q: lastWord(term),
					limit: options.max
				}, extraParams),
				success: function(data) {
					var parsed = options.parse && options.parse(data) || parse(data);
					cache.add(term, parsed);
					success(term, parsed);
				}
			});
		} else {
			// if we have a failure, we need to empty the list -- this prevents the the [TAB] key from selecting the last successful match
			select.emptyList();
			failure(term);
		}
	};
	
	function parse(data) {
		var parsed = [];
		var rows = data.split("\n");
		for (var i=0; i < rows.length; i++) {
			var row = $.trim(rows[i]);
			if (row) {
				row = row.split("|");
				parsed[parsed.length] = {
					data: row,
					value: row[0],
					result: options.formatResult && options.formatResult(row, row[0]) || row[0]
				};
			}
		}
		return parsed;
	};

	function stopLoading() {
		$input.removeClass(options.loadingClass);
	};

};

$.Autocompleter.defaults = {
	inputClass: "ac_input",
	resultsClass: "ac_results",
	loadingClass: "ac_loading",
	minChars: 1,
	delay: 400,
	matchCase: false,
	matchSubset: true,
	matchContains: false,
	cacheLength: 10,
	max: 100,
	mustMatch: false,
	extraParams: {},
	selectFirst: true,
	formatItem: function(row) { return row[0]; },
        onItemSelect: false,
	formatMatch: null,
	autoFill: false,
	width: 0,
	multiple: false,
	multipleSeparator: ", ",
	highlight: function(value, term) {
		return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
	},
    scroll: true,
    scrollHeight: 180
};

$.Autocompleter.Cache = function(options) {

	var data = {};
	var length = 0;
	
	function matchSubset(s, sub) {
		if (!options.matchCase) 
			s = s.toLowerCase();
		var i = s.indexOf(sub);
		if (options.matchContains == "word"){
			i = s.toLowerCase().search("\\b" + sub.toLowerCase());
		}
		if (i == -1) return false;
		return i == 0 || options.matchContains;
	};
	
	function add(q, value) {
		if (length > options.cacheLength){
			flush();
		}
		if (!data[q]){ 
			length++;
		}
		data[q] = value;
	}
	
	function populate(){
		if( !options.data ) return false;
		// track the matches
		var stMatchSets = {},
			nullData = 0;

		// no url was specified, we need to adjust the cache length to make sure it fits the local data store
		if( !options.url ) options.cacheLength = 1;
		
		// track all options for minChars = 0
		stMatchSets[""] = [];
		
		// loop through the array and create a lookup structure
		for ( var i = 0, ol = options.data.length; i < ol; i++ ) {
			var rawValue = options.data[i];
			// if rawValue is a string, make an array otherwise just reference the array
			rawValue = (typeof rawValue == "string") ? [rawValue] : rawValue;
			
			var value = options.formatMatch(rawValue, i+1, options.data.length);
			if ( value === false )
				continue;
				
			var firstChar = value.charAt(0).toLowerCase();
			// if no lookup array for this character exists, look it up now
			if( !stMatchSets[firstChar] ) 
				stMatchSets[firstChar] = [];

			// if the match is a string
			var row = {
				value: value,
				data: rawValue,
				result: options.formatResult && options.formatResult(rawValue) || value
			};
			
			// push the current match into the set list
			stMatchSets[firstChar].push(row);

			// keep track of minChars zero items
			if ( nullData++ < options.max ) {
				stMatchSets[""].push(row);
			}
		};

		// add the data items to the cache
		$.each(stMatchSets, function(i, value) {
			// increase the cache size
			options.cacheLength++;
			// add to the cache
			add(i, value);
		});
	}
	
	// populate any existing data
	setTimeout(populate, 25);
	
	function flush(){
		data = {};
		length = 0;
	}
	
	return {
		flush: flush,
		add: add,
		populate: populate,
		load: function(q) {
			if (!options.cacheLength || !length)
				return null;
			/* 
			 * if dealing w/local data and matchContains than we must make sure
			 * to loop through all the data collections looking for matches
			 */
			if( !options.url && options.matchContains ){
				// track all matches
				var csub = [];
				// loop through all the data grids for matches
				for( var k in data ){
					// don't search through the stMatchSets[""] (minChars: 0) cache
					// this prevents duplicates
					if( k.length > 0 ){
						var c = data[k];
						$.each(c, function(i, x) {
							// if we've got a match, add it to the array
							if (matchSubset(x.value, q)) {
								csub.push(x);
							}
						});
					}
				}				
				return csub;
			} else 
			// if the exact item exists, use it
			if (data[q]){
				return data[q];
			} else
			if (options.matchSubset) {
				for (var i = q.length - 1; i >= options.minChars; i--) {
					var c = data[q.substr(0, i)];
					if (c) {
						var csub = [];
						$.each(c, function(i, x) {
							if (matchSubset(x.value, q)) {
								csub[csub.length] = x;
							}
						});
						return csub;
					}
				}
			}
			return null;
		}
	};
};

$.Autocompleter.Select = function (options, input, select, config) {
	var CLASSES = {
		ACTIVE: "ac_over"
	};
	
	var listItems,
		active = -1,
		data,
		term = "",
		needsInit = true,
		element,
		list;
	
	// Create results
	function init() {
		if (!needsInit)
			return;
		element = $("<div/>")
		.hide()
		.addClass(options.resultsClass)
		.css("position", "absolute")
		.appendTo(document.body);
	
		list = $("<ul/>").appendTo(element).mouseover( function(event) {
			if(target(event).nodeName && target(event).nodeName.toUpperCase() == 'LI') {
	            active = $("li", list).removeClass(CLASSES.ACTIVE).index(target(event));
			    $(target(event)).addClass(CLASSES.ACTIVE);            
	        }
		}).click(function(event) {
			$(target(event)).addClass(CLASSES.ACTIVE);
			select();
			// TODO provide option to avoid setting focus again after selection? useful for cleanup-on-focus
			input.focus();
			return false;
		}).mousedown(function() {
			config.mouseDownOnSelect = true;
		}).mouseup(function() {
			config.mouseDownOnSelect = false;
		});
		
		if( options.width > 0 )
			element.css("width", options.width);
			
		needsInit = false;
	} 
	
	function target(event) {
		var element = event.target;
		while(element && element.tagName != "LI")
			element = element.parentNode;
		// more fun with IE, sometimes event.target is empty, just ignore it then
		if(!element)
			return [];
		return element;
	}

	function moveSelect(step) {
		listItems.slice(active, active + 1).removeClass(CLASSES.ACTIVE);
		movePosition(step);
        var activeItem = listItems.slice(active, active + 1).addClass(CLASSES.ACTIVE);
        if(options.scroll) {
            var offset = 0;
            listItems.slice(0, active).each(function() {
				offset += this.offsetHeight;
			});
            if((offset + activeItem[0].offsetHeight - list.scrollTop()) > list[0].clientHeight) {
                list.scrollTop(offset + activeItem[0].offsetHeight - list.innerHeight());
            } else if(offset < list.scrollTop()) {
                list.scrollTop(offset);
            }
        }
	};
	
	function movePosition(step) {
		active += step;
		if (active < 0) {
			active = listItems.size() - 1;
		} else if (active >= listItems.size()) {
			active = 0;
		}
	}
	
	function limitNumberOfItems(available) {
		return options.max && options.max < available
			? options.max
			: available;
	}
	
	function fillList() {
		list.empty();
		var max = limitNumberOfItems(data.length);
		for (var i=0; i < max; i++) {
			if (!data[i])
				continue;
			var formatted = options.formatItem(data[i].data, i+1, max, data[i].value, term);
			if ( formatted === false )
				continue;
			var li = $("<li/>").html( options.highlight(formatted, term) ).addClass(i%2 == 0 ? "ac_even" : "ac_odd").appendTo(list)[0];
			$.data(li, "ac_data", data[i]);
		}
		listItems = list.find("li");
		if ( options.selectFirst ) {
			listItems.slice(0, 1).addClass(CLASSES.ACTIVE);
			active = 0;
		}
		// apply bgiframe if available
		if ( $.fn.bgiframe )
			list.bgiframe();
	}
	
	return {
		display: function(d, q) {
			init();
			data = d;
			term = q;
			fillList();
		},
		next: function() {
			moveSelect(1);
		},
		prev: function() {
			moveSelect(-1);
		},
		pageUp: function() {
			if (active != 0 && active - 8 < 0) {
				moveSelect( -active );
			} else {
				moveSelect(-8);
			}
		},
		pageDown: function() {
			if (active != listItems.size() - 1 && active + 8 > listItems.size()) {
				moveSelect( listItems.size() - 1 - active );
			} else {
				moveSelect(8);
			}
		},
		hide: function() {
			element && element.hide();
			listItems && listItems.removeClass(CLASSES.ACTIVE);
			active = -1;
		},
		visible : function() {
			return element && element.is(":visible");
		},
		current: function() {
			return this.visible() && (listItems.filter("." + CLASSES.ACTIVE)[0] || options.selectFirst && listItems[0]);
		},
		show: function() {
			var offset = $(input).offset();
			element.css({
				width: typeof options.width == "string" || options.width > 0 ? options.width : $(input).width(),
				top: offset.top + input.offsetHeight,
				left: offset.left
			}).show();
            if(options.scroll) {
                list.scrollTop(0);
                list.css({
					maxHeight: options.scrollHeight,
					overflow: 'auto'
				});
				
                if($.browser.msie && typeof document.body.style.maxHeight === "undefined") {
					var listHeight = 0;
					listItems.each(function() {
						listHeight += this.offsetHeight;
					});
					var scrollbarsVisible = listHeight > options.scrollHeight;
                    list.css('height', scrollbarsVisible ? options.scrollHeight : listHeight );
					if (!scrollbarsVisible) {
						// IE doesn't recalculate width when scrollbar disappears
						listItems.width( list.width() - parseInt(listItems.css("padding-left")) - parseInt(listItems.css("padding-right")) );
					}
                }
                
            }
		},
		selected: function() {
			var selected = listItems && listItems.filter("." + CLASSES.ACTIVE).removeClass(CLASSES.ACTIVE);
			return selected && selected.length && $.data(selected[0], "ac_data");
		},
		emptyList: function (){
			list && list.empty();
		},
		unbind: function() {
			element && element.remove();
		}
	};
};

$.fn.selection = function(start, end) {
	if (start !== undefined) {
		return this.each(function() {
			if( this.createTextRange ){
				var selRange = this.createTextRange();
				if (end === undefined || start == end) {
					selRange.move("character", start);
					selRange.select();
				} else {
					selRange.collapse(true);
					selRange.moveStart("character", start);
					selRange.moveEnd("character", end);
					selRange.select();
				}
			} else if( this.setSelectionRange ){
				this.setSelectionRange(start, end);
			} else if( this.selectionStart ){
				this.selectionStart = start;
				this.selectionEnd = end;
			}
		});
	}
	var field = this[0];
	if ( field.createTextRange ) {
		var range = document.selection.createRange(),
			orig = field.value,
			teststring = "<->",
			textLength = range.text.length;
		range.text = teststring;
		var caretAt = field.value.indexOf(teststring);
		field.value = orig;
		this.selection(caretAt, caretAt + textLength);
		return {
			start: caretAt,
			end: caretAt + textLength
		}
	} else if( field.selectionStart !== undefined ){
		return {
			start: field.selectionStart,
			end: field.selectionEnd
		}
	}
};

})(jQuery);