<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="content">
    <div class="left" style="width:720px;">   
        <div class="statya_section_name">
            <h4>НАЗВАНИЕ РАЗДЕЛА</h4>
            <div class="statya_nav">
                <div class="left">
                    <a class="statya_left_arrow no_border" href="#">Предыдущая пост</a>
                </div>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="#">Следующая пост</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1><?=$arResult["NAME"]?></h1>
        <a class="another no_border" href="#">Администратор</a>, <span class="statya_date">20</span> декабря 2011, 11:20
        <br /><br />
        <i>Недлинный текст — введение из краткого описания статьи. Яркая, красивая, успешная, одна из самых известных московских женщин-рестораторов, рассказывает о своих рецептах успеха и планах на будущее.</i>
        <br /><br />
        <h2>Подзаголовок, например, название ресторана, о котором пойдет речь ниже</h2>
        <div class="left" style="width:155px;">
            <img class="indent" src="<?=SITE_TEMPLATE_PATH?>/images/3.png" width="148" /><br />
            <p><a class="font14" href="#">Ресторан Terrassa</a>,<br />
            Санкт-петербург</p>
            <p>
                <div class="rating">
                    <div class="small_star_a" alt="1"></div>
                    <div class="small_star_a" alt="2"></div>
                    <div class="small_star_a" alt="3"></div>
                    <div class="small_star_a" alt="4"></div>
                    <div class="small_star" alt="5"></div>
                    <div class="clear"></div>
                </div>
            </p>
            <p><b>Кухня:</b> Авторская, европейская, русская<br /></p>
            <p><b>Средний счет:</b> 1 500 р. — 2 000 р.<br /></p>
        </div>
        <div class="right"  style="width:565px;">
            <p>Новое кафе Abajour открылось на 9-й линии Васильевского острова. Это первый собственный проект Эльдара Кабирова (занимался маркетингом в холдинге «Гутцайт», ресторанах «Подворье», «Ялта») и Дмитрия Кадырова (работал поваром в различных ресторанах Петербурга и Алма-Аты).</p>
            <p>Кафе специализируется на европейской кухне. В меню, которое разрабатывал Дмитрий, много мясных блюд: голень ягнёнка, тушёная в соусе «Розмарин», филе цыплёнка с тимьяном в панировке из пармезана, филе миньон из говядины. Для стейков владельцы заказывают американскую охлаждённую мраморную говядину. Например, из неё готовится специальное предложение шеф-повара — стейк «Топ-глейд». Винной карты в заведении пока нет, зато гости могут приносить алкоголь с собой. В будущем владельцы собираются разработать постное и сезонное меню и ввести винную карту.</p>
            <p>Планировку, оставшуюся от предыдущего заведения, менять не стали. Дизайн продумывали сами, руководствуясь советами жены Дмитрия — Камилы. Изначально планировали создать уютный скандинавский интерьер.</p>
        </div>
        <div class="clear"></div>
        <br />
        <script>
            $(document).ready(function(){
               $(".articles_photo").galery();
            });
        </script>
        <div class="left articles_photo">
            <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/6<a class="next browse right"></a></div>
            <div class="clear"></div>
            <div class="img">
                <img src="/upload/resize_cache/iblock/579/397_271_1/c9d1zona_fvid1l.jpg" width="715">
                <p><i>1</i></p>
            </div>
            <div class="special_scroll">
                <div class="scroll_container">
                    <div class="item active">
                        <img src="/upload/resize_cache/iblock/579/397_271_1/c9d1zona_fvid1l.jpg" alt="1" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/fdd/397_271_1/7b68Kafe.jpg" alt="2" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/6dd/397_271_1/5f7boran_dvid1r.jpg" alt="3" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/313/397_271_1/6cb8naya_dvid2v.jpg" alt="4" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/faa/397_271_1/2a47Zal_jvid_3z.jpg" alt="5" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/973/397_271_1/2a47Zal_rvid_3m.jpg" alt="6" align="bottom">
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <h2>Подзаголовок, например, название ресторана, о котором пойдет речь ниже</h2>
        <div class="left" style="width:155px;">
            <img class="indent" src="<?=SITE_TEMPLATE_PATH?>/images/3.png" width="148" /><br />
            <p><a class="font14" href="#">Ресторан Terrassa</a>,<br />
            Санкт-петербург</p>
            <p>
                <div class="rating">
                    <div class="small_star_a" alt="1"></div>
                    <div class="small_star_a" alt="2"></div>
                    <div class="small_star_a" alt="3"></div>
                    <div class="small_star_a" alt="4"></div>
                    <div class="small_star" alt="5"></div>
                    <div class="clear"></div>
                </div>
            </p>
            <p><b>Кухня:</b> Авторская, европейская, русская<br /></p>
            <p><b>Средний счет:</b> 1 500 р. — 2 000 р.<br /></p>
        </div>
        <div class="right"  style="width:565px;">
            <p>Новое кафе Abajour открылось на 9-й линии Васильевского острова. Это первый собственный проект Эльдара Кабирова (занимался маркетингом в холдинге «Гутцайт», ресторанах «Подворье», «Ялта») и Дмитрия Кадырова (работал поваром в различных ресторанах Петербурга и Алма-Аты).</p>
            <p>Кафе специализируется на европейской кухне. В меню, которое разрабатывал Дмитрий, много мясных блюд: голень ягнёнка, тушёная в соусе «Розмарин», филе цыплёнка с тимьяном в панировке из пармезана, филе миньон из говядины. Для стейков владельцы заказывают американскую охлаждённую мраморную говядину. Например, из неё готовится специальное предложение шеф-повара — стейк «Топ-глейд». Винной карты в заведении пока нет, зато гости могут приносить алкоголь с собой. В будущем владельцы собираются разработать постное и сезонное меню и ввести винную карту.</p>
            <p>Планировку, оставшуюся от предыдущего заведения, менять не стали. Дизайн продумывали сами, руководствуясь советами жены Дмитрия — Камилы. Изначально планировали создать уютный скандинавский интерьер.</p>
        </div>
        <div class="clear"></div>
        <br /><Br />
        <table class="direct_speech">
            <tr>
                <td width="170">
                    <div class="author">
                        <img src="/tpl/images/author.png" />
                    </div>
                </td>
                <td width="150">
                    <span class="uppercase">имя героя<br /> в две строки</span><br/>
                    <i>должность автора прямой речи</i>
                </td>
                <td class="font16">
                    <i>«Мы работаем честно, предъявляя высокие требования прежде всего к самим себе. Основное отличие нашего заведения в том, что создатели кафе принимают непосредственное участие в работе.
                    я готовлю, Эльдар работает в зале. У нас нет лишних звеньев, которые зачастую только мешают.».</i>
                </td>
            </tr>            
        </table>
        <br /><br />
        <h2>Подзаголовок, ниже — список: выдержки из меню</h2>
        <p class="line24">
            — Карпаччо из мраморной говядины — 290 рублей<br />
            — Рулеты из ростбифа с рукколой и соусом из тунца — 350 рублей<br />
            — Тёплый салат с куриной печенью — 270 рублей<br />
            — «Пиль-пиль» с тигровыми креветками — 350 рублей<br />
            — Тыквенный крем-суп — 200 рублей<br />
            — Куриный бульон с домашней лапшой — 180 рублей<br />
            — Пенне с томатами и базиликом — 230 рублей<br />
            — Спагетти с соусом песто — 270 рублей<br />
            — Феттучини с цыплёнком и цукини в сливочном соусе — 280 рублей<br />
            — Кебаб из мраморной говядины — 530 рублей<br />
            — Цыплёнок, запечённый с прованскими травами — 450 рублей<br />
            — Стейк из лосося на гриле — 470 рублей<br />
            — Феттучини с цыплёнком и цукини в сливочном соусе — 280 рублей<br />
            — Кебаб из мраморной говядины — 530 рублей<br />
            — Цыплёнок, запечённый с прованскими травами — 450 рублей<br />
            — Стейк из лосося на гриле — 470 рублей<br />
        </p>
        <p>Теги: <a href="#" class="another">название тега 1</a>, <a href="#" class="another">название тега 2</a>, <a href="#" class="another">название тега 3</a></p>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#" class="another">15</a>)</div>
            <div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>
            <div class="right" style="padding-right:15px;"><input type="button" class="button" value="подписаться" /></div>
            <?$APPLICATION->IncludeComponent(
                                        "bitrix:asd.share.buttons",
                                        "likes",
                                        Array(
                                                "ASD_TITLE" => $_REQUEST["title"],
                                                "ASD_URL" => $_REQUEST["url"],
                                                "ASD_PICTURE" => $_REQUEST["picture"],
                                                "ASD_TEXT" => $_REQUEST["text"],
                                                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE",1=>"VK_LIKE",2=>"TWITTER",),
                                                "LIKE_TYPE" => "LIKE",
                                                "VK_API_ID" => "2628160",
                                                "VK_LIKE_VIEW" => "mini",
                                                "SCRIPT_IN_HEAD" => "N"
                                        )
            );?>                                    
            <div class="clear"></div>
        </div>
        <div id="order" style="width:700px">
            <div class="left">
                <script>

                    $(document).ready(function(){
                        var params = {
                            changedEl: "#order_what",
                            visRows: 5
                        }
                        cuSel(params);
                        var params = {
                            changedEl: "#order_many",
                            visRows: 5
                        }
                        cuSel(params);
                        $.tools.dateinput.localize("ru",  {
                           months:        'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря',
                           shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',
                           days:          'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',
                           shortDays:     'вс,пн,вт,ср,чт,пт,сб'
                        });
                        $(".whose_date").dateinput({lang: 'ru', firstDay: 1 , format:"dd/mm/yy",trigger:true, css: { trigger: 'caltrigger'}, change: function(e, date)  {
                            $(".caltrigger").html(this.getValue("dd mmmm")); 
                        } });                                                                        
                       $(".caltrigger").html("12 ноября");
                       $.maski.definitions['~']='[0-2]';
                       $.maski.definitions['!']='[0-5]';
                       $(".time").maski("~9:!9");
                    });
                </script>
                Бронирование 
                <select id="order_what" name="order_what">
                    <option selected="selected" value="1">столика</option>
                    <option  value="2">банкета</option>
                    <option value="3">фуршета</option>
                </select>
                в ресторане Terrassa<br />                            
                <span style="position: relative; display: inline">
                    <input class="whose_date" style="" type="date" style="visibility:hidden" />
                </span>
                в <input class="time" type="text" size="4" value="1230"/> на 
                <select id="order_many" name="order_many">
                    <option selected="2" value="2">2</option>
                    <option  value="4">4</option>
                    <option value="6">6</option>
                    <option value="8">8</option>
                </select>
                персоны
                <div class="phone">
                    <sub>Заказ столика или банкета:<br /> </sub>
                    +7 (495) 988-26-56, +7 (495) 506-00-33
                </div>
            </div>
            <div class="right">
                <input type="button" class="dark_button" value="ЗАБРОНИРОВАТЬ" />
                <div class="coupon">
                    -50% <span>скидка</span><br />
                    <a href="#">Купить купон</a>
                </div>

            </div>
        </div>
        <Br /><Br />
        <div id="tabs_block7">
            <ul class="tabs" ajax="ajax">
                <li>                                
                    <a href="<?=$templateFolder?>/comments.php">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>                                                                
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block"></div>
            </div>
        </div>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2709409, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <hr class="black" />
        <div class="statya_section_name">
            <h4>НАЗВАНИЕ РАЗДЕЛА</h4>        
            <div class="statya_nav">
                <div class="left">
                    <a class="statya_left_arrow no_border" href="#">Предыдущая пост</a>
                </div>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="#">Следующая пост</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <img alt="" title="" src="/upload/rk/aa3/middle_baner.png" width="728" height="90" border="0">
    </div>
    <div class="right" style="width:240px">
        <div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <div id="users_onpage"></div>
            </div>
            <div class="bottom"></div>
        </div>   
        <br />
        <img src="/bitrix_personal/templates/main/images/right_baner1.png" />
        <br /><br /><br />
        <div class="top_block">Читайте также</div>
         <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "interview",
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "NEWS_COUNT" => 6,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => Array("ACTIVE_TO"),
                        "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "kupon_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "PRICE_CODE" => "BASE"
                ),
            false
        );?>
        <div align="right">
            <img src="/bitrix_personal/templates/main/images/right_baner2.png">
        </div>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
    <div class="clear"></div>    
</div>
