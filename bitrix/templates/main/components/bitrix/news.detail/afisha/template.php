<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="content">
    <div class="left" style="width:720px;">   
        <div class="statya_section_name">
            <h4><?=$arResult["IBLOCK_TYPE_NAME"]?></h4>
            <div class="statya_nav">
                <div class="left">
                    <a class="statya_left_arrow no_border" href="#"><?=GetMessage("NEXT_POST")?></a>
                </div>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="#"><?=GetMessage("PREV_POST")?></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1><?=$arResult["NAME"]?></h1>
        <a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <!--<i>Недлинный текст — введение из краткого описания статьи. Яркая, красивая, успешная, одна из самых известных московских женщин-рестораторов, рассказывает о своих рецептах успеха и планах на будущее.</i>
        <br /><br />
        <h2>Подзаголовок, например, название ресторана, о котором пойдет речь ниже</h2>-->
        <div class="left" style="width:155px;">
            <?foreach ($arResult["RESTORAUNT"] as $rest):?>
                <img class="indent" src="<?=$rest["REST_PICTURE"]["src"]?>" width="148" /><br />
                <a class="font14" href="#"><?=$rest["NAME"]?></a>,<br />
                <?=$rest["CITY"]?>
                <p>
                    <div class="rating">
                        <?for($i = 0; $i < 5; $i++):?>
                            <div class="small_star<?if($i < $rest["RATIO"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                        <?endfor?>
                        <div class="clear"></div>
                    </div>
                    <b><?=GetMessage("CUISINE")?>:</b>
                    <?foreach($rest["CUISINE"] as $cuisine):?>
                        <?=$cuisine?><?if(end($rest["CUISINE"]) != $cuisine) echo ", ";?>
                    <?endforeach?>
                    <br />
                    <b><?=GetMessage("AVERAGE_BILL")?>:</b> <?=$rest["AVERAGE_BILL"]?><br />
                </p>
                <?if(end($arResult["RESTORAUNT"]) != $rest):?>
                    <hr class="dotted" />
                <?endif?>
            <?endforeach;?>
        </div>
        <div class="right"  style="width:565px;">
            <?=$arResult["DETAIL_TEXT"]?>
        </div>
        <div class="clear"></div>
        <br />
        <?
        //  photos
        if(is_array($arResult["PICTURES"])):?>
            <div class="left articles_photo">
                <?if($block["CNT_PICTURES"] == 1):?>
                    <div class="img">
                        <img src="<?=$arResult["PICTURES"][0]["src"]?>" width="715" />
                        <p><i><?=$arResult["PICTURES"][0]["description"]?></i></p>
                    </div>
                <?else:?>
                    <script type="text/javascript">
                        $(document).ready(function(){
                           $(".articles_photo").galery();
                        });
                    </script>
                    <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/<?=$arResult["CNT_PICTURES"]?><a class="next browse right"></a></div>
                    <div class="clear"></div>
                    <div class="img">
                        <img src="<?=$arResult["PICTURES"][0]["src"]?>" width="715" />
                        <p><i><?=$arResult["PICTURES"][0]["description"]?></i></p>
                    </div>
                    <div class="special_scroll">
                        <div class="scroll_container">
                            <?foreach($arResult["PICTURES"] as $keyPic=>$pic):?>
                                <div class="item<?if($keyPic == 0):?> active<?endif?>">
                                    <img src="<?=$pic["src"]?>" alt="<?=$pic["description"]?>" align="bottom" />
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                <?endif?>
            </div>
            <div class="clear"></div>
        <?
        $prevType = 'photos';
        endif?>
        <div class="clear"></div>        
        <p>Теги:
        <?foreach ($arResult["TAGS"] as $tag):?>
            <a href="#" class="another"><?=$tag?></a>
            <?if (end($arResult["TAGS"])!=$tag):?>, <?endif;?>
        <?endforeach;?>
        </p>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#comments" class="anchor another"><?=$arResult["COMMENTS_CNT"]?></a>)</div>
            <!--<div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <div class="right" style="padding-right:15px;"><input type="button" class="button" value="подписаться" /></div>
            <?$APPLICATION->IncludeComponent(
                                        "bitrix:asd.share.buttons",
                                        "likes",
                                        Array(
                                                "ASD_TITLE" => $_REQUEST["title"],
                                                "ASD_URL" => $_REQUEST["url"],
                                                "ASD_PICTURE" => $_REQUEST["picture"],
                                                "ASD_TEXT" => $_REQUEST["text"],
                                                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE",1=>"VK_LIKE",2=>"TWITTER",),
                                                "LIKE_TYPE" => "LIKE",
                                                "VK_API_ID" => "2628160",
                                                "VK_LIKE_VIEW" => "mini",
                                                "SCRIPT_IN_HEAD" => "N"
                                        )
            );?>                                    
            <div class="clear"></div>
        </div>
        <div id="order" style="width:700px">
            <div class="left">
                <script>

                    $(document).ready(function(){
                        var params = {
                            changedEl: "#order_what",
                            visRows: 5
                        }
                        cuSel(params);
                        var params = {
                            changedEl: "#order_many",
                            visRows: 5
                        }
                        cuSel(params);
                        $.tools.dateinput.localize("ru",  {
                           months:        'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря',
                           shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',
                           days:          'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',
                           shortDays:     'вс,пн,вт,ср,чт,пт,сб'
                        });
                        $(".whose_date").dateinput({lang: 'ru', firstDay: 1 , format:"dd/mm/yy",trigger:true, css: { trigger: 'caltrigger'}, change: function(e, date)  {
                            $(".caltrigger").html(this.getValue("dd mmmm")); 
                        } });                                                                        
                       $(".caltrigger").html("12 ноября");
                       $.maski.definitions['~']='[0-2]';
                       $.maski.definitions['!']='[0-5]';
                       $(".time").maski("~9:!9");
                    });
                </script>
                Бронирование 
                <select id="order_what" name="order_what">
                    <option selected="selected" value="1">столика</option>
                    <option  value="2">банкета</option>
                    <option value="3">фуршета</option>
                </select>
                в ресторане Terrassa<br />                            
                <span style="position: relative; display: inline">
                    <input class="whose_date" style="" type="date" style="visibility:hidden" />
                </span>
                в <input class="time" type="text" size="4" value="1230"/> на 
                <select id="order_many" name="order_many">
                    <option selected="2" value="2">2</option>
                    <option  value="4">4</option>
                    <option value="6">6</option>
                    <option value="8">8</option>
                </select>
                персоны
                <div class="phone">
                    <sub>Заказ столика или банкета:<br /> </sub>
                    +7 (495) 988-26-56, +7 (495) 506-00-33
                </div>
            </div>
            <div class="right">
                <input type="button" class="dark_button" value="ЗАБРОНИРОВАТЬ" />
                <div class="coupon">
                    -50% <span>скидка</span><br />
                    <a href="#">Купить купон</a>
                </div>

            </div>
        </div>
        <Br /><Br />
        <div id="tabs_block7">
            <ul class="tabs">
                <li>                                
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>                                                                
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block">
                   <?$APPLICATION->IncludeComponent(
                   	"restoran:main.comments",
                   	"",
                   	Array(
                   		"IBLOCK_TYPE" => "comments",
                   		"IBLOCK_ID" => $arResult["COMMENTS_IBLOCK_ID"],
                   		"SECTION_ID" => $arResult["COMMENTS_SECTION_ID"],
                   		"PROPERTY_PARENT_BIND_CODE" => "PARENT_ID",
                   		"PROPERTY_USER_BIND_CODE" => "USER_BIND",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("PROPERTY_PARENT_ID", "PROPERTY_USER_BIND"),
                                "ITEMS_LIMIT" => "10",
                   		"CACHE_TYPE" => "A",
                   		"CACHE_TIME" => "3600",
                   		"CACHE_NOTES" => ""
                   	),
                   false
                   );?>
               </div>
            </div>
        </div>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2709409, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <hr class="black" />
        <div class="statya_section_name">
            <h4><?=$arResult["IBLOCK_TYPE_NAME"]?></h4>        
            <div class="statya_nav">
                <div class="left">
                    <a class="statya_left_arrow no_border" href="#"><?=GetMessage("NEXT_POST")?></a>
                </div>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="#"><?=GetMessage("PREV_POST")?></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <img alt="" title="" src="/upload/rk/aa3/middle_baner.png" width="728" height="90" border="0">
    </div>
    <div class="right" style="width:240px">
        <div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <div id="users_onpage"></div>
            </div>
            <div class="bottom"></div>
        </div>   
        <br />
        <img src="/bitrix_personal/templates/main/images/right_baner1.png" />
        <br /><br /><br />
        <div class="top_block">Читайте также</div>
         <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "interview",
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "NEWS_COUNT" => 3,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => Array("ACTIVE_TO"),
                        "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "kupon_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "PRICE_CODE" => "BASE"
                ),
            false
        );?>
        <div align="right">
            <img src="/bitrix_personal/templates/main/images/right_baner2.png">
        </div>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
    <div class="clear"></div>    
</div>
