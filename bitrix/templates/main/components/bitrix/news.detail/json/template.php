<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//if (count($arResult["ITEMS"])>0)
//unset($arResult["DETAIL_TEXT"]);
unset($arResult["~DETAIL_TEXT"]);
unset($arResult["~DETAIL_PICTURE"]);
unset($arResult["~PREVIEW_PICTURE"]);
unset($arResult["PREVIEW_PICTURE"]);
unset($arResult["~DETAIL_TEXT_TYPE"]);
unset($arResult["DETAIL_TEXT_TYPE"]);
unset($arResult["~PREVIEW_TEXT_TYPE"]);    
unset($arResult["PREVIEW_TEXT_TYPE"]);
unset($arResult["~ID"]);
unset($arResult["~PREVIEW_TEXT"]);
unset($arResult["~LIST_PAGE_URL"]);
unset($arResult["LIST_PAGE_URL"]);
unset($arResult["LANG_DIR"]);
unset($arResult["~LANG_DIR"]);
unset($arResult["~SEARCHABLE_CONTENT"]);
unset($arResult["SEARCHABLE_CONTENT"]);
unset($arResult["IBLOCK_EXTERNAL_ID"]);
unset($arResult["~IBLOCK_EXTERNAL_ID"]);
unset($arResult["PREV_ARTICLE"]);
unset($arResult["NEXT_ARTICLE"]);
unset($arResult["~NAME"]);
foreach ($arResult["DISPLAY_PROPERTIES"] as &$prop)
{
        if (is_array($prop["DISPLAY_VALUE"]))
        {
            $prop = implode(", ",$prop["DISPLAY_VALUE"]);
        }
        else
        {
            $prop = $prop["DISPLAY_VALUE"];
        }
        $prop = strip_tags($prop);    
    
}
$rest = $arResult["PROPERTIES"]["RESTORAN"]["VALUE"][0]; 
unset($arResult["PROPERTIES"]);
unset($arResult["SECTION"]);
unset($arResult["IBLOCK"]);
require($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/.default/components/restoran/editor2/editor/phpQuery-onefile.php');	
$document = phpQuery::newDocument($arResult["DETAIL_TEXT"]);
$block = $document->find('.article_rest_text')->html();
$block = str_replace("<br>", "/n", $block);
$block = str_replace("<br/>", "/n", $block);
$block = str_replace("<br />", "/n", $block);
//$arResult["DETAIL_TEXT"] = strip_tags($arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = strip_tags($block);
$arResult["RESTORAN"] = $rest;
echo json_encode($arResult);
?>                