<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="content">
    <div class="left" style="width:720px;">   
        <div class="statya_section_name">
            <h4>НАЗВАНИЕ РАЗДЕЛА</h4>
            <div class="statya_nav">
                <div class="left">
                    <a class="statya_left_arrow no_border" href="#">Предыдущая пост</a>
                </div>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="#">Следующая пост</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1><?=$arResult["NAME"]?></h1>
        <a class="another no_border" href="#">Администратор</a>, <span class="statya_date">20</span> декабря 2011, 11:20
        <br /><br />
        <i>Яркая, красивая, успешная, одна из самых известных московских женщин-рестораторов, рассказывает о своих рецептах успеха и планах на будущее.</i>
        <br /> <br />       
        <div class="left" style="width:165px;">
            <img class="indent" src="<?=SITE_TEMPLATE_PATH?>/images/3.png" width="148" /><br />
            <p><a class="font14" href="#">Ресторан Terrassa</a>,<br />
            Санкт-петербург</p>
            <p>
                <div class="rating">
                    <div class="small_star_a" alt="1"></div>
                    <div class="small_star_a" alt="2"></div>
                    <div class="small_star_a" alt="3"></div>
                    <div class="small_star_a" alt="4"></div>
                    <div class="small_star" alt="5"></div>
                    <div class="clear"></div>
                </div>
            </p>
            <p><b>Кухня:</b> Авторская, европейская, русская<br /></p>
            <p><b>Средний счет:</b> 1 500 р. — 2 000 р.<br /></p>
            <hr class="dotted" />
            <img class="indent" src="<?=SITE_TEMPLATE_PATH?>/images/3.png" width="148" /><br />
            <p><a class="font14" href="#">Ресторан Terrassa</a>,<br />
            Санкт-петербург</p>
            <p>
                <div class="rating">
                    <div class="small_star_a" alt="1"></div>
                    <div class="small_star_a" alt="2"></div>
                    <div class="small_star_a" alt="3"></div>
                    <div class="small_star_a" alt="4"></div>
                    <div class="small_star" alt="5"></div>
                    <div class="clear"></div>
                </div>
            </p>
            <p><b>Кухня:</b> Авторская, европейская, русская<br /></p>
            <p><b>Средний счет:</b> 1 500 р. — 2 000 р.<br /></p>
        </div>
        <div class="interview_answer right" style="width:555px;">
            <h5>Как получилось, что Вы стали заниматься ресторанным бизнесом?</h5>
            <p>Я прошла долгий путь! В 1992 году я переехала из родного Еревана в Словакию, вышла замуж, но поняв, что мы с мужем разные люди, развелась, забрала маленькую дочку и рванула в Москву в 1994 году. Здесь меня никто не ждал. В моем арсенале было только знание двух языков – чешского и английского. Благодаря этому меня взяли на работу в закрытый ресторанный комплекс, который посещали дипломаты и граждане Чехии и Словакии. Там я начала свою карьеру официантки, потому что другую работу молодой 19-летней девушке получить было невозможно. Но потом моя карьера пошла в гору - я много работала, училась. Без поддержки мамы и брата, которые, бросив все, уехали из Еревана, чтобы поддержать меня, было бы сложно добиться всего, но это случилось. И в результате, я имею то, что имею.</p>
            <h5>С какими трудностями сталкивались в самом начале? Какие ошибки Вы теперь точно не повторите и предостережете от них других начинающих рестораторов?</h5>
            <p>Не нужно бояться работы и углубляться в самые низы ресторанного бизнеса, чтобы разбираться не только в желаниях гостя, но и полностью понимать всю схему работы повара, официанта, бармена. Профессионализм нарабатывается в процессе работы и обучения. Трудности у начинающего ресторатора будут в каждом цикле процесса: разрешительные документы, все проверяющие органы, правильная концепция, 'вкусный' повар, создание команды единомышленников, которые будут охотно бороться с любыми трудности и работать на результат.</p>
            <h5>Насколько тяжело быть женщиной-ресторатором? (извините за банальный вопрос)</h5>
            <p>Конечно же, тяжело, хотя я считаю, что женщина мыслит интереснее и более нестандартно. Она является генератором идей и решений в развитии ресторанного бизнеса. Женщины естественны в этой среде, и многие вопросы решают интуитивно, что, на мой взгляд, тоже является преимуществом. А если женщина-ресторатор еще имеет экономическое и управленческое образование, то ей просто нет равных.</p>
        </div>
        <div class="clear"></div>
        <br />
         <Br />
        <table class="direct_speech">
            <tr>
                <td width="170">
                    <div class="author">
                        <img src="/tpl/images/gayane.jpg" />
                    </div>
                </td>
                <td width="150">
                    <span class="uppercase">Гаяне бреиова</span><br/>
                    <i>ресторатор</i>
                </td>
                <td class="font16">
                    <i>«Не нужно бояться работы и углубляться в самые низы ресторанного бизнеса, чтобы разбираться не только в желаниях гостя, но и полностью понимать всю схему работы повара, официанта, бармена.»</i>
                </td>
            </tr>            
        </table>
        <br />
        <div class="interview_answer">
            <h5>Сейчас многие, в том числе девушки, мечтают о собственном уютном ресторанчике, каковы 3 основных правила успешности предприятия?</h5>
            <p>Харизма, профессионализм, честность.</p>
            <h5>Ваши проекты «На Лестнице» и La Scaletta очень популярны, а ваше имя – успешный бренд, почему не открываете новые проекты?</h5>
            <p>Проект «На лестнице» является моей гордостью, не каждый московский ресторан может похвастаться семилетним стажем работы. Однажды я летела в самолете и слышала, как пассажиры, которые сидели рядом, обсуждали и хвалили вечер, который они провели недавно в кафе 'На лестнице', - было ощущение, что я получила еще один диплом. В начале 2012 года я планирую открытие нового проекта. Обещаю, что Вы об этом узнаете в рядах первых, а я с удовольствием Вам расскажу о секретах именно этой кухни.</p>
            <h5>Что интересного в ближайшем будущем ожидает гостей в ваших ресторанах?</h5>
            <p>Хочу порадовать гостей необычной программой на семилетие кафе 'На лестнице', ведь цифра 7 сама по себе очень магическая и обязывает меня и мою команду удивить моих любимых гостей, ну а после празднования дня рождения будем готовиться ко встрече 2012 года!</p>
        </div>
        <script>
            $(document).ready(function(){
               $(".articles_photo").galery();
            });
        </script>
        <div class="left articles_photo">
            <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/6<a class="next browse right"></a></div>
            <div class="clear"></div>
            <div class="img">
                <img src="/upload/resize_cache/iblock/579/397_271_1/c9d1zona_fvid1l.jpg" width="715">
                <p><i>1</i></p>
            </div>
            <div class="special_scroll">
                <div class="scroll_container">
                    <div class="item active">
                        <img src="/upload/resize_cache/iblock/579/397_271_1/c9d1zona_fvid1l.jpg" alt="1" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/fdd/397_271_1/7b68Kafe.jpg" alt="2" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/6dd/397_271_1/5f7boran_dvid1r.jpg" alt="3" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/313/397_271_1/6cb8naya_dvid2v.jpg" alt="4" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/faa/397_271_1/2a47Zal_jvid_3z.jpg" alt="5" align="bottom">
                    </div>
                    <div class="item">
                        <img src="/upload/resize_cache/iblock/973/397_271_1/2a47Zal_rvid_3m.jpg" alt="6" align="bottom">
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <p>Теги: <a href="#" class="another">название тега 1</a>, <a href="#" class="another">название тега 2</a>, <a href="#" class="another">название тега 3</a></p>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#" class="another">15</a>)</div>
            <div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>
            <div class="right" style="padding-right:15px;"><input type="button" class="button" value="подписаться" /></div>
            <?$APPLICATION->IncludeComponent(
                                        "bitrix:asd.share.buttons",
                                        "likes",
                                        Array(
                                                "ASD_TITLE" => $_REQUEST["title"],
                                                "ASD_URL" => $_REQUEST["url"],
                                                "ASD_PICTURE" => $_REQUEST["picture"],
                                                "ASD_TEXT" => $_REQUEST["text"],
                                                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE",1=>"VK_LIKE",2=>"TWITTER",),
                                                "LIKE_TYPE" => "LIKE",
                                                "VK_API_ID" => "2628160",
                                                "VK_LIKE_VIEW" => "mini",
                                                "SCRIPT_IN_HEAD" => "N"
                                        )
            );?>                                    
            <div class="clear"></div>
        </div>
        <div id="order" style="width:700px">
            <div class="left">
                <script>

                    $(document).ready(function(){
                        var params = {
                            changedEl: "#order_what",
                            visRows: 5
                        }
                        cuSel(params);
                        var params = {
                            changedEl: "#order_many",
                            visRows: 5
                        }
                        cuSel(params);
                        $.tools.dateinput.localize("ru",  {
                           months:        'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря',
                           shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',
                           days:          'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',
                           shortDays:     'вс,пн,вт,ср,чт,пт,сб'
                        });
                        $(".whose_date").dateinput({lang: 'ru', firstDay: 1 , format:"dd/mm/yy",trigger:true, css: { trigger: 'caltrigger'}, change: function(e, date)  {
                            $(".caltrigger").html(this.getValue("dd mmmm")); 
                        } });                                                                        
                       $(".caltrigger").html("12 ноября");
                       $.maski.definitions['~']='[0-2]';
                       $.maski.definitions['!']='[0-5]';
                       $(".time").maski("~9:!9");
                    });
                </script>
                Бронирование 
                <select id="order_what" name="order_what">
                    <option selected="selected" value="1">столика</option>
                    <option  value="2">банкета</option>
                    <option value="3">фуршета</option>
                </select>
                в ресторане Terrassa<br />                            
                <span style="position: relative; display: inline">
                    <input class="whose_date" style="" type="date" style="visibility:hidden" />
                </span>
                в <input class="time" type="text" size="4" value="1230"/> на 
                <select id="order_many" name="order_many">
                    <option selected="2" value="2">2</option>
                    <option  value="4">4</option>
                    <option value="6">6</option>
                    <option value="8">8</option>
                </select>
                персоны
                <div class="phone">
                    <sub>Заказ столика или банкета:<br /> </sub>
                    +7 (495) 988-26-56, +7 (495) 506-00-33
                </div>
            </div>
            <div class="right">
                <input type="button" class="dark_button" value="ЗАБРОНИРОВАТЬ" />
                <div class="coupon">
                    -50% <span>скидка</span><br />
                    <a href="#">Купить купон</a>
                </div>

            </div>
        </div>
        <Br /><Br />
        <div id="tabs_block7">
            <ul class="tabs" ajax="ajax">
                <li>                                
                    <a href="<?=$templateFolder?>/comments.php">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>                                                                
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block"></div>
            </div>
        </div>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2709409, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <hr class="black" />
        <div class="statya_section_name">
            <h4>НАЗВАНИЕ РАЗДЕЛА</h4>        
            <div class="statya_nav">
                <div class="left">
                    <a class="statya_left_arrow no_border" href="#">Предыдущая пост</a>
                </div>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="#">Следующая пост</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <img alt="" title="" src="/upload/rk/aa3/middle_baner.png" width="728" height="90" border="0">
    </div>
    <div class="right" style="width:240px">
        <div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <div id="users_onpage"></div>
            </div>
            <div class="bottom"></div>
        </div>   
        <br />
        <img src="/bitrix_personal/templates/main/images/right_baner1.png" />
        <br /><br /><br />
        <div class="top_block">Читайте также</div>
         <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "interview",
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "NEWS_COUNT" => 6,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => Array("ACTIVE_TO"),
                        "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "kupon_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "PRICE_CODE" => "BASE"
                ),
            false
        );?>
        <div align="right">
            <img src="/bitrix_personal/templates/main/images/right_baner2.png">
        </div>
        <br /><br />
        <div class="phone_block2">
            <div>Закажите столик<br /> по телефонам: </div>
            <span>812</span> 338 38 58<br>
            <span>812</span> 338 38 25
        </div>
    </div>
    <div class="clear"></div>    
</div>
