<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
<script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />

<script type="text/javascript">
    $(document).ready(function(){
        $('.QapTcha').QapTcha({
            txtLock : 'Сдвиньте слайдер вправо.',
            txtUnlock : 'Готово. Теперь можно отправлять',
            disabledSubmit : true,
            autoRevert : true,
            PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
            autoSubmit : false});
                    
                    
        $(".fancy").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200, 
            'speedOut'		:	200, 
            //'overlayShow'	:	false
        });
    });
</script>

<?
//v_dump($arResult); 
$rsUser = CUser::GetByID($arResult["CREATOR_ID"]);
$arUser = $rsUser->Fetch();
?>
<script>
    $(document).ready(function(){
        $("#user_mail_send").click(function(){
            var _this = $(this);
            $.ajax({
                type: "POST",
                url: _this.attr("href"),
                data: "USER_NAME=<?= $arUser["NAME"] . " " . $arUser["LAST_NAME"] ?>&USER_ID=<?= intval($arUser["ID"]) ?>&<?= bitrix_sessid_get() ?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                    }
                    $('#mail_modal').html(data);
                    showOverflow();
                    setCenter($("#mail_modal"));
                    $("#mail_modal").fadeIn("300"); 
                }
            });
            return false;
        });
    });
</script>
<div class="content-wrapper">
    <div class="content-content">
        <div style="float:left; width: 600px;">
            <h1 class="content-h nopadding-bottom"><?= $arResult["NAME"] ?></h1>
            <p><span class="date-field"><span class="date-day"><?= $arResult["DISPLAY_ACTIVE_FROM_1"] ?></span> <?= $arResult["DISPLAY_ACTIVE_FROM_2"] ?> <?= $arResult["DISPLAY_ACTIVE_FROM_3"] ?></span></p>
        </div>
        <div style="float:right; width: 128px; padding-top: 20px;">
            <div class="print">
                <a target="_blank" href="?print=Y">Распечатать</a>
            </div>
        </div>
        <div class="clear"></div>
        <div class="info-detail-wrapper info-detail-wrapper-resume">
            <div class="info-detail-small info-detail-small-resume">
                <div class="big_avatar">
                    <? if ($arResult["CREATOR_PHOTO"]): ?>
                        <img src="<?= $arResult["CREATOR_PHOTO"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" />
                    <? endif; ?>

                </div>
                <div class="resume-detail-send-message">
                    <div class="" style="margin-top:7px; margin-right:15px"><a href="/tpl/ajax/im.php" id="user_mail_send">Сообщение</a></div>
                </div>
            </div>
            <div class="info-detail-big info-detail-big-resume">
                <dl class="resume-dl">
                    <dt>Имя:</dt>
                    <dd><a href="/users/id<?= $arResult["CREATOR_ID"] ?>/"><?= $arResult["CREATOR_NAME"] ?></a></dd>
                    <dt>Пол:</dt>
                    <dd><?= GetMessage("GENDER_" . $arResult["CREATOR_GENDER"]) ?></dd>
                    <? if ($arResult["CREATOR_BIRTHDAY"]): ?>
                        <dt>Дата рождения:</dt>
                        <dd><?= $arResult["CREATOR_BIRTHDAY"] ?></dd>
                    <? endif; ?>
                    <dt>Город:</dt>
                    <dd><?= $arResult["IBLOCK"]["NAME"] ?></dd>
                    <? if ($USER->IsAuthorized()) { ?>
                        <? if ($arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"]): ?>
                            <dt>Телефон:</dt>
                            <dd><?= $arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"] ?></dd>
                        <? endif; ?>
                    <? } ?>
                    <? if ($arResult["PROPERTIES"]["EDUCATION"]["VALUE"]): ?>
                        <dt>Образвание:</dt>
                        <dd><?= $arResult["PROPERTIES"]["EDUCATION"]["VALUE"] ?></dd>
                    <? endif; ?>
                    <? if ($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"]): ?>
                        <dt>Опыт работы:</dt>
                        <dd><?= $arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></dd>
                    <? endif; ?>        
                    <? if ($arResult["PROPERTIES"]["ADD_USER_INFO"]["VALUE"]): ?>
                        <dt class="last">О себе:</dt>
                        <dd class="last"><span class="italic em0_83"><?= $arResult["PROPERTIES"]["ADD_USER_INFO"]["VALUE"] ?></span></dd>
                    <? endif; ?>
                </dl>
            </div>
        </div>
        <div class="resume-params">
            <h3 class="content-h">Пожелания к месту работы</h3>
            <? if (strlen($arResult["PROPERTIES"]["SUGG_WORK_MOVE"]["VALUE"]) > 0): ?><span class="italic"> <?= $arResult["PROPERTIES"]["SUGG_WORK_MOVE"]["VALUE"] ?><? endif ?></span>
            <table class="table-info">
                <tr>
                    <? if ($arResult["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"]): ?>
                        <th>Заработная плата</th>
                    <? endif; ?>
                    <? if ($arResult["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]): ?>
                        <th>График работы</th>
                    <? endif; ?>
                    <? if ($arResult["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"]): ?>
                        <th>Метро</th>
                    <? endif; ?>
                </tr>
                <tr>
                    <? if ($arResult["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"]): ?>
                        <td><?= $arResult["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"] ?> &mdash; <?= $arResult["PROPERTIES"]["SUGG_WORK_ZP_TO"]["VALUE"] ?> <span class="rouble font12">e</span></td>
                    <? endif; ?>
                    <? if ($arResult["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]): ?>
                        <td><?= implode(" / ", $arResult["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]) ?></td>
                    <? endif; ?>
                    <? if ($arResult["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"]): ?>
                        <td><div class="metro_<?= CITY_ID ?>"><?= implode(', ', $arResult['PROPERTIES']["SUGG_WORK_SUBWAY"]["VALUE"]) ?></div></td>
                    <? endif; ?>
                </tr>
            </table>
        </div>
        <hr class="content-separator">
        <br class="br-content-blocks" style="margin-top: 1.5em;">
        <div class="resume-params">
            <? if ($arResult["PROPERTIES"]["EXP_WHEN_1"]["VALUE"]): ?>
            <h3 class="content-h">Опыт работы</h3>
            <div class="resume-params-experience">
                <table class="table-info">
                    <? foreach ($arResult["PROPERTIES"]["EXP_WHEN_1"]["VALUE"] as $keyWork => $work): ?>

                        <tr>
                            <th>Когда?</th>
                            <th>Где?</th>
                            <th>Должность</th>
                            <th>Обязанности</th>
                        </tr>
                        <tr>
                            <td><?= $work ?> — <?= $arResult["PROPERTIES"]["EXP_WHEN_2"]["VALUE"][$keyWork] ?></td>
                            <td><?= $arResult["PROPERTIES"]["EXP_WHERE"]["VALUE"][$keyWork] ?></td>
                            <td><?= $arResult["PROPERTIES"]["EXP_POST"]["VALUE"][$keyWork] ?></td>
                            <td width="35%"><?= $arResult["PROPERTIES"]["EXP_RESPONS"]["VALUE"][$keyWork] ?></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr class="content-row-separator">
                            </td>
                        </tr>
                    <? endforeach ?>
                </table>
            </div>
            <? endif; ?>
        </div>
        <div class="panel-actions">
            <div class="block-info-comments">
                Комментарии: (<a href="#comments" class="anchor another" onclick="return false;"><?= intval($arResult["PROPERTIES"]["COMMENTS"]["VALUE"]) ?></a>)
            </div>
            <div class="panel-action-likes">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:asd.share.buttons", "likes", Array(
                    "ASD_TITLE" => $arResult["NAME"],
                    "ASD_URL" => $arResult["DETAIL_PAGE_URL"],
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => $arResult["NAME"],
                    "ASD_INCLUDE_SCRIPTS" => array(0 => "FB_LIKE", 1 => "VK_LIKE", 2 => "TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                        )
                );
                ?>
            </div>
            <div class="panel-action-response">
                <form id="response_resume_form" action="<?= $templateFolder ?>/response.php">
                    <?= bitrix_sessid_post() ?>
                    <input type="hidden" name="resume_id" value="<?= $arResult["ID"] ?>" />
                    <a href="javascript:void(0)" id="response_resume">Откликнуться</a>
                </form>
            </div>
            <div class="panel-action-tofavourite">
                <?
                $params = array();
                $params["id"] = $arResult["ID"];
                $params = addslashes(json_encode($params));
                ?>
                <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?= $params ?>",favorite_success)'>В избранное</a>
            </div>
        </div>
        <div id="comments"></div>
        <div class="clear"><br /></div>
        <?
//        $APPLICATION->IncludeComponent(
//                "restoran:comments_add_new", "new", Array(
//            "IBLOCK_TYPE" => "comment",
//            "IBLOCK_ID" => (SITE_ID == "s1") ? "2438" : "2641",
//            "ELEMENT_ID" => $arResult["ID"],
//            "IS_SECTION" => "N",
//            "OPEN" => "N"
//                ), false
//        );
        ?>
        <div id="tabs_block8" style="margin-top: 15px;">
            <ul class="tabs">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?= GetMessage("R_VK") ?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?= GetMessage("R_FACEBOOK") ?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
                <div class="pane" style="display:block">
                    <!-- Put this script tag to the <head> of your page -->
                    <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

                    <script type="text/javascript">
                        VK.init({apiId: 2881483, onlyWidgets: true});
                    </script>

                    <!-- Put this div tag to the place, where the Comments block will be -->
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                        VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                    </script>
                </div>
                <div class="pane">
                    <div class="fb-comments" data-href="http://www.restoran.ru<?= $APPLICATION->GetCurPage() ?>" data-num-posts="20" data-width="728"></div>
                </div>
            </div>            
        </div>
    </div>
    <div class="content-sidebar">
        <div id="search_article">
            <?
//            $APPLICATION->IncludeComponent(
//                    "bitrix:subscribe.form", "main_page_subscribe", Array(
//                "USE_PERSONALIZATION" => "Y",
//                "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
//                "SHOW_HIDDEN" => "N",
//                "CACHE_TYPE" => "A",
//                "CACHE_TIME" => "3600",
//                "CACHE_NOTES" => ""
//                    ), false
//            );
            ?>           
        </div>
        <div align="center">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner", "", Array(
                "TYPE" => "right_2_main_page",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
                    ), false
            );
            ?>
        </div>
        <br /><br />
        <div class="top_block">
            Популярные
        </div>
        <?
        global $arrFilterPopular;
        $arrFilterPopular = Array(
            "!CODE" => $_REQUEST["CODE"]
        );
        $APPLICATION->IncludeComponent(
                "bitrix:news.list", "resume_right", Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "resume",
            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "SHOWS",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterPopular",
            "FIELD_CODE" => array("CREATED_BY"),
            "PROPERTY_CODE" => array("WAGES_OF", "WAGES_TO", "EXPERIENCE", "SUBWAY_BIND", "COMMENTS"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        ?>
        <Br />
        <div align="center">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner", "", Array(
                "TYPE" => "right_1_main_page",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
                    ), false
            );
            ?>
        </div>
        <br /><br />
        <?
        $APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/order_rest.php"), Array(), Array("MODE" => "html")
        );
        ?>
    </div>
</div>

