<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$APPLICATION->SetTitle($arResult["SECTION"]["PATH"][1]["NAME"]);
?>
<?
if (!CModule::IncludeModule("catalog"))
    return false;
if (!CModule::IncludeModule("sale"))
    return false;

if ($arResult["ID"])
{
        CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID(), false);
        Add2BasketByProductID($arResult["ID"], 1);
}
?>
