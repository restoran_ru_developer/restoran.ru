<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $DB;
if (!CModule::IncludeModule("catalog"))
    return false;
if (!CModule::IncludeModule("sale"))
    return false;

// format date
$date = $DB->FormatDate($arResult["SECTION"]["PATH"][0]["DATE_CREATE"], 'YYYY-MM-DD HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
$arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
$arTmpDate = explode(" ", $arTmpDate);
$arResult["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
$arResult["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

$arTmpDate = array();
$arTmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_FROM"]);
$arResult["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
$arResult["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
$arResult["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
$arTmpDate = array();
$arResult["DISPLAY_ACTIVE_TO"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ACTIVE_TO"], CSite::GetDateFormat()));
$arTmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_TO"]);
$arResult["DISPLAY_ACTIVE_TO_DAY"] = $arTmpDate[0];
$arResult["DISPLAY_ACTIVE_TO_MONTH"] = $arTmpDate[1];
$arResult["DISPLAY_ACTIVE_TO_YEAR"] = $arTmpDate[2];

/*$arResult["PRICE"] = CPrice::GetBasePrice($arResult["ID"]);
$arResult["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["PRICE"]["PRICE"]);        */
?>