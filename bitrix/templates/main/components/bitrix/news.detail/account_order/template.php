<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h2><?=GetMessage("YOUR_ACCOUNT")?></h2>
<div class="left">    
    <div class="detail_kupon_image" style="height: 118px; overflow: hidden;">
        <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="200" />
    </div>        
</div>
<div class="right" style="width:465px">
    <p><b><?=$arResult["NAME"]?></b></p>
    <p>
        <?=$arResult["PREVIEW_TEXT"]?>
    </p>
</div>
<div class="clear"></div>        