<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["NEXT_POST"] = "Previous post";
$MESS["PREV_POST"] = "Next post";
$MESS["CUISINE"] = "Kitchen";
$MESS["AVERAGE_BILL"] = "Average check";
$MESS["TAGS_TITLE"] = "Tags";
$MESS["R_ADD2FAVORITES"] = "Add to favorites";
$MESS["R_COMMENTS"] = "Comments";
$MESS["SUBSCRIBE"] = "подписаться";
$MESS["UNSUBSCRIBE"] = "отписаться";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_VK"] = "VK.com";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["READ_A"] = "Read also";
?>