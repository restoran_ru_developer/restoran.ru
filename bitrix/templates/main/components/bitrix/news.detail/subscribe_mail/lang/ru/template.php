<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["NEXT_POST"] = "Предыдущая рассылка";
$MESS["PREV_POST"] = "Следующая рассылка";
$MESS["CUISINE"] = "Кухня";
$MESS["AVERAGE_BILL"] = "Средний счет";
$MESS["TAGS_TITLE"] = "Теги";
$MESS["R_ADD2FAVORITES"] = "В избранное";
$MESS["R_COMMENTS"] = "Комментарии";
$MESS["SUBSCRIBE"] = "подписаться";
$MESS["UNSUBSCRIBE"] = "отписаться";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_VK"] = "Вконтакте";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["READ_A"] = "Читайте также";
?>