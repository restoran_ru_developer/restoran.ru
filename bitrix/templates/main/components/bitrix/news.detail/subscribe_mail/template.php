<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="content">
    <div class="left" style="width:730px;">        
        <div class="statya_section_name">
            <a class="uppercase another font14" href="<?=$arResult["SECTION_PAGE_URL"]?>?USER_ID=<?=$USER->GetId()?>">Рассылки</a>
            <div class="statya_nav"> 
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>&USER_ID=<?=$USER->GetId()?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>&USER_ID=<?=$USER->GetId()?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1>Рассылка по категориям от <?=$arResult["CREATED_DATE_FORMATED_1"]?> <?=$arResult["CREATED_DATE_FORMATED_2"]?></h1>
        
        <?=$arResult["DETAIL_TEXT"]?>
            <div class="clear"></div>
            <br />
        <?if($arParams["EDIT_NOW"]=="Y"):?>
            <a href="/users/id<?=$arResult["CREATED_BY"]?>/blog/editor/blog.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
                <input type="button" class="light_button" value="Продолжить редактирование">
            </a>
            <br /><Br />
        <?endif;?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "content_article_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
    <div class="right" style="width:240px">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>      
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br /><Br />
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
    </div>
    <div class="clear"></div>  
    <br /><br />
    <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
    <br /><Br />
    <?$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner",
        "",
        Array(
                "TYPE" => "bottom_rest_list",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
        ),
        false
    );
    ?>
    <br /><br />
</div>