<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div id="comments">
    <ul>
        <li>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td width="80">
                        <div class="ava"></div>
                    </td>
                    <td width="150">
                        <span class="name">Длинное имя автора комментария</span>
                        <span class="comment_date">20</span> декабря 2011, 11:20
                    </td>
                    <td>
                        <i>«Каждый наш гость найдёт в кафе уютную атмосферу, вкусную еду и честное обслуживание»
А на шестой фотографии сотрудник кафе настороженно, и может даже, мрачновато смотрит на фотографа.</i>
                    </td>
                </tr>
            </table>
        </li>
        <li>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td width="80">
                        <div class="ava"></div>
                    </td>
                    <td width="150">
                        <span class="name">Длинное имя автора комментария</span>
                        <span class="comment_date">20</span> декабря 2011, 11:20
                    </td>
                    <td>
                        <i>«Каждый наш гость найдёт в кафе уютную атмосферу, вкусную еду и честное обслуживание»
А на шестой фотографии сотрудник кафе настороженно, и может даже, мрачновато смотрит на фотографа.</i>
                    </td>
                </tr>
            </table>
            <ul>
                <li>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="80">
                                <div class="ava"></div>
                            </td>
                            <td width="150">
                                <span class="name">Длинное имя автора комментария</span>
                                <span class="comment_date">20</span> декабря 2011, 11:20
                            </td>
                            <td>
                                <i>«Каждый наш гость найдёт в кафе уютную атмосферу, вкусную еду и честное обслуживание»
                                А на шестой фотографии сотрудник кафе настороженно, и может даже, мрачновато смотрит на фотографа.</i>
                            </td>
                        </tr>
                    </table>                    
                </li>
            </ul>
        </li>
        <li>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td width="80">
                        <div class="ava"></div>
                    </td>
                    <td width="150">
                        <span class="name">Длинное имя автора комментария</span>
                        <span class="comment_date">20</span> декабря 2011, 11:20
                    </td>
                    <td>
                        <i>«Каждый наш гость найдёт в кафе уютную атмосферу, вкусную еду и честное обслуживание»
А на шестой фотографии сотрудник кафе настороженно, и может даже, мрачновато смотрит на фотографа.</i>
                    </td>
                </tr>
            </table>
        </li>
    </ul>
   <br /><br /><br />
   <script>
       $(document).ready(function(){
            $.tools.validator.localize("ru", {
                '*'	: 'Поле обязательно для заполнения',
                '[req]'	: 'Поле обязательно для заполнения'
            });
            $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
                if (!value)
                    return false;
                return true;
            });
            $("#comments > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
                var form = $(this);
                if (!e.isDefaultPrevented()) {
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize(),
                        success: function(data) {
                            $("#rating_overlay").html(data);
                            //$(".error").hide();
                             $('#rating_overlay').overlay({
                                top: 260,                    
                                closeOnClick: false,
                                closeOnEsc: false,
                                api: true
                            }).load();
                            $(".add_review").html("");
                        }
                    });
                    e.preventDefault();
                }
            });
            $("#comments > form").bind("onFail", function(e, errors)  {
                if (e.originalEvent.type == 'submit') {
                        $.each(errors, function()  {
                                var input = this.input;	                        
                        });
                }
            });
    });
   </script>
   <form action="/bitrix/components/restoran/review.add/ajax.php" method="post" novalidate="novalidate">
       <div class="left">
           <textarea class="add_review" name="review" req="req"></textarea>
       </div>
       <div class="right">
           <table>
               <tr>
                   <td>
                       <div class="ava"></div>
                   </td>
                   <td>
                       <span class="uppercase"><?=$USER->GetFullName()?></span>
                   </td>
               </tr>
           </table>           
           <div class="figure_link">
               <input type="submit" value="+ Комментировать">
               <!--<a href="#">+ Добавить отзыв</a>-->
           </div>
       </div>
        <div id="rating_overlay">
            <div class="close"></div>                           
        </div>
       <div class="clear"></div>
    </form>
   <br />
   <div class="grey">
       <p>Дорогие друзья! Помните, что администрация сайта будет удалять:</p>
    <p>1. Комментарии с грубой и ненормативной лексикой<br />
    2. Прямые или косвенные оскорбления героя поста или читателей<br />
    3. Короткие оценочные комментарии («ужасно», «класс«, «отстой»)<Br />
    4. Комментарии, разжигающие национальную и социальную рознь<br />
    <p><a href="#">Полная версия правил Restoran.ru</a></p>
   </div>
</div>