<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript" src="/tpl/js/jquery.photo.js"></script>
<script>
function add2basket_dostavka(obj, id, sess)
{
	var pos_y = $(obj).offset().top-10;
	var pos_x = $(obj).offset().left-10;
	var q = $("#quantity"+id).val();
	if (!$("body").find("div").hasClass("dostavka_popup"))
		$("body").append("<div class='dostavka_popup'></div>");
	$(".dostavka_popup").css('left',pos_x+"px");
	$(".dostavka_popup").css('top',pos_y+"px");
	$(".dostavka_popup").load("/tpl/ajax/add2basket_dostavka.php?id="+id+"&q="+q+"&"+sess);		
        $(".dostavka_popup").fadeIn(300);
}

function del_basket_item(id, sess)
{
	$(".dostavka_popup").load("/tpl/ajax/add2basket_dostavka.php?id="+id+"&action=delete&"+sess);				
}
function preview_modal(id)
{
    $.ajax({
            type: "POST",
            url: "/tpl/ajax/menu_preview.php",
            data: "ID="+id,
            success: function(data) {
                if (!$("#preview_modal").size())
                {
                    $("<div class='popup popup_modal' id='preview_modal'></div>").appendTo("body");                                                               
                }
                $('#preview_modal').html(data);
                showOverflow();
                setCenter($("#preview_modal"));
                $("#preview_modal").fadeIn("300"); 
            }
        });    
}
$(document).ready(function(){	
	$(window).keyup(function(event) {
	  if (event.keyCode == '27') {
		$(".dostavka_popup").fadeOut(300);
	   }
	}); 
        $(".menu_links a").attr("onclick","");
        $("#dostavka_table").photo();
});
</script>    
<h1 class="menu_name"><?=$arResult["NAME"]?></h1>
<?if (!count($arResult["ITEMS"])):?>
<?endif;?>
<?if (count($arResult["ITEMS"])>0):?>
    <table id="dostavka_table" cellpadding="0" cellspacing="0">        
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
        <?
        $cols = 1;
        if (!is_array($arElement["PREVIEW_PICTURE"]))
            $cols++;
        if (!trim($arElement["PREVIEW_TEXT"]))
            $cols++;
        if (!$arElement["PRICES"]["BASE"]["VALUE"]&&!$arElement["CAN_BUY"]&&!substr_count($_SERVER["HTTP_REFERER"],"dostavka"))
        {
            $preview = "end";
        }
        elseif ($arElement["PRICES"]["BASE"]["VALUE"]&&!$arElement["CAN_BUY"]&&!substr_count($_SERVER["HTTP_REFERER"],"dostavka"))
        {
            $price = "end";
        }
        else
        {
            $buy = "end";
        }
        ?>
		<tr class="<?=(end($arResult["ITEMS"])==$arElement)?"end":""?>">                    
                    <?if(is_array($arElement["PREVIEW_PICTURE"])):?>
                        <td>
                            <img photo-url="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>"  style="cursor:pointer" border="0" src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="80" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" />
                        </td>
                    <?endif;?>
                        <td colspan="<?=$cols?>" class="name">
                            <?=$arElement["~NAME"]?>
                        </td>
                    <?if (trim($arElement["PREVIEW_TEXT"])):?>
                        <td class="preview_text <?=$preview?>">
                            <?=$arElement["PREVIEW_TEXT"]?>
                        </td>
                    <?endif;?>
                    <?if ($arElement["PRICES"]["BASE"]["VALUE"]):?>
                        <td class="price <?=$price?>">
                            <?if (substr_count($_SERVER["HTTP_REFERER"],"dostavka")):?>
                                <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                        <?if($arPrice["CAN_ACCESS"]):?>
                                                <table width="100%" cellpadding="5">
                                                        <tbody>
                                                                <tr>
                                                                    <td align="left" width="40"><input class="inputtext" id="quantity<?=$arElement["ID"]?>" maxlength="3" type="text" value="1" size="1"></td>
                                                                    <td class="font16" width="20"> x </td>
                                                                    <td nowrap=""><?=$arPrice["VALUE"]?> <span class="rouble font24">e</span></td>
                                                                </tr>
                                                        </tbody>
                                                </table>							
                                        <?endif;?>
                                <?endforeach;?>				
                            <?else:?>
                                <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                    <?if($arPrice["CAN_ACCESS"]&&$arPrice["VALUE"]>0):?>
                                        <table width="100%" cellpadding="5">
                                            <tbody>
                                                <tr>
                                                    <?/*?>
                                                    <td align="left" width="10" class="font24">1</td>
                                                    <td class="font16" width="10"> x </td>
                                                    <?*/?>
                                                     
                                                    <?if ($arPrice["CURRENCY"]=="EUR"):?>
                                                        <td colspan="3" nowrap="">€ <?=sprintf("%01.2f", $arPrice["VALUE"]);?></td>
                                                    <?else:?>
                                                        <td colspan="3" nowrap=""><?=$arPrice["VALUE"]?> <span class="rouble font16">e</span></td>                                                        
                                                    <?endif;?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <?endif;?>
                                <?endforeach;?>
                            <?endif;?>
                        </td>
                    <?endif;?>
                    <?if($arElement["CAN_BUY"]&&substr_count($_SERVER["HTTP_REFERER"],"dostavka")):?>
                        <td class="<?=$buy?>">
                            <!--<input name="buy" class="button" type="button" value="<?= GetMessage("CATALOG_BUY") ?>" OnClick="window.location='<?echo CUtil::JSEscape($arElement["DETAIL_PAGE_URL"]."#buy")?>'" />-->
                            <input name="buy" class="button" type="button" value="<?=GetMessage("CATALOG_BUY")?>" onClick="add2basket_dostavka(this,'<?=$arElement["ID"]?>','<?=bitrix_sessid_get()?>')" />
                        </td>
                    <?endif?>
                </tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
    </table>
<?endif;?>
<div class="clear"></div>
<br />
<div align="right">
<?/*if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;*/?>
</div>