<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");

//проверяем права на запись, если права есть, то возвращаем элемент
function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ob = $res->GetNextElement()){
		$ar_res = $ob->GetFields();  
 		$ar_res["PROPERTIES"] = $ob->GetProperties();
		
		//если пользователь оператор или админ
		if(in_array(16, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return $ar_res;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return $ar_res;
			else return false;
		}		
	}else return false;
}


function add_new_el(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$el = new CIBlockElement;
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"NAME"           => $_REQUEST["NAME"],
  		"PREVIEW_TEXT"   => $_REQUEST["PREVIEW_TEXT"],
  		"TAGS"   => $_REQUEST["TAGS"],
  		"IBLOCK_SECTION_ID" => $_REQUEST["SECTION_ID"],
  		"IBLOCK_ID" => $_REQUEST["IBLOCK_ID"]
  	);

		//Основная фотка
	if(isset($_FILES["photo"])){
		$arLoadProductArray["PREVIEW_PICTURE"]=$_FILES["photo"];
	}	
	
	
		
	if($PRODUCT_ID = $el->Add($arLoadProductArray)){
  		CPrice::SetBasePrice($PRODUCT_ID, $_REQUEST["PRICE"], $_REQUEST["currency"]);
  		echo "ok";
	}else
  		echo "Error: ".$el->LAST_ERROR;


}


function add_new(){
	?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data" id="bludo_frm">
	<input type="hidden" name="act" value="add_new_el">
	<input type="hidden" name="SECTION_ID" value="<?=$_REQUEST["SECTION_ID"]?>">
        <input type="hidden" name="IBLOCK_ID" value="<?=$_REQUEST["IBLOCK_ID"]?>">
	
	<div class="question">
		Название<div><input type="text" req="req" class="inputtext" name="NAME" value="" size=""></div>
	</div> 
	
	<div class="question">
		Описание<div><textarea class="inputtext" name="PREVIEW_TEXT"></textarea></div>
	</div> 
        <div class="question">
            <input type="checkbox" name="TAGS" value="Y" id="ma">
            <label for="ma">Выводить на странице ресторана</label>
	</div> 

	<div class="question q2">
		<div class="lc">
			Цена<div><input type="text" req="req" class="inputtext" name="PRICE" value="" size=""></div>
		</div>
            <div class="lc" style="margin-left:20px;">
			Валюта
                        <div>
                            <select name="currency" id="cur_slc">
                                <option value="RUB" selected="selected">RUB</option>
                                <option value="EUR">EUR</option>
                            </select>
                        </div>                        
		</div>
	</div>    
	
	<div class="question">
		Фотография<input type="file" class="inptfl" name="photo" value="" size="">
	</div>
	
	<input class="light_button" type="submit" name="web_form_submit" value="Сохранить">
	<input class="dark_button popup_close" type="button" value="Закрыть">	
	</form>
	
	
	<?
}

function edit(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$res = CIBlockElement::GetByID($_REQUEST["ID"]);
	if($ob = $res->GetNextElement()){
		$ar_res = $ob->GetFields();  
		
 		$ar_res["PROPERTIES"] = $ob->GetProperties();
 		
 		$price = CPrice::GetBasePrice($ar_res["ID"]);
 		
 		
 		
 		$ress = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
		if($ar_ress = $ress->GetNext()){
		
			
		}
 		
	?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data" id="bludo_frm">
	<input type="hidden" name="act" value="save_el">
	<input type="hidden" name="ID" value="<?=$ar_res["ID"]?>">
	<div class="question">
		Название<div><input type="text" req="req" class="inputtext" name="NAME" value="<?=$ar_res["NAME"]?>" size=""></div>
	</div> 
	
	<div class="question">
		Описание<div><textarea class="inputtext" name="PREVIEW_TEXT"><?=$ar_res["PREVIEW_TEXT"]?></textarea></div>
	</div>
	<div class="question">
            <input type="checkbox" name="TAGS" <?=($ar_res["TAGS"]=="Y")?"checked='checked'":""?> value="Y" id="ma">
            <label for="ma">Выводить на странице ресторана</label>
	</div> 
	<div class="question">
		Раздел<div>
		<select name="SECTION" id="rzdl_slct" style="width:300px;">
			<?
			$arFilter = Array('IBLOCK_ID'=>$ar_res["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y'/*,"DEPTH_LEVEL"=>1*/);
	 		$db_list = CIBlockSection::GetList(Array("left_margin"=>"ASC"), $arFilter, false);
			while($ar_result = $db_list->GetNext()){
				echo '<option value="'.$ar_result["ID"].'"';
				if($ar_res["IBLOCK_SECTION_ID"]==$ar_result["ID"]) echo ' selected="selected"';
				echo '> &nbsp;&nbsp;';
                                for ($i=1;$i<$ar_result["DEPTH_LEVEL"];$i++)
                                    echo ".";
                                echo $ar_result["NAME"];
                                echo '</option>';
			}
			?>
		</select>
		</div>
	</div> 
	
	<script type="text/javascript">
		$("#rzdl_slct").chosen(); 			
	</script>
	
	<div class="question q2">
		<div class="lc">                    
			Цена<div><input type="text" req="req" class="inputtext" name="PRICE" value="<?=intVal($price["PRICE"])?>" size=""></div>
		</div>
		<div class="lc" style="margin-left:20px;">
			Валюта
                        <div>
                            <select name="currency" id="cur_slc">
                                <option value="RUB" <?=($price["CURRENCY"]=="RUB")?'selected="selected"':''?>>RUB</option>
                                <option value="EUR" <?=($price["CURRENCY"]=="EUR")?'selected="selected"':''?>>EUR</option>
                            </select>
                        </div>                        
		</div>
		
		
	</div>    
	
	<div class="question">
		Фотография<input type="file" class="inptfl" name="photo" value="" size="">
	</div>
	
	<input class="light_button" type="submit" name="web_form_submit" value="Сохранить">
	<input class="dark_button popup_close" type="button" value="Закрыть">	
	</form>
	<?
	}
}

function save_el(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$el = new CIBlockElement;
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"NAME"           => $_REQUEST["NAME"],
  		"PREVIEW_TEXT"   => $_REQUEST["PREVIEW_TEXT"],
                "TAGS"    => $_REQUEST["TAGS"],
  		"IBLOCK_SECTION_ID" =>$_REQUEST["SECTION"]
  	);

		//Основная фотка
	if(isset($_FILES["photo"])){
		$arLoadProductArray["PREVIEW_PICTURE"]=$_FILES["photo"];
	}	
		
	if($el->Update($_REQUEST["ID"], $arLoadProductArray)) echo "ok";
	CPrice::SetBasePrice($_REQUEST["ID"], $_REQUEST["PRICE"], $_REQUEST["currency"]);

}

function del(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	CIBlockElement::Delete($_REQUEST["ID"]);


}

if($_REQUEST["act"]=="edit") edit();
if($_REQUEST["act"]=="save_el") save_el();

if($_REQUEST["act"]=="del") del();

if($_REQUEST["act"]=="add_new") add_new();
if($_REQUEST["act"]=="add_new_el") add_new_el();


?>
