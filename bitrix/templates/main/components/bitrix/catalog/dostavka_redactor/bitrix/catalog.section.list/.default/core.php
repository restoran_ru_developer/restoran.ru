<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function new_rzdel(){
    CModule::IncludeModule("iblock")
	?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="act" value="add_new_razdel">
		<input type="hidden" name="SECTION_ID" value="<?=$_REQUEST["SECTION_ID"]?>">
                <input type="hidden" name="IBLOCK_ID" value="<?=$_REQUEST["IBLOCK_ID"]?>">
		<input type="hidden" name="ELEMENT_ID" value="<?=$_REQUEST["ELEMENT_ID"]?>">

		<div class="question">
			Название раздела<div><input type="text" req="req" class="inputtext" name="NAME" value="" size=""></div>
		</div> 
                <div class="question">
			Сортировка<div><input type="text" req="req" class="inputtext" name="SORT" value="" size="" style="width:100px"></div>
		</div> 
                <?if ($_REQUEST["IBLOCK_ID"]):?>
                <div class="question">
                        Раздел<div>
                        <select name="SECTION" id="rzdl_slct2" style="width:300px;">
                            <option>.</option>
                                <?
                                $arFilter = Array('IBLOCK_ID'=>$_REQUEST["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y',"DEPTH_LEVEL"=>1);
                                $db_list = CIBlockSection::GetList(Array("left_margin"=>"ASC"), $arFilter, false);
                                while($ar_result = $db_list->GetNext()){
                                        echo '<option value="'.$ar_result["ID"].'"';
                                        if($_REQUEST["SECTION_ID"]==$ar_result["ID"]) echo ' selected="selected"';
                                        echo '> &nbsp;&nbsp;';
                                        for ($i=1;$i<$ar_result["DEPTH_LEVEL"];$i++)
                                            echo ".";
                                        echo $ar_result["NAME"];
                                        echo '</option>';
                                }
                                ?>
                        </select>
                        </div>
                </div> 
                <?endif;?>
                <script type="text/javascript">
                        $("#rzdl_slct2").chosen(); 			
                </script>
	

		<input class="light_button" type="submit" name="web_form_submit" value="Добавить">
		<input class="dark_button popup_close" type="button" value="Закрыть">
	</form>


	<?
}



function add_new_razdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$bs = new CIBlockSection;
        if ($_REQUEST["SECTION_ID"])
            $_REQUEST["SECTION_ID"] = false;
        global $DB;
        $sql = "SELECT ID FROM b_iblock WHERE DESCRIPTION=".$DB->ForSql($_REQUEST["ELEMENT_ID"]);
        $db_list = $DB->Query($sql);
        if($ar_result = $db_list->GetNext())
        {
            $menu = $ar_result['ID'];
        }
        else
        {
            
            $sql = "SELECT NAME FROM b_iblock_element WHERE ID=".$DB->ForSql($_REQUEST["ELEMENT_ID"]);
            $db_list = $DB->Query($sql);
            if($ar_result = $db_list->Fetch())
            {
                $name = $ar_result['NAME'];
            }
            $ib = new CIBlock;
            $arFields = Array(
                "ACTIVE" => "Y",
                "NAME" => $name,
                "CODE" => '',
                "LIST_PAGE_URL" => '',
                "DETAIL_PAGE_URL" => '',
                "IBLOCK_TYPE_ID" => 'rest_menu_ru',
                "SITE_ID" => Array(SITE_ID),
                "SORT" => 500,
                "DESCRIPTION" => $_REQUEST["ELEMENT_ID"],
                "DESCRIPTION_TYPE" => 'text',
                "LIST_MODE" => 'C',
                "GROUP_ID" => Array("2"=>"R", "14"=>"W", "15"=>"W")
            );
            if($ID = $ib->Add($arFields)) {
                $menu = $ID;
            }
        }
    $arParams = array("replace_space"=>"-","replace_other"=>"-");
    $section_code = Cutil::translit($_REQUEST["NAME"],"ru",$arParams);

	$arFields = Array(
  		"ACTIVE" => "Y",
  		"IBLOCK_SECTION_ID" =>$_REQUEST["SECTION"],
  		"IBLOCK_ID" => $menu,
  		"NAME" => $_REQUEST["NAME"],
  		"SORT" => $_REQUEST["SORT"],
        'CODE' => $section_code
  	);

  	if($ID = $bs->Add($arFields)) echo "ok";
  	else echo $bs->LAST_ERROR;

}


function save_rzdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$bs = new CIBlockSection;
	$arFields = Array("NAME" => $_REQUEST["NAME"],"SORT"=> $_REQUEST["SORT"],"IBLOCK_SECTION_ID"=>$_REQUEST["SECTION"]);
	$res = $bs->Update($_REQUEST["SECTION_ID"], $arFields);
	
}


function edit_rzdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}        
	$res = CIBlockSection::GetByID($_REQUEST["SECTION_ID"]);
	if($ar_res = $res->GetNext()){
	?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="act" value="save_rzdel">
		<input type="hidden" name="SECTION_ID" value="<?=$_REQUEST["SECTION_ID"]?>">

		<div class="question">
			Название раздела<div><input type="text" req="req" class="inputtext" name="NAME" value="<?=$ar_res["NAME"]?>" size=""></div>
		</div> 
                <div class="question">
                        Раздел<div>
                        <select name="SECTION" id="rzdl_slct" style="width:300px;">
                            <option>.</option>
                                <?
                                $arFilter = Array('IBLOCK_ID'=>$_REQUEST["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y',"DEPTH_LEVEL"=>1);
                                $db_list = CIBlockSection::GetList(Array("left_margin"=>"ASC"), $arFilter, false);
                                while($ar_result = $db_list->GetNext()){
                                        echo '<option value="'.$ar_result["ID"].'"';
                                        if($ar_res["IBLOCK_SECTION_ID"]==$ar_result["ID"]) echo ' selected="selected"';
                                        echo '> &nbsp;&nbsp;';
                                        for ($i=1;$i<$ar_result["DEPTH_LEVEL"];$i++)
                                            echo ".";
                                        echo $ar_result["NAME"];
                                        echo '</option>';
                                }
                                ?>
                        </select>
                        </div>
                </div> 

                <script type="text/javascript">
                        $("#rzdl_slct").chosen(); 			
                </script>
                <div class="question">
			Сортировка<div><input type="text" req="req" class="inputtext" name="SORT" value="<?=$ar_res["SORT"]?>" size=""  style="width:100px"></div>
		</div>

		<input class="light_button" type="submit" name="web_form_submit" value="Сохранить">
		<input class="dark_button popup_close" type="button" value="Закрыть">
	</form>

	<?
	}
}

function del_rzdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if($_REQUEST["SECTION_ID"])
    	CIBlockSection::Delete($_REQUEST["SECTION_ID"]);


}


if($_REQUEST["act"]=="save_rzdel") save_rzdel();
if($_REQUEST["act"]=="del_rzdel") del_rzdel();
if($_REQUEST["act"]=="edit_rzdel") edit_rzdel();
if($_REQUEST["act"]=="new_rzdel") new_rzdel();
if($_REQUEST["act"]=="add_new_razdel") add_new_razdel();
?>

