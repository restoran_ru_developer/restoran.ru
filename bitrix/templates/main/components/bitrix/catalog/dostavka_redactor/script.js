

$(document).ready(function() {

    // select or deselect all checkboxes
    $('#select_all').on('change', function() {
        if($(this).is(':checked'))
        {
            // select all
            $('input.section_checkboxes').each(function(index) {
                $(this).attr('checked', true)
            });
        } else {
            // deselect all
            $('input.section_checkboxes').each(function(index) {
                $(this).attr('checked', false)
            });
        }
    });

    // remove  sections
    $('div.remove_sel_sections a').on('click', function(e) {

        var arSelSec = new Array();
        $('input.section_checkboxes').each(function(index) {
            if($(this).is(':checked'))
            {
                arSelSec[index] = $(this).val();
            }
        });

        if(arSelSec.length > 0)
        {
            if(confirm("Удалить выбранные разделы?"))
            {
                showOverflow();

                for (var i = 0; i < arSelSec.length; i++)
                {
                    if(arSelSec[i])
                    {
                        var lnk = $(this).attr('href') + arSelSec[i];
                        var obj = $('#tr_' + arSelSec[i]);
                        $.ajax({
                            type: "POST",
                            url: lnk,
                            async: false
                        })
                        .done(function( data ) {
                            obj.remove();
                        });
                    }
                }

                hideOverflow();
            }
        } else {
            alert("Выберите хотя бы один раздел!");
        }

        e.preventDefault();
    });



$("#add_new_bl").live("click", function(){
	var lnk = $(this).attr("href");
        showOverflow();
	$.post(lnk, function(html){
		
		if (!$("#bludo_modal").size()){
			$("<div class='popup popup_modal' id='bludo_modal'></div>").appendTo("body");                                                               
		}
        
 		$('#bludo_modal').html(html);

		setCenter($("#bludo_modal"));
		$("#bludo_modal").fadeIn("300");
		
		$('#bludo_modal .inptfl').customFileInput();		
		updt_frm();                

	});
	return false;
});


//Редактировать
$("#my_blud .icon-continue").live("click", function(){
	var lnk = $(this).attr("href");
        showOverflow();
	$.post(lnk, function(html){
		
		if (!$("#bludo_modal").size()){
			$("<div class='popup popup_modal' id='bludo_modal'></div>").appendTo("body");                                                               
		}
        
 		$('#bludo_modal').html(html);

		setCenter($("#bludo_modal"));
		$("#bludo_modal").fadeIn("300");
		
		$('#bludo_modal .inptfl').customFileInput();
		
		updt_frm();

	});
	return false;
});


function updt_frm(){
	//проверка формы
	function check_editor_form(a,f,o){
		var ret=true;
		o.dataType = "html";
		return ret;
	}

	//Отправка формы
	$('#bludo_modal form').ajaxForm({
		beforeSubmit: check_editor_form,
		success: function(data) {
			console.log(data);
			window.location.reload();
                        $(".menu_menu li.active a:first").click();
                        $("#bludo_modal").fadeOut(300);
                        window.location.reload();                       			
		}
	});
}

$(".popup_close").live("click", function(){
	$("#bludo_modal").fadeOut("100");
	$("#rzdl_modal").fadeOut("100");
	hideOverflow();
	return false;	
});

//Удалить
$("#my_blud .icon-delete").live("click", function(){
	if (confirm("Вы точно хотите удалить этот элемент?"))
        {
            var lnk = $(this).attr("href");
            var obj = $(this).parent("div").parent("td").parent("tr");
            $.post(lnk, function(html){
                    obj.remove();
            });
        }
	return false;
});


});