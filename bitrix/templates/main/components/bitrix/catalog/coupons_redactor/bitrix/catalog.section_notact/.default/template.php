<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if($arResult["ID"]>0){
?>
<a class="light_button" href="kup_edite/?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["ID"]?>" id="add_new_bl">+ Новый купон</a>
<? }?>

<? if (count($arResult["ITEMS"])>0){?>
<table class="profile-activity w100" id="my_blud" cellpadding="0" cellspacing="0">
	<tr>
		<th class="first">Фото</th>
		<th>Название</th>
		<th>Стоиомсть</th>
		<th>Описание</th>
		<th></th>
	</tr>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
	
	$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>73, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);
	?>
	<tr>
		<td class="first w-new">
			<img src="<?=$file["src"]?>" width="73" height="60" />
         </td>
		 <td>
		 	<?=$arElement["NAME"]?>
		</td>
		<td>
			<?=$arElement["PRICES"]["BASE"]["PRINT_VALUE"];?>
		</td>
		<td>
			<?=$arElement["PREVIEW_TEXT"]?>
		</td>
		
		<td>
		    <div class="activity-actions w-small">
				<a class="icon-continue" href="kup_edite/?ID=<?=$arElement["ID"]?>&IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arElement["IBLOCK_SECTION_ID"]?>">Редактировать</a> 
		    	<a class="icon-delete" href="<?=$templateFolder?>/core.php?act=del&ID=<?=$arElement["ID"]?>">Удалить</a> 
			</div>
		</td>
	</tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
</table>
<div class="clear"></div>
<hr class="bold">
<div align="right">
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>

<?
}else echo "<h4>В разделе нет товаров</h4>";

//v_dump($arResult);
?>




