<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?=$arResult["FORM_HEADER"]?>
<?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
    <?if ($arResult["isFormTitle"]):?>
        <div class="title" style="text-align: right; border:0px">
            <?//=$arResult["FORM_TITLE"]?>
        </div>
    <?endif;?>
<?endif;?>
<?//v_dump($arResult["QUESTIONS"])?>
<div class="ok">
    <?=$arResult["FORM_NOTE"]?>
</div>
<?if ($arResult["isFormErrors"] == "Y"):?>
    <?=$arResult["FORM_ERRORS_TEXT"];?>
<?endif;?>

<?if(!$arResult["FORM_NOTE"]):?>

    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion): ?>
        <div class="question">
            <?=$arQuestion["CAPTION"]?>
            <?=$arQuestion["HTML_CODE"]?><br />
        </div>
    <?endforeach;?>
    <?if($arResult["isUseCaptcha"] == "Y"): ?>
        <b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b><br />
        <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" /><br />
        <?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?><br />
        <input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /><br />
    <?endif; ?>
    <input type="hidden" name="web_form_apply" value="Y" />
    <div align="right"><input class="light_button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"];?>" /></div>
    <!--<input class="dark_button popup_close" type="button" value="<?=GetMessage("FORM_CLOSE")?>"/>-->

<?endif?>

<?=$arResult["FORM_FOOTER"]?>
