<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?if ($arResult["isFormErrors"] == "Y"):?>

	<?=$arResult["FORM_ERRORS_TEXT"];?>


	<script>
		$(window).load(function(){
		
			/**
			* Во время обработки формы (которая во всплывающем окне) обнаружены ошибки
			* Принудительно открываем её, спровоцировав нажатие по её ссылке
			*/
			setTimeout(function(){
				$("a.icon-continue").click();
			},150);
		});
	</script>


	
<?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y") 
{
?>
<?=$arResult["FORM_HEADER"]?>


<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y") 
{ 
?>
<?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormTitle"]) 
{
?>
	<h1 style="color:#FFFFFF;"><?=$arResult["FORM_TITLE"]?></h1>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>

			<p><?=$arResult["FORM_DESCRIPTION"]?></p>

	<? 
} // endif 
	?>

<br />
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>


	<?

	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
	$arQuestion['QCODE'] = $FIELD_SID;
	//echo $FIELD_SID;
	$breaker = false;
	switch($FIELD_SID)
	{
		
		case "Q_BANNER_PROLONGATE_2":
		?>
			<input type="hidden" size="0" name="form_text_56" class="inputtext w100" id="Q_BANNER_PROLONGATE_2">
		<?
		$breaker = true;	
		break;
		
	}
	
	if($breaker) continue;
	?>

				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>
				<br/><?=$arQuestion["CAPTION"]?><br/>
				<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>

			<?=$arQuestion["HTML_CODE"]?>
		<br/>
	<? 
	} //endwhile 
	?>
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
		<input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" />
		
		<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
<?
} // isUseCaptcha
?>


				
				<input type="submit" value="Отправить" class="button" name="web_form_submit">
				<?if ($arResult["F_RIGHT"] >= 15):?>
				&nbsp;<input type="hidden" name="web_form_apply" value="Y" />
				<?endif;?>


<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>