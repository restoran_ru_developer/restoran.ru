<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=$templateFolder?>/script.js"></script>
<?if (!$_REQUEST["web_form_apply"]&&!$_REQUEST["RESULT_ID"]):?>
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <div class="ajax_form" style="font-size:12px;">
<?endif;?>
<?=$arResult["FORM_HEADER"]?>
<?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
    <?if ($arResult["isFormTitle"]):?>
        <div class="title" style="text-align: right; border:0px">
            <?//=$arResult["FORM_TITLE"]?>
        </div>
    <?endif;?>
<?endif;?>
<?//v_dump($arResult["QUESTIONS"])?>
        <?if ($_REQUEST["RESULT_ID"]):?>
<div class="ok">    
    <?=GetMessage("FORM_DATA_SAVED1")?>
</div>
        <?endif;?>
<?if ($arResult["isFormErrors"] == "Y"):?>
    <?=$arResult["FORM_ERRORS_TEXT"];?>
<?endif;?>

<?//if(!$arResult["FORM_NOTE"]):?>

    <?/*foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion): ?>
        <div class="question" style="line-height:24px;">
            <?=$arQuestion["CAPTION"]?>:<Br />
            <?=$arQuestion["HTML_CODE"]?><br />
        </div>
    <?endforeach;*/?>
<?if (!$_REQUEST["web_form_apply"]&&!$_REQUEST["RESULT_ID"]):?>        
        <div class="question" style="line-height:24px;text-align: left">
            Ресторан: <a class="another"><?=$arResult["REST"]["NAME"]?></a>
        </div>
        <input type="hidden" class="inputtext" name="form_text_107" value="<?=$arResult["REST"]["NAME"]?> [<?=$arResult["REST"]["ID"]?>]" size="0">
        <div class="question" style="line-height:24px; text-align: left">
            Выберите необходимый тип размещения:<br />
            <select class="chzn-select" name="form_text_106" style="width:250px; margin:0 auto;">
                <?foreach ($arResult["ACCOUNTS"] as $ac):?>
                    <option <?=($ac["ID"]==$_REQUEST["priority"])?"selected":""?> value="<?=$ac["NAME"]?>"><?=$ac["NAME"]?></option>
                <?endforeach;?>
            </select>
            <style>
                .active-result
                {
                    color:#000;
                }
                .chzn-container-single .chzn-single
                {
                    width:250px;
                }
            </style>
            <script>
                $(".chzn-select").chosen();
            </script>
        </div>
        <div class="question" style="line-height:24px; text-align: left">
            Введите комментарий:<br />
            <textarea class="text" name="form_text_150" style="width:99%; height:70px;"></textarea>
        </div>
    <?if($arResult["isUseCaptcha"] == "Y"): ?>
        <b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b><br />
        <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" /><br />
        <?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?><br />
        <input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /><br />
    <?endif; ?>
    <input type="hidden" name="web_form_apply" value="Y" />
    <div class="question" align="right" style="padding-top:10px;">
        <input class="light_button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"];?>" />
    </div>
<?endif?>
<?=$arResult["FORM_FOOTER"]?>
    <?if (!$_REQUEST["web_form_apply"]&&!$_REQUEST["RESULT_ID"]):?>
</div>
<?endif;?>