$(document).ready(function(){
    $("#more_cities").popup({"obj":"more_cities_box","css":{'top':'50px','right':'-78px'}});
    $("#more_cities a").click(function(){
        $(".other_cities").css("display","none");
        var api = $("#cities_tabs").data("tabs");
         $("#"+$(this).attr("code")).css("display","block");
        api.click(parseInt($(this).attr("num")));
        $(".popup").fadeOut(300);
    });
});