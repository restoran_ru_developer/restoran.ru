<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["SECTIONS"] as $key=>$arSection):
    if ($arSection['CODE']==CITY_ID)
        $sec = Array($arSection["NAME"],$arSection["CODE"]);
endforeach;?>
<ul class="tabs" id="cities_tabs">
    <?foreach($arResult["SECTIONS"] as $key=>$arSection):?>
        <?if ($key==0&&$sec[1]):?>
            <li>
                <a href="#">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=$sec[0]?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
        <?endif;?>
        <?if ($arSection["CODE"]!=$sec[1]):?>
            <?if($arSection["CODE"]!="msk"&&$arSection["CODE"]!="spb"):?>
                <li style="display:none;" class="other_cities" id="<?=$arSection["CODE"]?>" num="<?=$key?>">
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=$arSection["NAME"]?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            <?else:?>
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=$arSection["NAME"]?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            <?endif;?>
        <?endif;?>
    <?endforeach;?>
</ul>
<div class="panes">    
    <?foreach($arResult["SECTIONS"] as $key=>$arSection):?>
        <?if ($key==0&&$sec[1]):?>
            <div class="pane">
                <?$APPLICATION->IncludeComponent("restoran:catalog.list", "contacts", array(
                        "IBLOCK_TYPE" => "contacts",
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "PARENT_SECTION_CODE" => $sec[1],
                        "NEWS_COUNT" => "10",
                        "SORT_BY1" => "PROPERTY_RATING",
                        "SORT_ORDER1" => "DESC",
                        "FILTER_NAME" => "arrFilter",
                        "PROPERTY_CODE" => array(
                                0 => "users",
                                1 => "subway",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_SHADOW" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "PREVIEW_TRUNCATE_LEN" => "100",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => ""
                        ),
                        false
                );?>

            </div>
        <?endif;?>
        <?if ($arSection["CODE"]!=$sec[1]):?>
            <div class="pane">
                <?$APPLICATION->IncludeComponent("restoran:catalog.list", "contacts", array(
                        "IBLOCK_TYPE" => "contacts",
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "PARENT_SECTION_CODE" => $arSection["CODE"],
                        "NEWS_COUNT" => "10",
                        "SORT_BY1" => "PROPERTY_RATING",
                        "SORT_ORDER1" => "DESC",
                        "FILTER_NAME" => "arrFilter",
                        "PROPERTY_CODE" => array(
                                0 => "users",
                                1 => "subway",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_SHADOW" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "PREVIEW_TRUNCATE_LEN" => "100",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => ""
                        ),
                        false
                );?>

            </div>
        <?endif;?>
    <?endforeach;?>
</div>
<div id="more_cities_box">
    <a href="javascript:void(0)" class="js" >Еще города</a>
</div>
<div id="more_cities" class="popup">
    <div class="title">Еще города</div>
    <?foreach($arResult["SECTIONS"] as $key=>$arSection):?>
        <?if($arSection["CODE"]!="msk"&&$arSection["CODE"]!="spb"):?>
        <a href="javascript:void(0)" class="another font14" num="<?=$key?>" code="<?=$arSection["CODE"]?>"><?=$arSection["NAME"]?></a>
        <?endif;?>
    <?endforeach;?>
</div>