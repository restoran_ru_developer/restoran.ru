<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul id="firms_block">
<?
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	if($CURRENT_DEPTH<$arSection["DEPTH_LEVEL"])
		echo "<ul>";
	elseif($CURRENT_DEPTH>$arSection["DEPTH_LEVEL"])
		echo str_repeat("</ul>", $CURRENT_DEPTH - $arSection["DEPTH_LEVEL"]);
	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
        $arSection["NAME"] = change_quotes($arSection["NAME"]);
?>
        <?if ($arSection["ELEMENT_CNT"]):?>
            <?if($arSection["CODE"]==$_REQUEST["FIRM_SECTION"]):?>
                <li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><?=$arSection["NAME"]?><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;(<?=$arSection["ELEMENT_CNT"]?>)<?endif;?></li>
            <?else:?>
                <li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><a class="another border" href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;(<?=$arSection["ELEMENT_CNT"]?>)<?endif;?></a></li>
            <?endif;?>
        <?endif;?>
<?endforeach?>
</ul>
<div class="clear"></div>
<script>
    $(document).ready(function(){
        $("#sver").click(function(){
           if (!$(this).hasClass("sver_a"))
           {
               $("#firms_block").slideUp();
               $(this).removeClass("sver").addClass("sver_a");
               $(this).find("a").html("<?=GetMessage("SVER_A")?>");
           }
           else
           {
                $("#firms_block").slideDown();
               $(this).removeClass("sver_a").addClass("sver");
               $(this).find("a").html("<?=GetMessage("SVER")?>");
           }
        });
    });
    
</script>
<div id="sver" class="sver"><a href="javascript:void(0)" class="js">Свернуть</a></div>