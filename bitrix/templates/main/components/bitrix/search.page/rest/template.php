<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page">
<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>
<?if(count($arResult["SEARCH"])>0):?>
        <h1><?=GetMessage("SR_TITLE")?></h1>
	<?foreach($arResult["SEARCH"] as $key=>$arItem):?>
		<!--<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
		<p><?echo $arItem["BODY_FORMATED"]?></p>
		<small><?=GetMessage("SEARCH_MODIFIED")?> <?=$arItem["DATE_CHANGE"]?></small><br /><?
		if($arItem["CHAIN_PATH"]):?>
			<small><?=GetMessage("SEARCH_PATH")?>&nbsp;<?=$arItem["CHAIN_PATH"]?></small><?
		endif;
		?><hr />-->
                <div class="new_restoraunt left<?if($key%3==2):?> end<?endif?>">
                    <?if ($arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"]):?>
                    <div class="image" style="position:relative;">
                        <a href="<?=$arItem["URL"]?>"><img src="<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]?>" width="232" height="153" /></a><br />
                        <?if ($arItem["ELEMENT"]["PROPERTIES"]["d_tours"]["VALUE"][0]):?>
                        <div style="position:absolute; right:10px; bottom:10px;"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour2.png" width="70"/></div>
                        <?endif;?>
                    </div>
                    <?endif;?>
                    <div class="title">
                        <a href="<?=$arItem["URL"]?>">                            
                            <?=$arItem["ELEMENT"]["NAME"]?>
                            <?if ($_REQUEST["search_in"]!="dostavka"):?>
                                <?if (substr_count($arItem["URL"],"restaurants")):?>
                                    [<?=GetMessage("R_RESTORAN")?>]
                                    <?$res = 1;?>
                                <?elseif(substr_count($arItem["URL"],"banket")):?>
                                    [<?=GetMessage("R_BANKET")?>]
                                    <?$res = 2;?>
                                <?elseif(substr_count($arItem["URL"],"dostavka")):?>
                                    [<?=GetMessage("R_DOSTAVKA")?>]
                                    <?$res = 3;?>
                                <?endif;?>
                            <?endif;?>
                        </a>
                    </div>                    
                    <div class="rating">
                        <?for($i = 1; $i <= 5; $i++):?>
                            <div class="small_star<?if($i <= round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                        <?endfor?>
                        <div class="clear"></div>
                    </div>
                    <?//v_dump($arItem["ELEMENT"]["DISPLAY_PROPERTIES"])?>
                    <div style="margin-top:5px;">            
                        <?if ($res==1||$res==2):?>
                            <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]):?>
                                <p class="metro_<?=CITY_ID?>"> 
                                <?
                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))
                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]);
                                    //echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]));
                                else
                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
                                ?>
                                </p>
                            <?endif;?>
                            <?if ($arItem["ELEMENT"]["PROPERTIES"]["address"]):?>
                                <p><b><?=GetMessage("FAV_ADRES")?></b>: 
                                <?
                                if ($arItem["ELEMENT"]["ID"]==387287):
                                    echo implode("<br />",$arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]);
                                else:
                                    if (is_array($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]))
                                        echo strip_tags($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"][0]);
                                        //echo strip_tags(implode(", ",$arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]));
                                    else
                                        echo strip_tags($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]);
                                endif;
                                ?>
                                </p>
                            <?endif;?>
                            <?if ($arItem["ELEMENT"]["PROPERTIES"]["sleeping_rest"]["VALUE"]=="Да"&&$arItem["ELEMENT"]["PROPERTIES"]["phone"]["VALUE"]&&$USER->IsAdmin()):?>
                                <p><b><?=GetMessage("FAV_PHONE")?></b>: 
                                    <?if (CITY_ID=="spb"):?>
                                        (812) 740-18-20
                                    <?else:?>
                                        (495) 988-26-56
                                    <?endif;?>   
                                </p>
                            <?endif;?>
                             <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["type"]):?>
                                <p><b><?=GetMessage("FAV_TYPE")?></b>: 
                                <?
                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]))
                                    echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]));
                                else
                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]);
                                ?>
                                </p>
                            <?endif;?>
                            <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]):?>
                                <p><b><?=GetMessage("FAV_KITCHEN")?></b>: 
                                <?
                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))
                                    echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]));
                                else
                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]);
                                ?>
                                </p>
                            <?endif;?>                                                    
                            <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]):?>
                                <p><b><?=GetMessage("FAV_BILL")?></b>:
                                <?
                                if (CITY_ID=="tmn"):
                                    if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))
                                        $av = strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]));
                                    else
                                        $av = strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]);
                                    echo str_replace("до 1000р","500-1000р",$av);
                                else:
                                    if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))
                                        echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]));
                                    else
                                        echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]);
                                endif;
                                ?>                               
                                </p>
                            <?endif;?> 
                        <?else:?>
                                <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]):?>
                                <p class="metro_<?=CITY_ID?>"> 
                                <?
                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))
                                    echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]));
                                else
                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
                                ?>
                                </p>
                            <?endif;?>
                            <?if ($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]):?>
                                <p><b><?=GetMessage("FAV_ADRES")?></b>: 
                                <?
                                if ($arItem["ELEMENT"]["ID"]==387287):
                                    echo implode("<br />",$arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]);
                                else:
                                    if (is_array($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]))
                                        echo strip_tags(implode(", ",$arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]));
                                    else
                                        echo strip_tags($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]);
                                endif;
                                ?>
                                </p>
                            <?endif;?>
                                <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kuhnyadostavki"]["DISPLAY_VALUE"]):?>
                                <p><b><?=GetMessage("FAV_KITCHEN")?></b>: 
                                <?
                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kuhnyadostavki"]["DISPLAY_VALUE"]))
                                    echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kuhnyadostavki"]["DISPLAY_VALUE"]));
                                else
                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kuhnyadostavki"]["DISPLAY_VALUE"]);
                                ?>
                                </p>
                            <?endif;?> 
                        <?endif;?>
                        <?if(CSite::InGroup( array(14,15,1))):?>                        
                            <?/*<div id="edit_link" ><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arItem["ELEMENT"]["ID"]?>">Редактировать</a></div>*/?>
                            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ELEMENT"]["ID"]?>">Редактировать</a></div>
                        <?endif;?>
                    </div>
                    <div class="clear"></div>
                </div>
                <?if($key%3==2):?><div class="clear"></div><?endif?>
	<?endforeach;?>
        <div class="clear"></div>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&& count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]):?>
            <div class="clear"></div>
            <div align="right"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>
        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div align="right"><a href="<?=$APPLICATION->GetCurPageParam("search_in=rest", array("search_in","x","y")); ?>" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?></a></div>
        <?endif;?>
        <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                
</div>
