<?
CModule::IncludeModule("iblock");
global $search;
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $search = 1;
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            foreach ($el["PROPERTIES"] as $key=>$prop)
            {
                //if (in_array($prop["CODE"], Array("type","kitchen","average_bill","kuhnya_dostavki")))
                if (in_array($prop["CODE"], Array("kitchen","average_bill","subway","sleeping_rest","phone")))
                    $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            }
            $arItem["ELEMENT"] = $el;
            if ($el["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true);
            elseif ($el["DETAIL_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true);                                    
             
            if (!$arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"];
            $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["PREVIEW_TEXT"]), 200);            
         }
    endforeach;?>
<?endif;?>
