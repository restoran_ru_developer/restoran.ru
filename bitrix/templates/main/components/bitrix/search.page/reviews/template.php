<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<script>
    function change_big_modal_next()
    {
            $(".big_modal img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().next().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().next().find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().next().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("id"));                    
                }
                $(".big_modal img").attr("src",img.src);
                showOverflow();
                //$(".big_modal img").css({"width":"0px","height":"0px"});
                $(".big_modal img").animate({"width":+img.width,"height":+img.height},100,function(){
                    $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":"="+eval($("window").height()/2-img.height/2),"left":"="+eval(($("window").width()/2)-(img.width/2))},300);
                });                                            
                $(".big_modal img").fadeIn(300); 

            });                                            

    }
    function change_big_modal_prev()
    {
            $(".big_modal img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().prev().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().prev().find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().prev().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("id"));                    
                }
                $(".big_modal img").attr("src",img.src);
                showOverflow();
                //$(".big_modal img").css({"width":"0px","height":"0px"});
                $(".big_modal img").animate({"width":+img.width,"height":+img.height},100,function(){
                    $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":"="+eval($("window").height()/2-img.height/2),"left":"="+eval(($("window").width()/2)-(img.width/2))},300);
                });                                            
                $(".big_modal img").fadeIn(300); 

            });                                            

    }
    $(document).ready(function(){
        //$("#photogalery").galery({});
        $("body").append("<div class=big_modal><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        setCenter($(".big_modal"));
        $(".slider_left").on("click",function(){
            change_big_modal_prev();
        });
        $(".slider_right").on("click",function(){
            change_big_modal_next();
        });
        $(".big_modal").hover(function(){
            $(".big_modal .slider_left").fadeIn(300);
            $(".big_modal .slider_right").fadeIn(300);
        },function(){
            $(".big_modal .slider_left").fadeOut(300);
            $(".big_modal .slider_right").fadeOut(300);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_modal img").attr("src",img.src);
            $(".big_modal img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_modal img").css({"width":"0px","height":"0px"});
            $(".big_modal img").animate({"width":+img.width,"height":+img.height},300);
            $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":"-="+eval(img.height/2),"left":"-="+eval(img.width/2)},300);
                $(".big_modal").fadeIn(300);
        });
        $("#system_overflow").click(function(){
            $(".big_modal").fadeOut();
            hideOverflow();
            $(".big_modal").css("width","0px");
            $(".big_modal").css("height","0px");
            setCenter($(".big_modal"));
            $(".big_modal img").css({"width":"0px","height":"0px"});
        });
    });
</script>
<div class="search-page">
<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>
<?if(count($arResult["SEARCH"])>0):?>
        <h1><?=GetMessage("SR_TITLE")?></h1>
        <div id="comments">
	<?foreach($arResult["SEARCH"] as $key=>$arItem):?>
		<div class="left left_block">
                    <div class="left rest_img">
                        <img src="<?=$arItem["ELEMENT"]["RESTORAN"]["SRC"]?>" width="70" />
                    </div>
                    <div class="left rest_name">
                        <?if ($arItem["ELEMENT"]["RESTORAN"]["ACTIVE"]=="Y"):?>
                            <a class="font14" href="<?=$arItem["ELEMENT"]["RESTORAN"]["DETAIL_PAGE_URL"]?>"><?=$arItem["ELEMENT"]["RESTORAN"]["NAME"]?></a>,<br />
                        <?else:?>
                            <?=$arItem["ELEMENT"]["RESTORAN"]["NAME"]?>,<br />
                        <?endif;?>
                        <?=$arItem["ELEMENT"]["RESTORAN"]["IBLOCK_NAME"]?>
                    </div>
                    <div class="clear"></div>
                    <div class="dotted"></div>
                    <div class="left rest_img">
                        <div class="ava">
                            <img src="<?=$arItem["ELEMENT"]["AVATAR"]["src"]?>" alt="<?=$arItem["ELEMENT"]["NAME"]?>" width="64" />
                        </div>
                    </div>
                    <div class="left" style="line-height:24px;">
                        <span class="name"><a class="another" href="/users/id<?=$arItem["ELEMENT"]["CREATED_BY"]?>/"><?=$arItem["ELEMENT"]["NAME"]?></a></span><br />
                        <span class="comment_date"><?=$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_YEAR"]?>
                        <br />
                        <div class="small_rating">
                            <?for($i=1;$i<=$arItem["ELEMENT"]["DETAIL_TEXT"];$i++):?>
                                <div class="small_star_a" alt="<?=$i?>"></div>
                            <?endfor;?>
                            <?for($i;$i<=5;$i++):?>
                                <div class="small_star" alt="<?=$i?>"></div>
                            <?endfor;?>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="right right_block">
                    <p>
                        <span class="preview_text<?=$arItem["ELEMENT"]["ID"]?>">
                            <?=$arItem["ELEMENT"]["PREVIEW_TEXT2"]?>
                            <?if (strlen($arItem["ELEMENT"]["PREVIEW_TEXT2"])!=strlen($arItem["ELEMENT"]["PREVIEW_TEXT"])):?>
                                <a onclick="$('.preview_text<?=$arItem["ELEMENT"]["ID"]?>').toggle()" class="jsanother">еще</a>
                            <?endif;?>
                        </span>
                        <?if (strlen($arItem["ELEMENT"]["PREVIEW_TEXT2"])!=strlen($arItem["ELEMENT"]["PREVIEW_TEXT"])):?>
                            <span class="preview_text<?=$arItem["ELEMENT"]["ID"]?>" style="display:none;">
                                <?=$arItem["ELEMENT"]["PREVIEW_TEXT"]?> <a onclick="$('.preview_text<?=$arItem["ELEMENT"]["ID"]?>').toggle()" class="jsanother">скрыть</a>
                            </span>
                        <?endif;?>
                    </p>
                    <?if ($arItem["ELEMENT"]["PROPERTIES"]["plus"]["VALUE"]):?>
                        <b>ДОСТОИНСТВА:</b><br />
                        <p><?=$arItem["ELEMENT"]["PROPERTIES"]["plus"]["VALUE"]?></p>
                    <?endif;?>
                    <?if ($arItem["ELEMENT"]["PROPERTIES"]["minus"]["VALUE"]):?>
                        <b>НЕДОСТАТКИ:</b><br />
                        <p><?=$arItem["ELEMENT"]["PROPERTIES"]["minus"]["VALUE"]?></p>
                    <?endif;?>
                    <?if ($arItem["ELEMENT"]["PROPERTIES"]["video"]["VALUE"]):?>
                        <div class="grey" style="margin-bottom:10px;">
                            <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.CFile::GetPath($arItem["ELEMENT"]["PROPERTIES"]["video"]["VALUE"])?>"></a> 
                            <script>
                                flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                    clip: {
                                            autoPlay: false, 
                                            autoBuffering: true
                                    }
                                });
                            </script>
                        </div>
                    <?endif;?>
                    <?if ($arItem["ELEMENT"]["PHOTOS"][0]["SRC"]):?>
                        <div class="grey photog">
                            <?foreach($arItem["ELEMENT"]["PHOTOS"] as $key=>$photo):?>
                                <div class="left <?=($key%3==2)?"end":""?>"><img id="photo<?=$photo["ID"]?>" src="<?=$photo["SRC"]?>" alt="<?=$photo["ORIGINAL_SRC"]?>" width="137" /></div>
                            <?endforeach;?>
                            <div class="clear"></div>
                        </div>                
                    <?endif;?>
                </div>
                <div class="clear"></div>
                <div class="more">
                    <div class="clear"></div>
                    <div class="left"><a class="another" href="<?=$arItem["URL"]?>">Далее</a></div>
                    <div class="left">Комментарии:(<a class="another" href="<?=$arItem["URL"]?>#comments"><?=($arItem["ELEMENT"]["PROPERTIES"]["COMMENTS"]["VALUE"])?$arItem["ELEMENT"]["PROPERTIES"]["COMMENTS"]["VALUE"]:"0"?></a>)</div>
                    <?if ($arItem["ELEMENT"]["RESTORAN"]["ACTIVE"]=="Y"):?>
                        <div class="left"><a href="<?=str_replace("detailed","opinions",$arItem["ELEMENT"]["RESTORAN"]["DETAIL_PAGE_URL"])?>"><?=GetMessage("ALL_REVIEWS_".$arItem["ELEMENT"]["RESTORAN"]["IBLOCK_TYPE_ID"])?></a></div>
                    <?endif;?>
                    <div class="clear"></div>
                </div>
                <?if (end($arResult["ITEMS"])!=$arItem):?>
                    <div class="black_hr"></div>            
                <?endif;?>                
	<?endforeach;?>
        </div>
        <div class="clear"></div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&&count($arResult["SEARCH"])>=$arParams["PAGE_RESULT_COUNT"]):?>
            <div align="right"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>
        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div align="right"><a href="#" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?></a></div>
        <?endif;?>
                    <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                 
</div>
