<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page">
    <?if(count($arResult["SEARCH"])>0):?>
        <h1>
            <?if ($arParams["SECTION_NAME"])
                echo $arParams["SECTION_NAME"];
            else 
                echo GetMessage("SR_TITLE")?>
        </h1>
        <?foreach($arResult["SEARCH"] as $key=>$arItem):?>
                <!--<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
                <p><?echo $arItem["BODY_FORMATED"]?></p>
                <small><?=GetMessage("SEARCH_MODIFIED")?> <?=$arItem["DATE_CHANGE"]?></small><br /><?
                if($arItem["CHAIN_PATH"]):?>
                        <small><?=GetMessage("SEARCH_PATH")?>&nbsp;<?=$arItem["CHAIN_PATH"]?></small><?
                endif;
                ?><hr />-->
                <div class="new_restoraunt left<?if($key%3==2):?> end<?endif?>">
                    <?if ($arItem["ELEMENT"]["PREVIEW_PICTURE"]):?>
                    <div class="image">
                        <a href="<?=$arItem["ELEMENT"]["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]?>" width="232" height="153" /></a><br />
                    </div>
                    <?endif;?>
                    <div class="title">
                        <a href="<?=$arItem["ELEMENT"]["DETAIL_PAGE_URL"]?>"><?=$arItem["ELEMENT"]["NAME"]?></a>
                    </div>
                    <?if ($arItem["ELEMENT"]["IBLOCK_TYPE_ID"]=="firms"):?>
                        <div class="rating">
                            <?for($i = 1; $i <= 5; $i++):?>
                                <div class="small_star<?if($i <= round($arItem["ELEMENT"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                            <?endfor?>
                            <div class="clear"></div>
                        </div>
                    <?endif;?>
                    <?//v_dump($arItem["ELEMENT"]["DISPLAY_PROPERTIES"])?>
                    <div style="margin-top:5px;">            
                        <?if ($arItem["ELEMENT"]["PREVIEW_TEXT"]):?>
                            <p><?=$arItem["ELEMENT"]["PREVIEW_TEXT"]?></p>
                        <?endif;?>
                    </div>
                    <div class="clear"></div>
                </div>
                <?if($key%3==2):?><div class="clear"></div><?endif?>
        <?endforeach;?>
        <div class="clear"></div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N" && count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]):?>
            <div class="clear"></div>
            <div align="right"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>
        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div align="right"><a href="<?=$APPLICATION->GetCurPageParam("search_in=".$arParams["SEARCH_IN"], array("search_in","x","y")); ?>" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?> <?=$arParams["SECTION_NAME"]?></a></div>
        <?endif;?>
<?endif;?>
</div>