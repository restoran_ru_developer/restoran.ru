<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form action="<?=$arResult["FORM_ACTION"]?>">
    <div class="title_main left"><?=GetMessage("WHAT_YOU_SEARCH_TEXT")?></div>
    <div class="search left">
        <div class="search_block">
            <div class="left">
                <?if($arParams["USE_SUGGEST"] === "Y"):?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:search.suggest.input",
                        "top_search_suggest",
                        array(
                            "NAME" => "q",
                            "VALUE" => GetMessage("BSF_T_SEARCH_EXMP_STRING"),
                            "INPUT_SIZE" => "",
                            "DROPDOWN_SIZE" => 10,
                        ),
                        $component, array("HIDE_ICONS" => "Y")
                    );?>
                <?else:?>
                    <input class="placeholder" type="text" name="q" value="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
                <?endif?>
            </div>
            <div class="right">
                <select id="search_in">
                    <option selected="selected" value="1"><?=GetMessage("BSF_T_WHERE_SEARCH_SITE")?></option>
                    <option><?=GetMessage("BSF_T_WHERE_SEARCH_NEWS")?></option>
                    <option><?=GetMessage("BSF_T_WHERE_SEARCH_RECIPE")?></option>
                </select>
            </div>
            <div class="clear"></div>
        </div>
        <?if ($APPLICATION->GetCurDir()=="/" || substr_count($APPLICATION->GetCurDir(),"/".CITY_ID."/catalog/")):?>
            <div class="filter_box">
                <div class="left filter_block">
                    <div class="title">Кухня</div>
                    <ul class="filter_category_block">
                        <li>Английская</li>
                        <li>Авторская</li>
                        <li>Австралийская</li>
                        <li>Австрийская</li>
                    </ul>
                    <div id="filter1"><a href="javascript:void(0)" class="filter_arrow"></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup").popup({"obj":"filter1","css":{"left":"-10px","top":"2px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi3",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup">
                        <div class="title">Кухня</div>
                        <select multiple="multiple" class="asd" id="multi3" name="kitchen[]" size="10">
                            <option value="2">Авторская</option>
                            <option value="2">Австралийская</option>
                            <option value="2">Австрийская</option>
                            <option value="2">Азербайджанская</option>
                            <option value="2">Армянская</option>
                            <option value="2">Африканская</option>
                            <option value="2">Арабская</option>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" type="submit" value="ВЫБРАТЬ"></div>
                    </div>
                </div>
                <div class="left filter_block">
                    <div class="title">Средний счет</div>
                    <script type="text/javascript">
                        var params = {
                            changedEl: "#multi1",
                            scrollArrows: false
                            }
                        cuSelMulti(params);
                    </script>
                    <select multiple class="abb" name="multi[]" id="multi1">
                        <option>0 — 200 р.</option>
                        <option>200 — 500 р.</option>
                        <option>500 — 1000 р.</option>
                        <option>более 1000 р.</option>
                    </select>
                </div>
                <div class="left filter_block">
                    <div class="title">Метро</div>
                    <ul class="filter_category_block">
                        <li>Пл.А.Невского</li>
                        <li>Маяковская</li>
                        <li>Пл. Восстания</li>
                        <li>Новочеркасская</li>
                    </ul>               
                    <div class="filter_arrow"></div>
                </div>
                <div class="left filter_block">
                    <div class="title">Тип</div>
                    <script type="text/javascript">
                        var params = {
                            changedEl: "#multi2",
                            scrollArrows: false
                            }
                        cuSelMulti(params);
                    </script>
                    <select multiple class="absb" name="multi" id="multi2">
                        <option>Бар</option>
                        <option>Бизнес ланч</option>
                        <option>Террасса</option>
                        <option>Кафе</option>
                    </select>
                </div>
            </div>        
        <?endif;?>
    </div>    
    <div class="button right">
        <input class="filter_button" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
        <?if ($APPLICATION->GetCurDir()=="/" || substr_count($APPLICATION->GetCurDir(),"/content/".CITY_ID."/catalog/")):?>
            <?$APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/top_search_links.php"),
                Array(),
                Array("MODE"=>"html")
            );?>
        <?endif;?>
    </div>
    <div class="clear"></div>
</form>