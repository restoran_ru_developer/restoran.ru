<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"USE_SUGGEST" => Array(
		"NAME" => GetMessage("TP_BSF_USE_SUGGEST"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
	),
    "CACHE_TYPE" => Array(
        "NAME" => GetMessage("TP_BSF_CACHE_TYPE"),
        "TYPE" => "LIST",
        "MULTIPLE" => "N",
        "VALUES" => Array(
            "A" => GetMessage("TP_BSF_CACHE_TYPE_A"),
            "Y" => GetMessage("TP_BSF_CACHE_TYPE_Y"),
            "N" => GetMessage("TP_BSF_CACHE_TYPE_N")
        )
    ),
    "CACHE_TIME"  =>  Array(
        "NAME" => GetMessage("TP_BSF_CACHE_TIME"),
        "DEFAULT"=>36000000
    ),
);
?>
