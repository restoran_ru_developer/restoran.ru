<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="wrap-div">
<?
    if($arResult["NEED_AUTH"] == "Y") {
        $APPLICATION->AuthForm("");
    } elseif (strlen($arResult["FatalError"]) > 0) {
        echo $arResult["FatalError"];
    } else {
        if(strlen($arResult["ErrorMessage"]) > 0) {
            echo $arResult["ErrorMessage"];
        }
        ?>
        <table class="mail">
            <?foreach($arResult["Events"] as $event):?>
            <tr class="<?if(!$event["IS_READ"]):?>active <?endif?>solid" id="correspondence_result_<?=$event["ID"]?>">
                <td class="mail-avatar" width="64"><div class="avatar"><a href="/users/id<?=$event["USER_ID"]?>/"><img src="<?=$event["USER_PERSONAL_PHOTO_FILE"]["src"]?>" /></div></a></td>
                <td class="mail-who" width="150">
                    <div>
                        <p class="mail-autor"> <a href="/users/id<?=$event["USER_ID"]?>/"><?=$event["USER_NAME"]?></a>
                            <!--<br/><?=($event["IS_RESTORATOR"] ? GetMessage("USER_RESTORATOR") : "")?> -->
                        </p>
                        <p class="mail-date"><em><?=$event["DATE_CREATE_FORMATED_1"]?></em> <?=$event["DATE_CREATE_FORMATED_2"]?> <?=GetMessage("DATE_FORMAT_V")?> <?=$event["DATE_CREATE_FORMATED_3"]?></p>
                    </div>
                </td>
                <td class="mail-text">
                    <?if($event["TRUNCATED_MESSAGE"]):?>
                        <p>
                            <div id="short_text_<?=$event["ID"]?>">
                                <?=$event["TRUNCATED_MESSAGE"]?>
                            </div>
                            <div id="full_text_<?=$event["ID"]?>" style="display: none;">
                                <?=$event["MESSAGE"]?>
                                <form action="<?=$templateFolder?>/send_msg.php" name="msg_form_<?=$event["ID"]?>" id="msg_form_<?=$event["ID"]?>">
                                    <?=bitrix_sessid_post()?>
                                    <input type="hidden" name="msg_id" value="<?=$event["ID"]?>" />
                                    <input type="hidden" name="second_user_id" value="<?=$event["USER_ID"]?>" />
                                    <input type="hidden" name="user_id" value="<?=$arParams["USER_ID"]?>" />
                                    <p>
                                        <textarea name="message"></textarea>
                                    </p>
                                    <p class="mail-actions">
                                        <a class="gray-link"  href="/users/id<?=$arParams["USER_ID"]?>/im/<?=$event["USER_ID"]?>/"><?=GetMessage("GET_CORRESPONDENCE")?></a>
                                        <input type="button" onclick="sendMsg('<?=$arParams["USER_ID"]?>', '<?=$event["USER_ID"]?>', '<?=$event["ID"]?>')" value="<?=GetMessage("SEND_MSG")?>" class="submit-blue" />
                                    </p>
                                </form>
                            </div>
                        </p>
                        <p class="mail-more">
                            <a href="javascript:void(null)" onclick="showAllMsg('<?=$event["ID"]?>')" id="show_all_link_<?=$event["ID"]?>"><?=GetMessage("SHOW_ALL")?></a>
                        </p>
                    <?else:?>
                        <?=$event["MESSAGE"]?>
                        <form action="<?=$templateFolder?>/send_msg.php" name="msg_form_<?=$event["ID"]?>" id="msg_form_<?=$event["ID"]?>" style="display: none;">
                            <?=bitrix_sessid_post()?>
                            <input type="hidden" name="msg_id" value="<?=$event["ID"]?>" />
                            <input type="hidden" name="second_user_id" value="<?=$event["USER_ID"]?>" />
                            <input type="hidden" name="user_id" value="<?=$arParams["USER_ID"]?>" />
                            <p>
                                <textarea name="message"></textarea>
                            </p>
                            <p class="mail-actions">
                                <a class="gray-link" href="/users/id<?=$arParams["USER_ID"]?>/im/<?=$event["USER_ID"]?>/"><?=GetMessage("GET_CORRESPONDENCE")?></a>
                                <input type="button" onclick="sendMsg('<?=$arParams["USER_ID"]?>', '<?=$event["USER_ID"]?>', '<?=$event["ID"]?>')" value="<?=GetMessage("SEND_MSG")?>" class="submit-blue" />
                            </p>
                        </form>
                        <p class="mail-more">
                            <a href="javascript:void(null)" onclick="answerMsg('<?=$event["ID"]?>')" id="answ_link_<?=$event["ID"]?>"><?=GetMessage("ANSWER")?></a>
                        </p>

                    <?endif?>
                </td>
                <td class="mail-del">
                    <p>
                        <a href="<?=$event["DELETE_LINK"]?>" onclick="if(!confirm('<?=GetMessage("DELETE_CONFIRM")?>')) return false;"><?=GetMessage("DELETE_LINK")?></a>
                    </p>
                </td>
            </tr>
            <?endforeach?>
        </table>
        <? } ?>
</div>