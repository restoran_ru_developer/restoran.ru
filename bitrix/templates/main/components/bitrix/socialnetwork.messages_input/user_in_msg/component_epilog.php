<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// mark all message as read
foreach($arResult["Events"] as $key=>$event) {
    if(!$event["IS_READ"])
        CSocNetMessages::MarkMessageRead($arParams["USER_ID"], $event["ID"]);
}
?>