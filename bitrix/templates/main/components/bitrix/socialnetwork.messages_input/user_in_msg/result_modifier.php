<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("iblock"))
    return false;

foreach($arResult["Events"] as $key=>$event) {
    // check user restorator stat
    $arGroups = CUser::GetUserGroup($event["USER_ID"]);
    if(in_array(RESTORATOR_GROUP, $arGroups))
        $arResult["Events"][$key]["IS_RESTORATOR"] = true;

    // resize photo
    $arResult["Events"][$key]["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($event["USER_PERSONAL_PHOTO_FILE"], array('width'=>64, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true);

    // format date
    $arResult["Events"][$key]["DATE_CREATE_FORMATED"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($event["DATE_CREATE"], CSite::GetDateFormat()));
    $tmpDate = explode(' ', $arResult["Events"][$key]["DATE_CREATE_FORMATED"]);
    $arResult["Events"][$key]["DATE_CREATE_FORMATED_1"] = $tmpDate[0];
    $arResult["Events"][$key]["DATE_CREATE_FORMATED_2"] = strtolower($tmpDate[1])." ".$tmpDate[2];
    $arResult["Events"][$key]["DATE_CREATE_FORMATED_3"] = CIBlockFormatProperties::DateFormat("G:i", MakeTimeStamp($event["DATE_CREATE"], CSite::GetDateFormat()));

    // truncate text
    if(strlen($event["MESSAGE"]) > $arParams["MESSAGE_LENGTH"]) {
        $obParser = new CTextParser;
        $arResult["Events"][$key]["TRUNCATED_MESSAGE"] = $obParser->html_cut($event["MESSAGE"], $arParams["MESSAGE_LENGTH"]);
    }
}
?>