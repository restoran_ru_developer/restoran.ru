<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="plans">    
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="plan-<?=($key+1)?> " align="center">        
            <p class="plan-desc"><?=$arItem['DISPLAY_PROPERTIES']['SIZE']['DISPLAY_VALUE']?><br/><i><?=$arItem['DISPLAY_PROPERTIES']['POSITION']['DISPLAY_VALUE']?></i></p>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="180" style="margin-right:0px"><br />
            <p style="margin-bottom:0px">Анимация: <span class="plan-desc"><?=$arItem['DISPLAY_PROPERTIES']['PRICE_ANIMATION']['DISPLAY_VALUE']?></span> <span class="rouble">e</span></p>
            <i><?=$arItem['DISPLAY_PROPERTIES']['PRICE_ANIMATION']['DESCRIPTION']?></i>            
        </div>    
    <?endforeach;?>
</div>
