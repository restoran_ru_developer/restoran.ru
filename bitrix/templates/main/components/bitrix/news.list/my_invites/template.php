<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])>0){?>
<table class="profile-activity w100" cellpadding="0" cellspacing="0">
	<tr>
		<th>Приглашение</th>
		<th>Мои гости</th>
		<th>Статус</th>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>		
	<tr <?=(end($arResult["ITEMS"])==$arItem)?"class='nobg'":""?> id="inv<?=$arItem["ID"]?>">
            
		<td valign="top" class="first w-new" width="240">
                    <div class="left">
                        <img src="<?=$arItem["RESTORAN"]["PICTURE"]?>" width="70" />                        
                    </div>
                    <div class="left">
                        <a class="name" href="<?=$arItem["RESTORAN"]["URL"]?>"><?=$arItem["RESTORAN"]["NAME"]?></a><br />
                        <div class="rating">
                            <?for($i = 1; $i <= 5; $i++):?>
                                <div class="small_star<?if($i <= round($arItem["RESTORAN"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                            <?endfor?>
                            <div class="clear"></div>
                        </div>
                        <?if ($arItem["RESTORAN"]["SECTION"]=="banket"):?>
                            <p class="upcase">банкетный зал</p>
                        <?else:?>
                            <p class="upcase">ресторан</p>
                        <?endif;?>                        
                    </div>
                    <div class="clear"></div>      
                    <br />
                    <?=$arItem["DISPLAY_ACTIVE_FROM"]?> в <?=$arItem["TIME"]?><Br /><Br />
                    <?if (!$arItem["PROPERTIES"]["BRON"]["VALUE"]):?>
                        <a class="icon-continue" href="javascript:void(0)" onclick="add_invite(<?=$arItem["ID"]?>)">Редактировать</a>
                        <a class="icon-delete" href="javascript:void(0)" onclick="delete_invite(<?=$arItem["ID"]?>)">Отменить</a> 
                    <?endif;?>
                </td>		   
		<td class="user-friends nofloat" colspan="2" valign="top">
                    <ul>
                        <?$us_co = 0;?>
                        <?foreach($arItem["USERS"] as $respUserReg):?>
                            <li <?=(end($arItem["USERS"])==$respUserReg&&!$arItem["NA_USERS"])?"class='nobg'":""?>>
                                <div class="avatar left">
                                    <img src="<?=$respUserReg["PHOTO"]["src"]?>" width="64" />
                                </div>
                                <div class="user-friend left">
                                        <a class="name" href="/users/id<?=$respUserReg["ID"]?>/"><?=$respUserReg["NAME"]?></a><br/>
                                        <?=$respUserReg["PERSONAL_CITY"]?><br />
                                        <p class="user-mail"><a href="javascript:void(0)" onclick="send_message(<?=$respUserReg["ID"]?>,'<?=$respUserReg["NAME"]?>')">Сообщение</a></p>
                                </div>
                                <div class="left" style="padding-top:6px;">
                                    <?if ($respUserReg["ACCEPT"]=="Y"):?>
                                        <i>Подтвердил</i>
                                        <?$us_co++;?>
                                    <?elseif ($respUserReg["ACCEPT"]=="N"):?>
                                        <i>Отклонено</i>
                                    <?else:?>
                                        <i>В ожидании</i>
                                    <?endif;?>                                    
                                </div>
                                <!--<div class="right" style="padding-top:6px;">
                                    <a class="icon-delete_f2" href="#" alt="Отклонить приглашение для данного пользователя" title="Отклонить приглашение для данного пользователя"></a> 
                                </div>-->
                                <div class="clear"></div>
                            </li>
                        <?endforeach?>
                            <?$us_na = 0;?>
                        <?foreach($arItem["NA_USERS"] as $respUserUnreg):?>
                            <li class="<?=(end($arItem["NA_USERS"])==$respUserUnreg)?"nopic nobg":""?>">
                                <div class="user-friend left" style="width:295px">
                                        <p><?=$respUserUnreg["NAME"]?></p>
                                </div>
                                <div class="left" style="padding-top:6px;">
                                    <?if ($respUserUnreg["ACCEPT"]=="Y"):?>
                                        <i>Подтвердил</i>
                                         <?$us_na++;?>
                                    <?else:?>
                                        <i>В ожидании</i>
                                    <?endif;?>
                                </div>
                                <div class="clear"></div>
                            </li>
                        <?endforeach?>
                        <?if (count($arItem["USERS"])==$us_co&&count($arItem["NA_USERS"])==$us_na&&!$arItem["PROPERTIES"]["BRON"]["VALUE"]):?>
                            <script>
                            $(document).ready(function(){
                                $(".add_bron<?=$arItem["ID"]?>").click(function(){
                                    $.ajax({
                                        type: "POST",
                                        url: "/tpl/ajax/online_order_rest.php",
                                        data: "what=0&invite=<?=$arItem["ID"]?>&date=<?=$arItem["DATE"]?>&name=<?=rawurlencode($arItem["RESTORAN"]["NAME"])?>&id=<?=$arItem["RESTORAN"]["ID"]?>&time=<?=$arItem["TIME"]?>&person=<?=($us_co+$us_na+1)?>&<?=bitrix_sessid_get()?>",
                                        success: function(data) {
                                            if (!$("#mail_modal").size())
                                            {
                                                $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                                            }
                                            $('#bron_modal').html(data);
                                            showOverflow();
                                            setCenter($("#bron_modal"));
                                            $("#bron_modal").fadeIn("300"); 
                                        }
                                    });
                                });
                            });
                            </script>
                            <input type="button" class="add_bron<?=$arItem["ID"]?> light_button" value="оформить бронирование" />                                
                        <?endif;?>
                        <?if (count($arItem["USERS"])!=$us_co||count($arItem["NA_USERS"])!=$us_na):
                            $fff = count($arItem["USERS"])-us_co+count($arItem["NA_USERS"])-$us_na;
                            ?>
                            <input  type="button" class="light_button" onclick=' alert("Внимание! <?=$fff?> пользователей не подтвердили приглашение!");' style="opacity:0.5" value="оформить бронирование" />                                
                        <?endif;?>
                        <?if ($arItem["PROPERTIES"]["BRON"]["VALUE"]):?>
                            <i>Следить за состоянием заказа Вы можете в разделе «Бронирования».</i>
                        <?endif;?>
                        <!--<p class="more-activity" style="margin-top:12px; margin-bottom:0;"><a href="#">Еще 3</a></p>-->
                    </ul>
                </td>
        </tr>	
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</table>
<?}else{?>
	Нет добавленных приглашений
<?}?>
