<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult["ITEMS"])>0):?>
        <table id="services_table" cellpadding="10" cellspacing="0" width="728">
            <?foreach($arResult["ITEMS"] as $cell=>$item):?>
            <tr class="<?=($cell%2==0)?"odd":""?>">
                <td width="120">
                    <?if ($item["PREVIEW_PICTURE"]["SRC"]):?>
                        <img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>" height="70" />
                    <?endif;?>
                </td>
                <td width="100"><?=$item["NAME"]?></td>
                <td><?=$item["PREVIEW_TEXT"]?></td>            
                <td align="center"><?=$item["PROPERTIES"]["price"]["VALUE"]?></td>
            </tr>
            <?endforeach?>
        </table>
<?else:?>
        <table class="services_table" cellpadding="0" cellspacing="0" width="730">
            <tr>
                <td class="font14"><?=GetMessage("NO_POSTER")?></td>
            </tr>
        </table>
<? endif; ?>
    <div class="right">
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>