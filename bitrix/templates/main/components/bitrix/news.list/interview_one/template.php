<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <img width="232" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />
        <div class="interview_name">
            <h3><?=$arItem["NAME"]?></h3>
        </div>
        <div class="interview_text">
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
        </div>
        <div class="left">Комментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)</div>
        <div class="right"><a class="another" href="#"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
<?endforeach;?>