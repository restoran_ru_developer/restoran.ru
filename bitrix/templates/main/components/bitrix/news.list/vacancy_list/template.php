<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 
$arres=CIBlockSection::GetById($arParams["PARENT_SECTION"]);
if($res=$arres->GetNext()){
    $sectionName = $res["NAME"];
    }
?>

<h1 class="content-h"><? if ($sectionName){ echo $sectionName;} else { echo "Вакансии";}?></h1>
<div class="content-vacancies">
    <?if (count($arResult["ITEMS"])>0):?>
            <div class="content-vrb-row">
                <? foreach ($arResult["ITEMS"] as $cell => $arItem):
                    //v_dump($arItem);
                    ?>
                    <div class="content-vrb-block <? if ($cell % 3 == 0): ?>vb-first<? endif ?> vb">
                        <div class="content-vrb-content">
                            <div class="content-vrb-content-title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></div>

                            <br class="block-param-break">
                            <div class="block-param">
                                <span class="block-param-title">Зарплата:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["WAGES_OF"]["VALUE"] ?> &mdash; <?= $arItem["PROPERTIES"]["WAGES_TO"]["VALUE"] ?> <span class="rouble font12">e</span></span>
                            </div> 
                            <div class="block-param">
                                <span class="block-param-title">Опыт работы:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></span>
                            </div>
                            <div class="block-param">
                                <span class="block-param-title">Компания:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["COMPANY_NAME"]["VALUE"] ?></span>
                            </div>
                            <div class="block-param">
                                <span class="block-param-title">Возраст:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["AGE_FROM"]["VALUE"] ?> - <?= $arItem["PROPERTIES"]["AGE_TO"]["VALUE"] ?></span>
                            </div>
                            <div class="block-param">
                                <span class="block-param-title">Образование:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["EDUCATION"]["VALUE"] ?></span>
                            </div>
                            <?if ($arItem["PROPERTIES"]["SHEDULE"]["VALUE"]):?>
                            <div class="block-param">
                                <span class="block-param-title">График:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["SHEDULE"]["VALUE"]?></span>
                            </div>
                            <?endif;?>
                            <div class="metro_msk">
            <?= implode(", ", $arItem["PROPERTIES"]["SUBWAY_BIND"]["DISPLAY_VALUE"]) ?>
                            </div>
                        </div>

                        <div class="content-vrb-links">
                            <div class="block-info-comments">
                                Комментарии: (<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments"><?= intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"]) ?></a>)
                            </div>
                            <div class="block-info-next">
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">Далее</a>
                            </div>
                        </div>
                    </div>
                    <? $cell++;
                    if ($cell % 3 == 0 && $arResult["ITEMS"][$cell]):
                        ?>
                        <hr class="content-row-separator">
                    <? endif ?>
                <? endforeach; ?>
                <? if ($cell % 3 != 0): ?>
                    <? while (($cell++) % 3 != 0): ?>
                        <div class="content-vrb-block vb"></div>
                    <? endwhile; ?>
        <? endif ?>
        <? if ($arParams["BLOCK_HEADER_POPULAR"] == "Y"): ?>
                    <div class="content-vrb-show-all">
                        <span><a href="/<?= CITY_ID ?>/joblist/<?= $arParams["IBLOCK_TYPE"] ?>/">Все вакансии</a></span>
                    </div>
        <? endif ?>
    </div>
    <?else:?>
        <p class="another_color"><?=GetMessage("NOTHING_FOUND")?></p>
    <?endif;?>
</div>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <div class="right"><?= $arResult["NAV_STRING"] ?></div>
<? endif; ?>