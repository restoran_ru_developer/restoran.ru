<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$todayDate = date("d.m.Y");
?>
<script>
    $(function()
    {
            $('.poster_pane').jScrollPane({verticalGutter:15, autoReinitialise: true});
    });
</script>
<div class="poster_pane">
    <table class="poster" cellpadding="5" cellspacing="0">
        <?foreach($arResult["AFISHA_ITEMS"] as $today):?>
            <?foreach ($today as $todayEvent):?>
                <tr>
                    <td class="time" width="80"><?=$todayEvent["PROPERTY_EVENT_DATE_VALUE"]?></td>
                    <td class="time"  width="100">
                        <div style="overflow:hidden; height:64px">
                            <img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" width="86" />
                        </div>
                    </td>
                    <td class="time"  width="80"><?=$todayEvent["PROPERTY_TIME_VALUE"]?></td>
                    <td class="name"><a id="poster_name" href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>
                    <td class="type"><?=strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?></td>
                    <!--<td width="220"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a><br /><?=$todayEvent["PROPERTIES"]["ADRES"]["VALUE"]?></td>-->
                </tr>
            <?endforeach;?>
        <?endforeach?>
    </table>
</div>
<br />
<?global $arrFilter;?>
<div align="right"><a class="uppercase" href="/<?=CITY_ID?>/afisha/?restoran=<?=$arrFilter["PROPERTY_RESTORAN"]?>">ВСЯ АФИША ресторана</a></div>