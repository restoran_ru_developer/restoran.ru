<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])>0):?>
<div id="news">        
    <?/*if ((CITY_ID=='spb'||CITY_ID=="msk")&&SITE_ID=="s1"):?>
        <a id="ng" href="/<?=CITY_ID?>/articles/new_year_corp/" alt="Новогодние корпоративы"></a>    
        <a id="ng_night" href="/<?=CITY_ID?>/articles/new_year_night/" alt="Новогодняя ночь"></a>    
    <?endif;?>    
    <?if ((CITY_ID=='tmn')&&SITE_ID=="s1"):?>
        <a id="ng" href="/<?=CITY_ID?>/articles/new_year_corp/" alt="Новогодние корпоративы"></a>    
    <?endif;*/?>
    <?/*if ((CITY_ID=='msk')&&SITE_ID=="s1"):?>
        <a id="pir" style="background:none;position: absolute; right:252px; top:6px" href="/activities/pir/" alt="Русский пир"><img src="/tpl/images/pir_icon_new.jpg" /></a>    
    <?endif;*/?>
    <div class="left left_block">        
            <ul class="tabs">
                <?foreach($arResult["ITEMS"] as $key=>$arItem):
                    //plate_scrollable holiday_scrollable news_scrollable
                    ?>
                <li>
                    <a class="news_scr" scrCont="<?=$arItem["CODE"]?>_scrollable" href="#">
                        <div class="left tab_left"></div>
                        <?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
                            <div class="icon_pic_box left name">
                                <?=$arItem["NAME"]?>
                                <div class="icon_pic">
                                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="70" />
                                </div>
                            </div>                            
                        <?else:?>
                            <div class="left name"><?=$arItem["NAME"]?></div>
                        <?endif;?>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <?endforeach;?>
            </ul>
            <!-- tab "panes" -->
            <div class="panes">
                <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                    <div class="pane newsItem" style="padding-bottom:0px;">
                        <?
                        $filter = "arrNoFilter_".$arItem["CODE"];
                        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
                        {    
                                global $$filter;
                                $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                        }
                        ?>
                        <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "news_main",
                            Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]=="blogs")?"created_date":"ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => ($$filter)?$filter:"",
                                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                                    "PROPERTY_CODE" => array(),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "500",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "3600000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arItem["PROPERTIES"]["LINK"]["VALUE"]
                            ),
                        false
                        );?>
                    </div>
                <?endforeach;?>
            </div>
    </div>
    <div class="right" id="news_baner_right_box">
        <script>
            $(document).ready(function(){
                $("#news_baner_right_box").html($("#news_main_banner_right").find("script").remove().end().html());
                //$("#news_main_banner_right").remove();
            });
        </script>
        <?/*$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_index_1",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );*/?>
    </div>
    <div class="clear"></div>
</div>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="rest_news_cont" id="<?=$arItem["CODE"]?>_scrollable" <?=($key>0)?"style='display: none;'":""?>>
        <?
        $filter = "arrNoFilter_".$arItem["CODE"];
        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
        {    
                global $$filter;
                $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
        }      
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            ($key!=0)?'plate_scrollable':"main_news_scrollable",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                    "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"20",
                    "SORT_BY1" => ($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]=="blogs")?"created_date":"ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array(),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "100",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );?>
    </div>
<?endforeach;?>
<?endif;?>