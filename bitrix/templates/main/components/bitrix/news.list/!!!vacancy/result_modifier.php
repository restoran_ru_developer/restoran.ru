<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $keyItem => $arItem) {
    // set age to site format
    foreach($arResult["ITEMS"][$keyItem]["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        if($pid == "AGE_FROM") {
            $arResult["ITEMS"][$keyItem]["DISPLAY_PROPERTIES"][$pid]["NAME"] = "Возраст";
            $arResult["ITEMS"][$keyItem]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = "от ".$arResult["ITEMS"][$keyItem]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]." до ".$arResult["ITEMS"][$keyItem]["DISPLAY_PROPERTIES"]["AGE_TO"]["DISPLAY_VALUE"];
            unset($arResult["ITEMS"][$keyItem]["DISPLAY_PROPERTIES"]["AGE_TO"]["DISPLAY_VALUE"]);
        }
    }

    // get section name
    $rsSection = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
    if($arSection = $rsSection->Fetch())
        $arResult["ITEMS"][$keyItem]["SECTION_NAME"] = $arSection["NAME"];

    // set iblock type name
    $arResult["ITEMS"][$keyItem]["IBLOCK_TYPE_NAME"] = ($arItem["IBLOCK_TYPE_ID"] == "resume" ? "Резюме" : "Вакансия");
}
?>