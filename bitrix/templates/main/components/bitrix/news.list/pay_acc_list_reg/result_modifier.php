<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("catalog"))
    return;
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $res = GetCatalogProductPrice($arItem["ID"], 1);
    $arResult["ITEMS"][$key]["PRICE_FORMATED"] = FormatCurrency($res["PRICE"], $res["CURRENCY"]);
    $arResult["ITEMS"][$key]["PRICE"] = round($res["PRICE"]);
}
?>