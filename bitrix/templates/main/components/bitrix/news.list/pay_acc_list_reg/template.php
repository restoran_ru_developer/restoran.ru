<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="plans">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="plan-<?=($key+1)?> <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>" align="center">
            <p class="upcase-premium"><?=$arItem["NAME"]?></p>
            <p class="plan-desc"><?=$arItem["PRICE"]?> <span class="rouble">e</span>/<?=GetMessage("PERIOD")?></p>
            <?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
                <p class="plan-desc" align="center"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="180" height="145" style="margin-right:0px" /></p>
            <?endif;?>
            <?=$arItem["PREVIEW_TEXT"]?>
        </div>
    <?endforeach;?>
    <div class="clear"></div>
</div>
<div class="clear"></div>