<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("catalog"))
    return;

foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arPrice = CPrice::GetBasePrice($arItem["ID"]);

    $arResult["ITEMS"][$key]["PRICE"] = $arPrice["PRICE"];
    $arResult["ITEMS"][$key]["PRICE_FORMATED"] = FormatCurrency($arPrice["PRICE"], $arPrice["CURRENCY"]);
}
?>