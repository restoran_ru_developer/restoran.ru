<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<link href="<?=$templateFolder?>/style.css" rel="stylesheet" media="all" />
<script src="<?=$templateFolder?>/script.js"></script>
<table cellpadding="5" class="typer" width="100%">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <tr <?=(end($arResult["ITEMS"])==$arItem)?"class='end'":""?>>
            <td width="200">
                <p class="upcase-premium" align="center"><?=$arItem["NAME"]?></p>            
                <p class="plan-desc"><b><?=$arItem["PRICE_FORMATED"]?></b> <span class="rouble">e</span>/<?=GetMessage("PERIOD")?></p>                
                <?if ($arParams["PRIORITY"]=="Y"):?>
                    <p align="center"><input type="button" class="light_button" value="Заказать" onclick="order_priority(<?=(int)$_REQUEST["ID"]?>,<?=(int)$arItem["ID"]?>)" /></p>
                <?else:?>
                    <p align="center"><input type="button" class="light_button" value="Заказать" onclick="order_type(<?=(int)$_REQUEST["ID"]?>,<?=(int)$arItem["ID"]?>)" /></p>                
                <?endif;?>                
            </td>
            <!--<form action="order.php" method="get">
                <input type="submit" class="light_button buy" value="<?=GetMessage("PLACE")?>" /><br /><br />
                <input type="hidden" name="ID" value="<?=$arItem["ID"]?>" />
                <input type="hidden" name="resto_bind" class="REST_ID" value="" />                
            </form>-->
            <!--<td>
                <p class="plan-desc"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="180" height="145" /></p>
            </td>-->
            <td>
                <?=$arItem["PREVIEW_TEXT"]?>                
            </td>
        </tr>
    <?endforeach;?>
</table>