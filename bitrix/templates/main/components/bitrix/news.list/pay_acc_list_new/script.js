$(document).ready(function() {
    // check rest id
    $('.buy').on('click', function() {
        if($('.REST_ID').val().length <= 0) {
            alert("Выберите ресторан!");
            return false;
        }
    });
});

function order_type(id,type)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/type_order.php",
        data: "id="+id+"&type="+type,
        success: function(data) {
            if (!$("#cbron_form").size())
            {
                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
            }
            $('#cbron_form').html(data);
            showOverflow();
            setCenter($("#cbron_form"));
            $("#cbron_form").css("display","block"); 
        }
    });
}

function order_priority(id,priority)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/priority_order.php",
        data: "id="+id+"&priority="+priority,
        success: function(data) {
            if (!$("#cbron_form").size())
            {
                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
            }
            $('#cbron_form').html(data);
            showOverflow();
            setCenter($("#cbron_form"));
            $("#cbron_form").css("display","block"); 
        }
    });
}