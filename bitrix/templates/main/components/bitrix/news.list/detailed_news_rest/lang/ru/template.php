<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "комментарий";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "комментария";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "комментариев";
$MESS["CT_BNL_ELEMENT_SEE_MORE"] = "Далее";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWSnews"] = "ВСЕ НОВОСТИ РЕСТОРАНА";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWSblogs"] = "ВСЕ БЛОГИ РЕСТОРАНА";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWSoverviews"] = "ВСЕ ОБЗОРЫ РЕСТОРАНА";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWSphotoreports"] = "ВСЕ ФОТООТЧЕТЫ РЕСТОРАНА";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWSmasterclasses"] = "ВСЕ РЕЦЕПТЫ ОТ ШЕФА РЕСТОРАНА";
?>