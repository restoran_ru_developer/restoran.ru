<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="rest_news left <?if($key == 1):?>end<?endif?>">
        <div class="left">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" align="left"/></a>
        </div>
        <div class="right" style="width:235px">          
            <div class="data"><span class="dates"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <span><?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></span></div>
            <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=HTMLToTxt($arItem["~NAME"])?></a></div>
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <div class="left">
                <i><?=intval($arItem["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"])?> <?=pluralForm(intval($arItem["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
            </div>
            <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_MORE")?></a></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
<?endforeach;?>
<div class="clear"></div>
<br />
<?if ($arParams["IBLOCK_TYPE"]=="cookery")
    $arParams["IBLOCK_TYPE"] = "masterclasses";?>
<?if ($arParams["WITHOUT_LINK"]!="Y"):?>
<div class="right"><a href="<?=$arResult["LINK"]?><?=$arParams["IBLOCK_TYPE"]?>/" class="uppercase"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS".$arParams["IBLOCK_TYPE"])?></a></div>
<?endif;?>
