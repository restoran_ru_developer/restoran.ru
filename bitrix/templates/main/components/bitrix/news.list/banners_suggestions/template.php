<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<a name="buy_place"></a>




	<div class="bg-gray">
		<div class="wrap-div">
			<div class="plans">

			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="plan-1 plan" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="plan-desc"><?=$arItem['DISPLAY_PROPERTIES']['SIZE']['DISPLAY_VALUE']?><br />
							<i><?=$arItem['DISPLAY_PROPERTIES']['POSITION']['DISPLAY_VALUE']?></i>
							<form action="" method="post">
								<input type="button" class="submit-blue order-banner" id="banner-order-button-<?=$arItem['ID']?>" name="submit" value="Приобрести"/>
								<input type="hidden" id="banner-suggestion-type-<?=$arItem['ID']?>" value="<?=$arItem['NAME']?>" />
								
							</form>
							</div>

							<p class="plan-desc"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="180" height="145" /></p>
							<ul class="plan-desc-ul" style="text-align: left;">
								<li>Анимация: <strong><?=$arItem['DISPLAY_PROPERTIES']['PRICE_ANIMATION']['DISPLAY_VALUE']?></strong> руб. <br />
								<em><?=$arItem['DISPLAY_PROPERTIES']['PRICE_ANIMATION']['DESCRIPTION']?></em></li>
								<li>Статика: <strong><?=$arItem['DISPLAY_PROPERTIES']['PRICE_STATIC']['DISPLAY_VALUE']?></strong> руб. <br />
								<em><?=$arItem['DISPLAY_PROPERTIES']['PRICE_STATIC']['DESCRIPTION']?></em></li>
							</ul>

				</div>

			<?endforeach;?>
				<input type="hidden" id="restraunt-id" value="" />
				<input type="hidden" id="restraunt-name" value="" />
			</div>
		</div>
	</div>
