<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // resize images
    if(!$arItem["PREVIEW_PICTURE"]) {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    }
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
        $arTmpDate = array();
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arTmpDate = array();
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ITEMS"][$key]["ACTIVE_TO"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_TO_YEAR"] = $arTmpDate[2];
    // truncate name
    $arResult["ITEMS"][$key]["NAME"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["NAME"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //
    $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
    
    //price
    CModule::IncludeModule("sale");
    CModule::IncludeModule("catalog");
    $arResult["ITEMS"][$key]["PRICE"] = CPrice::GetBasePrice($arItem["ID"]);
    $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]);    
    //date difference    
    $month = $days = $year = 0;
    $month = substr($arItem["ACTIVE_TO"],3,2); 
    $days = substr($arItem["ACTIVE_TO"],0,2); 
    $year = substr($arItem["ACTIVE_TO"],6,4);;     
    $arResult["ITEMS"][$key]["LAST_DAYS"] = ceil((mktime(0, 0, 0, $month, $days, $year) - time())/86400);
        $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
}
?>