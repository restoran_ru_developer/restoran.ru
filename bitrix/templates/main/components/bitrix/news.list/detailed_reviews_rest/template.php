<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="review <?=($key==1)?"right":"left"?>">
        <div class="left">
            <?for($i=1;$i<=$arItem["PROPERTIES"]["ratio"]["VALUE"];$i++):?>
                <div class="small_star_a" alt="<?=$i?>"></div>
            <?endfor;?>
            <?for($i;$i<=5;$i++):?>
                <div class="small_star" alt="<?=$i?>"></div>
            <?endfor;?>
            <div class="clear"></div>
       </div>
       <div class="data right"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></div>
       <div class="clear"></div>
       <p><i><?=$arItem["PREVIEW_TEXT"]?></i></p>
       <div align="right"><?=$arItem["NAME"]?></div>
    </div>
<?endforeach;?>
<div class="clear"></div>
<br />
<div class="right"><a href="#" class="uppercase"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
