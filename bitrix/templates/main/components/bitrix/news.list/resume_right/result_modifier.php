<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // get subway name
    foreach($arItem["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"] as $subwayKey=>$subway) {
        $rsSubway = CIBlockElement::GetByID($subway);
        $arSubway = $rsSubway->Fetch();

        $arResult["ITEMS"][$key]["PROPERTIES"]["SUGG_WORK_SUBWAY"]["DISPLAY_VALUE"][$subwayKey] = $arSubway["NAME"];
    }
    
    // get user-resume info
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    $arUser = $rsUser->Fetch();
    if ($arItem['PREVIEW_PICTURE'])
        $arResult["ITEMS"][$key]['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']["ID"], array('width' => 64, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
    if ($arUser['PERSONAL_PHOTO'])
        $arResult["ITEMS"][$key]["USER"]["PHOTO"] = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 64, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
    else
    {
        if($arUser["PERSONAL_GENDER"]=="M")
            $arResult["ITEMS"][$key]["USER"]["PHOTO"]["src"] = "/tpl/images/noname/new/man_nnm_42.png";
        elseif($arUser["PRESONAL_GENDER"]=="F")
            $arResult["ITEMS"][$key]["USER"]["PHOTO"]["src"] = "/tpl/images/noname/woman_nnm_42.png";
        else
            $arResult["ITEMS"][$key]["USER"]["PHOTO"]["src"] = "/tpl/images/noname/unisx_nnm_42.png";
    }
    $arResult["ITEMS"][$key]["USER"]["FIO"] = $arUser['LAST_NAME']." ".$arUser['NAME'];
    $arResult["ITEMS"][$key]["USER"]["CITY"] = $arUser['PERSONAL_CITY'];
}
?>