<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="content">
    <div class="left" style="width:730px">
        <h1>
            <?=strip_tags($arResult["ITEMS"][0]["DISPLAY_PROPERTIES"]["RESTORAN"]["DISPLAY_VALUE"])." - ".$arResult["IBLOCK_TYPE"]?>
        </h1>
        <?
        $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE","clear_cache");
        $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=ACTIVE_FROM&".(($_REQUEST["pageSort"]=="ACTIVE_FROM"&&$_REQUEST["by"]=="desc"||!$_REQUEST["pageSort"]) ? "by=asc": "by=desc"), $excUrlParams);                
        $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=shows&".(($_REQUEST["pageSort"]=="shows"&&$_REQUEST["by"]=="asc"||!$_REQUEST["pageSort"]) ? "by=desc": "by=asc"), $excUrlParams);
        ?>                
        <div class="sorting">
            <div class="left">
                <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                <?$by = ($_REQUEST["by"]=="asc")?"a":"z";?>
                <?if ($_REQUEST["pageSort"] == "ACTIVE_FROM" || !$_REQUEST["pageSort"]):                                                   
                    echo "<span class='".$by."'><a class='another' href='".$sortNewPage."'>".GetMessage("SORT_NEW_TITLE")."</a></span>";
                else:
                    echo '<span><a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a></span>';
                endif;?>
                    / 
                <?if ($_REQUEST["pageSort"] == "shows"):
                    echo "<span class='".$by."'><a class='another' href='".$sortPopularPage."'>".GetMessage("SORT_POPULAR_TITLE")."</a></span>";
                else:
                    echo '<span><a class="another" href="'.$sortPopularPage.'">'.GetMessage("SORT_POPULAR_TITLE").'</a></span>';
                endif;?>
            </div>
            <div class="right"><?//=$arResult["NAV_STRING"]?></div>                    
            <div class="clear"></div>
        </div>
        <?foreach($arResult["ITEMS"] as $key=>$arItem):?>   
            <div class="new_restoraunt left<?if($key%3==2):?> end<?endif?>">
                <div class="dates_metro">
                    <div class="left">
                        <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]." ".$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?>
                        <?if ($arItem["ACTIVE_TO"]):?>
                            <?="&ndash; ".$arItem["DISPLAY_ACTIVE_TO_DAY"]." ".$arItem["DISPLAY_ACTIVE_TO_MONTH"]?>
                        <?endif;?>
                    </div>
                    <div class="right metro_<?=CITY_ID?>"><?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]?></div>
                    <div class="clear"></div>
                </div>
                <div class="favorites" style="margin:5px 0px;">
                    <?
                    $params = array();
                    $params["id"] = $arItem["ID"];
                    $params = addslashes(json_encode($params));            
                    ?>
                    <a class="another" href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>
                    <div class="rating"  style="margin:5px 0px;">
                        <?for($i = 1; $i <= 5; $i++):?>
                            <div class="star<?if($i <= round($arItem["DISPLAY_PROPERTIES"]["ratio"]["DISPLAY_VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                        <?endfor?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="title"  style="margin:5px 0px;">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                </div>
                <div class="price">
                    <?=GetMessage("R_SALE")?><span> <?=$arItem["PROPERTIES"]["sale"]["VALUE"]?>%</span> <?=GetMessage("R_ZA")?> <span><?=$arItem["PRICE"]["PRICE"]?></span> <span class="rouble">e</span>
                </div>
                <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
                <div class="kupon_image_block">
                    <div class="kupon_image" style="position:relative">
                        <?if ($arItem["PROPERTIES"]["kupon_dnya"]["VALUE"]=="Да"):?>
                            <div class="new">Купон дня!</div>                
                        <?endif;?>
                        <?if ($arItem["PROPERTIES"]["new"]["VALUE"]=="Да"):?>
                            <div class="new">новинка!</div>
                        <?elseif ($arItem["PROPERTIES"]["popular"]["VALUE"]=="Да"):?>
                            <div class="new">популярное!</div>
                        <?endif;?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
                    </div>
                </div>
                <?endif;?>        
                <div>
                    Коментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)
                </div>
                <div class="grey" align="center">
                    Купили <span><?=($arItem["PROPERTIES"]["quantity"]["VALUE"]+$arItem["PROPERTIES"]["delta"]["VALUE"])?></span> купонов.<br />
                        <?if ($arItem["ACTIVE_TO"]):?>До конца продаж: <?=$arItem["LAST_DAYS"]?> <?=pluralForm(intval($arItem["LAST_DAYS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?><?endif;?>
                </div>
            </div>
            <?if($key%3==2):?>
                <div class="clear"></div>   
                <div class="border_line"></div>
            <?endif;?>
        <?endforeach;?>
        <?if ($arResult["NAV_STRING"]):?>
            <hr class="bold" />        
            <div class="left">
                <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
                <?=($_REQUEST["pageRestCnt"] == 20 || !$_REQUEST["pageRestCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">20</a>')?>
                <?=($_REQUEST["pageRestCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=40'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">40</a>')?>
                <?=($_REQUEST["pageRestCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=60'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">60</a>')?>
            </div>
            <div class="right">
                <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                    <?=$arResult["NAV_STRING"]?>
                <?endif;?>
            </div>
            <div class="clear"></div>
        <?endif;?>
    <br /><br />
    </div>
    <div class="right">
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <br />
        <div id="search_article">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.form",
                    "main_page_subscribe",
                    Array(
                            "USE_PERSONALIZATION" => "Y",
                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                            "SHOW_HIDDEN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => ""
                    ),
            false
            );?>           
        </div>
        <br />
        <div class="tags">
            <?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                                "SORT" => "CNT",
                                "PAGE_ELEMENTS" => "20",
                                "PERIOD" => "",
                                "URL_SEARCH" => "/search/index.php",
                                "TAGS_INHERIT" => "Y",
                                "CHECK_DATES" => "Y",
                                "FILTER_NAME" => "",
                                "arrFILTER" => array(
                                                0 => "iblock_kupons",
                                ),
                                "arrFILTER_iblock_kupons" => array(
                                                0 => $arResult["ITEMS"][0]["IBLOCK_ID"],
                                ),
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "3600",
                                "FONT_MAX" => "24",
                                "FONT_MIN" => "12",
                                "COLOR_NEW" => "24A6CF",
                                "COLOR_OLD" => "24A6CF",
                                "PERIOD_NEW_TAGS" => "",
                                "SHOW_CHAIN" => "Y",
                                "COLOR_TYPE" => "N",
                                "WIDTH" => "100%"
                                ),
                                $component
                );?> 

        </div>                
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    </div>
    <div class="clear"></div>
</div>