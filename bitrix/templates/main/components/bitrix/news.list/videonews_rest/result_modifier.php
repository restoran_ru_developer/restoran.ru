<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);

    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["NAME"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["NAME"]);
    preg_match("/#FID_([0-9]+)#/",$arItem["DETAIL_TEXT"],$video1);
    if ($video1[1])
        $arResult["ITEMS"][$key]["VIDEO"] = "http://".SITE_SERVER_NAME."".CFile::GetPath($video1[1]);
    preg_match("/<iframe ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9-_&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9-&;:?.=_\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9-&;:?.=_\/]+)\"/",$arItem["DETAIL_TEXT"],$video2);
    if ($video2[1])
    {
        for ($i=1;$i<count($video2);$i++)
        {
            if ($video2[$i]=="width")
                $width = $video2[++$i];
            elseif ($video2[$i]=="height")
                $height = $video2[++$i];
            elseif($video2[$i]=="src")
                $src = $video2[++$i];                
        }
    }
    
    if ($width&&$height&&$src)
    {       
        
        $height = 308*$height/$width;
        $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$src.'" width="308" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
}
CModule::IncludeModule("iblock");
$res = CIBlockElement::GetList(Array(),Array("ID"=>$arResult["ITEMS"][0]["PROPERTIES"]["RESTORAN"]["VALUE"]),false,false,Array("ID","DETAIL_PAGE_URL"));
if ($ar = $res->GetNext())
{
    $arResult["LINK"] = $ar["DETAIL_PAGE_URL"];
}
?>