<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // get subway name
    foreach($arItem["PROPERTIES"]["SUBWAY_BIND"]["VALUE"] as $subwayKey=>$subway) {
        $rsSubway = CIBlockElement::GetByID($subway);
        $arSubway = $rsSubway->Fetch();

        $arResult["ITEMS"][$key]["PROPERTIES"]["SUBWAY_BIND"]["DISPLAY_VALUE"][$subwayKey] = $arSubway["NAME"];
    }

    // get resto name
    $rsResto = CIBlockElement::GetByID($arItem["PROPERTIES"]["REST_BIND"]["VALUE"]);
    $arResto = $rsResto->Fetch();
    $arResult["ITEMS"][$key]["BIND_RESTO"] = $arResto["NAME"];

    // count comments
    if($arItem["PROPERTIES"]["COMMENTS"]["VALUE"]) {
        $arCntFilter = Array(
            "CNT_ACTIVE" => "Y"
        );
        $arResult["ITEMS"][$key]["COMMENTS_CNT"] = CIBlockSection::GetSectionElementsCount($arItem["PROPERTIES"]["COMMENTS"]["VALUE"], $arCntFilter);
    }

    // get contact person info
    $rsUserCP = CUser::GetByID($arItem["PROPERTIES"]["USER_CONTACT_BIND"]["VALUE"]);
    $arUserCP = $rsUserCP->Fetch();
    $arResult["ITEMS"][$key]["CONTACT_PERSON"]["FIO"] = $arUserCP["LAST_NAME"]." ".$arUserCP["NAME"];
    $arResult["ITEMS"][$key]["CONTACT_PERSON"]["PHONE"] = $arUserCP["PERSONAL_PHONE"];
    $arResult["ITEMS"][$key]["CONTACT_PERSON"]["CITY"] = $arUserCP["PERSONAL_CITY"];
    $arResult["ITEMS"][$key]["CONTACT_PERSON"]["PHOTO"] = CFile::ResizeImageGet($arUserCP["PERSONAL_PHOTO"], array('width' => 85, 'height' => 85), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}
?>