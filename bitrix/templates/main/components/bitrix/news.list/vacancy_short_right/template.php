<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
 <div class="top_block"><?=GetMessage("BLOCK_TITLE")?></div>
<?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
<div class="content-vrb-block vb-first vb"style="min-height: 180px;">
    <div class="content-vrb-content" style="padding-bottom: 0px;">
        <div class="content-vrb-content-title" style="font-size:18px;"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
        <div class="block-param">
            <span class="block-param-title">Зарплата:</span> <span class="block-param-content"><?=$arItem["PROPERTIES"]["WAGES_OF"]["VALUE"]?> &mdash; <?=$arItem["PROPERTIES"]["WAGES_TO"]["VALUE"]?> <span class="rouble font12">e</span></span>
        </div>
        <div class="block-param">
            <span class="block-param-title">Опыт работы:</span> <span class="block-param-content"><?=$arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"]?></span>
        </div>
        <div class="block-param">
            <span class="block-param-title">График:</span> <span class="block-param-content"><?=implode(" / ", $arItem["PROPERTIES"]["SHEDULE"]["VALUE"])?></span>
        </div>
        <br class="block-param-break">
        <div class="metro_<?=CITY_ID?>">
            <?=implode(", ", $arItem["PROPERTIES"]["SUBWAY_BIND"]["DISPLAY_VALUE"])?>
        </div>
    </div>
    <div class="content-vrb-links">
        <div class="block-info-comments">
            Комментарии: (<a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["COMMENTS_CNT"])?></a>)
        </div>
        <div class="block-info-next">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Далее</a>
        </div>
    </div>
</div>
<?endforeach?>