<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<div class="cols<?=$arParams["NEWS_COUNT"]?>">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="videonews left<?if($key == ($arParams["NEWS_COUNT"]-1)):?> end<?endif?>">
            <?if ($arItem["VIDEO"]):?>
                <a class="myPlayer" href="<?=$arItem["VIDEO"]?>"></a> 
                <script>
                    flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                        clip: {
                                autoPlay: false, 
                                autoBuffering: true
                        }
                    });
                </script>
            <?endif;?>
            <?if ($arItem["VIDEO_PLAYER"]):?>  
                <?=$arItem["VIDEO_PLAYER"]?>
            <?endif;?>
            <span class="data"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span>
            <span><?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></span>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>
            <div class="left">
                <?=GetMessage("VN_COMMENTS")?>: (<a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?$arItem["PROPERTIES"]["COMMENTS"]["VALUE"]:"0"?></a>)
            </div>
            <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
            <div class="clear"></div>
        </div>
    <?endforeach;?>
    <div class="clear"></div>
    <?if (!count($arResult["ITEMS"])):?>
        <div class="errortext">Ничего не найдено</div>
    <?else:?>
        <div align="right"><a class="uppercase" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_VIDEONEWS")?></a></div>
    <?endif;?>
</div>