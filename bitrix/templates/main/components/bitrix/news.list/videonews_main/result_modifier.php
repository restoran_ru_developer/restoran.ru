<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);

    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["NAME"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["NAME"]);
    preg_match("/#FID_([0-9]+)#/",$arItem["DETAIL_TEXT"],$video1);
    if ($video1[1])
        $arResult["ITEMS"][$key]["VIDEO"] = "http://".SITE_SERVER_NAME."".CFile::GetPath($video1[1]);
    /*preg_match("/<iframe src=\"([a-z0-9&;:?.=\/]+)\" width=\"([0-9]+)\" height=\"([0-9]+)\"/",$arItem["DETAIL_TEXT"],$video2);
    if ($video2[1]&&$video2[2]&&$video2[3])
    {
        $height = 350*$video2[3]/$video2[2];
        $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$video2[1].'" width="350" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
    preg_match("/<iframe width=\"([0-9]+)\" height=\"([0-9]+)\" src=\"([a-zA-Z:.=\/]+)\"/",$arItem["DETAIL_TEXT"],$video3);    
    if ($video3[1]&&$video3[2]&&$video3[3])
    {
        $height = 350*$video3[2]/$video3[1];
        $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$video3[3].'" width="350" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }*/
    
    preg_match("/<iframe ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\"/",$arItem["DETAIL_TEXT"],$video2);
    if ($video2[1])
    {
        for ($i=1;$i<count($video2);$i++)
        {
            if ($video2[$i]=="width")
                $width = $video2[++$i];
            elseif ($video2[$i]=="height")
                $height = $video2[++$i];
            elseif($video2[$i]=="src")
                $src = $video2[++$i];                
        }
    }
    if ($width&&$height&&$src)
    {            
        $height = 350*$height/$width;
        $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$src.'" width="350" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
}
?>