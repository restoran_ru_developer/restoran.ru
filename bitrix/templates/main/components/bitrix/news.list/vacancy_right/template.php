<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!--<span class="sdbr-panel-out"><span class="sdbr-panel-in"><span><?=GetMessage("BLOCK_TITLE")?></span></span></span>-->
<?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
<div class="content-vrb-block vb-first vb">
    <div class="content-vrb-content">
        <div class="content-vrb-content-title" align="left"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
<!--        <div class="content-vrb-content-personal">
            <div class="avatar resume-personal-image"><img src="<?=$arItem["CONTACT_PERSON"]["PHOTO"]["src"]?>" alt="<?=$arItem["CONTACT_PERSON"]["FIO"]?>"></div>
            <div class="resume-personal-content-wrapper">
                <div class="resume-personal-name"><?=$arItem["CONTACT_PERSON"]["FIO"]?></div>
                <div class="resume-personal-place"><?=$arItem["CONTACT_PERSON"]["CITY"]?></div>
            </div>
        </div>-->
        <br class="block-param-break">
        <div class="block-param">
            <span class="block-param-title">Зарплата:</span> <span class="block-param-content"><span class="nowrapping"><?=$arItem["PROPERTIES"]["WAGES_OF"]["VALUE"]?> &mdash; <?=$arItem["PROPERTIES"]["WAGES_TO"]["VALUE"]?> Р<span class="roubledash">&ndash;</span></span></span>
        </div>
        <div class="block-param">
            <span class="block-param-title">Опыт работы:</span> <span class="block-param-content"><?=$arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"]?></span>
        </div>
        <div class="block-param">
            <span class="block-param-title">График:</span> <span class="block-param-content">
                <?if (is_array($arItem["PROPERTIES"]["SHEDULE"]["VALUE"])):?>
                    <?=implode(" / ", $arItem["PROPERTIES"]["SHEDULE"]["VALUE"])?>
                <?else:?>
                    <?=$arItem["PROPERTIES"]["SHEDULE"]["VALUE"]?>
                <?endif;?>
            </span>
        </div>
        <br class="block-param-break">
        <div class="metro_<?=CITY_ID?>">
            <?=implode(", ", $arItem["PROPERTIES"]["SUBWAY_BIND"]["DISPLAY_VALUE"])?>
        </div>
    </div>
    <div class="content-vrb-links">
        <div class="block-info-comments">
            Комментарии: (<a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["COMMENTS_CNT"])?></a>)
        </div>
        <div class="block-info-next">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Далее</a>
        </div>
    </div>
</div>
<div class="dotted"></div>
<Br />
<?endforeach?>