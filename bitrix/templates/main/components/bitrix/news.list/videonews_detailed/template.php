<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="cols<?=$arParams["NEWS_COUNT"]?>"
        <div class="videonews left<?if($key == 1):?> end<?endif?>">
            <!--<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />-->        
            <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.$arItem["PROPERTIES"]["VIDEO"]["VALUE"]["path"]?>"></a> 
            <script>
                flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                    clip: {
                            autoPlay: false, 
                            autoBuffering: true
                    }
                });
            </script>
            <span class="data">20</span>
            <span>октября 2011</span>
            <h3><?=$arItem["NAME"]?></h3>
            <div class="left">
                Комментарии: (<a href="#">0</a>)
            </div>
            <div class="right"><a href="#"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
            <div class="clear"></div>
        </div>
    </div>
<?endforeach;?>
