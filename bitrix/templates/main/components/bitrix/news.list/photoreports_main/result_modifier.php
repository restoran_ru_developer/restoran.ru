<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    foreach($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["PHOTOS"]["VALUE"] as $pKey=>$photo)
        // resize  photos
        $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["PHOTOS_DISPLAY"][$pKey] = CFile::ResizeImageGet($photo, array('width' => 397, 'height' => 271), BX_RESIZE_IMAGE_EXACT, true);
}
?>