<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="left">
        <div class="img">
            <img src="<?=$arItem["DISPLAY_PROPERTIES"]["PHOTOS_DISPLAY"][0]["src"]?>" />
        </div>
        <div class="special_scroll">
            <div class="scroll_container">
                <?foreach($arItem["DISPLAY_PROPERTIES"]["PHOTOS_DISPLAY"] as $pKey=>$photo):?>
                    <div class="item<?if($pKey == 0):?> active<?endif?>" num="<?=$pKey+1?>">
                        <img src="<?=$photo["src"]?>" align="bottom" height="50" />
                    </div>
                <?endforeach?>
            </div>
        </div>
        <div class="left scroll_arrows">
            <a class="prev browse left"></a>
            <div align="center" class="left" style="text-align:center;width:40px;"><span class="scroll_num">1</span>/<?=count($arItem["DISPLAY_PROPERTIES"]["PHOTOS_DISPLAY"])?></div>
            <a class="next browse right"></a>
        </div>
    </div>
    <div class="right" style="width:310px;">
        <h3><?=$arItem["NAME"]?></h3>
        <?/*?><p><i>Отличный видовой ресторан в самом центре Петербурга</i></p><?*/?>
        <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <br />
        <div class="left"><?=GetMessage("COMMENTS_TITLE")?>: (<a href="#">0</a>)</div>
        <div class="right"><a href="#"><?=GetMessage("GO_TO_FULL")?></a></div>
        <div class="clear"></div>
    </div>
<?endforeach;?>