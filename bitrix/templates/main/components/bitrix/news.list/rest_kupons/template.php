<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>" style="<?if($key != 2):?>margin-right:38px;<?endif;?>width:300px">
        <div class="dates_metro">
            <div>
                <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]." ".$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?>
                <?if ($arItem["ACTIVE_TO"]):?>
                    <?="&ndash; ".$arItem["DISPLAY_ACTIVE_TO_DAY"]." ".$arItem["DISPLAY_ACTIVE_TO_MONTH"]?>
                <?endif;?>
            </div>
            <div class=" metro_<?=CITY_ID?>" style="margin:5px 0px;"><?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]?></div>
        </div>
        <div>
            <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
        </div>
        <div class="rating" style="margin:10px 0px;">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="star<?if($i <= round($arItem["PROPERTIES"]["RATIO"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
        </div>
         <div class="price">
            <?=GetMessage("R_SALE")?><span> <?=$arItem["PROPERTIES"]["sale"]["VALUE"]?>%</span> <?=GetMessage("R_ZA")?> <span><?=$arItem["PRICE"]["PRICE"]?></span> <span class="rouble">e</span>
        </div>
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="kupon_image_block">
            <div class="kupon_image">
                <?/*if ($arItem["PROPERTIES"]["kupon_dnya"]["VALUE"]=="Да"):?>
                    <div class="kupon_of_day">Купон дня!</div>                
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["new"]["VALUE"]=="Да"):?>
                    <div class="new">новинка!</div>
                <?elseif ($arItem["PROPERTIES"]["popular"]["VALUE"]=="Да"):?>
                    <div class="new">популярное!</div>
                <?endif;*/?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>             
            </div>
        </div>
        <?endif;?>
        <div class="left">
            Коментарии: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?></a>)
        </div>
        <div class="right">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a>
        </div>
        <div class="clear"></div>
    </div>
<?endforeach;?>
<div class="clear"></div>
<div align="right"><a class="uppercase" href="<?=$arResult["LINK"]?><?=$arParams["IBLOCK_TYPE"]?>/"><?=GetMessage("ALL_KUPONS")?></a></div>