<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>        
        <div class="kupon_name" align="center">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </div>
        <div class="price"  align="center">
            <?=GetMessage("R_SALE")?><span> <?=$arItem["PROPERTIES"]["sale"]["VALUE"]?>%</span> <?=GetMessage("R_ZA")?> <span><?=$arItem["PRICE"]["PRICE"]?></span> <span class="rouble">e</span>
        </div>
        <?if ($arItem["PREVIEW_PICTURE"]["SRC"]):?>
        <div class="kupon_image_block">
            <div class="kupon_image">
                <?if ($key==0):?>
                    <div class="new">Купон дня!</div>
                <?endif;?>
                <?if ($key==4):?>
                    <div class="new">новинка!</div>
                <?endif;?>
                <?if ($key==6):?>
                    <div class="new">популярное!</div>
                <?endif;?>
                <img width="232" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />               
            </div>
        </div>
        <?endif;?>        
        <div class="left">
            Коментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)
        </div>
        <div class="right">
            <a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Далее</a>
        </div>
        <div class="clear"></div>
        <div class="grey" align="center">
            Купили <span><?=($arItem["PROPERTIES"]["quantity"]["VALUE"]+$arItem["PROPERTIES"]["delta"]["VALUE"])?></span> купонов.            
        </div>
        <div class="grey" align="center">
            <?if ($arItem["ACTIVE_TO"]):?>До конца продаж: <?=$arItem["LAST_DAYS"]?> <?=pluralForm(intval($arItem["LAST_DAYS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?><?endif;?>
        </div>
<?endforeach;?>