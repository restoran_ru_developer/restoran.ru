<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div align="center" class=" <?if($key == 1):?>end<?endif?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /><br /><br />
        <?endif;?>
        <p class="font16"><?=$arItem["NAME"]?></p>
        <?if ($arItem["PRICE"]["PRICE"]>0):?>
            <?if (CITY_ID=="ast"||CITY_ID=="amt"):?>
                <p class="font18"><?=$arItem["PRICE"]["PRICE"]?> <span class="tenge font18">h</span></p>
            <?else:?>
                <?if ($arItem["PRICE"]["CURRENCY"]=="EUR"):?>
                    <p class="font18">€ <?=$arItem["PRICE"]["PRICE"]?></td>
                <?else:?>
                    <p class="font18"><?=$arItem["PRICE"]["PRICE"]?> <span class="rouble font18">e</span></p>
                <?endif;?>                
            <?endif;?>
        <?endif;?>
        <div class="clear"></div>
        <?if ($key==0):?>
        <div class="dotted"></div>
        <?endif;?>
    </div>
<?endforeach;?>
