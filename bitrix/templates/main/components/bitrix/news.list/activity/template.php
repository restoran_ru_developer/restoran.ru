<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["AJAX"]!="Y"):?>
    <h2>Активность</h2>
    <div id="user_activity">
<?endif;?>
    <table class="profile-activity">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?> 
        <tr>
                <td class="profile-activity-col1"><p><?=GetMessage("TYPE_".$arItem["IBLOCK_TYPE_ID"])?>
                        <br /><?=$arItem["CREATED_DATE_FORMATED_1"]?></p>
                </td>
                <td class="profile-activity-col11 <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="121" />
                </td>
                <td class="profile-activity-col2 <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">                            
                        <p><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
                        <p class="upcase"><?=GetMessage("LINKTYPE_".$arItem["LINK_IBLOCK_TYPE"])?></p>
                </td>
                <td class="profile-activity-col3 <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>"><?=$arItem["PREVIEW_TEXT"]?></td>
        </tr>    
    <?endforeach;?>           
    </table>
<?if ($arParams["AJAX"]!="Y"):?> 
        </div>
    <p class="more-activity"><a href="javascript:void(0)" onclick="send_ajax('<?=$this->__folder?>/ajax.php?USER_ID=<?=$_REQUEST["USER_ID"]?>&<?=bitrix_sessid_get()?>','html',false,add_activity)"><?=GetMessage("MORE_ACTIVITY")?></a></p>
<?endif;?>