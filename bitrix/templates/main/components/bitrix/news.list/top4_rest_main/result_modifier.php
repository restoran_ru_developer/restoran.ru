<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    // get and set user name or login
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->Fetch())
       $arResult["ITEMS"][$key]["AUTHOR_NAME"] = ($arUser["NAME"] || $arUser["LAST_NAME"] ? $arUser["NAME"]." ".$arUser["LAST_NAME"]  : $arUser["LOGIN"]);
    $arReviewsIB = getArIblock("reviews", CITY_ID);
    $arResult["ITEMS"][$key]["REVIEWS"] = getReviewsRatio($arReviewsIB["ID"],$arItem["PROPERTIES"]["reviews_bind"]["VALUE"]);
}

?>