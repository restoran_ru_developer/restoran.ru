<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="top_restoraunt<?if($key == 3):?> top_restoraunt_end<?endif?>">
        <div class="position left"><?=($key + 1)?></div>
        <div class="title"><?=$arItem["NAME"]?></div>
        <div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="small_star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
       </div>
       <div class="clear"></div>
       <p><i><?=$arItem["PREVIEW_TEXT"]?></i></p>
       <div class="left"><?=$arItem["AUTHOR_NAME"]?></div>
       <div class="right"><a href="#"><?=GetMessage("CT_BNL_ELEMENT_ALL_REVIEWS")?></a></div>
       <div class="clear"></div>
    </div>
<?endforeach;?>