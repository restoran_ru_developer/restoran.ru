<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $DB;
if($arIBType = CIBlockType::GetByIDLang($arResult["IBLOCK_TYPE_ID"], LANG))   
    $arResult["IBLOCK_TYPE_NAME"] = htmlspecialcharsex($arIBType["NAME"]);

//v_dump($arResult);

// get rest iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
// get cuisine iblock info
$arCuisineIB = getArIblock("cuisine", CITY_ID);
// get comments iblock info
$arCommIB = getArIblock("comments", CITY_ID);
// get author name
$rsUser = CUser::GetByID($arResult["SECTION"]["PATH"][0]["CREATED_BY"]);
if($arUser = $rsUser->GetNext())
    $arResult["AUTHOR_NAME"] = $arUser["NAME"];

// format date
$date = $DB->FormatDate($arResult["SECTION"]["PATH"][0]["DATE_CREATE"], 'YYYY-MM-DD HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
$arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
$arTmpDate = explode(" ", $arTmpDate);
$arResult["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
$arResult["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

// get rest info
foreach($arResult["ITEMS"] as $key=>$block) {
    if(is_array($block["PROPERTIES"]["REST_BIND"]["VALUE"])) {
        foreach($block["PROPERTIES"]["REST_BIND"]["VALUE"] as $restBind) {
            $rsRest = CIBlockElement::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "ID" => $restBind,
                ),
                false,
                false,
                Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_photos", "PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill")
            );
            while($arRest = $rsRest->GetNext()) {
                // get rest city name
                $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
                $arRestCity = $rsRestCity->GetNext();

                // get rest picture
                if(!$arRest["PREVIEW_PICTURE"]) {
                    $restPic = CFile::ResizeImageGet($arRest["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
                } else {
                    $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 148, 'height' => 118), BX_RESIZE_IMAGE_EXACT, true);
                }

                // get cuisine name
                $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
                $arCuisine = $rsCuisine->GetNext();

                // set rest info
                $arTmpRest["NAME"] = $arRest["NAME"];
                $arTmpRest["CITY"] = $arRestCity["NAME"];
                $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
                $arTmpRest["REST_PICTURE"] = $restPic;
                if(!in_array($arCuisine["NAME"], $arTmpRest["CUISINE"]))
                    $arTmpRest["CUISINE"][] = $arCuisine["NAME"];
                $arTmpRest["AVERAGE_BILL"] = $arRest["PROPERTY_AVERAGE_BILL_VALUE"];

                $arResult["ITEMS"][$key]["REST_BIND_ARRAY"][$arRest["ID"]] = $arTmpRest;
            }
            $arTmpRest["CUISINE"] = Array();
        }
    }

    // get respondent data
    if($block["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"] && $block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]) {
        $arResult["ITEMS"][$key]["RESPONDENT"]["PHOTO"] = CFile::ResizeImageGet($block["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"], array('width' => 168, 'height' => 168), BX_RESIZE_IMAGE_EXACT, true);
        $arResult["ITEMS"][$key]["RESPONDENT"]["NAME"] = $block["PROPERTIES"]["RESPONDENT_NAME"]["VALUE"];
        $arResult["ITEMS"][$key]["RESPONDENT"]["POST"] = $block["PROPERTIES"]["RESPONDENT_POST"]["VALUE"];
        if($block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"])
            $arResult["ITEMS"][$key]["RESPONDENT"]["SPEECH"] = $block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"];
    }

    // get photos
    foreach($block["PROPERTIES"]["PICTURES"]["VALUE"] as $picKey=>$pic) {
        $photoPic = CFile::ResizeImageGet($pic, array('width' => 394, 'height' => 271), BX_RESIZE_IMAGE_EXACT, true);
        $photoPic["description"] = $block["PROPERTIES"]["PICTURES"]["DESCRIPTION"][$picKey];
        $arResult["ITEMS"][$key]["PICTURES"][] = $photoPic;
        // cnt pictures
        $arResult["ITEMS"][$key]["CNT_PICTURES"] = count($arResult["ITEMS"][$key]["PICTURES"]);
    }
    if ($block["PROPERTIES"]["VIDEO"]["VALUE"])
    {
        $arResult["ITEMS"][$key]["VIDEO"] = $block["PROPERTIES"]["VIDEO"]["VALUE"];
    }
}

// get current date
$arResult["TODAY_DATE"] = CIBlockFormatProperties::DateFormat('j F', MakeTimeStamp(date('d.m.Y'), CSite::GetDateFormat()));

// get current date
$arResult["COMMENTS_IBLOCK_ID"] = $arCommIB["ID"];

// get section user props for comments
$rsSecComm = CIBlockSection::GetList(
    Array("SORT"=>"ASC"),
    Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => $arResult["ID"],
        "ID" => $arResult["SECTION"]["PATH"][0]["ID"]
    ),
    false,
    Array("ID", "UF_COMMENTS")
);
if($arSecComm = $rsSecComm->Fetch()) {
    $arResult["COMMENTS_CNT"] = intval($arSecComm["UF_COMMENTS"]);
}
//v_dump($arResult);
$arResult["NEXT_ARTICLE"] = RestIBlock::GetArticleNext($arResult["SECTION"]["PATH"][0]["IBLOCK_ID"],$arResult["SECTION"]["PATH"][0]["ID"]);
$arResult["PREV_ARTICLE"] = RestIBlock::GetArticlePrev($arResult["SECTION"]["PATH"][0]["IBLOCK_ID"],$arResult["SECTION"]["PATH"][0]["ID"]);
if ($arResult["SECTION"]["PATH"][0]["ID"])
{
    $db_list = CIBlockSection::GetList(Array(), Array("ID"=>$arResult["SECTION"]["PATH"][0]["ID"],"IBLOCK_ID"=>$arResult["SECTION"]["PATH"][0]["IBLOCK_ID"]), false, Array("UF_COUNT"));
    if ($r = $db_list->GetNext())
    {
        $r["UF_COUNT"]++;
        $bs = new CIBlockSection;
        $arFields = Array (
            "UF_COUNT" => $r["UF_COUNT"]
        );
        $res = $bs->Update($arResult["SECTION"]["PATH"][0]["ID"], $arFields);
    }
}
?>