<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$resto = array();?>
<div id="content">
    <div class="left" style="width:720px;">
        <div class="statya_section_name">
            <h4><?=$arResult["IBLOCK_TYPE_NAME"]?></h4>
            <div class="statya_nav">
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h1>
        <a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <?foreach($arResult["ITEMS"] as $key=>$block):?>
            <?
            // short text
            if($block["PREVIEW_TEXT"]):?>
                <i><?=$block["PREVIEW_TEXT"]?></i>
                <br /> <br />
            <?
            // prev type for layout
            $prevType = 'shortText';
            endif?>
            <?
            // rest bind
            if(is_array($block["REST_BIND_ARRAY"])):?>
                <div class="left" style="width:165px;">
                    <?foreach($block["REST_BIND_ARRAY"] as $rest):?>
                        <img class="indent" src="<?=$rest["REST_PICTURE"]["src"]?>" width="148" /><br />
                            <a class="font14" href="#"><?=$rest["NAME"]?></a>,<br />
                            <?=$rest["CITY"]?>
                        <p>
                            <div class="rating">
                                <?for($i = 0; $i < 5; $i++):?>
                                    <div class="small_star<?if($i < $rest["RATIO"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                                <?endfor?>
                                <div class="clear"></div>
                            </div>
                            <b><?=GetMessage("CUISINE")?>:</b>
                            <?foreach($rest["CUISINE"] as $cuisine):?>
                                <?=$cuisine?><?if(end($rest["CUISINE"]) != $cuisine) echo ", ";?>
                            <?endforeach?>
                            <br />
                            <b><?=GetMessage("AVERAGE_BILL")?>:</b> <?=$rest["AVERAGE_BILL"]?><br />
                        </p>
                        <?if(end($block["REST_BIND_ARRAY"]) != $rest):?>
                            <hr class="dotted" />
                        <?endif?>
                        <?$resto[] = Array("ID"=>$rest["ID"],"NAME"=>$rest["NAME"]);?>
                    <?endforeach?>
                </div>
            <?
            $prevType = 'rest';
            endif?>
            <?
            //  text
            if($block["DETAIL_TEXT"]):?>
                <div class="interview_answer right"<?if($prevType == 'rest'):?> style="width:555px;"<?endif?>><?=$block["DETAIL_TEXT"]?></div>
                <div class="clear"></div>
                <br /><br />
            <?endif?>
            <?
            // speech
            if($block["RESPONDENT"]):?>
                <div class="dotted"></div>
                <table class="direct_speech">
                    <tr>
                        <td width="170">
                            <div class="author">
                                <img src="<?=$block["RESPONDENT"]["PHOTO"]["src"]?>" />
                            </div>
                        </td>
                        <td width="150">
                            <span class="uppercase"><?=$block["RESPONDENT"]["NAME"]?></span><br/>
                            <i><?=$block["RESPONDENT"]["POST"]?></i>
                        </td>
                        <td class="font16">
                            <i>«<?=$block["RESPONDENT"]["SPEECH"]?>»</i>
                        </td>
                    </tr>
                </table>
                <div class="dotted"></div>
                <br />
            <?
            $prevType = 'speech';
            endif?>
            <?if(is_array($block["VIDEO"])):?>
                <div class="video">
                    <script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
                        <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.$block["VIDEO"]["path"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                }
                            });
                        </script>
                </div>
            <?endif;?>
            <?
            //  photos
            if(is_array($block["PICTURES"])):?>
                <div class="left articles_photo">
                    <?if($block["CNT_PICTURES"] == 1):?>
                        <div class="img">
                            <img src="<?=$block["PICTURES"][0]["src"]?>" width="720" />
                            <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                        </div>
                    <?else:?>
                        <script type="text/javascript">
                            $(document).ready(function(){
                               $(".articles_photo").galery();
                            });
                        </script>
                        <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/<?=$block["CNT_PICTURES"]?><a class="next browse right"></a></div>
                        <div class="clear"></div>
                        <div class="img">
                            <img src="<?=$block["PICTURES"][0]["src"]?>" width="720" />
                            <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                        </div>
                        <div class="special_scroll">
                            <div class="scroll_container">
                                <?foreach($block["PICTURES"] as $keyPic=>$pic):?>
                                    <div class="item<?if($keyPic == 0):?> active<?endif?>">
                                        <img src="<?=$pic["src"]?>" alt="<?=$pic["description"]?>" align="bottom" />
                                    </div>
                                <?endforeach?>
                            </div>
                        </div>
                    <?endif?>
                </div>
                <div class="clear"></div>
            <?
            $prevType = 'photos';
            endif?>
        <?endforeach?>
        <?
        // TODO not work
        /*
        ?>
        <p><?=GetMessage("TAGS_TITLE")?>: <a href="#" class="another">название тега 1</a>, <a href="#" class="another">название тега 2</a>, <a href="#" class="another">название тега 3</a></p>
        <?*/?>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#comments" class="anchor another" onclick="return false;"><?=$arResult["COMMENTS_CNT"]?></a>)</div>
            <!--<div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <div class="right" style="padding-right:15px;"><input type="button" class="button" value="<?=GetMessage("SUBSCRIBE")?>" /></div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
            <div class="clear"></div>
        </div>
        <?if(count($resto)):?>
            <div id="order" style="width:700px">
                <div class="left">
                    <script>

                        $(document).ready(function(){
                            var params = {
                                changedEl: "#order_what",
                                visRows: 5
                            }
                            cuSel(params);
                            var params = {
                                changedEl: "#order_many",
                                visRows: 5
                            }
                            cuSel(params);
                            var params = {
                                changedEl: "#rest",
                                visRows: 5
                            }
                            cuSel(params);
                            $.tools.dateinput.localize("ru",  {
                            months:        '<?=GetMessage("MONTHS_FULL")?>',
                            shortMonths:   '<?=GetMessage("MONTHS_SHORT")?>',
                            days:          '<?=GetMessage("WEEK_DAYS_FULL")?>',
                            shortDays:     '<?=GetMessage("WEEK_DAYS_SHORT")?>'
                            });
                            $(".whose_date").dateinput({lang: 'ru', firstDay: 1 , format:"dd/mm/yy",trigger:true, css: { trigger: 'caltrigger'}, change: function(e, date)  {
                                $(".caltrigger").html(this.getValue("dd mmmm"));
                            } });
                        $(".caltrigger").html("<?=$arResult["TODAY_DATE"]?>");
                        $.maski.definitions['~']='[0-2]';
                        $.maski.definitions['!']='[0-5]';
                        $(".time").maski("~9:!9");
                        });
                    </script>
                    Бронирование
                    <select id="order_what" name="order_what">
                        <option selected="selected" value="1">столика</option>
                        <option  value="2">банкета</option>
                        <option value="3">фуршета</option>
                    </select>
                    в ресторане 
                    <select id="rest" name="rest">
                        <?foreach($resto as $res):?>
                            <option selected="selected" value="<?=$res["ID"]?>"><?=$res["NAME"]?></option>
                        <?endforeach;?>
                    </select>
                    <br />
                    <span style="position: relative; display: inline">
                        <input class="whose_date" style="" type="date" style="visibility:hidden" />
                    </span>
                    в <input class="time" type="text" size="4" value="1230"/> на
                    <select id="order_many" name="order_many">
                        <option selected="2" value="2">2</option>
                        <option  value="4">4</option>
                        <option value="6">6</option>
                        <option value="8">8</option>
                    </select>
                    персоны
                    <div class="phone">
                        <sub>Заказ столика или банкета:<br /> </sub>
                        +7 (495) 988-26-56, +7 (495) 506-00-33
                    </div>
                </div>
                <div class="right">
                    <input type="button" class="dark_button" value="ЗАБРОНИРОВАТЬ" />
                    <div class="coupon">
                        -50% <span>скидка</span><br />
                        <a href="#">Купить купон</a>
                    </div>

                </div>
            </div>
        <?endif;?>
        <br /><br />
        <div id="tabs_block7">
            <ul class="tabs">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block">
                   <?$APPLICATION->IncludeComponent(
                   	"restoran:comments",
                   	"",
                   	Array(
                            "IBLOCK_TYPE" => "comment",
                            "ELEMENT_ID" => $arResult["SECTION"]["PATH"][0]["ID"],
                            "IS_SECTION" => "Y"
                   	),
                   false
                   );?>
               </div>
            </div>            
        </div>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2881483, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <hr class="black" />
        <div class="statya_section_name">
            <h4><?=$arResult["IBLOCK_TYPE_NAME"]?></h4>
            <div class="statya_nav">
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "content_article_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
    <div class="right" style="width:240px">
        <div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <div id="users_onpage"></div>
            </div>
            <div class="bottom"></div>
        </div>   
        <br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        <br /><br />
        <div class="top_block">Читайте также</div>
        <?
        global $arrFil;
        $arrFil = Array("!ID"=>$arResult["SECTION"]["PATH"][0]["ID"]);
        ?>
        <?$APPLICATION->IncludeComponent(
            "restoran:article.list",
            "interview_one_with_border",
            Array(
                    "IBLOCK_TYPE" => $arResult["IBLOCK_TYPE_ID"],
                    "IBLOCK_ID" => $arResult["SECTION"]["PATH"][0]["IBLOCK_ID"],
                    "FILTER_NAME" => "arrFil",
                    "PAGE_COUNT" => 3,
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "",
                    "PICTURE_WIDTH" => 235,
                    "PICTURE_HEIGHT" => 127,
                    "DESCRIPTION_TRUNCATE_LEN" => 200,
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "1",
                    "SECTION_FIELDS" => "",
                    "SECTION_USER_FIELDS" => Array(""),
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "Y"
            ),
            false
        );?> 
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
    <div class="clear"></div>
</div>