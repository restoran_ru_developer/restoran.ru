<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="faq-rand-left" style="width:225px;">
<?foreach($arResult["ITEMS"] as $k => $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <?if(strlen($arItem["PREVIEW_TEXT"]) > 0):?>
        <li<?if($k==2){?> class="last"<?}?>>
            <a href="/content/articles/help/<?=$arItem["~IBLOCK_SECTION_ID"]?>/#faq<?=$arItem["ID"]?>" class="faq-rand-left-faq-name"><?=$arItem["NAME"]?></a>
            <div>
                <span><?=$arItem["PREVIEW_TEXT"]?></span>
            </div>
        </li>
    <?endif?>
<?endforeach;?>
</ul>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>