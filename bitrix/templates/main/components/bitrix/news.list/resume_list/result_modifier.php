<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // get subway name
    foreach($arItem["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"] as $subwayKey=>$subway) {
        $rsSubway = CIBlockElement::GetByID($subway);
        $arSubway = $rsSubway->Fetch();

        $arResult["ITEMS"][$key]["PROPERTIES"]["SUGG_WORK_SUBWAY"]["DISPLAY_VALUE"][$subwayKey] = $arSubway["NAME"];
    }

    // get user-resume info
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    $arUser = $rsUser->Fetch();
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 64, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
    //v_dump($arResult["ITEMS"][$key]["PREVIEW_PICTURE"]);
    $arResult["ITEMS"][$key]["USER"]["PHOTO"] = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array('width' => 64, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
    $arResult["ITEMS"][$key]["USER"]["FIO"] = $arUser['LAST_NAME']." ".$arUser['NAME'];
    $arResult["ITEMS"][$key]["USER"]["CITY"] = $arUser['PERSONAL_CITY'];
    $arResult["ITEMS"][$key]["USER"]["ID"] = $arUser['ID'];    
}

?>