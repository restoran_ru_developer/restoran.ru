<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$arReviewsIB = getArIblock("reviews", CITY_ID);
foreach($arResult["ITEMS"] as $key=>$arItem) {
    if (is_array($arItem["PREVIEW_PICTURE"]))
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    else
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    // get and set user name or login
}

?>