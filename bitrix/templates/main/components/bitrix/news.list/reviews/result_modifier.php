<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // explode date
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ITEMS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    $arUser = $rsUser->Fetch();
    $arResult["ITEMS"][$key]["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    
    // get rest name
    $rsSec = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
    if($arSec = $rsSec->GetNext()) {
        $arResult["ITEMS"][$key]["RESTAURANT_NAME"] = $arSec["NAME"];
    }
}
?>