<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="comments">
    <ul>
        <?foreach($arResult["ITEMS"] as $key => $arItem):?>
        <li>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td width="80" valign="top">
                        <div class="ava">
                            <?if($arItem["AVATAR"]):?>
                                <img src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" />
                            <?endif?>
                        </div>
                    </td>
                    <td width="150"  valign="top">
                        <span class="name"><?=$arItem["NAME"]?></span>
                        <span class="comment_date"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                    </td>
                    <td>
                        <h3 style="margin-top:0px; padding-top:0px; line-height:14px;"><?=$arItem["RESTAURANT_NAME"]?></h3>
                        <div class="small_rating" style="margin-bottom:10px;">
                            <?for($i=1;$i<=$arItem["PROPERTIES"]["ratio"]["VALUE"];$i++):?>
                               <div class="small_star_a" alt="<?=$i?>"></div>
                            <?endfor;?>
                            <?for($i;$i<=5;$i++):?>
                               <div class="small_star" alt="<?=$i?>"></div>
                            <?endfor;?>
                           <div class="clear"></div>
                        </div>
                        <i><?=$arItem["PREVIEW_TEXT"]?></i>
                    </td>
                </tr>
            </table>            
        </li>
        <?endforeach?>
    </ul>
</div>