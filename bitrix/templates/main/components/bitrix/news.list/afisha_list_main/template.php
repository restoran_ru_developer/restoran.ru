<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$todayDate = date("d.m.Y");
?>

<script type="text/javascript">
			$(function()
			{
				$('.poster_pane').jScrollPane({verticalGutter:15});
			});
</script>



<ul id="poster_tabs">
    <li><a href="#">Сегодня</a></li>
    <?
    for($i = 1; $i < 5; $i++):
    $day = ConvertTimeStamp(strtotime($todayDate.' +'.$i.'day'), "SHORT", "ru");
    $days[$i] = $day; 
    ?>
        <li><a href="#"<?if($i == 1):?> class="long"<?endif?>><?if($i == 1):?><?=GetMessage("TOMORROW_LABEL")?><br /><?endif?><?=FormatDate($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($day), CSite::GetDateFormat("SHORT"))?></a></li>
    <?endfor?>
</ul>
<?//v_dump($arResult["AFISHA_ITEMS"]);?>
<div class="poster_panes">
    <div class="poster_pane">
        <table class="poster" cellpadding="5" cellspacing="0">
            <?if ($arResult["AFISHA_ITEMS"][$todayDate]):?>
            <?foreach($arResult["AFISHA_ITEMS"][$todayDate] as $todayEvent):?>
                <tr>
                    <td class="time">
                        <div style="overflow:hidden; height:64px">
                            <img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" width="86" />
                        </div>
                    </td>
                    <td class="time"><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?></td>
                    <td class="name"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>
                    <td class="type">
                        <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                            <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                        <?else:?>
                            <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                        <?endif;?>
                    </td>
                    <td width="220"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a><br /><?=$todayEvent["PROPERTIES"]["ADRES"]["VALUE"]?></td>
                </tr>
            <?endforeach?>
            <?else:?>
                <tr>
                    <td class="font14"><?=GetMessage("NO_POSTER")?></td>
                </tr>
            <?endif;?>
        </table>
    </div>
    <?foreach ($days as $da):?>
    <div class="poster_pane">
        <table class="poster" cellpadding="5" cellspacing="0">
        <?if ($arResult["AFISHA_ITEMS"][$da]):?>
        <?foreach($arResult["AFISHA_ITEMS"][$da] as $todayEvent):?>
            <tr>
                <td class="time">
                    <div style="overflow:hidden; height:64px">
                        <img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" width="86" />
                    </div>
                </td>
                <td class="time"><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?></td>
                <td class="name"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>
                <td class="type">                    
                    <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                        <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                    <?else:?>
                        <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                    <?endif;?>
                </td>
                <td width="220"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a><br /><?=$todayEvent["PROPERTIES"]["ADRES"]["VALUE"]?></td>
            </tr>
            <?endforeach?>
        <?else:?>
            <tr>
                <td class="font14"><?=GetMessage("NO_POSTER")?></td>
            </tr>
        <?endif;?>
        </table>
    </div>
    <?endforeach;?>
    <br />
    <div align="right"><a class="uppercase" href="/<?=CITY_ID?>/afisha/">ВСЕ АФИШИ</a></div>
</div>