<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!$_REQUEST["date"])
    $todayDate = date("d.m.Y");
else
    $todayDate = trim($_REQUEST["date"]);
foreach($_GET as $key=>$get)
{
    $page_param[] = $key."=".rawurlencode($get);
}
$page_param = implode("&",$page_param);
?>
<?if ($arParams["AJAX"]!="Y"):?>
<script>
$(function() 
{
    // press enter in form
    $('#a_filter input[type=text]').keypress(function (e) {
         if(e.which == 13) {
             filter_afisha('<?=$APPLICATION->GetCurPage()?>');
         }
    });

    //tabs left right
    var num_li = $("#afisha_box").find("#poster_tabs").find("li").size()-4;
    var afisha_obj = $("#afisha_box").find("#poster_tabs");
    var hash = location.hash.replace("#","");
    if (hash) $("#afisha_date").attr("value",hash);
    var current_afisha_date_num = afisha_obj.find("a[href^='"+hash+"']").parent().attr("num")*1;     
    if (current_afisha_date_num > 2 && current_afisha_date_num < (afisha_obj.find("li").size()-4))
        afisha_obj.animate({left:-(current_afisha_date_num-3)*160+"px"});
    else if (current_afisha_date_num >= (afisha_obj.find("li").size()-4))
        afisha_obj.animate({left:-(afisha_obj.find("li").size()-4)*160+"px"});
    else
        afisha_obj.animate({left:"0px"});
    $(".afisha_arrow_left").click(function(){
        if (afisha_obj.css("left").replace("px","")==0)
            afisha_obj.animate({left:-num_li*160+"px"})
        else
            afisha_obj.animate({left:"+=160"})
    });
    
    $(".afisha_arrow_right").click(function(){
        if (afisha_obj.css("left").replace("px","")<=-160*num_li)
            afisha_obj.animate({left:"0px"})
        else
            afisha_obj.animate({left:"-=160"})        
    });
    
    //focus:placeholder
    $("#afisha_filter .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
    });
    // blur:placeholder
    $("#afisha_filter .placeholder").on("blur", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val().length <= 0)
            $(this).val(defVal);
    });
    
    //datepicker
    $.tools.dateinput.localize("ru",  {
           months:        'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь',
           shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',
           days:          'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',
           shortDays:     'вс,пн,вт,ср,чт,пт,сб'
        });
    $(".datep").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy",min: -1, max: 28 });  

    //type of restoraunt
    var params = {  
        changedEl: "#type"
    };
    cuSel(params);
    
    $("#restoraunt").autocomplete("/tpl/ajax/afisha_suggest.php", {
        limit: 5,
        minChars: 3,
        width: 173,
        formatItem: function(data, i, n, value) {
            return value.split("###")[0];
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        }        
    });  
      $("#restoraunt").result(function(event, data, formatted) {
        if (data) {
            $("#restoran222").val(formatted.split("###")[1]);
        }
   	});
});
function filter_afisha(url)
{
    var params = "";//$("#a_filter").serialize();
    var date = "";
    if ($("#afisha_date").val() != $("#afisha_date").attr("defvalue"))
        var date = "#"+$("#afisha_date").val();
    if ($("#restoran222").val() != $("#restoran222").attr("defvalue"))
        params += "restoran="+$("#restoran222").val()+"&";
    if ($("#tags").val() != $("#tags").attr("defvalue"))
        params += "tags="+$("#tags").val()+"&";
    if ($("#type").val()!="0")
        params += "type="+$("#type").val();
    if (params)
        params = "?" + params;
    //console.log(url + params + date);
    location.href = url + params + date;
    var afisha_obj = $("#afisha_box").find("#poster_tabs");
    var hash = location.hash.replace("#","");
    var current_afisha_date_num = afisha_obj.find("a[href^='"+hash+"']").parent().attr("num")*1;     
    if (current_afisha_date_num > 2 && current_afisha_date_num < (afisha_obj.find("li").size()-4))
        afisha_obj.animate({left:-(current_afisha_date_num-3)*160+"px"});
    else if (current_afisha_date_num >= (afisha_obj.find("li").size()-4))
        afisha_obj.animate({left:-(afisha_obj.find("li").size()-4)*160+"px"});
    else
        afisha_obj.animate({left:"0px"});
}
</script>
<div id="afisha_filter">
    <form id="a_filter">
        <div style="position: absolute; top:-22px; right:0px;"><a href="/<?=CITY_ID?>/afisha/" class="js">сбросить фильтр</a></div>
        <div class="left"><input id="afisha_date" type="text" class="filter_text datep placeholder" value="Дата" defvalue="Дата"/></div>
        <div class="left">
            <select id="type">
                <option value="0" selected="selected">Тип события</option>
                <?foreach ($arResult["PROPERTY_EVENT_TYPE"] as $type):?>
                    <option <?=($_REQUEST["type"]==$type["ID"])?"selected":""?> value="<?=$type["ID"]?>"><?=$type["NAME"]?></option>                    
                <?endforeach;?>
            </select>
        </div>
        <div class="left">
            <input type="text" id="restoraunt" class="filter_text placeholder" value="<?=($_REQUEST["restoraunt"])?$_REQUEST["restoraunt"]:"Заведение"?>" size="30" defvalue="Заведение" />
            <input type="hidden" id="restoran222" value="<?=($_REQUEST["restoran"])?$_REQUEST["restoran"]:""?>" size="30" name="restoran"/>
        </div>
        <div class="left"><input type="text" class="filter_text italic placeholder" name="tags" value="<?=($_REQUEST["tags"])?$_REQUEST["tags"]:"Ключевое слово"?>" defvalue="Ключевое слово" size="25" id="tags" /></div>
        <div class="right"><input style="padding:0px 4px; width:96px;" id="afisha_filter_button" type="button" class="light_button end" value="Найти" onclick="filter_afisha('<?=$APPLICATION->GetCurPage()?>')" /></div>
        <div class="clear"></div>
    </form>
</div> 
<div class="left afisha_arrow_left"></div>
<div id ="afisha_box" class="overflow left" style="height: 55px; width: 640px; position:relative">
    <ul id="poster_tabs" class="left" style="width:2000em; position:absolute; left:0px;" ajax="ajax" ajax_url="/bitrix/templates/main/components/bitrix/news.list/afisha/ajax.php?<?=$page_param?>&date=">
        <li num="1"><a href="<?=$todayDate?>" class="long"><i><?=GetMessage("TODAY_LABEL")?>, <?=GetMessage("TEXT_DAY_".date("w"))?></i><br /><?=FormatDate($todayDate, MakeTimeStamp($day), CSite::GetDateFormat("SHORT"))?></a></li>
        <?   
        for($i = 1; $i <= 28; $i++):
            $day = ConvertTimeStamp(strtotime($todayDate.' +'.$i.'day'), "SHORT", "s1");
            $date = date_create($day);
            $text_day = date_format($date, 'w');            
        ?><li num="<?=($i+1)?>"><a href="<?=$day?>" class="long"><i><?if($i == 1):?><?=GetMessage("TOMORROW_LABEL")?>, <?endif?><?=GetMessage("TEXT_DAY_".$text_day)?></i><br /><?=FormatDate($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($day), CSite::GetDateFormat("SHORT"))?></a></li>
    <?endfor?>
    </ul>
</div>
<div class="right afisha_arrow_right"></div>
<div class="clear"></div>
<div class="poster_panes">
    <?for($i = 1; $i <= 29; $i++):?>
            <div class="poster_pane"></div>
    <?endfor?>
</div>
<?else:?>
<?//v_dump($arResult["AFISHA_ITEMS"][$todayDate])?>
    <?if (count($arResult["AFISHA_ITEMS"][$todayDate])>0):?>
        <table class="afisha_table poster" cellpadding="0" cellspacing="0" width="730">
            <?foreach($arResult["AFISHA_ITEMS"][$todayDate] as $todayEvent):?>
            <tr>
                <td>
                    <?if ($todayEvent["PREVIEW_PICTURE"]):?>
                    <div style="overflow:hidden; height:64px">
                        <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" width="86" /></a>
                    </div>
                    <?endif;?>
                </td>
                <td class="time"  style="vertical-align:middle"><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?></td>
                <td class="font14"  style="vertical-align:middle"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>            
                <td class="type" align="center"  style="vertical-align:middle"><?=strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?></td>
                <td width="220"  style="vertical-align:middle">
                    <?if ($todayEvent["RESTAURANT"]["NAME"]):?>
                    <a target="_blank" href="<?=$todayEvent["RESTAURANT"]["DETAIL_PAGE_URL"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a><br />
                    <?endif;?>
                    <?=$todayEvent["PROPERTIES"]["ADRES"]["VALUE"]?>
                </td>
            </tr>
            <?endforeach?>
        </table>
    <?else:?>
        <table class="afisha_table poster" cellpadding="0" cellspacing="0" width="730">
            <tr>
                <td class="font14"><?=GetMessage("NO_POSTER")?></td>
            </tr>
        </table>
    <?endif;?>
<? endif; ?>
