<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// get afisha iblock info
$arAfishaIB = getArIblock("afisha", CITY_ID);
if (!$arAfishaIB["ID"]) 
{
    ShowError("Section not found");
    die;
}
global $arrFilter;
$times = array();
$arFilter = Array("IBLOCK_ID"=>$arAfishaIB["ID"], "=PROPERTY_EVENT_DATE"=>array(false, ConvertTimeStamp($_REQUEST["date"], "FULL")),
                                    Array("LOGIC"=>"OR",
                                           Array("!PROPERTY_d1"=>false),
                                           Array("!PROPERTY_d2"=>false),
                                           Array("!PROPERTY_d3"=>false),
                                           Array("!PROPERTY_d4"=>false),
                                           Array("!PROPERTY_d5"=>false),
                                           Array("!PROPERTY_d6"=>false),
                                           Array("!PROPERTY_d7"=>false),
                                        ),
                                    Array("LOGIC"=>"OR",
                                            Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y h:i:s")),
                                            Array("DATE_ACTIVE_TO"=>false)
                                        )
    );
if ($arrFilter["PROPERTY_EVENT_TYPE"])
   $arFilter["PROPERTY_EVENT_TYPE"] = $arrFilter["PROPERTY_EVENT_TYPE"];
if ($arrFilter["PROPERTY_RESTORAN"])
   $arFilter["PROPERTY_RESTORAN"] = $arrFilter["PROPERTY_RESTORAN"];
//if ($arrFilter["PROPERTY_TAGS"])
//   $arFilter["PROPERTY_TAGS"] = $arrFilter["PROPERTY_TAGS"];

if($_REQUEST["tags"])
{
    $arFilter[] = Array(
        "LOGIC" => "OR",
        Array("NAME" => "%".trim($_REQUEST["tags"])."%"),
        Array("DETAIL_TEXT" => "%".trim($_REQUEST["tags"])."%")
    );
}

/*$db_enum_list = CIBlockProperty::GetPropertyEnum("TYPE", Array(), Array("IBLOCK_ID"=>$arAfishaIB["ID"]));
while ($ar_enum_list = $db_enum_list->GetNext())
    $arResult["PROPERTY_TYPE"][] = $ar_enum_list;    */
$res = CIBlockElement::GetList(Array(),$arFilter,false,false,Array("*","PROPERTY_d1","PROPERTY_d2","PROPERTY_d3","PROPERTY_d4","PROPERTY_d5","PROPERTY_d6","PROPERTY_d7","PROPERTY_RESTORAN","PROPERTY_EVENT_TYPE","PROPERTY_TIME","PROPERTY_ADRES","PROPERTY_EVENT_DATE")
                            );
while($ar = $res->GetNext())
{
    $day = date("N",  strtotime($_REQUEST["date"]));
    if ($ar["PROPERTY_D".$day."_VALUE"]=="Y" && !$ar["PROPERTY_EVENT_DATE_VALUE"])
    {
        $ar["ACTIVE_FROM2"] = $ar["PROPERTY_EVENT_DATE_VALUE"]; 
        $ar["PROPERTY_EVENT_DATE_VALUE"] = $_REQUEST["date"]; 
         $detail = $ar["DETAIL_PICTURE"];
            $preview = $ar["PREVIEW_PICTURE"]; 
            unset($ar["DETAIL_PICTURE"]);
            unset($ar["PREVIEW_PICTURE"]);
            if (is_array($detail))                
                $ar["DETAIL_PICTURE"] =  $detail; 
            else
                $ar["DETAIL_PICTURE"]["ID"] =  $detail; 
            if (is_array($preview))                
                $ar["PREVIEW_PICTURE"] =  $preview; 
            else
                $ar["PREVIEW_PICTURE"]["ID"] =  $preview; 
        $ar["PROPERTIES"]["RESTORAN"]["VALUE"] =  $ar["PROPERTY_RESTORAN_VALUE"]; 
        
        $r = CIBlockElement::GetByID($ar["PROPERTY_EVENT_TYPE_VALUE"]);
        $a = $r->Fetch();
        $ar["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"] = $a["NAME"];
        
        //$ar["PROPERTIES"]["EVENT_TYPE"]["VALUE"] =  $ar["PROPERTY_EVENT_TYPE_VALUE"]; 
        $ar["PROPERTIES"]["TIME"]["VALUE"] =  $ar["PROPERTY_TIME_VALUE"]; 
        $ar["PROPERTIES"]["ADRES"]["VALUE"] =  $ar["PROPERTY_ADRES_VALUE"]; 
        $arResult["ITEMS"][] = $ar;

    }
    
}
//v_dump($arResult["ITEMS"]);
foreach($arResult["ITEMS"] as $key=>$arItem) {
    /* get rest info */
    // get cur sec info
    /*$rsSec = CIBlockSection::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $arAfishaIB["ID"],
            "ID" => $arItem["IBLOCK_SECTION_ID"]
        ),
        false,
        Array("ID", "UF_REST_BIND")
    );
    $arSec = $rsSec->GetNext();*/
    // get rest info
    /*$rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $arRestIB["ID"],
            "ID" => $arSec["UF_REST_BIND"]
        ),
        false,
        false,
        Array("ID", "NAME", "CODE", "PROPERTY_address")
    );*/
    if (is_array($arItem["PROPERTIES"]["RESTORAN"]["VALUE"]))
    {
        $arItem["PROPERTIES"]["RESTORAN"]["VALUE"] = $arItem["PROPERTIES"]["RESTORAN"]["VALUE"][0];
    }
    $rsRest = CIBlockElement::GetByID($arItem["PROPERTIES"]["RESTORAN"]["VALUE"]);
    $arRest = $rsRest->GetNext();
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array("sort" => "asc"), Array("CODE"=>"sleeping_rest"));
        if($ar_props = $db_props->Fetch())
            $sleep = IntVal($ar_props["VALUE"]);
        
    if (!$arItem["PROPERTY_EVENT_DATE_VALUE"])
        $arItem["PROPERTY_EVENT_DATE_VALUE"] = $arItem["PROPERTIES"]["EVENT_DATE"]["VALUE"];
    if (!$sleep)
    {        
        $arItem["PROPERTY_EVENT_DATE_VALUE"] = date("d.m.Y",strtotime($arItem["PROPERTY_EVENT_DATE_VALUE"]));
        $arItem["RESTAURANT"]["NAME"] = $arRest["NAME"];
        $arItem["RESTAURANT"]["DETAIL_PAGE_URL"] = $arRest["DETAIL_PAGE_URL"];
        //$arItem["RESTAURANT"]["ADDRESS"] = $arRest["PROPERTY_ADDRESS_VALUE"];    
        if (!$arItem["PREVIEW_PICTURE"]["ID"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>96, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true);
        else
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>96, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true);
        $arItem["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"]["src"];
        if (!$arItem["PREVIEW_PICTURE"])
            $arItem["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm.png";
        $arItem["TIME"] = $arItem["PROPERTIES"]["TIME"]["VALUE"];
        // resort        
        $arResult["AFISHA_ITEMS"][$arItem["PROPERTY_EVENT_DATE_VALUE"]][] = $arItem;
        //$times[$arItem["PROPERTY_EVENT_DATE_VALUE"]][] = $arItem["TIME"];
    }
          
}
function cmp($a, $b)
{       
    
    $a = date("d.m.Y H:i",strtotime("1970-01-01 ".$a["TIME"].":00"));
    
    $b = date("H:i",strtotime($b["TIME"]));
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
//    return strcmp($a, $b);
}
//foreach ($times as &$time)
//{
//    foreach ($times as &$a)
//        usort($a, "cmp");
//}
$temp_array = array();
    $temp_array = $arResult["AFISHA_ITEMS"];
    $arResult["AFISHA_ITEMS"] = array();    
    foreach ($temp_array as $key=>$items)
    {
        $ar_to_time = array();
        $times = array();
        $ch = 1;
        foreach ($items as $key2=>$item)
        {
            $ti = "";           
            $t = explode("-",$item["PROPERTIES"]["TIME"]["VALUE"]);
            $t= $t[0];
            if (!$t)
                $t = "23:59";
            if ($t=="00:00")
                $t = "23:58";
            $ti = strtotime($t);                        
            if ($ti)
            {
                if (in_array($ti, $times))
                {
                    $ti = $ti+$ch;
                    $ar_to_time[$ti] = $item;            
                    $ch++;
                }
                else
                    $ar_to_time[$ti] = $item;            
            }
            else
                $ar_to_time[$key2] = $item;                        
            $times[] = $ti;            
        }
        ksort($ar_to_time);
        $arResult["AFISHA_ITEMS"][$key] = $ar_to_time;
    }
    unset($temp_array);
    unset($ar_to_time);
$rsType = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_ID"=>209,"ACTIVE"=>"Y"),false,false,Array("ID","NAME"));
     while ($arType = $rsType->GetNext())
         $arResult["PROPERTY_EVENT_TYPE"][] = $arType;
?>