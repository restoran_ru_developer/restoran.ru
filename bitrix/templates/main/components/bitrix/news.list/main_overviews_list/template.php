<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
           <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
           <div style="height:85px;overflow:hidden">
                <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
           </div>
           <p style="height:80px;overflow:hidden"><?=$arItem["PREVIEW_TEXT"]?></p>
           <div class="left"><?=GetMessage("COMMENTS_TITLE")?>: (<a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"])?></a>)</div>
           <div class="right"><a href="#"><?=GetMessage("SEE_FULL")?></a></div>
        </div>
<?endforeach;?>
