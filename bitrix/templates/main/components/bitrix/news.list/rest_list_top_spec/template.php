<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="title"><a href="<?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"http://www.federicoclub.ru/":$arItem["DETAIL_PAGE_URL"]?>" <?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"target='_blank'":""?>><?=$arItem["NAME"]?></a></div>
    <div class="active_rating left" style="padding-top:4px; padding-left:25px">
        <?for($k = 1; $k <= 5; $k++):?>
            <div class="star<?if($k <= $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>_a<?endif?>" alt="<?=$k?>"></div>
        <?endfor?>
        <div class="clear"></div>
    </div>
    <div class="new"><?=GetMessage("R_ADVERTISEMENT")?></div>
    <div class="clear"></div>
    <div class="left photogalery_top_spec" style="position:relative; width:392px">
        <div class="img" style="overflow:hidden; height:264px">
            <a href="<?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"http://www.federicoclub.ru/":$arItem["DETAIL_PAGE_URL"]?>" <?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"target='_blank'":""?>><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="392" /></a>
        </div>
        <?if(count($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"])>1):?>
        <div class="special_scroll">
            <div class="scroll_container">
                <?foreach($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"] as $pKey=>$photo):?>
                    <div class="item<?if($pKey == 0):?> active<?endif?>">
                        <img src="<?=$photo["SRC"]?>" align="bottom" />
                    </div>
                <?endforeach?>
            </div>
        </div>
        <div class="left scroll_arrows">
            <a class="prev browse left"></a>
             <span class="scroll_num">1</span>/<?=count($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"])?>
            <a class="next browse right"></a>
        </div>
        <?endif;?>
        <div class="right" style="width:225px; white-space: nowrap; padding-top:8px;">
            <?/*<div class="left">
                <script type="text/javascript"><!--
                document.write(VK.Share.button({url: '<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>' ,title: "<?=$arItem["NAME"]?>", image: '<?='http://'.SITE_SERVER_NAME.$arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"][0]["src"]?>'},{type: 'custom', text: '<img src="<?='http://'.SITE_SERVER_NAME?>/images/vk_like.png" height="20" />'}));
                --></script>
            </div>
            <div class="left" style="margin-left: 20px;">
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-text="<?=$arItem["NAME"]?>" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
            </div>
            <div class="left" style="margin-left: 20px;">
                <div class="fb-like" data-href="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-show-faces="true" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-font="tahoma" data-action="like"></div>
            </div>*/?>
        </div>
        <?if(CSite::InGroup( array(14,15,1))):?>                        
                            <?/*<div id="edit_link" style="position:absolute; bottom:-35px;"><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arItem["ID"]?>">Редактировать</a></div>*/?>
                            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ID"]?>">Редактировать</a></div>
                        <?endif;?>
    </div>
    <div class="right" style="width:305px;">
        <?//v_dump($arItem["DISPLAY_PROPERTIES"]["kitchen"])?>
        <?unset($arItem["DISPLAY_PROPERTIES"]["RATIO"])?>
        <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
            <p<?if($pid == "subway"):?> class="metro_<?=CITY_ID?>"<?endif?>><?if($pid != "subway"):?><b><?=GetMessage("R_PROPERTY_".$arProperty["CODE"])?>:</b><?endif?>
            <?if ($pid=="phone" && $_REQUEST["CONTEXT"]=="Y"):?>
					 <?if (CITY_ID=="spb"):?>
                    	<a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
					<?else:?>
						<a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
					<?endif;?>                                                     
				<?else:?>
				
					<?if ($pid=="phone"):?>
					<?
						
 						$arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);
 															
 						if(is_array($arProperty["DISPLAY_VALUE"])) 
							$arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
						else{
							$arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
							//$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
						}
															
						$arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);
					
						
																														
						$reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
						preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);
   															
   															
						$TELs=array();
						for($p=1;$p<5;$p++){
							foreach($matches[$p] as $key=>$v){
								$TELs[$key].=$v;
							}	
						}
   															
						foreach($TELs as $key => $T){
							if(strlen($T)>7)  $TELs[$key]="+7".$T;
						}
   															
						foreach($TELs as $key => $T){
						?>
							<a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=$arProperty["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
						<?}?>
					
					<?else:?>
					
					
					
                	<?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                   		<?=$arProperty["DISPLAY_VALUE"]?>
                	<?else:?>
                    	<?=implode(", ",$arProperty["DISPLAY_VALUE"])?>
                	<?endif?>
                	
                	<?endif?>
                <?endif?>
            </p>
        <?endforeach;?>
        <br />
        <div style="padding-bottom:30px; height:102px; overflow: hidden">
            <p><i class="font14"><?=$arItem["PREVIEW_TEXT"]?></i></p>
        </div>
        <div align="right">
            <?
            $params = array();
            $params["id"] = $arItem["ID"];
            $params = addslashes(json_encode($params));
            $params2 = "";
            $params2 = "name=".rawurlencode($arItem["NAME"])."&id=".$arItem["ID"]."&what=2&".bitrix_sessid_get();    
            ?>
            <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                                
            <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>
                <input onclick="ajax_bron('<?=$params2?>')" type="submit" value="<?=GetMessage("BOOK_BUTTON")?>" class="button" />
            <?/*elseif(CITY_ID=="kld"):?>
                <input onclick="ajax_bron('<?=$params2?>',false,'_<?=CITY_ID?>')" type="submit" value="<?=GetMessage("BOOK_BUTTON")?>" class="button" />
                <?*/?>
            <?endif;?>
        </div>
    </div>
    <div class="clear"></div>
<?endforeach;?>