<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])>0){?>
<table class="profile-activity w100" cellpadding="0" cellspacing="0">
	<tr>
		<th>Приглашение</th>
		<th>Гости</th>
		<th>Статус</th>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):?>	
        <?if($respUserReg["ACCEPT"]!="N"):?>
	<tr class="<?=(end($arResult["ITEMS"])==$arItem)?"nobg":""?> <?=($arItem["ACTIVE"]=="N")?"disable":""?>" id="inv<?=$arItem["ID"]?>">
            
		<td valign="top" class="first w-new" width="240">
                    <div class="left">
                        <img src="<?=$arItem["RESTORAN"]["PICTURE"]?>" width="70" />                        
                    </div>
                    <div class="left">
                        <a class="name" href="<?=$arItem["RESTORAN"]["URL"]?>"><?=$arItem["RESTORAN"]["NAME"]?></a><br />
                        <div class="rating">
                            <?for($i = 1; $i <= 5; $i++):?>
                                <div class="small_star<?if($i <= round($arItem["RESTORAN"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                            <?endfor?>
                            <div class="clear"></div>
                        </div>
                        <?if ($arItem["RESTORAN"]["SECTION"]=="banket"):?>
                            <p class="upcase">банкетный зал</p>
                        <?else:?>
                            <p class="upcase">ресторан</p>
                        <?endif;?>                        
                    </div>
                    <div class="clear"></div>      
                    <br />
                    <i>От: <a href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["USER_NAME"]?></a></i>
                    <br/><Br />
                    <?=$arItem["DISPLAY_ACTIVE_FROM"]?> в <?=$arItem["TIME"]?><Br /><Br />
                    <?if ($arItem["ACTIVE"]=="N"):?>
                        <i>Отменено</i>
                    <?endif;?>
                </td>		   
		<td class="user-friends nofloat" colspan="2" valign="top">
                    <ul>
                        <?foreach($arItem["USERS"] as $respUserReg):?>
                            <li <?=(end($arItem["USERS"])==$respUserReg&&!$arItem["NA_USERS"])?"class='nobg'":""?>>
                                <div class="avatar left">
                                    <img src="<?=$respUserReg["PHOTO"]["src"]?>" width="64" />
                                </div>
                                <div class="user-friend left" style="width:210px">
                                        <a class="name" href="/users/id<?=$respUserReg["ID"]?>/"><?=$respUserReg["NAME"]?></a><br/>
                                        <?=$respUserReg["PERSONAL_CITY"]?><br />
                                        <p class="user-mail"><a href="javascript:void(0)" onclick="send_message(<?=$respUserReg["ID"]?>,'<?=$respUserReg["NAME"]?>')">Сообщение</a></p>
                                </div>
                                <div class="left" style="padding-top:6px; text-align: center">
                                    <?if($respUserReg["ID"]!=$USER->GetID()):?>
                                        <?if ($respUserReg["ACCEPT"]=="Y"):?>
                                            <i>Подтвердил</i>
                                        <?else:?>
                                            <i>В ожидании</i>
                                        <?endif;?>                           
                                    <?else:?>
                                        <?if (!$arItem["PROPERTIES"]["BRON"]["VALUE"]):?>
                                            <?if(!$respUserReg["ACCEPT"]):?>
                                                <input type="button" class="light_button" value="Принять" onclick="confirm_invite(<?=$respUserReg["PROPERTY_ID"]?>)" style="margin-bottom:5px;" /><br />
                                                <input type="button" class="grey_button" onclick="declain_invite(<?=$respUserReg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)" value="Отклонить" />
                                            <?elseif($respUserReg["ACCEPT"]=="Y"):?>
                                                <input type="button" class="grey_button" onclick="declain_invite(<?=$respUserReg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)" value="Отклонить" />
                                            <?elseif($respUserReg["ACCEPT"]=="N"):?>
                                                <input type="button" class="light_button" value="Принять" onclick="confirm_invite(<?=$respUserReg["PROPERTY_ID"]?>)" style="margin-bottom:5px;" /><br />
                                            <?endif;?>
                                        <?else:?>
                                             <i>Столик забронирован</i>
                                        <?endif;?>
                                    <?endif;?>
                                </div>
                                <!--<div class="right" style="padding-top:6px;">
                                    <a class="icon-delete_f2" href="#" alt="Отклонить приглашение для данного пользователя" title="Отклонить приглашение для данного пользователя"></a> 
                                </div>-->
                                <div class="clear"></div>
                            </li>
                        <?endforeach?>
                        <?foreach($arItem["NA_USERS"] as $respUserUnreg):?>
                            <li class="<?=(end($arItem["NA_USERS"])==$respUserUnreg)?"nopic nobg":""?>">
                                <div class="user-friend left" style="width:285px">
                                        <p><?=$respUserUnreg["NAME"]?></p>
                                </div>
                                <div class="left" style="padding-top:6px;">
                                    <?if ($respUserUnreg["ACCEPT"]=="Y"):?>
                                        <i>Подтвердил</i>
                                    <?else:?>
                                        <i>В ожидании</i>
                                    <?endif;?>
                                </div>
                                <div class="clear"></div>
                            </li>
                        <?endforeach?>
                        <!--<p class="more-activity" style="margin-top:12px; margin-bottom:0;"><a href="#">Еще 3</a></p>-->
                    </ul>
                </td>
        </tr>
        <?endif;?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</table>
<?}else{?>
	Нет добавленных приглашений
<?}?>
