<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
           <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a>
           <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
           <p style="height:80px;overflow:hidden"><?=$arItem["PREVIEW_TEXT"]?></p>
           <div class="left"><?=GetMessage("COMMENTS_TITLE")?>: (<a href="#"><?=intval($arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"])?></a>)</div>
           <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("SEE_FULL")?></a></div>
           <div class="clear"></div>
        </div>
<?endforeach;?>
<?if (!count($arResult["ITEMS"])):?>
    <div class="errortext">Ничего не найдено</div>
<?else:?>
    <div align="right"><a class="uppercase" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_PHOTOREPORTS")?></a></div>
<?endif;?>
<div class="clear"></div>
<br />

