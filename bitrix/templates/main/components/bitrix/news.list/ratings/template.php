<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left<?if($key%3 == 2):?> end<?endif?>">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="232" height="127" /></a>            
        <div style="height:<?=($arParams["PARENT_SECTION_CODE"]=="kupons")?"140px":"80px"?>; overflow:hidden">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>        
            <div class="rating">
                <?for($i = 1; $i <= 5; $i++):?>
                    <div class="small_star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                <?endfor?>
                <div class="clear"></div>
            </div>        
        </div>
        <div style="height:<?=($arParams["PARENT_SECTION_CODE"]=="kupons")?"60px":"120px"?>;overflow:hidden;margin-top:5px;">            
            <?if ($arItem["PROPERTIES"]["phone"]):?>
                <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["PROPERTIES"]["phone"][0]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["address"]):?>
                <p><b><?=GetMessage("FAV_ADRESS")?></b>: <?=$arItem["PROPERTIES"]["address"][0]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["subway"]):?>
                <p class="metro_<?=CITY_ID?>"><?=$arItem["PROPERTIES"]["subway"][0]?></p>
            <?endif;?>
        </div>
        <div class="left">
            <i><?=intval($arItem["REVIEWS"]["COUNT"])?> <?=pluralForm(intval($arItem["REVIEWS"]["COUNT"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
	<?if ($key%3 == 2):?>
		<div class="clear"></div>
	<?endif;?>
        <?if ($key%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
                <div class="light_hr"></div>
	<?endif;?>
        <?if ($key%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
                <div class="clear"></div>
	<?endif;?>
<?endforeach;?>