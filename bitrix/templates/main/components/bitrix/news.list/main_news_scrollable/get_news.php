<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>
<?
$arrNewsID = Array(
    "ID" => (int)$_POST["ID"],
);
$arNewsIB = getArIblock("news", CITY_ID);
$APPLICATION->IncludeComponent(
    "restoran:catalog.list",
    "news_main",
    Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => $_POST["IBLOCK_TYPE"],
            "IBLOCK_ID" => (int)$_POST["IBLOCK_ID"],
            "NEWS_COUNT" => "1",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "",
            "SORT_ORDER2" => "",
            "FILTER_NAME" => "arrNewsID",
            "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
            "PROPERTY_CODE" => array(),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "300",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "43200",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
    ),
    false
    );?>
<?
/*$obCache = new CPHPCache; 
// время кеширования - 12 часов
$life_time = 12*60*60; 
$cache_id = $_POST["ID"]; 

if($obCache->InitCache($life_time, $cache_id, "/")):
    $vars = $obCache->GetVars();
    $arNews = $vars["RESULT"];
else :
    if(!CModule::IncludeModule('iblock'))
    return false;

    // get news data
    $rsNews = CIBlockElement::GetByID($_POST["ID"]);
    if($arNews = $rsNews->GetNext()) {
        // get detail picture file array
        $arNews["PICTURE"] = CFile::GetFileArray($arNews["DETAIL_PICTURE"]);
        // clear name
        $arNews["NAME"] = htmlspecialchars_decode($arNews["NAME"]);

        // explode date
        $arNews["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arNews["ACTIVE_FROM"], CSite::GetDateFormat()));
        $arTmpDate = explode(" ", $arNews["DISPLAY_ACTIVE_FROM"]);
        $arNews["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
        $arNews["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
        $arNews["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];

        // truncate preview text
        $arNews["PREVIEW_TEXT"] = strip_tags(htmlspecialchars_decode($arNews["PREVIEW_TEXT"]));
        $end_pos = 320;
        while(substr($arNews["PREVIEW_TEXT"], $end_pos, 1) != " " && $end_pos < strlen($arNews["PREVIEW_TEXT"]))
            $end_pos++;
        if($end_pos < strlen($arNews["PREVIEW_TEXT"]))
            $arNews["PREVIEW_TEXT"] = substr($arNews["PREVIEW_TEXT"], 0, $end_pos)."...";
        $arNews["PREVIEW_TEXT"] = html_entity_decode($arNews["PREVIEW_TEXT"]);
        // get rest name
        //$rsSec = CIBlockSection::GetByID($arNews["IBLOCK_SECTION_ID"]);
        //if($arSec = $rsSec->GetNext()) {
         //   $arNews["RESTAURANT_NAME"] = htmlspecialchars_decode($arSec["NAME"]);
        //}
        $arNews["NAME"] = change_quotes($arNews["NAME"]);
        $arNews["PREVIEW_TEXT"] = change_quotes($arNews["PREVIEW_TEXT"]);
        
    }
endif;

// начинаем буферизирование вывода
if($obCache->StartDataCache()):

    echo json_encode($arNews);
    $obCache->EndDataCache(array(
        "RESULT"    => $arNews
        )); 
endif;*/
?>