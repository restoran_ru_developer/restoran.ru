<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="image">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /><br />
        </div>
        <?endif;?>
        <div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
        </div>
        <div>
            <h3><?=$arItem["NAME"]?></h3>
        </div>
        <?if ($arItem["PREVIEW_TEXT"]):?>
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <?endif;?>
        <div class="left">
            <i><?=intval($arItem["REVIEWS"]["COUNT"])?> <?=pluralForm(intval($arItem["REVIEWS"]["COUNT"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
<?endforeach;?>