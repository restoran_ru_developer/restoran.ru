<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<div class="left news_img" id="cur_news_<?=$this->GetEditAreaId($arItem['ID']);?>" align="center">
    <div style="overflow:hidden; height:344px">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" width="480" /></a>
    </div>
    <div class="clear"></div>
    <?/*if($arItem["COMMENT"]):?>
        <div class="left"><b><?=GetMessage("CT_BNL_NEWS_COMMENT_TITLE")?>: </b></div>
        <div class="right comment"><?=$arItem["COMMENT"]["POST_MESSAGE"]?></div>
        <div class="clear"></div>
        <div class="left"><?=$arItem["COMMENT"]["AUTHOR_NAME"]?></div>
        <div class="right comment_more"><a href="#"><?=GetMessage("CT_BNL_NEWS_COMMENT_SHOW_ALL")?></a></div>
    <?endif*/?>
</div>
<div class="right news_text">
    <span class="date_size"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <span><?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></span>
    <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?=$arItem['NAME']?></a></div>
    <p><?=$arItem['PREVIEW_TEXT']?></p>
    <p align="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_NEWS_DETAIL_LINK")?></a></p>
    <div class="clear"></div>
    <br />
    <div align="right"><a class="uppercase" href="<?=$arItem["LIST_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ALL_NEWS_LINK")?></a></div>
</div>
<div class="clear"></div>
<?endforeach;?>