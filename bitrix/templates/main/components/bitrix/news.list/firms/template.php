<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?//v_dump($arItem);?>
    <div class="new_restoraunt left<?if($key%3 == 2):?> end<?endif?>">
        <div>
            <a class="another"  href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>        
        </div>
        <p><?=$arItem["DISPLAY_PROPERTIES"]["information"]["DISPLAY_VALUE"]?></p>
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="image_overflow">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="" src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a> 
        </div>
        <?endif;?>
        <div style="margin-top:5px;">            
            <?if ($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]):?>
                <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]?></p>
            <?endif;?>
            <?if ($arItem["DISPLAY_PROPERTIES"]["adres"]["DISPLAY_VALUE"]):?>
                <p><b><?=GetMessage("FAV_ADRESS")?></b>: <?=$arItem["DISPLAY_PROPERTIES"]["adres"]["DISPLAY_VALUE"]?></p>
            <?endif;?>
                <?//v_dump($arItem["DISPLAY_PROPERTIES"]["subway"]["VALUE"])?>
            <?if ($arItem["DISPLAY_PROPERTIES"]["subway"]):?>
                <p class="metro_<?=CITY_ID?>"><?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])?></p>
            <?endif;?>
        </div>
        <div class="left">
            <i><?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
	<?if ($key%3 == 2):?>
		<div class="clear"></div>
	<?endif;?>
        <?if ($key%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
                <div class="light_hr"></div>
	<?endif;?>
        <?if ($key%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
                <div class="clear"></div>
	<?endif;?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
<hr class="bold" />
    <div class="left">
        <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
        <?=($_REQUEST["pageRestCnt"] == 15 || !$_REQUEST["pageRestCnt"] ? "15" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">15</a>')?>
        <?=($_REQUEST["pageRestCnt"] == 30 ? "30" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=40'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">30</a>')?>
        <?=($_REQUEST["pageRestCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=60'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">60</a>')?>
    </div>
    <div class="right">
            <?=$arResult["NAV_STRING"]?>
        
    </div>
<div class="clear"></div>
<?endif;?>
<br /><br />