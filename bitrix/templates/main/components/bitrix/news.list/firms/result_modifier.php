<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$obParser = new CTextParser;
foreach($arResult["ITEMS"] as $key=>$arItem) {  

    if(is_array($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])) {
        foreach($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"] as $key1=>$subway) {
            $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][$key1] = strip_tags($subway);
        }
    } else {
        $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
    }
    if ($arItem["PREVIEW_PICTURE"])
    {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']["ID"], array('width'=>232, 'height'=>127), BX_RESIZE_IMAGE_EXACT, true);
    }   
    // cut detail text
    $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["information"]["DISPLAY_VALUE"] = $obParser->html_cut(strip_tags(html_entity_decode($arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["information"]["DISPLAY_VALUE"])), $arParams["PREVIEW_TRUNCATE_LEN"]);
 }
?>