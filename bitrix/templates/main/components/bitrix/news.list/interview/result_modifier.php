<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("blog"))
    return false;

foreach($arResult["ITEMS"] as $key=>$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);

    // count restaurant reviews
    $rsPostComment = CBlogComment::GetList(
        Array("ID"=>"DESC"),
        Array(
            "BLOG_ID" => 3,
            "POST_ID" => $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["reviews"]["DISPLAY_VALUE"]
        ),
        false,
        false,
        Array()
    );
    $arResult["ITEMS"][$key]["REVIEWS_CNT"] = $rsPostComment->SelectedRowsCount();
}
?>