<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arReviewIB = getArIblock("review", CITY_ID);
$arMasterIB = getArIblock("master", CITY_ID);
$arPhotoIB = getArIblock("photoreview", CITY_ID);
$arKuponsIB = getArIblock("kupons", CITY_ID);
?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <?if ($key==0):?>
        <div id="content">
            <div class="left" style="width:730px">
                <h1>Интервью</h1>
                <div class="sorting">
                    <div class="left"><span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span></div>
                    <div class="right"><?=$arResult["NAV_STRING"]?></div>                    
                    <div class="clear"></div>
                </div>
                <?$del = 3;
                $ost = 2?>
    <?endif;?>
    <?if ($key==9):?>
        <div id="additional_info_interview">
            <div class="block">
                <div class="interview_add left">
                    <div class="title_main" align="center">Обзоры</div>                    
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "interview_one",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "reviews",
                                "IBLOCK_ID" => $arReviewIB["ID"],
                                "NEWS_COUNT" => 1,
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "",
                                "FIELD_CODE" => Array("ACTIVE_TO"),
                                "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "150",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Купоны",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "kupon_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "PRICE_CODE" => "BASE"
                            ),
                        false
                    );?>
                </div>
                <div class="interview_add left">
                    <div class="title_main" align="center">Мастер-классы</div>
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "interview_one",
                            Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "master",
                                    "IBLOCK_ID" => $arMasterIB["ID"],
                                    "NEWS_COUNT" => 1,
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => Array("ACTIVE_TO"),
                                    "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "150",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Купоны",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "kupon_list",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "PRICE_CODE" => "BASE"
                            ),
                        false
                    );?>
                </div>
                <div class="interview_add left">
                    <div class="title_main" align="center">Фотоотчеты</div>
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "interview_one",
                            Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "photoreview",
                                    "IBLOCK_ID" => $arPhotoIB["ID"],
                                    "NEWS_COUNT" => 1,
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => Array("ACTIVE_TO"),
                                    "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "150",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Купоны",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "kupon_list",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "PRICE_CODE" => "BASE"
                            ),
                        false
                    );?>
                </div>
                <div class="interview_add_kupon kupons left end">
                    <div class="title_main" align="center">Акции</div>
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "kupon_one",
                            Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "kupons",
                                    "IBLOCK_ID" => $arKuponsIB["ID"],
                                    "NEWS_COUNT" => 1,
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => Array("ACTIVE_TO"),
                                    "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "150",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Купоны",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "kupon_list",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "PRICE_CODE" => "BASE"
                            ),
                        false
                    );?>
                </div>
                <div class="clear"></div>
            </div>
        </div>        
        <div id="content">        
            <?
            if (!$no){
                $del = 4;
                $ost = 0;
            }?>
    <?endif;?>
            <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-3)):?>
                <div class="left" style="width:730px">
                    <img src="/bitrix_personal/templates/kupon/images/middle_baner.png" />                    
                    <?$del = 3;
                    $ost = 1;
                    $no = 1;?>
            <?endif;?>

                <div class="new_restoraunt left<?if($key % $del == $ost):?> end<?endif?>">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />
                    <div class="interview_name">
                        <h3><?=$arItem["NAME"]?></h3>
                    </div>
                    <div class="interview_text">
                        <p><?=$arItem["PREVIEW_TEXT"]?></p>
                    </div>
                    <div class="left">Комментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)</div>
                    <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                    <div class="clear"></div>
                </div>
            <?if(($key % $del == $ost) && !(count($arResult["ITEMS"])>=20 && $key > (count($arResult["ITEMS"])-5))&&$key!=8):?>
                <div class="clear"></div>
                <div class="border_line"></div>        
            <?endif?>
    <?if ($key==8 || (count($arResult["ITEMS"])<9 && $atItem == end($arResult["ITEMS"]))):?>
            </div>
            <div class="right">
                <div id="search_article">
                    <form action="<?=$arResult["FORM_ACTION"]?>">
                        <div class="title">Найти интервью</div>
                        по автору / <a href="#" class="js another">по тэгу</a><br />
                        <div class="subscribe_block left">
                            <input class="placeholder" type="text" name="sf_EMAIL" value="<?=($arResult["EMAIL"] ? $arResult["EMAIL"] : "автор")?>" defvalue="автор" />
                        </div>
                        <div class="right">
                            <input class="subscribe" type="image" src="<?=SITE_TEMPLATE_PATH?>/images/subscribe_submit.png" name="OK" value="" title="<?=GetMessage("subscr_form_button")?>" />
                        </div>
                        <div class="clear"></div>
                    </form>            
                </div>
                <div align="right">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/right_baner1.png" />
                </div>
                <br />
                <div class="tags">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:search.tags.cloud",
                    	"interview_list",
                    	Array(
                    		"FONT_MAX" => "24",
                    		"FONT_MIN" => "12",
                    		"COLOR_NEW" => "24A6CF",
                    		"COLOR_OLD" => "24A6CF",
                    		"PERIOD_NEW_TAGS" => "",
                    		"SHOW_CHAIN" => "Y",
                    		"COLOR_TYPE" => "N",
                    		"WIDTH" => "100%",
                    		"SORT" => "NAME",
                    		"PAGE_ELEMENTS" => "20",
                    		"PERIOD" => "",
                    		"URL_SEARCH" => "/search/index.php",
                    		"TAGS_INHERIT" => "Y",
                    		"CHECK_DATES" => "N",
                    		"FILTER_NAME" => "",
                    		"CACHE_TYPE" => "A",
                    		"CACHE_TIME" => "3600",
                    		"arrFILTER" => array("iblock_interview"),
                    		"arrFILTER_iblock_interview" => array($arItem["IBLOCK_ID"])
                    	),
                    $component
                    );?>
                </div>
                <br />
                <div align="right">
                    <a href="#" class="uppercase">ВСЕ ТЕГИ</a>
                </div>
                <br />
                <div align="right">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/right_baner2.png">
                </div>
            </div>
            <div class="clear"></div>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-1)):?>
            </div>
            <div class="right">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/right_baner2.png" />
            </div>
            <div class="clear"></div>
    <?endif;?>                
    <?if (count($arResult["ITEMS"])>9 && ($key+1) == count($arResult["ITEMS"])):?>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])<9):?>
        </div></div>
    <?endif;?>
<?endforeach;?>          
<div class="clear"></div>        
<div id="content">
<hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
        </div>
        <div class="right">
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            	<?=$arResult["NAV_STRING"]?>
            <?endif;?>
        </div>
    <div class="clear"></div>
    <br /><br />
    <img src="<?=SITE_TEMPLATE_PATH?>/images/catalog/direct.jpg" />
    <br /><br />
    <img src="<?=SITE_TEMPLATE_PATH?>/images/top_baner.png" />    
    <br /><br />
    
</div>