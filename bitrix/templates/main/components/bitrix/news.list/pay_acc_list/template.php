<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="plans2" >
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="plan-<?=($key+1)?>">
            <p class="upcase-premium"><?=$arItem["NAME"]?></p>
            <p class="plan-desc"><b><?=$arItem["PRICE_FORMATED"]?></b> <span class="rouble">e</span>/<?=GetMessage("PERIOD")?></p>
            <!--<form action="order.php" method="get">
                <input type="submit" class="light_button buy" value="<?=GetMessage("PLACE")?>" /><br /><br />
                <input type="hidden" name="ID" value="<?=$arItem["ID"]?>" />
                <input type="hidden" name="resto_bind" class="REST_ID" value="" />                
            </form>-->
            <p class="plan-desc"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="180" height="145" /></p>
            <?=$arItem["PREVIEW_TEXT"]?>
        </div>
    <?endforeach;?>
</div>