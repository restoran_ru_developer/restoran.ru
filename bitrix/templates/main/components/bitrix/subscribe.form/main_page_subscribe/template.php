<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?// TODO add sections overlay?>
<?/*foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
	<label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
	<input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
	</label><br />
<?endforeach;*/?>

<form action="<?=$arResult["FORM_ACTION"]?>" id="subscribe_form">
    <div class="title" ><?=GetMessage("SUBSCR_TITLE")?></div>
    <div id="rubric_js">
        <noindex><nofollow><a href="javascript:void(0)" class="js multi12"><?=GetMessage("SUBSCR_CHOOSE_SECTION_TITLE")?></a></nofollow></noindex><br />
    </div>
    <script>
        $(document).ready(function(){
            $("#rubric_select").popup({"obj":"rubric_js","css":{'top':'-15px','right':'146px'}});
            var params = {
                changedEl: "#multi21",
                scrollArrows: true
            }
            cuSelMulti(params);
            $("#rubric_button_select").click(function(){
                /*var id = $(this).attr('filid');
                var filID = 'cuselMultiple-scroll-' + id;
                $('#' + filID + ' > span.cuselMultipleActive').each(function(index) {
                    if($(this).text() && index == 0)
                        $('.' + id).empty();
                    //if(index <= 3)
                        $('<a class="js" href="javascript:void(0)">' + $(this).text() + '</a><br />').appendTo('.' + id);
                });*/
                $("#rubric_select").fadeOut(300);
            });
            $("#subscribe_submit").click(function(){
                $(this).attr("disable",true);
                var params = $("#subscribe_form").serialize();
                $.ajax({
                    url: '/tpl/ajax/subscribe_add.php',
                    type: 'post',
                    dataType: "html",
                    data: params+"&<?=bitrix_sessid_get()?>",
                    statusCode: {
                        404:function() {
                            alert('Page not found');
                        }
                    },
                    success:function(data) {
                        $(this).attr("disable",false);
                        if (!$("#subscribe_modal").size())
                        {
                            $("<div class='popup popup_modal' id='subscribe_modal'></div>").appendTo("body");
                            setCenter($("#subscribe_modal"));
                            $("#subscribe_modal").html(data);
                        }
                        showOverflow();
                        $("#subscribe_modal").fadeIn("300");
                    }
                });
                return false;
            });
        });        
    </script>
    <div class="popup" id="rubric_select">
        <div class="title" align="right" style="width:100%;text-align: right; border:0px;"><?=GetMessage("SUBSCR_TITLE")?></div>
        <select id="multi21" name="rubric[]">
            <?foreach ($arResult["RUBRIC_LIST"] as $rubric):?>
                <option value="<?=$rubric["ID"]?>"><?=$rubric['NAME']?></option>
            <?endforeach;?>
        </select>
        <div class="clear"></div>
        <br />
        <div align="center"><input class="light_button" id="rubric_button_select" filid="multi12" type="button" value="ВЫБРАТЬ"></div>
    </div>
    <div class="subscribe_block left">
        <input class="placeholder" type="text" name="email" value="<?=($arResult["EMAIL"] ? $arResult["EMAIL"] : "e-mail")?>" defvalue="e-mail" />
    </div>
    <div class="right">
        <input class="subscribe" id="subscribe_submit" type="image" src="<?=SITE_TEMPLATE_PATH?>/images/subscribe_submit.png" name="OK" value="" title="<?=GetMessage("subscr_form_button")?>" />
    </div>
    <div class="clear"></div>
</form>