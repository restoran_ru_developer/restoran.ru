<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arResult["NEED_AUTH"] == "Y") {
	$APPLICATION->AuthForm("");
}
elseif (strlen($arResult["FatalError"]) > 0) {
	?>
	<?=$arResult["FatalError"]?>
	<?
} else {
	if(strlen($arResult["ErrorMessage"]) > 0) {
		?>
		<?=$arResult["ErrorMessage"]?>
		<?
	}

    $cntEv = count($arResult["Events"]);
	?>

    <?foreach($arResult["Events"] as $key=>$event):?>
    <tr class="<?if($key < ($cntEv - 1)):?>dotted<?else:?>solid<?endif?>">
        <td class="mail-avatar">
            <div class="avatar">
                <img src="<?=$event["USER_PERSONAL_PHOTO_FILE"]["src"]?>" />
            </div>
        </td>
        <td class="mail-who">
            <div>
                <p class="mail-autor"> <a href="#"><?=$event["USER_NAME"]?></a><br/>
                    <?=($event["IS_RESTORATOR"] ? GetMessage("USER_RESTORATOR") : "")?> </p>
                <p class="mail-date"><em><?=$event["DATE_CREATE_FORMATED_1"]?></em> <?=$event["DATE_CREATE_FORMATED_2"]?> <?=GetMessage("DATE_FORMAT_V")?> <?=$event["DATE_CREATE_FORMATED_3"]?></p>
            </div>
        </td>
        <td class="mail-text"><p><?=$event["MESSAGE"]?></p></td>
        <td class="mail-del"><p><?/*?><a href="#"><?=GetMessage("DELETE_LINK")?></a><?*/?></p></td>
    </tr>
    <?endforeach;?>
<? } ?>