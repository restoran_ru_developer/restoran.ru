<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="city_select_block" class="popup">
    <?$cities = "";?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if (CITY_ID!=$arItem["CODE"]):
            $cities .= '<li><a href="/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        else:
            $city = $arItem["NAME"];
        endif;?>
    <?endforeach;?>
    <div class="title"><?=$city?></div>
    <ul>
        <?=$cities?>
    </ul>
</div>
<ul class="city_select">
    <li id="city_select"><a href="javascript:void(0)"><?=$city?></a></li>
</ul>
