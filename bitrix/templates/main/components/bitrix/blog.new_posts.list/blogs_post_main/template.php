<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["POSTS"] as $ind => $CurPost):?>
    <div class="new_restoraunt left<?if($ind == 2):?> end<?endif?>">
        <?=$CurPost["DATE_PUBLISH_FORMATED"]?>
        <h3><?=$CurPost["TITLE"]?></h3>
        <p style="height:200px;overflow:hidden"><?=$CurPost["TEXT_FORMATED"]?></p>
        <div class="left"><i><?=($CurPost["~BLOG_USER_ALIAS"] ? $CurPost["~BLOG_USER_ALIAS"] : $CurPost["~AUTHOR_NAME"])?></i></div>
        <div class="right"><a href="#">Комментировать</a></div>
        <div class="clear"></div>
    </div>
<?endforeach?>