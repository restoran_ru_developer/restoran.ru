<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["MESSAGE"] as $itemID=>$itemValue)
	echo ShowMessage(array("MESSAGE"=>$itemValue, "TYPE"=>"OK"));
foreach($arResult["ERROR"] as $itemID=>$itemValue)
	echo ShowMessage(array("MESSAGE"=>$itemValue, "TYPE"=>"ERROR"));

if($arResult["ALLOW_ANONYMOUS"]=="N" && !$USER->IsAuthorized()):
	echo ShowMessage(array("MESSAGE"=>GetMessage("CT_BSE_AUTH_ERR"), "TYPE"=>"ERROR"));
else:
?>
<script>
    $(document).ready(function(){
       $(".subscribe_submit").click(function(){
          $(this).prev().prev().attr("checked",true);
          setTimeout("$('#subscribe_edit').submit()",50);
       });
       $(".subscribe_submit_o").click(function(){
          $(this).prev().prev().attr("checked",false);
          setTimeout("$('#subscribe_edit').submit()",50); 
       });
    });
</script>
    <div class="subscription">
        <form action="<?=$arResult["FORM_ACTION"]?>" method="post" id="subscribe_edit">
            <?echo bitrix_sessid_post();?>
            <input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
            <input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
            <input type="hidden" name="RUB_ID[]" value="0" />
            <?$p=0;?>
            <?foreach($arResult["GROUP_LIST"] as $groupName=>$groupRubric):?>
                <?if(count($groupRubric) > 1 && $groupName != $groupRubric[0]["NAME"]):?>
                    <?$sum=0;?>
                    <div class="left <?=($p%3==2)?"end":""?>" style="margin-right:15px; width:232px">
                        <p class="font18"><?=$groupName?></p>
                        <?foreach($groupRubric as $rubric):?>
                            <?$sum = $sum+$rubric["CNT_SUBSCRIBERS"]?>
                        <?endforeach?>
                        <p><i><?=$sum?> <?=pluralForm($sum, "подписчик ", "подписчика ", "подписчиков ");?></i></p>
                        <?foreach($groupRubric as $rubric):?>
                            <input type="checkbox" id="RUBRIC_<?=$rubric["ID"]?>" name="RUB_ID[]" value="<?=$rubric["ID"]?>"<?if($rubric["CHECKED"]) echo " checked"?> /><label for="RUBRIC_<?=$rubric["ID"]?>"><?=$rubric["NAME"]?></label><br/>
                        <?endforeach?>
                            <br />
                        <?if ($groupRubric[0]["CHECKED"]):?>
                            <input id="RUBRIC_<?=$rubric["ID"]?>" type="submit" class="greyb" value="Отписаться"/>
                        <?else:?>
                            <input id="RUBRIC_<?=$rubric["ID"]?>" type="submit" class="light" value="Подписаться"/>
                        <?endif;?>    
                    </div>
                    <?$p++;?>
                <?else:?>
                    <?foreach($groupRubric as $rubric):?>
                        <div class="left <?=($p%3==2)?"end":""?>" style="margin-right:15px; width:232px">
                            <p class="font18"><?=$rubric["NAME"]?><?=$cell?></p>
                            <p><i><?=$rubric["CNT_SUBSCRIBERS"]?> <?=pluralForm($rubric["CNT_SUBSCRIBERS"], "подписчик ", "подписчика ", "подписчиков ");?></i></p>
                            <?if ($rubric["CHECKED"]):?>
                                <input type="checkbox" name="RUB_ID[]" checked="checked" value="<?=$rubric["ID"]?>" style="visibility:hidden" /><Br />
                                <input id="RUBRIC_<?=$rubric["ID"]?>" type="button" class="subscribe_submit_o greyb" value="Отписаться"/>
                            <?else:?>
                                <input type="checkbox" name="RUB_ID[]" value="<?=$rubric["ID"]?>" style="visibility:hidden" /><br />
                                <input id="RUBRIC_<?=$rubric["ID"]?>" type="button" class="subscribe_submit light" value="Подписаться"/>
                            <?endif;?>
                            <!--<input type="checkbox" id="RUBRIC_<?=$rubric["ID"]?>" name="RUB_ID[]" value="<?=$rubric["ID"]?>"<?if($rubric["CHECKED"]) echo " checked"?> />-->
                        </div>
                        <?$p++;?>
                    <?endforeach?>
                <?endif?>
                <?if($p%3==0):?>
                    <div class="clear"></div>
                    <br />
                    <div class="light_hr"></div>
                <?endif?>
            <?endforeach?>
                    <div class="clear"></div>
                    <input type="hidden" name="EMAIL" value="<?echo $arResult["SUBSCRIPTION"]["EMAIL"]!=""? $arResult["SUBSCRIPTION"]["EMAIL"]: $arResult["REQUEST"]["EMAIL"];?>" class="subscription-email" /><br />
                   <input type="hidden" name="Save" value="<?echo ($arResult["ID"] > 0 ? GetMessage("CT_BSE_BTN_EDIT_SUBSCRIPTION"): GetMessage("CT_BSE_BTN_ADD_SUBSCRIPTION"))?>" />
        </form>
    </div>

    <?//v_dump($arResult["GROUP_LIST"]);?>
<?endif;?>