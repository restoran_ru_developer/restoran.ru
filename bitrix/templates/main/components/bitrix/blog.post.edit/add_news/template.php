<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;
?>
<div class="blog-post-edit">
<?
if(strlen($arResult["MESSAGE"])>0)
{
	?>
	<div class="blog-textinfo blog-note-box">
		<div class="blog-textinfo-text">
			<?=$arResult["MESSAGE"]?>
		</div>
	</div>
	<?
}
if(strlen($arResult["ERROR_MESSAGE"])>0)
{
	?>
	<div class="blog-errors blog-note-box blog-note-error">
		<div class="blog-error-text">
			<?=$arResult["ERROR_MESSAGE"]?>
		</div>
	</div>
	<?
}
if(strlen($arResult["FATAL_MESSAGE"])>0)
{
	?>
	<div class="blog-errors blog-note-box blog-note-error">
		<div class="blog-error-text">
			<?=$arResult["FATAL_MESSAGE"]?>
		</div>
	</div>
	<?
}
elseif(strlen($arResult["UTIL_MESSAGE"])>0)
{
	?>
	<div class="blog-textinfo blog-note-box">
		<div class="blog-textinfo-text">
			<?=$arResult["UTIL_MESSAGE"]?>
		</div>
	</div>
	<?
}
else
{
	// Frame with file input to ajax uploading in WYSIWYG editor dialog
	if($arResult["imageUploadFrame"] == "Y")
	{
		?>
		<script>
			<?if(!empty($arResult["Image"])):?>
			var imgTable = top.BX('blog-post-image');
			if (imgTable)
			{
				imgTable.innerHTML += '<div class="blog-post-image-item"><div class="blog-post-image-item-border"><?=$arResult["ImageModified"]?></div>' +
				'<div class="blog-post-image-item-input"><input name=IMAGE_ID_title[<?=$arResult["Image"]["ID"]?>] value="<?=Cutil::JSEscape($arResult["Image"]["TITLE'"])?>"></div>' +
				'<div><input type=checkbox name=IMAGE_ID_del[<?=$arResult["Image"]["ID"]?>] id=img_del_<?=$arResult["Image"]["ID"]?>> <label for=img_del_<?=$arResult["Image"]["ID"]?>><?=GetMessage("BLOG_DELETE")?></label></div></div>';
			}

			top.arImages.push('<?=$arResult["Image"]["ID"]?>');
			window.bxBlogImageId = top.bxBlogImageId = '<?=$arResult["Image"]["ID"]?>';
			top.bxBlogImageIdWidth = '<?=CUtil::JSEscape($arResult["Image"]["PARAMS"]["WIDTH"])?>';
			<?elseif(strlen($arResult["ERROR_MESSAGE"]) > 0):?>
				window.bxBlogImageError = top.bxBlogImageError = '<?=CUtil::JSEscape($arResult["ERROR_MESSAGE"])?>';
			<?endif;?>
		</script>
		<?
		die();
	}
	else
	{
		// TODO: Check it!
		//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/script.php");

		if($arResult["preview"] == "Y" && !empty($arResult["PostToShow"])>0)
		{
			echo "<p><b>".GetMessage("BLOG_PREVIEW_TITLE")."</b></p>";
			$className = "blog-post";
			$className .= " blog-post-first";
			$className .= " blog-post-alt";
			$className .= " blog-post-year-".$arResult["postPreview"]["DATE_PUBLISH_Y"];
			$className .= " blog-post-month-".IntVal($arResult["postPreview"]["DATE_PUBLISH_M"]);
			$className .= " blog-post-day-".IntVal($arResult["postPreview"]["DATE_PUBLISH_D"]);
			?>
			<div class="<?=$className?>">
				<h2 class="blog-post-title"><span><?=$arResult["postPreview"]["TITLE"]?></span></h2>
				<div class="blog-post-info-back blog-post-info-top">
					<div class="blog-post-info">
						<div class="blog-author"><div class="blog-author-icon"></div><?=$arResult["postPreview"]["AuthorName"]?></div>
						<div class="blog-post-date"><span class="blog-post-day"><?=$arResult["postPreview"]["DATE_PUBLISH_DATE"]?></span><span class="blog-post-time"><?=$arResult["postPreview"]["DATE_PUBLISH_TIME"]?></span><span class="blog-post-date-formated"><?=$arResult["postPreview"]["DATE_PUBLISH_FORMATED"]?></span></div>
					</div>
				</div>
				<div class="blog-post-content">
					<div class="blog-post-avatar"><?=$arResult["postPreview"]["BlogUser"]["AVATAR_img"]?></div>
					<?=$arResult["postPreview"]["textFormated"]?>
					<br clear="all" />
				</div>
				<div class="blog-post-meta">
					<div class="blog-post-info-bottom">
						<div class="blog-post-info">
							<div class="blog-author"><div class="blog-author-icon"></div><?=$arResult["postPreview"]["AuthorName"]?></div>
							<div class="blog-post-date"><span class="blog-post-day"><?=$arResult["postPreview"]["DATE_PUBLISH_DATE"]?></span><span class="blog-post-time"><?=$arResult["postPreview"]["DATE_PUBLISH_TIME"]?></span><span class="blog-post-date-formated"><?=$arResult["postPreview"]["DATE_PUBLISH_FORMATED"]?></span></div>
						</div>
					</div>
					<div class="blog-post-meta-util">
						<span class="blog-post-views-link"><a href=""><span class="blog-post-link-caption"><?=GetMessage("BLOG_VIEWS")?>:</span><span class="blog-post-link-counter">0</span></a></span>
						<span class="blog-post-comments-link"><a href=""><span class="blog-post-link-caption"><?=GetMessage("BLOG_COMMENTS")?>:</span><span class="blog-post-link-counter">0</span></a></span>
					</div>

					<?if(!empty($arResult["postPreview"]["Category"]))
					{
						?>
						<div class="blog-post-tag">
							<span><?=GetMessage("BLOG_BLOG_BLOG_CATEGORY")?></span>
							<?
							$i=0;
							foreach($arResult["postPreview"]["Category"] as $v)
							{
								if($i!=0)
									echo ",";
								?> <a href="<?=$v["urlToCategory"]?>"><?=$v["NAME"]?></a><?
								$i++;
							}
							?>
						</div>
						<?
					}
					?>
				</div>
			</div>
			<?
		}

		?>
		<form action="<?=POST_FORM_ACTION_URI?>" name="REPLIER" method="post" enctype="multipart/form-data">
		<?=bitrix_sessid_post();?>
		<div class="blog-edit-form blog-edit-post-form blog-post-edit-form">
		<div class="blog-post-fields blog-edit-fields">
			<div class="blog-post-field blog-post-field-title blog-edit-field blog-edit-field-title">
				<input maxlength="255" size="70" tabindex="1" type="text" name="POST_TITLE" id="POST_TITLE" value="<?=$arResult["PostToShow"]["TITLE"]?>">
			</div>
			<div class="blog-clear-float"></div>			
			<?if($arParams["ALLOW_POST_CODE"]):?>
				<?CUtil::InitJSCore(array('translit'));
				$bLinked = $arParams["ID"] <= 0 && $_POST["linked_state"]!=='N';
				
				?>
				<input type="hidden" name="linked_state" id="linked_state" value="<?if($bLinked) echo 'Y'; else echo 'N';?>">
				<script>
				var oldValue = '';
				var linked=<?if($bLinked) echo 'true'; else echo 'false';?>;
				
				function set_linked()
				{
					linked=!linked;
					var code_link = document.getElementById('code_link');
					if(code_link)
					{
						if(linked)
							code_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
						else
							code_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
					}
					var linked_state = document.getElementById('linked_state');
					if(linked_state)
					{
						if(linked)
							linked_state.value='Y';
						else
							linked_state.value='N';
					}
				}

				function transliterate()
				{
					if(linked)
					{
						var from = document.getElementById('POST_TITLE');
						var to = document.getElementById('CODE');
						var toText = document.getElementById('post-code-text');
						if(from && to && oldValue != from.value)
						{
							BX.translit(from.value, {
								'max_len' : 70,
								'change_case' : 'L',
								'replace_space' : '-',
								'replace_other' : '',
								'delete_repeat_replace' : true,
								'use_google' : <?echo $arParams['USE_GOOGLE_CODE'] == 'Y'? 'true': 'false'?>,
								'callback' : function(result){
										to.value = result; 
										toText.innerHTML = result;
										setTimeout('transliterate()', 250);
										}
							});
							oldValue = from.value;
						}
						else
						{
							setTimeout('transliterate()', 250);
						}
					}
					else
					{
						setTimeout('transliterate()', 250);
					}
				}
				
				function changeCode()
				{
					document.getElementById("post-code-text").style.display = "none";
					document.getElementById("post-code-input").style.display = "inline";
				}
				transliterate();
				</script>
				<div class="blog-post-field blog-post-field-code blog-edit-field blog-edit-field-code">
					<label for="CODE" class="blog-edit-field-caption"><?=GetMessage("BLOG_P_CODE")?>: </label><?=$arResult["PATH_TO_POST1"]?><a href="javascript:changeCode()" title="<?=GetMessage("BLOG_CHANGE_CODE")?>" id="post-code-text"><?=(strlen($arResult["PostToShow"]["CODE"]) > 0) ? $arResult["PostToShow"]["CODE"] : GetMessage("BLOG_P_CODE");?></a><span id="post-code-input"><input maxlength="255" size="70" tabindex="2" type="text" name="CODE" id="CODE" value="<?=$arResult["PostToShow"]["CODE"]?>"><image id="code_link" title="<?echo GetMessage("BLOG_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" /> </span><?=$arResult["PATH_TO_POST2"]?>
					
				</div>
				<div class="blog-clear-float"></div>
			<?endif;?>
			<div class="blog-post-field blog-post-field-date blog-edit-field blog-edit-field-post-date">
				<span><input type="hidden" id="DATE_PUBLISH_DEF" name="DATE_PUBLISH_DEF" value="<?=$arResult["PostToShow"]["DATE_PUBLISH"]?>">
				<div id="date-publ-text">
					<a href="javascript:changeDate()" title="<?=GetMessage("BLOG_DATE")?>"><?=$arResult["PostToShow"]["DATE_PUBLISH"]?></a>
				</div>
				<div id="date-publ" style="display:none;">
				<?
					$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'Y',
							'FORM_NAME' => 'REPLIER',
							'INPUT_NAME' => 'DATE_PUBLISH',
							'INPUT_VALUE' => $arResult["PostToShow"]["DATE_PUBLISH"],
							'SHOW_TIME' => 'N'
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);
				?>
				</div></span>
			</div>
			<div class="blog-clear-float"></div>
		</div>

		<div class="blog-post-message blog-edit-editor-area blog-edit-field-text">
			<div class="blog-comment-field blog-comment-field-bbcode">
				<?if($arResult["allow_html"] == "Y" && (($arResult["PostToShow"]["DETAIL_TEXT_TYPE"] == "html" && $_REQUEST["load_editor"] != "N") || $_REQUEST["load_editor"] == "Y"))
				{
					?>
					<input type="radio" id="blg-text-text" name="POST_MESSAGE_TYPE" value="text"<?if($arResult["PostToShow"]["DETAIL_TEXT_TYPE"] != "html" || $_REQUEST["load_editor"] == "N") echo " checked";?> onclick="window.location.href='<?=CUtil::JSEscape($APPLICATION->GetCurPageParam("load_editor=N", Array("load_editor")))?>';"> <label for="blg-text-text">Text</label> / <input type="radio" id="blg-text-html" name="POST_MESSAGE_TYPE" value="html"<?if(($arResult["PostToShow"]["DETAIL_TEXT_TYPE"] == "html" && $_REQUEST["load_editor"] != "N") || $_REQUEST["load_editor"] == "Y") echo " checked";?> onclick="window.location.href='<?=CUtil::JSEscape($APPLICATION->GetCurPageParam("load_editor=Y", Array("load_editor")))?>';"> <label for="blg-text-html">HTML</label>
				<?
				}
				
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/lhe.php");
				?>
				<div style="width:0; height:0; overflow:hidden;"><input type="text" tabindex="3" onFocus="window.oBlogLHE.SetFocus()" name="hidden_focus"></div>
			</div>
			<br />
			<div class="blog-post-field blog-post-field-images blog-edit-field" id="blog-post-image">
			<?
			if (!empty($arResult["Images"]))
			{
				?><div><?=GetMessage("BLOG_P_IMAGES")?></div><?
				foreach($arResult["Images"] as $aImg)
				{
					?>
						<div class="blog-post-image-item">
							<div class="blog-post-image-item-border"><?=$aImg["FileShow"]?></div>

								<div class="blog-post-image-item-input">
									<input name="IMAGE_ID_title[<?=$aImg["ID"]?>]" value="<?=$aImg["TITLE"]?>" title="<?=GetMessage("BLOG_BLOG_IN_IMAGES_TITLE")?>">
								</div>
								<div>
									<input type="checkbox" name="IMAGE_ID_del[<?=$aImg["ID"]?>]" id="img_del_<?=$aImg["ID"]?>"> <label for="img_del_<?=$aImg["ID"]?>"><?=GetMessage("BLOG_DELETE")?></label>
								</div>

						</div>
					<?
				}
			}
			?>
			</div>
		</div>
		<div class="blog-clear-float"></div>
<!--		<div class="blog-post-field blog-post-field-category blog-edit-field blog-edit-field-tags">
			<div class="blog-post-field-text">
			<label for="TAGS" class="blog-edit-field-caption"><?/*=GetMessage("BLOG_CATEGORY")*/?></label>
			</div>
			<span><?/*
					if(IsModuleInstalled("search"))
					{
						$arSParams = Array(
							"NAME"	=>	"TAGS",
							"VALUE"	=>	$arResult["PostToShow"]["CategoryText"],
							"arrFILTER"	=>	"blog",
							"PAGE_ELEMENTS"	=>	"10",
							"SORT_BY_CNT"	=>	"Y",
							"TEXT" => 'size="30" tabindex="4"'
							);
						if($arResult["bSoNet"] && $arResult["bGroupMode"])
						{
							$arSParams["arrFILTER"] = "socialnetwork";
							$arSParams["arrFILTER_socialnetwork"] = $arParams["SOCNET_GROUP_ID"];
						}
						$APPLICATION->IncludeComponent("bitrix:search.tags.input", ".default", $arSParams);
					}
					else
					{
						*/?><input type="text" id="TAGS" tabindex="4" name="TAGS" size="30" value="<?/*=$arResult["PostToShow"]["CategoryText"]*/?>">
						<?/*
					}*/?>
			</span>
		</div>-->
<!--		<div class="blog-clear-float"></div>
		
		<div class="blog-post-field blog-post-field-enable-comments blog-edit-field">
			<span><input name="ENABLE_COMMENTS" id="ENABLE_COMMENTS" type="checkbox" value="N"<?/*if($arResult["PostToShow"]["ENABLE_COMMENTS"] == "N") echo " checked"*/?>></span>
			<div class="blog-post-field-text"><label for="ENABLE_COMMENTS"><?/*=GetMessage("BLOG_ENABLE_COMMENTS")*/?></label></div>
		</div>-->
		<div class="blog-clear-float"></div>
		<?
        /*
        if(!$arResult["bSoNet"])
		{
			?>
			<div class="blog-post-field blog-post-field-favorite blog-edit-field">
				<span><input name="FAVORITE_SORT" id="FAVORITE_SORT" type="checkbox" value="100"<?if(IntVal($arResult["PostToShow"]["FAVORITE_SORT"]) > 0) echo " checked"?>></span>
				<div class="blog-post-field-text"><label for="FAVORITE_SORT"><?=GetMessage("BLOG_FAVORITE_SORT")?></label></div>
			</div>
			<div class="blog-clear-float"></div>
			<?
		}
        */
		?>

		<div class="blog-post-buttons blog-edit-buttons">
			<input type="hidden" name="save" value="Y">
			<input tabindex="5" type="submit" name="save" value="<?=GetMessage("BLOG_PUBLISH")?>">
			<?/*?><input type="submit" name="apply" value="<?=GetMessage("BLOG_APPLY")?>"><?*/?>
			<?/*if($arResult["perms"] >= BLOG_PERMS_WRITE):?>
				<input type="submit" name="draft" value="<?=GetMessage("BLOG_TO_DRAFT")?>">
			<?endif;*/?>
			<input type="submit" name="preview" value="<?=GetMessage("BLOG_PREVIEW")?>">
		</div>
		</div>
		</form>

		<script>
		<!--
		document.REPLIER.POST_TITLE.focus();
		//-->
		</script>
		<?
	}
}
?>
</div>