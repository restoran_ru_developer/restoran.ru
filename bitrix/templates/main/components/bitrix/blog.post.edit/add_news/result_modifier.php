<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// change path to portal post view
$arTmpPath = explode("?", $arResult["PATH_TO_POST1"]);
$arResult["PATH_TO_POST1"] = $arTmpPath[0];
// for restaurant news
// if restaurant news blog group
if($arResult["Blog"]["GROUP_ID"] == 13) {
    $arResult["PATH_TO_POST1"] = "/".CITY_ID."/news/restoransnews".CITY_ID."/";
}

// change date format
$arResult["PostToShow"]["DATE_PUBLISH"] = ConvertDateTime($arResult["PostToShow"]["DATE_PUBLISH"], "DD.MM.YYYY", "ru");
?>