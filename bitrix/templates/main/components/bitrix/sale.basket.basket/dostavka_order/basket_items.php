<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?echo ShowError($arResult["ERROR_MESSAGE"]);?>
<h2 style="margin-top:-10px;"><?=GetMessage("STB_ORDER_PROMT"); ?></h2>
<table width="100%" cellpadding="5" cellspacing="0">
	<?
	$i=0;
	foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems)
	{
		?>
		<tr>
			<td width="80">
				<div class="radius64">
					<img src="<?=$arBasketItems["PICTURE"]["src"]?>">
				</div>
			</td>
			<td width="100%"><a class="another"><?=$arBasketItems["NAME"]?></a> (<?=$arBasketItems["QUANTITY"]?>)</td>
			<td nowrap width="70">
				<span class="font18"><?=$arBasketItems["PRICE"]*$arBasketItems["QUANTITY"]?></span> <span class="rouble font18">e</span>                            
			</td>
			<td width="20"><!--<div class="dostavka_del" onclick="del_basket_item(<?=$arBasketItems["ID"]?>,'<?=bitrix_sessid_get()?>')"></div>--></td>
		</tr>
		<?if ($arBasketItems!=end($arResult["ITEMS"]["AnDelCanBuy"])):?>
		<tr>
			<td colspan="4"><div class="dotted"></div></td>
		</tr>
		<?endif;?>
		<?
		$i++;
	}
	?>
		<tr>
			<td colspan="4"><div class="black_2hr"></div></td>
		</tr>		
</table>
<?$url = str_replace("http://".SITE_SERVER_NAME, "", $_SERVER["HTTP_REFERER"]);
$url = explode("?",$url);
$url = $url[0];
?>
<table width="100%" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" class="font18"><?=GetMessage("SALE_TOTAL")?> <?=$arResult["allSum"]?> <span class="rouble font18">e</span></td>
		<td colspan="2" align="right"></td>
	</tr>
</table>