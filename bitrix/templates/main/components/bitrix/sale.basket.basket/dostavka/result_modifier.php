<?
CModule::IncludeModule("iblock");
foreach($arResult["ITEMS"]["AnDelCanBuy"] as &$arItem)
{
	$res = CIBlockElement::GetByID($arItem["PRODUCT_ID"]);
	if ($ar = $res->Fetch())
	{
		$arItem["PICTURE"] = CFile::ResizeImageGet($ar["PREVIEW_PICTURE"], array('width' => 65, 'height' => 65), BX_RESIZE_IMAGE_EXACT, true);				
	}
}
?>