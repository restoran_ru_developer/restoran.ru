<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1");
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");
?>

<?if($arResult["bDescPageNumbering"] === false):?>
        <?if(!$_REQUEST["page"] || strlen($_REQUEST["page"]) <= 0):?>
            <span class="num">&nbsp;<?=GetMessage("TOP_SPEC")?>&nbsp;</span>
        <?else:?>
            <a class="another nav" href="<?=$arResult["sUrlPath"]?>"><?=GetMessage("TOP_SPEC")?></a>
        <?endif?>
	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
		<?if($arResult["nStartPage"] == $arResult["NavPageNomer"] && strlen($_REQUEST["page"]) > 0):?>
            <span class="num">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false && strlen($_REQUEST["page"]) > 0):?>
			<a class="another nav" href=""><?=$arResult["nStartPage"]?></a>
		<?else:?>
			<a class="another nav" href="<?=$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

<?endif?>