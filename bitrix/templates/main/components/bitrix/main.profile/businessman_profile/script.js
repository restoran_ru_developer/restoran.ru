$(document).ready(function() {
    // set login from user email
    $('input[name=EMAIL]').blur(function() {
        var emailVal = $(this).val();
        $('input[name=LOGIN]').val(emailVal);
    });
});