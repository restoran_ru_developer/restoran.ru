<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<?//v_dump($arResult["arUser"])?>
<div id="content">    
    <div id="user_info" class="left">
        <table cellpadding="20" cellspacing="0">
            <tr>
                <td align="center" valign="top">
                    <div class="userpic">
                        <img src="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["src"]?>" width="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["width"]?>"  height="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["height"]?>" />
                    </div>                
                    <p id="photo"><a class="another no_border" href="#"><?=GetMessage('CHANGE_PHOTO')?></a></p>
                    <p id="personal"><a class="another no_border" href="/tpl/ajax/profile.php" onclick="show_popup(this,{'url':true,'css':{'top':'-220px','left':'177px'}});"><?=GetMessage('EDIT_DATA')?></a></p>
                    <div class="scores">
                        Мой счет<br />
                        <span>50</span> балов<br />
                        <a class="another" href="#">Потратить</a>
                    </div>
                </td>
                <td>
                    <div class="read">
                        <table cellpadding="5" class="user_params">
                            <tr>
                                <td width="160" class="name"><?=GetMessage('NAME')?></td>
                                <td class="value"><?=$arResult["arUser"]["NAME"]?> <?=$arResult["arUser"]["LAST_NAME"]?></td>
                            </tr>                    
                            <tr>
                                <td class="name"><?=GetMessage('USER_GENDER')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_GENDER"]?></td>
                            </tr>
                            <tr>
                                <td class="name"><?=GetMessage('USER_BIRTHDAY')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?></td>
                            </tr>
                            <tr>
                                <td class="name"><?=GetMessage('USER_CITY')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_CITY"]?></td>
                            </tr>                    
                            <tr>
                                <td class="name"><?=GetMessage('USER_PHONE')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_PHONE"]?></td>
                            </tr>                    
                            <tr>
                                <td class="name"><?=GetMessage('EMAIL')?></td>
                                <td class="value"><?=$arResult["arUser"]["EMAIL"]?></td>
                            </tr> 
                            <tr>
                                <td class="name"><?=GetMessage('PASSWORD')?></td>
                                <td class="value">****</td>
                            </tr>
                            <tr>
                                <td class="name"><?=GetMessage('KITCHEN')?></td>
                                <td class="value"><i><?=$arResult["arUser"]["PERSONAL_NOTES"]?></i></td>
                            </tr>
                            <tr>
                                <td class="name"><?=GetMessage('ABOUT')?></td>
                                <td class="value font12"><i><?=$arResult["arUser"]["WORK_NOTES"]?></i></td>
                            </tr>                    
                        </table>
                    </div>
                    <div class="read" style="display:none;">
                        <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
                            <?=$arResult["BX_SESSION_CHECK"]?>
                            <input type="hidden" name="lang" value="<?=LANG?>" />
                            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                            <table>
                                <tr>
                                        <td width="120" class="name"><?=GetMessage('NAME')?></td>
                                        <td class="value"><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></td>
                                </tr>
                                <tr>
                                        <td class="name"><?=GetMessage('LAST_NAME')?></td>
                                        <td class="value"><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('SECOND_NAME')?></font></td>
                                        <td><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('EMAIL')?></td>
                                        <td>
                                        <input type="text" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                                        <input type="hidden" name="LOGIN" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                                    </td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('NEW_PASSWORD_REQ')?></td>
                                        <td>
                                            <input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" />
                                            <input type="hidden" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
                                        </td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('NEW_PASSWORD_CONFIRM')?></td>
                                        <td></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('USER_PHONE')?></td>
                                        <td><input type="text" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('USER_CITY')?></td>
                                        <td><input type="text" name="PERSONAL_CITY" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></td>
                                </tr>
                                <tr>
                                    <td><?=GetMessage("USER_BIRTHDAY_DT")?> (<?=$arResult["DATE_FORMAT"]?>):</td>
                                    <td><?
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'SHOW_INPUT' => 'Y',
                                            'FORM_NAME' => 'form1',
                                            'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                            'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                            'SHOW_TIME' => 'N'
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?></td>
                                </tr>
                                <tr>
                                    <td><?=GetMessage('USER_GENDER')?></td>
                                    <td>
                                        <select name="PERSONAL_GENDER">
                                            <option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
                                            <option value="M"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_MALE")?></option>
                                            <option value="F"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?=GetMessage('PREFERENCE_IN_THE_FOOD')?></td>
                                    <td><textarea rows="5" cols="40" name="PERSONAL_NOTES"><?=$arResult["arUser"]["PERSONAL_NOTES"]?></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">&nbsp;&nbsp;<input type="reset" value="<?=GetMessage('MAIN_RESET');?>"></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </td>
            </tr>
        </table> 
    </div>
    <div class="right" style="width:240px">
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            </div>
    </div>
    <div class="clear"></div>
</div>
<div class="social-icons">
    <div class="wrap-div">
            <ul>
                    <li class="social-icon-empty">Другие аккаунты:</li>
                    <li class="social-icon-1"><a href="#">konstantin</a></li>
                    <li class="social-icon-2"><a href="#">1234567</a></li>
                    <li class="social-icon-3"><a href="#">@kostia</a></li>
                    <li class="social-icon-4"><a href="#">konstantin</a></li>
                    <li class="social-icon-5"><a href="#">konstantin</a></li>
                    <li class="social-icon-6"><a href="#">konstantin</a></li>
                    <li class="social-icon-7"><a href="#">konstantin</a></li>
                    <li class="social-icon-8"><a href="#">konstantin</a></li>
            </ul>
    </div>
</div>  
<div id="content">
    <div class="left" style="width:728px;">
        <?
        global $arrFilterTop4;
        $arrFilterTop4["CREATED_BY"] = $USER->GetID();
        $arrFilterTop4["IBLOCK_TYPE"] = Array("on_plate","cookery","reviews","comment","news","overviews","afisha","interview","photoreports","blogs");
        ?>
        <?$APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "activity",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "",
                        "IBLOCK_ID" => "",
                        "NEWS_COUNT" => 10,
                        "SORT_BY1" => "DATE_CREATE",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("ratio","reviews_bind"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?>
    </div>
    <div class="right">
         <?$APPLICATION->IncludeComponent(
            "bitrix:socialnetwork.user_friends",
            "profile_friends_list",
            Array(
                "SET_NAV_CHAIN" => "N",
                "ITEMS_COUNT" => "35",
                "PATH_TO_USER" => "",
                "PATH_TO_LOG" => "",
                "PATH_TO_USER_FRIENDS_ADD" => "",
                "PATH_TO_USER_FRIENDS_DELETE" => "",
                "PATH_TO_SEARCH" => "",
                "PAGE_VAR" => "",
                "USER_VAR" => "",
                "ID" => $arResult["ID"],
                "SET_TITLE" => "N"
            ),
            false
        );?>
        <br />
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>
        </div>
    </div>
    <div class="clear"></div>
</div>