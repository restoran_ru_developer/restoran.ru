<?
if ($arResult["arUser"]['PERSONAL_PHOTO'])
{
    $file = CFile::ResizeImageGet($arResult["arUser"]['PERSONAL_PHOTO'], array('width'=>165, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);                        
    $arResult["arUser"]['PERSONAL_PHOTO'] = $file;
}        
else
{
    $arResult["arUser"]['PERSONAL_PHOTO'] = Array("src"=>"/tpl/images/user.jpg","width"=>165,"height"=>165);
}
?>