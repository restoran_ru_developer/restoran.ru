<?
foreach ($arResult["PROPERTY_LIST_FULL"] as $keyPropertyID=>$arProperty) {
    // hook for template section view
    if($keyPropertyID == "IBLOCK_SECTION") {
        $arResult["PROPERTY_LIST_FULL"][$keyPropertyID]["ROW_COUNT"] = 1;
        $arResult["PROPERTY_LIST_FULL"][$keyPropertyID]["MULTIPLE"] = "N";
    }

    // set storage life value
    elseif($keyPropertyID == "DATE_ACTIVE_TO") {
        unset($arResult["PROPERTY_LIST_FULL"][$keyPropertyID]);
        $arDays = Array(
            1 => date("d.m.Y", strtotime("+1 days")),
            3 => date("d.m.Y", strtotime("+3 days")),
            10 => date("d.m.Y", strtotime("+10 days")),
            20 => date("d.m.Y", strtotime("+20 days")),
            30 => date("d.m.Y", strtotime("+30 days"))
        );
        $arResult["PROPERTY_LIST_FULL"]["DATE_ACTIVE_TO"] = Array(
            "PROPERTY_TYPE" => "L",
            "ROW_COUNT" => "5",
            "MULTIPLE" => "N",
            "ENUM" => Array(
                $arDays[1] => Array(
                    "VALUE" => "1 день"
                ),
                $arDays[3] => Array(
                    "VALUE" => "3 дня"
                ),
                $arDays[10] => Array(
                    "VALUE" => "10 дней"
                ),
                $arDays[20] => Array(
                    "VALUE" => "20 дней"
                ),
                $arDays[30] => Array(
                    "VALUE" => "30 дней"
                ),
            )

        );
    }
}

// set storage life to lst position
$key = array_search('DATE_ACTIVE_TO', $arResult["PROPERTY_LIST"]);
$arResult["PROPERTY_LIST"][] = $arResult["PROPERTY_LIST"][$key];
unset($arResult["PROPERTY_LIST"][$key]);
?>