<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arResult["NEED_AUTH"] == "Y")
{
	$APPLICATION->AuthForm("");
}
elseif (strlen($arResult["FatalError"])>0)
{
	?>
	<span class='errortext'><?=$arResult["FatalError"]?></span><br /><br />
	<?
}
else
{
	if(strlen($arResult["ErrorMessage"])>0)
	{
		?>
		<span class='errortext'><?=$arResult["ErrorMessage"]?></span><br /><br />
		<?
	}
	?>
	<?if ($arResult["Events"]):?>
                <h2><?=GetMessage("NEW_REQUEST")?></h2>
                <table class="friendlist">
                        <?foreach ($arResult["Events"] as $event):?>
                            <?if($cell%4 == 0):?>
                                <tr>
                            <?endif;?>
                                <td width="240">
                                    <div class="fleft">
                                        <div class="avatar">
                                            <a href="<?=$event["Event"]["USER_PROFILE_URL"]?>"><img src="<?=$event["Event"]["USER_PERSONAL_PHOTO_SRC"]["src"];?>" width="64" /></a>
                                        </div>               
                                        <a href="<?= $event["Urls"]["FriendAdd"] ?>"><?= GetMessage("SONET_C29_T_DO_FRIEND") ?></a><br>
                                        <a href="<?= $event["Urls"]["FriendReject"] ?>"><?= GetMessage("SONET_C29_T_DO_DENY") ?></a>
                                    </div>
                                    <div class="fright">
                                        <?//= GetMessage("SONET_C29_T_FRIEND_REQUEST") ?>                                                                               
                                        <p>
                                            <a href="<?=$event["Event"]["USER_PROFILE_URL"]?>"><?=$event["Event"]["USER_NAME_FORMATTED"]?></a><br/>
                                            <?if($event["Event"]["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
                                            <?=$event["Event"]["PERSONAL_CITY"]?>
                                        </p>
                                        <p class="user-mail"><a onclick="send_message(<?=$event["Event"]["USER_ID"]?>,'<?=$event["Event"]["USER_NAME_FORMATTED"]?>')" href="javascript:void(0)"><?=GetMessage("SEND_MSG")?></a></p>                                        
                                    </div>
                                </td>
                            <?$cell++;
                            if($cell%4 == 0):?>
                                </tr>
                            <?endif;?>
                        <?endforeach;?>

                        <?if($cell%4 != 0):?>
                            <?while(($cell++)%4 != 0):?>
                                <td>&nbsp;</td>
                            <?endwhile;?>
                            </tr>
                        <?endif?>                    
                </table>		
                <div class="black_2hr" style="margin-top:-1px;"></div>
	<?endif;?>
	<?
}
?>