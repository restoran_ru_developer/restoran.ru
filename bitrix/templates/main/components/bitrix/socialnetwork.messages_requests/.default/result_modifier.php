<?
foreach ($arResult["Events"] as &$event):
    if ($event["EventType"] == "FriendRequest"):                
        $arGroups = CUser::GetUserGroup($event["Event"]["USER_ID"]);
        if(in_array(RESTORATOR_GROUP, $arGroups))
            $event["Event"]["IS_RESTORATOR"] = true;
        // get user info
        $rsUser = CUser::GetByID($event["Event"]["USER_ID"]);
        $arUser = $rsUser->Fetch();
        $event["Event"]["PERSONAL_CITY"] = $arUser["PERSONAL_CITY"];
        unset($event["Event"]["USER_PERSONAL_PHOTO_FILE"]);
        if ($event["Event"]["USER_PERSONAL_PHOTO"])
        {
            
            $event["Event"]["USER_PERSONAL_PHOTO_SRC"] = CFile::ResizeImageGet($event["Event"]["USER_PERSONAL_PHOTO"], array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_EXACT, true, Array());
        }
        else
        {
            if ($arUser["PERSONAL_GENDER"]=="M")
                $event["Event"]["USER_PERSONAL_PHOTO_SRC"]["src"] = "/tpl/images/noname/man_nnm.png";
            elseif ($arUser["PERSONAL_GENDER"]=="F")
                $event["Event"]["USER_PERSONAL_PHOTO_SRC"]["src"] = "/tpl/images/noname/woman_nnm.png";
            else
                $event["Event"]["USER_PERSONAL_PHOTO_SRC"]["src"] = "/tpl/images/noname/unisx_nnm.png";
        }        
    else:
        unset($event);
    endif;
endforeach;
?>