<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(strlen($arResult["FatalError"])>0):?>
    <?=$arResult["FatalError"]?>
<?else:?>
    <?if(strlen($arResult["ErrorMessage"])>0):?>
        <?=$arResult["ErrorMessage"]?>
    <?endif?>
    <h2><?=GetMessage("FRIENDS_TITLE")?> <a class="caption-value" href="friends/"><?=$arResult["FRIENDS_COUNT"]?></a></h2>
    <div class="friends-list-right">
        <ul>
        <?foreach ($arResult["Friends"]["List"] as $cell=>$friend):?>                
            <li><a href="/users/id<?=$friend["USER_ID"]?>/"><img src="<?=$friend["USER_PERSONAL_PHOTO_FILE"]["src"]?>" height="50" alt="<?=$friend["USER_NAME"]?> <?=$friend["USER_LAST_NAME"]?>" title="<?=$friend["USER_NAME"]?> <?=$friend["USER_LAST_NAME"]?>" /></a></li>				          
        <?endforeach?>
            <?if($arResult["FRIENDS_COUNT"]>0):?>
                <li><a href="friends/"><img src="/tpl/images/friend-ava-more.gif" /></a></li>
            <?endif;?>
        </ul>
        <div class="clear"></div>
    </div>
<?endif?>