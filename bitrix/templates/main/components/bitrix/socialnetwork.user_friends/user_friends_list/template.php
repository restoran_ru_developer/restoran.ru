<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(strlen($arResult["FatalError"])>0):?>
    <?=$arResult["FatalError"]?>
<?else:?>
    <?if(strlen($arResult["ErrorMessage"])>0):?>
        <?=$arResult["ErrorMessage"]?>
    <?endif?>

    <!--<h2><?=GetMessage("FRIENDS_TITLE")?></h2>-->
    <table class="friendlist">
        <?if (count($arResult["Friends"]["List"][0])>0):?>
            <?foreach ($arResult["Friends"]["List"] as $cell=>$friend):?>
            <?if($cell%4 == 0):?>
                        <tr>
                    <?endif;?>
                <td id="friend_result_<?=$friend["USER_ID"]?>">
                    <div class="fleft">
                        <div class="avatar">
                            <img src="<?=$friend["USER_PERSONAL_PHOTO_FILE"]["src"]?>" />
                        </div>
                        <?if ($arParams["ID"]==$USER->GetID()):?>
                        <?$APPLICATION->IncludeComponent(
                            "restoran:user.friends_delete",
                            "remove_friend_from_list",
                            Array(
                                "FIRST_USER_ID" => $arParams["ID"],
                                "SECOND_USER_ID" => $friend["USER_ID"],
                                "RESULT_CONTAINER_ID" => "friend_result_".$friend["USER_ID"]
                            ),
                            false
                        );?>
                        <?endif;?>
                    </div>
                    <div class="fright">
                        <p>
                            <a href="/users/id<?=$friend["USER_ID"]?>/"><?=$friend["USER_NAME"]?> <?=$friend["USER_LAST_NAME"]?></a><br/>
                            <?if($friend["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
                            <?=$friend["PERSONAL_CITY"]?>
                        </p>
                        <p class="user-mail"><a onclick="send_message(<?=$friend["USER_ID"]?>,'<?=$friend["USER_NAME"]?> <?=$friend["USER_LAST_NAME"]?>')" href="javascript:void(0)"><?=GetMessage("SEND_MSG")?></a></p>
                        <?if (CSite::InGroup(Array(1,15,16,20))):?>
                            <p class="user-invitation"><a href="javascript:void(0)" id="invite2rest" onclick="invite2rest('USER_ID=<?=intval($friend["USER_ID"])?>&<?=bitrix_sessid_get()?>')">Пригласить в ресторан</a></p>
                        <?endif;?>
                    </div>
                </td>
            <?$cell++;
            if($cell%4 == 0):?>
                            </tr>
                    <?endif?>
            <?endforeach?>

            <?if($cell%4 != 0):?>
            <?while(($cell++)%4 != 0):?>
                <td>&nbsp;</td>
                <?endwhile;?>
            </tr>
            <?endif?>
        <?else:?>
            <tr><td><div class="errortext">
                <?if ($arParams["ID"]==$USER->GetID()):?>
                    <?=GetMessage("NO_YOU_FRIENDS")?>
                <?else:?>
                    <?=GetMessage("NO_FRIENDS")?>
                <?endif;?>
            </div></td></tr>
        <?endif;?>
    </table>

    <?if(strLen($arResult["NAV_STRING"]) > 0):?>
        <br><?=$arResult["NAV_STRING"]?><br /><br />
    <?endif;?>

<?endif?>