<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["Friends"]["List"] as $key=>$friend) {
    // check user restorator stat
    $arGroups = CUser::GetUserGroup($friend["USER_ID"]);
    if(in_array(RESTORATOR_GROUP, $arGroups))
        $arResult["Friends"]["List"][$key]["IS_RESTORATOR"] = true;

    // get user info
    $rsUser = CUser::GetByID($friend["USER_ID"]);
    $arUser = $rsUser->Fetch();
    $arResult["Friends"]["List"][$key]["PERSONAL_CITY"] = $arUser["PERSONAL_CITY"];

    // resize photo
    $arResult["Friends"]["List"][$key]["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($friend["USER_PERSONAL_PHOTO_FILE"], array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL, true);

}
?>