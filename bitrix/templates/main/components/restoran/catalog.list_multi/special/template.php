<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="content">
            <div class="left" style="width:730px;position:relative">
                <h1>
                        <?=$APPLICATION->GetTitle("h1");?>
                </h1>
                <div id="spec_block">
                    <table class="profile-activity" cellpadding="10">
                    <?foreach($arResult["ITEMS"] as $key=>$arItem):?> 
                        <tr>
                                <td class="profile-activity-col1">
                                    <img src="<?=CFile::GetPath($arItem["IBLOCK_PICTURE"])?>" width="121" />                        
                                </td>
                                <td class="profile-activity-col11">
                                    <p><a class="another font14" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["IBLOCK_NAME"]." в ресторане «".$arItem["NAME"]."»"?></a></p>                    
                                    <i><?=$arItem["PREVIEW_TEXT"]?></i>
                                </td>                                
                        </tr>    
                    <?endforeach;?>           
                    </table>
                <!--    <p class="more-activity"><a href="javascript:void(0)"><?=GetMessage("MORE_ACTIVITY")?></a></p>-->
                </div>
            </div>
        <div class="right">
        <!--<div id="search_article">
            <form action="<?=$arResult["FORM_ACTION"]?>">
                <div class="title">Найти интервью</div>
                по автору / <a href="#" class="js another">по тэгу</a><br />
                <div class="subscribe_block left">
                    <input class="placeholder" type="text" name="sf_EMAIL" value="<?=($arResult["EMAIL"] ? $arResult["EMAIL"] : "автор")?>" defvalue="автор" />
                </div>
                <div class="right">
                    <input class="subscribe" type="image" src="<?=SITE_TEMPLATE_PATH?>/images/subscribe_submit.png" name="OK" value="" title="<?=GetMessage("subscr_form_button")?>" />
                </div>
                <div class="clear"></div>
            </form>            
        </div>-->
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <br />                          
        <div id="subscribe">                          
        </div>                                    
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    </div>
    <div class="clear"></div>
</div>