<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["RESUME_STATUS"] = "Статус";
$MESS["RESUME_STATUS_Y"] = "Открыта";
$MESS["RESUME_STATUS_N"] = "Закрыта";
$MESS["RESPOND_TITLE"] = "Откликнулись";
$MESS["RESTORATOR_STATE"] = "Ресторатор";
?>