<?
CModule::IncludeModule("iblock");
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    $ra = CUser::GetByID($arItem["CREATED_BY"]);
    if ($u = $ra->Fetch())
    {
        if ($u["LAST_NAME"]||$u["NAME"])
            $arItem["USER_NAME"] = $u["LAST_NAME"]." ".$u["NAME"];
        else
            $arItem["USER_NAME"] = $u["PERSONAL_PROFESSION"];
    }
    $res = CIblockElement::GetByID($arItem["PROPERTIES2"]["RESTORAN"][0]);
    if ($ar = $res->GetNext())
    {
        $arItem["RESTORAN"] = array();
        $arItem["RESTORAN"]["NAME"] = $ar["NAME"];
        $arItem["RESTORAN"]["PICTURE"] = CFile::GetPath($ar["PREVIEW_PICTURE"]);
        $arItem["RESTORAN"]["URL"] = $ar["DETAIL_PAGE_URL"];
        $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"RATIO"));
        if($ar_props = $db_props->Fetch())
            $arItem["RESTORAN"]["RATIO"] = (int)$ar_props["VALUE"];
        $rsSection = CIBlockSection::GetByID($ar["IBLOCK_SECTION_ID"]);
        if ($arSection = $rsSection->Fetch())
            $arItem["RESTORAN"]["SECTION"] = $arSection["CODE"];
    }
    $temp = explode(" ",$arItem["ACTIVE_FROM"]);
    $arItem["DATE"] = $temp[0];
    $arItem["TIME"] = substr($temp[1],0,5);
    if ($arItem["PROPERTIES"]["USERS"][0])
    {
        foreach ($arItem["PROPERTIES"]["USERS"] as  $key=>$user)
        {
            $res = CUser::GetByID($user);
            if ($ar = $res->Fetch())
            {
                $photo = CFile::ResizeImageGet($ar["PERSONAL_PHOTO"],Array("width"=>70,"height"=>70),BX_RESIZE_IMAGE_EXACT,true);
                if (!$photo["src"]&&$ar["PERSONAL_GENDER"]=="M")
                    $photo["src"] = "/tpl/images/noname/man_nnm.png";
                elseif (!$photo["src"]&&$ar["PERSONAL_GENDER"]=="F")
                    $photo["src"] = "/tpl/images/noname/woman_nnm.png";
                elseif (!$photo["src"]&&!$ar["PERSONAL_GENDER"])
                    $photo["src"] = "/tpl/images/noname/unisx_nnm.png";
                $arItem["USERS"][] = Array(
                    "ID" => $ar["ID"],
                    "NAME" => $ar["LAST_NAME"]." ".$ar["NAME"],
                    "PHOTO" => $photo,
                    "CITY" => $ar["PERSONAL_CITY"],
                    "ACCEPT" => $arItem["DESCRIPTION"]["USERS"][$key],
                    "PROPERTY_ID" => $arItem["PROPERTY_VALUE_ID"]["USERS"][$key]
                );
                if ($ar["ID"]==$USER->GetID())
                    $arItem["MY_USER"] = Array(
                    "ID" => $ar["ID"],
                    "NAME" => $ar["LAST_NAME"]." ".$ar["NAME"],
                    "PHOTO" => $photo,
                    "CITY" => $ar["PERSONAL_CITY"],
                    "ACCEPT" => $arItem["DESCRIPTION"]["USERS"][$key],
                    "PROPERTY_ID" => $arItem["PROPERTY_VALUE_ID"]["USERS"][$key]
                );
            }
        }
    }
    if ($arItem["PROPERTIES"]["NA_USERS"][0])
    {
        foreach ($arItem["PROPERTIES"]["NA_USERS"] as  $key=>$user)
        {

                $arItem["NA_USERS"][] = Array(
                    "NAME" => $user,
                    "ACCEPT" => $arItem["DESCRIPTION"]["NA_USERS"][$key],
                    "PROPERTY_ID" => $arItem["PROPERTY_VALUE_ID"]["USERS"][$key]
                );
        }
    }
}
?>