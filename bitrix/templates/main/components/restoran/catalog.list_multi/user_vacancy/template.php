<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult["ITEMS"])>0){
?>
<table class="profile-activity w100" cellpadding="0" cellspacing="0">
	<tr>
		<th>Должность</th>
		<th>Город</th>
		<th>Образование</th>
		<th>Опыт</th>
		<th>График</th>
		<th>Заработная плата</th>
		<th>Откликнулись</th>
		<th>Статус</th>
		<th class="w-auto">&nbsp;</th>
	</tr>
<?foreach($arResult["ITEMS"] as $arItem):
        //v_dump($arItem);
?>		
	<tr>
            
		<td valign="top"><p><?=$arItem["NAME"]?></p></td>
		<td valign="top"><p><?=$arItem["CITY"]?></p>
                    <p><?=$arItem["PROPERTIES"]["ABILITY_TO_MOVE"]["VALUE"]?></p>
                </td>
                <?/*foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<td valign="top"><p>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</p></td>
		<?endforeach;*/?>  
		<td valign="top"><p><?=$arItem["PROPERTIES"]["EDUCATION"]["VALUE"]?></p></td>
                <td valign="top"><p><?=$arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"]?></p></td>
                <td valign="top"><p><?= implode('<br>', $arItem["PROPERTIES"]["SHEDULE"]["VALUE"]) ?></p></td>
                <td valign="top"><p><?= $arItem["PROPERTIES"]["WAGES_OF"]["VALUE"] ?></p></td>
		<td class="user-friends nofloat">
                    <ul>
                        <?foreach($arItem["RESPOND_USERS_REG"] as $respUserReg):?>
                            <li>
                                <img src="<?=$respUserReg["PERSONAL_PHOTO"]["SRC"]?>" width="70" />
                                <div class="user-friend">
                                        <a href="/users/id<?=$respUserReg["ID"]?>/"><?=$respUserReg["FULL_NAME"]?></a><br/>
                                        <?if($respUserReg["IS_RESTORATOR"]):?>
                                            <?=GetMessage("RESTORATOR_STATE")?><br />
                                        <?endif?>
                                        <?=$respUserReg["PERSONAL_CITY"]?>
                                        <p class="user-mail"><a href="#">Сообщение</a></p>
                                </div>
                            </li>
                        <?endforeach?>
                        <?foreach($arItem["RESPOND_USERS_UNREG"] as $respUserUnreg):?>
                            <li class="<?=(end($arItem["RESPOND_USERS_UNREG"])==$respUserUnreg)?"nopic nobg":""?>">
                                <div class="user-friend">
                                        <p><?=$respUserUnreg["EMAIL"]?><br/>
                                        <?=$respUserUnreg["FIO"]?></p>
                                </div>
                            </li>
                        <?endforeach?>
                        <!--<p class="more-activity" style="margin-top:12px; margin-bottom:0;"><a href="#">Еще 3</a></p>-->
                    </ul>
                </td>
                <td class="status"  valign="top">
                    <div class="activity-actions w-small">
                        <div>
                            <p class="block-expand" align="center"><?=GetMessage("RESUME_STATUS_".$arItem["ACTIVE"])?></p>
                            <a class="razmes" href="/users/id<?=$_REQUEST["USER_ID"]?>/work/vacancy_edit/<?=$arItem["ID"]?>/">Изменить</a>
                            <a class="icon-delete" href="#" vacancy-id="<?= $arItem["ID"] ?>" type="vacancy">Удалить</a>
                            <div class="clear"></div>
                        </div>
                    </div>
		</td>
        </tr>	
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</table>
<?}else{?>
Нет добавленных вакансий
<?}?>