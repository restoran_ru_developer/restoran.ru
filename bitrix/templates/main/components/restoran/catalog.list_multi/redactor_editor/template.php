<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $arrFilter;

//нужно запросить разделы
if($arParams["IBLOCK_ID"]>0){
	$properties = CIBlockProperty::GetList(Array("name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>"cat"));
	while ($prop_fields = $properties->GetNext()){
  		if($prop_fields["PROPERTY_TYPE"]=="E"){
  			$arFilter_prl = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"]);
			$res_prl = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_prl, false);
			while($ob_prl = $res_prl->GetNext()){
				$arResult["RAZDELS"][]=array("ID"=>$ob_prl['ID'], "NAME"=>$ob_prl['NAME']);
			}
  		}
	}
	
	if(count($arResult["RAZDELS"])==0){
		$arFilter2 = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y');
	 	$db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), $arFilter2, true);
  		while($ob_prl = $db_list->GetNext()){
			$arResult["RAZDELS"][]=array("ID"=>$ob_prl['ID'], "NAME"=>$ob_prl['NAME']);
		}
	}
	
}else{
	$res = CIBlock::GetList(
		Array(), 
		Array(
			'TYPE'=>$arParams["IBLOCK_TYPE"], 
			'SITE_ID'=>SITE_ID, 
			'ACTIVE'=>'Y', 
			"CNT_ACTIVE"=>"Y",
			"CHECK_PERMISSIONS"=>"Y",
			"MIN_PERMISSION"=>"W"
		), true
	);
	while($ar_res = $res->Fetch()){
		$arResult["CITYS"][]=array("ID"=>$ar_res['ID'], "NAME"=>$ar_res['NAME']);
	}
} 

//var_dump($arResult["RAZDELS"]);


$arrFilter["NAME"] = str_replace("%", "", $arrFilter["NAME"]);


if(is_array($arParams["IBLOCK_ID"]) || $arParams["ADD2CITY"]) $AT=$arParams["ADD2CITY"];
else $AT=$arParams["IBLOCK_ID"];


?>

<?if($arParams["REDACTOR"]=="Y"){?>
<div class="blog-items">
<?if(count($arResult["RAZDELS"])>0){?>
<div class="fltr">
	<select name="rzdel">
		<option value="">Все</option>
		<?foreach($arResult["RAZDELS"] as $R){?>
		<option value="<?=$R["ID"]?>" <? if($R["ID"]==$_REQUEST["RAZDEL"]) echo 'selected="selected"';?>><?=$R["NAME"]?></option>
		<?}?>
	</select>
    <div class="uppercase left" style="font-size:10px; line-height:24px;">Поиск: </div>
    <input type="text" name="search_by_name" class="pole" value="<?=$arrFilter["NAME"]?>" id="search_by_name" />
</div>
<?}else{?>

<?if(count($arResult["CITYS"])>0){?>

<div class="fltr">
	<select name="city">
		<option value="">Все</option>
		<?foreach($arResult["CITYS"] as $R){?>
		<option value="<?=$R["ID"]?>" <? if($R["ID"]==$_REQUEST["RAZDEL"]) echo 'selected="selected"';?>><?=$R["NAME"]?></option>
		<?}?>
	</select>
    <div class="uppercase left" style="font-size:10px; line-height:24px;">Поиск: </div>
	<input type="text" name="search_by_name" class="pole" value="<?=$arrFilter["NAME"]?>" id="search_by_name" />
</div>
<?}else{?>
	<div class="fltr">
            <div class="uppercase left" style="font-size:10px; line-height:24px;">Поиск: </div>
		<input type="text" name="search_by_name" class="pole" value="<?=$arrFilter["NAME"]?>" id="search_by_name"/>
	
	</div>
<?}}?>

<a  class="light_button" href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?IBLOCK_TYPE=<?=$arParams["IBLOCK_TYPE"]?>&IB=<?=$AT?>&SECTION=<?=$arParams["SECTION_ID"]?>">+ Новая публикация</a>
<?}?>
<ul>
<?
foreach($arResult["ITEMS"] as $arElement):?>
	<?
	//var_dump($arElement);

	$arElement["DATE_CREATE"] = CIBlockFormatProperties::DateFormat("<\e\m>j</\e\m> F Y", MakeTimeStamp($arElement["DATE_CREATE"], CSite::GetDateFormat()));
	
	
	//var_dump(time());
	?>
	<li class="blog-item <? if($arElement["ACTIVE"]!="Y" || ($arElement["ACTIVE_TO"]!="" && $arElement["ACTIVE_FROM"]!="" && MakeTimeStamp($arElement["ACTIVE_TO"], "DD.MM.YYYY")<=time())) echo 'not_active';?>">
				<p class="blog-top">
				<?
				//var_dump($arElement["ACTIVE"]);
				if($arElement["IBLOCK_SECTION_NAME"]==""){
					$res = CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
					if($ar_res = $res->GetNext())
				  		$arElement["IBLOCK_SECTION_NAME"]=$ar_res["NAME"];
				}
				?>
				<?if($arParams["NOT_SHOW_PARENT"]!="Y" && $arElement["IBLOCK_SECTION_NAME"]!="" && $arElement["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]==""){?>
					<strong><?=$arElement["IBLOCK_SECTION_NAME"]?></strong>, 
				<?}?>
				<?if($arElement["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]!=""){?>
					<strong><?=strip_tags($arElement["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"])?></strong>,
				<?}?>
				<?
				$arElement["ACTIVE_FROM"] =strval($arElement["ACTIVE_FROM"]);
				if($arElement["ACTIVE_FROM"]!="") 
					$arElement["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("<\e\m>j</\e\m> F Y", MakeTimeStamp($arElement["ACTIVE_FROM"], CSite::GetDateFormat()));
		
				if(!empty($arElement["ACTIVE_FROM"])) echo $arElement["ACTIVE_FROM"];
				else echo $arElement["DATE_CREATE"];
				?>
				</p>
				<p class="blog-caption"><?=$arElement["NAME"]?></p>
				<p class="blog-text"><?=$arElement["~PREVIEW_TEXT"]?></p>
				
				<p class="blog-info">
					<span class="blog-comments">Комментарии: (<a href="<?=$arElement["DETAIL_PAGE_URL"]?>#comments" target="_blank"><?=intval($arElement["PROPERTIES"]["comments"]["VALUE"])?></a>)</span>
					<span class="blog-tags">Теги: 
					<?if($arElement["TAGS"]!=""){?>
						<?
						$tar = explode(",", $arElement["TAGS"]);
						$i=0;
						?>
						<?foreach($tar as $t){?><?=$t?><?
						if($tar[$i+1]!="") echo ", ";
						$i++;
						?> 
						
						<?}?>
						
					<?}?>
					</span>
					<span class="blog-more"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>" target="_blank">Далее</a></span>
				</p>
				<ul class="blog-actions">
					<?if($arParams["REDACTOR"]=="Y"){?>
						<li class="blog-rborder"><a href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?IB=<?=$arElement["IBLOCK_ID"]?>&SECTION=<?=$arParams["SECTION_ID"]?>&ID=<?=$arElement["ID"]?>">Редактировать</a></li>
						<li><a href="<?=$templateFolder?>/core.php?act=del_item&ID=<?=$arElement["ID"]?>" rel="nofollow" class="del_item">Удалить</a></li>	
					<?}?>		
		</ul>
	<?
	//v_dump($arElement);
	?>
	</li>
	
<?endforeach?>
</ul>
<div class="pager">
	<?=$arResult["NAV_STRING"]?>
</div>
</div>