<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<?
if (CITY_ID=="tmn")
    $anons = Array("overviews", "photoreports", "cookery", "news","interview");
elseif (CITY_ID=="ufa"||CITY_ID=="kld")
    $anons = Array();
else
    $anons = Array("overviews","photoreports","cookery","news","interview");
$iblock_type = Array($arResult["ITEMS"][0]["IBLOCK_TYPE_ID"]);
$diff = array_diff($anons,$iblock_type);
$p=0;
foreach ($diff as $d)
{
    if ($p<4)
        $dif[] = $d;
    $p++;
}
foreach ($dif as $as)
{
    $arAnons[] = getArIblock($as, CITY_ID);
}
?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <?if ($key==0):?>
        <div id="content">
            <div class="left" style="width:730p;position:relative">
                <h1>
                        <?=$APPLICATION->GetTitle("h1");?>
                </h1>
                <?
                $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE","clear_cache");
                $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=created_date&".(($_REQUEST["pageSort"]=="created_date"&&$_REQUEST["by"]=="desc"||!$_REQUEST["pageSort"]) ? "by=asc": "by=desc"), $excUrlParams);                
                $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=PROPERTY_summa_golosov&".(($_REQUEST["pageSort"]=="PROPERTY_summa_golosov"&&$_REQUEST["by"]=="asc"||!$_REQUEST["pageSort"]) ? "by=desc": "by=asc"), $excUrlParams);
                ?>                
                <div class="sorting" style="top:0px; width:280px; left:320px">
                    <div class="left">
                        <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                        <?$by = ($_REQUEST["by"]=="asc")?"a":"z";?>
                        <?if ($_REQUEST["pageSort"] == "created_date" || !$_REQUEST["pageSort"]):                                                   
                            echo "<span class='".$by."'><a class='another' href='".$sortNewPage."'>".GetMessage("SORT_NEW_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a></span>';
                        endif;?>
                         / 
                        <?if ($_REQUEST["pageSort"] == "PROPERTY_summa_golosov"):
                            echo "<span class='".$by."'><a class='another' href='".$sortPopularPage."'>".GetMessage("SORT_POPULAR_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortPopularPage.'">'.GetMessage("SORT_POPULAR_TITLE").'</a></span>';
                        endif;?>
                    </div>
                    <div class="right"><?//=$arResult["NAV_STRING"]?></div>                    
                    <div class="clear"></div>
                </div>
                <div style="width:280px; right:0px; position:absolute; top:0px; line-height: 40px; text-align:right">
                    <noindex>
                        <?if ($_REQUEST["blog"]=="all"):?>
                            <?if (CITY_ID=="msk"):?>
                                <a class="another" href="/<?=CITY_ID?>/blogs/">только Москва</a> 
                            <?elseif(CITY_ID=="spb"):?>
                                <a class="another" href="/<?=CITY_ID?>/blogs/">только Петербург</a> 
                            <?elseif(CITY_ID=="anp"):?>
                                <a class="another" href="/<?=CITY_ID?>/blogs/">только Анапа</a> 
                            <?elseif(CITY_ID=="sch"):?>
                                <a class="another" href="/<?=CITY_ID?>/blogs/">только Сочи</a> 
                            <?elseif(CITY_ID=="krd"):?>
                                <a class="another" href="/<?=CITY_ID?>/blogs/">только Краснодар</a> 
                                <?elseif(CITY_ID=="kld"):?>
                                <a class="another" href="/<?=CITY_ID?>/blogs/">только Калининград</a> 
                            <?endif;?>
                            / 
                            по всем городам
                        <?else:?>
                            только 
                            <?if (CITY_ID=="msk"):?>
                                Москва
                            <?elseif(CITY_ID=="spb"):?>
                                Петербург
                            <?elseif(CITY_ID=="anp"):?>
                                Анапа
                            <?elseif(CITY_ID=="sch"):?>
                                Сочи
                            <?elseif(CITY_ID=="krd"):?>
                                Краснодар
                            <?endif;?>
                            / <a class="another" href="/all/blogs/">по всем городам</a>
                        <?endif;?>
                    </noindex>
                </div>
                <?$del = 3;
                $ost = 2?>
    <?endif;?>
    <?if ($key==9):?>
        <div id="additional_info_interview">
            <div class="block">
                <?foreach($arAnons as $key=>$anons):?>
                    <div class="interview_add left">
                        <div class="title_main" align="center"><?=GetMessage("ANONS_".$dif[$key])?></div>  
                        <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "interview_one_with_border",
                            Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $dif[$key],
                                    "IBLOCK_ID" => ($dif[$key]=="cookery")?145:$anons["ID"],
                                    "NEWS_COUNT" => 1,
                                    "SORT_BY1" => "ID",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "",
                                    "SORT_ORDER2" => "",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("ratio","COMMENTS"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "search_rest_list",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );?>
                    </div>
                <?endforeach;?>
                <div class="clear"></div>
            </div>
        </div>        
        <div id="content">        
            <?
            if (!$no){
                $del = 4;
                $ost = 0;
            }?>
    <?endif;?>
            <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-3)):?>
            <div class="clear"></div>
                <div class="left" style="width:730px">
                     <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "content_article_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>                 
                    <?$del = 3;
                    $ost = 1;
                    $no = 1;?>
            <?endif;?>

                <div class="new_restoraunt left<?if($key % $del == $ost):?> end<?endif?>">
                    <div style="margin-bottom:10px;">
                        <a href="/users/id<?=$arItem["CREATED_BY"]?>/" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
                    </div>
                    <?if ($arItem["VIDEO"]):?>
                        <a class="myPlayer" href="<?=$arItem["VIDEO"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                },
                                plugins: {
                                    controls: {
                                        all: false,
                                        play: true,
                                        scrubber: true,
                                        tooltips: {
                                            buttons: true,
                                            fullscreen: 'Enter fullscreen mode'
                                        }
                                    }
                                }
                            });
                        </script>
                    <?elseif ($arItem["VIDEO_PLAYER"]):?>  
                            <?=$arItem["VIDEO_PLAYER"]?>
                    <?else:?>
                        <!--<div style="overflow:hidden; height:127px">-->
                        <?if($arItem["PREVIEW_PICTURE"]["src"]):?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>
                        <?endif;?>
                        <!--</div>-->
                    <?endif;?>
                    <div class="title">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a>
                    </div>
                    <p><?=$arItem["PREVIEW_TEXT"]?></p>
                    <div class="left">Комментарии: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
                    <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                    <div class="clear"></div>
                </div>
            <?if(($key % $del == $ost) && !(count($arResult["ITEMS"])>=20 && $key > (count($arResult["ITEMS"])-5))&&$key!=8):?>
                <div class="clear"></div>
                <div class="border_line"></div>        
            <?endif?>
    <?if ($key==8 || (count($arResult["ITEMS"])<9 && $arItem == end($arResult["ITEMS"]))):?>
            </div>
            <div class="right">
                <!--<div id="search_article">
                    <form action="<?=$arResult["FORM_ACTION"]?>">
                        <div class="title">Найти интервью</div>
                        по автору / <a href="#" class="js another">по тэгу</a><br />
                        <div class="subscribe_block left">
                            <input class="placeholder" type="text" name="sf_EMAIL" value="<?=($arResult["EMAIL"] ? $arResult["EMAIL"] : "автор")?>" defvalue="автор" />
                        </div>
                        <div class="right">
                            <input class="subscribe" type="image" src="<?=SITE_TEMPLATE_PATH?>/images/subscribe_submit.png" name="OK" value="" title="<?=GetMessage("subscr_form_button")?>" />
                        </div>
                        <div class="clear"></div>
                    </form>            
                </div>-->
                <div align="right">
                    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                </div>
                <br />                          
                <div id="subscribe">                          
                </div>                
                <?/*if ($USER->IsAuthorized()):?>
                <div class="figure_border">
                    <div class="top"></div>
                    <div class="center">
                        <div id="users_onpage"></div>
                    </div>
                    <div class="bottom"></div>
                </div>
                <br /><br />
                <?endif;*/?>
                <div class="tags">
                    <?
                    if (CITY_ID=="msk"||CITY_ID=="spb")
                        $ci = Array(156,157);
                    else
                        $ci =  Array($arItem["IBLOCK_ID"]);
                    
                    $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
					"SORT" => "CNT",
					"PAGE_ELEMENTS" => "20",
					"PERIOD" => "",
					"URL_SEARCH" => "/search/index.php",
					"TAGS_INHERIT" => "Y",
					"CHECK_DATES" => "Y",
					"FILTER_NAME" => "",
					"arrFILTER" => array(
							0 => "iblock_".$arItem["IBLOCK_TYPE_ID"],
					),
					"arrFILTER_iblock_".$arItem["IBLOCK_TYPE_ID"] => $ci,
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"FONT_MAX" => "24",
					"FONT_MIN" => "12",
					"COLOR_NEW" => "24A6CF",
					"COLOR_OLD" => "24A6CF",
					"PERIOD_NEW_TAGS" => "",
					"SHOW_CHAIN" => "Y",
					"COLOR_TYPE" => "N",
                                        "SEARCH_IN" => $arItem["IBLOCK_TYPE_ID"],
					"WIDTH" => "100%"
					),
					$component
			);?> 
                   
                </div>                
                <div align="right">
                    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-1)):?>
            </div>
            <div class="right">
                 <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
            </div>
            <div class="clear"></div>
    <?endif;?>                
    <?if (count($arResult["ITEMS"])>9 && ($key+1) == count($arResult["ITEMS"])):?>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])<9 && end($arResult["ITEMS"])==$arItem):?>
        <!--</div></div>-->
    <?endif;?>
<?endforeach;?>          
<div class="clear"></div>        
<div id="content">
    <?if ($arResult["NAV_STRING"]):?>
        <hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
        </div>
        <div class="right">            
                <?=$arResult["NAV_STRING"]?>            
        </div>
        <div class="clear"></div>
    <?endif;?>
    <br /><br />
    <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
    <br /><br />
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
    );?>  
    <br /><br />
</div>
<div class="clear"></div>