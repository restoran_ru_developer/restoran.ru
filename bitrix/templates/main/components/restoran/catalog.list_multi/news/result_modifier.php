<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
    {
        if ($arUser["PERSONAL_PROFESSION"])
            $arItem["AUTHOR_NAME"] = $arUser["PERSONAL_PROFESSION"];
        else
        {
            if (!$arUser["NAME"])
                $arItem["AUTHOR_NAME"] = $arUser["LAST_NAME"];
            else
                $arItem["AUTHOR_NAME"] = $arUser["NAME"];
        }
    }

    // format date
   // if (!$arItem["DISPLAY_ACTIVE_FROM"]):
        $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    //else:
     //   $arTmpDate = $arItem["DISPLAY_ACTIVE_FROM"];
    //endif;    
    $arTmpDate = explode(" ", $arTmpDate);
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
    
    /*if ($arParams["IBLOCK_TYPE"]=="videonews")
    {
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$arItem["ID"],"PROPERTY_BLOCK_TYPE_VALUE"=>"Видео"),false,false,Array("PROPERTY_VIDEO"));
        if ($ar = $res->GetNext())
            $arItem["VIDEO"] = $ar["PROPERTY_VIDEO_VALUE"];        
    }*/
    $arItem["DETAIL_PAGE_URL"] = str_replace("#CITY_ID#", CITY_ID, $arItem["DETAIL_PAGE_URL"]);
    if ($arItem['IBLOCK_TYPE_ID']=="videonews"):
        preg_match("/#FID_([0-9]+)#/",$arItem["DETAIL_TEXT"],$video1);
        if ($video1[1])
            $arResult["ITEMS"][$key]["VIDEO"] = "http://".SITE_SERVER_NAME."".CFile::GetPath($video1[1]);
        preg_match("/<iframe ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\"/",$arItem["DETAIL_TEXT"],$video2);
        if ($video2[1])
        {
            for ($i=1;$i<count($video2);$i++)
            {
                if ($video2[$i]=="width")
                    $width = $video2[++$i];
                elseif ($video2[$i]=="height")
                    $height = $video2[++$i];
                elseif($video2[$i]=="src")
                    $src = $video2[++$i];                
            }
        }
        if ($width&&$height&&$src)
        {            
            $height = 232*$height/$width;
            $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$src.'" width="232" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        }
    endif;
}
?>