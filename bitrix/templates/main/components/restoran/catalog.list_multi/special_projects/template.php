<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="spec_block">
    <table class="profile-activity">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?> 
        <tr>
                <td class="profile-activity-col1">
                    <img src="<?=CFile::GetPath($arItem["IBLOCK_PICTURE"])?>" width="121" />                        
                </td>
                <td class="profile-activity-col11">
                    <p><a class="another font14" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["IBLOCK_NAME"]." в ресторане «".$arItem["NAME"]."»"?></a></p>                    
                    <i><?=$arItem["PREVIEW_TEXT"]?></i>
                </td>                                
        </tr>    
    <?endforeach;?>           
    </table>    
    
    <div align="right"><a class="uppercase" href="/<?=CITY_ID?>/detailed/restaurants/<?=$_REQUEST["code"]?>/special/">ВСЕ СОБЫТИЯ ресторана</a></div>
</div>