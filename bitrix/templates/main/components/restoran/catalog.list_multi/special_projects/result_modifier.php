<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("iblock");
foreach($arResult["ITEMS"] as $key=>&$arItem) 
{
    $res = CIBlock::GetByID($arItem["IBLOCK_ID"]);
    if ($ar = $res->Fetch())
    {        
        $arItem["IBLOCK_PICTURE"] = $ar["PICTURE"];
        $a = explode(" -",$ar["NAME"]);
        $arItem["IBLOCK_NAME"] = $a[0];
        $a = explode("&lt;",$arItem["NAME"]);
        $arItem["NAME"] = $a[0];
    }    
}
?>