<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if ($arResult["bSoNet"] && $arResult["BlogUser"]["AVATAR_file"] !== false)
{
	unset($arResult["BlogUser"]["AVATAR_img"]);
}

// get restaurant by owner id
if(CModule::IncludeModule("iblock")) {
    $rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "PROPERTY_user_bind" => Array($arResult["Blog"]["OWNER_ID"])
        ),
        false,
        false,
        Array()
    );
    if($arRest = $rsRest->Fetch())
        $arResult["RESTAURANT_NAME"] = $arRest["NAME"];
}
?>