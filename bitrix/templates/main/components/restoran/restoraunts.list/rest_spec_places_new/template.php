<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="priorities">
    <?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
            <div class="priority_img left">
                <?if(count($arItem["PROPERTIES"]["photos"]["VALUE"])>1):?>
                    <script type="text/javascript">
                            $(document).ready(function(){
                                $("#gallery<?=$arItem["ID"]?>").galery({});
                                $(".scroll_container").find("img").each(function(){
                                    if (!$(this).hasClass("active"))
                                    {

                                        $(this).attr("src",$(this).attr("data-href"));
                                    }
                                });
                            });                            
                    </script>
                    <div id="gallery<?=$arItem["ID"]?>">
                        <div class="special_scroll">
                            <div class="scroll_container">
                                <?//if ($USER->IsAdmin()):?>
                                    <?foreach($arItem["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photo):?>
                                            <div class="item<?if($pKey == 0):?> active<?endif?>">
                                                <img <?=($pKey == 0)?'src="'.$photo["src"].'"':'data-href="'.$photo["src"].'"'?> align="bottom" />
                                            </div>
                                    <?endforeach?>  
                                <?/*else:?>
                                    <?foreach($arItem["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photo):?>
                                        <div class="item<?if($pKey == 0):?> active<?endif?>">
                                            <img src="<?=$photo["src"]?>" align="bottom" />
                                        </div>
                                    <?endforeach?>
                                <?endif;*/?>
                            </div>
                        </div>                
                        <div class="img left" style="width:308px">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PROPERTIES"]["photos"]["VALUE"][0]["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                        </div>
                        <div class="left scroll_arrows">
                            <a class="prev browse" style="margin-bottom:10px"></a>                    
                            <a class="next browse"></a>
                        </div>
                    </div>
                <?else:?>
                    <div class="img left" style="width:308px">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                    </div>
                <?endif;?>
            </div>
            <div class="priority_left">
                <?if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                    <div class="uppercase font10"><?=implode(",",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?></div>
                <?else:?>
                    <div class="uppercase font10"><?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?></div>
                <?endif;?>
                <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                <div class="clear"></div>
                <div class="small_rating" style="margin-top:4px;margin-left:0px;margin-bottom:20px;">
                    <?for($i = 1; $i <= 5; $i++):?>
                        <div class="small_star<?if($i <= $arItem["DISPLAY_PROPERTIES"]["RATIO"]["VALUE"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                    <?endfor?>
                </div>
                <div class="clear"></div>
                 <?
                    $params = array();
                    $params["id"] = $arItem["ID"];
                    $params = addslashes(json_encode($params));
                    $params2 = "";
                    $params2 = "name=".rawurlencode($arItem["NAME"])."&id=".$arItem["ID"]."&".bitrix_sessid_get();    
                    ?>
                 <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>
                    <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>
                        <input onclick="ajax_bron('<?=$params2?>')" type="submit" value="Забронировать столик" class="button" style="margin-bottom:10px" />                                
                        <br />
                    <?endif;?>                                
                        <input onclick="ajax_bron2('<?=$params2?>&what=2&banket=Y')" type="submit" value="Заказать банкет" class="grey_button" style="height:28px;" />                                
                <?/*elseif (CITY_ID=="kld"):?>
                        <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>
                        <input onclick="ajax_bron('<?=$params2?>',false,'_<?=CITY_ID?>')" type="submit" value="Забронировать столик" class="button" style="margin-bottom:10px" />                                
                        <br />
                    <?endif;?>                                
                        <input onclick="ajax_bron2('<?=$params2?>&what=2&banket=Y',false,'_<?=CITY_ID?>')" type="submit" value="Заказать банкет" class="grey_button" style="height:28px;" />                                
                <?*/?>
                <?endif;?>
                    <br /><br /><br />                    
                    <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>                       
            </div>
            <div class="priority_right">
                <div class="new">Рекламное место</div>
                <div class="clear"></div>
                <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                    <?if ($pid!="type"&&$pid!="photos"&&$pid!="RATIO"):?>
                        <p<?if($pid == "subway"):?> class="metro_<?=CITY_ID?>"<?endif?>><?if($pid != "subway"):?><b><?=$arProperty["NAME"]?>:</b><?endif?>
                            <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                <?if (CITY_ID=="spb"):?>
                                        <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                <?else:?>
                                        <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                <?endif;?>                                                    
                            <?else:?>

                                <?if ($pid=="phone"):?>
                                <?

                                        $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                        if(is_array($arProperty["DISPLAY_VALUE"])) 
                                                $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
                                        else{
                                                $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
                                                //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                        }

                                        $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);



                                        $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                        preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);


                                        $TELs=array();
                                        for($p=1;$p<5;$p++){
                                                foreach($matches[$p] as $key=>$v){
                                                        $TELs[$key].=$v;
                                                }	
                                        }

                                        foreach($TELs as $key => $T){
                                                if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                        }
                                        $old_phone = "";
                                        foreach($TELs as $key => $T){
                                            if ($old_phone!=$T):
                                            
                                        ?>
                                                <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=$arProperty["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                            <?endif;$old_phone = $T;?>
                                        <?}?>
                                <?elseif($pid=="address"):?>
                                    <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                        <?=$arProperty["DISPLAY_VALUE"]?>
                                    <?else:?>
                                        <?=$arProperty["DISPLAY_VALUE"][0]?>
                                    <?endif?>
                                <?elseif($pid=="opening_hours"):?>
                                    <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                        <?=$arProperty["DISPLAY_VALUE"]?>
                                    <?else:?>
                                        <?=$arProperty["DISPLAY_VALUE"][0]?>
                                    <?endif?>                                                
                                <?else:?>
                                    <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                        <?=$arProperty["DISPLAY_VALUE"]?>
                                    <?else:?>
                                        <?=implode(", ",$arProperty["DISPLAY_VALUE"])?>
                                    <?endif?>
                            <?endif?>

                        <?endif?>    
                        </p>
                        <?endif;?>
                <?endforeach;?>                
            </div>
            <div class="clear"></div>
            <div class="light_hr"></div>
    <?endforeach;?>
</div>