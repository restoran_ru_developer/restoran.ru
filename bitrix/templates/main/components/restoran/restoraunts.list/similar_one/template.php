<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left end" style="margin-bottom:0px;">
        <div class="imgborder">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>            
        </div>
        <div>
            <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>        
            <div class="rating">
                <?for($i = 1; $i <= 5; $i++):?>
                    <div class="small_star<?if($i <= round($arItem["PROPERTIES"]["ratio"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                <?endfor?>
                <div class="clear"></div>
            </div>        
        </div>           
        <?if ($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]):?>
            <?if(is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                <p><b><?=$arItem["DISPLAY_PROPERTIES"]["type"]["NAME"]?></b>: <?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]))?></p>
            <?else:?>
                <p><b><?=$arItem["DISPLAY_PROPERTIES"]["type"]["NAME"]?></b>: <?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?></p>
            <?endif;?>
        <?endif;?>
        <?if ($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]):?>
            <?if(is_array($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):?>
                <p><b><?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["NAME"]?></b>: <?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?></p>
            <?else:?>
                <p><b><?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["NAME"]?></b>: <?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])?></p>
            <?endif;?>
        <?endif;?>
        <?if ($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]):?>
            <?if(is_array($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])):?>
                <p><b><?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["NAME"]?></b>: <?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))?></p>
            <?else:?>
                <p><b><?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["NAME"]?></b>: <?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])?></p>
            <?endif;?>                
        <?endif;?>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <?if ($arItem!=end($arResult["ITEMS"])):?>
        <div class="dotted"></div>
    <?endif;?>
<?endforeach;?>