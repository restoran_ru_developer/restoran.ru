<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    if ($arItem["PREVIEW_PICTURE"])
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    else
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"])
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm_new.png";
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);

}

?>