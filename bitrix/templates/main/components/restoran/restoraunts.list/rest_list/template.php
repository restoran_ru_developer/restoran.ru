<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (count($arResult["ITEMS"])>0):?>    
    <?if ((!$_REQUEST["arrFilter_pf"]&&!$_REQUEST["letter"]&&!$_REQUEST["RUBRIC"])||$_REQUEST["PRIORITY_kitchen"]||$_REQUEST["PRIORITY_type"]):?>
        <div class="special left" style="width:728px">
            <script type="text/javascript">
            $(document).ready(function(){
                $(".photogalery_top_spec").galery({});

            });
            </script>
            <?
            global $arrFilterTop1;
            if (intval($_REQUEST["PRIORITY_kitchen"]))
            {
                $arrFilterTop1 = Array(
                    "PROPERTY_priority_kitchen" => intval($_REQUEST["PRIORITY_kitchen"]),
                    "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
                );      
            }
            elseif(intval($_REQUEST["PRIORITY_type"]))
            {
                $arrFilterTop1 = Array(
                    "PROPERTY_priority_type" => intval($_REQUEST["PRIORITY_type"]),
                    "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
                );
            }
            else
            {
                $arrFilterTop1 = Array(
                    "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
                    "PROPERTY_priority_type" => false,
                    "PROPERTY_priority_kitchen" => false,
                    "!PROPERTY_sleeping_rest_VALUE" => "Да"
                );        
            }
            $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "rest_list_top_spec",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arResult["ID"],
                            "NEWS_COUNT" => "1",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFilterTop1",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "opening_hours", "phone", "address", "subway","RATIO"),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "160",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => $arParams["PARENT_SECTION_CODE"],
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "86400",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_NOTES" => "Nw",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CONTEXT"=>$_REQUEST["CONTEXT"]
                    ),
                $component
            );?>
        </div>
        <div id="right_2_main_page" class="right right_block banner_ajax"></div>        
        <div class="clear"></div>
        <br />
        <div class="light_hr"></div>
    <?endif;?>
    <?// check exist PAGEN_1?>
    <?if($_REQUEST["page"] || $_REQUEST["PAGEN_1"] || $_REQUEST["letter"]):?>        
        <?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
            <div class="<?if($cell%2 != 0):?>restoraunt_end right<?else:?>restoraunt left<?endif?>">
                <?
                if ($_REQUEST["letter"]&&$_REQUEST["letter"]!="09")
                {
                    $letter = iconv("windows-1251","utf-8",chr($_REQUEST["letter"]));
                    if ($letter!=substr($arItem["NAME"],0,1))
                    {   
                        $temp = array();
                        $temp = explode(",",$arItem["TAGS"]);
                        $arItem["NAME"] = $temp[0];
                    }
                }
                ?>
                <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                <div class="small_rating" style="margin-top:4px;">
                    <?for($i = 1; $i <= 5; $i++):?>
                        <div class="small_star<?if($i <= $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                    <?endfor?>
               </div>
                <?/*if(($_REQUEST["page"] || $_REQUEST["PAGEN_1"] == 1)&&$cell<=20):?>
                    <div class="new" style="line-height:20px;">New</div>
                <?endif*/?>
                <!--<div class="new">Best</div>-->
                <div class="clear"></div>
                <div class="left" style="position:relative">
                    <div class="img"  style="overflow:hidden; height:160px">                        
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                        <?if ($arItem["PROPERTIES"]["d_tours"]["VALUE"]):?>
                        <div style="position:absolute; right:20px; bottom:20px;"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour2.png" width="70"/></div>
                        <?endif;?>
                    </div>
                    <div>
                        <?/*if ($arItem["PROPERTIES"]["sale10"]["VALUE"]=="Да"):
                            echo "<img src='/tpl/images/list_svyaznoy2.png' />";
                        endif;*/?>
                        <?/*<div class="left">
                            <script type="text/javascript"><!--
                            document.write(VK.Share.button({url: '<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>' ,title: "<?=$arItem["NAME"]?>", image: '<?='http://'.SITE_SERVER_NAME.$arItem["PREVIEW_PICTURE"]["SRC"]?>'}, {type: 'custom', text: '<img src="<?='http://'.SITE_SERVER_NAME?>/images/vk_like.png" height="20" />'}));
                            --></script>
                        </div>
                        <div class="left" style="margin-left: 20px;">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-text="<?=$arItem["NAME"]?>" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                        </div>
                        <div class="left" style="margin-left: 20px;">
                            <div class="fb-like" data-href="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-show-faces="true" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-font="tahoma" data-action="like"></div>
                        </div>
                        <div class="clear"></div>    */?>                    
                    </div>    
                    <div class="clear"></div>
                    <?if(CSite::InGroup( array(14,15,1))):?>                        
                            <?/*<div id="edit_link" style="position:absolute; bottom:-35px;"><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arItem["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>*/?>
                            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ID"]?>">Редактировать</a></div>
                    <?endif;?>
                    <?if(CSite::InGroup( array(23))&&CITY_ID=="ast"):?>                        
                            <?/*<div id="edit_link" style="position:absolute; bottom:-35px;"><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arItem["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>*/?>
                            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ID"]?>">Редактировать</a></div>
                    <?endif;?>
                </div>
                <div class="right" style="width:249px;">
                    <?
                    //if (is_array($arItem["DISPLAY_PROPERTIES"]["sale10"]))
                      //  unset($arItem["DISPLAY_PROPERTIES"]["sale10"]);
                    ?>
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                        <p<?if($pid == "subway"):?> class="metro_<?=CITY_ID?>"<?endif?>>
                            <?if ($pid=="subway"&&CITY_ID=="urm"):?>
                                <b>Ж/д станция: </b>
                            <?endif;?>
                            <?
                            //Название свойства
                            if($pid != "subway"):?><b><?=GetMessage("R_PROPERTY_".$arProperty["CODE"])?>:</b><?endif?>
                            <?
                            //Значение свойства                            
                            if(is_array($arProperty["DISPLAY_VALUE"]) && !$arProperty["LINK_IBLOCK_ID"]):?>
                                <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                    <?if (CITY_ID=="spb"):?>
                                        <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                    <?else:?>
                                        <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                    <?endif;?>                                                  
                                <?else:?>
                                    <?
                                    $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);
                                    if(is_array($arProperty["DISPLAY_VALUE"])) 
                                        $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
                                    else
                                        $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];

                                    $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);

                                    $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                    preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);

                                    $TELs=array();
                                    for($p=1;$p<5;$p++){
                                        foreach($matches[$p] as $key=>$v){
                                            $TELs[$key].=$v;
                                        }	
                                    }

                                    foreach($TELs as $key => $T){
                                            if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                    }
                                    ?>
                                    <?foreach($TELs as $key => $T):?>                                    
                                        <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=$arProperty["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                    <?endforeach;?>
                                <?endif;?>
                            <?elseif(is_array($arProperty["DISPLAY_VALUE"]) && $arProperty["LINK_IBLOCK_ID"]):?>
                                <?// subway and other?>
                                <?if($pid == 'subway' && $_REQUEST["arrFilter_pf"]["subway"][0] &&  $USER->IsAdmin()):?>
                                    <?foreach($arProperty["VALUE"] as $propValKey=>$propVal) {
                                        if(in_array($propVal, $_REQUEST["arrFilter_pf"][$pid])) {
                                            echo $arProperty["DISPLAY_VALUE"][$propValKey];
                                            break;
                                        }
                                    }?>
                                <?else:?>                                    
                                        <?=  strip_tags(implode(", ",$arProperty["DISPLAY_VALUE"]))?>                                    
                                <?endif?>
                            <?elseif(!is_array($arProperty["DISPLAY_VALUE"]) && $arProperty["LINK_IBLOCK_ID"]):?>
                                <?if ($pid=="average_bill"&&CITY_ID=="tmn"):
                                    $ab = str_replace("до 1000р","500 - 1000р",$arProperty["DISPLAY_VALUE"]);
                                    echo $ab;
                                else:?>
                                    <?=($arProperty["DISPLAY_VALUE"]);?>
                                <?endif;?>
                            <?elseif(!is_array($arProperty["DISPLAY_VALUE"]) && !$arProperty["LINK_IBLOCK_ID"]):?>
                                <?if ($pid=="phone"):?>
                                	<?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                            <?if (CITY_ID=="spb"):?>
                                                <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                            <?else:?>
                                                <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                            <?endif;?>                                                    
                                        <?else:?>
                                            <?
                                            $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                            if(is_array($arProperty["DISPLAY_VALUE"])) 
                                                $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
                                            else
                                                $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];

                                            $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);

                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                            preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);

                                            $TELs=array();
                                            for($p=1;$p<5;$p++){
                                                    foreach($matches[$p] as $key=>$v){
                                                            $TELs[$key].=$v;
                                                    }	
                                            }

                                            foreach($TELs as $key => $T){
                                                    if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                            }

                                            foreach($TELs as $key => $T){?>
                                                <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=$arProperty["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                            <?}?>
                                        <?endif;?>
                                <?else:?>
                                    <?=($arProperty["DISPLAY_VALUE"])?>
                                <?endif;?>
                            <?endif?>                            
                        </p>
                    <?endforeach;?>
                </div>
                <div class="clear"></div>
                <div align="right">
                    <?
                    $params = array();
                    $params["id"] = $arItem["ID"];
                    $params = addslashes(json_encode($params));
                    $params2 = "";
                    $params2 = "name=".rawurlencode($arItem["NAME"])."&id=".$arItem["ID"]."&".bitrix_sessid_get();                                        
                    ?>
                    <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>
                        <input onclick="ajax_bron('<?=$params2?>')" type="submit" value="<?=GetMessage("BOOK_BUTTON")?>" class="button" />
                    <?/*elseif (CITY_ID=="kld"):?>
                        <input onclick="ajax_bron('<?=$params2?>',false,'_<?=CITY_ID?>')" type="submit" value="<?=GetMessage("BOOK_BUTTON")?>" class="button" />
                        <?*/?>
                    <?endif;?>
                </div>                
            </div>
            <?if($cell%2 != 0):?>
                <div class="clear"></div>
                <?if (end($arResult["ITEMS"])!=$arItem):?>
                <hr />
                <?endif;?>
            <?endif?>
        <?endforeach;?>
        <?if($cell%2 == 0):?>
            <?while(($cell++)%2 == 0):?>
                <div class="restoraunt_end right"></div>
                <div class="clear"></div>
                <hr />
            <?endwhile;?>
        <?endif?>
    <?else:?>
    <?// show ?>
    <?
    global $arrFilterSpecStr;
    $arrFilterSpecStr = Array(
        "PROPERTY_s_place_".$_REQUEST["CATALOG_ID"]."_VALUE" => Array("Да")
    );
    //v_dump($_REQUEST["CATALOG_ID"]);
    $APPLICATION->IncludeComponent("restoran:restoraunts.list", 
        "rest_spec_places_new", 
        Array(
    	"IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
    	"IBLOCK_ID" => CITY_ID,	// Код информационного блока
    	"PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
    	"NEWS_COUNT" => "20",	// Количество ресторанов на странице
    	"SORT_BY1" => "SORT",	// Поле для первой сортировки ресторанов
    	"SORT_ORDER1" => "DESC",	// Направление для первой сортировки ресторанов
    	"SORT_BY2" => "",	// Поле для второй сортировки ресторанов
    	"SORT_ORDER2" => "",	// Направление для второй сортировки ресторанов
    	"FILTER_NAME" => "arrFilterSpecStr",	// Фильтр
    	"PROPERTY_CODE" => array(	// Свойства
    		0 => "type",
    		1 => "kitchen",
    		2 => "average_bill",
    		3 => "opening_hours",
    		4 => "phone",
    		5 => "address",
    		6 => "subway",
    		7 => "photos",
                8 => "RATIO"
    	),
    	"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
    	"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
    	"AJAX_MODE" => "N",	// Включить режим AJAX
    	"AJAX_OPTION_SHADOW" => "Y",
    	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    	"CACHE_TYPE" => "Y",	// Тип кеширования
    	"CACHE_TIME" => "86400",	// Время кеширования (сек.)
    	"CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
    	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
    	"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
    	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
    	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
    	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
    	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
    	"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
    	"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
    	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
    	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
    	"PAGER_TITLE" => "Рестораны",	// Название категорий
    	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
    	"PAGER_TEMPLATE" => "rest_list",	// Название шаблона
    	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
    	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
    	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
    	"DISPLAY_DATE" => "Y",	// Выводить дату элемента
    	"DISPLAY_NAME" => "Y",	// Выводить название элемента
    	"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
    	"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
    	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"CONTEXT"=>$_REQUEST["CONTEXT"]
    	),
        $component
    );?>
    <?endif?>
    <?if ((!$_REQUEST["arrFilter_pf"]&&!$_REQUEST["letter"]&&!$_REQUEST["RUBRIC"])||$_REQUEST["PRIORITY_kitchen"]||$_REQUEST["PRIORITY_type"]):?>
        <div class="special left" style="width:728px;">
            <?
            global $arrFilterTop;
            global $TopSpec;
            if (intval($_REQUEST["PRIORITY_kitchen"]))
            {
                $arrFilterTop = Array(
                    "!ID" => $TopSpec[0],
                    "PROPERTY_priority_kitchen" => intval($_REQUEST["PRIORITY_kitchen"]),
                    "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                );      
            }
            elseif(intval($_REQUEST["PRIORITY_type"]))
            {
                $arrFilterTop = Array(
                    "!ID" => $TopSpec[0],
                    "PROPERTY_priority_type" => intval($_REQUEST["PRIORITY_type"]),
                    "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                );
            }
            else
            {
                $arrFilterTop = Array(
                    "!ID" => $TopSpec[0],
                    "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                    "PROPERTY_priority_type" => false,
                    "PROPERTY_priority_kitchen" => false,
                    "!PROPERTY_sleeping_rest_VALUE" => "Да"
                );
            }
            $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "rest_list_top_spec",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arResult["ID"],
                            "NEWS_COUNT" => "1",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFilterTop",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "opening_hours", "phone", "address", "subway"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "160",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => $arParams["PARENT_SECTION_CODE"],
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "86400",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CONTEXT"=>$_REQUEST["CONTEXT"]
                    ),
                $component
            );?>
        </div>
        <div id="right_1_main_page" class="right banner_ajax"></div>         
        <div class="clear"></div>
    <?endif;?>
    <hr class="bold" />
        <?if($_REQUEST["PAGEN_1"]):?>
            <div class="left">
                <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
                <?$excUrlParams=Array("pageCnt","pageRestCnt","index_php?page","index_php","CITY_ID","CATALOG_ID")?>
            <?=($_REQUEST["pageRestCnt"] == 20 || !$_REQUEST["pageRestCnt"] ? "20" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageRestCnt=20", $excUrlParams).'">20</a>')?>
            <?=($_REQUEST["pageRestCnt"] == 40 ? "40" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageRestCnt=40", $excUrlParams).'">40</a>')?>
            <?=($_REQUEST["pageRestCnt"] == 60 ? "60" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageRestCnt=60", $excUrlParams).'">60</a>')?>
            </div>
        <?endif?>
        <div class="right">
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            	<?=$arResult["NAV_STRING"]?>
            <?endif;?>
        </div>
    <div class="clear"></div>
    <?else:?>
        <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
    <?endif;?>
    <br /><br />
    <?/*if ($_REQUEST["arrFilter_pf"]["sale10"]=="Да"):?>
        <h2>Правила акции</h2>
        <p class="font14">1) Скидка действительна только в ресторанах, представленных в разделе по <a href="http://<?=(CITY_ID=="spb")?"spb":"www"?>.restoran.ru/<?=CITY_ID?>/catalog/restaurants/all/?page=1&arrFilter_pf%5Bsale10%5D=%D0%94%D0%B0">ссылке</a>;</p>
        <p class="font14">2) Сроки проведения акции: с 12 марта 2013 года по 8 июня 2013 года;</p>
        <p class="font14">3) Скидка предоставляется только при оплате счета банковскими картами Связного Банка;</p>
        <p class="font14">4) Скидка предоставляется при сумме счета от 500 рублей;</p>
        <p class="font14">5) Скидка не предоставляется компаниям более 10 человек;</p>
	<p class="font14">6) Скидка не распространяется на бизнес-ланчи и текущие акции</p>
        <p class="font14">7) В ресторане "Мамина Паста" скидка не распространяется на: бизнес-ланч, проведение банкетов от 6 человек, меню кулинарии "Мамина паста", мастер-классы.</p>
        <br /><Br />
    <?endif;*/?>
    <div id ="for_seo">
        
    </div>
    <div id="yandex_direct">
        <script type="text/javascript"> 
        //<![CDATA[
        yandex_partner_id = 47434;
        yandex_site_bg_color = 'FFFFFF';
        yandex_site_charset = 'utf-8';
        yandex_ad_format = 'direct';
        yandex_font_size = 1;
        yandex_direct_type = 'horizontal';
        yandex_direct_limit = 4;
        yandex_direct_title_color = '24A6CF';
        yandex_direct_url_color = '24A6CF';
        yandex_direct_all_color = '24A6CF';
        yandex_direct_text_color = '000000';
        yandex_direct_hover_color = '1A1A1A';
        document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
        //]]>
        </script>
    </div>
    <!--<img src="<?=SITE_TEMPLATE_PATH?>/images/catalog/direct.jpg" />-->
    <?$APPLICATION->IncludeComponent(
    	"bitrix:advertising.banner",
    	"",
    	Array(
    		"TYPE" => "bottom_rest_list",
    		"NOINDEX" => "N",
    		"CACHE_TYPE" => "A",
    		"CACHE_TIME" => "0"
    	),
    false
    );?>
    <br /><br />