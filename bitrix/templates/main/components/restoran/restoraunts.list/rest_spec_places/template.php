<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $cell=>$arItem):?>    
        <div class="<?if($cell%2 != 0):?>restoraunt_end right<?else:?>restoraunt left<?endif?>">
            <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
            <div class="small_rating" style="margin-top:4px;">
                <?for($i = 1; $i <= 5; $i++):?>
                    <div class="small_star<?if($i <= $arItem["PROPERTIES"]["ratio"]["VALUE"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                <?endfor?>
        </div>
            <div class="new" style="line-height:20px;">Best</div>
            <div class="clear"></div>
            <div class="left" style="position:relative">
                <div class="img" style="overflow:hidden; height:160px">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                </div>
                <div>
                    <?/*<div class="left">
                        <script type="text/javascript"><!--
                        document.write(VK.Share.button({url: '<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>' ,title: "<?=$arItem["NAME"]?>", image: '<?='http://'.SITE_SERVER_NAME.$arItem["PREVIEW_PICTURE"]["SRC"]?>'}, {type: 'custom', text: '<img src="<?='http://'.SITE_SERVER_NAME?>/images/vk_like.png" height="20" />'}));
                        --></script>
                    </div>
                    <div class="left" style="margin-left: 20px;">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-text="<?=$arItem["NAME"]?>" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                    </div>
                    <div class="left" style="margin-left: 20px;">
                        <div class="fb-like" data-href="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-show-faces="true" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-font="tahoma" data-action="like"></div>
                    </div>*/?>
                </div>
                <?if(CSite::InGroup( array(14,15,1))):?>                        
                    <div id="edit_link" style="position:absolute; bottom:-35px;"><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arItem["ID"]?>">Редактировать</a></div>
                <?endif;?>
            </div>
            <div class="right" style="width:249px;">
                <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                    <p<?if($pid == "subway"):?> class="metro_<?=CITY_ID?>"<?endif?>><?if($pid != "subway"):?><b><?=$arProperty["NAME"]?>:</b><?endif?>
                        <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                            <?if (CITY_ID=="spb"):?>
                            <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                            <?else:?>
                                                    <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                            <?endif;?>                                                    
                                    <?else:?>

                                            <?if ($pid=="phone"):?>
                                            <?

                                                    $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                                    if(is_array($arProperty["DISPLAY_VALUE"])) 
                                                            $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
                                                    else{
                                                            $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
                                                            //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                    }

                                                    $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);



                                                    $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                    preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);


                                                    $TELs=array();
                                                    for($p=1;$p<5;$p++){
                                                            foreach($matches[$p] as $key=>$v){
                                                                    $TELs[$key].=$v;
                                                            }	
                                                    }

                                                    foreach($TELs as $key => $T){
                                                            if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                                    }

                                                    foreach($TELs as $key => $T){
                                                    ?>
                                                            <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=$arProperty["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                                    <?}?>

                                            <?else:?>

                        <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                            <?=$arProperty["DISPLAY_VALUE"]?>
                        <?else:?>
                            <?=implode(", ",$arProperty["DISPLAY_VALUE"])?>
                        <?endif?>

                        <?endif?>

                    <?endif?>    
                    </p>
                <?endforeach;?>
            </div>
            <div class="clear"></div>
            <div align="right">
                <?
                $params = array();
                $params["id"] = $arItem["ID"];
                $params = addslashes(json_encode($params));
                $params2 = "";
                $params2 = "name=".rawurlencode($arItem["NAME"])."&id=".$arItem["ID"]."&".bitrix_sessid_get();    
                ?>                       
                <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input onclick="ajax_bron('<?=$params2?>')" type="submit" value="<?=GetMessage("BOOK_BUTTON")?>" class="button" />
            </div>
        </div>
        <?if($cell%2 != 0):?>
            <div class="clear"></div>
            <hr />
        <?endif?>
<?endforeach;?>
<?if($cell%2 == 0):?>
    <?while(($cell++)%2 == 0):?>
        <div class="restoraunt_end right"></div>
        <div class="clear"></div>
        <hr />
    <?endwhile;?>
<?endif?>