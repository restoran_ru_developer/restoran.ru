<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left<?if($key%3 == 2):?> end<?endif?>">
        <div class="imgborder" style="height:127px;">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="232" /></a>            
        </div>
        <div>
            <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>        
            <div class="rating">
                <?for($i = 1; $i <= 5; $i++):?>
                    <div class="small_star<?if($i <= round($arItem["PROPERTIES"]["ratio"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                <?endfor?>
                <div class="clear"></div>
            </div>        
        </div>
        <div style="margin-top:5px;">            
            <?if ($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]):?>
                <?if(is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                    <p><b><?=$arItem["DISPLAY_PROPERTIES"]["type"]["NAME"]?></b>: <?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]))?></p>
                <?else:?>
                    <p><b><?=$arItem["DISPLAY_PROPERTIES"]["type"]["NAME"]?></b>: <?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?></p>
                <?endif;?>
            <?endif;?>
            <?if ($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]):?>
                <?if(is_array($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):?>
                    <p><b><?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["NAME"]?></b>: <?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?></p>
                <?else:?>
                    <p><b><?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["NAME"]?></b>: <?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])?></p>
                <?endif;?>
            <?endif;?>
            <?if ($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]):?>
                <?if(is_array($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])):?>
                    <p><b><?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["NAME"]?></b>: <?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))?></p>
                <?else:?>
                    <p><b><?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["NAME"]?></b>: <?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])?></p>
                <?endif;?>                
            <?endif;?>
        </div>
        <div class="clear"></div>
    </div>
    <?if ($key%3 == 2):?>
            <div class="clear"></div>
    <?endif;?>
    <?if ($key%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
            <div class="light_hr"></div>
    <?endif;?>
    <?if ($key%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
            <div class="clear"></div>
    <?endif;?>
<?endforeach;?>