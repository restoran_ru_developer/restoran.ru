<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="catalog">
    <h1>Результат поиска по ресторанам</h1>
    <div class="sort">
        <?
        // set rest sort links
        $excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1");
        $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageRestSort=new", $excUrlParams);
        $sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageRestSort=price", $excUrlParams);
        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageRestSort=ratio", $excUrlParams);
        $sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageRestSort=alphabet", $excUrlParams);
        ?>
        <?=($_REQUEST["pageRestSort"] == "new" || !$_REQUEST["pageRestSort"] ? GetMessage("SORT_NEW_TITLE") : '<a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a>')?> /
        <?=($_REQUEST["pageRestSort"] == "price" ? GetMessage("SORT_PRICE_TITLE") : '<a class="another" href="'.$sortPricePage.'">'.GetMessage("SORT_PRICE_TITLE").'</a>')?> /
        <?=($_REQUEST["pageRestSort"] == "ratio" ? GetMessage("SORT_RATIO_TITLE") : '<a class="another" href="'.$sortRatioPage.'">'.GetMessage("SORT_RATIO_TITLE").'</a>')?> /
        <?=($_REQUEST["pageRestSort"] == "alphabet" ? GetMessage("SORT_ALPHABET_TITLE") : '<a class="another" href="'.$sortAlphabetPage.'">'.GetMessage("SORT_ALPHABET_TITLE").'</a>')?>
    </div>
        <?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
            <div class="<?if($cell%2 != 0):?>restoraunt_end right<?else:?>restoraunt left<?endif?>">
                <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                <div class="small_rating">
                    <?for($i = 1; $i <= 5; $i++):?>
                        <div class="small_star<?if($i <= $arItem["PROPERTIES"]["ratio"]["VALUE"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                    <?endfor?>
               </div>
                <?if($cell <= 20):?>
                    <div class="new">New</div>
                <?endif?>
                <!--<div class="new">Best</div>-->
                <div class="clear"></div>
                <div class="left">
                    <div class="img">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                    </div>
                    <div>
                        <div class="left">
                            <script type="text/javascript"><!--
                            document.write(VK.Share.button({url: '<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>' ,title: "<?=$arItem["NAME"]?>", image: '<?='http://'.SITE_SERVER_NAME.$arItem["PREVIEW_PICTURE"]["SRC"]?>'}, {type: 'custom', text: '<img src="<?='http://'.SITE_SERVER_NAME?>/images/vk_like.png" height="20" />'}));
                            --></script>
                        </div>
                        <div class="left" style="margin-left: 20px;">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-text="<?=$arItem["NAME"]?>" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
                        </div>
                        <div class="left" style="margin-left: 20px;">
                            <div class="fb-like" data-href="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-show-faces="true" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-font="tahoma" data-action="like"></div>
                        </div>
                    </div>
                </div>
                <div class="right" style="width:249px;">
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                        <p<?if($pid == "subway"):?> class="metro"<?endif?>><?if($pid != "subway"):?><b><?=$arProperty["NAME"]?>:</b><?endif?>
                            <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                <?=$arProperty["DISPLAY_VALUE"]?>
                            <?else:?>
                                <?=$arProperty["DISPLAY_VALUE"][0]?>
                            <?endif?>
                        </p>
                    <?endforeach;?>
                </div>
                <div class="clear"></div>
                <div align="right">
                    <a href="#">В избранное</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" value="ЗАБРОНИРОВАТЬ" class="button" />
                </div>
            </div>
            <?if($cell%2 != 0):?>
                <div class="clear"></div>
                <hr />
            <?endif?>
        <?endforeach;?>
        <?if($cell%2 == 0):?>
            <?while(($cell++)%2 == 0):?>
                <div class="restoraunt_end right"></div>
                <div class="clear"></div>
                <hr />
            <?endwhile;?>
        <?endif?>
    <div class="clear"></div><br /><br />
    <?if($arResult["ITEMS"]):?>
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageRestCnt"] == 20 || !$_REQUEST["pageRestCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageRestCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=40'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageRestCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=60'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">60</a>')?>
        </div>
    <?endif?>
    <div class="right">
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>
    <?if(!$arResult["ITEMS"]):?>
        <h4 align="center"><?=GetMessage("EMPTY_SEARCH_RESULT")?></h4><br />
        <h5 align="center"><a href="/<?=CITY_ID?>/catalog/restaurants/all"><?=GetMessage("EMPTY_SEARCH_GO")?></a></h5>
        <br /><br />
    <?endif?>
</div>