<?
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill"
);
v_dump($arResult);
foreach($arResult["ITEMS"] as $cell=>$arItem) {
    // resize images
    
    foreach($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photoID) {
        $arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["DISPLAY_VALUE"][$pKey] = CFile::ResizeImageGet($photoID, array('width' => 397, 'height' => 271), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    }
    if($arParams["PREVIEW_TRUNCATE_LEN"]>0)
    {
            $end_pos = $arParams["PREVIEW_TRUNCATE_LEN"];
            $arItem["PREVIEW_TEXT"] = strip_tags($arItem["DETAIL_TEXT"]);
            while(substr($arItem["PREVIEW_TEXT"],$end_pos,1)!=" " && $end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $end_pos++;
            if($end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $arItem["PREVIEW_TEXT"] = substr($arItem["PREVIEW_TEXT"], 0, $end_pos)."...";
            $arResult["ITEMS"][$cell]["PREVIEW_TEXT"] = $arItem["PREVIEW_TEXT"];
    }
    
    // remove links from subway name
    /*foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        // remove links from subway name
        if(in_array($pid, $arFormatProps)) {
            if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                }
            } else {
                $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
            }
        }
    }*/
}
?>