<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
    <div class="active_rating left" style="padding-top:4px; padding-left:25px">
        <?for($k = 1; $k <= 5; $k++):?>
            <div class="star<?if($k <= $arItem["PROPERTIES"]["ratio"]["VALUE"]):?>_a<?endif?>" alt="<?=$k?>"></div>
        <?endfor?>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="left photogalery_top_spec">
        <div class="img">
            <img src="<?=($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"][0]["src"])?$arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"][0]["src"]:$arItem["PREVIEW_PICTURE"]["SRC"]?>" />
        </div>
        <?if(count($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"])>1):?>
            <div class="special_scroll">
                <div class="scroll_container">
                    <?foreach($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"] as $pKey=>$photo):?>
                        <div class="item<?if($pKey == 0):?> active<?endif?>">
                            <img src="<?=$photo["src"]?>" align="bottom" />
                        </div>
                    <?endforeach?>
                </div>
            </div>
            <div class="left scroll_arrows">
                <a class="prev browse left"></a>
                <span class="scroll_num">1</span>/<?=count($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"])?>
                <a class="next browse right"></a>
            </div>
        <?endif?>
        <div class="right" style="width:225px; white-space: nowrap; padding-top:8px;">
            <div class="left">
                <script type="text/javascript"><!--
                document.write(VK.Share.button({url: '<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>' ,title: "<?=$arItem["NAME"]?>", image: '<?='http://'.SITE_SERVER_NAME.$arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"][0]["src"]?>'},{type: 'custom', text: '<img src="<?='http://'.SITE_SERVER_NAME?>/images/vk_like.png" height="20" />'}));
                --></script>
            </div>
            <div class="left" style="margin-left: 20px;">
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-text="<?=$arItem["NAME"]?>" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
            </div>
            <div class="left" style="margin-left: 20px;">
                <div class="fb-like" data-href="<?='http://'.SITE_SERVER_NAME.$arItem["DETAIL_PAGE_URL"]?>" data-show-faces="true" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-font="tahoma" data-action="like"></div>
            </div>
        </div>
    </div>
    <div class="right" style="width:305px;">
        <?//v_dump($arItem["DISPLAY_PROPERTIES"]);?>
        <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
            <p<?if($pid == "subway"):?> class="metro"<?endif?>><?if($pid != "subway"):?><b><?=$arProperty["NAME"]?>:</b><?endif?>
            	<?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
					<?if (CITY_ID=="spb"):?>
						(812) 740-18-20
					<?else:?>
						(495) 988-26-56
					<?endif;?>                                                    
				<?else:?>
                                                
                <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                    <?=$arProperty["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=$arProperty["DISPLAY_VALUE"][0]?>
                <?endif?>
                
                <?endif?>
            </p>
        <?endforeach;?>
        <br />
        <div style="padding-bottom:30px; height:102px; overflow: hidden">
            <p><i class="font14"><?=$arItem["PREVIEW_TEXT"]?></i></p>
        </div>
        <div align="right">
            <?
            $params = array();
            $params["id"] = $arItem["ID"];
            $params = addslashes(json_encode($params));
            $params2 = "";
            $params2 = "name=".$arItem["NAME"]."&id=".$arItem["ID"]."&".bitrix_sessid_get();    
            ?>
            <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                                
            <input onclick="ajax_bron('<?=$params2?>')" type="submit" value="<?=GetMessage("BOOK_BUTTON")?>" class="button" />
        </div>
    </div>
    <div class="clear"></div>
<?endforeach;?>