<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
{
    echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
    LocalRedirect($APPLICATION->GetCurDir());
    
}
?>
<?//v_dump($arResult["arUser"])?>
<div id="content">    
    <div id="user_info" class="left">
        <h1><?=GetMessage("PROFILE")?></h1>
        <?if ($_REQUEST["delete"]=="Y"):?>
            <?=ShowError("Ваш аккаунт был удален. Через несколько секунд Вы попадете на главную старницу сайта.")?>
            <script>
                setTimeout("location.href='/<?=CITY_ID?>/?logout=yes'",3000);
            </script>
        <?endif;?>
        <table cellpadding="20" cellspacing="0" width="100%">
            <tr>
                <td align="center" valign="top" style="width:240px">
                    <div class="userpic" style="background:url(<?=$arResult["arUser"]["PERSONAL_PHOTO"]["src"]?>) left top no-repeat">
                        <!--<img src="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["src"]?>" width="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["width"]?>"  height="<?=$arResult["arUser"]["PERSONAL_PHOTO"]["height"]?>" />-->
                    </div>
                    <?if (is_array($arResult["RESTORAN"])):?>
                        <p class="profile-description" align="center"><em>Ресторатор 
                        <?foreach($arResult["RESTORAN"] as $rest):?>
                            <a href="<?=$rest["DETAIL_PAGE_URL"]?>" class="another no_border"><?=$rest["NAME"]?></a>
                            <?if (end($arResult["RESTORAN"])!=$rest) echo ", ";?>
                        <?endforeach;?>
                        </em></p>
                    <?endif;?>
                    <!--<div class="scores">
                        <span>138</span> <br />
                        в рейтинге пользователей
                    </div>
                    <br />
                    <p class="profile-stat" align="center"><em>
                        с нами с <?//=$arResult["arUser"]["DATE_REGISTER"]?> года<br />
                        приглашала в ресторан: 0 раз,<br />
                        была приглашена: 15 раз</em>
                    </p>-->
                </td>
                <td valign="top" style="position:relative">
                    <?if ($arResult["ID"]==$USER->GetID()):?>
                        <script>
                            /*$(document).ready(function(){
                                $("#edit_profile").click(function(){
                                    var _this = $(this);
                                    $.ajax({
                                        type: "POST",
                                        url: _this.attr("href"),
                                        data: "USER_NAME=<?=$arUser["NAME"]." ".$arUser["LAST_NAME"]?>&USER_ID=<?=intval($_REQUEST["USER_ID"])?>&<?=bitrix_sessid_get()?>",
                                        success: function(data) {
                                            if (!$("#edit_modal").size())
                                            {
                                                $("<div class='popup popup_modal' id='edit_modal' style='width:700px;'></div>").appendTo("body");                                                               
                                            }
                                            $('#edit_modal').html(data);
                                            showOverflow();
                                            setCenter($("#edit_modal"));
                                            $("#edit_modal").fadeIn("300"); 
                                        }
                                    });
                                    return false;
                                });
                                });*/
                        </script>
                    <?endif;?>
                    <div class="read" style="position:relative">
                        <?if($arResult["arUser"]["ID"]==$USER->GetID()&&!$_REQUEST["delete"]){?><div id="personal"><a class="no_border" href="/users/id<?=$arResult["arUser"]["ID"]?>/profile_edit/"><?=GetMessage('EDIT_DATA')?></a></div><?}?>
                        <?if($arResult["arUser"]["ID"]==$USER->GetID()&&!$_REQUEST["delete"]){?><div id="personal_delete"><a class="no_border" href="/users/id<?=$arResult["arUser"]["ID"]?>/?delete=Y"><?=GetMessage('DELETE_DATA')?></a></div><?}?>
                        <table cellpadding="5" class="user_params">
                            <tr>
                                <td width="160" class="name"><?=GetMessage('NAME')?></td>
                                <td class="value"><?=$arResult["arUser"]["NAME"]?> <?=$arResult["arUser"]["LAST_NAME"]?></td>
                            </tr>                 
                            <?if ($arResult["arUser"]["PERSONAL_GENDER"]):?>
                            <tr>
                                <td class="name"><?=GetMessage('USER_GENDER')?></td>
                                <td class="value"><?=GetMessage("SEX_".$arResult["arUser"]["PERSONAL_GENDER"])?></td>
                            </tr>
                            <?endif;?>
                            <?if($arResult["arUser"]["PERSONAL_BIRTHDAY"]):?>
                            <tr>
                                <td class="name"><?=GetMessage('USER_BIRTHDAY')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?></td>
                            </tr>
                            <?endif;?>
                            <?if($arResult["arUser"]["PERSONAL_CITY"]):?>
                            <tr>
                                <td class="name"><?=GetMessage('USER_CITY')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_CITY"]?></td>
                            </tr>       
                            <?endif;?>
                            <?if(($arResult["arUser"]["PERSONAL_PHONE"] && (!$arResult["arUser"]["UF_HIDE_CONTACTS"] || $arResult["ID"] == $USER->GetID())) || ($arResult["arUser"]["PERSONAL_PHONE"] && $USER->IsAdmin())):?>
                            <tr>
                                <td class="name"><?=GetMessage('USER_PHONE')?></td>
                                <td class="value"><?=$arResult["arUser"]["PERSONAL_PHONE"]?></td>
                            </tr>       
                            <?endif;?>
                            <?if(($arResult["arUser"]["EMAIL"] && !$arResult["arUser"]["UF_HIDE_CONTACTS"] || $arResult["ID"] == $USER->GetID()) || ($arResult["arUser"]["EMAIL"] && $USER->IsAdmin())):?>
                            <tr>
                                <td class="name"><?=GetMessage('EMAIL')?></td>
                                <td class="value"><?=$arResult["arUser"]["EMAIL"]?></td>
                            </tr> 
                            <?endif;?>
                            <?/*if($arResult["arUser"]["PERSONAL_NOTES"]):?>
                            <tr>
                                <td class="name"><?=GetMessage('KITCHEN')?></td>
                                <td class="value"><i><?=$arResult["arUser"]["PERSONAL_NOTES"]?></i></td>
                            </tr>
                            <?endif;*/?>
                            <?if($arResult["arUser"]["UF_HIDE_CONTACTS"] && $arResult["ID"] == $USER->GetID()):?>
                            <tr>
                                <td class="name"><?=GetMessage('UF_HIDE_CONTACTS')?></td>
                                <td class="value" valign="bottom"><br /><i><?=GetMessage("UF_HIDE_CONTACTS_".$arResult["arUser"]["UF_HIDE_CONTACTS"])?></i></td>
                            </tr>
                            <?endif;?>
                            <?if($arResult["arUser"]["WORK_NOTES"]):?>
                            <tr>
                                <td class="name"><?=GetMessage('ABOUT')?></td>
                                <td class="value font12"><i><?=$arResult["arUser"]["WORK_NOTES"]?></i></td>
                            </tr>    
                            <?endif;?>
                        </table>
                    </div>
                    <!--<div class="read" style="display:none;">
                        <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
                            <?=$arResult["BX_SESSION_CHECK"]?>
                            <input type="hidden" name="lang" value="<?=LANG?>" />
                            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                            <table>
                                <tr>
                                        <td width="120" class="name"><?=GetMessage('NAME')?></td>
                                        <td class="value"><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></td>
                                </tr>
                                <tr>
                                        <td class="name"><?=GetMessage('LAST_NAME')?></td>
                                        <td class="value"><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('SECOND_NAME')?></font></td>
                                        <td><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('EMAIL')?></td>
                                        <td>
                                        <input type="text" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                                        <input type="hidden" name="LOGIN" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                                    </td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('NEW_PASSWORD_REQ')?></td>
                                        <td>
                                            <input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" />
                                            <input type="hidden" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
                                        </td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('NEW_PASSWORD_CONFIRM')?></td>
                                        <td></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('USER_PHONE')?></td>
                                        <td><input type="text" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /></td>
                                </tr>
                                <tr>
                                        <td><?=GetMessage('USER_CITY')?></td>
                                        <td><input type="text" name="PERSONAL_CITY" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></td>
                                </tr>
                                <tr>
                                    <td><?=GetMessage("USER_BIRTHDAY_DT")?> (<?=$arResult["DATE_FORMAT"]?>):</td>
                                    <td><?
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'SHOW_INPUT' => 'Y',
                                            'FORM_NAME' => 'form1',
                                            'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                            'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                            'SHOW_TIME' => 'N'
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?></td>
                                </tr>
                                <tr>
                                    <td><?=GetMessage('USER_GENDER')?></td>
                                    <td>
                                        <select name="PERSONAL_GENDER">
                                            <option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
                                            <option value="M"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_MALE")?></option>
                                            <option value="F"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?=GetMessage('PREFERENCE_IN_THE_FOOD')?></td>
                                    <td><textarea rows="5" cols="40" name="PERSONAL_NOTES"><?=$arResult["arUser"]["PERSONAL_NOTES"]?></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">&nbsp;&nbsp;<input type="reset" value="<?=GetMessage('MAIN_RESET');?>"></td>
                                </tr>
                            </table>
                        </form>
                    </div>-->
                </td>
            </tr>
        </table> 
    </div>
    <div class="right" style="width:240px">
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            </div>
    </div>
    <div class="clear"></div>
</div>
<?if ($arResult["arUser"]["WORK_WWW"]||$arResult["arUser"]["WORK_PHONE"]||$arResult["arUser"]["WORK_FAX"]||$arResult["arUser"]["WORK_PAGER"]||$arResult["arUser"]["WORK_STATE"]||$arResult["arUser"]["WORK_ZIP"]||$arResult["arUser"]["WORK_CITY"]||$arResult["arUser"]["WORK_MAILBOX"]):?>
<div class="social-icons">
    <div class="wrap-div">
            <ul>
                    <li class="social-icon-empty">Другие аккаунты:</li>
                    <?if ($arResult["arUser"]["WORK_WWW"]):?>
                        <li class="social-icon-1"><a target="_blank" href="<?=$arResult["arUser"]["WORK_WWW"]?>"><?=$arResult["arUser"]["WORK_WWW"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_PHONE"]):?>
                        <li class="social-icon-2"><a target="_blank" href="<?=$arResult["arUser"]["WORK_PHONE"]?>"><?=$arResult["arUser"]["WORK_PHONE"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_FAX"]):?>
                        <li class="social-icon-3"><a target="_blank" href="<?=$arResult["arUser"]["WORK_FAX"]?>"><?=$arResult["arUser"]["WORK_FAX"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_PAGER"]):?>
                        <li class="social-icon-4"><a target="_blank" href="<?=$arResult["arUser"]["WORK_PAGER"]?>"><?=$arResult["arUser"]["WORK_PAGER"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_STATE"]):?>
                        <li class="social-icon-5"><a target="_blank" href="<?=$arResult["arUser"]["WORK_STATE"]?>"><?=$arResult["arUser"]["WORK_STATE"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_ZIP"]):?>
                        <li class="social-icon-6"><a target="_blank" href="<?=$arResult["arUser"]["WORK_ZIP"]?>"><?=$arResult["arUser"]["WORK_ZIP"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_CITY"]):?>
                        <li class="social-icon-7"><a target="_blank" href="<?=$arResult["arUser"]["WORK_CITY"]?>"><?=$arResult["arUser"]["WORK_CITY"]?></a></li>
                    <?endif;?>
                    <?if ($arResult["arUser"]["WORK_MAILBOX"]):?>
                        <li class="social-icon-8"><a target="_blank" href="<?=$arResult["arUser"]["WORK_MAILBOX"]?>"><?=$arResult["arUser"]["WORK_MAILBOX"]?></a></li>
                    <?endif;?>
            </ul>
    </div>
</div>  
<?endif;?>
