$(document).ready(function() {
    $("a[rel*='ajax']").click(function(){
        $.ajax({
          type: "POST",
          url: $(this).attr("href"),
          dataType: "html",
          success: function(data){
                $("#rating_overlay").html(data);
                //showExpose();
                $('#rating_overlay').overlay({
                        top: 260,                    
                        closeOnClick: false,
                        closeOnEsc: false,
                        api: true
                    }).load();
          }
        });
        return false;
    });
    // set login from user email
    $('input[name=EMAIL]').blur(function() {
        var emailVal = $(this).val();
        $('input[name=LOGIN]').val(emailVal);
    });
    $("#personal_delete a").click(function(){
       if(confirm("Вы точно хотите удалить свой аккаунт?"))
           return true;
       else
           return false;
    });
});