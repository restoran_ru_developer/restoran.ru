<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="image">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /><br />
        </div>
        <?endif;?>
        <div style="height:85px;overflow:hidden">
            <h3><?=$arItem["NAME"]?></h3>
        </div>
        <div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="small_star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
        </div>
        <?//v_dump($arItem["DISPLAY_PROPERTIES"])?>
        <div style="height:120px;overflow:hidden;margin-top:5px;">            
            
            <?if ($arItem["DISPLAY_PROPERTIES"]["type"]):?>
                <p><b><?=GetMessage("FAV_TYPE")?></b>: 
                <?
                if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]))
                    echo strip_tags(implode(", ",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]));
                else
                    echo strip_tags($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]);
                ?>
                </p>
            <?endif;?>
            <?if ($arItem["DISPLAY_PROPERTIES"]["kitchen"]):?>
                <p><b><?=GetMessage("FAV_KITCHEN")?></b>: 
                <?
                if (is_array($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))
                    echo strip_tags(implode(", ",$arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]));
                else
                    echo strip_tags($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]);
                ?>
                </p>
            <?endif;?>
            <?if ($arItem["DISPLAY_PROPERTIES"]["average_bill"]):?>
                <p><b><?=GetMessage("FAV_BILL")?></b>: 
                <?
                if (is_array($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))
                    echo strip_tags(implode(", ",$arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]));
                else
                    echo strip_tags($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]);
                ?>
                </p>
            <?endif;?>
        </div>
        <div class="clear"></div>
    </div>
<?endforeach;?>