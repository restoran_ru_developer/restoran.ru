<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // resize images
    if(!$arItem["PREVIEW_PICTURE"]) {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    }
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    // count restaurant reviews
    /*$arReviewsIB = getArIblock("reviews", CITY_ID);
    if ($arItem["PROPERTIES"]["reviews_bind"]["VALUE"])
        $arResult["ITEMS"][$key]["REVIEWS"] = getReviewsRatio($arReviewsIB["ID"],$arItem["PROPERTIES"]["reviews_bind"]["VALUE"]);*/
}
?>