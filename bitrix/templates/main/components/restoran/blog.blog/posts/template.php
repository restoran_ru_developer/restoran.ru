<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="blog <?=($key==1)?"right":"left"?>">
        <p><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]." ".$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?></p>
        <h3><?=$arItem["TITLE"]?></h3>
        <p><?$temp = explode("[CUT]",$arItem["DETAIL_TEXT"]); echo $temp[0];?></p>
        <br />
        <div class="left"><i><?=$arItem["AUTHOR"]?></i></div>
       <div class="right"><a href="#"><?=GetMessage('CT_BNL_COMMENT')?></a></div>
       <div class="clear"></div>        
    </div>
<?endforeach;?>
<div class="clear"></div>
<br />
<div align="right"><a class="uppercase" href="#"><?=GetMessage('CT_BNL_ALL')?></a></div>
