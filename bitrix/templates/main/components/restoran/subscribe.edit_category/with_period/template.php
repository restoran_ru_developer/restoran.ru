<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script src="/tpl/js/jquery.checkbox.js"></script>
<?
foreach ($arResult["MESSAGE"] as $itemID => $itemValue)
    echo ShowMessage(array("MESSAGE" => $itemValue, "TYPE" => "OK"));
foreach ($arResult["ERROR"] as $itemID => $itemValue)
    echo ShowMessage(array("MESSAGE" => $itemValue, "TYPE" => "ERROR"));

if ($arResult["ALLOW_ANONYMOUS"] == "N" && !$USER->IsAuthorized()):
    echo ShowMessage(array("MESSAGE" => GetMessage("CT_BSE_AUTH_ERR"), "TYPE" => "ERROR"));
else:
    //выбор нужных рассылок
    $allRubrics = array();
    foreach ($arResult["GROUP_LIST"] as $key => $value) {
        if ($value[0]["ID"] == 26 || $value[0]["ID"] == 27 || $value[0]["ID"] == 28) {
            $allRubrics[] = $value;
        }
    }
    $arResult["GROUP_LIST"] = $allRubrics;
    $arResult["GROUP_LIST"] = array();


    //v_dump($aPostRub);
    //v_dump($arResult["GROUP_LIST"]);
    ?>
    <script>
        $(document).ready(function(){
            $(".subscribe_submit").click(function(){
                $(this).prev().prev().attr("checked",true);
                setTimeout("$('#subscribe_edit').submit()",50);
            });
            $(".subscribe_submit_o").click(function(){
                $(this).prev().prev().attr("checked",false);
                setTimeout("$('#subscribe_edit').submit()",50); 
            });
            /*var params = {
                changedEl:"#period_select",
                visRows:3
                    
            }
            cuSel(params);
            var params = {
                changedEl:"#rubric_select",
                visRows:3
                    
            }
            cuSel(params);*/
                    
        });
    </script>
    <?
    $rsUser = $USER->GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();
    $podpiska = unserialize($arUser["WORK_STREET"]);
    $selectedRubrics = array();
    foreach ($podpiska["rubriki"] as $key => $value) {
        $selectedRubrics[$value] = $value;
    }
    //v_dump($podpiska);
    ?>
    <form action="<?= $arResult["FORM_ACTION"] ?>" method="post" id="subscribe_edit">
        <!--<input type="hidden" value="Update" name="PostAction">-->
        <input type="hidden" value="Add" name="PostAction">
        <input type="hidden" value="<? echo $arResult["SUBSCRIPTION"]["ID"]; ?>" name="ID">
        <input type="hidden" value="html" name="FORMAT">

        <? echo bitrix_sessid_post(); ?>
        Выберите город: 
        <select id="period_select" class="cusel-select" name="CITY_ID" style="width:250px">
            <option value="msk" <?
    if ($podpiska["CITY_ID"] == "msk") {
        echo 'selected';
    }
        ?>>Москва</option>
            <option value="spb" <?
                if ($podpiska["CITY_ID"] == "spb") {
                    echo 'selected';
                }
        ?>>Санкт-Петербург</option>
        </select>
        <table>
            <tbody>
                <tr>
                    <td style="padding: 5px 0px;padding-right:5px">
                        <span class="niceCheck" style="background-position: 0px 0px;">
                            <input type="checkbox" name="rubriki[]" <?
                if ($selectedRubrics["new_rests"]) {
                    echo 'checked';
                }
        ?> id="new_rests" value="new_rests">
                        </span></td><td class="option2d"><label class="niceCheckLabel">Новые рестораны</label>
                    </td> 
                </tr>
                <tr>
                    <td style="padding: 5px 0px;padding-right:5px">
                        <span class="niceCheck" style="background-position: 0px 0px;">
                            <input type="checkbox" name="rubriki[]" <?
                               if ($selectedRubrics["reviews"]) {
                                   echo 'checked';
                               }
        ?> id="reviews" value="reviews">
                        </span></td><td class="option2d"><label class="niceCheckLabel">Обзоры</label>
                    </td> 
                </tr>
                <tr>
                    <td style="padding: 5px 0px;padding-right:5px">
                        <span class="niceCheck" style="background-position: 0px 0px;">
                            <input type="checkbox" name="rubriki[]" <?
                               if ($selectedRubrics["news"]) {
                                   echo 'checked';
                               }
        ?> id="news" value="news">
                        </span></td><td class="option2d"><label class="niceCheckLabel">Новости</label>
                    </td> 
                </tr>
                <tr>
                    <td style="padding: 5px 0px;padding-right:5px">
                        <span class="niceCheck" style="background-position: 0px 0px;">
                            <input type="checkbox" name="rubriki[]" <?
                               if ($selectedRubrics["receipts"]) {
                                   echo 'checked';
                               }
        ?> id="receipts" value="receipts">
                        </span></td><td class="option2d"><label class="niceCheckLabel">Рецепты</label>
                    </td> 
                </tr>
                <tr>
                    <td style="padding: 5px 0px;padding-right:5px">
                        <span class="niceCheck" style="background-position: 0px 0px;">
                            <input type="checkbox" name="rubriki[]" <?
                               if ($selectedRubrics["photoreports"]) {
                                   echo 'checked';
                               }
        ?> id="photoreports" value="photoreports">
                        </span></td><td class="option2d"><label class="niceCheckLabel">Фотоотчеты</label>
                    </td> 
                </tr>
                <tr>
                    <td style="padding: 5px 0px;padding-right:5px">
                        <span class="niceCheck" style="background-position: 0px 0px;">
                            <input type="checkbox" name="rubriki[]" <?
                               if ($selectedRubrics["blogs"]) {
                                   echo 'checked';
                               }
        ?> id="blogs" value="blogs">
                        </span></td><td class="option2d"><label class="niceCheckLabel">Блоги</label>
                    </td> 
                </tr>
            </tbody>  
        </table>
        <div class="clear"></div>
        <? if ($USER->IsAuthorized()): ?>
            <input type="hidden" name="EMAIL" value="<? echo $arResult["SUBSCRIPTION"]["EMAIL"] != "" ? $arResult["SUBSCRIPTION"]["EMAIL"] : $arResult["REQUEST"]["EMAIL"]; ?>" class="subscription-email" />
        <? else: ?>
            Введите ваш E-Mail: <input type="text" name="EMAIL" value="" class="subscription-email" />
        <? endif; ?>
        <div class="clear"></div>
        Выберите периодичность рассылки:
        <select id="rubric_select" class="cusel-select" style="z-index: 10;" name="RUBRIC_ID">
            <option value="26" <?
    if ($podpiska["RUBRIC_ID"] == "26") {
        echo 'selected';
    }
        ?>>Раз в три дня</option>
            <option value="27" <?
                if ($podpiska["RUBRIC_ID"] == "27") {
                    echo 'selected';
                }
        ?>>Раз в неделю</option>
            <option value="28" <?
                if ($podpiska["RUBRIC_ID"] == "28") {
                    echo 'selected';
                }
        ?>>Раз в месяц</option>
        </select>

        <? if (empty($selectedRubrics)): ?>
            <input type="submit" class="subscribe_submit light" name="Save" value="<?= GetMessage("CT_BSE_BTN_ADD_SUBSCRIPTION") ?>" />
        <? else: ?>
            <input type="submit" class="subscribe_submit light" name="Save" value="<?= GetMessage("CT_BSE_BTN_EDIT_SUBSCRIPTION") ?>" />
        <? endif; ?>
        <input type="hidden" name="USER_ID" value="<? echo $USER->GetID(); ?>" />
        <? if ($_REQUEST["register"] == "YES"): ?>
            <input type="hidden" name="register" value="YES" />
        <? endif; ?>
        <? if ($_REQUEST["authorize"] == "YES"): ?>
            <input type="hidden" name="authorize" value="YES" />
        <? endif; ?>
    </form>

    <? //v_dump($arResult["GROUP_LIST"]);    ?>
<? endif; ?>