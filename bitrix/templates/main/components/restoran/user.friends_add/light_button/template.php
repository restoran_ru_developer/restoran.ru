<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
function addUser2(arParams) {
    res_cont_id = arParams.RESULT_CONTAINER_ID;
    first_user_id = arParams.FIRST_USER_ID;
    second_user_id = arParams.SECOND_USER_ID;

    function __handleraddUser(data) {
        var obContainer = document.getElementById(res_cont_id);
        if (obContainer)
        {
            obContainer.innerHTML = data;
            if (!$("#review_modal").size())
            {
                $("<div class='popup popup_modal' id='user_add_modal'></div>").appendTo("body");                                                               
            }
            $('#user_add_modal').html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><br /><div align="center">'+add_message+'</div>');
            showOverflow();
            setCenter($("#user_add_modal"));
            $("#user_add_modal").fadeIn("300"); 
            setTimeout('$("#user_add_modal").fadeOut("300"); hideOverflow("300"); ',2000);
        }
    }

    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/main/components/restoran/user.friends_add/light_button/ajax.php',
        data: arParams,
        success: __handleraddUser
    });
}
function deleteUser2(arParams) {
    res_cont_id = arParams.RESULT_CONTAINER_ID;
    first_user_id = arParams.FIRST_USER_ID;
    second_user_id = arParams.SECOND_USER_ID;

    function __handlerDeleteParams(data) {
        var obContainer = document.getElementById(res_cont_id);
        if (obContainer)
        {
            obContainer.innerHTML = data;
        }
    }

    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/main/components/restoran/user.friends_delete/light_button/ajax.php',
        data: arParams,
        success: __handlerDeleteParams
    });
}
</script>
<?if ($arParams["AJAX_CALL"] != "Y" && !$arResult["ALLREADY_FRIENDS"]):?>
        <script>
            var add_message = "<?=GetMessage('ADD_MESSAGE')?>";
        </script>
        <input onclick="addUser2(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" type="button" class="light_button" value="<?=GetMessage("USER_RECOVER_FRIEND_BUTTON")?>" />
<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["ALLREADY_FRIENDS"]=="Z"):?>
        <input class="grey_button" style="width:142px;" value="<?=GetMessage("USER_REQUEST_BUTTON")?>" />
<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["REL_ID"] && $arResult["ALLREADY_FRIENDS"]=="F"):?>
        <input onclick="if(confirm('<?=GetMessage("USER_DELETE_QUEST")?>')) deleteUser2(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" type="button" class="light_button" value="<?=GetMessage("USER_DEL_FRIEND_BUTTON")?>" />
<?endif?>