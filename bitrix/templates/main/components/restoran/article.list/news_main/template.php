<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<div class="left news_img" id="cur_news_<?=$this->GetEditAreaId($arItem['ID']);?>" >
    <a href="<?=$arItem["SECTION_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" width="470" /></a>
    <div class="clear"></div>
    <?/*if($arItem["COMMENT"]):?>
        <div class="left"><b><?=GetMessage("CT_BNL_NEWS_COMMENT_TITLE")?>: </b></div>
        <div class="right comment"><?=$arItem["COMMENT"]["POST_MESSAGE"]?></div>
        <div class="clear"></div>
        <div class="left"><?=$arItem["COMMENT"]["AUTHOR_NAME"]?></div>
        <div class="right comment_more"><a href="#"><?=GetMessage("CT_BNL_NEWS_COMMENT_SHOW_ALL")?></a></div>
    <?endif*/?>
</div>
<div class="right news_text">
    <span class="date_size"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <span><?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></span>
    <div class="title"><a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem['NAME']?></a></div>
    
    <p style="height:130px; overflow:hidden"><?=$arItem['DESCRIPTION']?></p>
    <p align="right"><a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=GetMessage("CT_BNL_NEWS_DETAIL_LINK")?></a></p>
    <div class="clear"></div>
    <br />
    <div align="right"><a class="uppercase" href="<?=$arItem["LIST_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ALL_NEWS_LINK")?></a></div>
</div>
<?endforeach;?>