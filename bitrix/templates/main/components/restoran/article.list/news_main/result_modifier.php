<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // explode date
    $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arTmpDate);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["DESCRIPTION"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["DESCRIPTION"]);
    $arResult["ITEMS"][$key]["NAME"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["NAME"]);

    if ($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>470, 'height'=>346), BX_RESIZE_IMAGE_EXACT, true);
    }
    else
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    }
   
    
    $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["DESCRIPTION"] = change_quotes($arResult["ITEMS"][$key]["DESCRIPTION"]);
}
?>