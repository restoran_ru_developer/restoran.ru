<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["SHOW_CNT_TITLE"] = "Выводить по";
$MESS["SORT_NEW_TITLE"] = "по новизне";
$MESS["SORT_PRICE_TITLE"] = "по стоимости";
$MESS["SORT_RATIO_TITLE"] = "по рейтингу";
$MESS["SORT_ALPHABET_TITLE"] = "по алфавиту";
$MESS["EMPTY_SEARCH_RESULT"] = "Увы, ничего не найдено :(";
$MESS["EMPTY_SEARCH_GO"] = "Перейти в каталог ресторанов";
?>