<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arReviewIB = getArIblock("overviews", CITY_ID);
$arMasterIB = getArIblock("master", CITY_ID);
$arPhotoIB = getArIblock("photoreports", CITY_ID);
$arKuponsIB = getArIblock("kupons", CITY_ID);
?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <?if ($key==0):?>
        <div id="content">
            <div class="left" style="width:730px">
                <h1>
                    <?if ($arParams["SET_TITLE"]=="Y"):?>
                        <?=$arResult["TITLE"]?>
                    <?else:?>
                        <?=$APPLICATION->GetTitle(false);?>
                    <?endif;?>
                </h1>
                <?
                $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE","clear_cache");
                $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=new&".(($_REQUEST["pageSort"]=="new"&&$_REQUEST["by"]=="desc"||!$_REQUEST["pageSort"]) ? "by=asc": "by=desc"), $excUrlParams);                
                $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=popular&".(($_REQUEST["pageSort"]=="popular"&&$_REQUEST["by"]=="asc"||!$_REQUEST["pageSort"]) ? "by=desc": "by=asc"), $excUrlParams);
                ?>                
                <div class="sorting">
                    <div class="left">
                        <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                        <?$by = ($_REQUEST["by"]=="asc")?"a":"z";?>
                        <?if ($_REQUEST["pageSort"] == "new" || !$_REQUEST["pageSort"]):                                                   
                            echo "<span class='".$by."'><a class='another' href='".$sortNewPage."'>".GetMessage("SORT_NEW_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a></span>';
                        endif;?>
                         / 
                        <?if ($_REQUEST["pageSort"] == "popular"):
                            echo "<span class='".$by."'><a class='another' href='".$sortPopularPage."'>".GetMessage("SORT_POPULAR_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortPopularPage.'">'.GetMessage("SORT_POPULAR_TITLE").'</a></span>';
                        endif;?>
                    </div>
                    <div class="right"><?//=$arResult["NAV_STRING"]?></div>                    
                    <div class="clear"></div>
                </div>
                <?$del = 3;
                $ost = 2?>
    <?endif;?>
    <?if ($key==9):?>
        <div id="additional_info_interview">
            <div class="block">
                <div class="interview_add left">
                    <div class="title_main" align="center">Обзоры</div>                    
                    <?$APPLICATION->IncludeComponent(
                            "restoran:article.list",
                            "interview_one",
                            Array(
                                "IBLOCK_TYPE" => "overviews",
                                "IBLOCK_ID" => $arReviewIB["ID"],
                                "SECTION_ID" => "",
                                "SECTION_CODE" => "",
                                "PAGE_COUNT" => 1,
                                "SECTION_URL" => "",
                                "PICTURE_WIDTH" => 232,
                                "PICTURE_HEIGHT" => 127,
                                "DESCRIPTION_TRUNCATE_LEN" => 200,
                                "COUNT_ELEMENTS" => "Y",
                                "TOP_DEPTH" => "1",
                                "SET_TITLE" => "Y",
                                "SECTION_FIELDS" => "",
                                "SECTION_USER_FIELDS" => Array(""),
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "",
                                "CACHE_GROUPS" => "Y",
                                "PAGER_SHOW_ALWAYS" => "Y",
                                "PAGER_TEMPLATE" => "kupon_list",	
                                "PAGER_DESC_NUMBERING" => "N",	
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            ),
                        false
                    );?>
                </div>
                <div class="interview_add left">
                    <div class="title_main" align="center">Мастер-классы</div>
                        <?$APPLICATION->IncludeComponent(
                                "restoran:article.list",
                                "interview_one",
                                Array(
                                        "IBLOCK_TYPE" => "cookery",
                                        "IBLOCK_ID" => 70,
                                        "SECTION_ID" => 238,
                                        "SECTION_CODE" => "",
                                        "PAGE_COUNT" => 1,
                                        "SECTION_URL" => "/content/cookery/detail.php?SECTION_ID=#ID#",
                                        "PICTURE_WIDTH" => 232,
                                        "PICTURE_HEIGHT" => 127,
                                        "DESCRIPTION_TRUNCATE_LEN" => 200,
                                        "COUNT_ELEMENTS" => "Y",
                                        "TOP_DEPTH" => "3",
                                        "SECTION_FIELDS" => "",
                                        "SECTION_USER_FIELDS" => Array(""),
                                        "SORT_FIELDS" => "SORT",
                                        "SORT_BY" => "ASC",
                                        "ADD_SECTIONS_CHAIN" => "Y",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_NOTES" => "",
                                        "CACHE_GROUPS" => "Y",
                                        "PAGER_SHOW_ALWAYS" => "Y",
                                        "PAGER_TEMPLATE" => "kupon_list",	
                                        "PAGER_DESC_NUMBERING" => "N",	
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",					
                                ),
                                false
                        );?> 
                </div>
                <div class="interview_add left">
                    <div class="title_main" align="center">Фотоотчеты</div>
                    <?$APPLICATION->IncludeComponent(
                            "restoran:article.list",
                            "interview_one",
                            Array(
                                "IBLOCK_TYPE" => "photoreports",
                                "IBLOCK_ID" => $arPhotoIB["ID"],
                                "SECTION_ID" => "",
                                "SECTION_CODE" => "",
                                "PAGE_COUNT" => 1,
                                "SECTION_URL" => "",
                                "PICTURE_WIDTH" => 232,
                                "PICTURE_HEIGHT" => 127,
                                "DESCRIPTION_TRUNCATE_LEN" => 200,
                                "COUNT_ELEMENTS" => "Y",
                                "TOP_DEPTH" => "1",
                                "SET_TITLE" => "Y",
                                "SECTION_FIELDS" => "",
                                "SECTION_USER_FIELDS" => Array(""),
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "",
                                "CACHE_GROUPS" => "Y",
                                "PAGER_SHOW_ALWAYS" => "Y",
                                "PAGER_TEMPLATE" => "kupon_list",	
                                "PAGER_DESC_NUMBERING" => "N",	
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            ),
                        false
                    );?>
                </div>
                <div class="interview_add_kupon kupons left end">
                    <div class="title_main" align="center">Акции</div>
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "kupon_one",
                            Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "kupons",
                                    "IBLOCK_ID" => $arKuponsIB["ID"],
                                    "NEWS_COUNT" => 1,
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => Array("ACTIVE_TO"),
                                    "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "150",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Купоны",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "kupon_list",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "PRICE_CODE" => "BASE"
                            ),
                        false
                    );?>
                </div>
                <div class="clear"></div>
            </div>
        </div>        
        <div id="content">        
            <?
            if (!$no){
                $del = 4;
                $ost = 0;
            }?>
    <?endif;?>
            <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-3)):?>
            <div class="clear"></div>
                <div class="left" style="width:730px">
                     <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "content_article_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>                 
                    <?$del = 3;
                    $ost = 1;
                    $no = 1;?>
            <?endif;?>

                <div class="new_restoraunt left<?if($key % $del == $ost):?> end<?endif?>">
                    <div style="margin-bottom:10px;">
                        <a href="#" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
                    </div>
                    <?if ($arItem["VIDEO"]&&$arParams["IBLOCK_TYPE"]=="videonews"):?>
                        <script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
                        <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.$arItem["VIDEO"]["path"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                },
                                plugins: {
                                    controls: {
                                        all: false,
                                        play: true,
                                        scrubber: true,
                                        tooltips: {
                                            buttons: true,
                                            fullscreen: 'Enter fullscreen mode'
                                        }
                                    }
                                }
                            });
                        </script>
                    <?else:?>
                        <a href="<?=$arItem["SECTION_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>
                    <?endif;?>
                    <div class="title">
                        <a href="<?=$arItem["SECTION_PAGE_URL"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a>
                    </div>
                    <p><?=$arItem["DESCRIPTION"]?></p>
                    <div class="left">Комментарии: (<a class="another" href="<?=$arItem["SECTION_PAGE_URL"]?>#comments"><?=intval($arItem["UF_COMMENTS"])?></a>)</div>
                    <div class="right"><a class="another" href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                    <div class="clear"></div>
                </div>
            <?if(($key % $del == $ost) && !(count($arResult["ITEMS"])>=20 && $key > (count($arResult["ITEMS"])-5))&&$key!=8):?>
                <div class="clear"></div>
                <div class="border_line"></div>        
            <?endif?>
    <?if ($key==8 || (count($arResult["ITEMS"])<9 && $arItem == end($arResult["ITEMS"]))):?>
            </div>
            <div class="right">
                <div id="search_article">
                    <form action="<?=$arResult["FORM_ACTION"]?>">
                        <div class="title">Найти интервью</div>
                        по автору / <a href="#" class="js another">по тэгу</a><br />
                        <div class="subscribe_block left">
                            <input class="placeholder" type="text" name="sf_EMAIL" value="<?=($arResult["EMAIL"] ? $arResult["EMAIL"] : "автор")?>" defvalue="автор" />
                        </div>
                        <div class="right">
                            <input class="subscribe" type="image" src="<?=SITE_TEMPLATE_PATH?>/images/subscribe_submit.png" name="OK" value="" title="<?=GetMessage("subscr_form_button")?>" />
                        </div>
                        <div class="clear"></div>
                    </form>            
                </div>
                <div align="right">
                    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                </div>
                <br />
                <div class="tags">
                    <?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
					"SORT" => "CNT",
					"PAGE_ELEMENTS" => "20",
					"PERIOD" => "",
					"URL_SEARCH" => "/search/index.php",
					"TAGS_INHERIT" => "Y",
					"CHECK_DATES" => "Y",
					"FILTER_NAME" => "",
					"arrFILTER" => array(
							0 => "iblock_news",
					),
					"arrFILTER_iblock_cookery" => array(
							0 => $arItem["IBLOCK_ID"],
					),
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"FONT_MAX" => "24",
					"FONT_MIN" => "12",
					"COLOR_NEW" => "24A6CF",
					"COLOR_OLD" => "24A6CF",
					"PERIOD_NEW_TAGS" => "",
					"SHOW_CHAIN" => "Y",
					"COLOR_TYPE" => "N",
					"WIDTH" => "100%"
					),
					$component
			);?> 
                   
                </div>
                <br />
                <div align="right">
                    <a href="#" class="uppercase">ВСЕ ТЕГИ</a>
                </div>
                <br />
                <div align="right">
                    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-1)):?>
            </div>
            <div class="right">
                 <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
                    );?>
            </div>
            <div class="clear"></div>
    <?endif;?>                
    <?if (count($arResult["ITEMS"])>9 && ($key+1) == count($arResult["ITEMS"])):?>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])<9 && end($arResult["ITEMS"])==$arItem):?>
        </div></div>
    <?endif;?>
<?endforeach;?>          
<div class="clear"></div>        
<div id="content">
<hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
        </div>
        <div class="right">            
            	<?=$arResult["NAV_STRING"]?>            
        </div>
    <div class="clear"></div>
    <br /><br />
    <img src="<?=SITE_TEMPLATE_PATH?>/images/catalog/direct.jpg" />
    <br /><br />
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
    );?>  
    <br /><br />
    
</div>