<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!CModule::IncludeModule("iblock"))
    return false;
foreach($arResult["ITEMS"] as $key=>&$arItem) :    
    if ($arItem["PARENT_SECTION"]["SECTION_ID"])
    {
        $r = CIBlockSection::GetById($arItem["PARENT_SECTION"]["SECTION_ID"]);
        if ($ar = $r->Fetch())
        {
            $arItem["IBLOCK_SECTION_CODE"] = $ar["CODE"];
        }   
    }
    if ($arItem["UF_COMMENTS"])
    {        
        $arItem["RATIO"] = getReviewsRatio(88,$arItem["UF_COMMENTS"]);
    }        
endforeach;
?>