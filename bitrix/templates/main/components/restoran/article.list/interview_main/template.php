<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left <?=($i%3==2)?"end":""?>">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" />        
            <div class="interview_name">
                <h3><?=$arItem["NAME"]?></h3>                
                <div class="rating">
                    <?for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                        <div class="small_star_a"></div>                                        
                    <?endfor?>
                    <?for($p;$p<5;$p++):?>
                        <div class="small_star"></div>
                    <?endfor;?>
                </div>
                <div class="clear"></div>
            </div>            
            <div class="interview_text">
                <p><?=$arItem["DESCRIPTION"]?></p>
            </div>
        <div style="margin-top:10px">
            <div class="left">Комментарии: (<a class="another" href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/#comments"><?=intval($arItem["UF_SECTION_COMM_CNT"])?></a>)</div>
            <div class="right"><a class="another" href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>                
        </div>
    </div>
    <?if ($i%3==2):?>
        <div class="clear"></div>
    <?endif;?>
    <?$i++;?>
<?endforeach;?>
<div class="clear"></div>
<?if ($arParams["PAGE_COUNT"]>1):?>
<div align="right"><a class="uppercase" href="/content/cookery/"><?=GetMessage("ALL_OPINIONS")?></a></div>
<?endif;?>