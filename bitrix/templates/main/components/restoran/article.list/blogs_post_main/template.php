<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult["ITEMS"])>0):?>
    <?foreach($arResult["ITEMS"] as $key => $arItem):?>
        <div class="new_restoraunt left<?if($key % 3 == 2):?> end<?endif?>">
            <div style="margin-bottom:10px;">
                <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
            </div>
            <!--<a href="<?=$arItem["SECTION_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>-->
            <div class="title"><a href="/<?=CITY_ID?>/blogs/<?=$arItem["CODE"]?>/"><?=$arItem["NAME"]?></a></div>
            <div class="interview_text">
                <p><?=$arItem["DESCRIPTION"]?></p>
            </div>
            <div class="left"><a href="<?=USER_PROFILE.$arItem["AUTHOR_ID"]."/"?>" class="another"><?=$arItem["AUTHOR_NAME"]?></a></div>
            <div class="right"><a class="another" href="<?=$arItem["SECTION_PAGE_URL"]?>#comments"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
            <div class="clear"></div>
        </div>
        <?if($key % 3 == 2):?>
            <div class="clear"></div>
        <?endif;?>
    <?endforeach?>
            <div class="clear"></div>
<?else:?>
    <?=GetMessage("NO_BLOGS")?>
<? endif; ?>
