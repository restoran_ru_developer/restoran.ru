<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("blog"))
    return false;

foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["DESCRIPTION"] = change_quotes($arResult["ITEMS"][$key]["DESCRIPTION"]);

    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
    {
        $arItem["AUTHOR_NAME"] = $arUser["NAME"];
        $arItem["AUTHOR_ID"] = $arUser["ID"];
    }

    // format date
    $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arTmpDate);
    
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
}
?>