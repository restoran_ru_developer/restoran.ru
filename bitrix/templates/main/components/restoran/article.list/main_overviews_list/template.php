<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
        <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
           <a href="<?=$arItem["SECTION_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
           <div style="">
                <div class="title"><a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
           </div>
           <p style=""><?=$arItem["DESCRIPTION"]?></p>
           <div class="left"><?=GetMessage("COMMENTS_TITLE")?>: (<a href="<?=$arItem["SECTION_PAGE_URL"]?>#comments"><?=intval($arItem["DISPLAY_PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"])?></a>)</div>
           <div class="right"><a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=GetMessage("SEE_FULL")?></a></div>
        </div>
<?endforeach;?>
<div class="clear"></div>
<div align="right"><a class="uppercase" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_".$arParams["IBLOCK_TYPE"])?></a></div>
