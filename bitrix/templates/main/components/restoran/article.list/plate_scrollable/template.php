<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="special_scroll">
    <div class="scroll_container">
        <?foreach($arResult["ITEMS"] as $keyItem=>$arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item <?=($keyItem==0)?"active":""?>" newsID="<?=$arItem['ID']?>" num="<?=($keyItem+1)?>">
                <div class="border"></div>
                <div class="title">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><?=$arItem["NAME"]?></td>
                        </tr>
                    </table>
                </div>
                <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="281" align="bottom" />
            </div>    
        <?endforeach;?>
    </div>
</div>