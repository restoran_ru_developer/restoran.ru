<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("blog"))
    return false;

foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);

    // count restaurant reviews
    /*$rsPostComment = CBlogComment::GetList(
        Array("ID"=>"DESC"),
        Array(
            "BLOG_ID" => 3,
            "POST_ID" => $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["reviews"]["DISPLAY_VALUE"]
        ),
        false,
        false,
        Array()
    );
    $arResult["ITEMS"][$key]["REVIEWS_CNT"] = $rsPostComment->SelectedRowsCount();*/
    //v_dump($arItem);
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
        $arItem["AUTHOR_NAME"] = $arUser["NAME"];

    // format date
    $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arTmpDate);
    
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
}
?>