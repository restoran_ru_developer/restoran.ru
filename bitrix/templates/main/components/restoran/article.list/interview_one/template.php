<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt" style="margin:0px;">
        <div class="imgborder" style="border:0px;">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232"/>
        </div>
        <div class="interview_name title" style="height:68px;">
            <a href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </div>
        <div class="interview_text" style="height:105px;">
            <p><?=$arItem["DESCRIPTION"]?>...</p>
        </div>
        <div class="left">Комментарии: (<a class="another" href="<?=$arItem["SECTION_PAGE_URL"]?>#comments"><?=intval($arItem["UF_SECTION_COMM_CNT"])?></a>)</div>
        <div class="right"><a class="another" href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
<?endforeach;?>