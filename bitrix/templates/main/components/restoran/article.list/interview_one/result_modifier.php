<?
foreach ($arResult["ITEMS"] as $key=>$arItem):
    $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["DESCRIPTION"] = change_quotes($arResult["ITEMS"][$key]["DESCRIPTION"]);
    if (!$arItem["PREVIEW_PICTURE"]["src"])
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
endforeach;
?>
