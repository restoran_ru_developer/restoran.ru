<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>
<?
$arrNewsID = Array(
    "ID" => (int)$_POST["ID"],
);

$APPLICATION->IncludeComponent(
    "restoran:article.list",
    "news_main",
    Array(
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => $arNewsIB["ID"],
        "FILTER_NAME" => "arrNewsID",
        "PAGE_COUNT" => 1,
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "PICTURE_WIDTH" => 470,
        "PICTURE_HEIGHT" => 356,
        "DESCRIPTION_TRUNCATE_LEN" => 200,
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => Array("UF_SECTION_BIND", "UF_SECTION_COMM_CNT"),
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
    ),
false
);?>
<?
/*$obCache = new CPHPCache; 
// время кеширования - 12 часов
$life_time = 0; 
$cache_id = $_POST["ID"]; 

if($obCache->InitCache($life_time, $cache_id, "/")):
    $vars = $obCache->GetVars();
    $arNews = $vars["RESULT"];
else :
    if(!CModule::IncludeModule('iblock'))
    return false;

    // get news data
    $rsNews = CIBlockSection::GetByID((int)$_POST["ID"]);
    if($arNews = $rsNews->GetNext()) {
        // get detail picture file array
        $arNews["PICTURE"] = CFile::GetFileArray($arNews["DETAIL_PICTURE"]);
        // clear name
        $arNews["NAME"] = htmlspecialchars_decode($arNews["NAME"]);
        $arNews["NAME"] = change_quotes($arNews["NAME"]);
        $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
        $arTmpDate = explode(" ", $arTmpDate);
        $arNews["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
        $arNews["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
        $arNews["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];

        // truncate preview text
        $arNews["PREVIEW_TEXT"] = strip_tags(htmlspecialchars_decode($arNews["DESCRIPTION"]));
        $end_pos = 320;
        while(substr($arNews["PREVIEW_TEXT"], $end_pos, 1) != " " && $end_pos < strlen($arNews["PREVIEW_TEXT"]))
            $end_pos++;
        if($end_pos < strlen($arNews["PREVIEW_TEXT"]))
            $arNews["PREVIEW_TEXT"] = substr($arNews["PREVIEW_TEXT"], 0, $end_pos)."...";
        $arNews["PREVIEW_TEXT"] = change_quotes($arNews["PREVIEW_TEXT"]);
        
    }
endif;

// начинаем буферизирование вывода
if($obCache->StartDataCache()):

    echo json_encode($arNews);
    $obCache->EndDataCache(array(
        "RESULT"    => $arNews
        )); 
endif;*/
?>