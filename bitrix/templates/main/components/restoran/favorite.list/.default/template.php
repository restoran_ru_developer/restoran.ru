<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?//v_dump($arItem);?>
    <div class="new_restoraunt left<?if($key%3 == 2):?> end<?endif?>">
        <?if ($arParams["PARENT_SECTION_CODE"]!="vacancy"&&$arParams["PARENT_SECTION_CODE"]!="resume"):?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" height="127" /></a>            
        <?endif;?>
        <div>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>      
            <?if ($arParams["PARENT_SECTION_CODE"]!="vacancy"&&$arParams["PARENT_SECTION_CODE"]!="resume"):?>
                <div class="rating">
                    <?for($i = 1; $i <= 5; $i++):?>
                        <div class="small_star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                    <?endfor?>
                    <div class="clear"></div>
                </div>        
            <?endif?>
        </div>
        <div>            
            <?if ($arItem["PROPERTIES"]["phone"]):?>
                <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["PROPERTIES"]["phone"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["address"]):?>
                <p><b><?=GetMessage("FAV_ADRESS")?></b>: <?=$arItem["PROPERTIES"]["address"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["subway"]):?>
                <p class="metro_<?=CITY_ID?>"><?=$arItem["PROPERTIES"]["subway"]?></p>
            <?endif;?>
            <?if ($arParams["PARENT_SECTION_CODE"]=="cookery"):?>
                <?=$arItem["PREVIEW_TEXT"]?>
            <?endif;?>
        </div>
        <div class="left">
            <i><?=intval($arItem["PROPERTIES"]["comments"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["comments"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
        <div class="del">
            <a href="?del=<?=$arItem["ID"]?>&<?=bitrix_sessid_get()?>"><?=GetMessage("FAV_DELETE")?></a>
        </div>
    </div>
	<?if ($key%3 == 2):?>
		<div class="clear"></div>
	<?endif;?>
        <?if ($key%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
                <div class="light_hr"></div>
	<?endif;?>
        <?if ($key%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
                <div class="clear"></div>
	<?endif;?>
<?endforeach;?>