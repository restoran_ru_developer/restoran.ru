<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
    <? //v_dump($arItem); ?>
    <div class="new_restoraunt left<? if ($key % 3 == 2): ?> end<? endif ?>">
        <div>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><h3><?= $arItem["NAME"] ?></h3></a>      
        </div>
        <div>            
            <? if ($arItem["EDUCATION"]): ?>
                <p><b><?= GetMessage("EDUCATION") ?></b>: <?= $arItem["EDUCATION"] ?></p>
            <? endif; ?>
            <? if ($arItem["EXPERIENCE"]): ?>
                <p><b><?= GetMessage("EXPERIENCE") ?></b>: <?= $arItem["EXPERIENCE"] ?></p>
            <? endif; ?>
            <? if ($arItem["PROPERTIES"]["WAGES_OF"]): ?>
                <p><b><?= GetMessage("WAGES") ?></b>: <?= $arItem["PROPERTIES"]["WAGES_OF"] ?> - <?= $arItem["PROPERTIES"]["WAGES_TO"] ?></p>
            <? endif; ?>
        </div>
        <div class="left">
            <i><?= intval($arItem["PROPERTIES"]["comments"]) ?> <?= pluralForm(intval($arItem["PROPERTIES"]["comments"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3")) ?></i>
        </div>
        <div class="clear"></div>
        <div class="del">
            <a href="?del=<?= $arItem["ID"] ?>&<?= bitrix_sessid_get() ?>"><?= GetMessage("FAV_DELETE") ?></a>
        </div>
    </div>
    <? if ($key % 3 == 2): ?>
        <div class="clear"></div>
    <? endif; ?>
    <? if ($key % 3 == 2 && $arItem != end($arResult["ITEMS"])): ?>
        <div class="light_hr"></div>
    <? endif; ?>
    <? if ($key % 3 != 2 && $arItem == end($arResult["ITEMS"])): ?>
        <div class="clear"></div>
    <? endif; ?>
<? endforeach; ?>