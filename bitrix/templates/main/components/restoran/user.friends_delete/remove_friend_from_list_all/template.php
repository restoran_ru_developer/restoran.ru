<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
function deleteUser(arParams) {
    res_cont_id = arParams.RESULT_CONTAINER_ID;
    first_user_id = arParams.FIRST_USER_ID;
    second_user_id = arParams.SECOND_USER_ID;

    function __handlerDeleteParams(data) {
        var obContainer = document.getElementById(res_cont_id);
        if (obContainer)
        {
            obContainer.innerHTML = data;
        }
    }

    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/main/components/restoran/user.friends_delete/remove_friend_from_list_all/ajax.php',
        data: arParams,
        success: __handlerDeleteParams
    });
}
</script>
<?if($arParams["AJAX_CALL"] != "Y"):?>

    <a onclick="if(confirm('<?=GetMessage("USER_DELETE_QUEST")?>')) deleteUser(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" href="javascript:void(null);"><?=GetMessage("USER_DELETE_FRIEND_BUTTON")?></a>

<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["REL_DEL"]):?>

    <?$APPLICATION->IncludeComponent(
        "restoran:user.friends_add",
        "add_friend_from_list_all",
        Array(
            "FIRST_USER_ID" => $arParams["FIRST_USER_ID"],
            "SECOND_USER_ID" => $arParams["SECOND_USER_ID"],
            "RESULT_CONTAINER_ID" => "friend_result_".$arParams["SECOND_USER_ID"]
        ),
        false
    );?>

<?endif?>