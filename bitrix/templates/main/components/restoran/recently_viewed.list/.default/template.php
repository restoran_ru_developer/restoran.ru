<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?/*foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?//v_dump($arItem);?>
    <div class="new_restoraunt left<?if($key%3 == 2):?> end<?endif?>">
        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="232" height="127" />
        <div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="star<?if($i <= round($arItem["DISPLAY_PROPERTIES"]["ratio"]["DISPLAY_VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
        </div>
        <div style="height:85px;overflow:hidden">
            <h3><?=$arItem["NAME"]?></h3>
        </div>
        <p style="height:80px;overflow:hidden"><?=$arItem["PREVIEW_TEXT"]?></p>
        <div class="left">
            <i><?=intval($arItem["REVIEWS_CNT"])?> <?=pluralForm(intval($arItem["REVIEWS_CNT"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
	<?if($key%3 == 2):?>
		<div class="clear"></div>
	<?endif;?>
<?endforeach;*/?>

                
<?if ($arParams["IBLOCK_TYPE"]=="vacancy"&&count($arResult["ITEMS"])>0):?>
    <h2><?=GetMessage("VACANCY_NAME")?></h2>
<?elseif($arParams["IBLOCK_TYPE"]=="resume"&&count($arResult["ITEMS"])>0):?>
    <h2><?=GetMessage("RESUME_NAME")?></h2>
<?endif;?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?//v_dump($arItem);?>
    <div class="new_restoraunt left<?if($key%3 == 2):?> end<?endif?>">
        <?if ($arParams["IBLOCK_TYPE"]!="vacancy"&&$arParams["IBLOCK_TYPE"]!="resume"):?>
            <div style="height:127px; overflow:hidden">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>            
            </div>
        <?endif;?>
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>        
        <?if ($arParams["IBLOCK_TYPE"]!="vacancy"&&$arParams["IBLOCK_TYPE"]!="resume"):?>
            <div class="rating">
                <?for($i = 1; $i <= 5; $i++):?>
                    <div class="small_star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                <?endfor?>
                <div class="clear"></div>
            </div>        
        <?endif;?>
        <div style="margin-top:5px;">            
            <?if ($arItem["PROPERTIES"]["phone"]):?>
                <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["PROPERTIES"]["phone"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["address"]):?>
                <p><b><?=GetMessage("FAV_ADRESS")?></b>: <?=$arItem["PROPERTIES"]["address"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["subway"]):?>
                <p class="metro_<?=CITY_ID?>"><?=$arItem["PROPERTIES"]["subway"]?></p>
            <?endif;?>
            <?if ($arParams["IBLOCK_TYPE"]=="cookery"):?>
                <?=$arItem["PREVIEW_TEXT"]?>
            <?endif;?>
                
                
            <?if ($arItem["PROPERTIES"]["WAGES_OF"]):?>
                <p><b><?=GetMessage("FAV_WAGES_OF")?></b>: <?=$arItem["PROPERTIES"]["WAGES_OF"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["EXPERIENCE"]):?>
                <p><b><?=GetMessage("FAV_EXPERIENCE")?></b>: <?=$arItem["PROPERTIES"]["EXPERIENCE"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["COMPANY_FIO"]):?>
                <p><b><?=GetMessage("FAV_EXPERIENCE")?></b>: <?=$arItem["PROPERTIES"]["COMPANY_FIO"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["ADDRESS"]):?>
                <p><b><?=GetMessage("FAV_ADRESS")?></b>: <?=$arItem["PROPERTIES"]["ADDRESS"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["CONTACT_PHONE"]):?>
                <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["PROPERTIES"]["CONTACT_PHONE"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["SUBWAY_BIND"]):?>
                <p class="metro_<?=CITY_ID?>"><?=$arItem["PROPERTIES"]["SUBWAY_BIND"]?></p>
            <?endif;?>            
        </div>
        <div class="left">
            <i><?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
	<?if ($key%3 == 2):?>
		<div class="clear"></div>
	<?endif;?>
        <?if ($key%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
                <div class="light_hr"></div>
	<?endif;?>
        <?if ($key%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
                <div class="clear"></div>
	<?endif;?>
<?endforeach;?>