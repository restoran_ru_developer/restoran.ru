<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <table class="profile-activity" width="100%" cellpadding="10">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?> 
        <tr>
                <td width="150" class="profile-activity-col1"><p><?=GetMessage("TYPE_".$arItem["IBLOCK_TYPE_ID"])?>
                        <br /><?=$arItem["CREATED_DATE_FORMATED_1"]?></p>
                    <?if ($arItem["TRANSACT"]):?>
                        <p><i><?=$arItem["TRANSACT"]["DESCRIPTION"]?></i></p>
                    <?endif;?>                    
                </td>
                <td width="20"><?=($arItem["ACTIVE"]=="Y")?"<img src='".$templateFolder."/images/lamp_active.png' width='20' />":"<img src='".$templateFolder."/images/lamp_inactive.png' width='20' />"?></td>
                <!--<td class="profile-activity-col11 <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="100" />
                </td>-->
                <td width="150" class="profile-activity-col2 <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">                            
                        <p><a class="another" target="_blank" href="http://restoran.ru<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
                        <p class="upcase"><?=GetMessage("LINKTYPE_".$arItem["LINK_IBLOCK_TYPE"])?></p>
                </td>
                <td width="200" class="profile-activity-col3 <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>"><?=$arItem["PREVIEW_TEXT"]?></td>
        </tr>    
    <?endforeach;?>           
    </table>