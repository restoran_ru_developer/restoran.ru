<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!CModule::IncludeModule('iblock'))
    return false;

// get news data
$rsNews = CIBlockElement::GetByID($_POST["ID"]);
if($arNews = $rsNews->GetNext()) {
    // get detail picture file array
    $arNews["PICTURE"] = CFile::GetFileArray($arNews["DETAIL_PICTURE"]);
    // clear name
    $arNews["NAME"] = htmlspecialchars_decode($arNews["NAME"]);

    // explode date
    $arNews["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arNews["ACTIVE_FROM"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arNews["DISPLAY_ACTIVE_FROM"]);
    $arNews["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arNews["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arNews["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];

    // truncate preview text
    $arNews["PREVIEW_TEXT"] = strip_tags(htmlspecialchars_decode($arNews["PREVIEW_TEXT"]));
    $end_pos = 320;
    while(substr($arNews["PREVIEW_TEXT"], $end_pos, 1) != " " && $end_pos < strlen($arNews["PREVIEW_TEXT"]))
        $end_pos++;
    if($end_pos < strlen($arNews["PREVIEW_TEXT"]))
        $arNews["PREVIEW_TEXT"] = substr($arNews["PREVIEW_TEXT"], 0, $end_pos)."...";

    // get rest name
    $rsSec = CIBlockSection::GetByID($arNews["IBLOCK_SECTION_ID"]);
    if($arSec = $rsSec->GetNext()) {
        $arNews["RESTAURANT_NAME"] = htmlspecialchars_decode($arSec["NAME"]);
    }

    echo json_encode($arNews);
}
?>