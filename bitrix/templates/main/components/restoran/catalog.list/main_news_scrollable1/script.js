jQuery.fn.news_galary = function(options){
    var options = jQuery.extend({speed:1000, content_width:978},options);

    var container = jQuery(this).find(".special_scroll");
    var item = container.find(".item");
    var item_width = item.width()*1+item.css("margin-right").replace("px","")*1+item.css("padding-right").replace("px","")*1+item.css("padding-left").replace("px","")*1;
    var all_width = container.width();
    var delta_width = Math.round((all_width - options.content_width)/2);

    this.activate = function(obj) {
        if (container.find(".active:visible").next().hasClass("item")) {
            container.find(".active:visible").removeClass("active");
            obj.addClass("active");
        }
        else {
            container.find(".active:visible").removeClass("active");
            obj.addClass("active");
        }
    };

    this.next = function(obj) {
        this.activate(obj);
        var left = item_width*(obj.attr("num")-1)-delta_width;
        $(this).find(".scroll_container").animate({"marginLeft":-left},options.speed);
    };

    this.slide = function(a) {
        $(this).find(".scroll_container").animate({"marginLeft":-a},options.speed);
    };

    this.init = function() {
        var _this = this;
        container.find(".item").click(function(){
            _this.next($(this));
        });

        $(window).resize(function() {
            item_width = item.width()*1+item.css("margin-right").replace("px","")*1+item.css("padding-right").replace("px","")*1+item.css("padding-left").replace("px","")*1;
            all_width = container.width();
            delta_width = Math.round((all_width - options.content_width)/2);
        });
    };
    return this.init();
}



var index=999;
$(document).ready(function() {
    $(".rest_news_cont").news_galary();
    // set def active news
   ///////////////// var newsID = $('.news_img').attr('id').replace('cur_news_', '');

    // get and show news info
    $('.rest_news_cont .item').on('click', function() {
        var newsID = $(this).attr('newsID');
        var newsData = '{"ID": "'+ newsID +'"}';
        send_ajax('/bitrix/templates/main/components/bitrix/news.list/main_news_scrollable/get_news.php', false, newsData , resNewsAjax);
    });
});

function resNewsAjax(data) {
    /* set info to container */
    // set picture
    $('.news_img:visible > img').attr('src', data.PICTURE.SRC);
    // set date
    $('.news_text:visible > span:eq(0)').text(data.DISPLAY_ACTIVE_FROM_DAY);
    $('.news_text:visible > span:eq(1)').text(data.DISPLAY_ACTIVE_FROM_MONTH + " " + data.DISPLAY_ACTIVE_FROM_YEAR);
    // set news title
    $('.news_text:visible h3 > span:eq(0)').text(data.RESTAURANT_NAME);
    $('.news_text:visible h3 > span:eq(1)').text(data.NAME);
    // set preview text
    $('.news_text:visible > p:eq(0)').text(data.PREVIEW_TEXT);
}