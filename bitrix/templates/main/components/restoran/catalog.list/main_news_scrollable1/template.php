<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="special_scroll">
    <div class="scroll_container">
        <?foreach($arResult["ITEMS"] as $keyItem=>$arItem):?>
            <div class="item <?=($keyItem==0)?"active":""?>" newsID="<?=$arItem['ID']?>" num="<?=($keyItem+1)?>">
                <div class="border"></div>
                <div class="title">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><?=$arItem["RESTAURANT_NAME"]?></td>
                        </tr>
                    </table>
                    
                </div>
                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="241" height="181" align="bottom" />
            </div>    
        <?endforeach;?>
    </div>
</div>