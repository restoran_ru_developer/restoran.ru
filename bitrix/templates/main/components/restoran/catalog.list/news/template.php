<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/flowplayer.js"></script>
<?
if (LANGUAGE_ID!="en"):
if (CITY_ID=="tmn")
    $anons = Array("overviews", "photoreports", "cookery", "news","interview");
elseif (CITY_ID=="kld")
    $anons = Array("news_overviews","blogs", "cookery", "news","news_photo");
elseif (CITY_ID=="nsk")
    $anons = Array("news_restvew","blogs", "cookery", "news","news_photo");
elseif (CITY_ID=="ufa")
    $anons = Array();
elseif (CITY_ID=="rga"||CITY_ID=="urm")
    $anons = Array();
else
    $anons = Array("overviews", "photoreports", "cookery", "news", "blogs");
$iblock_type = Array($arResult["ITEMS"][0]["IBLOCK_TYPE_ID"]);
$diff = array_diff($anons, $iblock_type);
$p = 0;
foreach ($diff as $d) {
    if ($p < 4)
        $dif[] = $d;
    $p++;
}
foreach ($dif as $as) {
    if ($as == "news_overviews"||$as == "news_photo"||$as == "news_restvew")
        $as = "news";
    $arAnons[] = getArIblock($as, CITY_ID);    
}

endif;
?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <? if ($key == 0): ?>
            <div id="content">
                <div class="left" style="width:730px; position: relative">                                      
            <? if ($APPLICATION->GetCurDir() == "/msk/news/maslenitsa/"||$APPLICATION->GetCurDir() == "/spb/news/maslenitsa/"): ?>
                        <img src="/tpl/images/maslenica_new.jpg" />
                    <? endif; ?>                        
                    <?if ($APPLICATION->GetCurDir()=="/msk/news/23_fevralya/"||$APPLICATION->GetCurDir()=="/spb/news/23_fevralya/"):?>
                        <img src="/tpl/images/23february.jpg" />
                    <?endif;?>      
                    <?if ($APPLICATION->GetCurDir()=="/msk/news/paskha/"||$APPLICATION->GetCurDir()=="/spb/news/paskha/"):?>
                        <img src="/tpl/images/pacha.jpg" />
                    <?endif;?>                                                
                    <?/*if ($APPLICATION->GetCurDir()=="/msk/news/8marta/"||$APPLICATION->GetCurDir()=="/spb/news/8marta/"):?>
                        <img src="/tpl/images/8marta_long.jpg" />
                    <?endif;*/?>                        
                    <?if ($APPLICATION->GetCurDir()=="/spb/news/vostok_novyy_god/"||$APPLICATION->GetCurDir()=="/msk/news/vostochnyy_novyy_god/"):?>
                        <img src="/tpl/images/China.png" />
                    <?endif;?>
                    <?if ($APPLICATION->GetCurDir()=="/msk/news/novogodnie_yelki/"||$APPLICATION->GetCurDir()=="/spb/news/yelki/"):?>
                        <img src="/tpl/images/elki_bg.jpg" />
                        <?if (CITY_ID=="msk"):?>
                            <p>Московские рестораны готовятся встречать и радовать малышей! Сезон праздничных и сказочных представлений в ресторанах начнется 22 декабря.
    Выбирайте, куда пойдете вы!</p>
                        <?endif;?>
                    <?endif;?>
                    <?if ($APPLICATION->GetCurDir()=="/spb/news/tatyanin_den/"||$APPLICATION->GetCurDir()=="/msk/news/tatyanin/"):?>
                        <img src="/tpl/images/student.png" /><Br /><BR />
                        <p>
                            В Татьянин день рестораны расцветают! Яркая и непосредственная молодежь перебирается из скучных аудиторий в теплые и душевные кафе, где шумными компаниями и отмечают свой день — День студента! 
В этот день многие рестораны предлагают студенческие скидки, акции, придумывают специальные развлекательные программы — мы собрали для вас некоторые из них.
                        </p>
                        
                    <?endif;?>
                    <?if ($APPLICATION->GetCurDir()=="/msk/news/oktoberfest/"||$APPLICATION->GetCurDir()=="/spb/news/oktoberfest/"):?>
                        <img src="/tpl/images/oktoberfest.jpg" style="margin-bottom:10px;" />
                    <?endif;?>
                    <?if ($APPLICATION->GetCurDir()=="/msk/news/halloween/"||$APPLICATION->GetCurDir()=="/spb/news/halloween/"):?>
                        <img src="/tpl/images/halloween.jpg" style="margin-bottom:10px;" />
                    <?endif;?>
                    <?if($APPLICATION->GetCurDir()=="/msk/news/staryy_novyy_god/"):?>
                        <p>Череда новогодних праздников продолжается: сегодня в Москве выпал первый в этом году снег, а также пришло время отметить Старый Новый год! И понедельник — это еще не повод увильнуть от празднования. Торопитесь развлечься по полной программе этим вечером, ведь следующий по плану Новый год (на этот раз китайский) у нас только в феврале! </p>
                    <?endif;?>
                    <h1>
                    <? if ($arParams["SET_TITLE"] == "Y" && $arResult["IBLOCK_TYPE"] != "Рецепты"): ?>
                            <?
                            if ($arResult["SECTION"]["PATH"][0]["NAME"] == "Новые рестораны")
                                echo $arResult["SECTION"]["PATH"][0]["NAME"];
                            else
                                echo $arResult["IBLOCK_TYPE"]
                                ?>
                        <? else: ?>
                            <?= $APPLICATION->GetTitle(); ?>
                        <? endif; ?>
                    </h1>
                    <?
                    $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE", "clear_cache");
                    $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "") . "pageSort=ACTIVE_FROM&" . (($_REQUEST["pageSort"] == "ACTIVE_FROM" && $_REQUEST["by"] == "desc" || !$_REQUEST["pageSort"]) ? "by=asc" : "by=desc"), $excUrlParams);
                    $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "") . "pageSort=PROPERTY_summa_golosov&" . (($_REQUEST["pageSort"] == "PROPERTY_summa_golosov" && $_REQUEST["by"] == "asc" || !$_REQUEST["pageSort"]) ? "by=desc" : "by=asc"), $excUrlParams);
                    ?>                
                    <div class="sorting" <?if ($APPLICATION->GetCurDir()=="/msk/news/staryy_novyy_god/") echo "style=top:65px;"?><? if ($APPLICATION->GetCurDir() == "/msk/news/paskha/"||$APPLICATION->GetCurDir() == "/spb/news/paskha/" ) echo "style='top:95px;'" ?> <?=($APPLICATION->GetCurDir() == "/msk/news/oktoberfest/"||$APPLICATION->GetCurDir() == "/spb/news/oktoberfest/"||$APPLICATION->GetCurDir() == "/".CITY_ID."/news/halloween/"||$APPLICATION->GetCurDir() == "/spb/news/yelki/"||$APPLICATION->GetCurDir()=="/spb/news/vostok_novyy_god/"||$APPLICATION->GetCurDir()=="/msk/news/vostochnyy_novyy_god/"||$APPLICATION->GetCurDir() == "/msk/news/maslenitsa/"||$APPLICATION->GetCurDir() == "/spb/news/maslenitsa/"||$APPLICATION->GetCurDir() == "/msk/news/23_fevralya/"||$APPLICATION->GetCurDir() == "/spb/news/23_fevralya/"||$APPLICATION->GetCurDir() == "/spb/news/8marta/"||$APPLICATION->GetCurDir() == "/spb/news/8marta/")?"style='top:250px;'":""?> <?=($APPLICATION->GetCurDir() == "/msk/news/novogodnie_yelki/")?"style='top:280px;'":""?> <?=($APPLICATION->GetCurDir() == "/spb/news/tatyanin_den/" || $APPLICATION->GetCurDir() == "/msk/news/tatyanin/" )?"style='top:330px;'":""?>>
                        <div class="left">
                            <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                            <? $by = ($_REQUEST["by"] == "asc") ? "a" : "z"; ?>
                            <?
                            if ($_REQUEST["pageSort"] == "ACTIVE_FROM" || !$_REQUEST["pageSort"]):
                                echo "<span class='" . $by . "'><a class='another' href='" . $sortNewPage . "'>" . GetMessage("SORT_NEW_TITLE") . "</a></span>";
                            else:
                                echo '<span><a class="another" href="' . $sortNewPage . '">' . GetMessage("SORT_NEW_TITLE") . '</a></span>';
                            endif;
                            ?>
                            / 
                            <?
                            if ($_REQUEST["pageSort"] == "PROPERTY_summa_golosov"):
                                echo "<span class='" . $by . "'><a class='another' href='" . $sortPopularPage . "'>" . GetMessage("SORT_POPULAR_TITLE") . "</a></span>";
                            else:
                                echo '<span><a class="another" href="' . $sortPopularPage . '">' . GetMessage("SORT_POPULAR_TITLE") . '</a></span>';
                            endif;
                            ?>
                        </div>
                        <div class="right"><? //=$arResult["NAV_STRING"] ?></div>                    
                        <div class="clear"></div>
                    </div>

                    <? $del = 3;
                    $ost = 2
                    ?>
                        <? endif; ?>
                        <? if ($key == 9): ?>
            <? if (SITE_ID != "s2"&&CITY_ID!="ufa"&&CITY_ID!="rga"&&CITY_ID!="urm"&&CITY_ID!="nsk"): ?>
                        <div id="additional_info_interview">
                            <div class="block">
                                    <? foreach ($arAnons as $key => $anons): ?>
                                
                                    <div class="interview_add left">   
                                        <?if ($dif[$key]=="blogs"&&CITY_ID=="kld"):?>
                                            <div class="title_main" align="center">Блоги</div>  
                                        <?else:?>
                                            <div class="title_main" align="center"><?= GetMessage("ANONS_" . $dif[$key])?></div>  
                                        <?endif;?>
                                        <?
                                        $arrAddFilter = array();
                                        if ($dif[$key] == "blogs") {
                                            global $arrAddFilter;
                                            if (CITY_ID == "spb")
                                                $arrAddFilter = Array("CREATED_BY" => 65);
                                            elseif (CITY_ID == "msk")
                                                $arrAddFilter = Array("CREATED_BY" => 93);
                                            else
                                            {
                                                
                                            }
                                        }
                                        if ($dif[$key] == "news_overviews") {
                                            global $arrAddFilter;                                            
                                            $arrAddFilter = Array("SECTION_ID" => 215534);
                                            $dif[$key] = "news";                                            
                                        }
                                        if ($dif[$key] == "news_photo") {
                                            global $arrAddFilter;                                            
                                            $arrAddFilter = Array("SECTION_ID" => 215535);
                                            $dif[$key] = "news";
                                        }
                                        ?>
                                        <?
                                        $APPLICATION->IncludeComponent(
                                                "restoran:catalog.list", "interview_one_with_border", Array(
                                            "DISPLAY_DATE" => "N",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "N",
                                            "AJAX_MODE" => "N",
                                            "IBLOCK_TYPE" => $dif[$key],
                                            "IBLOCK_ID" => ($dif[$key] == "cookery") ? 145 : $anons["ID"],
                                            "NEWS_COUNT" => 1,
                                            "SORT_BY1" => "ID",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_BY2" => "",
                                            "SORT_ORDER2" => "",
                                            "FILTER_NAME" => "arrAddFilter",
                                            "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                            "PROPERTY_CODE" => array("ratio", "COMMENTS"),
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "PREVIEW_TRUNCATE_LEN" => "120",
                                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                            "SET_TITLE" => "Y",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_FILTER" => "Y",
                                            "CACHE_GROUPS" => "N",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "Новости",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_TEMPLATE" => "search_rest_list",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N"
                                                ), false
                                        );
                                        ?>
                                    </div>
                                <? endforeach; ?>                
                                <!--<div class="interview_add_kupon kupons left">
                                    <div class="title_main" align="center">Акции</div>
                                <? /* $APPLICATION->IncludeComponent(
                                  "bitrix:news.list",
                                  "kupon_one",
                                  Array(
                                  "DISPLAY_DATE" => "N",
                                  "DISPLAY_NAME" => "Y",
                                  "DISPLAY_PICTURE" => "Y",
                                  "DISPLAY_PREVIEW_TEXT" => "Y",
                                  "AJAX_MODE" => "N",
                                  "IBLOCK_TYPE" => "kupons",
                                  "IBLOCK_ID" => $arKuponsIB["ID"],
                                  "NEWS_COUNT" => 1,
                                  "SORT_BY1" => "ACTIVE_FROM",
                                  "SORT_ORDER1" => "DESC",
                                  "SORT_BY2" => "SORT",
                                  "SORT_ORDER2" => "ASC",
                                  "FILTER_NAME" => "",
                                  "FIELD_CODE" => Array("ACTIVE_TO"),
                                  "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                                  "CHECK_DATES" => "Y",
                                  "DETAIL_URL" => "",
                                  "PREVIEW_TRUNCATE_LEN" => "150",
                                  "ACTIVE_DATE_FORMAT" => "j F Y",
                                  "SET_TITLE" => "N",
                                  "SET_STATUS_404" => "N",
                                  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                  "ADD_SECTIONS_CHAIN" => "N",
                                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                  "PARENT_SECTION" => "",
                                  "PARENT_SECTION_CODE" => "",
                                  "CACHE_TYPE" => "A",
                                  "CACHE_TIME" => "36000000",
                                  "CACHE_FILTER" => "Y",
                                  "CACHE_GROUPS" => "N",
                                  "DISPLAY_TOP_PAGER" => "N",
                                  "DISPLAY_BOTTOM_PAGER" => "Y",
                                  "PAGER_TITLE" => "Купоны",
                                  "PAGER_SHOW_ALWAYS" => "N",
                                  "PAGER_TEMPLATE" => "kupon_list",
                                  "PAGER_DESC_NUMBERING" => "N",
                                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                  "PAGER_SHOW_ALL" => "N",
                                  "AJAX_OPTION_JUMP" => "N",
                                  "AJAX_OPTION_STYLE" => "Y",
                                  "AJAX_OPTION_HISTORY" => "N",
                                  "PRICE_CODE" => "BASE"
                                  ),
                                  false
                                  ); */ ?>
                                </div>-->
                                <div class="clear"></div>
                            </div>
                        </div>
                        <? endif; ?>
                    <div id="content">        
                        <?
                        if (!$no) {
                            $del = 4;
                            $ost = 0;
                        }
                        ?>
                        <? endif; ?>
                        <? if (count($arResult["ITEMS"]) >= 20 && $key == (count($arResult["ITEMS"]) - 3)): ?>
                        <div class="clear"></div>
                        <div class="left" style="width:730px">
                            <?
                            $APPLICATION->IncludeComponent(
                                    "bitrix:advertising.banner", "", Array(
                                "TYPE" => "content_article_list",
                                "NOINDEX" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                                    ), false
                            );
                            ?>                 
                            <?
                            $del = 3;
                            $ost = 1;
                            $no = 1;
                            ?>
        <? endif; ?>

                        <div class="new_restoraunt left<? if ($key % $del == $ost): ?> end<? endif ?>">
                            <div style="margin-bottom:10px;">
                                <a href="/users/id<?= $arItem["CREATED_BY"] ?>/" class="another"><?= $arItem["AUTHOR_NAME"] ?></a>,  <span class="statya_date"><?= $arItem["CREATED_DATE_FORMATED_1"] ?></span> <?= $arItem["CREATED_DATE_FORMATED_2"] ?>
                            </div>
        <? if ($arItem["VIDEO"]): ?>
                                <a class="myPlayer" href="<?= $arItem["VIDEO"] ?>"></a> 
                                <script>
                                    flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                        clip: {
                                            autoPlay: false, 
                                            autoBuffering: true
                                        },
                                        plugins: {
                                            controls: {
                                                all: false,
                                                play: true,
                                                scrubber: true,
                                                tooltips: {
                                                    buttons: true,
                                                    fullscreen: 'Enter fullscreen mode'
                                                }
                                            }
                                        }
                                    });
                                </script>
        <? elseif ($arItem["VIDEO_PLAYER"]): ?>  
                                <?= $arItem["VIDEO_PLAYER"] ?>
                            <? else: ?>
                                <!--<div style="overflow:hidden; height:127px">-->
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"] ?>" width="232" /></a>
                                <!--</div>-->
        <? endif; ?>
                            <div class="title">
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a>
                            </div>
                            <p><?= $arItem["PREVIEW_TEXT"] ?></p>                                        
                            <div class="left"><?= GetMessage("COMMENTS") ?>: (<a class="another" href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments"><?= intval($arItem["PROPERTIES"]["COMMENTS"]) ?></a>)</div>
                            <div class="right"><a class="another" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS") ?></a></div>
                            <div class="clear"></div>
                        </div>
                        <? if (($key % $del == $ost) && !(count($arResult["ITEMS"]) >= 20 && $key > (count($arResult["ITEMS"]) - 5)) && $key != 8): ?>
                            <div class="clear"></div>
                            <div class="border_line"></div>        
        <? endif ?>
        <? if ($key == 8 || (count($arResult["ITEMS"]) < 9 && $arItem == end($arResult["ITEMS"]))): ?>
                        </div>
                        <div class="right">
                            <!--<div id="search_article">
                                <form action="<?= $arResult["FORM_ACTION"] ?>">
                                    <div class="title">Найти интервью</div>
                                    по автору / <a href="#" class="js another">по тэгу</a><br />
                                    <div class="subscribe_block left">
                                        <input class="placeholder" type="text" name="sf_EMAIL" value="<?= ($arResult["EMAIL"] ? $arResult["EMAIL"] : "автор") ?>" defvalue="автор" />
                                    </div>
                                    <div class="right">
                                        <input class="subscribe" type="image" src="<?= SITE_TEMPLATE_PATH ?>/images/subscribe_submit.png" name="OK" value="" title="<?= GetMessage("subscr_form_button") ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </form>            
                            </div>-->
                            <div align="right">
                                <?
                                $APPLICATION->IncludeComponent(
                                        "bitrix:advertising.banner", "", Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                                        ), false
                                );
                                ?>
                            </div>
                            <br />                                     
                            <div id="subscribe">                                     
                            </div>                            
                            <div class="tags">
                                <?
                                if (LANGUAGE_ID == 'ru') {
                                    $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                                        "SORT" => "CNT",
                                        "PAGE_ELEMENTS" => "20",
                                        "PERIOD" => "",
                                        "URL_SEARCH" => "/search/index.php",
                                        "TAGS_INHERIT" => "Y",
                                        "CHECK_DATES" => "Y",
                                        "FILTER_NAME" => "",
                                        "arrFILTER" => array(
                                            0 => "iblock_" . $arItem["IBLOCK_TYPE_ID"],
                                        ),
                                        "arrFILTER_iblock_" . $arItem["IBLOCK_TYPE_ID"] => array(
                                            0 => $arItem["IBLOCK_ID"],
                                        ),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "86400",
                                        "FONT_MAX" => "24",
                                        "FONT_MIN" => "12",
                                        "COLOR_NEW" => "24A6CF",
                                        "COLOR_OLD" => "24A6CF",
                                        "PERIOD_NEW_TAGS" => "",
                                        "SHOW_CHAIN" => "Y",
                                        "COLOR_TYPE" => "N",
                                        "SEARCH_IN" => $arItem["IBLOCK_TYPE_ID"],
                                        "WIDTH" => "100%"
                                            ), $component
                                    );
                                }
                                ?> 

                            </div>                
                            <div align="right">
                                <?
                                $APPLICATION->IncludeComponent(
                                        "bitrix:advertising.banner", "", Array(
                                    "TYPE" => "right_1_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                                        ), false
                                );
                                ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                <? endif; ?>
                <? if (count($arResult["ITEMS"]) >= 20 && $key == (count($arResult["ITEMS"]) - 1)): ?>
                </div>
                <div class="right">
                    <?
                    $APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner", "", Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                            ), false
                    );
                    ?>
                </div>
                <div class="clear"></div>
        <? endif; ?>                
            <? if (count($arResult["ITEMS"]) > 9 && ($key + 1) == count($arResult["ITEMS"])): ?>
            </div>
            <? endif; ?>
        <? if (count($arResult["ITEMS"]) < 9 && end($arResult["ITEMS"]) == $arItem): ?>
            <!--</div></div>-->
        <? endif; ?>
    <? endforeach; ?>    
<? else: ?>
    <div id="content">
        <p><font class="notetext"><?= GetMessage("NOTHING_TO_FOUND") ?></font></p>
    </div>
<? endif; ?>
<div class="clear"></div>        
<div id="content">
<? if ($arResult["NAV_STRING"]): ?>
        <hr class="bold" />        
        <div class="left">
    <?= GetMessage("SHOW_CNT_TITLE") ?>&nbsp;
            <?= ($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page=' . $_REQUEST["PAGEN_1"] . ($_REQUEST["pageSort"] ? "&pageSort=" . $_REQUEST["pageSort"] : "") . '">20</a>') ?>
            <?= ($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page=' . $_REQUEST["PAGEN_1"] . '&pageCnt=40' . ($_REQUEST["pageSort"] ? "&pageSort=" . $_REQUEST["pageSort"] : "") . '">40</a>') ?>
            <?= ($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page=' . $_REQUEST["PAGEN_1"] . '&pageCnt=60' . ($_REQUEST["pageSort"] ? "&pageSort=" . $_REQUEST["pageSort"] : "") . '">60</a>') ?>
        </div>
        <div class="right">            
    <?= $arResult["NAV_STRING"] ?>            
        </div>
        <div class="clear"></div>
<? endif; ?>
    <br /><br />
    <div id="yandex_direct">
        <script type="text/javascript"> 
            //<![CDATA[
            yandex_partner_id = 47434;
            yandex_site_bg_color = 'FFFFFF';
            yandex_site_charset = 'utf-8';
            yandex_ad_format = 'direct';
            yandex_font_size = 1;
            yandex_direct_type = 'horizontal';
            yandex_direct_limit = 4;
            yandex_direct_title_color = '24A6CF';
            yandex_direct_url_color = '24A6CF';
            yandex_direct_all_color = '24A6CF';
            yandex_direct_text_color = '000000';
            yandex_direct_hover_color = '1A1A1A';
            document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
            //]]>
        </script>
    </div>
    <br /><br />
<?
$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner", "", Array(
    "TYPE" => "bottom_rest_list",
    "NOINDEX" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "0"
        ), false
);
?>  
    <br /><br />
</div>
<div class="clear"></div>