<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["NEW_ITEMS"] as $cell=>$arI):?>
    <div id="tabs_block<?=$cell?>">
        <ul class="tabs">
            <?foreach($arI as $key=>$arItem):
                //plate_scrollable holiday_scrollable news_scrollable
                ?>
            <li>
                <a href="#">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=$arItem["NAME"]?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <?endforeach;?>
        </ul>
        <!-- tab "panes" -->
        <div class="panes">
        <?foreach($arI as $key=>$arItem):?>
        <div class="pane">
            <?
            $filter = "arrNoFilter_".$arItem["CODE"];
            if (is_array($arItem["PROPERTIES"]["ELEMENTS"]))
            {                        global $$filter;
                    $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE_ID"]);
            }
            ?>
            <?
            switch($arItem["PROPERTIES"]["IBLOCK_TYPE"])
            {
                case "news":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "new_rest_main",
                        Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                            "NEWS_COUNT" => "3",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => ($$filter)?$filter:"",
                            "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array("ratio","reviews_quantity"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"],
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "43200",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "ratings":
                    if ($arItem["CODE"]=='ratings1')
                    {
                        $arIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "new_rest_main",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "PROPERTY_RATIO",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio","reviews_quantity"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );
                        echo "<div class='clear'></div>";
                    }
                    elseif ($arItem["CODE"]=='ratings2')
                    {
                        $arIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "new_rest_main",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "show_counter",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio","reviews_quantity"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );
                        echo "<div class='clear'></div>";
                    }
                    if ($arItem["CODE"]=='ratings3')
                    {
                        $arIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "new_rest_main",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "PROPERTY_restoran_ratio",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio","reviews_quantity"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );
                        echo "<div class='clear'></div>";
                    }
                break;
                case "videonews":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "videonews_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => "2",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("LIST_PAGE_URL"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );   
                break;
                case "kupons":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "kupons",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("ACTIVE_TO"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS","subway"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "100",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "overviews":             
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "reviews":
                case "comment":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "reviews_main",
                        Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "DATE_CREATE",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DATE_CREATE"),
                                "PROPERTY_CODE" => array("COMMENTS","ELEMENT"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "300",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                    );
                break;
                case "cookery":         
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
            case "photoreports":
            ?>
                <div id="photogalery" class="photogalery">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "photoreports",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => "3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                                "PROPERTY_CODE" => array("PHOTOS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );?>
                    </div>
                <?echo "<div class='clear'></div>";
                break;
                case "interview":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "afisha":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "afisha_list_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => "5",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("EVENT_TYPE","TIME"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                        false
                        );
                    echo "<div class='clear'></div>";
                break;
                case "blogs":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "43200",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );                   
                break;
                default:

                break;
            }


            ?>
        </div>
        <?endforeach;?>
        </div>
    </div>
<?endforeach;?>