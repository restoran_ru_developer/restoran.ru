<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arResult["NEW"][$arItem["IBLOCK_SECTION_ID"]][] = $arItem;
}
foreach ($arResult["NEW"] as $sect =>$item)
{
    $sections[] = $sect;
}
if (is_array($sections))
{
    CModule::IncludeModule("iblock");
    $res = CIBlockSection::GetList(Array("SORT"=>"ASC"),Array("ID"=>$sections),false);
    while ($ar=$res->Fetch())
    {
        $arResult["NEW_ITEMS"][$ar["ID"]] = $arResult["NEW"][$ar["ID"]];
    }
}
?>