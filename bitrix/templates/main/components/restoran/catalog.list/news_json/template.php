<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//if (count($arResult["ITEMS"])>0)
$items = array();
foreach ($arResult["ITEMS"] as &$item)
{   
    unset($item["PROPERTIES"]);
    unset($item["DETAIL_TEXT"]);
    unset($item["~DETAIL_TEXT"]);
    unset($item["~DETAIL_PICTURE"]);
    unset($item["~PREVIEW_PICTURE"]);
    unset($item["PREVIEW_PICTURE"]);
    unset($item["~DETAIL_TEXT_TYPE"]);
    unset($item["DETAIL_TEXT_TYPE"]);
    unset($item["~PREVIEW_TEXT_TYPE"]);    
    unset($item["PREVIEW_TEXT_TYPE"]);
    unset($item["~ID"]);
    unset($item["~PREVIEW_TEXT"]);
    unset($item["~LIST_PAGE_URL"]);
    unset($item["LIST_PAGE_URL"]);
    unset($item["LANG_DIR"]);
    unset($item["~LANG_DIR"]);
    unset($item["~SEARCHABLE_CONTENT"]);
    unset($item["SEARCHABLE_CONTENT"]);
    unset($item["IBLOCK_EXTERNAL_ID"]);
    unset($item["~IBLOCK_EXTERNAL_ID"]);
    $items[] = $item;
}
echo json_encode($items);
?>
         
         
