<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<script>
    var img = '';
    function change_big_modal_next()
    {
            $(".big_photo_reviews img").fadeOut(0,function(){
                img = new Image();                
                if ($("#"+$(this).attr("alt")).parent().next().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().next().find("img").attr("alt");                                            
                    $(".big_photo_reviews img").attr("alt",$("#"+$(this).attr("alt")).parent().next().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("alt");                                            
                    $(".big_photo_reviews img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("id"));                    
                }
                $(".big_photo_reviews img").attr("src",img.src);
                img.onload = new function(){                                        
                    $(".big_photo_reviews img").css({"width":"0px","height":"0px"});
                    $(".big_photo_reviews img").css({"width":img.width,"height":img.height});
                    $(".big_photo_reviews").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                    $(".big_photo_reviews img").fadeIn(300); 
                }
                //showOverflow();               

            });                                            

    }
    function change_big_modal_prev()
    {       
            $(".big_photo_reviews img").fadeOut(0,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().prev().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().prev().find("img").attr("alt");                                            
                    $(".big_photo_reviews img").attr("alt",$("#"+$(this).attr("alt")).parent().prev().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("alt");                                            
                    $(".big_photo_reviews img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("id"));                    
                }
                $(".big_photo_reviews img").attr("src",img.src);
                //showOverflow();
                img.onload = new function(){
                    //$(".big_modal img").css({"width":"0px","height":"0px"});
                    $(".big_photo_reviews img").css({"width":img.width,"height":img.height});
                    $(".big_photo_reviews").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                    $(".big_photo_reviews img").fadeIn(300); 
                }
            });                                          

    }
    $(document).ready(function(){    
        //$("#photogalery").galery({});
        $("body").append("<div class='big_modal big_photo_reviews'><div class='modal_close_galery'></div><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        //setCenter($(".big_modal"));
        $(".slider_left").on("click",function(){
            change_big_modal_prev();
            //setTimeout("change_big_modal_prev()",100);
        });
        $(".slider_right").on("click",function(){
            change_big_modal_next();
            //setTimeout("change_big_modal_next()",100);
            //change_big_modal_next();
        });
        $(".big_photo_reviews img").on("click",function(){            
            change_big_modal_next();
        });
        $(".big_photo_reviews").hover(function(){
            $(".big_photo_reviews .slider_left").fadeIn(0);
            $(".big_photo_reviews .slider_right").fadeIn(0);
        },function(){
            $(".big_photo_reviews .slider_left").fadeOut(0);
            $(".big_photo_reviews .slider_right").fadeOut(0);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_photo_reviews img").attr("src",img.src);
            $(".big_photo_reviews img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_photo_reviews img").css({"width":"0px","height":"0px"});
            img.onload = function(){
                $(".big_photo_reviews img").css({"width":img.width,"height":img.height});
                $(".big_photo_reviews").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                
                $(".big_photo_reviews").fadeIn(0);   
            }            
        });
        $(window).resize(function(){
            if (!$(".big_photo_reviews").is("hidden"))
            {
                var img = new Image();
                img.src = $(".big_photo_reviews").find("img").attr("src");
                if ($(window).width()<1100||$(window).height()<700)
                {
                    img.width = img.width*0.8;
                    img.height = img.height*0.8;
                }
                $(".big_photo_reviews img").css({"width":img.width,"height":img.height});
                $(".big_photo_reviews").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
            }
        });
        var img2 = new Array()
        $(".photog img").each(function(n){
           img2[n] = new Image();
           img2[n].src = $(this).attr("alt");
        });
    });
</script>
<div id="comments">
    <?if ($arResult["ITEMS"][0]["ID"]):?>
        <?foreach($arResult["ITEMS"] as $key => $arItem):?>
            <div class="left left_block">
                <?if (!$_REQUEST["CODE"]&&!$_REQUEST["id"]):?>
                <div class="left rest_img">
                    <img src="<?=$arResult["ITEMS"][$key]["RESTORAN"]["SRC"]?>" width="70" />
                </div>
                <div class="left rest_name">
                    <?if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"]=="Y"):?>
                        <a class="font14" href="<?=$arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"]?>"><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></a>,<br />
                    <?else:?>
                        <?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?>,<br />
                    <?endif;?>
                    <?=$arResult["ITEMS"][$key]["RESTORAN"]["IBLOCK_NAME"]?>
                </div>
                <div class="clear"></div>
                <div class="dotted"></div>
                <?endif;?>
                <div class="left rest_img">
                    <div class="ava">
                        <img src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" width="64" />
                    </div>
                </div>
                <div class="left" style="line-height:24px;">
                    <span class="name"><a class="another" href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["NAME"]?></a></span><br />
                    <span class="comment_date"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>, <?=$arItem["DISPLAY_ACTIVE_FROM_TIME"]?>
                    <br />
                    <div class="small_rating">
                        <?for($i=1;$i<=$arItem["DETAIL_TEXT"];$i++):?>
                            <div class="small_star_a" alt="<?=$i?>"></div>
                        <?endfor;?>
                        <?for($i;$i<=5;$i++):?>
                            <div class="small_star" alt="<?=$i?>"></div>
                        <?endfor;?>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="right right_block">                
                <?if ($arItem["IS_VIDEO"]):?>
                    <div class="preview_text<?=$arItem["ID"]?>">
                        <?=$arItem["PREVIEW_TEXT"]?>
                    </div>
                <?else:?>
                    <div class="preview_text<?=$arItem["ID"]?>">
                        <?=$arItem["PREVIEW_TEXT2"]?>
                    </div>
                    <?if (strlen($arItem["PREVIEW_TEXT2"])!=strlen($arItem["PREVIEW_TEXT"])):?>
                        <div class="preview_text<?=$arItem["ID"]?>" style="display:none;">
                            <p><?=$arItem["PREVIEW_TEXT"]?></p>
                        </div>
                    <?endif;?>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["video"][0]):?>
                    <div class="grey" style="margin-bottom:10px;">
                        <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.CFile::GetPath($arItem["PROPERTIES"]["video"][0])?>"></a> 
                        <script>
                            flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.12.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                }
                            });
                        </script>
                    </div>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["video_youtube"]):?>
                    <?
                    $temp = explode("v=",$arItem["PROPERTIES"]["video_youtube"]);
                    if (!$temp[1])
                        $temp = explode("/",$arItem["PROPERTIES"]["video_youtube"]);
                    ?>
                    <div class="grey" style="margin-bottom:10px;padding-bottom:20px;">
                        <iframe width="448" height="252" src="http://www.youtube.com/embed/<?=$temp[count($temp)-1]?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                <?endif;?>
                <?if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                    <div class="grey photog">
                        <?foreach($arItem["PROPERTIES"]["photos"] as $key=>$photo):?>
                            <div class="left <?=($key%3==2)?"end":""?>"><img id="photo<?=$photo["ID"]?>" src="<?=$photo["SRC"]?>" alt="<?=$photo["ORIGINAL_SRC"]["src"]?>" width="137" /></div>
                        <?endforeach;?>
                        <div class="clear"></div>
                    </div>                
                <?endif;?>
            </div>
            <div class="clear"></div>
            <div class="more" style="margin-left:0px;">
                <div class="clear"></div>
                <div class="left" style="margin-right:116px;<?=($_REQUEST["id"]||$_REQUEST["CODE"])?"visibility:hidden":""?>">
                        <?if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"]=="Y"):?>
                            <a href="<?=str_replace("detailed","opinions",$arItem["RESTORAN"]["DETAIL_PAGE_URL"])?>"><?=GetMessage("ALL_REVIEWS_".$arItem["RESTORAN"]["IBLOCK_TYPE_ID"])?></a>
                        <?endif;?>
                </div>
                <!--<div class="left"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Далее</a></div>
                <?if (!$_REQUEST["CODE"]):?>
                    <div class="left">Комментарии:(<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=($arItem["PROPERTIES"]["COMMENTS"])?$arItem["PROPERTIES"]["COMMENTS"]:"0"?></a>)</div>
                    <?if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"]=="Y"):?>
                        <div class="left"><a href="<?=str_replace("detailed","opinions",$arItem["RESTORAN"]["DETAIL_PAGE_URL"])?>"><?=GetMessage("ALL_REVIEWS_".$arItem["RESTORAN"]["IBLOCK_TYPE_ID"])?></a></div>
                    <?endif;?>
                <?endif;?>-->            
                <?if (strlen($arItem["PREVIEW_TEXT2"])!=strlen($arItem["PREVIEW_TEXT"])):?>
                <div class="left">                
                    <div class="razver razver<?=$arItem["ID"]?>" onclick="$('.preview_text<?=$arItem["ID"]?>').toggle(); $('.razver<?=$arItem["ID"]?>').toggle();"><?=GetMessage("RE_RAZVER")?></div>
                    <div class="razver razver<?=$arItem["ID"]?>" onclick="$('.preview_text<?=$arItem["ID"]?>').toggle(); $('.razver<?=$arItem["ID"]?>').toggle();" style="display:none"><?=GetMessage("RE_SVER")?></div>
                </div>
                <?endif;?>            
                <div class="right end">                
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><input type="button" class="light_button" value="<?=GetMessage("REVIEWS_ANSWER")?>"/></a>
                </div>
                <div class="right">                
                    <div class="answers" onclick="location.href='<?=$arItem["DETAIL_PAGE_URL"]?>#comments'">
                        <?=GetMessage("REVIEWS_ANSWERS")?> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="another"><?=($arItem["PROPERTIES"]["COMMENTS"])?$arItem["PROPERTIES"]["COMMENTS"]:"0"?></a>
                    </div>
                    <div class="answers_arrow"></div>
                </div>
                <div class="clear"></div>
            </div>
            <?if (end($arResult["ITEMS"])!=$arItem):?>
                <div class="black_hr"></div>            
            <?endif;?>
        <?endforeach;?>  
    <?else:?>
        <?if (!CSite::InGroup(Array(9))):?>
            <p style="font-style:italic"><?=GetMessage("NO_REVIEWS")?></p>
        <?endif;?>
    <?endif;?>
    <?if ($arParams["DISPLAY_BOTTOM_PAGER"]=="Y"):?>
        <hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
        </div>
        <div class="right">            
                <?=$arResult["NAV_STRING"]?>            
        </div>    
        <div class="clear"></div>
        <Br /><Br />
    <?endif;?>
    <?if ($arParams["URL"]):?>
        <Br />
        <p align="right"><a href="<?=str_replace("detailed","opinions",$arParams["URL"])?>" class="uppercase"><?=GetMessage("ALL_REVIEWS_catalog")?></a></p>
    <?endif;?>
</div>
