<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt<?if($key == 3):?> new_restoraunt_end<?endif?>">
        <div class="position left"><?=($key + 1)?></div>
        <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>      
        <div class="rating" style="padding:0px;">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="small_star<?if($i <= round($arItem["PROPERTIES"]["ratio"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
       </div>
       <div class="clear"></div>
       <p><?=$arItem["PREVIEW_TEXT"]?></p>
       <div class="left"><?=$arItem["AUTHOR_NAME"]?></div>
       <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_ALL_REVIEWS")?></a></div>
       <div class="clear"></div>
    </div>
    <?if ($arItem!=end($arResult["ITEMS"])):?>
            <div class="dotted"></div>
    <?endif;?>
<?endforeach;?>