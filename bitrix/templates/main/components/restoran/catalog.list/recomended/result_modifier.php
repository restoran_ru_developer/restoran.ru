<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ($arParams["SORT_BY1"]=="none")
{
    global $$arParams["FILTER_NAME"];
    $filter = $$arParams["FILTER_NAME"];
    $elem = array();
}
foreach($arResult["ITEMS"] as $key=>$arItem) {
    /*$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    if ($arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"])
    {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    }*/
    
    if(!$arItem["PREVIEW_PICTURE"]) {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
    }    
    if (!$arResult["ITEMS"][$key]["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    if ($arParams["SORT_BY1"]=="none")
        $elem[$arItem["ID"]] = $arResult["ITEMS"][$key];
}
if ($arParams["SORT_BY1"]=="none")
{
    $arResult["ITEMS"] = array();    
    foreach ($filter["ID"] as $id)
    {
        
        $arResult["ITEMS"][] = $elem[$id];
    }
}
?>