<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="top_restoraunt<?if($key == 3):?> top_restoraunt_end<?endif?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="image">
            <?if ($arItem["ID"]==1143939):?>
                <noindex><a href="http://www.prazdnikvkusa.ru" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a></noindex><br />
            <?elseif($arItem["ID"]==386954):?>
                <noindex><a href="http://www.discoveryclub.ru" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a></noindex><br />
            <?else:?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a><br />
            <?endif;?>
        </div>
        <?endif;?>
        <div class="title">
            <?if ($arItem["ID"]==1143939):?>
                <noindex><a href="http://www.prazdnikvkusa.ru" target="_blank"><?=$arItem["NAME"]?></a></noindex>
            <?elseif($arItem["ID"]==386954):?>
                <noindex><a href="http://www.discoveryclub.ru" target="_blank"><?=$arItem["NAME"]?></a></noindex>
            <?else:?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
            <?endif;?>
        </div>
        <div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="small_star<?if($i <= round($arItem["PROPERTIES"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
       </div>
       <div class="clear"></div>
       <?if($arItem["ID"]==386954):?>
            <p>Трехэтажный ресторан средиземноморской кухни с камином в старинном особняке в центре Москвы!</p>
       <?else:?>
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
       <?endif;?>
       <?if (!$arItem["PROPERTIES"]["COMMENTS"])
           $arItem["PROPERTIES"]["COMMENTS"] = 0;?>
       <div class="left"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_RS_REVIEWS1"), GetMessage("CT_RS_REVIEWS2"), GetMessage("CT_RS_REVIEWS3"))?></div>
       <?if ($arItem["ID"]==1175777):?>
        <div class="right"><noindex><a href="http://www.prazdnikvkusa.ru" target="_blank"><?=GetMessage("CT_BNL_ELEMENT_ALL_REVIEWS")?></a></noindex></div>
       <?elseif($arItem["ID"]==386954):?>
        <div class="right"><noindex><a href="http://www.discoveryclub.ru" target="_blank"><?=GetMessage("CT_BNL_ELEMENT_ALL_REVIEWS")?></a></noindex></div>
       <?else:?>
        <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_ALL_REVIEWS")?></a></div>
       <?endif;?>
       <div class="clear"></div>
    </div>
    <?if ($arItem!=end($arResult["ITEMS"])):?>
            <div class="dotted"></div>
    <?endif;?>
<?endforeach;?>
<div class="right">            
    <?if ($arParams["NEWEST"]=="Y"):?>    
        <a class="uppercase" href="/<?=CITY_ID?>/catalog/restaurants/all/?page=1&pageRestSort=new&by=desc"><?=GetMessage("CT_ALL")?></a>
    <?else:?>    
        <a class="uppercase" href="/<?=CITY_ID?>/ratings/#<?=$arParams["A"]?>"><?=GetMessage("CT_ALL")?></a>
    <?endif;?>
</div>
            <br />