<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // explode date
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["NAME"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["NAME"]);

    if ($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>470, 'height'=>344), BX_RESIZE_IMAGE_EXACT, false, Array(),false);
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = $arResult["ITEMS"][$key]["DETAIL_PICTURE"]["src"];
        //$arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::GetPath($arItem['DETAIL_PICTURE']);
    }
    else
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = "/tpl/images/noname/rest_nnm.png";
    }
    // get rest name
   /* $rsSec = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
    if($arSec = $rsSec->GetNext()) {
        $arResult["ITEMS"][$key]["RESTAURANT_NAME"] = htmlspecialchars_decode($arSec["NAME"]);
    }*/
    
    //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);   
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    preg_match("/#FID_([0-9]+)#/",$arItem["DETAIL_TEXT"],$video1);
    if ($video1[1])
        $arResult["ITEMS"][$key]["VIDEO"] = "http://".SITE_SERVER_NAME."".CFile::GetPath($video1[1]);
    preg_match("/<iframe ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\"/",$arItem["DETAIL_TEXT"],$video2);
    if ($video2[1])
    {
        for ($i=1;$i<count($video2);$i++)
        {
            if ($video2[$i]=="width")
                $width = $video2[++$i];
            elseif ($video2[$i]=="height")
                $height = $video2[++$i];
            elseif($video2[$i]=="src")
                $src = $video2[++$i];                
        }
    }
    if ($width&&$height&&$src)
    {            
        $height = 470*$height/$width;
        $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$src.'" width="470" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
}
?>