<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    // get and set user name or login
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->Fetch()){
    	if($arUser["PERSONAL_PROFESSION"]!="") $arResult["ITEMS"][$key]["AUTHOR_NAME"]=$arUser["PERSONAL_PROFESSION"];
		else $arResult["ITEMS"][$key]["AUTHOR_NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"];
			
    }
    //$arResult["ITEMS"][$key]["AUTHOR_NAME"] = ($arUser["NAME"] || $arUser["LAST_NAME"] ? $arUser["NAME"]." ".$arUser["LAST_NAME"]  : $arUser["LOGIN"]);
    $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
}

?>