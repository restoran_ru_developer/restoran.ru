<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])>0):?>
<div id="news">
    <div class="left left_block">
            <ul class="tabs">
                <?foreach($arResult["ITEMS"] as $key=>$arItem):
                    //plate_scrollable holiday_scrollable news_scrollable
                    ?>
                <li>
                    <a class="news_scr" scrCont="<?=$arItem["CODE"]?>_scrollable" href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=$arItem["NAME"]?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <?endforeach;?>
            </ul>
            <!-- tab "panes" -->
            <div class="panes">
                <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                    <div class="pane newsItem" style="padding-bottom:0px;">
                        <?
                        $filter = "arrNoFilter_".$arItem["CODE"];
                        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]))
                        {    
                                global $$filter;
                                $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE_ID"][0]);
                        }
                        ?>
                        <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "news_main",
                            Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => "",
                                    "SORT_ORDER1" => "",
                                    "SORT_BY2" => "",
                                    "SORT_ORDER2" => "",
                                    "FILTER_NAME" => ($$filter)?$filter:"",
                                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                                    "PROPERTY_CODE" => array(),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "500",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "43201",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );?>
                    </div>
                <?endforeach;?>
            </div>
    </div>
    <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "inc_right_banner",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => ""
            )
        );?>
    <div class="clear"></div>
</div>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="rest_news_cont" id="<?=$arItem["CODE"]?>_scrollable" <?=($key>0)?"style='display: none;'":""?>>
        <?
        $filter = "arrNoFilter_".$arItem["CODE"];
        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]))
        {    
                global $$filter;
                $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE_ID"]);
                v_dump($$filter);
        }      
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            ($key!=0)?'plate_scrollable':"main_news_scrollable",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"],
                    "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"])?$arItem["PROPERTIES"]["COUNT"]:"20",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array(),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "100",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"],
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "43200",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );?>
    </div>
<?endforeach;?>
<?endif;?>