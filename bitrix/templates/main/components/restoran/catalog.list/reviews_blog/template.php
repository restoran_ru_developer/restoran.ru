<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<script>
    function change_big_modal_next()
    {
            $(".big_modal img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().next().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().next().find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().next().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("id"));                    
                }
                $(".big_modal img").attr("src",img.src);
                showOverflow();
                //$(".big_modal img").css({"width":"0px","height":"0px"});
                $(".big_modal img").animate({"width":+img.width,"height":+img.height},100,function(){
                    $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":"="+eval($("window").height()/2-img.height/2),"left":"="+eval(($("window").width()/2)-(img.width/2))},300);
                });                                            
                $(".big_modal img").fadeIn(300); 

            });                                            

    }
    function change_big_modal_prev()
    {
            $(".big_modal img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().prev().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().prev().find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().prev().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("id"));                    
                }
                $(".big_modal img").attr("src",img.src);
                showOverflow();
                //$(".big_modal img").css({"width":"0px","height":"0px"});
                $(".big_modal img").animate({"width":+img.width,"height":+img.height},100,function(){
                    $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":"="+eval($("window").height()/2-img.height/2),"left":"="+eval(($("window").width()/2)-(img.width/2))},300);
                });                                            
                $(".big_modal img").fadeIn(300); 

            });                                            

    }
    $(document).ready(function(){
        //$("#photogalery").galery({});
        $("body").append("<div class=big_modal><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        setCenter($(".big_modal"));
        $(".slider_left").on("click",function(){
            change_big_modal_prev();
        });
        $(".slider_right").on("click",function(){
            change_big_modal_next();
        });
        $(".big_modal").hover(function(){
            $(".big_modal .slider_left").fadeIn(300);
            $(".big_modal .slider_right").fadeIn(300);
        },function(){
            $(".big_modal .slider_left").fadeOut(300);
            $(".big_modal .slider_right").fadeOut(300);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_modal img").attr("src",img.src);
            $(".big_modal img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_modal img").css({"width":"0px","height":"0px"});
            $(".big_modal img").animate({"width":+img.width,"height":+img.height},300);
            $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":"-="+eval(img.height/2),"left":"-="+eval(img.width/2)},300);
                $(".big_modal").fadeIn(300);
        });
        $("#system_overflow").click(function(){
            $(".big_modal").fadeOut();
            hideOverflow();
            $(".big_modal").css("width","0px");
            $(".big_modal").css("height","0px");
            setCenter($(".big_modal"));
            $(".big_modal img").css({"width":"0px","height":"0px"});
        });
    });
</script>


<?
if(is_array($arParams["IBLOCK_ID"])) $arParams["IBLOCK_ID"] = implode("|", $arParams["IBLOCK_ID"]);

//var_dump($arParams["ADD_TO_IB"]);

if($arParams["ADD_TO_IB"]>0) $arParams["IBLOCK_ID"]=$arParams["ADD_TO_IB"];
?>

<div id="response">
	<?if($arParams["REDACTOR"]=="Y"){?>
	<a class="light_button" href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?IB=<?=$arParams["IBLOCK_ID"]?>&SECTION=<?=$arParams["SECTION_ID"]?>">+ Новый отзыв</a>
	<?}?>
	
    <?foreach($arResult["ITEMS"] as $key => $arItem):?>
        <?
      //  var_dump($arResult["ITEMS"][$key]["ACTIVE"]);
        ?>
        <div class="<? if($arResult["ITEMS"][$key]["ACTIVE"]=="N") echo 'not_active';?>">
        <div class="left left_block_blog">
            <div class="left rest_img">
                <img src="<?=$arResult["ITEMS"][$key]["RESTORAN"]["SRC"]?>" width="70" />
            </div>
            <div class="left rest_name">
                <?if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"]=="Y"):?>
                    <a class="font14" href="<?=$arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"]?>"><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></a>,<br />
                <?else:?>
                    <?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?>,<br />
                <?endif;?>
                <?=$arResult["ITEMS"][$key]["RESTORAN"]["IBLOCK_NAME"]?>
            </div>
            <div class="clear"></div>
            <div class="dotted"></div>
            <div class="left rest_img">
                <div class="ava">
                    <img src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" width="64" />
                </div>
            </div>
            <div class="left" style="line-height:24px;">
                <span class="name"><a class="another" href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["NAME"]?></a></span><br />
                <span class="comment_date"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <br />
                <div class="small_rating">
                    <?for($i=1;$i<=$arItem["DETAIL_TEXT"];$i++):?>
                        <div class="small_star_a" alt="<?=$i?>"></div>
                    <?endfor;?>
                    <?for($i;$i<=5;$i++):?>
                        <div class="small_star" alt="<?=$i?>"></div>
                    <?endfor;?>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        
        
        
        
        <div class="right right_block_blog">
            <p>
                <span class="preview_text<?=$arItem["ID"]?>">
                    <?=$arItem["PREVIEW_TEXT2"]?>
                    <?if (strlen($arItem["PREVIEW_TEXT2"])!=strlen($arItem["PREVIEW_TEXT"])):?>
                        <a onclick="$('.preview_text<?=$arItem["ID"]?>').toggle()" class="jsanother">еще</a>
                    <?endif;?>
                </span>
                <?if (strlen($arItem["PREVIEW_TEXT2"])!=strlen($arItem["PREVIEW_TEXT"])):?>
                    <span class="preview_text<?=$arItem["ID"]?>" style="display:none;">
                        <?=$arItem["PREVIEW_TEXT"]?> <a onclick="$('.preview_text<?=$arItem["ID"]?>').toggle()" class="jsanother">скрыть</a>
                    </span>
                <?endif;?>
            </p>
            <?if ($arItem["PROPERTIES"]["plus2"]["TEXT"]):?>
                <b>ДОСТОИНСТВА:</b><br />
                <p>
                	<?
                	//var_dump($arItem["PROPERTIES"]["plus2"]);
                	?>
                    <span class="plus<?=$arItem["ID"]?>">
                        <?=$arItem["PROPERTIES"]["plus2"]["TEXT2"]?>
                        <?if (strlen($arItem["PROPERTIES"]["plus2"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["plus2"]["TEXT"])):?>
                            <a onclick="$('.plus<?=$arItem["ID"]?>').toggle()" class="jsanother">еще</a>
                        <?endif;?>
                    </span>
                    <?if (strlen($arItem["PROPERTIES"]["plus2"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["plus2"]["TEXT"])):?>
                        <span class="plus<?=$arItem["ID"]?>" style="display:none;">
                            <?=$arItem["PROPERTIES"]["plus2"]["TEXT"]?> <a onclick="$('.plus<?=$arItem["ID"]?>').toggle()" class="jsanother">скрыть</a>
                        </span>
                    <?endif;?>
                </p>                
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["minus2"]["TEXT"]):?>
                <b>НЕДОСТАТКИ:</b><br />
                <p>
                    <span class="minus<?=$arItem["ID"]?>">
                        <?=$arItem["PROPERTIES"]["minus2"]["TEXT2"]?>
                        <?if (strlen($arItem["PROPERTIES"]["minus2"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["minus2"]["TEXT"])):?>
                            <a onclick="$('.minus<?=$arItem["ID"]?>').toggle()" class="jsanother">еще</a>
                        <?endif;?>
                    </span>
                    <?if (strlen($arItem["PROPERTIES"]["minus2"]["TEXT2"])!=strlen($arItem["PROPERTIES"]["minus2"]["TEXT"])):?>
                        <span class="minus<?=$arItem["ID"]?>" style="display:none;">
                            <?=$arItem["PROPERTIES"]["minus2"]["TEXT"]?> <a onclick="$('.minus<?=$arItem["ID"]?>').toggle()" class="jsanother">скрыть</a>
                        </span>
                    <?endif;?>
                </p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["video"][0]):?>
                <div class="grey" style="margin-bottom:10px;">
                    <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.CFile::GetPath($arItem["PROPERTIES"]["video"][0])?>"></a> 
                    <script>
                        flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                            clip: {
                                    autoPlay: false, 
                                    autoBuffering: true
                            }
                        });
                    </script>
                </div>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                <div class="grey photog">
                    <?foreach($arItem["PROPERTIES"]["photos"] as $key=>$photo):?>
                        <div class="left <?=($key%3==2)?"end":""?>"><img id="photo<?=$photo["ID"]?>" src="<?=$photo["SRC"]?>" alt="<?=$photo["ORIGINAL_SRC"]?>" width="137" /></div>
                    <?endforeach;?>
                    <div class="clear"></div>
                </div>                
            <?endif;?>
        </div>
        <div class="clear"></div>
        <div class="more">
            <div class="clear"></div>
            
            <div class="left"><a class="another" href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?IB=<?=$arItem["IBLOCK_ID"]?>&SECTION=<?=$arParams["SECTION_ID"]?>&ID=<?=$arItem["ID"]?>">Редактировать</a></div>
            
            <div class="clear"></div>
        </div>
         </div>
        <?if (end($arResult["ITEMS"])!=$arItem):?>
            <div class="black_hr"></div>            
        <?endif;?>
        
    <?endforeach?>            
            
	<div class="pager">
		<?=$arResult["NAV_STRING"]?>
	</div>
</div>
