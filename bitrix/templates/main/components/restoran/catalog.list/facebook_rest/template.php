<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>	
<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?17"></script>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <div class="left left_block" style="height:24px;line-height:24px;">
        <?if (substr_count($arItem["DETAIL_PAGE_URL"], "banket")>0){
            $r = "Банкетный зал";
        }else
        {
            $r = "Ресторан";
        }?>
        <a class="font14" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>  [<?=$r?>]

    </div>
    <div class="right right_block" style="width:400px;">
        <div class="left" style="margin-right:15px;">
            <div id="fb-root"></div>
            <fb:like href="http://www.restoran.ru<?=$arItem["DETAIL_PAGE_URL"]?>" layout="button_count" action="like" data-width="75"></fb:like>
        </div>
        <div class="left">
            <a rel="nofollow" href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="<?= SITE_SERVER_NAME?>" data-url="http://www.restoran.ru<?=$arItem["DETAIL_PAGE_URL"]?>" data-lang="en">Tweet</a>
        </div>
        <div class="left">            
            <div id="vk_like<?=$arItem["ID"]?>"></div>
            <script type="text/javascript">
                    VK.init({apiId: 2881483, onlyWidgets: true});
                    VK.Widgets.Like('vk_like<?=$arItem["ID"]?>', {
                            type: 'max',
                            width: 80,
                            verb: <?= $arParams["LIKE_TYPE"]=="RECOMMEND" ? '1' : '0'?>,
                            pageUrl: '<?= CUtil::JSescape("http://www.restoran.ru".$arItem["DETAIL_PAGE_URL"])?>'
                    });
            </script>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>            
    <?if (end($arResult["ITEMS"])!=$arItem):?>
        <div class="border_line"></div>            
    <?endif;?>
<?endforeach;?>  
<?if ($arParams["DISPLAY_BOTTOM_PAGER"]=="Y"):?>
<hr class="bold" />        
<div class="left">
    <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
    <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
    <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
    <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
</div>
<div class="right">            
        <?=$arResult["NAV_STRING"]?>            
</div>    
<div class="clear"></div>
<Br /><Br />
<?endif;?>