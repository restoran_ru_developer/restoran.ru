<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="top_block">Ресторан недели</div>
    <div class="new_restoraunt" style="margin-right:0px; width:232px">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img vspace="10" src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a>
        <div class="title">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </div>
        <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <div class="left">Комментарии: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
        <?if ($arItem!=end($arResult["ITEMS"])):?>
            <div class="dotted"></div>
        <?endif;?>
    </div>
<?endforeach;?>