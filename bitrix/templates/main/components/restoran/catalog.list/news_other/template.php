<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="content">
    <div class="left" style="width:730px; position:relative">
        <h1 class="with_link">
            <?if ($arParams["SET_TITLE"]=="Y"):?>
                <a href="<?=$arResult["RESTORAN_URL"]?>"><?=$arResult["RESTORAN_NAME"]."</a> &ndash; ".$arResult["IBLOCK_TYPE"]?>
            <?else:?>
                <?=$APPLICATION->GetTitle();?>
            <?endif;?>
        </h1>
        <?
        $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE","clear_cache");
        $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=ACTIVE_FROM&".(($_REQUEST["pageSort"]=="ACTIVE_FROM"&&$_REQUEST["by"]=="desc"||!$_REQUEST["pageSort"]) ? "by=asc": "by=desc"), $excUrlParams);                
        $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=shows&".(($_REQUEST["pageSort"]=="shows"&&$_REQUEST["by"]=="asc"||!$_REQUEST["pageSort"]) ? "by=desc": "by=asc"), $excUrlParams);
        ?>                
        <div class="sorting">
            <div class="left">
                <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                <?$by = ($_REQUEST["by"]=="asc")?"a":"z";?>
                <?if ($_REQUEST["pageSort"] == "ACTIVE_FROM" || !$_REQUEST["pageSort"]):                                                   
                    echo "<span class='".$by."'><a class='another' href='".$sortNewPage."'>".GetMessage("SORT_NEW_TITLE")."</a></span>";
                else:
                    echo '<span><a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a></span>';
                endif;?>
                    / 
                <?if ($_REQUEST["pageSort"] == "shows"):
                    echo "<span class='".$by."'><a class='another' href='".$sortPopularPage."'>".GetMessage("SORT_POPULAR_TITLE")."</a></span>";
                else:
                    echo '<span><a class="another" href="'.$sortPopularPage.'">'.GetMessage("SORT_POPULAR_TITLE").'</a></span>';
                endif;?>
            </div>
            <div class="right"><?//=$arResult["NAV_STRING"]?></div>                    
            <div class="clear"></div>
        </div>
        <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <div class="new_restoraunt left<?if($key % 3 == 2):?> end<?endif?>">
                <div style="margin-bottom:10px;">
                    <a href="/users/id<?=$arItem["CREATED_BY"]?>/" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
                </div>
                <?if ($arParams["IBLOCK_TYPE"]=="videonews"):?>
                    <?if ($arItem["VIDEO"]):?>
                        <script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
                        <a class="myPlayer" href="<?=$arItem["VIDEO"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                },
                                plugins: {
                                    controls: {
                                        all: false,
                                        play: true,
                                        scrubber: true,
                                        tooltips: {
                                            buttons: true,
                                            fullscreen: 'Enter fullscreen mode'
                                        }
                                    }
                                }
                            });
                        </script>
                    <?elseif ($arItem["VIDEO_PLAYER"]):?>
                        <?=$arItem["VIDEO_PLAYER"]?>
                    <?endif;?>
                <?else:?>
                    <!--<div style="overflow:hidden; height:127px">-->
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>
                    <!--</div>-->
                <?endif;?>
                <div class="title">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a>
                </div>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
                <div class="left">Комментарии: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
                <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                <div class="clear"></div>
            </div>
            <?if(($key % 3 == 2)):?>
                <div class="clear"></div>
                <div class="border_line"></div>        
            <?endif?>
        <?endforeach;?>          
        <div class="clear"></div>        
        <?if ($arResult["NAV_STRING"]):?>
            <hr class="bold" />        
            <div class="left">
                <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
                <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
                <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
                <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
            </div>
            <div class="right">            
                    <?=$arResult["NAV_STRING"]?>            
            </div>
            <div class="clear"></div>
        <?endif;?>        
        <br /><br />
    </div>
    <div class="right">
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>        
        <br />
        <div class="tags">
            <?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                                "SORT" => "CNT",
                                "PAGE_ELEMENTS" => "20",
                                "PERIOD" => "",
                                "URL_SEARCH" => "/search/index.php",
                                "TAGS_INHERIT" => "Y",
                                "CHECK_DATES" => "Y",
                                "FILTER_NAME" => "",
                                "arrFILTER" => array(
                                                0 => "iblock_news",
                                ),
                                "arrFILTER_iblock_cookery" => array(
                                                0 => $arItem["IBLOCK_ID"],
                                ),
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "3600",
                                "FONT_MAX" => "24",
                                "FONT_MIN" => "12",
                                "COLOR_NEW" => "24A6CF",
                                "COLOR_OLD" => "24A6CF",
                                "PERIOD_NEW_TAGS" => "",
                                "SHOW_CHAIN" => "Y",
                                "COLOR_TYPE" => "N",
                                "WIDTH" => "100%"
                                ),
                                $component
                );?> 

        </div>                
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    </div>
    <div class="clear"></div>
    <br /><br />
        <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "bottom_rest_list",
            		"NOINDEX" => "N",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
        );?> 
</div>