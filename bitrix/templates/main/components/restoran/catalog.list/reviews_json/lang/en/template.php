<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "review";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "reviews";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "reviews";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "More";
$MESS["SHOW_CNT_TITLE"] = "Display";
$MESS["SORT_NEW_TITLE"] =" by newest";
$MESS["SORT_POPULAR_TITLE"] =" by popular";
$MESS["ALL_REVIEWS_catalog"] ="All restaurant reviews";
$MESS["ALL_REVIEWS_kupons"] ="Все отзывы купона";
$MESS["REVIEWS_ANSWERS"] ="Answers: ";
$MESS["REVIEWS_ANSWER"] ="Answer";
$MESS["RE_RAZVER"] ="Show";
$MESS["RE_SVER"] ="Hide";
$MESS["R_PLUS"] = "PROS";
$MESS["R_MINUS"] = "CONS";
$MESS["NO_REVIEWS"] = "No one's writing, you can be the first";
?>