<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$items = array();?>
<?if ($arResult["ITEMS"][0]["ID"]):
        foreach($arResult["ITEMS"] as $key => $arItem):
            unset($arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"]);            
            $items[$key]["AUTHOR"] = $arItem["NAME"];
            $items[$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arItem["DISPLAY_ACTIVE_FROM_DAY"];
            $items[$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arItem["DISPLAY_ACTIVE_FROM_MONTH"];
            $items[$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arItem["DISPLAY_ACTIVE_FROM_YEAR"];
            $items[$key]["DISPLAY_ACTIVE_FROM_TIME"] = $arItem["DISPLAY_ACTIVE_FROM_TIME"];
            $items[$key]["RATIO"] = $arItem["DETAIL_TEXT"];
            $items[$key]["TEXT"] = strip_tags($arItem["PREVIEW_TEXT"]);
            if ($arItem["PROPERTIES"]["video"][0])
                $items[$key]["VIDEO"] = "http://".SITE_SERVER_NAME.CFile::GetPath($arItem["PROPERTIES"]["video"][0]);
            if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):                    
                foreach($arItem["PROPERTIES"]["photos"] as $key=>$photo):
                        $items[$key]["PHOTOS"][] = Array("SMALL"=>$photo["SRC"], "BIG" => $photo["ORIGINAL_SRC"]["src"]);            
                endforeach;                                       
            endif;
            $items[$key]["COMMENTS"] = ($arItem["PROPERTIES"]["COMMENTS"])?$arItem["PROPERTIES"]["COMMENTS"]:"0";                    
            $items[$key]["RESTORAN"] = $arResult["ITEMS"][$key]["RESTORAN"];            
    endforeach;
endif;
echo json_encode($items);
?>
