<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<script>
    var img = '';
    function change_big_modal_next()
    {
            $(".big_modal img").fadeOut(300,function(){
                img = new Image();                
                if ($("#"+$(this).attr("alt")).parent().next().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().next().find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().next().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("id"));                    
                }
                $(".big_modal img").attr("src",img.src);
                img.onload = new function(){                                        
                    $(".big_modal img").css({"width":"0px","height":"0px"});
                    $(".big_modal img").css({"width":+img.width,"height":+img.height});
                    $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2+$(window).scrollTop()),"left":""+eval(($(window).width()/2)-(img.width/2))},300);
                    $(".big_modal img").fadeIn(300); 
                }
                //showOverflow();               

            });                                            

    }
    function change_big_modal_prev()
    {
            $(".big_modal img").fadeOut(300,function(){
                var img = new Image();
                if ($("#"+$(this).attr("alt")).parent().prev().hasClass("left"))
                {
                    img.src = $("#"+$(this).attr("alt")).parent().prev().find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().prev().find("img").attr("id"));
                }
                else
                {
                    img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("alt");                                            
                    $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("id"));                    
                }
                $(".big_modal img").attr("src",img.src);
                //showOverflow();
                img.onload = new function(){
                    //$(".big_modal img").css({"width":"0px","height":"0px"});
                    $(".big_modal img").animate({"width":+img.width,"height":+img.height},100,function(){
                        $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2+$(window).scrollTop()),"left":""+eval(($(window).width()/2)-(img.width/2))},300);
                    });                                            
                    $(".big_modal img").fadeIn(300); 
                }
            });                                            

    }
    $(document).ready(function(){    
        //$("#photogalery").galery({});
        $("body").append("<div class=big_modal><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        setCenter($(".big_modal"));
        $(".slider_left").on("click",function(){
            setTimeout("change_big_modal_prev()",100);
        });
        $(".slider_right").on("click",function(){
            setTimeout("change_big_modal_next()",100);
            //change_big_modal_next();
        });
        $(".big_modal").hover(function(){
            $(".big_modal .slider_left").fadeIn(300);
            $(".big_modal .slider_right").fadeIn(300);
        },function(){
            $(".big_modal .slider_left").fadeOut(300);
            $(".big_modal .slider_right").fadeOut(300);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_modal img").attr("src",img.src);
            $(".big_modal img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_modal img").css({"width":"0px","height":"0px"});
            img.onload = function(){
                $(".big_modal img").animate({"width":+img.width,"height":+img.height},300);
                $(".big_modal").animate({"width":+img.width,"height":+img.height,"top":""+eval($(window).height()/2-img.height/2+$(window).scrollTop()),"left":"-="+eval(img.width/2)},300);
                
                $(".big_modal").fadeIn(300);   
            }            
        });
        $("#system_overflow").click(function(){
            $(".big_modal").fadeOut();
            hideOverflow();
            $(".big_modal").css("width","0px");
            $(".big_modal").css("height","0px");
            setCenter($(".big_modal"));
            $(".big_modal img").css({"width":"0px","height":"0px"});
        });
        var img2 = new Array()
        $(".photog img").each(function(n){
           img2[n] = new Image();
           img2[n].src = $(this).attr("alt");
        });
    });
</script>
<div id="comments" class="hreview">
    <span class="type">
        <span class="value-title" title="business"></span>
    </span>
    <?foreach($arResult["ITEMS"] as $key => $arItem):?>
        <div class="left left_block">
            <div class="left item vcard">
                <abbr class="category" title="restaurant"></abbr>
                <div class="left rest_img">
                    <img class="photo" src="<?=$arResult["ITEMS"][$key]["RESTORAN"]["SRC"]?>" alt="<?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?>" width="70" />
                </div>
                <div class="left rest_name">
                    <a class="font14 fn permalink" href="<?=$arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"]?>"><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></a>,<br />
                    <?=$arResult["ITEMS"][$key]["RESTORAN"]["IBLOCK_NAME"]?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="dotted"></div>
            <div class="left reviewer vcard">
                <div class="left rest_img">
                    <div class="ava">
                        <img class="photo" src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" width="64" />
                    </div>
                </div>
                <div class="left" style="line-height:24px;">
                    <span class="name"><a class="another permalink fn" href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["NAME"]?></a></span><br />
                    <span class="dtreviewed">
                        <abbr class="value" title="<?=$arItem["ACTIVE_FROM"]?>"><span class="comment_date"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>, <?=$arItem["DISPLAY_ACTIVE_FROM_TIME"]?></abbr>                    

                    </span>
                    <br />
                    <div class="small_rating rating">
                        <abbr class="value" title="<?=$arItem["DETAIL_TEXT"]?>">
                            <?for($i=1;$i<=$arItem["DETAIL_TEXT"];$i++):?>
                                <div class="small_star_a" alt="<?=$i?>"></div>
                            <?endfor;?>
                            <?for($i;$i<=5;$i++):?>
                                <div class="small_star" alt="<?=$i?>"></div>
                            <?endfor;?>
                            <div class="clear"></div>
                        </abbr>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right right_block">
            <a class="permalink" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
            <p class="description"><?=$arItem["PREVIEW_TEXT"]?></p>
            <?if ($arItem["PROPERTIES"]["plus"]):?>
                <b>ДОСТОИНСТВА:</b><br />
                <p class="pro"><?=$arItem["PROPERTIES"]["plus"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["minus"]):?>
                <b>НЕДОСТАТКИ:</b><br />
                <p class="contra"><?=$arItem["PROPERTIES"]["minus"]?></p>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["video"][0]):?>
                <div class="grey1" style="margin-bottom:10px;">
                    <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.CFile::GetPath($arItem["PROPERTIES"]["video"][0])?>"></a> 
                    <script>
                        flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.14.swf", {
                            clip: {
                                    autoPlay: false, 
                                    autoBuffering: true
                            }
                        });
                    </script>
                </div>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["video_youtube"]):?>
                <?
                $temp = explode("v=",$arItem["PROPERTIES"]["video_youtube"]);
                if (!$temp[1])
                    $temp = explode("/",$arItem["PROPERTIES"]["video_youtube"]);
                ?>
                <div class="grey1" style="margin-bottom:10px;padding-bottom:20px;">
                    <iframe width="448" height="252" src="http://www.youtube.com/embed/<?=$temp[count($temp)-1]?>" frameborder="0" allowfullscreen></iframe>
                </div>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                <div class="grey1 photog">
                    <?foreach($arItem["PROPERTIES"]["photos"] as $key=>$photo):?>
                        <div class="left <?=($key%3==2)?"end":""?>"><img id="photo<?=$photo["ID"]?>" class="photo" src="<?=$photo["SRC"]?>" alt="<?=$photo["ORIGINAL_SRC"]["src"]?>" width="137" /></div>
                    <?endforeach;?>
                    <div class="clear"></div>
                </div>                
            <?endif;?>
        </div>
        <div class="clear"></div>
        <!--<div class="more">
            <div class="clear"></div>
            <div class="left">Комментарии:(<a class="another" href="#comments"><?=($arItem["PROPERTIES"]["COMMENTS"])?$arItem["PROPERTIES"]["COMMENTS"]:"0"?></a>)</div>
            <div class="right"><a class="reviewsurl" href="<?=str_replace("detailed","opinions",$arItem["RESTORAN"]["DETAIL_PAGE_URL"])?>"><?=GetMessage("ALL_REVIEWS_".$arItem["RESTORAN"]["IBLOCK_TYPE_ID"])?></a></div>
            <div class="clear"></div>
        </div>-->        
    <?endforeach?>              
    <div class="clear"></div>
</div>
