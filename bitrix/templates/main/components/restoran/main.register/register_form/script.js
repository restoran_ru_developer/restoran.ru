// show rest form selector
function showRestForm() {
    $('.rest_form_fields').toggle();
}

function ajaxNick() {
    nick = $('#nicknameinput').val()
    $.ajax({
        type: "POST",
        url: "/bitrix/templates/main/components/restoran/main.register/register_form/nickname-ajax.php",
        data: {
            nick:nick
        },
        success: function(data) {
            if(data == '1'){
                $('.nickError-block').stop(true, true).fadeOut();
            } else {
                $('.nickError-block').stop(true, true).fadeIn();
            }
        }
    })
}

function nickChanged() {
    if (typeof id !== 'undefined'){
        clearTimeout(id);
    }
    if ($('#nicknameinput').val() == ""){
        $('.nickError-block').stop(true, true).fadeOut();
    } else {
        
        id = setTimeout(ajaxNick, 1000);
    }
    
}



$(document).ready(function() {
    // set login from email
/*    $('#EMAIL').keyup(function() {
        $('#LOGIN').val($(this).val());
    });*/
    
    
    $('input[name=register_submit_button]').click(function() {
        $('#LOGIN').val($('#EMAIL').val());
    });    

    $('#show_rest_form').change(function(){
        showRestForm();
    });
    $.tools.dateinput.localize("ru",  {months: 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря', 
                            shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
                            days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
                            shortDays:     'вс,пн,вт,ср,чт,пт,сб'});
    $(".datew").maski("99.99.9999")
    $(".phone").maski("(999) 999-99-99");
    /*$(".sms").click(function(){
        $(".sms").removeClass("active");
        $(this).addClass("active");
        $("#sms").attr("value",$(this).attr("val"));
    });*/
    $(".gender").click(function(){
        $(".gender").removeClass("active");
        $(this).addClass("active");
        $("#gender").attr("value",$(this).attr("val"));
    });
    var params = {
                                    changedEl: "#multi5",
                                    visibleRows: 10,
                                    scrollArrows: true
                                }
                                cuSelMulti(params);
});