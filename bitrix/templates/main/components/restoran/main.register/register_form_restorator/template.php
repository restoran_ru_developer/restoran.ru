<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();		
if($arResult["VALUES"]["PERSONAL_CITY"]=="") $arResult["VALUES"]["PERSONAL_CITY"]=getCityByIP();
?>
<script src="<?=$templateFolder?>/script.js"></script>
<div class="register_form">
<h1><?=GetMessage("AUTH_REGISTER")?></h1>
<?if($USER->IsAuthorized()):?>
    <p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
    <div style="margin-bottom:400px;"></div>
<?else:?>
    <? 
    if (count($arResult["ERRORS"]) > 0):
            foreach ($arResult["ERRORS"] as $key => $error)
                            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                        ShowError(implode("<br />", $arResult["ERRORS"]));

    elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
    ?>
    <!--<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>-->
    <?endif?>
    <form id="formm" method="post" action="<?=POST_FORM_ACTION_URI?>#restorator" name="regform" enctype="multipart/form-data">
    <?if($arResult["BACKURL"] <> ''):?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?endif;?>
        <div class="left" style="width:400px;">
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_LAST_NAME")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["NAME"] == "Y"):?><span class="starrequired">*</span><?endif?>: <br />
                <input  type="text" class="placeholder" name="REGISTER[LAST_NAME]" placeholder="Иванов" value="<?=$_REQUEST["REGISTER"]["LAST_NAME"]?>" autocomplete="off" style="width:330px;"/>
            </div>

            <div class="question">
                <?=GetMessage("REGISTER_FIELD_NAME")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["NAME"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_NAME_DESCRIPTION")?></span><br />
                <input  type="text" class="placeholder" name="REGISTER[NAME]" placeholder="Иван" value="<?=$_REQUEST["REGISTER"]["NAME"]?>" autocomplete="off"  style="width:330px;"/>
            </div>

            <div class="question">
                <div class="nickError-block">
                    <div class="nickError">Этот ник уже занят</div>
                </div>
                <input type="hidden" id="nickisok" value="" />
                <?=GetMessage("REGISTER_FIELD_PERSONAL_PROFESSION")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["PERSONAL_PROFESSION"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PERSONAL_PROFESSION_DESCRIPTION")?></span><br />
                <input  type="text" class="placeholder" id="nicknameinput2" name="REGISTER[PERSONAL_PROFESSION]" onkeyup="nickChanged();" placeholder="nickname" value="<?=$_REQUEST["REGISTER"]["PERSONAL_PROFESSION"]?>" autocomplete="off"  style="width:330px;"/>
            </div>                       
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_PASSWORD")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PASSWORD_DESCRIPTION")?></span><br />
                <input type="password" class="placeholder" name="REGISTER[PASSWORD]"  autocomplete="off"   style="width:330px;" />
            </div>
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD_DESCRIPTION")?></span><br />
                <input type="password" class="placeholder" name="REGISTER[CONFIRM_PASSWORD]"   autocomplete="off"   style="width:330px;"/>
            </div>
            <!--<div class="question">
                <?=GetMessage("REGISTER_FIELD_PODT")?>: <a href="javascript:void(0)" val="sms" class="sms active no_border"><i><?=GetMessage("USER_SMS")?></i></a> / <a class="sms no_border" val="email" href="javascript:void(0)"><i><?=GetMessage("USER_EMAIL")?></i></a>
                <input type="hidden" id="sms" name="REGISTER[SMS]" value="email"/>
            </div>-->
                <div class="question">
                    <input type="hidden" id="sms" name="REGISTER[SMS]" value="email"/>
                <?if ($arResult["USE_CAPTCHA"] == "Y"):?>
                    <div class="question"><?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<br />
                        <input id="captchaSid" type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <img id="captchaImg" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" align="left"/>
                        <a id="reloadCaptcha">Поменять картинку</a>                        
                        
                            <script type="text/javascript">
                                $(document).ready(function(){
                                   $('#reloadCaptcha').click(function(){                                      
                                      $.getJSON('<?=$this->__folder?>/reload_captcha.php', function(data) {
                                         $('#captchaImg').attr('src','/bitrix/tools/captcha.php?captcha_sid='+data);
                                         $('#captchaSid').val(data);                                         
                                      });
                                      return false;
                                   });
                                });
                             </script>
                         
                        <input class="placeholder" type="text" name="captcha_word" maxlength="10" value="" size="15" style="margin-left:20px" />
                    </div>
                <?endif;?>
            </div>
        </div>
        <div class="right" style="width:550px;">
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["PERSONAL_PROFESSION"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE_DESCRIPTION")?></span><br />
                <input type="text" class="placeholder phone" name="REGISTER[PERSONAL_PHONE]" value="<?=$_REQUEST["REGISTER"]["PERSONAL_PHONE"]?>" autocomplete="off"  style="width:330px;"/>
            </div>
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_EMAIL")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["EMAIL"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_EMAIL_DESCRIPTION")?></span><br />
                <input id="EMAIL2" type="text" class="placeholder" name="REGISTER[EMAIL]" <?=($_REQUEST["invite_email"])?"readonly='readonly'":""?> value="<?=($_REQUEST["invite_email"])?$_REQUEST["invite_email"]:$arResult["VALUES"]["EMAIL"]?>"  autocomplete="off"   style="width:330px;"/>
                <input id="LOGIN2" type="hidden" class="placeholder" name="REGISTER[LOGIN]" value="<?=$_REQUEST["REGISTER"]["LOGIN"]?>"  autocomplete="off"   style="width:330px;"/>
                <input type="hidden" name="invite" value="1" />
                <?if ($_REQUEST["co"]):?>
                <input type="hidden" name="REGISTER[PERSONAL_PAGER]" value="<?=$_REQUEST["co"]?>" />
                <?endif;?>
            </div> 
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_CITY")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PERSONAL_CITY_DESCRIPTION")?></span><br />
                <input id="CITY" type="text" class="placeholder" name="REGISTER[PERSONAL_CITY]" value="<?=$_REQUEST["REGISTER"]["PERSONAL_CITY"]?>"  autocomplete="off"   style="width:330px;"/>
            </div>
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_BIRTHDAY")?>: <Br />
                <input type="text" class="datew placeholder" name="REGISTER[PERSONAL_BIRTHDAY]" value="<?=$_REQUEST["REGISTER"]["PERSONAL_BIRTHDAY"]?>" size="25" />
            </div>            
            <div class="question">                
                <?=GetMessage("REGISTER_FIELD_REST_INFO")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["WORK_PROFILE"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_REST_INFO_DESCRIPTION")?></span><br />
                <textarea class="placeholder" rows="5" name="REGISTER[WORK_PROFILE]" style="width:330px;height:60px; padding:10px"></textarea>
            </div>                        
            <div class="question">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_GENDER")?>:  <a href="javascript:void(0)" val="M" class="gender no_border"><i><?=GetMessage("USER_MALE")?></i></a> / <a class="gender no_border" val="F" href="javascript:void(0)"><i><?=GetMessage("USER_FEMALE")?></i></a>
                <input type="hidden" id="gender" name="REGISTER[PERSONAL_GENDER]" value="M"/>
            </div>                                    
            <input type="hidden" name="group" value="9" />
            <div align="right">
                <input class="light_button" id="register_submit_button" type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
                <div class="grey font12"><i>Регистируясь, Вы принимаете условия <a id="link_agreement" target="_blank" href="/auth/user_license_agreement.php">пользовательского соглашения</a></i></div>
            </div>
        </div>
        <div class="clear"></div>
        <br />        

    <!--<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
    <p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>-->

    </form>    
<?endif?>
</div>
<?/*if($_POST["REGISTER"]["RESTORATOR"] && $arResult["ERRORS"]):?>
    <script type="text/javascript">
        showRestForm();
    </script>
<?endif*/?>