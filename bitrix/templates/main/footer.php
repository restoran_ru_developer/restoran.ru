<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>        
<?if ($APPLICATION->GetCurDir()=="/restopraktik/"||$APPLICATION->GetCurDir()=="/fatcat/"||$APPLICATION->GetCurDir()=="/activities/gastronomic/"):?>
            </div>
        <?endif;?>        
    </div>    
    </div>
    <div id="footer">            
        <div class="footer_content">
            <div class="to_top"><?=GetMessage("GO_TOP")?></div>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom_menu",
                    Array(
                            "ROOT_MENU_TYPE" => "bottom_menu_".CITY_ID,
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array()
                    ),
            false
            );?>
            <div class="clear"></div>            
            <br /><br />
            <div class="left">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_copyright_1.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="left">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_copyright_2.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="right" style="text-align:right;">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_dev_company.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <?
    /*if (!$APPLICATION->get_cookie("POPUP_BANER")):
        $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "popup",
                    Array(
                            "TYPE" => "popup",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
        );
        $APPLICATION->set_cookie("POPUP_BANER", 1, time()+60*60*24, "/");
    endif;*/
    ?>
    <?
    setSeo();
    ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33504724-1']);
  _gaq.push(['_setDomainName', 'restoran.ru']);


  _gaq.push (['_addOrganic', 'images.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'blogs.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'video.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'mail.ru', 'q']);
  _gaq.push (['_addOrganic', 'go.mail.ru', 'q']);
  _gaq.push (['_addOrganic', 'google.com.ua', 'q']);
  _gaq.push (['_addOrganic', 'images.google.ru', 'q']);
  _gaq.push (['_addOrganic', 'maps.google.ru', 'q']);
  _gaq.push (['_addOrganic', 'rambler.ru', 'words']);
  _gaq.push (['_addOrganic', 'nova.rambler.ru', 'query']);
  _gaq.push (['_addOrganic', 'nova.rambler.ru', 'words']);
  _gaq.push (['_addOrganic', 'gogo.ru', 'q']);
  _gaq.push (['_addOrganic', 'nigma.ru', 's']);
  _gaq.push (['_addOrganic', 'search.qip.ru', 'query']);
  _gaq.push (['_addOrganic', 'webalta.ru', 'q']);
  _gaq.push (['_addOrganic', 'sm.aport.ru', 'r']);
  _gaq.push (['_addOrganic', 'meta.ua', 'q']);
  _gaq.push (['_addOrganic', 'search.bigmir.net', 'z']);
  _gaq.push (['_addOrganic', 'search.i.ua', 'q']);
  _gaq.push (['_addOrganic', 'index.online.ua', 'q']);
  _gaq.push (['_addOrganic', 'web20.a.ua', 'query']);
  _gaq.push (['_addOrganic', 'search.ukr.net', 'search_query']);
  _gaq.push (['_addOrganic', 'search.com.ua', 'q']);
  _gaq.push (['_addOrganic', 'search.ua', 'q']);
  _gaq.push (['_addOrganic', 'poisk.ru', 'text']);
  _gaq.push (['_addOrganic', 'go.km.ru', 'sq']);
  _gaq.push (['_addOrganic', 'liveinternet.ru', 'ask']);
  _gaq.push (['_addOrganic', 'gde.ru', 'keywords']);
  _gaq.push (['_addOrganic', 'affiliates.quintura.com', 'request']);
  _gaq.push (['_addOrganic', 'akavita.by', 'z']);
  _gaq.push (['_addOrganic', 'search.tut.by', 'query']);
  _gaq.push (['_addOrganic', 'all.by', 'query']);


  _gaq.push(['_trackPageview']);
  setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])",15000);


  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 
<?/*
<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter17073367 = new Ya.Metrika({id:17073367,
    enableAll: true});
    } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") +
    "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
 * 
 */?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter17073367 = new Ya.Metrika({id:17073367,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?/*if ($USER->IsAuthorized()):?>
<script>
    $(document).ready(function(){
        $.post("/tpl/ajax/get_system_messages.php",function(data){
            var c = String(data);
            c = c.replace(/^\s*([\S\s]*?)\s*$/, '$1');
            if (c.length>1)
            {
                $("#system_message").html(data);
                $("#system_message").fadeIn(300);
            }            
        });
    });
</script>
<?endif;*/?>
<?if ($_COOKIE["NEED_SELECT_CITY"]=="Y"):?>
    <script>
        if (!$("#city_modal").size())
        {
            $("<div class='popup popup_modal' id='city_modal' style='width:320px;padding-bottom:30px'></div>").appendTo("body");
            $('#city_modal').load('/tpl/ajax/city_select2.php?<?=bitrix_sessid_get()?>', function(data) {
                setCenter($("#city_modal"));
                showOverflow();
                $("#city_modal").fadeIn("300");
            });
        }
    </script>
    <?unset($_COOKIE["NEED_SELECT_CITY"]);?>
<?endif;?>
<?/*if (CITY_ID=="msk"):?>
    <script type='text/javascript'>
	var liveTex = true,
		liveTexID = 47884,
		liveTex_object = true;
	(function() {
		var lt = document.createElement('script');
		lt.type ='text/javascript';
		lt.async = true;
        lt.src = 'http://cs15.livetex.ru/js/client.js';
		var sc = document.getElementsByTagName('script')[0];
		if ( sc ) sc.parentNode.insertBefore(lt, sc);
		else  document.documentElement.firstChild.appendChild(lt);
	})();
    </script>
<?endif;*/?>
    <script>
//    $(document).ready(function(){
//        $.post("/tpl/ajax/get_system_messages.php",function(data){
//            var c = String(data);
//            c = c.replace(/^\s*([\S\s]*?)\s*$/, '$1');
//            if (c.length>1)
//            {
//                $("#system_message").html(data);
//                $("#system_message").fadeIn(300);
//            }            
//        });
//    });
</script>
</body>
</html>