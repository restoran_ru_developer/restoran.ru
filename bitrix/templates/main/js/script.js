var ajax_load = false;
jQuery(document).ready(function() {        
    if ($.browser.msie ) {
		if ($.browser.version=="8.0")
			$.fx.off = true;
    }
    //snow
    //$('body').snowFall({'color':'#24A6CF','total':'30','interval':'5'});
    //    
    // ajax loader    
    setInterval("start_rounded()",3000);
    jQuery('input[placeholder], textarea[placeholder]').placeholder();
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/144.gif" align="left"> Загрузка...</div>');
    $('body').append('<div id="system_overflow"></div>');    
    /*$("#system_loading").css("left", $(window).width() / 2 - 104 / 2 + "px");
    $("#system_loading").css("top", $(window).height() / 2 - 104 / 2 + "px");*/
    $.ajaxSetup({
        async: true,
        cache: true
    });
    
    //datepicker
    $.tools.dateinput.localize("ru",  {
        months:        'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря',
        shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',
        days:          'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',
        shortDays:     'вс,пн,вт,ср,чт,пт,сб'
    });
    $(".baner2 div").css("margin","0 auto");
    $("#invite_friend").hover(function(){
        $(this).find("div.restik").slideDown("fast");
    },
    function(){
        $(this).find("div.restik").slideUp("fast");
    });
    //Banners
    $(".banner_ajax").each(function(){        
        $(this).html($("#baner_inv_block").find("#baner_"+$(this).attr("id")).html());
        $("#baner_inv_block").find("#baner_"+$(this).attr("id")).html("");
    });
    $("#invite_friend").click(function(){
        var _this = $(this);
        $.ajax({
            type: "POST",
            url: "/tpl/ajax/invite2site.php",
            data: "",
            success: function(data) {
                if (!$("#add_invite").size())
                {
                    $("<div class='popup popup_modal' id='add_invite'></div>").appendTo("body");                                                               
                }
                $('#add_invite').html(data);
                showOverflow();
                setCenter($("#add_invite"));
                $("#add_invite").fadeIn("300"); 
            }
        });
        return false;
    });
    $("#system_loading").bind("ajaxSend",
        function() {
            $(this).css("display","block");
            ajax_load = true;
        }).bind("ajaxComplete", function() {
            $(this).css("display","none");
            ajax_load = false;
        });

    var params = {
        changedEl:"#search_in",
        visRows:12
        /*scrollArrows: true*/
    }
    cuSel(params);
    //Клик на серый фон
    $("#system_overflow").click(function(){
        $(".big_modal").css("display","none");
        hideOverflow();
        $(".popup").css("display","none");
    });
    
    $("ul.tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")
        {
            if ($(this).attr("history")=="true")
                var history = true;
            else
                var history = false;
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.panes > .pane", {                
                history: history,
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));
                        			
                    }	
                    
                }
            });
        }
        else
        {
            if ($(this).attr("history")=="true")
                var history = true;
            else
                var history = false;
            $(this).tabs("div.panes  .pane",{history: history,
            	onClick: function(event, i) {
            		$(".bx-yandex-map").each(function(){
            			var i = $(this).attr("id");
            			i = i.split('BX_YMAP_');
            			m = window.GLOBAL_arMapObjects[i[1]];
            			if(m) m.container.fitToViewport();
            		});
            		
            	}
            });
        }
    });
    
    

    
    $("#poster_tabs li").click(function(){
        if ($(this).parents(".pane").attr("main")=="main_page")
            $('.poster_pane').jScrollPane({verticalGutter:15});
    });  
    //$("ul#poster_tabs").tabs("div.poster_panes > .poster_pane");
    $("ul#poster_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.poster_panes > .poster_pane", {                
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"),function(){
                            if ($(this).parents(".pane").attr("main")=="main_page")
                                $('.poster_pane').jScrollPane({verticalGutter:15});
                        });			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.poster_panes > .poster_pane",{effect: 'fade'});
    }); 
    $("ul.history_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.panes > .pane", {                
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.panes > .pane",{effect: 'fade'});
    }); 
    $(".to_top").on('click', function() {
        $('html,body').animate({scrollTop:"0"}, 500);
    });    
    $("a.anchor").click(function(){
        var pos = $($(this).attr("href")).offset().top-10;
        $('html,body').animate({scrollTop:pos}, "300");
    });
    $("input[type^='radio']").each(
        function() {
             changeRadioStart($(this));
    });
    $(".modal_close").live('click',function(){
        $(this).parents(".popup_modal").css("display","none");
        hideOverflow();
    });
    $(".modal_close_galery").live('click',function(){
        $(".big_modal").css("display","none");
        hideOverflow();
    });

    jQuery("body").on('click',".niceCheck2", function() {
        changeCheck2(jQuery(this));
    });

    jQuery("body").on('click','.niceCheck2Label', function() {
        changeCheck2(jQuery(this).parent().parent().find(".niceCheck2"));
    });
    jQuery(".niceCheck2").each(
    function() {

        changeCheckStart2(jQuery(this));

    });     
    $("#add_rest").click(function(){
        $.ajax({
            type: "POST",
            url: "/tpl/ajax/add_rest.php",
            data: params,
            success: function(data) {
                if (!$("#add_rest_form").size())
                {
                    $("<div class='popup popup_modal' id='add_rest_form' style='width:335px'></div>").appendTo("body");                                                               
                }
                $('#add_rest_form').html(data);
                showOverflow();
                setCenter($("#add_rest_form"));
                $("#add_rest_form").css("display","block"); 
            }
        });
    });
});

function changeCheck2(el)
{
    var el = el,
        input = el.find("input").eq(0);
        if(!input.attr("checked")) {
                el.css("background-position","0 -19px");	
                input.attr("checked", true)
        } else {
                el.css("background-position","0 0");	
                input.attr("checked", false)
        }
    return true;
}

function changeCheckStart2(el)
{
    var el = el,
                input = el.find("input").eq(0);
    if(input.attr("checked")) {
                el.css("background-position","0 -19px");	
                }
    return true;
}

function showExpose() {
    $(document).mask({color: '#1a1a1a', loadSpeed: 200, closeSpeed:0, opacity: 0.5, closeOnEsc: false, closeOnClick: false});
}
function closeExpose() {
    $.mask.close();
}

function send_ajax(url, dataType, params, successFunc) {
    //showExpose();
    if(!dataType)
        dataType = 'json';
    params = eval('(' + params + ')');
    $.ajax({
        url:url,
        type: 'post',
        dataType: dataType,
        data: params,
        statusCode: {
            404:function() {
                alert('Page not found');
            }
        },
        success:function(data) {
            if (successFunc)
                successFunc(data);
            //closeExpose();
        }
    });
}

function sumbit_form(form, beforeFunc,successFunc) {
    var params = $(form).serialize();
    var url = $(form).attr("action");
    console.log(url);
    $.ajax({
        url:url,
        type:'post',
        data:params,
        statusCode:{
            404:function() {
                alert('Page not found');
            }
        },
        beforeSend:function() {
            /*if (typeof(before_submit_form) == "function")
                before_submit_form(form);
            return false;*/
            if (beforeFunc)
                beforeFunc(form);
        },
        success:function(data) {
            $(form).parent().html(data);
            /*if (typeof(success_submit_form) == "function")
                success_submit_form();*/
            if (successFunc)
                successFunc(form,data);
        }
    });
    return false;
}

function show_popup(obj,options)
{
    if(!options)
        options = {"obj":$(obj).parent().attr("id")};
    else
        options.obj = $(obj).parent().attr("id");    
    if (!$(obj).parent().find("div").hasClass("popup"))
        $("<div>").addClass("popup").appendTo($(obj).parent()).popup(options);
}

function pluralForm(n, form1, form2, form5){
    var n = Math.abs(n) % 100;
    var n1 = n % 10;
    if (n > 10 && n < 20) return form5;
    if (n1 > 1 && n1 < 5) return form2;
    if (n1 == 1) return form1;
    return form5;
}

function createModal(id)
{
    
    
}
function setCenter(obj)
{
    var ow = obj.width();
    var oh = obj.height();
    var opt = obj.css("padding-top");
    if (opt)
        opt = opt.replace("px","");
    var opb = obj.css("padding-bottom");
    if (opb)
        opb = opb.replace("px","");    
    var opl = obj.css("padding-left");
    if (opl)
        opl = opl.replace("px","");  
    var opr = obj.css("padding-right");
    if (opr)
        opr = opr.replace("px","");  
    if (opl=="auto")
        opl = 0;
    if (opr=="auto")
        opr = 0;
    if (opb=="auto")
        opb = 0;
    if (opt=="auto")
        opt = 0;
    
    var omt = obj.css("margin-top");
    if (omt)
        omt = omt.replace("px","");
    var omb = obj.css("margin-bottom");
    if (omb)
        omb = omb.replace("px","");    
    var oml = obj.css("margin-left");
    if (oml)
        oml = oml.replace("px","");  
    var omr = obj.css("margin-right");
    if (omr)
        omr = omr.replace("px","");
    if (oml=="auto")
        oml = 0;
    if (omr=="auto")
        omr = 0;
    if (omb=="auto")
        omb = 0;
    if (omt=="auto")
        omt = 0;
    oh = oh*1 + opt*1 + opb*1 + omt*1 + omb*1;
    ow = ow*1 + opl*1 + opr*1 + oml*1 + omr*1;
    if (obj.attr("class")=="big_modal")
    {
      obj.css("position","absolute");  
    }
    //obj.css("position","absolute");
    obj.css("top",($(window).height()-oh)/2+"px");
    obj.css("left",($(window).width()-ow)/2+"px");
    obj.css("z-index",10000);
}
function showOverflow()
{
    $("body").css("overflow","hidden");
    //$("#system_overflow").css("display","block");
    $("#system_overflow").fadeIn(300);
    //$("#system_overflow").css("opacity",.5);
    $("#system_overflow").css("height",$(document).height());
}
function hideOverflow()
{
    //$("#system_overflow").css("display","none");
    $("#system_overflow").fadeOut(300);
    $("body").css("overflow","auto");
}

function send_message(id,name)
{
    var _this = $(this);
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/im.php",
        data: "USER_NAME="+name+"&USER_ID="+id,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
            }
            $('#mail_modal').html(data);
            showOverflow();
            setCenter($("#mail_modal"));
            $("#mail_modal").fadeIn("300"); 
        }
    });
    return false;
}

function show_review_form()
{
    var _this = $(this);
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/review_form.php",
        data: "",
        success: function(data) {
            if (!$("#review_modal").size())
            {
                $("<div class='popup popup_modal' id='review_modal'></div>").appendTo("body");                                                               
            }
            $('#review_modal').html(data);
            showOverflow();
            setCenter($("#review_modal"));
            $("#review_modal").fadeIn("300"); 
        }
    });
    return false;
}

function ajax_bron(params,a)
{
    var b = "400px";
    if (a)
        b = "800px";
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_rest.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");                                                               
            }
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").css("display","block"); 
        }
    });
}

function ajax_bron2(params,a)
{
    var b = "400px";
    if (a)
        b = "800px";
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_banket.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");                                                               
            }
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").css("display","block"); 
        }
    });
}

function call_me(params)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/call_me.php",
        data: params,
        success: function(data) {
            if (!$("#call_me_form").size())
            {
                $("<div class='popup popup_modal' id='call_me_form' style='width:335px'></div>").appendTo("body");                                                               
            }
            $('#call_me_form').html(data);
            showOverflow();
            setCenter($("#call_me_form"));
            $("#call_me_form").css("display","block"); 
        }
    });
}

function invite2rest(params)
{
    
    var _this = $(this);
        $.ajax({
            type: "POST",
            url: "/tpl/ajax/invite2rest.php",
            data: params,
            success: function(data) {
                if (!$("#invite2rest_modal").size())
                {
                    $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                }
                $('#invite2rest_modal').html(data);
                showOverflow();
                setCenter($("#invite2rest_modal"));
                $("#invite2rest_modal").css("top","150px")
                $("#invite2rest_modal").fadeIn("300"); 
            }
        });
        return false;
}
function favorite_success(data)
{
    //data = eval('(' + data + ')');
    if (!$("#favorite_form").size())
    {
        $("<div class='popup popup_modal' id='favorite_form' style='width:335px'></div>").appendTo("body");                                                               
    }
    $('#favorite_form').html(data);
    showOverflow();
    setCenter($("#favorite_form"));
    $("#favorite_form").fadeIn("300"); 
    if (!data.ERROR)
    {        
        if (data.MESSAGE)
            $("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.MESSAGE);
    }
    else
            $("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.ERROR);
}

function start_rounded()
{
    if ($("#rounded1").is(":hidden"))
    {
        $("#rounded1").show();
        $("#rounded_first").show();
        $("#rounded2").hide();
        $("#rounded_second").hide();
    }
    else
    {
        $("#rounded1").hide();
        $("#rounded_first").hide();
        $("#rounded2").show();
        $("#rounded_second").show();
    }
}