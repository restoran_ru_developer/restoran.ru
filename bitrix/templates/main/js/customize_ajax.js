if (window.jsAjaxUtil) 
{ 
	// ������������� ����� jsAjaxUtil.ShowLocalWaitWindow() 
	jsAjaxUtil.ShowLocalWaitWindow = function (TID, cont, bShadow) 
	{ 
		if (typeof cont == 'string' || typeof cont == 'object' && cont.constructor == String) 
			var obContainerNode = document.getElementById(cont); 	
		else 
			var obContainerNode = cont; 
		if (null == bShadow) bShadow = true; 
			var container_id = obContainerNode.id; 
		if (bShadow) 
		{ 
			// ���� ����� ���������� ��������� - ���������17 
			var obWaitShadow = 
			document.body.appendChild(document.createElement('DIV')); 
			obWaitShadow.id = 'waitshadow_' + container_id + '_' + TID; 
			obWaitShadow.className = 'waitwindowlocalshadow'; 
			   
			if (jsAjaxUtil.IsIE())  
			{ 
				// ��� MSIE � �������� ���� �� ���� ������ ����������� ����
				obWaitShadow.style.height = document.body.scrollHeight + 'px'; 
			} 
			else  
			{ 
				// ��� ���������� ������� �� �������������
				obWaitShadow.style.position = 'fixed'; 
			} 
		} 
   
		// �������� ��������� � ��������
		var obWaitMessage = 
		document.body.appendChild(document.createElement('DIV')); 
		obWaitMessage.id = 'wait_' + container_id + '_' + TID; 
		obWaitMessage.className = 'waitwindowlocal'; 
   
		if (jsAjaxUtil.IsIE()) 
		{ 
			var left = parseInt(document.body.scrollLeft + 
			document.body.clientWidth/2 - obWaitMessage.offsetWidth/2); 
			var top = parseInt(document.body.scrollTop + 
			document.body.clientHeight/2 - obWaitMessage.offsetHeight/2); 
		} 
		else 
		{ 
			var left = parseInt(document.body.clientWidth/2 - 
			obWaitMessage.offsetWidth/2); 
			var top = parseInt(document.body.clientHeight/2 - 
			obWaitMessage.offsetHeight/2); 
			obWaitMessage.style.position = 'fixed'; 
		} 
		obWaitMessage.style.top = top; 18 

		obWaitMessage.style.left = left; 
   
		// ������� �����
		obWaitMessage.innerHTML = '���������, ���� ��������...'; 
		if(jsAjaxUtil.IsIE()) 
		{ 
			// ��� IE6 � ���� �������� ��� ���������� ��������� �����  
			var frame = document.createElement("IFRAME"); 
			frame.src = "javascript:''"; 
			frame.id = 'waitframe_' + container_id + '_' + TID; 
			frame.className = "waitwindowlocal"; 
			frame.style.width = obWaitMessage.offsetWidth + "px"; 
			frame.style.height = obWaitMessage.offsetHeight + "px"; 
			frame.style.left = obWaitMessage.style.left; 
			frame.style.top = obWaitMessage.style.top; 
			document.body.appendChild(frame); 
		} 
		// ������� ���������� ������� ������� Esc. 
		function __Close(e) 
		{ 
			if (!e) e = window.event 
			if (!e) return; 
			if (e.keyCode == 27) 
			{ 
				jsAjaxUtil.CloseLocalWaitWindow(TID, cont); 
				jsEvent.removeEvent(document, 'keypress', __Close); 
			} 
		} 
   
		jsEvent.addEvent(document, 'keypress', __Close); 
	} 
}