jQuery.fn.news_galary = function(options){
    // настройки по умолчанию
    var options = jQuery.extend({    
        speed:1000,
        content_width:978    
    },options);
    
    var container = jQuery(this).find(".special_scroll");
    var item = container.find(".item");
    //var item_width = item.width()*1+item.css("margin-right").replace("px","")*1+item.css("padding-right").replace("px","")*1+item.css("padding-left").replace("px","")*1;
    var item_width = item.width()*1+item.css("margin-right").replace("px","")*1;
    var all_width = container.width();
    var delta_width = Math.round((all_width - options.content_width)/2);    
    
    this.activate = function(obj) {
        if (container.find(".active").next().hasClass("item")) {                        
            container.find(".active").removeClass("active");
            obj.addClass("active");
        }
        else {            
            container.find(".active").removeClass("active");
            obj.addClass("active");            
        }        
    };
    
    this.next = function(obj) {
        this.activate(obj);
        console.log(obj);
        if (obj.attr("num")>4)
            $(".item").eq(0).remove().appendTo($(this).find(".scroll_container"));
        var left = item_width*(obj.attr("num")-1)-delta_width;
        $(this).find(".scroll_container").animate({"marginLeft":-left},options.speed);
    };
    
    this.slide = function(a) {
        $(this).find(".scroll_container").animate({"marginLeft":-a},options.speed);
    };
    
    this.init = function() {
        var _this = this;              
        container.find(".item").click(function(){            
            _this.next($(this));                     
        });
    
        $(window).resize(function() {
            item_width = item.width()*1+item.css("margin-right").replace("px","")*1+item.css("padding-right").replace("px","")*1+item.css("padding-left").replace("px","")*1;
            all_width = container.width();
            delta_width_a = delta_width;
            delta_width = Math.round((all_width - options.content_width)/2);    
            //_this.slide(-delta_width_a);
        });
    };
    return this.init();
}