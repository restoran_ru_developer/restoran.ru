jQuery.fn.galery = function(options){
    // настройки по умолчанию
    var options = jQuery.extend({
        num: 5,
        speed:500,
        count: true
    },options);
    
    var num = jQuery(this).find(".scroll_container div").length;
    var container = jQuery(this).find(".special_scroll");
    var item = container.find(".item");
    
    this.counter = function(a,b) {
        var obj = jQuery(this).find(".scroll_num");
        var obj_val = obj.html()*1;
        if (!b)
        {
            if (a) 
            {
                if (obj_val==num)
                    obj.html(1);
                else
                    obj.html(++obj_val);
            }
            else
            {
                if (obj_val==1)
                    obj.html(num);
                else
                    obj.html(--obj_val);
            }
        }
        else
        {
            obj.html(b.attr("num"));
        }
    };
    
    this.next = function(obj) {
        var url;
        var alt;
        if (container.find(".active").next().hasClass("item"))
        {                        
            if (typeof(obj)=="undefined")
            {
                url = container.find(".active").removeClass("active").next().addClass("active").find("img").attr("src");            
                alt = container.find(".active").removeClass("active").next().addClass("active").find("img").attr("alt");            
                
            }
            else
            {
                container.find(".active").removeClass("active");
                url = obj.addClass("active").find("img").attr("src");
                alt = obj.addClass("active").find("img").attr("alt");
            }
            if (options.count) this.counter(1,obj);
        }
        else
        {            
            if (typeof(obj)=="undefined")
            {
                url = container.find(".active").removeClass("active").end().find(".item:first").addClass("active").find("img").attr("src");            
                alt = container.find(".active").removeClass("active").end().find(".item:first").addClass("active").find("img").attr("alt");            
            }
            else
            {
                container.find(".active").removeClass("active");
                url = obj.addClass("active").find("img").attr("src");                
                alt = obj.addClass("active").find("img").attr("alt");                
            }
            if (options.count) this.counter(1,obj);
            $(this).find(".scroll_container").animate({"marginLeft":0},options.speed,"linear");                
        }
        var wrap = $(this).find(".img");
        var video = "";
        if (container.find(".active").find("img").attr("video"))
            video = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="300" border="0"><param name="movie" value="/tpl/js/panorama.swf?img='+url+'&amp;type=0"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"><embed src="/tpl/js/panorama.swf?img='+url+'&amp;type=0" width="397" height="300" border="0" type="application/x-shockwave-flash"></object>';
        if (video)
        {
            if (wrap) 
                wrap.fadeTo("medium", 0.2,function(){ $(this).html(video);}).fadeTo("fast", 1);
        }
        else
        {
            if (wrap) 
                wrap.fadeTo("medium", 0.2,function(){ $(this).find("img").attr("src",url); $(this).find("img").attr("alt",alt);}).fadeTo("fast", 1);                
        }
        if (container.find(".active").attr("num")>=options.num)
        {                
            var left = item.width()*1+item.css("margin-right").replace("px","")*1;
            $(this).find(".scroll_container").animate({"marginLeft":"-="+left},options.speed,"linear");                
        }
    };
    
    this.prev = function(obj) {
        var url;
        if (container.find(".active").prev().hasClass("item"))
        {                
            if (!obj)
                url = container.find(".active").removeClass("active").prev().addClass("active").find("img").attr("src");            
            else
            {
                container.find(".active").removeClass("active");
                url = obj.addClass("active").find("img").attr("src");
            }
            if (options.count) this.counter(0,obj);
            if (container.find(".active").attr("num")>=options.num)
            {                
                var left = item.width()*1+item.css("margin-right").replace("px","")*1;
                $(this).find(".scroll_container").animate({"marginLeft":"-="+-left},options.speed,"linear");                                    
            }
            if (obj&&obj.attr("num")*1<=options.num*1)
                $(this).find(".scroll_container").animate({"marginLeft":"0px"},options.speed,"linear");                

        }
        else
        {                
            if (!obj) 
                url =  container.find(".active").removeClass("active").end().find(".item:last").addClass("active").find("img").attr("src");            
            else
            {
                container.find(".active").removeClass("active");
                url = obj.addClass("active").find("img").attr("src");
            }
            if (options.count) this.counter(0,obj);
            var mleft = item.width()*1+item.css("margin-right").replace("px","")*1;
            var left = mleft*num-mleft*options.num;
            $(this).find(".scroll_container").animate({"marginLeft":-left},options.speed,"linear");                
        }
        var wrap = $(this).find(".img");
        var video = "";
        if (container.find(".active").find("img").attr("video"))
            video = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="397" height="300" border="0"><param name="movie" value="/tpl/js/panorama.swf?img='+url+'&amp;type=0"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"><embed src="/tpl/restoran.ru/js/panorama.swf?img='+url+'&amp;type=0" width="397" height="300" border="0" type="application/x-shockwave-flash"></object>';
        if (video)
        {
            if (wrap) 
                wrap.fadeTo("medium", 0.2,function(){ $(this).html(video);}).fadeTo("fast", 1);
        }
        else
        {
            if (wrap) 
                wrap.fadeTo("medium", 0.2,function(){ $(this).find("img").attr("src",url);}).fadeTo("fast", 1);                
        }
        if (wrap) wrap.fadeTo("medium", 0.2,function(){ $(this).find("img").attr("src",url);}).fadeTo("fast", 1); 
    };
    
    this.init = function() {
      var _this = this;              
        container.find(".item").click(function(){            
            var current = container.find(".active").attr("num")*1;
            var i=1;
            if ($(this).attr("num")*1>=options.num&&$(this).attr("num")*1>current)
            {
                    _this.next($(this));                     
            }
            else if ($(this).attr("num")*1<options.num&&$(this).attr("num")*1>current)
            {
                _this.next($(this));                     
            }
            else if ($(this).attr("num")*1>=options.num&&$(this).attr("num")*1<current)
            {
                _this.prev($(this));
            }
            else
                _this.prev($(this));
        });

        jQuery(this).find(".scroll_arrows").find(".next").click(function(){        
            _this.next();
        });
        
        jQuery(this).find(".scroll_arrows").find(".prev").click(function(){                    
           _this.prev();
        }); 
    };
    return this.init();
}