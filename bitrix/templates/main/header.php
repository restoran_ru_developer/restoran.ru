<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    
    <title><?$APPLICATION->ShowTitle()?></title>
    <?if (!$_REQUEST["CODE"]):?>
        <meta property="og:image" content="http://www.restoran.ru/images/logo.png" />
    <?endif;?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <?//$APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true)?>
    <?//$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/cusel.css"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadString('<link href="/tpl/css/styles.css?1234567"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/datepicker.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/style_min.css?244"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/popup.css?24"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadScript('http://cdn.jquerytools.org/1.2.6/all/jquery.tools.min.js')?>
        <?//$APPLICATION->AddHeadScript('/bitrix/templates/kupon/js/cusel-min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>    
    <?//$APPLICATION->AddHeadScript('/tpl/js/detail_galery.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js')?>    
    <?$APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel2.js')?> 
    <?//$APPLICATION->AddHeadScript('/tpl/js/mwheelIntent.js')?> 
    <?//$APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js')?>    
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.jscrollpane.js')?>             
    <?//$APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js')?>    
    <?//$APPLICATION->AddHeadScript('/tpl/js/cusel-min.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.ontop.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.system_message.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/radio.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery_placeholder.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/script_min.js?21')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/baner/script.js')?>
    <?////$APPLICATION->AddHeadScript('/tpl/js/jquery.letsinsnow.js')?>

    <?$APPLICATION->ShowHead()?>
    <?//$APPLICATION->ShowHeadScripts()?>
    <!--[if IE 7]><link href="/bitrix/templates/main/ie.css?3" rel="stylesheet" media="all" /><![endif]-->
    <!--[if IE 8]><link href="/bitrix/templates/main/ie.css?2" rel="stylesheet" media="all" /><![endif]-->
    <?
    //Строки от Антона
    if(substr_count($_SERVER["REQUEST_URI"], "blog")>0 || substr_count($_SERVER["REQUEST_URI"], "kupo")>0 || substr_count($_SERVER["REQUEST_URI"], "restoran_edit")>0 || substr_count($_SERVER["REQUEST_URI"], "restorator")>0 || substr_count($_SERVER["REQUEST_URI"], "businessman")>0 || substr_count($_SERVER["REQUEST_URI"], "rest_edit")>0){
    	if (!CSite::InGroup(Array(1,15,16,23)))
        {
            //$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);        
        }
        else                
            $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
    	$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor/css/redactor.css"  type="text/css" rel="stylesheet" />',true);
	    $APPLICATION->AddHeadScript('/tpl/js/jquery-ui-1.8.17.custom.min.js');
   	 	
   	 	 $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/redactor_list_script.js?12');
    }  
    $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);  
    
    CModule::IncludeModule("advertising");    
    if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
        $page = "others";
    else
        $page = "main";
    CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));
    
    //unset($_SESSION["CONTEXT"]);
    //unset($_REQUEST["CONTEXT"]);
    ?>
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
    <meta property="fb:app_id" content="297181676964377" />
    <?if (CITY_ID=="tmn"):?>
        <meta property="fb:admins" content="100005128267295,100004709941783" />    
    <?else:?>
        <meta property="fb:admins" content="100004709941783" />
    <?endif;?>
</head>
<body>    
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '297181676964377', // App ID
                channelURL : '//<?=SITE_SERVER_NAME?>/channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                oauth      : true, // enable OAuth 2.0
                xfbml      : true  // parse XFBML
            });
        };
        
        (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
        }(document));
    </script>
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="container" <?/*=($APPLICATION->GetCurDir()=="/msk/opinions/")?"style='background:url(/tpl/images/brend/nyanma/nyanma_bg.jpg) center top repeat-y #543028'":""*/?>>        
        <?/*if (($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/msk/")&&CITY_ID=="msk"):?>
            <div id="brend" style="background:url(/tpl/images/brend/beliy/beliy_3.jpg) center top no-repeat;  position:absolute; top:0px; z-index:99" onclick="location.href='http://beliyresto.ru/'"></div>
            <script>
            $(function(){
                $("#brend").css("height","824px");
                $("#brend").css("width",$("#wrapp").width()+"px");
                $("#wrapp").css("z-index",100);
                $("#wrapp").css("position","relative");
            })
            </script>
        <?endif;*/?>
        <?/*if ($USER->IsAdmin()&&$APPLICATION->GetCurDir()=="/msk/opinions/"):?>
            <div id="brend" style="background:url(/tpl/images/brend/nyanma/niyanma1_3.jpg) center top no-repeat #543028;  position:absolute; top:0px; z-index:99"></div>
            <script>
            $(function(){
                $("#brend").css("height",$("#wrapp").height()+"px");
                $("#brend").css("width",$("#wrapp").width()+"px");
                $("#wrapp").css("z-index",100);
                $("#wrapp").css("position","relative");
            })
            </script>
        <?endif;*/?>
        <?/*<?=($USER->IsAdmin())?"style='background:url(/tpl/images/brend/nyanma/niyanma1_3.jpg) 50% top no-repeat #543028'":""?>*/?>
        <div id="wrapp" <?/*=(CITY_ID=="msk"&&($APPLICATION->GetCurDir()=="/"||$APPLICATION->GetCurDir()=="/msk/"))?"class='gn'":""*/?>  <?/*=($APPLICATION->GetCurDir()=="/msk/opinions/")?"style='background:url(/tpl/images/brend/nyanma/niyanma1_3.jpg) 50% top no-repeat' class='nyan'":""*/?>>            
            <?/*if ($APPLICATION->GetCurDir()=="/msk/opinions/"):?>            
            <script>
                $(function(){
                   $("#wrapp").css("cursor","pointer");
                   $("#wrapp div").css("cursor","default");
                   $("#wrapp").click(function(){
                       location.target="_blank";
                       location.href="http://www.niyama.ru/about/promo/";                       
                   });
                   $("#wrapp div").click(function(e){
                       e.stopPropagation();
                   });
                });
            </script>
            <?endif;*/?>
            <div id="header">
                <div class="i_b_r">
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "top_main_page_".CITY_ID,
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "0"
                            ),
                    false
                    );?>
                </div> 
                <div id="logo" class="left"><a href="/<?=CITY_ID?>/"><img src="/tpl/images/logo.png" /></a></div>                
                <div class="city_select left" style="margin-top:20px; margin-right:10px;width:140px">  
                    <div style="margin-right:15px; margin-bottom:16px;">
                        <?$APPLICATION->IncludeComponent(
                            "restoran:city.selector",
                            "city_select",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_URL" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000000",
                                "CACHE_NOTES" => "new32123662428",
                                "CACHE_GROUPS" => "Y"
                            ),
                        false
                        );?>      
                        <div class="clear"></div>
                    </div>                    
                    <div style="margin-right:20px">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.site.selector",
                            "lang_selector",
                            Array(
                                "SITE_LIST" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "360000000000",
                                "CACHE_NOTES" => ""
                            ),
                        false
                        );?>                    
                        <div class="clear"></div>
                    </div>                    
                </div>
                <div class="left" style="margin-top:20px; text-align: center; margin-right:12px;">
                        <div class="phones" style="line-height:22px;margin-bottom:12px;font-size:32px; width:130px;text-align:left;<?=(CITY_ID=="msk"||CITY_ID=="spb")?"background:url(/tpl/images/phone_min.png) left top no-repeat; ":""?>">
                            <?
                            $APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                                Array(),
                                Array("MODE"=>"html")
                            );
                            ?>                            
                        </div>                        
                        <div>
                            <?/*if (CITY_ID=="ast"||CITY_ID=="krd"||CITY_ID=="tmn"||CITY_ID=="anp"||CITY_ID=="sch"||CITY_ID=="ufa"||CITY_ID=="kld"):?>
                                <script>
                                function cities_bron(params)
                                {
                                    $.ajax({
                                        type: "POST",
                                        url: "/tpl/ajax/bron_<?=CITY_ID?>.php",
                                        data: params,
                                        success: function(data) {
                                            if (!$("#cbron_form").size())
                                            {
                                                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
                                            }
                                            $('#cbron_form').html(data);
                                            showOverflow();
                                            setCenter($("#cbron_form"));
                                            $("#cbron_form").css("display","block"); 
                                        }
                                    });
                                }
                                </script>
                                <a href="javascript:void(0)" onclick="cities_bron('<?=bitrix_sessid_get()?>')" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                            <?else:*/?>
                                
                            <?//endif;?>
                        </div>
                </div>
                <div class="left" style="margin-top:22px;">
                    <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>                        
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient2"  onclick="ajax_bron('<?=bitrix_sessid_get()?>')"><?=GetMessage("TABLE_BOOK")?></a>
                        </div>                              
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient1"  onclick="ajax_bron2('<?=bitrix_sessid_get()?>&banket=Y',1)"><?=GetMessage("BANKET_BOOK")?></a>
                        </div>
                    <?/*elseif (CITY_ID=="kld"):?>                        
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient2"  onclick="ajax_bron('<?=bitrix_sessid_get()?>',false,'_<?=CITY_ID?>')"><?=GetMessage("TABLE_BOOK")?></a>
                        </div>
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient1"  onclick="ajax_bron2('<?=bitrix_sessid_get()?>&banket=Y',1,'_<?=CITY_ID?>')"><?=GetMessage("BANKET_BOOK")?></a>
                        </div>
                    <?else:*/?>                         
                    <?endif;?>
                    <?if ((CITY_ID=="ast"||CITY_ID=="sch"||CITY_ID=="krd"||CITY_ID=="ufa"||CITY_ID=="nsk")):?>                        
                        <?if ($USER->IsAdmin()||CSite::InGroup(Array(15,16,27,23,24))):?>
                            <a href="/redactor/" class="for_n_bu"></a>
                        <?elseif (CSite::InGroup(Array(9))):?>
                            <a href="/restorator/" class="for_n_bu"></a>
                        <?elseif ($USER->IsAuthorized()):?>
                            <a href="/auth/register_restorator.php#restorator" class="for_n_bu"></a>
                        <?else:?>
                            <a href="/auth/register_restorator.php#restorator" class="for_n_bu"></a>
                        <?endif;?>  
                    <?endif;?>
                </div>
                <div class="right" style="<?=(!$USER->IsAuthorized())?"margin-top:20px;":""?> width:150px; overflow:hidden; position:relative">   
                    <div class="top_middle_menu" style="margin-right:0px;">
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:system.auth.form",
                                "user",
                                Array(
                                        "REGISTER_URL" => "",
                                        "FORGOT_PASSWORD_URL" => "",
                                        "PROFILE_URL" => "",
                                        "SHOW_ERRORS" => "N"
                                ),
                        false
                        );?> 
                        <div class="clear"></div>     
                        <div style="margin-top:5px;">
                            <?if ($USER->IsAdmin()||CSite::InGroup(Array(15,16,27,23,24))):?>
                                    <a href="/redactor/" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?elseif (CSite::InGroup(Array(9))):?>
<!--                                    <a href="javascript:void(0)" id="add_rest" class="greyb"><?=GetMessage("ADD_PLACE")?></a>-->
                                    <a href="/restorator/" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?elseif ($USER->IsAuthorized()):?>
                                    <a href="/auth/register_restorator.php#restorator" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?else:?>
                                    <a href="/auth/register_restorator.php#restorator" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?endif;?>         
                        </div>
                    </div>                                                                             
                </div>                            
                <?/*else:?>
                    <div class="phone_block right" onclick="ajax_bron('<?=bitrix_sessid_get()?>')"  style="margin-right:20px;">
                        <?=GetMessage("ORDER_BUBBLE_TEXT")?>
                    </div>
                <?endif;*/?>
                <div class="clear"></div> 
                <?/*if (CITY_ID=="msk"&&($APPLICATION->GetCurDir()=="/"||$APPLICATION->GetCurDir()=="/msk/")):?>
                    <div class="brend1" onclick="location.href='http://beliyresto.ru/'"></div>
                    <div class="brend2"  onclick="location.href='http://beliyresto.ru/'"></div>
                <?endif;*/?>
                <?/*if ($APPLICATION->GetCurDir()=="/msk/options/"):?>
                    <div class="brend1" onclick="location.href='http://www.niyama.ru/'"></div>
                    <div class="brend2"  onclick="location.href='http://www.niyama.ru/'"></div>
                <?endif;*/?>
            </div>
                <div id="block960">
                    <?if (SITE_ID=="s1"):?>
                    <div class="left" style="<?=(CITY_ID=="ufa"||CITY_ID=="ast")?'width:960px':'width:740px'?>">     
                    <?endif;?>
                        <div id="main_menu" <?if (SITE_ID=="s1"):?>style="<?=(CITY_ID=="ast")?'width:960px':'width:740px'?>"<?endif;?>>
                                <script>
                                    $(document).ready(function(){
                                        $("#main_menu").css("height","37px");
                                    });
                                </script>
                                <ul>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top_1",
                                        Array(
                                            "ROOT_MENU_TYPE" => "top_1_new_".CITY_ID,
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "top_1",
                                            "USE_EXT" => "N",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_TIME" => "3600000000",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                            "MENU_CACHE_GET_VARS" => array(CITY_ID,1)
                                        ),
                                    false
                                    );?>
                                </ul>
                        </div>
                        <div id="toptop_menu_new" style="margin:0px;<?=(CITY_ID=="ufa")?'width:820px':'width:740px'?><?if (SITE_ID!="s1"):?>margin-bottom:10px<?endif;?>">
                            <ul>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top_small_2_new_2_11",
                                        Array(
                                                "ROOT_MENU_TYPE" => "art_".CITY_ID,
                                                "MAX_LEVEL" => "1",
                                                "CHILD_MENU_TYPE" => "",
                                                "USE_EXT" => "N",
                                                "DELAY" => "N",
                                                "ALLOW_MULTI_SELECT" => "N",
                                                "MENU_CACHE_TYPE" => "A",
                                                "MENU_CACHE_TIME" => "3600000000",
                                                "MENU_CACHE_USE_GROUPS" => "N",
                                                "MENU_CACHE_GET_VARS" => array(CITY_ID,1)
                                        ),
                                false
                                );?> 
                            </ul>
                        </div>
                    <?if (SITE_ID=="s1"):?>
                    </div>
                    <div class="right"  style="<?=(CITY_ID=="ufa")?'width:140px':'width:220px'?>">
                        <?if (CITY_ID!="tln"):?>
                            <?if (CITY_ID=="msk"||CITY_ID=='spb'||CITY_ID=='kld'||CITY_ID=='tmn'):?>
                                <div style="width:240px; height:90px; margin-top: -22px; margin-left:15px">
                                    <?/*if (CITY_ID=="msk"):?>
                                        <a href="/msk/catalog/restaurants/rubric/fashion/" title="лучшие рестораны Москвы"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/rest_best.png" alt="лучшие рестораны москвы" title="лучшие рестораны москвы" /></a>
                                    <?endif;?>
                                    <?if (CITY_ID=="spb"):?>
                                        <a href="/spb/catalog/restaurants/rubric/fashion/" title="лучшие рестораны Санкт-Петербурга"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/rest_best_spb.jpg" alt="лучшие рестораны Санкт-Петербурга" title="лучшие рестораны Санкт-Петербурга" /></a>
                                    <?endif;*/?>
                                     <?if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="kld"||CITY_ID=='tmn'):?>   
                                        <?                                       
                                            $arBanIB = getArIblock("banner_in_header", CITY_ID);
                                            $APPLICATION->IncludeComponent(    
                                                "bitrix:news.list",
                                                "banner_in_header",
                                                Array(
                                                    "DISPLAY_DATE" => "Y",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "N",
                                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                                    "AJAX_MODE" => "N",
                                                    "IBLOCK_TYPE" => "banner_in_header",
                                                    "IBLOCK_ID" => $arBanIB["ID"],
                                                    "NEWS_COUNT" => "10",
                                                    "SORT_BY1" => "SORT",
                                                    "SORT_ORDER1" => "ASC",
                                                    "SORT_BY2" => "",
                                                    "SORT_ORDER2" => "",
                                                    "FILTER_NAME" => "",
                                                    "FIELD_CODE" => array(),
                                                    "PROPERTY_CODE" => array(),
                                                    "CHECK_DATES" => "Y",
                                                    "DETAIL_URL" => "",
                                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                                    "SET_TITLE" => "N",
                                                    "SET_STATUS_404" => "N",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                    "ADD_SECTIONS_CHAIN" => "N",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                    "PARENT_SECTION" => "",
                                                    "PARENT_SECTION_CODE" => "",
                                                    "CACHE_TYPE" => "Y",
                                                    "CACHE_TIME" => "14400",
                                                    "CACHE_FILTER" => "Y",
                                                    "CACHE_GROUPS" => "N",
                                                    "CACHE_NOTES" => "n",            
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                                    "PAGER_TITLE" => "Новости",
                                                    "PAGER_SHOW_ALWAYS" => "N",
                                                    "PAGER_TEMPLATE" => "",
                                                    "PAGER_DESC_NUMBERING" => "N",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "N",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "AJAX_OPTION_HISTORY" => "N"
                                                ),
                                                false
                                                );                                            
                                            ?>
<!--                                            <a class='main_r <?=($random)?"' style='display:none'":"active"?>' href="/<?=CITY_ID?>/articles/valentine/" title="день святого валентина"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/main_valentine.jpg" alt="день святого валентина" title="день святого валентина" /></a>                                                                            -->
<!--                                            <a class='main_r <?=($random==1)?"active":"' style='display:none'"?>' href="/<?=CITY_ID?>/articles/sochi2014/" title="Олимпиада в Сочи 2014"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/sochi.jpg" alt="Олимпиада в Сочи 2014" title="Олимпиада в Сочи 2014" /></a>-->
<!--                                            <a class='main_r <?=($random==2)?"active":"' style='display:none'"?>' href="/<?=CITY_ID?>/news/23_fevralya/" title="23 февраля"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/main_23f.jpg" alt="23 февраля" title="23 февраля" /></a>-->
<!--                                            <a class='main_r <?=($random==2)?"active":"' style='display:none'"?>' href="/<?=CITY_ID?>/articles/8marta/" title="8 Марта"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/8marta_head.jpg" alt="8 Марта" title="8 Марта" /></a>                                            -->
                                            <!--<a class='main_r <?=($random==3)?"active":"' style='display:none'"?>' href="/<?=CITY_ID?>/news/maslenitsa/" title="Маленица"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/main_masl.jpg" alt="Маленица" title="Маленица" /></a>-->                                            
                                    <?endif;?>                                         
                                    <?/*if (!$USER->IsAdmin()):?>
                                        <a onclick="ajax_bron('<?=bitrix_sessid_get()?>')" id="bron_round" href="javascript:void(0)" style="position:relative; text-decoration:none;">    
                                            <div id="bron_round_phone"></div>
                                            <?if (CITY_ID=="spb"):?>
                                                <div id="rounded2" class="bron_round_blue"></div>
                                            <?else:?>
                                                <div id="rounded2" class="bron_round_red"></div>
                                            <?endif;?>                                    
                                            <div id="rounded1" class="bron_round_orange"></div>                                    
                                            <div id="rounded_first" class="appetite appetite_bron_15">Хочу заказать столик<?//=GetMessage("NEW_YEAR")?></div>                                   
                                            <?if (CITY_ID=="spb"):?>
                                                <div id="rounded_second" class="appetite appetite_bron_15_2">(812)<br />740-1820<?//=GetMessage("NEW_YEAR")?></div>                                                                           
                                            <?else:?>
                                                <div id="rounded_second" class="appetite appetite_bron_15_2">(495)<br />988-2656<?//=GetMessage("NEW_YEAR")?></div>
                                            <?endif;?>
                                        </a>   
                                    <?else:?>

                                        <a id="bron_round" href="/<?=CITY_ID?>/articles/new_year/" style="position:relative; text-decoration:none;">    
                                            <div class="left" style="width:90px;margin-bottom:2px;margin-right: 5px" align="center"><img src="/tpl/images/new_year/ny_head2.jpg" /></div>
                                            <div class="left letn_title" style="width:135px; padding-top: 5px;margin-bottom:2px;">
                                                <div style="font-size:17px; line-height:18px; color:#24a5cf; font-family: appetite">Новый год</div>
                                                <div style="font-size:17px; line-height:18px; color:#24a5cf; font-family: appetite">совсем скоро!</div>
                                                <div style="font-style:italic; line-height:18px;">Веселый корпоратив?<br/>Мы найдем ресторан!</div>
                                            </div>
                                            <div class="clear"></div>
                                        </a>
                                    <??>
                                        <a id="bron_round" href="/<?=CITY_ID?>/articles/letnie_verandy/" style="position:relative; text-decoration:none;">    
                                            <div class="left" style="width:100px;margin-bottom:2px;"><img src="/tpl/images/letn.png" /></div>
                                            <div class="left letn_title" style="width:140px; padding-top: 10px;margin-bottom:2px;">
                                                <div style="font-size:14px; line-height:24px; text-transform: uppercase"><span style="font-size:20px">Л</span>етние веранды</div>
                                                <div style="font-style:italic">Мы подобрали для Вас более 50 ресторанов с отличным видом!</div>
                                            </div>
                                            <div class="clear"></div>
                                        </a>
                                    <?*/?>
                                    <?//endif;?>
                                        <?/*                             
                                        <!--<a href="/<?=CITY_ID?>/articles/8marta/"><img src="<?=SITE_TEMPLATE_PATH?>/images/1362071058_bouquet.png" /></a>-->
                                        <div style="position: absolute; z-index: 100; margin: 0px auto;">
                                            <a href="/<?=CITY_ID?>/articles/8marta/"><img src="/bitrix/images/1.gif" width="157" height="116" border="0" alt=""></a>                                
                                        </div>
                                        <object classid="clsid:D27CDB6E-AE6D-11CF-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="157" height="116">
                                            <param name="movie" value="<?=SITE_TEMPLATE_PATH?>/images/8mart3.swf">
                                            <param name="quality" value="high">
                                            <param name="bgcolor" value="#FFFFFF">
                                            <param name="wmode" value="opacue">
                                            <embed src="<?=SITE_TEMPLATE_PATH?>/images/8mart3.swf" quality="high" bgcolor="#FFFFFF" wmode="opacue" width="157" height="116" name="banner" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">
                                        </object>  
                                        <a href="/<?=CITY_ID?>/articles/8marta/"><img src="<?=SITE_TEMPLATE_PATH?>/images/8m.png" /></a>
                                        <!--<a href="/<?=CITY_ID?>/articles/valentine/"><img src="/tpl/images/valentine.png" /></a>-->*/?>
                                </div>
                            <?endif;?>
                            <?if (CITY_ID=="kld"):?>
<!--                                <div style="width:240px; height:90px; margin-top: -22px; margin-left:-20px">                                   
                                    <a id="bron_round" href="/kld/catalog/restaurants/all/" style="position:relative; text-decoration:none;">    
                                        <div class="left" style="width:90px;margin-bottom:2px;margin-right: 5px" align="center"><img src="/tpl/images/new_year/ny_head2.jpg" /></div>
                                        <div class="left letn_title" style="width:140px; padding-top: 5px;margin-bottom:2px;">
                                            <div style="font-size:17px; line-height:22px; text-transform: uppercase">Новый год<br />не за горами</div>
                                            <div style="font-style:italic; line-height:18px;">Веселый корпоратив?<br/>Мы найдем ресторан!</div>
                                        </div>
                                        <div class="clear"></div>
                                    </a>                                    
                                </div>-->
                            <?endif;?>
                        <?else:?>
<!--                            <div style="width:240px; height:90px; margin-top: -22px; margin-left:-20px">
                                <div style="width:112px; height:90px; margin-top: -30px;">                                    
                                    <a href="/<?=CITY_ID?>/articles/reklama/"><img src="/tpl/images/tmn_povar.png" /></a>
                                </div>
                                <a id="bron_round" href="/<?=CITY_ID?>/articles/new_year_corp/" style="position:relative; text-decoration:none;">    
                                        <div class="left" style="width:90px;margin-bottom:2px;margin-right: 5px" align="center"><img src="/tpl/images/new_year/ny_head2.jpg" /></div>
                                        <div class="left letn_title" style="width:140px; padding-top: 5px;margin-bottom:2px;">
                                            <div style="font-size:17px; line-height:22px; text-transform: uppercase">Новый год<br />не за горами</div>
                                            <div style="font-style:italic; line-height:18px;">Веселый корпоратив?<br/>Мы найдем ресторан!</div>
                                        </div>
                                        <div class="clear"></div>
                                    </a>
                            </div>-->
                        <?endif;?>
                        <!--<div style="width:112px; height:90px; margin-top: -20px;">
                            <a id="ny_ded" href="javascript:void(0)" style="position:relative; text-decoration:none;cursor:default">    
                                <div class="ny_round_64"></div>
                                <div class="cur_s"></div>
                                <div class="appetite appetite_ny_15"><?=GetMessage("NEW_YEAR")?></div>
                                <div id="ny_dm_92"></div>
                            </a>
                        </div>-->
                    </div>
                    <div class="clear"></div>
                    <?endif;?>
                </div>            
        <?/*<div id="block960">                                                                        
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                        "",
                        Array(
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "links_inc",
                                "AREA_FILE_RECURSIVE" => "Y",
                                "EDIT_TEMPLATE" => ""
                        ),
                false
                );?>
            <div class="clear"></div>
        </div>*/?>
        <?if (substr_count($APPLICATION->GetCurDir(), "restorator")):
            
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "menu",
                    "EDIT_TEMPLATE" => ""
                ),
                false
            );
        endif;
        ?>
        <?if(substr_count($APPLICATION->GetCurDir(),"/users/")>0&&substr_count($APPLICATION->GetCurDir(),"/users/list/")==0):?>
            <div id="block960">            
                    <?if($USER->GetID()==$_REQUEST["USER_ID"]):?>
                        <h1 class="left"><?=$USER->GetFullName()?></h1>
                        <?/*
                        CModule::IncludeModule("sale");
                        $arBud = CSaleUserAccount::GetByUserID($USER->GetID(), "RUB")
                        <div class="restics_bg_small left" style="margin-top:-5px; margin-left: 15px;"><span style="font-size:12px;"><?=sprintf("%01.0f", $arBud["CURRENT_BUDGET"]);?></span></div>
                        <div class="clear"></div>
                        */?>
                        <div class="clear"></div>
                    <?else:?>
                        <h1 class="left">
                            <?
                            $rsUser = CUser::GetByID($_REQUEST["USER_ID"]);
                            $arUser = $rsUser->Fetch();
                            echo $arUser["NAME"]." ".$arUser["LAST_NAME"];
                            ?>
                        </h1>
                    <?endif;?>
                <?if($USER->GetID()!=$_REQUEST["USER_ID"]):?>
                    <div class="right" style="padding-top:3px;">
                        <script>
                            $(document).ready(function(){
                                $("#user_mail_send").click(function(){
                                    var _this = $(this);
                                    $.ajax({
                                        type: "POST",
                                        url: _this.attr("href"),
                                        data: "USER_NAME=<?=$arUser["NAME"]." ".$arUser["LAST_NAME"]?>&USER_ID=<?=intval($_REQUEST["USER_ID"])?>&<?=bitrix_sessid_get()?>",
                                        success: function(data) {
                                            if (!$("#mail_modal").size())
                                            {
                                                $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                                            }
                                            $('#mail_modal').html(data);
                                            showOverflow();
                                            setCenter($("#mail_modal"));
                                            $("#mail_modal").fadeIn("300"); 
                                        }
                                    });
                                    return false;
                                });
                                $("#invite2rest").click(function(){
                                    var _this = $(this);
                                    $.ajax({
                                        type: "POST",
                                        url: _this.attr("href"),
                                        data: "USER_ID=<?=intval($_REQUEST["USER_ID"])?>&<?=bitrix_sessid_get()?>",
                                        success: function(data) {
                                            if (!$("#invite2rest_modal").size())
                                            {
                                                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                                            }
                                            $('#invite2rest_modal').html(data);
                                            showOverflow();
                                            setCenter($("#invite2rest_modal"));
                                            $("#invite2rest_modal").css("top","150px")
                                            $("#invite2rest_modal").fadeIn("300"); 
                                        }
                                    });
                                    return false;
                                });
                            });
                        </script>
                        <?
                        if ($USER->IsAuthorized()){
                        CModule::IncludeModule("socialnetwork");
                        $friend = CSocNetUserRelations::GetRelation($USER->GetID(), $_REQUEST["USER_ID"]);?>
                        <div class="user-mail left" style="margin-top:7px; margin-right:15px"><a href="/tpl/ajax/im.php" id="user_mail_send">Сообщение</a></div>
                        <?if (CSite::InGroup(Array(1,15,16,20))):?>
                            <div class="left" style="margin-top:7px; margin-right:15px"><a href="/tpl/ajax/invite2rest.php" id="invite2rest">Пригласить в ресторан</a></div>
                        <?endif;?>
                        <div class="left" id="add_fr" style="padding-top:3px;">
                            <?if ($friend=="F"):?>
                                    <?$APPLICATION->IncludeComponent(
                                        "restoran:user.friends_delete",
                                        "light_button",
                                        Array(
                                            "FIRST_USER_ID" => $USER->GetID(),
                                            "SECOND_USER_ID" => (int)$_REQUEST["USER_ID"],
                                            "RESULT_CONTAINER_ID" => "add_fr"
                                        ),
                                        false
                                    );?>
                            <?elseif($friend=="Z"):?>
                                <input type="button" class="grey_button" value="Заявка отправлена" />
                            <?else:?>
                                <?$APPLICATION->IncludeComponent(
                                    "restoran:user.friends_add",
                                    "light_button",
                                    Array(
                                        "FIRST_USER_ID" => $USER->GetID(),
                                        "SECOND_USER_ID" => (int)$_REQUEST["USER_ID"],
                                        "RESULT_CONTAINER_ID" => "add_fr"
                                    ),
                                    false
                                );?>
                            <?endif;?>                       
                        <!--<div class="left"><input type="button" class="light_button" value="+ Добавить в друзья" /></div>-->
                        </div>
                        <?}?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                <?endif;?>
                
                        <?global $user_id;
                        $user_id = $_REQUEST["USER_ID"]?>
                        <?if($USER->GetID()==$_REQUEST["USER_ID"]):?>
                            <div style="position:absolute;top:0px; right:0px;">
                            <a href="javascript:void(0)" style="position:relative; text-decoration: none;" id="invite_friend">
                                <input type="button" style="width:218px!important;" class="light_button" value="Пригласи друга на сайт" />
                                <div class="restik">
                                    <div class="restics_bg2"><span>50</span></div>
                                </div>
                            </a>                            
                            <?/*if (in_array(RESTORATOR_GROUP, $USER->GetUserGroupArray())):?>
                                
                                    <script>
                                        $(document).ready(function(){
                                            $("#add_promo").click(function(){
                                                var _this = $(this);
                                                $.ajax({
                                                    type: "POST",
                                                    url: "/tpl/ajax/add_promo.php",
                                                    data: "USER_ID=<?=$USER->GetID()?>&<?=bitrix_sessid_get()?>",
                                                    success: function(data) {
                                                        if (!$("#promo_modal").size())
                                                        {
                                                            $("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
                                                        }
                                                        $('#promo_modal').html(data);
                                                        showOverflow();
                                                        setCenter($("#promo_modal"));
                                                        $("#promo_modal").fadeIn("300"); 
                                                    }
                                                });
                                                return false;
                                            });
                                        });
                                    </script>
                                    <a href="javascript:void(0)"><input id="add_promo" type="button" class="light_button" value="+ Ввести промо-код" /></a>                                
                                <?endif;*/?>
                                </div>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "restorator",
                                Array(
                                    "ROOT_MENU_TYPE" => "restorator",
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000000",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                ),
                            false
                            );?>
                            <div class="menu-horizontal-long">
                                <ul>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "users",
                                    Array(
                                        "ROOT_MENU_TYPE" => "personal",
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_TIME" => "3600000000",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                    ),
                                false
                                );?>
                                </ul>
                            </div>
                        <?else:?>
                            <div class="menu-horizontal-long">
                                <ul style="width:350px">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "users",
                                    Array(
                                        "ROOT_MENU_TYPE" => "user",
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "MENU_CACHE_TYPE" => "Y",
                                        "MENU_CACHE_TIME" => "60",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                    ),
                                false
                                );?>
                                </ul>
                            </div>
                        <?endif;?>                                         
            </div>
        <?endif;?>



        <?if ($APPLICATION->GetCurDir()=="/restopraktik/"||$APPLICATION->GetCurDir()=="/fatcat/"||$APPLICATION->GetCurDir()=="/activities/gastronomic/"):?>
            <div class="restopractic">     
        <?endif;?>
        <?if (substr_count($APPLICATION->GetCurDir(),"/content/personal")==0 && substr_count($APPLICATION->GetCurDir(),"/auth/")==0 && substr_count($APPLICATION->GetCurDir(),"/users/")==0&& substr_count($APPLICATION->GetCurDir(),"/restorator/")==0):?>
            <?if ($APPLICATION->GetCurPage()!="/".CITY_ID."/map/"&&$APPLICATION->GetCurPage()!="/".CITY_ID."/map/near/"):?>
                <div id="filter_bg" <?=($APPLICATION->GetCurDir()=="/restopraktik/")?"style='background:none; border-top:1px solid #eee;'":""?>>
            <?endif;?>
            <div id="filter" <?=($APPLICATION->GetCurPage()=="/".CITY_ID."/map/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/map/near/")?"style='width:980px; padding-top:15px'":""?>>
                <div class="filter">                    
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_suggest_new":"main_search_suggest_new",
                            Array(),
                        false
                    );?>
                    <?if(substr_count($APPLICATION->GetCurDir(),"/firms")==0&&$APPLICATION->GetCurPage()!="/test312.php"):?>
                    <?$arSpecIB = getArIblock("special_projects", CITY_ID);?>
                    <?$arRestIB = getArIblock("catalog", CITY_ID);?>
                    <?
                    if ($_REQUEST["CATALOG_ID"]=="restaurants"):?>
                    <noindex>
                    <?    $APPLICATION->IncludeComponent(
                                "restoran:catalog.filter",
                                "new3",
                                Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => $arRestIB["ID"],
                                        "SPEC_IBLOCK" => $arSpecIB['ID'],
                                        "FILTER_NAME" => "arrFilter",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => (CITY_ID=="urm"||CITY_ID=="rga")?array("kitchen", "average_bill", "area", "out_city"):array("kitchen", "average_bill", "subway", "out_city"),
                                        "PRICE_CODE" => array(),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "N",
                                        "LIST_HEIGHT" => "5",
                                        "TEXT_WIDTH" => "20",
                                        "NUMBER_WIDTH" => "5",
                                        "SAVE_IN_SESSION" => "N"
                                )
                        );?>
                    </noindex>
                    <?elseif ($_REQUEST["CATALOG_ID"]=="banket"):?>
                    <noindex>                        
                        <?$APPLICATION->IncludeComponent(
                                "restoran:catalog.filter",
                                "new3",
                                Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => $arRestIB["ID"],
                                        "SPEC_IBLOCK" => $arSpecIB['ID'],
                                        "FILTER_NAME" => "arrFilter",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => (CITY_ID=="urm"||CITY_ID=="rga")?array("kolichestvochelovek", "area", "average_bill","kitchen"):array("kolichestvochelovek","subway", "area", "average_bill","kitchen"),
                                        "PRICE_CODE" => array(),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "N",
                                        "LIST_HEIGHT" => "5",
                                        "TEXT_WIDTH" => "20",
                                        "NUMBER_WIDTH" => "5",
                                        "SAVE_IN_SESSION" => "N"
                                )
                        );?>
                    </noindex>
                    <?elseif($_REQUEST["CATALOG_ID"]=="dostavka"):?>
                        <noindex>
                        <?$APPLICATION->IncludeComponent(
                                "restoran:catalog.filter",
                                "new3",
                                Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => $arRestIB["ID"],
                                        "SPEC_IBLOCK" => $arSpecIB['ID'],
                                        "FILTER_NAME" => "arrFilter",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => array("preference", "subway_dostavka", "kuhnyadostavki", "average_bill","out_city"),
                                        "PRICE_CODE" => array(),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "N",
                                        "LIST_HEIGHT" => "5",
                                        "TEXT_WIDTH" => "20",
                                        "NUMBER_WIDTH" => "5",
                                        "SAVE_IN_SESSION" => "N"
                                )
                        );?>
                        </noindex>
                    <?
                    elseif ($APPLICATION->GetCurPage()=="/".CITY_ID."/map/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/map/near/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/map/index_1.php"||$APPLICATION->GetCurPage()=="/search/map/index_1.php"):?>
                    <noindex>
                        <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "map_new",
                            Array(
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arRestIB["ID"],
                                    "SPEC_IBLOCK" => $arSpecIB['ID'],
                                    "FILTER_NAME" => "arrFilter",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => array("kitchen", "average_bill", "subway", "out_city", "type"),
                                    "PRICE_CODE" => array(),
                                    "CACHE_TYPE" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_GROUPS" => "N",
                                    "LIST_HEIGHT" => "5",
                                    "TEXT_WIDTH" => "20",
                                    "NUMBER_WIDTH" => "5",
                                    "SAVE_IN_SESSION" => "N"
                            )
                        );?>
                    </noindex>
                     <?
                    elseif (substr_count($APPLICATION->GetCurDir(),"/vacancy/")):
                    ?>
                    <noindex>
                        <?$arRestIB = getArIblock("vacancy", CITY_ID);?>
                        <div id="job_filter">
                            <?$APPLICATION->IncludeComponent(
                                "restoran:catalog.filter",
                                "job",
                                Array(
                                        "IBLOCK_TYPE" => "vacancy",
                                        "IBLOCK_ID" => $arRestIB["ID"],
                                        //"SPEC_IBLOCK" => $arSpecIB['ID'],
                                        "FILTER_NAME" => "arrFilter",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => array("POSITION","EXPERIENCE", "SHEDULE","WAGES_OF"),
                                        "PRICE_CODE" => array(),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "N",
                                        "CACHE_NOTES" => "1",
                                        "LIST_HEIGHT" => "5",
                                        "TEXT_WIDTH" => "20",
                                        "NUMBER_WIDTH" => "5",
                                        "SAVE_IN_SESSION" => "N"
                                )
                            );?>
                        </div>
                    </noindex>
                    <?
                     elseif (substr_count($APPLICATION->GetCurDir(),"/resume/")):
                    ?>
                    <noindex>
                        <?$arRestIB = getArIblock("resume", CITY_ID);?>
                        <div id="job_filter">
                        <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "job",
                            Array(
                                    "IBLOCK_TYPE" => "resume",
                                    "IBLOCK_ID" => $arRestIB["ID"],
                                    //"SPEC_IBLOCK" => $arSpecIB['ID'],
                                    "FILTER_NAME" => "arrFilter",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => array("POSITION","EXPERIENCE", "SHEDULE","EXPERIENCE", "SUGG_WORK_GRAFIK","WAGES_OF","SUGG_WORK_ZP_FROM"),
                                    "PRICE_CODE" => array(),
                                    "CACHE_TYPE" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_GROUPS" => "N",
                                    "CACHE_NOTES" => "2",
                                    "LIST_HEIGHT" => "5",
                                    "TEXT_WIDTH" => "20",
                                    "NUMBER_WIDTH" => "5",
                                    "SAVE_IN_SESSION" => "N"
                            )
                        );?>
                        </div>
                    </noindex>
                    <?
                    elseif (substr_count($APPLICATION->GetCurDir(),"/joblist/")):
                    ?>
                    <noindex>
                        <?$arRestIB = getArIblock("vacancy", CITY_ID);?>
                        <div id="job_filter">
                            <?$APPLICATION->IncludeComponent(
                                "restoran:catalog.filter",
                                "job",
                                Array(
                                        "IBLOCK_TYPE" => "vacancy",
                                        "IBLOCK_ID" => $arRestIB["ID"],
                                        //"SPEC_IBLOCK" => $arSpecIB['ID'],
                                        "FILTER_NAME" => "arrFilter",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => array("POSITION","EXPERIENCE", "SHEDULE","WAGES_OF"),
                                        "PRICE_CODE" => array(),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_NOTES" => "1",
                                        "CACHE_GROUPS" => "N",
                                        "LIST_HEIGHT" => "5",
                                        "TEXT_WIDTH" => "20",
                                        "NUMBER_WIDTH" => "5",
                                        "SAVE_IN_SESSION" => "N"
                                )
                            );?>
                        </div>
                    </noindex>                    
                    <?/*elseif($_REQUEST["SECTION_CODE"]=="mcfromchif"):?>
                        <?$APPLICATION->IncludeComponent(
                                    "restoran:catalog.filter",
                                    "cook_new2",
                                    Array(
                                            "IBLOCK_TYPE" => "catalog",
                                            "IBLOCK_ID" => 139,
                                            "SPEC_IBLOCK" => 72,
                                            "FILTER_NAME" => "arrFilter",
                                            "FIELD_CODE" => array(),
                                            "PROPERTY_CODE" => array("cat", "osn_ingr", "prig_time", "cook", "povod", "prig"),
                                            "PRICE_CODE" => array(),
                                            "CACHE_TYPE" => "Y",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_GROUPS" => "N",
                                            "LIST_HEIGHT" => "5",
                                            "TEXT_WIDTH" => "20",
                                            "NUMBER_WIDTH" => "5",
                                            "SAVE_IN_SESSION" => "N"
                                    )
                        );?> 
                    <?*/else:?>
                    <noindex>
                    <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "new3",
                            Array(
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arRestIB["ID"],
                                    "SPEC_IBLOCK" => $arSpecIB['ID'],
                                    "FILTER_NAME" => "arrFilter",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => (CITY_ID=="urm"||CITY_ID=="rga")?array("kitchen", "average_bill", "area", "out_city"):array("kitchen", "average_bill", "subway", "out_city"),
                                    "PRICE_CODE" => array(),
                                    "CACHE_TYPE" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_GROUPS" => "N",
                                    "LIST_HEIGHT" => "5",
                                    "TEXT_WIDTH" => "20",
                                    "NUMBER_WIDTH" => "5",
                                    "SAVE_IN_SESSION" => "N"
                            )
                    );?>
                    <?endif;?>
                    <?if ($_REQUEST["CATALOG_ID"]):?>
                        <div id="alph_filtr">
                            <?$APPLICATION->IncludeComponent(
                                "restoran:alphabet.filter",
                                "rest",
                                Array(),
                            false
                            );?>    
                        </div>
                    <?endif;?>
                    </noindex>
                <?endif;?>
                    <?/*$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_suggest":"main_search_suggest",
                            Array(),
                        false
                    );*/?>
                <?if(substr_count($APPLICATION->GetCurDir(),"/firms")>0):?>
                    <div id="new_filter">
                        <?$APPLICATION->IncludeComponent(
                            "restoran:alphabet.filter",
                            "",
                            Array(),
                        false
                        );?>    
                    </div>       
                    <?if (!substr_count($APPLICATION->GetCurDir(),"/katering")):?>
                    <div class="top_baner" style="margin:0px; position: relative">
                            <div class="top_baner_block" style="padding:10px;">
                                    <div id="subscribe">
                                        <?$arFirmsIB = getArIblock("firms", CITY_ID);?>
                                            <?$APPLICATION->IncludeComponent(
                                                    "bitrix:catalog.section.list",
                                                    "firms",
                                                    Array(
                                                            "IBLOCK_TYPE" => "firms",
                                                            "IBLOCK_ID" => $arFirmsIB["ID"],
                                                            "SECTION_ID" => "",
                                                            "SECTION_CODE" => "",
                                                            "SECTION_URL" => "",
                                                            "COUNT_ELEMENTS" => "Y",
                                                            "TOP_DEPTH" => "1",
                                                            "SECTION_FIELDS" => array(),
                                                            "SECTION_USER_FIELDS" => array(),
                                                            "ADD_SECTIONS_CHAIN" => "Y",
                                                            "CACHE_TYPE" => "A",
                                                            "CACHE_TIME" => "36000000",
                                                            "CACHE_GROUPS" => "N"
                                                    )
                                            );?>
                                    </div>
                            </div>
                    </div>
                    <?endif;?>
                <?endif;?>
                    <?/*if(substr_count($APPLICATION->GetCurDir(),"/vacancy")>0):?>                                    
                    <div class="top_baner" style="margin:0px; position: relative">
                            <div class="top_baner_block" style="padding:10px;">
                                    <div id="subscribe">
                                        <?$arFirmsIB = getArIblock("vacancy", CITY_ID);?>
                                            <?$APPLICATION->IncludeComponent(
                                                    "bitrix:catalog.section.list",
                                                    "firms",
                                                    Array(
                                                            "IBLOCK_TYPE" => "vacancy",
                                                            "IBLOCK_ID" => $arFirmsIB["ID"],
                                                            "SECTION_ID" => "",
                                                            "SECTION_CODE" => "",
                                                            "SECTION_URL" => "",
                                                            "COUNT_ELEMENTS" => "Y",
                                                            "TOP_DEPTH" => "1",
                                                            "SECTION_FIELDS" => array(),
                                                            "SECTION_USER_FIELDS" => array(),
                                                            "ADD_SECTIONS_CHAIN" => "Y",
                                                            "CACHE_TYPE" => "A",
                                                            "CACHE_TIME" => "36000000",
                                                            "CACHE_GROUPS" => "N"
                                                    )
                                            );?>
                                    </div>
                            </div>
                    </div>
                    <?endif;?>
                    <?if(substr_count($APPLICATION->GetCurDir(),"/resume")>0):?>                                    
                    <div class="top_baner" style="margin:0px; position: relative">
                            <div class="top_baner_block" style="padding:10px;">
                                    <div id="subscribe">
                                        <?$arFirmsIB = getArIblock("resume", CITY_ID);?>
                                            <?$APPLICATION->IncludeComponent(
                                                    "bitrix:catalog.section.list",
                                                    "firms",
                                                    Array(
                                                            "IBLOCK_TYPE" => "vacancy",
                                                            "IBLOCK_ID" => $arFirmsIB["ID"],
                                                            "SECTION_ID" => "",
                                                            "SECTION_CODE" => "",
                                                            "SECTION_URL" => "",
                                                            "COUNT_ELEMENTS" => "Y",
                                                            "TOP_DEPTH" => "1",
                                                            "SECTION_FIELDS" => array(),
                                                            "SECTION_USER_FIELDS" => array(),
                                                            "ADD_SECTIONS_CHAIN" => "Y",
                                                            "CACHE_TYPE" => "A",
                                                            "CACHE_TIME" => "36000000",
                                                            "CACHE_GROUPS" => "N"
                                                    )
                                            );?>
                                    </div>
                            </div>
                    </div>
                    <?endif;*/?>
                    <div class="clear"></div>
                </div>
            </div>
        <?if ($APPLICATION->GetCurPage()!="/".CITY_ID."/map/"&&$APPLICATION->GetCurPage()!="/".CITY_ID."/map/near/"):?>
            </div>
        <?endif;?>
            <?/*$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "page",
                    "AREA_FILE_SUFFIX" => "inc_top",
                    "EDIT_TEMPLATE" => ""
                ),
            false
            );*/?>
        <?endif;?>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "top_content_main_page",
                            "NOINDEX" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
        <?/*if (!substr_count($APPLICATION->GetCurDir(),"/".CITY_ID."/map")&&!substr_count($APPLICATION->GetCurDir(),"/".CITY_ID."/opinions")&&!substr_count($APPLICATION->GetCurDir(),"/users/")&&!substr_count($APPLICATION->GetCurDir(),"firms")):?>
        <a href="javascript:void(0)" class="ajax font18" onclick="show_review_form()">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "top_content_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
        </a><br />
        <?endif;*/?>
        <?/*$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "top_main_980_50",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );*/
        ?>                        
<?if($APPLICATION->get_cookie("CONTEXT")=="Y") $_REQUEST["CONTEXT"]="Y";?>