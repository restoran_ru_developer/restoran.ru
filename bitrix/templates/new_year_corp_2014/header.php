<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <? $APPLICATION->ShowHead() ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/detail_galery.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/script.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/maskedinput.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/tools.js') ?>

        <? $APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/cusel.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/styles.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js') ?>            
        <? $APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js') ?>             
        <? $APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js') ?>  
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.system_message.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/cusel-min.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/radio.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js') ?>
        <!--Bubles-->
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bubles/jquery.easing.1.3.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/bubles/jquery-BubbleEngine.js') ?>
        <? $APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/js/bubles/BubbleEngineDemo.css";  type="text/css" rel="stylesheet" />', true) ?>
        <!--End Bubles-->
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
            <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
                <?
                CModule::IncludeModule("advertising");
                CAdvBanner::SetRequiredKeywords(array(CITY_ID));
                ?>
                </head>
                <body style="background:#FFF">
                    <div id="panel"><? $APPLICATION->ShowPanel() ?></div>
                    <div id="container" style="background: #032d50 url(/tpl/images/spec/new_year_corp_2014/new_year_corp2014.jpg) center top no-repeat">
                        <div id="wrapp">    
                            <div class="baner" style="height:90px; background: none; border-radius: 15px; width:980px; margin:0 auto; padding-top:20px;">                
                <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "new_year_top_980_90",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
            </div>
                            <div id="content" style="padding-top: 226px;">         
                                <a id="new_year_night_logo" href="/<?=CITY_ID?>/articles/new_year_corp/"></a>
                                <?if (CITY_ID!="tmn"):?>
                                <a id="new_year_back" href="/<?=CITY_ID?>/articles/new_year/"></a>
                                <?endif;?>
                                <a id="new_year_logo" href="/"></a>