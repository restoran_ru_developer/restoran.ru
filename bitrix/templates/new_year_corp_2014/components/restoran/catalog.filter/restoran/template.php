<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript("/tpl/js/jquery.checkbox.js")?>
<noindex>
<form action="/<?=CITY_ID?>/articles/new_year_corp/" method="get" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search">
            <div class="filter_box">
<!--                <div class="filter_block">
                    Найти по параметрам:
                </div>-->
            <?
            $p=0;
            $ar_cusel = Array(2,3,1,6);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
                if ($arProp["PROPERTY_TYPE"]!="L"):?>
                    <?if ($arProp["CODE"]!="subway"&&$arProp["CODE"]!="out_city"):?>
                        <div class="filter_block appetite" id="filter<?=$prop_id?>">
                            <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                            <div class="title appetite" style="font-size:14px;text-align: left"><a class="another" href="javascript:void(0)"><?=$arProp["NAME"]?></a></div>                                      
                            <script>
                                $(document).ready(function(){
                                    $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"right":"10px","top":"-16px", "width":"355px"}});
                                    var params = {
                                        changedEl: "#multi<?=$ar_cusel[$p]?>",
                                        scrollArrows: true,
                                        visRows:30
                                    }
                                    cuSelMulti(params);
                                })
                            </script>
                            <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                                <div class="title appetite another" style="font-size:14px;" align="right"><?=$arProp["NAME"]?></div>
                                <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="20">
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                                    <?endforeach?>
                                </select>
                                <br />
                                <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                                <br />
                            </div>
                        </div>        
                 <?elseif($arProp["CODE"]=="subway"):?>
                    <div class="filter_block" id="filter3" style="margin-top:5px;position: relative; height:30px;">                    
                        <div class="left">
                            <div class="title appetite" style="font-size:14px;text-align: left;line-height: 24px;">
                                <a href="/bitrix/components/restoran/metro.list/ajax.php?q=shema&template=shema8marta&<?= bitrix_sessid_get() ?>" onclick="load_metro(this); return false;" class="another"><?=$arProp["NAME"]?></a>
                            </div>              
                            <script>
                                var link = "";                            
                                function load_metro(obj)
                                {
                                    if (link != $(obj).attr('href'))
                                    {
                                        link = $(obj).attr('href');
                                        $(".filter_popup_4").css({'padding':'25px','padding-right':'0px','right':'10px','top':'-5px', 'width':'815px','position':'absolute','z-index':10000,'background': 'url(/tpl/images/popup_bg.png)'});
                                        $(".filter_popup_4").load(link, function(){$(".filter_popup_4").fadeIn(300);});                                          
                                    }
                                    else
                                    {
                                        $(".filter_popup_4").fadeIn(300);
                                    }
                                }
                                $(document).ready(function(){
                                    $(".filter_popup_4").click(function(e){                                
                                        e.stopPropagation();
                                        //return false;
                                    });
                                    $("#filter3").find("a.another").click(function(e){
                                        if (!$(this).hasClass("pri"))
                                        {
                                        e.stopPropagation();
                                        return false;
                                        }
                                    });
                                    $('html,body').click(function() {
                                        $(".filter_popup_4").fadeOut(300);
                                    });
                                    $(window).keyup(function(event) {
                                    if (event.keyCode == '27') {
                                        $(".filter_popup_4").fadeOut(300);
                                    }
                                    });
                                });
                            </script>
<!--                                <ul class="filter_category_block" id="fil321">
                                    <li><a href="/tpl/ajax/metro.php?q=abc&<?=bitrix_sessid_get()?>" class="filter_arrow" onclick="load_metro(this); return false;"><?=GetMessage("METRO_ABC")?></a></li>
                                    <li><a href="/tpl/ajax/metro.php?q=thread&<?=bitrix_sessid_get()?>" class="filter_arrow" onclick="load_metro(this); return false;"><?=GetMessage("METRO_VETKA")?></a></li>
                                    <li><a href="/bitrix/components/restoran/metro.list/ajax.php?q=shema&template=shema8marta&<?= bitrix_sessid_get() ?>" class="filter_arrow" onclick="load_metro(this); return false;"><?=GetMessage("SHEMA")?></a></li>                                    
                                </ul>                            -->
                            <!--<div class="filter_popup filter_popup_3 popup" style="display:none"></div>                    -->
                            <div class="filter_popup filter_popup_4" style="display:none"></div>
                            <?/*foreach($arResult["SUBWAY"] as $subway):?>
                                <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                            <?endforeach*/?>
                        </div>
                        <?if ($arResult["OUT_CITY"]):?>
                            <div class="right" id="filter<?=$arResult["OUT_CITY"]["ID"]?>" style="position:relative">
                                <div class="title appetite" style="margin-top:0px; line-height:24px; text-align: left; font-size:14px;"><a href="javascript:void(0)" class="filter_arrow another pri" style="border-bottom:1px dotted #FFF; text-decoration:none"><?=GetMessage("MORE_".$arResult["OUT_CITY"]["CODE"])?></a></div>
                                <script>
                                    $(document).ready(function(){
                                        $(".filter_popup_<?=$arResult["OUT_CITY"]["ID"]?>").popup({"obj":"filter<?=$arResult["OUT_CITY"]["ID"]?>","css":{"right":"-14px","top":"-11px", "width":"355px"}});
                                        var params = {
                                            changedEl: "#multi1",
                                            scrollArrows: true,
                                            visRows:40
                                        }
                                        cuSelMulti(params);
                                    })
                                </script>
                                <div class="filter_popup filter_popup_<?=$arResult["OUT_CITY"]["ID"]?>" style="display:none">
                                    <div class="title appetite another" style="font-size:14px;" align="right"><?=GetMessage("MORE_".$arResult["OUT_CITY"]["CODE"])?></div>
                                    <select multiple="multiple" class="asd" id="multi1" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arResult["OUT_CITY"]["CODE"]?>][]" size="40">
                                        <?foreach($arResult["OUT_CITY"]["VALUE_LIST"] as $key=>$val):?>
                                            <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arResult["OUT_CITY"]["CODE"]]))?"selected":""?>><?=$val?></option>
                                        <?endforeach?>
                                    </select>
                                    <br />
                                    <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                                    <Br />
                                </div>
                            </div>
                        <?endif;?>
                        <?$p++;?>                        
                    </div>                
                   <?endif;?>
                <?$p++;
                else:?>
                    <div class="filter_block appetite">
                        <table cellpadding="0" cellspacing="0">                                                                        
                            <tr class="select_all">
                                <td style="padding-left: 20px;padding-right:10px"><span class="niceCheck" style="background-position: 0px 0px; "><input name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>]" type="checkbox" value="Да" <?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]=="Да")?"checked":""?> name="" id=""></span></td>
                                <td class="option2"><label class="niceCheckLabel"><?=$arProp["NAME"]?></label></td>                                       
                            </tr>
                        </table>
                    </div>
                <?endif;          
            }
            ?>      
            <div class="filter_block">
                <input style="width:190px; height: 45px;" class="light_button appetite button_2014" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
            </div>
        </div>
    </div>
    <div class="clear"></div>
</form>
</noindex>