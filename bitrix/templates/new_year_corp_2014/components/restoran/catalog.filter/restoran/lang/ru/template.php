<?
$MESS ['IBLOCK_FILTER_TITLE'] = "Фильтр";
$MESS ['IBLOCK_SET_FILTER'] = "Найти ресторан";
$MESS ['IBLOCK_DEL_FILTER'] = "Сбросить";
$MESS ["SELECT_ALL"] = "Выбрать все";
$MESS ["UNSELECT_ALL"] = "Убрать отметки";
$MESS ["CHOOSE_BUTTON"] = "OK";
$MESS ["EXTENDED_FILTER"] = "Расширенный фильтр";
$MESS ["MAP_SEARCH"] = "Поиск по карте";
$MESS ["NEAR_YOU"] = "Ближайшие к Вам";
$MESS ["SHEMA"] = "Схема";
$MESS ["METRO_ABC"] = "По алфавиту";
$MESS ["METRO_VETKA"] = "По веткам";
$MESS ["MORE_kolichestvochelovek"] = "больше";
$MESS ["MORE_average_bill"] = "больше";
$MESS ["MORE_kitchen"] = "еще кухни";
$MESS ["MORE_preference"] = "еще предпочтения";
$MESS ["MORE_kuhnyadostavki"] = "еще кухни";
$MESS ["MORE_type"] = "еще типы";
$MESS ["MORE_out_city"] = "Загородные";
$MESS ["MORE_features"] = "все особенности";
?>