<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$excUrlParams = array("clear_cache","page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1", "pageRestCnt", "PAGEN_2", "SECTION_CODE","index_php?page");
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");
?>

<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
if($arResult["bDescPageNumbering"] === false) {
    //v_dump($_REQUEST);
    // to show always first and last pages
    $arResult["nStartPage"] = 1;
    $arResult["nEndPage"] = $arResult["NavPageCount"];

    $sPrevHref = '';
    if ($arResult["NavPageNomer"] > 1)
    {
        $bPrevDisabled = false;

        if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
        {
            $sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
        }
        else
        {
            $sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
        }
    }
    else
    {
        $bPrevDisabled = true;
    }

    $sNextHref = '';
    if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
    {
        $bNextDisabled = false;
        $sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
    }
    else
    {
        $bNextDisabled = true;
    }
    ?>

    <?/*if(!$_REQUEST["PAGEN_".$arResult["NavNum"]] || strlen($_REQUEST["PAGEN_".$arResult["NavNum"]]) <= 0):?>
        <span class="num">&nbsp;<?=GetMessage("TOP_SPEC")?>&nbsp;</span>
    <?else:?>
        <a class="another nav" href="<?=$arResult["sUrlPath"]?>"><?=GetMessage("TOP_SPEC")?></a>
    <?endif*/?>
<?if(!$_REQUEST["PAGEN_1"] || strlen($_REQUEST["PAGEN_1"]) <= 0):?>
            <span class="num">&nbsp;Best&nbsp;</span>
        <?else:?>
            <a class="another nav" href="<?=$arResult["sUrlPath"]?>">Best</a>
        <?endif?>
    <?
     
    $bFirst = true;
    $bPoints = false;
    do
    {
        if ($arResult["nStartPage"] <= 5 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
        {

            if (($arResult["nStartPage"] == $arResult["NavPageNomer"])&&$_REQUEST["PAGEN_1"]):?>
                <span class="num">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
            <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false && strlen($_REQUEST["PAGEN_".$arResult["NavNum"]]) > 0):?>
               <a class="another nav" href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
            <?else:?>
               <a class="another nav" href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
            <?endif;
            $bFirst = false;
            $bPoints = true;
        }
        else
        {
            if ($bPoints)
            {
                ?>...<?
                $bPoints = false;
            }
        }
        $arResult["nStartPage"]++;
    } while($arResult["nStartPage"] <= $arResult["nEndPage"]);
}
?>