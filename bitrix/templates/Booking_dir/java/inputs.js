$(document).ready(function() {
	/*открываем/скрываем выпадающий список*/
	$("div.select .selected_element").live("click", function(){
		var all_variants=$(this).next();
		if($(all_variants).is(":hidden")){
			$(".all_variants").slideUp(100);
			$(all_variants).slideDown(100);
			
			var cl=$(this).parent("div").attr("class");
			var new_class=cl.replace("_off","_on");
			$(this).parent("div").attr("class",new_class);
			
		}else{
			$(all_variants).slideUp(100);
			
			var cl=$(this).parent("div").attr("class");
			var new_class=cl.replace("_on","_off");
			$(this).parent("div").attr("class",new_class);
		}
	});
	
	/*скрываем выпадающий список*/
	$("div.all_variants a").live("click", function(){
		var name=$(this).parent("div").parent("div").attr("id");
		var txt=$(this).html();
		var val=$(this).attr("href");
		var tmp=$(this);
		
		var cl = $(this).attr("class");
		
		$("input[name="+name+"]").val(val);
		$(this).parent("div").parent("div").find(".selected_element").attr("class","selected_element "+cl).html(txt);
		
		$(".all_variants").slideUp(100);
		
		var cl=$(this).parent("div").parent("div").attr("class");
		var new_class=cl.replace("_on","_off");
		$(this).parent("div").parent("div").attr("class",new_class);
		
		
		
		if(name=="city"){
			 location.replace('/bs/dir/?new_city='+val); 
		}
		
		if(name=="type" || name=="source" || name=="source" || name=="status" ){
			$("#order_changed").val("Y");
		}

		if(name=="type" && val=="1552"){
			$("#format_m").show();
		}else{
			$("#format_m").hide();
		}
		
		return false;
	});
	
	$("body").click(function(){
		$(".all_variants").slideUp(100);
		$(".select").removeClass("select_on");
		$(".select").addClass("select_off");
		$("#street_search .variants").slideUp(100);
	});

	$(".radio a").click(function(){
		var name = $(this).attr("name");
		var val = $(this).attr("href") 
		$(".radio a[name="+name+"]").removeClass("radio_on");
		$(this).addClass("radio_on");
		$("input[name="+name+"]").val(val);
		return false;
	});

});
