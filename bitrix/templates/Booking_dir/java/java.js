var core = "/bitrix/templates/Booking_dir/core.php";
var ltop = 0; 
var show_ajax_runer=1;
var ajax_runer =0;
var auto_refresh = true;




$(document).ready(function() {	

	$("#city_pos").mouseenter(function(){
			$(this).animate({opacity:1}, 200);
		}).mouseleave(function(){
			$(this).animate({opacity:0.2}, 200);
		});
		
	//Возвращает высоту прокрутки
	function getBodyScrollTop(){  
    	return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);  
	}

	////Аякс лоадер
	$("#ajax_loader").ajaxStart(function(){
		ajax_runer=1;
		if(show_ajax_runer==1){
   			$(this).show();
   		}
	}).ajaxStop(function(){
   		$(this).hide();
   		ajax_runer=0;
	});
	
	$("#conteiner").mousemove(function(e){
		$("#ajax_loader").css("left",e.pageX+15);
		$("#ajax_loader").css("top",e.pageY+0);
	});
	
	
	
	//Показываем кнопки управления
	$("#dolg_list_form table tr").live("mouseenter", function(){
		var popup = $(this).find(".popuper");
		popup.show();
		popup.height($(this).height());
		popup.find(".m").height(popup.height());
	}).live("mouseleave", function(){
		var popup = $(this).find(".popuper");
		
		popup.hide();
	});
	
	
	//Сортировка таблицы с должниками
	$("#dolg_list_form table tr th a").live("click", function(){
		var to_send = {};
		to_send["act"] = "sort_table";
		to_send["by"] = $(this).attr("href");
		if($(this).hasClass("up")) to_send["order"]="down";
		else to_send["order"]="up";
		
		$.post(core, to_send, function(html){
			$("#dolg_list").replaceWith(html);					
		});
		return false;
	});
	
	//Постраничка
	$("#dolg_list .pager a").live("click", function(){
		var to_send = {};
		to_send["act"] = "pager_table";
		to_send["PAGEN_1"] = $(this).html();
		$.post(core, to_send, function(html){
			$("#dolg_list").replaceWith(html);					
		});
		return false;
	});
	
	
	
	/***********ПРОСТАВЛЯЕМ ГАЛОЧКИ*************/
	$("#dolg_list_form table input[name=check_all]").live("click", function(event){
		event.stopPropagation();
		if($(this).is(":checked")){
			$("#dolg_list_form table tr td input[type=checkbox]").attr("checked","checked");
		}else{
			$("#dolg_list_form table tr td input[type=checkbox]").removeAttr("checked");
		}
		
	});
	
	
	//Удаляем должника
	$("#dolg_list .popuper a.del").live("click",function(){
		var line = $(this).parent("div").parent("div").parent("td").parent("tr");
		var id = $(this).attr("href");
		line.remove();
		var to_send = {};
		to_send["act"] = "del_dolg";
		to_send["id"] = id;
		$.post(core, to_send, function(html){
							
		});
		return false;
	});
	
	
	//Удаляем оплату
	$("#dolg_list_form .popuper .del_pay").click(function(){
		var line = $(this).parent("div").parent("div").parent("td").parent("tr");
		var id = $(this).attr("href");
		line.remove();
		var to_send = {};
		to_send["act"] = "del_pay";
		to_send["id"] = id;
		$.post(core, to_send, function(html){
							
		});
		return false;
	});
	
	
	
	$("#dolg_list .popuper a.edite").live("click", function(){
		var id = $(this).attr("href");
		var to_send = {};
		to_send["act"] = "dolg_edit_form";
		to_send["id"] = id;
		$.post(core, to_send, function(html){
			$("#dialog:ui-dialog").dialog( "destroy" );
			
			$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
				
					modal: true,
					title: 'Редактирование записи',
					buttons: {
						"Сохранить": function() {
							//$( this ).dialog( "close" );
							var to_send = {};
							
							to_send["act"] = "edite_dolg";
							
							$("#dolg_edite_form input[type=text]").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							$("#dolg_edite_form input[type=hidden]").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
							$("#dolg_edite_form textarea").each(function(){
								to_send[$(this).attr("name")] = $(this).val();
							});
							
								
							$.post(core, to_send, function(html){		
								$("#dialog-confirm").dialog( "close" );
							});
						
						}
					}
				}).html(html).css("height", "auto");
				$(".ui-dialog").css("top", ($(window).height()-$("#dialog-confirm").height())/2+getBodyScrollTop());
				$("#dialog-confirm").css("padding-left", 0);							
		});
		return false;
	});
		

	$("#dolg_list_form .popuper .edite_pay").live("click", function(){
		var id = $(this).attr("href");
		var to_send = {};
		to_send["act"] = "get_pay_info";
		to_send["id"] = id;
		
		$.post(core, to_send, function(html){
			var payment = eval('(' + html + ')');
			
			$("#add_pay input[name=pay_id]").val(payment.id);
			$("#add_pay input[name=date]").val(payment.date);
			$("#add_pay input[name=summa]").val(payment.summa);
			$("#add_pay input[name=comment]").val(payment.comment);
			$("#add_pay input[name=s]").val("Сохранить");
		});
		
		return false;
	});

	$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
	$( "#pay_date" ).datepicker( $.datepicker.regional[ "ru" ] );
	
	$( "#date_s" ).datepicker( $.datepicker.regional[ "ru" ] );
	$( "#date_po" ).datepicker( $.datepicker.regional[ "ru" ] );
		
	
	//ПЕЧАТАЕМ ОТМЕЧЕННЫЕ ЗАКАЗЫ
	$(".print_dolg, .xls_dolg").live("click", function(){
		var lnk = $(this).attr("href");
		
		$("#dolg_list_form").attr("action", lnk);
		
		var co=$("#dolg_list_form table tr td input[type=checkbox]:checked").length;
				
		if(co==0){
			$("#dialog-confirm").dialog({
				resizable: false,
				draggable: false,
				height:140,
				modal: true,
				title: 'Печать должников',
				buttons: {
					"Ok": function() {
						$( this ).dialog( "close" );
					}
				}
			}).html('<span class="alert_icon"></span>Выберите рестораны для печати');
			
		}else $("#dolg_list_form").submit();
		
		return false;
	});		
	
	
	
	//Добавляем или сохраняем оплату
	$("#add_pay").submit(function(){
		var to_send = {};
		
		$("#add_pay input[type=text]").each(function(){
			to_send[$(this).attr("name")] = $(this).val();
		});
							
		$("#add_pay input[type=hidden]").each(function(){
			to_send[$(this).attr("name")] = $(this).val();
		});
							
		$("#add_pay textarea").each(function(){
			to_send[$(this).attr("name")] = $(this).val();
		});
		
		if(to_send["pay_id"]!="") to_send["act"]="save_pay";
		else to_send["act"]="add_pay";
		
		$.post(core, to_send, function(html){
			if(to_send["act"]=="add_pay"){
				window.location.reload(); 
			}else{
				$("#dialog-confirm").dialog({
					resizable: false,
					draggable: false,
					height:140,
					modal: true,
					title: 'Оплата',
					buttons: {
						"Ok": function() {
							$( this ).dialog( "close" );
						}
					}
				}).html('<span class="alert_icon"></span>Изменения созранены');
			}
		
		});
		return false;
	});
	
	//Фильтруем выплаты
	$("#pay_fltr").submit(function(){
		var to_send = {};
		to_send["act"]="pay_fltr";
		
		$("#pay_fltr input[type=text]").each(function(){
			to_send[$(this).attr("name")] = $(this).val();
		});

		$("#pay_fltr input[type=hidden]").each(function(){
			to_send[$(this).attr("name")] = $(this).val();
		});

		$.post(core, to_send, function(html){
			$("#dolg_list").replaceWith(html);	
		});
		
		return false;
	});
	
		
});
