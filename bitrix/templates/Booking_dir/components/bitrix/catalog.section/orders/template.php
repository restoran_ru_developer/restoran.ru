<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="dolg_list">
<?if(count($arResult["ITEMS"])>0){?>	

<form id="dolg_list_form" method="post" name="orders_otch" target="_blank">
	<table cellpadding="0" cellspacing="0" class="orders_list">
		<tr class="heder">
			<th class="c13">создан</th>
			<th class="c23">дата</th>
			<th class="c33">сумма заказа</th>
			<th class="c43">проценты</th>

		</tr>
		
<?
//var_dump($arResult["ITEMS"][0]["PROPERTIES"]);
?>		
		
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
<?
//var_dump($arElement);
?>
	<tr id="dolg_<?=$arElement["ID"]?>" title="">
		<td class="c13"><?=CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($arElement["DATE_CREATE"], CSite::GetDateFormat()))?></td>
		<td class="c23"><?=CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($arElement["ACTIVE_FROM"], CSite::GetDateFormat()))?> (<?=$arElement["DISPLAY_PROPERTIES"]["status"]["DISPLAY_VALUE"]?>)</td>
		<td class="c33"><?=$arElement["PROPERTIES"]["summ_sch"]["VALUE"]?> руб.</td>
		<td class="c43"><?=$arElement["PROPERTIES"]["procent"]["VALUE"]?> руб. 
			<?
			if(count($arResult["LOGS"][$arElement["ID"]])>0){
				$TTL="";
				foreach($arResult["LOGS"][$arElement["ID"]] as $T){
					$TTL.=CIBlockFormatProperties::DateFormat("d.m.Y H:i", MakeTimeStamp($T["DATE_CREATE"], CSite::GetDateFormat()))."\n".$T["PREVIEW_TEXT"]." (пользователь ".$arResult["USERS"][$T["CREATED_BY"]].")\n";
				}
				?>
				<a href="#" class="changed" title="<?=$TTL?>"></a>
				<?
			}
			?>
		</td>
		
	</tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
	</table>
</form>


<?}?>
</div>