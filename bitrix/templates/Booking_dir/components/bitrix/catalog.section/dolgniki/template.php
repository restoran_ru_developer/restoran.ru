<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if(count($arResult["ITEMS"])>0){?>	
<?
	//var_dump($arParams["USER"]);
	
$ADM_ARRAY=array(1,19215);	
	
//запрашиваем выплаты по всем долгам
foreach($arResult["ITEMS"] as $arElement) $Dolgsids[]=$arElement["ID"];

$arFilter = Array("IBLOCK_ID"=>2542, "ACTIVE"=>"Y", "PROPERTY_dolg"=>$Dolgsids);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
$PROPERTY_CODE_LIST = array('dolg','summa');
while($ob = $res->GetNextElement()){
//	$arFields = $ob->GetFields();
//	$arProps = $ob->GetProperties();
    $arProps = array();
    foreach ($PROPERTY_CODE_LIST as $prop_name) {
        $arProps[$prop_name] = $ob->GetProperty($prop_name);
    }
  
	$VIPLATI[$arProps["dolg"]["VALUE"]]+=$arProps["summa"]["VALUE"];
}

foreach($arResult["ITEMS"] as $cell=>$arElement){
	$DOLG = 0;
	$DOLG = $arElement["PROPERTIES"]["dolg"]["VALUE"]-$VIPLATI[$arElement["ID"]];
	$arResult["ITEMS"][$cell]["dolg"]=$DOLG;
}

function build_sorter($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp($a[$key], $b[$key]);
    };
}

//запросим все заказы для этого ресторана со статусом "отчет"
//$RDOLG=0;
$RDOLG = array();
$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_status"=>1808);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("PROPERTY_rest","PROPERTY_procent"));
while($arFields = $res->Fetch()){
	//var_dump($arFields);
		  
	$RDOLG[$arFields["PROPERTY_REST_VALUE"]]+=$arFields["PROPERTY_PROCENT_VALUE"];
}


if($arParams["ELEMENT_SORT_FIELD"]=="itog") usort($arResult["ITEMS"], build_sorter('dolg'));

if($arParams["ELEMENT_SORT_FIELD"]=="itog" && $arParams["ELEMENT_SORT_ORDER"]=="DESC") $arResult["ITEMS"] = array_reverse($arResult["ITEMS"]);




//var_dump($RDOLG);

?>

<div id="dolg_list">

<form id="dolg_list_form" method="post" name="orders_otch" target="_blank">
	<table cellpadding="0" cellspacing="0">
		<tr class="heder">
<!--			--><?//if($USER->IsAdmin()):?>
                <th class="c1 <? if($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_rest.NAME") echo 'active';?>"><a href="PROPERTY_rest.NAME" class="<? if($arParams["ELEMENT_SORT_ORDER"]=="ASC,nulls") echo 'up'; else echo 'down'?>">название<span></span></a></th>
<!--            --><?//else:?>
<!--			    <th class="c1 --><?// if($arParams["ELEMENT_SORT_FIELD"]=="NAME") echo 'active';?><!--"><a href="NAME" class="--><?// if($arParams["ELEMENT_SORT_ORDER"]=="ASC") echo 'up'; else echo 'down'?><!--">название<span></span></a></th>-->
<!--            --><?//endif?>
			<th class="c2 <? if($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_dolg") echo 'active';?>"><a href="PROPERTY_dolg" class="<? if($arParams["ELEMENT_SORT_ORDER"]=="ASC") echo 'up'; else echo 'down'?>">общая сумма<span></span></a></th>
			<th class="c3 <? if($arParams["ELEMENT_SORT_FIELD"]=="itog") echo 'active';?>"><a href="itog" class="<? if($arParams["ELEMENT_SORT_ORDER"]=="ASC") echo 'up'; else echo 'down'?>">сумма долга<span></span></a></th>
			<th class="c3"></th>
			<th class="c4">комментарии</th>
			<th class="c5"><input type="checkbox" name="check_all" value="Y"/></th>
		</tr>
		<?
		
		?>
<?
//FirePHP::getInstance()->info($arResult["ITEMS"][0]);
foreach($arResult["ITEMS"] as $cell=>$arElement):

    ?>
	<tr id="dolg_<?=$arElement["ID"]?>">
		<td class="c1">
            <div class="popuper">
            <div class="m">
                <a href="/bs/dir/<?=$arElement["ID"]?>/" class="payments"><span></span>Выплаты</a>
                    <?if ($arElement["PROPERTIES"]["rest"]["VALUE"]):?>
                        <a href="/bs/dir/rest<?=$arElement["PROPERTIES"]["rest"]["VALUE"]?>/" class="orders"><span></span>Заказы</a>
                    <?else:?>
                        <a href="javascript:void(0)" onclick="alert('Эта запись является дублем, см. запись с тем же названием')" class="orders"><span></span>Заказы</a>
                    <?endif;?>
                <?if(in_array($arParams["USER"], $ADM_ARRAY)){?>
                <a href="#<?=$arElement["ID"]?>" class="edite"><span></span>Редактировать</a>
                <a href="#<?=$arElement["ID"]?>" class="del"><span></span>Удалить</a>
                <?}?>
            </div>
            </div>
        <?if(!$arElement["DISPLAY_PROPERTIES"]['rest']['LINK_ELEMENT_VALUE'][$arElement["DISPLAY_PROPERTIES"]['rest']['VALUE']]['NAME']):?>
            <?
            $res = CIBlockElement::GetByID($arElement["DISPLAY_PROPERTIES"]['rest']['VALUE']);
            if($ar_res = $res->Fetch()){
                echo $ar_res['NAME'];
            }
            else {
                echo $arElement['NAME'];
            }
            ?>
        <?else:?>
		    <?=$arElement["DISPLAY_PROPERTIES"]['rest']['LINK_ELEMENT_VALUE'][$arElement["DISPLAY_PROPERTIES"]['rest']['VALUE']]['NAME']//$arElement['NAME']?>
        <?endif?>
        </td>
		<td class="c2"><?=$arElement["PROPERTIES"]["dolg"]["VALUE"]?> руб.</td>
		<td class="c3"><?=$arElement["dolg"]?> руб.</td>
		<td class="c3"><?if($RDOLG[$arElement["PROPERTIES"]["rest"]["VALUE"]]>0&&$arElement["PROPERTIES"]["rest"]["VALUE"]>0){?>(<?=$RDOLG[$arElement["PROPERTIES"]["rest"]["VALUE"]]?> руб.)<?}?></td>
		<td class="c4"><?=$arElement["PREVIEW_TEXT"]?></td>
		<td class="c5"><input type="checkbox" name="ids[]" value="<?=$arElement["ID"]?>" class="ch"/></td>
	</tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
	</table>
</form>

<div class="paginator">
	<?=$arResult["NAV_STRING"]?>
	<a href="print_dolg.php" target="_blank" class="print_dolg">Распечатать</a>
	<a href="get_dolg_xls.php" target="_blank" class="xls_dolg">*.xls</a>
</div>

</div>
<?}?>
