<?
define("NO_KEEP_STATISTIC", true);

define("SITE_TEMPLATE_PATH", "/bitrix_personal/templates/Booking_dir");
define("SITE_TEMPLATE_ID", "Booking_dir");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/*Добавить проверку на авторизацию и все такое*/
/*нужно сделать дву универсальные функциидля запроса инфы по заказу и по клиенту*/


function user_auth(){
	global $USER;
	if (!is_object($USER)) $USER = new CUser;
	$arAuthResult = $USER->Login($_REQUEST["USER_LOGIN"], $_REQUEST["USER_PASSWORD"], "N");
	
	if($arAuthResult["TYPE"]=="ERROR"){
		echo $arAuthResult["MESSAGE"];
	}else{
		echo "ok";
		$_SESSION["OPERATOR_NUMBER"] = $_REQUEST["OPERATOR_NUMBER"];

	}
}



function del_dolg(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$ID = str_replace("#", "", $_REQUEST["id"]);

	//сначала удаляем оплаты по должнику
	$arFilter = Array("IBLOCK_ID"=>2542, "ACTIVE"=>"Y", "PROPERTY_dolg"=>$ID);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
	while($arFields = $res->GetNext()){
		echo $arFields["ID"];
		CIBlockElement::Delete($arFields["ID"]);
	}
	
	
	//Теперь удаляем должника
	CIBlockElement::Delete($ID);
	
	echo 'ok';
}


function del_pay(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$ID = str_replace("#", "", $_REQUEST["id"]);

	//удаляем оплату
	CIBlockElement::Delete($ID);
	
	echo 'ok';
}


function dolg_edit_form(){
	global $APPLICATION;

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}


	$ELEMENT_ID = str_replace("#", "", $_REQUEST["id"]);

	$res = CIBlockElement::GetByID($ELEMENT_ID);
	if($ob = $res->GetNextElement()){
  		$arProps = $ob->GetProperties();
  		$arFields = $ob->GetFields();
	}
	
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/dolg_edit_form.php"),
		Array("ID"=>$ELEMENT_ID, "COMMENTS"=>$arFields["PREVIEW_TEXT"]),
		Array("MODE"=>"php")
	);


}


function sort_table(){
	global $APPLICATION;
//	global $USER;

//    if($USER->IsAdmin()){
        if($_REQUEST["order"]=="down") $_REQUEST["order"]="DESC,nulls";
        else $_REQUEST["order"]="ASC,nulls";
//    }
//    else {
//        if($_REQUEST["order"]=="down") $_REQUEST["order"]="DESC";
//        else $_REQUEST["order"]="ASC";
//    }

	
	if($_REQUEST["by"]!=$_SESSION["DOLG_BY"]) $_SESSION["DOLG_BY"]=$_REQUEST["by"];
	if($_REQUEST["order"]!=$_SESSION["DOLG_ORDER"]) $_SESSION["DOLG_ORDER"]=$_REQUEST["order"];
	
	$APPLICATION->IncludeComponent("bitrix:catalog.section", "dolgniki", Array(
	"IBLOCK_TYPE" => "booking_service",	// Тип инфоблока
	"IBLOCK_ID" => "2541",	// Инфоблок
	"SECTION_ID" => "",	// ID раздела
	"SECTION_CODE" => $_SESSION["CITY"],	// Код раздела
	"SECTION_USER_FIELDS" => array(	// Свойства раздела
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $_SESSION["DOLG_BY"],	// По какому полю сортируем элементы
	"ELEMENT_SORT_ORDER" => $_SESSION["DOLG_ORDER"],	// Порядок сортировки элементов
	"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
	"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
	"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
	"PAGE_ELEMENT_COUNT" => 2000000,	// Количество элементов на странице
	"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
	"PROPERTY_CODE" => array(	// Свойства
		0 => "rest",
		1 => "dolg",
		2 => "",
		3 => "",
		4 => "",
	),
	"OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
	"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
	"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
	"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
	"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
	"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
	"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "N",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
	"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
	"PRICE_CODE" => "",	// Тип цены
	"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
	"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
	"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
	"PRODUCT_PROPERTIES" => "",	// Характеристики товара
	"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
	"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
	"PAGER_TITLE" => "Товары",	// Название категорий
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "win_pager",	// Название шаблона
	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
	);

}


function edite_dolg(){
	global $APPLICATION;
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	$el = new CIBlockElement;
	
	$arLoadProductArray = Array("PREVIEW_TEXT"=> htmlspecialchars_decode($_REQUEST["comments"], ENT_NOQUOTES));
	
	$el->Update($_REQUEST["id"], $arLoadProductArray);
}


function pager_table(){
	global $APPLICATION;
	
	$APPLICATION->IncludeComponent("bitrix:catalog.section", "dolgniki", Array(
	"IBLOCK_TYPE" => "booking_service",	// Тип инфоблока
	"IBLOCK_ID" => "2541",	// Инфоблок
	"SECTION_ID" => "",	// ID раздела
	"SECTION_CODE" => $_SESSION["CITY"],	// Код раздела
	"SECTION_USER_FIELDS" => array(	// Свойства раздела
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $_SESSION["DOLG_BY"],	// По какому полю сортируем элементы
	"ELEMENT_SORT_ORDER" => $_SESSION["DOLG_ORDER"],	// Порядок сортировки элементов
	"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
	"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
	"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
	"PAGE_ELEMENT_COUNT" => 20000000,	// Количество элементов на странице
	"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
	"PROPERTY_CODE" => array(	// Свойства
		0 => "rest",
		1 => "dolg",
		2 => "",
		3 => "",
		4 => "",
	),
	"OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
	"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
	"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
	"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
	"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
	"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
	"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "N",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
	"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
	"PRICE_CODE" => "",	// Тип цены
	"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
	"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
	"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
	"PRODUCT_PROPERTIES" => "",	// Характеристики товара
	"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
	"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
	"PAGER_TITLE" => "Товары",	// Название категорий
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "win_pager",	// Название шаблона
	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
	);
}


function add_pay(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$el = new CIBlockElement;
	
	$PROP = array();
	$PROP["dolg"] = $_REQUEST["dolg"];  
	$PROP["summa"] = $_REQUEST["summa"]; 
	
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => 2542,
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => "Оплата от ".$_REQUEST["date"],
  		"ACTIVE"         => "Y",
  		"ACTIVE_FROM"=>$_REQUEST["date"],
  		"PREVIEW_TEXT"=>$_REQUEST["comment"]
  		);
	
	if($ID = $el->Add($arLoadProductArray)) echo $ID;
}


function save_pay(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$el = new CIBlockElement;
	
	$PROP = array();
	$PROP["dolg"] = $_REQUEST["dolg"];  
	$PROP["summa"] = $_REQUEST["summa"]; 
	
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => 2542,
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => "Оплата от ".$_REQUEST["date"],
  		"ACTIVE"         => "Y",
  		"ACTIVE_FROM"=>$_REQUEST["date"],
  		"PREVIEW_TEXT"=>$_REQUEST["comment"]
  		);
	
	$res = $el->Update($_REQUEST["pay_id"], $arLoadProductArray);
}


function get_pay_info(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	
	$ELEMENT_ID = str_replace("#", "", $_REQUEST["id"]);

	$res = CIBlockElement::GetByID($ELEMENT_ID);
	if($ob = $res->GetNextElement()){
  		$arProps = $ob->GetProperties();
  		$arFields = $ob->GetFields();
		
		$EXIT=array(
			"id"=>$ELEMENT_ID,
			"summa"=>$arProps["summa"]["VALUE"],
			"comment"=>$arFields["PREVIEW_TEXT"],
			"date"=>CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($arFields["ACTIVE_FROM"], CSite::GetDateFormat()))
	
		);
		
		echo json_encode($EXIT);
	}
}


function pay_fltr(){
	global $APPLICATION;
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	global $arrFilter;
	
	//if($_REQUEST["date_po"]!="") $_REQUEST["date_po"] = date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po"])));
	
	if($_REQUEST["date_s"]!="") $arrFilter[">=DATE_ACTIVE_FROM"]=$_REQUEST["date_s"];
	if($_REQUEST["date_po"]!="") $arrFilter["<=DATE_ACTIVE_FROM"]=$_REQUEST["date_po"];
	
	
	$arrFilter["PROPERTY_dolg"]=$_REQUEST["ID"];

	//var_dump($arrFilter);
	
	$APPLICATION->IncludeComponent("bitrix:catalog.section", "pays", Array(
	"IBLOCK_TYPE" => "booking_service",	// Тип инфоблока
	"IBLOCK_ID" => "2542",	// Инфоблок
	"SECTION_ID" => "",	// ID раздела
	"SECTION_CODE" => "",	// Код раздела
	"SECTION_USER_FIELDS" => array(	// Свойства раздела
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",	// По какому полю сортируем элементы
	"ELEMENT_SORT_ORDER" => "DESC",	// Порядок сортировки элементов
	"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
	"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
	"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
	"PAGE_ELEMENT_COUNT" => 200000000,	// Количество элементов на странице
	"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
	"PROPERTY_CODE" => array(	// Свойства
		0 => "",
		1 => "dolg",
		2 => "",
		3 => "",
		4 => "",
	),
	"OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
	"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
	"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
	"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
	"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
	"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
	"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
	"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
	"PRICE_CODE" => "",	// Тип цены
	"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
	"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
	"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
	"PRODUCT_PROPERTIES" => "",	// Характеристики товара
	"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
	"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
	"PAGER_TITLE" => "Товары",	// Название категорий
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "win_pager",	// Название шаблона
	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
	);

}


if($_REQUEST["act"]=="sort_table") sort_table();
if($_REQUEST["act"]=="del_dolg") del_dolg();
if($_REQUEST["act"]=="dolg_edit_form") dolg_edit_form();
if($_REQUEST["act"]=="edite_dolg") edite_dolg();
if($_REQUEST["act"]=="pager_table") pager_table();
if($_REQUEST["act"]=="add_pay") add_pay();
if($_REQUEST["act"]=="del_pay") del_pay();
if($_REQUEST["act"]=="get_pay_info") get_pay_info();
if($_REQUEST["act"]=="save_pay") save_pay();

if($_REQUEST["act"]=="pay_fltr") pay_fltr();

?>