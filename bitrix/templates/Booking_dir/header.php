<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="//www.google.com/jsapi"></script>

	<script type="text/javascript">
		google.load("jquery", "1");
		google.load("jqueryui", "1");
	</script>
	
	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle()?></title>

	<? 
	if($USER->IsAuthorized()){
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.maskedinput-1.3.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.ui.datepicker-ru.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jQuery.fileinput.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.form.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.typewatch.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/java.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/inputs.js');
		
	}else{
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/auth.js');
	}
	?>
	
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
<div id="conteiner">
			
	
	<div id="ajax_loader"></div>
	<?if ($USER->IsAuthorized()){?>
		
		<div class="center" id="content">
		<a href="/bs/" class="logo<? if($_SERVER["REAL_FILE_PATH"]=="/bs/dir/pays.php" || $_SERVER["REAL_FILE_PATH"]=="/bs/dir/rest_orders.php") echo'2';?>"></a>
		<input type="hidden" id="OPERATOR_ID" value="<?=$USER->GetID()?>"/>
		
		<input type="hidden" id="OPERATOR_NUMBER" value="<?=$_SESSION["OPERATOR_NUMBER"]?>"/>
		
		
		<?
		if($_SESSION["CITY"]==""){
			$rsUser = CUser::GetByID($USER->GetID());
			$arUser = $rsUser->Fetch();
			
			$_SESSION["CITY"]=$arUser["UF_CITY"];
			if($_SESSION["CITY"]=="") $_SESSION["CITY"]="msk";
		}
	
		//var_dump($_SERVER);
		$arGroups = CUser::GetUserGroup($USER->GetID());
		if((in_array(19, $arGroups)>0 || $USER->IsAdmin()) && $_SERVER["PHP_SELF"]=="/bs/dir/index.php"){

			if($_REQUEST["new_city"]!="" && $_SESSION["CITY"] != $_REQUEST["new_city"]) $_SESSION["CITY"] = $_REQUEST["new_city"];
		
			if($_SESSION["CITY"]=="msk") $CTY="Москва";
			if($_SESSION["CITY"]=="spb") $CTY="Санкт-Петербург";
		
		?>
		<form id="city_pos" action="/bs/" method="post">
			<input type="hidden" name="city" value=""/>
			<div class="select select_off" id="city" style="z-index: 800;">
				<div class="selected_element"><?=$CTY?></div>
				<div class="all_variants">
					<a href="msk" rel="nofollow" class="">Москва</a>
					<a href="spb" rel="nofollow" class="">Санкт-Петербург</a>
				</div>
			</div>
		</form>
		<?}?>
		
		
	
		<div id="dialog-confirm" title="">
			
		</div>
	<?}?>