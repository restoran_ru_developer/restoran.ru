<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /><br /><br />
        <?endif;?>
        <p><?=$arItem["NAME"]?></p>
        <?if ($arItem["PRICE"]["PRICE"]>0):?>
            <?if (CITY_ID=="ast"||CITY_ID=="amt"):?>
                <p><?=$arItem["PRICE"]["PRICE"]?> <span class="tenge font18">h</span></p>
            <?else:?>
                <p><?=$arItem["PRICE"]["PRICE"]?> <span class="rouble font18">e</span></p>
            <?endif;?>
        <?endif;?>
        <div class="clear"></div>
        <?if ($key==0):?>
        <div class="dotted"></div>
        <?endif;?>

<?endforeach;?>
