<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

//echo "<pre>"; print_r($arResult);echo "</pre>";

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1");
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");
?>

<?if($arResult["bDescPageNumbering"] === false):?>

	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <span class="num">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <a class="another nav" href="<?=$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
		<?else:?>
            <a class="another nav" href="<?=$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

<?endif?>