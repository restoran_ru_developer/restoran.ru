<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="catalog-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a class="catalog-item-img" href="/facebook/detail.php?RESTOURANT=<?= $arItem["ID"] ?>" style="background:url() top left no-repeat;"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" style="width:278px; height:185px;"></a>
			<?else:?>
				<img class="preview_picture" border="0" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" style="float:left" />
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="/facebook/detail.php?RESTOURANT=<?= $arItem["ID"] ?>"><?echo $arItem["NAME"]?></a>
			<?else:?>
				<?echo $arItem["NAME"]?>
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<?echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			
		<?endif?>
		
                    <?
                    //if (is_array($arItem["DISPLAY_PROPERTIES"]["sale10"]))
                      //  unset($arItem["DISPLAY_PROPERTIES"]["sale10"]);
                    ?>
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                        <span<?if($pid == "subway"):?> class="catalog-subway <?=CITY_ID?>"<?endif?>>                            
                            <?
                            //Название свойства
                            if($pid != "subway"):?><?=GetMessage("R_PROPERTY_".$arProperty["CODE"])?><?endif?>
                            <?
                            //Значение свойства                            
                            if(is_array($arProperty["DISPLAY_VALUE"]) && !$arProperty["LINK_IBLOCK_ID"]):?>
                                <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                    <?if (CITY_ID=="spb"):?>
                                        (812) 740-18-20
                                    <?else:?>
                                        (495) 988-26-56
                                    <?endif;?>                                                  
                                <?else:?>
                                    <?
                                    $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);
                                    if(is_array($arProperty["DISPLAY_VALUE"])) 
                                        $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
                                    else
                                        $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];

                                    $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);

                                    $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                    preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);

                                    $TELs=array();
                                    for($p=1;$p<5;$p++){
                                        foreach($matches[$p] as $key=>$v){
                                            $TELs[$key].=$v;
                                        }	
                                    }

                                    foreach($TELs as $key => $T){
                                            if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                    }
                                    ?>
                                    
                                <?endif;?>
                            <?elseif(is_array($arProperty["DISPLAY_VALUE"]) && $arProperty["LINK_IBLOCK_ID"]):?>
                                <?// subway and other?>
                                <?if($pid == 'subway' && $_REQUEST["arrFilter_pf"]["subway"][0] &&  $USER->IsAdmin()):?>
                                    <?foreach($arProperty["VALUE"] as $propValKey=>$propVal) {
                                        if(in_array($propVal, $_REQUEST["arrFilter_pf"][$pid])) {
                                            echo $arProperty["DISPLAY_VALUE"][$propValKey];
                                            break;
                                        }
                                    }?>
                                <?else:?>                                    
                                        <?=  strip_tags(implode(", ",$arProperty["DISPLAY_VALUE"]))?>                                    
                                <?endif?>
                            <?elseif(!is_array($arProperty["DISPLAY_VALUE"]) && $arProperty["LINK_IBLOCK_ID"]):?>
                                <?if ($pid=="average_bill"&&CITY_ID=="tmn"):
                                    $ab = str_replace("до 1000р","500 - 1000р",$arProperty["DISPLAY_VALUE"]);
                                    echo $ab;
                                else:?>
                                    <?=($arProperty["DISPLAY_VALUE"]);?>
                                <?endif;?>
                            <?elseif(!is_array($arProperty["DISPLAY_VALUE"]) && !$arProperty["LINK_IBLOCK_ID"]):?>
                                <?if ($pid=="phone"):?>
                                	<?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                            <?if (CITY_ID=="spb"):?>
                                                (812) 740-18-20
                                            <?else:?>
                                                (495) 988-26-56
                                            <?endif;?>                                                    
                                        <?else:?>
                                            <?
                                            $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                            if(is_array($arProperty["DISPLAY_VALUE"])) 
                                                $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);							
                                            else
                                                $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];

                                            $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);

                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                            preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);

                                            $TELs=array();
                                            for($p=1;$p<5;$p++){
                                                    foreach($matches[$p] as $key=>$v){
                                                            $TELs[$key].=$v;
                                                    }	
                                            }

                                            foreach($TELs as $key => $T){
                                                    if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                            }

                                            foreach($TELs as $key => $T){?>
                                                <?=$arProperty["DISPLAY_VALUE3"][$key]?><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                            <?}?>
                                        <?endif;?>
                                <?else:?>
                                    <?=($arProperty["DISPLAY_VALUE"])?>
                                <?endif;?>
                            <?endif?>                            
                        </span>
                    <?endforeach;?>
          
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
