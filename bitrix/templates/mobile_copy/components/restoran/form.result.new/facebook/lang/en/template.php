<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS ['FORM_ADD'] = "Add";
$MESS ['FORM_ACCESS_DENIED'] = "Web-form access denied.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you. Your application form #";
$MESS ['FORM_DATA_SAVED2'] = " was received.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Web-form module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "Web-form is not found.";
$MESS ['I_WANT'] = "I want ";
$MESS ['IN_RESTORAN'] = "in restaurant";
$MESS ['NA'] = "date";
$MESS ['AT'] = "time";
$MESS ['OUR_COMPANY'] = "Our company consists of";
$MESS ['PEOPLE'] = "persons";
$MESS ['HOW_MUCH_BUY'] = "we pland to spend";
$MESS ['BY_PERSON'] = "rub. by person";
$MESS ['MY_NAME'] = "My name is";
$MESS ['FOR_FEEDBACK'] = "For feedback I leave";
$MESS ['MY_PHONE'] = "my phone:";
$MESS ['MY_EMAIL'] = "E-mail:";   
$MESS ['MY_WISH'] = "Your wishes";   
$MESS ['CAPTCHA'] = "I confirm that I am not a bot, the picture reads:";  
?>
