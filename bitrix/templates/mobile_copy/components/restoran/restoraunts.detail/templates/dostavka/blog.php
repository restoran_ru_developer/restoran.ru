<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $arBlogFilter;
$arBlogFilter = Array("UF_USER"=>(int)$_REQUEST["user"],"UF_TYPE"=>2);
$APPLICATION->IncludeComponent(
    "restoran:article.list",
    "blogs_post_main",
    Array(
            "IBLOCK_TYPE" => "blogs",
            "IBLOCK_ID" => 103,
            "PAGE_COUNT" => 3,
            "SECTION_ID" => "",
            "SECTION_CODE" => "",
            "SECTION_URL" => "",
            "PICTURE_WIDTH" => 232,
            "PICTURE_HEIGHT" => 127,
            "DESCRIPTION_TRUNCATE_LEN" => 200,
            "COUNT_ELEMENTS" => "N",
            "TOP_DEPTH" => "1",
            "SECTION_FIELDS" => "",
            "SECTION_USER_FIELDS" => Array("UF_USER","UF_TYPE"),
            "FILTER_NAME" => "arBlogFilter",
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => "Y"
    ),
    false
);?>
<div align="right">
    <a href="/blogs/" class="uppercase">Все записи блога</a>
</div>
