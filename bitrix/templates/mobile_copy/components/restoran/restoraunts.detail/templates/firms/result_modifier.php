<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek"
);

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
        }
    }
}
//$arResult["NEXT_POST"] = getPost("next",$arParams["IBLOCK_TYPE"],$arResult["IBLOCK_ID"], $arResult["ID"]);                
//$arResult["PREV_POST"] = getPost("prev",$arParams["IBLOCK_TYPE"],$arResult["IBLOCK_ID"], $arResult["ID"]); 
// get preview text
$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut(strip_tags($arResult["DETAIL_TEXT"]), 250);
$arResult["NEXT_ARTICLE"] = RestIBlock::GetArticleNext($arResult["IBLOCK_ID"],$arResult["ID"]);
$arResult["PREV_ARTICLE"] = RestIBlock::GetArticlePrev($arResult["IBLOCK_ID"],$arResult["ID"]);


$res = array();
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>2423),false,Array("nTopCount"=>1));
$arResult["NEWS_COUNT"] = $res->SelectedRowsCount();

$res = array();
$arReviewsIB = getArIblock("news", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arReviewsIB["ID"]),false,Array("nTopCount"=>1));
$arResult["NEWS1_COUNT"] = $res->SelectedRowsCount();

?>