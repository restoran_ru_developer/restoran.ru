<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="HandheldFriendly" content="True" />
        <?$APPLICATION->ShowHead()?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadScript('/facebook/js/jquery-latest.min.js'); ?>                
        <link rel="stylesheet" href="/facebook/style.css" type="text/css"   rel="stylesheet"/>
        <link href="<?=SITE_TEMPLATE_PATH?>/popup.css?24"  type="text/css" rel="stylesheet" />
 <? $APPLICATION->AddHeadScript('/facebook/scripts/filter.js'); ?>  

        <!--<script type="text/javascript" src="/bitrix/templates/main/script_min.js"></script>-->
<script type="text/javascript" src="/bitrix/components/bitrix/search.title/script.js"></script>
<script type="text/javascript" src="/bitrix/templates/mobile_copy/components/bitrix/search.title/main_search_suggest_new1/script.js"></script>
<script type="text/javascript" src="/bitrix/components/restoran/catalog.filter/templates/new3/script.js"></script>
<script type="text/javascript" src="/bitrix/components/restoran/comments_add_new/templates/with_rating_new_on_page/script.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/header.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/util.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/button.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.base.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.form.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.xhr.js"></script>

<script type="text/javascript" src="/tpl/js/fu/js/dnd.js"></script>
<script type="text/javascript" src="/tpl/js/jquery.js"></script>
<script type="text/javascript" src="/facebook/js/jquery-ui.js"></script>
<script src="/facebook/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="/facebook/js/jquery.jscrollpane.min.js" type="text/javascript"></script> 
<script type="text/javascript" src="/tpl/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="/tpl/js/jquery_placeholder.js"></script>
<script type="text/javascript" src="/tpl/js/jquery.popup.js"></script>
    <script>
  $(function() {
    $( "#datepicker" ).datepicker({ altField: "#actualDate", dateFormat: "dd.mm.yy" });
  });
  </script>

<script>
	$(function()
{
	$('.scroll-pane').jScrollPane();
});
</script>

<script>
	$(function()
{
	$('.scroll-pane-down').jScrollPane();    
});
</script>



     
    </head>
    <body>

     <div id="wrap">
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
	<header id="header">
		<div class="logo"><a href="/facebook/"><img src="/facebook/img/logo.png"></a></div>
                <div class="city_select">
                    <?$APPLICATION->IncludeComponent(
                            "restoran:city.selector",
                            "city_select",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_URL" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000000",
                                "CACHE_NOTES" => "new32124332s36",
                                "CACHE_GROUPS" => "Y"
                            ),
                        false
                        );?> 
                </div>
		<div class="top-menu">
			<ul>
				<li><a href="/facebook/catalog.php">Рестораны</a></li>
				<li><a href="/facebook/opinions.php">Отзывы</a></li>
				<li><a href="/<?=CITY_ID?>/news/restoransnews<?=CITY_ID?>/" target="_blank">Спецпредложения</a></li>
			</ul>
		</div>
			<a href="/facebook/online_order.php" class="order-table-head"></a>
		<div class="tel-head">
                    <?
                        $APPLICATION->IncludeFile(
                            $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                            Array(),
                            Array("MODE"=>"html")
                        );
                    ?>
                </div>
		<div class="head-social">
			<div><a href="http://vk.com/restoranru" target="_blank"><img src="/facebook/img/vk.png"></a></div>
			<div><a href="https://www.facebook.com/Restoran.ru" target="_blank"><img src="/facebook/img/fb.png"></a></div>
		</div>


 

<?$arRestIB = getArIblock("catalog", CITY_ID);?>
<?$APPLICATION->IncludeComponent("bitrix:search.title", "main_search_suggest_new1", array(
	"NUM_CATEGORIES" => "1",
	"TOP_COUNT" => "5",
	"ORDER" => "date",
	"USE_LANGUAGE_GUESS" => "Y",
	"CHECK_DATES" => "N",
	"SHOW_OTHERS" => "N",
	"PAGE" => "#SITE_DIR#facebook/search.php",
	"CATEGORY_0_TITLE" => "",
	"CATEGORY_0" => array(
		0 => "iblock_catalog",
	),
	"CATEGORY_0_iblock_catalog" => array(
		0 => $arRestIB["ID"],
	)
	),
	false
);?>
	</header><!-- #header-->

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "page",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE" => ""
	),
	false
);?>

	<section id="middle" <?=($APPLICATION->GetCurPage()=="/facebook/opinion-add.php")?'class="opinion-page"':''?><?=($APPLICATION->GetCurPage()=="/facebook/search.php")?'class="search-page-bq"':''?><?=($APPLICATION->GetCurPage()=="/facebook/online_order.php")?'class="order-page"':''?><?=($APPLICATION->GetCurPage()=="/facebook/detail.php")?'class="rest-page"':''?>>