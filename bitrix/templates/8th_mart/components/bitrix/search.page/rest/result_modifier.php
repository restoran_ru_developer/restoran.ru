<?
CModule::IncludeModule("iblock");
global $search;
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $search = 1;
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();            
            $arItem["ELEMENT"] = $el;
            if ($el["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            elseif ($el["DETAIL_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_PROPORTIONAL, true);                                    
             
            if (!$arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm.png";
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"];            
         }
    endforeach;?>
<?endif;?>
