<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true)?>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js')?>

    <?$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/cusel.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/styles.css?1"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js')?>            
    <?$APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js')?>             
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js')?>  
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.system_message.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/radio.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js')?>
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
    <?
        CModule::IncludeModule("advertising");    
        CAdvBanner::SetRequiredKeywords (array(CITY_ID));
    ?>
</head>
<body style="background:#FFF">
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="container" style="background:url(/tpl/images/spec/new_year/new_year.jpg) center top no-repeat">        
        <div id="wrapp">
        <div id="header">
            <div class="baner" style="height:90px;">
                <?$APPLICATION->IncludeComponent(
                	"bitrix:advertising.banner",
                	"",
                	Array(
                		"TYPE" => "top_main_page_".CITY_ID,
                		"NOINDEX" => "Y",
                		"CACHE_TYPE" => "N",
                		"CACHE_TIME" => "600"
                	),
                false
                );?>
            </div>      
            <div id="logo"><a href="/"><img src="/tpl/images/logo_ny.png" /></a></div>  
            <div id="slogan">
                <p class="s3">
                        <?
                        $APPLICATION->IncludeFile(
                            $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </p>
            </div>            
            <div id="logo_spec1"><a href="/<?=CITY_ID?>/articles/new_year_corp/">Новогодние <div class="appetite">корпоративы</div></a></div>
            <div id="logo_spec2"><a href="/<?=CITY_ID?>/articles/new_year_night/">Новогодняя <div class="appetite">ночь</div></a></div>
            <?$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");?>            
        </div>                
        <div id="content">
            <div id="ny_gifts"><img src="/tpl/images/spec/new_year/ny_gifts.png" /></div>