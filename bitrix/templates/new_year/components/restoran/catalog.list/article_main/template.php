<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="left new_restoraunt <?=(end($arResult["ITEMS"])==$arItem||$key%3==2)?"end":""?>">
        <?if ($arResult["IBLOCK_TYPE_ID"]!="blogs"):?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img vspace="10" src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a>
        <?endif;?>
        <?if ($arResult["IBLOCK_TYPE_ID"]=="blogs"):?>
            <div style="margin-bottom:10px;">
                <a href="/users/id<?=$arItem["CREATED_BY"]?>/" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
            </div>
        <?endif;?>
        <div class="title">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </div>
        <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <div class="left">
            <?/*if ($arResult["IBLOCK_TYPE_ID"]=="blogs"):?>
                <i><?=$arItem["AUTHOR_NAME"]?></i>
            <?else:*/?>
                Комментарии: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)
            <?//endif;?>
        </div>
        <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
    <?if (end($arResult["ITEMS"])==$arItem||$key%3==2):?>
        <div class="clear"></div>
    <?endif?>
<?endforeach;?>
<div class="clear"></div>
<?//$arResult["ITEMS"][0]["LIST_PAGE_URL"] = str_replace("content",CITY_ID,$arResult["ITEMS"][0]["LIST_PAGE_URL"])?>
<?if ($arParams["IBLOCK_TYPE"]=="cookery"&&$arParams["IBLOCK_ID"]==145):?>
    <div align="right"><a class="uppercase" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_".$arParams["IBLOCK_TYPE"]."m")?></a></div>
<?else:?>
    <div align="right"><a class="uppercase" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_".$arParams["IBLOCK_TYPE"])?></a></div>
<?endif;?>