<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/*foreach($arResult["ITEMS"] as $key=>$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
  //  $arResult["SECTION"]["PATH"]
}*/
foreach($arResult["ITEMS"] as $key=>$arItem) {
    if ($arResult["IBLOCK_TYPE_ID"]):
        if($arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"])
            $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
        else
        {
            $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ITEMS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
            $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
        }
        $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
        $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
        $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
        $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
        if($arUser = $rsUser->Fetch())
        {
        	//if($arUser["PERSONAL_PROFESSION"]!="") $arResult["ITEMS"][$key]["AUTHOR_NAME"]=$arUser["PERSONAL_PROFESSION"];
		//	else $arResult["ITEMS"][$key]["AUTHOR_NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"];
            $arResult["ITEMS"][$key]["AUTHOR_NAME"] = $arUser["PERSONAL_PROFESSION"];      
            if (!$arResult["ITEMS"][$key]["AUTHOR_NAME"])
            {
                $temp = explode("@",$arUser["EMAIL"]);
                $arResult["ITEMS"][$key]["AUTHOR_NAME"] = $temp[0];
            }

			/*
            if ($arUser["LAST_NAME"])
                $arResult["ITEMS"][$key]["AUTHOR_NAME"] = $arUser["LAST_NAME"];
            else
                $arResult["ITEMS"][$key]["AUTHOR_NAME"] = $arUser["NAME"];
        	*/
        }
    endif;
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, false,Array());
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]  = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    //$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::GetPath($arResult["ITEMS"][$key]["DETAIL_PICTURE"]);
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("#CITY_ID#",CITY_ID,$arResult["ITEMS"][$key]["DETAIL_PAGE_URL"]);
    //$arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("content",CITY_ID,$arResult["ITEMS"][$key]["DETAIL_PAGE_URL"]);
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
}

?>