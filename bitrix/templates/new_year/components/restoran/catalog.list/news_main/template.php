<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="cool_news">
<?foreach($arResult["ELEMENTS"] as $arItem):?>
<div class="left news_img" align="center">
    <div style="overflow:hidden; height:344px">
        <?if ($arItem["VIDEO_PLAYER"]):?>
            <?=$arItem["VIDEO_PLAYER"]?>
        <?else:?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]?>" width="480" /></a>
        <?endif?>
    </div>
    <div class="clear"></div>
    <?/*if($arItem["COMMENT"]):?>
        <div class="left"><b><?=GetMessage("CT_BNL_NEWS_COMMENT_TITLE")?>: </b></div>
        <div class="right comment"><?=$arItem["COMMENT"]["POST_MESSAGE"]?></div>
        <div class="clear"></div>
        <div class="left"><?=$arItem["COMMENT"]["AUTHOR_NAME"]?></div>
        <div class="right comment_more"><a href="#"><?=GetMessage("CT_BNL_NEWS_COMMENT_SHOW_ALL")?></a></div>
    <?endif*/?>
</div>
<div class="right news_text">
    <span class="date_size"><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?></span> <span><?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></span>
    <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?=$arItem['NAME']?></a></div>
    <p><?=$arItem['PREVIEW_TEXT']?></p>
    <p align="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_NEWS_DETAIL_LINK")?></a></p>
    <div class="clear"></div>
    <br />
    <?//v_dump($arItem["DISPLAY_PROPERTIES"])?>
    <!--<div align="right"><a class="uppercase" href="<?=($arParams["ANOTHER_LINK"])?$arParams["ANOTHER_LINK"]:$arItem["SECTION_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ALL_NEWS_LINK")?></a></div>-->
</div>
<div class="clear"></div>
<?endforeach;?>
</div>