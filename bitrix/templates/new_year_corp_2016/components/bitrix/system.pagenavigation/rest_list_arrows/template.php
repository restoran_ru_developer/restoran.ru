<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];
if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "pageRestCnt","index_php?page","index_php",'AJAX_REQUEST','ajax','PAGEN_1');
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");
?>
<?//if($USER->IsAdmin()):
//    $distance_dir = $_REQUEST["PROPERTY"]=='all'&&$_REQUEST["PROPERTY_VALUE"]=='distance'?true:false;
    ?>
    <?if(LANGUAGE_ID!='en'):?>
        <div class="js-restaurant-list-more-wrap">
            <?if($arResult['NavPageCount']>1&&$arResult['NavPageNomer']!=$arResult['NavPageCount']/*&&!$distance_dir*/):?>
                <a class="js-restaurant-list-more" nav-page-num="<?=$arResult['NavPageNomer']?>" nav-page-count="<?=$arResult['NavPageCount']?>">Показать больше</a>
            <?endif?>
        </div>
    <?endif?>
<?//endif?>

<?if ($arResult["NavPageNomer"] > 1):?>
    <a href="<?=$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1)?>" class="prev icon-arrow-left"></a>
<?else:?>
    <a href="javascript:void(0)" class="prev icon-arrow-left"></a>
<?endif?>
    <div class="pages">
        <?
        $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
        $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
        if($arResult["bDescPageNumbering"] === false) {
            //v_dump($_REQUEST);
            // to show always first and last pages
            $arResult["nStartPage"] = 1;
            $arResult["nEndPage"] = $arResult["NavPageCount"];

            $sPrevHref = '';
            if ($arResult["NavPageNomer"] > 1)
            {
                $bPrevDisabled = false;

                if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
                {
                    $sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
                }
                else
                {
                    $sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
                }
            }
            else
            {
                $bPrevDisabled = true;
            }

            $sNextHref = '';
            if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
            {
                $bNextDisabled = false;
                $sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
            }
            else
            {
                $bNextDisabled = true;
            }
            ?>

            <?
            $bFirst = true;
            $bPoints = false;
            do
            {
                if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
                {

                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                        <span class="current">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
                    <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false && strlen($_REQUEST["page"]) > 0):?>
                        <a href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
                    <?else:?>
                        <a href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
                    <?endif;
                    $bFirst = false;
                    $bPoints = true;
                }
                else
                {
                    if ($bPoints)
                    {
                        ?>...<?
                        $bPoints = false;
                    }
                }
                $arResult["nStartPage"]++;
            } while($arResult["nStartPage"] <= $arResult["nEndPage"]);
        }
        ?>
    </div>
<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
    <a href="<?=$arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1)?>" class="next icon-arrow-right"></a>
<?else:?>
    <a href="javascript:void(0)" class="next icon-arrow-right"></a>
<?endif?>