<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
    $(function(){
        $(".articles_photo").galery();
    });
</script>
<div id="detail_rest" >
    <?
    if($arResult['PROPERTIES']['RESTORAN']['VALUE'][0]){
        $res = CIBlockElement::GetByID($arResult['PROPERTIES']['RESTORAN']['VALUE'][0]);
        if($ar_res = $res->GetNext()){
            $REST_LINK = $ar_res['DETAIL_PAGE_URL'];
        }
    }
    ?>
        <h1><?if($REST_LINK):?><a href="<?=$REST_LINK?>"><?endif?>Новогодние корпоративы в ресторане &#171;<?=HTMLToTxt($arResult["~NAME"])?>&#187;<?if($REST_LINK):?></a><?endif?></h1>
        <?//FirePHP::getInstance()->info($arResult);
        if($arResult["PREVIEW_TEXT"]):?>
            <i><?=$arResult["PREVIEW_TEXT"]?></i>
            <br />
        <?endif;?>
        <?=$arResult["DETAIL_TEXT"]?>
        <div class="clear"></div>
<!--        <div class="statya_section_name">            -->
<!--            --><?//if ($arResult["PREV_ARTICLE"]):?>
<!--                <div class="--><?//=($arResult["NEXT_ARTICLE"])?"left":"right"?><!--">-->
<!--                    <a class="statya_left_arrow no_border" href="--><?//=$arResult["PREV_ARTICLE"]?><!--">--><?//=GetMessage("NEXT_POST")?><!--</a>-->
<!--                </div>-->
<!--            --><?//endif;?>
<!--            --><?//if ($arResult["NEXT_ARTICLE"]):?>
<!--                <div class="right">-->
<!--                    <a class="statya_right_arrow no_border" href="--><?//=$arResult["NEXT_ARTICLE"]?><!--">--><?//=GetMessage("PREV_POST")?><!--</a>-->
<!--                </div>-->
<!--            --><?//endif;?>
<!--            <div class="clear"></div>            -->
<!--        </div>-->
</div>