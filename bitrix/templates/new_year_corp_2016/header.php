<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <meta name="author" content="Restoran.ru">    
        <meta property="fb:app_id" content="297181676964377" />    
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">         
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,700,700italic,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="/bitrix/templates/main_2014/css/datepicker.css"  type="text/css" rel="stylesheet" />

        <link href="<?=SITE_TEMPLATE_PATH?>/css/multiselect.css"  type="text/css" rel="stylesheet" />
        <link href="/bitrix/templates/main_2014/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->    
<!--        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>       -->


        <link href="<?=SITE_TEMPLATE_PATH?>/css/style.css"  type="text/css" rel="stylesheet" />

        <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-multiselect.js')?>        
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/bootstrap-datepicker.js?141145344014054')?>
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/maskedinput.js?14085357603362')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/jquery.autocomplete.min.js?140836218021406')?>
        <?$APPLICATION->ShowHead()?>
        <?
        CModule::IncludeModule("advertising");
        CAdvBanner::SetRequiredKeywords(array(CITY_ID));
        ?>
    </head>
    <body>
        <div id="panel"><? $APPLICATION->ShowPanel() ?></div>

        <div class="container <?if($_REQUEST['CODE']):?>inner-page<?endif?> " style="background: url(<?=SITE_TEMPLATE_PATH?>/img/top_NY_bg.jpg) center top no-repeat;">
            <div class="t_b_980">
            </div>
            <div class="page-header block" style="">
                <a class="mobile-book-phone" href="tel:<?if(CITY_ID=='spb'):?>+78127401820<?else:?>+74959882656<?endif?>"><span></span></a>
                <a class="mobile-book-trigger booking" href="/tpl/ajax/online_order_rest.php">бронировать</a>
                <a class="new_year_night_logo" href="/<?=CITY_ID?>/articles/new_year_corp/"></a>
<!--                --><?//if($_REQUEST['CODE']):?><!--<a class="back-to-cpecial-list" href="/--><?//=CITY_ID?><!--/articles/letnie_verandy/"><span class="icon-arrow-left"></span>назад к верандам</a>--><?//endif?>
                <a class="new_year_night_logo-to-root" href="/"></a>

                <div class="phone pt-serif">
                    <span class="header-phone-wrap"><?if(CITY_ID=='spb'):?>+7 (812) <span>740 18 20</span><?else:?>+7 (495) <span>988 26 56</span><?endif?></span>
                    <a class="city">
                        <?if(CITY_ID=='spb'):?>Санкт-Петербург<?else:?>Москва<?endif?>
                    </a>
<!--                    <div class="order_link">-->
<!--                        <a href="/tpl/ajax/online_order_rest.php" class="booking">Бронировать <span>Бесплатно</span></a>-->
<!--                    </div>-->
                </div>
                <a href="/tpl/ajax/online_order_rest.php" class="booking pink-booking-btn">Бронировать Бесплатно</a>
            </div>

            <?if(!$_REQUEST['CODE']):?>
            <div style="float: left;width: 100%;margin-bottom: 30px;padding-bottom: 20px;" class="filter-box-wrapper">
                <div class="filter-box block" style="    margin-top: 10px;">
                    <noindex>
                        <form class="form-inline" role="form" action="/<?=CITY_ID?>/articles/new_year_corp/" method="get" id="rest_filter_form" style="<?if($_REQUEST['CODE']):?>display: none;<?endif?>">
                            <div class="form-group">
                                <label for="name">Название ресторана</label>
                                <input type="text" class="form-control" name="arrFilter_ff[NAME]" value="<?=$_REQUEST["arrFilter_ff"]["NAME"]?>" placeholder="Название ресторана">
                            </div>
                            <?
                            $arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");


                            $filter = array("bill","subway", "out_city",'offers');

                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.filter", "restoran",
                                Array(
                                    "IBLOCK_TYPE" => "special_project",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "FILTER_NAME" => "arrFilter",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => $filter,
                                    "PRICE_CODE" => array(),
                                    "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//
                                    "CACHE_TIME" => "36000009",
                                    "CACHE_GROUPS" => "N",
                                    "LIST_HEIGHT" => "5",
                                    "TEXT_WIDTH" => "20",
                                    "NUMBER_WIDTH" => "5",
                                    "SAVE_IN_SESSION" => "N"
                                )
                            );
                            ?>
                        </form>
                    </noindex>
                </div>
            </div>
            <?endif?>
            <div class="content block">                                 