<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--            <div class="ontop"> наверх</div>-->
            <?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
            <?$APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/seotext_".CITY_ID.".php"),
                Array(),
                Array("MODE"=>"html")
            );?>

<noindex>

    <div class="modal fade new-free-form" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-close">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&#9587; <!--&times;--></span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <?php $APPLICATION->IncludeFile("/tpl/include_areas/counters-from-muslim.php",Array(),Array("MODE"=>"php","NAME"=>"Счетчики"));?>
    <!-- Rating@Mail.ru counter -->
    <script type="text/javascript">//<![CDATA[
        var _tmr = _tmr || [];
        _tmr.push({id: '432665', type: 'pageView', start: (new Date()).getTime()});
        (function (d, w) {
            var ts = d.createElement('script'); ts.type = 'text/javascript'; ts.async = true;
            ts.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//top-fwz1.mail.ru/js/code.js';
            var f = function () {var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ts, s);};
            if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
        })(document, window);
        //]]></script>
    <noscript>
        <div style="position:absolute;left:-10000px;">
            <img src="//top-fwz1.mail.ru/counter?id=432665;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
        </div>
    </noscript>
    <!-- //Rating@Mail.ru counter -->
    <div id="top100counter" style="position:absolute;left:-10000px;"></div>
    <div style="position:absolute;left:-10000px;">
        <!--LiveInternet counter--><script type="text/javascript"><!--
            document.write("<a href='http://www.liveinternet.ru/click' "+
            "target=_blank><img src='//counter.yadro.ru/hit?t41.5;r"+
            escape(document.referrer)+((typeof(screen)=="undefined")?"":
            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
            ";"+Math.random()+
            "' alt='' title='LiveInternet' "+
            "border='0' width='31' height='31'><\/a>")
            //--></script><!--/LiveInternet-->
    </div>
    <script type="text/javascript">
        var _top100q = _top100q || [];

        _top100q.push(["setAccount", "2838823"]);
        _top100q.push(["trackPageviewByLogo", document.getElementById("top100counter")]);


        (function(){
            var top100 = document.createElement("script"); top100.type = "text/javascript";

            top100.async = true;
            top100.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/pack/pack.min.js";
            var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(top100, s);
        })();
    </script>
</noindex>


        </div>
        <div class="to-top-btn-bg"></div>
    </div>    
    <?
    setSeo();
    ?>

    <div class="footer">
        <div class="container">
            <div class="content block">
                <div class="copyright">
                    © <?=date("Y")?> Ресторан.Ru
                </div>
                <a class="back-to-site" href="/<?=CITY_ID?>/">ВЕРНУТЬСЯ НА САЙТ</a>
            </div>
        </div>
    </div>


</body>
</html>