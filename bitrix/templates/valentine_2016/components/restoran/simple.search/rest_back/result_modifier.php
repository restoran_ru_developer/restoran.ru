<?
CModule::IncludeModule("iblock");
if (is_array($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();            
            $arItem["ELEMENT"] = $el;
            /*if ($arItem["ELEMENT"]["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::GetPath($arItem["ELEMENT"]["PREVIEW_PICTURE"]);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm.png";*/
            if ($el["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_EXACT, true);
            elseif ($el["DETAIL_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_EXACT, true);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_EXACT, true);
            if(!$arItem["ELEMENT"]["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
         }
    endforeach;?>
<?endif;?>
