<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // resize images
    if(!$arItem["PREVIEW_PICTURE"]) {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 313, 'height' => 218), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 313, 'height' => 218), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    }
    if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";


    $res = CIBlockElement::GetByID($arItem['PROPERTIES2']['RESTORAN'][0]);
    if($ar_res = $res->Fetch()){
        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arItem['PROPERTIES2']['RESTORAN'][0], "sort", "asc", array("CODE" => "RATIO"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$key]['PROPERTIES']["RATIO"]['VALUE'] = $ob['VALUE'];
        }

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arItem['PROPERTIES2']['RESTORAN'][0], "sort", "asc", array("CODE" => "subway"));
        while ($ob = $res->Fetch())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]['PROPERTIES']["SUBWAY"]['VALUE'][] = $a['NAME'];
        }

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arItem['PROPERTIES2']['RESTORAN'][0], "sort", "asc", array("CODE" => "address"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$key]['PROPERTIES']["address"]['VALUE'] = $ob['VALUE'];
        }

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arItem['PROPERTIES2']['RESTORAN'][0], "sort", "asc", array("CODE" => "phone"));
        if ($ob = $res->Fetch())
        {
            if(preg_match('/,/',$ob['VALUE'])){
                $phone_str_arr = explode(',',$ob['VALUE']);
                $phone_str = clearUserPhone($phone_str_arr[0]);
            }
            elseif(preg_match('/;/',$ob['VALUE'])){
                $phone_str_arr = explode(';',$ob['VALUE']);
                $phone_str = clearUserPhone($phone_str_arr[0]);
            }
            else {
                $phone_str = clearUserPhone($ob['VALUE']);
            }
            $arResult["ITEMS"][$key]['PROPERTIES']["phone"]['VALUE'] = $phone_str;
        }

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arItem['PROPERTIES2']['RESTORAN'][0], "sort", "asc", array("CODE" => "lat"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$key]['PROPERTIES']["lat"]['VALUE'] = $ob['VALUE'];
        }

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $arItem['PROPERTIES2']['RESTORAN'][0], "sort", "asc", array("CODE" => "lon"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$key]['PROPERTIES']["lon"]['VALUE'] = $ob['VALUE'];
        }
    }

    $arItem["DETAIL_PAGE_URL"] = str_replace("#CITY_ID#", CITY_ID, $arItem["DETAIL_PAGE_URL"]);


}


?>