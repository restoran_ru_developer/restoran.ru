<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>                  
<div id="spec_block_list">
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <div class="spec" onclick="location.href='<?= $arItem["DETAIL_PAGE_URL"] ?>'"> 
<!--            <div class="left pic">
                
                <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" />
            </div>-->
            <div class="left pic" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat"> 
                <div class="title_box">
                    <a class="title <?=(strlen($arItem["NAME"])>53||  substr_count($arItem["NAME"], "br"))?"big":""?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a>
                </div>        
            </div>
            <div class="right text">   
                <div style="font-size:20px; line-height:32px;">
                    <?=$arItem["NAME"]?>
                </div>
                <?if ($arItem["adres"]):?>
                    <p><?=$arItem["adres"]?></p>
                <?endif;?>
                <?if ($arItem["subway"]):?>
                    <p class="metro_<?=CITY_ID?>"><?=$arItem["subway"]?></p>
                <?endif;?>                
                <?if ($arItem["average_bill"]):?>
                    <p><b>Средний счет:</b> <?=$arItem["average_bill"]?></p>
                <?endif;?>                
                <?if ($arItem["opening_hours"]):?>
                    <p><b>Время работы:</b> <?=$arItem["opening_hours"]?></p>
                <?endif;?>                
                    
                    <Br />
                <?if ($arItem["PREVIEW_TEXT"]):?>
                    <?=$arItem["PREVIEW_TEXT"]?><br />
                <?endif;?>                
            </div>            
            <div class="clear"></div>            
        </div>
    <? endforeach; ?>    
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
<div class="clear"></div>