<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <meta name="author" content="Restoran.ru">    
        <meta property="fb:app_id" content="297181676964377" />    
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">         
        <link href="http://fonts.googleapis.com/css?family=Playfair+Display:400,900,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/multiselect.css"  type="text/css" rel="stylesheet" />
        <link href="/bitrix/templates/main_2014/css/datepicker.css"  type="text/css" rel="stylesheet" />        
        <link href="/bitrix/templates/main_2014/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" /> 
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->    
<!--        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>       -->

        <link href='http://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>

        <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js?136601382093869')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-multiselect.js')?>        
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js?1')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?> 
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/bootstrap-datepicker.js?141145344014054')?> 
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/maskedinput.js?14085357603362')?>         
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/jquery.autocomplete.min.js?140836218021406')?>                 
        <?$APPLICATION->ShowHead()?>
        <?
        CModule::IncludeModule("advertising");
        CAdvBanner::SetRequiredKeywords(array(CITY_ID));
        ?>
    </head>
    <body>               
        <div class="bg" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/letnie_bg.jpg); "></div>
        <div id="panel"><? $APPLICATION->ShowPanel() ?></div>
        <div class="container">                             
            <div class="page-header block">                
<!--                <a class="new_year_night_logo" href="" ></a>-->
                <a class="restoran_logo" href="/" style="background: url(<?=SITE_TEMPLATE_PATH?>/img/na_rest.png) left top no-repeat; "></a>                
                <a class="title" href="/<?=CITY_ID?>/articles/letnie_verandy/"><img src="<?=SITE_TEMPLATE_PATH?>/img/lentine_title.png" alt="Летние верданды"/></a>
                <h1>Летние веранды</h1>
                <div class="phone serif">
                    <a href="/tpl/ajax/online_order_rest.php" class="booking" style="display: block;height: 113px;width: 100%;position: absolute;top: 0;left: 0;"></a>
                    <a class="serif city">
                        <?if (CITY_ID=="msk"):?>
                        Москва
                        <?elseif(CITY_ID=="spb"):?>
                        Санкт-Петербург
                        <?endif;?>
                    </a>                    
                    <?if (CITY_ID=="spb"):?>
                    <a href="/tpl/ajax/online_order_rest.php" class="booking">+7 (812) <b>740&nbsp;<!-- FOR SKYPE-->18&nbsp;20</b></a>
                    <?elseif(CITY_ID=="msk"):?>
                    <a href="/tpl/ajax/online_order_rest.php" class="booking">+7 (495) 988&nbsp;<!-- FOR SKYPE-->26&nbsp;56</a>
                    <?endif;?>
                    <div class="order_link">
                        <a href="/tpl/ajax/online_order_rest.php" class="booking"><span>Бронировать</span> Бесплатно</a>
                    </div>                                                                            
                </div>                
            </div>
            <div class="filter-box block" style="margin-top:20px;">
                <noindex>
                    <form class="form-inline" role="form" action="/<?=CITY_ID?>/articles/letnie_verandy/" method="get" id="rest_filter_form">                
                        <div class="form-group">
                            <label for="name">Название ресторана</label>
                            <input type="text" class="form-control" id="name2" name="arrFilter_ff[NAME]" value="<?=$_REQUEST["arrFilter_ff"]["NAME"]?>" placeholder="Название">
                        </div>
                        <?                    
                        $_REQUEST["SECTION_CODE"] = 'letnie_verandy';
                        $arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");                
                        CModule::IncludeModule("iblock");
                        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arIB["ID"]));
                        while ($prop_fields = $properties->Fetch())
                        {
                            if ($prop_fields["CODE"]!="RESTORAN"&&$prop_fields["CODE"]!="lat"&&$prop_fields["CODE"]!="lon")
                                $filter[] = $prop_fields["CODE"];                        
                        }                                            
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter", "restoran", 
                            Array(
                                "IBLOCK_TYPE" => "special_project",
                                "IBLOCK_ID" => $arIB["ID"],
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array(),   
                                "PROPERTY_CODE" => $filter,
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N"
                            )
                        );
                        ?><script type="text/javascript">
//                            $(document).ready(function() {
//                                $('.multiselect').multiselect({
//                                    numberDisplayed:1,
//                                    nonSelectedText: '',
//                                    nSelectedText: "Выбрано",   
//                                    maxHeight: 250,
//                                    buttonWidth: "140px"
//                                });
//                            });
                        </script>                                       
                    </form>  
                </noindex>
            </div>
            <div class="content block">                                 