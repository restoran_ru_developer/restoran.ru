<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$arResult["NAME"] = preg_replace("/&lt;br[ \/]+&gt;[0-9A-zА-ярР .]+/"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br/>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br />"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br /&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br/&gt;"," ",$arResult["NAME"]);

$APPLICATION->SetTitle('Летняя веранда в ресторане '.$arResult["NAME"]);
$APPLICATION->SetPageProperty("description", 'Летняя веранда в ресторане '.$arResult["NAME"].'');
$APPLICATION->SetPageProperty("keywords",  'летние, веранды, ресторан, '.$arResult["NAME"]);

global $RESTORAN_NAME, $element_id;
$RESTORAN_NAME = $arResult["NAME"];
$element_id = $arResult["PROPERTIES"]["RESTORAN"]["VALUE"][0];
?>