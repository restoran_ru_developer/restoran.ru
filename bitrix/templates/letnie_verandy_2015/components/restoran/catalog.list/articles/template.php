<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>   
<div id="spec_block_list_other">
    <?if (count($arResult["ITEMS"])>0):?>
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <div class="spec left" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat" onclick="location.href='<?= $arItem["DETAIL_PAGE_URL"] ?>'"> 
                <div class="title_box">
                    <a class="title <?=(strlen($arItem["NAME"])>=20||  substr_count($arItem["NAME"], "br"))?"big":""?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>">
                        <?= $arItem["NAME"] ?>
                    </a>
                </div>        
            </div>
        <? endforeach; ?>
        <div class="clear"></div>        
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>                    
            <div class="navigation">
                <div class="pages"><?= $arResult["NAV_STRING"] ?></div>
            </div>                           
        <?endif;?>
    <?else:?>
    <?    
        echo "
<p>
На данный момент мы не располагаем информацией о предложениях ресторана. Воспользуйтесь поиском или обратитесь в нашу службу бронирования, и мы с удовольствием подберем для вас ресторан или банкетный зал с учётом ваших пожеланий.!</p>";
    ?>
    <?endif;?>
</div>