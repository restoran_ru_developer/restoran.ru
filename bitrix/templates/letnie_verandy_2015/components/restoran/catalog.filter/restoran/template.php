<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
    $(function(){
        $(".subza_a").click(function(){
            if (!$(this).hasClass('active'))
                $(".subza").toggle();
        })        
    })
</script>
<?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
    <div class="form-group <?=($arProp['CODE']=="subway"||$arProp['CODE']=="area")?"subza":""?>" <?=($arProp['CODE']=="area")?"style='display:none;'":""?>  <?=($arProp["CODE"]=="with_dm")?"style='top:-20px;position:relative;'":""?>>
        <?if ($arProp['CODE']=="subway"):?>
            <label for="name"><a class="subza_a active" href="javascript:void(0)">станция Метро</a> / <a class="subza_a" href="javascript:void(0)">Район</a></label>
        <?elseif ($arProp['CODE']=="area"):?>
            <label for="name"><a class="subza_a" href="javascript:void(0)">станция Метро</a> / <a class="subza_a active" href="javascript:void(0)">Район</a></label>
        <?else:?>
            <label for="name"><?=$arProp["NAME"]?></label>
        <?endif;?>
        <?//if ($arProp["PROPERTY_TYPE"]!="L"):?>       
            <select multiple class="multiselect-<?=$arProp["CODE"]?> form-control" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]">                            
                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                    <?if ($arProp["CODE"]!="subway"):?>
                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>                
                    <?else:?>
                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val["NAME"]?></option>                
                    <?endif;?>                        
                <?endforeach?>
                        <option value="delete"><b>Сбросить все</b></option>                
            </select> 
            <script type="text/javascript">                                        
                $(function() {
                    $('.multiselect-<?=$arProp["CODE"]?>').multiselect({
                        numberDisplayed:1,
                        nonSelectedText: '<?=GetMessage('multiselect-'.$arProp["CODE"])?>',
                        nSelectedText: "Выбрано",   
                        maxHeight: 250,
                        buttonWidth: "<?=($arProp['CODE']=="subway"||$arProp['CODE']=="area")?"226px":"180px"?>",
                        onChange: function(option, checked, select) {
                            if ($(option).val()=="delete")
                            {
                                $('.multiselect-<?=$arProp["CODE"]?>').multiselect('deselectAll', true);
                                $('.multiselect-<?=$arProp["CODE"]?>').multiselect('updateButtonText');
                            }
                        }
                    });
                    
                    
                });
            </script>
        <?/*else:?>
            <div class="checkbox-box">
                <div id="<?=$arProp["CODE"]?>" class="niceCheck <?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]=="Да")?"active":""?>"><?=$arProp["NAME"]?></div>                                
                <input type="hidden" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>]" value="<?=$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]?>" />
            </div>            
        <?endif;*/?>        
    </div>        
<?endforeach;?>        
<div class="form-group">
    <label>&nbsp;</label>
    <input type="submit" class="form-control btn-light" value="Далее">
</div>
<script type="text/javascript">                                        
                $(function() {
                    $(".checkbox-box div").click(function(){                        
                        console.log($(this).hasClass("active"));
                        if (!$(this).hasClass("active"))
                        {
                            $(this).next().val("Да");
                            $(this).addClass("active");
                        }
                        else
                        {
                            $(this).next().val("");                        
                            $(this).removeClass("active");
                        }
                    });
                });
            </script>