<?global $element_id,$RESTORAN_NAME;
?>
<div class="detail-order-banner">
    <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
    <a href="/tpl/ajax/online_order_rest.php" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>'>
        <?else:?>
        <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>'>
            <?endif;?>
            <div class="detail-order-banner-title">Бронировать столик</div>
            <div class="detail-order-banner-in-place-str">
            <?if(!$RESTORAN_NAME):?>
                <div>в ресторанах<br><?=CITY_ID=='msk'?'Москвы':''?><?=CITY_ID=='spb'?'Санкт-Петербурга':''?><?=CITY_ID=='urm'?'Юрмалы':''?><?=CITY_ID=='rga'?'Риги':''?></div>
            <?else:?>
                <div>в ресторане<br><?=$RESTORAN_NAME?></div>
            <?endif?>
            </div>           
            <div class="detail-order-banner-addition-text-str">
                Быстро
                <br>
                Бесплатно
                <br>
                <?= ($_REQUEST["CATALOG_ID"] == "banket") ? "Оператор свяжется с Вами в течение 30 мин." : "СМС-подтверждение<br>через 5 минут" ?>
            </div>
            <div class="detail-order-banner-phone-tel"></div>
            <div class="detail-order-banner-above-phone-str">на сайте или по телефону</div>
            <div class="detail-order-banner-phone-str"><?if(CITY_ID=='msk')echo "+7 (495) 988-26-56";elseif(CITY_ID=='spb')echo "+7 (812) 740-18-20";elseif(CITY_ID=='rga'||CITY_ID=='urm')echo "+371 661-031-06";?></div>
            <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                <a href="/tpl/ajax/online_order_rest.php" class="booking detail-banner-order-button" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>'>Начать бронирование<span></span></a>
            <?else:?>
                <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking detail-banner-order-button" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>'>Начать бронирование<span></span></a>
            <?endif?>
        </a>
</div>