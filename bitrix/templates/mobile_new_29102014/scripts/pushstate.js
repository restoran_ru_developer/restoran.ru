window.addEventListener("load", function(e) {
    setTimeout(function() {
        window.addEventListener('popstate', function(e){        
            var href =  location.pathname+location.search;              
            if (history && history.pushState&&!android_fail) {                        
                    $("body").addClass("historypushed");  
                                $("#overflow").show();
                                $(".bubblingG").show(); 
                                $("main").addClass("blur");
                                if (href.indexOf("detail")>=0)
                                    $("#bubbling_text").text("Загружаем анкету ресторана...");
                                else if (href=="/")
                                    $("#bubbling_text").text("Загружаем ближайшие рестораны...");
                                else
                                    $("#bubbling_text").text("Загружаем страницу...");      
    //                 if (href.indexOf("detail")>=0)
    //                 {                     
    //                     if (href.indexOf("id")=="-1")
    //                     {
    //                         console.log(href.indexOf("id"));                         
    //                         return false;
    //                     }
    //                 }

                     $.get( href,{ajax:"Y"}, function( data ) {
                        if (href!="/")
                            $(window).unbind("scroll");
                        $( "main" ).html($(data).find("#ajax"));
                        $("#overflow").hide();
                        $(".bubblingG").hide();
                        $("main").removeClass("blur");
                        if (href.indexOf("detail")>=0)
                            history.replaceState(null, null, href);                                                        
                        else
                            history.pushState(null, null, href);                                                        
                        $("title").html($(data).find("#title").val()); 
                        if (href.indexOf("detail")>=0)
                        {}
                        else
                        {
                            $(".myhead").html($(data).find(".myhead").html())
                            menuclick();
                        }
                        if (href=="/map/") {
                            $("main").addClass("map");
                            $(".myhead .right_btn").html("Список").attr("href","/").addClass("ajax");
                        }
                        else
                        {
                            $("main").removeClass("map");
                        }
                    });
            }else{

            }                                               
        }, false);
    }, 0);
}, false);

$(function(){
    setOverflowD();    
    
    $("body").on("click","a.ajax",function(e){        
            var _this = $(this);
            var href = $(this).attr("href");
            setOverflowD();
            //if (href!="/map/")
            {
                
                if(window.location.pathname!=href && href!=""){
                        if (history && history.pushState&&!android_fail) {
                            e.preventDefault();
                            $("body").addClass("historypushed");  
                            $("#overflow").show();
                            $(".bubblingG").show(); 
                            $("main").addClass("blur");
                            if (href.indexOf("detail")>=0)
                                $("#bubbling_text").text("Загружаем анкету ресторана...");
                            if (href.indexOf("map/detail")>=0)
                                $("#bubbling_text").text("Загружаем карту...");
                            if (href=="/")
                                $("#bubbling_text").text("Загружаем ближайшие рестораны...");
                            $.get( href,{ajax:"Y"}, function( data ) {
                                if (href!="/")
                                    $(window).unbind("scroll");
                                $("body").scrollTop(0);
                                $( "main" ).html($(data).find("#ajax"));
                                $('.ok').hide();
                                $("#overflow").hide();
                                $(".bubblingG").hide();
                                $("main").removeClass("blur");
                                history.pushState(null, null, href);                                                        
                                $("title").html($(data).find("#title").val());                                 
                                if (href.indexOf("detail")>=0||href.indexOf("booking")>=0)
                                {}
                                else
                                {
                                    $(".myhead").html($(data).find(".myhead"));                                
                                    menuclick();
                                }
                                if (href=="/map/") {
                                    $("main").addClass("map");
                                    $(".myhead .right_btn").html("Список").attr("href","/").addClass("ajax");            
                                }
                                else
                                {
                                    $("main").removeClass("map");
                                }
                                if (href!="/auth/") {
                                    $("main").removeClass("grad");
                                }
                                else
                                {
                                    $("main").addClass("grad");
                                }
                                $(".need_select_city").show(); 
                            });
//                            $( "#main" ).load(href +" #ajax", {ajax:"Y"},function (data){
//                                if (href!="/")
//                                    $(window).unbind("scroll");
//                                else
//                                    $(window).scroll(function(){
//                                        scroll_load();
//                                    });
//                                $("#overflow").hide();
//                                $(".bubblingG").hide();
//                                $("main").removeClass("blur");
//                                history.pushState({foo: 'bar'}, $("#title").val(), href);                                                        
//                                $("title").html($(data).find("#title").val());                                
//                            });
                    }else{

                    }                               
                }                
            }
    });
    
    $("main").on("submit","#search_form",function(e){        
            var _this = $(this);
            var params = $(this).serialize()+"&ajax=Y";
            var href = $(this).attr("action");		                            
            
            if(href!=""){
                    if (history && history.pushState&&!android_fail) {
                        e.preventDefault();
                        $("body").addClass("historypushed");  
                        $("#overflow").show();
                        $("#bubbling_text").text("Загружаем результаты поиска...");
                        $(".bubblingG").show();                                                
                        $( "main" ).load(href +" #ajax", params,function (data){
                            $("#overflow").hide();
                            $(".bubblingG").hide();                            
                            history.pushState({foo: 'bar'}, $("#title").val(), href+"?"+params);                                                        
                            $("title").html($(data).find("#title").val());
                        });
                }else{

                }                               
            }
        return false;
    });  
    
    $("main").on("submit","#booking",function(e){ 
        if (null!=$("#booking").length)
        {
            if ($("#phone").val().length!=10)
            {
                alert("Телефон должен содержать 10 цифр (без 8).\nНапример: 9217774565");
                return false;
            }       
            if (!$("#name").val())
            {
                alert("Заполните поле имя,\n чтобы мы знали как к Вам обратиться");
                return false;
            }
            if (!$("#count").val())
            {
                alert("Введите кол-во человек");
                return false;
            }
            if (!$("#restoran-id").val())
            {
                alert("Выберите ресторан");
                return false;
            }               
            var _this = $(this);
            var params = $(this).serialize()+"&ajax=Y";
//            var href = $("#booking").attr("action");		                            
//            e.preventDefault();
//            if(href!=""){
//                    if (history && history.pushState) {
//                        $("body").addClass("historypushed");  
//                        $("#overflow").show();
//                        $(".bubblingG").show();                        
//                        $( "main" ).load(href +" #ajax", params,function (data){
//                            $("#overflow").hide();
//                            $(".bubblingG").hide();
//                            history.pushState({foo: 'bar'}, $("#title").val(), href);                                                        
//                            $("title").html($(data).find("#title").val());
//                        });
//                }else{
//
//                }                               
//            }
            //return false;
        }
    });   
        
//    $("main").on('keyup',$("#restoran"),function(){        
//        if ($("#restoran").length&&$("#restoran").val().length>2)
//        {
//            if (null!=$(".restoran_suggest"))
//            {
//                var a = "";
//                if (location.pathname.indexOf("/search/")>=0)
//                    a = "search";
//                else
//                    a = "";
//                $( ".restoran_suggest" ).load("/ajax/suggest.php", {"q":$("#restoran").val(),"sessid":$("#sessid").val(),"page":a}, function (data) {
//                    if (data) {
//                        $(".restoran_suggest").show();
//                    }
//                });
//            }
//        }
//    });
    
    $("main").on('focus',$("#restoran"),function(){
        if (null!=$(".restoran_suggest ul"))
        {
            $(".restoran_suggest").show();            
        }
    });
    $("main").on('click',"a.suggest-item",function(){
        $( ".restoran_suggest" ).hide();
        $("#restoran").val($(this).text());
        $("#restoran-name").val($(this).text());
        $("#restoran-id").val($(this).data("value"));                        
    });
});