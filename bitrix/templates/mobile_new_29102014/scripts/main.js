$(function(){
    'use strict';
    
    var navdrawerContainer = document.querySelector('.navdrawer-container');
    var appbarElement = document.querySelector('.app-bar');
    var menuBtn = document.querySelector('.menu');   

    var main = document.querySelector('main');
    function closeMenu() {
        $(".app-bar").removeClass("open");
         $(".navdrawer-container").removeClass("open");
//        appbarElement.classList.remove('open');
//        navdrawerContainer.classList.remove('open');
        $("#overflow").hide();
        $("html,body").removeClass("overflow");        
        return false;
    }

    function toggleMenu() {               
        $(".app-bar").toggleClass('open');
        $(".navdrawer-container").toggleClass("open");
//        appbarElement.classList.toggle('open');
//        navdrawerContainer.classList.toggle('open');
        $("#overflow").toggle();
        $("html,body").toggleClass("overflow");                 
        return false;        
    }

    //main.addEventListener('ontouchstart', closeMenu);
    //main.addEventListener('click', closeMenu);
    menuBtn.addEventListener('click', toggleMenu);
    navdrawerContainer.addEventListener('click', closeMenu);    
    $("#overflow").click(function(){
        closeMenu();
    });    
    navdrawerContainer.addEventListener('ontouchstart', closeMenu);
//    navdrawerContainer.addEventListener('click', function (event) {
//        if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
//            closeMenu();
//        }
//    });

   
});

function menuclick()
{
    'use strict';

    var navdrawerContainer = document.querySelector('.navdrawer-container');
    var appbarElement = document.querySelector('.app-bar');
    var menuBtn = document.querySelector('.menu');

    var main = document.querySelector('main');
    function closeMenu() {
        $(".app-bar").removeClass("open");
         $(".navdrawer-container").removeClass("open");
        $("#overflow").hide();
        $("html,body").removeClass("overflow");        
        return false;
    }

    function toggleMenu() {
        $(".app-bar").toggleClass('open');
        $(".navdrawer-container").toggleClass("open");
        $("#overflow").toggle();
        $("html,body").toggleClass("overflow");           
        return false;
    }
    $("#overflow").click(function(){
        closeMenu();
    });    
    //main.addEventListener('click', closeMenu);
    menuBtn.addEventListener('click', toggleMenu);
    navdrawerContainer.addEventListener('click', closeMenu);
    navdrawerContainer.addEventListener('ontouchstart', closeMenu);
//    navdrawerContainer.addEventListener('click', function (event) {
//        if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
//            closeMenu();
//        }
//    });
}

$(function(){    
    $("main").on("click",".stars_new li",function() {
//        if(!$(this).parents(".popup").hasClass("popup"))  
//            $("#review").focus();                               
        var count = $(this).index()+1;
        var i = 1;
        $("#input_ratio").attr("value",count);
        $(this).parent().find("li").each(function(){                
            if (i<=count)
                $(this).attr("class","active");
            else
                $(this).attr("class","");
            i++;
        })
    });    
    $(".filter *").click(function(e){
        e.stopPropagation();
    });
    $("main").click(function(){
        $(".rlist").removeClass("blur");
        $('.popup.filter').hide();
        $('.restoran_suggest').hide();        
    });
    $("main").on("click",".close",function(e){
        $('.items-detail').removeClass('blur');
        $('header').removeClass('blur');
        $('.popup.review').hide();
    }); 
    $("main").on('click',"a.btn.filter",function(e){
        e.preventDefault();
        e.stopPropagation();        
        $('.popup.filter').show();
        $("#overflow").show();                    
    });
    $("#overflow").click(function(){
        $(".popup").hide();
    });
});
function scroll_load() {
    //if (($(".item").last().offset().top-$(".item").last().height()*5)<$(window).scrollTop()&&!ajax_load)        
    if (($(document).height()-$(".item").last().height()*4)<$(window).scrollTop()&&!ajax_load&&max_page>page)
    {
        $(".bubblingG").show();        
        $("#overflow").show();
        $(".bubblingG #bubbling_text").text("Загрузка следующей страницы ресторанов...");        
        ajax_load = 1;        
        $.ajax({
            type: "POST",
            url: "/ajax/list.php"+location.search,
            data: {
                page:++page,
                PAGEN_1:page,
                pageRestSort:$("#sort").val()
            },
            success: function(data) {
                $(".bubblingG").hide();
                $(".bubblingG #bubbling_text").text("");
                $("#overflow").hide();                
                $(".item").last().after(data);   
                ajax_load = 0;
                setOverflowD();
            }
        });
    } 
}

function setOverflowD()
{
    $("#overflow").css("height",$(document).height()).css("width",$(window).width());
}