<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
function add2basket_dostavka(obj, id, sess)
{
	var pos_y = $(obj).offset().top-10;
	var pos_x = $(obj).offset().left-10;
	var q = $("#quantity"+id).val();
	if (!$("body").find("div").hasClass("dostavka_popup"))
		$("body").append("<div class='dostavka_popup'></div>");
	$(".dostavka_popup").css('left',pos_x+"px");
	$(".dostavka_popup").css('top',pos_y+"px");
	$(".dostavka_popup").load("/tpl/ajax/add2basket_dostavka.php?id="+id+"&q="+q+"&"+sess);		
        $(".dostavka_popup").fadeIn(300);
}

function del_basket_item(id, sess)
{
	$(".dostavka_popup").load("/tpl/ajax/add2basket_dostavka.php?id="+id+"&action=delete&"+sess);				
}
function preview_modal(id)
{
    $.ajax({
            type: "POST",
            url: "/tpl/ajax/menu_preview.php",
            data: "ID="+id,
            success: function(data) {
                if (!$("#preview_modal").size())
                {
                    $("<div class='popup popup_modal' id='preview_modal'></div>").appendTo("body");                                                               
                }
                $('#preview_modal').html(data);
                showOverflow();
                setCenter($("#preview_modal"));
                $("#preview_modal").fadeIn("300"); 
            }
        });    
}
$(document).ready(function(){	
	$(window).keyup(function(event) {
	  if (event.keyCode == '27') {
		$(".dostavka_popup").fadeOut(300);
	   }
	});      
});
</script>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
	?>
		<div id="<?=$this->GetEditAreaId($arElement['ID']);?>" class="new_restoraunt left <?=($cell%3==2)?"end":""?>" style="width:222px;">
                        <?if(is_array($arElement["PREVIEW_PICTURE"])):?>
				<div style="position:relative;margin-bottom:5px;">
					<img <?=($arElement["PREVIEW_TEXT"])?'onclick="preview_modal('.$arElement["ID"].')"':''?> style="cursor:pointer" border="0" src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="200" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /><br />
					<?if($arElement["CAN_BUY"]&&$_REQUEST['CATALOG_ID']=="dostavka"):?>
						<div style="position:absolute; left:0px; top:0px;">
							<!--<input name="buy" class="button" type="button" value="<?= GetMessage("CATALOG_BUY") ?>" OnClick="window.location='<?echo CUtil::JSEscape($arElement["DETAIL_PAGE_URL"]."#buy")?>'" />-->
							<input name="buy" class="button" type="button" value="<?=GetMessage("CATALOG_BUY")?>" onClick="add2basket_dostavka(this,'<?=$arElement["ID"]?>','<?=bitrix_sessid_get()?>')" />
						</div>
					<?endif?>
				</div>
			<?else:?>
				<?if($arElement["CAN_BUY"]&&substr_count($_SERVER["HTTP_REFERER"],"dostavka")):?>
					<!--<input name="buy" class="button" type="button" value="<?= GetMessage("CATALOG_BUY") ?>" OnClick="window.location='<?echo CUtil::JSEscape($arElement["DETAIL_PAGE_URL"]."#buy")?>'" />-->
					<input name="buy" class="button" type="button" value="<?=GetMessage("CATALOG_BUY")?>" onClick="add2basket_dostavka(this,'<?=$arElement["ID"]?>','<?=bitrix_sessid_get()?>')" />
				<?endif?>
			<?endif;?>
			<a href="javascript:void(0)" <?=($arElement["PREVIEW_TEXT"])?'onclick="preview_modal('.$arElement["ID"].')"':''?> class="<?=($arElement["PREVIEW_TEXT"])?'js_menu':'js_menu2'?>"><?=$arElement["~NAME"]?></a>
                        <?//substr ?>
                        <?if (substr_count($_SERVER["HTTP_REFERER"],"dostavka")):?>
                            <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                    <?if($arPrice["CAN_ACCESS"]):?>
                                            <table width="100%" cellpadding="5">
                                                    <tbody>
                                                            <tr>
                                                                <td align="left" width="40"><input class="inputtext font18" id="quantity<?=$arElement["ID"]?>" maxlength="3" type="text" value="1" size="1"></td>
                                                                <td class="font16" width="20"> x </td>
                                                                <td nowrap="" class="font24"><?=$arPrice["VALUE"]?> <span class="rouble font24">e</span></td>
                                                            </tr>
                                                    </tbody>
                                            </table>							
                                    <?endif;?>
                            <?endforeach;?>				
                        <?else:?>
                            <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                <?if($arPrice["CAN_ACCESS"]&&$arPrice["VALUE"]>0):?>
                                    <table width="100%" cellpadding="5">
                                        <tbody>
                                            <tr>
                                                <?/*?>
                                                <td align="left" width="10" class="font24">1</td>
                                                <td class="font16" width="10"> x </td>
                                                <?*/?>
                                                <td colspan="3" nowrap="" class="font24"><?=$arPrice["VALUE"]?> <span class="rouble font24">e</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                <?endif;?>
                            <?endforeach;?>
                        <?endif;?>			
			<!--<p><?//=$arElement["PREVIEW_TEXT"]?></p>-->
		</div>
		<?if($cell%3==2&&$arElement!=end($arResult["ITEMS"])):?>
			<div class="clear"></div>
			<div class="dotted"></div>
		<?endif;?>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
                        <?if (!count($arResult["ITEMS"]))
                            echo "<h4>В разделе нет товаров</h4>";
                            ?>
<div class="clear"></div>
<hr class="bold">
<div align="right">
<?/*if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;*/?>
</div>