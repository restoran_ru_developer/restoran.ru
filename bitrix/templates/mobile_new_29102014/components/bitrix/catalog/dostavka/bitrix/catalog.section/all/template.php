<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>    
<h1 class="menu_name"><?=$arResult["NAME"]?></h1>
<?if (!count($arResult["ITEMS"])):?>
<?endif;?>
<?if (count($arResult["ITEMS"])>0):?>
    <table id="dostavka_table" cellpadding="0" cellspacing="0" width="100%">        
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
        <?
        $cols = 1;
        if (!is_array($arElement["PREVIEW_PICTURE"]))
            $cols++;
        if (!trim($arElement["PREVIEW_TEXT"]))
            $cols++;
        if (!$arElement["PRICES"]["BASE"]["VALUE"]&&!$arElement["CAN_BUY"]&&!substr_count($_SERVER["HTTP_REFERER"],"dostavka"))
        {
            $preview = "end";
        }
        elseif ($arElement["PRICES"]["BASE"]["VALUE"]&&!$arElement["CAN_BUY"]&&!substr_count($_SERVER["HTTP_REFERER"],"dostavka"))
        {
            $price = "end";
        }
        else
        {
            $buy = "end";
        }
        ?>
		<tr class="<?=(end($arResult["ITEMS"])==$arElement)?"end":""?>">                    
                    <?/*if(is_array($arElement["PREVIEW_PICTURE"])):?>
                        <td>
                            <img <?=($arElement["PREVIEW_TEXT"])?'onclick="preview_modal('.$arElement["ID"].')"':''?> style="cursor:pointer" border="0" src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="80" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" />
                        </td>
                    <?endif;*/?>
                        <td>
                            <div class="name"><?=$arElement["~NAME"]?></div>
                            <?if (trim($arElement["PREVIEW_TEXT"])):?>                        
                                <Br /><i><?=$arElement["PREVIEW_TEXT"]?></i>
                            <?endif;?>
                        </td>                    
                    <?if ($arElement["PRICES"]["BASE"]["VALUE"]):?>
                        <td class="price <?=$price?>" width="100" align="right">
                            <?if (substr_count($_SERVER["HTTP_REFERER"],"dostavka")):?>
                                <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                        <?if($arPrice["CAN_ACCESS"]):?>
                                                <table width="100%" cellpadding="5">
                                                        <tbody>
                                                                <tr>
                                                                    <td align="left" width="40"><input class="inputtext" id="quantity<?=$arElement["ID"]?>" maxlength="3" type="text" value="1" size="1"></td>
                                                                    <td class="font16" width="20"> x </td>
                                                                    <td nowrap=""><?=$arPrice["VALUE"]?> <span class="rouble font24">e</span></td>
                                                                </tr>
                                                        </tbody>
                                                </table>							
                                        <?endif;?>
                                <?endforeach;?>				
                            <?else:?>
                                <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                    <?if($arPrice["CAN_ACCESS"]&&$arPrice["VALUE"]>0):?>
                                        <table width="100%" cellpadding="5">
                                            <tbody>
                                                <tr>
                                                    <?/*?>
                                                    <td align="left" width="10" class="font24">1</td>
                                                    <td class="font16" width="10"> x </td>
                                                    <?*/?>
<!--                                                    <td colspan="3" nowrap=""><?=$arPrice["VALUE"]?> <span class="rouble font16">e</span></td>-->
                                                    <?if ($arPrice["CURRENCY"]=="EUR"):?>
                                                        <td class="menu_price" colspan="3" nowrap="">€ <?=$arPrice["VALUE"]?></td>
                                                    <?else:?>
                                                        <td class="menu_price" colspan="3" nowrap=""><?=$arPrice["VALUE"]?> руб.</td>                                                        
                                                    <?endif;?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <?endif;?>
                                <?endforeach;?>
                            <?endif;?>
                        </td>
                    <?endif;?>
                    <?if($arElement["CAN_BUY"]&&substr_count($_SERVER["HTTP_REFERER"],"dostavka")):?>
                        <td class="<?=$buy?>">
                            <!--<input name="buy" class="button" type="button" value="<?= GetMessage("CATALOG_BUY") ?>" OnClick="window.location='<?echo CUtil::JSEscape($arElement["DETAIL_PAGE_URL"]."#buy")?>'" />-->
                            <input name="buy" class="button" type="button" value="<?=GetMessage("CATALOG_BUY")?>" onClick="add2basket_dostavka(this,'<?=$arElement["ID"]?>','<?=bitrix_sessid_get()?>')" />
                        </td>
                    <?endif?>
                </tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
    </table>
<?endif;?>
<div class="clear"></div>
<br />
<div align="right">
<?/*if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;*/?>
</div>