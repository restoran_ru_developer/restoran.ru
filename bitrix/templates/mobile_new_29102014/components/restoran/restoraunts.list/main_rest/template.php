<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arParams["aja"]!="Y"):?>
<div class="items rlist">
<?endif;?>    
<? if (count($arResult["ITEMS"]) > 0): ?>            
    <?
    foreach ($arResult["ITEMS"] as $cell => $arItem):  	                      
        $lat1 = (float) $arItem["PROPERTIES"]['lat']["VALUE"][count($arItem["PROPERTIES"]['lat']["VALUE"])-1];
        $lon1 = (float) $arItem["PROPERTIES"]['lon']["VALUE"][count($arItem["PROPERTIES"]['lon']["VALUE"])-1];
        $lat2 = (float) $_SESSION['lat'];
        $lon2 = (float) $_SESSION['lon'];

        if ($_REQUEST["pageRestSort"]=="distance")
            $dist = $arItem["DISTANCE"];
        else
            $dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
        if ($dist < 1000)
            $distance = round($dist,0) . ' м';
        else
            $distance = round($dist/1000,2).' км';

        $arIB = getArIblock("catalog", CITY_ID);
        $arItem['PROPERTIES']['address']['VALUE']['0'] = str_replace($arIB["NAME"].", ", "", $arItem['PROPERTIES']['address']['VALUE']['0']);
        $arItem['PROPERTIES']['address']['VALUE']['0'] = str_replace("г.", "", $arItem['PROPERTIES']['address']['VALUE']['0']);
        ?>
        <div class="item sl <?=($cell==0)?"first":""?>">
           <a class="ajax jj" href="/detail.php?ID=<?= $arItem["ID"] ?>">
            <div class="pic" style='background:url(<?=$arItem['PREVIEW_PICTURE']['src']?>) no-repeat;'>
                <div class="info">
                    
                        <div class='left'>
				<h2><?=(strlen($arItem['NAME'])>23?substr($arItem['NAME'],0,23).'...':$arItem['NAME']);?></h2>
	                        <h3><?=(strlen($arItem['PROPERTIES']['address']['VALUE']['0'])>30?substr($arItem['PROPERTIES']['address']['VALUE']['0'],0,30).'...':$arItem['PROPERTIES']['address']['VALUE']['0']);?></h3>
                                <?if(count($arItem['PROPERTIES']['address']['VALUE'])==1&&$arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']):?>
                                    <h3 class='subway'>м. 
                                        <?if (is_array($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'])):
                                            echo strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'][0]);
                                        else:
                                            echo strip_tags($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
                                        endif;?>                                      
                                    </h3>
				<?else:
                                    echo "<h3 class='subway'>(и еще ".(count($arItem['PROPERTIES']['address']['VALUE'])-1)." ".  pluralForm((count($arItem['PROPERTIES']['address']['VALUE'])-1), "адрес", "адреса", "адресов").")</h3>";
                                endif;?>
			</div>
			<div class='right'>
				<span class='time'><?= $distance ?></span>
                                <?if (is_array($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'])):
                                                preg_match_all("/\d+/", strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE'][0]),$r);            
                                            else:
                                                preg_match_all("/\d+/", strip_tags($arItem["DISPLAY_PROPERTIES"]['average_bill']['DISPLAY_VALUE']),$r);            
                                            endif;?>   
                                <?if (end($r)):?>
				<span class="average_bill <?=(CITY_ID=="rga"||CITY_ID=="urm")?"euro":""?>"><?=end($r[0])?></span>
                                <?endif;?>
			</div>    
                        
                </div>                                
            </div>
               </a>
            <div class="item_back">
                    <?if (CITY_ID=="spb"):?>
                        <a href='tel:+78127401820'>Позвонить</a>
                    <?elseif(CITY_ID=="msk"):?>
                        <a href='tel:+74959882656'>Позвонить</a>                               
                    <?endif;?>                     
                    <a class="ajax" href="/booking/?id=<?=$arItem['ID']?>">Забронировать столик</a>
                    <a class="" href="/reviews/?CITY_ID=<?=CITY_ID?>&id=<?=$arItem['ID']?>">Оставить отзыв</a>                
                </div> 
            <?if ($cell==0&&$arParams["main"]=="Y"):?>
                <div class="helpme"></div>
                <script>
                    $(function(){
                        var _this = $(".item").first();
                        var a = (_this.find(".item_back").width())*(-1);
                        _this.find(".pic").css("margin-left",a+"px");                        
                        _this.find(".item_back").show();
                        setTimeout("clo()",3000);
                        $(".helpme").css("right",(-1)*a+"px");
                        $(".helpme").css("margin-right",(-1)*$(".helpme").width()/2+"px");
                        $(".helpme").show();
                    });
                    function clo(){
                        var _this = $(".item").first();
                        var a = _this.find(".item_back").width();
                        _this.find(".pic").css("margin-left","0px");
                        $(".helpme").hide();
                    }
                </script>
            <?endif;?>
        </div>
    <?endforeach;?>        
<?else:?>
    <h3>К сожалению, ничего не найдено</h3>
<?endif;?>
<?if ($arParams["aja"]!="Y"):?>
</div>
<?endif;?>
<?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
<script>
    $(function(){       
          $('.item').unbind("movestart");
          $('.item').unbind("swipeleft");
          $('.item').unbind("swiperight");
          $('.item').on('movestart', function(e) {  
            if ((e.distX > e.distY && e.distX < -e.distY) ||
                (e.distX < e.distY && e.distX > -e.distY)) {
              e.preventDefault();              
            }
          });
        
        $(".item").on('swipeleft.click', function(e) {
            e.stopPropagation();
            var a = ($(this).find(".item_back").width())*(-1);
            $(this).find(".pic").css("margin-left",a+"px");            
//            if ($(this).hasClass("first"))
//            {                
//                $(this).find(".pic .left h2").css("transform","translate("+(-1)*a+"px)");
//            }
            $(this).find(".item_back").show();
            return false;
        });
         $(".jj").on('click', function(e) {
            return true;
        });
        $(".item").on('swiperight', function(e) {
            var a = $(this).find(".item_back").width();
            $(this).find(".pic").css("margin-left","0px");
        });        
    });
</script>
<?endif;?>