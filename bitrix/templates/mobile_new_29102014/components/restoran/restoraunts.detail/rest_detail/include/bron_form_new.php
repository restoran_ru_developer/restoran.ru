<div id="order_new">
    <div class="left">
        <? if (LANGUAGE_ID == "ru"): ?>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                Забронировать по телефону<br />
                <?if (CITY_ID=="msk"):?>
                    <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                <?else:?>
                    <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                <?endif;?>
            <?else:?>
                <?= $arResult["CONTACT_DESCRIPTION"] ?>                      
            <?endif;?>
        <? endif; ?>
        <? if (LANGUAGE_ID == "en"): ?>
        <? //var_dump($arResult["CONTACT_DESCRIPTION"]);
            if (CITY_ID=="spb"):
                $arResult["CONTACT_DESCRIPTION"] = '<p style="font-size:14px; line-height:14px;">Banquet organization and tables reservation +7 (812) 740-18-20</p> 
                <p style="font-size:14px; line-height:14px;">Make restaurant reservations - we speak English.</p>';
            elseif(CITY_ID=="msk"):
                $arResult["CONTACT_DESCRIPTION"] = '<p style="font-size:14px; line-height:14px;">Banquet organization and tables reservation +7 (495) 988 26 56</p> 
                <p style="font-size:14px; line-height:14px;">Make restaurant reservations - we speak English.</p>';
            endif;?>
            <? //echo str_replace('Организация банкетов и заказ столиков', 'Banquet organization and tables reservation.<br> Make restaurant reservations - we speak English.', $arResult["CONTACT_DESCRIPTION"]) ?> 
        <? echo $arResult["CONTACT_DESCRIPTION"]; ?> 
        <? endif; ?>
    </div>       
    <div class="left h"></div>
    <div class="right">        
        <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>                  
        <a href="javascript:void(0)" class="bron_button_gradient2" onclick="ajax_bron('<?=bitrix_sessid_get()?>&name=<?= rawurlencode($arResult["NAME"]) ?>&id=<?= $arResult["ID"] ?>')"><?=GetMessage("R_BOOK_TABLE2")?></a>            
                <a href="javascript:void(0)" class="bron_button_gradient1" onclick="ajax_bron2('<?=bitrix_sessid_get()?>&name=<?= rawurlencode($arResult["NAME"]) ?>&id=<?= $arResult["ID"] ?>',1)"><?=GetMessage("R_ORDER_BANKET2")?></a>            
        <?else:?>                                                                                                         
            <button class="bron_button_new" onclick="ajax_bron2('<?=bitrix_sessid_get()?>&name=<?= rawurlencode($arResult["NAME"]) ?>&id=<?= $arResult["ID"] ?>',1)"><?= GetMessage("R_BOOK") ?></button>                
        <?endif;?>
    </div>
    <div class="clear"></div>
</div>
<?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
<noindex>
    <div id="order_new_overflow">
        <div class="left" style="width:465px;padding-top: 10px;">
            <div class="left otitle">
                <?=$arResult["NAME"]?>
            </div>
            <div class="left orating" ></div>
            <script>
                $(document).ready(function(){
                    send_ajax("/tpl/ajax/get_ratio.php?ID=<?=$arResult["ID"]?>&<?=bitrix_sessid_get()?>", "html", false, oratio); 
                });    
                function oratio(data)
                {
                    $(".orating").html(data);
                }
            </script>
        </div>
        <div class="left ophone">
            <?if (LANGUAGE_ID=="en"):?>
                Make restaurant reservations<Br />
            <?else:?>
                Забронировать столик или банкет по телефону:<br />
            <?endif;?>                
                <?if (CITY_ID=="msk"):?>
                    <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                <?else:?>
                    <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                <?endif;?>
        </div>
        <div class="left h"></div>
        <div class="right">
            <?if ($_REQUEST["CATALOG_ID"]!="banket"):?>                  
            <a href="javascript:void(0)" class="bron_button_gradient2" onclick="ajax_bron('<?=bitrix_sessid_get()?>&name=<?= rawurlencode($arResult["NAME"]) ?>&id=<?= $arResult["ID"] ?>')"><?=GetMessage("R_BOOK_TABLE2")?></a>            
                    <a href="javascript:void(0)" class="bron_button_gradient1" onclick="ajax_bron2('<?=bitrix_sessid_get()?>&name=<?= rawurlencode($arResult["NAME"]) ?>&id=<?= $arResult["ID"] ?>',1)"><?=GetMessage("R_ORDER_BANKET2")?></a>            
            <?else:?>                                                                                                         
                <button class="bron_button_new" onclick="ajax_bron2('<?=bitrix_sessid_get()?>&name=<?= rawurlencode($arResult["NAME"]) ?>&id=<?= $arResult["ID"] ?>',1)"><?= GetMessage("R_BOOK") ?></button>                
            <?endif;?>
        </div>
        <div class="clear"></div>
    </div>
</noindex>
<?endif;?>
<script>
    $(document).ready(function(){
        $(window).scroll(function(){            
            if (($(this).scrollTop()>$("#minus_plus_buts").offset().top-100)&&$("#order_new_overflow").css("opacity")=="0")
            {
                $("#order_new_overflow").css({"top":"0px","opacity":1});
            }
            if ($(this).scrollTop()<$("#minus_plus_buts").offset().top-100&&$("#order_new_overflow").css("opacity")=="1")
            {
                $("#order_new_overflow").css({"top":"-80px","opacity":0});
            }
        });
    });
</script>