<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?


$lat1 = (float) $arResult["PROPERTIES"]['lat']["VALUE"][0];
$lon1 = (float) $arResult["PROPERTIES"]['lon']["VALUE"][0];
$lat2 = (float) $_SESSION['lat'];
$lon2 = (float) $_SESSION['lon'];
$dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
if ($dist < 1000) {
    $distance = $dist . ' м<br>';
} elseif (($dist > 1000 && $dist < 10000)) {
    $distance = substr($dist, 0, 1) . '.' . substr($dist, 1, 1) . ' км<br>';
} elseif (($dist > 10000 && $dist < 100000)) {
    $distance = substr($dist, 0, 2) . '.' . substr($dist, 2, 1) . ' км<br>';
}
?>
<div class="items-detail">
    <div class='head swiper-container' style="">
        <div class="swiper-wrapper">           
            <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>
                <?if ($key<5):?>
                    <div class="swiper-slide" >
                        <div class="inner" style="background-image: url(<?=$photo['src']?>)">
                            <img src="<?=$photo['src']?>" style="visibility:hidden; width:100%;" />
                        </div>
                    </div>
                <?else:?>
                    <div class="swiper-slide" >
                        <div class="inner ins" style="">
                            <img src="<?=$arResult["PROPERTIES"]["photos"]["VALUE2"][0]["src"]?>" style="visibility:hidden; width:100%;" />
                        </div>
                    </div>
                <?endif;?>
            <?endforeach?>
            <div class="pagination"></div>
        </div>
        <?if ($arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"][0]):?>
            <?
            preg_match_all("/\d+/", $arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"][0],$r);            
            ?>
            <div id='average_bill'><?=end($r[0])?>
                <div>средний счет</div>
            </div>
        <?endif;?>
    </div>

    <div class='body'>
        <div class="row">
<!--            <h2>Очаровательное место в центре города</h2>-->
            <?
//            if ($arResult["SECTION"]["PATH"][0]["CODE"] == 'restaurants') {
//                $what = "Ресторан";
//            } else {
//                $what = "Банкетный зал";
//            }
            ?>
            <p class='type'>
                <?//=$what?>                
                <?
                global $arIB;
                if (is_array($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE']))
                    echo str_replace($arIB["NAME"].", ","",$arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'][0])." (м.&nbsp;".$arResult['DISPLAY_PROPERTIES']['subway']['DISPLAY_VALUE'][0].")";
                else
                    echo str_replace($arIB["NAME"].", ","",$arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])." (м.&nbsp;".$arResult['DISPLAY_PROPERTIES']['subway']['DISPLAY_VALUE'][0].")";
                if (count($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])>1)
                {
                    echo " <a class='nowrap' href='javascript:void(0)' onclick=$('.more_addres').toggle()>(и еще ".(count($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])-1)." ".  pluralForm((count($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])-1), "адрес", "адреса", "адресов").")</a>";
                    echo "<p class='more_addres'>";
                    foreach ($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'] as $key=>$ad)
                    {
                        if (!$key)
                            continue;
                        echo str_replace($arIB["NAME"].", ","",$ad)." (м.&nbsp;".$arResult['DISPLAY_PROPERTIES']['subway']['DISPLAY_VALUE'][$key].")<br />";
                    }
                    echo "</p>";
                }?>
                <?if($arResult['PROPERTIES']['sleeping_rest']["VALUE"]=="Да"||$_REQUEST["CONTEXT"]=="Y"):?>
                    <p>
                                Телефон:
                            
                                <?if (CITY_ID=="spb"):?>
                                   8(812)7401820
                                <?elseif(CITY_ID=="msk"):?>
                                   8(495)9882656
                                <?endif;?>
                    </p>              
                <?else:?>
                    <?if($arResult['DISPLAY_PROPERTIES']['phone']['VALUE']):?>
                    <p>
                                Телефон
                       
                       
                                <?//echo implode(', ',$arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE']);?>
                                <?if (is_array($arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'])):
                                    echo $arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'][0];
                                   // if (count($arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'])>1)
                                     //   echo "<a class='nowrap' href='javascript:void(0)' onclick=$('.more_addres').toggle()>(и еще ".(count($arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'])-1)."</a>";?>
                                <?else:?>
                                    <?echo $arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'];?>
                                <?endif;?>
                    </p>
                    <?endif;?>
                <?endif;?>                
            </p>
            <p class='description'>
                <?/*if(!empty($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"])):
                    echo($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"][0]) .' зала';
                    if (!empty($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])||!empty($arResult["DISPLAY_PROPERTIES"]["kolichestvochelovek"]["DISPLAY_VALUE"])):
                        //echo ',';
                    endif;
                endif;*/?>

                <?/*if(!empty($arResult["DISPLAY_PROPERTIES"]["kolichestvochelovek"]["DISPLAY_VALUE"])):
                    echo($arResult["DISPLAY_PROPERTIES"]["kolichestvochelovek"]["DISPLAY_VALUE"][0]);
                    if (!empty($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):
                        echo ',';
                    endif;
                endif;*/?>
                                            
                <?                
                if (!empty($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):
                    ?>
                    <?
                    if (!is_array($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])) {
                        $arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"] = explode(", ", $arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]);
                    }
                    $arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"] = implode(', ',$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]);
                    echo  $arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"];
                    if (substr_count($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"], ",")>=1)
                        echo " кухни";
                    else
                        echo " кухня";
                    ?>

                    <?                    
                endif;
                ?>
            </p>
            <?if($arResult["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"]):?>
<!--                <div class="rating">
                    <img src="<?=$arResult['PREVIEW_PICTURE']['AVATAR']['src']?>" alt="">
                    <?if($arResult["DISPLAY_PROPERTIES"]["RATIO"]["DISPLAY_VALUE"]):?>
                        <p class="type">Оценка</p>
                        <ul class="stars_new">
                            <?for($z=1;$z<=round($arResult["DISPLAY_PROPERTIES"]["RATIO"]["DISPLAY_VALUE"]);$z++):?>
                                <li></li>
                            <?endfor?>
                        </ul>
                    <?endif?>
                    <p class="description"><?=$arResult["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"]?> отзывов                     
                    </p>
                </div>-->
                <div class="row reviews">
                    <h2><?=round($arResult["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"])?> <?=  pluralForm($arResult["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"], "отзыв", "отзыва", "отзывов")?> 
                    <ul class="stars_new_n">
                            <?for($z=1;$z<=5;$z++):?>
                                <li <?=(round($arResult["DISPLAY_PROPERTIES"]["RATIO"]["DISPLAY_VALUE"])>=$z)?"class='active'":""?>></li>
                            <?endfor?>
                        </ul>
                    
                    </h2>
                    <?
                        $arReviewsIB = getArIblock("reviews", CITY_ID);
                        global $arrFilter1;
                        $arrFilter1 = array();
                        $arrFilter1["PROPERTY_ELEMENT"] = $arResult["ID"];
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list", "one_reviews", Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "reviews",
                                "IBLOCK_ID" => $arReviewsIB["ID"],
                                "NEWS_COUNT" => "1",
                                "SORT_BY1" => "created_date",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arrFilter1",
                                "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PAGE_URL"),
                                "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "COMMENTS"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "N",
                                "CACHE_TIME" => "3600000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "URL" => $_REQUEST["url"],
                            ), true
                        );
                    ?>                    
                </div>
            <?endif;?>
        </div>
        <?if ($arResult["PROPERTIES"]["photos"]["VALUE2"][1]['src']):?>
            <div class='head' style='background-image:url("<?=$arResult["PROPERTIES"]["photos"]["VALUE2"][1]['src']?>");'></div>
        <?endif;?>
        <?if ($arResult['DETAIL_TEXT']):?>
            <?if (!$arResult["MENU_ID"]):?>
            <div class="row reviews">
                <h2>Описание</h2>
                <div class="text"><?=$arResult['DETAIL_TEXT']?></div>
                <a href="" class="link">Читать дальше</a>
            </div>
            <?endif;?>
            <?if ($arResult["MENU_ID"]):
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "menu_rest",
                    Array(
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "rest_menu_ru",
                            "IBLOCK_ID" => $arResult["MENU_ID"],
                            "NEWS_COUNT" => "5",
                            "SORT_BY1" => "tags",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "date_create",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFil",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array("ratio", "reviews"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "150",
                            "ACTIVE_DATE_FORMAT" => "j F Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                    ),
                false
                );
            ?>
            <span class="row menu_all">
                <a href="/menu/?ID=<?=$arResult["ID"]?>&all=Y&CITY_ID=<?=CITY_ID?>" class="link">Все меню</a>
            </span>
            <?
            endif;
            ?>
            <?if ($arResult["PROPERTIES"]["photos"]["VALUE2"][2]['src']):?>
                <div class='head' style='background-image:url("<?=$arResult["PROPERTIES"]["photos"]["VALUE2"][2]['src']?>");'></div>
            <?endif;?>
        <?endif;?>
        <div class="list">
            <?if($arResult['PROPERTIES']['type']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Тип
                    </div>
                    <div class="desc">
                        <?echo strip_tags(implode(', ',$arResult['DISPLAY_PROPERTIES']['type']['DISPLAY_VALUE']));?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['music']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Музыка
                    </div>
                    <div class="desc">
                        <?echo strip_tags(implode(', ',$arResult['DISPLAY_PROPERTIES']['music']['DISPLAY_VALUE']));?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['credit_cards']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Кредитные карты
                    </div>
                    <div class="desc">
                        <?echo strip_tags(implode(', ',$arResult['DISPLAY_PROPERTIES']['credit_cards']['DISPLAY_VALUE']));?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['parking']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Парковка
                    </div>
                    <div class="desc">
                        <?echo implode(', ',$arResult['DISPLAY_PROPERTIES']['parking']['DISPLAY_VALUE']);?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['children']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Детям
                    </div>
                    <div class="desc">
                        <?echo strip_tags(implode(', ',$arResult['DISPLAY_PROPERTIES']['children']['DISPLAY_VALUE']));?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['proposals']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Предложения
                    </div>
                    <div class="desc">
                        <?echo strip_tags(implode(', ',$arResult['DISPLAY_PROPERTIES']['proposals']['DISPLAY_VALUE']));?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['opening_hours']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Время работы
                    </div>
                    <div class="desc">
                        <?foreach($arResult['PROPERTIES']['opening_hours']['VALUE'] as $arHours):?>
                            <?=$arHours?>
                            <?break;?>
                        <?endforeach?>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['ideal_place_for']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Идеальное место для
                    </div>
                    <div class="desc">
                        <?echo strip_tags(implode(', ',$arResult['DISPLAY_PROPERTIES']['ideal_place_for']['DISPLAY_VALUE']));?>
                    </div>
                </div>
            <?endif;?>            
            <?if($arResult['PROPERTIES']['features']['VALUE']):?>
                <div class="list-item row">
                    <div class="title">
                        Особенности
                    </div>
                    <div class="desc">
                        <?echo implode(', ',$arResult['DISPLAY_PROPERTIES']['features']['DISPLAY_VALUE']);?>
                    </div>                    
                </div>
            <?endif;?>
            <?if($arResult['PROPERTIES']['sleeping_rest']["VALUE"]=="Да"||$_REQUEST["CONTEXT"]=="Y"):?>
                <div class="list-item row">
                        <div class="title">
                            Телефон
                        </div>
                        <div class="desc">
                            <?if (CITY_ID=="spb"):?>
                               8(812)7401820
                            <?elseif(CITY_ID=="msk"):?>
                               8(495)9882656
                            <?endif;?>
                        </div>                    
                </div>                
            <?else:?>
                <?if($arResult['DISPLAY_PROPERTIES']['phone']['VALUE']):?>
                    <div class="list-item row">
                        <div class="title">
                            Телефон
                        </div>
                        <div class="desc">
                            <?//echo implode(', ',$arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE']);?>
                            <?if (is_array($arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'])):
                                echo $arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'][0];
                               // if (count($arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'])>1)
                                 //   echo "<a class='nowrap' href='javascript:void(0)' onclick=$('.more_addres').toggle()>(и еще ".(count($arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'])-1)."</a>";?>
                            <?else:?>
                                <?echo $arResult['DISPLAY_PROPERTIES']['phone']['DISPLAY_VALUE'];?>
                            <?endif;?>
                        </div>                    
                    </div>
                <?endif;?>
            <?endif;?>
            <div class='cl'></div>
        </div>
        <div class='map'>
            <a href="/map/detail.php?l=<?=$arResult['PROPERTIES']['map']['VALUE'][0]?>&id=<?=$arResult["ID"]?>" class="ajax">
                <div class='address'>                                        
                    <address><?=(is_array($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE']))?str_replace($arIB["NAME"].", ","",$arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'][0]):str_replace($arIB["NAME"].", ","",$arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE']);?> 
                    <?
                    if (count($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])>1)
                        echo "(и еще ".(count($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])-1)." ".  pluralForm((count($arResult['DISPLAY_PROPERTIES']['address']['DISPLAY_VALUE'])-1), "адрес", "адреса", "адресов").")";?>
                    </address>
                    <name><?=(strlen($arResult["NAME"])>20)?substr($arResult["NAME"], 0,20)."...":$arResult["NAME"]?></name>
                    <p><?=$arResult['DISPLAY_PROPERTIES']['subway']['DISPLAY_VALUE'][0]?>, <?=$arResult['DISPLAY_PROPERTIES']['area']['DISPLAY_VALUE'][0]?></p>                    
                </div>
    <!--            <div id='map'></div>-->
                <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?=$arResult['PROPERTIES']['map']['VALUE'][0]?>&markers=icon:http://m14.restoran.ru<?=SITE_TEMPLATE_PATH?>/images/map_point.png%7Clabel:%7C<?=$arResult['PROPERTIES']['map']['VALUE'][0]?>&zoom=17&size=640x480&sensor=true" />
            </a>
        </div>            
    </div>
    <div class='rest-buttons'>
        <div>
            <?if (CITY_ID=="spb"):?>
                <a class='button' href='tel:+78127401820'>Позвонить</a>
            <?elseif(CITY_ID=="msk"):?>
                <a class='button' href='tel:+74959882656'>Позвонить</a>                               
            <?else:?>   
                <a class='button' ></a>                               
            <?endif;?>
            <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>
                <a class='button boo ajax' href='/booking/?id=<?=$arResult['ID']?>&CITY_ID=<?=CITY_ID?>'>Забронировать<Br>столик</a>
                <?endif;?>
        <a class='button' href='/reviews/?id=<?=$arResult['ID']?>&CITY_ID=<?=CITY_ID?>'>Оставить отзыв</a>
        </div>
    </div>
</div>
<script>        
    $(function(){       
         $.post("/ajax/ajax_image.php",{id:<?=  json_encode($arResult["PROPERTIES"]["photos"]["VALUE"])?>}, function( data ) {
             if (data)
             {
                for ( var i in data )
                {    
                    var a = parseInt(i);                    
                    //$(".ins:eq("+a+")").remove();
                    //console.log($(".swiper-slide").eq((i+5)).find(".inner"));
                    $(".ins:eq("+eval(a)+")").css("background-image","url("+data[i]+")");                    
                    //setTimeout('console.log($(".ins"))',100);
                    
                    //$(".swiper-slide").eq((i+5)).find(".inner").css("background-image","url("+data[i]+")");                    
                }
             }
         },'json');              
        
        $('.items-detail .body .reviews .link').click(function(){
            $('.items-detail .body .reviews div.text').css('overflow','auto').css('max-height','inherit');
            return false;
        });
        $('.review-body a.link').click(function(){
            $('.items-detail').addClass('blur');
            $('header').addClass('blur');
            $('.popup.review').show();
        });
//        $('.popup.review div.close').click(function(){
//            $('.items-detail').removeClass('blur');
//            $('header').removeClass('blur');
//            $('.popup.review').hide();
//        });   
        
        
//        if($(window).width()>'864')
//            $('.swiper-slide .inner').css('width', '864');
//        else {
            $('.swiper-slide .inner').css('width', $("main").width());
        //}
        var gallery = $('.swiper-container').swiper({
            slidesPerView:'auto',
            watchActiveIndex: true,
            //loop: true,
            //centeredSlides: true,
            pagination:'.pagination',
            //paginationClickable: true,
            resizeReInit: true,
            keyboardControl: true,
            grabCursor: true,
            onImagesReady: function(){
                changeSize()
            }
        })
        function changeSize() {
            //Unset Width
            $('.swiper-slide').css('width','')
            //Get Size
            var imgWidth = $('.swiper-slide img').width();
            //if (imgWidth+40>$(window).width()) imgWidth = $(window).width()-40;
            //Set Width
            //$('.swiper-slide').css('width', imgWidth+40);
            $('.swiper-slide').css('width', imgWidth);
        }

        changeSize()

        //Smart resize
        $(window).resize(function(){
            changeSize()
            gallery.resizeFix(true)
        });        
        $(".menu").unbind("click");
        $(".logo").html("<h1>"+$("name").html()+"</h1>").attr("href","javascript:void(0)").removeClass("ajax");
        $(".logo a").unbind("click");
//        $(".logo a").click(function(e){
//            e.preventDefault();
//            e.stopPropagation();
//            return false;
//        });
        $(".menu").after($(".menu").clone().removeClass("menu").addClass("arrow_r"));//.removeClass("menu").addClass("arrow_r")));                
        $(".arrow_r").find("img").attr("src",$(".arrow_r img").attr("src").replace("menu.png","arrow_r.png"));
        $(".menu").hide();
        if (!$(".arrow_r").is(":hidden"))
        {
            $(".logo").css("margin-left",$(".arrow_r").width()+$(".arrow_r").css("margin-left")*1+"px");
        }
        $(".logo").css("margin-top","2px");
        $(".right_btn").attr("href","/map/detail.php?l=<?=$arResult['PROPERTIES']['map']['VALUE'][0]?>&id=<?=$arResult["ID"]?>");
        //$(".menu").addClass("arrow_r").removeClass("menu");
        $(".arrow_r").click(function(){
            //console.log(location);
            if (history.length>2)
                history.go(-1);
            else
                location.href='/';
        });
        $(".myhead .right_btn").show();
        
    });    
</script>

<? //v_dump($arResult["PROPERTIES"]["photos"]["VALUE2"])          ?>