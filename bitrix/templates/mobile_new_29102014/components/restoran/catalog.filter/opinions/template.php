<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<style>
    .ui-btn-inner {
        border: none;
    }
    .ui-controlgroup-horizontal .ui-btn.ui-last-child {
        border:0;
    }
</style>
<div class="filter-page">
    <div class="new font18 center">Фильтр отзывов</div>
    <Br />
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" data-ajax="false" action="/opinions.php" method="GET">    
    <div class="question">
        <div align="left">Город</div>
        <select data-theme="d" name="CITY_ID" data-mini="true">
            <option <?=(CITY_ID=="msk")?"selected":""?> value="msk">Москва</option>
            <option <?=(CITY_ID=="spb")?"selected":""?> value="spb">Санкт-Петербург</option>            
        </select>    
    </div>
    <br />
    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" >
        <input type="radio" data-theme="c" name="photos" id="photosa" value="N" <? if(!$_REQUEST['photos'] || $_REQUEST['photos'] == "N") { echo 'checked="checked"';}?>>
        <label for="photosa">Все отзывы</label>
        <input type="radio" data-theme="c" name="photos" id="photosb" value="Y" <? if($_REQUEST['photos'] == "Y") { echo 'checked="checked"';}?>>
        <label for="photosb">Только с фото</label>                  
    </fieldset>
    <?
    foreach($arResult["arrProp"] as $prop_id => $arProp)
    {
            $res = "";
            $arResult["arrInputNames"][$FILTER_NAME."_pf"]=true;
            if ($arProp["CODE"]!="wi_fi"&&$arProp["CODE"]!='subway_dostavka'&&$arProp["CODE"]!="hrs_24")
            {
                if ($arProp["PROPERTY_TYPE"]=="L" || $arProp["PROPERTY_TYPE"]=="E")
                {
                    $name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
                            $value = $arrPFV[$arProp["CODE"]];
                            $res = "";
                            if ($arProp["MULTIPLE"]=="Y")
                            {
                                    if (is_array($value) && count($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
                            else
                            {
                                    if (!is_array($value) && strlen($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
            ?>
  
                            <div class="question">
                                <div align="left"><?=$arProp["NAME"]?></div>
                            <?if ($arProp["CODE"]=="subway"):?>
                                <select  data-mini="true" data-theme="d" multiple name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>                                    
                                    <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val["NAME"]?></option>
                                    <? endforeach; ?>
                                </select>
                            <?else:?>
                                <select  data-mini="true" data-theme="d" multiple name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>                                    
                                        <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val?></option>
                                    <? endforeach; ?>
                                </select>
                            <?endif;?>
                            </div>
                            
            <?             
                    
                }
            }
    }
        ?>
        <br />
        <input data-theme="b" data-mini="true" class="light_button" type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />    
        <input type="hidden" name="set_filter" value="Y" />          
</form>
</div>
