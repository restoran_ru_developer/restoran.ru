<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<link rel="stylesheet" href="<?=$templateFolder?>/style.css" />

<div class="filter-page">
    <?if ($arParams["NO_NAME"]!="Y"):?>
        <div class="new font18 center">Подобрать ресторан</div>
        <Br />
    <?endif;?>
    <?if ($APPLICATION->get_cookie("COORDINATES")=="Y"):?>
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" data-ajax="true" action="/index.php" method="GET">
    <?else:?>
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" data-ajax="true" action="/popular/" method="GET">
    <?endif;?>
    <input type="hidden" name="page" value="1" />
      
    <input type="hidden" name="by" value="asc" />
<script>
    $(function(){
        $('select').mobiscroll().select({
            theme: 'mobiscroll',
            display: 'bottom',
            animate: 'none',
            mode: 'scroller',
            minWidth: 200,
            rows: 7
//            onSelect: function(a,b,c){
//                console.log(this);
//                console.log(b.values[0]);
//                if (b.values[0])
//                {
//                    
//                }               
//            }
        });

        $('#show').click(function () {
            $('#demo').mobiscroll('show');
            return false;
        });

        $('#clear').click(function () {
            $('select').mobiscroll('clear');
            $('.popup.filter').hide();
            $("#overflow").hide();
            //$('#demo').val(1).change();
            return false;
        });
        $('#clear2').click(function () {
            $('select').mobiscroll('clear');
            $(this).parents("form").submit();
            //$('#demo').val(1).change();
            return false;
        });
        $(".filter *").click(function(e){
            e.stopPropagation();
        });
        
    });
</script>
    <?
    foreach($arResult["arrProp"] as $prop_id => $arProp)
    {
        $res = "";
        $arResult["arrInputNames"][$FILTER_NAME."_pf"]=true;
        if ($arProp["CODE"]!="wi_fi"&&$arProp["CODE"]!='subway_dostavka'&&$arProp["CODE"]!="hrs_24")
        {
            if ($arProp["PROPERTY_TYPE"]=="L" || $arProp["PROPERTY_TYPE"]=="E")
            {
                $name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
                $value = $arrPFV[$arProp["CODE"]];
                $res = "";
                if ($arProp["MULTIPLE"]=="Y")
                {
                        if (is_array($value) && count($value) > 0)
                                ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                }
                else
                {
                        if (!is_array($value) && strlen($value) > 0)
                                ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                }
                ?>

                <div class="question">
                    <label for='<?=$arProp["CODE"]?>'><?=$arProp["NAME"]?></label>
                    <div class="input">
                        <?if ($arProp["CODE"]=="subway"):?>
                            <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" data-theme="d" multiple="multiple" id="<?=$arProp["CODE"]?>" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val["NAME"]?></option>
                                <? endforeach; ?>
                            </select>
                        <?else:?>
                            <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" multiple="multiple" data-theme="d" id="<?=$arProp["CODE"]?>" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                    <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val?></option>
                                <? endforeach; ?>
                            </select>
                        <?endif;?>
                        <input type="hidden" id="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>_flag" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>_flag" value="0" />
                    </div>
                </div>

        <?

            }
        }
    }
        ?>    
    <div class="line2"></div>
    <?
    $mskSelected = '';
    $spbSelected = '';
    if (isset($_REQUEST['CITY_ID'])){
        if ($_REQUEST['CITY_ID'] == 'msk'){
            $mskSelected = 'selected="selected"';
            $spbSelected = '';
        } else {
            $mskSelected = '';
            $spbSelected = 'selected="selected"';
        }
    } else {
        if (CITY_ID == 'msk'){
            $mskSelected = 'selected="selected"';
            $spbSelected = '';
        } else {
            $mskSelected = '';
            $spbSelected = 'selected="selected"';
        }
    }
    ?>    
        <?if ($_REQUEST["arrFilter_pf"]):?>
            <input class="light_button" type="submit" name="set_filter" value="Применить фильтры" style="float:left;width:50%;" />    
            <input id="clear2" class="light_button" type="button"  name="set_filter" value="Сбросить" style="float:right;width:50%;" />
            <duv class="cl"></div>
        <?else:?>
            <input class="light_button" type="submit" name="set_filter" value="Применить фильтры" style="float:left;width:50%;" />    
            <input id="clear" class="light_button" type="button"  name="set_filter" value="Сбросить" style="float:right;width:50%;" />
            <duv class="cl"></div>
        <?endif;?>
        <input type="hidden" name="set_filter" value="Y" />          
</form>
</div>
