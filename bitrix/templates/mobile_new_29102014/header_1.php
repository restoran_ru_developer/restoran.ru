<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width; target-densityDpi=device-dpi; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="HandheldFriendly" content="true"/>
        <title><?$APPLICATION->ShowTitle();?></title>
        <meta name="mobile-web-app-capable" content="yes">
        
        
        
        <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/images/touch/favicon.ico" type="image/x-icon" />
	<!-- Apple Touch Icons -->
	<link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-152x152.png" />
	<!-- Windows 8 Tile Icons -->
    	<meta name="msapplication-square70x70logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/smalltile.png" />
	<meta name="msapplication-square150x150logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/mediumtile.png" />
	<meta name="msapplication-wide310x150logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/widetile.png" />
	<meta name="msapplication-square310x310logo" content="<?=SITE_TEMPLATE_PATH?>/images/touch/largetile.png" />
        
        <!-- Chrome Add to Homescreen -->
        <link rel="shortcut icon" sizes="196x196" href="<?=SITE_TEMPLATE_PATH?>/images/touch/apple-touch-icon-152x152.png">

        <!-- For iOS web apps. Delete if not needed. https://github.com/h5bp/mobile-boilerplate/issues/94 -->
        <!--
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="Restoran.ru">
        -->

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>        
        <!-- build:css styles/components/main.min.css -->
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/h5bp.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/components/components.css');?>

        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/main.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/ratio.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/swiper.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.animation.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.icons.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.scroller.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/mobiscroll/mobiscroll.widget.css');?>


        <?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/hover.css');?>


        <?$APPLICATION->ShowHead();?>
	
        <!--<script src="<?=SITE_TEMPLATE_PATH?>/scripts/main.js?123134123412341231231"></script>-->
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/touch.js?12"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.zepto.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.core.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.scroller.js" type="text/javascript"></script>

        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.select.js" type="text/javascript"></script>

<!--        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.android.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.android-holo.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.ios.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.ios7.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.jqm.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.sense-ui.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/mobiscroll.widget.wp.js" type="text/javascript"></script>-->
        <script src="<?=SITE_TEMPLATE_PATH?>/scripts/markerclusterer.js"></script>
        <!-- endbuild -->
    </head>
    <body>          
        <?require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH . "/functions.php");?>
        <?if ($USER->GetID()!=1):?>
            <div class='panel'><?$APPLICATION->ShowPanel();?></div>
            <?endif;?>
            <header class="app-bar promote-layer">
                    <div class="app-bar-container myhead">
                            <button class="menu"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu.png"></button>
                            <a href="/" class="ajax logo"><img src='<?=SITE_TEMPLATE_PATH?>/images/logo.png'></a>
                            <section class="app-bar-actions">
                                <a href="/map/" class="right_btn ajax">Карта</a>
                            </section>
                    </div>
            </header>

            <nav class="navdrawer-container promote-layer">
<!--                    <header class="app-bar promote-layer">
                            <div class="app-bar-container">
                                <?if ($USER->IsAuthorized()):?>
                                <?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "auth", Array(

                                    ),
                                    false
                                );?>
                                <?else:?>
                                <section class="app-bar-actions">
                                            <a href="/auth/"  class="right_btn">Авторизация</a>
                                    </section>
                                <?endif;?>
                                    <a href='#' ><img src='<?=SITE_TEMPLATE_PATH?>/images/user.png'> <span>Sergey Kazakov</span></a>
                                    <section class="app-bar-actions">
                                            <a href="#"  class="right_btn">Выход</a>
                                    </section>
                            </div>
                    </header>-->
                    
                    <ul>                        
                            <li><a class="ajax" href="/search/">Поиск ресторана</a></li>
                            <li><a class="ajax" id="popular_link" href="/popular/">Популярное</a></li>
                            <li><a class="ajax" href="/">Рядом со мной</a></li>
                            <?if ((CITY_ID=="spb"||CITY_ID=="msk")):?>
                                <li><a class="ajax" href="/booking/">Бронировать</a></li>                            
                            <?endif;?>
                            <?if ($USER->IsAuthorized()&&(CITY_ID=="spb"||CITY_ID=="msk")):?>
                                <li><a href="/bookinghistory/">Мои бронирования</a></li>                            
                            <?endif;?>
                            <?if (CITY_ID=="spb"):?>
                                <li><a href="tel:+78127401820">Позвонить в Ресторан.ру</a></li>
                            <?elseif(CITY_ID=="msk"):?>
                                <li><a href="tel:+74959882656">Позвонить в Ресторан.ру</a></li>                            
                            <?endif;?>
                            <li><a   style="margin-top:10px; width:70%" href="/city_select">Выбрать город</a></li>                            
                            <li><a   style="margin-top:10px; width:70%" href="http://www.restoran.ru/?from_mobile=Y">Перейти на сайт</a></li>                            
                    </ul>                
                    <?include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/classes/Mobile_Detect.php");
                    $detect = new Mobile_Detect();
                    //if ($detect->isTablet()):?>
<!--                        <a style="position:absolute; bottom:30px; width:70%; font-size:24px; color:#666; max-width: 200px; left:45px; " href="http://www.restoran.ru/?from_mobile=Y">Перейти на сайт</a>-->
                    <?//endif;?>
            </nav>
            <div id="mm">
                <ul>
                        <li><a class="ajax" href="/search/">Поиск ресторана</a></li>
                        <li><a class="ajax" href="/popular/">Популярное</a></li>
                        <li><a class="ajax" href="/">Рядом со мной</a></li>
                        <li><a class="ajax" href="/booking/">Бронировать</a></li>                            
                        <li><a href="/bookinghistory/">Мои бронирования</a></li>                                                    
                </ul>
            </div>
            
            <main id="main"
                <?if($APPLICATION->GetCurDir()=="/map/"):
                    echo "class='map' id='map'";
                elseif(trim($APPLICATION->GetCurDir())=="/auth/"):
                    echo "class='grad'";
                endif;?>>
                <?if ($_REQUEST["ajax"]):?>
                    <div id="ajax">
                <? endif; ?>

