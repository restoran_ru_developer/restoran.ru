<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <? $APPLICATION->ShowHead() ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/detail_galery.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/script.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/maskedinput.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/tools.js') ?>

        <? $APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/cusel.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/styles.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js') ?>            
        <? $APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js') ?>             
        <? $APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js') ?>  
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.system_message.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/cusel-min.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/radio.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js') ?>
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
            <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
                <?
                CModule::IncludeModule("advertising");
                CAdvBanner::SetRequiredKeywords(array(CITY_ID));
                ?>
                </head>
                <body style="background:#FFF">
                    <div id="panel"><? $APPLICATION->ShowPanel() ?></div>
                    <h1 style="position: absolute;z-index: -1;"><?=$APPLICATION->ShowTitle("h1")?></h1>
                    <div id="container" style="background: #0d1c03 url(/tpl/images/spec/football/football_head.jpg) center top no-repeat">
                        <div id="wrapp">    
                            <div class="baner" style="height:90px; background: none; border-radius: 15px; width:980px; margin:0 auto; padding-top:20px;">                
                                <?$APPLICATION->IncludeComponent(
                                         "bitrix:advertising.banner",
                                         "",
                                         Array(
                                                 "TYPE" => "top_main_page",
                                                 "NOINDEX" => "Y",
                                                 "CACHE_TYPE" => "A",
                                                 "CACHE_TIME" => "600"
                                         ),
                                 false
                                 );?>
                            </div>
                            <div id="content" style="padding-top: 226px;">         
                                <a id="new_year_night_logo" href="/<?=CITY_ID?>/articles/sport/"></a>
                                <?/*if (CITY_ID!="tmn"):?>
                                <a id="new_year_back" href="/<?=CITY_ID?>/articles/new_year/"></a>
                                <?endif;*/?>
                                <a id="new_year_logo" href="/"></a>
                                <div id="football_date"><img src="/tpl/images/spec/football/football_date.png" /></div>