<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach ($arResult["ITEMS"] as &$arItem):
    $elems = array();
    foreach ($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"] as $el)
    {        
        $res = CIBlockElement::GetByID($el);
        if ($ar = $res->GetNext())
        {
            if ($ar["PREVIEW_PICTURE"])
            {
                $elems[] = Array(
                    "ID" => $ar["ID"],
                    "NAME" => $ar["NAME"],
                    "DETAIL_PAGE_URL" => $ar["DETAIL_PAGE_URL"],
                    "IMAGE" => CFile::ResizeImageGet($ar["PREVIEW_PICTURE"], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_EXACT, true),                
                );
            }
            else
            {
                $elems[] = Array(
                    "ID" => $ar["ID"],
                    "NAME" => $ar["NAME"],
                    "DETAIL_PAGE_URL" => $ar["DETAIL_PAGE_URL"],
                    "IMAGE" => CFile::ResizeImageGet($ar["DETAIL_PICTURE"], array('width' => 232, 'height' => 232), BX_RESIZE_IMAGE_EXACT, true),                
                );
            }
            //v_dump($elems);
        }        
    }    
    $arResult["ELEMENTS"] = $elems;
endforeach; ?>
