<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<noindex>
    <form action="/<?= CITY_ID ?>/articles/new_year_night/search/" method="get" name="rest_filter_form">
        <div class="left">
            <div class="appetite" style="width: 190px; float: left; color: white; text-align: left; margin-left: 10px; font-size: 14px; line-height: 36px;"><?= GetMessage("BY_NAME") ?></div>
            <div style="position: relative;">
                <? if (CITY_ID == "spb"): ?>
                    <input id="search_input" x-webkit-speech="" speech="" class="placeholder" type="text" name="q" value="<?= ($_REQUEST['q'] ? $_REQUEST['q'] : "") ?>" placeholder="<?= GetMessage("BSF_T_SEARCH_EXMP_STRING_spb") ?>" onspeechchange="startSearch" />
                <? else: ?>
                    <input id="search_input" x-webkit-speech="" speech="" class="placeholder" type="text" name="q" value="<?= ($_REQUEST['q'] ? $_REQUEST['q'] : "") ?>" placeholder="<?= GetMessage("BSF_T_SEARCH_EXMP_STRING") ?>" onspeechchange="startSearch" />
                <? endif; ?>       
                <input id="search_submit_new" type="submit" value=""/>
            </div>
            <div class="clear"></div>
        </div>            
        <div class="clear"></div>
    </form>
</noindex>