<?php
if (($_REQUEST["apply"]=="Применить"||$_REQUEST["save"]=="Сохранить"))
{
    $res = CIBlock::GetByID((int)$_REQUEST["IBLOCK_ID"]);
    if($ar_res = $res->GetNext())
        $iblock_code = $ar_res['CODE'];
    //Берем ID свойств спецпроекта

    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"THIS_RESTAURANT"));
    if($prop_fields = $properties->GetNext())
    {
        $property_restoran_id = $prop_fields["ID"];
        $property_restoran_iblock_id = $prop_fields["LINK_IBLOCK_ID"];
    }

//    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"phone"));
//    if($prop_fields = $properties->GetNext())
//    {
//        $property_phone_id = $prop_fields["ID"];
//    }

    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"subway"));
    if($prop_fields = $properties->GetNext())
    {
        $property_subway_id = $prop_fields["ID"];
    }

    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"average_bill"));
    if($prop_fields = $properties->GetNext())
    {
        $property_bill_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"area"));
    if($prop_fields = $properties->GetNext())
    {
        $property_area_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"kolichestvochelovek"));
    if($prop_fields = $properties->GetNext())
    {
        $property_person_num_id = $prop_fields["ID"];
    }


    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"kitchen"));
    if($prop_fields = $properties->GetNext())
    {
        $property_kitchen_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"type"));
    if($prop_fields = $properties->GetNext())
    {
        $property_type_id = $prop_fields["ID"];
    }

    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"features"));
    if($prop_fields = $properties->GetNext())
    {
        $property_features_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"address"));
    if($prop_fields = $properties->GetNext())
    {
        $property_address_id = $prop_fields["ID"];
    }
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(int)$_REQUEST["IBLOCK_ID"], "CODE"=>"phone"));
    if($prop_fields = $properties->GetNext())
    {
        $property_phone_id = $prop_fields["ID"];
    }

    if ($property_restoran_id&&$property_restoran_iblock_id)
    {
        foreach($_POST["PROP"][$property_restoran_id] as $rest_vals)
        {
            if ($rest_vals)
            {
                if (is_array($rest_vals)&&count($rest_vals["VALUE"])>0)
                {
                    $rest_id = $rest_vals["VALUE"];
                }
                else
                    $rest_id = $rest_vals;
            }

        }
        if ($rest_id)
        {

            if ($property_subway_id)
            {
                //Забираем у ресторана метро
                $subway = array();
                $subway_name = array();
                $subway[] = "";
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "subway"));
                while ($ob = $res->GetNext())
                {
                    $subway[] = $ob['VALUE'];
                    $r = CIBlockElement::GetByID($ob['VALUE']);
                    if ($ar = $r->Fetch())
                    {
                        $subway_name[] = $ar["NAME"]." [".$ar["ID"]."]";
                    }
                }
                $_POST["PROP"][$property_subway_id] = $subway;
                $_REQUEST["PROP"][$property_subway_id] = $subway;
                $_POST["visual_PROPx".$property_subway_id."x"] = implode("\n\r",$subway_name);
                $_REQUEST["visual_PROPx".$property_subway_id."x"] = implode("\n\r",$subway_name);
            }
            if ($property_bill_id && !substr_count($iblock_code, "new_year"))
            {
                //Забираем у ресторана средний счет
                $bill = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "average_bill"));
                while ($ob = $res->GetNext())
                {
                    $bill[] = $ob['VALUE'];
                }
                $_POST["PROP"][(int)$property_bill_id] = $bill;
            }
            if ($property_area_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "area"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_area_id] = $area;
            }
            if ($property_person_num_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "kolichestvochelovek"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_person_num_id] = $area;
            }





            if ($property_kitchen_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "kitchen"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_kitchen_id] = $area;
            }
            if ($property_type_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "type"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_type_id] = $area;
            }

            if ($property_features_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "features"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_features_id] = $area;
            }
            if ($property_address_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "address"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_address_id] = $area;
            }

            if ($property_phone_id)
            {
                //Забираем у ресторана район
                $area = array();
                $res = CIBlockElement::GetProperty($property_restoran_iblock_id, $rest_id, "sort", "asc", array("CODE" => "phone"));
                while ($ob = $res->GetNext())
                {
                    $area[] = $ob['VALUE'];
                }
                $_POST["PROP"][$property_phone_id] = $area;
            }
//
        }
    }
    $PROP = $_POST["PROP"];
}
?>
