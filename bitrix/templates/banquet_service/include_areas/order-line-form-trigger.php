<div class="reserve-block-wrapper filter-box">
    <form action="" method="post" id="reserve-form">
        <div class="one-reserve-filter-wrapper">
            <label for="datepicker">выберите дату</label>
            <div class="field-wrapper datepicker-wrapper">
                <input type="text" id="datepicker" class="reserve-calendar-input" value="">
                <span class="calendar-trigger"></span>
            </div>
        </div>
        <div class="one-reserve-filter-wrapper">
            <label for="datepicker">кол-во гостей</label>
            <div class="field-wrapper">
                <input type="text" class="reserve-guest-num-input" name="reserve_guest_num" value="">
                <div class="more-less-input-trigger">
                    <div class="more-button"></div>
                    <div class="less-button"></div>
                </div>
            </div>
        </div>

        <div class="one-reserve-filter-wrapper">
            <label for="datepicker">бюджет р./чел.</label>
            <div class="field-wrapper">
                <input type="text" class="reserve-budget-input" value="" name="budget_for_person">
            </div>
        </div>

        <div class="one-reserve-filter-wrapper">
            <label for="reserve-region"><a class="region-select-trigger" href="javascript:void(0)">Район</a> / <a class="subway-select-trigger" href="javascript:void(0)">станция Метро</a></label>
            <div class="field-wrapper">
                <div class="reserve-region-field-wrapper">
<!--                                            <input type="hidden" value="" name="reserve_region">-->

                        <div class="form-group region-metro-reservation-wrapper">
                            <div class="multiselect-region-wrapper">
                                <select  class="multiselect-region form-control" name="reserve_region" style="display: none">
                                    <option value="0" selected>Любой</option>
                                    <?
                                    $obCache = new CPageCache;
                                    $life_time = 30*24*60*60;
                                    $cache_id = 'area-banquet-reserve-form-'.CITY_ID.'select';
                                    if($obCache->StartDataCache($life_time, $cache_id)):
                                        $arIB = getArIblock("area", CITY_ID);

                                        $arSelect = Array('ID','NAME');
                                        $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y");
                                        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                                        while($ob = $res->GetNextElement())
                                        {
                                            $obj_field = $ob->GetFields();?>
                                            <option value="<?=$obj_field['ID']?>" ><?=$obj_field["NAME"]?></option>
                                        <?}

                                        $obCache->EndDataCache();
                                    endif;
                                    ?>
                                </select>
                                <script type="text/javascript">
                                    $(function() {
                                        $('.multiselect-region').multiselect({
                                            numberDisplayed:1,
                                            nonSelectedText: 'Любой',
                                            nSelectedText: "Выбрано",
                                            maxHeight: 300,
                                            buttonWidth: "174px"
                                        });


                                    });
                                </script>
                            </div>
                            <div class="multiselect-subway-wrapper" style="display: none;">
                                <select  class="multiselect-subway form-control" name="reserve_region" style="display: none">
                                    <option value="0" selected>Любая</option>
                                    <?
                                    $obCache = new CPageCache;
                                    $life_time = 30*24*60*60;
                                    $cache_id = 'metro-banquet-reserve-form-'.CITY_ID.'select';
                                    if($obCache->StartDataCache($life_time, $cache_id)):
                                        $arIB = getArIblock("metro", CITY_ID);

                                        $arSelect = Array('ID','NAME');
                                        $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y");
                                        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                                        while($ob = $res->GetNextElement())
                                        {
                                            $obj_field = $ob->GetFields();?>
                                            <option value="<?=$obj_field['ID']?>" ><?=$obj_field["NAME"]?></option>
                                        <?}

                                        $obCache->EndDataCache();
                                    endif;
                                    ?>
                                </select>
                                <script type="text/javascript">
                                    $(function() {
                                        $('.multiselect-subway').multiselect({
                                            numberDisplayed:1,
                                            nonSelectedText: 'Любая',
                                            nSelectedText: "Выбрано",
                                            maxHeight: 300,
                                            buttonWidth: "174px"
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="one-reserve-filter-wrapper">
            <label for="reserve-region">повод</label>
            <div class="field-wrapper">
                <div class="reserve-region-field-wrapper">
<!--                    <input type="hidden" value="" name="reserve_reason">-->

                        <div class="form-group region-metro-reservation-wrapper">
                            <select  class="multiselect-reason form-control" name="reserve_reason" style="display: none">
                                <option value="0" selected>Любой</option>
                                <option value="1" >Новогодний корпоратив</option>
                                <option value="2" >Свадьба</option>
                                <option value="3" >День рождения</option>
                                <option value="4" >Юбилей</option>
                                <option value="5" >Детский праздник</option>
                            </select>
                            <script type="text/javascript">
                                $(function() {
                                    $('.multiselect-reason').multiselect({
                                        numberDisplayed:1,
                                        nonSelectedText: 'Любой',
                                        nSelectedText: "Выбрано",
                                        maxHeight: 300,
                                        buttonWidth: "174px"
                                    });
                                });
                            </script>
                        </div>
                </div>
            </div>
        </div>

        <div class="one-reserve-filter-wrapper reserve-submit-wrapper">
            <label for="reserve-submit">Контактные данные</label>
            <div class="field-wrapper">
                <!--                                <input type="submit" id="reserve-submit" value="далее" >-->
                <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking reserve-submit-link">далее</a>
            </div>
        </div>
    </form>
</div>