<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<?$CUR_DIR = $APPLICATION->GetCurDir();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Restoran.ru">
    <meta property="fb:app_id" content="297181676964377" />

    <meta property="fb:admins" content="100004709941783" />
    <?if (!$_REQUEST["CODE"]):?>
        <meta property="og:image" content="https://www.restoran.ru/images/logo.png" />
    <?endif;?>
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/multiselect.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>



    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-multiselect.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script-new.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.ru.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/index.js')?>


    <?$APPLICATION->ShowHead()?>
    <?

    $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);

    CModule::IncludeModule("advertising");
    if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
        $page = "others";
    else
        $page = "main";
    CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));

    ?>

    <link href="<?=SITE_TEMPLATE_PATH?>/css/banquet.css"  type="text/css" rel="stylesheet" />


</head>
<body class="banquet-section <?if(preg_match('/selections/',$CUR_DIR)){echo 'inner-section-page';}?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=297181676964377&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div id="panel"><?$APPLICATION->ShowPanel()?></div>

<div class="container">

<div class="banquet-section-header">
<div class="wood-top-block-wrapper">
    <div class="center-block">

        <div class="right-phone-block">
            <div class="phone-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_TEMPLATE_PATH."/include_areas/banquet-top-right-phone-".CITY_ID.".php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>

            </div>
            <div class="reserve-wrapper-button"><a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking"><span>бронировать</span> бесплатно</a></div>
        </div>
        <div class="left-small-menu">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "top-header-left-menu", Array(
                    "ROOT_MENU_TYPE" => "banquet_service_menu",	// Тип меню для первого уровня
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                ),
                false
            );?>
        </div>

        <div class="main-center-section-banner-wrapper">
            <div class="city-list-wrapper">
                <div class="right-city-line"></div>
                <div class="left-city-line"></div>

                <div id="city_select">
                    <?$APPLICATION->IncludeComponent("restoran:city.selector", "banquet_city_select", array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_URL" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600000000",
                            "CACHE_GROUPS" => "Y"
                        ),
                        false
                    );?>
                </div>
            </div>

            <div class="main-center-section-banner">
                <a href="<?if(!preg_match('/selections/',$CUR_DIR)):?>/<?=CITY_ID?>/<?else:?>/<?=CITY_ID?>/banquet-service/<?endif;?>">
                    <span>Банкетная</span>
                    <br>
                    служба
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/little-logo-restoran.png" width="103" height="46" alt="logo">
                </a>

                <div class="left-flag flags"><a href="/<?=CITY_ID?>/catalog/bankets/all/">Банкетные залы</a></div>
                <div class="right-flag flags"><a href="/<?=CITY_ID?>/catalog/restaurants/all/">каталог ресторанов</a></div>
            </div>
        </div>

        <?if(preg_match('/selections/',$CUR_DIR)):?>
            <div class="filter-title">
                <h1><?$APPLICATION->ShowTitle(false)?></h1>
            </div>
            <div class="filter-sub-title">
                <strong>
                <?
                $APPLICATION->ShowViewContent('section_desc');
                if(!empty($_GET['arrFilter_pf'])){
                    $arIB = getArIblock("selection_of_restaurants", CITY_ID);
                    $arSelect = Array("PREVIEW_TEXT");
                    $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], 'CODE'=>'LIST_DESC');
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        echo $arFields['PREVIEW_TEXT'];
                    }
                }
                ?>
                </strong>
            </div>
        <?endif?>

    </div>
</div>

<?if(!preg_match('/selections/',$CUR_DIR)):?>

    <div class="under-wood-top-block-wrapper">
        <div class="center-block">
            <div class="filter-title">
                Найдем для вас ресторан мечты
            </div>
            <div class="filter-sub-title">Совершенно бесплатно!</div>

            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_TEMPLATE_PATH."/include_areas/order-line-form-trigger.php",
                    "EDIT_TEMPLATE" => ""
                ),
                false
            );?>


        </div>
    </div>

    <div class="blue-top-main-thought-categories-menu-wrapper">
        <div class="center-block">
            <div class="one-thought-wrapper">
                <div class="this-title">быстро</div>
                <div class="this-text-about">наши операторы<br>
                    всегда на связи</div>
            </div>
            <div class="thought-right-border"></div>
            <div class="one-thought-wrapper">
                <div class="this-title">просто</div>
                <div class="this-text-about">
                    выбери ресторан
                    <br>
                    в два клика
                </div>
            </div>
            <div class="thought-right-border"></div>
            <div class="one-thought-wrapper">
                <div class="this-title">бесплатно</div>
                <div class="this-text-about">
                    никаких комиссий
                    <br>
                    и скрытых платежей
                </div>
            </div>
            <div class="thought-right-border"></div>
            <div class="one-thought-wrapper">
                <div class="this-title">онлайн</div>
                <div class="this-text-about">
                    фото и 3d-туры
                    <br>
                    для вашего выбора
                </div>
            </div>
        </div>
    </div>
<?else:?>
    <div class="under-wood-top-block-wrapper">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_TEMPLATE_PATH."/include_areas/order-line-form-trigger.php",
                "EDIT_TEMPLATE" => ""
            ),
            false
        );?>
        <?
//        $arRestIB = getArIblock("catalog", CITY_ID);
//        $APPLICATION->IncludeComponent(
//            "restoran:catalog.filter",
//            "banquet_filter_new",
//            Array(
//                "IBLOCK_TYPE" => "catalog",
//                "IBLOCK_ID" => $arRestIB["ID"],
//                "FILTER_NAME" => "arrFilter_pf",
//                "FIELD_CODE" => array(),
//                "PROPERTY_CODE" => array("area", "subway", "average_bill",'kolichestvochelovek'),
//                "PRICE_CODE" => array(),
//                "CACHE_TYPE" => "Y",    //  Y
//                "CACHE_TIME" => "36000002",
//                "CACHE_GROUPS" => "N",
//                "LIST_HEIGHT" => "5",
//                "TEXT_WIDTH" => "20",
//                "NUMBER_WIDTH" => "5",
//                "SAVE_IN_SESSION" => "N"
//            )
//        );
        ?>
    </div>
<?endif?>

</div>

<div class="content content-indent">

    <div class="block">

