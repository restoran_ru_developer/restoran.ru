<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$param_sharpen = 30;
$arFilter = array("name" => "sharpen", "precision" => $param_sharpen);  //  можно просто false
foreach($arResult['ITEMS'] as $item_key=>$item){
    $arFileTmp = CFile::ResizeImageGet(
        $item['PREVIEW_PICTURE'],
        array("width" => 728, "height" => 400),
        BX_RESIZE_IMAGE_EXACT,
        true, $arFilter
    );

    $arResult["SLIDER_PIC"][$item_key] = array(
        "SRC" => $arFileTmp["src"],
        'WIDTH' => $arFileTmp["width"],
        'HEIGHT' => $arFileTmp["height"],
    );


}
?>