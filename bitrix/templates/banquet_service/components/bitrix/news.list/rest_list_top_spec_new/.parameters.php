<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arTemplateParameters['IS_LAST_SECTION'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => GetMessage('NEWS_LIST_INDEX_PAGE'),
    'TYPE' => 'TEXT',
    'DEFAULT' => 'N'
);

?>