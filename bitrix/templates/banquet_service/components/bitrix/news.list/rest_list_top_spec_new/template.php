<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <h2><a href="<?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"http://www.federicoclub.ru/":$arItem["DETAIL_PAGE_URL"]?>" <?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"target='_blank'":""?>><?=$arItem["NAME"]?></a></h2>
    <div class="name_ratio">
        <?for($k = 1; $k <= 5; $k++):?>
            <span class="icon-star<?if($k > $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
        <?endfor?>        
        <?/*if(CSite::InGroup( array(14,15,1))):?>                                    
            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ID"]?>">Редактировать</a></div>
        <?endif;*/?>
    </div>
    <?if($arParams['IS_LAST_SECTION']=='Y'):?>
        <div class="order-buttons">
            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=$arItem["NAME"]?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=$arItem["NAME"]?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
            <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
        </div>
    <?endif?>
    <div id="rest-<?=$arItem["ID"]?>" class="carousel slide" data-ride="carousel">                                                
        <div class="carousel-inner">
            <div class="item active">
                <a href="<?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"http://www.federicoclub.ru/":$arItem["DETAIL_PAGE_URL"]?>" <?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"target='_blank'":""?>><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
            </div>
            <?if(count($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"])>1):?>
                <?foreach($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"] as $pKey=>$photo):?>
                    <div class="item">
                        <a href="<?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"http://www.federicoclub.ru/":$arItem["DETAIL_PAGE_URL"]?>" <?=($arItem["ID"]=="387409"||$arItem["ID"]=="1099834")?"target='_blank'":""?>><img src="<?=$photo["src"]?>" align="bottom" /></a>
                    </div>
                <?endforeach?>
            <?endif;?>
        </div>
        <a class="left carousel-control" href="#rest-<?=$arItem["ID"]?>" role="button" data-slide="prev">
          <span class="galery-control galery-control-left icon-arrow-left2"></span>
        </a>
        <a class="right carousel-control" href="#rest-<?=$arItem["ID"]?>" role="button" data-slide="next">
          <span class="galery-control galery-control-right icon-arrow-right2"></span>
        </a>
    </div>
    <div class="props">                                
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_phone")?></div>
            <div class="value">
<!--                --><?//if(!is_array($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])):?>
<!--                    --><?//=$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]?>
<!--                --><?//else:?>
<!--                    --><?//=$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]?>
<!--                --><?//endif?>
                <?
                if($arItem['PROPERTIES']["SHOW_REST_PHONE"]['VALUE']=='Да'){
                    if(is_array($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                        if(preg_match('/,/',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0])){
                            $phone_str_arr = explode(',',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                            $phone_str = $phone_str_arr[0];
                        }
                        elseif(preg_match('/;/',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0])){
                            $phone_str_arr = explode(';',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                            $phone_str = $phone_str_arr[0];
                        }
                        else {
                            $phone_str = trim(preg_replace('/[^-+0-9() ]+/i','',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]));
                        }
                    }
                    elseif($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]) {
                        if(preg_match('/,/',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                            $phone_str_arr = explode(',',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                            $phone_str = $phone_str_arr[0];
                        }
                        elseif(preg_match('/;/',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                            $phone_str_arr = explode(';',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                            $phone_str = $phone_str_arr[0];
                        }
                        else {
                            $phone_str = trim(preg_replace('/[^-+0-9() ]+/i','',$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]));
                        }
                    }
                    echo $phone_str;
                }
                else {
                    if (CITY_ID=="msk"){
                        echo '+7 (495) 988 26 56';
                    }
                    else {
                        echo '+7 (812) 740-18-20';
                    }
                }

                ?>

            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_banquet_bill")?></div>
            <div class="value">
                <?if(!is_array($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])?>
                <?endif?>

            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_kitchens")?></div>
            <div class="value">
                <?if(!is_array($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_features")?></div>
            <div class="value">
                <?if(!is_array($arItem["DISPLAY_PROPERTIES"]["features"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["features"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["features"]["DISPLAY_VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_subway")?></div>
            <div class="value">
                <?if (is_array($arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]?>
                <?else:?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]?>
                <?endif;?>
            </div>
        </div>


    </div>       
<?endforeach;?>