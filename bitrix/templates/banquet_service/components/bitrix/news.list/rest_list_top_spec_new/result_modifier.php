<?
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill",'features'
);
//CModule::IncludeModule("iblock");
global $TopSpec;
$arIB = getArIblock("catalog", CITY_ID);
//if($USER->isAdmin()){
//    FirePHP::getInstance()->info(count($arResult['ITEMS']),$arResult['SECTION']['PATH'][1]['NAME']);
//}
foreach($arResult["ITEMS"] as $cell=>$arItem) {
//    FirePHP::getInstance()->info($arItem);
//    break;
    $TopSpec[] = $arItem["ID"];
    // resize images
//    $res = CIBlockElement::GetByID($arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE']);

    $res = CIBlockElement::GetProperty($arIB['ID'], $arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE'], "sort", "asc", array("CODE" => "photos"));
    while ($ob = $res->Fetch())
    {
//            $temp_val['kitchen'][$key][] = $ob['VALUE'];
        $arFile = CFile::GetFileArray($ob['VALUE']);
        if ($arFile)
            $arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][] = $arFile;
    }
    $res = CIBlockElement::GetByID($arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE']);
    if($ar_res = $res->GetNext()) {
        $arResult["ITEMS"][$cell]['DETAIL_PAGE_URL'] = $ar_res['DETAIL_PAGE_URL'];
        $arResult["ITEMS"][$cell]['NAME'] = $ar_res['NAME'];

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], "sort", "asc", array("CODE" => "RATIO"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$cell]['PROPERTIES']["RATIO"]['VALUE'] = $ob['VALUE'];
        }
    }

    foreach($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photoID) {
        //$arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["DISPLAY_VALUE"][$pKey] = CFile::GetFileArray($photoID);
        $arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["DISPLAY_VALUE"][$pKey] = CFile::ResizeImageGet($photoID, array('width' => 728, 'height' => 250), BX_RESIZE_IMAGE_EXACT, true, Array());
    }
    if($arParams["PREVIEW_TRUNCATE_LEN"]>0)
    {
            $end_pos = $arParams["PREVIEW_TRUNCATE_LEN"];
            $arItem["PREVIEW_TEXT"] = strip_tags($arItem["~DETAIL_TEXT"]);
            while(substr($arItem["PREVIEW_TEXT"],$end_pos,1)!=" " && $end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $end_pos++;
            if($end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $arItem["PREVIEW_TEXT"] = substr($arItem["PREVIEW_TEXT"], 0, $end_pos)."...";
            $arResult["ITEMS"][$cell]["PREVIEW_TEXT"] = $arItem["PREVIEW_TEXT"];
    }

    $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][0], array('width'=>728, 'height'=>250), BX_RESIZE_IMAGE_EXACT, true, Array());
    if(!$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    else
        unset($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["DISPLAY_VALUE"][0]);

    $res = CIBlockElement::GetProperty($arIB['ID'], $arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE'], "sort", "asc", array("CODE" => "SHOW_REST_PHONE"));
    if($ob = $res->Fetch()){
        $arResult["ITEMS"][$cell]['PROPERTIES']["SHOW_REST_PHONE"]['VALUE'] = $ob['VALUE_ENUM'];
    }
    
    // remove links from subway name
    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        if($arProperty["DISPLAY_VALUE"])
        {
            // remove links from subway name
            if(in_array($pid, $arFormatProps)) {
                if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                    foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                    }
                } else {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
                }
            }
        }
    }
}
?>