<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */?>
<?//print_r($arResult);
//UF_SHOW_IN_B_ROOT
//UF_ALL_NEWS_TITLE
//print_r($arParams['MAX_SECTION_NUM']);
//if($USER->IsAdmin()){
//    print_r($arResult);
//}

?>
<div class="title">Где отметить?</div>
<div class="selection">
    <i>Уникальные подборки мест на любой вкус и повод: доверьтесь опыту Restoran.ru</i>
    <ul>
        <?
        foreach ($arResult['SECTIONS'] as $key=>&$arSection)
        {
            if(($key+1)>intval($arParams['MAX_SECTION_NUM']) && $arParams['MAX_SECTION_NUM']!='ALL'){
//                echo 'here';
              break;
            }
            ?>
            <li><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></li>
        <?}?>
    </ul>
    <?if($arParams['MAX_SECTION_NUM']!='ALL'):?>
    <div class="more_links text-right">
        <a href="<?=$arResult['SECTIONS'][0]['LIST_PAGE_URL']?>" class="btn btn-light">Все подборки</a>
    </div>
    <?endif?>
</div>