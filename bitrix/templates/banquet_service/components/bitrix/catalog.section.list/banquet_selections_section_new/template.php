<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */?>
<?
$last_inner = true; // в последнем разделе
if(is_array($arResult['SECTIONS'][0])){// && $arResult['SECTION']['DEPTH_LEVEL']<2
    $last_inner = false;
}
?>
<?
$arIB = getArIblock("selection_of_restaurants", CITY_ID);
?>
<?if(!$last_inner):// не последний раздел?>
    <div class="left-side">
    <?
    foreach ($arResult['SECTIONS'] as $key=>&$arSection):
    ?>
        <div class="one-block-selection-wrapper">
            <div class="header_1" >
                <h2><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></h2>
            </div>
            <div class="special">
                <?
                //        top-slider одного заведения

                if(intval($arSection['ELEMENT_CNT'])>0 && intval($arSection['UF_TOP_SLIDER'])>0):
//                    $GLOBALS['arrFilterTop1'] = array('PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');
                    $GLOBALS['arrFilterTop1'] = array('ID'=>$arSection['UF_TOP_SLIDER']);
                    $GLOBALS['arrFilterTop1']['PROPERTY_THIS_RESTAURANT.ACTIVE'] = 'Y';

                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "rest_list_top_spec_new",
                        Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "selection_of_restaurants",
                            "IBLOCK_ID" => $arIB["ID"],
                            "NEWS_COUNT" => "1",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFilterTop1",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array("phone", "kitchen", "average_bill", "subway", 'features','THIS_RESTAURANT'),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "160",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arSection['ID'],
                            "PARENT_SECTION_CODE" => '',
                            "CACHE_TYPE" => "Y",//y
                            "CACHE_TIME" => "86404",
                            "CACHE_FILTER" => "Y",//y
                            "CACHE_GROUPS" => "N",
                            "CACHE_NOTES" => "Nww",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                        ),
                        false
                    );
                    unset($GLOBALS['arrFilterTop1']);
                endif;
                ?>
            </div>

            <div class="clearfix"></div>
            <div class="tab-content">
                <div class="tab-pane active sm">

                    <?
                    //            print_r($arSection);
                    if(intval($arSection['ELEMENT_CNT'])>0):
//                        $GLOBALS['arrFilter'] = array('!PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');

                        $GLOBALS['arrFilter'] = array(
                            array(
                                "LOGIC" => "OR",
                            ),
                        );
                        if($arSection['UF_FOUR_SECTION1']){
                            $GLOBALS['arrFilter'][0][0] = array("ID"=>$arSection['UF_FOUR_SECTION1']);
                        }
                        if($arSection['UF_FOUR_SECTION2']){
                            $GLOBALS['arrFilter'][0][1] = array("ID"=>$arSection['UF_FOUR_SECTION2']);
                        }
                        if($arSection['UF_FOUR_SECTION3']) {
                            $GLOBALS['arrFilter'][0][2] = array("ID" => $arSection['UF_FOUR_SECTION3']);
                        }

                        $GLOBALS['arrFilter']['PROPERTY_THIS_RESTAURANT.ACTIVE'] = 'Y';

                        if($arSection['UF_FOUR_SECTION1'] || $arSection['UF_FOUR_SECTION2'] || $arSection['UF_FOUR_SECTION3']){
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list", "banquet_main_news_new", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "selection_of_restaurants",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "FILTER",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "NAME",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => 'arrFilter',
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("THIS_RESTAURANT"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arSection['ID'],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",//A
                                "CACHE_TIME" => "36000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "REST_PROPS" => "Y",
                                'ALL_NEWS_TITLE'=> $arSection['UF_ALL_NEWS_TITLE'] ? $arSection['UF_ALL_NEWS_TITLE'] : $arSection['NAME'],
                                'BANQUET_INDEX_PAGE' => $arResult['SECTION']['SECTION_PAGE_URL'] ? 'N' : 'Y'
                            ), false
                            );
                        }

                        unset($GLOBALS['arrFilter']);
                        ?>
                    <?endif?>

                </div>
            </div>
        </div>
    <?endforeach;?>
<?else:?>
    <div class="header_1" ><?=$arResult['SECTION']['NAME']?></div>
    <div class="left-side">
    <div class="one-block-selection-wrapper">

        <div class="special">
            <?
            //        top-slider одного заведения
            if(intval($arResult['SECTION']['ELEMENT_CNT'])>0 && intval($arResult['SECTION']['UF_TOP_SLIDER'])>0):
    //                $GLOBALS['arrFilterTop1'] = array('PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');
                $GLOBALS['arrFilterTop1'] = array('ID'=>$arResult['SECTION']['UF_TOP_SLIDER']);
                $GLOBALS['arrFilterTop1']['PROPERTY_THIS_RESTAURANT.ACTIVE'] = 'Y';

                FirePHP::getInstance()->info($GLOBALS['arrFilterTop1'],'');
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "rest_list_top_spec_new",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "selection_of_restaurants",
                        "IBLOCK_ID" => $arIB["ID"],
                        "NEWS_COUNT" => "1",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrFilterTop1",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => array("phone", "kitchen", "average_bill", "subway", 'features','THIS_RESTAURANT'),
                        "CHECK_DATES" => "N",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "160",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => $arResult['SECTION']['ID'],
                        "PARENT_SECTION_CODE" => '',
                        "CACHE_TYPE" => "Y",//y
                        "CACHE_TIME" => "86402",
                        "CACHE_FILTER" => "Y",//y
                        "CACHE_GROUPS" => "N",
                        "CACHE_NOTES" => "Nww",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                    ),
                    false
                );
                unset($GLOBALS['arrFilterTop1']);
            endif;
            ?>
        </div>
    </div>

<?endif?>


</div>
    <div class="right-side <?if($last_inner){echo 'inner';}?>">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                "TYPE" => "right_2_main_page",
                "NOINDEX" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
            ),
            false
        );?>

        <?if(!$last_inner):?>
            <?
            //    $arSiteMenuIB = getArIblock("selection_of_restaurants", CITY_ID);
            $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "banquet_sections", array(
                    "IBLOCK_TYPE" => "selection_of_restaurants",
                    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "COUNT_ELEMENTS" => "N",
                    "TOP_DEPTH" => "1",
                    "SECTION_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SECTION_URL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000001",
                    "CACHE_GROUPS" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "VIEW_MODE" => "LINE",
                    "SHOW_PARENT_NAME" => "Y",
                    'MAX_SECTION_NUM' => 1000
                ),
                false
            );?>
        <?endif;?>
<!--        --><?//$APPLICATION->IncludeComponent(
//            "bitrix:main.include",
//            "",
//            Array(
//                "AREA_FILE_SHOW" => "file",
//                "PATH" => SITE_TEMPLATE_PATH."/include_areas/banquet-right-banner-".CITY_ID.".php",
//                "EDIT_TEMPLATE" => ""
//            ),
//            false
//        );?>
        <?
//        $APPLICATION->IncludeComponent(
//            "bitrix:main.include",
//            "",
//            Array(
//                "AREA_FILE_SHOW" => "file",
//                "PATH" => "/bitrix/templates/main_2014/include_areas/new-order-form-with-city.php",
//                "EDIT_TEMPLATE" => ""
//            ),
//            false
//        );
        ?>



    </div>
    <div class="clearfix"></div>

<?if($last_inner):?>
    <?if(intval($arResult['SECTION']['ELEMENT_CNT'])>0):?>
        <?
//        $GLOBALS['lastSectionFilter'] = array('!PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');
        $GLOBALS['lastSectionFilter'] = array('!ID'=>$arResult['SECTION']['UF_TOP_SLIDER']);
        $GLOBALS['lastSectionFilter']['PROPERTY_THIS_RESTAURANT.ACTIVE'] = 'Y';
        $APPLICATION->IncludeComponent("restoran:restoraunts.list",
            "rest_list_new",
            array(
                "IBLOCK_TYPE" => "selection_of_restaurants",
                "IBLOCK_ID" => $_REQUEST["CITY_ID"],
                "PARENT_SECTION_CODE" => $arResult['SECTION']['CODE'],
                "NEWS_COUNT" => "20",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY9" => "NAME",
                "SORT_ORDER9" => "ASC",
                "FILTER_NAME" => "lastSectionFilter",
                "PROPERTY_CODE" => array(
                    0 => "address",
                    1 => "opening_hours",
                    2 => "phone",
                    3 => "type",
                    4 => "kitchen",
                    5 => "average_bill",
                    6 => "subway",
                    8 => "IDEALLY",
                    9 => "THIS_RESTAURANT",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "Y",    //  Y
                "CACHE_TIME" => "86407",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "PREVIEW_PICTURE_MAX_WIDTH" => "231",
                "PREVIEW_PICTURE_MAX_HEIGHT" => "163",
                "PREVIEW_TRUNCATE_LEN" => "150",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PAGER_TEMPLATE" => "rest_list_arrows",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Рестораны",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                'PARENT_NEWS_LIST' => $convert_news,
                'THIS_SECTION_IDEALLY' => $arResult['SECTION']['UF_IDEALLY_FOR_SECT']
            ),
            false
        );
        unset($GLOBALS['lastSectionFilter']);
        ?>
    <?endif?>
<?endif?>