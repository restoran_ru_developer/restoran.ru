<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
if($arResult['SECTION']['ID']!=0){    //  раздел с подразделами
    $ALL_NEWS_PARENT_SECTION_ID = $arResult['SECTION']['IBLOCK_SECTION_ID'];


    $arrKeywors = explode(' ',$arResult['SECTION']['NAME']);
    $strKeywors = implode(',',$arrKeywors).'заказ,банкет';


    $APPLICATION->SetTitle($arResult['SECTION']['NAME']);
    $APPLICATION->SetPageProperty("keywords", $strKeywors);
    $APPLICATION->SetPageProperty("description", 'Заказ банкета:'.$arResult['SECTION']['NAME']);

    $APPLICATION->AddViewContent('section_desc', $arResult['SECTION']['DESCRIPTION']);
}
else {
    $APPLICATION->SetTitle('Все подборки банкетной службы');
    $APPLICATION->SetPageProperty("keywords", 'все,подборки,банкетной,службы,заказ,банкет');
    $APPLICATION->SetPageProperty("description", 'Заказ банкета: все подборки банкетной службы');
    /**добавить возможность править description, keywords , может стоит привязаться к ib**/
    $arSelect = Array("PREVIEW_TEXT");
    $arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], 'CODE'=>'LIST_DESC');
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $APPLICATION->AddViewContent('section_desc', $arFields['PREVIEW_TEXT']);
    }
}
?>