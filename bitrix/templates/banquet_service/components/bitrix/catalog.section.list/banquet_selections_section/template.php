<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */?>
<?
//print_r($arResult);
$last_inner = true; // в последнем разделе
if(is_array($arResult['SECTIONS'][0])){// && $arResult['SECTION']['DEPTH_LEVEL']<2
    $last_inner = false;
}
?>
<?
$arIB = getArIblock("catalog", CITY_ID);
?>
<?if(!$last_inner):// не последний раздел?>
    <div class="left-side">
        <?

        foreach ($arResult['SECTIONS'] as $key=>&$arSection):
        ?>
        <div class="one-block-selection-wrapper">
            <div class="header_1" >
                <?=$arSection['NAME']?>
            </div>
            <div class="special">
                <?
        //        top-slider одного заведения
                if(intval($arSection['ELEMENT_CNT'])>0):
                    $arSelect = Array('ID',"PROPERTY_THIS_RESTAURANT", 'PROPERTY_SHOW_IN_SLIDER');
                    $arFilter = Array("IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']), "ACTIVE"=>"Y", 'SECTION_ID'=>$arSection['ID'], 'INCLUDE_SUBSECTIONS'=>'Y', 'PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, array('nPageSize'=>1), $arSelect);
                    if($ob = $res->GetNextElement())
                    {
                        $obj_field = $ob->GetFields($obj_field);
                        ?>
<!--                        <div style="display: none">--><?//print_r($obj_field)?><!--</div>-->
    <?
        //                $output_news[$obj_field['ID']] = $obj_field;
                        $GLOBALS['arrFilterTop1']['ID'] = $obj_field['PROPERTY_THIS_RESTAURANT_VALUE'];
        //                $convert_news[$obj_field['PROPERTY_THIS_RESTAURANT_VALUE']] = $obj_field['ID'];
                    }
                    if(!empty($GLOBALS['arrFilterTop1'])):
                        $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "rest_list_top_spec",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => "1",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arrFilterTop1",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "subway", 'features', "RATIO"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "160",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => '',
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "86400",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "CACHE_NOTES" => "Nww",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                            ),
                            false
                        );
                        unset($GLOBALS['arrFilterTop1']);
                    endif;
                endif;
                ?>
            </div>


            <div class="tab-content">
                <div class="tab-pane active sm">

                    <?
        //            print_r($arSection);
                    if(intval($arSection['ELEMENT_CNT'])>0):
                        $arSelect = Array('ID',"PROPERTY_THIS_RESTAURANT");
                        $arFilter = Array("IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']), "ACTIVE"=>"Y", 'SECTION_ID'=>$arSection['ID'], 'INCLUDE_SUBSECTIONS'=>'Y', '!PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');
                        $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, array('nPageSize'=>3), $arSelect);    //  ограничить кол-во
                        while($ob = $res->GetNextElement())
                        {
                            $obj_field = $ob->GetFields();
                            $output_news[$obj_field['ID']] = $obj_field;
                            $convert_news[$obj_field['PROPERTY_THIS_RESTAURANT_VALUE']] = $obj_field['ID'];
                        }

        //                print_r($output_news);

                        $GLOBALS['banquet_filter'] = array(
                            array(
                                "LOGIC" => "OR",
                            ),
                        );

                        foreach ($output_news as $key=>$one_news) {
                            $GLOBALS['banquet_filter'][0][$key] = array("ID"=>$one_news['PROPERTY_THIS_RESTAURANT_VALUE']);
                        }
        //                print_r($GLOBALS['banquet_filter']);
                        if(count($output_news)):
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list", "banquet_main_news", Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "NEWS_COUNT" => 3,
//                                    "SORT_BY1" => "PROPERTY_rating_date",
//                                    "SORT_ORDER1" => "DESC",
//                                    "SORT_BY2" => "PROPERTY_stat_day",
//                                    "SORT_ORDER2" => "DESC",
//                                    "SORT_BY3" => "NAME",
//                                    "SORT_ORDER3" => "ASC",
                                    "FILTER_NAME" => 'banquet_filter',
                                    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//A
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "REST_PROPS" => "Y",
                                    'ALL_NEWS_TITLE'=> $arSection['UF_ALL_NEWS_TITLE'] ? $arSection['UF_ALL_NEWS_TITLE'] : 'все новости',
                                    'PARENT_NEWS_LIST' => $convert_news,
                                    'ALL_NEWS_PARENT_SECTION_ID' => $arSection['ID'],
                                    'BANQUET_INDEX_PAGE' => 'N',
                                    'THIS_SECTION_IDEALLY' => $arSection['UF_IDEALLY_FOR_SECT']
                                ), false
                            );
                        endif;
                        unset($output_news, $GLOBALS['banquet_filter'], $convert_news);
                        ?>
                    <?endif?>

                </div>
            </div>
        </div>
        <?endforeach;?>
    <script>
//        $(function(){
//            $('.ontop').text('еще банкетные залы');
//        })
    </script>
<?else:?>
    <h1><?=$arResult['SECTION']['NAME']?></h1>
    <div class="left-side">
        <div class="one-block-selection-wrapper">

            <div class="special">
                <?
                //        top-slider одного заведения
                if(intval($arResult['SECTION']['ELEMENT_CNT'])>0):
                    $arSelect = Array('ID',"PROPERTY_THIS_RESTAURANT");
                    $arFilter = Array("IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']), "ACTIVE"=>"Y", 'SECTION_ID'=>$arResult['SECTION']['ID'], 'INCLUDE_SUBSECTIONS'=>'Y', 'PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, array('nPageSize'=>1), $arSelect);
                    if($ob = $res->GetNextElement())
                    {
                        $obj_field = $ob->GetFields();
                        $GLOBALS['arrFilterTop1']['ID'] = $obj_field['PROPERTY_THIS_RESTAURANT_VALUE'];
    //                    print_r($obj_field);
                    }
                    if(!empty($GLOBALS['arrFilterTop1'])):
                        $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "rest_list_top_spec",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => "1",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arrFilterTop1",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "subway", 'features', "RATIO"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "160",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => '',
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "86400",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "CACHE_NOTES" => "Nww",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                'IS_LAST_SECTION' => "Y"

                            ),
                            false
                        );
                        unset($GLOBALS['arrFilterTop1']);
                    endif;
                endif;
                ?>
            </div>
        </div>

    <script>
//        $(function(){
//            $('.ontop').text('больше банкетных залов');
//        })
    </script>
<?endif?>


</div>
<div class="right-side <?if($last_inner){echo 'inner';}?>">

    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_TEMPLATE_PATH."/include_areas/banquet-right-banner.php",
            "EDIT_TEMPLATE" => ""
        ),
        false
    );?>

    <?if(!$last_inner):?>
        <?
    //    $arSiteMenuIB = getArIblock("selection_of_restaurants", CITY_ID);
        $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "banquet_sections", array(
                "IBLOCK_TYPE" => "selection_of_restaurants",
                "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "N",
                "TOP_DEPTH" => "1",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "VIEW_MODE" => "LINE",
                "SHOW_PARENT_NAME" => "Y",
                'MAX_SECTION_NUM' => 12
            ),
            false
        );?>
    <?endif;?>
</div>
<div class="clearfix"></div>

<?if($last_inner):?>
    <?if(intval($arResult['SECTION']['ELEMENT_CNT'])>0):?>

        <?



//        print_r($new_filter_arr);
        $arSelect = Array('ID',"PROPERTY_THIS_RESTAURANT");
        $arFilter = Array("IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']), "ACTIVE"=>"Y", 'SECTION_ID'=>$arResult['SECTION']['ID'], 'INCLUDE_SUBSECTIONS'=>'Y', '!PROPERTY_SHOW_IN_SLIDER_VALUE'=>'Да');

//        print_r($arFilter);

        $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, false, $arSelect);    //  ограничить кол-во
        while($ob = $res->GetNextElement())
        {
            $obj_field = $ob->GetFields();
            $output_news[$obj_field['ID']] = $obj_field;
            $convert_news[$obj_field['PROPERTY_THIS_RESTAURANT_VALUE']] = $obj_field['ID'];
        }

        //                print_r($output_news);

        $GLOBALS['lastSectionFilter'] = array(
            array(
                "LOGIC" => "OR",
            ),
//            'PROPERTY_sale10_VALUE'=>1
        );

        foreach ($output_news as $key=>$one_news) {
            $GLOBALS['lastSectionFilter'][0][] = array("ID"=>$one_news['PROPERTY_THIS_RESTAURANT_VALUE']);
        }

//        print_r($GLOBALS['lastSectionFilter']);
        ?>

        <?
        if(count($output_news)):

            $APPLICATION->IncludeComponent("restoran:restoraunts.list",
                "rest_list",
                array(
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $_REQUEST["CITY_ID"],
                    "PARENT_SECTION_CODE" => "",
                    "NEWS_COUNT" => "20",
//                    "SORT_BY1" => "NAME",
//                    "SORT_ORDER1" => "ASC",
//                    "SORT_BY2" => "SORT",
//                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "lastSectionFilter",
                    "PROPERTY_CODE" => array(
                        0 => "address",
                        1 => "opening_hours",
                        2 => "phone",
                        3 => "type",
                        4 => "kitchen",
                        5 => "average_bill",
                        6 => "subway",
                        7 => "ratio",
//                        8 => "sale10",
//                        9 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "Y",    //  Y
                    "CACHE_TIME" => "86400",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "PREVIEW_PICTURE_MAX_WIDTH" => "231",
                    "PREVIEW_PICTURE_MAX_HEIGHT" => "163",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PAGER_TEMPLATE" => "rest_list_arrows",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Рестораны",
                    "PAGER_SHOW_ALWAYS" => "Y",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    'PARENT_NEWS_LIST' => $convert_news,
                    'THIS_SECTION_IDEALLY' => $arResult['SECTION']['UF_IDEALLY_FOR_SECT']
                ),
                false
            );
        else:?>
            <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
        <?endif;
        unset($output_news, $GLOBALS['lastSectionFilter'],$convert_news);
        ?>
    <?endif?>
<?endif?>