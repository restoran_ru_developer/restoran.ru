<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1", "pageRestCnt","index_php?page","index_php","PAGEN_2",'SECTION_CODE');
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");
?>
<?if ($arResult["NavPageNomer"] > 1):?>
    <a href="<?=$APPLICATION->GetCurPageParam("page=".($arResult["NavPageNomer"]-1).$addNavParams,$excUrlParams)?>" class="prev glyphicon glyphicon-chevron-left"></a>        
<?else:?>
    <a href="javascript:void(0)" class="prev glyphicon glyphicon-chevron-left"></a>        
<?endif?>
<div class="pages">
<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
if($arResult["bDescPageNumbering"] === false) {
    //v_dump($_REQUEST);
    // to show always first and last pages
    $arResult["nStartPage"] = 1;
    $arResult["nEndPage"] = $arResult["NavPageCount"];

    $sPrevHref = '';
    if ($arResult["NavPageNomer"] > 1)
    {
        $bPrevDisabled = false;

        if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
        {
            $sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
        }
        else
        {
            $sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
        }
    }
    else
    {
        $bPrevDisabled = true;
    }

    $sNextHref = '';
    if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
    {
        $bNextDisabled = false;
        $sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
    }
    else
    {
        $bNextDisabled = true;
    }
    ?>
<!--    --><?//if (!$_REQUEST["letter"]):?>
<!--        --><?//if(!$_REQUEST["page"] || strlen($_REQUEST["page"]) <= 0):?>
<!--            <span class="current">&nbsp;--><?//=GetMessage("TOP_SPEC")?><!--&nbsp;</span>-->
<!--        --><?//else:?>
<!--            <a href="--><?//=$arResult["sUrlPath"]?><!--">--><?//=GetMessage("TOP_SPEC")?><!--</a>-->
<!--        --><?//endif?>
<!--    --><?//endif;?>
    <?
    $bFirst = true;
    $bPoints = false;
    do
    {
        if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
        {

            if ($arResult["nStartPage"] == $arResult["NavPageNomer"] && (strlen($_REQUEST["page"]) > 0 || $_REQUEST["letter"])):?>
                <span class="current">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
            <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false && strlen($_REQUEST["page"]) > 0):?>
                <a href="<?=$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
            <?else:?>
                <a href="<?=$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
            <?endif;
            $bFirst = false;
            $bPoints = true;
        }
        else
        {
            if ($bPoints)
            {
                ?>...<?
                $bPoints = false;
            }
        }
        $arResult["nStartPage"]++;
    } while($arResult["nStartPage"] <= $arResult["nEndPage"]);
}
?>
</div>
<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
    <a href="<?=$APPLICATION->GetCurPageParam("page=".($arResult["NavPageNomer"]+1).$addNavParams,$excUrlParams)?>" class="next glyphicon glyphicon-chevron-right"></a>
<?else:?>
    <a href="javascript:void(0)" class="next glyphicon glyphicon-chevron-right"></a>
<?endif?>