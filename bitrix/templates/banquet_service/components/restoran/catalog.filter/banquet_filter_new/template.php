<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="center-block">
    <?
    $this->setFrameMode(true);
    $this->createFrame()->begin('Загрузка');
    if ($_REQUEST["CATALOG_ID"])
        $cat = $_REQUEST["CATALOG_ID"];
    else
        $cat = "restaurants";
    ?>
    <noindex>
        <form action="/<?=CITY_ID?>/banquet-service/selections/" method="get" name="arrFilter_pf" class="form-inline fil" role="form" id="banquet-filter-form">
            <div class="this-section-filter this-section-filter-new">

                <!--            <input type="hidden" name="page" value="1">-->


                <!--            <div class="form-group inverted">-->
                <ul >
                    <li ><?=GetMessage("FIND_BY_PARAMS")?></li>
                    <div class="banquet-filter-items-right-border"></div>
                    <?
                    //                FirePHP::getInstance()->info($arResult["arrProp"]);
                    //                $need_props = array('area', 'subway', 'average_bill', 'kolichestvochelovek');
                    //                $need_props = array(429, 409, 411, 407);
                    foreach($arResult["arrProp"] as $prop_id => $arProp):
//                foreach($need_props as $prop_id):
//                    $arProp = $arResult["arrProp"][$prop_id];
                        if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
                            ?>
                            <li>
                                <div class="dropdown">
                                    <a data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>

                                    <?
                                    $e = 8; $pp=0;
                                    if ((count($arProp["VALUE_LIST"])-$e)>=8)
                                        $e = 10;
                                    if ((count($arProp["VALUE_LIST"])-$e)>=30)
                                        $e = 10;
                                    if ((count($arProp["VALUE_LIST"])-$e)>=50)
                                        $e = 28;
                                    if ((count($arProp["VALUE_LIST"])-$e)<8)
                                        $e = 5;
                                    if ((count($arProp["VALUE_LIST"])-8)<0)
                                        $e = 2;
                                    $s = ceil(count($arProp["VALUE_LIST"])/$e);
                                    $c = 160*$s;
                                    if ($c < 480)
                                        $c = 480;
                                    ?>

                                    <div class="dropdown-menu w<?=$c?>" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                                        <div class="title_box">
                                            <div class="pull-left this-item-name"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                            <div class="pull-right">
                                                <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" class="clear-all-filter-button" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>">
                                            </div>
                                        </div>

                                        <ul class="first-ul-str">
                                            <?$pp=0?>
                                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                            <?//=($pp%$e==0)?"<ul>":""?>

                                            <li><a data-val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>

                                                <?if (end($arProp["VALUE_LIST"])==$val):?>
                                                <?if ($arProp["CODE"]=="out_city"):?>
                                            <li><a class="unbind" href="/<?=CITY_ID?>/catalog/restaurants/out_city/all/"><?=GetMessage("ALL_OUT_CITY")?></a></li>
                                        <?endif;?>
                                        <?endif;?>
                                            <?if(($pp+1)%3==0 && $val!=end($arProp["VALUE_LIST"])):?>
                                        </ul><ul>
                                            <?endif?>
                                            <?//=($pp%$e==($e-1)||end($arProp["VALUE_LIST"])==$val)?"</ul>":""?>
                                            <?
                                            $pp++;
                                            endforeach;?>
                                        </ul>

                                        <div class="clearfix"></div>
                                        <?foreach($_REQUEST[$arParams["FILTER_NAME"]][$arProp["CODE"]] as $s):?>
                                            <input type="hidden" data-val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                                        <?endforeach;?>
                                    </div>
                                </div>
                            </li>
                            <?if(end($arResult["arrProp"])!=$arProp):?>
                            <div class="banquet-filter-items-right-border"></div>
                        <?endif;?>
                        <?elseif ($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                            <li>
                                <div class="dropdown">
                                    <a data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>
                                    <div class="dropdown-menu w700 subway" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                                        <div class="title_box">
                                            <div class="pull-left this-item-name"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                            <div class="pull-right">
                                                <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" class="clear-all-filter-button" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                                <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>">
                                            </div>
                                        </div>
                                        <div class="subway-canvas-wrapper">
                                            <div class="subway-map map-of-<?=CITY_ID?>">
                                            <?if (LANGUAGE_ID=="en"):?>
                                                <img src="/tpl/images/subway/<?=CITY_ID?>_en.gif" />
                                            <?else:?>
                                                <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                            <?endif;?>
                                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                                <?if ($val["STYLE"]):?>
                                                    <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" data-val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"> </a>
                                                <?endif;?>
                                            <?endforeach;?>
                                            <?foreach($_REQUEST[$arParams["FILTER_NAME"]]["subway"] as $s):?>
                                                <input type="hidden" name="arrFilter_pf[subway][]" data-val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                            <?endforeach;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?if(end($arResult["arrProp"])!=$arProp):?>
                                <div class="banquet-filter-items-right-border"></div>
                            <?endif;?>
                        <?endif;?>
                    <?endforeach;?>
                    <li class="banquet-filter-submit-wrapper"><input type="submit" id="banquet-filter-submit" value="найти"></li>
                </ul>
                <!--            </div>-->

                <div class="clearfix"></div>
                <?
                if ($cat=="restaurants"):
                    $mess = GetMessage("IBLOCK_SET_FILTER");
                else:
                    $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
                endif;
                ?>

                <div class="clearfix"></div>

            </div>
        </form>
    </noindex>

</div>

<div class="new_filter_results_wrapper">
    <div class="center-block">
        <div id="new_filter_results" style="display: none;">
            <div class="pull-left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
            <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
                <ul id="multi<?=$prop_id?>"><li class="end"></ul>
            <?endforeach;?>
            <ul id="multi1"><li class="end"></ul>
            <!--        <div class="pull-left">-->
            <!--            <input class="btn btn-light" type="submit" value="--><?//=$mess?><!--">-->
            <!--        </div>-->
            <div class="clearfix"></div>
        </div>
    </div>
</div>