<?
                
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "address", 'phone'
);

//FirePHP::getInstance()->info(count($arResult["ITEMS"]));
$arIB = getArIblock("catalog", CITY_ID);

foreach($arResult["ITEMS"] as $cell=>$arItem) {

//    FirePHP::getInstance()->info();
//    break;


    $res = CIBlockElement::GetProperty($arIB["ID"], $arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE'], "sort", "asc", array("CODE" => "sleeping_rest"));
    if ($ob = $res->Fetch())
    {
        if ($ob["VALUE"])
        {
            unset($arResult["ITEMS"][$cell]);
            continue;
        }
    }

    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        // remove links from subway and kitchen name
        if(in_array($pid, $arFormatProps)) {
            if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                }
            } else {
                $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
            }
        }
    }    
    if (LANGUAGE_ID=="en")
    {
        foreach ($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"] as &$properties)
        {        

                if (is_array($properties["DISPLAY_VALUE"]))
                {
                    if (is_array($properties["VALUE"]))
                    {
                        foreach($properties["VALUE"] as $key=>$val)
                        {
                            $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                            if ($ar = $r->Fetch())
                            {                    
                                if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                    $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);                        
                            }            
                        }
                    }
                    else
                    {
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {           
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                        } 
                    }
                }
                else
                {
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {   
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);

                    }   
                }    
        }
    }

    $res = CIBlockElement::GetByID($arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE']);
    if($ar_res = $res->GetNext()){
        $arResult["ITEMS"][$cell]['NAME'] = $ar_res['NAME'];
        $arResult["ITEMS"][$cell]['DETAIL_PAGE_URL'] = $ar_res['DETAIL_PAGE_URL'];
        $arFile = CFile::GetFileArray($ar_res["PREVIEW_PICTURE"]);

        if(!empty($arFile)) {
            $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arFile, array('width'=>232, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true, Array());
        }
        else {
            $arFile = CFile::GetFileArray($ar_res["DETAIL_PICTURE"]);
            $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arFile, array('width'=>232, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true, Array());
        }

        if(empty($arResult["ITEMS"][$cell]["PREVIEW_PICTURE"])){
            $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm_new.png";
        }

        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], "sort", "asc", array("CODE" => "RATIO"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$cell]['PROPERTIES']["RATIO"]['VALUE'] = $ob['VALUE'];
        }
    }

    foreach($arItem['PROPERTIES']['IDEALLY']['VALUE'] as $IDEALLY)
    {
        $r = CIBlockElement::GetByID($IDEALLY);
        if ($a = $r->Fetch()){

            if(!empty($arParams['THIS_SECTION_IDEALLY'])){
                if($a['ID']==$arParams['THIS_SECTION_IDEALLY']){    //  если совпадает с разделом
                    $arResult["ITEMS"][$cell]["IDEALLY"][0] = $a['NAME'];
                }
            }
            else {
                $arResult["ITEMS"][$cell]["IDEALLY"][] = $a['NAME'];
            }
        }
    }

    $res = CIBlockElement::GetProperty($arIB['ID'], $arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE'], "sort", "asc", array("CODE" => "SHOW_REST_PHONE"));
    if($ob = $res->Fetch()){
        $arResult["ITEMS"][$cell]['PROPERTIES']["SHOW_REST_PHONE"]['VALUE'] = $ob['VALUE_ENUM'];
    }


}
?>