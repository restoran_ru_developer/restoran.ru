<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//print_r($arResult);?>
<?if (count($arResult["ITEMS"])>0):?>    
    <?// check exist PAGEN_1?>
        <div class="restoran-list">
            <div class="hr"></div>
            <?$num_key=0;?>
            <?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
                <div class="item">
                    <?
                    if ($_REQUEST["letter"]&&$_REQUEST["letter"]!="09")
                    {
                        $letter = iconv("windows-1251","utf-8",chr($_REQUEST["letter"]));
                        if ($letter!=substr($arItem["NAME"],0,1))
                        {   
                            $temp = array();
                            $temp = explode(",",$arItem["TAGS"]);
                            $arItem["NAME"] = $temp[0];
                        }
                    }
                    ?>
                    <div class="type">
                        <?if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                            <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?>
                        <?else:?>
                            <?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
                        <?endif;?>
                    </div>
                    <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                    <div class="name_ratio">                        
                        <?for($i = 1; $i <= 5; $i++):?>
                            <span class="icon-star<?if($i > $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
                        <?endfor?>                    
                    </div>
                    <div class="clearfix"></div>
                    <div class="left pull-left">
                        <div class="pic">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                            <?if ($arItem["PROPERTIES"]["d_tours"]["VALUE"]):?>
                                <div style="position:absolute; right:20px; bottom:20px;"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour2.png" width="70"/></div>
                            <?endif;?>
                        </div>
<!--                        <div class="order-buttons">
                            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=$arItem["NAME"]?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                            <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
                        </div>-->
                    </div>
                    <div class="right pull-left">
                        <?
                        $arFormatProps = Array(
                            "kitchen", "average_bill", 'phone', "address", "subway"
                        );
//                        foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):
                        foreach($arFormatProps as $pid):
                            if(empty($arItem["DISPLAY_PROPERTIES"][$pid]))
                                continue;
                            ?>
                            <?if ($pid!="type"&&$pid!="photos"&&$pid!="RATIO"):?>
                                <?if ($pid!="subway"):?>
                                    <div class="prop">
                                        <div class="name">
                                            <?if ($pid=="subway"&&CITY_ID=="urm"):?>
                                                <b>Ж/д станция: </b>
                                            <?else:?>
                                                <?=$arItem["DISPLAY_PROPERTIES"][$pid]["NAME"]?>:
                                            <?endif;?>
                                        </div>
                                        <div class="value">
                                            <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                                <?if (CITY_ID=="spb"):?>
                                                        <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                                <?else:?>
                                                        <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                                <?endif;?>                                                    
                                            <?else:?>
                                                <?if ($pid=="phone"):?>
                                                    <?

                                                    $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = str_replace(";", "", $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);

                                                    if(is_array($arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]))
                                                            $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE2"]=implode(", ",$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
                                                    else{
                                                            $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE2"]=$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"];
                                                            //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                    }

                                                    $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE3"]=  explode(",", $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE2"]);



                                                    $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                    preg_match_all($reg, $arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE2"], $matches);


                                                    $TELs=array();
                                                    for($p=1;$p<5;$p++){
                                                            foreach($matches[$p] as $key=>$v){
                                                                    $TELs[$key].=$v;
                                                            }	
                                                    }

                                                    foreach($TELs as $key => $T){
                                                            if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                                    }
                                                    $old_phone = "";
                                                    foreach($TELs as $key => $T){
                                                        if ($old_phone!=$T):

                                                    ?>
                                                            <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE3"][$key]?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                                        <?endif;$old_phone = $T;?>
                                                    <?}?>                                        
                                                <?else:?>
                                                    <?if(!is_array($arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])):?>
                                                        <?=$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]?>
                                                    <?else:?>
                                                        <?=$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][0]?>
                                                    <?endif?>
                                                <?endif;?>
                                            <?endif;?>
                                        </div>
                                    </div>
                                <?else:
                                FirePHP::getInstance()->info($arItem["IDEALLY"]);
                                ?>
                                    <div class="prop">
                                        <div class="name">Идеально:</div>
                                        <?if(count($arItem["IDEALLY"])>1):?>
                                            <div class="value"><?=implode(", ",$arItem["IDEALLY"])?></div>
                                        <?else:?>
                                            <div class="value"><?=$arItem["IDEALLY"][0]?></div>
                                        <?endif?>
                                    </div>
                                    <div class="prop wsubway">
                                        <div class="subway">M</div>
                                        <div class="value">
                                            <?if(!is_array($arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])):?>
                                                <?=$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]?>
                                            <?else:?>
                                                <?=$arItem["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][0]?>
                                            <?endif?>
                                        </div>
                                    </div>
                                <?endif;?>                        
                            <?endif;?>
                        <?endforeach;?>



                    </div>
                    <div class="left pull-left">                        
                        <div class="order-buttons">
                            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=$arItem["NAME"]?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                            <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
                        </div>
                    </div>
                    <div class='clearfix'></div>
                </div>
                <?if ($num_key%2==1||end($arResult["ITEMS"])==$arItem):?>
                    <div class="clearfix"></div>
                    <div class="hr"></div>
                <?endif;?>
                <?$num_key++?>
            <?endforeach;?>
        </div>

    <div class="navigation_temp">
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>

    <script>
        $(function(){
            $('<div class="navigation"></div>').prependTo($('#footer .container'));

            $(".navigation").html($(".navigation_temp").html());
            $(".navigation_temp").remove();
        })
    </script>
<?else:?>
    <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
<?endif;?>