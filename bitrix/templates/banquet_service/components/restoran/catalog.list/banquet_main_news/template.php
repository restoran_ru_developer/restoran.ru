<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//print_r($arResult);?>
<?//print_r($arParams['ALL_NEWS_PARENT_SECTION_ID']);?>
<div class="three-pull-left-wrapper">
    <?$like_key=0;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>




    <div class="pull-left <?=($like_key%3==2)?"end":""?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
        </div>
        <?endif;?>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <?if ($arParams["REST_PROPS"]):?>            
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>                      
                </div>
                <div class="props">
                    <div class="prop">
                        <div class="name">Тип:</div>
                        <div class="value"><?=$arItem["TYPE"]?></div>
                    </div>

                    <div class="prop">
                        <div class="name">Кухни:</div>
                        <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                    </div>
                    <div class="prop">
                        <div class="name">Банкетный счет:</div>
                        <div class="value"><?=$arItem["BILL"]?></div>
                    </div>

                    <div class="prop">
                        <div class="name">Идеально:</div>
                        <?if(count($arItem["IDEALLY"])>1):?>
                            <div class="value"><?=implode(", ",$arItem["IDEALLY"])?></div>
                        <?else:?>
                            <div class="value"><?=$arItem["IDEALLY"][0]?></div>
                        <?endif?>
                    </div>

                    <div class="prop wsubway">
                        <div class="subway">M</div>
                        <div class="value"><?=$arItem["SUBWAY"]?></div>
                    </div>
                </div>
            <?else:?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <?endif;?>            

        </div>        
    </div>
    <?if(($like_key+1)%3==0 && $arItem!=end($arResult["ITEMS"])):?>
        </div><div class="three-pull-left-wrapper">
    <?endif?>
    <?$like_key++?>
<?endforeach;?>
    <?//if ($arItem==end($arResult["ITEMS"])):?>
        <div class="more_links text-right more-link-position-abs">
            <?if($arParams['BANQUET_INDEX_PAGE']=='Y'):?>
                <a href="<?=$arResult['THIS_ROOT_SECTION_PAGE_URL']?>" class="btn btn-light"><?=$arResult['THIS_ROOT_SECTION_NAME']?></a>
            <?else:?>
                <a href="<?=$arResult['THIS_ROOT_SECTION_PAGE_URL']?>" class="btn btn-light"><?=$arParams['ALL_NEWS_TITLE']?></a>
            <?endif?>
        </div>
    <?//endif;?>
</div>