<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_NAME" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_NAME"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
    "ALL_NEWS_TITLE" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_NEWS_ALL_TITLE"),
        "TYPE" => "TEXT",
        "DEFAULT" => GetMessage("T_IBLOCK_DESC_NEWS_ALL_TEXT"),
    ),
    "PARENT_NEWS_LIST" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_PARENT_NEWS_LIST"),
        "TYPE" => "TEXT",
        "DEFAULT" => '',
    ),
    "ALL_NEWS_PARENT_SECTION_ID" => Array(
        "NAME" => GetMessage("T_IBLOCK_DESC_ALL_NEWS_PARENT_SECTION_ID"),
        "TYPE" => "TEXT",
        "DEFAULT" => '',
    )

);
$arTemplateParameters['BANQUET_INDEX_PAGE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => GetMessage('CPT_BCSL_BANQUET_INDEX_PAGE'),
    'TYPE' => 'TEXT',
    'DEFAULT' => 'Y'
);
$arTemplateParameters['THIS_SECTION_IDEALLY'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CPT_BCSL_THIS_SECTION_IDEALLY'),
	'TYPE' => 'TEXT',
	'DEFAULT' => ''
);
?>
