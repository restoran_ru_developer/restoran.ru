<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="pull-left <?=($key%3==2)?"end":""?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
        </div>
        <?endif;?>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <?if ($arParams["REST_PROPS"]):?>            
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>                      
                </div>
                <div class="props">
                    <div class="prop">
                        <div class="name">Кухня:</div>
                        <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                    </div>
                    <div class="prop">
                        <div class="name">Средний счет:</div>
                        <div class="value"><?=$arItem["BILL"]?></div>
                    </div>                                        
                    <div class="prop wsubway">
                        <div class="subway">M</div>
                        <div class="value"><?=$arItem["SUBWAY"]?></div>
                    </div>
                </div>
            <?else:?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <?endif;?>            
            <?if ($arItem==end($arResult["ITEMS"])):?>
                <div class="more_links text-right">                    
                    <?if ($arParams["SORT_BY1"]=="PROPERTY_RATIO")
                          $aa = "ratio";
                      elseif($arParams["SORT_BY1"]=="show_counter")
                          $aa = "popular";
                      elseif($arParams["SORT_BY1"]=="PROPERTY_restoran_ratio")
                          $aa = "recomended";
                      else
                          $aa = "";
                      if ($aa):                          
                      
                    ?>                
                        <a href="/<?=CITY_ID?>/ratings/#<?=$aa?>" class="btn btn-light"><?=GetMessage("ALL_NEWS")?></a>
                    <?elseif($arItem["IBLOCK_TYPE_ID"] == "firms_news"):?>
                        <a href="http://restoran.ru/<?=CITY_ID?>/news/restoratoram/" class="btn btn-light"><?=GetMessage("ALL_NEWS")?></a>
                    <?elseif(substr_count($arResult["SECTION"]["PATH"][0]["CODE"], "newplace")):?>
                        <a href="<?=$arResult["SECTION"]["PATH"][0]["SECTION_PAGE_URL"]?>" class="btn btn-light"><?=GetMessage("ALL_NEWS1")?></a>
                    <?elseif ($arParams["NEW_REST"]):?>
                        <a href="/<?=CITY_ID?>/catalog/restaurants/all/?page=1&pageRestSort=new&by=desc" class="btn btn-light"><?=GetMessage("ALL_NEW_REST")?></a>
                    <?elseif($arParams["REST_PROPS"]=="Y"):?>
                        <a href="<?=$arResult["SECTION"]["PATH"][0]["SECTION_PAGE_URL"]?>" class="btn btn-light"><?=GetMessage("ALL_NEWS1")?></a>
                    <?elseif (!$arParams["NOLINK"]):?> 
                        <?if ($arParams["IBLOCK_TYPE"]=="cookery"&&$arParams["IBLOCK_ID"]==145):?>
                            <a class="btn btn-light" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_".$arParams["IBLOCK_TYPE"]."m")?></a>
                        <?else:?>
                            <a class="btn btn-light" href="<?=$arParams["LINK"]?>"><?=$arParams["LINK_NAME"]?></a>
                        <?endif;?>
                    <?endif;?>
                </div>
            <?endif;?>
        </div>        
    </div>
<?endforeach;?>