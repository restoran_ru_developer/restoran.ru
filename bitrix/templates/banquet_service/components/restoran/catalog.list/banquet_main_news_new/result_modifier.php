<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$this_section = end($arResult['SECTION']['PATH']);

$arResult['THIS_ROOT_SECTION_PAGE_URL'] = $this_section['SECTION_PAGE_URL'];
$arResult['THIS_ROOT_SECTION_NAME'] = $arParams['ALL_NEWS_TITLE'];

$arIB = getArIblock("catalog", CITY_ID);
foreach($arResult["ITEMS"] as $key=>$arItem) {

    $res = CIBlockElement::GetProperty($arIB["ID"], $arItem['PROPERTIES']['THIS_RESTAURANT']['VALUE'], "sort", "asc", array("CODE" => "sleeping_rest"));
    if ($ob = $res->Fetch())
    {
        if ($ob["VALUE"])
        {
            unset($arResult["ITEMS"][$cell]);
            continue;
        }
    }
    // resize images

    $res = CIBlockElement::GetByID($arItem['PROPERTIES2']['THIS_RESTAURANT'][0]);
    if($ar_res = $res->GetNext()){
        $arFile = CFile::GetFileArray($ar_res["DETAIL_PICTURE"]);
        if(!empty($arFile)) {

            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arFile, array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
        } else {
            $arFile = CFile::GetFileArray($ar_res["PREVIEW_PICTURE"]);
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arFile, array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
        }
        if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
        $arResult["ITEMS"][$key]['NAME'] = $ar_res['NAME'];
        $arResult["ITEMS"][$key]['DETAIL_PAGE_URL'] = $ar_res['DETAIL_PAGE_URL'];


        $res = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], "sort", "asc", array("CODE" => "RATIO"));
        if ($ob = $res->Fetch())
        {
            $arResult["ITEMS"][$key]['PROPERTIES']["RATIO"] = $ob['VALUE'];
        }
    }





    if($arItem["IBLOCK_TYPE_ID"] == "firms_news")
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("msk",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    }
    if (!$arItem["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    else
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //get props if it is a restaurant
    if ($arParams["REST_PROPS"]=="Y"):

        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
        while ($ob = $res->GetNext())
        {
//            $temp_val['kitchen'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
        if ($ob = $res->GetNext())
        {
//            $temp_val['average_bill'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
        if ($ob = $res->GetNext())
        {
//            $temp_val['subway'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["SUBWAY"] = $a['NAME'];
        }

        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "type"));
        if ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["TYPE"] = $a['NAME'];
        }








        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "IDEALLY"));

        while ($ob = $res->GetNext())
        {
//            $temp_val['kitchen'][$key][] = $ob['VALUE'];
            $r = CIBlockElement::GetByID($ob['VALUE']);

            if ($a = $r->Fetch()){

                if($a['ID']==$arParams['THIS_SECTION_IDEALLY']){    //  если совпадает с разделом
                    $arResult["ITEMS"][$key]["IDEALLY"][0] = $a['NAME'];
                }
                $temp_a[] = $a['NAME'];
            }
        }
        if(empty($arResult["ITEMS"][$key]["IDEALLY"]) && !empty($temp_a)){
            foreach($temp_a as $a_val){
                $arResult["ITEMS"][$key]["IDEALLY"][] = $a_val;
            }
        }

        unset($temp_a);

    endif;
}

?>