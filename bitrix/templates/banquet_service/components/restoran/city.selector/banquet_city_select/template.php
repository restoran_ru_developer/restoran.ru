<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="city_select">
    <div style="display: none">
        <?//print_r($arResult["ITEMS"]);?>
    </div>
    <?


    $cities = "";
    foreach($arResult["ITEMS"] as $arItem):
        if($arItem["CODE"]!='spb' && $arItem["CODE"]!='msk')
            continue;
        if (CITY_ID!=$arItem["CODE"]):
            if ($arItem["CODE"]=="msk")
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/banquet-service/">'.$arItem["NAME"].'</a></li>';
            elseif ($arItem["CODE"]=="spb")
            {
                if (SITE_ID=="s2")
                    $cities .= '<li><a href="http://en.restoran.ru/spb/banquet-service/">'.$arItem["NAME"].'</a></li>';
                else
                    $cities .= '<li><a href="http://'.$arItem["CODE"].'.restoran.ru/'.$arItem["CODE"].'/banquet-service/">'.$arItem["NAME"].'</a></li>';
            }            
            elseif($arItem["CODE"]=="tln") {}            
            elseif($arItem["CODE"]=="amt"&&CSite::InGroup(Array(1,23)))
            {
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'">'.$arItem["NAME"].'</a></li>';
            }
            elseif($arItem["CODE"]=="vrn"&&CSite::InGroup(Array(1)))
            {
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'">'.$arItem["NAME"].'</a></li>';
            }            
            elseif($arItem["CODE"]=="rga"||$arItem["CODE"]=="urm")
            {
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'">'.$arItem["NAME"].' <sup>New</sup></a></li>';
            }
            else
                $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'">'.$arItem["NAME"].'</a></li>';
        else:
            $city = $arItem["NAME"];
        endif;
    endforeach;
    if (SITE_ID=="s2")    
        $cities = str_replace("www.","en.",$cities);    
    ?>
    <a href="#" class="serif dropdown-toggle" data-toggle="dropdown"><?=$city?></a>
    <ul class="dropdown-menu">
        <?=$cities?>
    </ul>                        
</div>
