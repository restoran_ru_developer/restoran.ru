/**
 * Created by mac on 18.09.14.
 */

$(function() {
//              console.log($(window).scrollTop());
    $( "#datepicker" ).datepicker({
        showOn: "button",
        buttonImage: "img/reserve-calendar-button-bg.png",
        buttonImageOnly: true,
        buttonText: "Выберите дату",
        beforeShow: function(input, inst)
        {
            inst.dpDiv.css({marginTop: 0 + 'px', marginLeft: 0 + 'px'});
            //avoid_height = 349-234; //  115
            //if($(window).scrollTop()>avoid_height && $(window).scrollTop()<234){
            //    inst.dpDiv.css({marginTop: 0 + 'px', marginLeft: 0 + 'px'});
            //    console.log(1);
            //    console.log($(window).scrollTop());
            //}
            //else if($(window).scrollTop()<avoid_height){
            //    inst.dpDiv.css({marginTop: 0 + 'px', marginLeft: 0 + 'px'});
            //    console.log(1.5);
            //    console.log($(window).scrollTop());
            //}
            //else {
            //    inst.dpDiv.css({marginTop: 0 + 'px', marginLeft: 0 + 'px'});
            //    console.log(2);
            //    console.log($(window).scrollTop());
            //}
            //console.log($(window).scrollTop());

        }
    });
    $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
    $('#reserve-region').on('click', function(){
        $(this).parents('.reserve-region-field-wrapper').find('ul').toggleClass('active');
    });
    $('#reserve-reason').on('click', function(){
        $(this).parents('.reserve-region-field-wrapper').find('ul').toggleClass('active');
    });

    $('.reserve-region-field-wrapper ul li').on('click',function(){
        select_val = $(this).attr('select_value');
        select_name = $(this).text();
        $(this).parents('.reserve-region-field-wrapper').find('input[type="hidden"]').val(select_val);
        $(this).parents('.reserve-region-field-wrapper').find('.reserve-region-selected-value').text(select_name);
        $(this).parents('.reserve-region-field-wrapper').find('ul').toggleClass('active');
    });

    $('.more-button').on('click', function(){
        if(isNaN(parseInt($('input[name="reserve_guest_num"]').val())) || parseInt($('input[name="reserve_guest_num"]').val())<1){
            $('input[name="reserve_guest_num"]').val(1);
        }
        else {
            $('input[name="reserve_guest_num"]').val(parseInt($('input[name="reserve_guest_num"]').val())+1);
        }
    });
    $('.less-button').on('click', function(){
        if(isNaN(parseInt($('input[name="reserve_guest_num"]').val())) || parseInt($('input[name="reserve_guest_num"]').val())<1){
            $('input[name="reserve_guest_num"]').val(0);
        }
        else {
            $('input[name="reserve_guest_num"]').val(parseInt($('input[name="reserve_guest_num"]').val())-1);
        }
    });


});