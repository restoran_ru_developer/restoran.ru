$(function(){
    //To show hide element on click
    $(document).on('click', '[data-toggle=toggle]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#"+$(this).data("target")).toggle();               
        return false;        
    })
    //Slide to anchor position
    $(document).on('click', '[data-toggle=anchor]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var pos=$($(this).attr("href")).offset().top-10;
        $('html,body').animate({scrollTop:pos},"300");
        return false;        
    })
    //Scroll top top (animation?!) [always on page]
    $(".ontop").click(function(){
        //$("html,body").scrollTop(0);
        $('html,body').animate({scrollTop:0},"300");
    });
    //to send search form [always on page]
    $(".search_submit").click(function (){
        $(this).parents("form").submit();
    });
    //small galery on the best page of restaurants
    if (null!=$(".priority-slider")) 
    {
        $(".content").on("slide.bs.carousel",".priority-slider",function(e){        
            if (e.direction == "left")
            {
                var nextImage = $('.active.item', this).next('.item').find('img');
                if (null==nextImage.attr("src"))
                    nextImage.attr('src', nextImage.data('url'));
            }
            if (e.direction == "right")
            {
                if (!$('.active.item', this).prev('.item').length)
                {                
                    var nextImage = $('.item', this).last().find('img');
                    if (null==nextImage.attr("src"))
                        nextImage.attr('src', nextImage.data('url'));
                }
                else
                {
                    var nextImage = $('.active.item', this).prev('.item').find('img');
                    if (null==nextImage.attr("src"))
                        nextImage.attr('src', nextImage.data('url'));
                }
            }                
        });
    }
    //choose rating stars, when user choose rating need to focus text input
    if (null!=$(".choose-ratio"))
    {
        $(".choose-ratio span.glyphicon").click(
            function(){
                $("#review").focus();                               
                var count = $(this).index()+1;
                var i = 1;
                $("#input_ratio").attr("value",count);
                $(this).parent().find("span.glyphicon").each(function(){                
                    if (i<=count)
                        $(this).attr("class","glyphicon glyphicon-star");
                    else
                        $(this).attr("class","glyphicon glyphicon-star-empty");
                    i++;
                })
            }
        ); 
    }
    
    //show/hide big text    
    $(".content").on('click','a.toglletext',function(){
        $(this).parent().prev().toggleClass("hidden");
    });    
});