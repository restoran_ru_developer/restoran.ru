<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
<div class="clearfix"></div>
</div>
<div class="ontop icon-arrow-up"> наверх</div>
<div class="to-top-btn-bg"></div>
</div>
</div>

<div id="footer">
    <div class="container">
        <div class="first-footer-line">
            <div class="footer-logo-wrap">
                <?if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/"):?>
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="Ресторан.ру" />
                <?else:?>
                    <a href="/<?=CITY_ID?>/" alt="Ресторан.ру"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" /></a>
                <?endif;?>
            </div>
            <div class="footer-phone-wrap">
                <div class="phone-str">
                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=="msk"||CITY_ID=="spb"?"":"_rgaurm"?>.php" class="booking">
                        <?$APPLICATION->IncludeFile(
                            $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </a>
                </div>

                <div class="time-desc-to-call">Бронируем ежедневно с 09:00 до 23:00</div>
                <span class="vert-border-line"></span>
            </div>
            <div class="footer-order-link-wrap">
                <a href="/tpl/ajax/online_order_rest<?=CITY_ID=="msk"||CITY_ID=="spb"?"":"_rgaurm"?>.php" class="booking">
                    <span>Бронировать</span>
                    <span>бесплатно</span>
                </a>
            </div>
            <div class="footer-app-text-about">
                Приложение<br>Restoran.ru
            </div>
            <div class="footer-app-link-wrap">
                <a href="https://appsto.re/ru/yYEdfb.i" target="_blank">
                    <img src="/tpl/images/footer-app-link-logo.png" alt="Установить приложение"/>
                </a>
            </div>
        </div>
        <?
        $arSiteMenuIB = getArIblock("site_menu", CITY_ID);
        $APPLICATION->IncludeComponent("restoran:catalog.section.list", "bottom_site_menu-one-str", array(
            "IBLOCK_TYPE" => "site_menu",
            "IBLOCK_ID" => $arSiteMenuIB["ID"],
            "SECTION_ID" => "",
            "SECTION_CODE" => "",
            "COUNT_ELEMENTS" => "Y",
            "TOP_DEPTH" => "2",
            "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_URL" => "",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "36000011",
            "CACHE_GROUPS" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            'SORT_FIELD'=>'UF_FOOTER_SORT',
            'SORT_ORDER'=>'ASC'
        ),
            false
        );?>

        <div class="footer-age-inf">
            <div class="sixteen-copyright">16+</div>
            <div class="copyright-line">
                © <?=date("Y")?> Ресторан.Ru
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("include_areas/footer_copyright_3-n.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
        </div>
        <div class="footer-social-block">
            <a href="https://www.instagram.com/restoran_ru/" class="footer-social-inst-link" target="_blank"></a>
            <a href="<?=CITY_ID=='spb'?'https://vk.com/restoran_ru':'https://vk.com/restoranru'?>" class="footer-social-vk-link" target="_blank"></a>
            <a href="<?=CITY_ID=='spb'?'https://www.facebook.com/restoranspb/':'https://www.facebook.com/Restoran.ru/'?>" class="footer-social-f-link" target="_blank"></a>
        </div>
        <div class="clearfix"></div>
        <div class="comp_count">
            <ul class="companies">
                <li>
                    <noindex><a href="http://www.cakelabs.ru/portfolio/" rel="nofollow" target="_blank" class="cakelabs" alt="cakelabs.ru" title="<?=GetMessage('made_by_title')?>"><?=GetMessage('made_by')?></a>
                </li>
                <li>
                    <noindex><a rel="nofollow" target="_blank" class="sunmedia" href="http://www.sunmedia.ru" alt="sunmedia.ru" title="<?=GetMessage('seo_by_title')?>"><?=GetMessage('seo_by')?></a></noindex>
                </li>
            </ul>
            <div class="counters">
                <div class="pull-left" style="margin-top:5px;">
                    <noindex>
                        <!-- Rating@Mail.ru counter -->
                        <script type="text/javascript">
                            var _tmr = window._tmr || (window._tmr = []);
                            _tmr.push({id: "432665", type: "pageView", start: (new Date()).getTime()});
                            (function (d, w, id) {
                                if (d.getElementById(id)) return;
                                var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
                                ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
                                var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
                                if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
                            })(document, window, "topmailru-code");
                        </script><noscript><div style="position:absolute;left:-10000px;">
                                <img src="//top-fwz1.mail.ru/counter?id=432665;js=na" style="border:0;" height="1" width="1" alt="–ейтинг@Mail.ru" />
                            </div></noscript>
                        <!-- //Rating@Mail.ru counter -->
                        <!-- Rating@Mail.ru logo -->
                        <a href="http://top.mail.ru/jump?from=432665">
                            <img src="//top-fwz1.mail.ru/counter?id=432665;t=571;l=1"
                                 style="border:0;" height="40" width="88" alt="–ейтинг@Mail.ru" /></a>
                        <!-- //Rating@Mail.ru logo -->
                    </noindex>


                </div>
                <div class="pull-left" style="margin-left:10px;margin-top:5px;">
                    <noindex>
                        <div id="top100counter" style="float:left;"></div>
                        <p style="margin-bottom: 0; font-size:10px;"><a href="http://www.rambler.ru/" class="font10">Партнер «Рамблера»</a></p>
                        <script type="text/javascript">
                            var _top100q = _top100q || [];

                            _top100q.push(["setAccount", "2838823"]);
                            _top100q.push(["trackPageviewByLogo", document.getElementById("top100counter")]);


                            (function(){
                                var top100 = document.createElement("script"); top100.type = "text/javascript";

                                top100.async = true;
                                top100.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/pack/pack.min.js?3600000";
                                var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(top100, s);
                            })();
                        </script>
                    </noindex>
                </div>
                <div class="pull-left" style="margin-left:10px;margin-top:5px;">
                    <noindex>
                        <!--LiveInternet counter--><script type="text/javascript"><!--
                            document.write("<a href='http://www.liveinternet.ru/click' "+
                            "target=_blank><img src='//counter.yadro.ru/hit?t41.5;r"+
                            escape(document.referrer)+((typeof(screen)=="undefined")?"":
                            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                            ";"+Math.random()+
                            "' alt='' title='LiveInternet' "+
                            "border='0' width='31' height='31'><\/a>")
                            //--></script><!--/LiveInternet-->
                    </noindex>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="modal fade new-free-form<?//=$USER->IsAdmin()?'new-free-form':'';?>" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&#9587; <!--&times;--></span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="information" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                &nbsp;
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<?
setSeo();
?>
<?php $APPLICATION->IncludeFile("/tpl/include_areas/counters-from-muslim.php",Array(),Array("MODE"=>"php","NAME"=>"Счетчики"));?>
<?if ($_COOKIE["NEED_SELECT_CITY"]=="Y"):?>
    <script>
        if (!$("#city_modal").size())
        {
            $("<div class='popup popup_modal' id='city_modal' style='width:320px;padding-bottom:30px'></div>").appendTo("body");
            $('#city_modal').load('/tpl/ajax/city_select2.php?<?=bitrix_sessid_get()?>', function(data) {
                setCenter($("#city_modal"));
                showOverflow();
                $("#city_modal").fadeIn("300");
            });
        }
    </script>
    <?unset($_COOKIE["NEED_SELECT_CITY"]);?>
<?endif;?>
<div id="system_loading">Загрузка...</div>
</body>
</html>