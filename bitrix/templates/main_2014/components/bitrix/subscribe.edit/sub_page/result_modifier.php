<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["RUBRICS"] as $itemID => $itemValue) {
    // count subscribers
    $itemValue["CNT_SUBSCRIBERS"] = CRubric::GetSubscriptionCount($itemValue["ID"]);

    // get group name
    if(preg_match('/(.*)\{(.*)\}/is', $itemValue["NAME"], $aMatches)) {
        $itemValue["NAME"] = trim($aMatches[1]);
        $arResult["GROUP_LIST"][$aMatches[2]][] = $itemValue;
    } else {
        $arResult["GROUP_LIST"][$itemValue["NAME"]][] = $itemValue;
    }
}
?>