<?
$MESS["SONET_C29_T_SENDER"] = "Sender";
$MESS["SONET_C29_T_MESSAGE"] = "Message";
$MESS["SONET_C29_T_ACTIONS"] = "Actions";
$MESS["SONET_C29_T_FRIEND_REQUEST"] = "The user wants to list you as a friend.";
$MESS["SONET_C29_T_USER"] = "User";
$MESS["SONET_C29_T_INVITE"] = "invites you to the group";
$MESS["SONET_C29_T_DO_FRIEND"] = "Accept";
$MESS["SONET_C29_T_DO_DENY"] = "Decline";
$MESS["SONET_C29_T_DO_AGREE"] = "Accept";
$MESS["FRIENDS_TITLE"] = "My friends";
$MESS["RESTORATOR_STATE"] = "restorator";
$MESS["SEND_MSG"] = "Message";
$MESS["REST_INVITE"] = "Invite to restaurant";
$MESS["NO_FRIENDS"] = "This user has no friends";
$MESS["NO_YOU_FRIENDS"] = "You have not added any friends";
$MESS["NEW_REQUEST"] = "New applications";
?>