<?
$MESS["SONET_C29_T_SENDER"] = "Отправитель";
$MESS["SONET_C29_T_MESSAGE"] = "Сообщение";
$MESS["SONET_C29_T_ACTIONS"] = "Действия";
$MESS["SONET_C29_T_FRIEND_REQUEST"] = "Пользователь хочет добавить Вас в список своих друзей";
$MESS["SONET_C29_T_USER"] = "Пользователь";
$MESS["SONET_C29_T_INVITE"] = "приглашает Вас вступить в группу";
$MESS["SONET_C29_T_DO_FRIEND"] = "Добавить";
$MESS["SONET_C29_T_DO_DENY"] = "Отклонить";
$MESS["SONET_C29_T_DO_AGREE"] = "Принять";
$MESS["FRIENDS_TITLE"] = "Мои друзья";
$MESS["RESTORATOR_STATE"] = "ресторатор";
$MESS["SEND_MSG"] = "Сообщение";
$MESS["REST_INVITE"] = "Пригласить в ресторан";
$MESS["NO_FRIENDS"] = "У пользователя нет друзей";
$MESS["NO_YOU_FRIENDS"] = "Вы еще не добавили ни одного друга";
$MESS["NEW_REQUEST"] = "Новые заявки";
?>