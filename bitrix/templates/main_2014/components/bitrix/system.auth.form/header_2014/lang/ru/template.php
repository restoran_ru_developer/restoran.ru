<?
$MESS["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли свой пароль?";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_LOGOUT_BUTTON"] = "Выйти";
$MESS["AUTH_PROFILE"] = "Мой профиль";
$MESS["AUTH_A_INTERNAL"] = "Встроенная авторизация";
$MESS["AUTH_A_OPENID"] = "OpenID";
$MESS["AUTH_OPENID"] = "OpenID";
$MESS["AUTH_A_LIVEID"] = "LiveID";
$MESS["AUTH_LIVEID_LOGIN"] = "Log In";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REMEMBER_SHORT"] = "Запомнить меня";
$MESS["socserv_as_user_form"] = "Войти как пользователь:";
$MESS["AUTH_SECURE_NOTE"]="Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"]="Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
$MESS["LOGOUT"]="Выход";


$MESS["auth_profile_title"]="Профиль";
$MESS["auth_my_restaurants"]="Мои рестораны";
$MESS["auth_booking_statistic"]="Статистика бронирований";
$MESS["auth_blog_title"]="Блог";
$MESS["auth_messages_title"]="Сообщения";
$MESS["auth_job_title"]="Работа";
$MESS["auth_friends_title"]="Друзья";
$MESS["auth_bookings_title"]="Бронирования";
$MESS["auth_invitations_title"]="Приглашения";
$MESS["auth_subscriptions_title"]="Подписки";
$MESS["auth_reviews_title"]="Отзывы";
$MESS["auth_recipes_title"]="Рецепты";


?>