<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["FORM_TYPE"] == "login"):?>
    <?
    $arParamsToDelete = array(
        "login",
        "logout",
        "register",
        "forgot_password",
        "change_password",
        "confirm_registration",
        "confirm_code",
        "confirm_user_id",
        "logout_butt",
        "auth_service_id",
        "CITY_ID"
    );
    $currentUrl = $APPLICATION->GetCurPageParam("", $arParamsToDelete);
    ?>
<?else:?>
    <div class="header-auth">
        <?if($arResult["UNREAD"]>0){?><span class="unread"><?=$arResult["UNREAD"];?></span><?}?>
        <div class="avatar">
            <?if (CSite::InGroup(Array(9))):?>
                <a href="/restorator/" /><img src="<?=$arResult["USER_PHOTO"]['src']?>" /></a>
            <?else:?>
                <a href="/users/id<?=$USER->GetID()?>/?CITY_ID=<?=CITY_ID?>" /><img src="<?=$arResult["USER_PHOTO"]['src']?>" /></a>
            <?endif;?>
        </div>
        <div class="left">
            <a class="dropdown-toggle" data-toggle="dropdown"><?=($arResult["USER_LOGIN"]) ? substr($arResult["USER_LOGIN"],0,14) : substr($arResult["USER_NAME"],0,14)?> <span class="caret"></span></a>
            <?if (CSite::InGroup(Array(9)))://рестораторы?>
                <ul class="dropdown-menu speech-bubble-top text-center">
                    <li><a href="/restorator/profile.php"><?=GetMessage('auth_profile_title')?></a></li>
                    <li><a href="/restorator/"><?=GetMessage('auth_my_restaurants')?></a></li>
                    <li><a href="/restorator/bron_stat.php"><?=GetMessage('auth_booking_statistic')?></a></li>
                    <!--                    <li><a href="/restorator/opinions.php">Отзывы</a></li>-->
                    <li><a href="/restorator/blog/"><?=GetMessage('auth_blog_title')?></a></li>
                    <li><a href="/restorator/im.php"><?=GetMessage('auth_messages_title')?></a></li>
                    <li><a href="/restorator/work.php"><?=GetMessage('auth_job_title')?></a></li>

                    <li><a href="<?=$APPLICATION->GetCurPageParam("logout=yes");?>"><?=GetMessage("LOGOUT")?></a></li>
                </ul>
            <?else:?>
                <ul class="dropdown-menu speech-bubble-top text-center">
                    <li><a href="/users/id<?=$USER->GetID()?>/"><?=GetMessage('auth_profile_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/im/"><?=GetMessage('auth_messages_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/friends/"><?=GetMessage('auth_friends_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/bron/"><?=GetMessage('auth_bookings_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/invites/"><?=GetMessage('auth_invitations_title')?></a></li>
                    <!--                    <li><a href="/users/id--><?//=$USER->GetID()?><!--/subscribe/">--><?//=GetMessage('auth_subscriptions_title')?><!--</a></li>-->
                    <li><a href="/users/id<?=$USER->GetID()?>/blog/#blog.php"><?=GetMessage('auth_blog_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/blog/#response.php"><?=GetMessage('auth_reviews_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/blog/#recepts.php"><?=GetMessage('auth_recipes_title')?></a></li>
                    <li><a href="/users/id<?=$USER->GetID()?>/favorites/"><?=GetMessage('auth_favorites_title')?></a></li>
                    <li><a href="<?=$APPLICATION->GetCurPageParam("logout=yes");?>"><?=GetMessage("LOGOUT")?></a></li>
                </ul>
            <?endif;?>
        </div>
    </div>
<?endif?>