<?
$res = CUser::GetById($USER->GetID());
if ($uInfo = $res->GetNext())
{
	if($uInfo['PERSONAL_PROFESSION']!="") $arResult["USER_LOGIN"] = $uInfo['PERSONAL_PROFESSION'];
	 
    if ($uInfo['PERSONAL_PHOTO'])
    {
        $file = CFile::ResizeImageGet($uInfo['PERSONAL_PHOTO'], array('width'=>48, 'height'=>48), BX_RESIZE_IMAGE_EXACT  , true);                        
        //$file["src"] = CFile::GetPath($uInfo['PERSONAL_PHOTO']);                        
        $arResult['USER_PHOTO'] = $file;
    }
    else
    {        
        if ($uInfo['PERSONAL_GENDER']=="M")
            $arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/man_nnm_42.png";
        elseif($uInfo['PERSONAL_GENDER']=="M")
            $arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/woman_nnm_42.png";
        else
            $arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/unisx_nnm_42.png";
        //$file = CFile::ResizeImageGet("/tpl/images/noname/man_nnm.png", array('width'=>64, 'height'=>64), BX_RESIZE_IMAGE_PROPORTIONAL_ALT  , true);                        
        //$arResult['USER_PHOTO']["src"] = "/tpl/images/noname/new/man_nnm_42.png";
    }
    $arResult['USER_NAME'] = $USER->GetFullName();
    
	CModule::IncludeModule("socialnetwork");  
  	
  	$dbMessages = CSocNetMessages::GetList(
  		array("ID" => "DESC"),
		array("TO_USER_ID"=>$USER->GetID(),"MESSAGE_TYPE"=>SONET_MESSAGE_PRIVATE,"DATE_VIEW"=>false),
		false,
		false,
		array()
	);
	$arResult["UNREAD"]=0;
	while($arMessages = $dbMessages->GetNext()){
		$arResult["UNREAD"]++;
	}
  
  //	$res = CSocNetMessages::GetList(array("ID" => "DESC"), array("TO_USER_ID" => $USER->GetID()), array(), false, array("ID", "FROM_USER_ID", "TO_USER_ID", "MESSAGE"));
	///$messages = $res->arResult;
    //var_dump($messages);
}        
?>