<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["FORM_TYPE"] == "login"):?>
    <div class="ajax_form">
        <div class="title"><?=GetMessage("AUTH_TITLE")?></div>
    <script>
        function success_authorize(a,b)
        {
            if (!b)
            {                                
                location.reload();                
            }
        }
    </script>    
    <?
    if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
            ShowMessage($arResult['ERROR_MESSAGE']);
    ?>

    <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="/auth/?login=yes">
    <?/*if($arResult["BACKURL"] <> ''):?>
            <input type="hidden" name="backurl" value="/" />
    <?endif*/?>
    <?if($_REQUEST["backurl"]):?>
            <input type="hidden" name="backurl" value="<?=$_REQUEST["backurl"]?>" />
    <?endif?>
    <?foreach ($arResult["POST"] as $key => $value):?>
            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
    <?endforeach?>
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <div class="form-group">            
                <input type="email" class="form-control" placeholder="E-mail" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
            </div>            
            <div class="form-group end">
                <input class="form-control" placeholder="<?=GetMessage("AUTH_PASSWORD")?>" type="password" name="USER_PASSWORD" maxlength="255" />
            </div>

        <?

//        if($_SERVER['REMOTE_ADDR']=='31.28.10.18'):?>
            <script src="/tpl/js/jquery.checkbox.js"></script>
            <div class="remember-me">
                <span class="niceCheck" >
                    <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" style="display: none;">
                </span>
                <label for="USER_REMEMBER" class="niceCheckLabel" ><?=GetMessage("AUTH_REMEMBER_ME")?></label>
            </div>
<!--        --><?//endif?>
<!--            <div class="form-group">
                <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />
                <label for="USER_REMEMBER">&nbsp;<?=GetMessage("AUTH_REMEMBER_ME")?></label>                
            </div>     -->
            <?if ($arResult["CAPTCHA_CODE"]):?>
            <div class="question">
                <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
                <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                <div style="margin-bottom:5px;"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                <input class="inputtext" type="text" name="captcha_word" maxlength="50" value="" style="width:250px" />
            </div>
            <?endif?>           
            <div align="center">
                <input type="hidden" value="<?=GetMessage("AUTH_AUTHORIZE")?>" name="Login" /> 
                <input type="submit" class="btn btn-info btn-nb" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" style="width:100%"/>
            </div>
            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                <noindex>                    
                    <a class="forget" style="margin:0px;font-size:14px;" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>                                        
                </noindex>
            <?endif?>            
            <div class="dots"></div>
            <div class="orr"><?=GetMessage("AUTH_OR")?></div>
            <div class="italic"><?=GetMessage("AUTH_ALSO")?></div>
    </form>
    <script type="text/javascript">
        <?if (strlen($arResult["LAST_LOGIN"])>0):?>
        try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
        <?else:?>
        try{document.form_auth.USER_LOGIN.focus();}catch(e){}
        <?endif?>
    </script>
    <br /><br />
    <!--<div class="bx-auth-title">Войти как пользователь</div>-->    
    <?//$APPLICATION->AddHeadString('<link href="/bitrix/components/restoran/user.vkontakte_auth/style.css"  type="text/css" rel="stylesheet" />', true)?>
    <?//$APPLICATION->AddHeadScript('/bitrix/components/restoran/user.vkontakte_auth/script.js')?>
    <div class="pull-left">
        <?$APPLICATION->IncludeComponent("restoran:user.vkontakte_auth", ".default", array(
            ),
            false
        );?>
    </div>
    <?//$APPLICATION->AddHeadString('<link href="/bitrix/components/restoran/user.facebook_auth/style.css"  type="text/css" rel="stylesheet" />', true)?>
    <?//$APPLICATION->AddHeadScript('/bitrix/components/restoran/user.facebook_auth/script.js')?>
    <div class="pull-right">
        <?$APPLICATION->IncludeComponent("restoran:user.facebook_auth", ".default", array(
            ),
            false
        );?>
    </div>
    <div class="clearfix"></div>
    <br />
    <div class="reg">
        <div class="pull-left">
            <div class="italic" style="text-align:left;"><?=GetMessage("AUTH_REGISTER?")?></div>
        </div>
        <div class="pull-right" style="margin-top:8px;">            
            <a class="btn btn-info btn-nb-empty" href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>                                        
        </div> 
        <div class="clearfix"></div>
    </div>
    </div>
<?endif;?>