<?
$MESS["AUTH_PLEASE_AUTH"] = "Пожалуйста, авторизуйтесь:";
$MESS["AUTH_LOGIN"] = "Логин:";
$MESS["AUTH_PASSWORD"] = "Пароль:";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_AUTHORIZE"] = "Войти";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_REGISTER?"] = "Еще не с нами? <Br />
Присоединяйтесь!";
$MESS["AUTH_FIRST_ONE"] = "Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли свой пароль?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_TITLE"] = "Вход";
$MESS["AUTH_OR"] = "или";
$MESS["AUTH_ALSO"] = "через аккаунт социальных сетей:";
?>