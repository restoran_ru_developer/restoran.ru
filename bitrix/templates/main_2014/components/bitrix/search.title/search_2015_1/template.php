<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$search_in = array(
    'all'=>GetMessage("BSF_T_WHERE_ALL"),
    'rest'=>GetMessage("BSF_T_WHERE_SEARCH_REST"),
    'reviews'=>GetMessage("BSF_T_WHERE_SEARCH_REVIEWS"),
    'blog'=>GetMessage("BSF_T_WHERE_SEARCH_BLOG"),
    'news'=>GetMessage("BSF_T_WHERE_SEARCH_NEWS"),
    'recipe'=>GetMessage("BSF_T_WHERE_SEARCH_RECIPE")
);
?>
<noindex>
    <form  action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form" class="form-inline form_main_2015_summer_1" role="form" >
        <div class="form-group on-map-button-wrap">
            <a href="<?if((CITY_ID=='msk'||CITY_ID=='rga')&&LANGUAGE_ID!='en'):?>https://www.restoran.ru/<?=CITY_ID?>/map/near/<?elseif(CITY_ID=='spb'&&LANGUAGE_ID!='en'):?>https://spb.restoran.ru/<?=CITY_ID?>/map/near/<?elseif(LANGUAGE_ID=='en'):?>https://en.restoran.ru/<?=CITY_ID?>/map/near/<?else:?>/<?=CITY_ID?>/map/near/<?endif?>" class="btn btn-info on_map "><span class="<?=LANGUAGE_ID?>"><?=GetMessage("NEAR_YOU")?></span></a>
        </div>

        <div class="form-group search_box near-in-list">

            <span class="how-search-title">Поиск</span>
            <input type="hidden" id="search_in" name="search_in" value="<?=($arParams["COOKERY"])?"recipe":($_REQUEST['search_in']?$_REQUEST['search_in']:"all")?>">
            <?if (CITY_ID=="spb"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_spb")?>" onspeechchange="startSearch" />
            <?elseif (CITY_ID=="ast"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_ast")?>" onspeechchange="startSearch" />
            <?elseif (CITY_ID=="kld"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_kld")?>" onspeechchange="startSearch" />
            <?elseif (CITY_ID=="rga"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_rga")?>" onspeechchange="startSearch" />
            <?else:?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" onspeechchange="startSearch" />
            <?endif;?>
            <a class="search_submit"><span><?=GetMessage("search_2014_search_title")?></span></a>
        </div>

        <div class="form-group on-map-button-wrap">
            <a href="<?if((CITY_ID=='msk'||CITY_ID=='rga')&&LANGUAGE_ID!='en'):?>https://www.restoran.ru/<?=CITY_ID?>/catalog/restaurants/all/distance/<?elseif(CITY_ID=='spb'&&LANGUAGE_ID!='en'):?>https://spb.restoran.ru/<?=CITY_ID?>/catalog/restaurants/all/distance/<?elseif(LANGUAGE_ID=='en'):?>https://en.restoran.ru/<?=CITY_ID?>/catalog/restaurants/all/distance/<?else:?>/<?=CITY_ID?>/catalog/restaurants/all/distance/<?endif;?>" class="btn btn-info on_map near-in-list"><span class="<?=LANGUAGE_ID?>"><?=GetMessage("NEAR_YOU_IN_LIST")?></span></a>
        </div>
    </form>
</noindex>
<script>
    var lang_rest ="<?=GetMessage('SEARCH_TITLE_SEARCH_2014_RESTS')?>";
    var lang_blogs ="<?=GetMessage('SEARCH_TITLE_SEARCH_2014_BLOGS')?>";
    var lang_articles ="<?=GetMessage('SEARCH_TITLE_SEARCH_2014_ARTICLES')?>";
    var lang_recipe ="<?=GetMessage('SEARCH_TITLE_SEARCH_2014_RECIPE')?>";
    var lang_firms ="<?=GetMessage('SEARCH_TITLE_SEARCH_2014_FIRMS')?>";
    var lang_id="<?=LANGUAGE_ID?>";

    $(function(){
        $('.search-by-params').on('click',function(){
            $('#rest_filter_form').toggleClass('active');
        })
    })
</script>