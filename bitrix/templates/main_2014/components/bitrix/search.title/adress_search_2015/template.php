<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<noindex>
    <form  action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form" class="form-inline form_main_2015_summer_1" role="form"  id="map_adres">
        <div class="form-group on-map-button-wrap">
            <a href="<?if((CITY_ID=='msk'||CITY_ID=='rga')&&LANGUAGE_ID!='en'):?>https://www.restoran.ru/<?=CITY_ID?>/map/near/<?elseif(CITY_ID=='spb'&&LANGUAGE_ID!='en'):?>https://spb.restoran.ru/<?=CITY_ID?>/map/near/<?elseif(LANGUAGE_ID=='en'):?>https://en.restoran.ru/<?=CITY_ID?>/map/near/<?else:?>/<?=CITY_ID?>/map/near/<?endif?>" class="btn btn-info on_map "><span class="glyphicon glyphicon-map-marker <?=LANGUAGE_ID?>"><?=GetMessage("NEAR_YOU")?></span></a>
        </div>
        <div class="form-group search_box">
<!--            <span class="how-search-title">найти по названию</span>-->
            <div class="dropdown">
                <a data-toggle="dropdown" id="search_in_title" style=""><span class="t"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></span> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a data-value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></a></li>
                    <li><a data-value="adres"><?=GetMessage("BSF_T_WHERE_SEARCH_ADRES")?></a></li>
                </ul>
                <input type="hidden" id="search_in" name="search_in" value="rest">
            </div>
            <?if (CITY_ID=="spb"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_spb")?>" onspeechchange="startSearch" />
            <?elseif (CITY_ID=="ast"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_ast")?>" onspeechchange="startSearch" />
            <?elseif (CITY_ID=="kld"):?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_kld")?>" onspeechchange="startSearch" />
            <?else:?>
                <input class="search_input" x-webkit-speech="" speech="" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" onspeechchange="startSearch" />
            <?endif;?>
            <a class="search_submit"><span class=""><?=GetMessage("address_search_2014_search_title")?></span></a>
        </div>
    </form>
</noindex>