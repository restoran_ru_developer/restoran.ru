<?
$MESS ['BSF_T_SEARCH_BUTTON'] = "FIND";
$MESS ['BSF_T_WHERE_SEARCH_REST'] = "by restaurants";
$MESS ['BSF_T_WHERE_SEARCH_ADRES'] = "by address";
$MESS ['BSF_T_WHERE_SEARCH_NEWS'] = "by news";
$MESS ['BSF_T_WHERE_SEARCH_KUPONS'] = "by coupon";
$MESS ['BSF_T_WHERE_SEARCH_AFISHA'] = "by activities";
    $MESS ['BSF_T_WHERE_SEARCH_RECIPE'] = "by recipes";
$MESS ['BSF_T_WHERE_SEARCH_BLOG'] = "by blogs";
$MESS ['BSF_T_WHERE_SEARCH_OVERVIEWS'] = "by reviews";
$MESS ['BSF_T_WHERE_SEARCH_INTERVIEWS'] = "by interview";
$MESS ['BSF_T_WHERE_SEARCH_MASTER_CLASS'] = "by master classes";
$MESS ['BSF_T_WHERE_ALL'] = "through all site";
$MESS ['BSF_T_SEARCH_EXMP_STRING'] = "For example, a restaurant Moscow";
$MESS ['CUISINE_TITLE'] = "Kitchen";
$MESS ['AVERAGE_BILL_TITLE'] = "Average bill";
$MESS ['SUBWAY_TITLE'] = "Subway";
$MESS ['REST_TYPE_TITLE'] = "Type";
$MESS ['CHOOSE_BUTTON'] = "CHOOSE";
$MESS ['NEAR_YOU'] = "Restaurants<br /> on map";
$MESS ['address_search_2014_search_title'] = "Search";
?>