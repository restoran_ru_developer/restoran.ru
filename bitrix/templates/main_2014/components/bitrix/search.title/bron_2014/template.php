<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=$templateFolder?>/script.js"></script>
<?
if($_REQUEST['network_list']){
    //$(".chzn-select2").chosen();
    $this_rest_id = ($_REQUEST["form_text_44"])?$_REQUEST["form_text_44"]:$_REQUEST["id"];
    $arIB = getArIblock("catalog", CITY_ID);
    $db_props = CIBlockElement::GetProperty($arIB['ID'], $this_rest_id, array("sort" => "asc"), Array("CODE"=>"REST_NETWORK"));
    while($ar_props = $db_props->Fetch()){


        $res = CIBlockElement::GetByID($ar_props['VALUE']);
        if($ar_res = $res->GetNext()){

            $db_props_addr = CIBlockElement::GetProperty($arIB['ID'], $ar_props['VALUE'], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props_addr = $db_props_addr->Fetch()){
                $network_rest_list[$ar_props['VALUE']]['ADDRESS'] = $ar_props_addr['VALUE'];
            }

            $network_rest_list[$ar_props['VALUE']]['NAME'] = $ar_res['NAME'];
        }
    }
?>
    <ul style="list-style: none;padding: 0;margin: 0;">
        <li class="item-row">
            <p class="inpwrap">
                <select data-placeholder="Выберите ресторан сети" class="chzn-select2 network-select" style="width: 459px" name="" >
                    <option> </option>
                    <? foreach ($network_rest_list as $networkRestKey => $networkRestName):?>
                        <option value="<?= $networkRestKey ?>" addr_val="<?= $networkRestName['ADDRESS'] ?>"><?= $networkRestName['NAME'] ?></option>
                    <? endforeach?>
                </select>
            </p>
        </li>
    </ul>
    <link href="/tpl/css/chosen/chosen.css"  rel="stylesheet" />
    <script src="/tpl/css/chosen/chosen.jquery.js"></script>
    <style>
        .chzn-container.chzn-container-single {
            width: 459px;
            border-radius: 0;
            border: 1px solid #e3e3e3;
            height: 40px;
            border-left: 0;
        }
        .chzn-container.chzn-container-single > a {
            height: 38px;
            line-height: 40px;
            background: white;
            border-radius: 0;
            border: none;
            width: 458px;
            background: url('/tpl/css/chosen/arrow.png') no-repeat 98% 50%;
            background-color: #fff;
            border-left: 1px solid #e3e3e3;
        }
        .chzn-container-single .chzn-single div {
            position: absolute;
            right: 0;
            top: 0;
            height: 100%;
            width: 18px;
            display: none;
        }
        .chzn-container-active.chzn-container.chzn-container-single .chzn-drop {
            /*left: -1px !important;*/
            width: 459px;
            top: 38px !important;
            border: 1px solid #e3e3e3;
            border-top: 0;
            margin: 0 !important;
            padding: 0;
        }
        .chzn-container.chzn-container-single .chzn-drop .chzn-search {
            display: none;
        }
    </style>
    <script>
        $(".network-select").chosen().change(function(s){
            $('#top_search_input1').val($('.network-select option:selected').text())
            $('#top_search_name').val($('.network-select option:selected').text())
            $('#top_search_rest:not(.news-content-form-field)').val($('.network-select option:selected').val())
            $('input[name="rest_title"]:not(.news-content-form-field)').val($('.network-select option:selected').text())
            $('#top_search_adr').val($('.network-select option:selected').attr('addr_val'))
        });
    </script>
    <?
    unset($_REQUEST["name"]);
    unset($_REQUEST["id"]);
    ?>
    <div style="display: none">
<?}?>
    <input type="hidden" name="rest_title" value="<?=$_REQUEST["name"]?>" />
<?
if (CITY_ID=="msk"||CITY_ID=="spb"||CITY_ID=="nsk"):?>
    <input id="top_search_input1" class="search_rest form-control" AUTOCOMPLETE="false" name="form_text_32" type="text" value="<?=($_REQUEST["form_text_32"])?$_REQUEST["form_text_32"]:$_REQUEST["name"]?>" placeholder="<?=GetMessage('REST_NAME')?>"/>
    <input id="top_search_rest" type="hidden" value="<?=($_REQUEST["form_text_44"])?$_REQUEST["form_text_44"]:$_REQUEST["id"]?>" name="<?=$arParams["NAME"]?>"/>
    <input id="top_search_adr" type="hidden" value="<?=($_REQUEST["rest_adr"])?$_REQUEST["rest_adr"]:""?>" name="rest_adr"/>
    <input id="top_search_name" type="hidden" value="<?=($_REQUEST["rest_name"])?$_REQUEST["rest_name"]:""?>" name="rest_name"/>
<?else:?>
    <input id="top_search_input1" class="search_rest form-control" AUTOCOMPLETE="false" name="<?=$arParams["NAME2"]?>" type="text" value="<?=($_REQUEST[$arParams["NAME2"]])?$_REQUEST[$arParams["NAME2"]]:$_REQUEST["name"]?>" placeholder="<?=GetMessage('REST_NAME')?>"/>
    <input id="top_search_rest" type="hidden" value="<?=($_REQUEST[$arParams["NAME"]])?$_REQUEST[$arParams["NAME"]]:$_REQUEST["id"]?>" name="<?=$arParams["NAME"]?>"/>
<? endif; ?>
<?if($_REQUEST['network_list']):?>
    </div>
<?endif?>