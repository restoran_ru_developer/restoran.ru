<?
$MESS["BSF_T_SEARCH_BUTTON"] = "SEARCH";
$MESS["BSF_T_WHERE_SEARCH_REST"] = "by restaurants";
$MESS["BSF_T_WHERE_SEARCH_NEWS"] = "by news";
$MESS["BSF_T_WHERE_SEARCH_KUPONS"] = "coupon";
$MESS["BSF_T_WHERE_SEARCH_AFISHA"] = "on activities";
$MESS["BSF_T_WHERE_SEARCH_RECIPE"] = "by prescription";
$MESS["BSF_T_WHERE_SEARCH_BLOG"] = "by blogs";
$MESS["BSF_T_WHERE_SEARCH_OVERVIEWS"] = "by reviews";
$MESS["BSF_T_WHERE_SEARCH_INTERVIEWS"] = "by interview";
$MESS["BSF_T_WHERE_SEARCH_MASTER_CLASS"] = "by master classes";
$MESS["BSF_T_WHERE_ALL"] = "the entire site";
$MESS["BSF_T_SEARCH_EXMP_STRING"] = "For example, a restaurant Moscow";
$MESS["CUISINE_TITLE"] = "Kitchen";
$MESS["AVERAGE_BILL_TITLE"] = "Average bill";
$MESS["SUBWAY_TITLE"] = "Subway";
$MESS["REST_TYPE_TITLE"] = "Type";
$MESS["CHOOSE_BUTTON"] = "CHOOSE";
$MESS["REST_NAME"] = "Restaurant name";
?>