$(document).ready(function() {
    $(".search_input").autocomplete("/search/index_suggest.php", {
        max: 50,
        minChars: 3,
        delay:500,
        matchSubset: 0,
        selectFirst:false,
        formatItem: function(data, i, n, value) {

            // 1
            //if(lang_id!='en'){
                if(value.split("###")[3]=='rest'){
                    return "<div class='search-category-title-wrapper' >"+lang_rest+":</div><a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "<span>"+value.split("###")[4]+"</span>"+"</a>";
                }
                else if(value.split("###")[3]=='firms'){
                    return "<hr><div class='search-category-title-wrapper' >"+lang_firms+":</div><a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "</a>";
                }
                else if(value.split("###")[3]=='blogs'){
                    return "<hr><div class='search-category-title-wrapper' >"+lang_blogs+":</div><a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "</a>";
                }
                else if(value.split("###")[3]=='news'){
                    return "<hr><div class='search-category-title-wrapper' >"+lang_articles+":</div><a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "</a>";
                }
                else if(value.split("###")[3]=='recipe'){
                    return "<hr><div class='search-category-title-wrapper' >"+lang_recipe+":</div><a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "</a>";
                }

                else if(value.split("###")[3]=='address') {
                    return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "<span>"+value.split("###")[4]+"</span>"+"</a>";
                }
                else if(value.split("###")[1]=='last') {
                    return "<a class='suggest_res_url last' href='" + value.split("###")[2] + "'><img src='/tpl/images/search/ico_autosearch_more.png' width='15' height='15' style='margin-left: 12px;margin-right: 17px;margin-top: 4px;'> " + value.split("###")[0] +"</a>";
                }
                else {
                    return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'><img src='"+(value.split("###")[2]?value.split("###")[2]:"/tpl/images/search/preview-search.jpg")+"' width='40' height='40' > " + value.split("###")[0] + "</a>";
                }
            //}
            //else {
            //    return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
            //}
        },
        formatResult: function(data, value) {
            return value.split("###")[0].split("[")[0];
        },
        extraParams: {
            search_in: function() { return $('#search_in').val(); }
        },
        scrollHeight:300
    });




    $(".search_input").result(function(event, data, formatted) {
        if (data) {
            location.href = value.split("###")[1];
        }
    });
});