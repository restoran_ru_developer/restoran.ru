<?
$MESS["FORM_REQUIRED_FIELDS"] = "обязательно для заполнения";
$MESS["FORM_NOT_REQUIRED_FIELDS"] = "не обязательно";
$MESS["FORM_APPLY"] = "Применить";
$MESS["FORM_ADD"] = "Добавить";
$MESS["FORM_ACCESS_DENIED"] = "Не хватает прав доступа к веб-форме.";
$MESS["FORM_DATA_SAVED1"] = "Спасибо!<br><br>Ваша заявка №";
$MESS["FORM_DATA_SAVED2"] = " принята к рассмотрению.";
$MESS["FORM_MODULE_NOT_INSTALLED"] = "Модуль веб-форм не установлен.";
$MESS["FORM_NOT_FOUND"] = "Веб-форма не найдена.";
$MESS["I_WANT"] = "Я хочу";
$MESS["IN_RESTORAN"] = "Ресторан";
$MESS["NA"] = "Дата брони";
$MESS["AT"] = "Время";
$MESS["OUR_COMPANY"] = "Кол-во человек";
$MESS["PEOPLE"] = "Персон";
$MESS["HOW_MUCH_BUY"] = "Бюджет, €/ч";
$MESS["HOLIDAY"] = "Праздник под ключ";
$MESS["MY_NAME"] = "Фамилия и имя";
$MESS["FOR_FEEDBACK"] = "Для обратной связи я оставлю ";
$MESS["MY_PHONE"] = "Телефон";
$MESS["MY_EMAIL"] = "E-mail:";
$MESS["MY_WISH"] = "Пожелание к брони";
$MESS["CAPTCHA"] = "Подтверждаю, я не робот, на картинке написано:";
$MESS["NON_SMOKING_HALL"] = "Некурящий зал";
$MESS["SMOKING_HALL"] = "Курящий зал";
$MESS["YES"] = "Да";
$MESS["NO"] = "Нет";
$MESS["RESERVE"] = "Забронировать";
$MESS["RESERVE_TABLE"] = "Забронировать банкет бесплатно";
$MESS["RESERVE_BANKET"] = "Заказать банкет";
$MESS["BRONE_TXT_spb"] = "бронировать <span>бесплатно</span> по телефону: +7 (812) 740-18-20";
$MESS["BRONE_TXT_msk"] = "бронировать <span>бесплатно</span> по телефону: +7 (495) 988-26-56";
$MESS["BRONE_TXT_rga"] = "бронировать <span>бесплатно</span> по телефону: +371 661 031 06";
$MESS["BRONE_TXT_urm"] = "бронировать <span>бесплатно</span> по телефону: +371 661 031 06";
$MESS["BRONE_TXT_KLD"] = "© Restoran.ru. <span>Услуга <b>абсолютно бесплатна</b>.</span><Br /> <span class='phone'>Вы также можете забронировать по телефону:<Br /> +7 (952) 110-75-55</span>";
$MESS["BRONE_TXT_UFA"] = "© Restoran.ru. <span>Услуга <b>абсолютно бесплатна</b>.</span><Br /> <span class='phone'>Вы также можете забронировать по телефону:<Br /> +7 (964) 955-55-00</span>";
$MESS["BRONE_TXT_"] = "© Restoran.ru. <span>Услуга <b>абсолютно бесплатна</b>.</span><Br /><span class='phone'>Вы также можете забронировать по телефону:<Br /> +7 (495) 988-26-56</span>";
$MESS["F_PER"] = "<span class='rouble'>e</span><span class=''>/чел.</span>";
$MESS["F_ALC"] = "Свой алкоголь";
$MESS["F_EVENT"] = "Event-услуги";
$MESS["F_BUDGET"] = "Бюджет";
$MESS["CHEK_TIME"] = "Время";
$MESS["CHEK_PERSONS"] = "Кол-во человек";
$MESS["SERVICE_FREE"] = "Услуга предоставляется <b>бесплатно</b>";
$MESS["REST_NAME"] = "Название ресторана";

$MESS["CALLBACK_thanks_text"] = "Спасибо. Свяжемся с вами буквально через пару минут.";
$MESS["CALLBACK_no_want_fill"] = "Не хотите заполнять?";
$MESS["CALLBACK_we_will_make"] = "Мы с удовольствием сделаем это за вас. <span>И ресторан забронируем, разумеется, бесплатно!</span><br>
                        Подтверждение брони по смс через 5 минут.";
$MESS["CALLBACK_bron_service"] = "Служба бронирования:";
$MESS["CALLBACK_our_phone_text"] = "<span>Ваш телефон</span><br>
                            для обратной связи";
$MESS["CALLBACK_call_me"] = "перезвонить";
$MESS["CALLBACK_enter_phone_num"] = "Введите номер телефона";
?>