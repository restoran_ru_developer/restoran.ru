<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
    <script>
        $(document).ready(function(){
            var datePicker = $('.datepicker').datepicker({
                format:'dd.mm.yyyy',
                weekStart:1
            }).on('changeDate', function(ev){
                //$('.datepicker').datepicker('hide');

                if($('.today-tomorrow-select option[value="'+ev.currentTarget.value+'"]').length>0){
                    $('.today-tomorrow-select option[value="'+ev.currentTarget.value+'"]').attr('selected',true);
                }
                else {
                    $('.in-select-for-date').text(ev.currentTarget.value).attr('selected',true).show();
                    $('.in-select-for-date').val(ev.currentTarget.value);
                }

            });

            <?if(substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
            $('#date').on('change',function(){
                if($('.today-tomorrow-select option[value="'+$(this).val()+'"]').length>0){
                    $('.today-tomorrow-select option[value="'+$(this).val()+'"]').attr('selected',true);
                }
                else {
                    $('.in-select-for-date').text($(this).val()).attr('selected',true).show();
                    $('.in-select-for-date').val($(this).val());
                }
            })
            <?endif?>

            $('.today-tomorrow-select').on('change',function(event){
                this_selected_opt = $(this).find('option:selected');
                $('#date').val(this_selected_opt[0].value);
                $('#date').attr('data-date',this_selected_opt[0].value);
                <?if(!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
                $('.datepicker').datepicker('update', this_selected_opt[0].value);
                <?endif?>
                $('.in-select-for-date').hide();
            })

            $('.wishes-list-wrapper li').on('click touchstart', function(){
                $('.wishes-input').val($(this).text());
                $('.wishes-list-wrapper').removeClass('active');
                $('.wishes-input').focus();
            });

            $('.wish-list-new-trigger').hover(
                function(){
                    $('.wishes-list-wrapper').addClass('active');
                },
                function(){
                    $('.wishes-list-wrapper').removeClass('active');
                }
            )
            $('.wishes-list-wrapper').hover(
                function(){
                    $('.wishes-list-wrapper').addClass('active');
                },
                function(){
                    $('.wishes-list-wrapper').removeClass('active');
                }
            )

            <?if(CITY_ID=='rga'):?>
                $(".phone1").maski("9999999999?9999999999",{placeholder:""});
            <?else:?>
                $(".phone1").maski("+7(999) 999-99-99",{placeholder:""});
            <?endif?>

            $("#time").maski("99 : 99",{placeholder:""});

            $(".up").click(function(){
                $("#company").val($("#company").val()*1+1);
            });
            $(".down").click(function(){
                if ($("#company").val()*1>1)
                    $("#company").val($("#company").val()*1-1);
            });


            $("#order_online form").submit(function(){
                var err = "";
                if (!$("#top_search_rest").val())
                {
                    err = "<?=GetMessage('choose_rest_js')?><br />";
                }
                if (!$("#time").val())
                {
                    err = err+"<?=GetMessage('choose_time_js')?><br/>";
                }

                if (!$("#company").val()||$("#company").val().replace(/\D/g,'')==0)
                {
                    err = err+"<?=GetMessage('choose_people_num_js')?><br/>";
                }
                expr = /(.+) (.+)/;
                if (!$("#name").val()||!expr.test($("#name").val()))
                {
                    err = err+"<?=GetMessage('sign_name_js')?><br/>";
                }

                if (!$(".phone1").val())
                {
                    err = err+"<?=GetMessage('sign_phone_js')?>";
                }
                if (err)
                {
                    $(".errors").html(err+"<br /><Br />");
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url:'/tpl/ajax/online_order_rest_floating_form.php',
                    data: $(this).serialize()

                }).done(function(data){
                    var a =$(data).find(".ok").html();
                    if (a)
                    {
                        $("#order_online form").after("<h3 class='text-center'>"+a+"</h3>");
                        $("#order_online form").remove();

//                        yaCounter17073367.reachGoal('BRON');
                    }
                    else
                    {
                        alert("<?=GetMessage('error_happened_js')?>")
                    }
                });
                return false;
            });
        });
    </script>
<?
//if ($arResult["isFormNote"] != "Y") {     
?><?/*=bitrix_sessid();*/?>
<?$datehash = date("Ymd-His");?>
<?
if ($_REQUEST["name"] )
$_REQUEST["name"] = rawurldecode($_REQUEST["name"]);
if (!$_REQUEST["person"])
$_REQUEST["person"] = 0;
//if ($_REQUEST["banket"]=="Y")
//    $_REQUEST["person"] = 10;
if (!$_REQUEST["time"])
$_REQUEST["time"] = date("H   i");
?>
<link href="<?= $templateFolder ?>/style.css?<?=md5($datehash)?>"  type="text/css" rel="stylesheet" />
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <?
if (CITY_ID=='spb'){
$phone_to_call = '88127401820';
$phone_to_front = '+7 (812) 740-18-20';
}
elseif (CITY_ID=='rga'||CITY_ID=='urm'){
$phone_to_call = '37166103106';
$phone_to_front = '+371 661-031-06';
}
else {
$phone_to_call = '84959882656';
$phone_to_front = '+7 (495) 988-26-56';
}
?>

<div id="order_online" >
    <div class="form-full-content no-need-to-hide">
        <?//endif?>
<? endif; ?>

        <div class="title"><?=GetMessage("RESERVE_TABLE")?></div>

        <?= $arResult["FORM_HEADER"] ?>
            <div class="errors"></div>

        <?foreach ($arResult["QUESTIONS"] as $key => $question):
        $ar = array();
        $ar["CODE"] = $key;
        $questions[] = array_merge($question, $ar);
        endforeach;?>

        <div class="form-rest-name-wrap">
            <?
            $res = CIBlockElement::GetByID($_REQUEST['id']);
            $ar_res = $res->Fetch();
            $_REQUEST['NAME'] = $ar_res['NAME'];
            ?>
            <span class="in-rest-title"><?=GetMessage('in_rest')?></span><div class="form-rest-name"><?=$ar_res['NAME']?></div>

            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
            <input type="hidden" name="rest_title" value="<?=$ar_res['NAME']?>">
            <input class="search_rest form-control" AUTOCOMPLETE="false" name="form_text_32" type="text" value="<?=$ar_res['NAME']?>" style="display: none"/>
            <input id="top_search_rest" type="hidden" value="<?=$_REQUEST["id"]?>" name="<?="form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]?>"/>
            <?else:?>
            <input id="" class="search_rest form-control" AUTOCOMPLETE="false" name="<?="form_" . $questions[1]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[1]["STRUCTURE"][0]["ID"]?>" type="text" value="<?=$ar_res['NAME']?>" style="display: none"/>
            <input id="top_search_rest" type="hidden" value="<?=($_REQUEST["form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]])?$_REQUEST["form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]]:$_REQUEST["id"]?>" name="<?="form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]?>"/>
            <? endif; ?>
        </div>

        <? if (LANGUAGE_ID == 'ru'): ?>
                                    <?
        $s = $arResult["arrVALUES"]["form_dropdown_SIMPLE_QUESTION_547"];
        if (!$s)
        $s = $_REQUEST["form_dropdown_SIMPLE_QUESTION_547"];


        ?>
                <?if (!$s):?>
        <input type="hidden" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29">
        <?else:?>
        <input type="hidden" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="<?=$s?>">
        <?endif;?>
            <? endif; ?>
            <? if (LANGUAGE_ID == 'en'): ?>
        <input type="hidden" id="what_question_" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29" />
        <? endif; ?>
        <div class="form-group today-tomorrow-wrapper">
            <?
            if (!$_REQUEST["date"]){
            $_REQUEST["date"] = substr_count($_SERVER["HTTP_USER_AGENT"],"iPad") ? date("Y-m-d") : date("d.m.Y");
            }
            ?>
            <input <? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'type="text"'; ?> <? if (substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'readonly="true" type="date" '; ?>
                class="<? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'datepicker'; ?> form-control"
                id="date"
                data-date="<?= substr_count($_SERVER["HTTP_USER_AGENT"], "iPad") ? date("Y-m-d") : date("d.m.Y") ?>"
                value="<? echo ($_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]] : $_REQUEST["date"] ?>"
                name="form_<?= $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[2]["STRUCTURE"][0]["ID"] ?>"/>

            <?
            $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/tpl/include_areas/order-form-today-tomorrow.php",
            "EDIT_TEMPLATE" => ""
            ),
            false
            );
            ?>
        </div>

        <div class="form-group pull-left">
            <div class="like-half-part-form-fields">
                <label for="time"><?=GetMessage("AT")?>:</label>
                <input type="text" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>" id="time" value="<?=$arResult["arrVALUES"]["form_text_34"]?$arResult["arrVALUES"]["form_text_34"]:'12 : 00'?>" style="width: 66px;"/>
            </div>
        </div>
        <div class="form-group pull-left today-tomorrow-wrapper">

            <?if(!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
            <div class="up-down"><a href="javascript:void(0)" class="down">-</a><a href="javascript:void(0)" class="up">+</a></div>
            <input type="text" class="form-control" id="company"
                   value="<?= ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"] ?>"
                   size="4" maxlength="3"
                   name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>" style="width: 24px;"/>
            <span class="guest-label-title"><?=GetMessage('guest-label-title');?></span>
            <?else:?>
            <select class="form-control" onchange="//$('#company').val($(this).val());" id="company" name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>" style="width: 76px;">
                <?$selected_val = ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"]?>
                                        <?for($i=0;$i<21;$i++){?>
                <option value="<?=$i?>" <?if($selected_val==$i)echo 'selected';?>><?=$i?></option>
                <?}?>
            </select>
            <?endif?>
        </div>
        <div class="form-group">
            <input type="text" placeholder="<?=(LANGUAGE_ID=="en")?"Name":"Фамилия и имя"?>" class="form-control" id="name" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />
        </div>
        <div class="form-group ">
            <?
            $rsUser = CUser::GetByID($USER->GetID());
            $ar = $rsUser->Fetch();
            $ar["PERSONAL_PHONE"] = str_replace(" ", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("-", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("(", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace(")", "", $ar["PERSONAL_PHONE"]);
            $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);
            $phone = $ar["PERSONAL_PHONE"];
            $ar["PERSONAL_PHONE"] = substr($phone, 0, 3) . " " . substr($phone, 3, 3) . "  " . substr($phone, 6, 2) . "  " . substr($phone, 8, 2);
            ?>
            <input type="text" class="phone1 form-control" placeholder="___ ___-__-__" style="text-align:left" value="<?= ($_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]] : $ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" />
            <input type="hidden" class="form-control" id="email" value="<?= ($_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]] : $USER->GetEmail() ?>"
                   name="form_<?= $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[8]["STRUCTURE"][0]["ID"] ?>" />
        </div>

        <div class="form-group" style="position: relative;">
            <input type="text" class="form-control wishes-input" placeholder="<?= GetMessage("MY_WISH") ?>" name="form_<?= $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[11]["STRUCTURE"][0]["ID"] ?>" <?= trim($_REQUEST["form_" . $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[11]["STRUCTURE"][0]["ID"]]) ?>/>

            <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/tpl/include_areas/order-form-wishes-list.php",
            "EDIT_TEMPLATE" => ""
            ),
            false
            ); ?>

        </div>


        <div class="we-will-get-you"><?=GetMessage('we-will-get-you')?></div>

        <div class="form-group end">
            <a style="width:100%;" class="btn btn-info btn-nb" onclick="$('#order_online form').submit()"><?=($bbb=="b")?GetMessage("RESERVE"):GetMessage("RESERVE")?></a>
            <input style="display: none" class="btn btn-info btn-nb" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?=($bbb=="b")?GetMessage("RESERVE_BANKET"):GetMessage("RESERVE_TABLE")?>" />
        </div>

        <div class="ok"><?if ($arResult["isFormNote"] == "Y") {?><?= $arResult["FORM_NOTE"] ?>
            <?}?></div>
        <div class="font12" id="err">
            <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
        </div>
        <script>
            $(document).ready(function(){
                $(".time").click(function(){
                    $(".time").removeClass("active");
                    $(this).addClass("active");
                    $("#time").val($(this).text());
                    $("#chek .time2").html( $("#date-picker").val()+"<br /><?=GetMessage("CHEK_TIME")?>: "+$("#time").val()+"<br /><?=GetMessage("CHEK_PERSONS")?>: "+$(".updowninp").val());
                });
            });
        </script>
        <? if ($arResult["isUseCaptcha"] == "Y"): ?>
        <div class="question">
            <?= GetMessage("CAPTCHA") ?>
        </div>
        <div>
            <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
            <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")   ?><? //=$arResult["REQUIRED_SIGN"];   ?>
            <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
        </div>
        <? endif; ?>

            <div class="inv_cph">
                <input type="checkbox" name="lst_nm" value="1" />
                <input type="checkbox" name="scd_nm" value="1" checked="checked" />
            </div>
        <? if ($_REQUEST["invite"]): ?>
        <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
        <? endif; ?>
            <? if ($_REQUEST["CITY"]): ?>
        <input type="hidden" name="CITY" value="<?= $_REQUEST["CITY"] ?>" />
        <? else: ?>
        <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
        <? endif; ?>
            <input type="hidden" name="web_form_apply" value="Y" />

        <?
        //} //endif (isFormNote)
        ?>
            <?= $arResult["FORM_FOOTER"] ?>

<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <?//if($USER->IsAdmin()):?>
    </div><!--.form-full-content-->
    <?//endif?>
</div>

<? endif; ?>