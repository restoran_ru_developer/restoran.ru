<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
    ?>
            <li <?=($arItem["SELECTED"])?"class='active'":""?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                <?if ($arItem["PARAMS"]["message"]=="Y"):?>
                    <?//CModule::IncludeModule("socialnetwork")?>
                    <?
                    if ($USER->IsAuthorized()):
                        $dbResult = $GLOBALS["DB"]->Query(
                            "SELECT COUNT(ID) as CNT ".
                            "FROM b_sonet_messages ".
                            "WHERE TO_USER_ID = ".$USER->GetID()." ".
                            "    AND DATE_VIEW IS NULL ".
                            "    AND TO_DELETED = 'N' "
                        );
                        if ($arU = $dbResult->Fetch())
                            $cnt = IntVal($arU["CNT"]);
                        ?>
                        <?if($cnt):?>
                            <em>Новых: <strong><?=$cnt?></strong></em>
                        <?endif;?>
                    <?endif;?>
                <?endif;?>
                <?if ($arItem["PARAMS"]["friends"]=="Y"):?>
                    <?
                    $arU = array();
                    if ($USER->IsAuthorized()):
                        $dbResult = $GLOBALS["DB"]->Query(
                            "SELECT COUNT(ID) as CNT ".
                            "FROM b_sonet_user_relations ".
                            "WHERE SECOND_USER_ID = ".$USER->GetID()." AND RELATION = 'Z'"
                        );
                        if ($arU = $dbResult->Fetch())
                            $cnt = IntVal($arU["CNT"]);
                        ?>
                        <?if($cnt):?>
                            <em>Новых: <strong><?=$cnt?></strong></em>
                        <?endif;?>
                    <?endif;?>
                <?endif;?>
                            
            </li>
    <?endforeach?>
<?endif?>