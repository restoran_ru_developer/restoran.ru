<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="/subscribe.php" id="subscribe_form">
    <div class="title" ><?=GetMessage("SUBSCR_TITLE")?></div>
    <div class="subscribe_box">
        <form>
            <input type="email" name="email" class="subscribe_input" placeholder="<?=GetMessage("subscr_form_placeholder")?>">
            <input type="submit" class="subscribe_submit" value="" name="OK">
        </form>
    </div>
</form>