<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/bxslider-4-master/dist/jquery.bxslider.min.js"></script>
    <script>
        $(function(){
            $('.index-where-to-go-slider').bxSlider({
                pager:false,
                prevSelector:'.prev-slide',
                nextSelector:'.next-slide',
                slideWidth:35,
                moveSlides:1,
                minSlides:12,
                maxSlides:12,
                slideMargin: 40,
                hideControlOnEnd: true,
                infiniteLoop: false,
                onSliderLoad: function(){
                    // do funky JS stuff here
                    $('[data-toggle="tooltip-pic"]').tooltip({container:'.index-journal-wrapper',template:'<div class="tooltip white" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'})
                }
//                onSlideAfter: function(){
//                    $('[data-toggle="tooltip-pic"]').tooltip()
//                }
            });
        })
    </script>
<?if(count($arResult["ITEMS"])>0):?>
    <div class="index-journal-bg-line"></div>
    <div class="index-journal-title">Куда пойти?</div>

    <div class="under-index-journal-title-sign" >рестораны на любой вкус и повод</div>

    <div class="index-where-to-go-slider-wrap">
        <?if(count($arResult["ITEMS"])>12):?>
        <div class="prev-slide icon-arrow-left2"></div>
        <div class="next-slide icon-arrow-right2"></div>
        <?endif?>

        <div class="index-where-to-go-slider">
            <?foreach($arResult["ITEMS"] as $arItem):
                ?>
                <?if($arItem['CODE']=='na_kanikulakh_v_rige'){?>
                    <a href="http://www.restoran.ru/rga/catalog/restaurants/where_to_go/<?=$arItem['CODE']?>/"  data-toggle="tooltip-pic" data-placement="top" data-original-title="<?=$arItem['NAME']?>" >
                <?}else {?>
                    <?if(preg_match('/#/',$arItem['CODE'])):
                        preg_match("/#(.+)/", $arItem['CODE'], $output_link);
                        ?>
                        <a href="<?=$output_link[1]?>" data-toggle="tooltip-pic" data-placement="top" data-original-title="<?=$arItem['NAME']?>" >
                    <?else:?>
                        <a href="/<?=CITY_ID?>/catalog/restaurants/where_to_go/<?=$arItem['CODE']?>/" data-toggle="tooltip-pic" data-placement="top" data-original-title="<?=$arItem['NAME']?>" >
                    <?endif?>
                <?}?>
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="where-to-go-icon" width="" height="" alt="<?=$arItem['NAME']?>" title="" >
                </a>
            <?endforeach;?>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="index-where-to-go-wrapper-tiles">
        <div>
        <?foreach($arResult["ITEMS"] as $key=>$arItem):
            ?>
            <div class="pull-left ">
                <div class="pic">
                    <?if($arItem['CODE']=='na_kanikulakh_v_rige'){
                        $link_val = 'https://www.restoran.ru/rga/catalog/restaurants/where_to_go/'.$arItem['CODE'].'/';
                    }
                    else {
                        if(preg_match('/#/',$arItem['CODE'])):
                            preg_match("/#(.+)/", $arItem['CODE'], $output_link);
                            $link_val = $output_link[1];
                        else:
                            $link_val = '/'.CITY_ID.'/catalog/restaurants/where_to_go/'.$arItem['CODE'].'/';
                        endif;
                    }?>

                    <a href="<?=$link_val?>">
                        <?
                        $arFile = CFile::ResizeImageGet($arItem['PROPERTIES']['WTG_BG_PIC_FULL']['VALUE']?$arItem['PROPERTIES']['WTG_BG_PIC_FULL']['VALUE']:$arItem['PROPERTIES']['WTG_BG_PIC']['VALUE'], array('width'=>231, 'height'=>127), BX_RESIZE_IMAGE_EXACT, false);
                        ?>
                        <img src="<?=$arFile['src']?>" class="this-link-hover-bg" width="" height="" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" >
                    </a>
                </div>
                <div class="text">
                    <h2>
                        <a href="<?=$link_val?>"><?= $arItem['NAME'] ?></a>
                    </h2>
                    <?if($arItem['PREVIEW_TEXT']):?>
                    <div class="more_links">
                        <?
                        if($arItem['PREVIEW_TEXT']<5){
                            $rests_str = 'ресторана';
                        }
                        else {
                            $rests_str = 'ресторанов';
                        }
                        ?>
                        <a href="<?=$link_val?>" class="watch-all"><?=$arItem['PREVIEW_TEXT']?> <?=$rests_str?></a>
                    </div>
                    <?endif?>
                </div>
            </div>

            <?if(($key+1)%4==0&&$key!=0):?>
            </div>
                <hr>
            <div>
            <?endif?>
        <?endforeach;?>
        </div>
    </div>
    <div class="clearfix"></div>
<?endif?>