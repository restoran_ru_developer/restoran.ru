<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                                                                                                                                                                                                                                                
<?
$todayDate = date("d.m.Y");
?>
<script>
    $(function()
    {
        $('.restoran_poster').jScrollPane({verticalGutter:15});        
    });    
</script>
<div class="restoran_poster" style="width: 100% !important;">
    <table>
        <tbody>
            <?foreach($arResult["AFISHA_ITEMS"] as $today):?>
                <?foreach ($today as $todayEvent):?>
                    <?php
                    $dat = date("d.m.Y",strtotime($todayEvent["PROPERTY_EVENT_DATE_VALUE"]));
                    if ($dat == date("d.m.Y"))                   
                        $prefix = "<strong>Сегодня</strong>, ";
                    elseif ($dat == date("d.m.Y",strtotime(date("d.m.Y")." +1 day")))
                        $prefix = "<strong>Завтра</strong>, ";
                    else
                        $prefix = "";              
                    $date2 = date("d F",strtotime($todayEvent["PROPERTY_EVENT_DATE_VALUE"]));
                    $array1 = Array("December","January","February","March","April","May","June","July","August","September","October","November");
                    $array2 = Array("Декабря","Января","Февраля","Марта","Апреля","Мая","Июня","Июля","Августа","Сентября","Октября","Ноября");
                    $date2 = str_replace($array1,$array2,$date2);
                    ?>
                    <tr>
                        <td class="pic">                                                    
                            <div>
                                <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>"></a>
                            </div>
                        </td>
                        <td class="date"><?=$prefix.$date2?></td>
                        <td class="time"><?=$todayEvent["PROPERTY_TIME_VALUE"]?></td>
                        <td class="name"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>                                            
                    </tr>    
                <?endforeach;?>
            <?endforeach;?>
        </tbody>
    </table>
</div>