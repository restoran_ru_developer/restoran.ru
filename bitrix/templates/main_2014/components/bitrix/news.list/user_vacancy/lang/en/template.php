<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["RESUME_STATUS"] = "Status";
$MESS["RESUME_STATUS_Y"] = "Opened";
$MESS["RESUME_STATUS_N"] = "Closed";
$MESS["RESPOND_TITLE"] = "Responded";
$MESS["RESTORATOR_STATE"] = "Restorator";
$MESS["user_resumes_job_title"] = "job Title";
$MESS["user_resumes_city_title"] = "City";
$MESS["user_resumes_education_title"] = "Education";
$MESS["user_resumes_experience_title"] = "Experience";
$MESS["user_resumes_graph_title"] = "Graph";
$MESS["user_resumes_wages_title"] = "Wages";
$MESS["user_resumes_responded_title"] = "Responded";
$MESS["user_resumes_status_title"] = "Status";
$MESS["user_resumes_message_title"] = "Message";
$MESS["user_resumes_amend_title"] = "Amend";
$MESS["user_resumes_remove_title"] = "Remove";
$MESS["user_resumes_no_added_resume"] = "No added jobs";
?>