<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // get city name
    $arResumeIB = getArIblock("resume", CITY_ID);
    $arResult["ITEMS"][$key]["CITY"] = $arResumeIB["NAME"];

    // get respond reg users
    foreach($arItem["PROPERTIES"]["RESPOND_USERS_REG"]["VALUE"] as $userID) {
        $arTmpUser = Array();
        $rsUser = CUser::GetByID($userID);
        $arUser = $rsUser->Fetch();

        $arTmpUser["ID"] = $arUser["ID"];
        $arTmpUser["LOGIN"] = $arUser["LOGIN"];
        $arTmpUser["NAME"] = $arUser["NAME"];
        $arTmpUser["LAST_NAME"] = $arUser["LAST_NAME"];
        $arTmpUser["FULL_NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"];
        $arTmpUser["PERSONAL_PHOTO"] = CFile::GetFileArray($arUser["PERSONAL_PHOTO"]);
        $arTmpUser["PERSONAL_CITY"] = $arUser["PERSONAL_CITY"];
        // get user groups
        $arGroups = CUser::GetUserGroup($userID);
        if(in_array(RESTORATOR_GROUP, $arGroups))
            $arTmpUser["IS_RESTORATOR"] ="Y";

        // set respond reg user info
        $arResult["ITEMS"][$key]["RESPOND_USERS_REG"][] = $arTmpUser;
    }

    // get respond unreg users
    foreach($arItem["PROPERTIES"]["RES_USERS_UNR_EMAIL"]["VALUE"] as $keyUnreg=>$userEmail) {
        $arTmpUser = Array();

        $arTmpUser["EMAIL"] = $userEmail;
        $arTmpUser["FIO"] = $arItem["PROPERTIES"]["RES_USERS_UNR_FIO"]["VALUE"][$keyUnreg];

        // set respond reg user info
        $arResult["ITEMS"][$key]["RESPOND_USERS_UNREG"][] = $arTmpUser;
    }
}
?>