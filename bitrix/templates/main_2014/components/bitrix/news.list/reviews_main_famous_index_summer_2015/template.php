<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="index-famous-reviews">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <div class="pull-left <?=($arParams['AJAX_REVIEWS']=='Y'&&$key>2)||$arParams['AJAX_REVIEWS']!='Y'?'no-bottom-marge':''?>">
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <img src="<?=$arItem['PREVIEW_PICTURE']['src']?>">
            </a>
        </div>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["ITEMS"][$key]['FAMOUS_REVIEW']['NAME']?> о ресторане <?=$arResult["ITEMS"][$key]['FAMOUS_REVIEW']['RESTORAN']?></a></h2>
            <div class="more_links">
                <div class="index-one-main-news-bottom-line">
                    <?if($arItem['SHOW_COUNTER']):?><div class="index-one-main-news-viewed-counter-wrapper"><?=$arItem['SHOW_COUNTER']?></div><?endif;?>
                    <?if($arItem['PROPERTIES']['COMMENTS']['VALUE']):?><div class="index-one-main-news-comments-counter-wrapper"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" ><?=$arItem['PROPERTIES']['COMMENTS']['VALUE']?></a></div><?endif?>
                </div>
            </div>
        </div>
    </div>
    <?if(($key+1)%3==0&&$arItem!=end($arResult["ITEMS"])):?>
        <div class="clearfix"></div>
    <?endif?>
<?endforeach?>
    </div>