<?
foreach($arResult["ITEMS"] as $key=>$arItem) {

    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);

    $res = CIBlockElement::GetByID($arResult["ITEMS"][$key]['PROPERTIES']['FAMOUS_REVIEW']['VALUE']);
    if($ar_res = $res->Fetch()){
        if($ar_res['PREVIEW_PICTURE']){
            $arFile = CFile::GetFileArray($ar_res["PREVIEW_PICTURE"]);

            $arResult["ITEMS"][$key]['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arFile, array('width' => 148, 'height' => 148), BX_RESIZE_IMAGE_EXACT, true, Array());
        }

        $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['NAME'] = $ar_res['NAME'];

        $res = CIBlockElement::GetByID($arResult["ITEMS"][$key]['PROPERTIES']['ELEMENT']['VALUE']);
        if($ar_res = $res->Fetch()){
            $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['RESTORAN'] = $ar_res['NAME'];
        }

    }


//    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PROPERTIES"]["FAMOUS_PIC"]["VALUE"], array('width'=>148, 'height'=>148), BX_RESIZE_IMAGE_EXACT, false);
}
?>