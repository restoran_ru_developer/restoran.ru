<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="common-reviews">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>

    <div class="pull-left ">
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["RESTAURANT_NAME"]?></a></h2>
            <div class="ratio">
                <?for($z=1;$z<=5;$z++):?>
                    <span class="<?=(round($arItem["DETAIL_TEXT"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                <?endfor?>
            </div>
            <div class="review-text-about"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["PREVIEW_TEXT"]?></a></div>
            <div class="more_links">
                <div class="index-one-main-news-bottom-line">
                    <a href="/users/id<?=$arItem["CREATED_BY"]?>/" class="watch-all"><?=$arItem["USER_NAME"]?></a>
                    <div class="index-one-main-news-comments-counter-wrapper"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" ><?=$arItem['PROPERTIES']['COMMENTS']['VALUE']?$arItem['PROPERTIES']['COMMENTS']['VALUE']:0?></a></div>
                    <?if($arItem['SHOW_COUNTER']):?><div class="index-one-main-news-viewed-counter-wrapper"><?=$arItem['SHOW_COUNTER']?></div><?endif;?>
                </div>
            </div>
        </div>
    </div>

    <?if(($key+1)%4==0&&$arItem!=end($arResult["ITEMS"])):?>
        <div class="clearfix"></div>
    <?endif?>
<?endforeach?>
    </div>