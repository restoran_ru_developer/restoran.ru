<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arRestIB = getArIblock("catalog", CITY_ID);
if (count($arResult['ITEMS'])>0):
    foreach($arResult['ITEMS'] as $key=>$arItem):
        $filter = "arrNoFilter_".$arItem["CODE"];
        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
        {    
            global $$filter;
            $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
            $$filter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
        }
        echo '<div class="title">'.$arItem["NAME"].'</div>';
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "recomended",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arRestIB["ID"],
                "NEWS_COUNT" => "4",
                "SORT_BY1" => "none",
                "SORT_ORDER1" => "",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => ($$filter)?$filter:"",
                "FIELD_CODE" => array("CREATED_BY"),
                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "14400",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "A" => "recomended",
                "REST_PROPS"=>"Y"
            ),
        false
        );        
    endforeach;
else:
    $arrFilterTop4["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
    echo '<div class="title">Top 4</div>';
    $APPLICATION->IncludeComponent(
    	"restoran:catalog.list",
    	"recomended",
    	Array(
    		"DISPLAY_DATE" => "N",
    		"DISPLAY_NAME" => "Y",
    		"DISPLAY_PICTURE" => "Y",
    		"DISPLAY_PREVIEW_TEXT" => "N",
    		"AJAX_MODE" => "N",
    		"IBLOCK_TYPE" => "catalog",
    		"IBLOCK_ID" => $arRestIB["ID"],
    		"NEWS_COUNT" => "4",
    		"SORT_BY1" => "shows",
    		"SORT_ORDER1" => "DESC",
    		"SORT_BY2" => "",
    		"SORT_ORDER2" => "",
    		"FILTER_NAME" => "arrFilterTop4",
    		"FIELD_CODE" => array("CREATED_BY"),
    		"PROPERTY_CODE" => array("RATIO","COMMENTS"),
    		"CHECK_DATES" => "Y",
    		"DETAIL_URL" => "",
    		"PREVIEW_TRUNCATE_LEN" => "120",
    		"ACTIVE_DATE_FORMAT" => "d.m.Y",
    		"SET_TITLE" => "N",
    		"SET_STATUS_404" => "N",
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    		"ADD_SECTIONS_CHAIN" => "N",
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
    		"PARENT_SECTION" => "",
    		"PARENT_SECTION_CODE" => "",
    		"CACHE_TYPE" => "Y",
    		"CACHE_TIME" => "14401",
    		"CACHE_FILTER" => "Y",
    		"CACHE_GROUPS" => "N",
    		"DISPLAY_TOP_PAGER" => "N",
    		"DISPLAY_BOTTOM_PAGER" => "N",
    		"PAGER_TITLE" => "Новости",
    		"PAGER_SHOW_ALWAYS" => "N",
    		"PAGER_TEMPLATE" => "",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    		"PAGER_SHOW_ALL" => "N",
    		"AJAX_OPTION_JUMP" => "N",
    		"AJAX_OPTION_STYLE" => "Y",
    		"AJAX_OPTION_HISTORY" => "N",
                "A" => "recomended",
                "REST_PROPS"=>"Y"            
    	),
    false
    );
endif;
?>