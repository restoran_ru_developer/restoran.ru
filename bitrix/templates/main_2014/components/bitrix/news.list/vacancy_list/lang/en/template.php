<?
$MESS["BLOCK_TITLE_POPULAR"] = "Popular jobs";
$MESS["BLOCK_TITLE"] = "Jobs";
$MESS["NOTHING_FOUND"] = "Unfortunately, the selected filter options no results";
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
?>