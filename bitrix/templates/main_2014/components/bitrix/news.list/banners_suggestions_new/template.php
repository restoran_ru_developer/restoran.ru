<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=$templateFolder?>/script.js"></script>
<!--<a name="buy_place"></a>-->
<input id="baner_rest" type="hidden" value="<?=$_REQUEST["ID"]?>"/>
<table cellpadding="10">
    <?foreach($arResult["ITEMS"] as $arItem):?>
    <tr>
        <td>
            <div class="plan-desc"><?=$arItem['DISPLAY_PROPERTIES']['SIZE']['DISPLAY_VALUE']?><br />
                <i><?=$arItem['DISPLAY_PROPERTIES']['POSITION']['DISPLAY_VALUE']?></i>                
                <br />
                <input type="button" class="light_button order-banner" id="banner-order-button-<?=$arItem['ID']?>" name="submit" value="Заказать"/>
                <input type="hidden" id="banner-suggestion-type-<?=$arItem['ID']?>" value="<?=$arItem['NAME']?>" />                                        
            </div>
        </td>
        <td>
            <p class="plan-desc"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="280" /></p>
        </td>        
        <td>
            <ul class="plan-desc-ul" style="text-align: left;">
                    <li>Анимация: <strong><?=$arItem['DISPLAY_PROPERTIES']['PRICE_ANIMATION']['DISPLAY_VALUE']?></strong> руб. <br />
                    <em><?=$arItem['DISPLAY_PROPERTIES']['PRICE_ANIMATION']['DESCRIPTION']?></em></li>
                    <li>Статика: <strong><?=$arItem['DISPLAY_PROPERTIES']['PRICE_STATIC']['DISPLAY_VALUE']?></strong> руб. <br />
                    <em><?=$arItem['DISPLAY_PROPERTIES']['PRICE_STATIC']['DESCRIPTION']?></em></li>
            </ul>            
        </td>
    </tr>
    <?endforeach;?>
</table>
