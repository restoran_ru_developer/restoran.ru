<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $arrFilter;
$arrFilter["PROPERTY_EVENT_DATE"] = Array(false, date("Y-m-d H:i:s", strtotime($_REQUEST["date"])));
 if ($_REQUEST["type"])
    $arrFilter["PROPERTY_EVENT_TYPE"] = (int)$_REQUEST["type"];
if ($_REQUEST["restoran"])
{
    $arrFilter["PROPERTY_RESTORAN"] = trim($_REQUEST["restoran"]);
    $arrFilter["!PROPERTY_RESTORAN"] = false;
}
if ($_REQUEST["tags"])
{
    $temp = explode(" ",trim($_REQUEST["tags"]));
    $fil =implode(" || ".$temp);
    $arrFilter["PROPERTY_TAGS"] = $fil;
}

$arKuponsIB = getArIblock("afisha", CITY_ID);
$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "afisha",
            Array(
                    "AJAX" => "Y",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "afisha",
                    "IBLOCK_ID" => $arKuponsIB["ID"],
                    "NEWS_COUNT" => 180,
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => Array("ACTIVE_TO","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("ratio", "reviews", "subway","EVENT_TYPE","EVENT_DATE"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Купоны",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "kupon_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "PRICE_CODE" => "BASE"
            ),
        false
        );?>