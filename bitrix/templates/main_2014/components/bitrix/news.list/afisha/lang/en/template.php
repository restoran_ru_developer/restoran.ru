<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "This will erase all information associated with this record. Resume?";
$MESS["TOMORROW_LABEL"] = "Tomorrow";
$MESS["TODAY_LABEL"] = "Today";
$MESS["ALL_POSTERS"] = "All posters";
$MESS["NO_POSTER"] = "There is no posters on this date";
$MESS["TEXT_DAY_0"] = "Sunday";
$MESS["TEXT_DAY_1"] = "Monday";
$MESS["TEXT_DAY_2"] = "Tuesday";
$MESS["TEXT_DAY_3"] = "Wednesday";
$MESS["TEXT_DAY_4"] = "Thursday";
$MESS["TEXT_DAY_5"] = "Friday";
$MESS["TEXT_DAY_6"] = "Saturday";
?>