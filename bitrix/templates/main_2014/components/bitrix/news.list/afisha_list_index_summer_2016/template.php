<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams['ONLY_POST_CONTENT']!='Y'):?>
<div class="index-afisha-wrapper">

<div class="tab-pane active afisha">                                                                               
<?
$todayDate = date("d.m.Y");
?>
    <script>
        $(function(){
            var afishaDatePicker = $('.datepicker-afisha-trigger').datepicker({
                format:'yyyy/mm/dd',
                weekStart:1
            }).on('changeDate', function(ev){

                console.log('click date');
                var afisha_date_container = $($(ev.currentTarget).attr("href"));

                event_type_id = '';
                <?if($arParams['CUSTOM_AJAX']=='Y'):?>
                    event_type_id = "&s=<?=$GLOBALS['arAfishaFilter']['PROPERTY_EVENT_TYPE']?>";
                <?endif?>
                console.log(ev.date,'date from calendar');
                var afishaDateTime = new Date(ev.date);


                afishaDateTime = afishaDateTime.getDate()+'.'+(afishaDateTime.getMonth()+1)+'.'+afishaDateTime.getFullYear()

                console.log(afishaDateTime,'afishaDateTime to send');
                $(ev.currentTarget).attr('this-tab-date',afishaDateTime);

                $.ajax({
                    type:"POST",
                    url:$(ev.currentTarget).data("href"),
                    data:"CITY_ID=<?=CITY_ID?><?="&t=afisha_date&ib=".$arParams['IBLOCK_ID']?>&event_date="+afishaDateTime+event_type_id,
                    success:function(data){
                        console.log('got data');

                        afisha_date_container.remove()

                        afisha_date_container.appendTo($('.index-afisha-full-wrapper .nav-tabs li.active a').attr("href")+" .tab-pane.active .tab-content");

                        afisha_date_container.html(data)

                        $(ev.currentTarget).parent('li').removeClass('active');

                        $(ev.currentTarget).tab('show');
                    }
                });

            });

            <?if(!$arParams['CUSTOM_AJAX']):?>
            console.log('get script');
            $('.tab-content > .tab-pane.active .poster.active').jScrollPane({verticalGutter:15});

            $('.index-afisha-full-wrapper').on('shown.bs.tab', 'a[data-toggle="tab"]',function (e) {
                if($(e.target).data("afisha")=='y'||$(e.target).data("afisha")=='ny'){
                    console.log('new init');
                    if(somesomesome!==undefined){
                        somesomesome.destroy();
                    }
                    var somesomesome = $('.tab-content > .tab-pane.active .poster.active').jScrollPane({verticalGutter:15}).data().jsp;

                    //  set date to link to all
                    if(/date/.test($('.all-afisha-link').attr('href'))){

                        url_w_date = $('.all-afisha-link').attr('href').match(/(.*)\?/);

                        $('.all-afisha-link').attr('href',url_w_date[1]+'?date='+$('.tab-content > .tab-pane.active .tab-pane.active .nav li.active a').attr('this-tab-date'));
                    }
                    else {
                        $('.all-afisha-link').attr('href',$('.all-afisha-link').attr('href')+'?date='+$('.tab-content > .tab-pane.active .tab-pane.active .nav li.active a').attr('this-tab-date'));
                    }
                }
            })
            <?endif?>
        })
    </script>

    <ul class="nav poster_tabs ajax_date_tabs">
        <?$day = ConvertTimeStamp(strtotime($todayDate), "SHORT", "ru");?>
        <li class="active">
            <a href="#a1<?=$arParams['EVENT_TYPE_FOR_ID']?>" data-toggle="tab" data-afisha="ny" this-tab-date="<?=date('d.m.Y')?>">
                <b>Сегодня</b>,
                <?=FormatDate($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($day), CSite::GetDateFormat("SHORT"))?></a>
        </li>

        <?
        for($i = 1; $i < 5; $i++):
            $day = ConvertTimeStamp(strtotime($todayDate.' +'.$i.'day'), "SHORT", "ru");
            $days[$i] = $day;
            $url = "&t=afisha_date&ib=" . $arParams['IBLOCK_ID'].'&event_date='.date("d.m.Y", strtotime('+'.$i.' days'));
        ?>
            <li>
                <a href="#a<?=$i+1?><?=$arParams['EVENT_TYPE_FOR_ID']?>" data-toggle="tab" data-afisha="ny" class="ajax" data-href="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=<?= CITY_ID ?><?= $url ?>"
                   this-tab-date="<?=date("d.m.Y", strtotime('+'.$i.' days'))?>"
                    >
                    <b><?=$i==1?'Завтра':FormatDate('l',MakeTimeStamp($day))?></b>,
                    <?=FormatDate($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($day))?>
                </a>
            </li>
        <?endfor?>
        <li>
            <a style="padding: 10px 14px;" href="#a0<?=$arParams['EVENT_TYPE_FOR_ID']?>" data-toggle="tab" data-afisha="ny" class="datepicker-afisha-trigger" data-href="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php" this-tab-date="<?=date('d.m.Y')?>">
                <b>Календарь</b>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane poster" id="a0<?=$arParams['EVENT_TYPE_FOR_ID']?>" ></div>
        <div class="tab-pane active poster" id="a1<?=$arParams['EVENT_TYPE_FOR_ID']?>" >

<?endif;//ONLY_POST_CONTENT?>

            <table>
            <?
                $todayDate = $arParams['ONLY_POST_CONTENT']=='Y'?$arParams['AJAX_EVENT_DATE']:date("d.m.Y");
                if ($arResult["AFISHA_ITEMS"][$todayDate]):?>
                    <?foreach($arResult["AFISHA_ITEMS"][$todayDate] as $time_key=>$todayEvent):?>
                        <tr>
                            <td class="pic">
                                <div>
                                    <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" /></a>
                                </div>
                            </td>

                            <td class="full-time" ><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?><span class="<?if(!$todayEvent["PROPERTIES"]["TIME"]["VALUE"]):?>only-date-str<?endif?>" ><?=$arParams['AJAX_EVENT_DATE']!=date("d.m.Y")&&$arParams['AJAX_EVENT_DATE']?$arParams['AJAX_EVENT_DATE']:date('d.m.y',$time_key)?></span></td>
                            <td class="restoran"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>

                            <td class="afisha-restaurant-name"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a></td>
                            <td class="street">
                                <?if($todayEvent["RESTAURANT"]["address"]):?>
                                <div class="value"><?=$todayEvent["RESTAURANT"]["address"]?></div>
                                <?endif?>
                                <?if(is_array($todayEvent["RESTAURANT"]["subway"])):?>
                                    <div class="name">
                                        <div class="subway">M</div>
                                        <span><?=(count($todayEvent["RESTAURANT"]["subway"])>1?implode(', ',$todayEvent["RESTAURANT"]["subway"]):$todayEvent["RESTAURANT"]["subway"][0])?></span>
                                    </div>
                                <?endif?>
                            </td>

                            <td class="type">
                                <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                                    <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                                <?else:?>
                                    <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                                <?endif;?>
                            </td>
                        </tr>
                    <?endforeach?>
                <?else:?>
                        <tr>
                            <td class="restoran"><?=GetMessage("NO_POSTER")?></td>
                        </tr>
                <?endif;?>
            </table>

<?if($arParams['ONLY_POST_CONTENT']!='Y'):?>
        </div>
        <?
        foreach ($days as $key=>$da):?>
            <div class="tab-pane poster" id="a<?=$key+1?><?=$arParams['EVENT_TYPE_FOR_ID']?>" ></div>
        <?endforeach;?>

    </div>
</div>
</div>
<?endif?>