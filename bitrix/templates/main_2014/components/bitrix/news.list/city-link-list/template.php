<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="city-link-list-col-wrapper">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" ><?=$arParams['PARENT_SECTION_CODE']=='restaurants'?'Рестораны, '.$arItem['NAME']:'Банкетные залы, '.$arItem['NAME']?></a>
	</p>
    <?if(($key+1)%50==0 && $arItem!=end($arResult["ITEMS"])):?>
        </div>
        <?if(($key+1)%250==0):?>
            <div class="clear"></div>
        <?endif?>
        <div class="city-link-list-col-wrapper">
    <?endif?>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
