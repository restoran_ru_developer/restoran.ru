<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <div class="pull-left <?=($key%3==2)?"end":""?>">
        <div class="text">
            <div class="date">
                <?if ($arItem["CREATED_BY"]):?>
                    <a href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["USER_NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <?else:?>
                    <a href="javascript:void(0)"><?=$arItem["USER_NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <?endif;?>
            </div>
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=$arItem["RESTAURANT_NAME"]?></a></h2>
            <div class="ratio">
                <?for($z=1;$z<=5;$z++):?>
                    <span class="<?=(round($arItem["DETAIL_TEXT"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                <?endfor?>
            </div>
            <div class="review-text-about"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["PREVIEW_TEXT"]?></a></div>
<!--            <a class="read_full_message" href="--><?//=$arItem["DETAIL_PAGE_URL"]?><!--">--><?//=GetMessage("reviews_on_main_further")?><!--</a>-->
            <div class="reviews_links">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="pull-left">
                    Комментировать
                </a>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="pull-right">
                    <b>Ответов &ndash;</b> <?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?>
                </a>
            </div>
         </div>
    </div>
<?if ($arItem==end($arResult["ITEMS"])):?>
            <div class="clearfix"></div>
            <div class="more_links text-right">
                <a href="/<?=CITY_ID?>/opinions/" class="btn btn-light"><?=GetMessage("ALL_OPINIONS")?></a>
            </div>
        <?endif;?>
<?endforeach?>