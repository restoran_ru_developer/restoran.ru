<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="tab-pane active afisha">                                                                               
<?
$todayDate = date("d.m.Y");
?>
    <script type="text/javascript">
        $(function()
        {
            $('.poster').jScrollPane({verticalGutter:15});
            $('.poster').removeClass("active");
            $('.poster').eq(0).addClass("active");
        });
    </script>
    <ul class="nav nav-pills nav-justified poster_tabs">
        <?$day = ConvertTimeStamp(strtotime($todayDate), "SHORT", "ru");?>
        <li class="active"><a href="#a1" data-toggle="tab"><b>Сегодня</b>, <?=FormatDate($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($day), CSite::GetDateFormat("SHORT"))?></a></li>

        <?
        for($i = 1; $i < 5; $i++):
        $day = ConvertTimeStamp(strtotime($todayDate.' +'.$i.'day'), "SHORT", "ru");
        $days[$i] = $day;
        ?>
            <li><a href="#a<?=($i+1)?>" data-toggle="tab"><?/*if($i == 1):?><?=GetMessage("TOMORROW_LABEL")?><br /><?endif*/?><?=FormatDate($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($day), CSite::GetDateFormat("SHORT"))?></a></li>
        <?endfor?>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active poster" id="a1">       
            <table>
            <?
//            FirePHP::getInstance()->info(count($arResult["AFISHA_ITEMS"][$todayDate]));
            if ($arResult["AFISHA_ITEMS"][$todayDate]):?>
                <?foreach($arResult["AFISHA_ITEMS"][$todayDate] as $time_key=>$todayEvent):?>
                    <tr>
                        <td class="pic">
                            <div>
                                <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" /></a>
                            </div>
                        </td>
                        <td class="time" this_event_time="<?=$time_key?>"><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?></td>
                        <td class="name"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>
                        <td class="type">
                            <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                                <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                            <?else:?>
                                <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                            <?endif;?>
                        </td>
                        <td class="restoran"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a><br /><?=$todayEvent["PROPERTIES"]["ADRES"]["VALUE"]?></td>
                    </tr>
                <?endforeach?>
            <?else:?>
                    <tr>
                        <td class="restoran"><?=GetMessage("NO_POSTER")?></td>
                    </tr>
            <?endif;?>
            </table>
        </div>
        <?
        foreach ($days as $key=>$da):?>
            <div class="tab-pane active poster" id="a<?=$key+1?>">   
                <table>
                <?if ($arResult["AFISHA_ITEMS"][$da]):?>
                <?foreach($arResult["AFISHA_ITEMS"][$da] as $todayEvent):?>
                    <tr>
                        <td class="pic">
                            <div>
                                <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" /></a>
                            </div>
                        </td>
                        <td class="time"><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?></td>
                        <td class="name"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>
                        <td class="type">                    
                            <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                                <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                            <?else:?>
                                <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                            <?endif;?>
                        </td>
                        <td class="restoran"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a><br /><?=$todayEvent["PROPERTIES"]["ADRES"]["VALUE"]?></td>
                    </tr>
                    <?endforeach?>
                <?else:?>
                    <tr>
                        <td class="restoran"><?=GetMessage("NO_POSTER")?></td>
                    </tr>
                <?endif;?>
                </table>
            </div>
        <?endforeach;?>
        <div class="more_links text-right">                                           
            <a href="/<?=CITY_ID?>/afisha/" class="btn btn-light">вся афиша</a>
        </div>

<!--        --><?//foreach($arResult["AFISHA_ITEMS"] as $key=>$date_key):?>
<!--            --><?//print_r($key)?>
<!--        --><?//endforeach?>
    </div>
</div>