<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// get afisha iblock info
$arAfishaIB = getArIblock("afisha", CITY_ID);
if (!$arAfishaIB["ID"]) 
{
    ShowError("Section not found");
    die;
}
// get restaurants iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
global $arrFilter;
$arFilter = Array("IBLOCK_ID"=>$arAfishaIB["ID"],
                Array("LOGIC"=>"OR",
                       Array("!PROPERTY_d1"=>false),
                       Array("!PROPERTY_d2"=>false),
                       Array("!PROPERTY_d3"=>false),
                       Array("!PROPERTY_d4"=>false),
                       Array("!PROPERTY_d5"=>false),
                       Array("!PROPERTY_d6"=>false),
                       Array("!PROPERTY_d7"=>false),
                    ),
                Array("LOGIC"=>"OR",
                        Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y h:i:s")),
                        Array("DATE_ACTIVE_TO"=>false)
            ));
if ($arrFilter["PROPERTY_EVENT_TYPE"])
   $arFilter["PROPERTY_EVENT_TYPE"] = $arrFilter["PROPERTY_EVENT_TYPE"];
if ($arrFilter["PROPERTY_RESTORAN"])
   $arFilter["PROPERTY_RESTORAN"] = $arrFilter["PROPERTY_RESTORAN"];
if ($arrFilter["PROPERTY_TAGS"])
   $arFilter["PROPERTY_TAGS"] = $arrFilter["PROPERTY_TAGS"];

//print_r($arResult["ITEMS"]);

$res = CIBlockElement::GetList(Array(), $arFilter,false,array('nTopCount'=>10),Array("*","PROPERTY_d1","PROPERTY_d2","PROPERTY_d3","PROPERTY_d4","PROPERTY_d5","PROPERTY_d6","PROPERTY_d7","PROPERTY_RESTORAN","PROPERTY_EVENT_TYPE","PROPERTY_TIME","PROPERTY_ADRES","PROPERTY_EVENT_DATE",'SORT'));
while($ar = $res->GetNext())
{
    $day = date("N");
    $day--;
    $ar["ACTIVE_FROM2"] = $ar["PROPERTY_EVENT_DATE_VALUE"];
    for ($i=0;$i<=4;$i++)
    {
        if ($day==7)
            $day = 1;
        else
            $day++;

        $tomorrow = date("d.m.Y",strtotime('+'.$i.' day'));
        if ($ar["PROPERTY_D".$day."_VALUE"]=="Y" && $ar["PROPERTY_EVENT_DATE_VALUE"]!=$tomorrow)
        {
            $ar["ACTIVE_FROM2"] = $ar["PROPERTY_EVENT_DATE_VALUE"];
            $ar["PROPERTY_EVENT_DATE_VALUE"] = $tomorrow;
             $detail = $ar["DETAIL_PICTURE"];
            $preview = $ar["PREVIEW_PICTURE"];
            unset($ar["DETAIL_PICTURE"]);
            unset($ar["PREVIEW_PICTURE"]);
            if (is_array($detail))
                $ar["DETAIL_PICTURE"] =  $detail;
            else
                $ar["DETAIL_PICTURE"]["ID"] =  $detail;
            if (is_array($preview))
                $ar["PREVIEW_PICTURE"] =  $preview;
            else
                $ar["PREVIEW_PICTURE"]["ID"] =  $preview;
            $ar["PROPERTIES"]["RESTORAN"]["VALUE"] =  $ar["PROPERTY_RESTORAN_VALUE"];

            $r = CIBlockElement::GetByID($ar["PROPERTY_EVENT_TYPE_VALUE"]);
            $a = $r->Fetch();
            $ar["PROPERTIES"]["EVENT_TYPE"]["VALUE"] = $a["NAME"];

            //$ar["PROPERTIES"]["EVENT_TYPE"]["VALUE"] =  $ar["PROPERTY_EVENT_TYPE_VALUE"];
            $ar["PROPERTIES"]["TIME"]["VALUE"] =  $ar["PROPERTY_TIME_VALUE"];
            $ar["PROPERTIES"]["ADRES"]["VALUE"] =  $ar["PROPERTY_ADRES_VALUE"];
            $arResult["ITEMS"][] = $ar;
        }

        // что если ==$tomorrow    ?
    }
}

//print_r($arResult["ITEMS"]);

foreach($arResult["ITEMS"] as $key=>$arItem) {
    // get rest info
    if ($arItem["PROPERTIES"]["RESTORAN"]["VALUE"])
    {
        $rsRest = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arRestIB["ID"],
                "ID" => $arItem["PROPERTIES"]["RESTORAN"]["VALUE"]
            ),
            false,
            false,
            Array("ID", "NAME", "CODE","DETAIL_PAGE_URL", "ACTIVE")
        );
        // TODO на getbyid

        $arRest = $rsRest->GetNext();
        $arItem["RESTAURANT"]["NAME"] = $arRest["NAME"];
        $arItem["RESTAURANT"]["ADDRESS"] = $arItem["PROPERTIES"]["ADRES"]["VALUE"];
        //$arItem["RESTAURANT"]["LINK"] = "/".CITY_ID."/detailed/".$arRestSec["CODE"]."/".$arRest["CODE"]; // #IBLOCK_CODE#/detailed/#SECTION_CODE#/#CODE#
        $arItem["RESTAURANT"]["LINK"] = $arRest["DETAIL_PAGE_URL"]; // #IBLOCK_CODE#/detailed/#SECTION_CODE#/#CODE#
    }
    if (!$arItem["PROPERTY_EVENT_DATE_VALUE"])
        $arItem["PROPERTY_EVENT_DATE_VALUE"] = $arItem["PROPERTIES"]["EVENT_DATE"]["VALUE"];
    $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array("sort" => "asc"), Array("CODE"=>"sleeping_rest"));
    if($ar_props = $db_props->Fetch())
        $sleep = IntVal($ar_props["VALUE"]);
    if (!$sleep && $arRest["ACTIVE"]!="N")
    //if (!$sleep)
    {
        if (!$arItem["PREVIEW_PICTURE"]["ID"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => 96, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
        else        
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 96, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
        $arItem["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"]["src"];
        if (!$arItem["PREVIEW_PICTURE"])
            $arItem["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm.png";


//print_r($arItem["PROPERTY_EVENT_DATE_VALUE"]);

        // составляем список

        // resort
        if ($arItem["ACTIVE_FROM"]&&strtotime($arItem["ACTIVE_FROM"])<=strtotime(date("d.m.Y")))
            $arResult["AFISHA_ITEMS"][$arItem["PROPERTY_EVENT_DATE_VALUE"]][] = $arItem;
        elseif(!$arItem["ACTIVE_FROM"])
            $arResult["AFISHA_ITEMS"][$arItem["PROPERTY_EVENT_DATE_VALUE"]][] = $arItem;
    }

//    if($arItem['ID']==1561946){
//        print_r($arItem);
//    }

}

//FirePHP::getInstance()->info(count($arResult["AFISHA_ITEMS"]));
//if ($USER->IsAdmin())
{
    $temp_array = array();
    $temp_array = $arResult["AFISHA_ITEMS"];
    $arResult["AFISHA_ITEMS"] = array();
    foreach ($temp_array as $key=>$items)
    {
        $ar_to_time = array();
        $times = array();
        $ch = 1;
        foreach ($items as $key2=>$item)
        {
            $ti = "";
            $t = explode("-",$item["PROPERTIES"]["TIME"]["VALUE"]);
            $t= $t[0];
            if (!$t)
                $t = "23:59";
            if ($t=="00:00")
                $t = "23:58";
            $ti = strtotime($t);
            if ($ti)
            {
                if (in_array($ti, $times))
                {
                    $ti = $ti+$ch;
                    $ar_to_time[$ti] = $item;
                    $ch++;
                }
                else
                    $ar_to_time[$ti] = $item;
            }
            else
                $ar_to_time[$key2] = $item;
            $times[] = $ti;
        }
        ksort($ar_to_time);

//        print_r($ar_to_time);

//        foreach($ar_to_time as $temp_key=>$ar_time){
//            $new_ar_time[$ar_time['SORT']][$temp_key] = $ar_time;
//        }
//        ksort($new_ar_time);
//        FirePHP::getInstance()->info(count($new_ar_time));
//        $ar_to_time = array();
//        foreach($new_ar_time as $new_time){
//            foreach($new_time as $temp_key=>$need_time){
//                $ar_to_time[$temp_key] = $need_time;
//            }
//        }


//
//        print_r($ar_to_time);

//FirePHP::getInstance()->info($key,'DATE');

        $arResult["AFISHA_ITEMS"][$key] = $ar_to_time;
    }
    unset($temp_array);
    unset($ar_to_time);
//    print_r($arResult["AFISHA_ITEMS"]);
}
//if ($USER->IsAdmin())
//    print_r($arResult["AFISHA_ITEMS"]);

?>