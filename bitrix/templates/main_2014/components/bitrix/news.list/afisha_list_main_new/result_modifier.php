<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arRestIB = getArIblock("catalog", CITY_ID);
$AdditionalItems = array();
$FirstItems = $arResult['ITEMS'];
//FirePHP::getInstance()->info($arParams['CACHE_TYPE']);
//FirePHP::getInstance()->info($arResult['ITEMS'][0]['ID']);
foreach($FirstItems as $first_items_key=>&$arItem){

    $db_props = CIBlockElement::GetProperty($arRestIB["ID"], $arItem["PROPERTIES"]["RESTORAN"]["VALUE"][0], array("sort" => "asc"), Array("CODE"=>"sleeping_rest"));
    if($ar_props = $db_props->Fetch()){
        if($ar_props["VALUE_ENUM"]=='Да'){
            unset($arResult['ITEMS'][$first_items_key]);
            continue;
        }
    }

    $day = date("N");
    $day--;
    for ($i=0;$i<=4;$i++)
    {
        if ($day==7)
            $day = 1;
        else
            $day++;
        $tomorrow = date("d.m.Y",strtotime('+'.$i.' day'));
        if ($arItem['PROPERTIES']['d'.$day]['VALUE']=="Y" && $arItem['PROPERTIES']['EVENT_DATE']['VALUE']!=$tomorrow)
        {

            $thisItem = $arItem;
            $thisItem['PROPERTIES']['EVENT_DATE']['VALUE'] = $tomorrow;
            $detail = $arItem["DETAIL_PICTURE"];
            $preview = $arItem["PREVIEW_PICTURE"];
//            unset($arItem["DETAIL_PICTURE"]);
//            unset($arItem["PREVIEW_PICTURE"]);
            if (is_array($detail))
                $thisItem["DETAIL_PICTURE"] =  $detail;
            else
                $thisItem["DETAIL_PICTURE"]["ID"] =  $detail;
            if (is_array($preview))
                $thisItem["PREVIEW_PICTURE"] =  $preview;
            else
                $thisItem["PREVIEW_PICTURE"]["ID"] =  $preview;

            $r = CIBlockElement::GetByID($thisItem['PROPERTIES']['EVENT_TYPE']['VALUE']);
            $a = $r->Fetch();
            $thisItem["PROPERTIES"]["EVENT_TYPE"]["VALUE"] = $a["NAME"];

            $arResult["ITEMS"][] = $thisItem;
        }
    }
}



$arRestIB = getArIblock("catalog", CITY_ID);
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // get rest info
    if ($arItem["PROPERTIES"]["RESTORAN"]["VALUE"])
    {
        $rsRest = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arRestIB["ID"],
                "ID" => $arItem["PROPERTIES"]["RESTORAN"]["VALUE"]
            ),
            false,
            false,
            Array("ID","NAME","DETAIL_PAGE_URL")
        );
        $arRest = $rsRest->GetNext();
        $arItem["RESTAURANT"]["NAME"] = $arRest["NAME"];
        $arItem["RESTAURANT"]["LINK"] = $arRest["DETAIL_PAGE_URL"];
    }
    if (!$arItem["PREVIEW_PICTURE"]["ID"])
        $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => 96, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
    else
        $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 96, 'height' => 64), BX_RESIZE_IMAGE_EXACT, true);
    $arItem["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"]["src"];
    if (!$arItem["PREVIEW_PICTURE"])
        $arItem["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm.png";
    // resort


    if ($arItem["ACTIVE_FROM"]&&strtotime($arItem["ACTIVE_FROM"])<=strtotime(date("d.m.Y"))){
        $arResult["AFISHA_ITEMS"][$arItem["PROPERTIES"]["EVENT_DATE"]["VALUE"]][] = $arItem;
    }
    elseif(!$arItem["ACTIVE_FROM"]){
        $arResult["AFISHA_ITEMS"][$arItem["PROPERTIES"]["EVENT_DATE"]["VALUE"]][] = $arItem;
    }
}

// сортировка по времени
$temp_array = array();
$temp_array = $arResult["AFISHA_ITEMS"];
$arResult["AFISHA_ITEMS"] = array();
foreach ($temp_array as $key=>$items)
{
    $ar_to_time = array();
    $times = array();
    $ch = 1;
    foreach ($items as $key2=>$item)
    {
        $ti = "";
        $t = explode("-",$item["PROPERTIES"]["TIME"]["VALUE"]);
        $t= $t[0];
        if (!$t)
            $t = "23:59";
        if ($t=="00:00")
            $t = "23:58";
        $ti = strtotime($t);
        if ($ti)
        {
            if (in_array($ti, $times))
            {
                $ti = $ti+$ch;
                $ar_to_time[$ti] = $item;
                $ch++;
            }
            else
                $ar_to_time[$ti] = $item;
        }
        else
            $ar_to_time[$key2] = $item;
        $times[] = $ti;
    }
    ksort($ar_to_time);
    $arResult["AFISHA_ITEMS"][$key] = $ar_to_time;
}
unset($temp_array);
unset($ar_to_time);


// сортируем по сортировке, время оставляем в прежнем порядке
$temp_array = $arResult["AFISHA_ITEMS"];
$newSortAfishaArr = array();
foreach($temp_array as $key=>$dayItems){
    if(!$key){
        continue;
    }
    $i=0;
    foreach($dayItems as $time=>$item){
        if($item['SORT']==500){
            $newSortAfishaArr[$key][$time] = $item['SORT']-$i;
            $i++;
        }
        else {
            $newSortAfishaArr[$key][$time] = $item['SORT'];
        }
    }
    $some_arr[$key] = $newSortAfishaArr[$key];
    arsort($newSortAfishaArr[$key]);// [1437685140]=>500
//    print_r($newSortAfishaArr[$key]);
}
unset($temp_array);
$temp_array = $arResult["AFISHA_ITEMS"];

$arResult["AFISHA_ITEMS"] = array();

//FirePHP::getInstance()->info(,'$newSortAfishaArr');
//print_r($some_arr);
//print_r($newSortAfishaArr['23.07.2015']);
foreach($newSortAfishaArr as $date_key=>$arrWithTimeKey){
    foreach ($arrWithTimeKey as $timeKey => $itemWithTimeKey) {
        $arResult["AFISHA_ITEMS"][$date_key][$timeKey] = $temp_array[$date_key][$timeKey];
    }
}






















?>