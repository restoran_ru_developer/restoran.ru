<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>        
    <div class="spec_block_cookery" style="position:relative;">
        <div id="spec_block_image">
            <?foreach ($arItem["RECEPTS"] as $key=>$rec):?>
                <img class="chim <?=($key==0)?"active":""?>" src="<?=$rec["DETAIL_PICTURE"]["src"]?>" <?=($key==0)?"style='display:block'":""?> height="400" />
            <?endforeach?>
        </div>
        <div class="text">
                <b class="another">подборки рецептов</b>
                <div class="title"><?=$arItem["NAME"]?></div>                
                <div class="pull-left"><?=$arItem["DETAIL_TEXT"]?></div>
                <div class="pull-right"><a href="/content/cookery/spec/<?=$arItem["CODE"]?>/" class="btn btn-info btn-nb"><?=count($arItem["RECEPTS"])?> <?=  pluralForm(count($arItem["RECEPTS"]), "рецепт", "рецепта", "рецептов")?></a></div>
                <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
<?endforeach;?>