<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "day";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "day";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "days";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Further";
$MESS["R_ADD2FAVORITES"] = "Add to favorites";
$MESS["R_SALE"] = "Discount";
$MESS["R_ZA"] = "for";
$MESS["SHOW_CNT_TITLE"] = "Per";
?>