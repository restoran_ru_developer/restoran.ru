<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 
/*$arres=CIBlockSection::GetById($arParams["PARENT_SECTION"]);
if($res=$arres->GetNext()){
    $sectionName = $res["NAME"];
    }*/
?>
<h1 class="content-h"><? if ($sectionName){ echo $sectionName;} else { echo "Резюме";}?></h1>
<div class="content-resumes">
    <?if (count($arResult["ITEMS"])>0):?>
        <div class="content-vrb-row">
            <?
            foreach ($arResult["ITEMS"] as $cell => $arItem):
                //v_dump($arResult);
                ?>
                <div class="content-vrb-block <? if ($cell % 3 == 0): ?>vb-first<? endif ?> vb" style="min-height: 190px;">
                    <div class="content-vrb-content" style="padding-bottom: 0px;">
                        <div class="content-vrb-content-title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></div>
                        <div class="content-vrb-content-personal">
                            <div class="resume-personal-image">
                                <div class="avatar_small">
                                    <? if ($arItem["PREVIEW_PICTURE"]): ?>
                                        <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>"/>
                                    <? elseif ($arItem["USER"]["PHOTO"]["src"]): ?>
                                        <img src="<?= $arItem["USER"]["PHOTO"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" />
                                    <? else: ?>
                                        <img src="/tpl/images/noname/new/man_nnm_42.png" alt="<?= $arItem["USER"]["FIO"] ?>" />
                                    <? endif; ?>
                                </div>
                            </div>
                            <div class="resume-personal-content-wrapper">
                                <div class="resume-personal-name"><?= $arItem["USER"]["FIO"] ?></div>
                                <div class="resume-personal-place"><?= $arItem["USER"]["CITY"] ?></div>
                            </div>
                        </div>
                        <br class="block-param-break">
                        <div class="block-param">
                            <span class="block-param-title">Зарплата:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"] ?> &mdash; <?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_TO"]["VALUE"] ?> <span class="rouble font12">e</span></span>
                        </div>
                        <div class="block-param">
                            <span class="block-param-title">Опыт работы:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></span>
                        </div>

                        <div class="block-param">
                            <span class="block-param-title">График:</span> <span class="block-param-content"><?= implode(" / ", $arItem["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]) ?></span>
                        </div>
                        <br class="block-param-break">

                    </div>
                    <div class="content-vrb-links">
                        <div class="block-info-comments">
                            Комментарии: (<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments"><?= intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"]) ?></a>)
                        </div>
                        <div class="block-info-next">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">Далее</a>
                        </div>
                    </div>
                </div>
                <?
                $cell++;
                if ($cell % 3 == 0 && $arResult["ITEMS"][$cell]):
                    ?>
                    <hr class="content-row-separator">
                <? endif ?>
            <? endforeach; ?>
            <? if ($cell % 3 != 0): ?>
                <? while (($cell++) % 3 != 0): ?>
                    <div class="content-vrb-block vb"></div>
                <? endwhile; ?>
            <? endif ?>
            <? if ($arParams["BLOCK_HEADER_POPULAR"] == "Y"): ?>
                <div class="content-vrb-show-all">
                    <span><a href="/<?= CITY_ID ?>/joblist/<?= $arParams["IBLOCK_TYPE"] ?>/">Все резюме</a></span>
                </div>
            <? endif ?>
        </div>
    <?else:?>
        <p class="another_color"><?=GetMessage("NOTHING_FOUND")?></p>
    <?endif;?>
</div>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <div class="right"><?= $arResult["NAV_STRING"] ?></div>
<? endif; ?>