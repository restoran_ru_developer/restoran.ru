<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>
    $(document).ready(function(){
        $(".user_mail_send").click(function(){
            var _this = $(this);
            name = $(this).attr('name');
            lastname = $(this).attr('lastname');
            user_id = $(this).attr('user-id');

            $.ajax({
                type: "POST",
                url: _this.attr("href"),
                data: "USER_NAME="+name+" "+lastname+"&USER_ID="+user_id+"&<?= bitrix_sessid_get() ?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");
                    }
                    $('#mail_modal').html(data);
                    showOverflow();
                    setCenter($("#mail_modal"));
                    $("#mail_modal").fadeIn("300");
                }
            });
            return false;
        });
    });
</script>
<script>
    $(function() {

        $('.icon-delete').on('click', function() {
            if($(this).attr('type') == 'vacancy'){
                answer = '<?=GetMessage('user_resumes_this_vacancy')?>';
            }
            if($(this).attr('type') == 'resume'){
                answer = '<?=GetMessage('user_resumes_this_resume')?>';
            }

            if (confirm("<?=GetMessage('user_resumes_really_delete')?> "+answer+"?")) {
                if ($(this).attr('type') == 'resume'){
                    $.ajax({
                        type: 'POST',
                        url: "/users/resume_remove.php",
                        data: {
                            id:$(this).attr('resume-id')
                        },
                        success: function(data){
                            if (data.trim() == 'ok'){
                                location.reload();
                            }
                        }
                    });
                }

                if ($(this).attr('type') == 'vacancy'){
                    $.ajax({
                        type: 'POST',
                        url: "/users/vacancy_remove.php",
                        data: {
                            id:$(this).attr('vacancy-id')
                        },
                        success: function(data){
                            if (data.trim() == 'ok'){
                                location.reload();
                            }
                        }
                    });
                }
            }
            return false;
        });
    });
</script>
<? if (count($arResult["ITEMS"]) > 0) { ?>
    <table class="profile-activity w100" cellpadding="0" cellspacing="0">
        <tr>
            <th><?=GetMessage('user_resumes_job_title')?></th>
            <th><?=GetMessage('user_resumes_city_title')?></th>
            <th><?=GetMessage('user_resumes_education_title')?></th>
            <th><?=GetMessage('user_resumes_experience_title')?></th>
            <th><?=GetMessage('user_resumes_graph_title')?></th>
            <th><?=GetMessage('user_resumes_wages_title')?></th>
            <th><?=GetMessage('user_resumes_responded_title')?></th>
            <th><?=GetMessage('user_resumes_status_title')?></th>
            <th class="w-auto">&nbsp;</th>
        </tr>
        <?
        foreach ($arResult["ITEMS"] as $arItem):
            //v_dump($arItem); 
            ?>
            <tr>

                <td valign="top"><p><?= $arItem["NAME"] ?></p></td>
                <td valign="top"><p><?= $arItem["CITY"] ?></p>
                    <p><?= $arItem["PROPERTIES"]["ABILITY_TO_MOVE"]["VALUE"] ?></p>
                </td>
                <? /* foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                  <td valign="top"><p>
                  <?=$arProperty["NAME"]?>:&nbsp;
                  <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                  <?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
                  <?else:?>
                  <?=$arProperty["DISPLAY_VALUE"];?>
                  <?endif?>
                  </p></td>
                  <?endforeach; */ ?>
                <td valign="top"><p><?= $arItem["PROPERTIES"]["EDUCATION"]["VALUE"] ?></p></td>
                <td valign="top"><p><?= $arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></p></td>
                <td valign="top"><p><?= implode('<br>', $arItem["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]) ?></p></td>
                <td valign="top"><p><?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"] ?> - <?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_TO"]["VALUE"] ?> <span class="rouble font12">e</span></p></td>
                <td class="user-friends nofloat">
                    <ul>
                        <? foreach ($arItem["RESPOND_USERS_REG"] as $respUserReg): ?>
                            <li>
                                <img src="<?= $respUserReg["PERSONAL_PHOTO"]["SRC"] ?>" width="70" />
                                <div class="user-friend">
                                    <a href="/users/id<?= $respUserReg["ID"] ?>/"><?= $respUserReg["FULL_NAME"] ?></a><br/>
                                    <? if ($respUserReg["IS_RESTORATOR"]): ?>
                                        <?= GetMessage("RESTORATOR_STATE") ?><br />
                                    <? endif ?>
                                    <?= $respUserReg["PERSONAL_CITY"] ?>
                                    <p class="user-mail"><a class="user_mail_send" name="<?= $respUserReg["NAME"] ?>" lastname="<?= $respUserReg["LAST_NAME"] ?>" user-id="<?= $respUserReg["ID"] ?>" href="/tpl/ajax/im.php"><?=GetMessage('user_resumes_message_title')?></a></p>
                                </div>
                            </li>
                        <? endforeach ?>
                        <? foreach ($arItem["RESPOND_USERS_UNREG"] as $respUserUnreg): ?>
                            <li class="<?= (end($arItem["RESPOND_USERS_UNREG"]) == $respUserUnreg) ? "nopic nobg" : "" ?>">
                                <div class="user-friend">
                                    <p><?= $respUserUnreg["EMAIL"] ?><br/>
                                        <?= $respUserUnreg["FIO"] ?></p>
                                </div>
                            </li>
                        <? endforeach ?>
                        <!--<p class="more-activity" style="margin-top:12px; margin-bottom:0;"><a href="#">Еще 3</a></p>-->
                    </ul>
                </td>
                <td class="status"  valign="top">
                    <div class="activity-actions w-small">
                        <div>
                            <?if (mktime(date("d.m.Y"))>mktime(strtotime($arItem["ACTIVE_TO"]))):
                                $arItem["ACTIVE"] = "N";
                            else:

                            endif;?>
                            <p class="block-expand" align="center"><?= GetMessage("RESUME_STATUS_" . $arItem["ACTIVE"]) ?></p>
                            <a class="razmes" href="/users/id<?= $_REQUEST["USER_ID"] ?>/work/resume_edit/<?= $arItem["ID"] ?>/"><?=GetMessage('user_resumes_amend_title')?></a>
                            <a class="icon-delete" href="#" resume-id="<?= $arItem["ID"] ?>" type="resume"><?=GetMessage('user_resumes_remove_title')?></a>
                            <div class="clear"></div>
                        </div>
                    </div>
                </td>
            </tr>
        <? endforeach; ?>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <br /><?= $arResult["NAV_STRING"] ?>
        <? endif; ?>
    </table>
<? }else { ?>
    <?=GetMessage('user_resumes_no_added_resume')?>
<? } ?>
