<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["RESUME_STATUS"] = "Статус";
$MESS["RESUME_STATUS_Y"] = "Открыта";
$MESS["RESUME_STATUS_N"] = "Закрыта";
$MESS["RESPOND_TITLE"] = "Откликнулись";
$MESS["RESTORATOR_STATE"] = "Ресторатор";
$MESS["user_resumes_this_vacancy"] = "эту вакансию";
$MESS["user_resumes_this_resume"] = "это резюме";
$MESS["user_resumes_really_delete"] = "Вы действительно хотите удалить";
$MESS["user_resumes_job_title"] = "Должность";
$MESS["user_resumes_city_title"] = "Город";
$MESS["user_resumes_education_title"] = "Образование";
$MESS["user_resumes_experience_title"] = "Опыт";
$MESS["user_resumes_graph_title"] = "График";
$MESS["user_resumes_wages_title"] = "Заработная плата";
$MESS["user_resumes_responded_title"] = "Откликнулись";
$MESS["user_resumes_status_title"] = "Статус";
$MESS["user_resumes_message_title"] = "Сообщение";
$MESS["user_resumes_amend_title"] = "Изменить";
$MESS["user_resumes_remove_title"] = "Удалить";
$MESS["user_resumes_no_added_resume"] = "Нет добавленных резюме";
?>