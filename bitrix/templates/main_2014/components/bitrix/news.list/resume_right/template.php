<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
foreach ($arResult["ITEMS"] as $cell => $arItem):
    //v_dump($arItem);
    ?>
    <div class="content-vrb-block vb-first vb" style="min-height: 190px;">
        <div class="content-vrb-content" style="padding-bottom: 0px;">
            <div class="content-vrb-content-title"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></div>
            <div class="content-vrb-content-personal">
                <div class="resume-personal-image">
                    <div class="avatar_small">
                        <? if ($arItem["PREVIEW_PICTURE"]): ?>
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" width="46" />
                        <? else: ?>
                            <img src="<?= $arItem["USER"]["PHOTO"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" width="46" />
                        <? endif; ?>
                    </div>
                </div>
                <div class="resume-personal-content-wrapper">
                    <div class="resume-personal-name"><?= $arItem["USER"]["FIO"] ?></div>
                    <div class="resume-personal-place"><?= $arItem["USER"]["CITY"] ?></div>
                </div>
            </div>
            <br class="block-param-break">
            <div class="block-param">
                <span class="block-param-title">Зарплата:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"] ?> &mdash; <?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_TO"]["VALUE"] ?> <span class="rouble font12">e</span></span>
            </div>
            <div class="block-param">
                <span class="block-param-title">Опыт работы:</span> <span class="block-param-content"><?= $arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></span>
            </div>

            <div class="block-param">
                <span class="block-param-title">График:</span> <span class="block-param-content"><?= implode(" / ", $arItem["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]) ?></span>
            </div>
            <br class="block-param-break">
        </div>
        <div class="content-vrb-links">
            <div class="block-info-comments">
                Комментарии: (<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments"><?= intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"]) ?></a>)
            </div>
            <div class="block-info-next">
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">Далее</a>
            </div>
        </div>
    </div>
    <div class="dotted"></div>
<? endforeach; ?>