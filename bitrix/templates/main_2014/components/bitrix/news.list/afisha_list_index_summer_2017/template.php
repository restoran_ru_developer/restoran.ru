<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams['ONLY_POST_CONTENT']!='Y'):?>
<div class="index-afisha-wrapper inner-afisha<?//=$arParams['INNER_PAGE']=='Y'?'':''?>">

    <div class="tab-pane active afisha <?=$_REQUEST["restoran"]?'one-rest-afisha':''?>">

        <link href="/tpl/css/chosen/chosen.css"  rel="stylesheet" />
        <script src="/tpl/css/chosen/chosen.jquery.js"></script>
        <style>
            .index-afisha-filter .chzn-container.chzn-container-single {
                width: 159px;
                border-radius: 0;
                height: 24px;
                border-left: 0;
                padding-left: 15px;
                border: none;
                box-shadow: none;
            }
            .index-afisha-filter .chzn-container.chzn-container-single > a {
                height: 24px;
                line-height: 24px;
                background: white;
                border-radius: 0;
                width: 158px;
                background: url('/tpl/css/chosen/arrow.png') no-repeat 98% 50%;
                background-color: #fff;
                border: none;
                border-left: 0;
                padding-left: 0;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
            }
            .index-afisha-filter .chzn-container.chzn-container-single > a:focus {
                outline: none;
                box-shadow: none;
            }

            .index-afisha-filter .chzn-container-single .chzn-single div {
                position: absolute;
                right: 0;
                top: 0;
                height: 100%;
                width: 18px;
                display: none;
            }
            .index-afisha-filter .chzn-container-active.chzn-container.chzn-container-single .chzn-drop {
                /*left: -1px !important;*/
                width: 178px !important;
                top: 30px !important;
                border: 1px solid #e3e3e3;
                border-top: 0;
                margin: 0 !important;
                padding: 0;
            }
            .index-afisha-filter .chzn-container.chzn-container-single .chzn-drop .chzn-search {
                display: none;
            }
        </style>
        <script>
            $(function(){
                console.log('add script of template');
                var event_type_id = restoran_id = afisha_keywords ='';
                var afisha_date_container = $('#a1<?=$arParams['EVENT_TYPE_FOR_ID']?>');
                var afishaDateTime = new Date("<?=date('Y,m,d')?>");

                var afishaDatePicker = $('.datepicker-afisha-trigger.datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>').datepicker({
                    format:'dd.mm.yyyy',
                    weekStart:1
                }).on('changeDate', function(ev){
                    console.log('click date');

                    <?
                    if($arParams['CUSTOM_AJAX']=='Y'):
    //                    FirePHP::getInstance()->info('custom ajax');
                    ?>
                        if(event_type_id==''){
                            event_type_id = "&s=<?=$GLOBALS['arAfishaFilter']['PROPERTY_EVENT_TYPE']?>";
                        }
                    <?endif?>

                    afishaDateTime = new Date(ev.date);

                    sendAfishaRequest<?=$arParams['EVENT_TYPE_FOR_ID']?>();

                });

                $("#afisha-event-select-<?=$arParams['EVENT_TYPE_FOR_ID']?>").chosen().change(function(s){
                    event_type_id = '&s='+$(this).find('option:selected').val();
                    sendAfishaRequest<?=$arParams['EVENT_TYPE_FOR_ID']?>();
                });
                $('#afisha_ajax_form<?=$arParams['EVENT_TYPE_FOR_ID']?>').on('submit',function(){
                    //  todo проверить дублируемость requests
                    console.log('try to submit');
                    sendAfishaRequest<?=$arParams['EVENT_TYPE_FOR_ID']?>();
                    return false;
                })
                $('#send-form-trigger<?=$arParams['EVENT_TYPE_FOR_ID']?>').on('click',function(){
                    sendAfishaRequest<?=$arParams['EVENT_TYPE_FOR_ID']?>();
                })
                function sendAfishaRequest<?=$arParams['EVENT_TYPE_FOR_ID']?>(){
                    if($("#search_rest_for_afisha<?=$arParams['EVENT_TYPE_FOR_ID']?>").val()==''){
                        restoran_id = '';
                    }
                    afisha_keywords = '&afisha_keywords='+$("#afisha_keywords<?=$arParams['EVENT_TYPE_FOR_ID']?>").val();


                    $.ajax({
                        type:"POST",
                        url:$('.tab-content > .tab-pane.active form').attr("action"),
                        data:"CITY_ID=<?=CITY_ID?><?="&t=afisha_date_event_place&ib=".$arParams['IBLOCK_ID']?>&event_date="+afishaDateTime.toUTCString()+event_type_id+restoran_id+afisha_keywords,
                        success:function(data){
                            console.log('got data');

                            afisha_date_container.remove()

                            afisha_date_container.appendTo($('.index-afisha-full-wrapper .nav-tabs li.active a').attr("href")+" .tab-pane.active .tab-content");

                            afisha_date_container.html(data)

                            <?if($arParams['INNER_PAGE']!='Y'):?>
                            $('.tab-content > .tab-pane.active .poster.active').jScrollPane({verticalGutter:15})
                            <?endif?>

                            $('.datepicker-afisha-trigger.datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>').datepicker('hide');
                        }
                    });
                }

                <?if(!$arParams['CUSTOM_AJAX']):    //выполняем один раз, при загрузке страницы?>
                    console.log('get script');
                    <?if($arParams['INNER_PAGE']!='Y'):?>
                        $('.tab-content > .tab-pane.active .poster.active').jScrollPane({verticalGutter:15});

                        $('.index-afisha-full-wrapper').on('shown.bs.tab', 'a[data-toggle="tab"]',function (e) {

                            afisha_date_container = $($('.tab-content > .tab-pane.active form').attr("href"));

                            if($(e.target).data("afisha")=='y'||$(e.target).data("afisha")=='ny'){
                                console.log('new init');
                                if(somesomesome!==undefined){
                                    somesomesome.destroy();
                                }
                                var somesomesome = $('.tab-content > .tab-pane.active .poster.active').jScrollPane({verticalGutter:15}).data().jsp;
                            }
                        })
                    <?endif?>
                <?endif?>

                $("#search_rest_for_afisha<?=$arParams['EVENT_TYPE_FOR_ID']?>").autocomplete("/search/rest_bron.php", {
                    limit: 5,
                    minChars: 3,
                    width: 173,
                    formatItem: function(data, i, n, value) {
                        return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
                    },
                    formatResult: function(data, value) {
                        return value.split("###")[0];
                    },
                    onItemSelect: function(value) {
                        restoran_id = '&restoran_id='+value.split("###")[1];
    //                    $("#afisha_search_rest").attr("value",a);
                        sendAfishaRequest<?=$arParams['EVENT_TYPE_FOR_ID']?>();
                    },
                    extraParams: {
                        //search_in: function() { return $('#search_in').val(); },
                    }
                });

                $('.afisha-calendar-trigger.datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>').on('click',function(){
                    $('.datepicker-afisha-trigger.datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>').datepicker('show');
                    $('html,body').animate({scrollTop:$('.datepicker-afisha-trigger.datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>').offset().top},"300");
                })
            })
        </script>
        <div class="index-afisha-filter ">
            <form id="afisha_ajax_form<?=$arParams['EVENT_TYPE_FOR_ID']?>" action="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php<?=$_REQUEST['restoran']?'?restoran='.$_REQUEST['restoran']:''?>" method="POST" enctype="multipart/form-data" href="#a1<?=$arParams['EVENT_TYPE_FOR_ID']?>" >
                <div class="form-group pull-left">
                    <label for="datepicker-id-<?=$arParams['EVENT_TYPE_FOR_ID']?>" class="">Дата</label>
                    <input type="text" id="datepicker-id-<?=$arParams['EVENT_TYPE_FOR_ID']?>" class="datepicker-afisha-trigger form-control datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>"  data-date="<?=$_REQUEST['date']?date('d.m.Y',strtotime($_REQUEST['date'])):date("d.m.Y")?>" value="<?=$_REQUEST['date']?date('d.m.Y',strtotime($_REQUEST['date'])):date("d.m.Y")?>">
                    <label for="datepicker-id-<?=$arParams['EVENT_TYPE_FOR_ID']?>" class="afisha-datepicker-label" style="padding: 0;border: none;cursor: pointer;width: 20px;"></label>
                </div>

                <div class="form-group pull-left">
                    <label for="afisha-event-select-<?=$arParams['EVENT_TYPE_FOR_ID']?>">Тип</label>
                    <ul style="list-style: none;padding: 0;margin: 0;float: left;">
                        <li class="item-row">
                            <select data-placeholder="Все события" class="chzn-select2 afisha-event-select" style="width: 165px" name="" id="afisha-event-select-<?=$arParams['EVENT_TYPE_FOR_ID']?>">
                                <option> </option>
                                <? foreach ($arResult['afisha_events_list'] as $afishaEventKey => $afishaEventName):?>
                                    <option value="<?=$afishaEventKey?>" ><?=$afishaEventName?></option>
                                <? endforeach?>
                            </select>
                        </li>
                    </ul>


                </div>

                <?
                if(!$_REQUEST["restoran"]){
                    $APPLICATION->IncludeComponent(
                        "bitrix:search.title", "afisha_search_rest_2015", Array('INPUT_ID'=>$arParams['EVENT_TYPE_FOR_ID']), false
                    );
                }
                ?>

                <div class="form-group pull-left">
                    <input type="text" class=" form-control" value="" name="" id="afisha_keywords<?=$arParams['EVENT_TYPE_FOR_ID']?>" placeholder="Ключевое слово" style="padding-left: 0;">
                </div>

                <div class="form-group">
                    <div class="btn btn-info" id="send-form-trigger<?=$arParams['EVENT_TYPE_FOR_ID']?>"><span>Найти</span></div>
                    <input type="submit" name="" value="submit" style="display: none;">
                </div>
            </form>
        </div>

        <div class="tab-content">
            <div class="tab-pane active poster" id="a1<?=$arParams['EVENT_TYPE_FOR_ID']?>" >

<?endif;//ONLY_POST_CONTENT?>
                <?
                $todayDate = $arParams['ONLY_POST_CONTENT']=='Y'?$arParams['AJAX_EVENT_DATE']:($_REQUEST['date']?date('d.m.Y',strtotime($_REQUEST['date'])):date("d.m.Y"));
                ?>
                <?if($_REQUEST["restoran"]):?>
                <div class="event-month-wrapper">
                    <div class="index-journal-bg-line"></div>
                    <div class="center-align" >
                        <?
                        $month_in = array('январе','феврале','марте','апреле','мае','июне','июле','августе','сентябе','октябре','ноябре','декабре');
                        ?>
                        <div class="calendar-month" >В <?=$month_in[date('n',strtotime($todayDate))-1]?></div></div>
                </div>
                <?endif?>


                <table class="<?=$_REQUEST["restoran"]?'one-rest-afisha-table':''?>">
                    <?if($_REQUEST["restoran"]):?>
                        <?foreach ($arResult["AFISHA_ITEMS"] as $todayDateKey=>$todayDateArr):?>
                            <?//if(strtotime($todayDateKey)<time()){continue;}//отсеиваем старые мероприятия?>
                            <?foreach($todayDateArr as $time_key=>$todayEvent):?>
                                <tr>
                                    <td class="pic">
                                        <div>
                                            <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" /></a>
                                        </div>
                                    </td>
                                    <?
                                    if(date('d.m')==date('d.m',strtotime($todayDateKey))){
                                        $date_str = 'Сегодня';
                                    }
                                    else {
                                        if(date('d.m',strtotime($todayDateKey))>date('d.m',strtotime('+1 day'))){
                                            $date_str = '';
                                        }
                                        elseif(date('d.m',strtotime($todayDateKey))==date('d.m',strtotime('+1 day'))) {
                                            $date_str = 'Завтра';
                                        }
                                    }
                                    ?>
                                    <td class="full-time" ><?=$date_str?><span class="<?if(!$date_str):?>only-date-str<?endif?>" ><?=$_REQUEST['AJAX_EVENT_DATE']?$_REQUEST['AJAX_EVENT_DATE']:date('d.m.y',strtotime($todayDateKey))?></span></td>
                                    <td class="time" this_event_time="<?=$time_key?>"><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?></td>
                                    <td class="type">
                                        <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                                            <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                                        <?else:?>
                                            <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                                        <?endif;?>
                                    </td>


                                    <td class="restoran"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>

                                    <td class="name"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>">Подробнее</a></td>
                                </tr>
                            <?endforeach?>
                        <?endforeach?>
                    <?else:?>
                        <?if ($arResult["AFISHA_ITEMS"][$todayDate]):?>
                            <?foreach($arResult["AFISHA_ITEMS"][$todayDate] as $time_key=>$todayEvent):?>
                                <tr>
                                    <td class="pic">
                                        <div>
                                            <a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><img src="<?=$todayEvent["PREVIEW_PICTURE"]?>" /></a>
                                        </div>
                                    </td>

                                    <td class="full-time" ><?=$todayEvent["PROPERTIES"]["TIME"]["VALUE"]?><span class="<?if(!$todayEvent["PROPERTIES"]["TIME"]["VALUE"]):?>only-date-str<?endif?>" ><?=$arParams['AJAX_EVENT_DATE']!=date("d.m.Y")&&$arParams['AJAX_EVENT_DATE']?$arParams['AJAX_EVENT_DATE']:date('d.m.y',$time_key)?></span></td>
                                    <td class="restoran"><a href="<?=$todayEvent["DETAIL_PAGE_URL"]?>"><?=$todayEvent["NAME"]?></a></td>

                                    <td class="afisha-restaurant-name"><a target="_blank" href="<?=$todayEvent["RESTAURANT"]["LINK"]?>" class="another"><?=$todayEvent["RESTAURANT"]["NAME"]?></a></td>
                                    <td class="street">
                                        <?if($todayEvent["RESTAURANT"]["address"]):?>
                                            <div class="value"><?=$todayEvent["RESTAURANT"]["address"]?></div>
                                        <?endif?>
                                        <?if(is_array($todayEvent["RESTAURANT"]["subway"])):?>
                                            <div class="name">
                                                <div class="subway">M</div>
                                                <span><?=(count($todayEvent["RESTAURANT"]["subway"])>1?implode(', ',$todayEvent["RESTAURANT"]["subway"]):$todayEvent["RESTAURANT"]["subway"][0])?></span>
                                            </div>
                                        <?endif?>
                                    </td>

                                    <td class="type">
                                        <?if ($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"]):?>
                                            <?=  strip_tags($todayEvent["DISPLAY_PROPERTIES"]["EVENT_TYPE"]["DISPLAY_VALUE"])?>
                                        <?else:?>
                                            <?=  strip_tags($todayEvent["PROPERTIES"]["EVENT_TYPE"]["VALUE"])?>
                                        <?endif;?>
                                    </td>
                                </tr>
                            <?endforeach?>
                        <?else:?>
                            <tr>
                                <td class="restoran"><?=GetMessage("NO_POSTER")?></td>
                            </tr>
                        <?endif;?>
                    <?endif?>

                </table>
<?if($arParams['ONLY_POST_CONTENT']!='Y'):?>
            </div>
            <div class="tab-pane poster"></div>
        </div>
        <div class="more-events-calendar-trigger-wrap">Больше событий смотрите в <span class="afisha-calendar-trigger datepicker-class-<?=$arParams['EVENT_TYPE_FOR_ID']?>">календаре</span></div>

    </div>
</div>
<?endif?>