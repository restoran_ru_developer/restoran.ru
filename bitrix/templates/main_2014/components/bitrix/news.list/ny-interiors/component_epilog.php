<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//FirePHP::getInstance()->info($templateFolder);?>
<script>
    $(function(){
        $(".likes-buttons").on("click touchstart", "div.plus", function (event) {
            var obj = $(this);
            $.post("<?=$templateFolder?>/core.php", {ID: obj.attr('ny-id'), act: "plus"}, function (otvet) {
                if (parseInt(otvet)) {
                    obj.html(otvet);
                }
                else {
                    $('.likes-buttons .plus').tooltip({
                        'title': otvet,
                        'trigger': "manual",
                        'placement': 'top'
                    });
                    obj.tooltip("show");
                    setTimeout("$('.likes-buttons .plus').tooltip('hide')", 3000);
                }
            });
            return false;
        });

        $(".likes-buttons").on("click touchstart", "div.minus", function (event) {
            var obj = $(this);
            $.post("<?=$templateFolder?>/core.php", {ID: obj.attr('ny-id'), act: "minus"}, function (otvet) {
                if (parseInt(otvet)) {
                    obj.html(otvet);
                } else {
                    $('.likes-buttons .minus').tooltip({
                        'title': otvet,
                        'trigger': "manual",
                        'placement': 'right'
                    });
                    obj.tooltip("show");
                    setTimeout("$('.likes-buttons .minus').tooltip('hide')", 3000);
                }
            });
            return false;
        });

        setTimeout(asd, 300);
        function asd() {
            var id_list = [];
            $('.likes-buttons').each(function(indx, element){
                id_list[indx] = $(element).attr('my-buttons-id');
            })

            $.post("<?=$templateFolder?>/core.php", {ID: id_list, act: "generate_buts"}, function (otvet) {
                otvet = JSON.parse(otvet);
                $('.likes-buttons').each(function(indx, element){
                    $(element).html(otvet[$(element).attr('my-buttons-id')]);
                })
            });
        }
    })
</script>