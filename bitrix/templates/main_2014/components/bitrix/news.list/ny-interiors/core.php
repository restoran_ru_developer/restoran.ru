<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

function check_user_golos($ELID){
    //GetModuleEvents("askaron.ibvote");
    //var_dump($_SERVER["REMOTE_ADDR"]);
    global $USER;
    global $APPLICATION;

    if(CModule::IncludeModule("askaron.ibvote")){
        if (CAskaronIbvoteEvent::CheckVotingIP($ELID, $_SERVER["REMOTE_ADDR"], 86400)){
            echo 'Вы уже голосовали за эту публикацию';
            return false;
        }else{
            return true;
        }
    };
}


function minus(){
    CModule::IncludeModule("askaron.ibvote");
    if(check_user_golos($_REQUEST["ID"])){
        global $USER;
        $event = new CAskaronIbvoteEvent;
        $arEventFields = array(
            "ELEMENT_ID" =>  $_REQUEST["ID"],
            "ANSWER" => -1,
            "USER_ID" => $USER->GetID(),
        );
        $event->add($arEventFields);
        CModule::IncludeModule("iblock");
        $arFilter = array("ELEMENT_ID" => $_REQUEST["ID"], "ANSWER"=>-1);
        $count =  CAskaronIbvoteEvent::GetList( array(), $arFilter ,array());
        echo $count;
        CModule::IncludeModule("iblock");
        $res = CIBlockElement::GetByID($_REQUEST["ID"]);
        if($ob = $res->GetNextElement()){
            $arProps = $ob->GetProperties();

            $MINUS= $arProps["minus"]["VALUE"]+1;
            $SG = $arProps["summa_golosov"]["VALUE"]+1;

            CIBlockElement::SetPropertyValueCode($_REQUEST["ID"], "minus", $MINUS);
            CIBlockElement::SetPropertyValueCode($_REQUEST["ID"], "summa_golosov", $SG);
            //echo $PLUS;
        }
    }
}


function plus(){
    CModule::IncludeModule("askaron.ibvote");
    if(check_user_golos($_REQUEST["ID"])){
        global $USER;
        $event = new CAskaronIbvoteEvent;
        $arEventFields = array(
            "ELEMENT_ID" =>  $_REQUEST["ID"],
            "ANSWER" => 1,
            "USER_ID" => $USER->GetID(),
        );
        $event->add($arEventFields);

        $arFilter = array("ELEMENT_ID" => $_REQUEST["ID"], "ANSWER"=>1);
        $count =  CAskaronIbvoteEvent::GetList( array(), $arFilter ,array());
        echo $count;
        CModule::IncludeModule("iblock");
        $res = CIBlockElement::GetByID($_REQUEST["ID"]);
        if($ob = $res->GetNextElement()){
            $arProps = $ob->GetProperties();

            $PLUS = $arProps["plus"]["VALUE"]+1;
            $SG = $arProps["summa_golosov"]["VALUE"]+1;

            CIBlockElement::SetPropertyValueCode($_REQUEST["ID"], "plus", $PLUS);
            CIBlockElement::SetPropertyValueCode($_REQUEST["ID"], "summa_golosov", $SG);
            //echo $PLUS;
        }
    }
}

function generate_buts(){
//    FirePHP::getInstance()->info($_REQUEST["ID"],'$_REQUEST["ID"]');
    $output = array();
    foreach($_REQUEST["ID"] as $id_key=>$id):
        $PLUS = $MINUS = 0;
        CModule::IncludeModule("askaron.ibvote");
        $arFilter = array("ELEMENT_ID" => $id);
        $res = CAskaronIbvoteEvent::GetList( array(), $arFilter );
        while($arFields = $res->Fetch()){
            if($arFields["ANSWER"]==1) $PLUS++;
            if($arFields["ANSWER"]==-1) $MINUS++;
        }
        $output[$id] = '
            <div class="plus" ny-id="'.$id.'">'.intval($PLUS).'</div>
            <div class="minus" ny-id="'.$id.'">'.intval($MINUS).'</div>
        ';
    endforeach;

    echo json_encode($output);
    ?>
<?
}

if($_REQUEST["act"]=="minus") minus();
if($_REQUEST["act"]=="plus") plus();
if($_REQUEST["act"]=="generate_buts") generate_buts();
?>