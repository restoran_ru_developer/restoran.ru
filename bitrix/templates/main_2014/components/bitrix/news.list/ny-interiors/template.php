<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
foreach($arResult["ITEMS"] as $key=>$arItem):
    ?>
    <?if(!$arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'][0]){
        $arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'][0] = $arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'];
    }?>
    <div class="one-news-wrap">
        <?
        $arFileTmp = CFile::ResizeImageGet(
            $arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'][0],
            array("width" => 1920, "height" => 1080),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            false
        );
        ?>
        <a href="<?=$arFileTmp['src']?>" class="ny-slider-trigger fancy" rel="detail_group<?=$arItem['ID']?>" >
            <?
            $arFileTmp = CFile::ResizeImageGet(
                $arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'][0],
                array("width" => 148, "height" => 148),
                BX_RESIZE_IMAGE_EXACT,
                false
            );
            ?>
            <img src="<?=$arFileTmp['src']?>" width="148" height="148" alt="<?=$arItem['NAME']?>">
        </a>
        <a href="#" class="one-news-title-wrap" id="detail_group<?=$arItem['ID']?>" link-to-all-photos="<?if($arItem['DISPLAY_PROPERTIES']['REST']['VALUE']):?><?=$arItem['DISPLAY_PROPERTIES']['REST']['LINK_ELEMENT_VALUE'][$arItem['DISPLAY_PROPERTIES']['REST']['VALUE']]['DETAIL_PAGE_URL']?>photos/<?endif?>"><?=$arItem['NAME']?></a>
        <div class="likes-buttons" my-buttons-id="<?=$arItem['ID']?>"></div>

        <?if(count($arItem['PROPERTIES']['PHOTOS']['VALUE'])>1):?>
            <?unset($arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'][0]);?>
            <div class="photos-list-wrapper" style="display: none;">
                <?foreach($arItem['DISPLAY_PROPERTIES']['PHOTOS']['FILE_VALUE'] as $file):?>
                    <?
                    $arFileTmp = CFile::ResizeImageGet(
                        $file,
                        array("width" => 1920, "height" => 1080),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        false
                    );
                    ?>
                    <a href="<?=$arFileTmp['src']?>" class="ny-slider-trigger fancy" rel="detail_group<?=$arItem['ID']?>"></a>
                <?endforeach;?>
            </div>
        <?endif?>
    </div>
    <?if(($key+1)%3==0&&($key+1)!=count($arResult["ITEMS"])):?>
        <div class="ny-delimiter"></div>
    <?endif?>
<?endforeach;?>
