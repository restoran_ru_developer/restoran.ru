<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="pull-left <?if($arItem==end($arResult["ITEMS"])):?>end<?endif?>">
        <?if ($arItem["VIDEO"]):?>
            <div class="pic">
                <a class="myPlayer" href="<?=$arItem["VIDEO"]?>"></a>
            </div>

            <script>
                flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                    clip: {
                            autoPlay: false,
                            autoBuffering: true
                    }
                });
            </script>
        <?else:?>
            <div class="pic">
                <?if ($arItem["VIDEO_PLAYER"]):?>
                    <?=$arItem["VIDEO_PLAYER"]?>
                <?endif;?>
            </div>
        <?endif;?>

        <div class="text">
            <div class="date"><b><?=$arItem['ACTIVE_FROM']?></b></div>
            <h2>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
            </h2>
            <?=$arItem["PREVIEW_TEXT"]?>
        </div>

        <div class="clearfix"></div>
    </div>
<?endforeach;?>
<div class="clearfix"></div>

<div class="more_links text-right">
    <a href="<?=$arResult["LINK"]?>videonews/" class="btn btn-light"><?=GetMessage("ALL_VIDEONEWS")?></a>
</div>


