jQuery.fn.news_galary = function(options){
    var options = jQuery.extend({speed:1000, content_width:978},options);

    var container = jQuery(this).find(".special_scroll");
    var item = container.find(".item");    
    if (item.length!=0)    
        var item_width = item.width()*1+item.css("margin-right").replace("px","")*1;    
    else
        var item_width = 0;
    var all_width = $(window).width();
    var delta_width = Math.round((all_width - options.content_width)/2);
    var c = 0;
    

    this.activate = function(obj) {
        if (container.find(".active:visible").next().hasClass("item")) {
            container.find(".active:visible").removeClass("active");
            obj.addClass("active");
        }
        else {
            container.find(".active:visible").removeClass("active");
            obj.addClass("active");
        }
        if (obj.hasClass("to_begin"))
        {
            container.find(".active:visible").removeClass("active");
            container.find(".item").eq(0).addClass("active");
        }
    };

    this.next = function(obj) {
        this.activate(obj);
        item = container.find(".item");
        var t = item.length/3;
        var num = item.index(obj);
        var left = item_width*(num)-delta_width;        
        obj.parent().stop(true,false).animate({"marginLeft":-left},options.speed,function(){
           if (num>t)
            {
                var p = num-t;
                for (var i=0; i<p; i++)
                {
                    container.find(".item:first").appendTo(container.find(".scroll_container"));
                                        
                }
                container.find(".scroll_container").css("margin-left",(-left+item_width*p)+"px");
            }
            else if(num<t)
            {
                var p = t-num;
                for (var i=0; i<p; i++)
                {
                    container.find(".item:last").prependTo(container.find(".scroll_container"));                    
                }
                container.find(".scroll_container").css("margin-left",(-left-item_width*p)+"px");
            } 
        });                
    };

    this.slide = function(a) {
        container.find(".scroll_container").animate({"marginLeft":-a},options.speed);
    };

    this.init = function() {
        var _this = this;
        var p = Math.floor(delta_width/item_width);
        c = Math.ceil(delta_width/item_width);
        var context = container.find(".scroll_container").html().replace("active","");
        container.find(".scroll_container").prepend(context);
        container.find(".scroll_container").append(context);
                
        this.slide(-(delta_width-item_width*(item.length)));
        container.find(".item").click(function(){
            _this.next($(this));
        });

        $(window).resize(function() {
            item_width = item.width()*1+item.css("margin-right").replace("px","")*1+item.css("padding-right").replace("px","")*1+item.css("padding-left").replace("px","")*1;
            all_width = $(window).width();
            delta_width = Math.round((all_width - options.content_width)/2);
        });
    };
    return this.init();
}



var index=999;
$(document).ready(function() {    
    $(".rest_news_cont").each(function(){
            $(this).news_galary();
        }        
    );    
    // get and show news info
    $('.rest_news_cont .item').on('click', function() {
        var url = "/bitrix/templates/main_2014/components/restoran/catalog.list/main_news_scrollable/get_news.php";
        var newsID = $(this).attr('newsID');
        var newsType = $(this).attr('newsType');
        var newsIB = $(this).attr('newsIB');
        //var newsData = '{"ID": "'+ newsID +'","IBLOCK_TYPE":"'+newsType+'","IBLOCK_ID":"'+newsIB+'"}';                
        var newsData = 'ID='+ newsID +'&IBLOCK_TYPE='+newsType+'&IBLOCK_ID='+newsIB;                
        $(".newsItem:visible").load(url,newsData);                
    });
});