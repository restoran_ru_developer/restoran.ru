<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                 
<?if(count($arResult["ITEMS"])>0):?>     
    <?if ($arParams["TABS"]=="Y"):?>
        <ul class="nav nav-tabs">
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <li <?=($key==0)?"class='active'":""?>>
                <a class="news_scrollable" data-scrollable="<?=$arItem["CODE"]?>_scrollable" href="#<?=$arItem["CODE"]?>" data-toggle="tab"><?=$arItem["NAME"]?></a>
            </li>
            <?endforeach;?>
        </ul>            
        <div class="tab-content">
            <?                        
            foreach($arResult["ITEMS"] as $key=>$arItem):?>
                <div class="tab-pane <?=($key==0)?"active ":""?>big newsItem" id="<?=$arItem["CODE"]?>">
                    <?
                    $filter = "arrNoFilter_".$arItem["CODE"];                    
                    if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
                    {    
                            global $$filter;
                            $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                    }
                    
                    ?>
                    <?$APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "news_main",
                        Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => "1",
                                "SORT_BY1" => ($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]=="blogs")?"created_date":"ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "DESC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                                "PROPERTY_CODE" => array("RESTORAN"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "300",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000007",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "ANOTHER_LINK" => $arItem["PROPERTIES"]["LINK"]["VALUE"]
                        ),
                    false
                    );
                    ?>
                </div>
            <?endforeach;
            
            ?>
        </div>    
    <?else:?>
        <script src="<?=$templateFolder?>/script1.js?12"></script>
        <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <div class="rest_news_cont" id="<?=$arItem["CODE"]?>_scrollable" <?=($key>0)?"style='display: none;'":""?>>
                <?
                $filter = "arrNoFilter_".$arItem["CODE"];
                if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
                {    
                        global $$filter;
                        $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                }      
                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "main_news_scrollable",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                            "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"20",
                            "SORT_BY1" => ($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]=="blogs")?"created_date":"ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "DESC",
                            "FILTER_NAME" => ($$filter)?$filter:"",
                            "FIELD_CODE" => array("DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array(),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "100",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000001",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                    ),
                false
                );?>
            </div>
        <?endforeach;?>
    <?endif;?>
<?endif;?>