<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="pull-left <?=($arItem==end($arResult["ITEMS"]))?"end":""?>">
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>"></a>
        </div>
        <div class="text">
            <div class="date"><b></b><?if($arParams['IBLOCK_TYPE']=='blogs'):?><?=FormatDate('j F Y',strtotime($arItem['DATE_CREATE']))?><?else:?><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?><?endif?></div>
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=HTMLToTxt($arItem["~NAME"])?></a></h2>
            <?=$arItem["PREVIEW_TEXT"]?>
        </div>
        <div class="clearfix"></div>
    </div>    
<?endforeach;?>
<div class="clearfix"></div>
<div class="more_links text-right">                            
    <?if ($arParams["IBLOCK_TYPE"]=="cookery")
        $arParams["IBLOCK_TYPE"] = "masterclasses";?>
    <?if ($arParams["WITHOUT_LINK"]!="Y"):?>
        <a href="<?=$arResult["LINK"]?><?=$arParams["IBLOCK_TYPE"]?>/" class="btn btn-light"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS".$arParams["IBLOCK_TYPE"])?></a>
    <?endif;?>
</div>