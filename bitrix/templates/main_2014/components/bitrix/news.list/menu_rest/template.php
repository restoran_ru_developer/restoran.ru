<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="title" id='menu'><a href="<?=$url?>menu/"><?=GetMessage("CT_BNL_IN_MENU")?></a></div>
<div class="in-menu">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="menu-product-wrapper">
            <div class="product">
                <?if($arItem['PREVIEW_PICTURE']['src']):?>
                    <img src="<?=$arItem['PREVIEW_PICTURE']['src']?>" width="50" height="40">
                <?endif;?>
                <?=$arItem["NAME"]?>
                <?if ($arItem["PRICE"]["PRICE"]>0):?>
                    <?if (CITY_ID=="ast"||CITY_ID=="amt"):?>
                        <span class="price">—&nbsp;<?=$arItem["PRICE"]["PRICE"]?>&nbsp;т.</span>
                    <?else:?>
                        <?if ($arItem["PRICE"]["CURRENCY"]=="EUR"):?>
                            <span class="price">—&nbsp;€&nbsp;<?=$arItem["PRICE"]["PRICE"]?></span>
                        <?else:?>
                            <span class="price">—&nbsp;<?=$arItem["PRICE"]["PRICE"]?>&nbsp;р.</span>
                        <?endif;?>
                    <?endif;?>
                <?endif;?>
            </div>
        </div>
    <?endforeach;?>
    <div class="text-right sefif">
        <div class="last-update-text"><?=GetMessage('CT_BNL_LAST_UPDATE')?></div>
        <div class="menu-last-update-date"><?=$arResult["DATE"]?></div>
    </div>

    <div class="text-right clear-both">
        <?
        $url = $APPLICATION->GetCurPage();
        $url = str_replace("context", "detailed", $url);
        ?>
        <a href="<?=$url?>menu/" class="btn btn-light"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_MENU")?></a>
    </div>
</div>