<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
foreach($arResult["ITEMS"] as $key=>$arItem) {
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    if($arItem["PREVIEW_PICTURE"]){
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 50, 'height' => 40), BX_RESIZE_IMAGE_EXACT, false);
    }

    $arResult["ITEMS"][$key]["NAME"] = str_replace("'", "", $arItem["NAME"]);

    $db_res = CPrice::GetList(
        array(),
        array(
            "PRODUCT_ID" => $arItem["ID"],
            "CATALOG_GROUP_ID" => 1
        )
    );
    if($price = $db_res->Fetch()){
//        $arResult["ITEMS"][$key]["PRICE"] = CPrice::GetBasePrice($arItem["ID"]);
        $arResult["ITEMS"][$key]["PRICE"] = $price;
    }


    if ($arResult["ITEMS"][$key]["PRICE"]["CURRENCY"]=="EUR")
        $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.2f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]); 
    else
        $arResult["ITEMS"][$key]["PRICE"]["PRICE"] = sprintf("%01.0f", $arResult["ITEMS"][$key]["PRICE"]["PRICE"]);

}
if ($arParams["IBLOCK_ID"])
{


    $res = CIBlockElement::GetList(Array("TIMESTAMP_X"=>"DESC"), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]), false, Array("nTopCount"=>1), array('TIMESTAMP_X'));
    $ar_fields = $res->Fetch();
    $arResult["DATE"] = CIBlockFormatProperties::DateFormat('j.m.Y', MakeTimeStamp($ar_fields['TIMESTAMP_X'], CSite::GetDateFormat()));


//    $res = CIBlock::GetByID($arParams["IBLOCK_ID"]);
//    $ar = $res->Fetch();
//    $arResult["DATE"] = CIBlockFormatProperties::DateFormat('j.m.Y', MakeTimeStamp($ar["TIMESTAMP_X"], CSite::GetDateFormat()));
}


?>