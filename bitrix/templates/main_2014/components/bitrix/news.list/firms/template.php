<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="news-list detail_article">
<?if (count($arResult["ITEMS"])>0):?>    
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <?//v_dump($arItem);?>
        <div class="item pull-left<?if($key%3 == 2):?> end<?endif?>">                
            <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
            <div class="pic">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="" src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>"/></a> 
            </div>
            <?endif;?>
            <div class="text">            
                <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                <p><?=$arItem["DISPLAY_PROPERTIES"]["information"]["DISPLAY_VALUE"]?></p>
                <?if ($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]):?>
                    <p><b><?=GetMessage("FAV_PHONE")?></b>: <?=$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]?></p>
                <?endif;?>
                <?if ($arItem["DISPLAY_PROPERTIES"]["adres"]["DISPLAY_VALUE"]):?>
                    <p><b><?=GetMessage("FAV_ADRESS")?></b>: <?=$arItem["DISPLAY_PROPERTIES"]["adres"]["DISPLAY_VALUE"]?></p>
                <?endif;?>
                    <?//v_dump($arItem["DISPLAY_PROPERTIES"]["subway"]["VALUE"])?>
                <?if ($arItem["DISPLAY_PROPERTIES"]["subway"]):?>
                    <p class="metro_<?=CITY_ID?>"><?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])?></p>
                <?endif;?>
            </div>
            <div class="clearfix"></div>
        </div>
            <?if ($key%3 == 2):?>
                    <div class="clearfix dots"></div>
            <?endif;?>
            <?if ($key%3 == 2 && $arItem!=end($arResult["ITEMS"])):?>
                    <div class="clearfix dots"></div>
            <?endif;?>
            <?if ($key%3 != 2 && $arItem==end($arResult["ITEMS"])):?>
                    <div class="clearfix"></div>
            <?endif;?>
    <?endforeach;?>
<?else:?>
    <p class="another"><?=GetMessage("NOTHING_FOUND")?></p>
<?endif;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>    
    <div class="navigation">
        <?=$arResult["NAV_STRING"]?>        
    </div>
<div class="clear"></div>
<?endif;?>
</div>