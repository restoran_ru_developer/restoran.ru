<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_NEWS_COMMENT_TITLE"] = "Comments";
$MESS["CT_BNL_NEWS_COMMENT_SHOW_ALL"] = "Fully";
$MESS["CT_BNL_NEWS_DETAIL_LINK"] = "Further";
$MESS["CT_BNL_ALL_NEWS_LINK"] = "ALL NEWS";
?>