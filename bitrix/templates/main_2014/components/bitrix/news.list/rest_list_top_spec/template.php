<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <?if ($arItem["ID"]=="387409"||$arItem["ID"]=="1099834"):?>
        <noindex><a class="like-h2" href="http://www.federicoclub.ru/" target='_blank'><?=$arItem["NAME"]?></a></noindex>
    <?elseif($arItem["ID"]=="1820154"):?>        
        <noindex><a class="like-h2" href="http://rdisco.ru/" target='_blank'><?=$arItem["NAME"]?></a></noindex>
    <?elseif($arItem["ID"]=="386634"):?>
        <noindex><a class="like-h2" href="http://www.restorantan.ru" target='_blank'><?=$arItem["NAME"]?></a></noindex>
    <?elseif($arItem["ID"]=="388998"):?>
        <noindex><a class="like-h2" href="http://banquet-paradise.ru/halls/" target='_blank'><?=$arItem["NAME"]?></a></noindex>
    <?else:?>
        <div><a class="like-h2" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
    <?endif;?>
    <div class="name_ratio">
        <?for($k = 1; $k <= 5; $k++):?>
            <span class="icon-star<?if($k > $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
        <?endfor?>        
        <?/*if(CSite::InGroup( array(14,15,1))):?>                                    
            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ID"]?>">Редактировать</a></div>
        <?endif;*/?>
    </div>
    <div class="order-buttons">
        <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
            <?if($_REQUEST["CATALOG_ID"]!="banket"):?>
                <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <?else:?>
                <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
            <?endif?>
        <?elseif(CITY_ID=="rga"):?>
            <?if($_REQUEST["CATALOG_ID"]!="banket"):?>
                <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <?else:?>
                <a href="/tpl/ajax/online_order_rest_rgaurm.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
            <?endif?>
        <?endif;?>
        <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>        
    </div>

    <div id="rest-<?=$arItem["ID"]?>" class="carousel slide" data-ride="carousel">                                                
        <div class="carousel-inner">

            <div class="item active">
                <?if ($arItem["ID"]=="387409"||$arItem["ID"]=="1099834"):?>
                    <noindex><a class="like-h2" href="http://www.federicoclub.ru/" target='_blank'><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?elseif($arItem["ID"]=="1820154"):?>
                    <noindex><a class="like-h2" href="http://rdisco.ru/" target='_blank'><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?elseif($arItem["ID"]=="386634"):?>
                    <noindex><a class="like-h2" href="http://www.restorantan.ru" target='_blank'><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?elseif($arItem["ID"]=="388998"):?>
                    <noindex><a class="like-h2" href="http://banquet-paradise.ru/halls/" target='_blank'><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></noindex>
                <?else:?>
                    <div><a class="like-h2" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></div>
                <?endif;?>
            </div>
            <?if(count($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"])>0):?>
                <?foreach($arItem["PROPERTIES"]["photos"]["DISPLAY_VALUE"] as $pKey=>$photo):?>
                    <div class="item">
                        <?if ($arItem["ID"]=="387409"||$arItem["ID"]=="1099834"):?>
                            <noindex><a class="like-h2" href="http://www.federicoclub.ru/" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?elseif($arItem["ID"]=="1820154"):?>
                            <noindex><a class="like-h2" href="http://rdisco.ru/" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?elseif($arItem["ID"]=="386634"):?>
                            <noindex><a class="like-h2" href="http://www.restorantan.ru" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?elseif($arItem["ID"]=="388998"):?>
                            <noindex><a class="like-h2" href="http://banquet-paradise.ru/halls/" target='_blank'><img src="<?=$photo["src"]?>" align="bottom" /></a></noindex>
                        <?else:?>
                            <div><a class="like-h2" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$photo["src"]?>" align="bottom" /></a></div>
                        <?endif;?>                        
                    </div>
                <?endforeach?>
            <?endif;?>
        </div>
        <a class="left carousel-control" href="#rest-<?=$arItem["ID"]?>" role="button" data-slide="prev">
          <span class="galery-control galery-control-left icon-arrow-left2"></span>
        </a>
        <a class="right carousel-control" href="#rest-<?=$arItem["ID"]?>" role="button" data-slide="next">
          <span class="galery-control galery-control-right icon-arrow-right2"></span>
        </a>
    </div>
    <div class="props">                                
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_opening_hours")?></div>
            <div class="value">
                <?if(!is_array($arItem["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_average_bill")?></div>
            <div class="value">
                <?if(!is_array($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_kitchen")?></div>
            <div class="value">
                <?if(!is_array($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])?>
                <?endif?>
            </div>
        </div>
        <div class="col wsubway">
            <div class="name"><?=GetMessage("R_PROPERTY_address")?></div>
            <div class="value">
                <?$arIB = getArIblock("catalog",CITY_ID);?>
                <?=str_replace($arIB["NAME"].", ","",$arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?><br />              
                <?if (is_array($arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])):?>
                    <div class="subway">M</div>
                    <?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]?>
                <?elseif(!empty($arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])):?>
                    <div class="subway">M</div>
                    <?=$arItem["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]?>
                <?endif;?>
            </div>
        </div>
        <div class="col">
            <div class="name"><?=GetMessage("R_PROPERTY_phone")?></div>
            <div class="value">
                <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                    <?if (CITY_ID=="spb"):?>
                    	<a href="tel:7401820" class="<? echo MOBILE;?>">+7(812) 740-18-20</a>
                    <?else:?>
                        <a href="tel:9882656" class="<? echo MOBILE;?>">+7(495) 988-26-56</a>
                    <?endif;?> 
                <?else:
                    
                
                        $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = str_replace(";", "", $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);

                        if(is_array($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])) 
                                $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]=implode(", ",$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);							
                        else{
                                $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]=$arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"];                                
                        }

                       $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"]=  explode(",", $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]);

                        

                        $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                        preg_match_all($reg, $arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"], $matches);
                        
                        

                        $TELs=array();
                        for($p=1;$p<5;$p++){
                            foreach($matches[$p] as $key=>$v){
                                    $TELs[$key].=$v;
                            }
                        }

                        foreach($TELs as $key => $T){
                            if(strlen($T)>7)  $TELs[$key]="+7".$T;
                        }
                        
                        foreach($TELs as $key => $T){
                        ?>
                            <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=clearUserPhone($arItem["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"][$key])?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                        <?}
                endif;?>
            </div>
        </div>
    </div>
<!--    <script>-->
<!--        $(function(){-->
<!--            $('.navigation').css('margin-top','40px');-->
<!--        })-->
<!--    </script>-->
<?endforeach;?>