<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? foreach ($arResult["NEW_ITEMS"] as $cell => $arI): ?>      
    <ul class="nav nav-tabs">
        <?foreach ($arI as $key => $arItem):?>
            <li <?=($key==0)?'class="active"':''?>>
                <?
                $el = "";
                if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                    $el = implode("&el[]=", $arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                }
                $url = "&t=" . $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . "&el[]=" . $el . "&ib=" . $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"] . "&c=" . $arItem["PROPERTIES"]["COUNT"]["VALUE"] . "&s=" . $arItem["PROPERTIES"]["SECTION"]["VALUE"] . "&code=" . $arItem["CODE"];
                ?>
                <a href="#<?=$arItem["CODE"]?>" class="ajax" data-toggle="tab" data-href="<?=SITE_TEMPLATE_PATH?>/components/bitrix/news.list/index_block_new/ajax.php?CITY_ID=<?= CITY_ID ?><?= $url ?>">                        
                    <?= $arItem["NAME"] ?>
                </a>
            </li>
        <?endforeach; ?>
    </ul>
    <div class="tab-content">            
            <? foreach ($arI as $key => $arItem): ?>
            <? if ($key == 0): ?>
                <div class="tab-pane active sm <?=$arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]?>" main="main_page" id="<?=$arItem["CODE"]?>">                        
                    <?
                    $filter = "arrNoFilter_" . $arItem["CODE"];
                    if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                        global $$filter;
                        $$filter = Array("ID" => $arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                    }
                    ?>
                    <?
                    switch ($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]) {
                        case "new_rest":
                            $arIB = getArIblock("catalog", CITY_ID);
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB,
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "created_date",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "NEW_REST"=>"Y",
                                "REST_PROPS" => "Y"
                                    ), false
                            );                            
                            break;
                        case "news":
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000002",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                'LINK_NAME'=>'All places',
                                'LINK'=>'/'.CITY_ID.'/news/newplace/'
                                    ), false
                            );                            
                            break;
                        case "firms_news":
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                                    ), false
                            );                            
                            break;
                        case "ratings":
                            if ($arItem["CODE"] == 'ratings1') {
                                global $$filter;
                                $a = "Да";
                                $$filter = array("!PROPERTY_sleeping_rest_VALUE" => $a);
                                $arIB = getArIblock("catalog", CITY_ID);
                                $APPLICATION->IncludeComponent(
                                        "restoran:catalog.list", "new_rest_main", Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                    "SORT_BY1" => "PROPERTY_rating_date",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "PROPERTY_stat_day",
                                    "SORT_ORDER2" => "DESC",
                                    "SORT_ORDER2" => "DESC",
                                    "SORT_BY3" => "NAME",
                                    "FILTER_NAME" => $filter,
                                    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "restaurants",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "3601",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "REST_PROPS" => "Y"
                                        ), false
                                );
                                unset($$filter["!PROPERTY_sleeping_rest_VALUE"]);                                
                            } elseif ($arItem["CODE"] == 'ratings2') {
                                $arIB = getArIblock("catalog", CITY_ID);
                                $a = "Да";
                                $$filter = array("!PROPERTY_sleeping_rest_VALUE" => $a);
                                //$$filter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
                                $APPLICATION->IncludeComponent(
                                        "restoran:catalog.list", "new_rest_main", Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                    "SORT_BY1" => "PROPERTY_rating_date",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "PROPERTY_stat_day",
                                    "SORT_ORDER2" => "DESC",
                                    "SORT_BY3" => "NAME",
                                    "SORT_ORDER3" => "ASC",
                                    "FILTER_NAME" => ($$filter) ? $filter : "",
                                    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "banket",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000001",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "REST_PROPS" => "Y"
                                        ), false
                                );
                                unset($$filter["!PROPERTY_sleeping_rest_VALUE"]);                                
                            }
                            if ($arItem["CODE"] == 'ratings3') {
                                $arIB = getArIblock("catalog", CITY_ID);
                                global $arrfil;
                                $arrfil["!PROPERTY_restoran_ratio"] = false;
                                $APPLICATION->IncludeComponent(
                                        "restoran:catalog.list", "new_rest_main", Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                    "SORT_BY1" => "PROPERTY_restoran_ratio",
                                    "SORT_ORDER1" => "asc,nulls",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arrfil",
                                    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("RATIO", "COMMENTS"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000001",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "REST_PROPS" => "Y"
                                        ), false
                                );                               
                            }
                            break;
                        case "videonews":
                            $APPLICATION->IncludeComponent(
                                    "bitrix:news.list", "videonews_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => "2",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("LIST_PAGE_URL"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                                    ), false
                            );
                            break;
                        case "kupons":
                            $APPLICATION->IncludeComponent(
                                    "bitrix:news.list", "kupons", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("ACTIVE_TO"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS", "subway"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "100",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                                    ), false
                            );                            
                            break;
                        case "overviews":
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                                    ), false
                            );                            
                            break;
                        case "reviews":
                        case "comment":
                            $APPLICATION->IncludeComponent(
                                    "bitrix:news.list", "reviews_main", Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "created_date",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY"),
                                "PROPERTY_CODE" => array("COMMENTS", "ELEMENT"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "150",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000006",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                                    ), false
                            );
                            break;
                        case "cookery":
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "created_date",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("comments"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "150",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "14403",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",                                
                                    ), false
                            );
                            break;
                        case "photoreports":
                            ?>
                            <div id="photogalery" class="photogalery">
                                <?
                                $APPLICATION->IncludeComponent(
                                        "bitrix:news.list", "photoreports", Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => ($$filter) ? $filter : "",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL"),
                                    "PROPERTY_CODE" => array("PHOTOS"),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000001",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                                        ), false
                                );
                                ?>
                            </div>
                            <?                            
                            break;
                        case "interview":
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "360000001",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                                    ), false
                            );
                            break;
                        case "afisha":

//                            if($USER->IsAdmin()){
//                                $APPLICATION->IncludeComponent(
//                                    "bitrix:news.list", "afisha_list_main", Array(
//                                "DISPLAY_DATE" => "N",
//                                "DISPLAY_NAME" => "Y",
//                                "DISPLAY_PICTURE" => "Y",
//                                "DISPLAY_PREVIEW_TEXT" => "Y",
//                                "AJAX_MODE" => "N",
//                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
//                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
//                                "NEWS_COUNT" => "550",
//                                "SORT_BY1" => "SORT",
//                                "SORT_ORDER1" => "ASC",
//                                "SORT_BY2" => "SORT",
//                                "SORT_ORDER2" => "ASC",
//                                "FILTER_NAME" => ($$filter) ? $filter : "",
//                                "FIELD_CODE" => array("DETAIL_PICTURE"),
//                                "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE"),
//                                "CHECK_DATES" => "Y",
//                                "DETAIL_URL" => "",
//                                "PREVIEW_TRUNCATE_LEN" => "200",
//                                "ACTIVE_DATE_FORMAT" => "j F Y",
//                                "SET_TITLE" => "N",
//                                "SET_STATUS_404" => "N",
//                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                                "ADD_SECTIONS_CHAIN" => "N",
//                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                                "PARENT_SECTION" => "",
//                                "PARENT_SECTION_CODE" => "",
//                                "CACHE_TYPE" => "Y",
//                                "CACHE_TIME" => "7204",
//                                "CACHE_FILTER" => "Y",
//                                "CACHE_GROUPS" => "N",
//                                "DISPLAY_TOP_PAGER" => "N",
//                                "DISPLAY_BOTTOM_PAGER" => "N",
//                                "PAGER_TITLE" => "Новости",
//                                "PAGER_SHOW_ALWAYS" => "N",
//                                "PAGER_TEMPLATE" => "",
//                                "PAGER_DESC_NUMBERING" => "N",
//                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                                "PAGER_SHOW_ALL" => "N",
//                                "AJAX_OPTION_JUMP" => "N",
//                                "AJAX_OPTION_STYLE" => "Y",
//                                "AJAX_OPTION_HISTORY" => "N"
//                                    ), false
//                            );
//                            break;

//                                // ToDo доп выборка внутри только по PROPERTY_EVENT_DATE == today;
//                                global $arFilter;
//                                $arAfishaIB = getArIblock("afisha", CITY_ID);
//                                $arFilter = Array(
//                                    "IBLOCK_ID"=>$arAfishaIB["ID"],
//                                    Array("LOGIC"=>"OR",
//                                        Array("PROPERTY_EVENT_DATE"=>date('d.m.Y')),
//                                        Array("PROPERTY_EVENT_DATE"=>false)
//                                    ),
//                                    Array("LOGIC"=>"OR",
////                                        Array('PROPERTY_EVENT_DATE'=>date('d.m.Y')),
//                                        Array("!PROPERTY_d1"=>false),
//                                        Array("!PROPERTY_d2"=>false),
//                                        Array("!PROPERTY_d3"=>false),
//                                        Array("!PROPERTY_d4"=>false),
//                                        Array("!PROPERTY_d5"=>false),
//                                        Array("!PROPERTY_d6"=>false),
//                                        Array("!PROPERTY_d7"=>false)
//                                    ),
//                                    Array("LOGIC"=>"OR",
//                                        Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y h:i:s")),
//                                        Array("DATE_ACTIVE_TO"=>false)
//                                    ),
//
//                                    'PROPERTY_RESTORAN.ACTIVE'=>'Y'
//                                );
//
//                                $APPLICATION->IncludeComponent(
//                                    "bitrix:news.list", "afisha_list_main_new", Array(
//                                    "DISPLAY_DATE" => "N",
//                                    "DISPLAY_NAME" => "Y",
//                                    "DISPLAY_PICTURE" => "Y",
//                                    "DISPLAY_PREVIEW_TEXT" => "Y",
//                                    "AJAX_MODE" => "N",
//                                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
//                                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
//                                    "NEWS_COUNT" => "500",
//                                    "SORT_BY1" => "PROPERTY_EVENT_DATE",
//                                    "SORT_ORDER1" => "DESC",
//
//
//                                    "SORT_BY2" => "PROPERTY_EVENT_DATE",
//                                    "SORT_ORDER2" => "DESC",
//                                    "FILTER_NAME" => ($$filter) ? $filter : "arFilter",
//                                    "FIELD_CODE" => array("DETAIL_PICTURE"),
//                                    "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
//                                    "CHECK_DATES" => "Y",
//                                    "DETAIL_URL" => "",
//                                    "PREVIEW_TRUNCATE_LEN" => "200",
//                                    "ACTIVE_DATE_FORMAT" => "j F Y",
//                                    "SET_TITLE" => "N",
//                                    "SET_STATUS_404" => "N",
//                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                                    "ADD_SECTIONS_CHAIN" => "N",
//                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                                    "PARENT_SECTION" => "",
//                                    "PARENT_SECTION_CODE" => "",
//                                    "CACHE_TYPE" => $USER->IsAdmin()?'N':"Y",//y
//                                    "CACHE_TIME" => "7205",
//                                    "CACHE_FILTER" => "Y",
//                                    "CACHE_GROUPS" => "N",
//                                    "DISPLAY_TOP_PAGER" => "N",
//                                    "DISPLAY_BOTTOM_PAGER" => "N",
//                                    "PAGER_TITLE" => "Новости",
//                                    "PAGER_SHOW_ALWAYS" => "N",
//                                    "PAGER_TEMPLATE" => "",
//                                    "PAGER_DESC_NUMBERING" => "N",
//                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                                    "PAGER_SHOW_ALL" => "N",
//                                    "AJAX_OPTION_JUMP" => "N",
//                                    "AJAX_OPTION_STYLE" => "Y",
//                                    "AJAX_OPTION_HISTORY" => "N"
//                                ), false
//                                );
//                                break;
//                            }
//                            else {
                                global $arFilter;
                                $arAfishaIB = getArIblock("afisha", CITY_ID);
                                $arFilter = Array(
                                    "IBLOCK_ID"=>$arAfishaIB["ID"],
                                    Array("LOGIC"=>"OR",
                                        Array("!PROPERTY_d1"=>false),
                                        Array("!PROPERTY_d2"=>false),
                                        Array("!PROPERTY_d3"=>false),
                                        Array("!PROPERTY_d4"=>false),
                                        Array("!PROPERTY_d5"=>false),
                                        Array("!PROPERTY_d6"=>false),
                                        Array("!PROPERTY_d7"=>false),
                                        Array(">=PROPERTY_EVENT_DATE"=>date("d.m.Y h")),
//                                        Array("PROPERTY_d1"=>false,"PROPERTY_d2"=>false,"PROPERTY_d3"=>false,"PROPERTY_d4"=>false,"PROPERTY_d5"=>false,"PROPERTY_d6"=>false,"PROPERTY_d7"=>false,'!PROPERTY_EVENT_DATE'=>false),
                                    ),
                                    Array("LOGIC"=>"OR",
                                        Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y h")),
                                        Array("DATE_ACTIVE_TO"=>false)
                                    ),
//                                '!PROPERTY_RESTORAN.PROPERTY_sleeping_rest'=>'Да',
                                    'PROPERTY_RESTORAN.ACTIVE'=>'Y'
                                );
//                                FirePHP::getInstance()->info($arFilter,'$arFilter');
                                $APPLICATION->IncludeComponent(
                                    "bitrix:news.list", "afisha_list_main_new", Array(
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "550",
                                    "SORT_BY1" => "SORT",
                                    "SORT_ORDER1" => "ASC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => ($$filter) ? $filter : "arFilter",
                                    "FIELD_CODE" => array("DETAIL_PICTURE"),
                                    "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "200",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "Y",//y//$USER->IsAdmin()?'N'
                                    "CACHE_TIME" => "7207",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N"
                                ), false
                                );
                                break;
//                            }



                        case "blogs":
                            $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list", "new_rest_main", Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"]) ? $arItem["PROPERTIES"]["COUNT"]["VALUE"] : "3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000005",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "LINK"=> "/".CITY_ID."/blogs/",
                                "LINK_NAME"=> "Все блоги"        
                                    ), false
                            );
                            break;
                        default:

                            break;
                    }
                    ?>
                </div>
            <? else: ?>
                <div class="tab-pane sm" main="main_page" id="<?=$arItem["CODE"]?>"></div>
            <? endif; ?>
        <? endforeach; ?>
    </div>
<? endforeach; ?>