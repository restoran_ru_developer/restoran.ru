<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_NEWS_COMMENT_TITLE"] = "Комментарии";
$MESS["CT_BNL_NEWS_COMMENT_SHOW_ALL"] = "Полностью";
$MESS["CT_BNL_NEWS_DETAIL_LINK"] = "Далее";
$MESS["CT_BNL_ALL_NEWS_LINK"] = "ВСЕ НОВОСТИ";
$MESS["CT_BNL_ALL_NEWS_LINK_news"] = "НОВОСТИ";
$MESS["CT_BNL_ALL_NEWS_LINK_special_projects"] = "НОВОСТИ";
$MESS["CT_BNL_ALL_NEWS_LINK_overviews"] = "обзоры";
$MESS["CT_BNL_ALL_NEWS_LINK_blogs"] = "статьи";
$MESS["CT_BNL_ALL_NEWS_LINK_interview"] = "ВСЕ интервью";
$MESS["CT_BNL_ALL_NEWS_LINK_videonews"] = "новости";
$MESS["CT_BNL_ALL_NEWS_LINK_photoreports"] = "фотоотчеты";
$MESS["CT_BNL_ALL_NEWS_LINK_cookery"] = "рецепты от шефов";
$MESS["CT_BNL_ALL_NEWS_LINK_cookery_editor"] = "restoran.ru";
$MESS["CT_BNL_ALL_NEWS_LINK_new_places"] = "открытия";
$MESS["CT_BNL_ALL_NEWS_LINK_criticism"] = "критика";
$MESS["CT_BNL_ALL_NEWS_LINK_afisha"] = "афиша";
$MESS["CT_BNL_ALL_NEWS_LINK_critic"] = "критика";
?>