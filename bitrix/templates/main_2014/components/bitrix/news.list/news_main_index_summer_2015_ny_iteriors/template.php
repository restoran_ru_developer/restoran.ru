<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["ITEMS"] as $key=>$arItem):
    $arParams["ANOTHER_LINK"] = '/ny-interiors/';
    ?>
    <div class="pull-left <?if(end($arResult["ITEMS"])==$arItem&&$arParams['NEWS_COUNT']!=1):?>end<?endif?>">
        <div class="pic">
            <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>">
                <img src="<?=$arItem["DETAIL_PICTURE"]['src']?>"></a>
        </div>
        <div class="text">
            <div class="center-post-cut">
                <h2><a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></h2>
            </div>
        </div>
    </div>

    <?if(($key+1)%4==0 &&$arItem!=end($arResult["ITEMS"])&&$arParams['NEWS_COUNT']!=1&&$arParams['CENTER_POST']!='Y'):?>
        <div class="clearfix"></div>
    <?endif?>
<?endforeach;?>
<div class="more_links text-right">
    <a href="<?=$arParams["ANOTHER_LINK"]?>" class="btn btn-light">ВСЕ ИНТЕРЬЕРЫ</a>
</div>