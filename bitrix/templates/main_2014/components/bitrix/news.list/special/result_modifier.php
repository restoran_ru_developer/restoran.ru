<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!CModule::IncludeModule("iblock"))
    return false;
foreach($arResult["ITEMS"] as $key=>&$arItem) :
    $arParams["NAME"] = $arItem["NAME"];
    foreach ($arItem["PROPERTIES"]["RECEPTS"]["VALUE"] as $key=>$recept)
    {
        $ar = array();
        //if ($key<2)
        {
            $res = CIBlockElement::GetList(Array(),Array("ID"=>$recept,"IBLOCK_ID"=>139),false,false,Array("PROPERTY_ratio","DETAIL_PAGE_URL","ID","DETAIL_PICTURE","CODE","IBLOCK_SECTION_ID","NAME","PREVIEW_TEXT"));
            if ($ar = $res->Fetch())
            {
                //$ar["NAME"] = change_quotes($ar["NAME"]);
                //$ar["PREVIEW_TEXT"] = change_quotes($ar["PREVIEW_TEXT"]);
                $ar["PREVIEW_PICTURE"] = CFile::ResizeImageGet($ar["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
                $ar["DETAIL_PAGE_URL"] = "/content/cookery/".RestIBlock::getCookerySection($ar["IBLOCK_SECTION_ID"])."/".$ar["CODE"]."/";
                $arItem["RECEPTS"][] = $ar;
            }
        }
    }
endforeach;
?>