<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h1><?=$arResult["ITEMS"][0]["NAME"]?></h1>
<div class='clearfix'></div>
<p><?=$arResult["ITEMS"][0]["DETAIL_TEXT"]?></p>
<div class="news-list">
    <?foreach ($arResult["ITEMS"][0]["RECEPTS"] as $key=>$arItem):?>
        <div class="item pull-left<? if ($key % 3 == 2): ?> end<? endif ?>">
            <div class="pic">                    
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" width="232" /></a>                                                   
            </div>
            <div class="text">
                    <h2><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a></h2>
                    <?= $arItem["PREVIEW_TEXT"] ?>
            </div>                                                    
        </div>
        <? if ($key % 3 == 2&&$arItem!=end($arResult["ITEMS"])): ?>
            <div class="clearfix"></div>
        <?endif;?>                        
    <?endforeach;?>                    
</div>