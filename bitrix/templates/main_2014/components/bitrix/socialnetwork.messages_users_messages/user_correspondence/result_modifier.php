<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$curUserID = $USER->GetID();
$secUserID = $arParams["USER_ID"];

$rsCurUser = CUser::GetByID($curUserID);
$arCurUser = $rsCurUser->Fetch();
$arCurUser["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($arCurUser["PERSONAL_PHOTO"], array('width'=>74, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true);
// check user restorator stat
$arGroups = CUser::GetUserGroup($curUserID);
if(in_array(RESTORATOR_GROUP, $arGroups))
    $arCurUser["IS_RESTORATOR"] = true;

$rsSecUser = CUser::GetByID($secUserID);
$arSecUser = $rsSecUser->Fetch();
$arSecUser["USER_PERSONAL_PHOTO_FILE"] = CFile::ResizeImageGet($arSecUser["PERSONAL_PHOTO"], array('width'=>74, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true);
// check user restorator stat
$arGroups = CUser::GetUserGroup($secUserID);
if(in_array(RESTORATOR_GROUP, $arGroups))
    $arSecUser["IS_RESTORATOR"] = true;

$arTmpEvents = Array();
foreach($arResult["Events"] as $key=>$event) {
    // get only next msg
    if($event["ID"] > $arParams["EVENT_ID"]) {
        // format date
        $arTmpEvents[$key]["DATE_CREATE_FORMATED"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($event["DATE_CREATE_FORMAT"], CSite::GetDateFormat()));
        $tmpDate = explode(' ', $arTmpEvents[$key]["DATE_CREATE_FORMATED"]);
        $arTmpEvents[$key]["DATE_CREATE_FORMATED_1"] = $tmpDate[0];
        $arTmpEvents[$key]["DATE_CREATE_FORMATED_2"] = strtolower($tmpDate[1])." ".$tmpDate[2];
        $arTmpEvents[$key]["DATE_CREATE_FORMATED_3"] = CIBlockFormatProperties::DateFormat("G:i", MakeTimeStamp($event["DATE_CREATE_FORMAT"], CSite::GetDateFormat()));

        $arTmpEvents[$key]["MESSAGE"] = $event["MESSAGE"];

        // get user info
        switch($event["WHO"]) {
            case "IN":
                $arTmpEvents[$key]["USER_NAME"] = $arSecUser["NAME"];
                $arTmpEvents[$key]["IS_RESTORATOR"] = $arSecUser["IS_RESTORATOR"];
                $arTmpEvents[$key]["USER_PERSONAL_PHOTO_FILE"] = $arSecUser["USER_PERSONAL_PHOTO_FILE"];
            break;
            case "OUT":
                $arTmpEvents[$key]["USER_NAME"] = $arCurUser["NAME"];
                $arTmpEvents[$key]["IS_RESTORATOR"] = $arCurUser["IS_RESTORATOR"];
                $arTmpEvents[$key]["USER_PERSONAL_PHOTO_FILE"] = $arCurUser["USER_PERSONAL_PHOTO_FILE"];
            break;
        }
    }
}

$arResult["Events"] = Array();
$arResult["Events"] = $arTmpEvents;
?>