<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
<script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />

<script type="text/javascript">
    $(document).ready(function(){
        $('.QapTcha').QapTcha({
            txtLock : 'Сдвиньте слайдер вправо.',
            txtUnlock : 'Готово. Теперь можно отправлять',
            disabledSubmit : true,
            autoRevert : true,
            PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
            autoSubmit : false});
                    
                    
        $(".fancy").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200, 
            'speedOut'		:	200, 
            //'overlayShow'	:	false
        });
    });
</script>
<? //v_dump($arResult["IBLOCK"]["NAME"]);    ?>
<?
$rsUser = CUser::GetByID($arResult["CREATOR_ID"]);
$arUser = $rsUser->Fetch();
//v_dump($arUser);
?>

<script>
    $(document).ready(function(){
        $("#user_mail_send").click(function(){
            var _this = $(this);
            $.ajax({
                type: "POST",
                url: _this.attr("href"),
                data: "USER_NAME=<?= $arUser["NAME"] . " " . $arUser["LAST_NAME"] ?>&USER_ID=<?= intval($arUser["ID"]) ?>&<?= bitrix_sessid_get() ?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                    }
                    $('#mail_modal').html(data);
                    showOverflow();
                    setCenter($("#mail_modal"));
                    $("#mail_modal").fadeIn("300"); 
                }
            });
            return false;
        });
    });
</script>
<div class="content-wrapper">
    <!-- CONTENT -->
    <div class="content-content">
        <div style="float:left; width: 600px;">
            <h1 class="content-h nopadding-bottom"><?= $arResult["NAME"] ?></h1>
            <p><span class="date-field"><span class="date-day"><?= $arResult["DISPLAY_ACTIVE_FROM_1"] ?></span> <?= $arResult["DISPLAY_ACTIVE_FROM_2"] ?> <?= $arResult["DISPLAY_ACTIVE_FROM_3"] ?></span></p>
        </div>
        <div style="float:right; width: 128px; padding-top: 20px;">
            <div class="print">
                <a target="_blank" href="?print=Y">Распечатать</a>
            </div>
        </div>
        <div class="clear"></div>
        <div class="info-detail-wrapper">
            <div class="info-detail-big">
                <? if (!empty($arResult['PROPERTIES']["COMPANY_NAME"]["VALUE"])) { ?>
                    <h3 class="content-h"><?= $arResult['PROPERTIES']["COMPANY_NAME"]["VALUE"] ?></h3>
                <? } ?>

                <? if (!empty($arResult['PROPERTIES']["COMPANY_FIO"]["VALUE"])) { ?>
                    <div class="block-param">
                        <span class="block-param-title">Контактное лицо:</span> <span class="block-param-content"><?= $arResult['PROPERTIES']["COMPANY_FIO"]["VALUE"] ?></span>
                    </div>
                <? } ?>
                <? if (!empty($arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"])) { ?>
                    <? if ($USER->IsAuthorized()) { ?>
                        <div class="block-param">
                            <span class="block-param-title">Телефон:</span> <span class="block-param-content"><?= $arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"] ?></span>
                        </div>
                    <? } ?>
                <? } ?>
                <br class="block-param-break">
                <? if (!empty($arResult['PROPERTIES']["ADDRESS"]["VALUE"])) { ?>
                    <div class="block-param">
                        <span class="block-param-title">Адрес:</span> <span class="block-param-content"><?= $arResult['PROPERTIES']["ADDRESS"]["VALUE"] ?></span>
                    </div>
                <? } ?>

                <? if (!empty($arResult['PROPERTIES']["SUBWAY_BIND"]["VALUE"])) { ?>
                    <div class="metro_<?= CITY_ID ?>"><?= implode(', ', $arResult['PROPERTIES']["SUBWAY_BIND"]["VALUE"]) ?></div>
                <? } ?>


                <br class="block-param-break">
                <? if (!empty($arResult['PROPERTIES']["site"]["VALUE"])) { ?>
                    <div class="block-param-url italic">
                        <a href="<?= $arResult['PROPERTIES']["site"]["VALUE"] ?>"><?= $arResult['PROPERTIES']["site"]["VALUE"] ?></a>
                    </div>
                <? } ?>


            </div>
            <div class="info-detail-small">
                <div class="avatar vacancy-detail-image">
                    <a href="/users/id<?= $arResult["CREATOR_ID"] ?>/">
                        <? if ($arResult["CREATOR_PHOTO"]): ?>
                            <img src="<?= $arResult["CREATOR_PHOTO"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" width="64"/>
                        <? endif; ?>
                    </a>
                </div>
                <div class="vacancy-detail-info">
                    <div class="vacancy-detail-info-name">
                        <a href="/users/id<?= $arResult["CREATOR_ID"] ?>/"><?= $arResult["CREATOR_NAME"] ?></a>
                    </div>
                    <div class="vacancy-detail-info-city"><?= $arResult["IBLOCK"]["NAME"] ?></div>
                    <div class="vacancy-detail-info-send-message">
                        <div class="" style="margin-top:7px; margin-right:15px"><a href="/tpl/ajax/im.php" id="user_mail_send">Сообщение</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vacancy-params">

            <?
            $dontShowParams = empty($arResult["PROPERTIES"]["WAGES_OF"]["VALUE"]) && empty($arResult["PROPERTIES"]["WAGES_TO"]["VALUE"])
                    && empty($arResult["PROPERTIES"]["SHEDULE"]["VALUE"]) && empty($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"])
                    && empty($arResult["PROPERTIES"]["EDUCATION"]["VALUE"]) && empty($arResult["PROPERTIES"]["RESPONSIBILITY"]["VALUE"])
                    && empty($arResult["PROPERTIES"]["AGE_FROM"]["VALUE"]) && empty($arResult["PROPERTIES"]["AGE_TO"]["VALUE"]);

            if (!$dontShowParams) {
                ?>
                <div class="vacancy-params-left">
                    <h3 class="content-h">Условия и требования</h3>
                    <dl>
                        <? if (!empty($arResult["PROPERTIES"]["WAGES_OF"]["VALUE"]) || !empty($arResult["PROPERTIES"]["WAGES_TO"]["VALUE"])) { ?>
                            <dt>Заработная плата</dt>
                            <dd><?= $arResult["PROPERTIES"]["WAGES_OF"]["VALUE"] ?> &mdash; <?= $arResult["PROPERTIES"]["WAGES_TO"]["VALUE"] ?> <span class="rouble font12">e</span></dd>
                        <? } ?>
                        <? if (!empty($arResult["PROPERTIES"]["SHEDULE"]["VALUE"])) { ?>
                            <dt>График работы</dt>
                            <dd><?//= implode(" / ", $arResult["PROPERTIES"]["SHEDULE"]["VALUE"]) ?>
                                <?=$arResult["PROPERTIES"]["SHEDULE"]["VALUE"]?>
                            </dd>
                        <? } ?>
                        <? if (!empty($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"])) { ?>
                            <dt>Опыт работы</dt>
                            <dd><?= $arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></dd>
                        <? } ?>
                        <? if (!empty($arResult["PROPERTIES"]["EDUCATION"]["VALUE"])) { ?>
                            <dt>Образование</dt>
                            <dd><?= $arResult["PROPERTIES"]["EDUCATION"]["VALUE"] ?></dd>
                        <? } ?>
                        <? if (!empty($arResult["PROPERTIES"]["AGE_FROM"]["VALUE"]) || !empty($arResult["PROPERTIES"]["AGE_TO"]["VALUE"])) { ?>
                            <dt>Возраст</dt>
                            <dd><?= $arResult["PROPERTIES"]["AGE_FROM"]["VALUE"] ?> — <?= $arResult["PROPERTIES"]["AGE_TO"]["VALUE"] ?> лет</dd>
                        <? } ?>
                        <? if (!empty($arResult["PROPERTIES"]["RESPONSIBILITY"]["VALUE"])) { ?>
                            <dt>Обязанности</dt>
                            <dd><?= $arResult["PROPERTIES"]["RESPONSIBILITY"]["VALUE"] ?></dd>
                        <? } ?>
                    </dl>
                </div>


            <? } ?>



            <? if (!empty($arResult["PROPERTIES"]["ADD_INFO"]["VALUE"])) { ?>
                <div class="vacancy-params-right">
                    <h3 class="content-h">Дополнительно</h3>
                    <div class="vacancy-params-content italic"><?= $arResult["PROPERTIES"]["ADD_INFO"]["VALUE"] ?></div>
                </div>
            <? } ?>
        </div>
        <div class="panel-actions">
            <div class="block-info-comments">
                Комментарии: (<a href="#comments"><?= intval($arResult["PROPERTIES"]["COMMENTS"]["VALUE"]) ?></a>)
            </div>

            <div class="panel-action-likes">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:asd.share.buttons", "likes", Array(
                    "ASD_TITLE" => $arResult["NAME"],
                    "ASD_URL" => $arResult["DETAIL_PAGE_URL"],
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => $arResult["NAME"],
                    "ASD_INCLUDE_SCRIPTS" => array(0 => "FB_LIKE", 1 => "VK_LIKE", 2 => "TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                        )
                );
                ?>
            </div>
            <div class="panel-action-response">
                <? if ($USER->IsAuthorized()) { ?>
                    <form id="response_vacancy_form" action="<?= $templateFolder ?>/response.php">
                        <?= bitrix_sessid_post() ?>
                        <input type="hidden" name="vacancy_id" value="<?= $arResult["ID"] ?>" />
                        <a href="javascript:void(0)" id="response_vacancy">Откликнуться</a>
                    </form>
                <? } else { ?>
                    <form id="response_vacancy_form">
                        <a onclick="show_popup(this,{'url':true,'css':{'left':'-90px','top':'-138px'}});" href="/tpl/ajax/auth.php">Откликнуться</a>
                    </form>
                <? } ?>
            </div>
            <div class="panel-action-tofavourite">
                <? if ($USER->IsAuthorized()) { ?>
                    <?
                    $params = array();
                    $params["id"] = $arResult["ID"];
                    $params = addslashes(json_encode($params));
                    ?>
                    <a href="#" onclick='javascript: send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?= $params ?>", addFavouriteHandler); return false;'>В избранное</a>
                <? } else { ?>
                    <div id="addToFavourite">
                        <a onclick="show_popup(this,{'url':true,'css':{'left':'-110px','top':'-138px'}});" href="/tpl/ajax/auth.php">В избранное</a>
                    </div>
                <? } ?>
            </div>
        </div>
        <div id="comments"></div>
        <div class="clear"><br /></div>
        <?
//        $APPLICATION->IncludeComponent(
//                "restoran:comments_add_new", "new", Array(
//            "IBLOCK_TYPE" => "comment",
//            "IBLOCK_ID" => (SITE_ID == "s1") ? "2438" : "2641",
//            "ELEMENT_ID" => $arResult["ID"],
//            "IS_SECTION" => "N",
//            "OPEN" => "N"
//                ), false
//        );
        ?>
        <div id="tabs_block8" style="margin-top: 15px;">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#vk-reviews-vacancies" data-toggle="tab">
                        <?= GetMessage("R_VK") ?>
                    </a>
                </li>
                <li>
                    <a href="#fb-reviews-vacancies" data-toggle="tab">
                        <?= GetMessage("R_FACEBOOK") ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" style="" id="vk-reviews-vacancies">
                    <!-- Put this script tag to the <head> of your page -->
                    <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>

                    <script type="text/javascript">
                        VK.init({apiId: 2881483, onlyWidgets: true});
                    </script>

                    <!-- Put this div tag to the place, where the Comments block will be -->
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                        VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                    </script>
                </div>
                <div class="tab-pane" id="fb-reviews-vacancies">
                    <div class="fb-comments" data-href="http://www.restoran.ru<?= $APPLICATION->GetCurPage() ?>" data-num-posts="20" data-width="728"></div>
                </div>
            </div>            
        </div>
    </div>

    <!-- SIDEBAR -->
    <div class="content-sidebar">
        <div id="search_article">
            <?
//            $APPLICATION->IncludeComponent(
//                    "bitrix:subscribe.form", "main_page_subscribe", Array(
//                "USE_PERSONALIZATION" => "Y",
//                "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
//                "SHOW_HIDDEN" => "N",
//                "CACHE_TYPE" => "A",
//                "CACHE_TIME" => "3600",
//                "CACHE_NOTES" => ""
//                    ), false
//            );
            ?>           
        </div>
        <div align="center">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner", "", Array(
                "TYPE" => "right_2_main_page",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
                    ), false
            );
            ?>
        </div>
        <br /><br />
        <?
        global $arrFilterPopular;
        $arrFilterPopular = Array(
            "!CODE" => $_REQUEST["CODE"]
        );
        $APPLICATION->IncludeComponent(
                "bitrix:news.list", "vacancy_short_right", Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "vacancy",
            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
            "NEWS_COUNT" => "1",
            "SORT_BY1" => "SHOWS",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterPopular",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array("WAGES_OF", "WAGES_TO", "EXPERIENCE", "SUBWAY_BIND", "COMMENTS"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
                ), false
        );
        ?>
        <br />
        <div align="center">
            <?
            $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner", "", Array(
                "TYPE" => "right_1_main_page",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
                    ), false
            );
            ?>
        </div>
        <br /><br />
        <?
        $APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/order_rest.php"), Array(), Array("MODE" => "html")
        );
        ?>
    </div>
</div>

