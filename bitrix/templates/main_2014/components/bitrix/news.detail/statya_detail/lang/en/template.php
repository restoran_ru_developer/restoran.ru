<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["NEXT_POST"] = "Previous post";
$MESS["PREV_POST"] = "Next post";
$MESS["CUISINE"] = "Kitchen";
$MESS["AVERAGE_BILL"] = "Average check";
$MESS["TAGS_TITLE"] = "Tags";
$MESS["R_ADD2FAVORITES"] = "Add to favorites";
$MESS["R_COMMENTS"] = "Comments";
$MESS["SUBSCRIBE"] = "подписаться";
$MESS["UNSUBSCRIBE"] = "отписаться";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_VK"] = "VK.com";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["READ_A"] = "Read also";
$MESS["R_BOOK_TABLE2"] = "Table<br />reservation";
$MESS["R_ORDER_BANKET2"] = "Banquet<br />reservation";
$MESS["R_BOOK_TABLE"] = "Book a table";
$MESS["R_ORDER_BANKET"] = "Order a banquet";
$MESS["R_BUFFET"] = "Official buffet";
$MESS["R_IN"] = "in";
$MESS["R_AT"] = "at";
$MESS["R_ON"] = "on";
$MESS["R_FOR"] = "for";
$MESS["R_PERSONS"] = "persons";
$MESS["R_BOOK"] = "RESERVE";
$MESS["R_ZOOM_UP"] = "Enlarge";
$MESS["R_ZOOM_DOWN"] = "Reduce";

$MESS["DETAIL_ARTICLE_RESUME_EDIT"] = "Continue editing";
$MESS["DETAIL_ARTICLE_COMMENTS"] = "Comments";
$MESS["DETAIL_ARTICLE_RESUME_VK"] = "Vkontakte";
$MESS["DETAIL_ARTICLE_Move_the_slider_to_the_right"] = "Move the slider to the right.";
$MESS["DETAIL_ARTICLE_Done_You_can_now_send"] = "Done. You can now send";
?>