<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/templates/main/js/flowplayer.js"></script>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />
<script type="text/javascript">
    $(document).ready(function(){
        if ($(".special_scroll").length>0){
            $(".special_scroll .item").fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'cyclic'            :       true,
                'overlayColor'      :       '#1a1a1a',
                'speedIn'		:	200,
                'speedOut'		:	200,
                'showNavArrows'     : 'true'
            });
        }
//            else
//            {
        $(".articles_photo .images img").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200,
            'speedOut'		:	200,
            'showNavArrows'     : 'true'
        });
//            }
    });
</script>
<script>
    $(function(){
        $(".articles_photo").galery();
        $("#comments").html($("#comments_temp").find("script").remove().end().html());
        $("#comments_temp").remove();

        $(".articles_photo .images img").click(function(){
            if($(this).parents('.img').next('.special_scroll')){
                $(this).parents('.img').next('.special_scroll').find(".active").click();
            }
            else {
//                $(this).click();
            }
        });

        $(".articles_photo .images img").each(function(){
            $(this).attr("href", $(this).attr("src"));
        });

        $(".special_scroll").each(function(indx, element){
            $(element).find(".item").each(function(){
                $(this).attr("href", $(this).find("img").attr("src"));
                $(this).attr("rel","group"+indx);
            });
        });

    });
</script>
<script>
    $(document).ready(function(){
        $(".photos123").each(function(){
            var wid = $(this).width();
            $(this).append("<div class='block'><?=GetMessage("R_ZOOM_UP")?></div>");
            $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"});
        });
        $(".photos123").hover(function(){
                $(this).find(".block").show();
            },
            function(){
                $(this).find(".block").hide();
            });
        $(".photos123").toggle(function(){
                $(this).find("img:hidden").show().prev().hide();
                $(this).removeClass('left');
                $(this).find(".block").html("<?=GetMessage("R_ZOOM_DOWN")?>");
                $(this).css("width","728px");
                var wid = $(this).width();
                $(this).find(".block").css({"left":wid/2-50+"px"});
            },
            function(){
                $(this).find("img:hidden").show().next().hide();
                $(this).addClass('left');
                $(this).find(".block").html("<?=GetMessage("R_ZOOM_UP")?>");
                $(this).css("width","238px");
                var wid = $(this).width();
                $(this).find(".block").css({"left":wid/2-50+"px"});
            });
//    $(".photos123").each(function(){
//       var wid = $(this).width();
//       //var hei = $(this).height();
//       $(this).append("<div class='block'>Увеличить</div>");
//       $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
//       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
//    });
//    $(".photos123").hover(function(){
//        /*var wid = $(this).width();
//        var hei = $(this).height();
//        console.log(wid);
//        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"}); */
//        $(this).find(".block").show();
//    },
//    function(){
//        /*var wid = $(this).width();
//        var hei = $(this).height();
//        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"});*/
//        $(this).find(".block").hide();
//    });
//    $(".photos123 .block").toggle(function(){
//        $(this).parent().find("img:hidden").show().prev().hide();        
//        $(this).parent().removeClass('left');        
//        $(this).html("Уменьшить");
//        $(this).parent().css("width","728px");
//        var wid = $(this).parent().width();
//        //var hei = $(this).parent().height();
//        $(this).css({"left":wid/2-50+"px"});
//    },
//    function(){
//        $(this).parent().find("img:hidden").show().next().hide();
//        $(this).parent().addClass('left');        
//        $(this).html("Увеличить");
//        $(this).parent().css("width","238px");
//        var wid = $(this).parent().width();
//        //var hei = $(this).parent().height();
//        $(this).css({"left":wid/2-50+"px"});
//    });    





        //minus_plus_buts
        $("#minus_plus_buts").on("click","a.plus", function(event){
            var obj = $(this);
            $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
                if(parseInt(otvet))
                {
                    $(".all").html($(".all").html()*1+1);
                    obj.html(otvet);
                }
                else
                {
                    $('#minus_plus_buts .plus').tooltip({'title':otvet,'trigger':"manual",'placement':'left'});
                    $('#minus_plus_buts .plus').tooltip("show");
                    setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);
                }
            });
            return false;
        });

        $("#minus_plus_buts").on("click","a.minus", function(event){
            var obj = $(this);
            $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet)
            {
                if(parseInt(otvet)){
                    $(".all").html($(".all").html()*1+1);
                    obj.html(otvet);
                }else{
                    $('#minus_plus_buts .minus').tooltip({'title':otvet,'trigger':"manual",'placement':'right'});
                    $('#minus_plus_buts .minus').tooltip("show");
                    setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);
                }
            });
            return false;
        });

        $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
            $("#minus_plus_buts").html(otvet);
        });
    })
</script>


<?//=$arResult?><!-- в ресторане-->
<h1><?=$arResult["NAME"]?></h1>
<div class="clearfix"></div>
<div class="detail_article">
    <div class="date">
        <?if ($arResult["IBLOCK_TYPE_ID"]!="afisha"):?>
            <a href="/users/id<?=$arResult["CREATED_BY"]?>/"><?=$arResult["AUTHOR_NAME"]?></a>, <?=$arResult["CREATED_DATE_FORMATED_1"]?> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <?else:?>
            <a href="/users/id<?=$arResult["CREATED_BY"]?>/"><?=$arResult["AUTHOR_NAME"]?></a>,
            <?if ($arResult["PROPERTIES"]["EVENT_DATE"]):
                echo $arResult["PROPERTIES"]["EVENT_DATE"]["VALUE"];
                if ($arResult["PROPERTIES"]["TIME"]["VALUE"])
                    echo ", ".$arResult["PROPERTIES"]["TIME"]["VALUE"];
            endif;?>
        <?endif;?>
    </div>
    <?if($arResult["PREVIEW_TEXT"]):?>
        <br /><i><?=$arResult["PREVIEW_TEXT"]?></i>
    <?endif;?>
    <?=$arResult["DETAIL_TEXT"]?>
    <div class="clearfix"></div>
    <?if($arParams["EDIT_NOW"]=="Y"):?>
        <a href="/users/id<?=$arResult["CREATED_BY"]?>/blog/editor/blog.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
            <input type="button" class="light_button" value="<?=GetMessage("DETAIL_ARTICLE_RESUME_EDIT")?>">
        </a>
        <br /><Br />
    <?endif;?>

    <div class="clearfix"></div>
    <div class="article_likes">
        <div class="pull-left likes-buttons" id="minus_plus_buts">

        </div>
        <div class="pull-right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
        </div>
    </div>
<!--    <ul class="nav nav-tabs">-->
<!--        <li class="active"><a href="#comments" data-toggle="tab">--><?//=GetMessage("DETAIL_ARTICLE_COMMENTS")?><!--</a></li>-->
<!--        <li class=""><a href="#so3c" data-toggle="tab">--><?//=GetMessage("DETAIL_ARTICLE_RESUME_VK")?><!--</a></li>-->
<!--        <li class=""><a href="#so34c" data-toggle="tab">Facebook</a></li>-->
<!--    </ul>-->


    <div class="restoran-detail network-design" style="overflow: hidden">
        <div class="tab-str-wrapper">
            <ul class="nav nav-tabs" style="float: left;">
                <li class=""><a href="#so34c" data-toggle="tab">Facebook</a></li>
                <li class=""><a href="#so3c" data-toggle="tab"><?=GetMessage("DETAIL_ARTICLE_RESUME_VK")?></a><span>|</span></li>
                <li class="active"><a href="#comments" data-toggle="tab"><?=GetMessage("DETAIL_ARTICLE_COMMENTS")?></a><span>|</span></li>
            </ul>
            <div class="tabs-center-line"></div>
        </div>
    </div>

    <div class="tab-content" style="margin-top: 0">
        <div class="tab-pane active sm" id="comments" style="margin-top: -20px">
        </div>
        <div class="tab-pane sm" id="so3c">
            <!-- Put this script tag to the <head> of your page -->
            <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>

            <script type="text/javascript">
                VK.init({apiId: <?=(CITY_ID=="tmn")?"3559173":"2881483"?>, onlyWidgets: true});
            </script>

            <!-- Put this div tag to the place, where the Comments block will be -->
            <div id="vk_comments"></div>
            <script type="text/javascript">
                VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
            </script>
        </div>
        <div class="tab-pane sm" id="so34c">
            <div class="fb-comments" data-href="http://<?=SITE_SERVER_NAME?><?=$APPLICATION->GetCurPage()?>" data-numposts="20" data-colorscheme="light" data-width="728"></div>
        </div>
    </div>
</div>