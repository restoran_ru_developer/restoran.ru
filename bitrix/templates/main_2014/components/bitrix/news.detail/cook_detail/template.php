<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/templates/main/js/flowplayer.js"></script>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />
<script>
    $(function(){
        $(".articles_photo").galery();
        $("#comments").html($("#comments_temp").find("script").remove().end().html());
        $("#comments_temp").remove();
    });
</script>
<script>
    $(document).ready(function(){
        $(".photos123").each(function(){
            var wid = $(this).width();
            $(this).append("<div class='block'><?=GetMessage("R_ZOOM_UP")?></div>");
            $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"});
        });
        $(".photos123").hover(function(){
                $(this).find(".block").show();
            },
            function(){
                $(this).find(".block").hide();
            });
        $(".photos123").toggle(function(){
                $(this).find("img:hidden").show().prev().hide();
                $(this).removeClass('left');
                $(this).find(".block").html("<?=GetMessage("R_ZOOM_DOWN")?>");
                $(this).css("width","728px");
                var wid = $(this).width();
                $(this).find(".block").css({"left":wid/2-50+"px"});
            },
            function(){
                $(this).find("img:hidden").show().next().hide();
                $(this).addClass('left');
                $(this).find(".block").html("<?=GetMessage("R_ZOOM_UP")?>");
                $(this).css("width","238px");
                var wid = $(this).width();
                $(this).find(".block").css({"left":wid/2-50+"px"});
            });





        //minus_plus_buts
        $("#minus_plus_buts").on("click","a.plus", function(event){
            var obj = $(this);
            $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
                if(parseInt(otvet))
                {
                    $(".all").html($(".all").html()*1+1);
                    obj.html(otvet);
                }
                else
                {
                    $('#minus_plus_buts .plus').tooltip({'title':otvet,'trigger':"manual",'placement':'left'});
                    $('#minus_plus_buts .plus').tooltip("show");
                    setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);
                }
            });
            return false;
        });

        $("#minus_plus_buts").on("click","a.minus", function(event){
            var obj = $(this);
            $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet)
            {
                if(parseInt(otvet)){
                    $(".all").html($(".all").html()*1+1);
                    obj.html(otvet);
                }else{
                    $('#minus_plus_buts .minus').tooltip({'title':otvet,'trigger':"manual",'placement':'right'});
                    $('#minus_plus_buts .minus').tooltip("show");
                    setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);
                }
            });
            return false;
        });

        $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
            $("#minus_plus_buts").html(otvet);
        });
    })
</script>
<h1><?=$arResult["NAME"]?></h1>
<div class="clearfix"></div>
<div class="detail_article hrecipe" itemscope itemtype="http://schema.org/Recipe">
    <div class="date">
        <?if (SITE_ID!="s2"):?>
            <a itemprop="author" href="/users/id<?=$arResult['CREATED_BY']?>/"><?=$arResult["AUTHOR_NAME"]?></a>
        <?endif;?>
    </div>
    <?if($arResult["PREVIEW_TEXT"]):?>
        <br /><i itemprop="description"><?=$arResult["PREVIEW_TEXT"]?></i>
    <?endif;?>
    <div class="clearfix"></div><br />
    <div class="left recept_prop_block1" alt="blue-a">
        <?=GetMessage("R_COOKING_TIME")?> — <span itemprop="cookTime" class="duration"><?=$arResult["DISPLAY_PROPERTIES"]["prig_time"]["DISPLAY_VALUE"]?></span><br />
        <?=GetMessage("R_COMPLICATION")?> — <span><?=$arResult["DISPLAY_PROPERTIES"]["slognost"]["DISPLAY_VALUE"]?></span><br />
        <?
        $params = array();
        $params["id"] = $arResult["ID"];
        $params["section"] = 0;
        $params = addslashes(json_encode($params));
        ?>
        <div class="recept_book">
            <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="" data-toggle="favorite" data-restoran="<?=$arResult["ID"]?>"><?=GetMessage("R_ADD2FAVORITES")?></a>
        </div>
        <div class="print">
            <a href="?print=Y" target="_blank"><?=GetMessage("R_PRINT")?></a>
        </div>
    </div>
    <div class="right recept_prop_block2">
        <div class="left" style="margin-right:20px; width:180px">
            <?if ($arResult["DISPLAY_PROPERTIES"]["osn_ingr"]["DISPLAY_VALUE"]):?>
                <?=GetMessage("R_MAIN_INGRIDIENT")?>: <span><?=$arResult["DISPLAY_PROPERTIES"]["osn_ingr"]["DISPLAY_VALUE"]?></span><br />
            <?endif;?>
            <?if ($arResult["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]):?>
                <?=GetMessage("R_CATEGORY")?>: <span itemprop="recipeCategory" class="category"><?=$arResult["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]?></span><Br />
            <?endif;?>
            <?if ($arResult["DISPLAY_PROPERTIES"]["cook"]["DISPLAY_VALUE"]):?>
                <?=GetMessage("R_KITCHEN")?>: <span class="cuisine-type"><?=$arResult["DISPLAY_PROPERTIES"]["cook"]["DISPLAY_VALUE"]?></span>
            <?endif;?>
        </div>
        <div class="left" style="width:170px">
            <?if ($arResult["DISPLAY_PROPERTIES"]["povod"]["DISPLAY_VALUE"]):?>
                <?=GetMessage("R_POVOD")?>: <span><?=$arResult["DISPLAY_PROPERTIES"]["povod"]["DISPLAY_VALUE"]?></span><Br />
            <?endif;?>
            <!--Предпочтения: <span><?=$arResult["DISPLAY_PROPERTIES"]["predp"]["DISPLAY_VALUE"]?></span><br />-->
            <?if ($arResult["DISPLAY_PROPERTIES"]["prig"]["DISPLAY_VALUE"]):?>
                <?=GetMessage("R_PREPARATION")?>: <span><?=$arResult["DISPLAY_PROPERTIES"]["prig"]["DISPLAY_VALUE"]?></span>
            <?endif;?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <br />
    <div style="position:relative">
        <?if ($arResult["PROPERTIES"]["porc"]["VALUE"]):?>
            <div itemprop="recipeYield" class="yield portion">
                <div><?=$arResult["PROPERTIES"]["porc"]["VALUE"]?></div>
                <?=pluralForm($arResult["PROPERTIES"]["porc"]["VALUE"],GetMessage("R_PORTION1"),GetMessage("R_PORTION2"),GetMessage("R_PORTION3"))?>
            </div>
        <?endif;?>
        <?if ($arResult["DETAIL_PICTURE"]["width"]>=728):?>
            <img itemprop="image" class="photo" src="<?=$arResult["DETAIL_PICTURE"]["src"]?>" width="728" />
        <?else:?>
            <img itemprop="image" class="photo" src="<?=$arResult["DETAIL_PICTURE"]["src"]?>" />
        <?endif;?>
    </div>
    <br />
    <?=$arResult["DETAIL_TEXT"]?>
    <div class="clearfix"></div>
    <br />
    <div class="bon_appetite"><?=GetMessage("R_BON_APPETI")?></div>
    <?if(LANGUAGE_ID!="en"):?>
        <?
        $arrTags = explode(',', $arResult["TAGS"]);
        $count = count($arrTags);
        $i = 0;
        $t="";
        foreach($arrTags as $value):
            $i++;
            $value = trim($value);
            $t .= '<a class="another" href="/search/?tags='.str_replace(' ', '+', $value).'&search_in=recepts">'.$value.'</a>';
            if($i != $count) $t .= ', ';
        endforeach;
        ?>
        <br />
        <p><?=GetMessage("TAGS_TITLE")?>: <?=$t?></p>
    <?endif;?>
    <?if($arParams["EDIT_NOW"]=="Y"):?>
        <a href="/users/id<?=$arResult["CREATED_BY"]?>/blog/editor/blog.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
            <input type="button" class="light_button" value="Продолжить редактирование">
        </a>
        <br /><Br />
    <?endif;?>

    <div class="clearfix"></div>
    <div class="article_likes">
        <div class="pull-left likes-buttons" id="minus_plus_buts">

        </div>
        <div class="pull-right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
        </div>
    </div>
<!--    <ul class="nav nav-tabs">-->
<!--        <li class="active"><a href="#comments" data-toggle="tab">--><?//=GetMessage('feedback_message_title')?><!--</a></li>-->
<!--        <li class=""><a href="#so3c" data-toggle="tab">--><?//=GetMessage('feedback_vk_title')?><!--</a></li>-->
<!--        <li class=""><a href="#so34c" data-toggle="tab">Facebook</a></li>-->
<!--    </ul>-->

    <div class="restoran-detail network-design" style="overflow: hidden">
        <div class="tab-str-wrapper">
            <ul class="nav nav-tabs" style="float: left;">
                <li class=""><a href="#so34c" data-toggle="tab">Facebook</a></li>
                <li class=""><a href="#so3c" data-toggle="tab"><?=GetMessage("feedback_vk_title")?></a><span>|</span></li>
                <li class="active"><a href="#comments" data-toggle="tab"><?=GetMessage("feedback_message_title")?></a><span>|</span></li>
            </ul>
            <div class="tabs-center-line"></div>
        </div>
    </div>


    <div class="tab-content" style="margin-top: 0">
        <div class="tab-pane active sm" id="comments" style="margin-top: -20px">
        </div>
        <div class="tab-pane sm" id="so3c">
            <!-- Put this script tag to the <head> of your page -->
            <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>

            <script type="text/javascript">
                VK.init({apiId: <?=(CITY_ID=="tmn")?"3559173":"2881483"?>, onlyWidgets: true});
            </script>

            <!-- Put this div tag to the place, where the Comments block will be -->
            <div id="vk_comments"></div>
            <script type="text/javascript">
                VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
            </script>
        </div>
        <div class="tab-pane sm" id="so34c">
            <div class="fb-comments" data-href="http://<?=SITE_SERVER_NAME?><?=$APPLICATION->GetCurPage()?>" data-numposts="20" data-colorscheme="light" data-width="728"></div>
        </div>
    </div>
</div>