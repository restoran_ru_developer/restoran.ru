<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
global $links;
global $props;
$links["PREV"] = $arResult["PREV_ARTICLE"];
$links["NEXT"] = $arResult["NEXT_ARTICLE"];
$props["osn_ingr"] = $arResult["OSN_INGR"];
$props["cat"] = $arResult["CAT"];
//v_dump($props["cat"]);
$props["similar"] = $arResult["SIMILAR"];
// set title
$APPLICATION->SetTitle($arResult["SECTION"]["PATH"][0]["NAME"]);

$title = explode(" ",$arResult["NAME"]);
$keywords = array();
foreach ($title as $t)
{
    if (strlen($t)>3)
    {
        $t=trim($t);
        $t = str_replace(Array("«","!","-","_","@","#","$","%","^","*","(",")","»","'",'"'), "", $t);
        $keywords[] = $t;
    }
}         
$keywords[] = $arResult["IBLOCK_TYPE_NAME"];
$title = str_replace(Array("«","!","-","_","@","#","$","%","^","*","(",")","»","'",'"'), "", $arResult["NAME"]);
//if ($USER->IsAdmin())
{
    if(!$APPLICATION->GetPageProperty("keywords"))
    {
        $APPLICATION->SetPageProperty("keywords",  implode(", ",$keywords));
    }                
    if(!$APPLICATION->GetPageProperty("description"))
    {
        $APPLICATION->SetPageProperty("description",  $arResult["IBLOCK_TYPE_NAME"].": ".$title);
    }
}
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);            

$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
<script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
<!--<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />-->

<script type="text/javascript">
        $(document).ready(function(){
                $('.QapTcha').QapTcha({
                        txtLock : 'Сдвиньте слайдер вправо.',
                        txtUnlock : 'Готово. Теперь можно отправлять',
                        disabledSubmit : true,
                        autoRevert : true,
                        PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
                        autoSubmit : false});                    
        });
</script>
<div id="comments_temp">   
    <?
    $APPLICATION->IncludeComponent(
        "restoran:comments",
        "review_comment_new",
        Array(
            "IBLOCK_TYPE" => "comment",
            "ELEMENT_ID" => $arResult["ID"],
            "IBLOCK_ID" => (SITE_ID=="s1")?"2438":"2641",
            "IS_SECTION" => "N",
            "ADD_COMMENT_TEMPLATE" => "",
            "COUNT" => 999,
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000008",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "PROPERTY_CODE" => Array("photos","video")
        ),
        false
    );
    $APPLICATION->IncludeComponent(
                    "restoran:comments_add_new",
                    "new",
                    Array(
                            "IBLOCK_TYPE" => "comment",
                            "IBLOCK_ID" => (SITE_ID=="s1")?"2438":"2641",
                            "ELEMENT_ID" => $arResult["ID"],
                            "IS_SECTION" => "N",                                   
                    ),false
        );
//        echo "<br />";

    ?>
    <Br />
</div>