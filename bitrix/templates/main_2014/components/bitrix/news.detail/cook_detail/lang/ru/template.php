<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["NEXT_POST"] = "Предыдущий пост";
$MESS["PREV_POST"] = "Следующий пост";
$MESS["CUISINE"] = "Кухня";
$MESS["AVERAGE_BILL"] = "Средний счет";
$MESS["TAGS_TITLE"] = "Теги";
$MESS["R_COMMENTS"] = "Комментарии";
$MESS["SUBSCRIBE"] = "подписаться";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_ADD2FAVORITES"] = "В книгу рецептов";
$MESS["R_PRINT"] = "Распечатать";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_VK"] = "Вконтакте";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["R_BON_APPETI"] = "Приятного аппетита!";
$MESS["R_PORTION1"] = "порция";
$MESS["R_PORTION2"] = "порции";
$MESS["R_PORTION3"] = "порций";
$MESS["R_ADD_RECEPT"] = "Добавить рецепт";
$MESS["R_SIMILAR"] = "Похожие рецепты";

$MESS["R_COOKING_TIME"] = "Время приготовления";
$MESS["R_COMPLICATION"] = "Сложность";
$MESS["R_MAIN_INGRIDIENT"] = "Основной ингредиент";
$MESS["R_CATEGORY"] = "Категория";
$MESS["R_KITCHEN"] = "Кухня";
$MESS["R_POVOD"] = "Повод";
$MESS["R_PREPARATION"] = "Приготовление";

$MESS["R_ZOOM_UP"] = "Увеличить";
$MESS["R_ZOOM_DOWN"] = "Уменьшить";

$MESS["feedback_message_title"] = "Комментарии";
$MESS["feedback_vk_title"] = "Вконтакте";
?>