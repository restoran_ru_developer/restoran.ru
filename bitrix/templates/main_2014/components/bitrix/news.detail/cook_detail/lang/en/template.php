<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["NEXT_POST"] = "Previous recipe";
$MESS["PREV_POST"] = "Next recipe";
$MESS["CUISINE"] = "Kitchen";
$MESS["AVERAGE_BILL"] = "Average bill";
$MESS["TAGS_TITLE"] = "Tags";
$MESS["R_COMMENTS"] = "Comments";
$MESS["SUBSCRIBE"] = "subscribe";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_ADD2FAVORITES"] = "In recipe book";
$MESS["R_PRINT"] = "Print recipe";
$MESS["MONTHS_FULL"] = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря";
$MESS["MONTHS_SHORT"] = "янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек";
$MESS["WEEK_DAYS_FULL"] = "восресенье,понедельник,вторник,среда,четверг,пятница,суббота";
$MESS["WEEK_DAYS_SHORT"] = "вс,пн,вт,ср,чт,пт,сб";
$MESS["R_VK"] = "VK";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["R_BON_APPETI"] = "Bon appetite!";
$MESS["R_PORTION1"] = "portion";
$MESS["R_PORTION2"] = "portions";
$MESS["R_PORTION3"] = "portions";
$MESS["R_ADD_RECEPT"] = "Add a recipe";
$MESS["R_SIMILAR"] = "Similar recipes";

$MESS["R_COOKING_TIME"] = "Cooking time";
$MESS["R_COMPLICATION"] = "Complication";
$MESS["R_MAIN_INGRIDIENT"] = "Main ingridient";
$MESS["R_CATEGORY"] = "Category";
$MESS["R_KITCHEN"] = "Kitchen";
$MESS["R_POVOD"] = "Cause";
$MESS["R_PREPARATION"] = "Preparation";

$MESS["R_ZOOM_UP"] = "Zoom In";
$MESS["R_ZOOM_DOWN"] = "Zoom Out";

$MESS["feedback_message_title"] = "Comments";
$MESS["feedback_vk_title"] = "Vkontakte";
?>