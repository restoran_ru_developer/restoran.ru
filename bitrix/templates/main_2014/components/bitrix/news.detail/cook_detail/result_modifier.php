<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $DB;
if($arIBType = CIBlockType::GetByIDLang($arResult["IBLOCK_TYPE_ID"], LANGUAGE_ID))   
    $arResult["IBLOCK_TYPE_NAME"] = htmlspecialcharsex($arIBType["NAME"]);
//if ($USER->IsAdmin())
//    v_dump($arResult);
// get rest iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
// get author name
$rsUser = CUser::GetByID($arResult["CREATED_BY"]);
if($arUser = $rsUser->GetNext())
{
    if ($arUser["PERSONAL_PROFESSION"])
        $arResult["AUTHOR_NAME"] = $arUser["PERSONAL_PROFESSION"];
    else
        $arResult["AUTHOR_NAME"] = ($arUser["NAME"])?$arUser["NAME"]:$arUser["LAST_NAME"];
}
// format date

if (!$arResult["DISPLAY_ACTIVE_FROM"]):
    $date = $DB->FormatDate($arResult["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
else:
    $arTmpDate = $arResult["DISPLAY_ACTIVE_FROM"];
endif;
$arTmpDate = explode(" ", $arTmpDate);
$arResult["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
$arResult["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

function get_file($matches)
{
    //return "http://".SITE_SERVER_NAME."".CFile::GetPath($matches[1]);
    $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'real',
        'type'=>'image',
        'alpha_level'=>'30',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
    );
    $photo = array();
    $photo = CFile::ResizeImageGet($matches[1],Array(),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
    //return CFile::GetPath($matches[1]);
    return $photo["src"];
}
function get_file_av($matches)
{
    $photo = array();
    $photo = CFile::ResizeImageGet($matches[1],Array("width"=>150,"height"=>150),BX_RESIZE_IMAGE_EXACT,true,Array());
    return $photo["src"];
}
function get_rest($matches)
{
    $rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            //"ACTIVE" => "Y",
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arRestIB["ID"],
            "ID" => $matches[1],
        ),
        false,
        false,
        Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL","PROPERTY_RATIO")
    );
    if($arRest = $rsRest->GetNext()) {
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"kitchen"));
        while($ar_props = $db_props->GetNext())
        {
            $kitchen["NAME"] = $ar_props["NAME"];
            $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
            $arCuisine = $rsCuisine->GetNext();
            $kitchen["VALUE"][] = $arCuisine["NAME"];
        }
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"average_bill"));
        if($ar_props = $db_props->GetNext())
        {
            $avb["NAME"] = $ar_props["NAME"];
            $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
            $arCuisine = $rsCuisine->GetNext();
            $avb["VALUE"][] = $arCuisine["NAME"];
        }
        //"PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill"
        //v_dump($arRest);
        // get rest city name
        $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
        $arRestCity = $rsRestCity->GetNext();

        // get rest picture
        if ($arRest["DETAIL_PICTURE"])
             $restPic = CFile::ResizeImageGet($arRest["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        else
            $restPic["src"] = "/tpl/images/noname/rest_nnm.png"; 
        // get cuisine name
        $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
        $arCuisine = $rsCuisine->GetNext();

        // set rest info
        $arTmpRest["NAME"] = $arRest["NAME"];
        $arTmpRest["CITY"] = $arResult["IBLOCK"]["NAME"];
        $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
        $arTmpRest["REST_PICTURE"] = $restPic;
        $arTmpRest["URL"] = $arRest["DETAIL_PAGE_URL"];
        $arTmpRest["CUISINE"] = $kitchen;
        $arTmpRest["AVERAGE_BILL"] = $avb;
        $res = '<a href="'.$arTmpRest["URL"].'"><img class="indent" src="'.$arTmpRest["REST_PICTURE"]["src"].'" width="232" /></a><br />
                <a class="font14" href="'.$arTmpRest["URL"].'">'.$arTmpRest["NAME"].'</a>,<br />'.$arTmpRest["CITY"].'
                <p><div class="rating">';
        for($i = 0; $i < 5; $i++):
            if ($i < $arTmpRest["RATIO"]):
                $res .= '<div class="small_star_a" alt="'.$i.'"></div>';               
            else:
                $res .= '<div class="small_star" alt="'.$i.'"></div>';
            endif;
        endfor;
        $res .= '<div class="clear"></div></div>';
        $res .= '<b>'.$arTmpRest["CUISINE"]["NAME"].': </b>';
        $res .= implode(", ",$arTmpRest["CUISINE"]["VALUE"]);
        $res .= '<br /><b>'.$arTmpRest["AVERAGE_BILL"]["NAME"].':</b>';
        $res .= implode(", ",$arTmpRest["AVERAGE_BILL"]["VALUE"]);
        $res .= '<br /></p>';
        if(end($block["REST_BIND_ARRAY"]) != $rest):
            $res .= '<hr class="dotted" />';
        endif;
        $resto[] = Array("ID"=>$arRest["ID"],"NAME"=>$arRest["NAME"]);
    }
    return $res;
}

$arResult["DETAIL_TEXT"] = preg_replace_callback("/#FID_([0-9]+)#/","get_file",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = preg_replace_callback("/#FID_PR([0-9]+)#/","get_file_av",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = str_replace("#FID_#","/tpl/images/noname/rest_nnm.png",$arResult["DETAIL_TEXT"]);
$arResult["DETAIL_TEXT"] = preg_replace_callback("/#REST_([0-9]+)#/","get_rest",$arResult["DETAIL_TEXT"]);
if (SITE_ID=="s2")
    $arResult["DETAIL_TEXT"] = str_replace("Шаг","Step",$arResult["DETAIL_TEXT"]);
 $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'real',
        'type'=>'image',
        'alpha_level'=>'30',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
    );
$arResult["DETAIL_PICTURE"] = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width' => 728, 'height' => 485), BX_RESIZE_IMAGE_PROP, true, $watermark);
if (!$arResult["PROPERTIES"]["COMMENTS"]["VALUE"])
    $arResult["PROPERTIES"]["COMMENTS"]["VALUE"] = 0;
//$arResult["DETAIL_TEXT"] = preg_replace("|(#FID_[0-9]+#)|","",$arResult["DETAIL_TEXT"]);

// get rest info
foreach($arResult["ITEMS"] as $key=>$block) {
    if(is_array($block["PROPERTIES"]["REST_BIND"]["VALUE"])) {
        foreach($block["PROPERTIES"]["REST_BIND"]["VALUE"] as $restBind) {
            $rsRest = CIBlockElement::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "ID" => $restBind,
                ),
                false,
                false,
                Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_photos", "PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill")
            );
            while($arRest = $rsRest->GetNext()) {
                // get rest city name
                $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
                $arRestCity = $rsRestCity->Fetch();

                // get rest picture
                if(!$arRest["PREVIEW_PICTURE"]) {
                    $restPic = CFile::ResizeImageGet($arRest["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
                } else {
                    $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
                }

                // get cuisine name
                $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
                $arCuisine = $rsCuisine->Fetch();

                // set rest info
                $arTmpRest["NAME"] = $arRest["NAME"];
                $arTmpRest["CITY"] = $arRestCity["NAME"];
                $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
                $arTmpRest["REST_PICTURE"] = $restPic;
                if(!in_array($arCuisine["NAME"], $arTmpRest["CUISINE"]))
                    $arTmpRest["CUISINE"][] = $arCuisine["NAME"];
                $arTmpRest["AVERAGE_BILL"] = $arRest["PROPERTY_AVERAGE_BILL_VALUE"];

                $arResult["ITEMS"][$key]["REST_BIND_ARRAY"][$arRest["ID"]] = $arTmpRest;
            }
            $arTmpRest["CUISINE"] = Array();
        }
    }

    // get respondent data
    if($block["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"] && $block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]) {
        $arResult["ITEMS"][$key]["RESPONDENT"]["PHOTO"] = CFile::ResizeImageGet($block["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"], array('width' => 168, 'height' => 168), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["RESPONDENT"]["NAME"] = $block["PROPERTIES"]["RESPONDENT_NAME"]["VALUE"];
        $arResult["ITEMS"][$key]["RESPONDENT"]["POST"] = $block["PROPERTIES"]["RESPONDENT_POST"]["VALUE"];
        if($block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"])
            $arResult["ITEMS"][$key]["RESPONDENT"]["SPEECH"] = $block["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"];
    }

    // get photos
    foreach($block["PROPERTIES"]["PICTURES"]["VALUE"] as $picKey=>$pic) {
        $photoPic = CFile::ResizeImageGet($pic, array('width' => 394, 'height' => 271), BX_RESIZE_IMAGE_EXACT, true, Array());
        $photoPic["description"] = $block["PROPERTIES"]["PICTURES"]["DESCRIPTION"][$picKey];
        $arResult["ITEMS"][$key]["PICTURES"][] = $photoPic;
        // cnt pictures
        $arResult["ITEMS"][$key]["CNT_PICTURES"] = count($arResult["ITEMS"][$key]["PICTURES"]);
    }
    if ($block["PROPERTIES"]["VIDEO"]["VALUE"])
    {
        $arResult["ITEMS"][$key]["VIDEO"] = $block["PROPERTIES"]["VIDEO"]["VALUE"];
    }
}

// get current date
$arResult["TODAY_DATE"] = CIBlockFormatProperties::DateFormat('j F', MakeTimeStamp(date('d.m.Y'), CSite::GetDateFormat()));

$arResult["NEXT_ARTICLE"] = RestIBlock::GetArticleNext($arResult["IBLOCK_ID"],$arResult["ID"]);
$arResult["PREV_ARTICLE"] = RestIBlock::GetArticlePrev($arResult["IBLOCK_ID"],$arResult["ID"]);

foreach ($arResult["DISPLAY_PROPERTIES"] as &$properties)
{        
    if (LANGUAGE_ID=="en")
    {
        if (is_array($properties["DISPLAY_VALUE"]))
        {
            if (is_array($properties["VALUE"]))
            {
                foreach($properties["VALUE"] as $key=>$val)
                {
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {                    
                        $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);                        
                    }            
                }
            }
            else
            {
                $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                if ($ar = $r->Fetch())
                {                
                    $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                } 
            }
        }
        else
        {
            $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
            if ($ar = $r->Fetch())
            {   
                $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                
            }   
        }
    }
    if (is_array($properties["DISPLAY_VALUE"]))
       $properties["DISPLAY_VALUE"] = implode(", ",$properties["DISPLAY_VALUE"]);
    
    //$properties["DISPLAY_VALUE"] = str_replace("content",CITY_ID,$properties["DISPLAY_VALUE"]);
    //else
      //  $properties["DISPLAY_VALUE"] = strip_tags($properties["DISPLAY_VALUE"]);
}
if ($_REQUEST["SECTION_CODE"])
    $arResult["SECTION_PAGE_URL"] = $arResult["LIST_PAGE_URL"].$_REQUEST["SECTION_CODE"]."/";
else
    $arResult["SECTION_PAGE_URL"] = $arResult["LIST_PAGE_URL"];
//$arResult["SECTION_PAGE_URL"] = str_replace("content",CITY_ID,$arResult["SECTION_PAGE_URL"]);


global $APPLICATION;
$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult['IMAGE'] = $arResult["DETAIL_PICTURE"]["src"];
	$cp->arResult['TEXT'] = strip_tags($arResult["PREVIEW_TEXT"]);
	$cp->arResult['IBLOCK_TYPE_NAME'] = strip_tags($arResult["IBLOCK_TYPE_NAME"]);
	$cp->arResult['OSN_INGR'] = $arResult["PROPERTIES"]["osn_ingr"]["VALUE"];
	$cp->arResult['CAT'] = $arResult["PROPERTIES"]["cat"]["VALUE"];
	$cp->arResult['SIMILAR'] = $arResult["PROPERTIES"]["similar_recepts"]["VALUE"];
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
        $cp->SetResultCacheKeys(array('IMAGE','TEXT','PREV_ARTICLE','NEXT_ARTICLE',"IBLOCK_TYPE_NAME","OSN_INGR","CAT","SIMILAR"));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
	if (!isset($arResult['IMAGE']))
	{
		$arResult['IMAGE'] = $cp->arResult['IMAGE'];
		$arResult['TEXT'] = $cp->arResult['TEXT'];
	}        
        if (!isset($arResult['PREV_ARTICLE']))
	{
            $arResult['PREV_ARTICLE'] = $cp->arResult['PREV_ARTICLE'];
        }
        if (!isset($arResult['NEXT_ARTICLE']))
	{
            $arResult['NEXT_ARTICLE'] = $cp->arResult['NEXT_ARTICLE'];
        }
        if (!isset($arResult['IBLOCK_TYPE_NAME']))
	{
            $arResult['IBLOCK_TYPE_NAME'] = $cp->arResult['IBLOCK_TYPE_NAME'];
        }
        if (!isset($arResult['OSN_INGR']))
	{
            $arResult['OSN_INGR'] = $cp->arResult['OSN_INGR'];
        }
        if (!isset($arResult['CAT']))
	{
            $arResult['CAT'] = $cp->arResult['CAT'];
        }
        if (!isset($arResult['SIMILAR']))
	{
            $arResult['SIMILAR'] = $cp->arResult['SIMILAR'];
        }
}
?>