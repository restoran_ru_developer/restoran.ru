 <?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

//echo "<pre>"; print_r($arResult);echo "</pre>";sUrlPath - "/spb/opinions/"

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1","PAGEN_2","PAGEN_3",'AJAX_REQUEST');
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");
?>
<?//if($USER->IsAdmin()):
 if($arResult['NavPageCount']>1&&$arResult['NavPageNomer']!=$arResult['NavPageCount']&&(preg_match('/\/opinions\//',$arResult['sUrlPath'])||(preg_match('/\/cookery\/|\/news\/|\/photos\/|\/blogs\//',$arResult['sUrlPath'])))):?>
     <div class="js-restaurant-list-more-wrap">
         <a class="js-restaurant-list-more for-<?=preg_match('/\/cookery\/|\/news\/|\/photos\/|\/blogs\//',$arResult['sUrlPath'])?'news':'opinion'?>-list" nav-page-num="<?=$arResult['NavPageNomer']?>" nav-page-count="<?=$arResult['NavPageCount']?>" >Показать больше</a>
     </div>
 <?endif;?>
<?//endif?>
<?if ($arResult["NavPageNomer"] > 1):?>
    <a href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]-1).$addNavParams,$excUrlParams)?>" class="prev icon-arrow-left"></a>        
<?else:?>
    <a href="javascript:void(0)" class="prev glyphicon icon-arrow-left"></a>        
<?endif?>
<div class="pages">
<?if($arResult["bDescPageNumbering"] === false):?>

	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <span class="current">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <a href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
		<?else:?>
            <a href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

<?endif?>
</div>
<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
    <a href="<?=$APPLICATION->GetCurPageParam("PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]+1).$addNavParams,$excUrlParams)?>" class="next icon-arrow-right"></a>
<?else:?>
    <a href="javascript:void(0)" class="next icon-arrow-right"></a>
<?endif?>