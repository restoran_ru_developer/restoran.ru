<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];
if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1", "pageRestCnt","index_php?page","index_php",'AJAX_REQUEST');
$addNavParams = ($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "");
$addNavParams .= ($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "");

?>
<?//if($USER->IsAdmin()):
//    $distance_dir = $_REQUEST["PROPERTY"]=='all'&&$_REQUEST["PROPERTY_VALUE"]=='distance'?true:false;
    ?>
    <?if(LANGUAGE_ID!='en'):?>
        <div class="js-restaurant-list-more-wrap">
            <?if($arResult['NavPageCount']>1&&$arResult['NavPageNomer']!=$arResult['NavPageCount']/*&&!$distance_dir*/):?>
                <a class="js-restaurant-list-more" nav-page-num="<?=!$_REQUEST["page"] || strlen($_REQUEST["page"]) <= 0?0:$arResult['NavPageNomer']?>" nav-page-count="<?=$arResult['NavPageCount']?>">Показать больше</a>
            <?endif?>
            <?if($_REQUEST['CATALOG_ID']!='banket'&&!$_REQUEST['NEAR']&&$_REQUEST['PROPERTY']!='STREET'):?>
                <?$some['arrFilter_pf'] = $_REQUEST['arrFilter_pf'];
                unset($some['arrFilter_pf']['sleeping_rest']);
                ?>
                <a href="/<?=CITY_ID?>/map/near/<?=$some['arrFilter_pf']?'?':''?><?=http_build_query($some,'flags_')?>" class="on-map-square-filter-button">Показать на карте</a>
            <?endif?>
        </div>
    <?endif?>
<?//endif?>
<?
$add_url_for_en = '';
if($APPLICATION->GetCurDir()=='/'&&LANGUAGE_ID=='en'){
    $add_url_for_en = '/'.CITY_ID;
}
?>
<?if ($arResult["NavPageNomer"] > 1):?>
    <a href="<?=$add_url_for_en.$APPLICATION->GetCurPageParam("page=".($arResult["NavPageNomer"]-1).$addNavParams,$excUrlParams)?>" class="prev icon-arrow-left"></a>
<?else:?>
    <a href="javascript:void(0)" class="prev icon-arrow-left"></a>
<?endif?>
    <div class="pages">
        <?
        $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
        $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
        if($arResult["bDescPageNumbering"] === false) {
            //v_dump($_REQUEST);
            // to show always first and last pages
            $arResult["nStartPage"] = 1;
            $arResult["nEndPage"] = $arResult["NavPageCount"];

            $sPrevHref = '';
            if ($arResult["NavPageNomer"] > 1)
            {
                $bPrevDisabled = false;

                if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
                {
                    $sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
                }
                else
                {
                    $sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
                }
            }
            else
            {
                $bPrevDisabled = true;
            }

            $sNextHref = '';
            if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
            {
                $bNextDisabled = false;
                $sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
            }
            else
            {
                $bNextDisabled = true;
            }
            ?>
            <?if ((!$_REQUEST["letter"]&&SITE_LANGUAGE_ID!='en'&&!$_REQUEST['PROPERTY']&&!$_REQUEST['NEAR'])||($_REQUEST['PROPERTY']&&$_REQUEST['bFirst20SectionId']&&!$_REQUEST['NO_BEST']))://$ITS_FIRST = true;?>
                <?if(!$_REQUEST["page"] || strlen($_REQUEST["page"]) <= 0):?>
                    <span class="current">&nbsp;<?=GetMessage("TOP_SPEC")?>&nbsp;</span>
                <?else:?>
                    <a href="<?=$arResult["sUrlPath"]?>"><?=GetMessage("TOP_SPEC")?></a>
                <?endif?>
            <?endif;?>
            <?
            $bFirst = true;
            $bPoints = false;
            do
            {
                if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
                {

                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"] && (strlen($_REQUEST["page"]) > 0 || $_REQUEST["letter"])):?>
                        <span class="current">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
                    <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false && strlen($_REQUEST["page"]) > 0):?>
                        <a href="<?=$add_url_for_en.$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
                    <?else:?>
                        <a href="<?=$add_url_for_en.$APPLICATION->GetCurPageParam("page=".$arResult["nStartPage"].$addNavParams, $excUrlParams)?>"><?=$arResult["nStartPage"]?></a>
                    <?endif;
                    $bFirst = false;
                    $bPoints = true;
                }
                else
                {
                    if ($bPoints)
                    {
                        ?>...<?
                        $bPoints = false;
                    }
                }
                $arResult["nStartPage"]++;
            } while($arResult["nStartPage"] <= $arResult["nEndPage"]);
        }
        ?>
    </div>
<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
    <a href="<?=$add_url_for_en.$APPLICATION->GetCurPageParam("page=".($arResult["NavPageNomer"]+1).$addNavParams,$excUrlParams)?>" class="next icon-arrow-right"></a>
<?else:?>
    <a href="javascript:void(0)" class="next icon-arrow-right"></a>
<?endif?>