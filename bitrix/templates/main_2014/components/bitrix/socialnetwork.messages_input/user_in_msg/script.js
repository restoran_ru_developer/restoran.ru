// show full message
function showAllMsg(id) {
    if(!id)
        return false;

    $('#short_text_' + id).fadeOut('normal', function() {
        $('#full_text_' + id).fadeIn('normal');
    });
    $('#show_all_link_' + id).hide();
}

// show answer form
function answerMsg(id) {
    if(!id)
        return false;

    $('#msg_form_' + id + ' > textarea').val('');
    $('#msg_form_' + id).fadeIn();
    $('#answ_link_' + id).hide();
}

// send message
function sendMsg(curUserID, secUserID, eventID) {
    var actionURL = $('#msg_form_' + eventID).attr('action');
    $.ajax({
        type: "post",
        data: $('#msg_form_' + eventID).serialize(),
        url: actionURL,
        cache: false,
        dataType: 'html',
        success: function(data) {
            //$('#msg_form_' + eventID).hide();
            //alert(data.RESULT);           
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
            }
            $('#mail_modal').html(data);
            showOverflow();
            setCenter($("#mail_modal"));
            $("#mail_modal").fadeIn("300"); 
            setTimeout("location.reload()",1500);
        }
    });
}

// get correspondence
function getCorrespondence(secUserID, eventID) {
    $.ajax({
        type: "post",
        data: "secUserID="+secUserID+"&eventID="+eventID,
        url: "/bitrix/templates/main/components/bitrix/socialnetwork.messages_input/user_in_msg/get_correspondence.php",
        dataType: 'html',
        success: function(data) {
            $('#correspondence_result_' + eventID).after(data);
        }
    });
}