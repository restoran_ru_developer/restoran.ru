<?
$MESS["USER_RESTORATOR"] = "restorator";
$MESS["DELETE_LINK"] = "Remove";
$MESS["SHOW_ALL"] = "Fully";
$MESS["HIDE_ALL"] = "Turn";
$MESS["ANSWER"] = "Reply";
$MESS["DATE_FORMAT_V"] = "in";
$MESS["SEND_MSG"] = "Reply";
$MESS["DELETE_CONFIRM"] = "Delete a post?";
$MESS["GET_CORRESPONDENCE"] = "View conversation";
$MESS["SONET_C27_T_DO_READ"] = "Mark as read";
$MESS["SONET_C27_T_DO_DELETE"] = "Delete";
$MESS["SONET_C27_T_SELECT_ALL"] = "Check All / Uncheck All";
$MESS["SONET_C27_T_SENDER"] = "Sender";
$MESS["SONET_C27_T_MESSAGE"] = "Message";
$MESS["SONET_C27_T_ACTIONS"] = "Actions";
$MESS["SONET_C27_T_ALL_MSGS"] = "All Messages";
$MESS["SONET_C27_T_ANSWER"] = "Reply";
$MESS["SONET_C27_T_MARK_READ"] = "Mark as read";
$MESS["SONET_C27_T_DELETE"] = "Delete";
$MESS["SONET_C27_T_BAN"] = "Ban";
$MESS["SONET_C27_T_EMPTY"] = "No incoming messages.<br>This pane shows messages from your contacts.";
?>