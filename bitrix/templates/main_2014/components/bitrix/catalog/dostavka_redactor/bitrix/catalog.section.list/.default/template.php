<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<link href="<?=$templateFolder?>/style.css" type="text/css" rel="stylesheet">
<a href="<?=$templateFolder?>/core.php?act=new_rzdel&ELEMENT_ID=<?=$_REQUEST["RESTORAN"]?>&SECTION_ID=<?=$arResult["SECTION"]["ID"]?>&IBLOCK_ID=<?=$arResult["SECTION"]["IBLOCK_ID"]?>" class="btn add_new_section">+ Добавить раздел</a>
<?if ($arParams["SECTION_ID"]):?>
    <a id="add_new_bl" href="/bitrix/templates/main/components/bitrix/catalog/dostavka_redactor/bitrix/catalog.section/.default/core.php?act=add_new&SECTION_ID=<?=$arResult["SECTION"]["ID"]?>&IBLOCK_ID=<?=$arResult["SECTION"]["IBLOCK_ID"]?>" class="btn btn-info" >+ Новое блюдо</a>
<?endif;?>
<?/*if (CSite::InGroup(Array(1,15,16))):?>
    <input type="button" id="from_csv" class="light_button" value="Загрузить из файла" style="float:right" />                       
    <div class="clear"></div>
<?endif;*/?>
<br />
<table class="menu_edit" cellpadding="10" cellspacing="0">
    <tr class="th">
        <th>Название</th>
        <th width="70">Сортировка</th>
        <th></th>
    </tr>    
    <?if ($_REQUEST["SECTION_ID"]&&!$_GET["PARENT_SECTION_ID"]):?>
        <tr>
            <td colspan="3">
                <span class="razd_icon_up"></span>
                <a href="menu_edit.php?RESTORAN=<?=$_REQUEST["RESTORAN"]?>">...</a>
            </td>
        </tr>
    <?endif;?>
    <?if ($_REQUEST["SECTION_ID"]&&$_GET["PARENT_SECTION_ID"]):?>
        <tr>
            <td colspan="3">
                <span class="razd_icon_up"></span>
                <a href="menu_edit.php?RESTORAN=<?=$_REQUEST["RESTORAN"]?>&SECTION_ID=<?=$_GET["PARENT_SECTION_ID"]?>">...</a>
            </td>
        </tr>
    <?endif;?>
<?
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $key=>$arSection):
//	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
//	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
//	if($CURRENT_DEPTH<$arSection["DEPTH_LEVEL"])
//		echo "<ul>";
//	elseif($CURRENT_DEPTH>$arSection["DEPTH_LEVEL"])
//		echo str_repeat("</ul>", $CURRENT_DEPTH - $arSection["DEPTH_LEVEL"]);
//	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
?>
	<tr class="<?=($key%2==1)?"bg2":""?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
            <td style="width:75%">
                <span class="razd_icon"></span>
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>&RESTORAN=<?=$_REQUEST["RESTORAN"]?>&PARENT_SECTION_ID=<?=$arParams["SECTION_ID"]?>&<?=bitrix_sessid_get()?>"><?=$arSection["NAME"]?></a><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;<sup><?=$arSection["ELEMENT_CNT"]?></sup><?endif;?>
            </td>
            <td align="center">
                <?=$arSection["SORT"]?>
            </td>
            <td align="right">
<!--                <a href="<?=$templateFolder?>/core.php?act=edit_rzdel&SECTION_ID=<?=$arSection["ID"]?>&IBLOCK_ID=<?=$arSection["IBLOCK_ID"]?>">Редактировать</a>
                <a href="<?=$templateFolder?>/core.php?act=del_rzdel&SECTION_ID=<?=$arSection["ID"]?>">Удалить</a>-->
                <a href="<?=$templateFolder?>/core.php?act=edit_rzdel&SECTION_ID=<?=$arSection["ID"]?>&IBLOCK_ID=<?=$arSection["IBLOCK_ID"]?>" class="edite_section"></a>
                <a href="<?=$templateFolder?>/core.php?act=del_rzdel&SECTION_ID=<?=$arSection["ID"]?>" class="del_section"></a>
            </td>	            
	</tr>
<?endforeach?>
<?if (!$arParams["SECTION_ID"]):?>
</table>
<?endif;?>


<?
//v_dump($_REQUEST["SECTION_ID"]);
//v_dump($arResult["SECTION"]);
?>