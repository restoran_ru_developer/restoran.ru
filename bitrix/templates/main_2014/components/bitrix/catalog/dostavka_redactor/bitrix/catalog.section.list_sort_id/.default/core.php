<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function new_rzdel(){
	?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="act" value="add_new_razdel">
		<input type="hidden" name="SECTION_ID" value="<?=$_REQUEST["SECTION_ID"]?>">

		<div class="question">
			Название раздела<div><input type="text" req="req" class="inputtext" name="NAME" value="" size=""></div>
		</div> 
	

		<input class="light_button" type="submit" name="web_form_submit" value="Добавить">
		<input class="dark_button popup_close" type="button" value="Закрыть">
	</form>


	<?
}



function add_new_razdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$bs = new CIBlockSection;
	$arFields = Array(
  		"ACTIVE" => "Y",
  		"IBLOCK_SECTION_ID" =>$_REQUEST["SECTION_ID"],
  		"IBLOCK_ID" => 151,
  		"NAME" => $_REQUEST["NAME"]
  	);

  	if($ID = $bs->Add($arFields)) echo "ok";
  	else echo $bs->LAST_ERROR;

}


function save_rzdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$bs = new CIBlockSection;
	$arFields = Array("NAME" => $_REQUEST["NAME"]);
	$res = $bs->Update($_REQUEST["SECTION_ID"], $arFields);
	
}


function edit_rzdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$res = CIBlockSection::GetByID($_REQUEST["SECTION_ID"]);
	if($ar_res = $res->GetNext()){
	?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="act" value="save_rzdel">
		<input type="hidden" name="SECTION_ID" value="<?=$_REQUEST["SECTION_ID"]?>">

		<div class="question">
			Название раздела<div><input type="text" req="req" class="inputtext" name="NAME" value="<?=$ar_res["NAME"]?>" size=""></div>
		</div> 
	

		<input class="light_button" type="submit" name="web_form_submit" value="Сохранить">
		<input class="dark_button popup_close" type="button" value="Закрыть">
	</form>

	<?
	}
}

function del_rzdel(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	CIBlockSection::Delete($_REQUEST["SECTION_ID"]);


}


if($_REQUEST["act"]=="save_rzdel") save_rzdel();
if($_REQUEST["act"]=="del_rzdel") del_rzdel();
if($_REQUEST["act"]=="edit_rzdel") edit_rzdel();
if($_REQUEST["act"]=="new_rzdel") new_rzdel();
if($_REQUEST["act"]=="add_new_razdel") add_new_razdel();
?>

