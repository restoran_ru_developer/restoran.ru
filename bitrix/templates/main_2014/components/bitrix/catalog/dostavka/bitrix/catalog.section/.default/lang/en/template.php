<?
$MESS["CATALOG_BUY"] = "Buy";
$MESS["CATALOG_COMPARE"] = "Compare";
$MESS["CATALOG_NOT_AVAILABLE"] = "(not available from stock)";
$MESS["CATALOG_QUANTITY"] = "Quantity";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "from #FROM# to #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# and more";
$MESS["CATALOG_QUANTITY_TO"] = "up to #TO#";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CS_PRINT"] = "Print";
$MESS["CS_LIST"] = "List of menu";
$MESS["CS_ALLLIST"] = "All menu";
$MESS["CS_SHOWALLLIST"] = "Show all menu";
?>