<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$inc_key=0;
$inner_key = 2;
//FirePHP::getInstance()->info($arResult["SECTIONS"]);
?>
<div class="pull-left menu-1 bottom-menu-block-wrap">
<?
foreach($arResult["SECTIONS"] as $key=>$arSection):
//	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
//	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
?>


    <ul>
        <li id="<?//=$this->GetEditAreaId($arSection['ID']);?>" <?//=($arSection["ELEMENT_CNT"]?'class="dropdown"':"")?>>
            <?if(!empty($arSection["CODE"])):?>
                <a href="<?=$arSection["CODE"]?>">
                    <?=$arSection["NAME"]?>
                </a>
            <?else:?>
                <?=$arSection["NAME"]?>
            <?endif?>
            <?if ($arSection["ELEMENT_CNT"]):?>
                <ul>
                    <?foreach ($arSection["ELEMENTS"] as $item):?>
                        <li><a href="<?=$item["CODE"]?>"><?=$item["NAME"]?></a></li>
                    <?endforeach;?>
                </ul>
            <?endif;?>
        </li>
    </ul>
    <?if(($inc_key+1)%3==2 && $arSection!=end($arResult["SECTIONS"])):?>
        </div>
        <div class="pull-left menu-<?=$inner_key++?> bottom-menu-block-wrap">
    <?endif;?>
    <?if($arSection==end($arResult["SECTIONS"])):?>
        <div class="pull-left hexagon">16+</div>
        <div class="logo2"></div>
        <div class="clearfix"></div>
        <div class="copyright">
            © <?=date("Y")?> Ресторан.Ru
            <?$APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/footer_copyright_2.php"),
                Array(),
                Array("MODE"=>"html")
            );?>
        </div>
    <?endif?>
    <?$inc_key++;?>
<?endforeach?>
        </div>