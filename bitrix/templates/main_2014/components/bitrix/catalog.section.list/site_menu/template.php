<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="nav navbar-nav">
    <?
    if(substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")){
        $bIpad = true;
    }

    foreach($arResult["SECTIONS"] as $arSection):
        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li id="<?=$this->GetEditAreaId($arSection['ID']);?>" <?=($arSection["ELEMENT_CNT"]?'class="dropdown"':"")?>>
            <a href="<?if(!$bIpad):?><?=($arSection["CODE"])?$arSection["CODE"]:"javascript:void(0)"?><?else: echo "javascript:void(0)";?><?endif?>" <?=($arSection["ELEMENT_CNT"]?'class="dropdown-toggle" ':"")?>>
                <?=$arSection["NAME"]?>
            </a>
            <?if (count($arSection["ELEMENTS"])):?>
                <ul class="dropdown-menu speech-bubble-top">
                    <?foreach ($arSection["ELEMENTS"] as $item):?>
                        <li><a href="<?=$item["CODE"]?>"><?=$item["NAME"]?></a></li>
                    <?endforeach;?>
                </ul>
            <?endif;?>
        </li>
    <?endforeach?>
</ul>