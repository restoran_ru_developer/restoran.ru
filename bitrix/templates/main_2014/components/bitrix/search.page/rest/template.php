<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="news-list detail_article">

<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>
<?if(count($arResult["SEARCH"])>0):?>
        <h1><?=GetMessage("SR_TITLE")?></h1>
        <div class="clearfix"></div>
	<?foreach($arResult["SEARCH"] as $key=>$arItem):
        ?>
                <div class="item pull-left<?if($key%3==2):?> end<?endif?>">
                    <?if ($arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"]):?>
                        <div class="pic">
                            <a href="<?=$arItem["URL"]?>"><img src="<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]?>" /></a>
                            <?/*if ($arItem["ELEMENT"]["PROPERTIES"]["d_tours"]["VALUE"][0]):?>
                                <div style="position:absolute; right:10px; bottom:10px;"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour2.png" width="70"/></div>
                            <?endif;*/?>
                        </div>
                        <?endif;?>
                        <div class="text">
                            <h2><a href="<?=$arItem["URL"]?>">
                                <?=$arItem["ELEMENT"]["NAME"]?>
                                <?if (substr_count($arItem["URL"],"restaurants")):?>
                                    [<?=GetMessage("R_RESTORAN")?>]
                                    <?$res = 1;?>
                                <?elseif(substr_count($arItem["URL"],"banket")):?>
                                    [<?=GetMessage("R_BANKET")?>]
                                    <?$res = 2;?>
                                <?elseif(substr_count($arItem["URL"],"dostavka")):?>
                                    [<?=GetMessage("R_DOSTAVKA")?>]
                                    <?$res = 3;?>
                                <?endif;?>
                                    </a>
                            </h2>                                                        
                            <div class="ratio">
                                <?for($z=1;$z<=5;$z++):?>
                                    <span class="<?=(round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                                <?endfor?>                      
                            </div>
                            <div class="props">                                
                                <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]):?>
                                    <div class="prop">
                                        <div class="name"><?=GetMessage("FAV_KITCHEN")?>:</div> 
                                        <div class="value">
                                            <?
                                            if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))
                                                echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]));
                                            else
                                                echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]);
                                            ?>
                                        </div>
                                    </div>
                                <?endif;?>                                                    
                                <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]):?>
                                    <div class="prop">
                                        <div class="name"><?=GetMessage("FAV_BILL")?>:</div>   
                                        <div class="value">
                                            <?
                                            if (CITY_ID=="tmn"):
                                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))
                                                    $av = strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]));
                                                else
                                                    $av = strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]);
                                                echo str_replace("до 1000р","500-1000р",$av);
                                            else:
                                                if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))
                                                    echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]));
                                                else
                                                    echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]);
                                            endif;
                                            ?>              
                                        </div>
                                    </div>
                                    </p>
                                <?endif;?>

                                <?
                                if(!$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['REST_NETWORK']['DISPLAY_VALUE']):?>
                                    <?if ($arItem["ELEMENT"]["PROPERTIES"]["address"]):?>
                                        <div class="prop">
                                            <div class="name"><?=GetMessage("FAV_ADRES")?></div>
                                            <div class="value">
                                            <?
                                                if ($arItem["ELEMENT"]["ID"]==387287):
                                                    echo implode("<br />",$arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]);
                                                else:
                                                    if (is_array($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]))
                                                        echo strip_tags($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"][0]);
                                                        //echo strip_tags(implode(", ",$arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]));
                                                    else
                                                        echo strip_tags($arItem["ELEMENT"]["PROPERTIES"]["address"]["VALUE"]);
                                                endif;
                                                ?>
                                            </div>
                                        </div>
                                    <?endif;?>
                                    <?if ($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]):?>
                                        <div class="prop wsubway">
                                            <div class="subway">М</div>
                                            <div class="value">
                                                    <?
                                                    if (is_array($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))
                                                        echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]);
                                                        //echo strip_tags(implode(", ",$arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]));
                                                    else
                                                        echo strip_tags($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]);
                                                    ?>
                                            </div>
                                        </div>
                                    <?endif;?>
                                <?else:?>
                                    <div class="prop">
                                        <div class="name">
                                            <?=GetMessage('NETWORK_REST_TITLE')?>:
                                        </div>
                                        <div class="value">
                                            <?=count($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['REST_NETWORK']['DISPLAY_VALUE'])?> <?if(count($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['REST_NETWORK']['DISPLAY_VALUE'])==1)echo GetMessage('ADDRESS_OF_NETWORK_TITLE');elseif(count($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['REST_NETWORK']['DISPLAY_VALUE'])<5)echo GetMessage('ADDRESS_OF_NETWORK_TITLE_A');elseif(count($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]['REST_NETWORK']['DISPLAY_VALUE'])>4)echo GetMessage('ADDRESS_OF_NETWORK_TITLE_OF');?>
                                        </div>
                                    </div>
                                <?endif;?>
                            </div>                                                  
                        </div>                                    
                        <?if(CSite::InGroup( array(14,15,1))):?>                        
                            <?/*<div id="edit_link" ><a class="no_border" href="/users/id<?=$USER->GetID()?>/restoran_edit/?REST_ID=<?=$arItem["ELEMENT"]["ID"]?>">Редактировать</a></div>*/?>
                            <div id="edit_link" ><a class="no_border" href="/redactor/edit.php?ID=<?=$arItem["ELEMENT"]["ID"]?>">Редактировать</a></div>
                        <?endif;?>
                </div>                                    
                <?if($key%3==2):?><div class="clearfix"></div><?endif?>
	<?endforeach;?>
        <div class="clear"></div>
            <?if(count($arResult["SEARCH"])<$arParams["PAGE_RESULT_COUNT"]&&($_REQUEST['PAGEN_1']==1||!$_REQUEST['PAGEN_1'])):?>

            <?else:?>
                <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"):?>
                    <div class="navigation">
                        <?= $arResult["NAV_STRING"] ?>
                    </div>
                <?endif;?>
            <?endif;?>

        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div class="more_links text-right">                    
                    <a href="<?=$APPLICATION->GetCurPageParam("search_in=rest", array("search_in","x","y")); ?>" class="btn btn-light"><?=GetMessage("SR_ALL_ITEMS")?></a>
            </div>            
        <?endif;?>
        <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                
</div>
