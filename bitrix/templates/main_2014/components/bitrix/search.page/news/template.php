<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="news-list">
<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>
<?if(count($arResult["SEARCH"])>0):?>
        <h1><?=$arParams["SECTION_NAME"]?></h1>
        <div class="clearfix"></div>
	<?foreach($arResult["SEARCH"] as $key=>$arItem):?>		
                <div class="item pull-left<?if($key%3==2):?> end<?endif?>">
                    <div class="date"><?=$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_DAY"]." ".$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_MONTH"]." ".$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_YEAR"]?></div>
                    <br />
                    <?if ($arItem["ELEMENT"]["PREVIEW_PICTURE"]):?>
                    <div class="pic">
                        <a href="<?=$arItem["URL"]?>"><img src="<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]?>" /></a>
                    </div>
                    <?endif;?>                    
                    <div class="text">
                        <h2><a href="<?=$arItem["URL"]?>">
                                <?=$arItem["ELEMENT"]["NAME"]?></a>
                        </h2>                        
                    </div>                    
                    <p><?=$arItem["ELEMENT"]["PREVIEW_TEXT"]?></p>                                        
                </div>
                <?if($key%3==2):?><div class="clearfix"></div><?endif?>
	<?endforeach;?>
        <div class="clearfix"></div>
<!--	--><?//if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&& count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]):?>
<!--            <div class="navigation">-->
<!--                --><?//= $arResult["NAV_STRING"] ?><!--                    -->
<!--            </div>-->
<!--        --><?//endif;?>

    <?if(count($arResult["SEARCH"])<$arParams["PAGE_RESULT_COUNT"]&&($_REQUEST['PAGEN_1']==1||!$_REQUEST['PAGEN_1'])):?>

    <?else:?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"):?>
            <div class="navigation"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>
    <?endif;?>

        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div class="more_links text-right">
                <?//FirePHP::getInstance()->info(GetMessage("SR_ALL_ITEMS"),'SR_ALL_ITEMS');?>
                    <a href="<?=$APPLICATION->GetCurPageParam("search_in=".$arParams["SEARCH_IN"], array("search_in","x","y")); ?>" class="btn btn-light"><?=GetMessage("SR_ALL_ITEMS_NEW")?> <?=$arParams["SECTION_NAME"]?></a>
            </div>            
        <?endif;?>
        <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                 
</div>
