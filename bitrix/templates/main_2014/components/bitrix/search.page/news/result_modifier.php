<?
CModule::IncludeModule("iblock");
$arReviewsIB = getArIblock("reviews", CITY_ID);
global $search;
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $search = 1;
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();                                                
                if (!$el["ACTIVE_FROM"])
                {
                    $el["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($el["CREATED_DATE"], "YYYY.MM.DD"));;
                }
                else
                {
                    $el["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($el["ACTIVE_FROM"], CSite::GetDateFormat()));
                }
                $arTmpDate = explode(" ", $el["DISPLAY_ACTIVE_FROM"]);
                $el["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
                $el["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
                $el["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
            foreach ($el["PROPERTIES"] as $key=>$prop)
                $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            //v_dump($el["DISPLAY_PROPERTIES"]);
            $arItem["ELEMENT"] = $el;
            if ($el["PREVIEW_PICTURE"])
            {
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true, Array());
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"];
            }
            else
            {
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true, Array());
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"];
            }
            $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["PREVIEW_TEXT"]), 200);
            if (!$arItem["ELEMENT"]["PREVIEW_TEXT"])
                $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["DETAIL_TEXT"]), 200);
         }
    endforeach;?>
<?endif;?>
