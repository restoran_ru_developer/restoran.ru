<?
CModule::IncludeModule("iblock");
if (is_array($arResult["SEARCH"])):?>
    <?$el = array();?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($ar = $res->GetNext())
        {
            $res2 = CIBlockSection::GetByID($ar["IBLOCK_SECTION_ID"]);
            if ($ar2 = $res2->Fetch())
                $ar["SECTION_NAME"] = $ar2["NAME"];
            $arItem["ELEMENT"] = $ar;
        }
        $arFileTmp = CFile::ResizeImageGet(
            $arItem["ELEMENT"]['DETAIL_PICTURE'],
            array("width" => 40, "height" => 40),
            BX_RESIZE_IMAGE_EXACT,
            false
        );
        $arItem["ELEMENT"]['DETAIL_PICTURE_PREVIEW'] = $arFileTmp['src'];
    endforeach;
    ?>
<?//v_dump($arResult["SEARCH"])?>
<?endif;?>
