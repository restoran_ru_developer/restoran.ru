<?
CModule::IncludeModule("iblock");
global $search;
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $search = 1;
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            
            if ($el["ACTIVE_FROM"])
                $el["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($el["ACTIVE_FROM"], CSite::GetDateFormat()));
            elseif ($el["PROPERTIES"]["EVENT_DATE"]["VALUE"])
                $el["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($el["PROPERTIES"]["EVENT_DATE"]["VALUE"], CSite::GetDateFormat()));
            $arTmpDate = explode(" ", $el["DISPLAY_ACTIVE_FROM"]);
            $el["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
            $el["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
            $el["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
            foreach ($el["PROPERTIES"] as $key=>$prop)
                $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            //v_dump($el["DISPLAY_PROPERTIES"]);
            $arItem["ELEMENT"] = $el;
            /*if (is_array($el["PREVIEW_PICTURE"]))
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            else*/
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
                $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["PREVIEW_TEXT"]), 200);
         }
    endforeach;?>
<?endif;?>
