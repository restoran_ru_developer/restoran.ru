<?
$MESS["SEARCH_ALL"] = "(everywhere)";
$MESS["SEARCH_GO"] = "Search";
$MESS["SEARCH_ERROR"] = "In search phrase error was detected:";
$MESS["SEARCH_CORRECT_AND_CONTINUE"] = "Correct the search phrase and search again.";
$MESS["SEARCH_SINTAX"] = "<b> search query syntax: </ b> <br /> <br /> Usually request is a simple one or more words, for example: <br /> <i> Contact Information </ i> <br /> On this query will find pages containing both query words. <br /> <br /> The logical operators allow building more complex queries, such as: <br /> <i> contact information or phone </ i> <br /> This query will find pages containing words & quot ; contact & quot; and & quot; information & quot ;, or the word & quot; phone & quot ;. <br /> <br /> <i> contact information not phone </ i> <br /> This query will find pages containing words & quot; contact & quot; and & quot; information & quot ;, but not the word & quot; phone & quot ;. <br /> You can use brackets to build more complex queries. <br />";
$MESS["SEARCH_LOGIC"] = "Logical operators:";
$MESS["SEARCH_OPERATOR"] = "Operator";
$MESS["SEARCH_SYNONIM"] = "Synonyms";
$MESS["SEARCH_DESCRIPTION"] = "Description";
$MESS["SEARCH_AND"] = "and";
$MESS["SEARCH_AND_ALT"] = "Operator <i> logical &quot; and &quot; </ i> implied and can be omitted: a query &quot; contact information &quot; fully equivalent to &quot; and contact information &quot ;.";
$MESS["SEARCH_OR"] = "or";
$MESS["SEARCH_OR_ALT"] = "Operator <i> logical &quot; or &quot; </ i> allows you to search for products that contain at least one of the operands.";
$MESS["SEARCH_NOT"] = "not";
$MESS["SEARCH_NOT_ALT"] = "Operator <i> logical &quot; no &quot; </ i> limits the search to pages that do not contain the word that follows the operator.";
$MESS["SEARCH_BRACKETS_ALT"] = "<i> Parentheses </ i> define the logical operators.";
$MESS["SEARCH_MODIFIED"] = "Last modified:";
$MESS["SEARCH_NOTHING_TO_FOUND"] = "Unfortunately, your search did not found.";
$MESS["SEARCH_PATH"] = "Path:";
$MESS["SEARCH_SORT_BY_RANK"] = "Sort by relevance";
$MESS["SEARCH_SORTED_BY_DATE"] = "Sorted by date";
$MESS["SEARCH_SORTED_BY_RANK"] = "Sorted by relevance";
$MESS["SEARCH_SORT_BY_DATE"] = "Sort by date";
$MESS["CT_BSP_ADDITIONAL_PARAMS"] = "More search options";
$MESS["CT_BSP_KEYBOARD_WARNING"] = "The query \"#query#\" keyboard layout is restored.";
$MESS["FAV_TYPE"] = "Type";
$MESS["FAV_KITCHEN"] = "Kitchen";
$MESS["FAV_BILL"] = "Average bill";
$MESS["R_SALE"] = "Discount";
$MESS["R_ZA"] = "for";
$MESS["SR_TITLE"] = "Coupons";
$MESS["SR_ALL_ITEMS"] = "All coupons";
?>