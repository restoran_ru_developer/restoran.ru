<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="block">
    <div class="ajax_form" style="" id="auth">
    <?if($arResult["AUTH_SERVICES"]):?>
            <div class="title"><?echo GetMessage("AUTH_TITLE")?></div>
    <?endif?>
            <!--<div><?=GetMessage("AUTH_PLEASE_AUTH")?></div>-->
            <form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="AUTH" />
                <?if (strlen($arResult["BACKURL"]) > 0):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                <?endif?>
                <?foreach ($arResult["POST"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                <?endforeach?>
                <div class="form-group">            
                    <input type="email" class="form-control" placeholder="E-mail" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
                </div>            
                <div class="form-group end">
                    <input class="form-control" placeholder="<?=GetMessage("AUTH_PASSWORD")?>" type="password" name="USER_PASSWORD" maxlength="255" />
                </div>

                <script src="/tpl/js/jquery.checkbox.js"></script>
                <div class="remember-me">
                    <span class="niceCheck" >
                        <input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" style="display: none;">
                    </span>
                    <label for="USER_REMEMBER" class="niceCheckLabel" ><?=GetMessage("AUTH_REMEMBER_ME")?></label>
                </div>

                <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                    <noindex>                    
                        <a class="forget" style="margin:0px;font-size:14px;" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>                                        
                    </noindex>
                <?endif?>
                <div align="center">
                    <input type="submit" class="btn btn-info btn-nb" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" style="width:100%"/>
                </div>
                <?/*if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
                    <noindex>
                            <p>
                                    <a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a><br />
                                    <?=GetMessage("AUTH_FIRST_ONE")?> 
                            </p>
                    </noindex>
                <?endif*/?>
            </form>
            <script type="text/javascript">
                <?if (strlen($arResult["LAST_LOGIN"])>0):?>
                try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
                <?else:?>
                try{document.form_auth.USER_LOGIN.focus();}catch(e){}
                <?endif?>
                </script>
                <!--<div class="bx-auth-title">Войти как пользователь</div>-->
                <div class="bx-auth-note" style="line-height:16px;margin:10px 0px;" align="center">Также Вы можете войти на сайт, если вы зарегистрированы на одном из этих сервисов:</div>
                <div class="pull-left">
                    <?$APPLICATION->IncludeComponent("restoran:user.vkontakte_auth", ".default", array(
                        ),
                        false
                    );?>
                </div>
                <div class="pull-right">
                    <?$APPLICATION->IncludeComponent("restoran:user.facebook_auth", ".default", array(
                        ),
                        false
                    );?>
                </div>
                <div class="clearfix"></div>
                <br />
                <div class="reg">
                    <div class="pull-left">
                        <div class="italic" style="text-align:left;"><?=GetMessage("AUTH_REGISTER?")?></div>
                    </div>
                    <div class="pull-right" >            
                        <a class="btn btn-info btn-nb-empty" href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>                                        
                    </div> 
                    <div class="clearfix"></div>
                </div>
    </div>    
</div>