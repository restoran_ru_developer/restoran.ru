<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<link href="<?=$templateFolder?>/style.css" rel="stylesheet" media="all" />
<script src="<?=$templateFolder?>/script.js"></script>
<?if ($arResult["isFormErrors"] == "Y"):?>

	<?=$arResult["FORM_ERRORS_TEXT"];?>


<!--	<script>
		$(window).load(function(){
		
			setTimeout(function(){
			
				$(".order-banner").click();
				
			
			},150);
		
		
		});
	</script>-->


	
<?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y") 
{
?>
<?=$arResult["FORM_HEADER"]?>


<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y") 
{ 
?>
<?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormTitle"]) 
{
?>
	<h1 style="color:#FFFFFF;"><?=$arResult["FORM_TITLE"]?></h1>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>

			<p><?=$arResult["FORM_DESCRIPTION"]?></p>

	<? 
} // endif 
	?>

<br />
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>


	<?

	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
	$arQuestion['QCODE'] = $FIELD_SID;
	//echo $FIELD_SID;
	$breaker = false;
	switch($FIELD_SID)
	{                
		case "SIMPLE_QUESTION_BANNER_7":
		?>
			<br/><?=$arQuestion["CAPTION"]?><br/><input type="text" value="<?=$USER->GetFullName();?>" name="form_text_52" class="inputtext w100">
		<?
		$breaker = true;	
		break;
		case "SIMPLE_QUESTION_BANNER_3":
		?>
			<br/><?=$arQuestion["CAPTION"]?><br/><input type="file" size="0" class="inputfile w100" name="form_file_49">
		<?
		$breaker = true;	
		break;
		
	
		case "SIMPLE_QUESTION_BANNER_8":
		?>
			<br/><br/><?=$arQuestion["CAPTION"]?><br/><input type="text" value="<?=$USER->GetEmail();?>" name="form_text_53" class="inputtext w100">
		<?
		$breaker = true;	
		break;
		
		case "SIMPLE_QUESTION_BANNER_9":
		?>
			<br/><br/><?=$arQuestion["CAPTION"]?><br/><input type="text" name="form_text_54" class="phone1 inputtext w100"><br/><br/>
		<?
		$breaker = true;	
		break;
                case "SIMPLE_QUESTION_BANNER_2":
		?>
			<br/><br/><?=$arQuestion["CAPTION"]?><br/><input id="date" type="text" name="form_text_48" class="inputtext w100"><br/><br/>
                        <script>
                            $.tools.dateinput.localize("ru",  { months: 'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь', 
                            shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
                            days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
                            shortDays:     'вс,пн,вт,ср,чт,пт,сб'});       
                            $(".phone1").maski("(999) 999-99-99",{placeholder:" "});
                            $("#date").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});                                                                                                                   
                    </script>
		<?
		$breaker = true;	
		break;
		
		case "SIMPLE_QUESTION_BANNER_1":
		?>
			<input type="hidden" name="form_text_47" value="<?=$_REQUEST["id"]?>" />
<!--                        <input type="hidden" size="0" name="form_text_47" class="inputtext w100" id="SIMPLE_QUESTION_BANNER_1">-->
		<?
		$breaker = true;	
		break;
		
		case "SIMPLE_QUESTION_BANNER_5":
		
                    $arRestIB = getArIblock("catalog", CITY_ID);
			$resRestraunts = CIBlockElement::GetList(array("NAME" => "asc"), array("IBLOCK_ID" => $arRestIB["ID"], "PROPERTY_user_bind" => $USER->GetID()));
			$select = '';
			while($arRest = $resRestraunts->Fetch())
			{				
                            if ($arRest["ID"]==$_REQUEST["id"])                                
                            {
				echo "<input type='hidden' class='inputtext w100'  name='form_text_50'  value='[".$arRest['ID']."] ".$arRest['NAME']."'>";
                                echo "<input type=hidden name='id'  value='{$arRest['ID']}' />";
                                $rest_name = "<input type='text' class='inputtext w100' disabled  value='".$arRest['NAME']."'>";
                            }
                            //else
                              //  $select .= "<option value='".$arRest['ID']."-".$arRest['NAME']."'>".$arRest['NAME']."</option>\r\n";
			}
			
		?> 
		<?=$arQuestion["CAPTION"]?><br/><a><?=$rest_name?></a>
<!--                <select name="form_text_50" id="SIMPLE_QUESTION_BANNER_5" class="w100">-->
			<?=$select;?>
<!--			</select>-->
                        <br/>
		
		<?
		$breaker = true;	
		break;
		
		
		case "SIMPLE_QUESTION_BANNER_6":
		
		
			$resRestraunts = CIBlockElement::GetList(array("NAME" => "asc"), array("IBLOCK_ID" => 155, "ACTIVE" => "Y"));
			$select = '';
			while($arRest = $resRestraunts->Fetch())
			{		
                                if ($arRest["NAME"]==$_REQUEST["baner"])
                                {
                                    echo "<input type=hidden name='form_text_51'  value='[{$arRest['ID']}] {$arRest['NAME']}' />";
                                    echo "<input type=hidden name='baner'  value='{$arRest['NAME']}' />";
                                    $baner_name = "<input type='text' class='inputtext w100' disabled  value='".$arRest['NAME']."'>";
                                }   
//                                else
//                                    $select .= "<option value='{$arRest['ID']}'>{$arRest['NAME']}</option>\r\n";
			}
			
		?> 
			<br/><?=$arQuestion["CAPTION"]?><br/><?=$baner_name?>
<!--                        <select name="form_text_51" id="SIMPLE_QUESTION_BANNER_6" class="w100">
			<?=$select;?>
			</select>-->
                        <br/>
		
		<?
		$breaker = true;	
		break;
	}
	
	if($breaker) continue;
	?>

				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>
				<br/><?=$arQuestion["CAPTION"]?><br/>
				<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>

			<?=$arQuestion["HTML_CODE"]?>
		<br/>
	<? 
	} //endwhile 
	?>
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
		<input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" />
		
		<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
<?
} // isUseCaptcha
?>


				
				<input type="button" value="Отправить" id="order_banner123" class="button" name="web_form_submit">
                                <script>
                                    $(document).ready(function(){
                                            $("#order_banner123").click(function(){      
                                                var form = $(this).parents("form");
                                                $.ajax({
                                                            type: "POST",
                                                            url: form.attr("action"),
                                                            data: form.serialize()+"&web_form_submit=1",
                                                            success: function(data) {                                                                
                                                                $('.ok').fadeIn(500);
                                                                form.parent().html(data);                    
                                                                setTimeout("$('.ok').fadeOut(500)",10000);
                                                            }
                                                        }); 
                                                    return false;
                                                });
                                        });
                                </script>
				<?if ($arResult["F_RIGHT"] >= 15):?>
				&nbsp;<input type="hidden" name="web_form_apply" value="Y" />
				<?endif;?>


<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>