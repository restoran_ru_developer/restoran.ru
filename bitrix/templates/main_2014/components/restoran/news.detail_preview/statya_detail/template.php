<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/templates/main/js/flowplayer.js"></script>
<script>
$(document).ready(function(){
   flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.12.swf", {
        clip: {
                autoPlay: false, 
                autoBuffering: true,
                accelerated: true,
                start: 15
        }
    });
    $(".articles_photo").galery(); 
});
</script>
<script>
$(document).ready(function(){
    $(".photos123").each(function(){
       var wid = $(this).width();
       //var hei = $(this).height();
       $(this).append("<div class='block'>Увеличить</div>");
       $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
    });
    $(".photos123").hover(function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        console.log(wid);
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"}); */
        $(this).find(".block").show();
    },
    function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"});*/
        $(this).find(".block").hide();
    });
    $(".photos123 .block").toggle(function(){
        $(this).parent().find("img:hidden").show().prev().hide();        
        $(this).parent().removeClass('left');        
        $(this).html("Уменьшить");
        $(this).parent().css("width","728px");
        var wid = $(this).parent().width();
        //var hei = $(this).parent().height();
        $(this).css({"left":wid/2-50+"px"});
    },
    function(){
        $(this).parent().find("img:hidden").show().next().hide();
        $(this).parent().addClass('left');        
        $(this).html("Увеличить");
        $(this).parent().css("width","238px");
        var wid = $(this).parent().width();
        //var hei = $(this).parent().height();
        $(this).css({"left":wid/2-50+"px"});
    });    
})
</script>  
<?//$resto = array();?>
<?
//global $resto;
?>
<div id="content" class="detail_article">
    <div class="left" style="width:728px;">
        <div class="statya_section_name">
            <a class="uppercase another font14" href=<?=$arResult["SECTION_PAGE_URL"]?>><?=$arResult["IBLOCK_TYPE_NAME"]?></a>            
        </div>
        <hr class="black" />
        <h1><?=$arResult["NAME"]?></h1>
        <a class="another no_border" href="/users/id<?=$arResult["CREATED_BY"]?>/"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <?if($arResult["PREVIEW_TEXT"]):?>
            <i><?=$arResult["PREVIEW_TEXT"]?></i>
            <br />
        <?endif;?>
        <?if ($_REQUEST["IBLOCK_TYPE"]=="cookery"):?>
            <br /><br />
            <div style="position:relative">
                <?if ($arResult["PROPERTIES"]["porc"]["VALUE"]):?>
                    <div itemprop="recipeYield" class="yield portion">
                        <div><?=$arResult["PROPERTIES"]["porc"]["VALUE"]?></div>
                        <?=pluralForm($arResult["PROPERTIES"]["porc"]["VALUE"],GetMessage("R_PORTION1"),GetMessage("R_PORTION2"),GetMessage("R_PORTION3"))?>
                    </div>
                <?endif;?>
                <?if ($arResult["DETAIL_PICTURE"]["width"]>=720):?>
                    <img itemprop="image" class="photo" src="<?=$arResult["DETAIL_PICTURE"]["src"]?>" width="720" />
                <?else:?>
                    <img itemprop="image" class="photo" src="<?=$arResult["DETAIL_PICTURE"]["src"]?>" />
                <?endif;?>
            </div>
            <br />
        <?endif;?>
        <?=$arResult["DETAIL_TEXT"]?>
            <div class="clear"></div>
            <br />
        <?if($arParams["EDIT_NOW"]=="Y"):?>
            <div class="left">
                <?if (CSite::InGroup(Array(9))):?>
                    <?if ($_REQUEST["IBLOCK_TYPE"]=="afisha"):?>
                        <a href="/restorator/blog/editor/afisha.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
                            <input type="button" class="light_button" value="Продолжить редактирование">
                        </a>
                    <?else:?>
                        <a href="/users/id<?=$arResult["CREATED_BY"]?>/blog/editor/blog.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
                            <input type="button" class="light_button" value="Продолжить редактирование">
                        </a>
                    <?endif;?>
                <?else:?>
                    <?if ($_REQUEST["IBLOCK_TYPE"]=="cookery"):?>
                        <a href="/users/id<?=$arResult["CREATED_BY"]?>/blog/editor/recepts.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
                            <input type="button" class="light_button" value="Продолжить редактирование">
                        </a>
                    <?else:?>
                        <a href="/users/id<?=$arResult["CREATED_BY"]?>/blog/editor/blog.php?IB=<?=$arResult["IBLOCK_ID"]?>&SECTION=<?=$arResult["IBLOCK_SECTION_ID"]?>&ID=<?=$arResult["ID"]?>" style="text-decoration:none">
                            <input type="button" class="light_button" value="Продолжить редактирование">
                        </a>
                    <?endif;?>
                <?endif;?>
            </div>
            <div class="right">     
                <?
                switch ($_REQUEST["IBLOCK_TYPE"])
                {
                    case "cookery":
                        $bcu = "recepts";
                    case "news":
                        $bcu = "news";
                    case "review":
                        $bcu = "response";
                    default:
                        $bcu = "blog";                    
                }
                ?>                
                <input type="button" class="dark_button" id="save_this" backurl="/users/id<?=$arResult["CREATED_BY"]?>/blog/#<?=$bcu?>.php" value="Опубликовать">
                <input type="hidden" id="ELEMENT_ID" value="<?=$arResult["ID"]?>">
                <input type="hidden" id="IBLOCK_ID" value="<?=$arResult["IBLOCK_ID"]?>">
            </div>
            <div class="clear"></div>
            <br /><Br />
        <?endif;?>
        <?/*?>
        <p><?=GetMessage("TAGS_TITLE")?>: <a href="#" class="another">название тега 1</a>, <a href="#" class="another">название тега 2</a>, <a href="#" class="another">название тега 3</a></p>
        <?*/?>                                   
        <hr class="black" />
        <div class="statya_section_name">
            <a class="uppercase another font14" href=<?=$arResult["SECTION_PAGE_URL"]?>><?=$arResult["IBLOCK_TYPE_NAME"]?></a>            
        </div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "content_article_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
    <div class="right-side" style="width:240px">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        <br />
        <?if ($USER->IsAuthorized()):?>
            <!--<div class="figure_border">
                <div class="top"></div>
                <div class="center">
                    <div id="users_onpage"></div>
                </div>
                <div class="bottom"></div>
            </div>   
            <br /><br />-->
        <?endif;?>
        <?if (($arResult["MORE_ARTICLES"]>0&&$arResult["IBLOCK_TYPE_ID"]=="afisha")||$arResult["IBLOCK_TYPE_ID"]!="afisha"):?>
            <div class="title">Читайте также</div>
            <?
            global $arrFil;
            $arrFil = Array("!ID"=>$arResult["ID"]);        
            if ($arResult["IBLOCK_TYPE_ID"]=="afisha")
                $arrFil[">=ACTIVE_FROM"] = date("d.m.Y");        
            //else
              //      $arrFil[">=ACTIVE_FROM"] = date("d.m.Y", strtotime("-12 week"));
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => $arResult["IBLOCK_TYPE_ID"],
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "NEWS_COUNT" => 3,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "shows",
                        "SORT_ORDER2" => "DESC",
                        "FILTER_NAME" => "arrFil",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("ratio","reviews_bind"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );
            ?>
        <?endif;?>       
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br /><Br />
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
    </div>
    <div class="clear"></div>  
    <br /><br />
    <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
    <br /><Br />
    <?$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner",
        "",
        Array(
                "TYPE" => "bottom_rest_list",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
        ),
        false
    );?>
    <br /><br />
</div>
<?if ($USER->IsAdmin())
{
    v_dump($arResult["IBLOCK"]["IBLOCK_TYPE_ID"]);
    v_dump($_SERVER["HTTP_REFERER"]);
}?>