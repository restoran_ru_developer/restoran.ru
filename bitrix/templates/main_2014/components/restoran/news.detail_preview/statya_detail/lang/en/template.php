<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["NEXT_POST"] = "Previous post";
$MESS["PREV_POST"] = "Next post";
$MESS["CUISINE"] = "Kitchen";
$MESS["AVERAGE_BILL"] = "Average bill";
$MESS["TAGS_TITLE"] = "Tags";
$MESS["R_ADD2FAVORITES"] = "Add to favorites";
$MESS["R_COMMENTS"] = "Comments";
$MESS["SUBSCRIBE"] = "sign up";
$MESS["UNSUBSCRIBE"] = "unsubscribe";
$MESS["MONTHS_FULL"] = "January, February, March, April, May, June, July, August, September, October, November, December";
$MESS["WEEK_DAYS_FULL"] = "Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday";
$MESS["R_VK"] = "Vkontacte";
$MESS["R_FACEBOOK"] = "Facebook";
$MESS["R_PORTION1"] = "serving";
$MESS["R_PORTION2"] = "servings";
$MESS["R_PORTION3"] = "servings";
$MESS["CT_RECIPE"] = "Recipe";
?>