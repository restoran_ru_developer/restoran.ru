<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<script>
$(function(){
    console.log('call booking');
    var datePicker = $('.datepicker').datepicker({
        format:'dd.mm.yyyy',  
        weekStart:1
    }).on('changeDate', function(ev){
        //$('.datepicker').datepicker('hide');

        if($('.today-tomorrow-select option[value="'+ev.currentTarget.value+'"]').length>0){
            $('.today-tomorrow-select option[value="'+ev.currentTarget.value+'"]').attr('selected',true);
        }
        else {
            $('.in-select-for-date').text(ev.currentTarget.value).attr('selected',true).show();
            $('.in-select-for-date').val(ev.currentTarget.value);
        }

    });

    <?if(substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
    $('#date').on('change',function(){
        if($('.today-tomorrow-select option[value="'+$(this).val()+'"]').length>0){
            $('.today-tomorrow-select option[value="'+$(this).val()+'"]').attr('selected',true);
        }
        else {
            $('.in-select-for-date').text($(this).val()).attr('selected',true).show();
            $('.in-select-for-date').val($(this).val());
        }
    })
    <?endif?>

    $('.today-tomorrow-select').on('change',function(event){
        this_selected_opt = $(this).find('option:selected');
        $('#date:not(.news-content-form-field)').val(this_selected_opt[0].value);
        $('#date:not(.news-content-form-field)').attr('data-date',this_selected_opt[0].value);
        <?if(!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
            $('.datepicker').datepicker('update', this_selected_opt[0].value);
        <?endif?>
        $('.in-select-for-date').hide();
    })

    $('.wishes-list-wrapper li').on('click touchstart', function(){
        $('.wishes-input').val($(this).text());
        $('.wishes-list-wrapper').removeClass('active');
        $('.wishes-input').focus();
    });

    $('.wish-list-new-trigger').hover(
        function(){
            $('.wishes-list-wrapper').addClass('active');
        },
        function(){
            $('.wishes-list-wrapper').removeClass('active');
        }
    )
    $('.wishes-list-wrapper').hover(
        function(){
            $('.wishes-list-wrapper').addClass('active');
        },
        function(){
            $('.wishes-list-wrapper').removeClass('active');
        }
    )

       //    $(".datew").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});                                                                                                                   
    $(".phone1").maski("(999) 999-99-99",{placeholder:""});
    $(".up").click(function(){
       $("#company:not(.news-content-form-field)").val($("#company:not(.news-content-form-field)").val()*1+1);
    });
    $(".down").click(function(){
        if ($("#company:not(.news-content-form-field)").val()*1>1)
            $("#company:not(.news-content-form-field)").val($("#company:not(.news-content-form-field)").val()*1-1);
    });


    $("#order_online:not(.news-content-form) form").submit(function(){

        this_form = $(this);
        var err = "";
        if (!this_form.find("#top_search_rest").val())
        {
            err = "<?=GetMessage('choose_rest_js')?><br />";
        }
        if (!this_form.find("#time").val())
        {
            err = err+"<?=GetMessage('choose_time_js')?><br/>";
        }

        if (!this_form.find("#company").val()||this_form.find("#company").val().replace(/\D/g,'')==0)
        {
            err = err+"<?=GetMessage('choose_people_num_js')?><br/>";
        }
        expr = /(.+) (.+)/;
        if (!this_form.find("#name").val()||!expr.test(this_form.find("#name").val()))
        {
            err = err+"<?=GetMessage('sign_name_js')?><br/>";
        }

        if (!this_form.find(".phone1").val())
        {
            err = err+"<?=GetMessage('sign_phone_js')?>";
        }
        if (err)
        {
            this_form.find(".errors").html(err+"<br /><Br />");
            return false;
        }
        
        $.ajax({
            type: "POST",
            url:$(this).attr("action"),
            data: $(this).serialize()
            
        }).done(function(data){
            var a =$(data).find(".ok").html();
            if (a)
            {
                this_form.after("<h3 class='text-center'>"+a+"</h3>");
                this_form.remove();

                <?if($APPLICATION->GetCurPage()=='/2gis-widget-form.php'&&!$USER->IsAdmin()){    //  2gis отправочка?>
                    $.getScript("/bitrix/templates/main_2014/js/2gis-success.js", function(){
                        console.log("Running 2gis script");
                    });
                <?}?>
                yaCounter17073367.reachGoal('BRON');
                $('.call-me-back-wrapper').addClass('active');

            }
            else
            {
                alert("<?=GetMessage('error_happened_js')?>")
            }
        });
        return false;
    });
});     
</script>
<? 
//if ($arResult["isFormNote"] != "Y") {     
?><?/*=bitrix_sessid();*/?>
<?$datehash = date("Ymd-His");?>
<?
if ($_REQUEST["name"] )
    $_REQUEST["name"] = rawurldecode($_REQUEST["name"]);
if (!$_REQUEST["person"])
    $_REQUEST["person"] = 0;
//if ($_REQUEST["banket"]=="Y")
//    $_REQUEST["person"] = 10;
if (!$_REQUEST["time"])
    $_REQUEST["time"] = date("H   i");
?>
<link href="<?= $templateFolder ?>/style.css?<?=md5($datehash)?>"  type="text/css" rel="stylesheet" />
<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <div id="order_online"  class="">
        <?//if($USER->IsAdmin()):?>
        <script>
            $(function(){
                <?if(CITY_ID=='rga'||CITY_ID=='urm'):?>
                $("#call-me-back-input").maski("+9999999999?9999999999",{placeholder:""});
                <?else:?>
                $("#call-me-back-input").maski("+7(999) 999-99-99",{placeholder:""});
                <?endif?>
                $('#booking_form').on('hide.bs.modal', function (e) {
                    console.log('hide.bs.modal');
                    if(!$('.call-me-back-wrapper').hasClass('active')&&!$(e.target).hasClass('datepicker')){
                        e.preventDefault();
                        console.log('form closed');
                        $('.call-me-back-wrapper').addClass('active').fadeIn(1000);
                        $('.form-full-content:not(.no-need-to-hide)').hide();
                    }
                })

                $("#call_me_submit").on('click',function(){
                    if($('#call-me-back-input').val()!=''){
                        $.ajax({
                            type: "POST",
                            url:$(this).attr("action"),
                            data: 'call_me_back_city_id='+$('input[name="call_me_back_city_id"]').val()+'&call_me_back_phone='+$('input[name="call_me_back_phone"]').val()

                        }).done(function(data){
                            $('.success-callback-message-wrapper').show();
                            $('.call-me-back-form-wrapper').hide();
                        });
                    }
                    else {
                        alert('<?=GetMessage('CALLBACK_enter_phone_num')?>');
                    }

                    return false;
                });
            })
        </script>
        <?
        if(CITY_ID=='spb'){
        $phone_to_call = '88127401820';
        $phone_to_front = '+7 (812) 740-18-20';
        }
        elseif(CITY_ID=='rga'||CITY_ID=='urm'){
        $phone_to_call = '37166103106';
        $phone_to_front = '+371 661-031-06';
        }
        else {
        $phone_to_call = '84959882656';
        $phone_to_front = '+7 (495) 988-26-56';
        }
        ?>
        <div class="call-me-back-wrapper" style="display: none">
            <div class="success-callback-message-wrapper" style="display: none">
                <h3 class="text-center"><?=GetMessage('CALLBACK_thanks_text')?></h3>
            </div>
            <div class="call-me-back-form-wrapper">
                <div class="call-me-back-title"><?=GetMessage('CALLBACK_no_want_fill')?></div>
                <div class="call-me-back-description"><?=GetMessage('CALLBACK_we_will_make')?></div>
                <a href="tel:<?=$phone_to_call?>">
                    <div class="call-me-back-phone-button-wrapper">
                        <div class="text-phone-to-call">
                            <?=GetMessage('CALLBACK_bron_service')?><br>
                            <span><?=$phone_to_front?></span>
                        </div>
                    </div>
                </a>
                <div class="call-me-back-phone-field-wrapper">
                    <div class="text-phone-to-call">
                        <?=GetMessage('CALLBACK_our_phone_text')?>
                    </div>
                    <div class="call-back-field-wrap">
                        <input type="hidden" name="call_me_back_city_id" value="<?=CITY_ID?>"/>
                        <input type="text" value="" placeholder="<?if(CITY_ID=='rga'||CITY_ID=='urm'):?>+___ ___-___-__<?else:?>+7(___) ___-__-__<?endif?>" id="call-me-back-input" class="form-control" name="call_me_back_phone"/>
                        <input type="submit" id="call_me_submit" class="btn btn-info" value="<?=GetMessage('CALLBACK_call_me')?>" action="/tpl/ajax/call_me_back_handler.php"/>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-full-content">
            <?//endif?>
<? endif; ?>

            <?if($arParams['SHOW_THIS_FORM_TITLE']!='N'):?>
            <div class="title"><?=preg_match('/\/articles\//',$_SERVER['HTTP_REFERER'])?GetMessage("RESERVE_TABLE_SPEC"):GetMessage("RESERVE_TABLE")?></div>
            <div class="online-and-by-phone-str">
                <?=GetMessage('online-or-by-phone');?>
                <br>
                <span class="phone"><?=$phone_to_front?></span>
            </div>
            <?endif;?>
                <?= $arResult["FORM_HEADER"] ?>
            <div class="errors"></div>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="left_column">
                        <?foreach ($arResult["QUESTIONS"] as $key => $question):
                        $ar = array();
                        $ar["CODE"] = $key;
                        $questions[] = array_merge($question, $ar);
                        endforeach;?>
                                <?//if (!$_REQUEST["name"]&&!$_REQUEST["form_text_32"]):?>
                        <div class="form-group">
                            <?
                            $APPLICATION->IncludeComponent(
                            "bitrix:search.title", "bron_2014", Array(
                            "NAME" => "form_" . $questions[9]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[9]["STRUCTURE"][0]["ID"]
                            ), false
                            );
                            ?>
                        </div>
                        <? if (LANGUAGE_ID == 'ru'): ?>
                            <?
                            $s = $arResult["arrVALUES"]["form_dropdown_SIMPLE_QUESTION_547"];
                            if (!$s)
                            $s = $_REQUEST["form_dropdown_SIMPLE_QUESTION_547"];
                            ?>
                            <?if (!$s):?>
                                <input type="hidden" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29">
                            <?else:?>
                                <input type="hidden" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="<?=$s?>">
                            <?endif;?>
                        <? endif; ?>
                                <? if (LANGUAGE_ID == 'en'): ?>
                        <input type="hidden" id="what_question_" name="form_<?= $questions[0]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[0]["CODE"] ?>" value="29" />
                        <? endif; ?>
                        <div class="form-group pull-left today-tomorrow-wrapper">
                            <label for="date"><?=GetMessage("NA")?></label>
                            <?
                            if (!$_REQUEST["date"]){
                            $_REQUEST["date"] = substr_count($_SERVER["HTTP_USER_AGENT"],"iPad") ? date("Y-m-d") : date("d.m.Y");
                            }
                            ?>
                            <input <? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'type="text"'; ?> <? if (substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'readonly="true" type="date" '; ?>
                                class="<? if (!substr_count($_SERVER["HTTP_USER_AGENT"], "iPad"))echo 'datepicker'; ?> form-control"
                                id="date"
                                data-date="<?= substr_count($_SERVER["HTTP_USER_AGENT"], "iPad") ? date("Y-m-d") : date("d.m.Y") ?>"
                                value="<? echo ($_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[2]["STRUCTURE"][0]["ID"]] : $_REQUEST["date"] ?>"
                                name="form_<?= $questions[2]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[2]["STRUCTURE"][0]["ID"] ?>"/>

                            <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/tpl/include_areas/order-form-today-tomorrow-floating-form.php",
                            "EDIT_TEMPLATE" => ""
                            ),
                            false
                            ); ?>

                            <!--<div class="today-tomorrow-trigger"></div>-->


                        </div>
                        <div class="form-group pull-right" style="position:relative">
                            <label for="company"><?=GetMessage("OUR_COMPANY")?></label>

                            <?if(!substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")):?>
                            <div class="up-down"><a href="javascript:void(0)" class="up"></a><a href="javascript:void(0)" class="down"></a></div>
                            <input type="text" class="form-control" id="company"
                                   value="<?= ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"] ?>"
                                   size="4" maxlength="3"
                                   name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>"/>
                            <?else:?>
                            <select class="form-control" onchange="//$('#company').val($(this).val());" id="company" name="form_<?= $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[4]["STRUCTURE"][0]["ID"] ?>">
                                <?$selected_val = ($_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[4]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[4]["STRUCTURE"][0]["ID"]] : $_REQUEST["person"]?>
                                        <?for($i=0;$i<21;$i++){?>
                                <option value="<?=$i?>" <?if($selected_val==$i)echo 'selected';?>><?=$i?></option>
                                <?}?>
                            </select>
                            <?endif?>

                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group" style="margin-top:7px;">
                            <input type="text" class="form-control wishes-input" placeholder="<?= GetMessage("MY_WISH") ?>" name="form_<?= $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[11]["STRUCTURE"][0]["ID"] ?>" <?= trim($_REQUEST["form_" . $questions[11]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[11]["STRUCTURE"][0]["ID"]]) ?>/>

                            <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/tpl/include_areas/order-form-wishes-list.php",
                            "EDIT_TEMPLATE" => ""
                            ),
                            false
                            ); ?>

                        </div>
                        <div class="form-group pull-left end">
                            <label for="name"><?=(LANGUAGE_ID=="en")?"Name":"Имя"?></label>
                            <input type="text" placeholder="<?=(LANGUAGE_ID=="en")?"Name":"Имя и фамилия"?>" class="form-control" id="name" value="<?= ($_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[6]["STRUCTURE"][0]["ID"]] : $USER->GetFullName() ?>" name="form_<?= $questions[6]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[6]["STRUCTURE"][0]["ID"] ?>" />
                        </div>
                        <div class="form-group pull-right end">
                            <label for="phone"><?=GetMessage("MY_PHONE")?></label>  <br />
                            <?
                            $rsUser = CUser::GetByID($USER->GetID());
                            $ar = $rsUser->Fetch();
                            $ar["PERSONAL_PHONE"] = str_replace(" ", "", $ar["PERSONAL_PHONE"]);
                            $ar["PERSONAL_PHONE"] = str_replace("-", "", $ar["PERSONAL_PHONE"]);
                            $ar["PERSONAL_PHONE"] = str_replace("(", "", $ar["PERSONAL_PHONE"]);
                            $ar["PERSONAL_PHONE"] = str_replace(")", "", $ar["PERSONAL_PHONE"]);
                            $ar["PERSONAL_PHONE"] = str_replace("+7", "", $ar["PERSONAL_PHONE"]);
                            $phone = $ar["PERSONAL_PHONE"];
                            $ar["PERSONAL_PHONE"] = substr($phone, 0, 3) . " " . substr($phone, 3, 3) . "  " . substr($phone, 6, 2) . "  " . substr($phone, 8, 2);
                            ?>
                            <div class="pull-left phone-code-wrapper no-checkbox" >+7</div>
                            <div class="pull-right">
                                <input type="text" class="phone1 form-control" placeholder="(___) ___-__-__" style="text-align:left" value="<?= ($_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[7]["STRUCTURE"][0]["ID"]] : $ar["PERSONAL_PHONE"] ?>" name="form_<?= $questions[7]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[7]["STRUCTURE"][0]["ID"] ?>" />
                            </div>
                            <input type="hidden" class="form-control" id="email" value="<?= ($_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]]) ? $_REQUEST["form_" . $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $questions[8]["STRUCTURE"][0]["ID"]] : $USER->GetEmail() ?>"
                                   name="form_<?= $questions[8]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[8]["STRUCTURE"][0]["ID"] ?>" />

                        </div>
                        <div class="clearfix"></div>
                    </td>
                    <td class="right_column">
                        <div class="times">
                            <div class="more2">
                                <?
                                for ($i=10;$i<=24; $i++):
                                    if ($i>=24)
                                        $ii="0".($i-24);
                                    else
                                        $ii = $i;
                                ?>
                                    <div class="time <?=($i>=22)?"end":""?> <?=($arResult["arrVALUES"]["form_text_34"]==$ii.":00")?"active":""?>"><?=$ii?>:00</div>
                                    <?if ($i==27):?>
                                        </div>
                                        <div class="moretime" onclick="$('.more2').toggle()">…</div>
                                        <div class="more2" style="display:none">
                                    <?endif;?>
                                    <div class="time <?=($i>=22)?"end":""?>  <?=($arResult["arrVALUES"]["form_text_34"]==$ii.":30")?"active":""?>"><?=$ii?>:30</div>
                                <?endfor;?>
                            </div>
                            <input type="hidden" name="form_<?= $questions[3]["STRUCTURE"][0]["FIELD_TYPE"] ?>_<?= $questions[3]["STRUCTURE"][0]["ID"] ?>" id="time" value="<?=$arResult["arrVALUES"]["form_text_34"]?>" class=""/>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="form-group">
                <input style="width:0px;height:0px;padding:0px;margin:0px;" class="btn btn-info btn-nb" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" value="<?=($bbb=="b")?GetMessage("RESERVE_BANKET"):GetMessage("RESERVE_TABLE")?>" />
                <a class="btn btn-info btn-nb submit-free-order-form-trigger" onclick="$('#order_online:not(.news-content-form) form').submit()"><?=($bbb=="b")?GetMessage("RESERVE"):GetMessage("RESERVE")?></a>
            </div>
            <? /* if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
              <?if ($arResult["isFormTitle"]):?>
              <div class="title" style="text-align: right; border:0px"><?=$arResult["FORM_TITLE"]?></div>
              <?endif;?>
              <?endif; */ ?>
            <div class="ok"><?if ($arResult["isFormNote"] == "Y") {?><?= $arResult["FORM_NOTE"] ?>
            <?}?></div>
            <div class="font12" id="err">
                <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
            </div>
            <script>
                $(function(){
                    <?if ($_REQUEST["banket"]=="Y"):?>
                    if (!$("#top_search_rest").val())
                        $("#top_search_rest").val(1);
                    <?endif;?>
                    $("#top_search_input1").blur(function(){
                        if ($("#top_search_adr").val()&&$("#top_search_name").val())
                        {
                            $("#chek .rest_name").html($("#top_search_name").val());
                            $("#chek .rest_adress").html($("#top_search_adr").val()+"<hr />");
                        }
                    });
                    $(".time").click(function(){
                        $(".time").removeClass("active");
                        $(this).addClass("active");
                        $("#time:not(.news-content-form-field)").val($(this).text());
                        //$("#chek .time2").html( $("#date-picker").val()+"<br /><?//=GetMessage("CHEK_TIME")?>: "+$("#time:not(.news-content-form-field)").val()+"<br /><?//=GetMessage("CHEK_PERSONS")?>: "+$(".updowninp").val());
                    });
                });
            </script>
            <? if ($arResult["isUseCaptcha"] == "Y"): ?>
            <div class="question">
                <?= GetMessage("CAPTCHA") ?>
            </div>
            <div>
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>" width="180" height="40" align="left" />
                <? //=GetMessage("FORM_CAPTCHA_FIELD_TITLE")   ?><? //=$arResult["REQUIRED_SIGN"];   ?>
                <input type="text" name="captcha_word" size="15" maxlength="7" value="" class="text" style="height:32px" />
            </div>
            <? endif; ?>

                <div class="inv_cph">
                    <input type="checkbox" name="lst_nm" value="1" />
                    <input type="checkbox" name="scd_nm" value="1" checked="checked" />
                </div>
            <? if ($_REQUEST["invite"]): ?>
            <input type="hidden" id="invite" name="invite" value="<?= $_REQUEST["invite"] ?>" />
            <? endif; ?>
            <? if ($_REQUEST["CITY"]): ?>
            <input type="hidden" name="CITY" value="<?= $_REQUEST["CITY"] ?>" />
            <? else: ?>
            <input type="hidden" name="CITY" value="<?= $APPLICATION->get_cookie("CITY_ID"); ?>" />
            <? endif; ?>
        <input type="hidden" name="web_form_apply" value="Y" />
            <div class="clear"></div>

            <!--div class="left">
                <p class="another_color font12" style="line-height:24px;">* — <?= GetMessage("FORM_REQUIRED_FIELDS") ?></p>
            </div-->



            <div class="clear"></div>

            <?
            //} //endif (isFormNote)
            ?>
            <?= $arResult["FORM_FOOTER"] ?>

<? if (!$_REQUEST["web_form_apply"] && !$_REQUEST["RESULT_ID"]): ?>
    <?//if($USER->IsAdmin()):?>
        </div><!--.form-full-content-->
        <?//endif?>
    </div>

<? endif; ?>