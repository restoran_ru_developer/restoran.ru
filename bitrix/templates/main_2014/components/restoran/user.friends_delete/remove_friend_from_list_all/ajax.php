<?
define("STOP_STATISTICS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent(
    'restoran:user.friends_delete',
    'remove_friend_from_list_all',
    array(
        "AJAX_CALL" => "Y",
        "FIRST_USER_ID" => intval($_REQUEST["FIRST_USER_ID"]),
        "SECOND_USER_ID" => intval($_REQUEST["SECOND_USER_ID"]),
        "RESULT_CONTAINER_ID" => $_REQUEST["RESULT_CONTAINER_ID"],
        "ACTION" => "delete_friend",
    ),
    null,
    array('HIDE_ICONS' => 'Y'));

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");
?>