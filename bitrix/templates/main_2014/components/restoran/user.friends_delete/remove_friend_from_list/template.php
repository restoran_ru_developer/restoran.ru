<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["AJAX_CALL"] != "Y"):?>

    <a onclick="if(confirm('<?=GetMessage("USER_DELETE_QUEST")?>')) deleteUser(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" href="javascript:void(null);"><?=GetMessage("USER_DELETE_FRIEND_BUTTON")?></a>

<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["REL_DEL"]):?>

    <?$APPLICATION->IncludeComponent(
        "restoran:user.friends_add",
        "add_friend_from_list",
        Array(
            "FIRST_USER_ID" => $arParams["FIRST_USER_ID"],
            "SECOND_USER_ID" => $arParams["SECOND_USER_ID"],
            "RESULT_CONTAINER_ID" => "friend_result_".$arParams["SECOND_USER_ID"]
        ),
        false
    );?>

<?endif?>