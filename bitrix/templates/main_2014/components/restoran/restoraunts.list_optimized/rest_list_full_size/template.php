<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (count($arResult["ITEMS"])>0):?>
    <?
    if($_REQUEST["page"] || $_REQUEST["PAGEN_1"] || $_REQUEST["letter"]):?>
        <?if($arParams['AJAX_REQUEST']!='Y'):?>
        <div class="priority-list">
            <div class="hr"></div>
        <?endif;?>
            <?//    FIRST 20
            if(!$_REQUEST["letter"]&&($_REQUEST["page"]==1||$_REQUEST["PAGEN_1"]==1)&&$arParams['BEST_SECTION_ID']):
                global $arrFilterSpecStr;
                $arIB = getArIblock("first_20", $_REQUEST["CITY_ID"]);
                if($arParams['BEST_SECTION_ID']===true){ //  index catalog page
                    $arParams['BEST_SECTION_ID'] = false;
                }
                $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y",'SECTION_ID'=>$arParams['BEST_SECTION_ID']);
                $res = CIBlockElement::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), $arFilter, false, Array("nTopCount"=>20), array('PROPERTY_RESTAURANT'));
                while($ob = $res->Fetch())
                {
                    $arrFilterSpecStr['ID'][] = $ob['PROPERTY_RESTAURANT_VALUE'];
                    $BestSortArr[] = $ob['PROPERTY_RESTAURANT_VALUE'];
                }

                $_REQUEST["CUSTOM_PAGEN_01"] = '';
                $arrFilterSpecStr["!PROPERTY_sleeping_rest_VALUE"] = 'Да';

                $APPLICATION->IncludeComponent("restoran:restoraunts.list",
                    "rest_spec_places_new",
                    Array(
                        "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                        "IBLOCK_ID" => CITY_ID,	// Код информационного блока
                        "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                        "NEWS_COUNT" => "20",	// Количество ресторанов на странице
                        "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
                        "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
                        "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
                        "FILTER_NAME" => "arrFilterSpecStr",	// Фильтр
                        "PROPERTY_CODE" => $arParams['PROPERTY_CODE'],
                        "CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                        "AJAX_MODE" => "N",	// Включить режим AJAX
                        "AJAX_OPTION_SHADOW" => "Y",
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "CACHE_TYPE" => $arParams['CACHE_TYPE'],	// Тип кеширования
                        "CACHE_TIME" => $arParams['CACHE_TIME'],	// Время кеширования (сек.)
                        "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "N",	// Учитывать права доступа
                        "CACHE_NOTES" => '',
                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                        "DISPLAY_BOTTOM_PAGER" => $_REQUEST['PROPERTY']&&!$_REQUEST['arrFilter_pf']?"N":'Y',	// Выводить под списком
                        "PAGER_TITLE" => "Рестораны",	// Название категорий
                        "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                        "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                        "CONTEXT"=>$_REQUEST["CONTEXT"],
                        'BEST_SORT_ARRAY' => $BestSortArr,
                        "NO_ADS_SIGN"=>'Y',
                    ),
                    false
                );
                ?>
            <?else:?>
                <?

                // SET PREV PAGE NUM IF FIRST_20 WAS SET

                if($arParams['BEST_SECTION_ID']){
                    $_REQUEST["CUSTOM_PAGEN_01"]=$_REQUEST["page"]-1;

                    global $arrFilter;
                    $APPLICATION->IncludeComponent("restoran:restoraunts.list",
                        "rest_spec_places_new",
                        Array(
                            "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                            "IBLOCK_ID" => CITY_ID,	// Код информационного блока
                            "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                            "NEWS_COUNT" => "20",	// Количество ресторанов на странице
                            "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
                            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
                            "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
                            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
                            "FILTER_NAME" => "arrFilter",	// Фильтр
                            "PROPERTY_CODE" => $arParams['PROPERTY_CODE'],
                            "CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "AJAX_OPTION_SHADOW" => "Y",
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                            "CACHE_TYPE" => $arParams['CACHE_TYPE'],	// Тип кеширования
                            "CACHE_TIME" => $arParams['CACHE_TIME'],	// Время кеширования (сек.)
                            "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "N",	// Учитывать права доступа
                            "CACHE_NOTES" => '',
                            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                            "DISPLAY_BOTTOM_PAGER" => $_REQUEST['PROPERTY']&&!$_REQUEST['arrFilter_pf']?"N":'Y',	// Выводить под списком
                            "PAGER_TITLE" => "Рестораны",	// Название категорий
                            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                            "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                            "DISPLAY_NAME" => "Y",	// Выводить название элемента
                            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                            "CONTEXT"=>$_REQUEST["CONTEXT"],
                            "NO_ADS_SIGN"=>'Y',
                        ),
                        false
                    );
                }
                else {?>
                    <?
                    $obParser = new CTextParser;    //  ниже обрезаем адрес
                    foreach($arResult["ITEMS"] as $cell=>$arItem):?>
                        <?if($arItem['ID']==2148589){
                            $arItem["DETAIL_PAGE_URL"] = 'http://terrine.ru/';
                        }
                        if($arItem['ID']==2255173){
                            $arItem["DETAIL_PAGE_URL"] = 'http://nabatpalace.ru/zal/';
                        }
                        ?>
                        <div class="item">
                            <div class="pic pull-left" lat="<?=$arItem['PROPERTIES']['lat']['VALUE'][0]?>" lon="<?=$arItem['PROPERTIES']['lon']['VALUE'][0]?>" this-rest-id="<?=$arItem['ID']?>">
                                <div class="distance-place"><span></span> КМ</div>
                                <div id="carousel-priority_id<?=$arItem["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval="false">
                                    <div class="carousel-inner">
                                        <?foreach($arItem["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photo):?>
                                            <div class="item<?if($pKey == 0):?> active<?endif?>">
                                                <?if ($pKey>2):?>
                                                    <a target="_blank" href="<?if($arItem['ID']==385873&&1==2){echo'http://www.praz-d-nik.com/';}
                                                    else{echo $arItem["DETAIL_PAGE_URL"];}?>"
                                                        <?if($arItem['ID']==2148589||$arItem['ID']==2255173){echo 'target="_blank"';}?>><img data-url="<?=$photo["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                                                <?else:?>
                                                    <a target="_blank" href="<?if($arItem['ID']==385873&&1==2){echo'http://www.praz-d-nik.com/';}
                                                    else{echo $arItem["DETAIL_PAGE_URL"];}?>"
                                                        <?if($arItem['ID']==2148589||$arItem['ID']==2255173){echo 'target="_blank"';}?>><img src="<?=$photo["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                                                <?endif;?>
                                            </div>
                                        <?endforeach?>
                                    </div>
                                    <?if(count($arItem["PROPERTIES"]["photos"]["VALUE"])>1):?>
                                    <a class="prevel carousel-control icon-arrow-left2" href="#carousel-priority_id<?=$arItem["ID"]?>" role="button" data-slide="prev"></a>
                                    <a class="nextel carousel-control icon-arrow-right2" href="#carousel-priority_id<?=$arItem["ID"]?>" role="button" data-slide="next"></a>
                                    <?endif?>
                                </div>
                            </div>
                            <div class="names pull-left">
                                <div class="type">
                                    <?if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                                        <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?>
                                    <?else:?>
                                        <?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
                                    <?endif;?>
                                </div>
                                <h2><a target="_blank" href="<?if($arItem['ID']==385873&&1==2){echo'http://www.praz-d-nik.com/';}
                                    else{echo $arItem["DETAIL_PAGE_URL"];}?>"
                                        <?if($arItem['ID']==2148589||$arItem['ID']==2255173){echo 'target="_blank"';}?>><?=$arItem["NAME"]?></a></h2>
                                <div class="name_ratio">
                                    <?for($i = 1; $i <= 5; $i++):?>
                                        <span class="icon-star<?if($i > $arItem["DISPLAY_PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
                                    <?endfor?>
                                </div>
                                <div class="order-buttons">
                                    <?if (CITY_ID=="msk"||CITY_ID=="spb"||CITY_ID=="rga"):?>
                                        <?if(CITY_ID=="rga"):?>
                                            <?if($_REQUEST["CATALOG_ID"]!="banket"):?>
                                                <a href="/tpl/ajax/online_order_rest_rgaurm.php"
                                                   class="booking btn btn-info btn-nb" data-id='<?= $arItem["ID"] ?>'
                                                   data-restoran='<?= $arItem["NAME"] ?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                                            <?else:?>
                                                <a href="/tpl/ajax/online_order_rest_rgaurm.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
                                            <?endif?>
                                        <?else:?>
                                            <?if($_REQUEST["CATALOG_ID"]!="banket"):?>
                                                <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                                            <?else:?>
                                                <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
                                            <?endif?>
                                        <?endif?>
                                    <?endif;?>
                                </div>
                                <div class="list-ration-favorite-wrap">
                                    <div class="list-rating-wrap"><span><?=sprintf("%01.1f",$arItem["DISPLAY_PROPERTIES"]["RATIO"]["VALUE"]);?></span></div>
                                    <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
                                </div>
                            </div>
                            <div class="props pull-right">
                                <div class="re"></div>
                                <div class="clearfix"></div>
                                <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                                    <?if ($pid!="type"&&$pid!="photos"&&$pid!="RATIO"&&$pid!="FEATURES_IN_PICS"&&$pid!="STREET"):?>
                                        <?if ($pid!="subway"):?>
                                            <div class="prop">
                                                <div class="name"><?=$pid=='banket_average_bill'&&$_REQUEST["CATALOG_ID"]=="banket"?GetMessage('banket_bill_for_person'):$arProperty["NAME"]?>:</div>
                                                <div class="value">
                                                    <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                                        <?if (CITY_ID=="spb"):?>
                                                            <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]!="banket"?"":"?banket=Y"?>" class="<? echo MOBILE;?> booking">+7(812) 740-18-20</a>
                                                        <?else:?>
                                                            <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]!="banket"?"":"?banket=Y"?>" class="<? echo MOBILE;?> booking">+7(495) 988-26-56</a>
                                                        <?endif;?>
                                                    <?else:?>
                                                        <?if ($pid=="phone"):?>
                                                            <?

                                                            $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                                            if(is_array($arProperty["DISPLAY_VALUE"]))
                                                                $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);
                                                            else{
                                                                $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
                                                                //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                            }

                                                            $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);



                                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                            preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);


                                                            $TELs=array();
                                                            for($p=1;$p<5;$p++){
                                                                foreach($matches[$p] as $key=>$v){
                                                                    $TELs[$key].=$v;
                                                                }
                                                            }

                                                            foreach($TELs as $key => $T){
                                                                if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                                            }
                                                            $old_phone = "";
                                                            foreach($TELs as $key => $T){
                                                                if ($old_phone!=$T):
                                                                    if($key>1)
                                                                        break;
                                                                    ?>
                                                                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php<?=$_REQUEST["CATALOG_ID"]!="banket"?"":"?banket=Y"?>" class="tnum <? echo MOBILE;?> booking">
                                                                    <?=clearUserPhone($arProperty["DISPLAY_VALUE3"][$key])?>
                                                                    </a>
                                                                    <?if(isset($TELs[$key+1]) && $TELs[$key+1]!=""&&($key+1<2)):
                                                                        echo ', ';
                                                                    elseif(isset($TELs[$key+1])&&($key+1>=2)):?>
                                                                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>#props" target="_blank" class="more-props-from-list">...</a>
                                                                    <?endif;?>
                                                                <?endif;
                                                                $old_phone = $T;?>
                                                            <?}?>
                                                        <?else:
                                                            ?>
                                                            <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                                                <?
                                                                if($pid=='opening_hours'){//||$pid=='address'
                                                                    $prop_val = TruncateText($arProperty["DISPLAY_VALUE"], $pid=='opening_hours'?38:45);
                                                                    if(preg_match('/\.\.\./',$prop_val)){
                                                                        echo str_replace('...','<a href="'.$arItem['DETAIL_PAGE_URL'].'#props" target="_blank" class="more-props-from-list">...</a>',$prop_val);
                                                                    }
                                                                    else {
                                                                        echo $prop_val;
                                                                    }
                                                                }
                                                                elseif($pid=='address'){
                                                                    $prop_val = $obParser->html_cut($arProperty["DISPLAY_VALUE"], 45);
                                                                    if(preg_match('/\.\.\./',$prop_val)){
                                                                        echo str_replace('...',' <a href="'.$arItem['DETAIL_PAGE_URL'].'#props" target="_blank" class="more-props-from-list">...</a>',$prop_val);
                                                                    }
                                                                    else {
                                                                        echo $prop_val;
                                                                    }
                                                                }
                                                                else {
                                                                    echo $arProperty["DISPLAY_VALUE"];
                                                                }
                                                                ?>
                                                            <?else:?>
                                                                <?if(LANGUAGE_ID=='en'):?>
                                                                    <?=implode(', ',$arProperty["DISPLAY_VALUE"])?>
                                                                <?else:?>
                                                                    <?=$arProperty["DISPLAY_VALUE"][0]?>
                                                                <?endif;?>
                                                            <?endif?>
                                                        <?endif;?>
                                                    <?endif;?>
                                                </div>
                                            </div>
                                        <?else:?>
                                            <div class="prop wsubway">
                                                <?if($_REQUEST['PROPERTY']!='subway'):?><div class="subway">M</div><?endif?>
                                                <?if($_REQUEST['PROPERTY']=='subway'&&$_REQUEST['PROPERTY_VALUE']){
                                                    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
                                                    if($ar_res = $res->Fetch()){
                                                        $subway_name = $ar_res['NAME'];
                                                    }
                                                }?>
                                                <?if($subway_name):?><div class="name"><?=GetMessage('subway_str_title')?>: </div><?endif?>
                                                <div class="value" <?if($subway_name):?>title="<?=GetMessage('subway_str_title')?> <?=$subway_name?>"<?endif;?>>
                                                    <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                                        <?=$arProperty["DISPLAY_VALUE"]?>
                                                    <?else:?>
                                                        <?if(LANGUAGE_ID=='en'):?>
                                                            <?=implode(', ',$arProperty["DISPLAY_VALUE"])?>
                                                        <?else:?>
                                                            <?=$arProperty["DISPLAY_VALUE"][0]?>
                                                        <?endif;?>
                                                    <?endif?>
                                                </div>
                                            </div>
                                        <?endif;?>
                                    <?endif;?>
                                <?endforeach;?>
                                <?if(LANGUAGE_ID!='en')://$USER->IsAdmin()&&
                                    ?>
                                    <div class="features-in-pic-wrapper list-style">
                                        <?
                                        $i=0;
                                        foreach($arItem['DISPLAY_PROPERTIES']['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'] as $feature_in_pic){
                                            if($i>=6)
                                                break;

//                                            $res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>5235, "ACTIVE"=>"Y", '=PROPERTY_RESTAURANT'=>$arItem['ID'], '=PROPERTY_FEATURES_IN_PICS'=>$feature_in_pic['ID']), false, array("nTopCount"=>1), array("NAME"));
//                                            if($ob = $res->Fetch())
//                                            {
//                                                $feature_in_pic['NAME'] = $ob['NAME'];
//                                            }
                                            $arFile = CFile::GetFileArray($feature_in_pic['PREVIEW_PICTURE']);
                                            ?>
                                            <img src="<?=$arFile['SRC']?>" width="" height="" alt="<?=$feature_in_pic['NAME']?>" title="<?=$feature_in_pic['NAME']?>" data-toggle="tooltip-pic" data-placement="top">
                                            <?$i++;
                                        }?>
                                    </div>
                                <?endif?>
                            </div>
                        </div>
                        <div class="hr"></div>
                    <?endforeach;?>
                <?}
                ?>
            <?endif?>
        <?if($arParams['AJAX_REQUEST']!='Y'):?>
        </div>
        <?endif?>
    <?else: //   BEST_FOR_MAIN_CATALOGS?>
        <div class="priority-list">
            <div class="hr"></div>
            <?

            global $arrFilterSpecStr;
            if($arParams['BEST_SECTION_ID']):
                $arIB = getArIblock("best", $_REQUEST["CITY_ID"]);
                $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y",'SECTION_ID'=>$arParams['BEST_SECTION_ID']);
                $res = CIBlockElement::GetList(array('SORT'=>'ASC','NAME'=>'ASC'), $arFilter, false, Array("nTopCount"=>20), array('PROPERTY_RESTAURANT'));
                while($ob = $res->Fetch())
                {
                    $arrFilterSpecStr['ID'][] = $ob['PROPERTY_RESTAURANT_VALUE'];
                    $BestSortArr[] = $ob['PROPERTY_RESTAURANT_VALUE'];
                }
            FirePHP::getInstance()->info($BestSortArr,'$BestSortArr');
            else://if(!$_REQUEST["PROPERTY"])
                $arrFilterSpecStr = Array(
                    "PROPERTY_s_place_".$_REQUEST["CATALOG_ID"]."_VALUE" => Array("Да")
                );
            endif;
            $arrFilterSpecStr["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $APPLICATION->IncludeComponent("restoran:restoraunts.list",
                "rest_spec_places_new",
                Array(
                    "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => CITY_ID,	// Код информационного блока
                    "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                    "NEWS_COUNT" => "20",	// Количество ресторанов на странице
                    "SORT_BY1" => "SORT",	// Поле для первой сортировки ресторанов
                    "SORT_ORDER1" => "DESC",	// Направление для первой сортировки ресторанов
                    "SORT_BY2" => "",	// Поле для второй сортировки ресторанов
                    "SORT_ORDER2" => "",	// Направление для второй сортировки ресторанов
                    "FILTER_NAME" => "arrFilterSpecStr",	// Фильтр
                    "PROPERTY_CODE" => $arParams['PROPERTY_CODE'],
                    "CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                    "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                    "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                    "CACHE_TYPE" => $arParams['CACHE_TYPE'],	// Тип кеширования
                    "CACHE_TIME" => $arParams['CACHE_TIME'],	// Время кеширования (сек.)
                    "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",	// Учитывать права доступа
                    "CACHE_NOTES" => '',
                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => $_REQUEST['PROPERTY']&&!$_REQUEST['arrFilter_pf']?"N":'Y',	// Выводить под списком
                    "PAGER_TITLE" => "Рестораны",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                    "PAGER_TEMPLATE" => "rest_list",	// Название шаблона
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                    "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                    "CONTEXT"=>$_REQUEST["CONTEXT"],
                    'BEST_SORT_ARRAY' => $BestSortArr,
                ),
                false
            );?>
        </div>
    <?endif?>

    <div class="navigation_temp firsttt">
        <?
        if($arParams["DISPLAY_BOTTOM_PAGER"]&&!$_REQUEST['CUSTOM_PAGEN_01']):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>
<?else:?>
    <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
<?endif;?>