<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                        
<?
$obParser = new CTextParser;    //  ниже обрезаем адрес
foreach($arResult["ITEMS"] as $cell=>$arItem):?>
    <?if($arItem['ID']==2148589){
        $arItem["DETAIL_PAGE_URL"] = 'http://terrine.ru/';
    }
    if($arItem['ID']==2255173){
        $arItem["DETAIL_PAGE_URL"] = 'http://nabatpalace.ru/zal/';
    }
    ?>
    <div class="item">            
        <div class="pic pull-left" lat="<?=$arItem['PROPERTIES']['lat']['VALUE'][0]?>" lon="<?=$arItem['PROPERTIES']['lon']['VALUE'][0]?>" this-rest-id="<?=$arItem['ID']?>">
            <div class="distance-place"><span></span> КМ</div>
            <div id="carousel-priority_id<?=$arItem["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">                        
                    <?foreach($arItem["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photo):?>
                        <div class="item<?if($pKey == 0):?> active<?endif?>">
                            <?if ($pKey>2):?>
                                <a target="_blank" href="<?if($arItem['ID']==385873){echo'http://www.praz-d-nik.com/';}
                                else{echo $arItem["DETAIL_PAGE_URL"];}?>"
                                    <?if($arItem['ID']==385873||$arItem['ID']==2148589||$arItem['ID']==2255173){echo 'target="_blank"';}?>><img data-url="<?=$photo["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                            <?else:?>
                                <a target="_blank" href="<?if($arItem['ID']==385873){echo'http://www.praz-d-nik.com/';}
                                else{echo $arItem["DETAIL_PAGE_URL"];}?>"
                                    <?if($arItem['ID']==385873||$arItem['ID']==2148589||$arItem['ID']==2255173){echo 'target="_blank"';}?>><img src="<?=$photo["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                            <?endif;?>
                        </div>
                    <?endforeach?>                     
                </div>
                <a class="prevel carousel-control icon-arrow-left2" href="#carousel-priority_id<?=$arItem["ID"]?>" role="button" data-slide="prev"></a>
                <a class="nextel carousel-control icon-arrow-right2" href="#carousel-priority_id<?=$arItem["ID"]?>" role="button" data-slide="next"></a>
            </div>            
        </div>
        <div class="names pull-left">
            <div class="type">
                <?if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?>
                <?else:?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
                <?endif;?>
            </div>
            <h2><a target="_blank" href="<?if($arItem['ID']==385873){echo'http://www.praz-d-nik.com/';}
                else{echo $arItem["DETAIL_PAGE_URL"];}?>"
                    <?if($arItem['ID']==385873||$arItem['ID']==2148589||$arItem['ID']==2255173){echo 'target="_blank"';}?>><?=$arItem["NAME"]?></a></h2>
            <div class="name_ratio">
                <?for($i = 1; $i <= 5; $i++):?>
                    <span class="icon-star<?if($i > $arItem["DISPLAY_PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
                <?endfor?>                    
            </div>                                                        
            <div class="order-buttons">
                <?if (CITY_ID=="msk"||CITY_ID=="spb"||CITY_ID=="rga"):?>
                    <?if(CITY_ID=="rga"):?>
                        <?if($_REQUEST["CATALOG_ID"]!="banket"):?>
                            <a href="/tpl/ajax/online_order_rest_rgaurm.php"
                                class="booking btn btn-info btn-nb" data-id='<?= $arItem["ID"] ?>'
                                data-restoran='<?= $arItem["NAME"] ?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                        <?else:?>
                            <a href="/tpl/ajax/online_order_rest_rgaurm.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
                        <?endif?>
                    <?else:?>
                        <?if($_REQUEST["CATALOG_ID"]!="banket"):?>
                            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                        <?else:?>
                            <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
                        <?endif?>
                    <?endif?>
                <?endif;?>
                <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>                        
            </div>
            <div class="list-rating-wrap"><span><?=sprintf("%01.1f",$arItem["DISPLAY_PROPERTIES"]["RATIO"]["VALUE"]);?></span></div>
        </div>   
        <div class="props pull-right">                            
            <div class="re"><?if($_REQUEST["PROPERTY"]!='rest_group'):?><?if($arParams['NO_ADS_SIGN']!='Y'):?>Реклама<?endif?><?endif?></div>
            <div class="clearfix"></div>  
            <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                <?if ($pid!="type"&&$pid!="photos"&&$pid!="RATIO"&&$pid!="FEATURES_IN_PICS"&&$pid!="STREET"):?>
                    <?if ($pid!="subway"):?>
                        <div class="prop">
                            <div class="name"><?=$pid=='banket_average_bill'&&$_REQUEST["CATALOG_ID"]=="banket"?GetMessage('banket_bill_for_person'):$arProperty["NAME"]?>:</div>
                            <div class="value">
                                <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                    <?if (CITY_ID=="spb"):?>
                                            <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]!="banket"?"":"?banket=Y"?>" class="<? echo MOBILE;?> booking">+7(812) 740-18-20</a>
                                    <?else:?>
                                            <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]!="banket"?"":"?banket=Y"?>" class="<? echo MOBILE;?> booking">+7(495) 988-26-56</a>
                                    <?endif;?>                                                    
                                <?else:?>
                                    <?if ($pid=="phone"):?>
                                        <?

                                        $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                        if(is_array($arProperty["DISPLAY_VALUE"]))
                                            $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);
                                        else{
                                            $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
                                            //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                        }

                                        $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);

                                        $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                        preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);

                                        $TELs=array();
                                        for($p=1;$p<5;$p++){
                                            foreach($matches[$p] as $key=>$v){
                                                $TELs[$key].=$v;
                                            }
                                        }

                                        foreach($TELs as $key => $T){
                                            if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                        }
                                        $old_phone = "";
                                        foreach($TELs as $key => $T){
                                            if ($old_phone!=$T):
                                                if($key>1)
                                                    break;
                                                ?>
                                                <a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php<?=$_REQUEST["CATALOG_ID"]!="banket"?"":"?banket=Y"?>" class="tnum <? echo MOBILE;?> booking">
                                                    <?=clearUserPhone($arProperty["DISPLAY_VALUE3"][$key])?>
                                                </a>
                                                <?if(isset($TELs[$key+1]) && $TELs[$key+1]!=""&&($key+1<2)):
                                                echo ', ';
                                            elseif(isset($TELs[$key+1])&&($key+1>=2)):?>
                                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>#props" target="_blank" class="more-props-from-list">...</a>
                                            <?endif;?>
                                            <?endif;
                                            $old_phone = $T;?>
                                        <?}?>
                                    <?else:?>
                                        <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                            <?
                                            if($pid=='opening_hours'){//||$pid=='address'
                                                $prop_val = TruncateText($arProperty["DISPLAY_VALUE"], $pid=='opening_hours'?38:45);
                                                if(preg_match('/\.\.\./',$prop_val)){
                                                    echo str_replace('...','<a href="'.$arItem['DETAIL_PAGE_URL'].'#props" target="_blank" class="more-props-from-list">...</a>',$prop_val);
                                                }
                                                else {
                                                    echo $prop_val;
                                                }
                                            }
                                            elseif($pid=='address'){
                                                $prop_val = $obParser->html_cut($arProperty["DISPLAY_VALUE"], 40);
                                                if(preg_match('/\.\.\./',$prop_val)){
                                                    echo str_replace('...',' <a href="'.$arItem['DETAIL_PAGE_URL'].'#props" target="_blank" class="more-props-from-list">...</a>',$prop_val);
                                                }
                                                else {
                                                    echo $prop_val;
                                                }
                                            }
                                            else {
                                                echo $arProperty["DISPLAY_VALUE"];
                                            }
                                            ?>
                                        <?else:?>
                                            <?if(LANGUAGE_ID=='en'):?>
                                                <?=implode(', ',$arProperty["DISPLAY_VALUE"])?>
                                            <?else:?>
                                                <?=$arProperty["DISPLAY_VALUE"][0]?>
                                            <?endif;?>
                                        <?endif?>
                                    <?endif;?>
                                <?endif;?>
                            </div>
                        </div>
                    <?else:?>
                        <div class="prop wsubway">
                            <?if($_REQUEST['PROPERTY']!='subway'):?><div class="subway">M</div><?endif?>
                            <?if($_REQUEST['PROPERTY']=='subway'&&$_REQUEST['PROPERTY_VALUE']){
                                $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
                                if($ar_res = $res->Fetch()){
                                    $subway_name = $ar_res['NAME'];
                                }
                            }?>
                            <?if($subway_name):?><div class="name"><?=GetMessage('subway_str_title')?>: </div><?endif?>
                            <div class="value" <?if($subway_name):?>title="<?=GetMessage('subway_str_title')?> <?=$subway_name?>"<?endif;?>>
                                <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                    <?=$arProperty["DISPLAY_VALUE"]?>
                                <?else:?>
                                    <?=$arProperty["DISPLAY_VALUE"][0]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif;?>                        
                <?endif;?>
            <?endforeach;?>
            <?if(LANGUAGE_ID!='en'):
                ?>
                <div class="features-in-pic-wrapper list-style">
                    <?
                    $i=0;
                    foreach($arItem['DISPLAY_PROPERTIES']['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'] as $feature_in_pic){
                        if($i>=6)
                            break;
                        $arFile = CFile::GetFileArray($feature_in_pic['PREVIEW_PICTURE']);
                        ?>
                        <img src="<?=$arFile['SRC']?>" width="" height="" alt="<?=$feature_in_pic['NAME']?>" title="<?=$feature_in_pic['NAME']?>" data-toggle="tooltip-pic" data-placement="top">
                        <?$i++;
                    }?>
                </div>
            <?endif?>
        </div>   
    </div>
    <div class="hr"></div>
<?endforeach;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]&&$_REQUEST["CUSTOM_PAGEN_01"]&&$arParams['NO_ADS_SIGN']=='Y'):?>
    <div class="navigation_temp">
        <?=$arResult["NAV_STRING"]?>
    </div>
<?endif;?>