<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//***********************************
//setting section
//***********************************
?>
<script src="/tpl/js/jquery.checkbox.js"></script>
<script>
    function hideInactive(){
        $('.row-ckeckbox').show();
        if($('[name=CITY_ID]').val() == 'tmn'){
            $('.row-blogs').hide();
            $('.row-new_rests').hide();
            $('.row-afisha').hide();
        }
        if($('[name=CITY_ID]').val() == 'kld'){
            $('.row-blogs').hide();
            $('.row-reviews').hide();
            $('.row-photoreports').hide();
            $('.row-afisha').hide();
        }
    }
    
    $(document).ready(function(){
        hideInactive();
    });
</script>
<?
//foreach ($arResult["MESSAGE"] as $itemID => $itemValue)
//    echo ShowMessage(array("MESSAGE" => $itemValue, "TYPE" => "OK"));
//foreach ($arResult["ERROR"] as $itemID => $itemValue)
//    echo ShowMessage(array("MESSAGE" => $itemValue, "TYPE" => "ERROR"));
//выбор нужных рассылок
$allRubrics = array();
foreach ($arResult["GROUP_LIST"] as $key => $value) {
    if ($value[0]["ID"] == 26 || $value[0]["ID"] == 27 || $value[0]["ID"] == 28) {
        $allRubrics[] = $value;
    }
}

$arResult["GROUP_LIST"] = $allRubrics;
$arResult["GROUP_LIST"] = array();
$rsUser = $USER->GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$podpiska = unserialize($arUser["WORK_STREET"]);
$selectedRubrics = array();
foreach ($podpiska["rubriki"] as $key => $value) {
    $selectedRubrics[$value] = $value;
}
foreach ($arResult["RUBRICS"] as $itemID => $itemValue) {
    if (!in_array($itemValue["ID"], array(26, 27, 28))) {
        unset($arResult["RUBRICS"][$itemID]);
    }
};
if(!isset($podpiska["CITY_ID"])){
    $podpiska["CITY_ID"] = CITY_ID;
}
?>
<form action="<?= $arResult["FORM_ACTION"] ?>" method="post">
    <? echo bitrix_sessid_post(); ?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
        <thead><tr><td colspan="2"></td></tr></thead>
        <tr valign="top">
            <td width="40%">
                <p>
                    <? if ($USER->IsAuthorized()): ?>
                        <input type="hidden" name="EMAIL" value="<? echo $arResult["SUBSCRIPTION"]["EMAIL"] != "" ? $arResult["SUBSCRIPTION"]["EMAIL"] : $arResult["REQUEST"]["EMAIL"]; ?>" class="subscription-email" />
                    <? else: ?>
                        <span class="font14">Введите ваш E-Mail: </span><br />
                        <input type="text" name="EMAIL" value="<?=($_REQUEST["email"])?$_REQUEST["email"]:""?>" class="subscription-email inputtext" size="60" /> <Br />
                    <? endif; ?>
                    <? // echo GetMessage("subscr_rub")  ?><!--<span class="starrequired">*</span><br />-->

                    <!--
                    <? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
                                                            <label><input type="checkbox" name="RUB_ID[]" value="<?= $itemValue["ID"] ?>"<? if ($itemValue["CHECKED"]) echo " checked" ?> /><?= $itemValue["NAME"] ?></label><br />
                    <? endforeach; ?>-->

                    <span class="font14">Выберите город: </span>
                    <select id="period_select" class="cusel-select" name="CITY_ID" style="width:250px" onchange="hideInactive();">
                        <option value="msk" <?
                    if ($podpiska["CITY_ID"] == "msk") {
                        echo 'selected';
                    }
                    ?>>Москва</option>
                        <option value="spb" <?
                                if ($podpiska["CITY_ID"] == "spb") {
                                    echo 'selected';
                                }
                    ?>>Санкт-Петербург</option>
                        <option value="kld" <?
                                if ($podpiska["CITY_ID"] == "kld") {
                                    echo 'selected';
                                }
                    ?>>Калининград</option>
                        <option value="tmn" <?
                                if ($podpiska["CITY_ID"] == "tmn") {
                                    echo 'selected';
                                }
                    ?>>Тюмень</option>
                    </select>
                <table>
                    <tbody>
                        <tr class="row-ckeckbox row-new_rests">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                if ($selectedRubrics["new_rests"]) {
                                    echo 'checked';
                                }
                    ?> id="new_rests" value="new_rests">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Новые рестораны</label>
                            </td> 
                        </tr>
                        <tr class="row-ckeckbox row-reviews">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                           if ($selectedRubrics["reviews"]) {
                                               echo 'checked';
                                           }
                    ?> id="reviews" value="reviews">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Обзоры</label>
                            </td> 
                        </tr>
                        <tr class="row-ckeckbox row-news">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                           if ($selectedRubrics["news"]) {
                                               echo 'checked';
                                           }
                    ?> id="news" value="news">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Новости</label>
                            </td> 
                        </tr>
                        <tr class="row-ckeckbox row-receipts">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                           if ($selectedRubrics["receipts"]) {
                                               echo 'checked';
                                           }
                    ?> id="receipts" value="receipts">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Рецепты</label>
                            </td> 
                        </tr>
                        <tr class="row-ckeckbox row-photoreports">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                           if ($selectedRubrics["photoreports"]) {
                                               echo 'checked';
                                           }
                    ?> id="photoreports" value="photoreports">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Фотоотчеты</label>
                            </td> 
                        </tr>
                        <tr class="row-ckeckbox row-blogs">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                           if ($selectedRubrics["blogs"]) {
                                               echo 'checked';
                                           }
                    ?> id="blogs" value="blogs">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Критика</label>
                            </td> 
                        </tr>
                        <tr class="row-ckeckbox row-afisha">
                            <td style="padding: 5px 0px;padding-right:5px">
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input type="checkbox" name="rubriki[]" <?
                                           if ($selectedRubrics["afisha"]) {
                                               echo 'checked';
                                           }
                    ?> id="afisha" value="afisha">
                                </span></td><td class="option2d"><label class="niceCheckLabel">Афиша</label>
                            </td> 
                        </tr>
                    </tbody>  
                </table>
                <div class="clear"></div> 
                <? if ($USER->IsAuthorized()): ?>
                    <hr />
                    <br/>                
                    <table width="700">    

                        <tr>
                            <td>
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input id="where1" type="checkbox" name="where[]" <?= (in_array(1, $podpiska["where"])) ? "checked" : "" ?> value="1" />
                                </span>
                            </td>
                            <td class="option2d">
                                <label class="niceCheckLabel">Хочу получать почтой</label>
                            </td>      
                        </tr>

                        <tr>
                            <td>
                                <span class="niceCheck" style="background-position: 0px 0px;">
                                    <input id="where2" type="checkbox" name="where[]" <?= (in_array(2, $podpiska["where"])) ? "checked" : "" ?> value="2" />
                                </span>
                            </td>
                            <td class="option2d"><label class="niceCheckLabel">Хочу читать в своем личном кабинете<br /><span class="grey"><i>(Выпуски хранятся 3 месяца. Доступно только зарегистрированным пользователям)</i></span></label></td>
                        </tr>                    
                    </table> 
                    <hr />
                    <br/>
                <? else: ?>
                    <input type="hidden" name="where[]" value="1" />
                <? endif; ?>                
                <span class="font14">Выберите периодичность рассылки:</span>
                <br />
                <select id="rubric_select" class="cusel-select" style="z-index: 10;" name="RUBRIC_ID">
                    <option value="26" <?
                if ($podpiska["RUBRIC_ID"] == "26") {
                    echo 'selected';
                }
                ?>>Раз в неделю</option>
                    <option value="27" <?
                            if ($podpiska["RUBRIC_ID"] == "27") {
                                echo 'selected';
                            }
                ?>>Раз в две недели</option>
                    <option value="28" <?
                            if ($podpiska["RUBRIC_ID"] == "28") {
                                echo 'selected';
                            }
                ?>>Раз в месяц</option>
                </select>

                </p>


                <input type="hidden" value="html" name="FORMAT">
            </td>
            <td width="60%">
                <p><? // echo GetMessage("subscr_settings_note1")     ?></p>
                <p><? // echo GetMessage("subscr_settings_note2")     ?></p>
            </td>
        </tr>
        <tfoot><tr><td colspan="2">
                    <input type="submit" class="btn btn-info btn-nb" name="Save" value="<? echo ($arResult["ID"] > 0 ? GetMessage("subscr_upd") : GetMessage("subscr_add")) ?>" />                    
                </td></tr></tfoot>
    </table>
    <input type="hidden" name="PostAction" value="<? echo ($arResult["ID"] > 0 ? "Update" : "Add") ?>" />
    <input type="hidden" name="ID" value="<? echo $arResult["SUBSCRIPTION"]["ID"]; ?>" />
    <input type="hidden" name="action" value="activate" />
    <input type="hidden" name="activate" value="<? echo GetMessage("subscr_activate") ?>" />
    <? if ($_REQUEST["register"] == "YES"): ?>
        <input type="hidden" name="register" value="YES" />
    <? endif; ?>
    <? if ($_REQUEST["authorize"] == "YES"): ?>
        <input type="hidden" name="authorize" value="YES" />
    <? endif; ?>
</form>
<br />
<? if ($arResult["SUBSCRIPTION"]["CONFIRMED"] == "Y"): ?>
    <? if ($arResult["SUBSCRIPTION"]["ACTIVE"] == "Y"): ?>
        <form action="<?= $arResult["FORM_ACTION"] ?>" method="get">
            <input type="submit" class="btn btn-info btn-nb btn-nb-empty" name="unsubscribe" value="<? echo GetMessage("subscr_unsubscr") ?>" />
            <input type="hidden" name="action" value="unsubscribe" />
            <input type="hidden" name="ID" value="<? echo $arResult["SUBSCRIPTION"]["ID"]; ?>" />                                
        </form>
    <? endif; ?>
<? endif; ?>
