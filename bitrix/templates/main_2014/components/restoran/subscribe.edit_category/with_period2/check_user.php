<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
global $USER;
if (isset($_POST["RUBRIC_ID"])) {
    $_REQUEST["RUB_ID"] = array($_POST["RUBRIC_ID"]);
    if ($USER->IsAuthorized()) {
        $rsUser = CUser::GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
    } else {
        $rsUser = CUser::GetByLogin($_POST["EMAIL"]);
        $arUser = $rsUser->Fetch();
        if (!$arUser) {
            $user = new CUser;
            $pass = randString(8);
            $arFields = Array(
                "EMAIL" => $_POST["EMAIL"],
                "LOGIN" => $_POST["EMAIL"],
                "ACTIVE" => "Y",
                "GROUP_ID" => array(3),
                "PASSWORD" => $pass,
                "CONFIRM_PASSWORD" => $pass
            );

            $ID = $user->Add($arFields);            
            $USER->Authorize($ID);
            $_REQUEST["USER_ID"] = $ID;
            $rsUser = CUser::GetByID($ID);
            $arUser = $rsUser->Fetch();
        }
    }

    $resultArray["RUBRIC_ID"] = $_POST["RUBRIC_ID"];
    $resultArray["EMAIL"] = $_POST["EMAIL"];
    $resultArray["CITY_ID"] = $_POST["CITY_ID"];
    $resultArray["rubriki"] = $_POST["rubriki"];

    $podpiska = unserialize($arUser["WORK_STREET"]);
    $USER->update($arUser["ID"], array("WORK_STREET" => serialize($resultArray)));
    $oldRubricId = $podpiska["RUBRIC_ID"];
    $newRubricId = $resultArray["RUBRIC_ID"];    
}
?>
