<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["RESUME_STATUS"] = "Status";
$MESS["RESUME_STATUS_Y"] = "Opened";
$MESS["RESUME_STATUS_N"] = "Closed";
$MESS["RESPOND_TITLE"] = "Responded";
$MESS["RESTORATOR_STATE"] = "Restorator";


$MESS["my_invites_invite_title"] = "Invitation";
$MESS["my_invites_my_guests_title"] = "My guests";
$MESS["my_invites_status_title"] = "Status";
$MESS["my_invites_banqueting_hall_title"] = "banqueting hall";
$MESS["my_invites_restaurant_title"] = "restaurant";
$MESS["my_invites_edit_title"] = "Edit";
$MESS["my_invites_cancel_title"] = "Cancel";
$MESS["my_invites_remove_title"] = "Remove";
$MESS["my_invites_message_title"] = "Message";
$MESS["my_invites_confirmed_title"] = "Confirmed";
$MESS["my_invites_declined_title"] = "Declined";
$MESS["my_invites_pending_title"] = "Pending";
$MESS["my_invites_make_a_reservation_title"] = "make a reservation";
$MESS["my_invites_cant_make_a_reservation_title"] = "You can not place an order, since not all invited decided.";
$MESS["my_invites_follow_booking"] = "Monitor the status of your order, you can see «Reservations». Write in your changes, you can call";
$MESS["my_invites_no_invites_title"] = "No added invitations";
?>