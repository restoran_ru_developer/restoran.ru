<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["RESUME_STATUS"] = "Статус";
$MESS["RESUME_STATUS_Y"] = "Открыта";
$MESS["RESUME_STATUS_N"] = "Закрыта";
$MESS["RESPOND_TITLE"] = "Откликнулись";
$MESS["RESTORATOR_STATE"] = "Ресторатор";



$MESS["my_invites_invite_title"] = "Приглашение";
$MESS["my_invites_my_guests_title"] = "Гости";
$MESS["my_invites_status_title"] = "Статус";
$MESS["my_invites_banqueting_hall_title"] = "банкетный зал";
$MESS["my_invites_restaurant_title"] = "ресторан";

$MESS["my_invites_remove_title"] = "Удалить";
$MESS["my_invites_message_title"] = "Сообщение";
$MESS["my_invites_confirmed_title"] = "Подтвердил";
$MESS["my_invites_pending_title"] = "В ожидании";
$MESS["my_invites_no_invites_title"] = "Нет добавленных приглашений";
$MESS["my_invites_reject_title"] = "Отклонить";
$MESS["my_invites_guests_came_title"] = "гости пришли";
$MESS["my_invites_event_took_place_title"] = "Событие состоялось";
$MESS["my_invites_guests_cancel_the_order_title"] = "гости отменили заказ";
$MESS["my_invites_reservation_is_canceled_by_the_user_title"] = "Бронирование отменено пользователем";
$MESS["my_invites_table_booked_title"] = "Столик забронирован";

?>