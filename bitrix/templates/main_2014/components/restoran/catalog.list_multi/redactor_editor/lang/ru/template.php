<?
$MESS["CATALOG_BUY"] = "Купить";
$MESS["CATALOG_ADD"] = "В корзину";
$MESS["CATALOG_COMPARE"] = "Сравнить";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_QUANTITY"] = "Количество";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "От #FROM# до #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "От #FROM#";
$MESS["CATALOG_QUANTITY_TO"] = "До #TO#";
$MESS["CT_BCS_QUANTITY"] = "Количество";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["redactor_editor_new_publication"] = "+ Новая публикация";
$MESS["redactor_editor_further"] = "Далее";
$MESS["redactor_editor_edit"] = "Редактировать";
$MESS["redactor_editor_delete"] = "Удалить";
$MESS["redactor_editor_comments"] = "Комментарии:";
$MESS["redactor_editor_tags"] = "Теги:";
$MESS["redactor_editor_search_title"] = "Поиск:";
$MESS["redactor_editor_all_title"] = "Все";

?>