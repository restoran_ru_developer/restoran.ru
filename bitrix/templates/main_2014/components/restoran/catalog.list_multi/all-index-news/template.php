<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams['AJAX_REQUEST']!='Y'):?>
<script>
    $(function(){
        var index_news_page = 1;
        $(".update-index-news-trigger.ajax").click(function(e){
            if(index_news_page>5){
                index_news_page=1;
            }
            else {
                index_news_page++;
            }
            e.preventDefault();
            var p = $($(this).attr("href"));
            var _this = $(this);
            p.load($(this).data("href")+'&PAGEN_1='+index_news_page,function(){
                _this.tab('show');
            });

            return false;
        });
    })
</script>
<div class="index-news-wrapper" >
    <div class="update-index-news-trigger ajax" href="#index-ajax-news-wrapper" data-href="/bitrix/templates/main_2014/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=<?=CITY_ID?>&t=all_news"></div>
    <div class="clearfix"></div>
    <div id="index-ajax-news-wrapper">
<?endif?>
<?
//FirePHP::getInstance()->info($arResult["ITEMS"][0]);
foreach($arResult["ITEMS"] as $arItem):
    ?>
    <?

    $link_str_for_all_news = false;
    if(preg_match('/newplace/',$arItem["DETAIL_PAGE_URL"])&&$arItem['IBLOCK_TYPE_ID']=='news'){
        $all_news_link = '/'.CITY_ID.'/news/newplace/';
        $link_str_for_all_news = 'newplace';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='news'){
        $all_news_link = '/'.CITY_ID.'/news/restoransnews'.CITY_ID.'/';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='overviews'){
        $all_news_link = '/'.CITY_ID.'/news/obzor_'.CITY_ID.'/';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='blogs'){
        $all_news_link = '/'.CITY_ID.'/blogs/';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='photoreports'){
        $all_news_link = '/'.CITY_ID.'/photos/';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='catalog'){
        $all_news_link = '/'.CITY_ID.'/catalog/restaurants/all/';
    }
    elseif(preg_match('/mcfromchif/',$arItem["DETAIL_PAGE_URL"])&&$arItem['IBLOCK_TYPE_ID']=='cookery'){
        $all_news_link = '/content/news/mcfromchif/';
        $link_str_for_all_news = 'mcfromchif';
    }
    elseif(preg_match('/editor/',$arItem["DETAIL_PAGE_URL"])&&$arItem['IBLOCK_TYPE_ID']=='cookery'){
        $all_news_link = '/content/cookery/editor/';
        $link_str_for_all_news = 'editor';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='cookery'){
        $all_news_link = '/content/cookery/users/';
        $link_str_for_all_news = 'cookery';
    }
    elseif($arItem['IBLOCK_TYPE_ID']=='afisha'){
        $all_news_link = '/'.CITY_ID.'/afisha/';

        $db_props = CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort" => "asc"), Array("CODE"=>"EVENT_DATE"));
        if($ar_props = $db_props->Fetch())
            $arItem['PROPERTIES']['EVENT_DATE'] = $ar_props;

        $db_props = CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort" => "asc"), Array("CODE"=>"RESTORAN"));
        if($ar_props = $db_props->Fetch()){

            $res = CIBlockElement::GetByID($ar_props['VALUE']);
            if($ar_res = $res->Fetch()){
                $arItem['PROPERTIES']['RESTORAN'] = $ar_res['NAME'];
            }
        }
    }

    ?>
	<div class="index-news-item" >
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DATE_CREATE"]):?>
            <?
            if(date('N',strtotime($arItem["DATE_CREATE"]))==date('N')&&date('d.m')==date('d.m',strtotime($arItem["DATE_CREATE"]))){
                $date_str = GetMessage('TODAY');
            }
            elseif(date('N',strtotime($arItem["DATE_CREATE"]))==(date('N')-1)&&date('d.m',strtotime('-1 day'))==date('d.m',strtotime($arItem["DATE_CREATE"]))) {
                $date_str = GetMessage('YESTERDAY');
            }
            else {
                $date_str = FormatDate("Q", MakeTimeStamp($arItem["DATE_CREATE"]));
            }

//            $time_arr_str = explode('-',$arItem["DATE_CREATE"]);
            $time_arr_str = date('H:i',strtotime($arItem["DATE_CREATE"]));

            $clear_city_arr = array(
                'msk'=>"/( +|)Москва[, ]+/i",
                'spb'=>"/( +|)Санкт-Петербург[, ]+/i",
                'rga'=>"/( +|)Рига[, ]+/i",
                'urm'=>"/( +|)Юрмала[, ]+/i",
                'kld'=>"/( +|)Калиниград[, ]+/i",
                'nsk'=>"/( +|)Новосибирск[, ]+/i",
                'sch'=>"/( +|)Сочи[, ]+/i"
            );

            ?>
			<span class="index-news-date-time"><strong><?=$date_str?><?=$date_str!=GetMessage('TODAY')&&$date_str!=GetMessage('YESTERDAY')?' назад':''?>, <?echo $time_arr_str?></strong>,
                <a href="<?=$all_news_link?>" class="watch-all-in-text"><?=GetMessage('SHOW_ALL_'.($link_str_for_all_news?$link_str_for_all_news:$arItem['IBLOCK_TYPE_ID']))?></a>
                <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                        <?echo trim($arItem["NAME"])?><?if($arItem['IBLOCK_TYPE_ID']=='afisha'):?>. <?=$time_arr_str = date('j',strtotime($arItem['PROPERTIES']['EVENT_DATE']['VALUE'])).' '.strtolower(FormatDate('F',strtotime($arItem['PROPERTIES']['EVENT_DATE']['VALUE']))).'. '.$arItem['PROPERTIES']['RESTORAN'];?>
                        <?endif?>
                    <?if($arItem["PROPERTIES"]["REST_NETWORK"]):?>
                        : сеть ресторанов
                    <?else:?>
                        <?if($arItem["PROPERTIES"]["address"]):?>
                            : <?=is_array($arItem["PROPERTIES"]["address"])?preg_replace($clear_city_arr[CITY_ID],'',$arItem["PROPERTIES"]["address"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arItem["PROPERTIES"]["address"])?>
                        <?endif?>
                    <?endif?>
                </a>
            </span>
		<?endif?>
	</div>
<?endforeach;?>
    <?if($arParams['AJAX_REQUEST']!='Y'):?>
        </div>
    <a href="/<?=CITY_ID?>/allevents/" class="watch-all"><?=GetMessage('SHOW_ALL_NEWS')?></a>
</div>
<?endif?>