<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "comment";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "comment";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "commets";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Further";
$MESS["SHOW_CNT_TITLE"] = "Per";
$MESS["SORT_NEW_TITLE"] = " novelty";
$MESS["SORT_POPULAR_TITLE"] = " by popularity";
$MESS["ANONS_overviews"] = "Reviews";
$MESS["ANONS_photoreports"] = "Photo reports";
$MESS["ANONS_cookery"] = "Master classes";
$MESS["ANONS_news"] = "News";
$MESS["ANONS_interview"] = "Interview";
$MESS["COMMENTS"] = "Comments";
?>