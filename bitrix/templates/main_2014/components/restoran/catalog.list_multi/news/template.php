<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if (count($arResult["ITEMS"]) > 0): ?>
    <?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
    <div class="news-list">
    <?endif?>
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>                                                                    
            <div class="item pull-left<? if ($key % 3 == 2): ?> end<? endif ?>">
                <div class="date" style="margin-bottom: 10px;">
                    <a href="/users/id<?= $arItem["CREATED_BY"] ?>/" class="another"><?= $arItem["AUTHOR_NAME"] ?></a>,  <?= $arItem["CREATED_DATE_FORMATED_1"] ?> <?= $arItem["CREATED_DATE_FORMATED_2"] ?>
                </div>       
                <div class="pic">
                    <? if ($arItem["VIDEO_PLAYER"]): ?>  
                        <?= $arItem["VIDEO_PLAYER"] ?>
                    <? else: ?>                                
                        <a href="<?=($_SERVER["HTTP_HOST"]!='www.restoran.ru'&&$_REQUEST["blog"]=="all"?'http://www.restoran.ru':'')?><?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" width="232" /></a>
                    <? endif; ?>
                </div>
                <div class="text">
                    <h2><a href="<?=($_SERVER["HTTP_HOST"]!='www.restoran.ru'&&$_REQUEST["blog"]=="all"?'http://www.restoran.ru':'')?><?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a></h2>
                    <a href="<?=($_SERVER["HTTP_HOST"]!='www.restoran.ru'&&$_REQUEST["blog"]=="all"?'http://www.restoran.ru':'')?><?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" class="no-decoration"><?= $arItem["PREVIEW_TEXT"] ?></a>
                    <div class="more_links">                        
                        <div class="pull-left comments_link">
                            <?= GetMessage("COMMENTS") ?> &ndash; <a href="<?=($_SERVER["HTTP_HOST"]!='www.restoran.ru'&&$_REQUEST["blog"]=="all"?'http://www.restoran.ru':'')?><?= $arItem["DETAIL_PAGE_URL"] ?>#comments"><?= intval($arItem["PROPERTIES"]["COMMENTS"]) ?></a>
                        </div>

                        <?if($arItem['SHOW_COUNTER']):?><div class="news-viewed-counter-wrapper"><?=$arItem['SHOW_COUNTER']?></div><?endif;?>

                        <div class="pull-right"><a class="serif" href="<?=($_SERVER["HTTP_HOST"]!='www.restoran.ru'&&$_REQUEST["blog"]=="all"?'http://www.restoran.ru':'')?><?= $arItem["DETAIL_PAGE_URL"] ?>"><?= GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS") ?></a></div>
                        <div class="clearfix"></div>
                    </div>
                </div>                            
            </div> 
            <? if ($key % 3 == 2&&$arItem!=end($arResult["ITEMS"])): ?>
                <div class="clearfix dots"></div>
            <?endif;?>
    <? endforeach; ?>
    <?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
    </div>
    <?endif?>
<?else: ?>    
    <p><font class="notetext"><?= GetMessage("NOTHING_TO_FOUND") ?></font></p>    
<?endif; ?>                      
<?if ($arResult["NAV_STRING"]): ?>            
    <div class="navigation for-news-list no-map-link">
        <?= $arResult["NAV_STRING"] ?>                    
    </div>
<?endif; ?>    