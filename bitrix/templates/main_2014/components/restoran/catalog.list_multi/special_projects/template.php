<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($_REQUEST['NEW_DESIGN']=="Y"):?>
<div class="tab-str-wrapper">
    <div class="nav-tab-str-title">
        <?=GetMessage("PODBORKI")?>
    </div>
</div>
<?else:?>
<div class="restoran-detail network-design">
    <div class="tab-str-wrapper">
        <div class="nav-tab-str-title"><?=GetMessage("PODBORKI")?></div>
    </div>
</div>
<!--<ul class="nav nav-tabs">-->
<!--    <li class="active"><a href="#podborki" data-toggle="tab">--><?//=GetMessage("PODBORKI")?><!--</a></li>                        -->
<!--</ul>-->
<?endif;?>
<div class="tab-content">
    <div class="tab-pane active sm" id="podborki">                            
        <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <div class="pull-left <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">
                <div class="pic">
                     <img src="<?=CFile::GetPath($arItem["IBLOCK_PICTURE"])?>"/>
                </div>
                <div class="text">                                    
                    <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["IBLOCK_NAME"]." в ресторане «".$arItem["NAME"]."»"?></a></h2>                                        
                </div>
                <?if (end($arResult["ITEMS"])==$arItem):?>
                
                <?endif;?>
            </div>
        <?endforeach;?>  
        <div class="clearfix"></div>
        <div class="more_links text-right">                                           
            <a href="<?=$APPLICATION->GetCurPage()?>special/" class="btn btn-light"><?=GetMessage("ALL_PODBORKI")?></a>
        </div>
    </div>                        
</div>