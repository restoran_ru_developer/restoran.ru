<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="tab-str-wrapper">
    <div class="nav-tab-str-title">
        <?=GetMessage("PODBORKI")?>
    </div>
    <div class="tabs-center-line"></div>
</div>
<div class="tab-content ">
    <div class="tab-pane medium active" id="podborki">
        <?foreach($arResult["ITEMS"] as $key=>$arItem):
            ?>
            <div class="pull-left <?=(end($arResult["ITEMS"])==$arItem)?"end":""?>">
                <div class="pic">
                     <img src="<?=CFile::GetPath($arItem["IBLOCK_PICTURE"])?>"/>
                </div>
                <div class="text">
                    <div class="date"><b></b><?=FormatDate('j F Y',strtotime($arItem['TIMESTAMP_X']))?></div>
                    <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?if(preg_match('/letnie_verandy/',$arItem['IBLOCK_CODE'])):?><?="Летняя веранда ресторана «".$arItem["NAME"]."»"?><?else:?><?=$arItem["IBLOCK_NAME"]." в ресторане «".$arItem["NAME"]."»"?><?endif?></a></h2>
                    <?=$arItem["PREVIEW_TEXT"]?>
                </div>

            </div>
        <?endforeach;?>  
        <div class="clearfix"></div>
        <div class="more_links text-right">                                           
            <a href="<?=$APPLICATION->GetCurPage()?>special/" class="btn btn-light"><?=GetMessage("ALL_PODBORKI")?></a>
        </div>
    </div>                        
</div>