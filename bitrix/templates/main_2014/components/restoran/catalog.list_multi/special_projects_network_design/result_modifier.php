<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("iblock");
foreach($arResult["ITEMS"] as $key=>&$arItem)
{
    $res = CIBlock::GetByID($arItem["IBLOCK_ID"]);
    if ($ar = $res->Fetch())
    {        
        $arItem["IBLOCK_PICTURE"] = $ar["PICTURE"];
        $arItem['NAME'] = strip_tags($arItem["NAME"]);        
        $a = explode(" -",$ar["NAME"]);
        $arItem["IBLOCK_NAME"] = $a[0];
        $arItem['NAME'] = str_replace("<br>"," ",$arItem["~NAME"]);
        $arItem['NAME'] = str_replace("<br/>"," ",$arItem["~NAME"]);
        $arItem['NAME'] = str_replace("<br />"," ",$arItem["~NAME"]);        
        $a = explode("&lt;",$arItem["NAME"]);
        $arItem["NAME"] = $a[0];
    }
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
}
?>