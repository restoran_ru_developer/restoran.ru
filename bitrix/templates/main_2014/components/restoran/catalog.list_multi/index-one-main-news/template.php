<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index-one-main-news-wrapper">
    <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arResult['ITEMS'][0]['DETAIL_PAGE_URL']?>" class="index-one-main-news-pic-wrapper">
        <img
            class="index-one-main-news-pic"
            border="0"
            src="<?=$arResult["CROP_PREVIEW_PICTURE"]["src"]?>"
            width="<?=$arResult["CROP_PREVIEW_PICTURE"]["width"]?>"
            height="<?=$arResult["CROP_PREVIEW_PICTURE"]["height"]?>"
            alt="<?=$arResult['ITEMS'][0]['NAME']?>"
            title="<?=$arResult['ITEMS'][0]["NAME"]?>"
            />
    </a>
    <div class="index-one-main-news-name-wrapper"><a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arResult['ITEMS'][0]['DETAIL_PAGE_URL']?>"><?=$arResult['ITEMS'][0]['NAME']?></a></div>
    <div class="index-one-main-news-about-wrapper"><a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arResult['ITEMS'][0]['DETAIL_PAGE_URL']?>"><?=$arResult['ITEMS'][0]['PREVIEW_TEXT']?></a></div>
<?

if(preg_match('/newplace/',$arResult['ITEMS'][0]["DETAIL_PAGE_URL"])&&$arResult['ITEMS'][0]['IBLOCK_TYPE_ID']=='news'){
    $all_news_link = '/'.CITY_ID.'/news/newplace/';
    $link_str_for_all_news = 'newplace';
}
elseif($arResult['ITEMS'][0]['IBLOCK_TYPE_ID']=='news'){
    $all_news_link = '/'.CITY_ID.'/news/restoransnews'.CITY_ID.'/';
}
elseif($arResult['ITEMS'][0]['IBLOCK_TYPE_ID']=='afisha'){
    $all_news_link = '/'.CITY_ID.'/afisha/';
}
elseif($arResult['ITEMS'][0]['IBLOCK_TYPE_ID']=='overviews'){
    $all_news_link = '/'.CITY_ID.'/news/obzor_'.CITY_ID.'/';
}
elseif($arResult['ITEMS'][0]['IBLOCK_TYPE_ID']=='blogs'){
    $all_news_link = '/'.CITY_ID.'/blogs/';
}
elseif($arResult['ITEMS'][0]['IBLOCK_TYPE_ID']=='photoreports'){
    $all_news_link = '/'.CITY_ID.'/photos/';
}
else {
    $all_news_link = $arResult['ITEMS'][0]["SECTION_PAGE_URL"];
}

?>
    <div class="index-one-main-news-bottom-line">
        <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$all_news_link?>" class="watch-all"><?=GetMessage('SHOW_ALL_'.($link_str_for_all_news?$link_str_for_all_news:$arResult['ITEMS'][0]['IBLOCK_TYPE_ID']))?></a>
        <?
        if($arResult['ITEMS'][0]['IBLOCK_TYPE_ID']!='afisha'){
            if($arResult['ITEMS'][0]['PROPERTIES']['COMMENTS']['VALUE']):?><div class="index-one-main-news-comments-counter-wrapper"><?=$arResult['ITEMS'][0]['PROPERTIES']['COMMENTS']['VALUE']?></div><?endif?>
        <?}?>
        <?if($arResult['ITEMS'][0]['SHOW_COUNTER']):?><div class="index-one-main-news-viewed-counter-wrapper"><?=$arResult['ITEMS'][0]['SHOW_COUNTER']?></div><?endif?>
    </div>
</div>