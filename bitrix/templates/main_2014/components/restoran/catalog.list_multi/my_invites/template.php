<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])>0){?>
    <table class="profile-activity w100" cellpadding="0" cellspacing="0">
        <tr>
            <th><?=GetMessage('my_invites_invite_title')?></th>
            <th><?=GetMessage('my_invites_my_guests_title')?></th>
            <th><?=GetMessage('my_invites_status_title')?></th>
        </tr>
        <?foreach($arResult["ITEMS"] as $arItem):?>

            <?
            $act=1;
            if ($arItem["ACTIVE_FROM"]<date("d.m.Y  H:i:s")):
                $act = 0;
            endif;
            ?>
            <tr class='<?=(end($arResult["ITEMS"])==$arItem)?"nobg":""?> <?=(!$act)?"disable":""?>' id="inv<?=$arItem["ID"]?>">

                <td valign="top" class="first w-new" width="240">
                    <div class="left">
                        <img src="<?=$arItem["RESTORAN"]["PICTURE"]?>" width="70" />
                    </div>
                    <div class="left">
                        <a class="name" href="<?=$arItem["RESTORAN"]["URL"]?>"><?=$arItem["RESTORAN"]["NAME"]?></a><br />
                        <div class="rating">
                            <?for($i = 1; $i <= 5; $i++):?>
                                <div class="small_star<?if($i <= round($arItem["RESTORAN"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                            <?endfor?>
                            <div class="clear"></div>
                        </div>
                        <?if ($arItem["RESTORAN"]["SECTION"]=="banket"):?>
                            <p class="upcase"><?=GetMessage('my_invites_banqueting_hall_title')?></p>
                        <?else:?>
                            <p class="upcase"><?=GetMessage('my_invites_restaurant_title')?></p>
                        <?endif;?>
                    </div>
                    <div class="clear"></div>
                    <br />
                    <?=$arItem["DISPLAY_ACTIVE_FROM"]?> в <?=$arItem["TIME"]?><Br /><Br />
                    <?if (!$arItem["PROPERTIES2"]["BRON"][0]):?>
                        <?if ($act):?>
                            <a class="icon-continue" href="javascript:void(0)" onclick="add_invite(<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_edit_title')?></a>
                            <a class="icon-delete" href="javascript:void(0)" onclick="delete_invite(<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_cancel_title')?></a>
                        <?else:?>
                            <a class="icon-delete" href="javascript:void(0)" onclick="del_invite(<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                        <?endif;?>
                    <?else:?>
                        <?if (!$act):?>
                            <a class="icon-delete" href="javascript:void(0)" onclick="del_invite(<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                        <?endif;?>
                    <?endif;?>
                </td>
                <td class="user-friends nofloat" colspan="2" valign="top">
                    <ul>
                        <?$us_co = 0;?>
                        <?$co = 0;?>
                        <?if ($arItem["USERS"][0]): ?>
                            <?foreach($arItem["USERS"] as $respUserReg):?>
                                <li <?=(end($arItem["USERS"])==$respUserReg&&!$arItem["NA_USERS"])?"class='nobg'":""?>>
                                    <div class="avatar left">
                                        <img src="<?=$respUserReg["PHOTO"]["src"]?>" width="64" />
                                    </div>
                                    <div class="user-friend left">
                                        <a class="name" href="/users/id<?=$respUserReg["ID"]?>/"><?=$respUserReg["NAME"]?></a><br/>
                                        <?=$respUserReg["PERSONAL_CITY"]?><br />
                                        <p class="user-mail"><a href="javascript:void(0)" onclick="send_message(<?=$respUserReg["ID"]?>,'<?=$respUserReg["NAME"]?>')"><?=GetMessage('my_invites_message_title')?></a></p>
                                    </div>
                                    <div class="left" style="padding-top:6px;">
                                        <?if ($respUserReg["ACCEPT"]=="Y"):?>
                                            <i><?=GetMessage('my_invites_confirmed_title')?></i>
                                            <?$us_co++;?>
                                            <?$co++;?>
                                            <a class="icon-delete" href="javascript:void(0)" onclick="del_user(<?=$respUserReg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                                        <?elseif ($respUserReg["ACCEPT"]=="N"&&$arItem["PROPERTIES2"]["BRON"][0]):?>
                                            <i><?=GetMessage('my_invites_confirmed_title')?></i>
                                            <?$us_co++;?>
                                            <?$co++;?>
                                        <?elseif ($respUserReg["ACCEPT"]=="N"&&!$arItem["PROPERTIES2"]["BRON"][0]):?>
                                            <i><?=GetMessage('my_invites_declined_title')?></i>
                                            <?$us_co++;?>
                                            <a class="icon-delete" href="javascript:void(0)" onclick="del_user(<?=$respUserReg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                                        <?else:?>
                                            <i><?=GetMessage('my_invites_pending_title')?></i>
                                            <a class="icon-delete" href="javascript:void(0)" onclick="del_user(<?=$respUserReg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                                        <?endif;?>
                                    </div>
                                    <!--<div class="right" style="padding-top:6px;">
                                        <a class="icon-delete_f2" href="#" alt="Отклонить приглашение для данного пользователя" title="Отклонить приглашение для данного пользователя"></a> 
                                    </div>-->
                                    <div class="clear"></div>
                                </li>
                            <?endforeach?>
                        <?endif;?>
                        <?$us_na = 0;?>
                        <?$na = 0;?>
                        <?if ($arItem["NA_USERS"][0]): ?>
                            <?foreach($arItem["NA_USERS"] as $respUserUnreg):?>
                                <li class="<?=(end($arItem["NA_USERS"])==$respUserUnreg)?"nopic nobg":""?>">
                                    <div class="user-friend left" style="width:295px">
                                        <p><?=$respUserUnreg["NAME"]?></p>
                                    </div>
                                    <div class="left" style="padding-top:6px;">
                                        <?if ($respUserUnreg["ACCEPT"]=="Y"):?>
                                            <i><?=GetMessage('my_invites_confirmed_title')?></i>
                                            <?$us_na++;?>
                                            <?$na++;?>
                                            <a class="icon-delete" href="javascript:void(0)" onclick="del_user(<?=$respUserUnreg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                                        <?elseif ($respUserUnreg["ACCEPT"]=="N"):?>
                                            <i><?=GetMessage('my_invites_declined_title')?></i>
                                            <?$us_na++;?>
                                            <a class="icon-delete" href="javascript:void(0)" onclick="del_user(<?=$respUserUnreg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                                        <?else:?>
                                            <i><?=GetMessage('my_invites_pending_title')?></i>
                                            <a class="icon-delete" href="javascript:void(0)" onclick="del_user(<?=$respUserUnreg["PROPERTY_ID"]?>,<?=$arItem["ID"]?>)"><?=GetMessage('my_invites_remove_title')?></a>
                                        <?endif;?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                            <?endforeach?>
                        <?endif;?>
                        <?if ($act):?>
                            <?if (count($arItem["USERS"])==$us_co&&count($arItem["NA_USERS"])==$us_na&&($na+$co)>0&&!$arItem["PROPERTIES2"]["BRON"][0]):?>
                                <script>
                                    $(document).ready(function(){
                                        $(".add_bron<?=$arItem["ID"]?>").click(function(){
                                            $.ajax({
                                                type: "POST",
                                                url: "/tpl/ajax/online_order_rest.php",
                                                data: "what=0&CITY=<?=$arItem["IBLOCK_CODE"]?>&invite=<?=$arItem["ID"]?>&date=<?=$arItem["DATE"]?>&name=<?=rawurlencode($arItem["RESTORAN"]["NAME"])?>&id=<?=$arItem["RESTORAN"]["ID"]?>&time=<?=$arItem["TIME"]?>&person=<?=($co+$na+1)?>&<?=bitrix_sessid_get()?>",
                                                success: function(data) {
                                                    if (!$("#mail_modal").size())
                                                    {
                                                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");
                                                    }
                                                    $('#bron_modal').html(data);
                                                    showOverflow();
                                                    setCenter($("#bron_modal"));
                                                    $("#bron_modal").fadeIn("300");
                                                }
                                            });
                                        });
                                    });
                                </script>
                                <input type="button" class="add_bron<?=$arItem["ID"]?> light_button" value="<?=GetMessage('my_invites__title')?>оформить бронирование" />
                            <?endif;?>

                            <?if (count($arItem["USERS"])!=$us_co||count($arItem["NA_USERS"])!=$us_na):
                                $fff = count($arItem["USERS"])-us_co+count($arItem["NA_USERS"])-$us_na;
                                ?>
                                <input  type="button" class="light_button" onclick=' alert("<?=GetMessage('my_invites_cant_make_a_reservation_title')?>");' style="opacity:0.5" value="<?=GetMessage('my_invites_make_a_reservation_title')?>" />
                            <?endif;?>
                        <?endif;?>
                        <?if ($arItem["PROPERTIES2"]["BRON"][0]):?>
                            <i><?=GetMessage('my_invites_follow_booking')?> <?=(CITY_ID=="spb")?"(812) 740-18-20":"(495) 988-26-56"?></i>
                        <?endif;?>
                        <!--<p class="more-activity" style="margin-top:12px; margin-bottom:0;"><a href="#">Еще 3</a></p>-->
                    </ul>
                </td>
            </tr>
        <?endforeach;?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </table>
<?}else{?>
    <?=GetMessage('my_invites_no_invites_title')?>
<?}?>
