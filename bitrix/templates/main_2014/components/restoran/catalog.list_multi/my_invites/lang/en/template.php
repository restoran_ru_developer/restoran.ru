<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["RESUME_STATUS"] = "Status";
$MESS["RESUME_STATUS_Y"] = "Opened";
$MESS["RESUME_STATUS_N"] = "Closed";
$MESS["RESPOND_TITLE"] = "Responded";
$MESS["RESTORATOR_STATE"] = "Restorator";

$MESS["my_invites_invite_title"] = "Invitation";
$MESS["my_invites_my_guests_title"] = "My guests";
$MESS["my_invites_status_title"] = "Status";
$MESS["my_invites_banqueting_hall_title"] = "banqueting hall";
$MESS["my_invites_restaurant_title"] = "restaurant";


$MESS["my_invites_remove_title"] = "Remove";
$MESS["my_invites_message_title"] = "Message";
$MESS["my_invites_confirmed_title"] = "Confirmed";
$MESS["my_invites_pending_title"] = "Pending";
$MESS["my_invites_no_invites_title"] = "No added invitations";
$MESS["my_invites_reject_title"] = "Reject";
$MESS["my_invites_guests_came_title"] = "guests came";
$MESS["my_invites_event_took_place_title"] = "The event took place";
$MESS["my_invites_guests_cancel_the_order_title"] = "guests cancel the order";
$MESS["my_invites_reservation_is_canceled_by_the_user_title"] = "The reservation is canceled by the user";
$MESS["my_invites_table_booked_title"] = "Table booked";
?>