<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["RESUME_STATUS"] = "Статус";
$MESS["RESUME_STATUS_Y"] = "Открыта";
$MESS["RESUME_STATUS_N"] = "Закрыта";
$MESS["RESPOND_TITLE"] = "Откликнулись";
$MESS["RESTORATOR_STATE"] = "Ресторатор";

$MESS["my_invites_invite_title"] = "Приглашение";
$MESS["my_invites_my_guests_title"] = "Мои гости";
$MESS["my_invites_status_title"] = "Статус";
$MESS["my_invites_banqueting_hall_title"] = "банкетный зал";
$MESS["my_invites_restaurant_title"] = "ресторан";
$MESS["my_invites_edit_title"] = "Редактировать";
$MESS["my_invites_cancel_title"] = "Отменить";
$MESS["my_invites_remove_title"] = "Удалить";
$MESS["my_invites_message_title"] = "Сообщение";
$MESS["my_invites_confirmed_title"] = "Подтвердил";
$MESS["my_invites_declined_title"] = "Отклонено";
$MESS["my_invites_pending_title"] = "В ожидании";
$MESS["my_invites_make_a_reservation_title"] = "оформить бронирование";
$MESS["my_invites_cant_make_a_reservation_title"] = "Вы не можете оформить заказ, т.к. ещё не все приглашённые приняли решение.";
$MESS["my_invites_follow_booking"] = "Следить за состоянием заказа Вы можете в разделе «Бронирования». Внести в бронь изменения вы можете по телефону";
$MESS["my_invites_no_invites_title"] = "Нет добавленных приглашений";
?>