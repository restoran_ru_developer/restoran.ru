<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();		
if($arResult["VALUES"]["PERSONAL_CITY"]=="") $arResult["VALUES"]["PERSONAL_CITY"]=getCityByIP();
?>
<script src="<?=$templateFolder?>/script.js"></script>
<div class="register_form">
<h1><?=GetMessage("AUTH_REGISTER")?></h1>
<div class="clearfix"></div>
<?if($USER->IsAuthorized()):?>
    <p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
    <div style="margin-bottom:400px;"></div>
<?else:?>
    <? 
    if (count($arResult["ERRORS"]) > 0):
            foreach ($arResult["ERRORS"] as $key => $error)
                            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                        ShowError(implode("<br />", $arResult["ERRORS"]));

    elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
    ?>
    <!--<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>-->        
    <?endif?>
    <form id="formm" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
    <?if($arResult["BACKURL"] <> ''):?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?endif;?>
        <div class="pull-left" style="width:400px;">
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_LAST_NAME")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["NAME"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 another">(не обязательно для заполнения)</span><br />
                <input  type="text" class="form-control" name="REGISTER[LAST_NAME]" placeholder="Иванов" value="<?=$_REQUEST["REGISTER"]["LAST_NAME"]?>" autocomplete="off" style="width:330px;"/>
            </div>

            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_NAME")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["NAME"] == "Y"):?><span class="starrequired">*</span><?endif?>: <!--<span class="font12 grey"><?=GetMessage("REGISTER_FIELD_NAME_DESCRIPTION")?></span>--><span class="font12 another">(не обязательно для заполнения)</span><br />
                <input  type="text" class="form-control" name="REGISTER[NAME]" placeholder="Иван" value="<?=$_REQUEST["REGISTER"]["NAME"]?>" autocomplete="off"  style="width:330px;"/>
            </div>

            <div class="form-group">
                <div class="nickError-block">
                    <div class="nickError">Этот ник уже занят</div>
                </div>
                <input type="hidden" id="nickisok" value="" />
                <?=GetMessage("REGISTER_FIELD_PERSONAL_PROFESSION")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["PERSONAL_PROFESSION"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PERSONAL_PROFESSION_DESCRIPTION")?></span><br />
                <input  type="text" class="form-control" id="nicknameinput" name="REGISTER[PERSONAL_PROFESSION]" onkeyup="nickChanged();" placeholder="nickname" value="<?=$_REQUEST["REGISTER"]["PERSONAL_PROFESSION"]?>" autocomplete="off"  style="width:330px;"/>
            </div>                     
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_PASSWORD")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PASSWORD_DESCRIPTION")?></span><br />
                <input type="password" class="form-control" name="REGISTER[PASSWORD]"  autocomplete="off"   style="width:330px;" />
            </div>
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD_DESCRIPTION")?></span><br />
                <input type="password" class="form-control" name="REGISTER[CONFIRM_PASSWORD]"   autocomplete="off"   style="width:330px;"/>
            </div>
            <!--<div class="question">
                <?=GetMessage("REGISTER_FIELD_PODT")?>: <a href="javascript:void(0)" val="sms" class="sms active no_border"><i><?=GetMessage("USER_SMS")?></i></a> / <a class="sms no_border" val="email" href="javascript:void(0)"><i><?=GetMessage("USER_EMAIL")?></i></a>
                <input type="hidden" id="sms" name="REGISTER[SMS]" value="email"/>
            </div>-->
                <input type="hidden" id="sms" name="REGISTER[SMS]" value="email"/>                        
                <div class="form-group" style="position:relative;">
                <?if ($arResult["USE_CAPTCHA"] == "Y"):?>
                    <div class="question"><?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<br />
                        <input id="captchaSid" type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <img id="captchaImg" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" align="left" style="margin-right: 20px"/>
                        <a id="reloadCaptcha">Поменять картинку</a>                        
                        
                            <script type="text/javascript">
                                $(document).ready(function(){
                                   $('#reloadCaptcha').click(function(){                                      
                                      $.getJSON('<?=$this->__folder?>/reload_captcha.php', function(data) {
                                         $('#captchaImg').attr('src','/bitrix/tools/captcha.php?captcha_sid='+data);
                                         $('#captchaSid').val(data);                                         
                                      });
                                      return false;
                                   });
                                });
                             </script>
                         
                        <input class="form-control" type="text" name="captcha_word" maxlength="10" value="" size="15" style="width:100px;margin-left:20px" />
                    </div>
                <?endif;?>
            </div>
        </div>
        <div class="pull-right" style="width:500px;">
            <?/*if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                    <div class="question">
                        <?=$arResult["USER_PROPERTIES"]["DATA"]["UF_KITCHEN"]["EDIT_FORM_LABEL"]?><Br />
                        <div style="border:1px solid #CCC; padding:10px">
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                "iblock_element",
                                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arResult["USER_PROPERTIES"]["DATA"]["UF_KITCHEN"], "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
                        </div>                       
                    </div>
            <?endif;*/?>
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE_DESCRIPTION")?></span><br />
                <input type="text" class="form-control phone" name="REGISTER[PERSONAL_PHONE]" value="<?=$_REQUEST["REGISTER"]["PERSONAL_PHONE"]?>" autocomplete="off"  style="width:330px;"/>
            </div>
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_EMAIL")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"]["EMAIL"] == "Y"):?><span class="starrequired">*</span><?endif?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_EMAIL_DESCRIPTION")?></span><br />
                <input id="EMAIL" type="text" class="form-control" name="REGISTER[EMAIL]" <?=($_REQUEST["invite_email"])?"readonly='readonly'":""?> value="<?=($_REQUEST["invite_email"])?$_REQUEST["invite_email"]:$arResult["VALUES"]["EMAIL"]?>"  autocomplete="off"   style="width:330px;"/>
                <input id="LOGIN" type="hidden" class="placeholder" name="REGISTER[LOGIN]" value="<?=$_REQUEST["REGISTER"]["LOGIN"]?>"  autocomplete="off"   style="width:330px;"/>
                <input type="hidden" name="invite" value="1" />
                <?if ($_REQUEST["co"]):?>
                <input type="hidden" name="REGISTER[PERSONAL_PAGER]" value="<?=$_REQUEST["co"]?>" />
                <?endif;?>
            </div>   
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_CITY")?>: <span class="font12 grey"><?=GetMessage("REGISTER_FIELD_PERSONAL_CITY_DESCRIPTION")?></span><br />
                <input id="EMAIL" type="text" class="form-control" name="REGISTER[PERSONAL_CITY]" value="<?=$_REQUEST["REGISTER"]["PERSONAL_CITY"]?>"  autocomplete="off"   style="width:330px;"/>
            </div>
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_BIRTHDAY")?>: <Br />
                <input type="text" class="datew form-control" name="REGISTER[PERSONAL_BIRTHDAY]" value="<?=$_REQUEST["REGISTER"]["PERSONAL_BIRTHDAY"]?>"  style="width:330px;"/>
            </div>
            <div class="form-group">
                <?=GetMessage("REGISTER_FIELD_PERSONAL_GENDER")?>:  <a href="javascript:void(0)" val="M" class="gender no_border"><i><?=GetMessage("USER_MALE")?></i></a> / <a class="gender no_border" val="F" href="javascript:void(0)"><i><?=GetMessage("USER_FEMALE")?></i></a>
                <input type="hidden" id="gender" name="REGISTER[PERSONAL_GENDER]" value="M"/>
            </div>   
            <div class="form-group">
                <script src="/tpl/js/jquery.checkbox.js"></script>
                <table cellpadding="0" cellspacing="5">
                    <tr>
                        <td><span class="niceCheck" id="agreement_s"><input type="checkbox" name="SUBSCRIBE" value="1"></span></td>
                        <td><label class="niceCheckLabel" style="font-size:14px;">Я хочу получать информационную рассылку с этого сайта</label></td>
                    </tr>
                </table>
            </div>
            <div class="form-group">                
                <table cellpadding="0" cellspacing="5">
                    <tr>
                        <td><span class="niceCheck" id="agreement_s"><input type="checkbox" name="agreement" id="agreement"></span></td>
                        <td><label class="niceCheckLabel" style="font-size:14px;">Я принимаю условия</label> <a id="link_agreement" target="_blank" href="/auth/user_license_agreement.php">пользовательского соглашения</a></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="clear"></div>
        <input type="hidden" name="group" id="group_u" value="" />
        <br />
        <div align="right"><input class="btn btn-info btn-nb disabled" id="register_submit_button" disabled type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" /></div>

    <!--<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
    <p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>-->

    </form>
<?endif?>
</div>
<?/*if($_POST["REGISTER"]["RESTORATOR"] && $arResult["ERRORS"]):?>
    <script type="text/javascript">
        showRestForm();
    </script>
<?endif*/?>