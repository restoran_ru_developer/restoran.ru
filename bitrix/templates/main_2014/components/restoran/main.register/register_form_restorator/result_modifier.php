<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// don't show email error
if($arResult["ERRORS"])
    unset($arResult["ERRORS"]["EMAIL"]);

// add link to license agreement label
$strLink = '<a href="#" onclick="window.open(\'/auth/user_license_agreement.php\', \'\', \'location=yes,status=no,scrollbars=yes,resizable=yes,width=700,height=550 top=\'+Math.floor((screen.height - 550)/2-14)+\',left=\'+Math.floor((screen.width - 700)/2-5)); return false;">пользовательского соглашения</a>';

$arResult["USER_PROPERTIES"]["DATA"]["UF_USER_AGREEMENT"]["EDIT_FORM_LABEL"] = str_replace("пользовательского соглашения", $strLink, $arResult["USER_PROPERTIES"]["DATA"]["UF_USER_AGREEMENT"]["EDIT_FORM_LABEL"]);

$arResult["VALUES"]["PERSONAL_CITY"]=getCityByIP();
?>