// show rest form selector
function showRestForm() {
    $('.rest_form_fields').toggle();
}

function ajaxNick() {
    nick = $('#nicknameinput2').val()
    $.ajax({
        type: "POST",
        url: "/bitrix/templates/main/components/restoran/main.register/register_form/nickname-ajax.php",
        data: {
            nick:nick
        },
        success: function(data) {
            if(data == '1'){
                $('.nickError-block').stop(true, true).fadeOut();
            } else {
                $('.nickError-block').stop(true, true).fadeIn();
            }
        }
    })
}

function nickChanged() {
    if (typeof id !== 'undefined'){
        clearTimeout(id);
    }
    if ($('#nicknameinput2').val() == ""){
        $('.nickError-block').stop(true, true).fadeOut();
    } else {
        
        id = setTimeout(ajaxNick, 1000);
    }
    
}



$(document).ready(function() {
    // set login from email
/*    $('#EMAIL').keyup(function() {
        $('#LOGIN').val($(this).val());
    });*/
    
    
    $('input[name=register_submit_button]').click(function() {
        $('#LOGIN2').val($('#EMAIL2').val());
    });    

    $('#show_rest_form').change(function(){
        showRestForm();
    });    
    $(".datew").maski("99.99.9999")
    $(".phone").maski("(999) 999-99-99");
    /*$(".sms").click(function(){
        $(".sms").removeClass("active");
        $(this).addClass("active");
        $("#sms").attr("value",$(this).attr("val"));
    });*/
    $(".gender").click(function(){
        $(".gender").removeClass("active");
        $(this).addClass("active");
        $("#gender").attr("value",$(this).attr("val"));
    });
//    var params = {
//        changedEl: "#multi5",
//        visibleRows: 10,
//        scrollArrows: true
//    }
//    cuSelMulti(params);
        $("#CITY").autocomplete("/tpl/ajax/get_city.php", {
            limit: 5,
            minChars: 3,
            formatItem: function(data, i, n, value) {
                return value;
            },
            formatResult: function(data, value) {
                return value;
            },
        });
});