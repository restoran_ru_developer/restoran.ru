<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if ($arParams["AJAX_CALL"] != "Y" && !$arResult["ALLREADY_FRIENDS"]):?>

    <div class="fleft">
        <div class="avatar"></div>
        <a onclick="addUser(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" href="javascript:void(null);"><?=GetMessage("USER_RECOVER_FRIEND_BUTTON")?></a>
    </div>
    <div class="fright">
        <p>
            <a href="#"><?=$arResult["USER_INFO"]["NAME"]?> <?=$arResult["USER_INFO"]["LAST_NAME"]?></a><br/>
            <?if($arResult["USER_INFO"]["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
            <?=$arResult["USER_INFO"]["PERSONAL_CITY"]?>
        </p>
        <p></p>
        <p></p>
    </div>
<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["ALLREADY_FRIENDS"]=="Z"):?>
    <div class="fleft">
        <div class="avatar">
            <img src="<?=$arResult["USER_INFO"]["USER_PERSONAL_PHOTO_FILE"]["src"]?>" />
        </div>
        <a href="javascript:void(0)"><?=GetMessage("USER_REQUEST_BUTTON")?></a>
    </div>
    <div class="fright">
        <p>
            <a href="#"><?=$arResult["USER_INFO"]["NAME"]?> <?=$arResult["USER_INFO"]["LAST_NAME"]?></a><br/>
            <?if($arResult["USER_INFO"]["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
            <?=$arResult["USER_INFO"]["PERSONAL_CITY"]?>
        </p>
        <p class="user-mail"><a href="#"><?=GetMessage("SEND_MSG")?></a></p>
        <p class="user-invitation"><a href="#"><?=GetMessage("REST_INVITE")?></a></p>
    </div>
<?elseif($arParams["AJAX_CALL"] == "Y" && $arResult["REL_ID"] && $arResult["ALLREADY_FRIENDS"]=="F"):?>
    <div class="fleft">
        <div class="avatar">
            <img src="<?=$arResult["USER_INFO"]["USER_PERSONAL_PHOTO_FILE"]["src"]?>" />
        </div>
        <a onclick="if(confirm('<?=GetMessage("USER_DELETE_QUEST")?>')) deleteUser(<?=htmlspecialchars($arResult["JS_PARAMS"])?>)" href="javascript:void(null);"><?=GetMessage("USER_DEL_FRIEND_BUTTON")?></a>
    </div>
    <div class="fright">
        <p>
            <a href="#"><?=$arResult["USER_INFO"]["NAME"]?> <?=$arResult["USER_INFO"]["LAST_NAME"]?></a><br/>
            <?if($arResult["USER_INFO"]["IS_RESTORATOR"]):?><em><?=GetMessage("RESTORATOR_STATE")?></em><br /><?endif?>
            <?=$arResult["USER_INFO"]["PERSONAL_CITY"]?>
        </p>
        <p class="user-mail"><a href="#"><?=GetMessage("SEND_MSG")?></a></p>
        <p class="user-invitation"><a href="#"><?=GetMessage("REST_INVITE")?></a></p>
    </div>
<?endif?>