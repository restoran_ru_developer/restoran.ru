<?
$MESS["USER_RECOVER_FRIEND_BUTTON"] = "+ Add to friends";
$MESS["RESTORATOR_STATE"] = "restorator";
$MESS["SEND_MSG"] = "Message";
$MESS["REST_INVITE"] = "Invite to restaurant";
$MESS["USER_DELETE_QUEST"] = "Remove from Spica friends?";
$MESS["USER_DEL_FRIEND_BUTTON"] = "Remove from friends";
$MESS["USER_REQUEST_BUTTON"] = "Application sent";
$MESS["ADD_MESSAGE"] = "Application for adding a friend sent.";
?>