<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="pull-left <?=($key%3==2)?"end":""?>">
        <?if ($arParams["PARENT_SECTION_CODE"]!="vacancy"&&$arParams["PARENT_SECTION_CODE"]!="resume"):?>
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
        </div>
        <?endif;?>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>            
                <?if ($arParams["PARENT_SECTION_CODE"]!="vacancy"&&$arParams["PARENT_SECTION_CODE"]!="resume"):?>
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["REVIEWS"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>                      
                </div>           
                <?endif;?>
                <div class="props">
                    <?if ($arItem["PROPERTIES"]["phone"]):?>
                    <div class="prop">
                        <div class="name"><?=GetMessage("FAV_PHONE")?></div>
                        <div class="value"><?=$arItem["PROPERTIES"]["phone"]?></div>
                    </div>
                    <?endif;?>
                    <?if ($arItem["PROPERTIES"]["address"]):?>
                    <div class="prop">
                        <div class="name"><?=GetMessage("FAV_ADRESS")?></div>
                        <div class="value"><?=$arItem["PROPERTIES"]["address"]?></div>
                    </div>                                        
                    <?endif;?>
                    <?if ($arItem["PROPERTIES"]["subway"]):?>
                    <div class="prop wsubway">
                        <div class="subway">M</div>
                        <div class="value"><?=$arItem["PROPERTIES"]["subway"]?></div>
                    </div>
                    <?endif;?>
                </div>    
                <?if ($arParams["PARENT_SECTION_CODE"]=="cookery"):?>
                    <?=$arItem["PREVIEW_TEXT"]?>
                <?endif;?>
            <a href="?del=<?= $arItem["ID"] ?>&<?= bitrix_sessid_get() ?>"><?= GetMessage("FAV_DELETE") ?></a>
        </div>        
    </div>
    <?=($key%3==2||$arItem==end($arResult["ITEMS"]))?"<div class='clearfix'></div>":""?>	
<?endforeach;?>