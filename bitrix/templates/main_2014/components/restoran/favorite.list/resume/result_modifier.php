<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

foreach ($arResult["ITEMS"] as $key => &$arItem):

    
    $pr_res = CIBlockElement::GetByID($arItem["~PREVIEW_TEXT"]);
    if ($pr_res = $pr_res->GetNext()) {
        $ID = $pr_res["ID"];
        $IblockID = $pr_res["IBLOCK_ID"];
    }
    
    
    $db_props = CIBlockElement::GetProperty($IblockID, $ID, array("sort" => "asc"), Array("CODE" => "EDUCATION"));
    $ar_props = $db_props->Fetch();
    $arResult["ITEMS"][$key]["EDUCATION"] = $ar_props["VALUE_ENUM"];
    
     
    $db_props = CIBlockElement::GetProperty($IblockID, $ID, array("sort" => "asc"), Array("CODE" => "EXPERIENCE"));
    $ar_props = $db_props->Fetch();
    $arResult["ITEMS"][$key]["EXPERIENCE"] = $ar_props["VALUE_ENUM"];
    
    
    if ($arItem["PREVIEW_PICTURE"])
        $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROP, true);
    elseif ($arItem["DETAIL_PICTURE"])
        $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROP, true);
//v_dump($arItem);
endforeach;

?>