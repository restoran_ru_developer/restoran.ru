<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
    <? //v_dump($arItem); ?>
    <div class="pull-left <? if ($key % 3 == 2): ?> end<? endif ?>">
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>       
            
            <? if ($arItem["EDUCATION"]): ?>
                <p><b><?= GetMessage("EDUCATION") ?></b>: <?= $arItem["EDUCATION"] ?></p>
            <? endif; ?>
            <? if ($arItem["EXPERIENCE"]): ?>
                <p><b><?= GetMessage("EXPERIENCE") ?></b>: <?= $arItem["EXPERIENCE"] ?></p>
            <? endif; ?>
            <? if ($arItem["PROPERTIES"]["SUGG_WORK_ZP_FROM"]): ?>
                <p><b><?= GetMessage("WAGES") ?></b>: <?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_FROM"] ?> - <?= $arItem["PROPERTIES"]["SUGG_WORK_ZP_TO"] ?></p>
            <? endif; ?>
        </div>        
        <div class="del">
            <a href="?del=<?= $arItem["ID"] ?>&<?= bitrix_sessid_get() ?>"><?= GetMessage("FAV_DELETE") ?></a>
        </div>
    </div>
    <?=($key%3==2||$arItem==end($arResult["ITEMS"]))?"<div class='clearfix'></div>":""?>	
<? endforeach; ?>