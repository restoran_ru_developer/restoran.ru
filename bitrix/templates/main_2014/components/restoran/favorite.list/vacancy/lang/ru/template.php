<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "отзыв";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "отзыва";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "отзывов";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Далее";
$MESS["FAV_PHONE"] = "Телефоны";
$MESS["FAV_ADRESS"] = "Адрес";
$MESS["FAV_DELETE"] = "Удалить";
$MESS["WAGES"] = "Зарплата";
$MESS["EXPERIENCE"] = "Опыт";
$MESS["EDUCATION"] = "Образование";
?>