<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["NO_COMMENTS"] = "No one's writing, you can be the first";
$MESS ["comments_review_comment_new_brawser_doesnt_support_video"] = "Your browser does not support video playback";
$MESS ["comments_review_comment_new_answer"] = "reply";
$MESS ["comments_title"] = "Comments";
?>