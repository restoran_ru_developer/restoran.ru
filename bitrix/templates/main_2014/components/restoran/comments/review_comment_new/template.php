<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script>
    $(function(){
        $(".fancy").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200,
            'speedOut'		:	200,
            //'overlayShow'	:	false
        });
    })
</script>

<?
global $child;
global $params;
$params = $arParams;
$child = $arResult["CHILDS"];
function getChilds($id)
{
    global $child;
    global $params;
    global $APPLICATION;
    global $USER;
    foreach ($child[$id] as $commKey=>$commVal):
        ?>
        <div class="media">
            <a class="pull-left" href="#">
                <div class="avatar"><img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" /></div>
            </a>
            <div class="media-body">
                <div class="date"><a href="/users/id<?=$commVal["CREATED_BY"]?>/"><?=($commVal["NICKNAME"])?$commVal["NICKNAME"]:$commVal["USER_NAME"]?></a>, <?=$commVal["FORMATED_DATE_1"]?> <?=$commVal["FORMATED_DATE_2"]?></div>
                <p><?=$commVal["PREVIEW_TEXT"]?></p>
                <?if ($commVal["PROPERTIES"]["photos"]):?>
                <div class="review_atachment">
                    <?endif;?>
                    <?foreach($commVal["PROPERTIES"]["photos"] as $photo):?>
                        <div class="pic"><a href="<?=$photo["SRC"]?>" class="fancy" rel="group<?=$commVal["ID"]?>"><img src="<?=$photo["SRC"]?>" height="" /></a></div>
                    <?endforeach;?>
                    <?foreach($commVal["PROPERTIES"]["video"] as $photo):?>
                        <div class="pic">
                            <video src="<?=$photo["SRC"]?>" width="340" controls><?=GetMessage('comments_review_comment_new_brawser_doesnt_support_video')?></video>
                        </div>
                    <?endforeach;?>
                    <?if ($commVal["PROPERTIES"]["photos"] || $commVal["PROPERTIES"]["video"]):?>
                </div>
            <?endif;?>
                <a href="javascript:void(0)" class="answer_link" onclick="show_close_textarea(<?=$commVal["ID"]?>)"><?=GetMessage('comments_review_comment_new_answer')?></a>
                <div class="review_comment_add review_comment_add<?=$commVal["ID"]?>" style="display:none;">
                    <?
                    $APPLICATION->IncludeComponent(
                        "restoran:comments_add_new",
                        "new",
                        Array(
                            "IBLOCK_TYPE" => $params["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $params["IBLOCK_ID"],
                            "ELEMENT_ID" => $params["ELEMENT_ID"],
                            "IS_SECTION" => "N",
                            "PARENT" => $commVal["ID"]
                        ),false
                    );?>
                </div>
                <?
                if ($child[$commVal["ID"]])
                    getChilds($commVal["ID"]);
                ?>
            </div>
        </div>
    <?
    endforeach;
}
?>
<div class="comments-list">
    <?if (sizeof($arResult["COMMENTS"])>0):?>
        <div class="h1_style"><?=GetMessage('comments_title')?></div>
        <?foreach($arResult["COMMENTS"] as $commKey=>$commVal):?>
            <div class="media">
                <a class="pull-left" href="#">
                    <div class="avatar"><img src="<?=$commVal["AVATAR"]["src"]?>" alt="<?=$commVal["NAME"]?>" /></div>
                </a>
                <div class="media-body">
                    <div class="date"><a href="/users/id<?=$commVal["CREATED_BY"]?>/"><?=($commVal["NICKNAME"])?$commVal["NICKNAME"]:$commVal["USER_NAME"]?></a>, <?=$commVal["FORMATED_DATE_1"]?> <?=$commVal["FORMATED_DATE_2"]?></div>
                    <p><?=$commVal["PREVIEW_TEXT"]?></p>
                    <?if ($commVal["PROPERTIES"]["photos"]):?>
                    <div class="review_atachment">
                        <?endif;?>
                        <?foreach($commVal["PROPERTIES"]["photos"] as $photo):?>
                            <div class="pic">
                                <a href="<?=$photo["SRC"]?>" class="fancy" rel="group<?=$commVal["ID"]?>">
                                    <?
                                    $file = CFile::GetFileArray($photo['ID']);
                                    $this_photo_prev = CFile::ResizeImageGet($file,array("height"=>124,"width"=>137),BX_RESIZE_IMAGE_EXACT,true)?>
                                    <img src="<?=$this_photo_prev['src']?>" height="" />
                                </a>
                            </div>
                        <?endforeach;?>
                        <?foreach($commVal["PROPERTIES"]["video"] as $photo):?>
                            <div class="pic">
                                <video src="<?=$photo["SRC"]?>" width="340" controls><?=GetMessage('comments_review_comment_new_brawser_doesnt_support_video')?></video>
                            </div>
                        <?endforeach;?>
                        <?if ($commVal["PROPERTIES"]["photos"] || $commVal["PROPERTIES"]["video"]):?>
                    </div>
                <?endif;?>
                    <a href="javascript:void(0)" class="answer_link" data-toggle="toggle" data-target="review_comment_add<?=$commVal["ID"]?>"><?=GetMessage('comments_review_comment_new_answer')?></a>
                    <?
                    if ($child[$commVal["ID"]])
                    {
                        echo "<div class='dots' style='margin:10px 0px; height:1px'></div>";
                        getChilds($commVal["ID"]);
                    }
                    ?>
                </div>
                <div class="review_comment_add" id="review_comment_add<?=$commVal["ID"]?>" style="display:none;" iblock_id="<?=$params["IBLOCK_ID"]?>">
                    <?
                    $APPLICATION->IncludeComponent(
                        "restoran:comments_add_new",
                        "new",
                        Array(
                            "IBLOCK_TYPE" => $params["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $params["IBLOCK_ID"],
                            "ELEMENT_ID" => $params["ELEMENT_ID"],
                            "IS_SECTION" => "N",
                            "PARENT" => $commVal["ID"]
                        ),false
                    );?>
                </div>
            </div>
        <?endforeach?>
    <?else:?>
        <?if (!CSite::InGroup(Array(9))):?>
            <?=GetMessage("NO_COMMENTS")?>
        <?endif;?>
    <?endif;?>
</div>