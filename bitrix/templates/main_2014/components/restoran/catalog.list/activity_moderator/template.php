<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>
    function add_activity(data)
    {
        $("#user_activity").html(data);
        $(".more-activity").remove();
    }

    function getNextP(thisPageNum){
        $.ajax({
            type: "POST",
            url: "/bitrix/templates/main/components/restoran/catalog.list/activity_moderator/ajax.php",
            data: {
                PAGEN_1:thisPageNum, 
                id:id
            },
            success: function(data) {
                $('#processing').val(0);
                $('.ajax_container').hide();
                $(".activity_details_container").append(data);
            }
        });
    }

    $(document).ready(function(){
        id = $('#createid').val();
        var scrH = $(window).height();
        var scrHP = $(".activity_details_container").height();
        var thisPageNum = 1;
        var thisWork = 1;
        var maxPage = $('#maxpage').val();
        $('.activity_details_box').scroll(function(){
            var scro = $(this).scrollTop();
            var scrHP = $(".activity_details_container").height();
            var scrH2 = 0;
            scrH2 = scrH + scro;
            var leftH = scrHP - scrH2;
            if(leftH < -50){
                if (thisPageNum < maxPage) {
                    if($('#processing').val() == 0){
                        $('#processing').val(1);
                        $('.ajax_container').show();
                        thisPageNum = thisPageNum+1;
                        getNextP(thisPageNum);
                    }
                }            
            }
        });
    });
</script>
<input id="processing" type="hidden" value="0">

<input id="maxpage" type="hidden" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
<input id="createid" type="hidden" value="<?= $_REQUEST['id'] ?>">
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
</div>
<br />
<div class="activity_details_box">
    <div class="activity_details_container">
        <? if (count($arResult["ITEMS"]) > 0): ?>
            <table class="profile-activity" width="100%" cellpadding="10">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?> 
                    <tr>
                        <td width="150" class="profile-activity-col1"><p><?= GetMessage("TYPE_" . $arItem["IBLOCK_TYPE_ID"]) ?>
                                <br /><?= $arItem["CREATED_DATE_FORMATED_1"] ?></p>
                            <? if ($arItem["TRANSACT"]): ?>
                                <p><i><?= $arItem["TRANSACT"]["DESCRIPTION"] ?></i></p>
                            <? endif; ?>                    
                        </td>
                        <td width="20"><?= ($arItem["ACTIVE"] == "Y") ? "<img src='" . $templateFolder . "/images/lamp_active.png' width='20' />" : "<img src='" . $templateFolder . "/images/lamp_inactive.png' width='20' />" ?></td>
                        <!--<td class="profile-activity-col11 <?= (end($arResult["ITEMS"]) == $arItem) ? "end" : "" ?>">
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" width="100" />
                        </td>-->
                        <td width="150" class="profile-activity-col2 <?= (end($arResult["ITEMS"]) == $arItem) ? "end" : "" ?>">                            
                            <p><a class="another" target="_blank" href="http://restoran.ru<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></p>
                            <p class="upcase"><?= GetMessage("LINKTYPE_" . $arItem["LINK_IBLOCK_TYPE"]) ?></p>
                        </td>
                        <td width="200" class="profile-activity-col3 <?= (end($arResult["ITEMS"]) == $arItem) ? "end" : "" ?>"><?= $arItem["PREVIEW_TEXT"] ?></td>
                    </tr>    
                <? endforeach; ?>
            </table>
        <? else: ?>
            <h3 style="color:#FFF">Возможно активность была удалена, как спам</h3>
        <? endif; ?>
    </div>
    <div class="ajax_container"><div class="ajax_loader"></div></div>
</div>