<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // resize images
    if(!$arItem["PREVIEW_PICTURE"]) {    
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 200, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    } else {    
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 200, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    }
    if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
    
    if($arItem["IBLOCK_TYPE_ID"] == "firms_news")
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("msk",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    }
    if (!$arItem["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    else
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //get props if it is a restaurant
    if ($arParams["REST_PROPS"]=="Y"):        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
        while ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
        if ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
        }
        
//        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
//        if ($ob = $res->GetNext())
//        {
//            $r = CIBlockElement::GetByID($ob['VALUE']);
//            if ($a = $r->Fetch())
//                $arResult["ITEMS"][$key]["SUBWAY"] = $a['NAME'];
//        }
    endif;
}
global $arSimilar;
if (is_array($arSimilar["PROPERTY_rest_group"]))
    $arSimilar["PROPERTY_rest_group"] = $arSimilar["PROPERTY_rest_group"][0];
if ($arSimilar["PROPERTY_rest_group"])
{    
    $res = CIBlockElement::GetByID((int)$arSimilar["PROPERTY_rest_group"]);
    if ($ar = $res->Fetch())
    {
        $arResult["REST_GROUP"] = $ar["CODE"];
    }
}
?>