<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="near-rest-slider-wrap">
        <?if(count($arResult["ITEMS"])>4):?>
            <div class="prev-slide icon-arrow-left2 group-prev-slide"></div>
            <div class="next-slide icon-arrow-right2 group-next-slide"></div>
        <?endif?>
        <div class="near-rest-slider group-slider <?if(count($arResult['ITEMS'])<=4):FirePHP::getInstance()->info('no-four-slide');?>no-four-slide<?endif?>">
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="pull-left">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="pic">
            <a href="http://restoran.ru<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
        </div>
        <?endif;?>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <?if ($arParams["REST_PROPS"]):?>            
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>                      
                </div>
                <div class="props">
                    <div class="prop">
                        <div class="name">Кухня:</div>
                        <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                    </div>
                    <div class="prop">
                        <div class="name">Средний счет:</div>
                        <div class="value"><?=$arItem["BILL"]?></div>
                    </div>                                                            
                </div>
            <?else:?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <?endif;?>            
            <?if ($arItem==end($arResult["ITEMS"])):?>                
<!--                <div class="more_links text-right">                        -->
<!--                    <a href="/--><?//=CITY_ID?><!--/catalog/group/--><?//=$arResult["REST_GROUP"]?><!--/" class="btn btn-light">--><?//=GetMessage("ALL_NEWS")?><!--</a>-->
<!--                </div>-->
            <?endif;?>
        </div>        
    </div>
<?endforeach;?>
        </div>
    </div>