<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    foreach($arItem['PROPERTIES']['photos'] as $photo_id){
        $arFile = CFile::GetFileArray($photo_id);
        $arResult["ITEMS"][$key]['REVIEW_PHOTOS'][$photo_id]['FULL_SIZE'] = $arFile;
        if($arFile){
            $arResult["ITEMS"][$key]['REVIEW_PHOTOS'][$photo_id]['CROP_SIZE'] = CFile::ResizeImageGet($arFile, array('width'=>81, 'height'=>81), BX_RESIZE_IMAGE_EXACT, true, Array());
        }
    }
}
?>