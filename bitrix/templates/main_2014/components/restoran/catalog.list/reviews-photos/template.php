<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="reviews-photo-container">
    <ul>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <?foreach($arResult["ITEMS"][$key]['REVIEW_PHOTOS'] as $review_photo_obj):?>
        <li>
            <a class="fancybox" rel="group" href="<?=$review_photo_obj['FULL_SIZE']['SRC']?>">
                <img src="<?=$review_photo_obj['CROP_SIZE']['src']?>" width="81" height="" alt="">
            </a>
        </li>
    <?endforeach;?>
<?endforeach?>
    </ul>
</div>
