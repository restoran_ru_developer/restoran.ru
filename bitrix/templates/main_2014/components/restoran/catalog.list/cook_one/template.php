<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt">           
        <?if ($arItem["VIDEO"]):?>
                <script src="/tpl/js/flowplayer.js"></script>
                <a class="myPlayer1" href="<?=$arItem["VIDEO"]?>"></a> 
                <script>
                    flowplayer("a.myPlayer1", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                        clip: {
                                autoPlay: false, 
                                autoBuffering: true
                        },
                        plugins:  {
                                controls:  {
                                        volume: false,
                                        time: false
                                }
                        }
                    });
                </script>
        <?elseif ($arItem["VIDEO_PLAYER"]):?>
            <?=$arItem["VIDEO_PLAYER"]?>
        <?else:?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a>
        <?endif;?>
        <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>                        
        <div class="clear"></div>           
        <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <div style="margin-top:10px">
            <div class="left"><?=GetMessage("R_COMMENTS")?>: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
            <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>                
            <div class="clear"></div>
        </div>
    </div>
    <?if (end($arResult["ITEMS"])!=$arItem):?>
        <div class="dotted"></div>
    <?endif;?>
<?endforeach;?>