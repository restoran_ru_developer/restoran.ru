<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "review";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "reviews";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "reviews";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "More";
$MESS["SHOW_CNT_TITLE"] = "Display";
$MESS["SORT_NEW_TITLE"] =" by newest";
$MESS["SORT_POPULAR_TITLE"] =" by popular";
$MESS["R_COMMENTS"] ="Comments";
?>