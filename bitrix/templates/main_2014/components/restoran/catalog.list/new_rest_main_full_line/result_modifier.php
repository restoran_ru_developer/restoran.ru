<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

//  сортировка от фильтрующего массива
if($arParams['BEST_SORT_ARRAY']){
    foreach($arParams['BEST_SORT_ARRAY'] as $parent_news_id){
        foreach($arResult["ITEMS"] as  $key=> $one_item){
            if($one_item['ID']==$parent_news_id)
                $new_items_arr['ITEMS'][] = $one_item;
        }
    }
    $arResult["ITEMS"] = $new_items_arr['ITEMS'];
}

foreach($arResult["ITEMS"] as $key=>$arItem) {
    // resize images
    if($key==4 && $arParams['WITH_LAST_DOUBLE_NEWS']=='Y'){
        if(!$arItem["PREVIEW_PICTURE"]) {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 480, 'height' => 278), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
        } else {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 480, 'height' => 278), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
        }
        if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
    }
    else {
        if(!$arItem["PREVIEW_PICTURE"]) {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
        } else {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
        }
        if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
    }

    
    if($arItem["IBLOCK_TYPE_ID"] == "firms_news")
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("msk",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    }
    if (!$arItem["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    else
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //get props if it is a restaurant
    if ($arParams["REST_PROPS"]=="Y"):

        if(LANGUAGE_ID!='en'){
            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
            while ($ob = $res->GetNext())
            {
                $r = CIBlockElement::GetByID($ob['VALUE']);
                if ($a = $r->Fetch())
                    $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];
            }

            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
            if ($ob = $res->GetNext())
            {
                $r = CIBlockElement::GetByID($ob['VALUE']);
                if ($a = $r->Fetch())
                    $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
            }

            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
            if ($ob = $res->GetNext())
            {
                $r = CIBlockElement::GetByID($ob['VALUE']);
                if ($a = $r->Fetch())
                    $arResult["ITEMS"][$key]["SUBWAY"] = $a['NAME'];
            }
        }
        else {
            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
            while ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetList(Array("SORT"=>"ASC"),array('ID'=>$ob['VALUE']),false,false,array('PROPERTY_eng_name'));
                if ($a = $r->Fetch()){
                    $arResult["ITEMS"][$key]["KITCHEN"][] = $a['PROPERTY_ENG_NAME_VALUE'];
                }
            }
            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
            if ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetList(Array("SORT"=>"ASC"),array('ID'=>$ob['VALUE']),false,false,array('PROPERTY_eng_name'));
                if ($a = $r->Fetch()){
                    $arResult["ITEMS"][$key]["BILL"] = $a['PROPERTY_ENG_NAME_VALUE'];
                }
            }
            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
            if ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetList(Array("SORT"=>"ASC"),array('ID'=>$ob['VALUE']),false,false,array('PROPERTY_eng_name'));
                if ($a = $r->Fetch()){
                    $arResult["ITEMS"][$key]["SUBWAY"] = $a['PROPERTY_ENG_NAME_VALUE'];
                }
            }
        }

    endif;
}

if ($arParams['IBLOCK_ID'] == 139 && $arParams['PARENT_SECTION']==57432) {
    $res = CIBlockElement::GetList(Array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'PROPERTY_FIFTH_RECEPT_VALUE' => 'Да'), false, false, array());
    if ($ob = $res->GetNext()) {
        $arResult["ITEMS"][$key + 1] = $ob;
        $res = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $ob["ID"], "sort", "asc", array("CODE" => "prig_time"));
        if ($ob = $res->Fetch()) {

            $res = CIBlockElement::GetById($ob['VALUE']);
            if ($ar_res = $res->Fetch()) {
                $arResult["ITEMS"][$key + 1]['PROPERTIES']['prig_time'][0] = $ar_res['NAME'];
            }
        }

        if (!$arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"]) {
            $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key + 1]["DETAIL_PICTURE"], array('width' => 480, 'height' => 278), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"]["src"];
        } else {
            $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"], array('width' => 480, 'height' => 278), BX_RESIZE_IMAGE_EXACT, true, Array());
            $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"]["src"];
        }
        if (!$arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"])
            $arResult["ITEMS"][$key + 1]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";

    }
}
?>