<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):

    $target = '';
    if($arItem['ID']==388078){
        $arItem["DETAIL_PAGE_URL"] = 'http://new.niyama.ru/';
        $target = 'target="_blank"';
    }
    ?>
    <div class="pull-left <?=(($key+1)%4==0&&$key!=0?"end":"")?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]&&$arResult["IBLOCK_TYPE_ID"]!="blogs"):?>
            <div class="pic <?if($key==4&&$arParams['WITH_LAST_DOUBLE_NEWS']=='Y'):?>last-double<?endif?>">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" <?=$target?>>
                    <img src="<?=$arItem["PREVIEW_PICTURE"]?>" />
                    <?if($arParams['WITH_LAST_DOUBLE_NEWS']=='Y'&&$key==4):?>
                        <div class="post-bottom-text">
                            <h2><?=$arItem["NAME"]?></h2>
                            <div class="post-bottom-text-time-wrapper">
                                <?=$arItem["PROPERTIES"]['prig_time'][0]?>
                            </div>
                        </div>
                    <?endif?>
                </a>
            </div>
        <?endif;?>
        <?if(($key!=4 && $arParams['WITH_LAST_DOUBLE_NEWS']=='Y') || empty($arParams['WITH_LAST_DOUBLE_NEWS'])):?>
            <div class="text">
                <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" <?=$target?>><?=$arItem["NAME"]?></a></h2>
                <?if ($arParams["REST_PROPS"]):?>
                    <div class="ratio">
                        <?for($z=1;$z<=5;$z++):?>
                            <span class="<?=(round($arItem["PROPERTIES"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                        <?endfor?>
                    </div>
                    <div class="props">
                        <?if ($arItem["KITCHEN"]):?>
                            <div class="prop">
                                <div class="name"><?=GetMessage('new_rest_main_kitchen')?></div>
                                <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                            </div>
                        <?endif;?>
                        <?if ($arItem["BILL"]):?>
                            <div class="prop">
                                <div class="name"><?=GetMessage('new_rest_main_average_bill')?></div>
                                <div class="value"><?=$arItem["BILL"]?></div>
                            </div>
                        <?endif;?>
                        <?if ($arItem["SUBWAY"]):?>
                            <div class="prop wsubway">
                                <div class="subway">M</div>
                                <div class="value"><?=$arItem["SUBWAY"]?></div>
                            </div>
                        <?endif;?>
                    </div>
                <?else:?>
                    <p><?=$arItem["PREVIEW_TEXT"]?></p>
                <?endif;?>
                <?if($arResult["IBLOCK_TYPE_ID"]=="catalog"):?>
                    <div class="more_links">
                        <div class="pull-left">
                            <i class="serif"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
                        </div>

                        <div class="pull-right"><a class="serif" href="<?=$arItem["DETAIL_PAGE_URL"]?>" <?=$target?>><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                        <div class="clearfix"></div>
                    </div>
                <?else:?>
                    <div class="more_links">
                        <div class="pull-left comments_link">
                            <?=GetMessage("COMMENTS_TITLE")?> &ndash; <a href="<?=$target?$arItem["DETAIL_PAGE_URL"]:$arItem["DETAIL_PAGE_URL"].'#comments'?>" <?=$target?>><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>
                        </div>
                        <div class="pull-right"><a class="serif" href="<?=$arItem["DETAIL_PAGE_URL"]?>" <?=$target?>><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                        <div class="clearfix"></div>
                    </div>
                <?endif;?>
            </div>
        <?endif?>
    </div>
    <?=((($key+1)%4==0&&$key!=0)||$arItem==end($arResult["ITEMS"]))?"<div class='clearfix'></div>":""?>

    <?if ($arItem==end($arResult["ITEMS"])&&!$arParams["NO_LINK"]):?>
        <div class="more_links text-right">
            <?if ($arParams["SORT_BY1"]=="PROPERTY_RATIO")
                $aa = "ratio";
            elseif($arParams["SORT_BY1"]=="show_counter")
                $aa = "popular";
            elseif($arParams["SORT_BY1"]=="PROPERTY_restoran_ratio")
                $aa = "recomended";
            elseif($arParams["SORT_BY1"]=="PROPERTY_rating_date")
                $aa = "popular";
            else
                $aa = "";



            if ($aa):
                ?>
                <a href="/<?=CITY_ID?>/ratings/#<?=$aa?>" class="btn btn-light"><?=GetMessage("ALL_NEWS")?></a>
            <?elseif($arItem["IBLOCK_TYPE_ID"] == "firms_news"):?>
                <a href="http://restoran.ru/<?=CITY_ID?>/news/restoratoram/" class="btn btn-light"><?=GetMessage("ALL_NEWS")?></a>
            <?elseif(substr_count($arResult["SECTION"]["PATH"][0]["CODE"], "newplace") || substr_count($arResult["SECTION"]["PATH"][0]["CODE"], "novye") || substr_count($arResult["SECTION"]["PATH"][0]["CODE"], "fotootchety")):?>
                <a href="<?=$arResult["SECTION"]["PATH"][0]["SECTION_PAGE_URL"]?>" class="btn btn-light"><?=GetMessage("ALL_NEWS1")?></a>
            <?elseif ($arParams["NEW_REST"]):?>
                <a href="/<?=CITY_ID?>/catalog/restaurants/all/?page=1&pageRestSort=new&by=desc" class="btn btn-light"><?=GetMessage("ALL_NEW_REST")?></a>
            <?elseif($arParams["REST_PROPS"]=="Y"):?>
                <a href="<?=$arResult["SECTION"]["PATH"][0]["SECTION_PAGE_URL"]?>" class="btn btn-light"><?=GetMessage("ALL_NEWS1")?></a>
            <?elseif (!$arParams["NOLINK"]):?>
                <?if ($arParams["IBLOCK_TYPE"]=="cookery"&&$arParams["IBLOCK_ID"]==145):?>
                    <a class="btn btn-light" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>"><?=GetMessage("ALL_".$arParams["IBLOCK_TYPE"]."m")?></a>
                <?elseif ($arParams["IBLOCK_TYPE"]=="cookery"&&$arParams["A"]!="a"):?>
                    <a class="btn btn-light" href="<?=$arResult["SECTION"]["PATH"][0]["SECTION_PAGE_URL"]?>"><?=GetMessage("ALL_".$arParams["IBLOCK_TYPE"])?></a>
                <?else:?>
                    <a class="btn btn-light" href="<?=$arParams["LINK"]?>"><?=$arParams["LINK_NAME"]?></a>
                <?endif;?>
            <?endif;?>
        </div>
    <?endif;?>
<?endforeach;?>
