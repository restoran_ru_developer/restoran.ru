<?
if ($arResult["RESTORAN"])
{
    global $APPLICATION;
    $APPLICATION->SetTitle("Отзывы о ресторане ".$arResult["RESTORAN"]);
    $APPLICATION->SetPageProperty("keywords",  "отзывы, ресторан, ".$arResult["RESTORAN"]);

    $city_arr_for_desc = array(
        'msk'=>"Москве",
        'spb'=>"Санкт-Петербурге",
        'rga'=>"Риге",
        'urm'=>"Юрмале",
        'kld'=>"Калиниграде",
        'nsk'=>"Новосибирске",
        'sch'=>"Сочи"
    );

    $APPLICATION->SetPageProperty("description",  "Все отзывы посетителей о ресторане ".$arResult["RESTORAN"]." в ".$city_arr_for_desc[CITY_ID]);
}?>