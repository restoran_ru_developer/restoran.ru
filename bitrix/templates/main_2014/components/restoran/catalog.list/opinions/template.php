<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
<div class="reviews-list">
<?endif;?>
    <?if ($arResult["ITEMS"][0]["ID"]):?>
        <?foreach ($arResult["ITEMS"] as $key=>$arItem):?>
            <div class="item">
                <div class="rest_name">
                    <?if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"]=="Y"):?>
                        <h2><a target="_blank" href="<?=$arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"]?>" title="<?=GetMessage('OPTIONS_LINK_TITLE_REST')?> <?=$arItem["RESTORAN"]["NAME"]?>"><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></a></h2>
                    <?else:?>
                        <h2><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></h2>
                    <?endif;?>
                    <Br />
                    <?=$arResult["ITEMS"][$key]["RESTORAN"]["IBLOCK_NAME"]?><br />
                    <!--<div class="subway-icon">Пушкинская, Чеховская</div>-->
                    <a target="_blank" href="<?=str_replace("detailed","opinions",$arItem["RESTORAN"]["DETAIL_PAGE_URL"])?>" class="all_rest_reviews" title="<?=GetMessage('OPTIONS_LINK_TITLE_REST_REVIEWS')?> <?=$arItem["RESTORAN"]["NAME"]?>"><?=GetMessage('ALL_REVIEWS_catalog')?></a>
                </div>
                <div class="author">
                    <div class="avatar"><img src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" /></div>
                    <div class="date">
                        <?if ($arItem["CREATED_BY"]):?>
                            <a target="_blank" href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>, <?=$arItem["DISPLAY_ACTIVE_FROM_TIME"]?>
                        <?else:?>
                            <a target="_blank" href="javascript:void(0)"><?=$arItem["NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>, <?=$arItem["DISPLAY_ACTIVE_FROM_TIME"]?>
                        <?endif;?>
                    </div>
                    <div class="ratio">
                        <?for($i=1;$i<=5;$i++):?>
                            <span class="icon-star<?=($i>$arItem["DETAIL_TEXT"])?"-empty":""?>"></span>
                        <?endfor;?>
                    </div>
                </div>
                <div class="attachments">
                    <?if(count($arItem["PROPERTIES"]["photos"])&&$arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                        <div class="photo-icon" onclick="$('a.fancy[rel=group<?=$key?>]:first-child').click();"><?=count($arItem["PROPERTIES"]["photos"])?> фото</div>
                    <?endif;?>
                    <?
                    $str = $arItem["PROPERTIES"]["video_youtube"];
                    preg_match_all('/v=(.*)http/sU', $str, $matches);
                    $last_matche = end(explode('v=', $str));
                    if ($matches[1]) {
                        $all_matches = $matches[1];
                    }
                    else {
                        $all_matches = array();
                    }
                    if($last_matche[0]){
                        array_push($all_matches, $last_matche);
                    }
                    if(!$arItem["PROPERTIES"]["video"][0]){
                        unset($arItem["PROPERTIES"]["video"][0]);
                    }
                    ?>
                    <?if((count($arItem["PROPERTIES"]["video"])&&$arItem["PROPERTIES"]["video"][0]) || $arItem["PROPERTIES"]['video_youtube']):?>
                        <a target="_blank" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><div class="video-icon"><?=count($arItem["PROPERTIES"]["video"])+count($all_matches)?> видео</div></a>
                    <?endif;?>
                </div>
                <div class="clearfix"></div>


                <p>
                <div class="preview_text<?=$arItem['ID']?>">
                    <?=$arItem["PREVIEW_TEXT2"]?>
                </div>
                <div class="preview_text<?=$arItem['ID']?>" style="display:none;">
                    <?=$arItem["PREVIEW_TEXT"]?>
                </div>
                </p>
                <?if(strlen(strip_tags($arItem["PREVIEW_TEXT"]))>400):?>
                    <div class="left">
                        <div class="razver razver<?=$arItem['ID']?>" onclick="$('.preview_text<?=$arItem['ID']?>').toggle(); $('.razver<?=$arItem['ID']?>').toggle();">Развернуть<span class="caret"></span></div>
                        <div class="razver razver<?=$arItem['ID']?>" onclick="$('.preview_text<?=$arItem['ID']?>').toggle(); $('.razver<?=$arItem['ID']?>').toggle();" style="display: none;">Свернуть<span class="caret"></span></div>
                    </div>
                <?endif?>

                <!--            <div class="plus">
                                <b>достоинства</b>
                                Уютный интерьер, вкусный салат, вежливый персонал
                            </div>
                            <div class="minus">
                                <b>недостатки</b>
                    Уютный интерьер, вкусный салат
                            </div>-->
                <div class="clearfix"></div>
                <?if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                    <div class="review_atachment">
                        <?foreach($arItem["PROPERTIES"]["photos"] as $photo_key=>$photo):?>
                            <div class="pic"><a class="fancy" rel="group<?=$key?>" href="<?=$photo["ORIGINAL_SRC"]["src"]?>">
                                    <img src="<?=$photo["SRC"]?>" alt="<?=$photo["ORIGINAL_SRC"]["src"]?>" /></a></div>
                        <?endforeach;?>
                    </div>
                <?endif;?>
                <a target="_blank" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="answer_link"><?=GetMessage("REVIEWS_ANSWER")?></a>
                <a target="_blank" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="answer_link">
                    Ответов &ndash; <span><?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?></span>
                </a>
                <?if($arItem['SHOW_COUNTER']):?><div class="counter-inf-wrapper"><div class="news-viewed-counter-wrapper"><?=$arItem['SHOW_COUNTER']?></div></div><?endif;?>
            </div>
        <?endforeach;?>
    <?else:?>
        <?if (!CSite::InGroup(Array(9))):?>
            <p style="font-style:italic"><?=GetMessage("NO_REVIEWS")?></p>
        <?endif;?>
    <?endif;?>
    <?if ($arParams["DISPLAY_BOTTOM_PAGER"]=="Y"):?>
        <div class="navigation no-map-link" style="margin-top: 70px !important;">
            <?=$arResult["NAV_STRING"]?>
        </div>
    <?endif;?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
</div>
<?endif?>