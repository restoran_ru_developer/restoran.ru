<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(count($arResult["ITEMS"])>1&&$arParams['CENTER_POST']=='Y'){
    $tempItem = $arResult["ITEMS"][0];
    $arResult["ITEMS"][0] = $arResult["ITEMS"][1];
    $arResult["ITEMS"][1] = $tempItem;
}
foreach($arResult["ITEMS"] as $key=>$arItem):
    $temp_another_link = $arParams["ANOTHER_LINK"];
//    if($arItem['CODE']=='pivnoy_festival_oktoberfest_v_moskve')
//        $arParams["ANOTHER_LINK"] = '/msk/news/oktoberfest/';
//    else
    if($arItem['PROPERTIES']['LINK'])
        $arParams["ANOTHER_LINK"] = $arItem['PROPERTIES']['LINK'];
    else
        $arParams["ANOTHER_LINK"] = $temp_another_link;
    ?>
    <?if(($arParams['CENTER_POST']=='Y'&&$arParams['NEWS_COUNT']==1)||($arParams['CENTER_POST']=='Y'&&$key==1)):?>
        <div class="pull-left center-index-news-post">
            <div class="pic">
                <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>"></a>
                <img src="<?=$arItem["DETAIL_PICTURE"]?>">
            </div>
            <div class="text">
                <div class="center-post-cut">
                    <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>"></a>
                    <h2><a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></h2>
                    <div class="more_links">
                        <div class="index-one-main-news-bottom-line">
                            <a href="<?=($arParams["ANOTHER_LINK"])?$arParams["ANOTHER_LINK"]:$arItem["SECTION_PAGE_URL"]?>" class="watch-all">
                                <?if($arParams["IBLOCK_TYPE"]!='cookery'):?>
                                    <?if(!$arParams['ALL_NEWS_TITLE']):?>
                                        <?=GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["IBLOCK_TYPE"])?>
                                    <?else:?>
                                        <?=GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["ALL_NEWS_TITLE"])?>
                                    <?endif?>
                                <?else:?>
                                    <?
                                    if($arParams['PARENT_SECTION']!=0){
                                        if($arParams['PARENT_SECTION']==57432){
                                            echo GetMessage("CT_BNL_ALL_NEWS_LINK_".'cookery_editor');
                                        }
                                        else {
                                            echo $arItem['USER_NAME'];
                                        }
                                    }
                                    else {
                                        echo GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["IBLOCK_TYPE"]);
                                    }
                                    ?>
                                <?endif?>
                            </a>

                            <?if($arItem['PROPERTIES']['COMMENTS']['VALUE']):?><div class="index-one-main-news-comments-counter-wrapper"><?=$arItem['PROPERTIES']['COMMENTS']['VALUE']?></div><?endif?>
                            <?if($arItem['SHOW_COUNTER']):?><div class="index-one-main-news-viewed-counter-wrapper"><?=$arItem['SHOW_COUNTER']?></div><?endif;?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?else:?>
        <div class="pull-left <?if(end($arResult["ITEMS"])==$arItem&&$arParams['NEWS_COUNT']!=1):?>end<?endif?>">
            <div class="pic">
                <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>">
                    <img src="<?=$arItem["DETAIL_PICTURE"]?>"></a>
            </div>
            <div class="text">
                <div class="center-post-cut">
                    <h2><a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></h2>
                    <p class="about-news-wrapper"><a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["DETAIL_PAGE_URL"]?>"><?if($arItem['USER_PHOTO']):?><img src="<?=$arItem['USER_PHOTO']['src']?>" width="" height="" alt=""><?endif;?><?=$arItem['PREVIEW_TEXT']?></a></p>
                </div>
                <div class="more_links">
                    <div class="index-one-main-news-bottom-line">
                        <?if($arParams["IBLOCK_TYPE"]=='afisha') $arItem["SECTION_PAGE_URL"] = '/'.CITY_ID.'/afisha/';?>
                        <?if($arParams["IBLOCK_TYPE"]=='blogs') $arItem["SECTION_PAGE_URL"] = $arParams['LINK']?$arParams['LINK']:$arItem["SECTION_PAGE_URL"];?>
                        <a href="<?=$arParams["ANOTHER_LINK"]?$arParams["ANOTHER_LINK"]:$arItem["SECTION_PAGE_URL"]?>" class="watch-all">
                            <?if($arParams["IBLOCK_TYPE"]!='cookery'):?>
                                <?if(!$arParams['ALL_NEWS_TITLE']):?>
                                    <?=GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["IBLOCK_TYPE"])?>
                                <?else:?>
                                    <?=GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["ALL_NEWS_TITLE"])?>
                                <?endif?>
                            <?else:?>
                                <?
                                if($arParams['PARENT_SECTION']!=0){
                                    if($arParams['PARENT_SECTION']==57432){
                                        echo GetMessage("CT_BNL_ALL_NEWS_LINK_".'cookery_editor');
                                    }
                                    else {
                                        echo $arItem['USER_NAME'];
                                    }
                                }
                                else {
                                    echo GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["IBLOCK_TYPE"]);
                                }
                                ?>
                            <?endif?>
                        </a>

                        <?if($arItem['PROPERTIES']['COMMENTS']['VALUE']):?><div class="index-one-main-news-comments-counter-wrapper"><?=$arItem['PROPERTIES']['COMMENTS']['VALUE']?></div><?endif?>
                        <?if($arItem['SHOW_COUNTER']):?><div class="index-one-main-news-viewed-counter-wrapper"><?=$arItem['SHOW_COUNTER']?></div><?endif;?>
                    </div>
                </div>
            </div>
        </div>
    <?endif?>


    <?if(($key+1)%4==0 &&$arItem!=end($arResult["ITEMS"])&&$arParams['NEWS_COUNT']!=1&&$arParams['CENTER_POST']!='Y'):?>
        <div class="clearfix"></div>
    <?elseif($arParams['CENTER_POST']=='Y'&&($key+1)%3==0&&($key+2)!=count($arResult["ITEMS"])):?>
        <div class="clearfix"></div>
    <?endif?>
<?endforeach;?>
