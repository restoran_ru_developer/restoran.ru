<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
//    FirePHP::getInstance()->info($arItem['NAME']);


    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["NAME"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["NAME"]);

    if ($arItem["DETAIL_PICTURE"])
    {
        if($arParams['CENTER_POST']=='Y'&&$arParams['NEWS_COUNT']==1){
            $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>480, 'height'=>305), BX_RESIZE_IMAGE_EXACT, false, Array(),false, 72);
        }
        elseif($arParams['CENTER_POST']=='Y'&&$arParams['NEWS_COUNT']!=1&&$key==0){
            $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>480, 'height'=>305), BX_RESIZE_IMAGE_EXACT, false, Array(),false, 72);
        }
        else {
            $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>232, 'height'=>127), BX_RESIZE_IMAGE_EXACT, false, Array(),false, 72);
        }

        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = $arResult["ITEMS"][$key]["DETAIL_PICTURE"]["src"];       
    }
    else
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = "/tpl/images/noname/rest_nnm.png";
    }
    // get rest name    
    if ($arItem["PROPERTIES2"]["RESTORAN"][0])
    {
        $res = CIBlockElement::GetByID($arItem["PROPERTIES2"]["RESTORAN"][0]);
        if ($ar = $res->GetNext())
        {
            $arResult["ITEMS"][$key]["RESTORAN_NAME"] =  $ar["NAME"];
            $arResult["ITEMS"][$key]["RESTORAN_LINK"] =  $ar["DETAIL_PAGE_URL"];
        }
    }

    if($arParams['IBLOCK_TYPE']=='cookery'&&$arParams['PARENT_SECTION']==57431){
        $res = CUser::GetByID($arItem["CREATED_BY"]);
        if ($ar = $res->Fetch())
        {
            $arResult["ITEMS"][$key]["USER_NAME"] = $ar["PERSONAL_PROFESSION"];
            if (!$arResult["ITEMS"][$key]["USER_NAME"])
            {
                $temp = explode("@",$ar["EMAIL"]);
                $arResult["ITEMS"][$key]["USER_NAME"] = $temp[0];
            }
        }
    }
//    $res = CUser::GetById($arItem['CREATED_BY']);
//    if ($uInfo = $res->GetNext())
//    {
//        if ($uInfo['PERSONAL_PHOTO'])
//        {
//            $file = CFile::ResizeImageGet($uInfo['PERSONAL_PHOTO'], array('width'=>60, 'height'=>60), BX_RESIZE_IMAGE_EXACT, false);
//            $arResult["ITEMS"][$key]['USER_PHOTO'] = $file;
//        }
//    }
}
?>