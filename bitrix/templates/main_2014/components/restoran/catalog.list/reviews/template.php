<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <div class="pull-left <?=($arItem==end($arResult["ITEMS"]))?"end":""?>">
        <div class="text">
            <div class="date">
                <?if ($arItem["CREATED_BY"]):?>
                    <a href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["USER_NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <?else:?>
                    <a href="javascript:void(0)"><?=$arItem["USER_NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <?endif;?>
            </div>
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=$arItem["RESTAURANT_NAME"]?></a></h2>
            <div class="ratio">
                <?for($z=1;$z<=5;$z++):?>
                    <span class="<?=(round($arItem["DETAIL_TEXT"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                <?endfor?>
            </div>
            <div class="review">
                <?=$arItem["PREVIEW_TEXT"]?>
                <a class="read_full_message" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("ALL_REVIEWS_further")?></a>
            </div>
            <div class="reviews_links">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="pull-left">
                    Комментировать
                </a>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="pull-right">
                    <b>Ответов &ndash;</b> <?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?>
                </a>
            </div>
        </div>
        <?if ($_REQUEST["wr"]!="Да"):?>
            <?if ($arItem==end($arResult["ITEMS"])):?>
                <div class="more_links text-right">
                    <a href="<?=str_replace("detailed","opinions",$_REQUEST["url"])?>" class="btn btn-light"><?=GetMessage("ALL_REVIEWS_catalog")?></a>
                </div>
            <?endif;?>
        <?endif;?>
    </div>
<?endforeach?>
<div class="clearfix"></div>