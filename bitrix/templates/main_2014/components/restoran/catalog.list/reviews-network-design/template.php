<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <div class="comment-str">
        <?if($arParams['IN_NETWORK']=='Y'):?>
        <div class="reviews-in-network-top-inf-wrapper">
            <div class="reviews-rest-name"><a href="<?=$arItem['REVIEW_REST_URL']?>"><?=$arItem['RESTAURANT_NAME']?></a></div>
            <div class="reviews-rest-address"><?=$arItem['REVIEW_REST_ADDRESS']?></div>
        </div>
        <?endif?>
        <div class="text">
            <div class="date">
                <?if ($arItem["CREATED_BY"]):?>
                    <a href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["USER_NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <?else:?>
                    <a href="javascript:void(0)"><?=$arItem["USER_NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>
                <?endif;?>
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["DETAIL_TEXT"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>
                </div>
            </div>
            <div class="review">
                <?=$arItem["PREVIEW_TEXT"]?>
                <a class="read_full_message" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("ALL_REVIEWS_further")?></a>
            </div>
            <div class="reviews_links">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="">
                    <b>Комментировать</b>
                </a>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments" class="">
                    <b>Ответов &ndash;</b> <?=intval($arItem["PROPERTIES"]["COMMENTS"]["VALUE"])?>
                </a>
            </div>
        </div>
        <?if ($_REQUEST["wr"]!="Да"):?>
            <?if ($arItem==end($arResult["ITEMS"])):?>
                <div class="more_links text-right">
                    <?if($arParams['IN_NETWORK']!='Y'):?><a tabindex="-1" data-toggle="toggle" data-target="comment_form" class="btn btn-info btn-nb-empty"><?=GetMessage("LEAVE_COMMENT")?></a><?endif;//&&$_REQUEST["id"]!=2022840?>
                    <a class="terms" href='/auth/user_license_agreement.php' target='_blank'><?=GetMessage("R_TERMS")?></a>
                    <a href="<?=str_replace("detailed","opinions",$_REQUEST["url"]).($arParams['IN_NETWORK']=='Y'?'?REST_NETWORK='.$_REQUEST['REST_NETWORK']:'')?>" class="btn btn-light"><?=GetMessage("ALL_REVIEWS_catalog")?></a>
                </div>
            <?endif;?>
        <?endif;?>
    </div>
<?endforeach?>
<div class="clearfix"></div>