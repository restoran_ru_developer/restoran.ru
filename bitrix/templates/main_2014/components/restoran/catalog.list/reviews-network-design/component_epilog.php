<?
if ($arResult["RESTORAN"])
{
    global $APPLICATION;
    $APPLICATION->SetTitle("Отзывы ресторана ".$arResult["RESTORAN"]);
    $s = "Отзывы ресторана «<a href='".$arResult["RESTORAN_URL"]."'>".$arResult["RESTORAN"]."</a>»";    
?>
    <script>
        $(document).ready(function(){
            $("#opinion_h1").html("<?=$s?>");
        });
    </script>
<?}?>

<script>
    $(function(){
        <?if(!$arResult['ITEMS']):?>
        $('#comment_form').show();
        <?endif?>
    })
</script>