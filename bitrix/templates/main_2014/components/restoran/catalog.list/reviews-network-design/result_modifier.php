<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // explode date
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ITEMS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["DATE"] = $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"]." ".$arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"]." ".$arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"];
    // get rest name
//    FirePHP::getInstance()->info($arItem["PROPERTIES_ORIGINAL"]["ELEMENT"],'ELEMENT');
    $rsSec = CIBlockElement::GetByID($arItem["PROPERTIES_ORIGINAL"]["ELEMENT"]["VALUE"]);
    if($arSec = $rsSec->GetNext()) {
        $arResult["ITEMS"][$key]["RESTAURANT_NAME"] = $arSec["NAME"];
        $arResult["ITEMS"][$key]["REVIEW_REST_URL"] = $arSec["DETAIL_PAGE_URL"];

        if($arParams['IN_NETWORK']=='Y'){
            $db_props = CIBlockElement::GetProperty($arSec['IBLOCK_ID'], $arItem["PROPERTIES_ORIGINAL"]["ELEMENT"]["VALUE"], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
                $arResult["ITEMS"][$key]['REVIEW_REST_ADDRESS'] = $ar_props["VALUE"];
            }
        }
    }


    
    $res = CUser::GetByID($arItem["CREATED_BY"]);
    if ($ar = $res->Fetch())
    {
        $arResult["ITEMS"][$key]["USER_NAME"] = $ar["PERSONAL_PROFESSION"];      
        if (!$arResult["ITEMS"][$key]["USER_NAME"])
        {
            $temp = explode("@",$ar["EMAIL"]);
            $arResult["ITEMS"][$key]["USER_NAME"] = $temp[0];
        }
    }
    else 
    {
        $temp = explode("@",$arResult["ITEMS"][$key]["NAME"]);
        $arResult["ITEMS"][$key]["USER_NAME"] = $temp[0];
    }
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);


}
?>