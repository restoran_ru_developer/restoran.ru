<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="item pull-left <?=($key%3==2)?"end":""?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
        </div>
        <?endif;?>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <?if ($arParams["REST_PROPS"]):?>            
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>                      
                </div>
                <div class="props">
                    <div class="prop">
                        <div class="name">Кухня:</div>
                        <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                    </div>
                    <div class="prop">
                        <div class="name">Средний счет:</div>
                        <div class="value"><?=$arItem["BILL"]?></div>
                    </div>                                                            
                </div>
            <?else:?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <?endif;?>                        
        </div>        
    </div>    
    <?=($key%3==2&&end($arResult["ITEMS"])!=$arItem)?"<div class='clearfix dots'></div>":""?>
<?endforeach;?>
<div class="clearfix"></div>