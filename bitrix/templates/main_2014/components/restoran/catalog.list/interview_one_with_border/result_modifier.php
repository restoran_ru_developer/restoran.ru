<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?



function get_file_av($matches)
{
    $photo = array();
    $photo = CFile::ResizeImageGet($matches,Array("width"=>66,"height"=>66),BX_RESIZE_IMAGE_EXACT,true,Array());
    return $photo["src"];
}

if ($arParams["SORT_BY1"]=="none")
{
    global $$arParams["FILTER_NAME"];
    $filter = $$arParams["FILTER_NAME"];
    $elem = array();
}

if($arParams["IBLOCK_TYPE"]=="cookery"){
    require($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/.default/components/restoran/editor2/editor/phpQuery-onefile.php');
}

foreach($arResult["ITEMS"] as $key=>&$arItem) {
    /*$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    if ($arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"])
    {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    }*/

    if($arParams["IBLOCK_TYPE"]=="cookery"){
        $matches = array();
        preg_match("/#FID_PR([0-9]+)#/",$arItem["DETAIL_TEXT"],$matches);

        if($matches[1]){
            $arItem["CHIEF_PHOTO"] = get_file_av($matches[1]);

            $document = phpQuery::newDocument($arItem["DETAIL_TEXT"]);
            if($chief_name_str = $document->find('.direct_speech')){
                $arItem['CHIEF_NAME']= preg_replace('/,/i','',trim($chief_name_str->find(".txt1")->text()));
            }
        }
    }



    
    if(!$arItem["PREVIEW_PICTURE"]) {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
    }    
    if (!$arResult["ITEMS"][$key]["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    
    if ($arParams["REST_PROPS"]=="Y"):          
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
        while ($ob = $res->Fetch())
        {            
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];    
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
        if ($ob = $res->Fetch())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
        if ($ob = $res->Fetch())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["SUBWAY"] = $a['NAME'];
        }
    endif;
    
    if ($arParams["SORT_BY1"]=="none")
        $elem[$arItem["ID"]] = $arResult["ITEMS"][$key];
        
}
if ($arParams["SORT_BY1"]=="none")
{
    $arResult["ITEMS"] = array();    
    foreach ($filter["ID"] as $id)
    {
        
        $arResult["ITEMS"][] = $elem[$id];
    }
}
?>