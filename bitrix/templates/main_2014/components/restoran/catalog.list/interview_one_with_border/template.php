<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="sm">                                    
<?
//FirePHP::getInstance()->info($arResult["ITEMS"][0]);
foreach($arResult["ITEMS"] as $key=>$arItem):

    ?>
    <div class="item <?=$arParams["IBLOCK_TYPE"]=="cookery"?"cookery-list-item":""?>">

        <?if($arParams["IBLOCK_TYPE"]=="cookery"):?>
            <?if($arItem['CHIEF_PHOTO']):?>
                <div class="chief-photo-wrapper">
                    <?$chief_name = explode(' ',$arItem['CHIEF_NAME']);?>
                    <div class="name"><?=$chief_name[0]?></div>
                    <div class="chief-photo"><img src="<?=$arItem['CHIEF_PHOTO']?>" width="66" height="66" alt=""></div>
                    <div class="name"><?=$chief_name[1]?></div>
                </div>
            <?endif?>
            <h2 class=""><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
        <?endif?>
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
            <div class="pic">                
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a><br />                
            </div>
        <?endif;?>
        <div class="text">
            <?if($arParams['IBLOCK_TYPE']=='afisha'):?>
                <div class="props">
                    <div class="prop afisha-blue-prop-name">
                        <div class="name"><?=$arItem['PROPERTIES']['EVENT_DATE']?$arItem['PROPERTIES']['EVENT_DATE']."&nbsp;&nbsp;&nbsp;&nbsp;":""?><?=$arItem['PROPERTIES']['TIME']?></div>
                    </div>
                </div>
            <?endif?>
            <?if($arParams["IBLOCK_TYPE"]!="cookery"):?>
                <h2 class="<?=$arParams['IBLOCK_TYPE']=='afisha'?"afisha-right-side-title":""?>"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <?endif?>
            <?if($arParams['IBLOCK_TYPE']=='afisha'):?>
                <?if($arItem['PROPERTIES']['RESTORAN'][0]):?>
                <div class="props">
                    <div class="prop">
                        <div class="name">Ресторан:</div>
                        <div class="value"><?=$arItem['PROPERTIES']['RESTORAN'][0]?></div>
                    </div>
                </div>
                <?endif?>
            <?endif?>
            <?if($arResult["IBLOCK_TYPE_ID"]!="afisha"):?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>         
            <?endif;?>
            <?if ($arItem==end($arResult["ITEMS"])):

                $link_all = "/".CITY_ID."/ratings/#".$arParams["A"];

                $arResult["IBLOCK_TYPE_ID"]?$arResult["IBLOCK_TYPE_ID"]:$arParams['SIMULAR_IBLOCK_KEY'];

                if($arResult["IBLOCK_TYPE_ID"]=='overviews'){
                    $link_all = '/'.CITY_ID.'/news/restvew/';
                }
                elseif($arResult["IBLOCK_TYPE_ID"]=='blogs'){
                    if($arParams['SIMULAR_IBLOCK_KEY']){
                        if(CITY_ID=='spb'){
                            $link_all = '/'.CITY_ID.'/blogs/65/';
                        }
                        elseif(CITY_ID=='msk'){
                            $link_all = '/'.CITY_ID.'/blogs/93/';
                        }
                        else {
                            $link_all = '/'.CITY_ID.'/blogs/';
                        }
                    }
                    else {
                        $link_all = '/'.CITY_ID.'/blogs/';
                    }
                }
                elseif($arResult["IBLOCK_TYPE_ID"]=='photoreports'){
                    $link_all = '/'.CITY_ID.'/photos/';
                }
                elseif($arResult["IBLOCK_TYPE_ID"]=='usual_blog'){
                    $link_all = '/'.CITY_ID.'/blogs/';
                }
                elseif($arResult["IBLOCK_TYPE_ID"]=='cookery'){
                    $link_all = '/'.CITY_ID.'/news/mcfromchif/';
                }
                elseif($arResult["IBLOCK_TYPE_ID"]=='news'){
                    $link_all = '/'.CITY_ID.'/news/restoransnews'.CITY_ID.'/';
                }
                ?>
                <div class="more_links text-right">
                    <a class="btn btn-light" href="<?=$APPLICATION->GetCurDir()=='/content/cookery/'?"/".CITY_ID."/news/mcfromchif/":$link_all?>"><?=$APPLICATION->GetCurDir()=='/content/cookery/'?GetMessage("RECIPES_FROM_CHIEF"):GetMessage("CT_ALL")?></a>
                </div>
            <?endif;?>
        </div>
    </div>    
<?endforeach;?>
</div>