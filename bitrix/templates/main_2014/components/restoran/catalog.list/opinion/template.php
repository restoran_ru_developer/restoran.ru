<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/flowplayer.js"></script>
<div class="reviews-list">
    <?if ($arResult["ITEMS"][0]["ID"]):?>
        <?foreach ($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="item">

            <?if($arResult['FAMOUS_REVIEW']):?>
                <div class="famous-review-left-side">
                    <img src="<?=$arResult['FAMOUS_REVIEW']['PREVIEW_PICTURE']['src']?>" width="130" height="130" alt="<?=$arResult['FAMOUS_REVIEW']['NAME']?>">
                    <div class="famous-detail-name"><?=$arResult['FAMOUS_REVIEW']['NAME']?></div>
                    <div class="famous-detail-about"><?=$arResult['FAMOUS_REVIEW']['PREVIEW_TEXT']?></div>
                    <div class="famous-detail-photo-source"><?=$arResult['FAMOUS_REVIEW']['DETAIL_TEXT']?></div>
                </div>
                <p>
                    <?=$arItem["PREVIEW_TEXT"]?>
                </p>
                <div class="detail_article famous-review-restoran-link">
                    <?=$arItem['FAMOUS_REVIEW']['RESTORAN']?>
                </div>
            <?else:?>
                <div class="rest_name">
                    <?if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"]=="Y"):?>
                        <h2><a href="<?=$arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"]?>" title="<?=GetMessage('OPTIONS_LINK_TITLE_REST')?> <?=$arItem["RESTORAN"]["NAME"]?>"><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></a></h2>
                    <?else:?>
                        <h2><?=$arResult["ITEMS"][$key]["RESTORAN"]["NAME"]?></h2>
                    <?endif;?>
                    <Br />
                    <?=$arResult["ITEMS"][$key]["RESTORAN"]["IBLOCK_NAME"]?><br />
                    <!--<div class="subway-icon">Пушкинская, Чеховская</div>-->
                    <a href="<?=str_replace("detailed","opinions",$arItem["RESTORAN"]["DETAIL_PAGE_URL"])?>" class="all_rest_reviews" title="<?=GetMessage('OPTIONS_LINK_TITLE_REST_REVIEWS')?> <?=$arItem["RESTORAN"]["NAME"]?>">Все отзывы ресторана</a>
                </div>
                <div class="author">
                    <div class="avatar"><img src="<?=$arItem["AVATAR"]["src"]?>" alt="<?=$arItem["NAME"]?>" /></div>
                    <div class="date">
                        <?if ($arItem["CREATED_BY"]):?>
                            <a href="/users/id<?=$arItem["CREATED_BY"]?>/"><?=$arItem["NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>, <?=$arItem["DISPLAY_ACTIVE_FROM_TIME"]?>
                        <?else:?>
                            <a href="javascript:void(0)"><?=$arItem["NAME"]?></a>, <?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?>, <?=$arItem["DISPLAY_ACTIVE_FROM_TIME"]?>
                        <?endif;?>
                    </div>
                    <div class="ratio">
                        <?for($i=1;$i<=5;$i++):?>
                            <span class="icon-star<?=($i>$arItem["DETAIL_TEXT"])?"-empty":""?>"></span>
                        <?endfor;?>
                    </div>
                </div>
                <div class="attachments">
                    <?if(count($arItem["PROPERTIES"]["photos"])&&$arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                        <div class="photo-icon" onclick="$('a.fancy[rel=group<?=$key?>]:first-child').click();"><?=count($arItem["PROPERTIES"]["photos"])?> фото</div>
                    <?endif;?>
                    <!--                --><?//if(count($arItem["PROPERTIES"]["video"])&&$arItem["PROPERTIES"]["video"][0]):?>
                    <!--                    <div class="video-icon">--><?//=count($arItem["PROPERTIES"]["video"])?><!-- видео</div>-->
                    <!--                --><?//endif;?>
                </div>
                <div class="clearfix"></div>
                <p>
                    <?=$arItem["PREVIEW_TEXT"]?>
                </p>
            <?endif?>


            <?if ($arItem["PROPERTIES"]["plus"]):?>
                <div class="plus">
                    <b>достоинства</b>
                    <?=$arItem["PROPERTIES"]["plus"]?>
                </div>
            <?endif;?>
            <?if($arItem["PROPERTIES"]["minus"]):?>
                <div class="minus">
                    <b>недостатки</b>
                    <?=$arItem["PROPERTIES"]["minus"]?>
                </div>
            <?endif;?>
            <div class="clearfix"></div>
             <?if ($arItem["PROPERTIES"]["video_youtube"] || $arItem["PROPERTIES"]['video'][0]):?>
                <?

                 if($arItem["PROPERTIES"]["video_youtube"]):
                     $str = $arItem["PROPERTIES"]["video_youtube"];
                     preg_match_all('/v=(.*)http/sU', $str, $matches);
                     $last_matche = end(explode('v=', $str));
                     if ($matches[1]) {
                         $all_matches = $matches[1];
                     }
                     else {
                         $all_matches = array();
                     }
                     array_push($all_matches, $last_matche);
                 endif;
                ?>

                <div class="review_atachment">
                    <?
                    if($arItem["PROPERTIES"]["video_youtube"]):
                        foreach($all_matches as $one_video):?>
                            <iframe width="448" height="252" src="https://www.youtube.com/embed/<?=$one_video?>" frameborder="0" allowfullscreen></iframe>
                        <?endforeach;
                    endif;
                    ?>
                    <?if($arItem['PROPERTIES']['video'][0]):?>
                        <?$arrLocalVideo = CFile::GetFileArray($arItem['PROPERTIES']['video'][0]);?>
                        <div class="pic" style="width: 448px;height: 252px;">
                            <a class="myPlayer" href="http://restoran.ru<?=$arrLocalVideo['SRC']?>">
                            </a>
                        </div>

                        <script>
                            flowplayer("a.myPlayer", "https://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                    autoPlay: false,
                                    autoBuffering: true
                                }
                            });
                        </script>

<!--                        <iframe width="448" height="252" src="http://restoran.ru" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
                    <?endif?>
                </div>
            <?endif;?>
            <?if ($arItem["PROPERTIES"]["photos"][0]["SRC"]):?>
                <div class="review_atachment ">
                    <?foreach($arItem["PROPERTIES"]["photos"] as $inner_key=>$photo):?>
                      <div class="pic"><a class="fancy" rel="group<?=$key?>" href="<?=$photo["ORIGINAL_SRC"]["src"]?>"><img src="<?=$photo["SRC"]?>" alt="" /></a></div>
                    <?endforeach;?>                    
                </div>                
            <?endif;?>            
<!--            <a href="--><?//=$arItem["DETAIL_PAGE_URL"]?><!--#comments" class="answer_link" data-toggle="toggle" data-target="add_co">--><?//=GetMessage("REVIEWS_ANSWER")?><!--</a>-->
        </div>
        <?endforeach;?>
    <?else:?>
        <?if (!CSite::InGroup(Array(9))):?>
            <p style="font-style:italic"><?=GetMessage("NO_REVIEWS")?></p>
        <?endif;?>
    <?endif;?>   
</div>