<?

function get_url_mime_type($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_exec($ch);
        return curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
}
function get_rest($rest_id)
{
    $arRestIB = getArIblock("catalog", CITY_ID);
    $rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            //"ACTIVE" => "Y",
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arRestIB["ID"],
            "ID" => $rest_id,
        ),
        false,
        false,
        Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL","PROPERTY_RATIO")
    );
    if($arRest = $rsRest->GetNext()) {
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"kitchen"));
        while($ar_props = $db_props->Fetch())
        {
            $kitchen["NAME"] = $ar_props["NAME"];
            if (LANGUAGE_ID=="en"){
                $rsCuisine = CIBlockElement::GetList(Array(),Array("ID"=>$ar_props["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                $arCuisine = $rsCuisine->Fetch();
                $kitchen["VALUE"][] = $arCuisine["PROPERTY_ENG_NAME_VALUE"];
            }
            else {
                $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
                $arCuisine = $rsCuisine->Fetch();
                $kitchen["VALUE"][] = $arCuisine["NAME"];
            }
        }
        $db_props = CIBlockElement::GetProperty($arRest["IBLOCK_ID"], $arRest["ID"], array(), Array("CODE"=>"average_bill"));
        if($ar_props = $db_props->Fetch())
        {
            $avb["NAME"] = $ar_props["NAME"];

            if (LANGUAGE_ID=="en"){
                $rsCuisine = CIBlockElement::GetList(Array(),Array("ID"=>$ar_props["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                $arCuisine = $rsCuisine->Fetch();
                $avb["VALUE"][] = $arCuisine["PROPERTY_ENG_NAME_VALUE"];
            }
            else {
                $rsCuisine = CIBlockElement::GetByID($ar_props["VALUE"]);
                $arCuisine = $rsCuisine->Fetch();
                $avb["VALUE"][] = $arCuisine["NAME"];
            }
        }
        //"PROPERTY_ratio", "PROPERTY_kitchen", "PROPERTY_average_bill"
        //v_dump($arRest);
        // get rest city name
        $rsRestCity = CIBlock::GetByID($arRest["IBLOCK_ID"]);
        $arRestCity = $rsRestCity->Fetch();

        // get rest picture
        if ($arRest["PREVIEW_PICTURE"])
            $restPic = CFile::ResizeImageGet($arRest["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        elseif ($arRest["DETAIL_PICTURE"])
            $restPic = CFile::ResizeImageGet($arRest["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        else
            $restPic["src"] = "/tpl/images/noname/rest_nnm.png";
        // get cuisine name
        $rsCuisine = CIBlockElement::GetByID($arRest["PROPERTY_KITCHEN_VALUE"]);
        $arCuisine = $rsCuisine->Fetch();

        // set rest info
        $arTmpRest["NAME"] = $arRest["NAME"];
        $arTmpRest["CITY"] = $arResult["IBLOCK"]["NAME"];
        $arTmpRest["RATIO"] = $arRest["PROPERTY_RATIO_VALUE"];
        $arTmpRest["REST_PICTURE"] = $restPic;
        $arTmpRest["URL"] = $arRest["DETAIL_PAGE_URL"];
        $arTmpRest["CUISINE"] = $kitchen;
        $arTmpRest["AVERAGE_BILL"] = $avb;

        if(LANGUAGE_ID!="en"){
            if (substr_count($arTmpRest["URL"], "banket"))
                $ty = "Банкетный зал";
            else
                $ty = "Ресторан";
        }
        else {
            if (substr_count($arTmpRest["URL"], "banket"))
                $ty = "Banquet hall";
            else
                $ty = "Restaurant";
        }

        $res = '<a href="'.$arTmpRest["URL"].'"><img class="indent" src="'.$arTmpRest["REST_PICTURE"]["src"].'" width="232" /></a>
                        <h2><a class="font14" href="'.$arTmpRest["URL"].'">'.$ty.' '.$arTmpRest["NAME"].'</a></h2>
                    <div class="ratio">';
        for($i = 0; $i < 5; $i++):
            if ($i < $arTmpRest["RATIO"]):
                $res .= '<span class="icon-star"></span>';
            else:
                $res .= '<span class="icon-star-empty"></span>';
            endif;
        endfor;
        $res .= '</div>';
        $res .= '<b>'.$arTmpRest["CUISINE"]["NAME"].':</b> ';
        $res .= implode(", ",$arTmpRest["CUISINE"]["VALUE"]);
        $res .= '<br /><b>'.$arTmpRest["AVERAGE_BILL"]["NAME"].':</b> ';
        $res .= implode(", ",$arTmpRest["AVERAGE_BILL"]["VALUE"]);
        $res .= '<br /></p>';
        if(end($block["REST_BIND_ARRAY"]) != $rest):
            $res .= '<hr class="dotted" />';
        endif;
        $resto[] = Array("ID"=>$arRest["ID"],"NAME"=>$arRest["NAME"]);
    }
    return $res;
}
foreach($arResult["ITEMS"] as $key=>$arItem) {

    //v_dump($arItem['PREVIEW_TEXT']);
    // explode date
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"]." G:i", MakeTimeStamp($arResult["ITEMS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    //$arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ITEMS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_TIME"] = $arTmpDate[3];
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if ($arUser = $rsUser->Fetch())
    {
        if ($arUser["PERSONAL_PROFESSION"])
            $arResult["ITEMS"][$key]["NAME"] = $arUser["PERSONAL_PROFESSION"];
        else
            $arResult["ITEMS"][$key]["NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"]; 
    }
    else
    {
        $temp = explode("@",$arItem["NAME"]);
        $arResult["ITEMS"][$key]["NAME"] = $temp[0];        
    }

//    if($USER->IsAdmin()){
        if($arResult["ITEMS"][$key]['PROPERTIES_ORIGINAL']['FAMOUS_REVIEW']['VALUE']){

            $res = CIBlockElement::GetByID($arResult["ITEMS"][$key]['PROPERTIES_ORIGINAL']['FAMOUS_REVIEW']['VALUE']);
            if($ar_res = $res->GetNext()){
                $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['PREVIEW_TEXT'] = $ar_res['PREVIEW_TEXT'];
                $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['DETAIL_TEXT'] = $ar_res['DETAIL_TEXT'];
                $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['NAME'] = $ar_res['NAME'];

                if($ar_res['PREVIEW_PICTURE']){
                    $arFile = CFile::GetFileArray($ar_res["PREVIEW_PICTURE"]);

                    $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arFile, array('width' => 130, 'height' => 130), BX_RESIZE_IMAGE_EXACT, true, Array());
                }
            }

            $arResult["ITEMS"][$key]['FAMOUS_REVIEW']['RESTORAN'] = get_rest($arItem["PROPERTIES2"]["ELEMENT"][0]);

//        $arResult["ITEMS"][$key]['PROPERTIES']['FAMOUS_PIC'][0] &&
//        $arResult["ITEMS"][$key]["AVATAR"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]['PROPERTIES']['FAMOUS_PIC'][0], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT, true, Array());
        }
//    }

//    else {
        $arResult["ITEMS"][$key]["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT, true, Array());
        if (!$arResult["ITEMS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="M")
            $arResult["ITEMS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/man_nnm.png";
        elseif (!$arResult["ITEMS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="F")
            $arResult["ITEMS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/woman_nnm.png";
        elseif (!$arResult["ITEMS"][$key]["AVATAR"]["src"])
            $arResult["ITEMS"][$key]["AVATAR"]['src'] = "/tpl/images/noname/unisx_nnm.png";
//    }

    
    // get rest name
    $rsSec = CIBlockElement::GetByID($arItem["PROPERTIES2"]["ELEMENT"][0]);
    if($arSec = $rsSec->GetNext()) {
        //$pic = CFile::ResizeImageGet($arSec['PREVIEW_PICTURE'], array('width'=>70, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);   
        //if (!$pic["src"])
        //    $pic = CFile::ResizeImageGet($arSec['DETAIL_PICTURE'], array('width'=>60, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);   
        $pic["src"] = CFile::GetPath($arSec['PREVIEW_PICTURE']);   
        if (!$pic["src"])
            $pic["src"] = CFile::GetPath($arSec['DETAIL_PICTURE']);   
        $arResult["ITEMS"][$key]["RESTORAN"] = Array("ID"=>$arSec["ID"],"NAME"=>$arSec["NAME"],"IBLOCK_NAME"=>$arSec["IBLOCK_NAME"],"SRC"=>$pic["src"],"DETAIL_PAGE_URL"=>$arSec["DETAIL_PAGE_URL"],"IBLOCK_TYPE_ID"=>$arSec["IBLOCK_TYPE_ID"],'ACTIVE'=>$arSec['ACTIVE']);
    }
    
//    $APPLICATION;
    $cp = $this->__component; // объект компонента
    if (is_object($cp))
    {
            // добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
            $cp->arResult['RESTORAN_URL'] = $arResult["ITEMS"][$key]["RESTORAN"]["DETAIL_PAGE_URL"];
            $cp->arResult['RESTORAN'] = $arResult["ITEMS"][$key]["RESTORAN"]["NAME"];
            $cp->arResult['PREVIEW_TEXT'] = $arResult["ITEMS"][$key]["PREVIEW_TEXT"];
            $cp->arResult['FAMOUS_REVIEW'] = $arResult["ITEMS"][$key]["FAMOUS_REVIEW"];


            //Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
            $cp->SetResultCacheKeys(array('RESTORAN','RESTORAN_URL','PREVIEW_TEXT','FAMOUS_REVIEW'));
            // сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
            if (!isset($arResult['RESTORAN']))
            {
                $arResult['RESTORAN'] = $cp->arResult['RESTORAN'];
                $arResult['RESTORAN_URL'] = $cp->arResult['RESTORAN_URL'];
            }
    }
    
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = stripslashes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT2"] = TruncateText($arResult["ITEMS"][$key]["PREVIEW_TEXT"], 400);
    
    /*$arResult["ITEMS"][$key]["PROPERTIES"]["pluss"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["plus"]);
    $arResult["ITEMS"][$key]["PROPERTIES"]["pluss"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["plus"], 400);
    
    $arResult["ITEMS"][$key]["PROPERTIES"]["minuss"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["minus"]);
    $arResult["ITEMS"][$key]["PROPERTIES"]["minuss"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["minus"], 400);*/
    $arResult["ITEMS"][$key]["DETAIL_TEXT"] = strip_tags($arResult["ITEMS"][$key]["DETAIL_TEXT"]);
    foreach ($arResult["ITEMS"][$key]["PROPERTIES"]["photos"] as &$photo)
    {
        $id = $photo;
        $photo = array();
        $photo["ID"] = $id;
        $photo["SRC"] = CFile::ResizeImageGet($id,array("height"=>124,"width"=>137),BX_RESIZE_IMAGE_EXACT,true);
        $photo["SRC"] = $photo["SRC"]["src"];
        $photo["ORIGINAL_SRC"] = CFile::ResizeImageGet($id,array("height"=>600,"width"=>800),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
    }
    
    


    $type = array();
    $arItem["PREVIEW_TEXT"] = strip_tags($arItem["PREVIEW_TEXT"]);
    $text = $arItem["PREVIEW_TEXT"];
    preg_match_all("#(https?|ftp)://\S+[^\s.,>)\];'\"!?]#", $text, $links);

    
    foreach ($links[0] as $value) {
        
          $type[] = get_url_mime_type($value);
    }
    
    for ($i = 0; $i < count($type); $i++) {
        $imageType = preg_match("#(image/)#", $type[$i], $pregResult);
        $textType = preg_match("#(text/)#", $type[$i], $pregResult);
        if ($imageType == 1) {
            $types[$i] = 'image';
        }
        if ($textType == 1) {
            $videoType = preg_match("#(youtube.com|vimeo.com|video.yandex.ru|video.google.com|ru.youtube.com|rutube.ru|video.mail.ru|vkadre.ru|vision.rambler.ru|dailymotion.com|smotri.com|flickr.com)#", $links[0][$i], $pregResult);
            if ($videoType == 1) {
                $types[$i] = 'video';
            } else {
                $types[$i] = 'text';
            }
        }
    }
    //v_dump($types);
    for ($i = 0; $i < count($types); $i++) {
        $result[$i] = array('link' => $links[0][$i], 'type' => $types[$i]);
        if ($types[$i] == "video") {
            $links[0][$i] = str_replace('http:','https:',$links[0][$i]);
            if (substr_count($links[0][$i], "youtube")) {
                //v_dump($links[0][$i]);
                $temp = explode("v=", $links[0][$i]);
                if (!$temp[1])
                    $temp = explode("/", $links[0][$i]);
                $arItem["PREVIEW_TEXT"] = str_replace($links[0][$i], "<div align='center'><iframe width='400' height='300' src='https://www.youtube.com/embed/" . $temp[count($temp) - 1] . "' frameborder='0' allowfullscreen></iframe></div>", $arItem["PREVIEW_TEXT"]);
            }
            if (substr_count($links[0][$i], "smotri")) {
                //$links[0][$i] = str_replace('#', '', $links[0][$i]);
                $temp = explode("id=", $links[0][$i]);
                if (!$temp[1]) {
                    $temp = explode("/", $links[0][$i]);
                }

                $arItem["PREVIEW_TEXT"] = str_replace($links[0][$i], "<div align='center'><iframe width='400' height='300' src='https://pics.smotri.com/player.swf?file=" . str_replace('#', '', $temp[count($temp) - 1]) . "' frameborder='0' allowfullscreen></iframe></div>", $arItem["PREVIEW_TEXT"]);
            }
            if (substr_count($links[0][$i], "vimeo")) {
                $temp = explode("/", $links[0][$i]);
                $arItem["PREVIEW_TEXT"] = str_replace($links[0][$i], "<div align='center'><iframe width='400' height='300' src='https://player.vimeo.com/video/" . $temp[count($temp) - 1] . "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>", $arItem["PREVIEW_TEXT"]);
            }
            if (substr_count($links[0][$i], "rutube")) {
                $temp = explode("/", $links[0][$i]);
                if (!$temp[1])
                    $temp = explode("/", $links[0][$i]);
                $rutubeObject = json_decode(file_get_contents('https://rutube.ru/api/oembed/?url=[' . $links[0][$i] . ']&format=json'));
                $rutubeArray = get_object_vars($rutubeObject);
                //echo $rutubeArray['html'];
                $rutubeArray['html'] = str_replace('iframe', 'iframe style="width:400px; height:300px; margin-left: 45px;"', $rutubeArray['html']);
                $arItem["PREVIEW_TEXT"] = str_replace($links[0][$i], '<div align="center rutube-video">'.$rutubeArray['html'].'</div>', $arItem["PREVIEW_TEXT"]);
            }
            /* if (substr_count($links[0][$i], "yandex"))
              {
              $temp = explode("/",$links[0][$i]);
              if (!$temp[1])
              $temp = explode("/",$links[0][$i]);
              $arComments["PREVIEW_TEXT"] = str_replace($links[0][$i],"<div align='center'><iframe width='400' height='300' src='http://rutube.ru/video/".$temp[count($temp)-2]."' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe></div>",$arComments["PREVIEW_TEXT"]);
              } */
        }
//        $arItem["PREVIEW_TEXT"] = str_replace($links[0][$i], "<a class='another' href='" . $links[0][$i] . "' target='_blank'>" . substr($links[0][$i], 0, 45) . "...</a>", $arItem["PREVIEW_TEXT"]);
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = $arItem["PREVIEW_TEXT"];
    }
}
?>