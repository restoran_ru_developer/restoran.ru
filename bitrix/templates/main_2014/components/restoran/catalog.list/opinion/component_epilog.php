<?
if ($arResult["RESTORAN"])
{
    global $APPLICATION;

    preg_match('/.+?[\.!\?]/',strip_tags($arResult['PREVIEW_TEXT']),$output_review_str);

    $APPLICATION->SetTitle("Отзыв о ресторане ".$arResult["RESTORAN"]);
    $APPLICATION->SetPageProperty("keywords",  "отзыв, ресторан, ".$arResult["RESTORAN"]);
    $APPLICATION->SetPageProperty("title",  "Отзыв о ресторане ".$arResult["RESTORAN"].':'.$output_review_str[0]);

    $city_arr_for_desc = array(
        'msk'=>"Москве",
        'spb'=>"Санкт-Петербурге",
        'rga'=>"Риге",
        'urm'=>"Юрмале",
        'kld'=>"Калиниграде",
        'nsk'=>"Новосибирске",
        'sch'=>"Сочи"
    );

    $APPLICATION->SetPageProperty("description",  "Отзыв о ресторане ".$arResult["RESTORAN"]." в ".$city_arr_for_desc[CITY_ID].': '.$output_review_str[0]);

    if($arResult['FAMOUS_REVIEW']){
        $APPLICATION->SetTitle($arResult['FAMOUS_REVIEW']['NAME'].' рассказывает о ресторане '.$arResult["RESTORAN"]);
    }
}


if($arResult['ITEMS'][0]['ID']){
    CIBlockElement::CounterInc($arResult['ITEMS'][0]['ID']);
}

?>