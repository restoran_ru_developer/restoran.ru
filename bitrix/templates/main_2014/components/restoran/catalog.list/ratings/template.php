<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i==0;?>
<?$page = $arResult["NAV_RESULT"]->PAGEN;
if (!$page)
    $page =1;
?>
<div class="news-list">
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    
    <?if ($key==0):?>
        <div class="item_big pull-left">
            <div class="pic_big">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
            </div>
            <div class="text_big">
                <div class="position pull-left"><?=($key+(($page-1)*19) + 1)?></div>                
                    <div class="ratio">
                        <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=substr($arItem["NAME"],0,20)?><?=(strlen($arItem["NAME"])>20)?"...":""?></a></h2>
                    <?for($p = 1; $p <= 5; $p++):?>
                        <span class="glyphicon glyphicon-star<?if($p > round($arItem["PROPERTIES"]["ratio"])):?>-empty<?endif?>" alt="<?=$i?>"></span>
                    <?endfor?>                                          
                </div>
                <div class="clearfix"></div>
                <div class="props">
                    <?if ($arItem["PROPERTIES"]["phone"][0]):?>
                        <div class="prop">
                            <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                <div class="name"><?=GetMessage("FAV_PHONE")?>:</div>
                                <div class="value">
                                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php<?=$arParams["PARENT_SECTION_CODE"]!="banket"?"":"?banket=Y"?>" class="tnum <? echo MOBILE;?> booking">
                                        <?if (CITY_ID=="spb"):?>
                                            +7(812) 740-18-20
                                        <?elseif(CITY_ID=='rga'):?>
                                            +371 661 031 06
                                        <?else:?>
                                            +7(495) 988-26-56
                                        <?endif;?>
                                    </a>
                                </div>
                            <?else:?>
                                <div class="name"><?=GetMessage("FAV_PHONE")?>:</div>
                                <?$phone = preg_split("/,|;/", $arItem["PROPERTIES"]["phone"][0]);?>
                                <div class="value">
                                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php<?=$arParams["PARENT_SECTION_CODE"]!="banket"?"":"?banket=Y"?>" class="tnum <? echo MOBILE;?> booking">
                                        <?=clearUserPhone($phone[0])?>
                                    </a>
                                </div>
                            <?endif;?>
                        </div>
                    <?endif;?>
                     <?if ($arItem["PROPERTIES"]["address"][0]):?>
                        <div class="prop">
                            <div class="name"><?=GetMessage("FAV_ADRESS")?>:</div>                            
                            <div class="value"><?
                                //$temp = explode(";",$arItem["PROPERTIES"]["address"][0]); echo $temp[0]
                                if(!$arItem["PROPERTIES"]["address"]['LINK']){
                                    $temp = explode(";",$arItem["PROPERTIES"]["address"][0]);
                                    echo $temp[0];
                                }
                                else {
                                    echo $arItem["PROPERTIES"]["address"]['LINK'];
                                }
                                ?></div>
                        </div>
                    <?endif;?>
                    <?if ($arItem["PROPERTIES"]["subway"][0]):?>
                        <div class="prop wsubway">
                            <div class="subway">M</div>
                            <div class="value"><?=$arItem["PROPERTIES"]["subway"]['LINK']//$arItem["PROPERTIES"]["subway"][0]?></div>
                        </div>
                    <?endif;?>                   

                </div>
                <Br />
                <div class="pull-left">
                    <i><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
                </div>
                <div class="pull-right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                <div class="clearfix"></div>
                <Br />                
                <div class="pull-right">
                    <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'>В избранное</a>
                    <?if (CITY_ID!="tmn"&&CITY_ID!="ast"&&CITY_ID!="amt"&&CITY_ID!="vrn"&&CITY_ID!="tln"):?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=rawurlencode($arItem["NAME"])?>'>Бронировать</a>
                    <?endif;?>
                </div>
                <div class="clearfix"></div>   
            </div>
            <div class="clearfix"></div>
        </div>
     <div class="clearfix dots"></div>
        <?$i = 3;?>
    <?else:?>
        <div class="item pull-left <?=($i%3 == 2)?"end":""?>">
            <div class="pic">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /></a>            
            </div>
            <div class="text">
                <div class="position pull-left"><?=($key+(($page-1)*19) + 1)?></div>                
                <div class="ratio">
                    <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                    <?for($p = 1; $p <= 5; $p++):?>
                        <span class="glyphicon glyphicon-star<?if($p > round($arItem["PROPERTIES"]["ratio"])):?>-empty<?endif?>" alt="<?=$i?>"></span>
                    <?endfor?>                                          
                </div> 
                <div class="clearfix"></div>
                <div class="props">
                    <?if ($arItem["PROPERTIES"]["phone"][0]):?>
                        <div class="prop">
                            <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                <div class="name"><?=GetMessage("FAV_PHONE")?>:</div>
                                <div class="value">
                                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php<?=$arParams["PARENT_SECTION_CODE"]!="banket"?"":"?banket=Y"?>" class="tnum <? echo MOBILE;?> booking">
                                        <?if (CITY_ID=="spb"):?>
                                            +7(812) 740-18-20
                                        <?elseif(CITY_ID=='rga'):?>
                                            +371 661 031 06
                                        <?else:?>
                                            +7(495) 988-26-56
                                        <?endif;?>
                                    </a>
                                </div>
                            <?else:?>
                                <div class="name"><?=GetMessage("FAV_PHONE")?>:</div>
                                <?$phone = preg_split("/,|;/", $arItem["PROPERTIES"]["phone"][0]);?>
                                <div class="value">
                                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=='rga'?"_rgaurm":""?>.php<?=$arParams["PARENT_SECTION_CODE"]!="banket"?"":"?banket=Y"?>" class="tnum <? echo MOBILE;?> booking">
                                        <?=clearUserPhone($phone[0])?>
                                    </a>
                                </div>
                            <?endif;?>
                        </div>
                    <?endif;?>
                     <?if ($arItem["PROPERTIES"]["address"][0]):?>
                        <div class="prop">
                            <div class="name"><?=GetMessage("FAV_ADRESS")?>:</div>                            
                            <div class="value"><?
                            if(!$arItem["PROPERTIES"]["address"]['LINK']){
                                $temp = explode(";",$arItem["PROPERTIES"]["address"][0]);
                                echo $temp[0];
                            }
                            else {
                                echo $arItem["PROPERTIES"]["address"]['LINK'];
                            }?></div>
                        </div>
                    <?endif;?>
                    <?if ($arItem["PROPERTIES"]["subway"][0]):?>
                        <div class="prop wsubway">
                            <div class="subway">M</div>
                            <div class="value"><?=$arItem["PROPERTIES"]["subway"]['LINK']?></div>
                        </div>
                    <?endif;?>                   
                    
                </div>
                <Br />
                <div class="pull-left">
                    <i><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
                </div>
                <div class="pull-right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                <div class="clearfix"></div>
            </div>            
        </div>
        <?if ($i%3 == 2):?>
                <div class="clearfix dots"></div>
        <?endif;?>
        <?$i++;?>
    <?endif;?>
<?endforeach;?>
    <?if ($arResult["NAV_STRING"]&&$arParams["DISPLAY_BOTTOM_PAGER"]):?>           
        <div class="navigation">            
                <?=$arResult["NAV_STRING"]?>            
        </div>
    <?endif;?>
</div>