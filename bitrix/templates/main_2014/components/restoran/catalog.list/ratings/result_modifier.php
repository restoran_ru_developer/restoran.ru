<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem)
{
    if ($key==0)
    {
        if ($arItem["PREVIEW_PICTURE"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>370, 'height'=>240), BX_RESIZE_IMAGE_EXACT, true);
        elseif($arItem["PROPERTIES"]["photos"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PROPERTIES"]["photos"][0], array('width'=>370, 'height'=>240), BX_RESIZE_IMAGE_EXACT, true);
    }
    else
    {
        if ($arItem["PREVIEW_PICTURE"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>232, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);
        elseif($arItem["PROPERTIES"]["photos"])
            $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PROPERTIES"]["photos"][0], array('width'=>232, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);
    }
    if (!$arItem["PREVIEW_PICTURE"]["src"])
        $arItem["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";

    if(LANGUAGE_ID=='en') {
        $res = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], "sort", "asc", array("CODE" => "subway"));
        if ($ob = $res->Fetch()) {
            $res = CIBlockElement::GetList(Array("SORT" => "ASC"), array('ID' => $ob['VALUE'],'IBLOCK_ID'=>$ob["LINK_IBLOCK_ID"]), false, false, array('PROPERTY_eng_name'));
            if ($ob = $res->Fetch()) {
                $arItem["PROPERTIES"]["subway"][0] = $ob['PROPERTY_ENG_NAME_VALUE'];
            }
        }
    }

    if(!LANGUAGE_ID!='en'){
        foreach($arItem["PROPERTIES3_FULL"] as $pid=>$arProperty) {
            if ($pid == 'subway') {
                $arResult["ITEMS"][$key]["PROPERTIES"][$pid]["LINK"] = '<a href="/'.CITY_ID.'/catalog/'.$arParams['PARENT_SECTION_CODE'].'/metro/'.$arProperty[0]['CODE'].'/">'.$arItem["PROPERTIES"]["subway"][0].'</a>';
            }
            if($pid=='STREET'&&$arProperty){
                if(CITY_ID=='msk'||CITY_ID=='spb'){
                    $db_props = CIBlockElement::GetProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort" => "asc"), Array("CODE"=>"SHOW_YA_STREET"));
                    if($ar_props = $db_props->Fetch()){
                        if($ar_props["VALUE_ENUM"]=='Да'){
                            $temp = explode(";",$arItem["PROPERTIES"]["address"][0]);
                            $arResult["ITEMS"][$key]["PROPERTIES"]['address']["LINK"] = '<a href="'.($arParams['PARENT_SECTION_CODE']=='banket'?preg_replace('/restaurants/','banket',$arProperty[0]['DETAIL_PAGE_URL']):$arProperty[0]['DETAIL_PAGE_URL']).'">'.$temp[0].'</a>';
                        }
                    }
                }
            }
        }
    }
}
?>