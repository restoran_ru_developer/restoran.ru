<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (count($arResult["ITEMS"]) > 0): ?>    
    <? if ($APPLICATION->GetCurDir() == "/msk/news/maslenitsa/"||$APPLICATION->GetCurDir() == "/spb/news/maslenitsa/"): ?>
        <img src="/tpl/images/maslenica_new.jpg" />
    <? endif; ?>                        
    <?if ($APPLICATION->GetCurDir()=="/msk/news/23_fevralya/"||$APPLICATION->GetCurDir()=="/spb/news/23_fevralya/"):?>
        <img src="/tpl/images/23february.jpg" />
    <?endif;?>      
    <?if ($APPLICATION->GetCurDir()=="/msk/news/paskha/"||$APPLICATION->GetCurDir()=="/spb/news/paskha/"):?>
        <img src="/tpl/images/pacha.jpg" />
    <?endif;?>                                                                         
    <?if ($APPLICATION->GetCurDir()=="/spb/news/vostok_novyy_god/"||$APPLICATION->GetCurDir()=="/msk/news/vostochnyy_novyy_god/"):?>
        <img src="/tpl/images/China.png" />
    <?endif;?>
    <?if ($APPLICATION->GetCurDir()=="/msk/news/novogodnie_yelki/"||$APPLICATION->GetCurDir()=="/spb/news/yelki/"):?>
        <img src="/tpl/images/elki_bg.jpg" />
        <?if (CITY_ID=="msk"):?>
            <p>Московские рестораны готовятся встречать и радовать малышей! Сезон праздничных и сказочных представлений в ресторанах начнется 22 декабря.
Выбирайте, куда пойдете вы!</p>
        <?endif;?>
    <?endif;?>
    <?if ($APPLICATION->GetCurDir()=="/spb/news/tatyanin_den/"||$APPLICATION->GetCurDir()=="/msk/news/tatyanin/"):?>
        <img src="/tpl/images/student.png" /><Br /><BR />
        <p>
            В Татьянин день рестораны расцветают! Яркая и непосредственная молодежь перебирается из скучных аудиторий в теплые и душевные кафе, где шумными компаниями и отмечают свой день — День студента! 
В этот день многие рестораны предлагают студенческие скидки, акции, придумывают специальные развлекательные программы — мы собрали для вас некоторые из них.
        </p>
    <?endif;?>
    <?if ($APPLICATION->GetCurDir()=="/msk/news/oktoberfest/"||$APPLICATION->GetCurDir()=="/spb/news/oktoberfest/"):?>
        <img src="/tpl/images/oktoberfest.jpg" style="margin-bottom:10px;" />
    <?endif;?>
    <?if ($APPLICATION->GetCurDir()=="/msk/news/halloween/"||$APPLICATION->GetCurDir()=="/spb/news/halloween/"):?>
        <img src="/tpl/images/halloween.jpg" style="margin-bottom:10px;" />
    <?endif;?>
    <?if($APPLICATION->GetCurDir()=="/msk/news/staryy_novyy_god/"):?>
        <p>Череда новогодних праздников продолжается: сегодня в Москве выпал первый в этом году снег, а также пришло время отметить Старый Новый год! И понедельник — это еще не повод увильнуть от празднования. Торопитесь развлечься по полной программе этим вечером, ведь следующий по плану Новый год (на этот раз китайский) у нас только в феврале! </p>
    <?endif;?>
    <h1>
    <? if ($arParams["SET_TITLE"] == "Y" && $arResult["IBLOCK_TYPE"] != "Рецепты"): ?>
            <?
            if ($arResult["SECTION"]["PATH"][0]["NAME"] == "Новые рестораны")
                echo $arResult["SECTION"]["PATH"][0]["NAME"];
            else
                echo $arResult["IBLOCK_TYPE"]
                ?>
        <? else: ?>
            <?= $APPLICATION->GetTitle(); ?>
        <? endif; ?>
    </h1>
    <?
    $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE", "clear_cache");
    $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "") . "pageSort=ACTIVE_FROM&" . (($_REQUEST["pageSort"] == "ACTIVE_FROM" && $_REQUEST["by"] == "desc" || !$_REQUEST["pageSort"]) ? "by=asc" : "by=desc"), $excUrlParams);
    $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "") . "pageSort=PROPERTY_summa_golosov&" . (($_REQUEST["pageSort"] == "PROPERTY_summa_golosov" && $_REQUEST["by"] == "asc" || !$_REQUEST["pageSort"]) ? "by=desc" : "by=asc"), $excUrlParams);
    ?>                
    <div class="sort" <?if ($APPLICATION->GetCurDir()=="/msk/news/staryy_novyy_god/") echo "style=top:65px;"?><? if ($APPLICATION->GetCurDir() == "/msk/news/paskha/"||$APPLICATION->GetCurDir() == "/spb/news/paskha/" ) echo "style='top:95px;'" ?> <?=($APPLICATION->GetCurDir() == "/msk/news/oktoberfest/"||$APPLICATION->GetCurDir() == "/spb/news/oktoberfest/"||$APPLICATION->GetCurDir() == "/".CITY_ID."/news/halloween/"||$APPLICATION->GetCurDir() == "/spb/news/yelki/"||$APPLICATION->GetCurDir()=="/spb/news/vostok_novyy_god/"||$APPLICATION->GetCurDir()=="/msk/news/vostochnyy_novyy_god/"||$APPLICATION->GetCurDir() == "/msk/news/maslenitsa/"||$APPLICATION->GetCurDir() == "/spb/news/maslenitsa/"||$APPLICATION->GetCurDir() == "/msk/news/23_fevralya/"||$APPLICATION->GetCurDir() == "/spb/news/23_fevralya/"||$APPLICATION->GetCurDir() == "/spb/news/8marta/"||$APPLICATION->GetCurDir() == "/spb/news/8marta/")?"style='top:250px;'":""?> <?=($APPLICATION->GetCurDir() == "/msk/news/novogodnie_yelki/")?"style='top:280px;'":""?> <?=($APPLICATION->GetCurDir() == "/spb/news/tatyanin_den/" || $APPLICATION->GetCurDir() == "/msk/news/tatyanin/" )?"style='top:330px;'":""?>>                        
            <? $by = ($_REQUEST["by"] == "asc") ? "asc" : "desc"; ?>
            <?
            if ($_REQUEST["pageSort"] == "ACTIVE_FROM" || !$_REQUEST["pageSort"]):
                echo "<a class='" . $by . "' href='" . $sortNewPage . "'>" . GetMessage("SORT_NEW_TITLE") . "</a>";
            else:
                echo '<a class="another" href="' . $sortNewPage . '">' . GetMessage("SORT_NEW_TITLE") . '</a>';
            endif;
            ?>            
            <?
            if ($_REQUEST["pageSort"] == "PROPERTY_summa_golosov"):
                echo "<a class='" . $by . "' href='" . $sortPopularPage . "'>" . GetMessage("SORT_POPULAR_TITLE") . "</a>";
            else:
                echo '<a class="another" href="' . $sortPopularPage . '">' . GetMessage("SORT_POPULAR_TITLE") . '</a>';
            endif;
            ?>                        
    </div>    
    <div class="clearfix"></div>
    <div class="news-list">
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>                                                                    
            <div class="item pull-left<? if ($key % 3 == 2): ?> end<? endif ?>">
                <div class="date">
                    <a href="/users/id<?= $arItem["CREATED_BY"] ?>/" class="another"><?= $arItem["AUTHOR_NAME"] ?></a>,  <?= $arItem["CREATED_DATE_FORMATED_1"] ?> <?= $arItem["CREATED_DATE_FORMATED_2"] ?>                    
                </div>       
                <br />
                <div class="pic">
                    <? if ($arItem["VIDEO_PLAYER"]): ?>  
                        <?= $arItem["VIDEO_PLAYER"] ?>
                    <? else: ?>                                
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"] ?>" width="232" /></a>                               
                    <? endif; ?>
                </div>
                <div class="text">
                    <h2><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a></h2>
                    <?= $arItem["PREVIEW_TEXT"] ?>                    
<!--                    <div class="f12">
                        <br />
                        <div class="pull-left"><?= GetMessage("COMMENTS") ?>: (<a class="another" href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments"><?= intval($arItem["PROPERTIES"]["COMMENTS"]) ?></a>)</div>
                        <div class="pull-right"><a class="another" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS") ?></a></div>
                        <div class="clear"></div>
                    </div>-->
                </div>                            
            </div> 
            <? if ($key % 3 == 2&&$arItem!=end($arResult["ITEMS"])): ?>
                <div class="clearfix dots"></div>
            <?endif;?>
    <? endforeach; ?> 
    </div>
<?else: ?>    
    <p><font class="notetext"><?= GetMessage("NOTHING_TO_FOUND") ?></font></p>    
<?endif; ?>                      
<?if ($arResult["NAV_STRING"]): ?>            
    <div class="navigation">
        <?= $arResult["NAV_STRING"] ?>                    
    </div>
<?endif; ?>    