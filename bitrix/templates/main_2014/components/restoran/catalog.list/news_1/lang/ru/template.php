<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "отзыв";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "отзыва";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "отзывов";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Далее";
$MESS["SHOW_CNT_TITLE"] = "Выводить по";
$MESS["SORT_NEW_TITLE"] = " по новизне";
$MESS["SORT_POPULAR_TITLE"] = " по популярности";
$MESS["ANONS_overviews"] = "Обзоры";
$MESS["ANONS_news_overviews"] = "Обзоры";
$MESS["ANONS_news_photo"] = "Фотоотчеты";
$MESS["ANONS_photoreports"] = "Фотоотчеты";
$MESS["ANONS_cookery"] = "Рецепты от шефа";
$MESS["ANONS_news"] = "Новости";
$MESS["ANONS_interview"] = "Интервью";
$MESS["ANONS_blogs"] = "Критика";
$MESS["NOTHING_TO_FOUND"] = "К сожалению, по вашему запросу ничего не найдено.";
$MESS["COMMENTS"] = "Комментарии";
?>