<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="sm">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="item">
            <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
                <div class="pic">
                    <?if ($arItem["ID"]==1143939):?>
                        <noindex><a href="http://www.prazdnikvkusa.ru" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></noindex><br />
                    <?elseif($arItem["ID"]==386954):?>
                        <noindex><a href="http://www.discoveryclub.ru" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a></noindex><br />
                    <?else:?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a><br />
                    <?endif;?>
                </div>
            <?endif;?>
            <div class="text">
                <?if ($arItem["ID"]==1143939):?>
                    <noindex><h2><a href="http://www.prazdnikvkusa.ru" target="_blank"><?=$arItem["NAME"]?></a></h2></noindex>
                <?elseif($arItem["ID"]==386954):?>
                    <noindex><h2><a href="http://www.discoveryclub.ru" target="_blank"><?=$arItem["NAME"]?></a></h2></noindex>
                <?else:?>
                    <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                <?endif;?>
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>
                </div>
                <?if($arItem["ID"]==386954):?>
                    <p><?=GetMessage("for_386954_text")?><?=GetMessage('recomended_for_386954')?></p>
                <?else:?>
                    <?if ($arParams["REST_PROPS"]=="Y"):?>
                        <div class="props">
                            <?if ($arItem["KITCHEN"]):?>
                                <div class="prop">
                                    <div class="name"><?=GetMessage('recomended_kitchen')?></div>
                                    <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                                </div>
                            <?endif;?>
                            <?if ($arItem["BILL"]):?>
                                <div class="prop">
                                    <div class="name"><?=GetMessage('recomended_average_bill')?></div>
                                    <div class="value"><?=$arItem["BILL"]?></div>
                                </div>
                            <?endif;?>
                            <?if ($arItem["SUBWAY"]):?>
                                <div class="prop wsubway">
                                    <div class="subway">M</div>
                                    <div class="value"><?=$arItem["SUBWAY"]?></div>
                                </div>
                            <?endif;?>
                        </div>
                    <?else:?>
                        <p><?=$arItem["PREVIEW_TEXT"]?></p>
                    <?endif;?>
                <?endif;?>
                <?if ($arItem==end($arResult["ITEMS"])):?>
                    <div class="more_links text-right">
                        <?if ($arParams["NEWEST"]=="Y"):?>
                            <a class="btn btn-light" href="/<?=CITY_ID?>/catalog/restaurants/all/?page=1&pageRestSort=new&by=desc"><?=GetMessage("CT_ALL")?></a>
                        <?else:?>
                            <a class="btn btn-light" href="/<?=CITY_ID?>/ratings/#<?=$arParams["A"]?>"><?=GetMessage("CT_ALL")?></a>
                        <?endif;?>
                    </div>
                <?endif;?>
            </div>
        </div>
    <?endforeach;?>
</div>