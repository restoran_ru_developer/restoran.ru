<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="pic">
        <?if ($arItem["VIDEO_PLAYER"]):?>
            <?=$arItem["VIDEO_PLAYER"]?>
        <?else:?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]?>" /></a>
        <?endif?>
    </div>
    <div class="text">
        <div class="date"><b></b><?=$arItem["DISPLAY_ACTIVE_FROM_DAY"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_MONTH"]?> <?=$arItem["DISPLAY_ACTIVE_FROM_YEAR"]?></div>
        <?/*if ($arItem["RESTORAN_NAME"]):?>
            <div class="restoran-name"><a href="<?=$arItem["RESTORAN_LINK"]?>"><?=$arItem['RESTORAN_NAME']?></a></div>
        <?endif;*/?>
        <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></h2>
        <?=$arItem['PREVIEW_TEXT']?>
        <div class="more_links text-right">
            <a class="serif" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_NEWS_DETAIL_LINK")?></a>
            <a href="<?=($arParams["ANOTHER_LINK"])?$arParams["ANOTHER_LINK"]:$arItem["SECTION_PAGE_URL"]?>" class="btn btn-light" iblock="<?=$arParams["IBLOCK_TYPE"]?>"><?=GetMessage("CT_BNL_ALL_NEWS_LINK_".$arParams["IBLOCK_TYPE"])?></a>
        </div>
    </div>
<?endforeach;?>