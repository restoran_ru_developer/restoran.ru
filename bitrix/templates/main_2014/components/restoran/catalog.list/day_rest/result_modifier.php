<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::GetPath($arResult["ITEMS"][$key]["PREVIEW_PICTURE"]);
    if (!$arResult["ITEMS"][$key]["PREVIEW_TEXT"])
         $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    
    $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
        while ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
        if ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
        }
        
        $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "subway"));
        if ($ob = $res->GetNext())
        {
            $r = CIBlockElement::GetByID($ob['VALUE']);
            if ($a = $r->Fetch())
                $arResult["ITEMS"][$key]["SUBWAY"] = $a['NAME'];
        }
    
}

?>