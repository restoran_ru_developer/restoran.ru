<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="title"><?=GetMessage("DAY_REST_WEEK_RESTAURANT")?></div>
    <div class="sm" id="most-viewed">
        <div class="item">
            <div class="pic">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
            </div>
            <div class="text">
                <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                <div class="props">
                    <?if ($arItem["KITCHEN"]):?>
                        <div class="prop">
                            <div class="name"><?=GetMessage("DAY_REST_KITCHEN")?></div>
                            <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                        </div>
                    <?endif;?>
                    <?if ($arItem["BILL"]):?>
                        <div class="prop">
                            <div class="name"><?=GetMessage("DAY_REST_AVERAGE_BILL")?></div>
                            <div class="value"><?=$arItem["BILL"]?></div>
                        </div>
                    <?endif;?>
                    <?if ($arItem["SUBWAY"]):?>
                        <div class="prop wsubway">
                            <div class="subway">M</div>
                            <div class="value"><?=$arItem["SUBWAY"]?></div>
                        </div>
                    <?endif;?>
                </div>
            </div>
        </div>
    </div>
<?endforeach;?>