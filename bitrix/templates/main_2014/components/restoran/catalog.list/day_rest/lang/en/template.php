<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "review";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "review";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "reviews";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Further";
$MESS["DAY_REST_WEEK_RESTAURANT"] = "Restaurant week";
$MESS["DAY_REST_KITCHEN"] = "Kitchen:";
$MESS["DAY_REST_AVERAGE_BILL"] = "Average bill:";
?>