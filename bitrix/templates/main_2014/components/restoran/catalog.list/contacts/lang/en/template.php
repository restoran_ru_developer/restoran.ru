<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "comment";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "comment";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "comments";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Further";
$MESS["FAV_PHONE"] = "Phones";
$MESS["FAV_ADRESS"] = "Address";
$MESS["FAV_DELETE"] = "Delete";
?>