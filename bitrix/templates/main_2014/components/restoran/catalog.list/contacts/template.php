<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i==0;?>
<?if ($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]):?>
    <div class="contact_description">
        <div class="left" style="width:400px"><?=$arResult["SECTION"]["PATH"][0]["DESCRIPTION"]?></div>        
        <div class="right"><a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-nb btn-info ">Бронирование ONline</a></div>
        <div class="clear"></div>
    </div>
<?endif;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <h2><?=$arItem["NAME"]?></h2>
            <div class="left"><?=$arItem["PREVIEW_TEXT"]?></div>
            <div class="metro_<?=$arParams["PARENT_SECTION_CODE"]?> left" style="margin-left:10px;"><?=$arItem["PROPERTIES"]["subway"][0]?></div>
            <div class="clear"></div>
            <br /><br />
            <?if (count($arItem["USERS"])):?>
                <table width="100%" cellpadding="0" cellspacing="0" class="contact_user">
                    <?foreach ($arItem["USERS"] as $user):?>
                        <tr>
                            <td class="<?=(end($arItem["USERS"])==$user)?"ma":"light_hr"?>" width="180">
                                <div class="big_avatar">
                                    <img src="<?=$user["PERSONAL_PHOTO"]["src"]?>" width="150" height="150" />
                                </div>
                            </td>
                            <td class="<?=(end($arItem["USERS"])==$user)?"ma":"light_hr"?>" style="padding-top:40px; width:300px">
                                <h3 style="margin-bottom:10px;"><?=$user["NAME"]?> <?=$user["LAST_NAME"]?></h3>
                                <p><i><?=$user["~WORK_POSITION"]?></i></p>
                                <p><?=$user["PERSONAL_CITY"]?></p>
                                <p class="user-mail"><a href="javascript:void(0)" onclick="send_message(<?=$user["ID"]?>,'<?=$user["NAME"]?>')">Сообщение</a></p>
                            </td>
                            <td class="<?=(end($arItem["USERS"])==$user)?"ma":"light_hr"?>" style="padding-top:40px;">
                                <h5><?=$user["WORK_NOTES"]?></h5>
                                <p><?=$user["PERSONAL_PHONE"]?></p>
                                <p><?=$user["EMAIL"]?></p>
                            </td>
                        </tr>
                    <?endforeach;?>
                </table>
            <?endif;?>
<?endforeach;?>