<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "This will erase all information associated with this record. Resume?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "comment";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "comment";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "comments";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Further";
$MESS["SHOW_CNT_TITLE"] = "Per";
$MESS["SORT_NEW_TITLE"] = "by newest";
$MESS["SORT_POPULAR_TITLE"] = "by popularity";
$MESS["ALL_REVIEWS_catalog"] = "All reviews restaurant";
$MESS["ALL_REVIEWS_kupons"] = "All reviews coupon";
$MESS["REVIEWS_BLOG_new_review"] = "+ New review";
$MESS["REVIEWS_BLOG_more"] = "more";
$MESS["REVIEWS_BLOG_hide"] = "hide";
$MESS["REVIEWS_BLOG_edit"] = "Edit";
?>