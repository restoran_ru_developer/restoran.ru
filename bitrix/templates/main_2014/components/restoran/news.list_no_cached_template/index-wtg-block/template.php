<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])>0):?>
    <div class="index-journal-bg-line"></div>
    <div class="index-journal-title">Куда пойти?</div>

    <?
    if($arResult["REQUEST_OBJECT_COUNT"]>6):?>
<!--                <div class="index-one-main-news-bottom-line">-->
<!--                    <a href="/--><?//=CITY_ID?><!--/wheretogo/" class="watch-all">Все подборки</a>-->
<!--                </div>-->
        <div class="index-nav-tabs-wrapper">
            <a href="/<?=CITY_ID?>/wheretogo/" class="btn btn-info btn-nb-empty">Все подборки</a>
        </div>
    <?endif;?>

    <div class="under-index-journal-title-sign">рестораны на любой вкус и повод</div>

    <div class="index-where-to-go-wrapper">
        <?foreach($arResult["ITEMS"] as $arItem):
            ?>
        <div class="where-to-go-link-wrapper" >
            <?if($arItem['CODE']=='na_kanikulakh_v_rige'){?>
                <a href="http://www.restoran.ru/rga/catalog/restaurants/where_to_go/<?=$arItem['CODE']?>/">
            <?}else {?>
                <?if(preg_match('/#/',$arItem['CODE'])):
                    preg_match("/#(.+)/", $arItem['CODE'], $output_link);
                    ?>
                    <a href="<?=$output_link[1]?>">
                <?else:?>
                    <a href="/<?=CITY_ID?>/catalog/restaurants/where_to_go/<?=$arItem['CODE']?>/">
                <?endif?>
            <?}?>
                <?
                $arFile = CFile::ResizeImageGet($arItem['PROPERTIES']['WTG_BG_PIC']['VALUE'], array('width'=>148, 'height'=>148), BX_RESIZE_IMAGE_EXACT, false);
                ?>
                <img src="<?=$arFile['src']?>" class="this-link-hover-bg" width="" height="" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" >
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="where-to-go-icon" hover-img-src="" width="" height="" alt="" title="">
                <img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="where-to-go-active-icon" width="" height="" alt="" title="">
                <span><?=$arItem['NAME']?></span>
            </a>
        </div>
        <?endforeach;?>
    </div>
    <div class="clearfix"></div>

<?endif?>