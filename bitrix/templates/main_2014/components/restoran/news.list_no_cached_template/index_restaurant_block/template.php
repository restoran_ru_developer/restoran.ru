<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                 
<?if(count($arResult["ITEMS"])>0):
    $cache_time = 36000020;
    ?>

    <div class="index-journal-wrapper">
        <div class="index-journal-bg-line"></div>
        <div class="index-journal-title"><?=$arParams['PAGER_TITLE']?></div>
        <div class="under-index-journal-title-sign">популярные рубрики</div>
        <div class="index-nav-tabs-wrapper">
            <ul class="nav nav-tabs new-history">
                <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                    <li class="<?=$key==0?'active':''?>">
                        <?
                        $el = "";
                        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                            $el = implode("&el[]=", $arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                        }
                        $url = "&t=" . $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . "&el[]=" . $el . "&ib=" . $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"] . "&c=" . $arItem["PROPERTIES"]["COUNT"]["VALUE"] . "&s=" . $arItem["PROPERTIES"]["SECTION"]["VALUE"] . "&code=" . $arItem["CODE"];
                        ?>
                        <a href="#index_journal_<?=$arItem["CODE"]?>" class="ajax" data-toggle="tab" data-href="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=<?= CITY_ID ?><?= $url ?>">
                            <?= $arItem["NAME"] ?>
                        </a>
                    </li>

                <?endforeach;?>
            </ul>
            <?if(CITY_ID=='msk'||CITY_ID=='spb'):?><a href="/<?=CITY_ID?>/news/newplace<?=CITY_ID=='spb'?'spb':''?>/" class="btn btn-info btn-nb-empty">Все открытия</a><?endif?>
        </div>

        <div class="tab-content">
            <?if(CITY_ID=='urm'||CITY_ID=='rga'):?>
                <div class="tab-pane sm" id="index_journal_000000"></div>
            <?endif?>
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                <?if($key==0):?>
                    <div class="tab-pane sm active" id="index_journal_<?=$arItem["CODE"]?>">
                    <?
                        $filter = "arrNoFilter_".$arItem["CODE"];
                        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
                        {
                            global $$filter;
                            $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                        }
                        ?>
                        <?if($arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"]):?>
                            <?$APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "7",
                                    "SORT_BY1" => ($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]=="blogs")?"created_date":"ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => ($$filter)?$filter:"",
                                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL",'SHOW_COUNTER','SECTION_CODE'),
                                    "PROPERTY_CODE" => array("RESTORAN",'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => $cache_time,
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arItem["PROPERTIES"]["LINK"]["VALUE"],
                                    'CENTER_POST' => 'Y',
                                    'ALL_NEWS_TITLE' => 'new_places'
                                ),
                                false
                            );
                        ?>
                        <?else:?>
                            <?if ($arItem["CODE"]=='ratings1')
                            {
                                global $$filter;
                                $a = "Да";
                                $$filter = array("!PROPERTY_sleeping_rest_VALUE" => $a);
                                $arIB = getArIblock("catalog", CITY_ID);
                                $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list",
                                    "new_rest_main_full_line",
                                    Array(
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => $arIB["ID"],
                                        "NEWS_COUNT" => "8",
                                        "SORT_BY1" => "PROPERTY_rating_date",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "PROPERTY_stat_day",
                                        "SORT_ORDER2" => "DESC",
                                        "SORT_ORDER2" => "DESC",
                                        "SORT_BY3" => "NAME",
                                        "FILTER_NAME" => ($$filter)?$filter:"",
                                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "120",
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "restaurants",
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "3600",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "REST_PROPS" => "Y"
                                    ),
                                    false
                                );
                                unset($$filter["!PROPERTY_sleeping_rest_VALUE"]);
                            }
                            elseif ($arItem["CODE"]=='ratings2')
                            {
                                $arIB = getArIblock("catalog", CITY_ID);
                                $a = "Да";
                                $$filter = array("!PROPERTY_sleeping_rest_VALUE" => $a);
                                $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list",
                                    "new_rest_main_full_line",
                                    Array(
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => $arIB["ID"],
                                        "NEWS_COUNT" => "8",
                                        "SORT_BY1" => "PROPERTY_rating_date",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "PROPERTY_stat_day",
                                        "SORT_ORDER2" => "DESC",
                                        "SORT_BY3" => "NAME",
                                        "SORT_ORDER3" => "ASC",
                                        "FILTER_NAME" => ($$filter)?$filter:"",
                                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "120",
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "banket",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "3600000".$cach,
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "REST_PROPS" => "Y"
                                    ),
                                    false
                                );
                                unset($$filter["!PROPERTY_sleeping_rest_VALUE"]);
                            }
                            if ($arItem["CODE"]=='ratings3')
                            {
                                $arIB = getArIblock("catalog", CITY_ID);
                                global $arrfil;
                                $arrfil["!PROPERTY_restoran_ratio"] = false;
                                $APPLICATION->IncludeComponent(
                                    "restoran:catalog.list",
                                    "new_rest_main_full_line",
                                    Array(
                                        "DISPLAY_DATE" => "N",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "N",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => $arIB["ID"],
                                        "NEWS_COUNT" => "8",
                                        "SORT_BY1" => "PROPERTY_restoran_ratio",
                                        "SORT_ORDER1" => "asc,nulls",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "arrfil",
                                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "120",
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "3600000".$cach,
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "REST_PROPS" => "Y"
                                    ),
                                    false
                                );
                            }?>
                        <?endif?>

                    </div>
                <?else:?>
                    <div class="tab-pane sm" id="index_journal_<?=$arItem["CODE"]?>"></div>
                <?endif?>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>