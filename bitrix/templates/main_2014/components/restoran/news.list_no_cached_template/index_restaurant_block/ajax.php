<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>
<?
if ($_REQUEST["c"]>20)
    $_REQUEST["c"]=3;
$filter = "arrNoFilter_".$_REQUEST["code"];
if (is_array($_REQUEST["el"]))
{    
        global $$filter;
        $$filter = Array("ID"=>$_REQUEST["el"]);
}
?>
<?
$cach = 15;
$afisha_cache_time = 86401;
switch($_REQUEST["t"])
{
    case "all_news":
        $ar1 = getArIblock('news', CITY_ID);
            $ar2 = getArIblock('afisha', CITY_ID);
        $ar3 = getArIblock('catalog', CITY_ID);
        $ar4 = getArIblock('overviews', CITY_ID);
        $ar5 = getArIblock('blogs', CITY_ID);

        $GLOBALS['all_news_filter'] = array("!PROPERTY_sleeping_rest_VALUE" => "Да","PROPERTY_NETWORK_REST" => false,
            "!ID" => CIBlockElement::SubQuery("ID", array(
                "IBLOCK_ID" => $ar2["ID"],
                "!PROPERTY_SHOW_EVENT_IN_INDEX_NEWS_VALUE" => "Y",
            ))
        );
//        $GLOBALS['all_news_filter'] = array();
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list_multi",
            "all-index-news",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "news",
                "IBLOCK_ID" => array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],145),
                "NEWS_COUNT" => 7,
                "SORT_BY1" => 'created',
                "SORT_ORDER1" => 'DESC',
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "all_news_filter",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('address','REST_NETWORK'),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "d.m.Y-H:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",//Y
                "CACHE_TIME" => "3616",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                'AJAX_REQUEST'=>'Y'
            ),
            false
        );
    break;

    case "new_rest":
        $arIB = getArIblock("catalog", CITY_ID);
        //global $arrFil;
        $$filter = Array("!PROPERTY_sleeping_rest_VALUE" => "Да");

        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "new_rest_main",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arIB,
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"3",
                "SORT_BY1" => "created_date",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => ($$filter)?$filter:"arrFil",
                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["s"],
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "3600000".$cach,
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "NEW_REST"=>"Y",
                "REST_PROPS" => "Y"
            ),
        false
        );        
    break;
    case "news":                    
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "news_main_index_summer_2015",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => $_REQUEST["t"],
                "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"3",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => ($$filter)?$filter:"",
                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("RATIO","COMMENTS",'LINK'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["s"],
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600000".$cach,
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                'LINK_NAME'=>'ВСЕ НОВОСТИ'
            ),
        false
        );
    break;
    case "ny_interiors":
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "news_main_index_summer_2015_ny_iteriors",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => 'special_projects',
                "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"8",
                "SORT_BY1" => "TIMESTAMP_X",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('PHOTOS'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_LAST_MODIFIED" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000002",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SET_STATUS_404" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
        );
    break;
    case "firms_news":
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "new_rest_main",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => $_REQUEST["t"],
                "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"3",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => ($$filter)?$filter:"",
                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => $_REQUEST["s"],
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600000".$cach,
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );
    break;
    case "ratings":
        if ($_REQUEST["code"]=='ratings1')
        {

            $BestSortArr = $_REQUEST["el"];

            $$filter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';

            $arIB = getArIblock("catalog", CITY_ID);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "new_rest_main_full_line",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arIB["ID"],
                    "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"8",
                    "SORT_BY1" => "none",
                    "SORT_ORDER1" => "",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3601",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "REST_PROPS" => "Y",
                    'BEST_SORT_ARRAY' => $BestSortArr
                ),
            false
            );
            unset($$filter["!PROPERTY_sleeping_rest_VALUE"]);            
        }
        elseif ($_REQUEST["code"]=='ratings2')
        {
            $arIB = getArIblock("catalog", CITY_ID);
            $a = "Да";
            $$filter = array("!PROPERTY_sleeping_rest_VALUE" => $a);
            //$$filter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "new_rest_main_full_line",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arIB["ID"],
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"8",
                    "SORT_BY1" => "PROPERTY_rating_date",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "PROPERTY_stat_day",
                    "SORT_ORDER2" => "DESC",                                
                    "SORT_BY3" => "NAME",
                    "SORT_ORDER3" => "ASC",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "restaurants",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000".$cach,
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "REST_PROPS" => "Y"
                ),
            false
            );
            unset($$filter["!PROPERTY_sleeping_rest_VALUE"]);            
        }
        if ($_REQUEST["code"]=='ratings3')
        {
            $arIB = getArIblock("catalog", CITY_ID);
            global $$filter;
            $a = "Да";
            $$filter = array("!PROPERTY_sleeping_rest_VALUE" => $a);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "new_rest_main_full_line",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arIB["ID"],
                    "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"8",
                    "SORT_BY1" => "PROPERTY_restoran_ratio",
                    "SORT_ORDER1" => "asc,nulls",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000".$cach,
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "REST_PROPS" => "Y"
                ),
            false
            );
        }
    break;
    case "overviews":             
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "news_main_index_summer_2015",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $_REQUEST["t"],
                    "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"3",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("RATIO",'LINK'),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600000".$cach,
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );
    break;
    case "reviews":

        if($_REQUEST['code']=='famous'){
            $GLOBALS['needFamousReviews'] = array('!PROPERTY_FAMOUS_REVIEW'=>false);
            $APPLICATION->IncludeComponent(
                "bitrix:news.list", "reviews_main_famous_index_summer_2015", Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => $_REQUEST["t"],
                "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => "6",
                "SORT_BY1" => "created_date",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "needFamousReviews",
                "FIELD_CODE" => array('SHOW_COUNTER'),
                "PROPERTY_CODE" => array("COMMENTS",'FAMOUS_TITLE'),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "150",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",//a
                "CACHE_TIME" => "36000007",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                'AJAX_REVIEWS'=>'Y'
            ), false
            );
        }
        else {
            $GLOBALS['needFamousReviews'] = array('PROPERTY_FAMOUS_REVIEW'=>false);
            $APPLICATION->IncludeComponent(
                "bitrix:news.list", "reviews_main_index_summer_2015", Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => $_REQUEST["t"],
                "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => "8",
                "SORT_BY1" => "created_date",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "needFamousReviews",
                "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY",'SHOW_COUNTER'),
                "PROPERTY_CODE" => array("COMMENTS", "ELEMENT"),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "150",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000008",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ), false
            );
        }

    break;
    case "cookery":           
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "news_main_index_summer_2015",
            Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $_REQUEST["t"],
                    "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => ($_REQUEST["c"])?$_REQUEST["c"]:"8",
                    "SORT_BY1" => ($_REQUEST["code"]=="master-klassy")?"active_from":"created_date",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => ($$filter)?$filter:"",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("comments"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => $_REQUEST["s"],
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "1440".$cach,
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
            ),
        false
        );
    break;
    case "photoreports":
    ?>
        <?$APPLICATION->IncludeComponent(
        "restoran:catalog.list",
        "news_main_index_summer_2015",
        Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => $_REQUEST["t"],
            "IBLOCK_ID" => $_REQUEST["ib"],
            "NEWS_COUNT" => "8",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "DESC",
            "FILTER_NAME" => ($$filter)?$filter:"",
            "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
            "PROPERTY_CODE" => array("RESTORAN"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "140",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => $_REQUEST["s"],
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",//a
            "CACHE_TIME" => "36000009",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
//            "ANOTHER_LINK" => $arItem["PROPERTIES"]["LINK"]["VALUE"]
        ),
        false
    );
        ?>
        <?
        break;
    case "blogs":
        if($_REQUEST['code']=='critic'){
            $$filter = array_merge(array("PROPERTY_CRITIC_POST_VALUE"=>'Y'),$$filter);
            if($_REQUEST["ib"]=='156'){
                $critic_id = 65;
            }
            else {
                $critic_id = 93;
            }
            $all_news_title = 'critic';
        }

        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "news_main_index_summer_2015",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => $_REQUEST["t"],
                "IBLOCK_ID" => $_REQUEST["ib"],
                "NEWS_COUNT" => "8",
                "SORT_BY1" => "created_date",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "DESC",
                "FILTER_NAME" => ($$filter)?$filter:"",
                "FIELD_CODE" => array("DATE_CREATE","CREATED_BY","DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("COMMENTS",'LINK'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600000".$cach,
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "LINK"=> "/".CITY_ID."/blogs/".$critic_id.'/',
                "LINK_NAME"=> "Все блоги",
                'ALL_NEWS_TITLE' => $all_news_title
            ),
            false
        );
        break;

    case "afisha_list_index_summer_2015_1":

        $day = date("N");

        global $arAfishaFilter;
        $arAfishaFilter = Array(
            Array("LOGIC"=>"OR",
                Array("!PROPERTY_d".$day=>false),
                Array("PROPERTY_EVENT_DATE" => date('Y-m-d').' 00:00:00')//'2015-09-21 00:00:00'
            ),
            'PROPERTY_RESTORAN.ACTIVE'=>'Y',
        );
        if(intval($_REQUEST['s'])){
            $arAfishaFilter['PROPERTY_EVENT_TYPE'] = intval($_REQUEST['s']);
        }
        if(intval($_REQUEST['restoran_id'])){
            $arAfishaFilter['PROPERTY_RESTORAN'] = intval($_REQUEST['restoran_id']);
        }

        $_REQUEST["restoran"] = $_REQUEST['restoran_id'];
//        FirePHP::getInstance()->info($arAfishaFilter,'$arAfishaFilter');

        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "afisha_list_index_summer_2015_1", Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => 'afisha',
            "IBLOCK_ID" => $_REQUEST['ib'],
            "NEWS_COUNT" => "550",//550
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arAfishaFilter",
            "FIELD_CODE" => array("DETAIL_PICTURE"),
            "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "200",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => 'Y',//$USER->IsAdmin()?"N":
            "CACHE_TIME" => $afisha_cache_time,//7208
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            'EVENT_TYPE_FOR_ID' => 'for_id_'.$_REQUEST['s'],
            'CUSTOM_AJAX' => 'Y',
            'AJAX_EVENT_DATE' => date("d.m.Y")
        ), false
        );
    break;
    case "afisha_list_index_summer_2017":

        global $arAfishaFilter;
        $arAfishaFilter = Array(
            'PROPERTY_RESTORAN.ACTIVE'=>'Y',
            Array("LOGIC"=>"OR",
                Array(">=PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
                Array("PROPERTY_EVENT_DATE"=>false),
            ),
        );
        if(intval($_REQUEST['s'])){
            $arAfishaFilter['PROPERTY_EVENT_TYPE'] = intval($_REQUEST['s']);
        }
        if(intval($_REQUEST['restoran_id'])){
            $arAfishaFilter['PROPERTY_RESTORAN'] = intval($_REQUEST['restoran_id']);
        }

        $_REQUEST["restoran"] = $_REQUEST['restoran_id'];
//        FirePHP::getInstance()->info($arAfishaFilter,'$arAfishaFilter');

        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "afisha_list_index_summer_2017", Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => 'afisha',
            "IBLOCK_ID" => $_REQUEST['ib'],
            "NEWS_COUNT" => "250",//550
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arAfishaFilter",
            "FIELD_CODE" => array("DETAIL_PICTURE"),
            "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "200",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => 'Y',//$USER->IsAdmin()?"N":
            "CACHE_TIME" => $afisha_cache_time,//7208
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            'EVENT_TYPE_FOR_ID' => 'for_id_'.$_REQUEST['s'],
            'CUSTOM_AJAX' => 'Y',
            'AJAX_EVENT_DATE' => date("d.m.Y")
        ), false
        );
    break;
    case "afisha_date_event_place":

        global $arAfishaDateFilter;

        $date = $_REQUEST['event_date'];

        $day = date("N",strtotime($date));
//        FirePHP::getInstance()->info(date('Y-m-d', strtotime($date)).' 00:00:00'));

        if($date){
            $arAfishaDateFilter = Array(
                Array("LOGIC"=>"OR",
                    Array("!PROPERTY_d".$day=>false),
                    Array("PROPERTY_EVENT_DATE" => date('Y-m-d', strtotime($date)).' 00:00:00')//'2015-09-21 00:00:00'
                ),
                Array("LOGIC"=>"OR",
                    Array("!DATE_ACTIVE_FROM"=>"NULL", "<=DATE_ACTIVE_FROM" => date("d.m.Y",strtotime($date))),
                    Array("DATE_ACTIVE_FROM"=>false)
                ),
                Array("LOGIC"=>"OR",
                    Array("!DATE_ACTIVE_TO"=>"NULL", ">=DATE_ACTIVE_TO" => date("d.m.Y",strtotime($date))),
                    Array("DATE_ACTIVE_TO"=>false)
                ),
                'PROPERTY_RESTORAN.ACTIVE'=>'Y',
            );
            if(intval($_REQUEST['s'])){
                $arAfishaDateFilter['PROPERTY_EVENT_TYPE'] = intval($_REQUEST['s']);
            }

            if((int)$_REQUEST['restoran']){
                $arAfishaDateFilter['PROPERTY_RESTORAN'] = intval($_REQUEST['restoran']);
            }
            elseif(intval($_REQUEST['restoran_id'])){
                $arAfishaDateFilter['PROPERTY_RESTORAN'] = intval($_REQUEST['restoran_id']);
            }

            $afisha_keywords = htmlspecialchars(trim($_REQUEST['afisha_keywords']));
            if($afisha_keywords){
                $afisha_keywords = ToLower($afisha_keywords);
                $arAfishaDateFilter['NAME'] = '%'.$afisha_keywords.'%';
            }


            $APPLICATION->IncludeComponent(
                "bitrix:news.list", "afisha_list_index_summer_2015_1", Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => 'afisha',
                "IBLOCK_ID" => $_REQUEST['ib'],
                "NEWS_COUNT" => "550",//550
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arAfishaDateFilter",
                "FIELD_CODE" => array("DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "200",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => $afisha_cache_time,//7208
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                'EVENT_TYPE_FOR_ID' => 'for_id_'.$_REQUEST['s'].'afisha_date',
                'ONLY_POST_CONTENT'=>'Y',
                'CUSTOM_AJAX' => 'Y',
                'AJAX_EVENT_DATE' => date("d.m.Y",strtotime($date))
            ), false
            );
        }


    break;
    case "afisha":

        $day = date("N");

        $days_to_show[] = $day;
        for ($i=0;$i<4;$i++) {
            if ($day == 7)
                $day = 1;
            else
                $day++;
            $days_to_show[] = $day;
        }

        global $arAfishaFilter;
        $arAfishaFilter = Array(
            Array(
                "LOGIC"=>"OR",
                Array("!PROPERTY_d".$days_to_show[0]=>false),
                Array("!PROPERTY_d".$days_to_show[1]=>false),
                Array("!PROPERTY_d".$days_to_show[2]=>false),
                Array("!PROPERTY_d".$days_to_show[3]=>false),
                Array("!PROPERTY_d".$days_to_show[4]=>false),
                Array(">=PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
            ),
            Array("LOGIC"=>"OR",
                Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y h")),
                Array("DATE_ACTIVE_TO"=>false)
            ),
            'PROPERTY_RESTORAN.ACTIVE'=>'Y',
            'PROPERTY_EVENT_TYPE'=>intval($_REQUEST['s'])
        );
        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "afisha_list_index_summer_2015", Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => 'afisha',
            "IBLOCK_ID" => $_REQUEST['ib'],
            "NEWS_COUNT" => "550",//550
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arAfishaFilter",
            "FIELD_CODE" => array("DETAIL_PICTURE"),
            "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "200",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//Y
            "CACHE_TIME" => $afisha_cache_time,//7208
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            'EVENT_TYPE_FOR_ID' => 'for_id_'.$_REQUEST['s'],
            'CUSTOM_AJAX' => 'Y'
        ), false
        );

    break;
    case "afisha_date":

        global $arAfishaDateFilter;

        $date = $_REQUEST['event_date'];

        $day = date("N",strtotime($date));


        if($date){

            FirePHP::getInstance()->info(date("d.m.Y",strtotime($date)),'date server');
            $arAfishaDateFilter = Array(
                Array("LOGIC"=>"OR",
                    Array("!PROPERTY_d".$day=>false),
                    Array("PROPERTY_EVENT_DATE"=>date('Y-m-d',strtotime($date)).' 00:00:00'),
                ),
                Array("LOGIC"=>"OR",
                    Array("!DATE_ACTIVE_FROM"=>"NULL", "<=DATE_ACTIVE_FROM" => date("d.m.Y",strtotime($date))),
                    Array("DATE_ACTIVE_FROM"=>false)
                ),
                Array("LOGIC"=>"OR",
                    Array("!DATE_ACTIVE_TO"=>"NULL", ">=DATE_ACTIVE_TO" => date("d.m.Y",strtotime($date))),
                    Array("DATE_ACTIVE_TO"=>false)
                ),
                'PROPERTY_RESTORAN.ACTIVE'=>'Y',
            );

            if(intval($_REQUEST['s'])){
                $arAfishaDateFilter['PROPERTY_EVENT_TYPE'] = intval($_REQUEST['s']);
            }

            $APPLICATION->IncludeComponent(
                "bitrix:news.list", "afisha_list_index_summer_2015", Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => 'afisha',
                "IBLOCK_ID" => $_REQUEST['ib'],
                "NEWS_COUNT" => "550",//550
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arAfishaDateFilter",
                "FIELD_CODE" => array("DETAIL_PICTURE"),
                "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "200",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => $afisha_cache_time,//7208а
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                'EVENT_TYPE_FOR_ID' => 'for_id_'.$_REQUEST['s'].'afisha_date',
                'ONLY_POST_CONTENT'=>'Y',
                'CUSTOM_AJAX' => 'Y',
                'AJAX_EVENT_DATE' => date("d.m.Y",strtotime($date))
            ), false
            );
        }


    break;

    default:

    break;
}


?>