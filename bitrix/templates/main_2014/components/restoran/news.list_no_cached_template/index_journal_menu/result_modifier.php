<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$cp = $this->__component; // объект компонента
if (is_object($cp))
{
    foreach ($arResult['ITEMS'] as $arItem) {
        $itemCodeArr[] = $arItem['CODE'];
    }

    $cp->arResult['ITEMS_CODE_ARR'] = $itemCodeArr;

    $cp->SetResultCacheKeys(array('ITEMS_CODE_ARR'));
}
?>