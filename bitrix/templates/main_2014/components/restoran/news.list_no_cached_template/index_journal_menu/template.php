<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                 
<?if(count($arResult["ITEMS"])>0):?>     

        <div class="index-journal-bg-line"></div>
        <div class="index-journal-title">Журнал</div>
        <div class="under-index-journal-title-sign">популярные рубрики</div>
        <div class="index-nav-tabs-wrapper relative-menu">
            <?foreach($arResult["ITEMS"] as $key=>$arItem):
                $link_to_all = $arItem["PROPERTIES"]['LINK']['VALUE'];
                ?>
                <?if($link_to_all):?>
                <a href="<?=$link_to_all?>" class="btn btn-info btn-nb-empty display-none link-to-all-from-journal" id="link-to-all-<?=$arItem["CODE"]?>">
                    <?=$arItem["PROPERTIES"]['ALL_NEWS_LINK_NAME']['VALUE']?>
                </a>
            <?endif?>
            <?endforeach;?>
            <ul class="nav nav-tabs new-history">
                <?if(CITY_ID!='urm'&&CITY_ID!='rga'){?><li class="active"><a href="#index_journal_all" data-toggle="tab">Все вместе</a></li><?}?>
                <?
                //FirePHP::getInstance()->info($arResult["ITEMS"],'ITEMS');
                foreach($arResult["ITEMS"] as $key=>$arItem):?>
                    <li <?if((CITY_ID=='urm'||CITY_ID=='rga')&&$key==0):?>class="active"<?endif?>>
                        <?
                        $el = "";
                        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                            $el = implode("&el[]=", $arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                        }
                        $url = "&t=" . $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . "&el[]=" . $el . "&ib=" . $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"] . "&c=" . $arItem["PROPERTIES"]["COUNT"]["VALUE"] . "&s=" . $arItem["PROPERTIES"]["SECTION"]["VALUE"] . "&code=" . $arItem["CODE"];
                        ?>
                        <a href="#index_journal_<?=$arItem["CODE"]?>" class="ajax" data-toggle="tab" data-href="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=<?= CITY_ID ?><?= $url ?>">
                            <?= $arItem["NAME"] ?>
                        </a>
                    </li>
                <?endforeach;?>
            </ul>
        </div>

<!--    <div class="tab-content">-->
<!--    --><?//foreach($arResult["ITEMS"] as $key=>$arItem):?>
<!--        <div class="tab-pane sm" id="index_journal_--><?//=$arItem["CODE"]?><!--"></div>-->
<!--    --><?//endforeach;?>
<?endif;?>