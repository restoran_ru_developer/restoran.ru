<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                 
<?if(count($arResult["ITEMS"])>0):?>

    <?
    $arOne1 = getArIblock('news', CITY_ID);
//    $arOne2 = getArIblock('afisha', CITY_ID);
    $arOne4 = getArIblock('overviews', CITY_ID);
    $arOne5 = getArIblock('blogs', CITY_ID);
    $arOne6 = getArIblock('photoreports', CITY_ID);

    global $oneMainNewsFilter;
    $oneMainNewsFilter = array('ID'=>$arResult["ITEMS"][0]['PROPERTIES']['ELEMENTS']['VALUE'][0]);
    $APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi",
        "index-one-main-news",
        Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "news",
            "IBLOCK_ID" => array($arOne1["ID"],$arOne4["ID"],$arOne5["ID"],$arOne6["ID"]),
            "NEWS_COUNT" => "1",
            "SORT_BY1" => "created_date",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "oneMainNewsFilter",
            "FIELD_CODE" => array('DETAIL_PICTURE','SHOW_COUNTER'),
            "PROPERTY_CODE" => array('COMMENTS'),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y-H:i",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "CACHE_TYPE" => "Y",//y
            "CACHE_TIME" => "3602",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            'ANOTHER_LINK' => $arResult["ITEMS"][0]['PROPERTIES']['LINK']['VALUE']
        ),
        false
    );
    ?>
<?endif;?>