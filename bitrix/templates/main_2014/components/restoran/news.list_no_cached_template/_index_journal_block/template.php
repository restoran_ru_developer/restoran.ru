<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                 
<?if(count($arResult["ITEMS"])>0):?>     

    <div class="index-journal-wrapper">
        <?if(CITY_ID!='msk'&&CITY_ID!='spb'):?>
            <div class="index-journal-bg-line"></div>
            <div class="index-journal-title">Журнал</div>
            <div class="under-index-journal-title-sign">популярные рубрики</div>
            <div class="index-nav-tabs-wrapper">
                <ul class="nav nav-tabs">
                    <?if(CITY_ID!='urm'&&CITY_ID!='rga'){?><li class="active"><a href="#index_journal_all" data-toggle="tab">Все вместе</a></li><?}?>
                    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                        <li <?if((CITY_ID=='urm'||CITY_ID=='rga')&&$key==0):?>class="active"<?endif?>>
                            <?
                            $el = "";
                            if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                                $el = implode("&el[]=", $arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                            }
                            $url = "&t=" . $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . "&el[]=" . $el . "&ib=" . $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"] . "&c=" . $arItem["PROPERTIES"]["COUNT"]["VALUE"] . "&s=" . $arItem["PROPERTIES"]["SECTION"]["VALUE"] . "&code=" . $arItem["CODE"];
                            ?>
                            <a href="#index_journal_<?=$arItem["CODE"]?>" class="ajax" data-toggle="tab" data-href="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=<?= CITY_ID ?><?= $url ?>">
                                <?= $arItem["NAME"] ?>
                            </a>
                        </li>
                    <?endforeach;?>
                </ul>
            </div>
        <?else:?>
            <?
            unset($arrFirstBlock);
            global $arrFirstBlock;
            $arrFirstBlock["SECTION_CODE"] = "first1";
            $APPLICATION->IncludeComponent(
                "restoran:news.list_no_cached_template",
                "index_journal_menu",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page",
                    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFirstBlock",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",//Y
                    "CACHE_TIME" => "3600000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_NOTES" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "TABS"=>"Y"
                ),
                false,
                Array(
                    'ACTIVE_COMPONENT' => 'Y'
                )
            );
            ?>
        <?endif?>

        <div class="tab-content">
            <div class="tab-pane active sm" id="index_journal_all">
                <?
                if(CITY_ID=='urm'||CITY_ID=='rga'){
                    $filter = "arrNoFilter_".$arResult["ITEMS"][0]["CODE"];
                    if (is_array($arResult["ITEMS"][0]["PROPERTIES"]["ELEMENTS"]["VALUE"]))
                    {
                        global $$filter;
                        $$filter = Array("ID"=>$arResult["ITEMS"][0]["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                    }
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "news_main_index_summer_2015",
                        Array(
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                            "IBLOCK_ID" => $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => "7",
                            "SORT_BY1" => ($arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"]=="blogs")?"created_date":"ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "DESC",
                            "FILTER_NAME" => ($$filter)?$filter:"",
                            "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL",'SHOW_COUNTER'),
                            "PROPERTY_CODE" => array("RESTORAN",'COMMENTS'),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "140",
                            "ACTIVE_DATE_FORMAT" => "j F Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arResult["ITEMS"][0]["PROPERTIES"]["SECTION"]["VALUE"],
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",//a
                            "CACHE_TIME" => "36000010",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "ANOTHER_LINK" => $arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"],
                            'CENTER_POST' => 'Y'

                        ),
                        false
                    );
                }
                else {
                    if ($arResult["ITEMS"][1]) {
                        $filter = "arrNoFilter_" . $arResult["ITEMS"][1]["CODE"];
                        if (is_array($arResult["ITEMS"][1]["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                            global $$filter;
                            $$filter = Array("ID" => $arResult["ITEMS"][1]["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                        }
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "news_main_index_summer_2015",
                            Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => "1",
                                "SORT_BY1" => ($arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "DESC",
                                "FILTER_NAME" => ($$filter) ? $filter : "",
                                "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "140",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arResult["ITEMS"][1]["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",//a
                                "CACHE_TIME" => "36000010",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "ANOTHER_LINK" => $arResult["ITEMS"][1]["PROPERTIES"]["LINK"]["VALUE"]
                            ),
                            false
                        );
//                }
                        ?>

                        <?
                        if ($arResult["ITEMS"][0]) {
                            $filter = "arrNoFilter_" . $arResult["ITEMS"][0]["CODE"];
                            if (is_array($arResult["ITEMS"][0]["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                                global $$filter;
                                $$filter = Array("ID" => $arResult["ITEMS"][0]["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                            }
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => ($$filter) ? $filter : "",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                    "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arResult["ITEMS"][0]["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => "36000012",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"],
                                    'CENTER_POST' => 'Y'
                                ),
                                false
                            );
                        }
                        ?>

                        <?
                        if ($arResult["ITEMS"][2]) {
                            $filter = "arrNoFilter_" . $arResult["ITEMS"][2]["CODE"];
                            if (is_array($arResult["ITEMS"][2]["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                                global $$filter;
                                $$filter = Array("ID" => $arResult["ITEMS"][2]["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                            }
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => ($$filter) ? $filter : "",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                    "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arResult["ITEMS"][2]["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => "36000010",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arResult["ITEMS"][2]["PROPERTIES"]["LINK"]["VALUE"],
                                    'ALL_NEWS_TITLE' => 'criticism'
                                ),
                                false
                            );
                        }
                        ?>
                        <div class="clearfix"></div>

                        <?

                        if ($arResult["ITEMS"][2]) {
                            //filter
                            if (is_array($arResult["ITEMS"][2]["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                                $temp_needed_elemets = $arResult["ITEMS"][2]["PROPERTIES"]["ELEMENTS"]["VALUE"];
                                foreach ($temp_needed_elemets as $filter_element_id_key => $filter_element_id) {
                                    if (in_array($filter_element_id, $GLOBALS['except_news_id_' . $arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . $arResult["ITEMS"][2]["PROPERTIES"]["SECTION"]["VALUE"]])) {
                                        unset($temp_needed_elemets[$filter_element_id_key]);
                                    }
                                }

                                $GLOBALS['oneNewsInListFilter'] = array('ID' => $temp_needed_elemets);
                            } else {
                                $GLOBALS['oneNewsInListFilter'] = array('!ID' => $GLOBALS['except_news_id_' . $arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . $arResult["ITEMS"][2]["PROPERTIES"]["SECTION"]["VALUE"]]);
                            }

                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arResult["ITEMS"][2]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => "oneNewsInListFilter",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                    "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arResult["ITEMS"][2]["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => "36000010",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arResult["ITEMS"][2]["PROPERTIES"]["LINK"]["VALUE"],
                                    'ALL_NEWS_TITLE' => 'criticism'
                                ),
                                false
                            );
                        }
                        ?>

                        <?
                        if ($arResult["ITEMS"][1]) {
//                // filter
                            if (is_array($arResult["ITEMS"][1]["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                                $temp_needed_elemets = $arResult["ITEMS"][1]["PROPERTIES"]["ELEMENTS"]["VALUE"];
                                foreach ($temp_needed_elemets as $filter_element_id_key => $filter_element_id) {
                                    if (in_array($filter_element_id, $GLOBALS['except_news_id_' . $arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . $arResult["ITEMS"][1]["PROPERTIES"]["SECTION"]["VALUE"]])) {
                                        unset($temp_needed_elemets[$filter_element_id_key]);
                                    }
                                }

                                $GLOBALS['oneNewsInListFilter'] = array('ID' => $temp_needed_elemets);
                            } else {
                                $GLOBALS['oneNewsInListFilter'] = array('!ID' => $GLOBALS['except_news_id_' . $arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . $arResult["ITEMS"][1]["PROPERTIES"]["SECTION"]["VALUE"]]);
                            }

                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arResult["ITEMS"][1]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => "oneNewsInListFilter",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                    "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arResult["ITEMS"][1]["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => "36000010",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arResult["ITEMS"][1]["PROPERTIES"]["LINK"]["VALUE"]
                                ),
                                false
                            );
                        }
                        ?>

                        <?
                        if ($arResult["ITEMS"][0]) {
//                // filter
                            if (is_array($arResult["ITEMS"][0]["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                                $temp_needed_elemets = $arResult["ITEMS"][0]["PROPERTIES"]["ELEMENTS"]["VALUE"];
                                foreach ($temp_needed_elemets as $filter_element_id_key => $filter_element_id) {
                                    if (in_array($filter_element_id, $GLOBALS['except_news_id_' . $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . $arResult["ITEMS"][0]["PROPERTIES"]["SECTION"]["VALUE"]])) {
                                        unset($temp_needed_elemets[$filter_element_id_key]);
                                    }
                                }

                                $GLOBALS['oneNewsInListFilter'] = array('ID' => $temp_needed_elemets);
                            } else {
                                $GLOBALS['oneNewsInListFilter'] = array('!ID' => $GLOBALS['except_news_id_' . $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . $arResult["ITEMS"][0]["PROPERTIES"]["SECTION"]["VALUE"]]);
                            }

                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arResult["ITEMS"][0]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => "oneNewsInListFilter",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                    "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arResult["ITEMS"][0]["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => "36000010",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"]
                                ),
                                false
                            );
                        }
                        ?>

                        <?
                        if ($arResult["ITEMS"][3]) {
                            $APPLICATION->IncludeComponent(
                                "restoran:catalog.list",
                                "news_main_index_summer_2015",
                                Array(
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "AJAX_MODE" => "N",
                                    "IBLOCK_TYPE" => $arResult["ITEMS"][3]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                    "IBLOCK_ID" => $arResult["ITEMS"][3]["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                    "NEWS_COUNT" => "1",
                                    "SORT_BY1" => ($arResult["ITEMS"][3]["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] == "blogs") ? "created_date" : "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "DESC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => array("DETAIL_PICTURE", "LIST_PAGE_URL", 'SHOW_COUNTER'),
                                    "PROPERTY_CODE" => array("RESTORAN", 'COMMENTS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "140",
                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => $arResult["ITEMS"][3]["PROPERTIES"]["SECTION"]["VALUE"],
                                    "PARENT_SECTION_CODE" => "",
                                    "CACHE_TYPE" => "A",//a
                                    "CACHE_TIME" => "36000010",
                                    "CACHE_FILTER" => "Y",
                                    "CACHE_GROUPS" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "ANOTHER_LINK" => $arResult["ITEMS"][3]["PROPERTIES"]["LINK"]["VALUE"]
                                ),
                                false
                            );
                        }
                        ?>
                    <? }//!nsk
                }?>
            </div>
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                <div class="tab-pane sm" id="index_journal_<?=$arItem["CODE"]?>"></div>
            <?endforeach;?>

        </div>
    </div>
<?endif;?>