<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                                 
<?if(count($arResult["ITEMS"])>0):?>
    <div class="index-journal-wrapper index-afisha-full-wrapper">

        <?if(preg_match('/afisha/',$APPLICATION->GetCurDir())):?>
            <div class="index-journal-title-wrapper">
                <?if($_REQUEST['restoran']):
                    $res = CIBlockElement::GetByID($_REQUEST['restoran']);
                    $ar_res = $res->GetNext();
                    ?>
                    <div class="index-journal-title"><h1>Афиша <?if($ar_res['NAME']):?><a href="<?=$ar_res['DETAIL_PAGE_URL']?>">Ресторана <?=$ar_res['NAME']?></a><?endif?></h1></div>
                <?else:?>
                    <div class="index-journal-bg-line"></div>
                    <div class="index-journal-title"><h1>Афиша</h1></div>
                    <div class="under-index-journal-title-sign">популярные мероприятия</div>
                <?endif?>
            </div>

        <?else:?>
            <div class="index-journal-bg-line"></div>
            <div class="index-journal-title">Афиша</div>
            <div class="under-index-journal-title-sign">популярные мероприятия</div>
        <?endif?>


        <div class="index-nav-tabs-wrapper">
            <ul class="nav nav-tabs new-history">
                <li class="active"><a href="#index_afisha" data-toggle="tab" class="ajax" data-afisha="y">Все вместе</a></li>
                <?
                $arAfishaIB = getArIblock("afisha", CITY_ID);

                foreach($arResult["ITEMS"] as $key=>$arItem):?>
                    <li>
                        <?
                        if($_REQUEST['NEW_FILTER']=='Y'&&!$_REQUEST['restoran']){
                            $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] = 'afisha_list_index_summer_2015_1';
                        }
                        elseif($_REQUEST['restoran']&&$_REQUEST['NEW_FILTER']=='Y'){
                            $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] = 'afisha_list_index_summer_2017';    //  ШАБЛОН ДЛЯ ВЫВОДА МЕРОПРИЯТИЙ РЕСТОРАНА БЕЗ ТЕКУЩЕЙ ДАТЫ - ВСЯ АФИША РЕСТОРАНА - СТАРТОВАЯ СТРАНИЦА
                        }
                        ?>
                        <?
                        $el = "";
                        if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"])) {
                            $el = implode("&el[]=", $arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
                        }
                        $url = "&t=" . $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] . "&el[]=" . $el . "&ib=" . $arAfishaIB['ID'] . "&c=" . $arItem["PROPERTIES"]["COUNT"]["VALUE"] . "&s=" . $arItem["PROPERTIES"]["SECTION"]["VALUE"] . "&code=" . $arItem["CODE"] . "&restoran_id=".$_REQUEST['restoran'];
                        ?>
                        <a href="#index_afisha_<?=$arItem["CODE"]?>" class="ajax" data-afisha="y" data-toggle="tab" data-href="<?=SITE_TEMPLATE_PATH?>/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=<?= CITY_ID ?><?= $url ?>">
                            <?= $arItem["NAME"] ?>
                        </a>
                    </li>
                <?endforeach;?>
            </ul>
            <?if(!preg_match('/afisha/',$APPLICATION->GetCurDir())):?><a href="/<?=CITY_ID?>/afisha/" class="btn btn-info btn-nb-empty all-afisha-link">Вся афиша</a><?endif?>
        </div>

        <div class="tab-content">
            <div class="tab-pane afisha active" id="index_afisha">
                <?
                if($_REQUEST['NEW_FILTER']=='Y'){   //  внутренние страницы
                    global $arFilter;

                    $special_date = htmlentities($_REQUEST['date']);
                    if($special_date){
                        $day = date("N", strtotime($special_date));
                        $arFilter = Array(
                            Array("LOGIC"=>"OR",
                                Array("!PROPERTY_d".$day=>false),
                                Array("PROPERTY_EVENT_DATE"=>date('Y-m-d',strtotime($special_date)).' 00:00:00'),
                            ),
                            'PROPERTY_RESTORAN.ACTIVE'=>'Y'
                        );
                    }
                    else {

                        if($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"] != 'afisha_list_index_summer_2017'){
                            $day = date("N");
                            $arFilter = Array(
                                Array("LOGIC"=>"OR",
                                    Array("!PROPERTY_d".$day=>false),
                                    Array("PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
                                ),
                                'PROPERTY_RESTORAN.ACTIVE'=>'Y'
                            );
                        }
                        else {  // ДЛЯ afisha_list_index_summer_2017
                            $arFilter = Array(
                                'PROPERTY_RESTORAN.ACTIVE'=>'Y',
                                Array("LOGIC"=>"OR",
                                    Array(">=PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
                                    Array("PROPERTY_EVENT_DATE"=>false),
                                ),
                            );
                        }

                    }

                    if ($_REQUEST["restoran"])
                    {
                        $arFilter["PROPERTY_RESTORAN"] = $_REQUEST["restoran"];
                    }

                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list", $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"], Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => 'afisha',
                        "IBLOCK_ID" => $arAfishaIB['ID'],
                        "NEWS_COUNT" => "250",//550
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => 'SORT',
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arFilter",
                        "FIELD_CODE" => array("DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "200",
                        "ACTIVE_DATE_FORMAT" => "j F",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//
                        "CACHE_TIME" => "86412",//7208
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        'AJAX_EVENT_DATE' => $_REQUEST['date']?date('d.m.Y',strtotime($_REQUEST['date'])):date("d.m.Y"),
                        'INNER_PAGE'=>$arParams['INNER_PAGE']
                    ), false
                    );


                }
                else {  //  главная

//                    if($_REQUEST['MOST_NEW_FILTER']){

                        global $arFilter;

                        $day = date("N");

                        $arFilter = Array(
                            Array("LOGIC"=>"OR",
                                Array("!PROPERTY_d".$day=>false),
                                Array("PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
                            ),
                            'PROPERTY_RESTORAN.ACTIVE'=>'Y'
                        );

                        //$arFilter = Array(
                        //    Array("LOGIC"=>"OR",
                        //        Array("!PROPERTY_d".$day=>false),
                        //        Array(">=PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
                        //    ),
                        //    Array("LOGIC"=>"OR",
                        //        Array("!DATE_ACTIVE_FROM"=>"NULL", "<=DATE_ACTIVE_FROM" => date("d.m.Y")),
                        //        Array("DATE_ACTIVE_FROM"=>false)
                        //    ),
                        //    Array("LOGIC"=>"OR",
                        //        Array("!DATE_ACTIVE_TO"=>"NULL", ">=DATE_ACTIVE_TO" => date("d.m.Y")),
                        //        Array("DATE_ACTIVE_TO"=>false)
                        //    ),
                        //    'PROPERTY_RESTORAN.ACTIVE'=>'Y'
                        //);
                        $APPLICATION->IncludeComponent(
                            "bitrix:news.list", "afisha_list_index_summer_2016", Array( //afisha_list_index_summer_2016 - для ajax вкладок по датам , в остальных случаях afisha_list_index_summer_2015
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => 'afisha',
                            "IBLOCK_ID" => $arAfishaIB['ID'],
                            "NEWS_COUNT" => "550",//550
                            "SORT_BY1" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => 'SORT',
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arFilter",
                            "FIELD_CODE" => array("DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array("EVENT_TYPE"),//, "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "200",
                            "ACTIVE_DATE_FORMAT" => "j F",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "Y",//$USER->IsAdmin()?"N":
                            "CACHE_TIME" => "86412",//7208
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ), false
                        );
//                    }
//                    else {
//
//                        global $arFilter;
//
//                        $day = date("N");
//
//                        $days_to_show[] = $day;
//                        for ($i=0;$i<4;$i++) {
//                            if ($day == 7)
//                                $day = 1;
//                            else
//                                $day++;
//                            $days_to_show[] = $day;
//                        }
//
////                        FirePHP::getInstance()->info(date("d.m.Y", strtotime('+4 days')),'last date');
//
//                        $arFilter = Array(
//                            Array("LOGIC"=>"OR",
//                                Array("!PROPERTY_d".$days_to_show[0]=>false),
//                                Array("!PROPERTY_d".$days_to_show[1]=>false),
//                                Array("!PROPERTY_d".$days_to_show[2]=>false),
//                                Array("!PROPERTY_d".$days_to_show[3]=>false),
//                                Array("!PROPERTY_d".$days_to_show[4]=>false),
//                                Array(">=PROPERTY_EVENT_DATE"=>date('Y-m-d').' 00:00:00'),
//                            ),
//                            Array("LOGIC"=>"OR",
//                                Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y h")),
//                                Array("DATE_ACTIVE_TO"=>false)
//                            ),
//                            'PROPERTY_RESTORAN.ACTIVE'=>'Y'
//                        );
////                        ,
////                        Array("LOGIC"=>"OR",
////                            Array("!DATE_ACTIVE_FROM"=>"NULL", "<=DATE_ACTIVE_FROM" => date("d.m.Y h"))),
////                            Array("DATE_ACTIVE_FROM"=>false)
////                        ),
////                        Array("LOGIC"=>"OR",
////                            Array("!DATE_ACTIVE_TO"=>"NULL",">=DATE_ACTIVE_TO" => date("d.m.Y", strtotime('+4 days')),
////                            Array("DATE_ACTIVE_TO"=>false)
////                        ),
//
//                        $APPLICATION->IncludeComponent(
//                            "bitrix:news.list", "afisha_list_index_summer_2015", Array(
//                            "DISPLAY_DATE" => "N",
//                            "DISPLAY_NAME" => "Y",
//                            "DISPLAY_PICTURE" => "Y",
//                            "DISPLAY_PREVIEW_TEXT" => "Y",
//                            "AJAX_MODE" => "N",
//                            "IBLOCK_TYPE" => 'afisha',
//                            "IBLOCK_ID" => $arAfishaIB['ID'],
//                            "NEWS_COUNT" => "550",//550
//                            "SORT_BY1" => "SORT",
//                            "SORT_ORDER1" => "ASC",
//                            "SORT_BY2" => 'SORT',
//                            "SORT_ORDER2" => "ASC",
//                            "FILTER_NAME" => "arFilter",
//                            "FIELD_CODE" => array("DETAIL_PICTURE"),
//                            "PROPERTY_CODE" => array("EVENT_TYPE", "TIME", "EVENT_DATE",'d1','d2','d3','d4','d5','d6','d7','RESTORAN','ADRES'),
//                            "CHECK_DATES" => "Y",
//                            "DETAIL_URL" => "",
//                            "PREVIEW_TRUNCATE_LEN" => "200",
//                            "ACTIVE_DATE_FORMAT" => "j F",
//                            "SET_TITLE" => "N",
//                            "SET_STATUS_404" => "N",
//                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                            "ADD_SECTIONS_CHAIN" => "N",
//                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                            "PARENT_SECTION" => "",
//                            "PARENT_SECTION_CODE" => "",
//                            "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",
//                            "CACHE_TIME" => "86414",//7208
//                            "CACHE_FILTER" => "Y",
//                            "CACHE_GROUPS" => "N",
//                            "DISPLAY_TOP_PAGER" => "N",
//                            "DISPLAY_BOTTOM_PAGER" => "N",
//                            "PAGER_TITLE" => "Новости",
//                            "PAGER_SHOW_ALWAYS" => "N",
//                            "PAGER_TEMPLATE" => "",
//                            "PAGER_DESC_NUMBERING" => "N",
//                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                            "PAGER_SHOW_ALL" => "N",
//                            "AJAX_OPTION_JUMP" => "N",
//                            "AJAX_OPTION_STYLE" => "Y",
//                            "AJAX_OPTION_HISTORY" => "N"
//                        ), false
//                        );
//                    }

                }


                ?>
            </div>
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                <div class="tab-pane sm afisha" id="index_afisha_<?=$arItem["CODE"]?>"></div>
            <?endforeach;?>
        </div>
    </div>


<?endif;?>