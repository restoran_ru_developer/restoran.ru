<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>

<h3>Профайл ресторатора</h3>

<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
    <?=$arResult["BX_SESSION_CHECK"]?>
    <input type="hidden" name="lang" value="<?=LANG?>" />
    <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

    <b><i>Мои данные</i></b>
    
    <table>
        <tr>
       		<td><?=GetMessage('NAME')?></td>
       		<td><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></td>
       	</tr>
        <tr>
       		<td><?=GetMessage('LAST_NAME')?></td>
       		<td><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></td>
       	</tr>
        <tr>
       		<td><?=GetMessage('SECOND_NAME')?></font></td>
       		<td><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></td>
       	</tr>
        <tr>
       		<td><?=GetMessage('EMAIL')?></td>
       		<td>
                <input type="text" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                <input type="hidden" name="LOGIN" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
            </td>
       	</tr>
        <tr>
       		<td><?=GetMessage('NEW_PASSWORD_REQ')?></td>
       		<td><input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" /></td>
       	</tr>
        <tr>
       		<td><?=GetMessage('NEW_PASSWORD_CONFIRM')?></td>
       		<td><input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /></td>
       	</tr>
        <tr>
       		<td><?=GetMessage('USER_PHONE')?></td>
       		<td><input type="text" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /></td>
       	</tr>
        <tr>
       		<td><?=GetMessage('USER_CITY')?></td>
       		<td><input type="text" name="PERSONAL_CITY" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></td>
       	</tr>
        <tr>
            <td><?=GetMessage("USER_BIRTHDAY_DT")?> (<?=$arResult["DATE_FORMAT"]?>):</td>
            <td><?
            $APPLICATION->IncludeComponent(
                'bitrix:main.calendar',
                '',
                array(
                    'SHOW_INPUT' => 'Y',
                    'FORM_NAME' => 'form1',
                    'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                    'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                    'SHOW_TIME' => 'N'
                ),
                null,
                array('HIDE_ICONS' => 'Y')
            );
            ?></td>
        </tr>
        <tr>
            <td><?=GetMessage('USER_GENDER')?></td>
            <td>
                <select name="PERSONAL_GENDER">
                    <option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
                    <option value="M"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_MALE")?></option>
                    <option value="F"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><?=GetMessage('PREFERENCE_IN_THE_FOOD')?></td>
            <td><textarea rows="5" cols="40" name="PERSONAL_NOTES"><?=$arResult["arUser"]["PERSONAL_NOTES"]?></textarea></td>
        </tr>
    </table>

    <p><input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">&nbsp;&nbsp;<input type="reset" value="<?=GetMessage('MAIN_RESET');?>"></p>

    <b><i>Мои фирмы</i></b><br />
    <i>В разработке...</i><br /><br />

    <b><i>Мои подписки и уведомления</i></b><br />
    <i>В разработке...</i><br /><br />

    <b><i>Мои подписки и уведомления</i></b><br />
    <i>В разработке...</i><br /><br />

    <b><i>Мои бронирования</i></b><br />
    <i>В разработке...</i><br /><br />

    <b><i>Мои купоны</i></b><br />
    <i>В разработке...</i><br /><br />

    <b><i>Заказы</i></b><br />
    <i>В разработке...</i><br /><br />
</form>