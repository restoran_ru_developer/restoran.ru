<?
$MESS["PROFILE_DATA_SAVED"] = "Изменения сохранены";
$MESS["NAME"] = "Имя:";
$MESS["LAST_NAME"] = "Фамилия:";
$MESS["SECOND_NAME"] = "Отчество:";
$MESS["EMAIL"] = "E-Mail:";
$MESS["LOGIN"] = "Логин (мин. 3 символа):";
$MESS["NEW_PASSWORD"] = "Новый пароль (мин. 6 символов):";
$MESS["NEW_PASSWORD_CONFIRM"] = "Подтверждение нового пароля:";
$MESS["USER_GENDER"] = "Пол:";
$MESS["USER_DONT_KNOW"] = "(неизвестно)";
$MESS["USER_MALE"] = "Мужской";
$MESS["USER_FEMALE"] = "Женский";
$MESS["USER_BIRTHDAY"] = "Дата рождения (текст)";
$MESS["USER_BIRTHDAY_DT"] = "Дата рождения";
$MESS["USER_PHONE"] = "Телефон:";
$MESS["USER_CITY"] = "Город:";
$MESS["NEW_PASSWORD_REQ"] = "Новый пароль:";
$MESS["PREFERENCE_IN_THE_FOOD"] = "Предпочтения в кухне:";
?>