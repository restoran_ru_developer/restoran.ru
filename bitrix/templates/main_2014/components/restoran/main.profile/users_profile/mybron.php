<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/components/bitrix/main.profile/users_profile/lang/".SITE_LANGUAGE_ID."/template.php");
?>
<table width="100%" cellpadding="10" cellspacing="0"  class="my_bron">
    <tr>
        <th><?=$MESS['PLACE']?></th>
        <th width="150"><?=$MESS['STATUS']?></th>
        <th width="120"><?=$MESS['DATA']?></th>
        <th width="80"><?=$MESS['TIME']?></th>
        <th width="100"></th>
    </tr>
    <tr>
        <td><span class="another_color">Ресторан</a><br />
            <a href="#" class="another font18 no_border">Pinot Grigio</a></td>
        <td align="center"><br />Забронировано</td>
        <td align="center"><br />24.10.2011</td>
        <td align="center"><br />14:30</td>
        <td>
            <ul class="actions">
                <li class="retry">Повторить</li>
                <li class="del">Отменить</li>
            </ul>
        </td>
    </tr>
</table>
<br />
<div align="right">
        <input class="light_button" type="button" value="<?=$MESS["BRON"]?>" />
</div>