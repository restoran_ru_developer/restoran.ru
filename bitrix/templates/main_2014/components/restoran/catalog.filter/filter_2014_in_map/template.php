<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$this->setFrameMode(true);
$this->createFrame()->begin('Загрузка');
if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
?>
<noindex>
    <form action="/<?=CITY_ID?>/map/near/" method="get" name="rest_filter_form" class="form-inline fil prop_fil" role="form" id="rest_filter_form">
        <input type="hidden" name="page" value="1">
        <div class="form-group inverted">
            <?=GetMessage("FIND_BY_PARAMS")?>
        </div>
        <div class="form-group inverted">

            <?if (LANGUAGE_ID!="en"):?>
                <?
                if ($_REQUEST["CATALOG_ID"]=="restaurants"||!$_REQUEST["CATALOG_ID"]):
                    $prop_id = 11;
                ?>
                <div class="d"></div>
                <div class="dropdown">
                    <a data-toggle="dropdown" href="#">Тип<span class="caret"></span></a>
                    <div class="dropdown-menu w480 toc" data-prop="<?=$prop_id?>" data-code="type">
                        <div class="title_box">
                            <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_FEATURES")?></div>
                            <div class="pull-right">
                                <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                            </div>
                        </div>
                        <ul>
                            <li><a data-code="type" data-val="Банкетный зал" id="1349" class="<?=(in_array(1349,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Банкетный зал</a></li>
                            <li><a data-code="type" data-val="Бар" id="1343" class="<?=(in_array(1343,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Бар</a></li>
                            <li><a data-code="type" data-val="Гастробар" id="1990798" class="<?=(in_array(1990798,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Гастробар</a></li>
                            <li><a data-code="type" data-val="Загородный ресторан" id="6670" class="<?=(in_array(6670,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Загородный ресторан</a></li>
                            <li><a data-code="type" data-val="Караоке-бар" id="6752" class="<?=(in_array(6752,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Караоке-бар</a></li>
                            <li><a data-code="type" data-val="Кафе" id="1330" class="<?=(in_array(1330,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Кафе</a></li>
                        </ul>
                        <ul>
                            <li><a data-code="type" data-val="Кафе-бар" id="184374" class="<?=(in_array(184374,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Кафе-бар</a></li>
                            <li><a data-code="type" data-val="Кейтеринг" id="6794" class="<?=(in_array(6794,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Кейтеринг</a></li>
                            <li><a data-code="type" data-val="Клуб" id="1372" class="<?=(in_array(1372,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Клуб</a></li>
                            <li><a data-code="type" data-val="Кофейня/Кондитерская" id="6682" class="<?=(in_array(6682,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Кофейня/Кондитерская</a></li>
                            <li><a data-code="type" data-val="Паб" id="6706" class="<?=(in_array(6706,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Паб</a></li>
                            <li><a data-code="type" data-val="Пивной ресторан" id="432588" class="<?=(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Пивной ресторан</a></li>
                        </ul>
                        <ul>
                            <li><a data-code="type" data-val="Ресторан" id="1345" class="<?=(in_array(1345,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Ресторан</a></li>
                            <li><a data-code="type" data-val="Суши бар" id="6656" class="<?=(in_array(6656,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Суши бар</a></li>
                            <li><a data-code="type" data-val="Теплоход" id="6732" class="<?=(in_array(6732,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Теплоход</a></li>
                            <li><a data-code="type" data-val="Чайхана" id="2070767" class="<?=(in_array(2070767,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Чайхана</a></li>
                            <li><a data-code="type" data-val="Центр развлечений" id="6671" class="<?=(in_array(6671,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Центр развлечений</a></li>
                            <li><a data-code="type" data-val="Чайный ресторан" id="6763" class="<?=(in_array(6763,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Чайный ресторан</a></li>
                        </ul>
                        <?if(in_array(1349,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Банкетный зал"  name="arrFilter_pf[type][]" id="hid1349" value="1349" />
                        <?endif;?>
                        <?if(in_array(1343,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Бар"  name="arrFilter_pf[type][]" id="hid1343" value="1343" />
                        <?endif;?>
                        <?if(in_array(1990798,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Гастробар"  name="arrFilter_pf[type][]" id="hid1990798" value="1990798" />
                        <?endif;?>

                        <?if(in_array(6752,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Караоке-бар"  name="arrFilter_pf[type][]" id="hid6752" value="6752" />
                        <?endif;?>
                        <?if(in_array(1330,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Кафе"  name="arrFilter_pf[type][]" id="hid1330" value="1330" />
                        <?endif;?>
                        <?if(in_array(6794,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Кейтеринг"  name="arrFilter_pf[type][]" id="hid6794" value="6794" />
                        <?endif;?>
                        <?if(in_array(184374,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Кафе-бар"  name="arrFilter_pf[type][]" id="hid184374" value="184374" />
                        <?endif;?>
                        <?if(in_array(1372,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Клуб"  name="arrFilter_pf[type][]" id="hid1372" value="1372" />
                        <?endif;?>
                        <?if(in_array(6682,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Кофейня/Кондитерская"  name="arrFilter_pf[type][]" id="hid6682" value="6682" />
                        <?endif;?>
                        <?if(in_array(6706,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Паб"  name="arrFilter_pf[type][]" id="hid6706" value="6706" />
                        <?endif;?>
                        <?if(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Пивной ресторан"  name="arrFilter_pf[type][]" id="hid432588" value="432588" />
                        <?endif;?>
                        <?if(in_array(1345,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Ресторан"  name="arrFilter_pf[type][]" id="hid1345" value="1345" />
                        <?endif;?>
                        <?if(in_array(6656,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Суши бар"  name="arrFilter_pf[type][]" id="hid6656" value="6656" />
                        <?endif;?>
                        <?if(in_array(6732,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Теплоход"  name="arrFilter_pf[type][]" id="hid6732" value="6732" />
                        <?endif;?>
                        <?if(in_array(2070767,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Чайхана"  name="arrFilter_pf[type][]" id="hid2070767" value="2070767" />
                        <?endif;?>
                        <?if(in_array(6671,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Центр развлечений"  name="arrFilter_pf[type][]" id="hid6671" value="6671" />
                        <?endif;?>
                        <?if(in_array(6763,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Чайный ресторан"  name="arrFilter_pf[type][]" id="hid6763" value="6763" />
                        <?endif;?>
                    </div>
                </div>

                <?endif?>
            <?endif?>

            <?
            foreach($arResult["arrProp"] as $prop_id => $arProp):
                if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
            ?>
                    <div class="d"></div>
                    <div class="dropdown" style="position:<?=$arProp["CODE"]=='area'&&CITY_ID=='msk'?"inherit":"relative"?>;">
                        <a id="as<?=$arProp["CODE"]?>" data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>    
                        
                        <?
                        $e = 8; $pp=0;
                        if ((count($arProp["VALUE_LIST"])-$e)>=8)
                            $e = 8;                                                
                        if ((count($arProp["VALUE_LIST"])-$e)>=30)
                            $e = 12;
                        if ((count($arProp["VALUE_LIST"])-$e)>=50)
                            $e = 28;
                        if ((count($arProp["VALUE_LIST"])-$e)<8)
                            $e = 5;  
                        if ((count($arProp["VALUE_LIST"])-8)<0)
                            $e = 2;


                        $s = ceil(count($arProp["VALUE_LIST"])/$e);
                        $c = 160*$s;
                        if ($c < 480)
                            $c = 480;
                        if ($c > 700)
                            $c = 700;

                        if($arProp["CODE"]=='area'&&CITY_ID=='msk'){
                            $c = 100;
                            $e = 22;
                        }
                        ?>                        
                        <div class="dropdown-menu toc w<?=$c?>" role="menu" aria-labelledby="as<?=$arProp["CODE"]?>" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                            <div class="title_box">
                                <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
<!--                                    <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="--><?//=GetMessage("NF_OK")?><!--"> -->
                                </div>                                    
                            </div>                                                           
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>      
                                <?=($pp%$e==0)?"<ul>":""?>
                                <li><a data-val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>                                
<!--                                --><?//if (end($arProp["VALUE_LIST"])==$val):?>
<!--                                    --><?//if ($arProp["CODE"]=="out_city"):?>
<!--                                        <li><a class="unbind" href="/--><?//=CITY_ID?><!--/catalog/restaurants/out_city/all/">--><?//=GetMessage("ALL_OUT_CITY")?><!--</a></li>-->
<!--                                    --><?//endif;?>
<!--                                --><?//endif;?><!--   -->
                                <?=($pp%$e==($e-1)||end($arProp["VALUE_LIST"])==$val)?"</ul>":""?>
                            <?$pp++;endforeach;?>                            
                            <div class="clearfix"></div>
                            <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                <input type="hidden" data-val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                            <?endforeach;?>                                        
                        </div>
                    </div>      
                <?elseif ($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                    <div class="d"></div>
                    <div class="dropdown">
                        <a data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>    
                        <div class="dropdown-menu toc w700 subway" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                            <div class="title_box">
                                <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?>

                                    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/selectize-last.js" ></script>
                                    <link href="<?=SITE_TEMPLATE_PATH?>/css/selectize.css"  type="text/css" rel="stylesheet" />
                                    <script>
                                        $(function(){
                                            var was_click=false;
                                            var $select = $('#select-subway').selectize({
                                                placeholder: 'Выберите метро...',
                                                onItemAdd: function(value, $item){
                                                    if(!was_click){
                                                        search_lobj = $('#'+value+'.subway_station');
                                                        search_lobj.click();
                                                    }
                                                    was_click = false;
                                                },
                                                onItemRemove: function(value){
                                                    if(!was_click) {
                                                        search_lobj = $('#' + value + '.subway_station');
                                                        search_lobj.click();
                                                    }
                                                    was_click = false;
                                                }
                                            });
                                            selectize = $select[0].selectize;

                                            $('.selectize-input.items').on('click','.item', function(){
                                                if($('#select-subway option').length>1){
                                                    selectize.removeItem($(this).attr('data-value'));
                                                }
                                                else {
                                                    $('a.subway-reset-button').click();
                                                }
                                            });

                                            $('.subway_station').on('click', function(){    //  жмаканье на карте метро
                                                if($(this).hasClass('selected')){
                                                    was_click = true;
                                                    selectize.addItem($(this).attr('id'));
                                                }
                                                else {
                                                    was_click = true;
                                                    selectize.removeItem($(this).attr('id'));
                                                }
                                            });


                                        });
                                    </script>
                                </div>
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" class="subway-reset-button" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
<!--                                    <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="--><?//=GetMessage("NF_OK")?><!--"> -->
                                </div>                                    
                            </div>

                            <div class="dropdown-subway-wrapper" style="position: relative;">
                                <select id="select-subway" class="" multiple="multiple" >
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?> ><?=$val['NAME']?></option>
                                    <?endforeach;?>
                                </select>
                            </div>

                            <div class="subway-canvas-wrapper">
                                <div class="subway-map map-of-<?=CITY_ID?>">
                                    <?if (LANGUAGE_ID=="en"):?>
                                        <img src="/tpl/images/subway/<?=CITY_ID?>_en.gif" />
                                    <?else:?>
                                        <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                    <?endif;?>
                                    <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                        <?if ($val["STYLE"]):?>
                                            <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" data-val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"> </a>
                                        <?endif;?>
                                    <?endforeach;?>
                                    <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                            <input type="hidden" name="arrFilter_pf[subway][]" data-val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>  
                <?endif;?>
            <?endforeach;?>


            <?if (LANGUAGE_ID!="en") {
                if ($_REQUEST["CATALOG_ID"]=="restaurants"||!$_REQUEST["CATALOG_ID"]||$_REQUEST["CATALOG_ID"]=="banket"):
                    $prop_id = 1;

                ?>
                <div class="d"></div>
                <div class="dropdown">
                    <a data-toggle="dropdown" href="#"><?=GetMessage("FEATURES")?><span class="caret"></span></a>    
                    <div class="dropdown-menu w480 toc features" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                        <div class="title_box">
                            <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_FEATURES")?></div>
                            <div class="pull-right">
                                <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
<!--                                <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="--><?//=GetMessage("NF_OK")?><!--"> -->
                            </div>                                    
                        </div>
                        <ul>
                            <?if($_REQUEST["CATALOG_ID"]=="banket"&&(CITY_ID=='msk'||CITY_ID=='spb')):?>
                                <li><a data-code="type" data-val="Загородный ресторан" id="6670" class="<?=(in_array(6670,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Загородный ресторан</a></li>
                                <li><a data-code="features" data-val="Заведение у воды" id="6784" class="<?=(in_array(6784,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Заведение у воды</a></li>
                                <li><a data-code="features" data-val="При отеле" id="1968" class="<?=(in_array(1968,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">При отеле</a></li>
                                <li><a data-code="features" data-val="Летняя веранда" id="1921" class="<?=(in_array(1921,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Летняя веранда</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Шатер" id="2010637" class="<?=(in_array(2010637,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Шатер</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Подарки молодоженам" id="2010638" class="<?=(in_array(2010638,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Подарки молодоженам</a></li>
                                <li><a data-code="features" data-val="Панорамный вид на город" id="6766" class="<?=(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Панорамный вид на город</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Место для выездной регистрации" id="2010639" class="<?=(in_array(2010639,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Место для выездной регистрации</a></li>
                                <li><a data-code="ALLOWED_ALCOHOL" data-val="Свой алкоголь" id="1881746" class="<?=(in_array(1881746,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["ALLOWED_ALCOHOL"]))?"selected":""?>">Свой алкоголь</a></li>
                            <?else:?>
                                <li><a data-code="features" data-val="Панорамный вид" id="6766" class="<?=(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Панорамный вид</a></li>
                                <li><a data-code="children" data-val="Детские программы" id="185511" class="<?=(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"]))?"selected":""?>">Детские программы</a></li>
                                <li><a data-code="children" data-val="Детская комната" id="184520" class="<?=(in_array(184520,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"]))?"selected":""?>">Детская комната</a></li>

                                <?if(CITY_ID=='msk'||CITY_ID=='spb'):?>
                                    <li><a data-code="features" data-val="Камин" id="151655" class="<?=(in_array(151655,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Камин</a></li>
                                <?endif?>

                                <li><a data-code="proposals" data-val="Кальяны" id="444604" class="<?=(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"]))?"selected":""?>">Кальяны</a></li>
                                <li><a data-code="breakfast"  data-val="Завтраки" id="Y" class="<?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"]=="Y")?"selected":""?>">Завтраки</a></li>
                                <li><a data-code="entertainment" style="width:190px" data-val="Спортивные трансляции" id="2013" class="<?=(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Спортивные трансляции</a></li>
                                <li><a data-code="entertainment" data-val="Караоке" id="1290" class="<?=(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Караоке</a></li>

                                <?if(CITY_ID=='msk'):?><li><a data-code="features" data-val="Отдельный кабинет" id="1059243" class="<?=(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Отдельный кабинет</a></li><?endif?>

                            <?endif;?>
                        </ul>
                        <ul>
                            <?if($_REQUEST["CATALOG_ID"]=="banket"&&(CITY_ID=='msk'||CITY_ID=='spb')):?>

                                <li><a data-code="FEATURES_B_H" data-val="Отсутствие пробкового сбора" id="2010642" class="<?=(in_array(2010642,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Отсутствие пробкового сбора</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Аренда зала" id="2010643" class="<?=(in_array(2010643,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Аренда зала</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Наличие техоборудования" id="2010644" class="<?=(in_array(2010644,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Наличие техоборудования</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Круглые столы" id="2010647" class="<?=(in_array(2010647,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Круглые столы</a></li>
                                <li><a data-code="BANKET_SPECIAL_EQUIPMENT" data-val="Сцена" id="1881755" class="<?=(in_array(1881755,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["BANKET_SPECIAL_EQUIPMENT"]))?"selected":""?>">Сцена</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Живая музыка по заказу" id="2010648" class="<?=(in_array(2010648,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Живая музыка по заказу</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Программа" id="2010649" class="<?=(in_array(2010649,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Программа</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Украшение зала" id="2010650" class="<?=(in_array(2010650,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Украшение зала</a></li>
                                <li><a data-code="FEATURES_B_H" data-val="Парковка" id="2010651" class="<?=(in_array(2010651,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"]))?"selected":""?>">Парковка</a></li>

                            <?else:?>
                                <?if(CITY_ID=='spb'):?><li><a data-code="features" data-val="Отдельный кабинет" id="1059243" class="<?=(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Отдельный кабинет</a></li><?endif?>
    <!--                            <li><a href="/--><?//=CITY_ID?><!--/articles/wedding/" data-val="Свадьбы" id="1" class="--><?//=(in_array(1,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["wedding"]))?"selected":""?><!--">Свадьбы</a></li>-->
                                <?if(CITY_ID!='msk'):?><li><a data-code="d_tours" data-val="С 3D туром" id="2" class="<?=(in_array(2,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["d_tours"]))?"selected":""?>">С 3D туром</a></li><?endif?>
                                <li><a data-code="features" data-val="У воды" id="6784" class="<?=(in_array(6784,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">У воды</a></li>
                                <?if($_REQUEST["CATALOG_ID"]=="banket"):?>
                                    <li><a data-code="type" data-val="Теплоходы" id="6732" class="<?=(in_array(6732,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Теплоходы</a></li>
                                <?endif?>
                                <?if(CITY_ID=='msk'):?>
                                    <li><a data-code="rest_group" data-val="Ginza Project" id="1126744" class="<?=(in_array(1126744,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">Ginza Project</a></li>
                                    <li><a data-code="rest_group" data-val="Новиков Групп" id="1126933" class="<?=(in_array(1126933,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">Новиков Групп</a></li>
                                    <li><a data-code="rest_group" data-val="Maison Dellos" id="1126948" class="<?=(in_array(1126948,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">Maison Dellos</a></li>
                                    <li><a data-code="rest_group" data-val="White Rabbite Family" id="1983321" class="<?=(in_array(1983321,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">White Rabbite Family</a></li>
                                    <li><a data-code="rest_group" data-val="Рестораны А. Раппопорта" id="1879414" class="<?=(in_array(1879414,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">Рестораны А. Раппопорта</a></li>
                                <?endif?>
                                <?if(CITY_ID=='spb'):?>
                                    <li><a data-code="rest_group" data-val="Ginza Project" id="1128953" class="<?=(in_array(1128953,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">Ginza Project</a></li>
                                    <li><a data-code="rest_group" data-val="Italy-Group" id="2042108" class="<?=(in_array(2042108,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"]))?"selected":""?>">Italy-Group</a></li>
                                <?endif?>
                            <?endif?>
                        </ul>

                        <?if(CITY_ID=='msk'):?>
                            <?if(in_array(1126744,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Ginza Project"  name="arrFilter_pf[rest_group][]" id="hid1126744" value="1126744" />
                            <?endif;?>
                            <?if(in_array(1126933,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Новиков Групп"  name="arrFilter_pf[rest_group][]" id="hid1126933" value="1126933" />
                            <?endif;?>
                            <?if(in_array(1126948,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Maison Dellos"  name="arrFilter_pf[rest_group][]" id="hid1126948" value="1126948" />
                            <?endif;?>
                            <?if(in_array(1126863,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Ресторанный синдикат"  name="arrFilter_pf[rest_group][]" id="hid1126863" value="1126863" />
                            <?endif;?>
                            <?if(in_array(1983321,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="White Rabbite Family"  name="arrFilter_pf[rest_group][]" id="hid1983321" value="1983321" />
                            <?endif;?>
                            <?if(in_array(1879414,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Рестораны А. Раппопорта"  name="arrFilter_pf[rest_group][]" id="hid1879414" value="1879414" />
                            <?endif;?>
                        <?endif?>
                        <?if(CITY_ID=='spb'):?>
                            <?if(in_array(1128953,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Ginza Project"  name="arrFilter_pf[rest_group][]" id="hid1128953" value="1128953" />
                            <?endif;?>
                            <?if(in_array(2042108,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["rest_group"])):?>
                                <input type="hidden" data-val="Italy-Group"  name="arrFilter_pf[rest_group][]" id="hid2042108" value="2042108" />
                            <?endif;?>
                        <?endif?>



                        <?if(in_array(2010639,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Место для выездной регистрации"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010639" value="2010639" />
                        <?endif;?>
                        <?if(in_array(1881746,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["ALLOWED_ALCOHOL"])):?>
                            <input type="hidden" data-val="Свой алкоголь"  name="arrFilter_pf[ALLOWED_ALCOHOL][]" id="hid1881746" value="1881746" />
                        <?endif;?>
                        <?if(in_array(2010642,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Отсутствие пробкового сбора"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010642" value="2010642" />
                        <?endif;?>
                        <?if(in_array(2010643,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Аренда зала"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010643" value="2010643" />
                        <?endif;?>
                        <?if(in_array(2010644,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Наличие техоборудования"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010644" value="2010644" />
                        <?endif;?>
                        <?if(in_array(2010647,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Круглые столы"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010647" value="2010647" />
                        <?endif;?>
                        <?if(in_array(1881755,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["BANKET_SPECIAL_EQUIPMENT"])):?>
                            <input type="hidden" data-val="Сцена"  name="arrFilter_pf[BANKET_SPECIAL_EQUIPMENT][]" id="hid1881755" value="1881755" />
                        <?endif;?>
                        <?if(in_array(2010648,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Живая музыка по заказу"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010648" value="2010648" />
                        <?endif;?>
                        <?if(in_array(2010649,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Программа"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010649" value="2010649" />
                        <?endif;?>
                        <?if(in_array(2010650,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Украшение зала"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010650" value="2010650" />
                        <?endif;?>
                        <?if(in_array(2010651,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Парковка"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010651" value="2010651" />
                        <?endif;?>





                        <?if(in_array(6670,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Загородный ресторан"  name="arrFilter_pf[type][]" id="hid6670" value="6670" />
                        <?endif;?>
                        <?if(in_array(1968,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="При отеле"  name="arrFilter_pf[features][]" id="hid1968" value="1968" />
                        <?endif;?>
                        <?if(in_array(151655,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="Камин"  name="arrFilter_pf[features][]" id="hid151655" value="151655" />
                        <?endif;?>
                        <?if(in_array(1921,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="Летняя веранда"  name="arrFilter_pf[features][]" id="hid1921" value="1921" />
                        <?endif;?>
                        <?if(in_array(2010637,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Шатер"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010637" value="2010637" />
                        <?endif;?>
                        <?if(in_array(2010638,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["FEATURES_B_H"])):?>
                            <input type="hidden" data-val="Подарки молодоженам"  name="arrFilter_pf[FEATURES_B_H][]" id="hid2010638" value="2010638" />
                        <?endif;?>

                        <?if(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="Панорамный вид"  name="arrFilter_pf[features][]" id="hid6766" value="6766" />
                        <?endif;?>
                        <?if(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"])):?>
                            <input type="hidden" data-val="Детские программы"  name="arrFilter_pf[children][]" id="hid185511" value="185511" />
                        <?endif;?>
                        <?if(in_array(184520,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"])):?>
                            <input type="hidden" data-val="Детская комната"  name="arrFilter_pf[children][]" id="hid184520" value="184520" />
                        <?endif;?>
                        <?if(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"])):?>
                            <input type="hidden" data-val="Кальяны"  name="arrFilter_pf[proposals][]" id="hid444604" value="444604" />
                        <?endif;?>
                        <?if(in_array("Y",$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"])||$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"]=='Y'):?>
                            <input type="hidden" data-val="Завтраки"  name="arrFilter_pf[breakfast][]" id="hidY" value="Y" />
                        <?endif;?>
                        <?if(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                            <input type="hidden" data-val="Спортивные трансляции"  name="arrFilter_pf[entertainment][]" id="hid2013" value="2013" />
                        <?endif;?>
                        <?if(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                            <input type="hidden" data-val="Караоке"  name="arrFilter_pf[entertainment][]" id="hid1290" value="1290" />
                        <?endif;?>
                    <?if($_REQUEST["CATALOG_ID"]=="banket"):?>
                        <?if(in_array(1990798,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Гастробар"  name="arrFilter_pf[type][]" id="hid1990798" value="1990798" />
                        <?endif;?>
                        <?if(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Пивной ресторан"  name="arrFilter_pf[type][]" id="hid432588" value="432588" />
                        <?endif;?>
                    <?endif?>
                        <?if(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="Отдельный кабинет"  name="arrFilter_pf[features][]" id="hid1059243" value="1059243" />
                        <?endif;?>
                        <?if(in_array(6784,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="У воды"  name="arrFilter_pf[features][]" id="hid6784" value="6784" />
                        <?endif;?>
                        <?if(in_array(2,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["d_tours"])):?>
                            <input type="hidden" data-val="С 3D туром"  name="arrFilter_pf[d_tours]" id="hid2" value="2" />
                        <?endif;?>
                    <?if($_REQUEST["CATALOG_ID"]=="banket"):?>
                        <?if(in_array(6732,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Теплоходы"  name="arrFilter_pf[type]" id="hid6732" value="6732" />
                        <?endif;?>
                    <?endif;?>
                    </div>
                </div>                                        
                <?endif;?>
            <?}?>
        </div>
<!--        <div class="pull-right inverted">-->
<!--            --><?//if (!substr_count($APPLICATION->GetCurDir(), "cookery")):?>
<!--                <a href="/--><?//=CITY_ID?><!--/filter/">--><?//=GetMessage("EXTENDED_FILTER")?><!--</a>-->
<!--            --><?//endif;?>
<!--        </div>    -->
        <div class="clearfix"></div>
        <?
        if ($cat=="restaurants"):
            $mess = GetMessage("IBLOCK_SET_FILTER");
        else:
            $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
        endif;  
        ?>
        <div id="new_filter_results" style="display: none;">
            <div class="pull-left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
            <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
                <ul id="multi<?=$prop_id?>"><li class="end"></ul>
            <?endforeach;?>
                <ul id="multi11"><li class="end"></ul>
                <ul id="multi1"><li class="end"></ul>
<!--            <div class="pull-left">-->
<!--                <input class="btn btn-light" type="submit" value="--><?//=$mess?><!--">-->
<!--            </div>-->
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>   
    </form>
</noindex>