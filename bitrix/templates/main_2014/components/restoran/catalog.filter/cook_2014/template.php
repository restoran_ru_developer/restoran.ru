<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$this->setFrameMode(true);
$this->createFrame()->begin('Загрузка');
if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
?>
<noindex>
    <form action="/content/cookery/search.php" method="get" name="rest_filter_form" class="form-inline fil" role="form">
        <input type="hidden" name="page" value="1">
        <div class="form-group inverted">
            <?=GetMessage("FIND_BY_PARAMS")?>
        </div><div class="form-group inverted">
            <?
            $rrr = 0;
            foreach($arResult["arrProp"] as $prop_id => $arProp):
                if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
            ?>
                    <div class="d"></div>
                    <div class="dropdown cook">
                        <a data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>    
                        
                        <?
                        $e = 8; $pp=0;
                        if ((count($arProp["VALUE_LIST"])-$e)>=8)
                            $e = 6;
                        if ((count($arProp["VALUE_LIST"])-$e)>=18)
                            $e = 7;
                        if ((count($arProp["VALUE_LIST"])-$e)>=22)
                            $e = 8;
                        if ((count($arProp["VALUE_LIST"])-$e)>=50)
                            $e = 28;
                        if ((count($arProp["VALUE_LIST"])-$e)<8)
                            $e = 4;  
                        if ((count($arProp["VALUE_LIST"])-8)<0)
                            $e = 2;  
                        $s = ceil(count($arProp["VALUE_LIST"])/$e);                        
                        $c = 160*$s;
                        if ($c < 480)
                            $c = 480;
                        ?>
                        
                        <div class="dropdown-menu toc w<?=$c?> <?=($rrr>=3)?"wright":""?>" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                            <div class="title_box">
                                <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                    <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>"> 
                                </div>                                    
                            </div>                                                           
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>      
                                <?=($pp%$e==0)?"<ul>":""?>
                                <li><a data-val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>                                
                                <?if (end($arProp["VALUE_LIST"])==$val):?>
                                    <?if ($arProp["CODE"]=="out_city"):?>
                                        <li><a class="unbind" href="/<?=CITY_ID?>/catalog/restaurants/out_city/all/"><?=GetMessage("ALL_OUT_CITY")?></a></li>
                                    <?endif;?>
                                <?endif;?>   
                                <?=($pp%$e==($e-1)||end($arProp["VALUE_LIST"])==$val)?"</ul>":""?>
                            <?$pp++;endforeach;?>                            
                            <div class="clearfix"></div>
                            <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                <input type="hidden" data-val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                            <?endforeach;?>                                        
                        </div>
                    </div>      
                <?elseif ($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                    <div class="dropdown">
                        <a data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>    
                        <div class="dropdown-menu w700 subway" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                            <div class="title_box">
                                <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                    <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>"> 
                                </div>                                    
                            </div>                
                            <div class="subway-map">    
                                <?if (LANGUAGE_ID=="en"):?>
                                    <img src="/tpl/images/subway/<?=CITY_ID?>_en.gif" />
                                <?else:?>
                                    <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                <?endif;?>
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                    <?if ($val["STYLE"]):?>
                                        <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" data-val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"> </a>
                                    <?endif;?>
                                <?endforeach;?>
                                <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                        <input type="hidden" name="arrFilter_pf[subway][]" data-val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>  
                <?endif;?>
                    <?$rrr++;?>
            <?endforeach;?>          
        </div>        
        <div class="clearfix"></div>
        <?
        if ($cat=="restaurants"):
            $mess = GetMessage("IBLOCK_SET_FILTER");
        else:
            $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
        endif;  
        ?>
        <div id="new_filter_results" style="display: none;">
            <div class="pull-left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
            <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
                <ul id="multi<?=$prop_id?>"><li class="end"></ul>
            <?endforeach;?>
                <ul id="multi1"><li class="end"></ul>
            <div class="pull-left">
                <input class="btn btn-light" type="submit" value="<?=$mess?>">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>   
    </form>
</noindex>