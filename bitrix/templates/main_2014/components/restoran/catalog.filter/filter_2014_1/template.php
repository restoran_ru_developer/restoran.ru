<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$this->setFrameMode(true);
$this->createFrame()->begin('Загрузка');
if ($_REQUEST["CATALOG_ID"])
    $cat = $_REQUEST["CATALOG_ID"];
else
    $cat = "restaurants";
?>
<noindex>
    <form action="/<?=CITY_ID?>/catalog/<?=$cat?>/all/" method="get" name="rest_filter_form" class="form-inline fil prop_fil" role="form">
        <input type="hidden" name="page" value="1">
        <div class="form-group inverted">
            <?=GetMessage("FIND_BY_PARAMS")?>
        </div>
        <div class="form-group inverted">
            <?
            foreach($arResult["arrProp"] as $prop_id => $arProp):
                if ($arProp["CODE"]!="subway"&&count($arProp["VALUE_LIST"])>1):
            ?>
                    <div class="d"></div>
                    <div class="dropdown" style="position:relative;">                        
                        <a id="as<?=$arProp["CODE"]?>" data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>    
                        
                        <?
                        $e = 8; $pp=0;
                        if ((count($arProp["VALUE_LIST"])-$e)>=8)
                            $e = 8;                                                
                        if ((count($arProp["VALUE_LIST"])-$e)>=30)
                            $e = 12;
                        if ((count($arProp["VALUE_LIST"])-$e)>=50)
                            $e = 28;
                        if ((count($arProp["VALUE_LIST"])-$e)<8)
                            $e = 5;  
                        if ((count($arProp["VALUE_LIST"])-8)<0)
                            $e = 2;  
                        $s = ceil(count($arProp["VALUE_LIST"])/$e);
                        $c = 160*$s;
                        if ($c < 480)
                            $c = 480;
                        if ($c > 700)
                            $c = 700;
                        ?>                        
                        <div style="z-index:1000" class="dropdown-menu w<?=$c?>" role="menu" aria-labelledby="as<?=$arProp["CODE"]?>" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                            <div class="title_box">
                                <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                    <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>"> 
                                </div>                                    
                            </div>                                                           
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>      
                                <?=($pp%$e==0)?"<ul>":""?>
                                <li><a data-val="<?=$val?>" id="<?=$key?>" class="<?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>"><?=$val?></a>                                
                                <?if (end($arProp["VALUE_LIST"])==$val):?>
                                    <?if ($arProp["CODE"]=="out_city"):?>
                                        <li><a class="unbind" href="/<?=CITY_ID?>/catalog/restaurants/out_city/all/"><?=GetMessage("ALL_OUT_CITY")?></a></li>
                                    <?endif;?>
                                <?endif;?>   
                                <?=($pp%$e==($e-1)||end($arProp["VALUE_LIST"])==$val)?"</ul>":""?>
                            <?$pp++;endforeach;?>                            
                            <div class="clearfix"></div>
                            <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]] as $s):?>
                                <input type="hidden" data-val="<?=$arProp["VALUE_LIST"][$s]?>"  name="arrFilter_pf[<?=$arProp["CODE"]?>][]" id="hid<?=$s?>" value="<?=$s?>" />
                            <?endforeach;?>                                        
                        </div>
                    </div>      
                <?elseif ($arProp["CODE"]=="subway"&&count($arProp["VALUE_LIST"])>1):?>
                    <div class="d"></div>
                    <div class="dropdown">
                        <a data-toggle="dropdown"><?=GetMessage("MORE_".$arProp["CODE"])?><span class="caret"></span></a>    
                        <div class="dropdown-menu w700 subway" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                            <div class="title_box">
                                <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_".$arProp["CODE"])?></div>
                                <div class="pull-right">
                                    <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                    <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>"> 
                                </div>                                    
                            </div>                
                            <div class="subway-map">    
                                <?if (LANGUAGE_ID=="en"):?>
                                    <img src="/tpl/images/subway/<?=CITY_ID?>_en.gif" />
                                <?else:?>
                                    <img src="/tpl/images/subway/<?=CITY_ID?>.gif" />
                                <?endif;?>
                                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                    <?if ($val["STYLE"]):?>
                                        <a href="javascript:void(0)" class="subway_station <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>" id="<?=$key?>" data-val="<?=$val["NAME"]?>" style="<?=$val["STYLE"]?>"> </a>
                                    <?endif;?>
                                <?endforeach;?>
                                <?foreach($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["subway"] as $s):?>
                                        <input type="hidden" name="arrFilter_pf[subway][]" data-val="<?=$arProp["VALUE_LIST"][$s]["NAME"]?>" id="hid<?=$s?>" value="<?=$s?>" />
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>  
                <?endif;?>
            <?endforeach;?>


            <?if (LANGUAGE_ID!="en") {
                if ($_REQUEST["CATALOG_ID"]=="restaurants"||!$_REQUEST["CATALOG_ID"]):
                    $prop_id = 1;
            ?>  
                <div class="d"></div>
                <div class="dropdown">
                    <a data-toggle="dropdown" href="#"><?=GetMessage("FEATURES")?><span class="caret"></span></a>    
                    <div class="dropdown-menu w480 features" data-prop="<?=$prop_id?>" data-code="<?=$arProp["CODE"]?>">
                        <div class="title_box">
                            <div class="pull-left"><?=GetMessage("NF_CHOOSE")?> <?=GetMessage("NF_FEATURES")?></div>
                            <div class="pull-right">
                                <a href="javascript:void(0)" data-filter="filter_popup_<?=$prop_id?>" data-filid="multi<?=$prop_id?>"><?=GetMessage("NF_RESET")?></a>
                                <input type="button" class="btn btn-info filter_button" data-filid="multi1" value="<?=GetMessage("NF_OK")?>"> 
                            </div>                                    
                        </div>
                        <ul>
                            <li><a data-code="features" data-val="Панорамный вид" id="6766" class="<?=(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Панорамный вид</a></li>                                    
                            <li><a data-code="children" data-val="Детские программы" id="185511" class="<?=(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"]))?"selected":""?>">Детские программы</a></li>
                            <li><a data-code="proposals" data-val="Кальяны" id="444604" class="<?=(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"]))?"selected":""?>">Кальяны</a></li>
                            <li><a data-code="breakfast"  data-val="Завтраки" id="Y" class="<?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"]=="Y")?"selected":""?>">Завтраки</a></li>
                            <li><a data-code="entertainment" style="width:190px" data-val="Спорт на большом экране" id="2013" class="<?=(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Спорт на большом экране</a></li>
                            <li><a data-code="entertainment" data-val="Караоке" id="1290" class="<?=(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"]))?"selected":""?>">Караоке</a></li>
                        </ul>
                        <ul>
                            <li><a data-code="type" data-val="Пивной ресторан" id="432588" class="<?=(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">Пивной ресторан</a></li>
                            <li><a data-code="features" data-val="Отдельный кабинет" id="1059243" class="<?=(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"]))?"selected":""?>">Отдельный кабинет</a></li>
                            <li><a href="/<?=CITY_ID?>/articles/wedding/" data-val="Свадьбы" id="1" class="<?=(in_array(1,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["wedding"]))?"selected":""?>">Свадьбы</a></li>
                            <li><a data-code="d_tours" data-val="С 3D туром" id="2" class="<?=(in_array(2,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["d_tours"]))?"selected":""?>">С 3D туром</a></li>
                            <li><a data-code="type" data-val="На воде" id="6732" class="<?=(in_array(6732,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"]))?"selected":""?>">На воде</a></li>
                        </ul>
                        <?if(in_array(6766,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="Панорамный вид"  name="arrFilter_pf[features][]" id="hid6766" value="6766" />
                        <?endif;?>
                        <?if(in_array(185511,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["children"])):?>
                            <input type="hidden" data-val="Детские программы"  name="arrFilter_pf[children][]" id="hid185511" value="185511" />
                        <?endif;?>
                        <?if(in_array(444604,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["proposals"])):?>
                            <input type="hidden" data-val="Кальяны"  name="arrFilter_pf[proposals][]" id="hid444604" value="444604" />
                        <?endif;?>
                        <?if(in_array("Y",$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["breakfast"])):?>
                            <input type="hidden" data-val="Завтраки"  name="arrFilter_pf[breakfast][]" id="hidY" value="Y" />
                        <?endif;?>                                    
                        <?if(in_array(2013,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                            <input type="hidden" data-val="Спорт на большом экране"  name="arrFilter_pf[entertainment][]" id="hid2013" value="2013" />
                        <?endif;?>
                        <?if(in_array(1290,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["entertainment"])):?>
                            <input type="hidden" data-val="Караоке"  name="arrFilter_pf[entertainment][]" id="hid1290" value="1290" />
                        <?endif;?>                                
                        <?if(in_array(432588,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["type"])):?>
                            <input type="hidden" data-val="Пивной ресторан"  name="arrFilter_pf[type][]" id="hid432588" value="432588" />
                        <?endif;?>
                        <?if(in_array(1059243,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["features"])):?>
                            <input type="hidden" data-val="Отдельный кабинет"  name="arrFilter_pf[type][]" id="hid1059243" value="1059243" />
                        <?endif;?>
                        <?if(in_array(2,$_REQUEST[$arParams["FILTER_NAME"]."_pf"]["d_tours"])):?>
                            <input type="hidden" data-val="С 3D туром"  name="arrFilter_pf[d_tours]" id="hid2" value="2" />
                        <?endif;?>
                    </div>
                </div>                                        
                <?endif;?>
            <?}?>
        </div>
        <div class="pull-right inverted">
            <?if (!substr_count($APPLICATION->GetCurDir(), "cookery")):?>
                <a href="/<?=CITY_ID?>/filter/"><?=GetMessage("EXTENDED_FILTER")?></a>
            <?endif;?>
        </div>    
        <div class="clearfix"></div>
        <?
        if ($cat=="restaurants"):
            $mess = GetMessage("IBLOCK_SET_FILTER");
        else:
            $mess = GetMessage("IBLOCK_SET_FILTER_BANKET");
        endif;  
        ?>
        <div id="new_filter_results" style="display: none;">
            <div class="pull-left your_choose"><?=GetMessage("YOU_CHOOSE")?></div>
            <?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
                <ul id="multi<?=$prop_id?>"><li class="end"></ul>
            <?endforeach;?>
                <ul id="multi1"><li class="end"></ul>
            <div class="pull-left">
                <input class="btn btn-light" type="submit" value="<?=$mess?>">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>   
    </form>
</noindex>