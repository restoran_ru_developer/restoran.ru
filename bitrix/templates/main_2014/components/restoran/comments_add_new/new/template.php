<?
//  $APPLICATION->AddHeadString('<link href="'.$templateFolder.'/fineuploader.css" rel="stylesheet" type="text/css"/>');
?>
<?if (!$USER->IsAuthorized()):?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:system.auth.form",
        "no_auth",
        Array(
            "REGISTER_URL" => "",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "",
            "SHOW_ERRORS" => "N"
        ),
        false
    );?>
<?endif;?>
<form class="grey_form" name="attach" action="/bitrix/components/restoran/comments_add_new/ajax.php" method="post" id="comment_form<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>" enctype="multipart/form-data" >
    <?=bitrix_sessid_post()?>
    <div class="form-group input-block">
        <textarea name="review" class="review" placeholder="<?=GetMessage("REVIEW_TEXT")?>"></textarea>
        <input type="hidden" value="" id="input_ratio" name="ratio" />
    </div>
    <?if (!$USER->IsAuthorized()):?>
        <div class="form-group input-block email">
            <input value="" name="email" placeholder="Email" req="req" />
        </div>
        <div class="form-group captcha">
            <div class="QapTcha"></div>
        </div>
        <div class="clearfix"></div>
    <?endif?>
    <div class="img-container" id="img-container">
        <div class="clear"></div>
    </div>
    <div class="attach">
        <b><?=GetMessage('add_photo_before')?></b>
        <a class="attach_f" id="attach_photo<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>"><?=GetMessage('photo-link-title')?></a>
    </div>
    <input type="submit" id="add_commm" class="btn btn-info btn-nb pull-right" value="<?=GetMessage("REVIEWS_PUBLISH")?>">
    <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
    <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
    <input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
    <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />
    <input type="hidden" name="PARENT" value="<?=$arParams["PARENT"]?>">
    <div class="clearfix"></div>
</form>
<script>
    $(document).ready(function(){
//        console.log(<?//=$templateFolder?>//,'templateFolder');
        var files = new Array();
        var errorHandler = function(event, id, fileName, reason) {
            qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
            console.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);

        };
        $('#attach_photo<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>').fineUploader({
            text: {
                uploadButton: "<?=GetMessage('comment_add_new_new_photo')?>",
                cancelButton: "",
                waitingForResponse: "",
                dropProcessing: ""
            },

            multiple: true,
            disableCancelForFormUploads: true,
            request: {
                endpoint: "<?=$templateFolder?>/upload.php",
                params: {"generateError": true,"f":"images"}
            },
            validation:{
                allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
            },
            failedUploadTextDisplay: {
                maxChars: 5
            },
            messages: {
                typeError: "<?=GetMessage('warning-file-type')?>"
            }
        })
            .on('upload', function(id, fileName){
                $(".qq-upload-list").show();
            })
            .on('error', errorHandler)
            .on('complete', function(event, id, fileName, response) {
                if (response.success)
                {
                    console.log('complete');
                    files.push(response.resized);
                    $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                    $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                    $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
                    $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));
                    $(".qq-upload-list").hide();
                    $(".qq-upload-success").remove();
                }
            });

        $(".img-container").on("click",".remove-link",function(){
            var file = $(this).next().attr("src");
            $(this).parents(".image-block").remove();
            return false;
        });

        $("#comment_form<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>").keypress(function(e){
            e = e || window.event;
            //for chrome & safari
            if (e.ctrlKey) {
                if(e.keyCode == 10){
                    $("#comment_form<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>").submit();
                    return false;
                }
            };
            //for firefox
            if (e.keyCode == 13 && e.ctrlKey) {
                $("#comment_form<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>").submit();
                return false;
            };
        });
        $("#comment_form<?=$arParams["ELEMENT_ID"]?><?=$arParams["PARENT"]?>").submit(function(){
            if (!$(this).find(".review").val())
            {
                alert("<?=GetMessage('comment_add_new_new_enter_comment')?>");
                return false;
            }
            $("#add_commm").attr("disabled",true);
            var params = $(this).serialize();
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: params,
                success: function(data) {
                    $("#add_commm").attr("disabled",false);
                    if(data)
                    {
                        data = eval('('+data+')');
                        alert(data.MESSAGE);
                        if (data.ERROR=="1")
                        {
                            $("#add_commm").attr("disabled",false);
                        }
                        if (data.STATUS=="1")
                        {
                            location.reload();
                            //setTimeout("location.reload()","3500");
                        }
                    }
                }
            });
            return false;
        });
    });

</script>