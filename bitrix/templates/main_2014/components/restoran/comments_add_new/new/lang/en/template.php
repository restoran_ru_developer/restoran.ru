<?
$MESS["ATTACH"] = "Attach";
$MESS["IMAGE"] = "Image";
$MESS["VIDEO"] = "Video";
$MESS["COMMENT"] = "Comment";
$MESS["R_COMMENT"] = "Comment";
$MESS["YOUR_REVIEW"] = "Your comment";
$MESS["NO_COMMENTS"] = "There still has not been written, you can be the first";
$MESS["ALL_REVIEWS"] = "ALL COMMENTS";
$MESS["ONLY_AUTHORIZED"] = "Only authorized users can leave comments.";
$MESS["CLICK_TO_RATE"] = "Click to rate restaurant";
$MESS["ADD_PHOTO"] = "Add Photo";
$MESS["ADD_VIDEO"] = "Add Video";
$MESS["ADD_PROS_CONS"] = "Add pros and cons of places";
$MESS["REVIEW_TEXT"] = "Your review";
$MESS["PROS"] = "ADVANTAGES";
$MESS["CONS"] = "DISADVANTAGES";
$MESS["MORE"] = "More";
$MESS["YOUTUBE_LINK"] = "or a link from Youtube";
$MESS["MOVE_SLIDER"] = "Move the slider to the right.";
$MESS["MOVE_SLIDER_DONE"] = "Done. You can now send";
$MESS["REVIEWS_PUBLISH"] = "Publish";
$MESS["ER_RATING"] = "Not selected rating";
$MESS["ER_TEXT"] = "Fill in the comment";
$MESS["R_TERMS"] = "<p>Dear friends! Remember that the administration of the site will be removed:</p>
        <p>1. Comments with profanity and rough<br />
        2. Direct or indirect insults hero or post readers<br />
        3. Short evaluative comments (\"awful\", \"class\", \"sucks\")<Br />
        4. Comments that incite national and social strife<br />
        <p><a href='/auth/user_license_agreement.php' target='_blank'>The full version of the rules Restoran.ru</a></p>";
$MESS["SAVE_COMMENT"] = "Thank you, your comment is saved.";
$MESS["LEAVE_COMMENT"] = "Write a comment";
$MESS["YOUR_COMMENT"] = "Your comment";
$MESS["R_COMMENT_ADD"] = "Comment";
$MESS["add_photo_before"] = "add:";
$MESS["photo-link-title"] = "photo";
$MESS["warning-file-type"] = "Invalid file type. You can upload the following formats: jpg, gif, png";
$MESS["comment_add_new_new_photo"] = "photo";
$MESS["comment_add_new_new_enter_comment"] = "Enter a comment!";
?>