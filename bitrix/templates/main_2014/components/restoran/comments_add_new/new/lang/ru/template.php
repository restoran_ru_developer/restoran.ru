<?
$MESS["ATTACH"] = "Прикрепить";
$MESS["IMAGE"] = "Фотографию";
$MESS["VIDEO"] = "Видеозапись";
$MESS["COMMENT"] = "Оставить комментарий";
$MESS["R_COMMENT"] = "Комментарий";
$MESS["YOUR_REVIEW"] = "Ваш отзыв";
$MESS["NO_COMMENTS"] = "Здесь еще никто не писал, Вы можете быть первым";
$MESS["ALL_REVIEWS"] = "ВСЕ ОТЗЫВЫ";
$MESS["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут оставлять комментарии.";
$MESS["CLICK_TO_RATE"] = "Нажмите, чтобы оценить ресторан";
$MESS["ADD_PHOTO"] = "Добавить Фото";
$MESS["ADD_VIDEO"] = "Добавить Видео";
$MESS["ADD_PROS_CONS"] = "Добавить достоинства и недостатки заведения";
$MESS["REVIEW_TEXT"] = "Текст отзыва";
$MESS["PROS"] = "ДОСТОИНСТВА";
$MESS["CONS"] = "НЕДОСТАТКИ";
$MESS["MORE"] = "Еще";
$MESS["YOUTUBE_LINK"] = "или ссылка с Youtube";
$MESS["MOVE_SLIDER"] = "Сдвиньте слайдер вправо.";
$MESS["MOVE_SLIDER_DONE"] = "Готово. Теперь можно отправлять";
$MESS["REVIEWS_PUBLISH"] = "Опубликовать";
$MESS["ER_RATING"] = "Не выбран рейтинг";
$MESS["ER_TEXT"] = "Заполните поле комментарий";
$MESS["R_TERMS"] = "<p>Дорогие друзья! Помните, что администрация сайта будет удалять:</p>
        <p>1. Комментарии с грубой и ненормативной лексикой<br />
        2. Прямые или косвенные оскорбления героя поста или читателей<br />
        3. Короткие оценочные комментарии («ужасно», «класс«, «отстой»)<Br />
        4. Комментарии, разжигающие национальную и социальную рознь<br />
        <p><a href='/auth/user_license_agreement.php' target='_blank'>Полная версия правил Restoran.ru</a></p>";
$MESS["SAVE_COMMENT"] = "Спасибо, Ваш комментарий сохранен.";
$MESS["LEAVE_COMMENT"] = "Написать комментарий";
$MESS["YOUR_COMMENT"] = "Ваш комментарий";
$MESS["R_COMMENT_ADD"] = "Комментировать";
$MESS["add_photo_before"] = "добавить:";
$MESS["photo-link-title"] = "фото";
$MESS["warning-file-type"] = "Неверный тип файла. Поддерживается загрузка следующих форматов: jpg, gif, png";
$MESS["comment_add_new_new_photo"] = "фото";
$MESS["comment_add_new_new_enter_comment"] = "Введите комментарий!";
?>