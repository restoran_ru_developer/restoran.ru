<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["NO_RATING"] = "Не выбран рейтинг";
$MESS ["NO_COMMENT"] = "Заполните поле комментарий";
$MESS ["NO_REST"] = "Не выбран ресторан";


$MESS ["NO_COMMENTS"] = "Здесь еще никто не писал, Вы можете быть первым";
$MESS ["ALL_REVIEWS"] = "ВСЕ ОТЗЫВЫ";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут писать отзывы.";
$MESS ["YOUR_REVIEW"] = "Ваше мнение";

$MESS ["CHOOSE_REST"] = "Выберите ресторан";
$MESS ["ONLY_REST"] = "Отзыв можно оставить только для присутствующих на сайте ресторанов";
//$MESS [] = "Feedback can be left only to those present at the site restaurants"
$MESS ["NO_COMMENTS"] = "Здесь еще никто не писал, Вы можете быть первым";
$MESS ["ALL_REVIEWS"] = "ВСЕ ОТЗЫВЫ";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут писать отзывы.";
$MESS ["CLICK_TO_RATE"] = "Нажмите, чтобы оценить ресторан";
$MESS ["ADD_PHOTO"] = "Добавить Фото";
$MESS ["ADD_VIDEO"] = "Добавить Видео";
$MESS ["ADD_PROS_CONS"] = "Добавить достоинства и недостатки заведения";
$MESS ["REVIEW_TEXT"] = "Расскажите нам о своих впечатлениях";
$MESS ["PROS"] = "Достоинства";
$MESS ["CONS"] = "Недостатки";
$MESS ["MORE"] = "Еще";
$MESS ["YOUTUBE_LINK"] = "или ссылка с Youtube";
$MESS ["MOVE_SLIDER"] = "Сдвиньте слайдер вправо";
$MESS ["MOVE_SLIDER_DONE"] = "Готово. Теперь можно отправлять";
$MESS ["REVIEWS_PUBLISH"] = "Опубликовать";
$MESS ["ER_RATING"] = "Не выбран рейтинг";
$MESS ["ER_TEXT"] = "Заполните поле комментарий";
$MESS ["R_TERMS"] = "Полная версия правил Restoran.ru";
$MESS ["email_placeholder"] = "Введите ваш Email адрес";
?>