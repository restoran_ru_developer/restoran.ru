<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["NO_RATING"] = "Please, select rating";
$MESS ["NO_COMMENT"] = "Fill comment";
$MESS ["NO_REST"] = "Choose restaurant";

$MESS ["NO_COMMENTS"] = "No one's writing, you can be the first";
$MESS ["ALL_REVIEWS"] = "ALL REVIEWS";
$MESS ["ONLY_REST"] = "Feedback can be left only to those present at the site restaurants";
$MESS ["CHOOSE_REST"] = "Choose restaurant";
$MESS ["YOUR_REVIEW"] = "Your review";
$MESS ["ONLY_AUTHORIZED"] = "Only authorized users can write reviews";
$MESS ["CLICK_TO_RATE"] = "Click to rate restaurant";
$MESS ["ADD_PHOTO"] = "Add Photo";
$MESS ["ADD_VIDEO"] = "Add Video";
$MESS ["ADD_PROS_CONS"] = "Add pros and cons of place";
$MESS ["REVIEW_TEXT"] = "Review";
$MESS ["PROS"] = "PROS";
$MESS ["CONS"] = "CONS";
$MESS ["MORE"] = "MORE";
$MESS ["YOUTUBE_LINK"] = "Youtube Link";
$MESS ["MOVE_SLIDER"] = "Move the slider to the right";
$MESS ["MOVE_SLIDER_DONE"] = "Done. You can send now";
$MESS ["REVIEWS_PUBLISH"] = "Publish";
$MESS ["ER_RATING"] = "Not selected rating";
$MESS ["ER_TEXT"] = "Comment is empty";
$MESS ["R_TERMS"] = "Full terms of Restoran.ru";
//<p>Dear friends! Remember that the editor will remove:</p>
//<p>1. Comments with profanity and rough<br />
//        2. Direct or indirect insults hero post or readers<br />
//        3. Short evaluation comments («awful», «class», «sucks»)<Br />
//        4. Comments that incite ethnic and social strife<br />

$MESS["add_REVIEW_ADD_IN_DETAIL"] = "add";
$MESS["dignity_REVIEW_ADD_IN_DETAIL"] = "dignities";
$MESS["limitations_REVIEW_ADD_IN_DETAIL"] = "limitations";
$MESS["photo_REVIEW_ADD_IN_DETAIL"] = "photo";
$MESS["video_REVIEW_ADD_IN_DETAIL"] = "video";
$MESS["not_type_REVIEW_ADD_IN_DETAIL"] = "Invalid file type. You can upload the following formats: jpg, gif, png";
$MESS["not_video_type_REVIEW_ADD_IN_DETAIL"] = "Invalid file type. You can upload the following formats: m4v, avi, mov, flv, 3gp";
$MESS["estimate_REVIEW_ADD_IN_DETAIL"] = "Before sending the revocation vote restaurant with the help of stars.";
$MESS ["email_placeholder"] = "Enter Email address";
?>