<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER["DOCUMENT_ROOT"].'/tpl/ajax/qqFileUploader.php';

$uploader = new qqFileUploader();

// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
if ($_REQUEST["f"]=="images")
    $uploader->allowedExtensions = array("jpeg","jpg","bmp","gif","png");
elseif($_REQUEST["f"]=="video")
    $uploader->allowedExtensions = array("m4v","mov","flv","3gp","avi");
else
    $uploader->allowedExtensions = array("jpeg","jpg","bmp","gif","png");


// Specify max file size in bytes.
if ($_REQUEST["f"]=="images")
    $uploader->sizeLimit = 10 * 1024 * 1024;
elseif($_REQUEST["f"]=="video")
    $uploader->sizeLimit = 100 * 1024 * 1024;
else
    $uploader->sizeLimit = 10 * 1024 * 1024;
// Specify the input name set in the javascript.
$uploader->inputName = 'qqfile';

// If you want to use resume feature for uploader, specify the folder to save parts.
//$uploader->chunksFolder = 'chunks';

// To save the upload with a specified name, set the second parameter.
// $result = $uploader->handleUpload('uploads/', md5(mt_rand()).'_'.$uploader->getName());

// To return a name used for uploaded file you can use the following line.

global $USER;
if ($USER->IsAuthorized())
{
    $DIR=$_SERVER["DOCUMENT_ROOT"].'/upload/user_files/'.$USER->GetID().'/';
    if (!is_dir($DIR))
    {
        mkdir($DIR);
    }    
}
else
    $DIR=$_SERVER["DOCUMENT_ROOT"].'/upload/user_files/';

// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
$result = $uploader->handleUpload($DIR);
$result['uploadName'] = $uploader->getUploadName();
$result['path'] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$DIR).$uploader->getUploadName();

if ($_REQUEST["f"]!="video")
{    
    $new_name = translateFilename(trim($result['uploadName']));
    $temp = explode(".",$new_name);        
    $rasch = $temp[count($temp)-1];
    unset($temp[count($temp)-1]);
    $nn = implode("",$temp);
    $new_file_name = $nn.substr(md5(mt_rand()), 0, 3).".".$rasch;
    $s = $DIR.$new_file_name;
    CFile::ResizeImageFile($DIR.'/'.$result['uploadName'], $s, Array("width"=>728,"height"=>486),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,array(),60,false);
    $result["resized"] = str_replace($_SERVER["DOCUMENT_ROOT"],"",$s);
    unlink($DIR.trim($result['uploadName']));
}
header("Content-Type: text/plain");
echo json_encode($result);
?>