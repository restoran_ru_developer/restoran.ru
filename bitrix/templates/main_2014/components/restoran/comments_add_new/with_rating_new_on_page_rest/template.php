<script type="text/javascript" src="/tpl/js/fu/js/header.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/util.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/button.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.base.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.form.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/handler.xhr.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/uploader.basic.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/dnd.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/uploader.js"></script>
<script type="text/javascript" src="/tpl/js/fu/js/jquery-plugin.js"></script>
<link href="<?=$templateFolder?>/style.css?12" rel="stylesheet" type="text/css"/>
<script>
    $(function(){
        $(".choose-ratio span.ic").click(
            function(){
                $("#review").focus();                               
                var count = $(this).index()+1;
                var i = 1;
                $("#input_ratio").attr("value",count);
                $(this).parent().find("span.ic").each(function(){                
                    if (i<=count)
                        $(this).attr("class","ic icon-star");
                    else
                        $(this).attr("class","ic icon-star-empty");
                    i++;
                })
            }
        );
    });
</script>
<?/*if (!$USER->IsAuthorized()):?>
    <?$APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "no_auth",
            Array(
                    "REGISTER_URL" => "",
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "",
                    "SHOW_ERRORS" => "N"
            ),
    false
    );?> 
    <br /><br />
<?endif;*/
?>
<?//TODO убрать ссылку после запуска сетевых?>
<?if(!$_REQUEST['NEW_DESIGN']):?><a class="terms" href='/auth/user_license_agreement.php' target='_blank'><?=GetMessage("R_TERMS")?></a><?endif?>
<form class="grey_form" name="attach" action="/bitrix/components/restoran/comments_add_new/ajax.php" method="post" id="comment_form" enctype="multipart/form-data" <?if($_REQUEST['NEW_DESIGN']&&!$_REQUEST['REST_NETWORK']):?>style="display:none;"<?endif?>>
    <?=bitrix_sessid_post()?>
    <div class="form-group">
        <div class="title"><?=GetMessage("YOUR_REVIEW")?></div>
        <div class="choose-ratio">
            <span class="ic icon-star-empty"></span>
            <span class="ic icon-star-empty"></span>
            <span class="ic icon-star-empty"></span>
            <span class="ic icon-star-empty"></span>
            <span class="ic icon-star-empty"></span>
            <span class="text"> —&nbsp;<i><?=GetMessage("CLICK_TO_RATE")?></i></span>
            
        </div>
    </div>                            
        <div class="form-group input-block">
            <textarea name="review" id="review" placeholder="<?=GetMessage("REVIEW_TEXT")?>"></textarea>                                    
            <input type="hidden" value="" id="input_ratio" name="ratio" />
<!--            <div class="left"><a href="javascript:void(0)" class="js another" onclick="$('#plus_minus').toggle(300)"><?=GetMessage("ADD_PROS_CONS")?></a></div>-->            
        </div>
        <div class="form-group input-block" id="plus_block">
            <textarea name="plus" placeholder="<?=GetMessage("PROS")?>"></textarea>                                    
        </div>
        <div class="form-group input-block" id="minus_block">
            <textarea name="minus" placeholder="<?=GetMessage("CONS")?>"></textarea>                                                
        </div>        
        <?if (!$USER->IsAuthorized()):?>
            <div class="form-group input-block email">
                <input value="" name="email" placeholder="<?=GetMessage('email_placeholder')?>" req="req" style="width: 100%"/>
            </div>                    
            <div class="form-group captcha">
                <div class="QapTcha"></div>
                <link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
                <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
                <script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
                <script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
                <script type="text/javascript">
                        $(document).ready(function(){
                                $('.QapTcha').QapTcha({
                                        txtLock : '<?=GetMessage("MOVE_SLIDER")?>',
                                        txtUnlock : '<?=GetMessage("MOVE_SLIDER_DONE")?>',
                                        disabledSubmit : true,
                                        autoRevert : true,
                                        PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
                                        autoSubmit : false});
                        });
                </script>
            </div>
            <div class="clearfix"></div>
        <?endif?>        
        <div class="img-container" id="img-container">
                <div class="clear"></div>
        </div>
        <div class="attach">
            <b><?=GetMessage('add_REVIEW_ADD_IN_DETAIL')?>:</b>
            <a href="javascript:void(0)" onclick="$('#plus_block').toggle(300)"><?=GetMessage('dignity_REVIEW_ADD_IN_DETAIL')?></a>
            <a href="javascript:void(0)" onclick="$('#minus_block').toggle(300)"><?=GetMessage('limitations_REVIEW_ADD_IN_DETAIL')?></a>
            <a id="attach_photo"><?=GetMessage('photo_REVIEW_ADD_IN_DETAIL')?></a>
            <a id="attach_video"><?=GetMessage('video_REVIEW_ADD_IN_DETAIL')?></a>
        </div>
        <input type="submit" id="add_commm" class="btn btn-info btn-nb pull-right" value="<?=GetMessage("REVIEWS_PUBLISH")?>">               
        <input type="hidden" name="IBLOCK_TYPE" value="<?=$arParams["IBLOCK_TYPE"]?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
        <input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
        <input type="hidden" name="IS_SECTION" value="<?=$arParams["IS_SECTION"]?>" />               
        <div class="clearfix"></div>        
</form>
<script>
$(document).ready(function(){                      
        var files = new Array();
        var errorHandler = function(event, id, fileName, reason) {
          qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
        $('#attach_photo').fineUploader({
            text: {
                uploadButton: "<?=GetMessage('photo_REVIEW_ADD_IN_DETAIL')?>",
                cancelButton: "",
                waitingForResponse: "",
                dropProcessing: ""
            },
            dragAndDrop: {
                disableDefaultDropzone:true
            },
            multiple: true,
            disableCancelForFormUploads: true,            
            request: {
                endpoint: "<?=$templateFolder?>/upload.php",
                params: {"generateError": true,"f":"images"}
            },
            validation:{
                allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
               typeError: "<?=GetMessage('not_type_REVIEW_ADD_IN_DETAIL')?>"
            }
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push(response.resized);            
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
                $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));                        
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        }); 
        $('#attach_video').fineUploader({
            text: {
                uploadButton: "<?=GetMessage('video_REVIEW_ADD_IN_DETAIL')?>",
                cancelButton: "",
                waitingForResponse: "",
                 dropProcessing: ""
            },
            multiple: true,
            disableCancelForFormUploads : true,
            request: {
                endpoint: "<?=$templateFolder?>/upload.php",
                params: {"generateError": true, "f":"video"}
            },
             validation:{
                allowedExtensions : ["m4v","avi","mov","flv","3gp"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
               typeError: "<?=GetMessage('not_video_type_REVIEW_ADD_IN_DETAIL')?>"
            }                
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push("video");                        
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="<?=$templateFolder?>/images/video_file.png" height="70">').appendTo($('#img'+files.length));
                $('<div class="name">'+response.uploadName+'</div>').appendTo($('#img'+files.length));
                $('<input value="'+response.path+'" type="hidden" name="video[]" />').appendTo($('#img'+files.length));                            
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        });
         $("#wr_cm").click(function(){
            $(this).hide();
            $("#write_comment").show("500");                
        });                 
        $(".img-container").on("click",".remove-link",function(){
            var file = $(this).next().attr("src");
            $(this).parents(".image-block").remove();                
            return false;
        });
        
        $("#comment_form").keypress(function(e){
            e = e || window.event;    
            //for chrome & safari
            if (e.ctrlKey) {
                if(e.keyCode == 10){
                    $("#comment_form").submit();
                    return false;
                }
            };
            //for firefox
            if (e.keyCode == 13 && e.ctrlKey) {
                $("#comment_form").submit();
                return false;
            };
        });
        $("#comment_form").submit(function(){            
            if (!$(this).find("#review").val())
                {
                    alert("Введите комментарий!");
                    return false;
                }
            if (!$(this).find("#input_ratio").val())
                {
                    alert("<?=GetMessage('estimate_REVIEW_ADD_IN_DETAIL')?>");
                    return false;
                }
            $("#add_commm").attr("disabled",true);
            var params = $(this).serialize();
            $.ajax({
                    type: "POST",
                    url: $(this).attr("action"),
                    data: params,
                    success: function(data) {
                        $("#add_commm").attr("disabled",false);
                        if(data)
                        {
//                            console.log(data);
                            data = eval('('+data+')');                        
                            alert(data.MESSAGE);                            
                            if (data.ERROR=="1")
                            {
                                $("#add_commm").attr("disabled",false);
                            }
                            if (data.STATUS=="1")
                            {
                                location.reload();
                                //setTimeout("location.reload()","3500");
                            }
                        }
                    }
                });
        return false; 
        });
    });
    
</script>