<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/bxslider-4-master/dist/jquery.bxslider.min.js"></script>
<script>
    $(function(){
        <?if($arParams['HAS_SIMILAR_ARR']!='Y'&&!count($arResult['ITEMS'])):?>
        $('.main_2015_summer_content').hide();
        <?endif?>
        <?if(count($arResult['ITEMS'])>0):?>
        var slider = $('.near-rest-slider').bxSlider({
            pager:false,
            prevSelector:'.prev-slide',
            nextSelector:'.next-slide',
            slideWidth:200,
            moveSlides:1,
            minSlides:4,
            maxSlides:4,
            slideMargin: 30,
            hideControlOnEnd: true,
            infiniteLoop: false
        });
        <?endif?>
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var slider = $('.near-rest-slider.near-slider').bxSlider({
                pager:false,
                prevSelector:'.near-prev-slide',
                nextSelector:'.near-next-slide',
                slideWidth:200,
                moveSlides:1,
                minSlides:4,
                maxSlides:4,
                slideMargin: 30,
                hideControlOnEnd: true,
                infiniteLoop: false
            });
            $('.near-rest-slider.group-slider:not(.no-four-slide)').bxSlider({
                pager:false,
                prevSelector:'.group-prev-slide',
                nextSelector:'.group-next-slide',
                slideWidth:200,
                moveSlides:1,
                minSlides:4,
                maxSlides:4,
                slideMargin: 30,
                hideControlOnEnd: true,
                infiniteLoop: false
            });
        })
    })
</script>
<div class="near-rest-slider-wrap">
    <?
    //FirePHP::getInstance()->info(count($arResult["ITEMS"]),'count');
    if(count($arResult["ITEMS"])>4):?>
        <div class="prev-slide icon-arrow-left2 near-prev-slide"></div>
        <div class="next-slide icon-arrow-right2 near-next-slide"></div>
    <?endif?>
    <div class="near-rest-slider near-slider">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="pull-left">
            <? if ($arItem["PREVIEW_PICTURE"]): ?>
                <div class="pic">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"] ?>"/></a>
                </div>
            <? endif; ?>
            <div class="text">
                <h2><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></h2>
                <? if ($arParams["REST_PROPS"]): ?>
                    <div class="ratio">
                        <? for ($z = 1; $z <= 5; $z++): ?>
                            <span
                                class="<?= (round($arItem["PROPERTIES"]["RATIO"]['VALUE']) >= $z) ? "icon-star" : "icon-star-empty" ?>"></span>
                        <? endfor ?>
                    </div>
                    <div class="props">
                        <div class="prop">
                            <div class="name"><?= GetMessage('NEAR_TITLE_FOR_KITCHEN') ?>:</div>
                            <div class="value"><?= implode(", ", $arItem["KITCHEN"]) ?></div>
                        </div>
                        <div class="prop">
                            <div class="name"><?= GetMessage('NEAR_TITLE_FOR_BILL') ?>:</div>
                            <div class="value"><?= $arItem["BILL"] ?></div>
                        </div>
                        <div class="prop">
                            <div class="name"><?= GetMessage('NEAR_TITLE_FOR_ADDRESS') ?>:</div>
                            <?
                            $clear_city_arr = array(
                                'msk' => "/( +|)Москва[, ]+/i",
                                'spb' => "/( +|)Санкт-Петербург[, ]+/i",
                                'rga' => "/( +|)Рига[, ]+/i",
                                'urm' => "/( +|)Юрмала[, ]+/i",
                            );
                            ?>
                            <div class="value"><?= is_array($arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]) ? preg_replace($clear_city_arr[CITY_ID], '', $arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][0]) : preg_replace($clear_city_arr[CITY_ID], '', $arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]) ?></div>
                        </div>
                    </div>
                <? else: ?>
                    <p><?= $arItem["PREVIEW_TEXT"] ?></p>
                <? endif; ?>
            </div>
        </div>
    <?endforeach;?>
    </div>
</div>