<?if($arParams['AJAX_REQUEST']!='Y'):?>

    <?
        ob_start();?>
            <div class="found-num-rest">
                <span class="how-many-found-wrap">Найдено <span><?=$arResult['REQUEST_OBJECT_COUNT']?></span> ресторанов</span>
                <?$some['arrFilter_pf'] = $_REQUEST['arrFilter_pf'];?>
                <a href="/<?=CITY_ID?>/map/near/?<?=http_build_query($some,'flags_')?>" class="on-map-square-filter-button">Показать на карте</a>
            </div>
        <?$out1 = ob_get_contents();
        ob_end_clean();
        $APPLICATION->AddViewContent("found_num_rest", $out1);
    ?>


<script>
    $(function(){
        if ($(".preview_seo_text").html())
        {
            $("#for_seo").html($(".preview_seo_text").html());
            $(".preview_seo_text").remove();
        }
    });
</script>
<div class="preview_seo_text">
            <?$APPLICATION->ShowViewContent("preview_seo_text");?>
</div>
<?
if($_REQUEST['PROPERTY']=='rest_group') {
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['rest_group'][0]);
    if($ar_res = $res->Fetch()) {
        $APPLICATION->SetPageProperty("title", "Ресторанная группа «".$ar_res["NAME"]."»");
        $APPLICATION->SetPageProperty("description", "Ресторанная группа ".$ar_res["NAME"]);
        $APPLICATION->SetPageProperty("keywords", "Ресторанная, группа, ".$ar_res["NAME"]);
        $APPLICATION->SetPageProperty("H1", "Ресторанная группа «".$ar_res["NAME"]."»");
    }
}

if (CITY_ID == "rga"):
    $phone_str = "+371 661 031 06";
elseif (CITY_ID == "spb"):
    $phone_str = "+7 (812) 740-18-20";
else:
    $phone_str = "+7 (495) 988-26-56";
endif;

$page_num = $_REQUEST["PAGEN_1"]?$_REQUEST["PAGEN_1"]:$_REQUEST['page'];
$page_str = '';
if($page_num>1){
    $page_str = " Страница: ".$page_num;
}

if(CITY_ID=='spb'){
    $city_name_first = 'Санкт-Петербурга';
    $city_name_second = 'Санкт-Петербурге';
    $city_name_third = 'санкт-петербург';
}
elseif(CITY_ID=='msk'){
    $city_name_first = 'Москвы';
    $city_name_second = 'Москве';
    $city_name_third = 'москва';
}
elseif(CITY_ID=='rga'){
    $city_name_first = 'Риги';
    $city_name_second = 'Риге';
    $city_name_third = 'рига';
}

//FirePHP::getInstance()->info($_REQUEST);
if($_REQUEST['PROPERTY']=='STREET'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['STREET'][0]);
    if($ar_res = $res->Fetch()){
        if(CITY_ID=='spb'){
            //title: Рестораны на Загребском бульваре - Рестораны Санкт-Петербурга.
            //keywords: рестораны, загребский бульвар, санкт-петербург
            //description: Каталог ресторанов на Загребском бульваре, Санкт-Петербург.
            //H1: Рестораны на Загребском бульваре

            if($ar_res['PREVIEW_TEXT']){
                if($_REQUEST['CATALOG_ID']=='restaurants'){
                    $APPLICATION->SetPageProperty("title",  "Рестораны на ".$ar_res['PREVIEW_TEXT']." - Рестораны Санкт-Петербурга.");
                    $APPLICATION->SetPageProperty("description",  "Каталог ресторанов на ".$ar_res['PREVIEW_TEXT'].", Санкт-Петербург.");
                    $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", санкт-петербург");
                    $APPLICATION->SetPageProperty("H1",  "Рестораны на ".$ar_res['PREVIEW_TEXT']);
                    $APPLICATION->SetPageProperty("catalog_prop_description", "У всех нас разные вкусы и предпочтения, а также требования к интерьеру и сервису. Вот уже не первый десяток лет рестораны на " . $ar_res['PREVIEW_TEXT'] . " в Санкт-Петербурге работают по принципу вариативности — отличные по стилю, формату, концепции и даже режиму работы
                они всегда радуют своих гостей возможностью не только выбрать максимально комфортное заведение, но и попробовать что-то кардинально новое. Рестораны на " . $ar_res['PREVIEW_TEXT'] . " в Санкт-Петербурге представлены в огромном количестве, вам остается подобрать лишь тот, где ваш отдых наверняка пройдет отлично!");

                }
                elseif($_REQUEST['CATALOG_ID']=='banket'){
                    $APPLICATION->SetPageProperty("title",  "Банкетные залы на ".$ar_res['PREVIEW_TEXT']." - Банкетные залы Санкт-Петербурга.");
                    $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов на ".$ar_res['PREVIEW_TEXT'].", Санкт-Петербург.");
                    $APPLICATION->SetPageProperty("keywords",  "банкетные залы, ".strtolower($ar_res['NAME']).", санкт-петербург");
                    $APPLICATION->SetPageProperty("H1",  "Банкетные залы на ".$ar_res['PREVIEW_TEXT']);

                    $APPLICATION->SetPageProperty("catalog_prop_description", "У всех нас разные вкусы и предпочтения, а также требования к интерьеру и сервису. Вот уже не первый десяток лет банкетные залы на " . $ar_res['PREVIEW_TEXT'] . " в Санкт-Петербурге работают по принципу вариативности — отличные по стилю, формату, концепции и даже режиму работы
                они всегда радуют своих гостей возможностью не только выбрать максимально комфортное заведение, но и попробовать что-то кардинально новое. Банкетные залы на " . $ar_res['PREVIEW_TEXT'] . " в Санкт-Петербурге представлены в огромном количестве, вам остается подобрать лишь тот, где ваш отдых наверняка пройдет отлично!");
                }
            }
            else {
                if($_REQUEST['CATALOG_ID']=='restaurants'){
                    $APPLICATION->SetPageProperty("title",  "Рестораны, ".$ar_res['NAME']." - Рестораны Санкт-Петербурга.");
                    $APPLICATION->SetPageProperty("description",  "Каталог ресторанов по адресу ".$ar_res['NAME'].", Санкт-Петербург.");
                    $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", санкт-петербург");
                    $APPLICATION->SetPageProperty("H1",  "Рестораны, ".$ar_res['NAME']."");
                    $APPLICATION->SetPageProperty("catalog_prop_description", "У всех нас разные вкусы и предпочтения, а также требования к интерьеру и сервису. Вот уже не первый десяток лет рестораны на улице " . $ar_res['NAME'] . " в Санкт-Петербурге работают по принципу вариативности — отличные по стилю, формату, концепции и даже режиму работы
                        они всегда радуют своих гостей возможностью не только выбрать максимально комфортное заведение, но и попробовать что-то кардинально новое. Рестораны на улице " . $ar_res['NAME'] . " в Санкт-Петербурге представлены в огромном количестве, вам остается подобрать лишь тот, где ваш отдых наверняка пройдет отлично!");
                }
                elseif($_REQUEST['CATALOG_ID']=='banket'){
                    $APPLICATION->SetPageProperty("title",  "Банкетные залы, ".$ar_res['NAME']." - Банкетные залы Санкт-Петербурга.");
                    $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов по адресу ".$ar_res['NAME'].", Санкт-Петербург.");
                    $APPLICATION->SetPageProperty("keywords",  "банкетные залы, ".strtolower($ar_res['NAME']).", санкт-петербург");
                    $APPLICATION->SetPageProperty("H1",  "Банкетные залы, ".$ar_res['NAME']."");

                    $APPLICATION->SetPageProperty("catalog_prop_description", "У всех нас разные вкусы и предпочтения, а также требования к интерьеру и сервису. Вот уже не первый десяток лет банкетные залы на улице " . $ar_res['NAME'] . " в Санкт-Петербурге работают по принципу вариативности — отличные по стилю, формату, концепции и даже режиму работы
                они всегда радуют своих гостей возможностью не только выбрать максимально комфортное заведение, но и попробовать что-то кардинально новое. Банкетные залы на улице " . $ar_res['NAME'] . " в Санкт-Петербурге представлены в огромном количестве, вам остается подобрать лишь тот, где ваш отдых наверняка пройдет отлично!");
                }

            }
        }
        elseif(CITY_ID=='msk'){
            if($ar_res['PREVIEW_TEXT']){
                if($_REQUEST['CATALOG_ID']=='restaurants'){
                    $APPLICATION->SetPageProperty("title",  "Рестораны на ".$ar_res['PREVIEW_TEXT']." - Рестораны Москвы.");
                    $APPLICATION->SetPageProperty("description",  "Каталог ресторанов на ".$ar_res['PREVIEW_TEXT'].", Москва.");
                    $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", москва");
                    $APPLICATION->SetPageProperty("H1",  "Рестораны на ".$ar_res['PREVIEW_TEXT']."");

                    $APPLICATION->SetPageProperty("catalog_prop_description",  "Рестораны  на ".$ar_res['PREVIEW_TEXT']." в Москве дают возможность всем желающим выбрать из огромного многообразия мест то, которое обязательно придется по вкусу. Среди прочих, здесь можно отыскать и китайские, и итальянские, и японские, и кавказские, и русские заведения.
                    Более того, рестораны на ".$ar_res['PREVIEW_TEXT']." в Москве никогда не останавливаются на достигнутом, имея множество возможностей для обновления и саморазвития. Детские зоны, кальянные комнаты, караоке, мастер-классы и кулинарные фестивали здесь давно уже не в новинку. Выбор за вами!");
                }
                elseif($_REQUEST['CATALOG_ID']=='banket'){
                    $APPLICATION->SetPageProperty("title",  "Банкетные залы на ".$ar_res['PREVIEW_TEXT']." - Банкетные залы Москвы.");
                    $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов на ".$ar_res['PREVIEW_TEXT'].", Москва.");
                    $APPLICATION->SetPageProperty("keywords",  "банкетные залы, ".strtolower($ar_res['NAME']).", москва");
                    $APPLICATION->SetPageProperty("H1",  "Банкетные залы на ".$ar_res['PREVIEW_TEXT']."");

                    $APPLICATION->SetPageProperty("catalog_prop_description", "Банкетные залы  на " . $ar_res['PREVIEW_TEXT'] . " в Москве дают возможность всем желающим выбрать из огромного многообразия мест то, которое обязательно придется по вкусу. Среди прочих, здесь можно отыскать и китайские, и итальянские, и японские, и кавказские, и русские заведения.
                    Более того, банкетные залы на " . $ar_res['PREVIEW_TEXT'] . " в Москве никогда не останавливаются на достигнутом, имея множество возможностей для обновления и саморазвития. Детские зоны, кальянные комнаты, караоке, мастер-классы и кулинарные фестивали здесь давно уже не в новинку. Выбор за вами!");
                }
            }
            else {
                if($_REQUEST['CATALOG_ID']=='restaurants'){
                    $APPLICATION->SetPageProperty("title",  "Рестораны, ".$ar_res['NAME']." - Рестораны Москвы.");
                    $APPLICATION->SetPageProperty("description",  "Каталог ресторанов по адресу ".$ar_res['NAME'].", Москва.");
                    $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", москва");
                    $APPLICATION->SetPageProperty("H1",  "Рестораны, ".$ar_res['NAME']."");

                    $APPLICATION->SetPageProperty("catalog_prop_description",  "Рестораны  на  улице ".$ar_res['NAME']." в Москве дают возможность всем желающим выбрать из огромного многообразия мест то, которое обязательно придется по вкусу. Среди прочих, здесь можно отыскать и китайские, и итальянские, и японские, и кавказские, и русские заведения.
                    Более того, рестораны на улице ".$ar_res['NAME']." в Москве никогда не останавливаются на достигнутом, имея множество возможностей для обновления и саморазвития. Детские зоны, кальянные комнаты, караоке, мастер-классы и кулинарные фестивали здесь давно уже не в новинку. Выбор за вами!");
                }
                elseif($_REQUEST['CATALOG_ID']=='banket'){
                    $APPLICATION->SetPageProperty("title",  "Банкетные залы, ".$ar_res['NAME']." - Банкетные залы Москвы.");
                    $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов по адресу ".$ar_res['NAME'].", Москва.");
                    $APPLICATION->SetPageProperty("keywords",  "банкетные залы, ".strtolower($ar_res['NAME']).", москва");
                    $APPLICATION->SetPageProperty("H1",  "Банкетные залы, ".$ar_res['NAME']."");

                    $APPLICATION->SetPageProperty("catalog_prop_description", "Банкетные залы  на  улице " . $ar_res['NAME'] . " в Москве дают возможность всем желающим выбрать из огромного многообразия мест то, которое обязательно придется по вкусу. Среди прочих, здесь можно отыскать и китайские, и итальянские, и японские, и кавказские, и русские заведения.
                    Более того, банкетные залы на улице " . $ar_res['NAME'] . " в Москве никогда не останавливаются на достигнутом, имея множество возможностей для обновления и саморазвития. Детские зоны, кальянные комнаты, караоке, мастер-классы и кулинарные фестивали здесь давно уже не в новинку. Выбор за вами!");
                }
            }
        }
    }
}

if($_REQUEST['PROPERTY']=='subway'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){
//    FirePHP::getInstance()->info($_REQUEST['PROPERTY_VALUE']);
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
    if($ar_res = $res->Fetch()){
        //Title: Рестораны у метро Садовая - Рестораны Санкт-Петербурга.
        //h1: Рестораны у метро Садовая
        //keywords: рестораны, метро, садовая, санкт-петербург
        //desc: Каталог ресторанов по возле метро Садовая
        //Restoran.ru представляет полный перечень ресторанов у метро ХХХ. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан на любой вкус! А бесплатно забронировать столик в ресторанах у метро ХХХ можно по телефону: YYY

        if($_REQUEST['CATALOG_ID']=='restaurants'){
            $APPLICATION->SetPageProperty("title",  "Рестораны у метро ".$ar_res['NAME']." - Рестораны $city_name_first.".$page_str);
            $APPLICATION->SetPageProperty("description",  "Каталог ресторанов возле метро ".$ar_res['NAME']." в $city_name_second.".$page_str);
            $APPLICATION->SetPageProperty("keywords",  "рестораны, метро, ".strtolower($ar_res['NAME']).", $city_name_third");
            $APPLICATION->SetPageProperty("H1",  "Рестораны у метро ".$ar_res['NAME']);
            $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов у метро ".$ar_res['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан на любой вкус! А бесплатно забронировать столик в ресторанах у метро ".$ar_res['NAME']." можно по телефону: ".$phone_str);
        }
        elseif($_REQUEST['CATALOG_ID']=='banket'){
            $APPLICATION->SetPageProperty("title",  "Банкетные залы у метро ".$ar_res['NAME']." - Банкетные залы Санкт-Петербурга.".$page_str);
            $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов возле метро ".$ar_res['NAME']." в Санкт-Петербурге.".$page_str);
            $APPLICATION->SetPageProperty("keywords",  "банкетные залы, метро, ".strtolower($ar_res['NAME']).", санкт-петербург");
            $APPLICATION->SetPageProperty("H1",  "Банкетные залы у метро ".$ar_res['NAME']);
            $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень банкетных залов у метро ".$ar_res['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать банкетный зал на любой вкус! А бесплатно забронировать столик в банкетных залах у метро ".$ar_res['NAME']." можно по телефону: ".$phone_str);
        }
    }
}
if($_REQUEST['PROPERTY']=='area'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='rga')){//район
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['area'][0]);
    if($ar_res = $res->Fetch()) {
        //Title: Банкетные залы в Кировском районе - Банкетные залы Санкт-Петербурга.
        //h1: Банкетные залы в Кировском районе
        //keywords: Банкетные залы, кировский, район, санкт-петербург
        //desc: Каталог банкетных залов в Кировском районе в Санкт-Петербурге.

        if($ar_res['PREVIEW_TEXT']){
            $region_str = $ar_res['PREVIEW_TEXT']." районе";
        }
        else {
            $region_str = "районе ".$ar_res['NAME'];
        }

        if ($_REQUEST['CATALOG_ID'] == 'restaurants') {
            $APPLICATION->SetPageProperty("title", "Рестораны в $region_str - Рестораны $city_name_first." . $page_str);
            $APPLICATION->SetPageProperty("description", "Каталог ресторанов в $region_str в $city_name_second." . $page_str);
            $APPLICATION->SetPageProperty("keywords", "рестораны, район, " . strtolower($ar_res['NAME']) . ", $city_name_third");
            $APPLICATION->SetPageProperty("H1", "Рестораны в $region_str");
            $APPLICATION->SetPageProperty("catalog_prop_description", "Представляем вашему вниманию список ресторанов в $region_str. На сайте размещена контактная информация, отзывы посетителей, интерьерные фотографии, меню, новости. Перейдя на страницу заведения, можно не только предварительно познакомиться с ресторанами в $region_str, но и бесплатно забронировать столик по телефону " . $phone_str . ".");
        } elseif ($_REQUEST['CATALOG_ID'] == 'banket') {
            $APPLICATION->SetPageProperty("title", "Банкетные залы в $region_str - Банкетные залы $city_name_first." . $page_str);
            $APPLICATION->SetPageProperty("description", "Каталог банкетных залов в $region_str в $city_name_second." . $page_str);
            $APPLICATION->SetPageProperty("keywords", "банкетные залы, район, " . strtolower($ar_res['NAME']) . ", $city_name_third");
            $APPLICATION->SetPageProperty("H1", "Банкетные залы в $region_str");
            $APPLICATION->SetPageProperty("catalog_prop_description", "Представляем вашему вниманию список банкетных залов в $region_str. На сайте размещена контактная информация, отзывы посетителей, интерьерные фотографии, меню, новости. Перейдя на страницу заведения, можно не только предварительно познакомиться с банкетными залами в $region_str, но и бесплатно забронировать столик по телефону " . $phone_str . ".");
        }
    }
}
if($_REQUEST['PROPERTY']=='out_city'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//пригород
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['out_city'][0]);
    if($ar_res = $res->Fetch()){
        //Restoran.ru помогает найти рестораны в XXX по разным запросам. Вы узнаете о заведении всё: адрес, время работы, меню, средний счет, количество мест, отзывы посетителей. Бесплатно забронировать столик в одном из приглянувшихся ресторанов в XXX вы можете по телефону YYY.

        $region_str = $ar_res['PREVIEW_TEXT']?$ar_res['PREVIEW_TEXT']:$ar_res['NAME'];

        if ($_REQUEST['CATALOG_ID'] == 'restaurants') {
            $APPLICATION->SetPageProperty("title", "Рестораны в $region_str - Рестораны $city_name_first." . $page_str);
            $APPLICATION->SetPageProperty("description", "Каталог ресторанов в $region_str в $city_name_second." . $page_str);
            $APPLICATION->SetPageProperty("keywords", "рестораны, пригород, " . strtolower($ar_res['NAME']) . ", $city_name_third");
            $APPLICATION->SetPageProperty("H1", "Рестораны в $region_str");
            $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru помогает найти рестораны в $region_str по разным запросам. Вы узнаете о заведении всё: адрес, время работы, меню, средний счет, количество мест, отзывы посетителей. Бесплатно забронировать столик в одном из приглянувшихся ресторанов в $region_str вы можете по телефону ".$phone_str.".");
        }
//        elseif ($_REQUEST['CATALOG_ID'] == 'banket') {
//            $APPLICATION->SetPageProperty("title", "Банкетные залы в $region_str - Банкетные залы $city_name_first." . $page_str);
//            $APPLICATION->SetPageProperty("description", "Каталог банкетных залов в $region_str в $city_name_second." . $page_str);
//            $APPLICATION->SetPageProperty("keywords", "банкетные залы, пригород, " . strtolower($ar_res['NAME']) . ", $city_name_third");
//            $APPLICATION->SetPageProperty("H1", "Банкетные залы в $region_str");
//            $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru помогает найти банкетные залы в $region_str по разным запросам. Вы узнаете о заведении всё: адрес, время работы, меню, средний счет, количество мест, отзывы посетителей. Бесплатно забронировать столик в одном из приглянувшихся банкетных залах в $region_str вы можете по телефону ".$phone_str.".");
//        }
    }
}

//  new
if($_REQUEST['PROPERTY']=='type'&&$_REQUEST['PROPERTY2']=='subway'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные тип-метро
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['type'][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
        if($ar_res2 = $res2->Fetch()){  //    subway

            $type_str = $ar_res['PREVIEW_TEXT']?explode('/',$ar_res['PREVIEW_TEXT']):$ar_res['NAME'];

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  (is_array($type_str)?$type_str[0]:$type_str)." у метро ".$ar_res2['NAME']." - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ".strtolower(is_array($type_str)?$type_str[1]:$type_str)." возле метро ".$ar_res2['NAME']." в $city_name_second.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  strtolower(is_array($type_str)?trim(str_replace('рестораны','',strtolower($type_str[0]))):$type_str).", рестораны, метро, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  (is_array($type_str)?$type_str[0]:$type_str)." у метро ".$ar_res2['NAME']);
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ".strtolower(is_array($type_str)?$type_str[1]:$type_str)." у метро ".$ar_res2['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ".strtolower(is_array($type_str)?$type_str[3]:$type_str)." на любой вкус! А бесплатно забронировать столик в ".strtolower(is_array($type_str)?$type_str[2]:$type_str)." у метро ".$ar_res2['NAME']." можно по телефону: ".$phone_str);
            }
        }

    }
}
if($_REQUEST['PROPERTY']=='type'&&$_REQUEST['PROPERTY2']=='area'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='rga')){//перекрестные тип-район
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['type'][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['area'][0]);
        if($ar_res2 = $res2->Fetch()){  //    area

            if($ar_res2['PREVIEW_TEXT']){
                $region_str = $ar_res2['PREVIEW_TEXT']." районе";
            }
            else {
                $region_str = "районе ".$ar_res2['NAME'];
            }

            $type_str = $ar_res['PREVIEW_TEXT']?explode('/',$ar_res['PREVIEW_TEXT']):$ar_res['NAME'];

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  (is_array($type_str)?$type_str[0]:$type_str)." в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ".strtolower(is_array($type_str)?$type_str[1]:$type_str)." в $region_str в $city_name_second.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  strtolower(is_array($type_str)?trim(str_replace('рестораны','',strtolower($type_str[0]))):$type_str).", рестораны, район, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  (is_array($type_str)?$type_str[0]:$type_str)." в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ".strtolower(is_array($type_str)?$type_str[1]:$type_str)." в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ".strtolower(is_array($type_str)?$type_str[3]:$type_str)." на любой вкус! А бесплатно забронировать столик в ".strtolower(is_array($type_str)?$type_str[2]:$type_str)." в $region_str можно по телефону: ".$phone_str);
            }
        }

    }
}
if($_REQUEST['PROPERTY']=='type'&&$_REQUEST['PROPERTY2']=='out_city'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные тип-пригород
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['type'][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['out_city'][0]);
        if($ar_res2 = $res2->Fetch()){  //    out_city

            $region_str = $ar_res2['PREVIEW_TEXT']?$ar_res2['PREVIEW_TEXT']:$ar_res2['NAME'];

            $type_str = $ar_res['PREVIEW_TEXT']?explode('/',$ar_res['PREVIEW_TEXT']):$ar_res['NAME'];

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  (is_array($type_str)?$type_str[0]:$type_str)." в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ".strtolower(is_array($type_str)?$type_str[1]:$type_str)." в $region_str в $city_name_second.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  strtolower(is_array($type_str)?trim(str_replace('рестораны','',strtolower($type_str[0]))):$type_str).", рестораны, пригород, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  (is_array($type_str)?$type_str[0]:$type_str)." в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ".strtolower(is_array($type_str)?$type_str[1]:$type_str)." в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ".strtolower(is_array($type_str)?$type_str[3]:$type_str)." на любой вкус! А бесплатно забронировать столик в ".strtolower(is_array($type_str)?$type_str[2]:$type_str)." в $region_str можно по телефону: ".$phone_str);
            }
        }

    }
}

if($_REQUEST['PROPERTY']=='kitchen'&&$_REQUEST['PROPERTY2']=='subway'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные кухня-метро
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['kitchen'][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
        if($ar_res2 = $res2->Fetch()){  //    subway
            //Title:  Рестораны немецкой кухни у метро Горьковская в Санкт-Петербурге на Ресторан.ру
            //description: Каталог ресторанов немецкой кухни у метро Горьковская в Санкт-Петербурге на Ресторан.ру
            //keywords: рестораны, немецкая, кухня, метро, горьковская, санкт-петербург
            //Текстовка под выборкой:
            //Restoran.ru представляет полный перечень ресторанов немецкой кухни в Кировском районе. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан немецкой кухни на любой вкус! А бесплатно забронировать столик в ресторанах немецкой кухни в Кировском районе можно по телефону: +7 (812) 740-18-20

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." кухни у метро ".$ar_res2['NAME']." - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." кухни возле метро ".$ar_res2['NAME']." в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", кухня, метро, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." кухни у метро ".$ar_res2['NAME']);
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." кухни у метро ".$ar_res2['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан ".strtolower($ar_res['PREVIEW_TEXT'])." кухни на любой вкус! А бесплатно забронировать столик в ресторанах ".strtolower($ar_res['PREVIEW_TEXT'])." кухни у метро ".$ar_res2['NAME']." можно по телефону: ".$phone_str);
            }
        }
    }
}
if($_REQUEST['PROPERTY']=='kitchen'&&$_REQUEST['PROPERTY2']=='area'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='rga')){//перекрестные кухня-район
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['kitchen'][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['area'][0]);
        if($ar_res2 = $res2->Fetch()){  //    area

            if($ar_res2['PREVIEW_TEXT']){
                $region_str = $ar_res2['PREVIEW_TEXT']." районе";
            }
            else {
                $region_str = "районе ".$ar_res2['NAME'];
            }

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", кухня, район, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан ".strtolower($ar_res['PREVIEW_TEXT'])." кухни на любой вкус! А бесплатно забронировать столик в ресторанах ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str можно по телефону: ".$phone_str);
            }
        }
    }
}
if($_REQUEST['PROPERTY']=='kitchen'&&$_REQUEST['PROPERTY2']=='out_city'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные кухня-пригород
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['kitchen'][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['out_city'][0]);
        if($ar_res2 = $res2->Fetch()){  //    area

            $region_str = $ar_res2['PREVIEW_TEXT']?$ar_res2['PREVIEW_TEXT']:$ar_res2['NAME'];

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", кухня, пригород, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан ".strtolower($ar_res['PREVIEW_TEXT'])." кухни на любой вкус! А бесплатно забронировать столик в ресторанах ".strtolower($ar_res['PREVIEW_TEXT'])." кухни в $region_str можно по телефону: ".$phone_str);
            }
        }
    }
}

if(($_REQUEST['PROPERTY']=='features'||$_REQUEST['PROPERTY']=='children'||$_REQUEST['PROPERTY']=='proposals'||$_REQUEST['PROPERTY']=='entertainment'||$_REQUEST['PROPERTY']=='parking'||$_REQUEST['PROPERTY']=='FEATURES_B_H'||$_REQUEST['PROPERTY']=='ALLOWED_ALCOHOL')&&$_REQUEST['PROPERTY2']=='subway'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные особенности-метро
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
        if($ar_res2 = $res2->Fetch()){  //    subway
            //Title:  Рестораны с панорамным видом у метро Горьковская в Санкт-Петербурге на Ресторан.ру
            //description: Каталог ресторанов с панорамным видом у метро Горьковская в Санкт-Петербурге на Ресторан.ру
            //keywords: рестораны, панорамный вид, кухня, метро, горьковская, санкт-петербург
            //Restoran.ru представляет полный перечень ресторанов с панорамным видом в Кировском районе. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан с панорамным видом на любой вкус! А бесплатно забронировать столик в ресторанах с панорамным видом в Кировском районе можно по телефону: +7 (812) 740-18-20
            if($_REQUEST['PROPERTY']=='parking'){
                $ar_res['NAME'] = $ar_res['NAME']." парковка";
            }
            if($_REQUEST['PROPERTY']=='ALLOWED_ALCOHOL'){
                $ar_res['NAME'] = "свой алкоголь";
            }

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME']." - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." возле метро ".$ar_res2['NAME']." в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", метро, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME']);
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан ".strtolower($ar_res['PREVIEW_TEXT'])." на любой вкус! А бесплатно забронировать столик в ресторанах ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME']." можно по телефону: ".$phone_str);
            }
            elseif ($_REQUEST['CATALOG_ID'] == 'banket') {
                $APPLICATION->SetPageProperty("title",  "Банкетные залы ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME']." - Банкетные залы $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов ".strtolower($ar_res['PREVIEW_TEXT'])." возле метро ".$ar_res2['NAME']." в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "банкетные залы, ".strtolower($ar_res['NAME']).", метро, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Банкетные залы ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME']);
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень банкетных залов ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать банкетный зал ".strtolower($ar_res['PREVIEW_TEXT'])." на любой вкус! А бесплатно забронировать столик в банкетных залах ".strtolower($ar_res['PREVIEW_TEXT'])." у метро ".$ar_res2['NAME']." можно по телефону: ".$phone_str);
            }
        }
    }
}
if(($_REQUEST['PROPERTY']=='features'||$_REQUEST['PROPERTY']=='children'||$_REQUEST['PROPERTY']=='proposals'||$_REQUEST['PROPERTY']=='entertainment'||$_REQUEST['PROPERTY']=='parking'||$_REQUEST['PROPERTY']=='FEATURES_B_H'||$_REQUEST['PROPERTY']=='ALLOWED_ALCOHOL')&&$_REQUEST['PROPERTY2']=='area'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='rga')){//перекрестные особенности-район
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['area'][0]);
        if($ar_res2 = $res2->Fetch()){  //    area
            if($_REQUEST['PROPERTY']=='parking'){
                $ar_res['NAME'] = $ar_res['NAME']." парковка";
            }
            if($_REQUEST['PROPERTY']=='ALLOWED_ALCOHOL'){
                $ar_res['NAME'] = "свой алкоголь";
            }

            if($ar_res2['PREVIEW_TEXT']){
                $region_str = $ar_res2['PREVIEW_TEXT']." районе";
            }
            else {
                $region_str = "районе ".$ar_res2['NAME'];
            }

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", район, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан ".strtolower($ar_res['PREVIEW_TEXT'])." на любой вкус! А бесплатно забронировать столик в ресторанах ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str можно по телефону: ".$phone_str);
            }
            elseif ($_REQUEST['CATALOG_ID'] == 'banket') {
                $APPLICATION->SetPageProperty("title",  "Банкетные залы ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str - Банкетные залы $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог банкетных залов ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "банкетные залы, ".strtolower($ar_res['NAME']).", район, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Банкетные залы ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень банкетных залов ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать банкетный зал ".strtolower($ar_res['PREVIEW_TEXT'])." на любой вкус! А бесплатно забронировать столик в банкетных залах ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str можно по телефону: ".$phone_str);
            }
        }
    }
}
if(($_REQUEST['PROPERTY']=='features'||$_REQUEST['PROPERTY']=='children'||$_REQUEST['PROPERTY']=='proposals'||$_REQUEST['PROPERTY']=='entertainment'||$_REQUEST['PROPERTY']=='parking')&&$_REQUEST['PROPERTY2']=='out_city'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные особенности-район
    $res = CIBlockElement::GetByID($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']][0]);
    if($ar_res = $res->Fetch()){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['out_city'][0]);
        if($ar_res2 = $res2->Fetch()){  //    out_city
            if($_REQUEST['PROPERTY']=='parking'){
                $ar_res['NAME'] = $ar_res['NAME']." парковка";
            }

            $region_str = $ar_res2['PREVIEW_TEXT']?$ar_res2['PREVIEW_TEXT']:$ar_res2['NAME'];

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ".strtolower($ar_res['NAME']).", пригород, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан ".strtolower($ar_res['PREVIEW_TEXT'])." на любой вкус! А бесплатно забронировать столик в ресторанах ".strtolower($ar_res['PREVIEW_TEXT'])." в $region_str можно по телефону: ".$phone_str);
            }
        }
    }
}

if($_REQUEST['PROPERTY']=='breakfast'&&$_REQUEST['PROPERTY2']=='subway'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные завтрак-метро
    if($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']]=='Y'){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['subway'][0]);
        if($ar_res2 = $res2->Fetch()){  //    subway
            if($_REQUEST['PROPERTY']=='breakfast'){
                $prop_str = 'завтрак';
                $prop_str_with = 'с завтраком';
            }
            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны $prop_str_with у метро ".$ar_res2['NAME']." - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов $prop_str_with возле метро ".$ar_res2['NAME']." в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, $prop_str, метро, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны $prop_str_with у метро ".$ar_res2['NAME']);
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов $prop_str_with у метро ".$ar_res2['NAME'].". Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан $prop_str_with на любой вкус! А бесплатно забронировать столик в ресторанах $prop_str_with у метро ".$ar_res2['NAME']." можно по телефону: ".$phone_str);
            }
        }
    }
}
if($_REQUEST['PROPERTY']=='breakfast'&&$_REQUEST['PROPERTY2']=='area'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='rga')){//перекрестные завтрак-район
    if($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']]=='Y'){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['area'][0]);
        if($ar_res2 = $res2->Fetch()){  //    area
            if($_REQUEST['PROPERTY']=='breakfast'){
                $prop_str = 'завтрак';
                $prop_str_with = 'с завтраком';
            }

            if($ar_res2['PREVIEW_TEXT']){
                $region_str = $ar_res2['PREVIEW_TEXT']." районе";
            }
            else {
                $region_str = "районе ".$ar_res2['NAME'];
            }

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны $prop_str_with в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов $prop_str_with в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, $prop_str, район, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны $prop_str_with в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов $prop_str_with в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан $prop_str_with на любой вкус! А бесплатно забронировать столик в ресторанах $prop_str_with в $region_str можно по телефону: ".$phone_str);
            }
        }
    }
}
if($_REQUEST['PROPERTY']=='breakfast'&&$_REQUEST['PROPERTY2']=='out_city'&&LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb')){//перекрестные завтрак-пригород
    if($_REQUEST['arrFilter_pf'][$_REQUEST['PROPERTY']]=='Y'){
        $res2 = CIBlockElement::GetByID($_REQUEST['arrFilter_pf']['out_city'][0]);
        if($ar_res2 = $res2->Fetch()){  //    out_city
            if($_REQUEST['PROPERTY']=='breakfast'){
                $prop_str = 'завтрак';
                $prop_str_with = 'с завтраком';
            }

            $region_str = $ar_res2['PREVIEW_TEXT']?$ar_res2['PREVIEW_TEXT']:$ar_res2['NAME'];

            if($_REQUEST['CATALOG_ID']=='restaurants'){
                $APPLICATION->SetPageProperty("title",  "Рестораны $prop_str_with в $region_str - Рестораны $city_name_first.".$page_str);
                $APPLICATION->SetPageProperty("description",  "Каталог ресторанов $prop_str_with в $region_str в $city_name_second на Ресторан.ру.".$page_str);
                $APPLICATION->SetPageProperty("keywords",  "рестораны, $prop_str, район, ".strtolower($ar_res2['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Рестораны $prop_str_with в $region_str");
                $APPLICATION->SetPageProperty("catalog_prop_description", "Restoran.ru представляет полный перечень ресторанов $prop_str_with в $region_str. Адреса, фотографии интерьера, актуальное меню, отзывы посетителей и независимый рейтинг. Вы сможете подобрать ресторан $prop_str_with на любой вкус! А бесплатно забронировать столик в ресторанах $prop_str_with в $region_str можно по телефону: ".$phone_str);
            }
        }
    }
}
?>
<?endif?>