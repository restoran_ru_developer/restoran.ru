<?
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", 'STREET'
);
foreach($arResult["ITEMS"] as $cell=>$arItem) {
    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        // remove links from subway and kitchen name
        if(in_array($pid, $arFormatProps)) {
            if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {

                $subway_str = '';
                foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                    if(LANGUAGE_ID=='en'){
//                        FirePHP::getInstance()->info($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]);

                        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"),array('ID'=>$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"][$key],'IBLOCK_ID'=>$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["LINK_IBLOCK_ID"]),false,false,array('PROPERTY_eng_name'));
                        if ($ob = $res->Fetch()) {
                            $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = $ob['PROPERTY_ENG_NAME_VALUE'];
                        }
                    }
                    else {
                        if($pid=='subway'){
                            $subway_str .= '<a href="/'.CITY_ID.'/catalog/restaurants/metro/'.$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]['LINK_ELEMENT_VALUE'][$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"][$key]]['CODE'].'/">'.strip_tags($subway).'</a>'.(end($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])!=$subway?', ':'');
                        }
                        elseif($pid=='kitchen'){
                            $subway_str .= '<a href="/'.CITY_ID.'/catalog/restaurants/'.$pid.'/'.$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]['LINK_ELEMENT_VALUE'][$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"][$key]]['CODE'].'/">'.strip_tags($subway).'</a>'.(end($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])!=$subway?', ':'');
                        }
                        else {
                            $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                        }
                    }
                }
                if(($pid=='subway' || $pid=='kitchen') && LANGUAGE_ID!='en') {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = $subway_str;
                }
            }
            else {
                if(LANGUAGE_ID=='en'){
                    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"),array('ID'=>$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"][0],'IBLOCK_ID'=>$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["LINK_IBLOCK_ID"]),false,false,array('PROPERTY_eng_name'));
                    if ($ob = $res->Fetch()) {
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = $ob['PROPERTY_ENG_NAME_VALUE'];
                    }
                }
                else {
                    if($pid=='STREET'&&$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]['VALUE']){
                        if(CITY_ID=='msk'||CITY_ID=='spb'){
                            $db_props = CIBlockElement::GetProperty($arResult["ITEMS"][$cell]['IBLOCK_ID'], $arResult["ITEMS"][$cell]['ID'], array("sort" => "asc"), Array("CODE"=>"SHOW_YA_STREET"));
                            if($ar_props = $db_props->Fetch()){
                                if($ar_props["VALUE_ENUM"]=='Да'){
                                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"]['address']["DISPLAY_VALUE"] = '<a href="'.$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]['LINK_ELEMENT_VALUE'][$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"]]['DETAIL_PAGE_URL'].'">'.(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"]['address']["DISPLAY_VALUE"])?$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"]['address']["DISPLAY_VALUE"][0] : $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"]['address']["DISPLAY_VALUE"]).'</a>';
                                }
                            }
                        }
                    }
                    elseif($pid=='subway'){
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = '<a href="/'.CITY_ID.'/catalog/restaurants/metro/'.$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]['LINK_ELEMENT_VALUE'][$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"][0]]['CODE'].'/">'.strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]).'</a>';
                    }
                    elseif($pid=='kitchen'){
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = '<a href="/'.CITY_ID.'/catalog/restaurants/'.$pid.'/'.$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]['LINK_ELEMENT_VALUE'][$arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["VALUE"][0]]['CODE'].'/">'.strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]).'</a>';
                    }
                    else {
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
                    }
                }
            }
        }
    }

    if($arItem["PREVIEW_PICTURE"])
    {
        $arItem["PROPERTIES"]["photos"]["VALUE"][] = $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"];
    }
    elseif($arItem["DETAIL_PICTURE"])
    {
        $arItem["PROPERTIES"]["photos"]["VALUE"][] = $arResult["ITEMS"][$cell]["DETAIL_PICTURE"];
    }

    foreach ($arItem["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
    {
        $arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][$key] = CFile::ResizeImageGet($photo, array('width' => 372, 'height' => 218), BX_RESIZE_IMAGE_EXACT, true, Array());
    }

    if(!$arItem["PROPERTIES"]["photos"]["VALUE"])
    {
        $arItem["PROPERTIES"]["photos"]['VALUE'][0]["SRC"] = "/tpl/images/noname/rest_nnm.png";
    }
}
?>