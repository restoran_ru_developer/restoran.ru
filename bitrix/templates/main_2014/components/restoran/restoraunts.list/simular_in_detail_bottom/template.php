<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//FirePHP::getInstance()->info($arResult["ITEMS"][0]['PROPERTIES']['address']);
//    FirePHP::getInstance()->info($arResult["ITEMS"][0]['PROPERTIES']['RATIO']);

?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):
    ?>
    <div class="pull-left">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="pic">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
        </div>
        <?endif;?>
        <div class="text">
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <?if ($arParams["REST_PROPS"]):?>            
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"]['VALUE'])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>                      
                </div>
                <div class="props">
                    <div class="prop">
                        <div class="name"><?=GetMessage('NEAR_TITLE_FOR_KITCHEN')?>:</div>
                        <div class="value"><?=implode(", ",$arItem["KITCHEN"])?></div>
                    </div>
                    <div class="prop">
                        <div class="name"><?=GetMessage('NEAR_TITLE_FOR_BILL')?>:</div>
                        <div class="value"><?=$arItem["BILL"]?></div>
                    </div>
                    <div class="prop">
                        <div class="name"><?=GetMessage('NEAR_TITLE_FOR_ADDRESS')?>:</div>
                        <?
                        $clear_city_arr = array(
                            'msk'=>"/( +|)Москва[, ]+/i",
                            'spb'=>"/( +|)Санкт-Петербург[, ]+/i",
                            'rga'=>"/( +|)Рига[, ]+/i",
                            'urm'=>"/( +|)Юрмала[, ]+/i",
                            'kld'=>"/( +|)Калиниград[, ]+/i",
                            'nsk'=>"/( +|)Новосибирск[, ]+/i",
                            'sch'=>"/( +|)Сочи[, ]+/i"
                        );
                        ?>
                        <div class="value"><?=is_array($arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arItem["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?></div>
                    </div>
                </div>
            <?else:?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <?endif;?>
        </div>        
    </div>
<?endforeach;?>