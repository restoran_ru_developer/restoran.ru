<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // resize images
    if(!$arItem["PREVIEW_PICTURE"]) {    
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    } else {    
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    }
    if (!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = "/tpl/images/noname/rest_nnm_new.png";
    
    if($arItem["IBLOCK_TYPE_ID"] == "firms_news")
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = str_replace("msk",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    }
    if (!$arItem["PREVIEW_TEXT"])
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    else
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //get props if it is a restaurant
    if ($arParams["REST_PROPS"]=="Y"):
        if (LANGUAGE_ID=="en")
        {
            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
            while ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetList(Array(),Array("ID"=>$ob['VALUE']), false,false, Array("PROPERTY_eng_name"));
                if ($ar = $r->Fetch())
                {
                    if ($ar["PROPERTY_ENG_NAME_VALUE"]){
                        $arResult["ITEMS"][$key]["KITCHEN"][] = $ar['PROPERTY_ENG_NAME_VALUE'];
                    }
                }
            }

            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
            if ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetList(Array(),Array("ID"=>$ob['VALUE']), false,false, Array("PROPERTY_eng_name"));
                if ($ar = $r->Fetch())
                {
                    if ($ar["PROPERTY_ENG_NAME_VALUE"]){
                        $arResult["ITEMS"][$key]["BILL"] = $ar['PROPERTY_ENG_NAME_VALUE'];
                    }
                }
            }
        }
        else {
            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "kitchen"));
            while ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetByID($ob['VALUE']);
                if ($a = $r->Fetch())
                    $arResult["ITEMS"][$key]["KITCHEN"][] = $a['NAME'];
            }

            $res = CIBlockElement::GetProperty($arResult["ITEMS"][$key]["IBLOCK_ID"], $arResult["ITEMS"][$key]["ID"], "sort", "asc", array("CODE" => "average_bill"));
            if ($ob = $res->Fetch())
            {
                $r = CIBlockElement::GetByID($ob['VALUE']);
                if ($a = $r->Fetch())
                    $arResult["ITEMS"][$key]["BILL"] = $a['NAME'];
            }
        }

    endif;
}
global $arSimilar;
if (is_array($arSimilar["PROPERTY_rest_group"]))
    $arSimilar["PROPERTY_rest_group"] = $arSimilar["PROPERTY_rest_group"][0];
if ($arSimilar["PROPERTY_rest_group"])
{    
    $res = CIBlockElement::GetByID((int)$arSimilar["PROPERTY_rest_group"]);
    if ($ar = $res->Fetch())
    {
        $arResult["REST_GROUP"] = $ar["CODE"];
    }
}
?>