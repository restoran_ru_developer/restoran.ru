<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "review";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "reviews";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "reviews";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "More";
$MESS["ALL_NEWS"] = "View all";
$MESS["ALL_NEWS1"] = "ALL NEWS";
$MESS["ALL_NEW_REST"] = "ALL NEW";
$MESS["ALL_interview"] = "All interviews";
$MESS["ALL_overviews"] = "All overviews";
$MESS["ALL_blogs"] = "All blogs";
$MESS["ALL_cookery"] = "All recipes";
$MESS["ALL_cookerym"] = "All master-classes";
$MESS["ALL_"] = "All overviews";
$MESS["NEAR_TITLE_FOR_KITCHEN"] = "Kitchen";
$MESS["NEAR_TITLE_FOR_BILL"] = "Average bill";
$MESS["NEAR_TITLE_FOR_ADDRESS"] = "Address";
?>