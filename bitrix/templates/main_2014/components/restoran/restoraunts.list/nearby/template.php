<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="sm">   
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="item " style="margin-bottom:0px;">
            <div class="pic">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>"  /></a>            
            </div>
            <div class="text">
                <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                <div class="ratio">
                    <?for($i = 1; $i <= 5; $i++):?>
                        <span class="icon-star<?if($i > round($arItem["PROPERTIES"]["RATIO"]["VALUE"])):?>-empty<?endif?>" alt="<?=$i?>"></span>
                    <?endfor?>
                    <div class="clear"></div>
                </div> 
                <div class="props">
                    <?if ($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]):?>
                        <div class="prop">
                            <?if(is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                                <div class="name"><?=$arItem["DISPLAY_PROPERTIES"]["type"]["NAME"]?>:</div>
                                <div class="value"><?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]))?></div>
                            <?else:?>
                                <div class="name"><?=$arItem["DISPLAY_PROPERTIES"]["type"]["NAME"]?>:</div>
                                <div class="value"><?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?></div>
                            <?endif;?>
                        </div>
                    <?endif;?>
                    <?if ($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]):?>
                        <div class="prop">
                            <?if(is_array($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):?>
                                <div class="name"><?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["NAME"]?>:</div>
                                <div class="value"><?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?></div>
                            <?else:?>
                                <div class="name"><?=$arItem["DISPLAY_PROPERTIES"]["kitchen"]["NAME"]?>:</div>
                                <div class="value"><?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])?></div>
                            <?endif;?>
                        </div>
                    <?endif;?>
                    <?if ($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]):?>
                        <div class="prop">
                            <?if(is_array($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])):?>
                                <div class="name"><?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["NAME"]?>:</div> 
                                <div class="value"><?=  strip_tags(implode(", ",  $arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))?></div>
                            <?else:?>
                                <div class="name"><?=$arItem["DISPLAY_PROPERTIES"]["average_bill"]["NAME"]?>:</div> 
                                <div class="value"><?=  strip_tags($arItem["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])?></div>
                            <?endif;?>                
                        </div>
                    <?endif;?>
                </div>
            </div>                                   
        </div>        
    <?endforeach;?>
</div>