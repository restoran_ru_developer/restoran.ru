<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $cell=>$arItem) {
    if ($arItem["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    else
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    if (!$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"])
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm_new.png";
    $arResult["ITEMS"][$cell]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["DETAIL_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);


    if (LANGUAGE_ID=="en")
    {
        foreach ($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"] as &$properties)
        {

            if (is_array($properties["DISPLAY_VALUE"]))
            {
                if (is_array($properties["VALUE"]))
                {
                    foreach($properties["VALUE"] as $key=>$val)
                    {
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);
                        }
                    }
                }
                else
                {
                    if($properties["VALUE"]){
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                        }
                    }
                }
            }
            else
            {
                if($properties["VALUE"]){
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                    }
                }
            }
        }
    }
}

?>