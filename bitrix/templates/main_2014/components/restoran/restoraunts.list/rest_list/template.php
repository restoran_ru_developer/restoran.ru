<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (count($arResult["ITEMS"])>0):?>
    <?// check exist PAGEN_1?>
    <?

    if($_REQUEST["page"] || $_REQUEST["PAGEN_1"] || $_REQUEST["letter"]):?>
        <div class="restoran-list">
            <div class="hr"></div>

            <?//    FIRST 20
            if(!$_REQUEST["letter"]&&($_REQUEST["page"]==1||$_REQUEST["PAGEN_1"]==1)&&$arParams['BEST_SECTION_ID']):
                global $arrFilterSpecStr;
                $arIB = getArIblock("first_20", $_REQUEST["CITY_ID"]);
                if($arParams['BEST_SECTION_ID']===true){ //  index catalog page
                    $arParams['BEST_SECTION_ID'] = false;
                }
                $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y",'SECTION_ID'=>$arParams['BEST_SECTION_ID']);
                $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, Array("nTopCount"=>20), array('PROPERTY_RESTAURANT'));
                while($ob = $res->Fetch())
                {
                    $arrFilterSpecStr['ID'][] = $ob['PROPERTY_RESTAURANT_VALUE'];
                    $BestSortArr[] = $ob['PROPERTY_RESTAURANT_VALUE'];
                }

                $_REQUEST["CUSTOM_PAGEN_01"] = '';
                $APPLICATION->IncludeComponent("restoran:restoraunts.list", "first-20", Array(
                    "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => $_REQUEST["CITY_ID"],	// Код информационного блока
                    "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                    "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
                    "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
                    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
                    "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
                    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
                    "FILTER_NAME" => "arrFilterSpecStr",	// Фильтр
                    "PROPERTY_CODE" => array(	// Свойства
                        0 => "type",
                        1 => "kitchen",
                        2 => "average_bill",
                        3 => "opening_hours",
                        4 => "phone",
                        5 => "address",
                        6 => "subway",
                        7 => "ratio",
                        8 => "sale10",
                        9 => "REST_NETWORK",
                    ),
                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                    "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                    "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                    "CACHE_TYPE" => "Y",	// Тип кеширования// Y
                    "CACHE_TIME" => "86406",	// Время кеширования (сек.)
                    "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",	// Учитывать права доступа
                    "CACHE_NOTES" => $arParams['CACHE_NOTES'],
                    "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                    "PAGER_TITLE" => "Рестораны",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                    "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36001",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                    "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                    'BEST_SORT_ARRAY' => $BestSortArr
                ),
                    $component
                );?>
            <?else:?>
                <?
                // SET PREV PAGE NUM IF FIRST_20 EXIST

                if($arParams['BEST_SECTION_ID']){
                    $_REQUEST["CUSTOM_PAGEN_01"]=$_REQUEST["page"]-1;

                    global $arrFilter;
                    $APPLICATION->IncludeComponent("restoran:restoraunts.list", "first-20", Array(
                        "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                        "IBLOCK_ID" => $_REQUEST["CITY_ID"],	// Код информационного блока
                        "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                        "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
                        "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
                        "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
                        "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
                        "FILTER_NAME" => 'arrFilter',	// Фильтр
                        "PROPERTY_CODE" => array(	// Свойства
                            0 => "type",
                            1 => "kitchen",
                            2 => "average_bill",
                            3 => "opening_hours",
                            4 => "phone",
                            5 => "address",
                            6 => "subway",
                            7 => "ratio",
                            8 => "sale10",
                            9 => "REST_NETWORK",
                        ),
                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                        "AJAX_MODE" => "N",	// Включить режим AJAX
                        "AJAX_OPTION_SHADOW" => "Y",
                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "CACHE_TYPE" => "Y",	// Тип кеширования// Y
                        "CACHE_TIME" => "86406",	// Время кеширования (сек.)
                        "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "N",	// Учитывать права доступа
                        "CACHE_NOTES" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                        "PAGER_TITLE" => "Рестораны",	// Название категорий
                        "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                        "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36001",	// Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
//                            "CUSTOM_PAGER_WITHOUT_EN" => $arParams['BEST_SECTION_ID']?"",	// Дополнительный идентификатор

                    ),
                        $component
                    );

                }
                else {?>
                    <?
                    foreach($arResult["ITEMS"] as $cell=>$arItem):?>
                        <div class="item" >
                            <?
                            if ($_REQUEST["letter"]&&$_REQUEST["letter"]!="09")
                            {
                                $letter = iconv("windows-1251","utf-8",chr($_REQUEST["letter"]));
                                if ($letter!=substr($arItem["NAME"],0,1))
                                {
                                    $temp = array();
                                    $temp = explode(",",$arItem["TAGS"]);
                                    $arItem["NAME"] = $temp[0];
                                }
                            }
                            ?>
                            <div class="type">
                                <?if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?>
                                <?else:?>
                                    <?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
                                <?endif;?>
                            </div>
                            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                            <div class="name_ratio">
                                <?for($i = 1; $i <= 5; $i++):?>
                                    <span class="icon-star<?if($i > $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
                                <?endfor?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="left pull-left">
                                <div class="pic">
                                    <?if($arParams['SORT_BY1']=='distance'&&$arItem["PROPERTIES"]["lat"]["VALUE"][0]&&$arItem["PROPERTIES"]["lon"]["VALUE"][0]&&$_SESSION['lat'] && $_SESSION['lon']):?>
                                        <?
                                        $this_distance_value = '';
                                        $distance = calculateDistanceToRest($_SESSION['lat'], $_SESSION['lon'], $arItem["PROPERTIES"]["lat"]["VALUE"][0], $arItem["PROPERTIES"]["lon"]["VALUE"][0]);
                                        $this_distance_value = round($distance / 1000, 1);
                                        ?>
                                        <div class="distance-place" this-rest-id="<?//=$arItem['ID']?>" lat="<?//=$arItem["PROPERTIES"]["lat"]["VALUE"][0]?>" lon="<?//=$arItem["PROPERTIES"]["lon"]["VALUE"][0]?>"><span><?=$this_distance_value?></span> КМ</div>
                                    <?endif?>
                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                                    <?if ($arItem["PROPERTIES"]["d_tours"]["VALUE"]):?>
                                        <div style="position:absolute; right:20px; bottom:20px;"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour2.png" width="70"/></div>
                                    <?endif;?>
                                </div>
                            </div>
                            <div class="right pull-left">
                                <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                                    <?if($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE']&&($pid=='subway'||$pid=='address')):?>
                                        <?if($pid=='address')
                                            continue;
                                        ?>
                                        <div class="prop">
                                            <div class="name">
                                                <?=GetMessage('NETWORK_REST_TITLE')?>:
                                            </div>
                                            <div class="value">
                                                <?=count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])?> <?if(count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])==1)echo GetMessage('ADDRESS_OF_NETWORK_TITLE');elseif(count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])<5)echo GetMessage('ADDRESS_OF_NETWORK_TITLE_A');elseif(count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])>4)echo GetMessage('ADDRESS_OF_NETWORK_TITLE_OF');?>
                                            </div>
                                        </div>
                                    <?else:?>
                                        <?if ($pid!="type"&&$pid!="photos"&&$pid!="RATIO"&&$pid!='REST_NETWORK'):?>
                                            <?if ($pid!="subway"):?>
                                                <div class="prop">
                                                    <div class="name">
                                                        <?if ($pid=="subway"&&CITY_ID=="urm"):?>
                                                            <b>Ж/д станция: </b>
                                                        <?else:?>
                                                            <?=$arProperty["NAME"]?>:
                                                        <?endif;?>
                                                    </div>
                                                    <div class="value">
                                                        <?if ($pid=="phone"&&($_REQUEST["CONTEXT"]=="Y"||$arItem['PROPERTIES']['sleeping_rest']['VALUE'])):?>
                                                            <?if (CITY_ID=="spb"):?>
                                                                <a href="tel:+78127401820" class="<? echo MOBILE;?>">+7(812) 740-18-20</a>
                                                            <?elseif(CITY_ID == "rga" || CITY_ID == "urm"):?>
                                                                <a href="tel:+37166103106" class="<? echo MOBILE;?>">+371 661 031 06</a>
                                                            <?else:?>
                                                                <a href="tel:+74959882656" class="<? echo MOBILE;?>">+7(495) 988-26-56</a>
                                                            <?endif;?>
                                                        <?else:?>
                                                            <?if ($pid=="phone"):?>
                                                                <?

                                                                $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                                                if(is_array($arProperty["DISPLAY_VALUE"]))
                                                                    $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);
                                                                else{
                                                                    $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
                                                                    //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                                }

                                                                $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);



                                                                $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                                preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);


                                                                $TELs=array();
                                                                for($p=1;$p<5;$p++){
                                                                    foreach($matches[$p] as $key=>$v){
                                                                        $TELs[$key].=$v;
                                                                    }
                                                                }

                                                                foreach($TELs as $key => $T){
                                                                    if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                                                }
                                                                $old_phone = "";
                                                                foreach($TELs as $key => $T){
                                                                    if ($old_phone!=$T):

                                                                        ?>
                                                                        <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=clearUserPhone($arProperty["DISPLAY_VALUE3"][$key])?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                                                    <?endif;$old_phone = $T;?>
                                                                <?}?>
                                                            <?else:?>
                                                                <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                                                    <?=$arProperty["DISPLAY_VALUE"]?>
                                                                <?else:?>
                                                                    <?=$arProperty["DISPLAY_VALUE"][0]?>
                                                                <?endif?>
                                                            <?endif;?>
                                                        <?endif;?>
                                                    </div>
                                                </div>
                                            <?else:?>
                                                <div class="prop wsubway">
                                                    <div class="subway">M</div>
                                                    <div class="value">
                                                        <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                                            <?=$arProperty["DISPLAY_VALUE"]?>
                                                        <?else:?>
                                                            <?=$arProperty["DISPLAY_VALUE"][0]?>
                                                        <?endif?>
                                                    </div>
                                                </div>
                                            <?endif;?>
                                        <?endif;?>
                                    <?endif;?>
                                <?endforeach;?>
                            </div>
                            <div class="left pull-left">
                                <div class="order-buttons">
                                    <?if (CITY_ID=="msk"||CITY_ID=="spb"||CITY_ID=="nsk"):?>
                                        <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "banket") ? "?banket=Y" : "" ?>"
                                           class="booking btn btn-info btn-nb" data-id='<?= $arItem["ID"] ?>'
                                           data-restoran='<?= $arItem["NAME"] ?>'><?= ($_REQUEST["CATALOG_ID"] == "banket") ? GetMessage("R_ORDER_BANKET2") : GetMessage("R_BOOK_TABLE2") ?></a>
                                    <?endif;?>
                                    <?if (CITY_ID=="rga"||CITY_ID=="urm"):?>
                                        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "banket") ? "?banket=Y" : "" ?>"
                                           class="booking btn btn-info btn-nb" data-id='<?= $arItem["ID"] ?>'
                                           data-restoran='<?= $arItem["NAME"] ?>'><?= ($_REQUEST["CATALOG_ID"] == "banket") ? GetMessage("R_ORDER_BANKET2") : GetMessage("R_BOOK_TABLE2") ?></a>
                                    <?endif;?>
                                    <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
                                </div>
                            </div>
                            <div class='clearfix'></div>
                        </div>
                        <?if ($cell%2==1||end($arResult["ITEMS"])==$arItem):?>
                            <div class="clearfix"></div>
                            <div class="hr"></div>
                        <?endif;?>
                    <?endforeach;?>
                <?}
                ?>
            <?endif?>
        </div>
    <?else:?>
        <div class="priority-list">
            <div class="hr"></div>
            <?
            global $arrFilterSpecStr;
            if($arParams['BEST_SECTION_ID']):
                $arIB = getArIblock("best", $_REQUEST["CITY_ID"]);
                $arFilter = Array("IBLOCK_ID"=>$arIB['ID'], "ACTIVE"=>"Y",'SECTION_ID'=>$arParams['BEST_SECTION_ID']);
                $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter, false, Array("nTopCount"=>20), array('PROPERTY_RESTAURANT'));
                while($ob = $res->Fetch())
                {
                    $arrFilterSpecStr['ID'][] = $ob['PROPERTY_RESTAURANT_VALUE'];
                    $BestSortArr[] = $ob['PROPERTY_RESTAURANT_VALUE'];
                }
            else:
                $arrFilterSpecStr = Array(
                    "PROPERTY_s_place_".$_REQUEST["CATALOG_ID"]."_VALUE" => Array("Да")
                );
            endif;
            $APPLICATION->IncludeComponent("restoran:restoraunts.list",
                "rest_spec_places_new",
                Array(
                    "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => CITY_ID,	// Код информационного блока
                    "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
                    "NEWS_COUNT" => "20",	// Количество ресторанов на странице
                    "SORT_BY1" => "SORT",	// Поле для первой сортировки ресторанов
                    "SORT_ORDER1" => "DESC",	// Направление для первой сортировки ресторанов
                    "SORT_BY2" => "",	// Поле для второй сортировки ресторанов
                    "SORT_ORDER2" => "",	// Направление для второй сортировки ресторанов
                    "FILTER_NAME" => "arrFilterSpecStr",	// Фильтр
                    "PROPERTY_CODE" => array(	// Свойства
                        0 => "type",
                        1 => "kitchen",
                        2 => "average_bill",
                        3 => "opening_hours",
                        4 => "phone",
                        5 => "address",
                        6 => "subway",
                        7 => "photos",
                        8 => "RATIO"
                    ),
                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                    "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                    "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "AJAX_OPTION_SHADOW" => "Y",
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                    "CACHE_TYPE" => "Y",	// Тип кеширования
                    "CACHE_TIME" => "86409",	// Время кеширования (сек.)
                    "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",	// Учитывать права доступа
                    "CACHE_NOTES" => $arParams['CACHE_NOTES'],
                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                    "PAGER_TITLE" => "Рестораны",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
                    "PAGER_TEMPLATE" => "rest_list",	// Название шаблона
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                    "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                    "CONTEXT"=>$_REQUEST["CONTEXT"],
                    'BEST_SORT_ARRAY' => $BestSortArr
                ),
                $component
            );?>
        </div>
    <?endif?>
    <div class="navigation_temp firsttt">
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>
<?else:?>
    <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
<?endif;?>