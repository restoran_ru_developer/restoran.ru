<?
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "address"
);

//  сортировка от фильтрующего массива
if($arParams['BEST_SORT_ARRAY']){
    foreach($arParams['BEST_SORT_ARRAY'] as $parent_news_id){
        foreach($arResult["ITEMS"] as  $key=> $one_item){
            if($one_item['ID']==$parent_news_id)
                $new_items_arr['ITEMS'][] = $one_item;
        }
    }
    $arResult["ITEMS"] = $new_items_arr['ITEMS'];
}

foreach($arResult["ITEMS"] as $cell=>$arItem) {
    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        // remove links from subway and kitchen name
        if(in_array($pid, $arFormatProps)) {
            if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                $subway_str = '';
                foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                    if($pid=='subway' || $pid=='kitchen'){
                        $subway_str .= strip_tags($subway).(end($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])!=$subway?', ':'');
                    }
                    else {
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                    }
                }
                if($pid=='subway' || $pid=='kitchen'){
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = $subway_str;
                }
            } else {
                $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
            }
        }
    }
    if (LANGUAGE_ID=="en")
    {
        foreach ($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"] as $pid=>&$properties)
        {
            if(in_array($pid, $arFormatProps)) {
                if (is_array($properties["DISPLAY_VALUE"]))
                {
                    foreach ($properties["VALUE"] as $key => $val) {
                        if($properties['PROPERTY_TYPE']=='E'){
                            $res = CIBlockElement::GetProperty($properties['LINK_ELEMENT_VALUE'][$val]['IBLOCK_ID'], $val, "sort", "asc", array("CODE" => 'eng_name'));
                            if ($ob = $res->Fetch()) {
                                $properties["DISPLAY_VALUE"][$key] = $ob['VALUE'];
                            }
                        }
                        else {
                            $properties["DISPLAY_VALUE"][$key] = $val;
                        }
                    }
                }
                else
                {
                    if($properties['PROPERTY_TYPE']=='E') {
                        $res = CIBlockElement::GetProperty($properties['LINK_ELEMENT_VALUE'][$properties['VALUE'][0]]['IBLOCK_ID'], $properties['LINK_ELEMENT_VALUE'][$properties['VALUE'][0]]['ID'], "sort", "asc", array("CODE" => 'eng_name'));
                        if ($ob = $res->Fetch()) {
                            $properties["DISPLAY_VALUE"] = $ob['VALUE'];
                        }
                    }
                    else {
                        $properties["DISPLAY_VALUE"] = $properties['VALUE'];
                    }
                }
            }
        }
    }
    if($arItem["PREVIEW_PICTURE"])
    {
        //$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = $arItem["PREVIEW_PICTURE"]["SRC"];
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>232, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true, Array());
    }
    elseif($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>232, 'height'=>155), BX_RESIZE_IMAGE_EXACT, true, Array());

    }
    elseif($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][0])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][0], array('width' => 232, 'height' => 155), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
    }
    else
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm_new.png";
    //v_dump($arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]);

}

?>