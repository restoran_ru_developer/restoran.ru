<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult["ITEMS"])>0):?>

    <?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
        <div class="item">
            <?
            if ($_REQUEST["letter"]&&$_REQUEST["letter"]!="09")
            {
                $letter = iconv("windows-1251","utf-8",chr($_REQUEST["letter"]));
                if ($letter!=substr($arItem["NAME"],0,1))
                {
                    $temp = array();
                    $temp = explode(",",$arItem["TAGS"]);
                    $arItem["NAME"] = $temp[0];
                }
            }
            ?>
            <div class="type">
                <?if (is_array($arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):?>
                    <?=implode(", ",$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])?>
                <?else:?>
                    <?=$arItem["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]?>
                <?endif;?>
            </div>
            <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
            <div class="name_ratio">
                <?for($i = 1; $i <= 5; $i++):?>
                    <span class="icon-star<?if($i > $arItem["PROPERTIES"]["RATIO"]["VALUE"]):?>-empty<?endif?>"></span>
                <?endfor?>
            </div>
            <div class="clearfix"></div>
            <div class="left pull-left">
                <div class="pic">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                    <?if ($arItem["PROPERTIES"]["d_tours"]["VALUE"]):?>
                        <div style="position:absolute; right:20px; bottom:20px;"><img alt="3D тур" title="3D тур" src="/tpl/images/3dtour2.png" width="70"/></div>
                    <?endif;?>
                </div>
                <!--                        <div class="order-buttons">
                            <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arItem["ID"]?>' data-restoran='<?=$arItem["NAME"]?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
                            <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
                        </div>-->
            </div>
            <div class="right pull-left">
                <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

                    <?if($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE']&&($pid=='subway'||$pid=='address')):?>
                        <?if($pid=='address')
                            continue;
                        ?>
                        <div class="prop">
                            <div class="name">
                                <?=GetMessage('NETWORK_REST_TITLE')?>:
                            </div>
                            <div class="value">
                                <?=count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])?> <?if(count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])==1)echo GetMessage('ADDRESS_OF_NETWORK_TITLE');elseif(count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])<5)echo GetMessage('ADDRESS_OF_NETWORK_TITLE_A');elseif(count($arItem["DISPLAY_PROPERTIES"]['REST_NETWORK']['VALUE'])>4)echo GetMessage('ADDRESS_OF_NETWORK_TITLE_OF');?>
                            </div>
                        </div>
                    <?else:?>
                        <?if ($pid!="type"&&$pid!="photos"&&$pid!="RATIO"&&$pid!='REST_NETWORK'):?>
                            <?if ($pid!="subway"):?>
                                <div class="prop">
                                    <div class="name">
                                        <?if ($pid=="subway"&&CITY_ID=="urm"):?>
                                            <b>Ж/д станция: </b>
                                        <?else:?>
                                            <?=$arProperty["NAME"]?>:
                                        <?endif;?>
                                    </div>
                                    <div class="value">
                                        <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                            <?if (CITY_ID=="spb"):?>
                                                <a href="tel:+78127401820" class="<? echo MOBILE;?>">+7(812) 740-18-20</a>
                                            <?elseif(CITY_ID == "rga" || CITY_ID == "urm"):?>
                                                <a href="tel:+37166103106" class="<? echo MOBILE;?>">+371 661 031 06</a>
                                            <?else:?>
                                                <a href="tel:+74959882656" class="<? echo MOBILE;?>">+7(495) 988-26-56</a>
                                            <?endif;?>
                                        <?else:?>
                                            <?if ($pid=="phone"):?>
                                                <?

                                                $arProperty["DISPLAY_VALUE"] = str_replace(";", "", $arProperty["DISPLAY_VALUE"]);

                                                if(is_array($arProperty["DISPLAY_VALUE"]))
                                                    $arProperty["DISPLAY_VALUE2"]=implode(", ",$arProperty["DISPLAY_VALUE"]);
                                                else{
                                                    $arProperty["DISPLAY_VALUE2"]=$arProperty["DISPLAY_VALUE"];
                                                    //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                }

                                                $arProperty["DISPLAY_VALUE3"]=  explode(",", $arProperty["DISPLAY_VALUE2"]);



                                                $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                preg_match_all($reg, $arProperty["DISPLAY_VALUE2"], $matches);


                                                $TELs=array();
                                                for($p=1;$p<5;$p++){
                                                    foreach($matches[$p] as $key=>$v){
                                                        $TELs[$key].=$v;
                                                    }
                                                }

                                                foreach($TELs as $key => $T){
                                                    if(strlen($T)>7)  $TELs[$key]="+7".$T;
                                                }
                                                $old_phone = "";
                                                foreach($TELs as $key => $T){
                                                    if ($old_phone!=$T):

                                                        ?>
                                                        <a href="tel:<?=$T?>" class="tnum <? echo MOBILE;?>"><?=clearUserPhone($arProperty["DISPLAY_VALUE3"][$key])?></a><?if(isset($TELs[$key+1]) && $TELs[$key+1]!="") echo ', ';?>
                                                    <?endif;$old_phone = $T;?>
                                                <?}?>
                                            <?else:?>
                                                <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                                    <?=$arProperty["DISPLAY_VALUE"]?>
                                                <?else:?>
                                                    <?=$arProperty["DISPLAY_VALUE"][0]?>
                                                <?endif?>
                                            <?endif;?>
                                        <?endif;?>
                                    </div>
                                </div>
                            <?else:?>
                                <div class="prop wsubway">
                                    <div class="subway">M</div>
                                    <div class="value">
                                        <?if(!is_array($arProperty["DISPLAY_VALUE"])):?>
                                            <?=$arProperty["DISPLAY_VALUE"]?>
                                        <?else:?>
                                            <?=$arProperty["DISPLAY_VALUE"][0]?>
                                        <?endif?>
                                    </div>
                                </div>
                            <?endif;?>
                        <?endif;?>
                    <?endif;?>
                <?endforeach;?>
            </div>
            <div class="left pull-left">
                <div class="order-buttons">
                    <?if (CITY_ID=="msk"||CITY_ID=="spb"||CITY_ID=="nsk"):?>
                        <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "banket") ? "?banket=Y" : "" ?>"
                           class="booking btn btn-info btn-nb" data-id='<?= $arItem["ID"] ?>'
                           data-restoran='<?= $arItem["NAME"] ?>'><?= ($_REQUEST["CATALOG_ID"] == "banket") ? GetMessage("R_ORDER_BANKET2") : GetMessage("R_BOOK_TABLE2") ?></a>
                    <?endif;?>
                    <?if (CITY_ID=="rga"||CITY_ID=="urm"):?>
                        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "banket") ? "?banket=Y" : "" ?>"
                           class="booking btn btn-info btn-nb" data-id='<?= $arItem["ID"] ?>'
                           data-restoran='<?= $arItem["NAME"] ?>'><?= ($_REQUEST["CATALOG_ID"] == "banket") ? GetMessage("R_ORDER_BANKET2") : GetMessage("R_BOOK_TABLE2") ?></a>
                    <?endif;?>
                    <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="serif favorite" data-toggle="favorite" data-restoran='<?=$arItem["ID"]?>'><?=GetMessage("R_ADD2FAVORITES")?></a>
                </div>
            </div>
            <div class='clearfix'></div>
        </div>
        <?if ($cell%2==1||end($arResult["ITEMS"])==$arItem):?>
            <div class="clearfix"></div>
            <div class="hr"></div>
        <?endif;?>
    <?endforeach;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]&&$_REQUEST["CUSTOM_PAGEN_01"]):?>
        <div class="navigation_temp">
            <?=$arResult["NAV_STRING"]?>
        </div>
    <?endif;?>
<?else:?>
    <p class="another_color"><?=GetMessage("NOT_FOUND")?></p>
<?endif;?>

