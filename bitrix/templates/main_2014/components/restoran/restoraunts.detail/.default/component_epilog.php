<?php
/*$count = count($_COOKIE["RECENTLY_VIEWED"]["catalog"]);
if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["catalog"]))
    setcookie ("RECENTLY_VIEWED[catalog][".$count."]", $arResult["ID"], time() + 3600*12, "/");*/


global $element_id, $menu_id, $count, $without_reviews, $similar_rest, $RGROUP, $menu_items, $FOURSQUARE_USER_LOGIN, $INSTAGRAM_USER_LOGIN, $photoSectionId, $RESTORAN_NAME, $SHOW_REST_PHONE_IN_ORDER_BANNER, $restaurant_phone;

$RESTORAN_NAME = $arResult['NAME'];
$FOURSQUARE_USER_LOGIN = $arResult['PROPERTIES']['FOURSQUARE_USER_LOGIN']['VALUE'];
$INSTAGRAM_USER_LOGIN = $arResult['PROPERTIES']['INSTAGRAM_USER_LOGIN']['VALUE'];

if($arResult['PROPERTIES']['SHOW_REST_PHONE_IN_ORDER_BANNER']['VALUE_ENUM']=='Да'){
    $SHOW_REST_PHONE_IN_ORDER_BANNER = true;

    if(is_array($arResult["PROPERTIES"]["phone"]["VALUE"])){
        $restaurant_phone = $arResult["PROPERTIES"]["phone"]["VALUE"][0];
    }
    else {
        $restaurant_phone = $arResult["PROPERTIES"]["phone"]["VALUE"];
    }
}


$photoSectionId = $arResult['photoSectionId'];

$element_id = $arResult["ID"];
$menu_id = $arResult["MENU_ID"];
$without_reviews = $arResult['WITHOUT_REVIEWS'];
if (!$arResult["PREVIEW_TEXT"]&&!$arResult["DETAIL_TEXT"])
    $menu_items = 2;
else
    $menu_items = 5;
$similar_rest = $arResult["SIMILAR"];
$count["AFISHA_COUNT"] = $arResult["AFISHA_COUNT"];
$count["NEWS_COUNT"] = $arResult["NEWS_COUNT"];
$count["MC_COUNT"] = $arResult["MC_COUNT"];
$count["SPEC_COUNT"] = $arResult["SPEC_COUNT"];
$count["BLOG_COUNT"] = $arResult["BLOG_COUNT"];
$count["OVERVIEWS_COUNT"] = $arResult["OVERVIEWS_COUNT"];
$count["INTERVIEW_COUNT"] = $arResult["INTERVIEW_COUNT"];
$count["PHOTO_COUNT"] = $arResult["PHOTO_COUNT"];
$count["CONTACTS"] = $arResult["CONTACTS"];
$count["LAT"] = $arResult["LAT"];
$count["LON"] = $arResult["LON"];
$count["VIDEO_NEWS_COUNT"] = $arResult["VIDEO_NEWS_COUNT"];
$RGROUP = $arResult["RGROUP"];


$ratio = '<div class="name_ratio">';
for($i = 1; $i <= 5; $i++):
    if ($i > $arResult["RATIO"])
        $ratio .= '<span class="icon-star-empty"></span>';
    else
        $ratio .= '<span class="icon-star"></span>';
endfor;
$ratio .= '</div><div class="pull-right" id="for_edit_link"></div>';
$APPLICATION->AddViewContent("restoran_name_view_content", $arResult["H1"].$ratio);
global $APPLICATION;
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
//$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);

if($_REQUEST["CONTEXT"]=="Y"){
	//$_SESSION["CONTEXT"]="Y";
	//var_dump($APPLICATION->get_cookie("CONTEXT"));
	if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
}
//if ($USER->IsAdmin())
//$APPLICATION->SetPageProperty("title",  "Ресторан ".$arResult["NAME"].". Адрес, меню, фото, отзывы на сайте Ресторан.Ру");
    //$APPLICATION->SetTitle();
if ($arResult["IBLOCK"]["NAME"]=="Москва")
        $n = "Москве";
    elseif ($arResult["IBLOCK"]["NAME"]=="Юрмала")
        $n = "Юрмале";
    elseif ($arResult["IBLOCK"]["NAME"]=="Рига")
        $n = "Риге";
    elseif ($arResult["IBLOCK"]["NAME"]=="Тюмень")
        $n = "Тюмени";
    elseif ($arResult["IBLOCK"]["NAME"]=="Тюмень")
        $n = "Тюмени";
    elseif ($arResult["IBLOCK"]["NAME"]=="Сочи")
        $n = "Сочи";
    elseif ($arResult["IBLOCK"]["NAME"]=="Уфа")
        $n = "Уфе";
    elseif ($arResult["IBLOCK"]["NAME"]=="Астана")
        $n = "Астане";
    elseif ($arResult["IBLOCK"]["NAME"]=="Алмата")
        $n = "Алмате";
    else
        $n = $arResult["IBLOCK"]["NAME"]."е";
    
    $APPLICATION->SetPageProperty("title",  "Ресторан ".$arResult["NAME"]." в ".$n." на Ресторан.Ру");
    $APPLICATION->SetPageProperty("description",  "Ресторан ".$arResult["NAME"]." в ".$n." на Ресторан.Ру");
    $APPLICATION->SetPageProperty("keywords",  "ресторан, ".$arResult["NAME"].", ".$arResult["IBLOCK"]["NAME"]."");
?>
<script>
$(window).load(function(){
    if (window.location.hash=='#map_tab') {
        $('.anchors-list a[href="'+window.location.hash+'"]').click();
        var pos=$('.anchors-list').offset().top;
        $('html,body').animate({scrollTop:pos},"300");
    }
});
    $(document).ready(function(){
        <?if(CSite::InGroup( array(14,15,1,23,24,29,28,30,34))):?>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
            $("#for_edit_link").append('<div id="edit_link" style="margin-top:12px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');
            <?else:?>
            $("#for_edit_link").append('<div id="edit_link" style="margin-top:12px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');
            <?endif;?>
        <?endif;?>

        <?if ($_REQUEST["bron"]=="Y"):?>
            var params = {"id":$('.order-panel a.booking').data('id'),"name":$('.order-panel a.booking').data('restoran')};
            $.ajax({
                type: "POST",
                url: $('.order-panel a.booking').attr("href"),
                data: params
            })
                .done(function(data) {
                    $('#booking_form .modal-body').html(data);
                });
            $('#booking_form .modal-dialog').removeClass("small");
            $('#booking_form').modal('show');
        <?endif;?>
        <?if ($_REQUEST["bron_banket"]=="Y"):?>
            $('html,body').animate({scrollTop:600}, "300");
            $.ajax({
                type: "POST",
                url: "/tpl/ajax/online_order_rest.php",
                data: "what=2&date="+$(".whose_date").val()+"&name=<?=rawurlencode($arResult["NAME"])?>&id=<?=$arResult["ID"]?>&time="+$(".time").val()+"&person="+$("#person").val()+"&<?=bitrix_sessid_get()?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");
                    }
                    $('#bron_modal').html(data);
                    showOverflow();
                    setCenter($("#bron_modal"));
                    $("#bron_modal").fadeIn("300");
                }
            });
        <?endif;?>
    });
</script>
<?
					//if ($USER->IsAdmin()) {
// костыль. Принять данные из $_REQUEST, которые пришли из формы на Яндексе (острова)

//$arResult["arrVALUES"]["form_text_64"]
if (!empty($_REQUEST["form_text_32"])) {
$arr_name = explode('~',$_REQUEST["form_text_32"]);
// имя и id ресторана
	//$arResult["arrVALUES"]["form_text_32"] = $_REQUEST["form_text_32"].'';
	//$arResult["NAME"] = $_REQUEST["form_text_32"].'';
	?>
<input type="hidden" name="frm_form_text_32" id="frm_form_text_32" value="<?=$arr_name[0]?>" />
<input type="hidden" name="rcurrent_id" id="rcurrent_id" value="<?=$arr_name[1]?>" />
	<?
}

if (!empty($_REQUEST["form_text_39"])) {
// фио
	?>
<input type="hidden" name="frm_form_text_39" id="frm_form_text_39" value="<?=$_REQUEST["form_text_39"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_40"])) {
// телефон
	?>
<input type="hidden" name="frm_form_text_40" id="frm_form_text_40" value="<?=$_REQUEST["form_text_40"]?>" />
	<?
}

if (!empty($_REQUEST["form_email_41"])) {
// E-mail
	?>
<input type="hidden" name="frm_form_email_41" id="frm_form_email_41" value="<?=$_REQUEST["form_email_41"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_35"])) {
// персон
	?>
<input type="hidden" name="frm_form_text_35" id="frm_form_text_35" value="<?=$_REQUEST["form_text_35"]?>" />
	<?
}

if (!empty($_REQUEST["form_checkbox_smoke"])) {
// курящий / не курящий
	?>
<input type="hidden" name="frm_form_checkbox_smoke" id="frm_form_checkbox_smoke" value="<?=$_REQUEST["form_checkbox_smoke"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_34"])) {
// time
	?>
<input type="hidden" name="frm_form_text_34" id="frm_form_text_34" value="<?=$_REQUEST["form_text_34"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_33"])) {
// date
$curDate = $_REQUEST["form_text_33"];
$curDate = preg_replace("/^([\d]{2}+)([\d]{2}+)/is","$1.$2.",$curDate);
	?>
<input type="hidden" name="frm_form_text_33" id="frm_form_text_33" value="<?=$curDate?>" />
<input type="hidden" name="frm_form_date_33" id="frm_form_date_33" value="<?=$curDate?>" />
	<?
}
?>