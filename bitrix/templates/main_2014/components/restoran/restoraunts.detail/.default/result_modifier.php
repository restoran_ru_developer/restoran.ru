<?

// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek","opening_hours"
);

//  показывать телефон ресторатора
if(preg_match('/banquet-service/',$_SERVER['HTTP_REFERER'])){
    $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams["ELEMENT_ID"], "sort", "asc", Array("CODE"=>"SHOW_REST_PHONE"));
    if($ar_props = $db_props->Fetch()){
        if($ar_props['VALUE_ENUM']=='Да'){
            $arResult['SHOW_REST_PHONE'] = true;
        }
    }
}

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $old_val = $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"];
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = array();
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][0] = strip_tags($old_val);
        }
    }
}
if (LANGUAGE_ID=="en")
{
    foreach ($arResult["DISPLAY_PROPERTIES"] as &$properties)
    {
        if($properties['PROPERTY_TYPE']=='E') {
            if (is_array($properties["DISPLAY_VALUE"])) {
                if (is_array($properties["VALUE"])) {
                    foreach ($properties["VALUE"] as $key => $val) {
                        $r = CIBlockElement::GetList(Array(), Array("ID" => $val), false, false, Array("ID", "NAME", "PROPERTY_eng_name"));
                        if ($ar = $r->Fetch()) {
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"], $ar["PROPERTY_ENG_NAME_VALUE"], $properties["DISPLAY_VALUE"][$key]);
                        }
                    }
                } else {
                    $r = CIBlockElement::GetList(Array(), Array("ID" => $properties["VALUE"]), false, false, Array("ID", "NAME", "PROPERTY_eng_name"));
                    if ($ar = $r->Fetch()) {
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"], $ar["PROPERTY_ENG_NAME_VALUE"], $properties["DISPLAY_VALUE"]);
                    }
                }
            } else {

                $r = CIBlockElement::GetList(Array(), Array("ID" => $properties["VALUE"]), false, false, Array("ID", "NAME", "PROPERTY_eng_name"));
                if ($ar = $r->Fetch()) {
                    if ($ar["PROPERTY_ENG_NAME_VALUE"])
                        $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"], $ar["PROPERTY_ENG_NAME_VALUE"], $properties["DISPLAY_VALUE"]);

                }
            }
        }
        if ($properties["CODE"]=="wi_fi")
                    $properties["DISPLAY_VALUE"] = "Yes";
            
    }
}

$old_val = "";
if ($arResult["PROPERTIES"]["network"]["VALUE"])
{
    $i=1;
    $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"],"PROPERTY_network"=>$arResult["PROPERTIES"]["network"]["VALUE"],"!ID"=>$arResult["ID"]),false,false,Array("ID", "DETAIL_PAGE_URL","PROPERTY_address","PROPERTY_phone","PROPERTY_subway"));
    while ($ar = $res->GetNext())
    {
        //v_dump($ar);
        $arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][$i] = $ar["PROPERTY_ADDRESS_VALUE"];
        $arResult["DISPLAY_PROPERTIES"]["network_url"][$i] = $ar["DETAIL_PAGE_URL"];
        $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][$i] = $ar["PROPERTY_PHONE_VALUE"];
        
        $rrr = CIBlockElement::GetByID($ar["PROPERTY_SUBWAY_VALUE"]);
        if ($arr = $rrr->Fetch())
            $arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][$i] = $arr["NAME"];
        $i++;
    }  
}
//$res = array();
//$arNightIB = getArIblock("special_projects", CITY_ID,"new_year_night"."_");
//$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNightIB["ID"]),false,Array("nTopCount"=>1));
//if ($aNight = $res->GetNext())
//{
//    $arResult["NIGHT_COUNT"] = 1;
//    $arResult["NIGHT_LINK"] = $aNight["DETAIL_PAGE_URL"];
//}
//
//$res = array();
//$arCorpIB = getArIblock("special_projects", CITY_ID,"new_year_corp"."_");
//$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//if ($aCorp = $res->GetNext())
//{
//    $arResult["CORP_COUNT"] = 1;
//    $arResult["CORP_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//}


$res = array();
$arNewsIB = getArIblock("news", CITY_ID);
if ($arNewsIB["ID"])
{    
    if (CITY_ID!="msk"&&CITY_ID!="spb")
    {
        $r = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arNewsIB["ID"],"CODE"=>"letnie_verandy"));
        if ($arL = $r->Fetch())
        {
            $sectL = $arL["ID"];
        }
        $res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("!SECTION_ID"=>$sectL,"PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["NEWS_COUNT"] = $res->SelectedRowsCount();
        if ($sectL)
        {
            $res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("SECTION_ID"=>$sectL,"PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
            $arResult["LETN_COUNT"] = $res->SelectedRowsCount();        
        }
    }
    else
    {
        $res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["NEWS_COUNT"] = $res->SelectedRowsCount();
    }
    
    $r = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arNewsIB["ID"],"CODE"=>"oktoberfest"));
    if ($arL = $r->Fetch())
    {
        $sectL = $arL["ID"];        
    }
    if ($sectL)
    {
        $res = CIBlockElement::GetList(Array(),Array("SECTION_ID"=>$sectL,"PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
        if ($aCorp = $res->GetNext())
        {
            $arResult["OKT_COUNT"] = 1;
            $arResult["OKT_LINK"] = $aCorp["DETAIL_PAGE_URL"];
        }
    }
}
if (CITY_ID=="msk"||CITY_ID=="spb")
{
    $ar1 = getArIblock("special_projects", CITY_ID,"easter_");
    $ar2 = getArIblock("special_projects", CITY_ID,"8marta_");
    $ar3 = getArIblock("special_projects", CITY_ID,"valentine_");
    $ar4 = getArIblock("special_projects", CITY_ID,"post_");
    $ar5 = getArIblock("special_projects", CITY_ID,"new_year_night_");
    $ar6 = getArIblock("special_projects", CITY_ID,"new_year_corp_");
    $ar7 = getArIblock("special_projects", CITY_ID,"letnie_verandy_");        
    $ar8 = getArIblock("special_projects", CITY_ID,"sport_");        
    $ar_spec = Array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],$ar6["ID"],$ar7["ID"],$ar8["ID"]);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
    $arResult["SPEC_COUNT"] = $res->SelectedRowsCount();

    $ar_spec = Array($ar5["ID"],$ar6["ID"]);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
    $arResult["NY_SPEC_COUNT"] = $res->SelectedRowsCount();

    $ar_spec = Array($ar3["ID"]);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
    $arResult["VALENTINE_SPEC_COUNT"] = $res->SelectedRowsCount();

//    $ar_spec = Array($ar4["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["POST_SPEC_COUNT"] = $res->SelectedRowsCount();

//    $ar_spec = Array($ar7["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["VERANDA_SPEC_COUNT"] = $res->SelectedRowsCount();
}

$res = array();
if (CITY_ID!="ast"&&CITY_ID!="alm"&&CITY_ID!="vrn"&&CITY_ID!="tln"&&CITY_ID!="krd"&&CITY_ID!="sch"&&CITY_ID!="kld"&&CITY_ID!="ufa")
{    
    $arVideonewsIB = getArIblock("videonews", CITY_ID);
    if ($arVideonewsIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arVideonewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["VIDEONEWS_COUNT"] = $res->SelectedRowsCount();        
    }
    $res = array();
    $arPhotoIB = getArIblock("photoreports", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["PHOTO_COUNT"] = $res->SelectedRowsCount();
    }
    
    $res = array(); 
    $arPhotoIB = getArIblock("overviews", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["OVERVIEWS_COUNT"] = $res->SelectedRowsCount();        
    }
    $res = array(); 
    $arPhotoIB = getArIblock("interview", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["INTERVIEW_COUNT"] = $res->SelectedRowsCount();        
    }
    $res = array(); 
    $arPhotoIB = getArIblock("blogs", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));        
        $arResult["BLOG_COUNT"] = $res->SelectedRowsCount();        
    }

    $arPhotoIB = getArIblock("videonews", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["VIDEO_NEWS_COUNT"] = $res->SelectedRowsCount();
    }

    if (SITE_ID=="s1")
    {
        $res = array(); 
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>145),false,Array("nTopCount"=>1));
        $arResult["MC_COUNT"] = $res->SelectedRowsCount();
                
        
        //if (CITY_ID=="msk"||CITY_ID=="spb"):
//            $res = array();
//            $arCorpIB = getArIblock("special_projects", CITY_ID,"letnie_verandy"."_");
//            if ($arCorpIB["ID"])
//            {
//                $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//                if ($aCorp = $res->GetNext())
//                {
//                    $arResult["VERANDA_COUNT"] = 1;
//                    $arResult["VERANDA_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//                }
//            }
//            $res = array();
//            $arCorpIB = getArIblock("special_projects", CITY_ID,"valentine"."_");
//            $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//            if ($aCorp = $res->GetNext())
//            {
//                $arResult["VALENTINE_COUNT"] = 1;
//                $arResult["VALENTINE_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//            }
//            $res = array();
//            $arCorpIB = getArIblock("special_projects", CITY_ID,"8marta"."_");
//            $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//            if ($aCorp = $res->GetNext())
//            {
//                $arResult["8MARTA_COUNT"] = 1;
//                $arResult["8MARTA_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//            }                  
//        $arCorpIB = getArIblock("special_projects", CITY_ID,"post"."_");
//        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//        if ($aCorp = $res->GetNext())
//        {
//            $arResult["POST_COUNT"] = 1;
//            $arResult["POST_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//        }
//        $arCorpIB = getArIblock("special_projects", CITY_ID,"easter"."_");
//        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//        if ($aCorp = $res->GetNext())
//        {
//            $arResult["EASTER_COUNT"] = 1;
//            $arResult["EASTER_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//        }
//        $res = array();
//        $arCorpIB = getArIblock("special_projects", CITY_ID,"sport"."_");
//        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arCorpIB["ID"]),false,Array("nTopCount"=>1));
//        if ($aCorp = $res->GetNext())
//        {
//            $res = CIBlock::GetByID($arCorpIB["ID"]);
//            if ($ar = $res->Fetch())            
//                $arResult["HOCKEY_PIC"] = $ar["PICTURE"];
//            $arResult["HOCKEY_COUNT"] = 1;            
//                
//            $arResult["HOCKEY_LINK"] = $aCorp["DETAIL_PAGE_URL"];
//        }            
//        if ($arResult["PROPERTIES"]["place_new_rest"]["VALUE"])
//        {               
//            $rrr = CIBlockElement::GetByID($arResult["PROPERTIES"]["place_new_rest"]["VALUE"]);
//            if ($rr = $rrr->GetNext())
//                $arResult["NEW_REST_PLACE"] = Array("NAME"=>$rr["NAME"],"URL"=>$rr["DETAIL_PAGE_URL"]);                
//        }
//        endif;
         $res = array();
            
    }
}
/*$res = array();
$arKuponIB = getArIblock("kupons", CITY_ID);
$res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","ACTIVE_DATE"=>"Y","IBLOCK_ID"=>$arKuponIB["ID"]),false,Array("nTopCount"=>1));
$arResult["KUPONS_COUNT"] = $res->SelectedRowsCount();*/

$res = array();
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arResult["AFISHA_IB"] = $arAfishaIB;
if ($arAfishaIB["ID"]) 
{
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y", "ACTIVE_DATE"=>"Y", ">=PROPERTY_EVENT_DATE"=>array(false, date('Y-m-d')),
        Array("LOGIC"=>"OR",
            Array(            
                "PROPERTY_d1"=>"false",
                "PROPERTY_d2"=>"false",
                "PROPERTY_d3"=>"false",
                "PROPERTY_d4"=>"false",
                "PROPERTY_d5"=>"false",
                "PROPERTY_d6"=>"false",
                "PROPERTY_d7"=>"false",
            ),
            Array(
                "!PROPERTY_d1"=>"false",
            ),
            Array(
                "!PROPERTY_d2"=>"false",
            ),
            Array(
                "!PROPERTY_d3"=>"false",
            ),
            Array(
                "!PROPERTY_d4"=>"false",
            ),
            Array(
                "!PROPERTY_d5"=>"false",
            ),
            Array(
                "!PROPERTY_d6"=>"false",
            ),
            Array(
                "!PROPERTY_d7"=>"false",
            ),
        ),
        "IBLOCK_ID"=>$arAfishaIB["ID"]),false,Array("nTopCount"=>1));
    $arResult["AFISHA_COUNT"] = $res->SelectedRowsCount();    
//    if ($USER->IsAdmin())
//        v_dump($arResult["AFISHA_COUNT"]);
}

//if ($arResult["PROPERTIES"]["user_bind"]["VALUE"])
//{
//    $res = array();
//    $arBlogsIB = getArIblock("blogs", CITY_ID);
//    $res = CIBlockElement::GetList(Array(),Array("CREATED_BY"=>$arResult["PROPERTIES"]["user_bind"]["VALUE"][0],"IBLOCK_ID"=>$arBlogsIB["ID"]));
//    $arResult["BLOGS_COUNT"] = $res->SelectedRowsCount();
//}

global $DB;
$sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a 
        JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=".$DB->ForSql($arResult["ID"])." ";
$db_list = $DB->Query($sql);
if($ar_result = $db_list->GetNext())
{
    if ($ar_result["ELEMENT_CNT"]>0)
        $arResult["MENU_ID"] = $ar_result['ID'];

    //  получение id фото раздела
    $db_list = CIBlockSection::getList(array(),array('IBLOCK_ID'=>$arResult["MENU_ID"], 'CODE'=>'foto'),false, array('ID'));
    if($ar_result = $db_list->Fetch()){
        $arResult['photoSectionId']=$ar_result['ID'];
    }
}
$p =0;

// get preview text
if ($arResult["MENU_ID"])
    $truncate_len = 200;
else
    $truncate_len = 100;
$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut($arResult["DETAIL_TEXT"], $truncate_len);
//$arResult["PREVIEW_TEXT"] = strip_tags($arResult["PREVIEW_TEXT"],"<p><a><ul><li><ol>");
$arResult["PREVIEW_TEXT_VK"] = $obParser->html_cut($arResult["DETAIL_TEXT"], 100);
 $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'medium',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark1.png', 
        ),
    );
foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
{
    $file = CFile::GetFileArray($photo);
    $arResult['MULTIPLE_PHOTO_ARR'][$key] = CFile::ResizeImageGet($file,Array("width"=>$file['WIDTH'],"height"=>$file['HEIGHT']),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
    if($file['WIDTH'] < $file['HEIGHT']){
        $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($photo,Array("width"=>728,"height"=>400),BX_RESIZE_IMAGE_PROPORTIONAL,true);
    }
    else {
        $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($photo,Array("width"=>728,"height"=>400),BX_RESIZE_IMAGE_EXACT,true);
    }
    $p++;
}

$res = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>101,"CODE"=>CITY_ID));
if ($ars = $res->GetNext())
{
    if($ars["DESCRIPTION"])
    {
        $ars["DESCRIPTION"] = explode("<i>",$ars["DESCRIPTION"]);
        $arResult["CONTACT_DESCRIPTION"] = $ars["DESCRIPTION"][0];

        }
}
//if ($USER->IsAdmin())
{    
    $new_rest_name = $arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"][0]." ";    
}
if ($arResult["SECTION"]["PATH"][0]["CODE"]=="restaurants"):
    $rest_name = GetMessage("R_REST")." ";    
else:
    $rest_name = GetMessage("R_BH")." ";
endif;
if ($arResult["ID"]=="1786885")
{
    $rest_name = GetMessage("R_CAFE")." ";
}
if (!$new_rest_name)
        $new_rest_name = $rest_name;
$arResult["H1"] = '<h1>'.$new_rest_name.$arResult["NAME"].'</h1>';
$APPLICATION;
$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]["SRC"];
        $cp->arResult['MENU_ID'] = $arResult["MENU_ID"];
        $cp->arResult['photoSectionId'] = $arResult['photoSectionId'];
	$cp->arResult['TEXT'] = strip_tags($arResult["PREVIEW_TEXT"]);
        $cp->arResult['H1'] = $arResult["H1"];
        $cp->arResult['NEWS_COUNT'] = $arResult["NEWS_COUNT"];
        $cp->arResult['MC_COUNT'] = $arResult["MC_COUNT"];
        $cp->arResult['AFISHA_COUNT'] = $arResult["AFISHA_COUNT"];
        $cp->arResult['SPEC_COUNT'] = $arResult["SPEC_COUNT"];
//    $cp->arResult['VALENTINE_SPEC_COUNT'] = $arResult["VALENTINE_SPEC_COUNT"];
        $cp->arResult['BLOG_COUNT'] = $arResult["BLOG_COUNT"];
        $cp->arResult['VIDEO_NEWS_COUNT'] = $arResult["VIDEO_NEWS_COUNT"];
        $cp->arResult['PHOTO_COUNT'] = $arResult["PHOTO_COUNT"];
        $cp->arResult['OVERVIEWS_COUNT'] = $arResult["OVERVIEWS_COUNT"];
        $cp->arResult['INTERVIEW_COUNT'] = $arResult["INTERVIEW_COUNT"];
        $cp->arResult['RATIO'] = sprintf("%01.2f", $arResult["PROPERTIES"]["RATIO"]["VALUE"]);
        $cp->arResult['SIMILAR'] = $arResult["PROPERTIES"]["similar_rest"]["VALUE"];
        $cp->arResult['RGROUP'] = Array("ID" => $arResult["PROPERTIES"]["rest_group"]["VALUE"],"NAME"=>  strip_tags($arResult["DISPLAY_PROPERTIES"]["rest_group"]["DISPLAY_VALUE"]));
        $cp->arResult['CONTACTS'] = count($arResult["PROPERTIES"]["map"]["VALUE"]);
        $cp->arResult['LAT'] = $arResult["PROPERTIES"]["lat"]["VALUE"][0];
        $cp->arResult['LON'] = $arResult["PROPERTIES"]["lon"]["VALUE"][0];
        $cp->arResult['DETAIL_TEXT'] = $arResult["DETAIL_TEXT"];
        $cp->arResult['PREVIEW_TEXT'] = $arResult["PREVIEW_TEXT"];
        $cp->arResult['WITHOUT_REVIEWS'] = $arResult["PROPERTIES"]["without_reviews"]["VALUE"];
        
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
        $cp->SetResultCacheKeys(array('IMAGE','TEXT','H1','RATIO','MENU_ID','photoSectionId','NEWS_COUNT','MC_COUNT','AFISHA_COUNT','SPEC_COUNT','OVERVIEWS_COUNT','INTERVIEW_COUNT','VIDEO_NEWS_COUNT','BLOG_COUNT','PHOTO_COUNT','SIMILAR','RGROUP','CONTACTS','LAT','LON',"DETAIL_TEXT","PREVIEW_TEXT","WITHOUT_REVIEWS"));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
	if (!isset($arResult['IMAGE']))
	{
		$arResult['IMAGE'] = $cp->arResult['IMAGE'];
		$arResult['TEXT'] = $cp->arResult['TEXT'];
	}
        if (!isset($arResult['H1']))
            $arResult['H1'] = $cp->arResult['H1'];
        if (!isset($arResult['RATIO']))
            $arResult['RATIO'] = $cp->arResult['RATIO'];
        if (!isset($arResult['MENU_ID']))
            $arResult['MENU_ID'] = $cp->arResult['MENU_ID'];
        if (!isset($arResult['photoSectionId']))
                $arResult['photoSectionId'] = $cp->arResult['photoSectionId'];
        if (!isset($arResult['NEWS_COUNT']))
            $arResult['NEWS_COUNT'] = $cp->arResult['NEWS_COUNT'];
        if (!isset($arResult['MC_COUNT']))
            $arResult['MC_COUNT'] = $cp->arResult['MC_COUNT'];
        if (!isset($arResult['AFISHA_COUNT']))
            $arResult['AFISHA_COUNT'] = $cp->arResult['AFISHA_COUNT'];
        if (!isset($arResult['SPEC_COUNT']))
            $arResult['SPEC_COUNT'] = $cp->arResult['SPEC_COUNT'];
        if (!isset($arResult['BLOG_COUNT']))
            $arResult['BLOG_COUNT'] = $cp->arResult['BLOG_COUNT'];
        if (!isset($arResult['VIDEO_NEWS_COUNT']))
            $arResult['VIDEO_NEWS_COUNT'] = $cp->arResult['VIDEO_NEWS_COUNT'];
        if (!isset($arResult['OVERVIEWS_COUNT']))
            $arResult['OVERVIEWS_COUNT'] = $cp->arResult['OVERVIEWS_COUNT'];
        if (!isset($arResult['INTERVIEW_COUNT']))
            $arResult['INTERVIEW_COUNT'] = $cp->arResult['INTERVIEW_COUNT'];
        if (!isset($arResult['PHOTO_COUNT']))
            $arResult['PHOTO_COUNT'] = $cp->arResult['PHOTO_COUNT'];
        if (!isset($arResult['SIMILAR']))
            $arResult['SIMILAR'] = $cp->arResult['SIMILAR'];        
        if (!isset($arResult['RGROUP']))
            $arResult['RGROUP'] = $cp->arResult['RGROUP'];        
        if (!isset($arResult['CONTACTS']))
            $arResult['CONTACTS'] = $cp->arResult['CONTACTS'];        
        if (!isset($arResult['LAT']))
            $arResult['LAT'] = $cp->arResult['LAT'];        
        if (!isset($arResult['LON']))
            $arResult['LON'] = $cp->arResult['LON'];        
        if (!isset($arResult['DETAIL_TEXT']))
            $arResult['DETAIL_TEXT'] = $cp->arResult['DETAIL_TEXT'];        
        if (!isset($arResult['PREVIEW_TEXT']))
            $arResult['PREVIEW_TEXT'] = $cp->arResult['PREVIEW_TEXT'];        
        if (!isset($arResult['WITHOUT_REVIEWS']))
            $arResult['WITHOUT_REVIEWS'] = $cp->arResult['WITHOUT_REVIEWS'];        
}
if ($arResult["PROPERTIES"]["d_tours"]["VALUE"][0])
{    
    $arTourIB = getArIblock("system_lookon", CITY_ID);
    foreach($arResult["PROPERTIES"]["d_tours"]["VALUE"] as $key=>$dtour_id)
    {        
        $rrr = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arTourIB["ID"],"ID"=>$dtour_id,"ACTIVE"=>"Y"),false,false,Array("ID","NAME","PREVIEW_PICTURE","DETAIL_PICTURE","PROPERTY_tour","PROPERTY_widgetPath"));
        if ($rr = $rrr->Fetch())
        {
            $lookon_tour = str_replace("index.html", "start.swf", $rr["PROPERTY_WIDGETPATH_VALUE"]);
            $lookon_xml = str_replace("index.html", "start.xml", $rr["PROPERTY_WIDGETPATH_VALUE"]); 
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key] = $lookon_tour;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][$key] = $lookon_xml;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][$key] = $rr["PROPERTY_TOUR_VALUE"];
            $arResult["PROPERTIES"]["d_tour"]["VALUE"][$key] = $rr["PREVIEW_PICTURE"];
        }
    }    
}
?>