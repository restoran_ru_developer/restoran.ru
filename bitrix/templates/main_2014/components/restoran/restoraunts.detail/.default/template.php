<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($_REQUEST["CONTEXT"]=="Y"):?>
    <script>
        $(document).ready(function(){
            $('html,body').animate({scrollTop:"290px"},500);
        });
    </script>
<?endif;?>
    <script>
        var map_load1 = 0;
        $(window).load(function () {
            //if (!$.isEmptyObject($("#d3dload")))
            {
                $("#d3dload").click(function () {
                    $("#rest3d-<?=$arResult["ID"]?> iframe").attr("src", $("#rest3d-<?=$arResult["ID"]?> iframe").data("src"));
                });
            }
            $("#map_load").click(function () {
                if (!map_load1) {
                    {
                        setTimeout("OnPageLoad()", 100);
                        map_load1++;
                    }
                }
            });
            //minus_plus_buts
            $("#minus_plus_buts").on("click", "a.plus", function (event) {
                var obj = $(this);
                $.post("/tpl/ajax/plus_minus.php", {ID: <?=$arResult["ID"]?>, act: "plus"}, function (otvet) {
                    if (parseInt(otvet)) {
                        $(".all").html($(".all").html() * 1 + 1);
                        obj.html(otvet);
                    }
                    else {
                        $('#minus_plus_buts .plus').tooltip({
                            'title': otvet,
                            'trigger': "manual",
                            'placement': 'left'
                        });
                        $('#minus_plus_buts .plus').tooltip("show");
                        setTimeout("$('#minus_plus_buts .plus').tooltip('hide')", 3000);
                    }
                });
                return false;
            });

            $("#minus_plus_buts").on("click", "a.minus", function (event) {
                var obj = $(this);
                $.post("/tpl/ajax/plus_minus.php", {ID: <?=$arResult["ID"]?>, act: "minus"}, function (otvet) {
                    if (parseInt(otvet)) {
                        $(".all").html($(".all").html() * 1 + 1);
                        obj.html(otvet);
                    } else {
                        $('#minus_plus_buts .minus').tooltip({
                            'title': otvet,
                            'trigger': "manual",
                            'placement': 'right'
                        });
                        $('#minus_plus_buts .minus').tooltip("show");
                        setTimeout("$('#minus_plus_buts .plus').tooltip('hide')", 3000);
                    }
                });
                return false;
            });

            setTimeout(asd, 300);
            function asd() {
                $.post("/tpl/ajax/plus_minus.php", {ID: <?=$arResult["ID"]?>, act: "generate_buts"}, function (otvet) {
                    $("#minus_plus_buts").html(otvet);
                });
            }
        });
    </script>
<?
if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="rga"||CITY_ID=="urm"||CITY_ID=="nsk"):
    if ($arResult["PROPERTIES"]["place_new_rest"]["VALUE"])
        echo "<div class='new_rest_place'>".GetMessage('detail_restaurant_on_him_place')." <a href=".$arResult["NEW_REST_PLACE"]["URL"].">".$arResult["NEW_REST_PLACE"]["NAME"]."</a></div>";
    include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/bron_form_new.php");
endif;
?>

<?if($arResult['VALENTINE_SPEC_COUNT']):?>
    <div class="valentine-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>
    <div class="clearfix"></div>
<?endif?>
<?//if($arResult['NY_SPEC_COUNT']):?>
<!--    <div class="new-year-event-slide-trigger" href="#podborki" data-toggle="anchor"><span>--><?//=GetMessage('new_year_event');?><!--</span> 2017</div>-->
<?//endif?>

<?//if($arResult['POST_SPEC_COUNT']):?>
<!--    <div class="post-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>-->
<!--    <div class="clearfix"></div>-->
<?//endif?>

    <div class="sort anchors-list">
        <a href="#photos_tab" data-toggle='detail_tabs' class="asc"><?=GetMessage('detail_restaurant_photo_gallery');?></a>
        <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]):?>
            <a href="#video" data-toggle='detail_tabs'><?=GetMessage('detail_restaurant_video_panorama');?></a>
        <?endif;?>
        <?if ($arResult["PROPERTIES"]["d_tours"]["VALUE"]):?>
            <a href="#d3d_tab" id="d3dload" data-toggle='detail_tabs'><span class="i3d"></span><?=GetMessage('detail_restaurant_3d_tour');?></a>
        <?endif;?>
        <a href="#props" data-toggle='anchor'><?=GetMessage('detail_restaurant_contacts');?></a>
        <?if($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0]):?>
            <a href="#map_tab" id="map_load" data-toggle='detail_tabs'><?=GetMessage('detail_restaurant_on_map');?></a>
        <?endif;?>
        <?if ($arResult["MENU_ID"]):?>
            <a href="<?=$APPLICATION->GetCurDir()."menu/"?>" target='_blank'><?=GetMessage('detail_restaurant_menu');?></a>
        <?endif;?>
        <a href="#reviews" data-toggle='anchor'><?=GetMessage('detail_restaurant_reviews');?></a>
        <?if ($arResult["NEWS_COUNT"]):?>
            <a href="#news" data-toggle='anchor'><?=GetMessage('detail_restaurant_news');?></a>
        <?endif;?>
        <?if ($arResult["AFISHA_COUNT"]):?>
            <a href="#afisha" data-toggle='anchor'><?=GetMessage('detail_restaurant_poster');?></a>
        <?endif;?>
        <?if ($arResult["SPEC_COUNT"]&&!$arResult['NY_SPEC_COUNT']):?>
            <a href="#podborki" data-toggle='anchor'><?=GetMessage('detail_restaurant_events');?></a>
        <?endif;?>
<!--        --><?//if($arResult['NY_SPEC_COUNT']):?>
<!--            <div class="new-year-event-slide-trigger" href="#podborki" data-toggle="anchor"><span>--><?//=GetMessage('new_year_event');?><!--</span> 2016</div>-->
<!--        --><?//endif?>
    </div>
    <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="favorite" data-toggle="favorite" data-restoran='<?=$arResult["ID"]?>'><?=GetMessage('detail_restaurant_to_favorite');?></a>
    <div class="clearfix"></div>
    <div class='detail_tabs' id='photos_tab'>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/photos.php");?>
    </div>
    <div class='detail_tabs' id='video'>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/video.php");?>
    </div>
    <div class='detail_tabs' id='d3d_tab'>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/3d.php");?>
    </div>
    <div class='detail_tabs' id='map_tab'>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/map.php");?>
    </div>
    <div class="article_likes">
        <div class="pull-left likes-buttons" id="minus_plus_buts">

        </div>
        <div class="pull-left review-button">
            <a href="#reviews" data-toggle="anchor" class="btn btn-info btn-nb-empty"><?=GetMessage('detail_restaurant_leave_review');?></a>
        </div>
        <div class="pull-right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "https://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
        </div>
    </div>
<?
include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/contacts.php");
?>
<?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/props.php");?>
<?if ($arResult["DETAIL_TEXT"]):?>
    <div class="description">
        <?=$arResult["DETAIL_TEXT"].' <a href="'.$APPLICATION->GetCurDir().'photos/">Фотографии ресторана '.$arResult['NAME'].'</a>'?>
        <!--<span id='t' class="hid" ><?//=$arResult["PREVIEW_TEXT"]?></span>
        <span id='t2' class="hid" style="display:none"><?//=$arResult["DETAIL_TEXT"]?></span>    -->
    </div>
    <p id="tr">...</p>
    <div class="text-right">
        <a class="btn btn-light " onclick="$('.description').addClass('open_d'); $('#tr').hide(); $(this).hide().next().show()"><?=GetMessage("R_MORE_TEXT_1")?></a>
        <a class="btn btn-light " onclick="$('.description').removeClass('open_d'); $('#tr').show(); $(this).hide().prev().show()" style="display: none"><?=GetMessage("R_MORE_TEXT_2")?></a>
    </div>
    <br />
<?endif;?>