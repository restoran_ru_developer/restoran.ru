<div class="order-panel" >

    <?if(count($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])<2):?>
        <?if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="nsk"):?>
            <?if($_REQUEST["CATALOG_ID"] != "banket"):?>
                <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <?else:?>
                <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
            <?endif;?>
        <?endif;?>
        <?if (CITY_ID=="rga"||CITY_ID=="urm"):?>
            <?if($_REQUEST["CATALOG_ID"] != "banket"):?>
                <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
            <?else:?>
                <a href="/tpl/ajax/online_order_rest_rgaurm.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
            <?endif;?>
        <?endif;?>
    <?endif;?>


    <div class="order-text">
        <?if($_REQUEST["CONTEXT"]=="Y"||$_REQUEST['RESTOURANT']=='imperator'):?>
            <? if (LANGUAGE_ID == "ru"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Бронировать бесплатно:</span><br />
                    </a>

                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (495) 988 26 56</a></div>
                    <?else:?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (812) 740-18-20</a></div>
                    <?endif;?>
                <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Бронировать бесплатно:</span><br />
                    </a>
                    <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+371 661 031 06</a></div>
                <?elseif(CITY_ID!="nsk"):?>
                    <?= $arResult["CONTACT_DESCRIPTION"] ?>
                <?endif;?>
            <? endif; ?>
            <? if (LANGUAGE_ID == "en"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Book for free<!-- - we speak English-->.</span><br />
                    </a>
                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (495) 988 26 56</a></div>
                    <?else:?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (812) 740-18-20</a></div>
                    <?endif;?>
                <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Book for free</span><br />
                    </a>
                    <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+371 661 031 06</a></div>
                <?else:?>
                    <? echo $arResult["CONTACT_DESCRIPTION"]; ?>
                <? endif; ?>
            <?endif;?>
        <?else:?>
            <?
            if($arResult['SHOW_REST_PHONE']==true || !preg_match('/banquet-service/',$_SERVER['HTTP_REFERER'])){
                if(is_array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                    if(preg_match('/,/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0])){
                        $phone_str_arr = explode(',',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                        $phone_str = $phone_str_arr[0];
                    }
                    elseif(preg_match('/;/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0])){
                        $phone_str_arr = explode(';',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                        $phone_str = $phone_str_arr[0];
                    }
                    else {
                        $phone_str = trim(preg_replace('/[^-+0-9() ]+/i','',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]));
                    }
                }
                elseif($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]) {
                    if(preg_match('/,/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                        $phone_str_arr = explode(',',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                        $phone_str = $phone_str_arr[0];
                    }
                    elseif(preg_match('/;/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                        $phone_str_arr = explode(';',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                        $phone_str = $phone_str_arr[0];
                    }
                    else {
                        $phone_str = trim(preg_replace('/[^-+0-9() ]+/i','',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]));
                    }
                }
            }
            else {
                if (CITY_ID=="spb"):
                    $phone_str = "+7 (812) 740-18-20";
                elseif(CITY_ID == "rga" || CITY_ID == "urm"):
                    $phone_str = "+371 661 031 06";
                else:
                    $phone_str = "+7 (495) 988-26-56";
                endif;
            }



            ?>
            <? if (LANGUAGE_ID == "ru"): ?>
                <?if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="nsk"):?>
                    <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Бронировать бесплатно:</span><br />
                    </a>
                    <?if($phone_str):?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                    <?else:?>
                        <?= $arResult["CONTACT_DESCRIPTION"] ?>
                    <?endif?>
                <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Бронировать бесплатно:</span><br />
                    </a>
                    <?if($phone_str):?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                    <?else:?>
                        <?= $arResult["CONTACT_DESCRIPTION"] ?>
                    <?endif?>
                <?else:?>
                    <?if($phone_str):?>
                        <div class="phone"><a href="tel:+<?=$phone_str?>"><?=$phone_str?></a></div>
                    <?else:?>
                        <?= $arResult["CONTACT_DESCRIPTION"] ?>
                    <?endif?>
                <?endif?>

            <? endif; ?>
            <? if (LANGUAGE_ID == "en"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Book for free<!-- - we speak English-->.</span><br />
                    </a>
                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                    <?else:?>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                    <?endif;?>
                <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                        <span class="another">Book for free</span><br />
                    </a>
                    <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                <?else:?>
                    <? echo $arResult["CONTACT_DESCRIPTION"]; ?>
                <? endif; ?>
            <?endif;?>
        <?endif?>
    </div>
</div>
<?/*if (CITY_ID=="msk"||CITY_ID=="spb"):?>
<noindex>
    <div class="order-panel" id="order_new_overflow">
        <div class="otitle pull-left"><?=$arResult["NAME"]?></div>
        <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=$arResult["NAME"]?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
        <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=$arResult["NAME"]?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
        <div class="order-text">
            <? if (LANGUAGE_ID == "ru"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <span class="another">Бронировать по телефону:</span><br />
                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                    <?else:?>
                        <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                    <?endif;?>
                <?else:?>
                    <?= $arResult["CONTACT_DESCRIPTION"] ?>                      
                <?endif;?>
            <? endif; ?>        
            <? if (LANGUAGE_ID == "en"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <span class="another">Make restaurant reservations - we speak English.</span><br />
                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                    <?else:?>
                        <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                    <?endif;?>        
                <?else:?>
                    <? echo $arResult["CONTACT_DESCRIPTION"]; ?> 
                <? endif; ?>
            <?endif;?>
        </div>
    </div>
</noindex>

<script>
    $(document).ready(function(){
        $(window).scroll(function(){            
            if (($(this).scrollTop()>$("#minus_plus_buts").offset().top-100)&&$("#order_new_overflow").css("opacity")=="0")
            {
                $("#order_new_overflow").css({"top":"0px","opacity":1});
            }
            if ($(this).scrollTop()<$("#minus_plus_buts").offset().top-100&&$("#order_new_overflow").css("opacity")=="1")
            {
                $("#order_new_overflow").css({"top":"-80px","opacity":0});
            }
        });
    });
</script>
<?endif;*/?>