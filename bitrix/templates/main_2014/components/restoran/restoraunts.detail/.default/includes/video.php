<?
$param_sharpen = 30;
$arFilter = array("name" => "sharpen", "precision" => $param_sharpen);  //  можно просто false
foreach($arResult["PROPERTIES"]["videopanoramy"]["VALUE"] as $item_key=>$item){
    $arFile = CFile::GetFileArray($item);

    $arFileTmp = CFile::ResizeImageGet(
        $arFile,
        array("width" => $arFile['WIDTH'], "height" => 400),
        BX_RESIZE_IMAGE_EXACT,
        true, $arFilter
    );
    $arResult["VIDEOPANORAMA"][$item_key] = array(
        "SRC" => $arFileTmp["src"],
        'WIDTH' => $arFileTmp["width"],
        'DESCRIPTION' => $arFile['DESCRIPTION']
    );
}
?>
<div id="restvideo-<?=$arResult["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval='false'>

    <script>
        $(function(){
            <?if(!(count($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]) > 1)){?>
                slider_phone="<?=GetMessage('detail_restaurant_photo_rest_phone')?><br><strong><?=$TELs;?></strong>";
                $('#restvideo-<?=$arResult["ID"]?>').append('<span class="photo-phone-wrapper">'+slider_phone+'</span>');
            <?}?>
            $('#restvideo-<?=$arResult["ID"]?>').append('<div class="photo_num">Видео <span>1</span> <?=GetMessage('detail_restaurant_photo_from')?> <?=(!count($arResult["VIDEOPANORAMA"]))?"1":count($arResult["VIDEOPANORAMA"])?></div>');

            $('#restvideo-<?=$arResult["ID"]?>').on('slid.bs.carousel', function () {
                $('#restvideo-<?=$arResult["ID"]?> .photo_num>span').text(parseInt($('#restvideo-<?=$arResult["ID"]?> .carousel-inner .item.active').attr('data-num'))+1);
            })
        });
    </script>

        <div class="carousel-inner">
            <?foreach($arResult["VIDEOPANORAMA"] as $key=>$photo):?>
                <?if ($key==0):?>

                    <div class="item active" data-num="<?=$key?>">
                        <div class="myMarquee">
                            <?
//                            $r = CFile::GetByID($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]);
//                            if ($a = $r->Fetch())
//                            {
//                                $a = $a;
//                            }
                            ?>
                            <div class="scroller-wrapper" style="width: <?=($photo["WIDTH"]*2)?>px;">
                                <div class="scroller <?=(!$photo['DESCRIPTION'])?"":"left-right"?>">
                                    <img src="<?=$photo['SRC']?>" height="400">
                                    <img src="<?=$photo['SRC']?>" height="400">
                                </div>
                            </div>
                        </div>
<!--                        <div class="photo_num">-->
<!--                            Видео <span>--><?//=$key+1?><!--</span> из --><?//=count($arResult["VIDEOPANORAMA"])?>
<!--                        </div>-->
                    </div>
                <?else:?>
                    <div class="item" data-num="<?=$key?>">
                        <div class="myMarquee">

                            <div class="scroller-wrapper" style="width: <?=($photo["WIDTH"]*2)?>px;">
                                <div class="scroller <?=(!$photo['DESCRIPTION'])?"":"left-right"?>">
                                    <img src="<?=$photo['SRC']?>" height="400">
                                    <img src="<?=$photo['SRC']?>" height="400">
                                </div>
                            </div>
                        </div>
<!--                        <div class="photo_num">-->
<!--                            Видео <span>--><?//=($key+1)?><!--</span> из --><?//=count($arResult["VIDEOPANORAMA"])?>
<!--                        </div>-->
                    </div>
                <?endif;?>
            <?endforeach;?>                          
        </div>
        <?if (count($arResult["PROPERTIES"]["videopanoramy"]["VALUE"])>1):?>
            <a class="left carousel-control" href="#restvideo-<?=$arResult["ID"]?>" role="button" data-slide="prev">
              <span class="galery-control galery-control-left icon-arrow-left2"></span>
            </a>
            <a class="right carousel-control" href="#restvideo-<?=$arResult["ID"]?>" role="button" data-slide="next">
              <span class="galery-control galery-control-right icon-arrow-right2"></span>
            </a>
        <?endif;?>
    </div>
<br />
    <?/*if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?> 
        <div class="thumbnail_row">
            <div><b>Фото:</b>
                <Br/> 
                1 из <?=count($arResult["PROPERTIES"]["photos"]["VALUE2"])?>
            </div>
            <?foreach($arResult["PROPERTIES"]["photos"]["VALUE_SMALL"] as $key=>$photo):?>
                <div class="th <?=($key==0)?"active":""?>" data-num="<?=$key?>"><img src="<?=$photo["src"]?>" alt="..."></div>
            <?endforeach;?>    
        </div>
    <?endif;*/?>    
    <script>
    $(function(){
//        $('#rest-<?=$arResult["ID"]?>').on('slide.bs.carousel', function (e) {    
//            //console.log(e);        
//            $(".thumbnail_row div.active").removeClass("active");
//            $(".thumbnail_row .th").eq($(e.relatedTarget).data("num")).addClass("active");               
//
//        }); 
//        $(".thumbnail_row .th").click(function(){
//            var a =$(this).data("num")*1;                        
//            $('#rest-<?=$arResult["ID"]?> .item').eq(a).find("img").attr('src', $('#rest-<?=$arResult["ID"]?> .item').eq(a).find("img").data('url'));
//            $('#rest-<?=$arResult["ID"]?>').carousel(a);
//        });
        //show/hide big text    
        $('a.toglletext').click(function(){
            $(".description .hid").toggle();
        });
    });
    </script>