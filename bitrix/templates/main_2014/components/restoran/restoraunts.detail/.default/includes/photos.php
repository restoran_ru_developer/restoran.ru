
<div id="rest-<?=$arResult["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval='7000'>
    <?
    if (!empty($arResult['PROPERTIES']['phone']['VALUE'][0]) && !preg_match('/banquet-service/',$_SERVER['HTTP_REFERER']) && $_REQUEST['CONTEXT']!='Y') {
        $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
        preg_match($reg, $arResult['PROPERTIES']['phone']['VALUE'][0], $matches);

        $TELs = $matches[0];
    }
    else {
        if (CITY_ID=="spb"):
            $TELs = "(812) 740-18-20";
        elseif(CITY_ID=="rga" || CITY_ID=="urm"):
            $TELs = "+371 661 031 06";
        else:
            $TELs = "(495) 988-26-56";
        endif;
    }?>
    <script>
        $(function(){
            <?if(!(count($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]) > 1)){?>
                slider_phone="<?=GetMessage('detail_restaurant_photo_rest_phone')?><br><strong><?=$TELs;?></strong>";
                $('#rest-<?=$arResult["ID"]?>').append('<span class="photo-phone-wrapper">'+slider_phone+'</span>');
            <?}?>
            $('#rest-<?=$arResult["ID"]?>').append('<div class="photo_num"><?=GetMessage('detail_restaurant_photo_photo')?> <span>1</span> <?=GetMessage('detail_restaurant_photo_from')?> <?=(!count($arResult["PROPERTIES"]["photos"]["VALUE2"]))?"1":count($arResult["PROPERTIES"]["photos"]["VALUE2"])?></div>');

            $('#rest-<?=$arResult["ID"]?>').on('slid.bs.carousel', function () {
                $('#rest-<?=$arResult["ID"]?> .photo_num>span').text(parseInt($('.carousel-inner .item.active').attr('data-num'))+1);
            })
        });
    </script>

    <div class="carousel-inner">

        <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>
            <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>

                <div class="item <?if($key==0)echo 'active';?>"  data-num="<?=$key?>">
                    <div class="inline-wrapper">
                        <a href="<?=$arResult["MULTIPLE_PHOTO_ARR"][$key]['src']?>" class="fancy" rel="detail_group" title="<?=GetMessage('R_REST') . " " . $arResult['NAME'] . ($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key] ? ", " . $arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key] : " - фотография №" . ($key + 1))?>" >
                            <img <? if ($key == 0): ?>src="<?= $photo["src"] ?>"<? endif; ?>
                                 data-url="<?= $photo['src'] ?>"
                                 title="<?=GetMessage('R_REST') . " " . $arResult['NAME'] . ($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key] ? ", " . $arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key] : " - фотография №" . ($key + 1))?>"
                                  alt="<?=GetMessage("R_REST") . " " . $arResult["NAME"] ?><?= ($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key] ? ", " . $arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key] : "") . " - фотография №" . ($key + 1) ?>">
                        </a>
                    </div>
                </div>
            <?endforeach;?>
        <?else:?>
            <?if ($arResult["PREVIEW_PICTURE"]["SRC"]):?>
                <div class="inline-wrapper">
                    <a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" class="fancy" rel="detail_group">
                        <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>"/>
                    </a>
                </div>
            <?else:?>
                <img src="/tpl/images/noname/rest_nnm_new_new.png" width="" />
            <?endif;?>
        <?endif;?>
    </div>
    <?if (count($arResult["PROPERTIES"]["photos"]["VALUE"])>1):?>
        <a class="left carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="prev">
            <span class="galery-control galery-control-left icon-arrow-left2"></span>
        </a>
        <a class="right carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="next">
            <span class="galery-control galery-control-right icon-arrow-right2"></span>
        </a>
    <?endif;?>

</div>
<br />

<script>
    $(function(){
        //show/hide big text    
        $('a.toglletext').click(function(){
            $(".description .hid").toggle();
        });
    });
</script>