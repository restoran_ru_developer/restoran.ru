<div class="order-panel" >
    <?if(!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
        <div class="order-text">
            <?if($_REQUEST["CONTEXT"]=="Y"||$_REQUEST['RESTOURANT']=='imperator'):?>
                <? if (LANGUAGE_ID == "ru"): ?>
                    <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                        <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Бронировать бесплатно:</span>
                        </a>

                        <?if (CITY_ID=="msk"):?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (495) 988 26 56</a></div>
                        <?else:?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (812) 740-18-20</a></div>
                        <?endif;?>
                    <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Бронировать бесплатно:</span>
                        </a>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+371 661 031 06</a></div>
                    <?elseif(CITY_ID!="nsk"):?>
                        <?= $arResult["CONTACT_DESCRIPTION"] ?>
                    <?endif;?>
                <? endif; ?>
                <? if (LANGUAGE_ID == "en"): ?>
                    <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                        <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Book for free<!-- - we speak English-->.</span>
                        </a>
                        <?if (CITY_ID=="msk"):?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (495) 988 26 56</a></div>
                        <?else:?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+7 (812) 740-18-20</a></div>
                        <?endif;?>
                    <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Book for free</span><br />
                        </a>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>+371 661 031 06</a></div>
                    <?else:?>
                        <? echo $arResult["CONTACT_DESCRIPTION"]; ?>
                    <? endif; ?>
                <?endif;?>
            <?else:?>
                <?
                if($arResult['SHOW_REST_PHONE']==true || !preg_match('/banquet-service/',$_SERVER['HTTP_REFERER'])){
                    if(is_array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                        if(preg_match('/,/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0])){
                            $phone_str_arr = explode(',',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                            $phone_str = clearUserPhone($phone_str_arr[0]);
                        }
                        elseif(preg_match('/;/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0])){
                            $phone_str_arr = explode(';',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                            $phone_str = clearUserPhone($phone_str_arr[0]);
                        }
                        else {
                            $phone_str = clearUserPhone($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                        }
                    }
                    elseif($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]) {
                        if(preg_match('/,/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                            $phone_str_arr = explode(',',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                            $phone_str = clearUserPhone($phone_str_arr[0]);
                        }
                        elseif(preg_match('/;/',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])){
                            $phone_str_arr = explode(';',$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                            $phone_str = clearUserPhone($phone_str_arr[0]);
                        }
                        else {
                            $phone_str = clearUserPhone($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                        }
                    }
                }
                else {
                    if (CITY_ID=="spb"):
                        $phone_str = "+7 (812) 740-18-20";
                    elseif(CITY_ID == "rga" || CITY_ID == "urm"):
                        $phone_str = "+371 661 031 06";
                    else:
                        $phone_str = "+7 (495) 988-26-56";
                    endif;
                }

                ?>
                <? if (LANGUAGE_ID == "ru"): ?>
                    <?if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="nsk"):?>
                        <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Бронировать бесплатно:</span>
                        </a>
                        <?if($phone_str):?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                        <?else:?>
                            <?= $arResult["CONTACT_DESCRIPTION"] ?>
                        <?endif?>
                    <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Бронировать бесплатно:</span>
                        </a>
                        <?if($phone_str):?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                        <?else:?>
                            <?= $arResult["CONTACT_DESCRIPTION"] ?>
                        <?endif?>
                    <?else:?>
                        <?if($phone_str):?>
                            <div class="phone"><a href="tel:+<?=$phone_str?>"><?=$phone_str?></a></div>
                        <?else:?>
                            <?= $arResult["CONTACT_DESCRIPTION"] ?>
                        <?endif?>
                    <?endif?>

                <? endif; ?>
                <? if (LANGUAGE_ID == "en"): ?>
                    <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                        <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Book for free<!-- - we speak English-->.</span>
                        </a>
                        <?if (CITY_ID=="msk"):?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                        <?else:?>
                            <div class="phone"><a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                        <?endif;?>
                    <?elseif (CITY_ID=="urm"||CITY_ID=="rga"):?>
                        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'>
                            <span class="another">Book for free</span>
                        </a>
                        <div class="phone"><a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" class="booking" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>'><?=$phone_str?></a></div>
                    <?else:?>
                        <? echo $arResult["CONTACT_DESCRIPTION"]; ?>
                    <? endif; ?>
                <?endif;?>
            <?endif?>
        </div>
    <?endif?>

    <?if ($arResult["DISPLAY_PROPERTIES"]["average_bill"]["VALUE"]):?>
    <div class="top-average-bill-wrapper">
        <div class="value">
            <a href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/srednij_schet/<?=$arResult["DISPLAY_PROPERTIES"]["average_bill"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["average_bill"]['VALUE'][0]]['CODE']?>/" style="color: #000000"><?=$arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"][0]?></a>
        </div>
        <div class="name"><?=GetMessage("R_PROPERTY_average_bill")?></div>
    </div>
    <?endif;?>

    <?if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']&&$arResult["PROPERTIES"]['kitchen']['VALUE']):?>
        <div class="top-average-bill-wrapper">
            <div class="value">
                <?if(count($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])>3):?>
                    <?$kitchen_str = strip_tags($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"][0].', '.$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"][1].', '.$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"][2])?>
                <?else:?>
                    <?$kitchen_str = strip_tags(implode(', ',$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?>
                <?endif?>

                <?=$kitchen_str?>
            </div>
            <div class="name"><?=GetMessage("R_PROPERTY_kitchens")?></div>
        </div>
    <?endif?>

    <?if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
        <div class="top-average-bill-wrapper">
            <div class="value blue-font-style">
                <a href="#carousel-contacts" data-toggle="anchor">
                <?=count($arResult["PROPERTIES"]['REST_NETWORK']['VALUE'])?> <?=(count($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']))<5?GetMessage("address_str_title"):GetMessage("few_address_str_title")?>
                </a>
            </div>
            <div class="name"><?=GetMessage("in_rest_network")?></div>
        </div>
    <?endif?>


    <?
    if(!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
        <?
        if ($arResult["DISPLAY_PROPERTIES"]["opening_hours_google"]["VALUE"]):?>
        <div class="top-average-bill-wrapper">
            <div class="value">
                <?

                if(is_array($arResult["DISPLAY_PROPERTIES"]["opening_hours_google"]["DISPLAY_VALUE"])){
                    $open_hours_arr = preg_replace('/ +/','',$arResult["DISPLAY_PROPERTIES"]["opening_hours_google"]["DISPLAY_VALUE"][0]);
                }
                else {
                    $open_hours_arr = preg_replace('/ +/','',$arResult["DISPLAY_PROPERTIES"]["opening_hours_google"]["DISPLAY_VALUE"]);
                }
                $days_arr = array(
                    'Sun',
                    'Mon',
                    'Tue',
                    'Wen',
                    'Thu',
                    'Fr',
                    'Sat'
                );

                foreach ($days_arr as $key=>$day_str) {
                    if(preg_match('/'.$day_str.'.+?[;|,|\/]|'.$day_str.'.+/', $open_hours_arr, $temp_hour_match_key)){
                        if(is_array($temp_hour_match_key)){
                            $time_arr[$key] = $temp_hour_match_key[0];
                            $pre_temp_hour_match_key = $temp_hour_match_key[0];
                        }
                        else {
                            $time_arr[$key] = $temp_hour_match_key;
                            $pre_temp_hour_match_key = $temp_hour_match_key;
                        }
                    }
                    else {
                        if($pre_temp_hour_match_key){
                            $time_arr[$key] = $pre_temp_hour_match_key;
                        }
                    }
                }
                FirePHP::getInstance()->info($open_hours_arr,'$open_hours_arr');
                FirePHP::getInstance()->info($time_arr,'$time_arr');
                $today = date('w');
                if(preg_match('/Everyday/', $open_hours_arr)){
                    $open_str = GetMessage('now_open_IN_DETAIL');
                    if(preg_match('/24hours/', $open_hours_arr, $hour_match)){
                        $hour_match = '24';
                    }
                    else {
                        preg_match('/[-|–](\d{2}:\d{2})/', $open_hours_arr, $hour_match);
                    }
                }
                elseif(!preg_match('/closed/', $time_arr[$today])&&$time_arr[$today]){
                    $open_str = GetMessage('now_open_IN_DETAIL');
                    if(preg_match('/24hours/', $time_arr[$today], $hour_match)){
                        $hour_match = '24';
                    }
                    else {
//                        FirePHP::getInstance()->info($time_arr[$today],'here');
                        preg_match('/[-|–](\d{2}:\d{2})/', $time_arr[$today], $hour_match);
                    }
                }
                elseif(preg_match('/closed/', $time_arr[$today])){//preg_match('/'.$days_arr[$today].'(closed)/', $open_hours_arr, $pre_hour_match)||($today>=$first_close_key&&$today<=$last_close_key)
                    $open_str = GetMessage('now_closed_IN_DETAIL');
                }
                if($open_str==GetMessage('now_open_IN_DETAIL')){?>
                    <?if($hour_match=='24'):?>
                        <?=GetMessage("24_hours")?>
                    <?else:?>
                        <?=GetMessage('open_up_IN_DETAIL')?> <?=$hour_match[1]?>
                    <?endif?>
                <?}?>
            </div>
            <div class="name"><?=$open_str?></div>
        </div>
        <?endif;?>

        <?$clear_city_arr = array(
            'msk'=>"/( +|)Москва[, ]+/i",
            'spb'=>"/( +|)Санкт-Петербург[, ]+/i",
            'rga'=>"/( +|)Рига[, ]+/i",
            'urm'=>"/( +|)Юрмала[, ]+/i",
            'kld'=>"/( +|)Калиниград[, ]+/i",
            'nsk'=>"/( +|)Новосибирск[, ]+/i",
            'sch'=>"/( +|)Сочи[, ]+/i"
        );


//        FirePHP::getInstance()->info($arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']);
//        FirePHP::getInstance()->info($arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE']);
        ?>

        <?if ($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]):?>
        <div class="top-average-bill-wrapper steet-top-wrapper">
            <div class="value">
                <?if($arResult['SHOW_YA_STREET']&&$arResult["DISPLAY_PROPERTIES"]["STREET"]["DISPLAY_VALUE"]):?>
                    <?if(!preg_match('/\/'.$arParams['TO_LINKS_CATALOG_ID'].'\//',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'])){
                        if(preg_match('/banket/',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'])){
                            $arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'] = preg_replace('/banket/','restaurants',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']);
                        }
                        else {
                            $arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'] = preg_replace('/restaurants/','banket',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']);
                        }
                    }?>
                    <a href="<?=$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']?>"><?=str_replace('г.','',is_array($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]))?></a><?//=$arResult["DISPLAY_PROPERTIES"]["STREET"]["DISPLAY_VALUE"]?>
                <?else:?>
                    <?=str_replace('г.','',is_array($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]))?>
                <?endif?>
            </div>
            <?if($arResult["DISPLAY_PROPERTIES"]["subway"]['VALUE']):?>
            <div class="name"><?//метро: ?>
                <div class="subway">M</div>
                <span><a href="/<?=CITY_ID?>/catalog/<?=$_REQUEST['CATALOG_ID']?>/metro/<?=$arResult["DISPLAY_PROPERTIES"]["subway"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["subway"]['VALUE'][0]]['CODE']?>/"><?=$arResult["DISPLAY_PROPERTIES"]["subway"]['DISPLAY_VALUE'][0]?></a></span></div>
            <?endif;?>
        </div>
        <?endif;?>
    <?endif?>
</div>