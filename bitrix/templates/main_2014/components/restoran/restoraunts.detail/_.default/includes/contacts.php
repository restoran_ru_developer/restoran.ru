<?if ($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
    <?if($arResult['REVIEWS_COUNTER']>5):?>
    <script type="text/javascript">
        $(function()
        {
            $('.network-restaurants-poster').jScrollPane({verticalGutter:15});
            $('.network-restaurants-poster').removeClass("active");
            $('.network-restaurants-poster').eq(0).addClass("active");
        });
    </script>
    <?endif?>
    <div class="tab-str-wrapper">
        <div class="nav-tab-str-title">
            <?=$arResult['ID']==2316325?'Рестораны и банкетные залы отеля':GetMessage("network_rest_address_title")?>
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane active">
            <div id="carousel-contacts" >
                <div class="carousel-inner">
                    <div class="item active network-restaurants-poster">
                        <?//FirePHP::getInstance()->info($arResult["PROPERTIES"]["subway"]);?>

                        <?foreach ($arResult["PROPERTIES"]['address']['VALUE'] as $key=>$adress):?>
                        <div class="contact props">
                            <div class="name"><a href="<?=$arResult['PROPERTIES']['REST_NETWORK']['URL'][$key]?>"><?=$arResult['PROPERTIES']['REST_NETWORK']['NETWORK_REST_NAME'][$key]?></a></div>
                            <?if($arResult["PROPERTIES"]["subway"]["VALUE"]):?>
                                <div class="prop wsubway">
                                    <div class="subway">M</div>
                                    <div class="value">
                                        <?//=(is_array($arResult["PROPERTIES"]["subway"]["VALUE"]) ? $arResult["PROPERTIES"]["subway"]["VALUE"][$key] : $arResult["PROPERTIES"]["subway"]["VALUE"])?>
                                        <?//FirePHP::getInstance()->info($arResult["DISPLAY_PROPERTIES"]["subway"]["VALUE"],'subway');?>
                                        <a href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/metro/<?=$arResult["PROPERTIES"]["subway"]["CODE_ELEMENT"][$key]?>/"><?=$arResult["PROPERTIES"]["subway"]["VALUE"][$key]?></a>
                                    </div><!--<a href="<?//=$arResult["DISPLAY_PROPERTIES"]["subway"]["LINK_ELEMENT_VALUE"][$arResult["DISPLAY_PROPERTIES"]["subway"]['VALUE'][$key]]['CODE']?>"></a>-->
                                </div>
                            <?endif;?>

                            <div class="prop street-address-in-contacts">
                                <div class="value"><a href="<?=$arResult['PROPERTIES']['REST_NETWORK']['URL'][$key]?>"><?=$adress?></a></div>
                            </div>
                            <div class="prop phones-in-contacts">
                                <div class="value">
                                    <?if ($_REQUEST["CONTEXT"]!="Y"):?>
                                        <?
                                        $arResult["DISPLAY_PROPERTIES"]["phone2"]["DISPLAY_VALUE2"][$key]=$arResult["PROPERTIES"]["phone"]["VALUE"][$key];
                                        $arResult["DISPLAY_PROPERTIES"]["phone2"]["DISPLAY_VALUE3"][$key] = explode(",", $arResult["PROPERTIES"]["phone"]["VALUE"][$key]);


                                        $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                        preg_match_all($reg, $arResult["DISPLAY_PROPERTIES"]["phone2"]["DISPLAY_VALUE2"][$key], $matches);

                                        $TELs=array();
                                        for($p=1;$p<5;$p++){
                                            foreach($matches[$p] as $key2=>$v){
                                                $TELs[$key2].=$v;
                                            }
                                        }

                                        foreach($TELs as $key2 => $T){
                                            if(strlen($T)>7)  $TELs[$key2]="+7".$T;
                                        }
                                        ?>
                                        <?
                                        foreach($TELs as $key2 => $T){
                                                ?>
                                            <?if(CITY_ID=='msk'||CITY_ID=='spb'):?>
                                                <a href="/tpl/ajax/online_order_rest.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>' class="mnum phone-prop-value booking <? echo MOBILE;?>"><?=clearUserPhone($arResult["DISPLAY_PROPERTIES"]["phone2"]["DISPLAY_VALUE3"][$key][$key2])?></a><?if(isset($TELs[$key2+1]) && $TELs[$key2+1]!="") echo ', ';?>
                                            <?elseif(CITY_ID=='rga'||CITY_ID=='urm'):?>
                                                <a href="/tpl/ajax/online_order_rest_rgaurm.php<?=$_REQUEST["CATALOG_ID"]=="banket"?'?banket=Y':''?>" data-id='<?=$arResult["ID"]?>' data-restoran='<?=  rawurlencode($arResult["NAME"])?>' class="mnum phone-prop-value booking <? echo MOBILE;?>"><?=$arResult["DISPLAY_PROPERTIES"]["phone2"]["DISPLAY_VALUE3"][$key][$key2]?></a><?if(isset($TELs[$key2+1]) && $TELs[$key2+1]!="") echo ', ';?>
                                            <?else:?>
                                                <a href="tel:<?=$T?>" class="mnum phone-prop-value <? echo MOBILE;?>"><?=$arResult["DISPLAY_PROPERTIES"]["phone2"]["DISPLAY_VALUE3"][$key][$key2]?></a><?if(isset($TELs[$key2+1]) && $TELs[$key2+1]!="") echo ', ';?>
                                            <?endif?>
                                        <?}?>
                                        </p>
                                    <?endif;?>
                                </div>
                            </div>

                            <?if ($arResult["PROPERTIES"]["map"]["VALUE"][$key]):?>
                                <div class="prop last">
                                    <p><a class="another" href="javascript:void(0)" onclick="showMeRest('<?=$arResult["PROPERTIES"]["map"]["VALUE"][$key]?>',<?=$key?>)"><?=GetMessage("SHOW_ON_MAP")?></a></p>
                                </div>
                            <?endif;?>
                        </div>
                    <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?endif;?>