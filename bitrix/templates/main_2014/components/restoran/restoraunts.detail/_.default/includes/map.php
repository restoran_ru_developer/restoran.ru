<?//TODO (/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/result_modifier.php строка 424) $arResult['PROPERTIES']['REST_NETWORK'] - ключи: VALUE - привязанные рестораны сети, NETWORK_REST_NAME, URL, address - $arResult['PROPERTIES']['address']['VALUE'], map - $arResult['PROPERTIES']['map']['VALUE'], phone - $arResult['PROPERTIES']['phone']['VALUE']?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fullscreen-0.4.2.min.js"></script>

<?

if ($_GET["a"] == "u") {

if (count($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"])>1) $mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0];
else $mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0];

?>
    <link href="/tpl/css/map.css" rel="stylesheet">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">


        var myMap,
        objectManager;

        function init() {
		var zoomer = 14;

        var coord = "<?=COption::GetOptionString("main",CITY_ID."_center")?>";
        var u_pos = coord.split(',');
        myMap = new ymaps.Map("map",
			{
				center: [u_pos[1], u_pos[0]],
				zoom: zoomer,
				controls: ["geolocationControl", "zoomControl", "rulerControl"],
				type: "yandex#map"

	        },
			{
				minZoom: 10,
				maxZoom: 18,
			}
		);


		objectManager = new ymaps.ObjectManager({
			clusterize: true,
		    clusterHasBalloon: true,
		    geoObjectOpenBalloonOnClick: true,
		});

		objectManager.objects.options.set({
        	iconLayout: 'default#image',
			iconImageHref: '/tpl/images/map/pin1-2.png',
			iconImageSize: [37, 47],
        	iconImageOffset: [-18, -46],
            hideIconOnBalloonOpen: false,
            openEmptyBalloon: false
    	});

		//objectManager.clusters.options.set({
		//	preset: 'islands#blueCircleIcon'
		//});

        objectManager.clusters.options.set({

            clusterIcons: [{
            href: '/tpl/images/map/pin1-4.png',
            size: [43, 43],
            offset: [-21, -21]

            }]
        });

		myMap.geoObjects.add(objectManager);


        objectManager.objects.events.add('click', function (e) {

            var objectId = e.get('objectId'),
            object = objectManager.objects.getById(objectId);
            var blnContent = object.properties.balloonContent;

            /* пробуем назначать ID из базы
            $("#blnTemp").html(blnContent);
            var restId = $("#blnTemp").children(".mbs").data("aid");
            console.log (restId);
            */

            restId = objectId;

            if (blnContent.length) {
                objectManager.objects.balloon.open(objectId);
                return;
            }

            $.ajax({
                method: "GET",
                dataType: "json",
                url: "/tpl/ajax/map_get_rest_info.php?ID="+restId
            }).done(function(data) {
                //console.log (data);
                $("#blnProto").children(".mb_r_title").children("a").attr("href", data.detail);
                $("#blnProto").children(".mb_r_title").children("a").html(data.type+" "+data.name);

                if (data.bill == null) data.bill = "нет информации";
                if (data.open == null) data.open = "нет информации";
                if (data.phone == null) data.phone = "нет информации";
                if (data.address == null) data.address = "нет информации";
                if (data.pic == null) data.pic = "/tpl/images/no_photo_map.png";

                $("#blnProto").children(".mb_r_body").children(".mb_r_pic").children("a").attr("href", data.detail);
                $("#blnProto").children(".mb_r_body").children(".mb_r_pic").children("a").children("img").attr("src", data.pic);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = bill]").children("span").html(data.bill);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = open]").children("span").html(data.open);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = phone]").children("span").html(data.phone);
                $("#blnProto").children(".mb_r_body").children(".mb_r_ank").children("[data-type = address]").children("span").html(data.address);


                if (restId != 2222385894) {
                    //console.log (object);
                    objectManager.objects.setObjectProperties( objectId, {balloonContent: $("#blnProto").html()} );
                    objectManager.objects.balloon.open(objectId);

                }
            });





		});

		objectManager.objects.events.add('mouseenter', function (e) {

            var objectId = e.get('objectId');
            objectManager.objects.setObjectOptions(objectId, {
                iconImageHref: '/tpl/images/map/pin1-3.png'
            });

		});

		objectManager.objects.events.add('mouseleave', function (e) {
            var objectId = e.get('objectId');
            objectManager.objects.setObjectOptions(objectId, {
                iconImageHref: '/tpl/images/map/pin1-2.png'
            });
		});

		myMap.events.add('boundschange', function () {
			//console.log (myMap.getZoom());
            if (myMap.getZoom() > 16) {
                objectManager.options.set({
                    clusterize: false
                });
            } else {
                objectManager.options.set({
                    clusterize: true
                });
            }
		});

		getRests();
    }

	//
    function OnPageLoad (){
        ymaps.ready(init);
    }



    function getRests () {

        <?

        $object_collection = array();
		$object_collection["type"] = "FeatureCollection";

        ?>
        <?if (count($arResult["PROPERTIES"]["map"]["VALUE"])>1):?>

        <?
        foreach ($arResult["PROPERTIES"]["map"]["VALUE"] as $key=>$coords):
            if(!$coords)
                continue;
            if ($key==0)
                $temp2 = explode(",",$coords);
            $temp = explode(",",$coords);
            if (!$first_coords[0]) {
                    $first_coords = $temp;
            }
            //print_r ($arResult);
            $object_collection["features"][] = array(type=>"Feature",id=>$arResult["ID"],geometry=>array(type=>"Point",coordinates=>array($temp[0], $temp[1])), properties=>array(balloonContent=>"",hintContent=>addslashes($arResult["NAME"])));

            ?>

        <?endforeach;?>
        <?endif;?>
        <?
            if (count($object_collection["features"])<1) {

                $coords_b = explode (",", $mmm);
                $first_coords = $coords_b;
                $object_collection["features"][] = array(type=>"Feature",id=>$arResult["ID"],geometry=>array(type=>"Point",coordinates=>array($coords_b[0], $coords_b[1])), properties=>array(balloonContent=>"",hintContent=>addslashes($arResult["NAME"])));
            }
            ?>

        var data = <? echo json_encode($object_collection); ?>


console.log (data);

        if (data) objectManager.add(data);
        myMap.setCenter ([<? echo $first_coords[0];?>, <? echo $first_coords[1];?>], 14);

	}

    </script>




<div id="map" style="width: 100%;margin: 0 auto;height: 400px;margin-bottom:15px"></div>
<div style="display:none;">
    <div id="blnProto">
        <div class="mb_r_title"><a target="_blank"></a></div>
        <div class="mb_r_body">
            <div class="mb_r_pic"><a target="_blank"><img width="137" height="90" /></a></div>
            <div class="mb_r_ank">
                <div data-type="bill">СРЕДНИЙ СЧЕТ:&nbsp;<span></span></div>
                <div data-type="open">ОТКРЫТ:&nbsp;<span></span></div>
                <div data-type="phone">ТЕЛЕФОН:&nbsp;<span></span></div>
                <div data-type="address">АДРЕС:&nbsp;<span></span></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>



<? } else { ?>

<script src="https://api-maps.yandex.ru/1.1/index.xml?loadByRequire=1&key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript" ></script><?//if(!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?><!--async --><?//endif?>
    <?if (count($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"])>1):?>
        <?$mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0];?>
    <?else:
        $mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0];
    endif;?>
    <script type="text/javascript">
        var map = null;
        var map_markers = {};
        var markers_data = {};
        var map_filter = {};
        var hidden_marker = null;
        var sat_map_type = false;
        var map_type = 'default';
        var to_show = null;
        var cur_user = 0;
        var my_style = {};
        var map_c = {};

        <?if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
        //            window.onload = OnPageLoad;
//        YMaps.jQuery(function () {
            OnPageLoad();
//        })
        <?endif?>
        function OnPageLoad (){
            GetMap();
        }
        function GetMap ()
        {
            YMaps.load ( function () {
                map = new YMaps.Map ( YMaps.jQuery ( "#map_area" )[0] );
                var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
                var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
                var typeControl = new YMaps.TypeControl ( [] );
                typeControl.addType ( MTmap );
                typeControl.addType ( MTsat );

                map.setMinZoom (8);
                map.addControl (typeControl);
                map.addControl(new YMaps.Zoom());
                YMaps.Events.observe ( map, map.Events.Update, function () {
                        ShowMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.TypeChange, function () {
                        hideMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                    ShowMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                    hideMyBalloon ();
                } );
                YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                    hideMyBalloon ();
                } );
                YMaps.Events.observe ( map, map.Events.Click, function () {
                    if(map.getZoom()<=16)
                        hideMyBalloon ();
                });

                var toolbar = new YMaps.ToolBar();
                // Создание кнопки-флажка
                var button = new YMaps.ToolBarToggleButton({
                    icon: "/tpl/images/map/icon-fullscreen.png",
                    hint: "Разворачивает карту на весь экран"
                });

                // Если кнопка активна, то карта разворачивается во весь экран
                YMaps.Events.observe(button, button.Events.Select, function () {
                    $('#map_area').fullscreen();
                });
                // Если кнопка неактивна, то карта принимает фиксированный размер
                YMaps.Events.observe(button, button.Events.Deselect, function () {
                    $.fullscreen.exit();
                });
                // Добавление кнопки на панель инструментов
                toolbar.add(button);
                // Добавление панели инструментов на карту
                map.addControl(toolbar);

                $(document).bind('fscreenchange', function(e, state, elem) {
                    if(!state){
                        button.deselect();
                    }
                });

                my_style = new YMaps.Style();
                my_style.iconStyle = new YMaps.IconStyle();
                my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
                my_style.iconStyle.size = new YMaps.Point(27, 32);
                my_style.iconStyle.offset = new YMaps.Point(-15, -32);

                <?if (count($arResult["PROPERTIES"]["map"]["VALUE"])>1):?>
                <?foreach ($arResult["PROPERTIES"]["map"]["VALUE"] as $key=>$coords):
                    if(!$coords)
                        continue;
                    if ($key==0)
                        $temp2 = explode(",",$coords);
                    $temp = explode(",",$coords);
                    ?>
                map_c[<?=$key?>] = new YMaps.Placemark ( new YMaps.GeoPoint ( <?=$temp[1]?>, <?=$temp[0]?> ), { style: my_style, hasBalloon: false, db_id: <?=$key?>,hideIcon: false } );
                map_c[<?=$key?>].id = <?=$key?>;
                var data = {};
                data.name = '<?= addslashes($arResult["NAME"])?>';
                data.adres = '<?=$arResult["PROPERTIES"]["address"]["VALUE"][$key]?>';
                data.url = '<?=$arResult['PROPERTIES']["NETWORK_REST"]["URL"][$key]?>';
                data.id = '<?=$key?>';
                markers_data[<?=$key?>] = data;
                <?$points[] = 'new YMaps.GeoPoint('.$temp[1].', '.$temp[0].')';?>
                <?endforeach;?>
                console.log(markers_data);

                <?endif;?>
                if (typeof ( map_c[0] ) == 'undefined')
                {
                    var coord = "<?=$mmm?>";
                    coord = coord.split(",");
                    SetMapCenter ( coord[0], coord[1], 15 );
                    map_markers[0] = new YMaps.Placemark ( new YMaps.GeoPoint ( coord[1], coord[0] ), { style: my_style, hasBalloon: false, db_id: 0,hideIcon: false } );
                    map.addOverlay ( map_markers[0] );
                    ShowMyBalloon(0);
                }
                else
                {
                    console.log(map_c);
                    SetMapCenter ( '<?=$temp2[0]?>', '<?=$temp2[1]?>', 13 );
                    for (var i=0;i<count_prs(map_c);i++)
                    {
                        map.addOverlay (map_c[i]);
                        YMaps.Events.observe ( map_c[i], map_c[i].Events.Click, function ( m, e ) {
                            hideMyBalloon();
                            ShowMyBalloon(m.id);
                        } );
                        //ShowMyBalloon(i);
                    }
                    //hideMyBalloon();
                }

                <?if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
                var points = [<?=implode(',',$points)?>],
                // Создаем область показа по группе точек
                    bounds = new YMaps.GeoCollectionBounds(points);
                // Применяем область показа к карте
                map.setBounds(bounds);
                <?endif?>
            })


        }

        function SetMapCenter ( lat, lng, zoom_i )
        {
            zoom = zoom_i ? zoom_i : 16;
            map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
        }

        function ShowMyBalloon (id)
        {                                    
            if (typeof ( map_c[0] ) == 'undefined')
            {
                hideMyBalloon ();
                <?if($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]):
                    $metro123 = '<p class="metro_'.CITY_ID.'" style="margin-bottom:0px;">'.(is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]) ? implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]) : $arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]).'</p>';
                endif;?>
                var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
                jQuery("#map_area").append('<div id="baloon'+id+'" class="map_ballon"><div class="balloon_content"><?=$rest_name.htmlspecialchars($arResult["NAME"],ENT_QUOTES)?><?=$metro123?><br /><?=htmlspecialchars($arResult["PROPERTIES"]["address"]["VALUE"][0],ENT_QUOTES)?><br />Телефон: <?=$arResult["PROPERTIES"]["phone"]["VALUE"][0]?><br /><?=($arResult["PROPERTIES"]["opening_hours"]["VALUE"][0])?"Часы работы: ".$arResult["PROPERTIES"]["opening_hours"]["VALUE"][0]:""?></div><div class="balloon_tail"> </div></div>');
                jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-10+"px");
                jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
            }
            else
            {
                hideMyBalloon ();                
                var point = map.converter.coordinatesToLocalPixels(map_c[id].getGeoPoint () );
                
                jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'><a href='"+markers_data[id].url+"' target='_blank'><?=$rest_name.$arResult["NAME"]?><br />"+markers_data[id].adres+"</div></a><div class='balloon_tail'> </div></div>");
                jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
                jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
            }
        }

        function hideMyBalloon ()
        {
            jQuery(".map_ballon").remove();
        }
        function showMeRest(point, id)
        {
            $("#map_load").click();
            $('html,body').animate({scrollTop:400},"300");
            if (!map_load1);
                OnPageLoad();
            point = point.split(",");     
            map.setZoom(15,{smooth:true,position:new YMaps.GeoPoint(point[1], point[0]),centering:true,callback:function(){
                    map.setZoom(16,{smooth:true,position:new YMaps.GeoPoint(point[1], point[0]),centering:true});
            }});

            ShowMyBalloon(id);
        }
        function count_prs(obj)
        {
           var count = 0; 
           for(var prs in obj) 
           { 
                count++;
           } 
           return count; 
        }
    </script>
    <script type="text/javascript">

    </script>
    <div id="map_area" style="width: 100%;margin: 0 auto;height: 400px;margin-bottom:15px"></div>


<? } ?>