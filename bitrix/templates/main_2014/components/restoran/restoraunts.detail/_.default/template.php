<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//FirePHP::getInstance()->info($arResult["PROPERTIES"]['map']);
?>

<div class="left-side" >
<?if ($_REQUEST["CONTEXT"]=="Y"):?>
    <script>
        $(document).ready(function(){
            $('html,body').animate({scrollTop:"290px"},500);
        });
    </script>
<?endif;?>
    <script>
        var map_load1 = 0;
        $(function(){
            $("#map_load").click(function () {
                if (!map_load1) {
                    {
                        setTimeout("OnPageLoad()", 100);
                        map_load1++;
                    }
                }
            });
        })
        $(window).load(function () {
            $("#d3dload").click(function () {
                $("#rest3d-<?=$arResult["ID"]?> iframe").attr("src", $("#rest3d-<?=$arResult["ID"]?> iframe").data("src"));
            });

            //minus_plus_buts
            $("#minus_plus_buts").on("click", "a.plus", function (event) {
                var obj = $(this);
                $.post("/tpl/ajax/plus_minus.php", {ID: <?=$arResult["ID"]?>, act: "plus"}, function (otvet) {
                    if (parseInt(otvet)) {
//                        $(".all").html($(".all").html() * 1 + 1);
                        obj.html(otvet);
                    }
                    else {
                        $('#minus_plus_buts .plus').tooltip({
                            'title': otvet,
                            'trigger': "manual",
                            'placement': 'left'
                        });
                        $('#minus_plus_buts .plus').tooltip("show");
                        setTimeout("$('#minus_plus_buts .plus').tooltip('hide')", 3000);
                    }
                });
                return false;
            });

            $("#minus_plus_buts").on("click", "a.minus", function (event) {
                var obj = $(this);
                $.post("/tpl/ajax/plus_minus.php", {ID: <?=$arResult["ID"]?>, act: "minus"}, function (otvet) {
                    if (parseInt(otvet)) {
//                        $(".all").html($(".all").html() * 1 + 1);
                        obj.html(otvet);
                    } else {
                        $('#minus_plus_buts .minus').tooltip({
                            'title': otvet,
                            'trigger': "manual",
                            'placement': 'right'
                        });
                        $('#minus_plus_buts .minus').tooltip("show");
                        setTimeout("$('#minus_plus_buts .plus').tooltip('hide')", 3000);
                    }
                });
                return false;
            });

            setTimeout(asd, 300);
            function asd() {
                <?
                    $en_ru_element_id = $arResult["EN_RU_ELEMENT_ID"]?array($arResult["ID"],$arResult["EN_RU_ELEMENT_ID"]):array($arResult["ID"]);
                ?>
                $.post("/tpl/ajax/plus_minus.php?NEW_DESIGN=Y", {ID: "<?=implode('|',$en_ru_element_id)?>", act: "generate_buts"}, function (otvet) {
                    $("#minus_plus_buts").html(otvet);
                });
            }
        });
    </script>
<?
if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="rga"||CITY_ID=="urm"||CITY_ID=="nsk"):
    if ($arResult["PROPERTIES"]["place_new_rest"]["VALUE"])
        echo "<div class='new_rest_place'>".GetMessage('detail_restaurant_on_him_place')." <a href=".$arResult["NEW_REST_PLACE"]["URL"].">".$arResult["NEW_REST_PLACE"]["NAME"]."</a></div>";
    include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/bron_form_new.php");
endif;
?>

<!--    --><?//if($arResult['VERANDA_SPEC_COUNT']):?>
<!--        <div class="veranda-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>-->
<!--    --><?//endif?>

<!--    --><?//if($arResult['EASTER_SPEC_COUNT']):?>
<!--        <div class="easter-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>-->
<!--    --><?//endif?>

<!--    --><?//if($arResult['POST_SPEC_COUNT']):?>
<!--        <div class="post-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>-->
<!--        <div class="clearfix"></div>-->
<!--    --><?//endif?>

<!--    --><?//if($arResult['VALENTINE_SPEC_COUNT']):?>
<!--        <div class="valentine-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>-->
<!--        <div class="clearfix"></div>-->
<!--    --><?//endif?>

    <?if($arResult['FEBRUARY23_SPEC_COUNT']):?>
        <div class="february23-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>
    <?endif?>

<!--    --><?//if($arResult['8MARTA_SPEC_COUNT']):?>
<!--        <div class="marta-event-slide-trigger" href="#podborki" data-toggle="anchor"></div>-->
<!--    --><?//endif?>
<!--    --><?//if($arResult['NY_SPEC_COUNT']):?>
<!--        <div class="new-year-event-slide-trigger" href="#podborki" data-toggle="anchor"><span>--><?//=GetMessage('new_year_event');?><!--</span> 2017</div>-->
<!--    --><?//endif?>

    <div class="clearfix"></div>


    <div class="sort anchors-list">

        <?if(!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
            <a href="#photos_tab" data-toggle='detail_tabs' class="asc"><?=GetMessage('detail_restaurant_photo_gallery');?></a>
        <?else:?>
            <a href="#map_tab" id="map_load" data-toggle='detail_tabs' class="asc"><?=GetMessage('detail_restaurant_on_map');?></a>
        <?endif?>
        <?if($arResult["PROPERTIES"]["videopanoramy"]["VALUE"][0]):?>
            <a href="#video" data-toggle='detail_tabs'><?=GetMessage('detail_restaurant_video_panorama');?></a>
        <?endif;?>
        <?if ($arResult["PROPERTIES"]["d_tours"]["VALUE"]):?>
            <a href="#d3d_tab" id="d3dload" data-toggle='detail_tabs'><span class="i3d"></span><?=GetMessage('detail_restaurant_3d_tour');?></a>
        <?endif;?>
        <a href="#<?if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>carousel-contacts<?else:?>props<?endif?>" data-toggle='anchor'><?=GetMessage('detail_restaurant_contacts');?></a>
        <?if($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0]&&!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
            <a href="#map_tab" id="map_load" data-toggle='detail_tabs'><?=GetMessage('detail_restaurant_on_map');?></a>
        <?endif;?>
        <?if ($arResult["MENU_ID"]):?>
            <a href="<?=$APPLICATION->GetCurDir()."menu/"?>" target='_blank'><?=GetMessage('detail_restaurant_menu');?></a>
        <?endif;?>
        <a href="#reviews" data-toggle='anchor'><?=GetMessage('detail_restaurant_reviews');?></a>
        <?if ($arResult["NEWS_COUNT"]):?>
            <a href="#news" data-toggle='anchor'><?=GetMessage('detail_restaurant_news');?></a>
        <?endif;?>
        <?if ($arResult["AFISHA_COUNT"]):?>
            <a href="#afisha" data-toggle='anchor'><?=GetMessage('detail_restaurant_poster');?></a>
        <?endif;?>
        <?if ($arResult["SPEC_COUNT"])://&&!$arResult['NY_SPEC_COUNT']?>
            <a href="#podborki" data-toggle='anchor'><?=GetMessage('detail_restaurant_events');?></a>
        <?endif;?>
<!--        --><?//if($arResult['NY_SPEC_COUNT']):?>
<!--            <div class="new-year-event-slide-trigger" href="#podborki" data-toggle="anchor"><span>--><?//=GetMessage('new_year_event');?><!--</span> 2016</div>-->
<!--        --><?//endif?>


    </div>

    <a href="/bitrix/components/restoran/favorite.add/ajax.php" class="favorite" data-toggle="favorite" data-restoran='<?=$arResult["ID"]?>'><?=GetMessage('detail_restaurant_to_favorite');?></a>
    <div class="clearfix"></div>
    <?if(!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
        <div class='detail_tabs' id='photos_tab'>
            <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/photos.php");?>
        </div>
    <?endif?>
    <div class='detail_tabs' id='video'>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/video.php");?>
    </div>
    <div class='detail_tabs' id='d3d_tab'>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/3d.php");?>
    </div>
    <div class='detail_tabs' id='map_tab' <?if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>style="display: block;"<?endif?>>
        <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/map.php");?>
    </div>
</div>


<?
include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/contacts.php");
?>
<div class="article_likes">
    <div class="features-in-pic-wrapper">
    <?
//    FirePHP::getInstance()->info($arResult['DISPLAY_PROPERTIES']['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE']);
    $i=0;
    foreach($arResult['DISPLAY_PROPERTIES']['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'] as $feature_in_pic){
        if($i>=6)
            break;

        $res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>5235, "ACTIVE"=>"Y", '=PROPERTY_RESTAURANT'=>$arResult['ID'], '=PROPERTY_FEATURES_IN_PICS'=>$feature_in_pic['ID']), false, array("nTopCount"=>1), array("NAME"));
        if($ob = $res->Fetch())
        {
            $feature_in_pic['NAME'] = $ob['NAME'];
        }
        $arFile = CFile::GetFileArray($feature_in_pic['PREVIEW_PICTURE']);?>
        <img src="<?=$arFile['SRC']?>" width="" height="" alt="<?=$feature_in_pic['NAME']?>" title="<?=$feature_in_pic['NAME']?>" data-toggle="tooltip-pic" data-placement="top">
    <?$i++;}?>

    <?if(count($arResult['DISPLAY_PROPERTIES']['FEATURES_IN_PICS']['LINK_ELEMENT_VALUE'])>0):?>
    <script>
        $(function () {
            $('[data-toggle="tooltip-pic"]').tooltip()
        })
    </script>
    <?endif?>

    </div>
    <div class="pull-right">
        <?$APPLICATION->IncludeComponent(
            "bitrix:asd.share.buttons",
            "likes",
            Array(
                "ASD_TITLE" => $APPLICATION->GetTitle(false),
                "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                "ASD_PICTURE" => "",
                "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                "LIKE_TYPE" => "LIKE",
                "VK_API_ID" => "2881483",
                "VK_LIKE_VIEW" => "mini",
                "SCRIPT_IN_HEAD" => "N"
            )
        );?>
    </div>

    <div class="likes-buttons" id="minus_plus_buts">

    </div>


</div>


<?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/props.php");?>

