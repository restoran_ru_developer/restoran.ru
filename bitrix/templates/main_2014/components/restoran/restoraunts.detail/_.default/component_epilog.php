<?php
/*$count = count($_COOKIE["RECENTLY_VIEWED"]["catalog"]);
if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["catalog"]))
    setcookie ("RECENTLY_VIEWED[catalog][".$count."]", $arResult["ID"], time() + 3600*12, "/");*/
IncludeModuleLangFile(__FILE__);

global $element_id, $count, $without_reviews, $similar_rest, $RGROUP, $FOURSQUARE_USER_LOGIN, $INSTAGRAM_USER_LOGIN, $RESTORAN_NAME, $SHOW_REST_PHONE_IN_ORDER_BANNER, $restaurant_phone, $REST_NETWORK, $en_ru_element_id;

$RESTORAN_NAME = $arResult['NAME'];
$FOURSQUARE_USER_LOGIN = $arResult['PROPERTIES']['FOURSQUARE_USER_LOGIN']['VALUE'];
$INSTAGRAM_USER_LOGIN = $arResult['PROPERTIES']['INSTAGRAM_USER_LOGIN']['VALUE'];
$REST_NETWORK = array_merge($arResult['PROPERTIES']['REST_NETWORK']['VALUE'],array($arResult['ID']));

if($arResult['PROPERTIES']['SHOW_REST_PHONE_IN_ORDER_BANNER']['VALUE_ENUM']=='Да'){
    $SHOW_REST_PHONE_IN_ORDER_BANNER = true;

    if(is_array($arResult["PROPERTIES"]["phone"]["VALUE"])){
        $phone = preg_split("/,|;/", $arResult["PROPERTIES"]["phone"]["VALUE"][0]);
        $restaurant_phone = clearUserPhone($phone[0]);
    }
    else {
        $phone = preg_split("/,|;/", $arResult["PROPERTIES"]["phone"]["VALUE"][0]);
        $restaurant_phone = clearUserPhone($phone[0]);
    }
}


$en_ru_element_id = $arResult["EN_RU_ELEMENT_ID"];
$element_id = $arResult["ID"];
$menu_id = $arResult["MENU_ID"];
$without_reviews = $arResult['WITHOUT_REVIEWS'];

$similar_rest = $arResult["SIMILAR"];
$count["PHOTO_REVIEWS_COUNTER"] = $arResult["PHOTO_REVIEWS_COUNTER"];
$count["AFISHA_COUNT"] = $arResult["AFISHA_COUNT"];
//$count["REVIEWS_COUNTER"] = $arResult["REVIEWS_COUNTER"];
$count["NEWS_COUNT"] = $arResult["NEWS_COUNT"];
$count["MC_COUNT"] = $arResult["MC_COUNT"];
$count["SPEC_COUNT"] = $arResult["SPEC_COUNT"];
$count["BLOG_COUNT"] = $arResult["BLOG_COUNT"];
$count["OVERVIEWS_COUNT"] = $arResult["OVERVIEWS_COUNT"];
$count["INTERVIEW_COUNT"] = $arResult["INTERVIEW_COUNT"];
$count["PHOTO_COUNT"] = $arResult["PHOTO_COUNT"];
$count["CONTACTS"] = $arResult["CONTACTS"];
$count["LAT"] = $arResult["LAT"];
$count["LON"] = $arResult["LON"];
$count["VIDEO_NEWS_COUNT"] = $arResult["VIDEO_NEWS_COUNT"];
$count["PREVIEW_TEXT"] = $arResult["PREVIEW_TEXT"];
$RGROUP = $arResult["RGROUP"];

$ratio = '<div class="name_ratio" >';//
for($i = 1; $i <= 5; $i++):
    if ($i > $arResult["RATIO"])
        $ratio .= '<span class="icon-star-empty"></span>';
    else
        $ratio .= '<span class="icon-star"></span>';
endfor;
$ratio .= '</div><div class="pull-right" id="for_edit_link"></div>';
$APPLICATION->AddViewContent("restoran_name_view_content", $arResult["H1"].$ratio);

ob_start();
echo '<div class="right-side booking-inf-wrapper">
        <div class="book-counter-wrapper">
            <div class="book-counter-title">'.GetMessage("BRON_COUNT_TITLE_IN_DETAIL").'</div>
            <div class="book-counter-today">'.GetMessage("BRON_COUNT_today_IN_DETAIL").': <span>'.$arResult['BOOK_COUNTER_TODAY'].'</span> '.GetMessage("BRON_COUNT_people_IN_DETAIL").'</div>
            <div class="book-counter-all">'.GetMessage("BRON_COUNT_total_IN_DETAIL").': <span>'.$arResult['BOOK_COUNTER_ALL'].'</span> '.GetMessage("BRON_COUNT_people_IN_DETAIL").'</div>
        </div>';
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH."/include_areas/new-order-detail-form.php",
        "EDIT_TEMPLATE" => ""
    ),
    false
);

//$APPLICATION->IncludeComponent(
//    "bitrix:advertising.banner",
//    "",
//    Array(
//        "TYPE" => "right_2_main_page",
//        "NOINDEX" => "Y",
//        "CACHE_TYPE" => "A",
//        "CACHE_TIME" => "0"
//    ),
//    false
//);

echo '<div id="counter" align="center"><img src="/tpl/ajax/counter.php?ID='.$arResult['ID'].'" alt="Статистика Ресторан.Ру" title="Статистика Ресторан.Ру"></div>
            </div>';
$out1 = ob_get_contents();

ob_end_clean();

$APPLICATION->AddViewContent("restoran_counter_content", $out1);?>


<?
/**MENU OUTPUT**/
ob_start();
if(!$arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):?>
    <div class="detail-menu-wrapper">
        <?
        FirePHP::getInstance()->info($arResult["PROPERTIES"]["SKYDISH_ID"]['VALUE'],'skyyy');
        if($arResult["PROPERTIES"]["SKYDISH_ID"]['VALUE']>0){
	        $APPLICATION->IncludeComponent(
				"restoran:skydish.menu",
				"rest_detail",
				Array("DIR"=>$APPLICATION->GetCurDir(),"REST_ID"=>$arResult["PROPERTIES"]["SKYDISH_ID"]['VALUE']),
				false
			);	
        }elseif ($arResult["MENU_ID"]){
            if (!$arResult["PREVIEW_TEXT"]&&!$arResult["DETAIL_TEXT"])
                $menu_items = 2;
            else
                $menu_items = 5;

            global $arrFilMenu;

            if($arResult['photoSectionId']){
                $arrFilMenu['!SECTION_ID']=$arResult['photoSectionId'];
            }

            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "menu_rest",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "rest_menu_ru",
                    "IBLOCK_ID" => $arResult["MENU_ID"],
                    "NEWS_COUNT" => $menu_items,
                    "SORT_BY1" => "tags",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "date_create",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilMenu",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array("ratio", "reviews"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "150",
                    "ACTIVE_DATE_FORMAT" => "j.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",//a
                    "CACHE_TIME" => "36000007",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
            );
        }
        ?>
    </div>
    <?
	    
	   //$arResult["PROPERTIES"]["SKYDISH_ID"]['VALUE']; 
    ?>
<?endif;?>

<?
if (trim(strip_tags($arResult["DETAIL_TEXT"]))):?>
    <div class="tab-str-wrapper">
        <div class="nav-tab-str-title">
            <?=GetMessage("ABOUT_REST_IN_DETAIL")?>
        </div>
    </div>

    <div class="detail-description-wrapper">
        <div class="description <?//if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']):half-height-description?><?//endif?>">
            <?=$arResult["DETAIL_TEXT"]?>
        </div>
        <div class="text-right">
            <a class="btn btn-light " onclick="$('.description').addClass('open_d'); $(this).hide().next().show()"><?=GetMessage("R_MORE_TEXT_1")?></a>
            <a class="btn btn-light " onclick="$('.description').removeClass('open_d'); $(this).hide().prev().show()" style="display: none"><?=GetMessage("R_MORE_TEXT_2")?></a>
        </div>
        <br />
    </div>
<?endif;?>

<?
$out1 = ob_get_contents();
ob_end_clean();

$APPLICATION->AddViewContent("restoran_menu", $out1);
?>

<?
global $APPLICATION;
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
//$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);

if($_REQUEST["CONTEXT"]=="Y"){
	//$_SESSION["CONTEXT"]="Y";
	if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
}
//if ($USER->IsAdmin())
//$APPLICATION->SetPageProperty("title",  "Ресторан ".$arResult["NAME"].". Адрес, меню, фото, отзывы на сайте Ресторан.Ру");
    //$APPLICATION->SetTitle();
    if ($arResult["IBLOCK"]["NAME"]=="Москва")
        $n = "Москве";
    elseif ($arResult["IBLOCK"]["NAME"]=="Юрмала")
        $n = "Юрмале";
    elseif ($arResult["IBLOCK"]["NAME"]=="Рига")
        $n = "Риге";
    elseif ($arResult["IBLOCK"]["NAME"]=="Тюмень")
        $n = "Тюмени";
    elseif ($arResult["IBLOCK"]["NAME"]=="Тюмень")
        $n = "Тюмени";
    elseif ($arResult["IBLOCK"]["NAME"]=="Сочи")
        $n = "Сочи";
    elseif ($arResult["IBLOCK"]["NAME"]=="Уфа")
        $n = "Уфе";
    elseif ($arResult["IBLOCK"]["NAME"]=="Астана")
        $n = "Астане";
    elseif ($arResult["IBLOCK"]["NAME"]=="Алмата")
        $n = "Алмате";
    else
        $n = $arResult["IBLOCK"]["NAME"]."е";
    
    $APPLICATION->SetPageProperty("title",  "Ресторан ".$arResult["NAME"]." в ".$n." на Ресторан.Ру");
    $APPLICATION->SetPageProperty("description",  "Ресторан ".$arResult["NAME"]." в ".$n." на Ресторан.Ру");
    $APPLICATION->SetPageProperty("keywords",  "ресторан, ".$arResult["NAME"].", ".$arResult["IBLOCK"]["NAME"]."");
?>
<script>
$(window).load(function(){
    if (window.location.hash=='#map_tab') {
        $('.anchors-list a[href="'+window.location.hash+'"]').click();
        var pos=$('.anchors-list').offset().top;
        $('html,body').animate({scrollTop:pos},"300");
    }
});
    $(document).ready(function(){
        <?if(CSite::InGroup( array(14,15,1,23,24,29,28,30,34))):?>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
            $("#for_edit_link").append('<div id="edit_link" style="margin-top:12px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');
            <?else:?>
            $("#for_edit_link").append('<div id="edit_link" style="margin-top:12px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');
            <?endif;?>
        <?endif;?>

        <?if ($_REQUEST["bron"]=="Y"):?>
            var params = {"id":$('.order-panel a.booking').data('id'),"name":$('.order-panel a.booking').data('restoran')};
            $.ajax({
                type: "POST",
                url: $('.order-panel a.booking').attr("href"),
                data: params
            })
                .done(function(data) {
                    $('#booking_form .modal-body').html(data);
                });
            $('#booking_form .modal-dialog').removeClass("small");
            $('#booking_form').modal('show');
        <?endif;?>
        <?if ($_REQUEST["bron_banket"]=="Y"):?>
            $('html,body').animate({scrollTop:600}, "300");
            $.ajax({
                type: "POST",
                url: "/tpl/ajax/online_order_rest.php",
                data: "what=2&date="+$(".whose_date").val()+"&name=<?=rawurlencode($arResult["NAME"])?>&id=<?=$arResult["ID"]?>&time="+$(".time").val()+"&person="+$("#person").val()+"&<?=bitrix_sessid_get()?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");
                    }
                    $('#bron_modal').html(data);
                    showOverflow();
                    setCenter($("#bron_modal"));
                    $("#bron_modal").fadeIn("300");
                }
            });
        <?endif;?>
    });
</script>
<?
					//if ($USER->IsAdmin()) {
// костыль. Принять данные из $_REQUEST, которые пришли из формы на Яндексе (острова)

//$arResult["arrVALUES"]["form_text_64"]
if (!empty($_REQUEST["form_text_32"])) {
$arr_name = explode('~',$_REQUEST["form_text_32"]);
// имя и id ресторана
	//$arResult["arrVALUES"]["form_text_32"] = $_REQUEST["form_text_32"].'';
	//$arResult["NAME"] = $_REQUEST["form_text_32"].'';
	?>
<input type="hidden" name="frm_form_text_32" id="frm_form_text_32" value="<?=$arr_name[0]?>" />
<input type="hidden" name="rcurrent_id" id="rcurrent_id" value="<?=$arr_name[1]?>" />
	<?
}

if (!empty($_REQUEST["form_text_39"])) {
// фио
	?>
<input type="hidden" name="frm_form_text_39" id="frm_form_text_39" value="<?=$_REQUEST["form_text_39"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_40"])) {
// телефон
	?>
<input type="hidden" name="frm_form_text_40" id="frm_form_text_40" value="<?=$_REQUEST["form_text_40"]?>" />
	<?
}

if (!empty($_REQUEST["form_email_41"])) {
// E-mail
	?>
<input type="hidden" name="frm_form_email_41" id="frm_form_email_41" value="<?=$_REQUEST["form_email_41"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_35"])) {
// персон
	?>
<input type="hidden" name="frm_form_text_35" id="frm_form_text_35" value="<?=$_REQUEST["form_text_35"]?>" />
	<?
}

if (!empty($_REQUEST["form_checkbox_smoke"])) {
// курящий / не курящий
	?>
<input type="hidden" name="frm_form_checkbox_smoke" id="frm_form_checkbox_smoke" value="<?=$_REQUEST["form_checkbox_smoke"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_34"])) {
// time
	?>
<input type="hidden" name="frm_form_text_34" id="frm_form_text_34" value="<?=$_REQUEST["form_text_34"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_33"])) {
// date
$curDate = $_REQUEST["form_text_33"];
$curDate = preg_replace("/^([\d]{2}+)([\d]{2}+)/is","$1.$2.",$curDate);
	?>
<input type="hidden" name="frm_form_text_33" id="frm_form_text_33" value="<?=$curDate?>" />
<input type="hidden" name="frm_form_date_33" id="frm_form_date_33" value="<?=$curDate?>" />
	<?
}



if($arResult['SHOW_COUNTER_WORK']&&$arResult['SHOW_COUNTER']){
    $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), Array("CODE"=>"SHOW_COUNTER"));
    if($ar_props = $db_props->Fetch()){
        $sleeping_val_id = array(
            'spb'=>1468,
            'msk'=>1419,
            'urm'=>2573,
            'rga'=>2556
        );

        if($ar_props["VALUE"]!=''&&$arResult['SHOW_COUNTER']!=$ar_props['DESCRIPTION']){    //  в DESCRIPTION - записываeтся SHOW_COUNTER
            if($ar_props["VALUE"]<1){
                CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arResult['IBLOCK_ID'], array('sleeping_rest' => $sleeping_val_id[CITY_ID]));
                CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arResult['IBLOCK_ID'], array('SHOW_COUNTER' => ''));

                $el = new CIBlockElement;
                $el->Update($arResult["ID"], Array(), false, true, true);

//                //  отправка письма

                $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $element_id, array("sort" => "asc"), Array("CODE"=>"user_bind"));
                while($ar_props = $db_props->Fetch()){
                    $restorator_id = $ar_props['VALUE'];
                    if($restorator_id){
                        $rs = CUser::GetByID($restorator_id);
                        if($arUser = $rs->Fetch()){
                            if($arUser["EMAIL"]){
                                $mail_message = "у ресторана ".$RESTORAN_NAME." закончилось количество показов, ресторан переводится в спящий режим.";
                                $arMessage = Array(
                                    "MESSAGE"=> $mail_message,
                                    'EMAIL_TO'=>$arUser["EMAIL"]
                                );
                                $res = CEvent::Send("SHOW_COUNTER_END", "s1", $arMessage);
                            }
                        }
                    }
                }




            }
            else {
                $deduct_count = $arResult['SHOW_COUNTER']-$ar_props['DESCRIPTION'];
                $prop = $ar_props["VALUE"] - $deduct_count;

                $prop = array("VALUE"=>$prop,'DESCRIPTION'=>$arResult['SHOW_COUNTER']);

                CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arResult['IBLOCK_ID'], array('SHOW_COUNTER' => $prop));


                //  добавление статистики
                global $DB;

                $sql = "SELECT * FROM show_counter_statistic WHERE rest_id=".$arResult['ID']." AND stat_date='".date('Y-m-d')."'";
                $db_list = $DB->Query($sql);
                if($ar_result = $db_list->Fetch())
                {
                    //update

                    $show_counter_day_count_val =  $deduct_count+$ar_result['count_val'];
                    $sql = "UPDATE show_counter_statistic SET count_val=".$show_counter_day_count_val." WHERE rest_id=".$arResult['ID']." AND stat_date='".date('Y-m-d')."'";
                    $db_list = $DB->Query($sql);
                    $db_list->Fetch();
                }
                else {
                    $sql = "INSERT INTO show_counter_statistic (rest_id, stat_date, count_val) VALUES(".$arResult['ID'].",'".date('Y-m-d')."',".$deduct_count.")";
                    $db_list = $DB->Query($sql);
                    $db_list->Fetch();
                }


                if($prop['VALUE']==50){
                    //  отправка письма
                    $db_props = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $element_id, array("sort" => "asc"), Array("CODE"=>"user_bind"));
                    while($ar_props = $db_props->Fetch()){
                        $restorator_id = $ar_props['VALUE'];
                        if($restorator_id){
                            $rs = CUser::GetByID($restorator_id);
                            if($arUser = $rs->Fetch()){
                                if($arUser["EMAIL"]){
                                    $mail_message = "у ресторана ".$RESTORAN_NAME." заканчивается количество показов, через 50 показов ресторан будет переведен в спящий режим.";
                                    $arMessage = Array(
                                        "MESSAGE"=> $mail_message,
                                        'EMAIL_TO'=>$arUser["EMAIL"]
                                    );
                                    $res = CEvent::Send("SHOW_COUNTER_END", "s1", $arMessage);
                                }
                            }
                        }
                    }

                }

            }

        }
    }
}
?>

<?ob_start();?>
<?//$arReviewsIB = getArIblock("reviews", CITY_ID);
//$res = CIBlockElement::GetList(Array(), array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>array($arResult['ID'], $arResult['EN_RU_ELEMENT_ID'])), false, false, array("DETAIL_TEXT"));
//while($ob = $res->Fetch())
//{?>
    <meta itemprop="ratingValue" content="<?=floor($arResult["RATIO"])?>" />
<?//}?>

<meta itemprop="bestRating" content="5" />
<meta itemprop="ratingCount" content="<?=$arResult['REVIEWS_COUNTER']?>" />

<?$out1 = ob_get_contents();
ob_end_clean();
$APPLICATION->AddViewContent("AggregateRating", $out1);?>
