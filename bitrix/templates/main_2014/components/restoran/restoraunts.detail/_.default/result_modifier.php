<?
// reformat properties
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill", "music", "area", "administrative_distr", "proposals", "entertainment", "parking", "features", "stoimostmenyu", "kolichestvochelovek","opening_hours"
);

//  показывать телефон ресторатора
if(preg_match('/banquet-service/',$_SERVER['HTTP_REFERER'])){
    $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams["ELEMENT_ID"], "sort", "asc", Array("CODE"=>"SHOW_REST_PHONE"));
    if($ar_props = $db_props->Fetch()){
        if($ar_props['VALUE_ENUM']=='Да'){
            $arResult['SHOW_REST_PHONE'] = true;
        }
    }
}

foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
    if(in_array($pid, $arFormatProps)) {
        if(is_array($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
            foreach($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
            }
        } else {
            $old_val = $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"];
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = array();
            $arResult["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][0] = strip_tags($old_val);
        }
    }
}
if (LANGUAGE_ID=="en")
{
    foreach ($arResult["DISPLAY_PROPERTIES"] as &$properties)
    {        
            if (is_array($properties["DISPLAY_VALUE"]))
            {                
                if (is_array($properties["VALUE"]))
                {                    
                    foreach($properties["VALUE"] as $key=>$val)
                    {
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$val), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {                                                
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"][$key] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"][$key]);                        
                        }            
                    }
                }
                else
                {
                    if($properties["VALUE"]){
                        $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                        if ($ar = $r->Fetch())
                        {
                            if ($ar["PROPERTY_ENG_NAME_VALUE"])
                                $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                        }
                    }
                }
            }
            else
            {
                if($properties["VALUE"]){
                    $r = CIBlockElement::GetList(Array(),Array("ID"=>$properties["VALUE"]), false,false, Array("ID","NAME","PROPERTY_eng_name"));
                    if ($ar = $r->Fetch())
                    {
                        if ($ar["PROPERTY_ENG_NAME_VALUE"])
                            $properties["DISPLAY_VALUE"] = str_replace($ar["NAME"],$ar["PROPERTY_ENG_NAME_VALUE"],$properties["DISPLAY_VALUE"]);
                    }
                }
            }
            if ($properties["CODE"]=="wi_fi")
                    $properties["DISPLAY_VALUE"] = "Yes";
            
    }
}

$old_val = "";


$res = array();
$arNewsIB = getArIblock("news", CITY_ID);
if ($arNewsIB["ID"])
{    
    if (CITY_ID!="msk"&&CITY_ID!="spb")
    {
        $r = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arNewsIB["ID"],"CODE"=>"letnie_verandy"));
        if ($arL = $r->Fetch())
        {
            $sectL = $arL["ID"];
        }
        $res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("!SECTION_ID"=>$sectL,"PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["NEWS_COUNT"] = $res->SelectedRowsCount();
        if ($sectL)
        {
            $res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("SECTION_ID"=>$sectL,"PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
            $arResult["LETN_COUNT"] = $res->SelectedRowsCount();        
        }
    }
    else
    {
        $res = CIBlockElement::GetList(Array("ACTIVE_FROM"),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["NEWS_COUNT"] = $res->SelectedRowsCount();
    }
    
    $r = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arNewsIB["ID"],"CODE"=>"oktoberfest"));
    if ($arL = $r->Fetch())
    {
        $sectL = $arL["ID"];        
    }
    if ($sectL)
    {
        $res = CIBlockElement::GetList(Array(),Array("SECTION_ID"=>$sectL,"PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arNewsIB["ID"]),false,Array("nTopCount"=>1));
        if ($aCorp = $res->GetNext())
        {
            $arResult["OKT_COUNT"] = 1;
            $arResult["OKT_LINK"] = $aCorp["DETAIL_PAGE_URL"];
        }
    }
}
if((CITY_ID=="msk"||CITY_ID=="spb")&&SITE_LANGUAGE_ID!='en')
{
    $ar1 = getArIblock("special_projects", CITY_ID,"easter_");
    $ar2 = getArIblock("special_projects", CITY_ID,"8marta_");
    $ar3 = getArIblock("special_projects", CITY_ID,"valentine_");
    $ar4 = getArIblock("special_projects", CITY_ID,"post_");
    $ar5 = getArIblock("special_projects", CITY_ID,"new_year_night_");
    $ar6 = getArIblock("special_projects", CITY_ID,"new_year_corp_");
    $ar7 = getArIblock("special_projects", CITY_ID,"letnie_verandy_");        
    $ar8 = getArIblock("special_projects", CITY_ID,"sport_");
    $ar9 = getArIblock("special_projects", CITY_ID,"february23_");
    $ar_spec = Array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],$ar6["ID"],$ar7["ID"],$ar8["ID"],$ar9["ID"]);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
    $arResult["SPEC_COUNT"] = $res->SelectedRowsCount();




//    $ar_spec = Array($ar4["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["POST_SPEC_COUNT"] = $res->SelectedRowsCount();


//    $ar_spec = Array($ar7["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["VERANDA_SPEC_COUNT"] = $res->SelectedRowsCount();


//    $ar_spec = Array($ar1["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["EASTER_SPEC_COUNT"] = $res->SelectedRowsCount();

    $ar_spec = Array($ar5["ID"],$ar6["ID"]);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
    $arResult["NY_SPEC_COUNT"] = $res->SelectedRowsCount();

//    $ar_spec = Array($ar3["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["VALENTINE_SPEC_COUNT"] = $res->SelectedRowsCount();

    $ar_spec = Array($ar9["ID"]);
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
    $arResult["FEBRUARY23_SPEC_COUNT"] = $res->SelectedRowsCount();

//    $ar_spec = Array($ar2["ID"]);
//    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$ar_spec),false,Array("nTopCount"=>1));
//    $arResult["8MARTA_SPEC_COUNT"] = $res->SelectedRowsCount();
}

$res = array();
if (CITY_ID!="ast"&&CITY_ID!="alm"&&CITY_ID!="vrn"&&CITY_ID!="tln"&&CITY_ID!="krd"&&CITY_ID!="sch"&&CITY_ID!="kld"&&CITY_ID!="ufa")
{    
    $arVideonewsIB = getArIblock("videonews", CITY_ID);
    if ($arVideonewsIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arVideonewsIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["VIDEONEWS_COUNT"] = $res->SelectedRowsCount();        
    }
    $res = array();
    $arPhotoIB = getArIblock("photoreports", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["PHOTO_COUNT"] = $res->SelectedRowsCount();
    }
    
    $res = array(); 
    $arPhotoIB = getArIblock("overviews", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["OVERVIEWS_COUNT"] = $res->SelectedRowsCount();        
    }
    $res = array(); 
    $arPhotoIB = getArIblock("interview", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["INTERVIEW_COUNT"] = $res->SelectedRowsCount();        
    }
    $res = array(); 
    $arPhotoIB = getArIblock("blogs", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));        
        $arResult["BLOG_COUNT"] = $res->SelectedRowsCount();        
    }

    $arPhotoIB = getArIblock("videonews", CITY_ID);
    if ($arPhotoIB["ID"])
    {
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>$arPhotoIB["ID"]),false,Array("nTopCount"=>1));
        $arResult["VIDEO_NEWS_COUNT"] = $res->SelectedRowsCount();
    }

    if (SITE_ID=="s1")
    {
        $res = array(); 
        $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y","IBLOCK_ID"=>145),false,Array("nTopCount"=>1));
        $arResult["MC_COUNT"] = $res->SelectedRowsCount();
                
        $res = array();
    }
}


$res = array();
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arResult["AFISHA_IB"] = $arAfishaIB;
if ($arAfishaIB["ID"])
{
    $res = CIBlockElement::GetList(Array(),Array("PROPERTY_RESTORAN"=>$arResult["ID"],"!PROPERTY_RESTORAN"=>false,"ACTIVE"=>"Y", "ACTIVE_DATE"=>"Y", ">=PROPERTY_EVENT_DATE"=>array(false, date('Y-m-d')),
        Array("LOGIC"=>"OR",
            Array(            
                "PROPERTY_d1"=>"false",
                "PROPERTY_d2"=>"false",
                "PROPERTY_d3"=>"false",
                "PROPERTY_d4"=>"false",
                "PROPERTY_d5"=>"false",
                "PROPERTY_d6"=>"false",
                "PROPERTY_d7"=>"false",
            ),
            Array(
                "!PROPERTY_d1"=>"false",
            ),
            Array(
                "!PROPERTY_d2"=>"false",
            ),
            Array(
                "!PROPERTY_d3"=>"false",
            ),
            Array(
                "!PROPERTY_d4"=>"false",
            ),
            Array(
                "!PROPERTY_d5"=>"false",
            ),
            Array(
                "!PROPERTY_d6"=>"false",
            ),
            Array(
                "!PROPERTY_d7"=>"false",
            ),
        ),
        "IBLOCK_ID"=>$arAfishaIB["ID"]),false,Array("nTopCount"=>1));
    $arResult["AFISHA_COUNT"] = $res->SelectedRowsCount();    
}

global $DB;
$sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a 
        JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=".$DB->ForSql($arResult["ID"])." ";
$db_list = $DB->Query($sql);
if($ar_result = $db_list->Fetch())
{
    if ($ar_result["ELEMENT_CNT"]>0)
        $arResult["MENU_ID"] = $ar_result['ID'];

    //  получение id фото раздела
    $db_list = CIBlockSection::getList(array(),array('IBLOCK_ID'=>$arResult["MENU_ID"], 'CODE'=>'foto'),false, array('ID'));
    if($ar_result = $db_list->Fetch()){
        $arResult['photoSectionId']=$ar_result['ID'];
    }
}
$p =0;

// get preview text
if ($arResult["MENU_ID"])
    $truncate_len = 200;
else
    $truncate_len = 100;

if($arResult["PROPERTIES"]['REST_NETWORK']['VALUE']){
    $truncate_len = 100;
}

$obParser = new CTextParser;
$arResult["PREVIEW_TEXT"] = $obParser->html_cut($arResult["DETAIL_TEXT"], $truncate_len);
//$arResult["PREVIEW_TEXT"] = strip_tags($arResult["PREVIEW_TEXT"],"<p><a><ul><li><ol>");
$arResult["PREVIEW_TEXT_VK"] = $obParser->html_cut($arResult["DETAIL_TEXT"], 100);
 $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'medium',
        'type'=>'image',
        'alpha_level'=>'40',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark1.png', 
        ),
    );
foreach($arResult["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
{
    $file = CFile::GetFileArray($photo);
    $arResult['MULTIPLE_PHOTO_ARR'][$key] = CFile::ResizeImageGet($file,Array("width"=>$file['WIDTH'],"height"=>$file['HEIGHT']),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
    if($file['WIDTH'] < $file['HEIGHT']){
        $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($photo,Array("width"=>728,"height"=>400),BX_RESIZE_IMAGE_PROPORTIONAL,true);
    }
    else {
        $arResult["PROPERTIES"]["photos"]["VALUE2"][$p] = CFile::ResizeImageGet($photo,Array("width"=>728,"height"=>400),BX_RESIZE_IMAGE_EXACT,true);
    }
    $p++;
}

$res = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>101,"CODE"=>CITY_ID));
if ($ars = $res->GetNext())
{
    if($ars["DESCRIPTION"])
    {
        $ars["DESCRIPTION"] = explode("<i>",$ars["DESCRIPTION"]);
        $arResult["CONTACT_DESCRIPTION"] = $ars["DESCRIPTION"][0];

        }
}

if($arResult['ID']==2316325){
    $arResult["H1"] = '<h1>Рестораны и банкетные залы «Талион Империал Отеля»</h1>';
}
elseif($arResult['PROPERTIES']['REST_NETWORK']['VALUE'] && $arResult['ID']!=1951150){
    $arResult["H1"] = '<h1>'.GetMessage('REST_NETWORK').' '.$arResult["NAME"].'</h1>';
}
else {
    $new_rest_name = $arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"][0]." ";
    if ($arResult["SECTION"]["PATH"][0]["CODE"]=="restaurants"):
        $rest_name = GetMessage("R_REST")." ";
    else:
        $rest_name = GetMessage("R_BH")." ";
    endif;
    if ($arResult["ID"]=="1786885")
    {
        $rest_name = GetMessage("R_CAFE")." ";
    }
    if (!$new_rest_name)
        $new_rest_name = $rest_name;
    $arResult["H1"] = '<h1 '.($arResult['PROPERTIES']['REST_NETWORK']['VALUE']?'':'itemprop="name"').'>'.$new_rest_name.$arResult["NAME"].'</h1>';
}



//  book counter
$arResult['BOOK_COUNTER_ALL'] = 0;
$arSelect = Array("PROPERTY_guest");
$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y",'PROPERTY_rest'=>$arResult['ID'],'!PROPERTY_status'=>array(1502,1418));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch())
{
    $arResult['BOOK_COUNTER_ALL'] += $ob['PROPERTY_GUEST_VALUE'];
}
FirePHP::getInstance()->info($arResult['BOOK_COUNTER_ALL'],'BOOK_COUNTER_ALL');

$arResult['BOOK_COUNTER_TODAY'] = 0;
$arSelect = Array("PROPERTY_guest");
$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y",'PROPERTY_rest'=>$arResult['ID'],'>=DATE_ACTIVE_FROM'=>ConvertTimeStamp(strtotime(date('d.m.Y'))),'>=DATE_CREATE'=>ConvertTimeStamp(strtotime(date('d.m.Y'))),'<=DATE_CREATE'=>ConvertTimeStamp(strtotime(date('d.m.Y',strtotime('+1 day')))),'!PROPERTY_status'=>1418);//'<=DATE_ACTIVE_FROM'=>ConvertTimeStamp(strtotime(date('d.m.Y',strtotime('+1 day'))))
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->Fetch())
{
    $arResult['BOOK_COUNTER_TODAY'] += $ob['PROPERTY_GUEST_VALUE'];
}


//   для отзывов en-ru
$en_ru_synchronize_compare = array(11 => 2586, 12 => 2587, 3566 => 3624, 3567 => 3625);
if(LANGUAGE_ID=='en'){
    $en_ru_synchronize_compare = array_flip($en_ru_synchronize_compare);
}

$get_en_obj = CIBlockElement::GetList(Array(), array('CODE'=>$arResult['CODE'],'IBLOCK_ID'=>$en_ru_synchronize_compare[$arResult['IBLOCK_ID']]), false, Array('nTopCount'=>1), array('ID'));
if($ob = $get_en_obj->SelectedRowsCount())
{
    $en_ru_element_id_obj = $get_en_obj->Fetch();
    $en_ru_element_id = $en_ru_element_id_obj['ID'];
}


//  количество отзывов
$arReviewsIB = getArIblock("reviews", CITY_ID);
$arSelect = Array("ID");
$arFilter = Array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>array($arResult['ID'],$en_ru_element_id));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
if($ob = $res->SelectedRowsCount())
{
    $arResult['REVIEWS_COUNTER'] = $ob;
}
else {
    $arResult['REVIEWS_COUNTER'] = 0;
}


$arFilter = Array("IBLOCK_ID"=>$arReviewsIB['ID'], "ACTIVE"=>"Y",'PROPERTY_ELEMENT'=>array($arResult['ID'],$en_ru_element_id),'!PROPERTY_photos'=>false);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
if($ob = $res->SelectedRowsCount())
{
    $arResult['PHOTO_REVIEWS_COUNTER'] = $ob;
}
else {
    $arResult['PHOTO_REVIEWS_COUNTER'] = 0;
}


if(LANGUAGE_ID=='en'){
    $db_props = CIBlockElement::GetProperty($en_ru_synchronize_compare[$arResult['IBLOCK_ID']], $en_ru_element_id, array("sort" => "asc"), Array("CODE"=>"FOURSQUARE_USER_LOGIN"));
    if($ar_props = $db_props->Fetch()){
        $arResult['PROPERTIES']['FOURSQUARE_USER_LOGIN']['VALUE'] = $ar_props["VALUE"];
    }
}

//  для новых сетевых
if($arResult['PROPERTIES']['REST_NETWORK']['VALUE']){
    unset($arResult['PROPERTIES']['address']['VALUE']);
    unset($arResult['PROPERTIES']['map']['VALUE']);
    unset($arResult['PROPERTIES']['subway']['VALUE']);
    unset($arResult['PROPERTIES']['phone']['VALUE']);

    foreach($arResult['PROPERTIES']['REST_NETWORK']['VALUE'] as $rest_network){

        $arSelect = Array("DETAIL_PAGE_URL",'NAME');
        $arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'],'ID'=>$rest_network);//"ACTIVE"=>"Y"
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNext())
        {
            $arResult['PROPERTIES']['REST_NETWORK']['NETWORK_REST_NAME'][] = $ob['NAME'];
            $arResult['PROPERTIES']['REST_NETWORK']['URL'][] = $ob['DETAIL_PAGE_URL'];
            $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
                $arResult['PROPERTIES']['address']['VALUE'][] = $ar_props["VALUE"];
            }
            else {
                $arResult['PROPERTIES']['address']['VALUE'][] = '';
            }
            $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"map"));
            if($ar_props = $db_props->Fetch()){
                $arResult['PROPERTIES']['map']['VALUE'][] = $ar_props["VALUE"];
            }
            else {
                $arResult['PROPERTIES']['map']['VALUE'][] = '';
            }
            $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"subway"));
            if($ar_props = $db_props->Fetch()){
                $rrr = CIBlockElement::GetByID($ar_props["VALUE"]);
                if ($arr = $rrr->Fetch()){
                    $arResult["PROPERTIES"]["subway"]["VALUE"][] = $arr["NAME"];
                    $arResult["PROPERTIES"]["subway"]["CODE_ELEMENT"][] = $arr["CODE"];
                }
            }
            else {
                $arResult['PROPERTIES']['subway']['VALUE'][] = '';
            }

            $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"SHOW_NETWORK_PHONE_COMMON_PAGE"));
            if($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"real_tel"));
                    if($ar_props = $db_props->Fetch()){
                        $arResult['PROPERTIES']['phone']['VALUE'][] = $ar_props["VALUE"];
                    }
                    else {
                        $arResult['PROPERTIES']['phone']['VALUE'][] = '';
                    }
                }
                else {
                    $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"phone"));
                    if($ar_props = $db_props->Fetch()){
                        $arResult['PROPERTIES']['phone']['VALUE'][] = $ar_props["VALUE"];
                    }
                    else {
                        $arResult['PROPERTIES']['phone']['VALUE'][] = '';
                    }
                }
            }
            else {
                $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $rest_network, array("sort" => "asc"), Array("CODE"=>"phone"));
                if($ar_props = $db_props->Fetch()){
                    $arResult['PROPERTIES']['phone']['VALUE'][] = $ar_props["VALUE"];
                }
                else {
                    $arResult['PROPERTIES']['phone']['VALUE'][] = '';
                }
            }

        }
    }
//    FirePHP::getInstance()->info($arResult["PROPERTIES"]["subway"],'CODE');

}

//  ya streets
if(CITY_ID=='msk'||CITY_ID=='spb'){
    $db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), Array("CODE"=>"SHOW_YA_STREET"));
    if($ar_props = $db_props->Fetch()){
        if($ar_props["VALUE_ENUM"]=='Да'){
            $arResult['SHOW_YA_STREET'] = true;
        }
    }
}





$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
    if($arResult['PROPERTIES']['SHOW_COUNTER']['VALUE']!=''){
        $arResult["SHOW_COUNTER_WORK"] = true;
        $cp->arResult['SHOW_COUNTER_WORK'] = $arResult["SHOW_COUNTER_WORK"];
    }

	$cp->arResult['EN_RU_ELEMENT_ID'] = $en_ru_element_id;
	$cp->arResult['PHOTO_REVIEWS_COUNTER'] = $arResult["PHOTO_REVIEWS_COUNTER"];
	$cp->arResult['REVIEWS_COUNTER'] = $arResult["REVIEWS_COUNTER"];
	$cp->arResult['BOOK_COUNTER_ALL'] = $arResult["BOOK_COUNTER_ALL"];
	$cp->arResult['BOOK_COUNTER_TODAY'] = $arResult["BOOK_COUNTER_TODAY"];
	$cp->arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]["SRC"];
    $cp->arResult['MENU_ID'] = $arResult["MENU_ID"];
    $cp->arResult['photoSectionId'] = $arResult['photoSectionId'];
	$cp->arResult['TEXT'] = strip_tags($arResult["PREVIEW_TEXT"]);
    $cp->arResult['H1'] = $arResult["H1"];
    $cp->arResult['NEWS_COUNT'] = $arResult["NEWS_COUNT"];
    $cp->arResult['MC_COUNT'] = $arResult["MC_COUNT"];
    $cp->arResult['AFISHA_COUNT'] = $arResult["AFISHA_COUNT"];
    $cp->arResult['SPEC_COUNT'] = $arResult["SPEC_COUNT"];
//    $cp->arResult['NY_SPEC_COUNT'] = $arResult["NY_SPEC_COUNT"];
//    $cp->arResult['VALENTINE_SPEC_COUNT'] = $arResult["VALENTINE_SPEC_COUNT"];
//    $cp->arResult['8MARTA_SPEC_COUNT'] = $arResult["8MARTA_SPEC_COUNT"];
    $cp->arResult['BLOG_COUNT'] = $arResult["BLOG_COUNT"];
    $cp->arResult['VIDEO_NEWS_COUNT'] = $arResult["VIDEO_NEWS_COUNT"];
    $cp->arResult['PHOTO_COUNT'] = $arResult["PHOTO_COUNT"];
    $cp->arResult['OVERVIEWS_COUNT'] = $arResult["OVERVIEWS_COUNT"];
    $cp->arResult['INTERVIEW_COUNT'] = $arResult["INTERVIEW_COUNT"];
    $cp->arResult['RATIO'] = sprintf("%01.2f", $arResult["PROPERTIES"]["RATIO"]["VALUE"]);
    $cp->arResult['SIMILAR'] = $arResult["PROPERTIES"]["similar_rest"]["VALUE"];

    if(is_array($arResult["PROPERTIES"]["rest_group"]["VALUE"])){
        if(count($arResult["DISPLAY_PROPERTIES"]["rest_group"]['DISPLAY_VALUE'])>1){
            $first_group_arr = current($arResult["DISPLAY_PROPERTIES"]["rest_group"]["LINK_ELEMENT_VALUE"]);
            $cp->arResult['RGROUP'] = Array("ID" => $arResult["PROPERTIES"]["rest_group"]["VALUE"][0],"NAME"=>  strip_tags($arResult["DISPLAY_PROPERTIES"]["rest_group"]["DISPLAY_VALUE"][0]),'CODE'=>$first_group_arr['CODE']);//
        }
        else {
            $first_group_arr = current($arResult["DISPLAY_PROPERTIES"]["rest_group"]["LINK_ELEMENT_VALUE"]);
            $cp->arResult['RGROUP'] = Array("ID" => $arResult["PROPERTIES"]["rest_group"]["VALUE"][0],"NAME"=>  strip_tags($arResult["DISPLAY_PROPERTIES"]["rest_group"]["DISPLAY_VALUE"]),'CODE'=>$first_group_arr['CODE']);
        }
    }
    else {
        $first_group_arr = current($arResult["DISPLAY_PROPERTIES"]["rest_group"]["LINK_ELEMENT_VALUE"]);
        $cp->arResult['RGROUP'] = Array("ID" => $arResult["PROPERTIES"]["rest_group"]["VALUE"],"NAME"=>  strip_tags($arResult["DISPLAY_PROPERTIES"]["rest_group"]["DISPLAY_VALUE"]),'CODE'=>$first_group_arr['CODE']);
    }

//  todo CONTACTS проверка на REST_NETWORK вместо map
    $cp->arResult['CONTACTS'] = count($arResult["PROPERTIES"]["REST_NETWORK"]["VALUE"]?$arResult["PROPERTIES"]["REST_NETWORK"]["VALUE"]:$arResult["PROPERTIES"]["map"]["VALUE"]);
    $cp->arResult['LAT'] = $arResult["PROPERTIES"]["lat"]["VALUE"][0];
    $cp->arResult['LON'] = $arResult["PROPERTIES"]["lon"]["VALUE"][0];

    $cp->arResult['DETAIL_TEXT'] = $arResult["DETAIL_TEXT"];
    $cp->arResult['PREVIEW_TEXT'] = $arResult["PREVIEW_TEXT"];
    $cp->arResult['WITHOUT_REVIEWS'] = $arResult["PROPERTIES"]["without_reviews"]["VALUE"];

//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
    $cp->SetResultCacheKeys(array('IMAGE','TEXT','H1','RATIO','MENU_ID','photoSectionId','NEWS_COUNT','MC_COUNT','AFISHA_COUNT','SPEC_COUNT','OVERVIEWS_COUNT','INTERVIEW_COUNT','VIDEO_NEWS_COUNT','BLOG_COUNT','PHOTO_COUNT','SIMILAR','RGROUP','CONTACTS','LAT','LON',"DETAIL_TEXT","PREVIEW_TEXT","WITHOUT_REVIEWS",'BOOK_COUNTER_ALL','BOOK_COUNTER_TODAY','SHOW_COUNTER_WORK','PHOTO_REVIEWS_COUNTER','REVIEWS_COUNTER','EN_RU_ELEMENT_ID'));//,'VALENTINE_SPEC_COUNT'

}
if ($arResult["PROPERTIES"]["d_tours"]["VALUE"][0])
{    
    $arTourIB = getArIblock("system_lookon", CITY_ID);
    foreach($arResult["PROPERTIES"]["d_tours"]["VALUE"] as $key=>$dtour_id)
    {        
        $rrr = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arTourIB["ID"],"ID"=>$dtour_id,"ACTIVE"=>"Y"),false,false,Array("ID","NAME","PREVIEW_PICTURE","DETAIL_PICTURE","PROPERTY_tour","PROPERTY_widgetPath"));
        if ($rr = $rrr->Fetch())
        {
            $lookon_tour = str_replace("index.html", "start.swf", $rr["PROPERTY_WIDGETPATH_VALUE"]);
            $lookon_xml = str_replace("index.html", "start.xml", $rr["PROPERTY_WIDGETPATH_VALUE"]); 
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_TOUR"][$key] = $lookon_tour;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_XML"][$key] = $lookon_xml;
            $arResult["PROPERTIES"]["d_tour"]["LOOKON_IFRAME"][$key] = $rr["PROPERTY_TOUR_VALUE"];
            $arResult["PROPERTIES"]["d_tour"]["VALUE"][$key] = $rr["PREVIEW_PICTURE"];
        }
    }    
}
?>