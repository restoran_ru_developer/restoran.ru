<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?//if($USER->IsAdmin()):?>
<?
$arReviewsIB = getArIblock("reviews", CITY_ID);
?>
<?if ($_REQUEST["id"]&&count($arReviewsIB)):
    global $arrFilter;
    $arrFilter = array();

    if($_REQUEST['REST_NETWORK']){
        $REST_NETWORK = explode('|',$_REQUEST['REST_NETWORK']);
        $arrFilter['PROPERTY_ELEMENT'] = $REST_NETWORK;
    }
    else {
        $arrFilter["PROPERTY_ELEMENT"] = explode('|',$_REQUEST["id"]);
    }

//    FirePHP::getInstance()->info($arrFilter,'$arrFilter');
//    FirePHP::getInstance()->info($arReviewsIB,'$arReviewsIB');
//        FirePHP::getInstance()->info($arrFilter['PROPERTY_ELEMENT'],'PROPERTY_ELEMENT');
    $APPLICATION->IncludeComponent(
        "restoran:catalog.list",
        "reviews-network-design",
        Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "reviews",
                "IBLOCK_ID" => $arReviewsIB["ID"],//!$_REQUEST['REST_NETWORK'] ? array($arReviewsIB[0]["ID"],$arReviewsIB[1]["ID"]) :
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "created_date",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array("DATE_CREATE","CREATED_BY","DETAIL_PAGE_URL"),
                "PROPERTY_CODE" => array("ELEMENT","photos","video","COMMENTS"),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",//a
                "CACHE_TIME" => "3600010",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "PREVIEW_TRUNCATE_LEN" => 75,
                "URL" => $_REQUEST["url"],
                'IN_NETWORK' => $_REQUEST['REST_NETWORK'] ? 'Y':'N'
        ),
        false
    );
    if ($_REQUEST["wr"]!="Да")//&&$_REQUEST['id']!=2022840
    {
        $ELEMENT_ID = explode('|',$_REQUEST["id"]);
        if (!CSite::InGroup(Array(9))||CSite::InGroup(Array(1))):?>
            <div class="left-side" style="width: 100%">
            <?$APPLICATION->IncludeComponent(
                "restoran:comments_add_new",
                "with_rating_new_on_page_rest",
                Array(
                        "IBLOCK_TYPE" => "reviews",
                        "IBLOCK_ID" => $arReviewsIB["ID"],//$arReviewsIB[0]["ID"]
                        "ELEMENT_ID" => $ELEMENT_ID[0],
                        "IS_SECTION" => "N",                        
                ),false
            );?>
            </div>
        <?endif;
    }
endif;
?>
<?//else:?>
<!--    --><?//$arReviewsIB = getArIblock("reviews", CITY_ID);?>
<!--    --><?//if ($_REQUEST["id"]&&$arReviewsIB["ID"]):
//        global $arrFilter;
//        $arrFilter = array();
//        $REST_NETWORK = explode('|',$_REQUEST['REST_NETWORK']);
//        if($REST_NETWORK[0]){
//            $arrFilter = array(
//                array(
//                    "LOGIC" => "OR",
//                ),
//            );
//
//            $arrFilter[0][0] = array("PROPERTY_ELEMENT"=>$_REQUEST["id"]);
//            foreach ($REST_NETWORK as $rest_network) {
//                $arrFilter[0][] = array("PROPERTY_ELEMENT"=>$rest_network);
//            }
//        }
//        else {
//            $arrFilter["PROPERTY_ELEMENT"] = $_REQUEST["id"];
//        }
//        $APPLICATION->IncludeComponent(
//            "restoran:catalog.list",
//            "reviews-network-design",
//            Array(
//                "DISPLAY_DATE" => "Y",
//                "DISPLAY_NAME" => "Y",
//                "DISPLAY_PICTURE" => "Y",
//                "DISPLAY_PREVIEW_TEXT" => "Y",
//                "AJAX_MODE" => "N",
//                "IBLOCK_TYPE" => "reviews",
//                "IBLOCK_ID" => $arReviewsIB["ID"],
//                "NEWS_COUNT" => "5",
//                "SORT_BY1" => "created_date",
//                "SORT_ORDER1" => "DESC",
//                "SORT_BY2" => "SORT",
//                "SORT_ORDER2" => "ASC",
//                "FILTER_NAME" => "arrFilter",
//                "FIELD_CODE" => array("DATE_CREATE","CREATED_BY","DETAIL_PAGE_URL"),
//                "PROPERTY_CODE" => array("ELEMENT","photos","video","COMMENTS"),
//                "CHECK_DATES" => "N",
//                "DETAIL_URL" => "",
//                "PREVIEW_TRUNCATE_LEN" => "",
//                "ACTIVE_DATE_FORMAT" => "j F Y",
//                "SET_TITLE" => "N",
//                "SET_STATUS_404" => "N",
//                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                "ADD_SECTIONS_CHAIN" => "N",
//                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                "PARENT_SECTION" => "",
//                "PARENT_SECTION_CODE" => "",
//                "CACHE_TYPE" => "A",//a
//                "CACHE_TIME" => "3600010",
//                "CACHE_FILTER" => "Y",
//                "CACHE_GROUPS" => "N",
//                "DISPLAY_TOP_PAGER" => "N",
//                "DISPLAY_BOTTOM_PAGER" => "N",
//                "PAGER_TITLE" => "Новости",
//                "PAGER_SHOW_ALWAYS" => "N",
//                "PAGER_TEMPLATE" => "search_rest_list",
//                "PAGER_DESC_NUMBERING" => "N",
//                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                "PAGER_SHOW_ALL" => "N",
//                "AJAX_OPTION_JUMP" => "N",
//                "AJAX_OPTION_STYLE" => "Y",
//                "AJAX_OPTION_HISTORY" => "N",
//                "PREVIEW_TRUNCATE_LEN" => 75,
//                "URL" => $_REQUEST["url"],
//                'IN_NETWORK' => $_REQUEST['REST_NETWORK'] ? 'Y':'N'
//            ),
//            false
//        );
//        if ($_REQUEST["wr"]!="Да")
//        {
//            if (!CSite::InGroup(Array(9))||CSite::InGroup(Array(1))):?>
<!--                <div class="left-side" style="width: 100%">-->
<!--                    --><?//$APPLICATION->IncludeComponent(
//                        "restoran:comments_add_new",
//                        "with_rating_new_on_page_rest",
//                        Array(
//                            "IBLOCK_TYPE" => "reviews",
//                            "IBLOCK_ID" => $arReviewsIB["ID"],
//                            "ELEMENT_ID" => $_REQUEST["id"],
//                            "IS_SECTION" => "N",
//                        ),false
//                    );?>
<!--                </div>-->
<!--            --><?//endif;
//        }
//    endif;
//    ?>
<?//endif?>