<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $arSimilar;
$arSimilar["!ID"] = $_REQUEST['restoran'];
$arSimilar["PROPERTY_rest_group"] = $_REQUEST["id"];
$arIB = getArIblock("catalog", CITY_ID);
$APPLICATION->IncludeComponent("restoran:catalog.list", 'restoran_group_detail_slider',array(
    "IBLOCK_TYPE" => 'system',
    "IBLOCK_ID" => $arIB["ID"],
    "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"]?$_REQUEST["CATALOG_ID"]:$_REQUEST["catalog_id"],
    "NEWS_COUNT" => '10',
    "SORT_BY1" => "property_sleeping",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "SORT",
    "SORT_ORDER2" => "ASC",
    "FILTER_NAME" => "arSimilar",
    "PROPERTY_CODE" => Array("RATIO"),
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_SHADOW" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "Y",//a
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "SET_TITLE" => "N",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "ADD_SECTIONS_CHAIN" => "Y",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "PAGER_TITLE" => "Рестораны",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "NO_SLEEP" => "Y",
    "REST_PROPS"=>"Y"
),
    false
);
?>

