<div align="center">
    <?if (count($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"])>1):?>
        <?$mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0];?>
    <?else:
        $mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"][0];
    endif;?>
    <script type="text/javascript">
        function GetMap ()
        {
            YMaps.jQuery ( function () {
                map = new YMaps.Map ( YMaps.jQuery ( "#map_area" )[0] );
                var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
                var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
                var typeControl = new YMaps.TypeControl ( [] );
                typeControl.addType ( MTmap );
                typeControl.addType ( MTsat );

                map.setMinZoom (12);
                map.addControl (typeControl);
                map.addControl(new YMaps.Zoom());
                YMaps.Events.observe ( map, map.Events.Update, function () {
                        ShowMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.TypeChange, function () {
                        hideMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                    ShowMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                    hideMyBalloon ();
                } );
                YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                    hideMyBalloon ();
                } );
                    YMaps.Events.observe ( map, map.Events.Click, function () {
                    if(map.getZoom()<=16)
                        hideMyBalloon ();
                });
            })
            my_style = new YMaps.Style();
               my_style.iconStyle = new YMaps.IconStyle();
               my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
               my_style.iconStyle.size = new YMaps.Point(27, 32);
               my_style.iconStyle.offset = new YMaps.Point(-15, -32);
        }

        function SetMapCenter ( lat, lng, zoom_i )
        {
            zoom = zoom_i ? zoom_i : 16;
            map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
        }

        function ShowMyBalloon (id)
        {
            if (typeof ( map_c[0] ) == 'undefined')
            {
                hideMyBalloon ();
                <?if($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]):
                    $metro123 = "<p class='metro_".CITY_ID."' style=margin-bottom:0px;>".(is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]) ? implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]) : $arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])."</p>";
                endif;?>
                var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
                jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'><?=$rest_name.$arResult["NAME"]?><?=$metro123?><br /><?=$arResult["PROPERTIES"]["address"]["VALUE"][0]?><br />Телефон: <?=$arResult["PROPERTIES"]["phone"]["VALUE"][0]?><br /><?=($arResult["PROPERTIES"]["opening_hours"]["VALUE"][0])?"Часы работы: ".$arResult["PROPERTIES"]["opening_hours"]["VALUE"][0]:""?></div><div class='balloon_tail'></div></div>");
                jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-10+"px");
                jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
            }
            else
            {
                hideMyBalloon ();

                var point = map.converter.coordinatesToLocalPixels(map_c[id].getGeoPoint () );
                jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'><?=$rest_name.$arResult["NAME"]?><br />"+markers_data[id].adres+"</div><div class='balloon_tail'> </div></div>");
                jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
                jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
            }
        }

        function hideMyBalloon ()
        {
            jQuery(".map_ballon").remove();
        }
        function showMeRest(point, id)
        {
            console.log(point);
            point = point.split(",");     
            map.setZoom(15,{smooth:true,position:new YMaps.GeoPoint(point[1], point[0]),centering:true,callback:function(){
                    map.setZoom(16,{smooth:true,position:new YMaps.GeoPoint(point[1], point[0]),centering:true});
            }});
            //map.panTo(new YMaps.GeoPoint(point[1], point[0]), {flying:0,callback:function(a){
                    //map.panTo(new YMaps.GeoPoint(point[1], point[0]), {flying:1});
                //      map.setZoom(16,{smooth:true});
                //}
            //});


            //SetMapCenter ( point[0], point[1], 16 );
            ShowMyBalloon(id);
        }
        function count_prs(obj)
        {
           var count = 0; 
           for(var prs in obj) 
           { 
        count++;
           } 
           return count; 
        }
    </script>
    <script type="text/javascript">
        var map = null;
        var map_markers = {};
        var markers_data = {};
        var map_filter = {};
        var hidden_marker = null;
        var sat_map_type = false;
        var map_type = 'default';
        var to_show = null;
        var cur_user = 0;
        var my_style = {};
        var map_c = {};

        window.onload = OnPageLoad;
        function OnPageLoad (){
            GetMap();                            
            <?if (count($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"])>1):?>
                <?foreach ($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"] as $key=>$coords):
                    if ($key==0)
                        $temp2 = explode(",",$coords);
                    $temp = explode(",",$coords);
                    ?>                                
                    map_c[<?=$key?>] = new YMaps.Placemark ( new YMaps.GeoPoint ( <?=$temp[1]?>, <?=$temp[0]?> ), { style: my_style, hasBalloon: false, db_id: <?=$key?>,hideIcon: false } );
                    map_c[<?=$key?>].id = <?=$key?>;
                    var data = {};
                    data.name = '<?= addslashes($arResult["NAME"])?>';
                    data.adres = '<?=$arResult["PROPERTIES"]["address"]["VALUE"][$key]?>';
                    markers_data[<?=$key?>] = data;

                <?endforeach;?>
                    console.log(markers_data);
            <?endif;?>
            if (typeof ( map_c[0] ) == 'undefined')
            {
                var coord = "<?=$mmm?>";
                coord = coord.split(",");
                SetMapCenter ( coord[0], coord[1], 16 );
                map_markers[0] = new YMaps.Placemark ( new YMaps.GeoPoint ( coord[1], coord[0] ), { style: my_style, hasBalloon: false, db_id: 0,hideIcon: false } );
                map.addOverlay ( map_markers[0] );
                ShowMyBalloon(0);
            }
            else
            {
                SetMapCenter ( '<?=$temp2[0]?>', '<?=$temp2[1]?>', 13 );
                for (var i=0;i<count_prs(map_c);i++)
                {
                    map.addOverlay (map_c[i]);
                    YMaps.Events.observe ( map_c[i], map_c[i].Events.Click, function ( m, e ) {
                        hideMyBalloon();
                        ShowMyBalloon(m.id);
                    } );
                    //ShowMyBalloon(i);
                }
                //hideMyBalloon();
            }
        }
    </script>
    <div id="map_area" style="width: 100%;margin: 0 auto;height: 400px;"></div>
</div>
<br /><br />