<?
//Oh, shit...
$p = 0;?>
<div id='props' class="props" itemscope itemtype="http://schema.org/Organization">    
    <?if ($arResult["DISPLAY_PROPERTIES"]["kitchen"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
                <div class="col-sm-4 ">
                    <div class="prop">        
                        <div class="name"><?=GetMessage("R_PROPERTY_kitchen")?>:</div>            
                        <div class="value">
                            <?if (is_array($arResult["DISPLAY_PROPERTIES"]["kitchen"])&&count($arResult["DISPLAY_PROPERTIES"]["kitchen"])>3):?>
                                <?
                                for ($i=0;$i<count($arResult["DISPLAY_PROPERTIES"]["kitchen"]["~VALUE"]);$i++) {
                                        $r_item = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["kitchen"]["VALUE"][$i]);
                                        $ar_item = $r_item->Fetch();														
                                if (!empty($ar_item["CODE"])){?><a class="url" href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/kitchen/<?=$ar_item["CODE"]?>/"><?}?><?=$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"][$i]?><?if (!empty($ar_item["CODE"])){?></a><?}
                                if ($i<count($arResult["DISPLAY_PROPERTIES"]["kitchen"]["~VALUE"]) - 1) echo ', ';
                                }?>
                            <?else:?>
                                <?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?>
                            <?endif;?>
                        </div>
                    </div>
                </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>
        <?$p++;?>    
    <?endif;?>   
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["average_bill"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_average_bill")?>:</div>                
                    <div class="value">
                        <?$ab = strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))?>                                                
                        <?if (CITY_ID=="tmn"):
                            $ab = str_replace("до 1000р","500 - 1000р",$ab);
                            echo $ab;
                        else:?>
                            <?=$ab?>
                        <?endif;?>                                                
                    </div>
                </div>
            </div>
        <?if ($p%3==2&&$p!=2):?>
            </div>
        <?endif;?>      
        <?$p++;?>                    
    <?endif;?>         
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["subway"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop wsubway"> 
                    <?if (CITY_ID!="urm"):?>
                        <div class="name"><?=GetMessage("R_PROPERTY_subway")?>:</div>
                    <?else:?>
                        <div class="name"><?=GetMessage("R_PROPERTY_subway_urm")?>:</div>
                    <?endif;?>
                    <?if (CITY_ID!="urm"):?>
                    <div class="subway">M</div>
                    <?endif?>
                    <div class="value">                    
                         <?if ($arResult["ID"]==387287):?>
                            <p class="metro_<?=CITY_ID?>"><?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))?></p>
                        <?else:?>
                            <?if (is_array($arResult["DISPLAY_PROPERTIES"]["subway"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"])>3):?>
                                <p class="metro_<?=CITY_ID?>"><?
                                        $r_item = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["subway"]["~VALUE"][0]);
                                        $ar_item = $r_item->Fetch();														
                                if (!empty($ar_item["CODE"])){?><a class="url" href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/metro/<?=$ar_item["CODE"]?>/"><?}?><?=$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]?><?if (!empty($ar_item["CODE"])){?></a><?}?></p>
                            <?else:?>
                                <p class="metro_<?=CITY_ID?>"><?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))?></p>
                            <?endif;?>
                        <?endif;?>
                    </div>
                </div>
            </div> 
        <?if ($p%3==2):?>
            </div>
        <?endif;?>        
        <?$p++;?>    
    <?endif;?>
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["VALUE"])==1):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_opening_hours")?>:</div>                
                    <div class="value">            
                        <?$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0] = str_replace(",", ", ", $arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0]);?>
                        <?$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0] = str_replace("-", " - ", $arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0]);?>
                        <?=implode(", ",$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"])?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>  
        <?$p++;?>    
    <?endif;?>
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"])==1):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_number_of_rooms")?>:</div>                
                    <div class="value">                        
                        <?
                        if (is_array($arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"])):
                            echo $arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"][0];
                        else:
                            echo $arResult["DISPLAY_PROPERTIES"]["number_of_rooms"]["VALUE"];
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>  
        <?$p++;?>
    <?endif;?>
    <?if ($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])==1):
        ?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 " itemscope itemtype="http://schema.org/PostalAddress">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_address")?>:</div>                    
                    <div class="value" itemprop="streetAddress">

                        <?if($arResult['SHOW_YA_STREET']&&$arResult["DISPLAY_PROPERTIES"]["STREET"]["DISPLAY_VALUE"]):?>
                            <?if(!preg_match('/\/'.$arParams['TO_LINKS_CATALOG_ID'].'\//',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'])){
                                if(preg_match('/banket/',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'])){
                                    $arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'] = preg_replace('/banket/','restaurants',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']);
                                }
                                else {
                                    $arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL'] = preg_replace('/restaurants/','banket',$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']);
                                }
                            }?>
                            <a href="<?=$arResult["DISPLAY_PROPERTIES"]["STREET"]['LINK_ELEMENT_VALUE'][$arResult["DISPLAY_PROPERTIES"]["STREET"]['VALUE']]['DETAIL_PAGE_URL']?>"><?=str_replace('г.','',is_array($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]))?></a>
                        <?else:?>
                            <?=str_replace('г.','',is_array($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]))?>
                        <?endif?>

                        <?
//                        if (is_array($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])):
//                            echo $arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"][0];
//                        else:
//                            echo $arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"];
//                        endif;
                        ?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;?>
            
    <?if ($arResult["DISPLAY_PROPERTIES"]["type"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["type"]["VALUE"])==1):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_type")?>:</div>                    
                    <div class="value">                           
                        <?
                        if (is_array($arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):
                            echo strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]));
                        else:
                            echo strip_tags($arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]);
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;?>
    
    <?if ($arResult["PROPERTIES"]["banket_average_bill"]["VALUE"]):?>
        <?if ($p==0||($p%3==0&&$p!=2)):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_banket_average_bill")?>:</div>                
                    <div class="value">                    
                        <?=$arResult["PROPERTIES"]["banket_average_bill"]["VALUE"]?>                                                
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;?> 
    
    
    
    
                
    <?/*if ($arResult["DISPLAY_PROPERTIES"]["area"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">                     
                    <div class="name"><?=GetMessage("R_PROPERTY_area")?>:</div>                                    
                    <div class="value">
                        <?if (is_array($arResult["DISPLAY_PROPERTIES"]["area"])&&count($arResult["DISPLAY_PROPERTIES"]["area"])>3):?>
                        <?
                                                                                        for ($i=0;$i<count($arResult["DISPLAY_PROPERTIES"]["area"]["~VALUE"]);$i++) {
                                                                                                $r_item = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["area"]["~VALUE"][$i]);
                                                                                                $ar_item = $r_item->Fetch();														
                                                                                        if (!empty($ar_item["CODE"])){?><a class="url" href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/rajon/<?=$ar_item["CODE"]?>/"><?}?><?=$arResult["DISPLAY_PROPERTIES"]["area"]["DISPLAY_VALUE"][$i]?><?if (!empty($ar_item["CODE"])){?></a><?}?>
                                                                                        <?
                                                                                        if ($i<count($arResult["DISPLAY_PROPERTIES"]["area"]["~VALUE"]) - 1) echo ', ';
                                                                                        }?></p>
                            <?else:?>
                                <?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["area"]["DISPLAY_VALUE"]))?>
                            <?endif;?>

                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;*/?>            
                
    <?if ($arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">                     
                    <div class="name"><?=GetMessage("R_PROPERTY_phone")?>:</div>                                    
                    <div class="value">                                        
                        <?if ($arResult["ID"]!=387287):?>
                            <?if ($_REQUEST["CONTEXT"]=="Y"):?>
                                <?if (CITY_ID=="spb"):?>
                                    <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                <?else:?>
                                    <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                <?endif;?>   
                            <?else:?>

                                                <?
                                                if ($arResult["ID"] == 387180) {
                                                    $temp_phones = $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"];
                                                    $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = Array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"][0]);
                                                }
                                                $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = str_replace(";", "", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);

                                                if (is_array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]))
                                                    $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"] = implode(", ", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                else {
                                                    $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"] = $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"];
                                                    //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
                                                }
                                                $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"] = explode(",", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"]);

                                                $reg = "/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                                preg_match_all($reg, $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE2"], $matches);


                                                $TELs = array();
                                                for ($p = 1; $p < 5; $p++) {
                                                    foreach ($matches[$p] as $key => $v) {
                                                        //if($p==1 && $v!="") $TELs[$key].="+7";
                                                        $TELs[$key].=$v;
                                                    }
                                                }

                                                foreach ($TELs as $key => $T) {
                                                    if (strlen($T) > 7)
                                                        $TELs[$key] = "+7" . $T;
                                                }
                                                $p_counter = 0;
                                                foreach ($TELs as $key => $T) {
                                                    ?>
                                                    <a href="tel:<?= $T ?>" class="<? echo MOBILE; ?>"><?= $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE3"][$key] ?></a><? if (isset($TELs[$key + 1]) && $TELs[$key + 1] != "") echo ',<br /> '; ?>
                                                    <?
                                                    if (is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]) > 5):
                                                        break;
                                                    endif;
                                                    ?>
                                                <? }
                                                ?>
                                                <?
                                                if ($arResult["ID"] == 387180) {
                                                    $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = $temp_phones;
                                                }
                                                ?>
                            <? endif; ?>
                        <? endif; ?>
                        </div>
                </div>
            </div>
                <?/*if ($arResult["ID"]==387287):?>
                        <tr>
                            <td colspan="2" align="right">
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] as $key=>$ph_bizon):
                                    echo $ph_bizon." (".str_replace("Москва, ","",$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"][$key]).")<Br />";
                                endforeach;?>
                            </td>
                        </tr>
                        <?endif;?>
                <?endif;*/?>             
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;?>   
              
        
    
    <div id="more_properties">        
        <?
        $i=0;
        if (is_array($arResult["DISPLAY_PROPERTIES"]["sale10"]))
            unset($arResult["DISPLAY_PROPERTIES"]["sale10"]);        
        foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):
        
        if($arProperty["DISPLAY_VALUE"]&&$arProperty["CODE"]!="address"&&$arProperty["CODE"]!="type"&&$arProperty["CODE"]!="RATIO"&&$arProperty["CODE"]!="COMMENTS"&&$arProperty["CODE"]!="map"&&$arProperty["CODE"]!="add_props"&&$arProperty["CODE"]!="landmarks"&&$arProperty["CODE"]!="number_of_rooms"&&$arProperty["CODE"]!="phone"&&$arProperty["CODE"]!="subway"&&$arProperty["CODE"]!="average_bill"&&$arProperty["CODE"]!="kitchen"&&$arProperty["CODE"]!="opening_hours"&&$arProperty["CODE"]!="d_tours"&&$arProperty["CODE"]!="rest_group"&&$arProperty["CODE"]!="place_new_rest"):?>                            
            <?=($i%3==0)?"<div class='row'>":""?>
                <div class="col-sm-4 ">
                    <div class="prop">                                
                        <?if($pid=="address"&&$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):?>
                        <?else:?>
                            <div class="name"><?=GetMessage("R_PROPERTY_".$arProperty["CODE"])?>:</div>
                        <?endif;?>
                            <div class="value">
                                <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                        <?if ($pid=="site"):?>
                                            <?foreach($arProperty["DISPLAY_VALUE"] as &$value):
                                                $pattern = "/(<a href=\"\/bitrix\/redirect.php?)([^>]*)>/";
                                                $replacement = "$1$2 onclick=\"this.target='_blank'\">";
                                                $site[] = preg_replace($pattern, $replacement, $value);
                                            endforeach;?>                                                                
                                            <?=implode(", ", $site);?>
                                        <?elseif($pid=="address"&&is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):?>

                                        <?elseif($pid=="email"):?>
                                            <?
                                                foreach($arProperty["DISPLAY_VALUE"] as $em)
                                                {
                                                    $email_temp = explode("@",$em)?>
                                                    <script>
                                                        document.write("<a href='mailto:"+"<?=$email_temp[0]?>"+"@"+"<?=$email_temp[1]?>"+"'>"+"<?=$email_temp[0]?>"+"@"+"<?=$email_temp[1]?>"+"</a>");
                                                    </script> 
                                                    <?
                                                }
                                            ?>
                                        <?else:?>      
                                            <?foreach($arProperty["DISPLAY_VALUE"] as &$value):
                                                    $value = strip_tags($value);
                                            endforeach;?>
                                            <?=implode(", ", $arProperty["DISPLAY_VALUE"]);?>
                                        <?endif;?>
                                <?else:?>
                                    <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                        <?if (CITY_ID=="spb"):?>
                                                    <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                                        <?else:?>
                                                    <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                                <?endif;?>  
                                    <?elseif ($pid=="email"):?>                                                                  
                                            <?
                                            $email_temp = explode("@",$arProperty["DISPLAY_VALUE"])?>
                                            <script>
                                                document.write("<a href='mailto:"+"<?=$email_temp[0]?>"+"@"+"<?=$email_temp[1]?>"+"'>"+"<?=$email_temp[0]?>"+"@"+"<?=$email_temp[1]?>"+"</a>");
                                            </script> 
                                    <?elseif($pid=="address"&&is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):?>

                                    <?else:?>
                                        <?if ($pid=="site"):?>
                                            <?
                                            $site = "";
                                            $link_value = $arProperty["DISPLAY_VALUE"];
                                            $pattern = "/(<a href=\"\/bitrix\/redirect.php?)([^>]*)>/";
                                            $replacement = "$1$2 onclick=\"this.target='_blank'\">";
                                            $site = preg_replace($pattern, $replacement, $link_value);
                                            ?>
                                            <?=$site?>
                                        <?else:?>
                                            <?=strip_tags($arProperty["DISPLAY_VALUE"]);?>
                                        <?endif;?>
                                    <?endif;?>
                                <?endif?>
                            </div>                                                                            
                            <?=($i%3==2)?"</div>":""?>
                            <?$i++?>
                            </div>
                </div>
                            <?endif;?>                                                      
                    <?endforeach;?>
                    <?=($i%3!=0)?"</div>":""?>
                    <?if ($arResult["DISPLAY_PROPERTIES"]["landmarks"]["DISPLAY_VALUE"]):?>
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="prop"> 
                                    <div class="name"><?=GetMessage("R_PROPERTY_".$arResult["DISPLAY_PROPERTIES"]["landmarks"]["CODE"])?>:</div>
                                    <div class="value"><?=$arResult["DISPLAY_PROPERTIES"]["landmarks"]["DISPLAY_VALUE"]?></div>
                                </div>
                            </div>
                        </div>                        
                    <?endif;?>
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="prop"> 
                                    <div class="name"><?=GetMessage("R_PROPERTY_".$arResult["DISPLAY_PROPERTIES"]["add_props"]["CODE"])?>:</div>
                                    <div class="value"><?=$arResult["DISPLAY_PROPERTIES"]["add_props"]["DISPLAY_VALUE"]?></div>
                                </div>
                            </div>
                        </div>                                   
    </div>    
    <div class="row">
        <div class="col-sm-4">
            <a href="javscript:void(0)" data-toggle="toggle" data-target="more_properties" class="btn btn-light">все характеристики</a>
        </div>
    </div>
</div>