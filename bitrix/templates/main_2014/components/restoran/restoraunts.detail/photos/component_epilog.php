<?php
/*$count = count($_COOKIE["RECENTLY_VIEWED"]["catalog"]);
if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["catalog"]))
    setcookie ("RECENTLY_VIEWED[catalog][".$count."]", $arResult["ID"], time() + 3600*12, "/");*/

global $element_id, $menu_id, $count, $similar_rest, $RGROUP, $menu_items;
$element_id = $arResult["ID"];
$menu_id = $arResult["MENU_ID"];
if (!$arResult["PREVIEW_TEXT"]&&!$arResult["DETAIL_TEXT"])
    $menu_items = 2;
else
    $menu_items = 5;
$similar_rest = $arResult["SIMILAR"];
$count["AFISHA_COUNT"] = $arResult["AFISHA_COUNT"];
$count["NEWS_COUNT"] = $arResult["NEWS_COUNT"];
$count["SPEC_COUNT"] = $arResult["SPEC_COUNT"];
$count["BLOG_COUNT"] = $arResult["BLOG_COUNT"];
$count["OVERVIEWS_COUNT"] = $arResult["OVERVIEWS_COUNT"];
$count["PHOTO_COUNT"] = $arResult["PHOTO_COUNT"];
$count["CONTACTS"] = $arResult["CONTACTS"];
$count["LAT"] = $arResult["LAT"];
$count["LON"] = $arResult["LON"];
$RGROUP = $arResult["RGROUP"];

//$ratio = '<div class="name_ratio">';
//for($i = 1; $i <= 5; $i++):
//    if ($i > $arResult["RATIO"])
//        $ratio .= '<span class="icon-star-empty"></span>';
//    else
//        $ratio .= '<span class="icon-star"></span>';
//endfor;
//$ratio .= '</div>';
$APPLICATION->AddViewContent("restoran_name_view_content", $arResult["NAME"]);
global $APPLICATION;
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
//$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);

if($_REQUEST["CONTEXT"]=="Y"){
	//$_SESSION["CONTEXT"]="Y";
	//var_dump($APPLICATION->get_cookie("CONTEXT"));
	if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
}
//if ($USER->IsAdmin())

if(CITY_ID=='spb'){
    $title_city_name = 'в Санкт-Петербурге';
    $title_city_name_keyword = 'Санкт-Петербург';
}
elseif(CITY_ID=='msk'){
    $title_city_name = 'в Москве';
    $title_city_name_keyword = 'Санкт-Петербург';
}
elseif(CITY_ID=='rga'){
    $title_city_name = 'в Риге';
    $title_city_name_keyword = 'Рига';
}
elseif(CITY_ID=='urm'){
    $title_city_name = 'в Юрмале';
    $title_city_name_keyword = 'Юрмала';
}
elseif(CITY_ID=='tmn'){
    $title_city_name = 'в Тюмени';
    $title_city_name_keyword = 'Тюмень';
}
elseif(CITY_ID=='kld'){
    $title_city_name = 'в Калининграде';
    $title_city_name_keyword = 'Калининград';
}
elseif(CITY_ID=='krd'){
    $title_city_name = 'в Краснодаре';
    $title_city_name_keyword = 'Краснодар';
}
elseif(CITY_ID=='sch'){
    $title_city_name = 'в Сочи';
    $title_city_name_keyword = 'Сочи';
}
elseif(CITY_ID=='ufa'){
    $title_city_name = 'в Уфе';
    $title_city_name_keyword = 'Уфа';
}
elseif(CITY_ID=='ast'){
    $title_city_name = 'в Астане';
    $title_city_name_keyword = 'Астана';
}
$photos_num = count($arResult["PROPERTIES"]["photos"]["VALUE_SMALL"]);
$APPLICATION->SetPageProperty("title",  "Фотографии ресторана '".$arResult["NAME"]."' $title_city_name. $photos_num фото.");
$APPLICATION->SetPageProperty("keywords",  "фото, фотография, ресторан, '".$arResult["NAME"]."', $title_city_name_keyword");
$APPLICATION->SetPageProperty("description",  "Фотографии ($photos_num фото) ресторана '".$arResult["NAME"]."' $title_city_name.");
    //$APPLICATION->SetTitle();
?>
<script>
$(document).ready(function(){
    
    <?if(CSite::InGroup( array(14,15,1,23,24,29,28,30))):?>  
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                $("#for_edit_link").append('<div id="edit_link" style="position:absolute; top:-155px; right:10px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');                            
            <?else:?>
                $("#for_edit_link").append('<div id="edit_link" style="position:absolute; top:-45px; right:10px;"><a class="no_border" href="/redactor/edit.php?ID=<?=$arResult["ID"]?>&CITY_ID=<?=CITY_ID?>">Редактировать</a></div>');                            
            <?endif;?>
    <?endif;?>
    
    <?if ($_REQUEST["bron"]=="Y"):?>
        $('html,body').animate({scrollTop:600}, "300");
        $.ajax({
                type: "POST",
                url: "/tpl/ajax/online_order_rest.php",
                data: "what="+$("#order_what").val()+"&date="+$(".whose_date").val()+"&name=<?=$arResult["NAME"]?>&id=<?=$arResult["ID"]?>&time="+$(".time").val()+"&person="+$("#person").val()+"&<?=bitrix_sessid_get()?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                    }
                    $('#bron_modal').html(data);
                    showOverflow();
                    setCenter($("#bron_modal"));
                    $("#bron_modal").fadeIn("300"); 
                }
            });
    <?endif;?>
    <?if ($_REQUEST["bron_banket"]=="Y"):?>
        $('html,body').animate({scrollTop:600}, "300");
        $.ajax({
                type: "POST",
                url: "/tpl/ajax/online_order_rest.php",
                data: "what=2&date="+$(".whose_date").val()+"&name=<?=$arResult["NAME"]?>&id=<?=$arResult["ID"]?>&time="+$(".time").val()+"&person="+$("#person").val()+"&<?=bitrix_sessid_get()?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                    }
                    $('#bron_modal').html(data);
                    showOverflow();
                    setCenter($("#bron_modal"));
                    $("#bron_modal").fadeIn("300"); 
                }
            });
    <?endif;?>
});
</script>
<?
					//if ($USER->IsAdmin()) {
// костыль. Принять данные из $_REQUEST, которые пришли из формы на Яндексе (острова)

//$arResult["arrVALUES"]["form_text_64"]
if (!empty($_REQUEST["form_text_32"])) {
$arr_name = explode('~',$_REQUEST["form_text_32"]);
// имя и id ресторана
	//$arResult["arrVALUES"]["form_text_32"] = $_REQUEST["form_text_32"].'';
	//$arResult["NAME"] = $_REQUEST["form_text_32"].'';
	?>
<input type="hidden" name="frm_form_text_32" id="frm_form_text_32" value="<?=$arr_name[0]?>" />
<input type="hidden" name="rcurrent_id" id="rcurrent_id" value="<?=$arr_name[1]?>" />
	<?
}

if (!empty($_REQUEST["form_text_39"])) {
// фио
	?>
<input type="hidden" name="frm_form_text_39" id="frm_form_text_39" value="<?=$_REQUEST["form_text_39"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_40"])) {
// телефон
	?>
<input type="hidden" name="frm_form_text_40" id="frm_form_text_40" value="<?=$_REQUEST["form_text_40"]?>" />
	<?
}

if (!empty($_REQUEST["form_email_41"])) {
// E-mail
	?>
<input type="hidden" name="frm_form_email_41" id="frm_form_email_41" value="<?=$_REQUEST["form_email_41"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_35"])) {
// персон
	?>
<input type="hidden" name="frm_form_text_35" id="frm_form_text_35" value="<?=$_REQUEST["form_text_35"]?>" />
	<?
}

if (!empty($_REQUEST["form_checkbox_smoke"])) {
// курящий / не курящий
	?>
<input type="hidden" name="frm_form_checkbox_smoke" id="frm_form_checkbox_smoke" value="<?=$_REQUEST["form_checkbox_smoke"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_34"])) {
// time
	?>
<input type="hidden" name="frm_form_text_34" id="frm_form_text_34" value="<?=$_REQUEST["form_text_34"]?>" />
	<?
}

if (!empty($_REQUEST["form_text_33"])) {
// date
$curDate = $_REQUEST["form_text_33"];
$curDate = preg_replace("/^([\d]{2}+)([\d]{2}+)/is","$1.$2.",$curDate);
	?>
<input type="hidden" name="frm_form_text_33" id="frm_form_text_33" value="<?=$curDate?>" />
<input type="hidden" name="frm_form_date_33" id="frm_form_date_33" value="<?=$curDate?>" />
	<?
}
?>