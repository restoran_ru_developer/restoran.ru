<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    </div>
</div>


<?
//print_r($arResult);
//FirePHP::getInstance()->info($arResult);
if ($arResult["SECTION"]["PATH"][0]["CODE"]=="restaurants"):
    $rest_name = GetMessage("R_REST_from_little")." ";
    $rest_name_title = GetMessage("R_REST_MULTIPLE");
else:
    $rest_name = GetMessage("R_BH_from_little")." ";
    $rest_name_title = GetMessage("R_BH_MULTIPLE");
endif;

if(CITY_ID=='spb'){
    $title_city_name = 'в Санкт-Петербурге';
    $title_city_name_keyword = 'Санкт-Петербург';
}
elseif(CITY_ID=='msk'){
    $title_city_name = 'в Москве';
    $title_city_name_keyword = 'Санкт-Петербург';
}
elseif(CITY_ID=='rga'){
    $title_city_name = 'в Риге';
    $title_city_name_keyword = 'Рига';
}
elseif(CITY_ID=='urm'){
    $title_city_name = 'в Юрмале';
    $title_city_name_keyword = 'Юрмала';
}
elseif(CITY_ID=='tmn'){
    $title_city_name = 'в Тюмени';
    $title_city_name_keyword = 'Тюмень';
}
elseif(CITY_ID=='kld'){
    $title_city_name = 'в Калининграде';
    $title_city_name_keyword = 'Калининград';
}
elseif(CITY_ID=='krd'){
    $title_city_name = 'в Краснодаре';
    $title_city_name_keyword = 'Краснодар';
}
elseif(CITY_ID=='sch'){
    $title_city_name = 'в Сочи';
    $title_city_name_keyword = 'Сочи';
}
elseif(CITY_ID=='ufa'){
    $title_city_name = 'в Уфе';
    $title_city_name_keyword = 'Уфа';
}
elseif(CITY_ID=='ast'){
    $title_city_name = 'в Астане';
    $title_city_name_keyword = 'Астана';
}
?>
<div class='detail_tabs' id='photos_tab'>
    <div id="rest-<?=$arResult["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval='7000'>
        <div class="carousel-inner-wrapper">
            <div class="carousel-inner">
                <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>
                    <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>
                        <?if ($key==0):?>
                            <div class="item active" data-num="<?=$key?>">
                                <img src="<?=$photo["src"]?>" alt="<?=$rest_name.' '.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0]:""?>" title="Фотографии <?=$rest_name_title?> '<?=$arResult["NAME"]?>'. Фото №<?=($key+1)?>." />
                            </div>
                        <?else:?>
                            <div class="item"  data-num="<?=$key?>">
                                <img src="<?=$photo["src"]?>" data-url="<?=$photo["src"]?>" alt="<?=$rest_name.' '.$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][0]:""?>" title="Фотографии <?=$rest_name_title?> '<?=$arResult["NAME"]?>'. Фото №<?=($key+1)?>.">
                            </div>
                        <?endif;?>
                    <?endforeach;?>
                <?else:?>
                    <?if ($arResult["PREVIEW_PICTURE"]["SRC"]):?>
                        <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" title="Фотографии <?=$rest_name_title?> '<?=$arResult["NAME"]?>'. Фото №1."/>
                    <?else:?>
                        <img src="/tpl/images/noname/rest_nnm_new.png" width="380" />
                    <?endif;?>
                <?endif;?>
            </div>
            <?if (count($arResult["PROPERTIES"]["photos"]["VALUE"])>1):?>
                <a class="left carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="prev">
                    <span class="galery-control galery-control-left icon-arrow-left2"></span>
                </a>
                <a class="right carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="next">
                    <span class="galery-control galery-control-right icon-arrow-right2"></span>
                </a>
            <?endif;?>
        </div>

    </div>
<br />
    <div class="block">
        <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>
            <div class="thumbnail_row">
                <div><b>Фото:</b>
                    <Br/>
                    <span class="now-count" >1</span> из <?=count($arResult["PROPERTIES"]["photos"]["VALUE2"])?>
                </div>
                <?foreach($arResult["PROPERTIES"]["photos"]["VALUE_SMALL"] as $key=>$photo):?>
                    <div class="th <?=($key==0)?"active":""?>" data-num="<?=$key?>"><img src="<?=$photo["src"]?>" alt="..."></div>
                <?endforeach;?>
            </div>
        <?endif;/**/?>
        <script>
        $(function(){
            $('#rest-<?=$arResult["ID"]?>').on('slide.bs.carousel', function (e) {
                //console.log(e);
                $(".thumbnail_row div.active").removeClass("active");
                $(".thumbnail_row .th").eq($(e.relatedTarget).data("num")).addClass("active");
                $('.now-count').text($(e.relatedTarget).data("num")+1);

            });
            $(".thumbnail_row .th").click(function(){
                var a =$(this).data("num")*1;
//                $('#rest-<?//=$arResult["ID"]?>// .item').eq(a).find("img").attr('src', $('#rest-<?//=$arResult["ID"]?>// .item').eq(a).find("img").data('url'));
                $('#rest-<?=$arResult["ID"]?>').carousel(a);
            });
            //show/hide big text
            $('a.toglletext').click(function(){
                $(".description .hid").toggle();
            });
        });
        </script>
    </div>
</div>

<div>
    <div class="block">
        <div class="description desc-under-photos">
            <span id='t' class="hid" ><noindex><?=$arResult["DETAIL_TEXT"]?></noindex></span>
        </div>
        <a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="btn btn-info btn-nb-empty show-this-place-photos-link" title="Ресторан '<?=$arResult['NAME']?>' <?=$title_city_name?>">перейти на страницу ресторана</a>
<!--        <div class="show-this-place-photos-link">перейти на страницу ресторана</div>-->
