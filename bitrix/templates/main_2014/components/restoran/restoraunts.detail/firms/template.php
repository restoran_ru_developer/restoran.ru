<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($_REQUEST["CONTEXT"]=="Y"):?>
    <script>
        $(document).ready(function(){
                $('html,body').animate({scrollTop:"602px"},500);
        });
    </script>
<?endif;?>
<script>        
    $(window).load(function() {        
        var map_load = 0;
        $("#map_load").click(function(){
            if (!map_load)
            {
                setTimeout("OnPageLoad()",100);
                map_load++;
            }
        });
        //minus_plus_buts     
        $("#minus_plus_buts").on("click","a.plus", function(event){
                var obj = $(this);
                $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
                        if(parseInt(otvet))
                        {
                                $(".all").html($(".all").html()*1+1);
                                obj.html(otvet);
                        }
                        else
                        {                                                
                            $('#minus_plus_buts .plus').tooltip({'title':otvet,'trigger':"manual",'placement':'left'});
                            $('#minus_plus_buts .plus').tooltip("show");
                            setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);                        
                        }
                });
                return false;
        });

        $("#minus_plus_buts").on("click","a.minus", function(event){
            var obj = $(this);
            $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet)
            {
                if(parseInt(otvet)){                            
                    $(".all").html($(".all").html()*1+1);
                    obj.html(otvet);
                }else{
                    $('#minus_plus_buts .minus').tooltip({'title':otvet,'trigger':"manual",'placement':'right'});
                    $('#minus_plus_buts .minus').tooltip("show");
                    setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);
                }
            });
            return false;
        });

        $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){        
            $("#minus_plus_buts").html(otvet);
        }); 
    });
</script>  
<div class="sort anchors-list">
    <a href="#photos_tab" data-toggle='detail_tabs' class="asc">фотогалерея</a>
    <?if ($arResult["PROPERTIES"]["d_tours"]["VALUE"]):?>
        <a href="#d3d_tab" id="d3dload" data-toggle='detail_tabs'><span class="i3d"></span>3d-тур</a>
    <?endif;?>
    <a href="#props" data-toggle='anchor'>контакты</a>
    <?if($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]):?>
        <a href="#map_tab" id="map_load" data-toggle='detail_tabs'>на карте</a>   
    <?endif;?>
    <?if ($arResult["NEWS_COUNT"]):?>
        <a href="#news_link" data-toggle='anchor'>новости</a>
    <?endif;?>    
    <?if ($arResult["SPEC_COUNT"]):?>
        <a href="#podborki" data-toggle='anchor'>события</a>
    <?endif;?>    
</div>
<div class="clearfix"></div>
<div class='detail_tabs' id='photos_tab'>
    <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/photos.php");?>
</div>
<div class='detail_tabs' id='map_tab'>
    <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/map.php");?>
</div>
<div class="article_likes">
    <div class="pull-left likes-buttons" id="minus_plus_buts"></div>
    <div class="pull-right">
        <?$APPLICATION->IncludeComponent(
            "bitrix:asd.share.buttons",
            "likes",
            Array(
                "ASD_TITLE" => $APPLICATION->GetTitle(false),
                "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                "ASD_PICTURE" => "",
                "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                "LIKE_TYPE" => "LIKE",
                "VK_API_ID" => "2881483",
                "VK_LIKE_VIEW" => "mini",
                "SCRIPT_IN_HEAD" => "N"
            )
        );?>
    </div>
</div>      
<?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/props.php");?>
<?if ($arResult["DETAIL_TEXT"]):?>
    <div class="description">
        <?=$arResult["DETAIL_TEXT"]?>        
        <!--<span id='t' class="hid" ><?=$arResult["PREVIEW_TEXT"]?></span>
        <span id='t2' class="hid" style="display:none"><?=$arResult["DETAIL_TEXT"]?></span>    -->
    </div>                 
    <p id="tr">...</p>
    <div class="text-right">        
        <a class="btn btn-light " onclick="$('.description').addClass('open_d'); $('#tr').hide(); $(this).hide().next().show()"><?=GetMessage("R_MORE_TEXT_1")?></a>
        <a class="btn btn-light " onclick="$('.description').removeClass('open_d'); $('#tr').show(); $(this).hide().prev().show()" style="display: none"><?=GetMessage("R_MORE_TEXT_2")?></a>
    </div>
    <br />
<?endif;?>               