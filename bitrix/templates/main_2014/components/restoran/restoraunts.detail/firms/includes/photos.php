 
<div id="rest-<?=$arResult["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval='7000'>

<!--    <div class="photo_num">-->
<!--        Фото <span>1</span> из --><?//=(!count($arResult["PROPERTIES"]["photos"]["VALUE2"]))?"1":count($arResult["PROPERTIES"]["photos"]["VALUE2"])?><!--        -->
<!--    </div>-->
<!--    <div class="hexagon-black photo_ratio">-->
<!--        --><?//=GetMessage("R_RATIO")?>
<!--        <span>--><?//=sprintf("%01.2f", $arResult["PROPERTIES"]["RATIO"]["VALUE"]);?><!--</span>-->
<!--    </div>-->

        <div class="carousel-inner">            
            <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>  
                <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>

                    <div class="item <?if($key==0)echo 'active';?>"  data-num="<?=$key?>">
                        <div class="inline-wrapper">

                            <a href="<?=$arResult["MULTIPLE_PHOTO_ARR"][$key]['src']?>" class="fancy" rel="detail_group" this_rest_phone="ТЕЛЕФОН РЕСТОРАНА:<br><?=$TELs?>">
                                <img <?if($key==0):?>src="<?=$photo["src"]?>"<?endif;?> data-url="<?=$photo["src"]?>" title='<?=GetMessage("R_REST")." ".$arResult["NAME"]." - фотография №".($key+1)?>' alt="<?=GetMessage("R_REST")." ".$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key]:""?>">
                            </a>                            
                            <div class="photo_num">
                                Фото <span><?=$key+1?></span> из <?=(!count($arResult["PROPERTIES"]["photos"]["VALUE2"]))?"1":count($arResult["PROPERTIES"]["photos"]["VALUE2"])?>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
            <?else:?>
                <?if ($arResult["PREVIEW_PICTURE"]["SRC"]):?>
                    <div class="inline-wrapper">
                        <a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" class="fancy" rel="detail_group">
                            <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>"/>
                        </a>                        
                        <div class="photo_num">
                            Фото <span><?=$key+1?></span> из <?=(!count($arResult["PROPERTIES"]["photos"]["VALUE2"]))?"1":count($arResult["PROPERTIES"]["photos"]["VALUE2"])?>
                        </div>
                    </div>
                <?else:?>
                    <img src="/tpl/images/noname/rest_nnm_new_new.png" width="" />
                <?endif;?>
            <?endif;?>                
        </div>
        <?if (count($arResult["PROPERTIES"]["photos"]["VALUE"])>1):?>
            <a class="left carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="prev">
              <span class="galery-control galery-control-left icon-arrow-left2"></span>
            </a>
            <a class="right carousel-control" href="#rest-<?=$arResult["ID"]?>" role="button" data-slide="next">
              <span class="galery-control galery-control-right icon-arrow-right2"></span>
            </a>
        <?endif;?>
    </div>
<br />
    <?/*if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?> 
        <div class="thumbnail_row">
            <div><b>Фото:</b>
                <Br/> 
                1 из <?=count($arResult["PROPERTIES"]["photos"]["VALUE2"])?>
            </div>
            <?foreach($arResult["PROPERTIES"]["photos"]["VALUE_SMALL"] as $key=>$photo):?>
                <div class="th <?=($key==0)?"active":""?>" data-num="<?=$key?>"><img src="<?=$photo["src"]?>" alt="..."></div>
            <?endforeach;?>    
        </div>
    <?endif;*/?>    
    <script>
    $(function(){
//        $('#rest-<?//=$arResult["ID"]?>//').on('slide.bs.carousel', function (e) {
//            var max = <?//=count($arResult["PROPERTIES"]["photos"]["VALUE2"])?>//;
//            var a = 0;
//            if (e.direction=="left")
//            {
//                a = $(".photo_num span").text()*1+1;
//                if (a > max)
//                    $(".photo_num span").text(1);
//                else
//                    $(".photo_num span").text(a);
//            }
//            if (e.direction=="right")
//            {
//                a = $(".photo_num span").text()*1-1;
//                if (a == 0)
//                    $(".photo_num span").text(max);
//                else
//                    $(".photo_num span").text(a);
//            }
//        });
//        $(".thumbnail_row .th").click(function(){
//            var a =$(this).data("num")*1;                        
//            $('#rest-<?//=$arResult["ID"]?> .item').eq(a).find("img").attr('src', $('#rest-<?//=$arResult["ID"]?> .item').eq(a).find("img").data('url'));
//            $('#rest-<?//=$arResult["ID"]?>').carousel(a);
//        });
        //show/hide big text    
        $('a.toglletext').click(function(){
            $(".description .hid").toggle();
        });
    });
    </script>