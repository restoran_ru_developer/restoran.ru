<script src="https://api-maps.yandex.ru/1.1/index.xml?key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript"></script>
    <?if ($arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]):?>
        <?$mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"];?>
    <?else:
        $mmm = $arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"];
    endif;?>
    <script type="text/javascript">
                        function GetMap ()
                        {
                            YMaps.jQuery ( function () {
                                map = new YMaps.Map ( YMaps.jQuery ( "#map_area" )[0] );
                                var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
                                var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
                                var typeControl = new YMaps.TypeControl ( [] );
                                typeControl.addType ( MTmap );
                                typeControl.addType ( MTsat );

                                map.setMinZoom (13);
                                map.addControl (typeControl);
                                map.addControl(new YMaps.Zoom());
                                YMaps.Events.observe ( map, map.Events.Update, function () {
                                        ShowMyBalloon (0);
                                } );
                                YMaps.Events.observe ( map, map.Events.TypeChange, function () {
                                        hideMyBalloon (0);
                                } );
                                YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                                    ShowMyBalloon (0);
                                } );
                                YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                                    hideMyBalloon ();
                                } );
                                YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                                    hideMyBalloon ();
                                } );
                            })
                            my_style = new YMaps.Style();
                               my_style.iconStyle = new YMaps.IconStyle();
                               my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
                               my_style.iconStyle.size = new YMaps.Point(27, 32);
                               my_style.iconStyle.offset = new YMaps.Point(-15, -32);
                        }

                        function SetMapCenter ( lat, lng, zoom_i )
                        {
                            zoom = zoom_i ? zoom_i : 16;
                            map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
                        }

                        function ShowMyBalloon (id)
                        {
                            hideMyBalloon ();
                            var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
                            jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'><?=$arResult["NAME"]?><br /><?=$arResult["PROPERTIES"]["address"]["VALUE"][0]?></div><div class='balloon_tail'> </div></div>");
                            jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
                            jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
                        }

                        function hideMyBalloon ()
                        {
                            jQuery(".map_ballon").remove();
                        }
                    </script>
                    <script type="text/javascript">
                        var map = null;
                        var map_markers = {};
                        var markers_data = {};
                        var map_filter = {};
                        var hidden_marker = null;
                        var sat_map_type = false;
                        var map_type = 'default';
                        var to_show = null;
                        var cur_user = 0;
                        var my_style = {};

                        window.onload = OnPageLoad;
                        function OnPageLoad (){
                            GetMap();
                            var coord = "<?=$arResult["DISPLAY_PROPERTIES"]["map"]["VALUE"]?>";
                            coord = coord.split(",");
                            SetMapCenter ( coord[0], coord[1], 16 );
                            map_markers[0] = new YMaps.Placemark ( new YMaps.GeoPoint ( coord[1], coord[0] ), { style: my_style, hasBalloon: false, db_id: 0,hideIcon: false } );
                            map.addOverlay ( map_markers[0] );
                            ShowMyBalloon(0);
                        }
                    </script>
    <div id="map_area" style="width: 100%;margin: 0 auto;height: 400px;margin-bottom:15px"></div>