$(document).ready(function(){
    $("body").on("click",".lookon_iframe",function(){
        if (!$("#lookon_iframe").size())
        {
            $("<div class='popup popup_modal' id='lookon_iframe'></div>").appendTo("body");                                                               
        }
        $('#lookon_iframe').css("width","85%");
        $('#lookon_iframe').css("height","80%");
        $('#lookon_iframe').html("<div class='modal_close_lookon'></div><iframe style='border:0px;' width='100%' height='100%' src='"+$(this).attr("href")+"'></iframe>");
        showOverflow();
        setCenter($("#lookon_iframe"));
        $("#lookon_iframe").show();  
        return false;
    });
    $("body").on("click",".modal_close_lookon",function(){
       hideOverflow();
       $("#lookon_iframe").hide();
    });
  
    $("#likes").prepend($("#likes_inv").html());
    $("#likes_inv").remove();
    $(".active_rating div").hover(
        function(){
            $(this).parent().find("div").attr("class","star");
            var count = $(this).attr("alt");
            var i = 1;
            $(this).parent().find("div").each(function(){                
                if (i<=count)
                    $(this).attr("class","star_a");
                else
                    return;
                i++;
            })            
        }
    );        
});