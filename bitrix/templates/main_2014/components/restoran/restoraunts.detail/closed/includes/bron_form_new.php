<div class="order-panel">
    <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
    <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=rawurlencode($arResult["NAME"])?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
    <div class="order-text">
        <? if (LANGUAGE_ID == "ru"): ?>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                <span class="another">Бронировать по телефону:</span><br />
                <?if (CITY_ID=="msk"):?>
                    <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                <?else:?>
                    <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                <?endif;?>
            <?else:?>
                <?= $arResult["CONTACT_DESCRIPTION"] ?>                      
            <?endif;?>
        <? endif; ?>        
        <? if (LANGUAGE_ID == "en"): ?>
            <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                <span class="another">Make restaurant reservations - we speak English.</span><br />
                <?if (CITY_ID=="msk"):?>
                    <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                <?else:?>
                    <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                <?endif;?>        
            <?else:?>
                <? echo $arResult["CONTACT_DESCRIPTION"]; ?> 
            <? endif; ?>
        <?endif;?>
    </div>
</div>
<?/*if (CITY_ID=="msk"||CITY_ID=="spb"):?>
<noindex>
    <div class="order-panel" id="order_new_overflow">
        <div class="otitle pull-left"><?=$arResult["NAME"]?></div>
        <a href="/tpl/ajax/online_order_rest.php" class="booking btn btn-info btn-nb" data-id='<?=$arResult["ID"]?>' data-restoran='<?=$arResult["NAME"]?>'><?=GetMessage("R_BOOK_TABLE2")?></a>
        <a href="/tpl/ajax/online_order_rest.php?banket=Y" class="booking btn btn-info btn-nb-empty" data-id='<?=$arResult["ID"]?>' data-restoran='<?=$arResult["NAME"]?>'><?=GetMessage("R_ORDER_BANKET2")?></a>
        <div class="order-text">
            <? if (LANGUAGE_ID == "ru"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <span class="another">Бронировать по телефону:</span><br />
                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                    <?else:?>
                        <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                    <?endif;?>
                <?else:?>
                    <?= $arResult["CONTACT_DESCRIPTION"] ?>                      
                <?endif;?>
            <? endif; ?>        
            <? if (LANGUAGE_ID == "en"): ?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <span class="another">Make restaurant reservations - we speak English.</span><br />
                    <?if (CITY_ID=="msk"):?>
                        <div class="phone"><a href="tel:+74959882656">+7 (495) 988 26 56</a></div>
                    <?else:?>
                        <div class="phone"><a href="tel:+78127401820">+7 (812) 740-18-20</a></div>
                    <?endif;?>        
                <?else:?>
                    <? echo $arResult["CONTACT_DESCRIPTION"]; ?> 
                <? endif; ?>
            <?endif;?>
        </div>
    </div>
</noindex>
<?endif;?>
<script>
    $(document).ready(function(){
        $(window).scroll(function(){            
            if (($(this).scrollTop()>$("#minus_plus_buts").offset().top-100)&&$("#order_new_overflow").css("opacity")=="0")
            {
                $("#order_new_overflow").css({"top":"0px","opacity":1});
            }
            if ($(this).scrollTop()<$("#minus_plus_buts").offset().top-100&&$("#order_new_overflow").css("opacity")=="1")
            {
                $("#order_new_overflow").css({"top":"-80px","opacity":0});
            }
        });
    });
</script>
<?*/?>
 