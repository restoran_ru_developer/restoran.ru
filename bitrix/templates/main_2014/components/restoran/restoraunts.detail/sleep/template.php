<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>        
$(window).load(function() {       
    //minus_plus_buts     
    $("#minus_plus_buts").on("click","a.plus", function(event){
            var obj = $(this);
            $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
                    if(parseInt(otvet))
                    {
                            $(".all").html($(".all").html()*1+1);
                            obj.html(otvet);
                    }
                    else
                    {                                                
                        $('#minus_plus_buts .plus').tooltip({'title':otvet,'trigger':"manual",'placement':'left'});
                        $('#minus_plus_buts .plus').tooltip("show");
                        setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);                        
                    }
            });
            return false;
    });

    $("#minus_plus_buts").on("click","a.minus", function(event){
        var obj = $(this);
        $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet)
        {
            if(parseInt(otvet)){                            
                $(".all").html($(".all").html()*1+1);
                obj.html(otvet);
            }else{
                $('#minus_plus_buts .minus').tooltip({'title':otvet,'trigger':"manual",'placement':'right'});
                $('#minus_plus_buts .minus').tooltip("show");
                setTimeout("$('#minus_plus_buts .plus').tooltip('hide')",3000);
            }
        });
        return false;
    });

    $.post("/tpl/ajax/plus_minus.php",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){        
        $("#minus_plus_buts").html(otvet);
    }); 
})
</script>  
<?                  
if (CITY_ID=="spb"||CITY_ID=="msk"):
    if ($arResult["PROPERTIES"]["place_new_rest"]["VALUE"])
        echo "<div class='new_rest_place'>На его месте сейчас <a href=".$arResult["NEW_REST_PLACE"]["URL"].">".$arResult["NEW_REST_PLACE"]["NAME"]."</a></div>";                      
    //include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/bron_form_new.php");                        
endif;
?>
<?/*<div class="sort anchors-list">
    <a href="#photos_tab" data-toggle='detail_tabs' class="asc">фотогалерея</a>    
    <a href="#props" data-toggle='anchor'>контакты</a>        
    <a href="#reviews" data-toggle='anchor'>отзывы</a>                            
</div>
        
<a href="/bitrix/components/restoran/favorite.add/ajax.php" class="favorite" data-toggle="favorite" data-restoran='<?=$arResult["ID"]?>'>В избранное</a>*/?>
<div class="clearfix"></div>
<div class='detail_tabs' id='photos_tab'>
    <?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/photos.php");?>
</div>
<div class="article_likes">
    <div class="pull-left likes-buttons" id="minus_plus_buts">
            
    </div>        
    <div class="pull-left review-button">
        <a href="#reviews" data-toggle="anchor" class="btn btn-info btn-nb-empty">Оставить отзыв</a>
    </div>          
    <div class="pull-right">
        <?$APPLICATION->IncludeComponent(
            "bitrix:asd.share.buttons",
            "likes",
            Array(
                "ASD_TITLE" => $APPLICATION->GetTitle(false),
                "ASD_URL" => "https://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                "ASD_PICTURE" => "",
                "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                "LIKE_TYPE" => "LIKE",
                "VK_API_ID" => "2881483",
                "VK_LIKE_VIEW" => "mini",
                "SCRIPT_IN_HEAD" => "N"
            )
        );?>
    </div>
</div>      
<?include_once($_SERVER["DOCUMENT_ROOT"].$templateFolder."/includes/props.php");?>