<div id="rest-<?=$arResult["ID"]?>" class="carousel slide priority-slider" data-ride="carousel" data-interval='7000'>
        <div class="carousel-inner">
            <?if(is_array($arResult["PROPERTIES"]["photos"]["VALUE"])):?>  
                <?foreach($arResult["PROPERTIES"]["photos"]["VALUE2"] as $key=>$photo):?>
                    <?if ($key==0):?>
                        <div class="item active" data-num="<?=$key?>">
                            <div class="inline-wrapper">

                                <a href="<?=$arResult["MULTIPLE_PHOTO_ARR"][$key]['src']?>" class="fancy" rel="detail_group">
                                    <img <?if($key==0):?>src="<?=$photo["src"]?>"<?endif;?> data-url="<?=$photo["src"]?>" title='<?=GetMessage("R_REST")." ".$arResult["NAME"]." - фотография №".($key+1)?>' alt="<?=GetMessage("R_REST")." ".$arResult["NAME"]?><?=($arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key])?", ".$arResult["PROPERTIES"]["photos"]["DESCRIPTION"][$key]:""?>">
                                </a>                                
                            </div>
                        </div>
                    <?endif;?>
                <?endforeach;?>
            <?else:?>
                <?if ($arResult["PREVIEW_PICTURE"]["SRC"]):?>
                    <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>"/>
                <?else:?>
                    <img src="/tpl/images/noname/rest_nnm_new_new.png" width="" />
                <?endif;?>
            <?endif;?>                
        </div>
    </div>    

    <script>
    $(function(){
        $('#rest-<?=$arResult["ID"]?>').on('slide.bs.carousel', function (e) {    
            //console.log(e);        
            $(".thumbnail_row div.active").removeClass("active");
            $(".thumbnail_row .th").eq($(e.relatedTarget).data("num")).addClass("active");               

        }); 
        $(".thumbnail_row .th").click(function(){
            var a =$(this).data("num")*1;                        
            $('#rest-<?=$arResult["ID"]?> .item').eq(a).find("img").attr('src', $('#rest-<?=$arResult["ID"]?> .item').eq(a).find("img").data('url'));
            $('#rest-<?=$arResult["ID"]?>').carousel(a);
        });
        //show/hide big text    
        $('a.toglletext').click(function(){
            $(".description .hid").toggle();
        });
    });
    </script>