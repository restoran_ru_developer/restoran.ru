<?
//Oh, shit...
$p = 0;?>
<div id='props' class="props" itemscope itemtype="http://schema.org/Organization">    
    <?if ($arResult["DISPLAY_PROPERTIES"]["kitchen"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
                <div class="col-sm-4 ">
                    <div class="prop">        
                        <div class="name"><?=GetMessage("R_PROPERTY_kitchen")?>:</div>            
                        <div class="value">
                            <?if (is_array($arResult["DISPLAY_PROPERTIES"]["kitchen"])&&count($arResult["DISPLAY_PROPERTIES"]["kitchen"])>3):?>
                                <?
                                for ($i=0;$i<count($arResult["DISPLAY_PROPERTIES"]["kitchen"]["~VALUE"]);$i++) {
                                        $r_item = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["kitchen"]["VALUE"][$i]);
                                        $ar_item = $r_item->Fetch();														
                                if (!empty($ar_item["CODE"])){?><a class="url" href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/kitchen/<?=$ar_item["CODE"]?>/"><?}?><?=$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"][$i]?><?if (!empty($ar_item["CODE"])){?></a><?}
                                if ($i<count($arResult["DISPLAY_PROPERTIES"]["kitchen"]["~VALUE"]) - 1) echo ', ';
                                }?>
                            <?else:?>
                                <?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]))?>
                            <?endif;?>
                        </div>
                    </div>
                </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>
        <?$p++;?>    
    <?endif;?>   
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["average_bill"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_average_bill")?>:</div>                
                    <div class="value">
                        <?$ab = strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"]))?>                                                
                        <?if (CITY_ID=="tmn"):
                            $ab = str_replace("до 1000р","500 - 1000р",$ab);
                            echo $ab;
                        else:?>
                            <?=$ab?>
                        <?endif;?>                                                
                    </div>
                </div>
            </div>
        <?if ($p%3==2&&$p!=2):?>
            </div>
        <?endif;?>      
        <?$p++;?>                    
    <?endif;?>         
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["subway"]["VALUE"]):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop wsubway"> 
                    <?if (CITY_ID!="urm"):?>
                        <div class="name"><?=GetMessage("R_PROPERTY_subway")?>:</div>
                    <?else:?>
                        <div class="name"><?=GetMessage("R_PROPERTY_subway_urm")?>:</div>
                    <?endif;?>
                    <?if (CITY_ID!="urm"):?>
                        <div class="subway">M</div>
                    <?endif?>
                    <div class="value">                    
                         <?if ($arResult["ID"]==387287):?>
                            <p class="metro_<?=CITY_ID?>"><?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))?></p>
                        <?else:?>
                            <?if (is_array($arResult["DISPLAY_PROPERTIES"]["subway"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"])>3):?>
                                <p class="metro_<?=CITY_ID?>"><?
                                        $r_item = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["subway"]["~VALUE"][0]);
                                        $ar_item = $r_item->Fetch();														
                                if (!empty($ar_item["CODE"])){?><a class="url" href="/<?=CITY_ID?>/catalog/<?=$_REQUEST["CATALOG_ID"]?>/metro/<?=$ar_item["CODE"]?>/"><?}?><?=$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]?><?if (!empty($ar_item["CODE"])){?></a><?}?></p>
                            <?else:?>
                                <p class="metro_<?=CITY_ID?>"><?=strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"]))?></p>
                            <?endif;?>
                        <?endif;?>
                    </div>
                </div>
            </div> 
        <?if ($p%3==2):?>
            </div>
        <?endif;?>        
        <?$p++;?>    
    <?endif;?>
    
    <?if ($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["VALUE"])==1):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_opening_hours")?>:</div>                
                    <div class="value">            
                        <?$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0] = str_replace(",", ", ", $arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0]);?>
                        <?$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0] = str_replace("-", " - ", $arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0]);?>
                        <?=implode(", ",$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"])?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>  
        <?$p++;?>    
    <?endif;?>
    

    <?if ($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])==1):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 " itemscope itemtype="http://schema.org/PostalAddress">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_address")?>:</div>                    
                    <div class="value" itemprop="streetAddress">                           
                        <?
                        if (is_array($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])):
                            echo $arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"][0];
                        else:
                            echo $arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"];
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;?>
            
    <?if ($arResult["DISPLAY_PROPERTIES"]["type"]["VALUE"]&&count($arResult["DISPLAY_PROPERTIES"]["type"]["VALUE"])==1):?>
        <?if ($p==0||$p%3==0):?>
            <div class="row">
        <?endif;?>
            <div class="col-sm-4 ">
                <div class="prop">        
                    <div class="name"><?=GetMessage("R_PROPERTY_type")?>:</div>                    
                    <div class="value">                           
                        <?
                        if (is_array($arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"])):
                            echo strip_tags(implode(", ",$arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]));
                        else:
                            echo strip_tags($arResult["DISPLAY_PROPERTIES"]["type"]["DISPLAY_VALUE"]);
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        <?if ($p%3==2):?>
            </div>
        <?endif;?>    
        <?$p++;?>    
    <?endif;?>

    <?if ($p%3!=0):?>
</div>
<?endif;?>
<!--    --><?//if ($arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"]):?>
<!--        --><?//if ($p==0||$p%3==0):?>
<!--            <div class="row">-->
<!--        --><?//endif;?>
<!--        -->
<!--        -->
<!--            </div>-->
<!--        -->
<!--        --><?//$p++;?><!--    -->
<!--    --><?//endif;?><!--                                     -->
</div>