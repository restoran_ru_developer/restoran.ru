<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="city_select">
    <?

    $cities = "";
    foreach($arResult["ITEMS"] as $arItem):
        $add_class = '';
        if($arItem["CODE"]=='ast'||$arItem["CODE"]=='ufa'||$arItem["CODE"]=='krd'||$arItem["CODE"]=="tmn"||$arItem["CODE"]=="kld"||$arItem["CODE"]=="nsk"||$arItem["CODE"]=="sch"||$arItem["CODE"]=="urm"){
//            $add_class = 'dont-show';
            continue;
        }
        if (CITY_ID!=$arItem["CODE"]):
//            if($arParams['MSIE']=='Y'&&SITE_ID=='s1'&&$arItem["CODE"]=='msk'){
//                $cities .= '<li><a href="http://www.restoran.ru/msk/">'.$arItem["NAME"].'</a></li>';
//            }
//            else {
                if ($arItem["CODE"]=="msk"){
                    if (SITE_ID=="s2")
                        $cities .= '<li><a href="https://en.restoran.ru/msk/">'.$arItem["NAME"].'</a></li>';
                    else
                        $cities .= '<li><a href="https://www.restoran.ru/?CITY_ID=msk" rel="nofollow">'.$arItem["NAME"].'</a></li>';
                }
                elseif ($arItem["CODE"]=="spb")
                {
                    if (SITE_ID=="s2")
                        $cities .= '<li><a href="https://en.restoran.ru/spb/">'.$arItem["NAME"].'</a></li>';
                    else
                        $cities .= '<li><a href="https://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
                }
                elseif ($arItem["CODE"]=="urm")
                {
                    if (SITE_ID=="s2")
                        $cities .= '<li><a href="https://en.restoran.ru/urm/">'.$arItem["NAME"].'</a></li>';
                    else
                        $cities .= '<li><a href="https://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="tln"){}
                elseif($arItem["CODE"]=="amt"){}
                elseif($arItem["CODE"]=="vrn"){}
                else
                    $cities .= '<li class="'.$add_class.'"><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
//            }
        else:
            $city = $arItem["NAME"];
        endif;
    endforeach;
    if (SITE_ID=="s2")    
        $cities = str_replace("www.","en.",$cities);    
    ?>
    <a href="#" class="serif dropdown-toggle" data-toggle="dropdown"><?=$city?></a>
    <ul class="dropdown-menu">
        <?=$cities?>
    </ul>                        
</div>
