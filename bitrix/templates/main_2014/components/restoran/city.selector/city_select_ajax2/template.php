<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<link href="/bitrix/templates/main_2014/components/restoran/city.selector/city_select_ajax2/style.css"  type="text/css" rel="stylesheet" />     
<div class="city_select">
<div class="title"><?=GetMessage("WRONG_CITY")?></div>
<?$cities = "";?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?//if (CITY_ID!=$arItem["CODE"]):
        if($arItem["CODE"]!='spb'&&$arItem["CODE"]!='msk'&&$arItem["CODE"]!='rga'){
            continue;
        }

        if ($arItem["CODE"]=="msk"){
            if(SITE_ID!="s2"){
                $cities .= '<li><a href="https://www.restoran.ru/?CITY_ID='.$arItem["CODE"].'" rel="nofollow">'.$arItem["NAME"].'</a></li>';
            }
            else {
                $cities .= '<li><a href="https://en.restoran.ru/'.$arItem["CODE"].'/" >'.$arItem["NAME"].'</a></li>';
            }
        }
        elseif ($arItem["CODE"]=="urm")
            $cities .= '<li><a href="https://urm.restoran.ru/">'.$arItem["NAME"].'</a></li>';
        elseif ($arItem["CODE"]=="spb")
        {
            if (SITE_ID=="s2")
                $cities .= '<li><a href="https://en.restoran.ru/spb/">'.$arItem["NAME"].'</a></li>';
            else
                $cities .= '<li><a href="https://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="ast")
        {
            $cities .= '<li><a href="https://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="tln")
        {
            //$cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="amt"&&CSite::InGroup(Array(1,23)))
        {
            $cities .= '<li><a href="https://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="vrn"&&CSite::InGroup(Array(1)))
        {
            $cities .= '<li><a href="https://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="ast"||$arItem["CODE"]=="amt"||$arItem["CODE"]=="vrn")
        {

        }
        else
            $cities .= '<li><a href="https://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
endforeach;?>
<ul class="nav nav-pills  nav-stacked">
    <?=$cities?>
</ul>
</div>