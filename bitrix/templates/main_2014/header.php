<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<?
//FirePHP::getInstance()->info($_SESSION["lat"],'lat_h');
//FirePHP::getInstance()->info($_SESSION["lon"],'lon_h');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?if(ERROR_404=='Y' && $APPLICATION->GetCurPage()!='/404.php'&&!$USER->IsAdmin()):?>
            <script>location.href = 'http://www.restoran.ru/404.php';</script>
        <?endif;?>
        <title><?$APPLICATION->ShowTitle()?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--        <meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="Restoran.ru">
        <meta property="fb:app_id" content="297181676964377" />
        <?if(CITY_ID=='urm'):?>
            <meta name='yandex-verification' content='4ce75f3fcd8d4779' />
        <?elseif(CITY_ID=='spb'):?>
            <meta name="yandex-verification" content="71f56fa8c98996e1" />
        <?else:?>
            <meta name='yandex-verification' content='6b320b6dff8af5fc' />
        <?endif?>
        <?if (CITY_ID=="tmn"):?>
            <meta property="fb:admins" content="100005128267295,100004709941783" />
        <?else:?>
            <meta property="fb:admins" content="100004709941783" />
        <?endif;?>

        <?if (!$_REQUEST["CODE"]):?>
            <meta property="og:image" content="http://www.restoran.ru/images/logo.png" />
        <?endif;?>


        <?if (CITY_ID=="msk"&&$APPLICATION->GetCurDir()=='/'):?>
            <link rel="canonical" href="http://www.restoran.ru/" />
        <?endif?>

        <?if (CITY_ID=="spb"&&$APPLICATION->GetCurDir()=='/'):?>
            <link rel="canonical" href="http://spb.restoran.ru/" />
        <?endif?>

        <?if (CITY_ID=="rga"&&$APPLICATION->GetCurDir()=='/'):?>
            <link rel="canonical" href="http://www.restoran.ru/rga/" />
        <?endif?>

        <?if (LANGUAGE_ID=='en'&&$APPLICATION->GetCurDir()=='/'):?>
            <link rel="canonical" href="http://en.restoran.ru/" />
        <?endif?>

        <?
        if($APPLICATION->GetCurDir()!='/'&&LANGUAGE_ID!='en'):
            if($_REQUEST['CATALOG_ID']){?>
                <link rel="canonical" href="http://<?=CITY_ID=='spb'?'spb':'www'?>.restoran.ru<?=$APPLICATION->GetCurPageParam('',array('CITY_ID','CATALOG_ID','SECTION_CODE','PROPERTY','PROPERTY_VALUE','PROPERTY2','PROPERTY_VALUE2','near','PAGEN_1','USER_ID','RESTOURANT','CODE','sessid','spec'))?>" />
            <?}
            else {?>
                <?
                if($_REQUEST['CITY_ID']=='content'||$APPLICATION->GetCurDir()=='/content/cookery/'||$_REQUEST['SECTION_CODE']=='mcfromchif'):?>
                    <link rel="canonical" href="http://www.restoran.ru<?=$APPLICATION->GetCurPageParam('',array('CITY_ID','CATALOG_ID','SECTION_CODE','PROPERTY','PROPERTY_VALUE','near','USER_ID','RESTOURANT','CODE','sessid','spec'))?>" />
                <?else:?>
                    <link rel="canonical" href="http://<?=CITY_ID=='spb'?'spb':'www'?>.restoran.ru<?=$APPLICATION->GetCurPageParam('',array('CITY_ID','CATALOG_ID','SECTION_CODE','PROPERTY','PROPERTY_VALUE','near','USER_ID','RESTOURANT','CODE','sessid','spec'))?>" />
                <?endif?>
            <?}
        elseif($APPLICATION->GetCurDir()!='/'&&LANGUAGE_ID=='en'):
            if($_REQUEST['CATALOG_ID']){?>
                <link rel="canonical" href="http://en.restoran.ru<?=$APPLICATION->GetCurPageParam('',array('CITY_ID','CATALOG_ID','SECTION_CODE','PROPERTY','PROPERTY_VALUE','near','PAGEN_1','USER_ID','RESTOURANT','CODE','sessid','spec'))?>" />
            <?}
            else {?>
                <link rel="canonical" href="http://en.restoran.ru<?=$APPLICATION->GetCurPageParam('',array('CITY_ID','CATALOG_ID','SECTION_CODE','PROPERTY','PROPERTY_VALUE','near','USER_ID','RESTOURANT','CODE','sessid','spec'))?>" />
            <?}
        endif;?>

        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">

        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />                  
        <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/selectivizr-1.0.2/selectivizr-min.js"></script>
        <link href="<?=SITE_TEMPLATE_PATH?>/cap-ie-style.css" rel="stylesheet">
        <script>
            $(function(){
                $('.cap-wrapper').css('display','block');
                $('.cap-overlay').css('display','block');
                $('.container').remove();
            });
        </script>
        <![endif]-->
        <!--[if lt IE 7]>
        <script>
            $(function(){
                $("*:first-child").addClass("firstChild");
            });
        </script>
        <![endif]-->

        <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>

        <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>
        <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>

        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
        <?$APPLICATION->ShowHead()?>
        <?
         //Строки от Антона
        if(substr_count($_SERVER["REQUEST_URI"], "blog/")>0 || substr_count($_SERVER["REQUEST_URI"], "kup")>0 || substr_count($_SERVER["REQUEST_URI"], "restoran_edit")>0 || substr_count($_SERVER["REQUEST_URI"], "restorator")>0 || substr_count($_SERVER["REQUEST_URI"], "businessman")>0 || substr_count($_SERVER["REQUEST_URI"], "rest_edit")>0)
        {
            if (!CSite::InGroup(Array(1,15,16,23)))
            {
                //$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
            }
            else
                $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor/css/redactor.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadScript('/tpl/js/jquery-ui-1.8.17.custom.min.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/redactor_list_script.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/redactor_list_script.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js');
        }
        if (substr_count($APPLICATION->GetCurPage(),"restorator")>0||substr_count($APPLICATION->GetCurPage(),"redactor")>0)
        {
            $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor/css/redactor.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/j.js');
        }
        if (substr_count($APPLICATION->GetCurPage(),"afisha")>0)
        {
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js');
        }
        $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);  

        CModule::IncludeModule("advertising");    
        if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
            $page = "others";
        else
            $page = "main";
        CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));
        global $brand1;
        //unset($_SESSION["CONTEXT"]);
        //unset($_REQUEST["CONTEXT"]);
        ///$_SESSION["CITY_ID"] = CITY_ID;
?>

    </head>
<body>

    <div id="panel"><?$APPLICATION->ShowPanel()?></div>

    <!--[if lt IE 9]>
    <div class="cap-wrapper">
        <div class="cap-top-title">Ваш браузер устарел!</div>
        <div class="cap-text-about">
            Вы пользуетесь устаревшей версией браузера Internet Explorer. Данная версия браузера не поддерживает многие
            современные технологии,<br>из-за чего многие страницы отображаются некорректно
        </div>
        <div class="browser-icons-wrapper">
            <ul>
                <li><a href="http://www.apple.com/safari/download/" target="_blank">safari</a></li>
                <li><a href="http://www.mozilla.com/firefox/" target="_blank">firefox</a></li>
                <li><a href="http://www.opera.com/download/" target="_blank">opera</a></li>
                <li><a href="http://www.google.com/chrome/" target="_blank">chrome</a></li>
            </ul>
        </div>
        <div class="cap-company-logo"></div>
    </div>
    <div class="cap-overlay"></div>
    <![endif]-->

    <?$APPLICATION->IncludeComponent(
        "bitrix:advertising.banner",
        "brand1",
        Array(
                "TYPE" => "brand_1",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "0"
        ),
        false
    );?>
    <div class="container">
        <div class="t_b_980">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "top_main_page_".CITY_ID,
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="page-header block">
            <div class="pull-left">
                <div class="logo">
                    <?if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/"):?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="Ресторан.ру" />
                    <?else:?>
                        <a href="/<?=CITY_ID?>/" alt="Ресторан.ру"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" /></a>
                    <?endif;?>
                    <?$APPLICATION->IncludeComponent(
                        "restoran:city.selector",
                        "city_select_2014",
                        Array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_URL" => "",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "36000000006",
                            "CACHE_NOTES" => "new3212366243391",//.preg_match('/msie/i',$_SERVER['HTTP_USER_AGENT'])?'msie':''
                            "CACHE_GROUPS" => "N",
                            //'MSIE' => preg_match('/msie/i',$_SERVER['HTTP_USER_AGENT'])?'Y':'N'
                        ),
                        false
                    );?>
                    <div class="special_links">
                    <?
                    if(SITE_LANGUAGE_ID!='en'):
                        $arBanIB = getArIblock("banner_in_header", CITY_ID);
                        $APPLICATION->IncludeComponent(    
                            "bitrix:news.list",
                            "banner_in_header",
                            Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "banner_in_header",
                                "IBLOCK_ID" => $arBanIB["ID"],
                                "NEWS_COUNT" => "2",
                                "SORT_BY1" => "SORT",
                                "SORT_ORDER1" => "ASC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array(),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "14400",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "CACHE_NOTES" => "n",            
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                            false
                            );
                        endif;
                        ?>
                    </div>                                                
                </div>            
                <div id="main_menu">
                    <?
                    $strIpad = '';
                    if(substr_count($_SERVER["HTTP_USER_AGENT"],"iPad")){
                        $strIpad = 'ipad';
                    }
                    $arSiteMenuIB = getArIblock("site_menu", CITY_ID);
                    $APPLICATION->IncludeComponent(
                            "bitrix:catalog.section.list",
                            "site_menu",
                            Array(
                                    "IBLOCK_TYPE" => "site_menu",
                                    "IBLOCK_ID" => $arSiteMenuIB["ID"],
                                    "COUNT_ELEMENTS" => "Y",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000002",
                                    "CACHE_NOTES" => $strIpad,
                                    "CACHE_GROUPS" => "N"
                            ),
                    false
                    );?>                                                       
                </div>
            </div>
            <div class="pull-right">
                <div class="right_links">
                    <?if ($USER->IsAuthorized()):?>                        
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "header_2014",
                            Array(
                                "REGISTER_URL" => "",
                                "FORGOT_PASSWORD_URL" => "",
                                "PROFILE_URL" => "",
                                "SHOW_ERRORS" => "N"
                            ),
                            false
                        );?> 
                    <?endif;?>
                    <?if (SITE_ID=="s1"):?>
                        <a href="http://en.restoran.ru<?=$APPLICATION->GetCurPage()?>">In English</a>
                    <?else:?>
                        <a href="http://www.restoran.ru<?=$APPLICATION->GetCurPage()?>">На русском</a>
                    <?endif;?>
                    <br />
                    
                    <?/*if ((CITY_ID=="ast"||CITY_ID=="sch"||CITY_ID=="krd"||CITY_ID=="ufa")):?>                        
                        <?if ($USER->IsAdmin()||CSite::InGroup(Array(15,16,27,23,24))):?>
                            <a href="/redactor/" class="for_n_bu"></a>
                        <?elseif (CSite::InGroup(Array(9))):?>
                            <a href="/restorator/" class="for_n_bu"></a>
                        <?elseif ($USER->IsAuthorized()):?>
                            <a href="/auth/register_restorator.php#restorator" class="for_n_bu"></a>
                        <?else:?>
                            <a href="/auth/register_restorator.php#restorator" class="for_n_bu"></a>
                        <?endif;?>  
                    <?endif;*/

                    ?>

                    <?if (!$USER->IsAuthorized()):?>
                    <?
                        $arParamsToDelete = array(
                            "login",
                            "logout",
                            "register",
                            "forgot_password",
                            "change_password",
                            "confirm_registration",
                            "confirm_code",
                            "confirm_user_id",
                            "logout_butt",
                            "auth_service_id",
                            "CITY_ID"
                        );
                        $currentUrl = $APPLICATION->GetCurPageParam("", $arParamsToDelete);
                        ?>
                        <a href="/tpl/ajax/auth.php?backurl=<?=$currentUrl?>" class="auth"><?=GetMessage("ENTER_LINK")?></a> &nbsp;
                    <?endif;?>
                    <?if ($USER->IsAdmin()||CSite::InGroup(Array(15,16,27,23,24))):?>
                        <a href="/redactor/"><?=GetMessage("TMPL_ADD_PLACE")?></a>
                    <?elseif (CSite::InGroup(Array(9)) && in_array(CITY_ID,array('spb','msk','rga','urm','tmn','kld','nsk'))):?>
                        <a href="/restorator/"><?=GetMessage("ADD_PLACE")?></a>
                    <?elseif ($USER->IsAuthorized()&&in_array(CITY_ID,array('spb','msk','rga','urm','tmn','kld','nsk'))):?>
                        <a href="/auth/register_restorator.php#restorator"><?=GetMessage("TMPL_ADD_PLACE")?></a>
                    <?elseif(!$USER->IsAuthorized()):?>
                        <a href="/auth/register.php"><?=GetMessage("REGISTER_LINK")?></a>                        
                    <?endif;?>                       
                </div>
                <?if(CITY_ID!='tmn'&&CITY_ID!='nsk'&&CITY_ID!='kld'):?>
                <div class="phone serif">
                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=="msk"||CITY_ID=="spb"?"":"_rgaurm"?>.php" class="booking">
                    <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                    </a>
                </div>
                <?endif?>
                <?if (CITY_ID=="msk"||CITY_ID=="spb"):?>
                    <div class="order_link">
                        <a href="/tpl/ajax/online_order_rest.php" class="booking"><span><?=GetMessage("TMPL_HEADER_ORDER")?></span><span> <?=GetMessage("TMPL_FREE")?></span></a>
                    </div>
                <?endif;?>

                <?if (CITY_ID=="nsk"):?>
                    <br>
                    <div class="order_link">
                        <a href="/tpl/ajax/online_order_rest.php" class="booking"><span><?=GetMessage("TMPL_HEADER_ORDER")?></span><span> <?=GetMessage("TMPL_FREE")?></span></a>
                    </div>
                <?endif;?>
                
                <?
                //if ($USER->IsAdmin()):
                if (CITY_ID=="rga"||CITY_ID=="urm"):?>
                    <div class="order_link">
                        <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking"><?=GetMessage("TMPL_HEADER_ORDER")?> <span><?=GetMessage("TMPL_FREE")?></span></a>
                    </div>
                <?endif;?>
                <?//endif;?>
            </div>            
        </div>

        <?if(SITE_LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='urm'||CITY_ID=='rga')):?>
            <div class="filter_box <?=(substr_count($APPLICATION->GetCurDir(),"/map")>0||$brand1)?"nobackground":""?> <?//if(!preg_match('/\/map\//',$APPLICATION->GetCurDir())||$USER->IsAdmin()):?>main_2015_summer<?//endif;?> <?=(substr_count($APPLICATION->GetCurDir(),"/map")>0)?'on-map-page':''?>">
                <div class="filter block">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:search.title",
                        substr_count($APPLICATION->GetCurDir(),"/map")>0?"adress_search_2015":"search_2015_1",//$USER->IsAdmin()?(():((substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_2014":"search_2015_1"),
                        Array(
                            "COOKERY" => substr_count($APPLICATION->GetCurPage(), "cookery")
                        ),
                        false
                    );?>
                    <?if($APPLICATION->GetCurDir()=='/'||$APPLICATION->GetCurDir()=='/spb/'||$APPLICATION->GetCurDir()=='/msk/'||$APPLICATION->GetCurDir()=='/rga/'||$APPLICATION->GetCurDir()=='/urm/'||$APPLICATION->GetCurDir()=='/tmn/'||$APPLICATION->GetCurDir()=='/kld/'||$APPLICATION->GetCurDir()=='/nsk/'):?>
                        <h1 class="index-h" ><?=$APPLICATION->ShowTitle("h1")?></h1>
                    <?endif?>
                    <?
                    if (substr_count($APPLICATION->GetCurPage(), "cookery")>0):
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "cook_2014",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => 139,
                                "FILTER_NAME" => 72,
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("cat", "osn_ingr", "prig_time", "cook", "povod", "prig"),
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N",
                            )
                        );
                    elseif (!substr_count($APPLICATION->GetCurDir(), "firms")):
                        if ($_REQUEST["CATALOG_ID"]=="restaurants"):
                            $prop_fil = (CITY_ID=="urm"||CITY_ID=="rga")?array("kitchen", "average_bill", "area", "out_city"):array("kitchen", "average_bill", "subway", 'area',"out_city");
                        elseif ($_REQUEST["CATALOG_ID"]=="banket"):
                            $prop_fil = (CITY_ID=="urm"||CITY_ID=="rga")?array("kolichestvochelovek", "area", "BANKET_MENU_SUM"):array("kolichestvochelovek","subway", "area", "BANKET_MENU_SUM");
                        else:
                            $prop_fil = (CITY_ID=="urm"||CITY_ID=="rga")?array("kitchen", "average_bill", "area", "out_city"):(CITY_ID=="msk"||CITY_ID=="spb"?array("kitchen", "average_bill", "subway", "area", "out_city"):array("kitchen", "average_bill", "subway", "out_city"));
                        endif;
                        $arRestIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"filter_2014_in_map":"filter_2014",//$USER->IsAdmin()?():((substr_count($APPLICATION->GetCurDir(),"/map")>0)?"map_2014":"filter_2014"),
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arRestIB["ID"],
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => $prop_fil,
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//
                                "CACHE_TIME" => "36000020",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N"
                            )
                        );
                    endif;
                    ?>
                    <?if (($_REQUEST["CATALOG_ID"] || substr_count($APPLICATION->GetCurDir(), "firms"))&&!substr_count($APPLICATION->GetCurDir(),'detailed')):?>
                        <noindex>
                            <?$APPLICATION->IncludeComponent(
                                "restoran:alphabet.filter",
                                "2014",
                                Array(),
                                false
                            );?>
                        </noindex>
                    <?endif;?>
                </div>
            </div>
        <?else:?>
            <div class="filter_box <?=(substr_count($APPLICATION->GetCurDir(),"/map")>0||$brand1)?"nobackground":""?>">
                <div class="filter block">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:search.title",
                        (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"adress_search_2014":"search_2014",
                        Array(
                            "COOKERY" => substr_count($APPLICATION->GetCurPage(), "cookery")
                        ),
                        false
                    );?>
                    <?if($APPLICATION->GetCurDir()=='/'||$APPLICATION->GetCurDir()=='/spb/'||$APPLICATION->GetCurDir()=='/msk/'||$APPLICATION->GetCurDir()=='/rga/'||$APPLICATION->GetCurDir()=='/urm/'||$APPLICATION->GetCurDir()=='/tmn/'||$APPLICATION->GetCurDir()=='/kld/'||$APPLICATION->GetCurDir()=='/nsk/'):?>
                        <h1 class="index-h" ><?=$APPLICATION->ShowTitle("h1")?></h1>
                    <?endif?>
                    <?
                    if (substr_count($APPLICATION->GetCurPage(), "cookery")>0):
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "cook_2014",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => 139,
                                "FILTER_NAME" => 72,
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("cat", "osn_ingr", "prig_time", "cook", "povod", "prig"),
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N",
                            )
                        );
                    elseif (!substr_count($APPLICATION->GetCurDir(), "firms")):
                        if ($_REQUEST["CATALOG_ID"]=="restaurants"):
                            $prop_fil = (CITY_ID=="urm"||CITY_ID=="rga")?array("kitchen", "average_bill", "area", "out_city"):array("kitchen", "average_bill", "subway", 'area',"out_city");
                        elseif ($_REQUEST["CATALOG_ID"]=="banket"):
                            $prop_fil = (CITY_ID=="urm"||CITY_ID=="rga")?array("kolichestvochelovek", "area", "BANKET_MENU_SUM"):array("kolichestvochelovek","subway", "area", "BANKET_MENU_SUM");
                        else:
                            $prop_fil = (CITY_ID=="urm"||CITY_ID=="rga")?array("kitchen", "average_bill", "area", "out_city"):(CITY_ID=="msk"||CITY_ID=="spb"?array("kitchen", "average_bill", "subway", "area", "out_city"):array("kitchen", "average_bill", "subway", "out_city"));
                        endif;
                        $arRestIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"map_2014":"filter_2014",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arRestIB["ID"],
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => $prop_fil,
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000013",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N"
                            )
                        );
                    endif;
                    ?>
                    <?if ($_REQUEST["CATALOG_ID"] || substr_count($APPLICATION->GetCurDir(), "firms")):?>
                        <noindex>
                            <?$APPLICATION->IncludeComponent(
                                "restoran:alphabet.filter",
                                "2014",
                                Array(),
                                false
                            );?>
                        </noindex>
                    <?endif;?>
                </div>
            </div>
        <?endif;?>

        <?if (substr_count($APPLICATION->GetCurDir(), "restorator")):          
            $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "menu",
                    "EDIT_TEMPLATE" => ""
                ),
                false
            );
        endif;
        ?>
        <?if(substr_count($APPLICATION->GetCurDir(),"/users/")>0&&substr_count($APPLICATION->GetCurDir(),"/users/list/")==0&&!substr_count($APPLICATION->GetCurDir(),"/cookery/")):?>
            <div class="block">            
                <?if($USER->GetID()==$_REQUEST["USER_ID"]):?>
                    <h1 class="pull-left"><?=$USER->GetFullName()?></h1>                        
                    <div class="clearfix"></div>
                <?else:?>
                    <h1 class="pull-left">
                        <?
                        $rsUser = CUser::GetByID($_REQUEST["USER_ID"]);
                        $arUser = $rsUser->Fetch();
                        echo $arUser["NAME"]." ".$arUser["LAST_NAME"];
                        ?>
                    </h1>
                <?endif;?>
                <?if($USER->GetID()!=$_REQUEST["USER_ID"]):?>
                    <div class="pull-right" style="padding-top:3px;">
                        <script src="/bitrix/components/restoran/ajax.invite2restoran/templates/.default/chosen.jquery.js"></script>
                        <script>

                            $(document).ready(function() {
                                $("#user_mail_send").click(function(){
                                    var _this = $(this);
                                    $.ajax({
                                        type: "POST",
                                        url: _this.attr("href"),
                                        data: "USER_NAME=<?=$arUser["NAME"]." ".$arUser["LAST_NAME"]?>&USER_ID=<?=intval($_REQUEST["USER_ID"])?>&<?=bitrix_sessid_get()?>",
                                        success: function(data) {
                                            if (!$("#mail_modal").size())
                                            {
                                                $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                                            }
                                            $('#mail_modal').html(data);
                                            showOverflow();
                                            setCenter($("#mail_modal"));
                                            $("#mail_modal").fadeIn("300"); 
                                        }
                                    });
                                    return false;
                                });
                                $("#invite2rest").click(function(){
                                    var _this = $(this);
                                    $.ajax({
                                        type: "POST",
                                        url: _this.attr("href"),
                                        data: "USER_ID=<?=intval($_REQUEST["USER_ID"])?>&<?=bitrix_sessid_get()?>",
                                        success: function(data) {
                                            if (!$("#invite2rest_modal").size())
                                            {
                                                $("<div class='popup popup_modal' id='invite2rest_modal'></div>").appendTo("body");                                                               
                                            }
                                            $('#invite2rest_modal').html(data);
                                            showOverflow();
                                            setCenter($("#invite2rest_modal"));
                                            $("#invite2rest_modal").css("top","150px")
                                            $("#invite2rest_modal").fadeIn("300");


                                            $(".datew").datepicker({format: 'dd.mm.yyyy', language: 'ru'});
                                            $.maski.definitions['~']='[0-2]';
                                            $.maski.definitions['!']='[0-5]';
                                            $(".time").maski("~9   !9",{placeholder:" "});
                                            $("#choose_user").data("placeholder","Выберите пользователя...").chosen();

                                            $("#top_search_input2").autocomplete("/search/rest_bron.php", {
                                                limit: 5,
                                                minChars: 3,
                                                formatItem: function(data, i, n, value) {
                                                    return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
                                                },
                                                formatResult: function(data, value) {
                                                    return value.split("###")[0];
                                                },
                                                onItemSelect: function(value) {

                                                    var a = value.split("###")[1];
                                                    $("#top_search_rest2").attr("value",a);
                                                },
                                                extraParams: {
                                                    //search_in: function() { return $('#search_in').val(); },
                                                }
                                            });

                                            $('#order_online form').on('submit', function(){

                                                var form = $(this);
                                                $.ajax({
                                                    type: "POST",
                                                    url: form.attr("action"),
                                                    dataType: "json",
                                                    data: form.serialize(),
                                                    success: function(data) {
                                                        $('.ok').html(data.MESSAGE);
                                                        $('.ok').fadeIn(500);
                                                        if (data.STATUS==1)
                                                            $("#ivite_button").attr("disabled",true);
                                                        if (data.RELOAD && data.RELOAD=="2")
                                                        {
                                                            location.reload();
                                                        }
                                                        if (data.RELOAD && data.RELOAD=="1")
                                                            location.reload();
                                                    }
                                                });
                                                return false;
                                            });
                                            $(".ajax_form > form").bind("onFail", function (e, errors) {
                                                if (e.originalEvent.type == 'submit') {
                                                    $.each(errors, function () {
                                                        var input = this.input;
                                                    });
                                                }
                                            });


                                        }
                                    });
                                    return false;
                                });
                            });
                        </script>
                        <?
                        if ($USER->IsAuthorized()){
                        CModule::IncludeModule("socialnetwork");
                        $friend = CSocNetUserRelations::GetRelation($USER->GetID(), $_REQUEST["USER_ID"]);?>
                        <div class="user-mail left" style="margin-top:7px; margin-right:15px"><a href="/tpl/ajax/im.php" id="user_mail_send">Сообщение</a></div>
                        <?if (CSite::InGroup(Array(1,15,16,20))):?>
                            <div class="left" style="margin-top:7px; margin-right:15px"><a href="/tpl/ajax/invite2rest.php" id="invite2rest">Пригласить в ресторан</a></div>
                        <?endif;?>
                        <div class="left" id="add_fr" style="padding-top:3px;">
                            <?if ($friend=="F"):?>
                                    <?$APPLICATION->IncludeComponent(
                                        "restoran:user.friends_delete",
                                        "light_button",
                                        Array(
                                            "FIRST_USER_ID" => $USER->GetID(),
                                            "SECOND_USER_ID" => (int)$_REQUEST["USER_ID"],
                                            "RESULT_CONTAINER_ID" => "add_fr"
                                        ),
                                        false
                                    );?>
                            <?elseif($friend=="Z"):?>
                                <input type="button" class="grey_button" value="Заявка отправлена" />
                            <?else:?>
                                <?$APPLICATION->IncludeComponent(
                                    "restoran:user.friends_add",
                                    "light_button",
                                    Array(
                                        "FIRST_USER_ID" => $USER->GetID(),
                                        "SECOND_USER_ID" => (int)$_REQUEST["USER_ID"],
                                        "RESULT_CONTAINER_ID" => "add_fr"
                                    ),
                                    false
                                );?>
                            <?endif;?>                       
                        <!--<div class="left"><input type="button" class="light_button" value="+ Добавить в друзья" /></div>-->
                        </div>
                        <?}?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                <?endif;?>
                
                        <?global $user_id;
                        $user_id = $_REQUEST["USER_ID"]?>
                        <?if($USER->GetID()==$_REQUEST["USER_ID"]):?>
<!--                            <div style="position:absolute;top:0px; right:0px;">-->
<!--                                <a href="javascript:void(0)" style="position:relative; text-decoration: none;" id="invite_friend">-->
<!--                                    <input type="button" style="width:218px!important;" class="light_button" value="Пригласи друга на сайт" />                                    -->
<!--                                </a>                                                        -->
<!--                            </div>-->
                            <?/*$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "restorator",
                                Array(
                                    "ROOT_MENU_TYPE" => "restorator",
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000000",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                ),
                            false
                            );*/?>
                            <div class="menu-horizontal-long">
                                <ul>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "users",
                                    Array(
                                        "ROOT_MENU_TYPE" => "personal",
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_TIME" => "3600000000",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                    ),
                                false
                                );?>
                                </ul>
                            </div>
                        <?else:?>
                            <div class="menu-horizontal-long">
                                <ul style="width:350px">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "users",
                                    Array(
                                        "ROOT_MENU_TYPE" => "user",
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "MENU_CACHE_TYPE" => "Y",
                                        "MENU_CACHE_TIME" => "60",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(CITY_ID)
                                    ),
                                false
                                );?>
                                </ul>
                            </div>
                        <?endif;?>                                         
            </div>
        <?endif;?>
        <div class="content">
            <div class="t_b_980">
                <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "top_content_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                false
                );?>                       
            </div>
            <?if($APPLICATION->get_cookie("CONTEXT")=="Y") $_REQUEST["CONTEXT"]="Y";?>