<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
<div class="clearfix"></div>
<div class="ontop icon-arrow-up"><?=GetMessage("GO_TOP")?></div>
<div class="to-top-btn-bg <?=$_REQUEST['CATALOG_ID']=='restaurants'||$_REQUEST['CATALOG_ID']=='banket'?'catalog-button':''?>"></div>
</div>
</div>
<?if(LANGUAGE_ID!='en'):?>
    <div id="footer">
        <div class="container">
            <div class="first-footer-line">
                <div class="footer-logo-wrap">
                    <?if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/"):?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="Ресторан.ру" />
                    <?else:?>
                        <a href="/<?=CITY_ID?>/" alt="Ресторан.ру"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" /></a>
                    <?endif;?>
                </div>
                <div class="footer-phone-wrap">
                    <div class="phone-str">
                        <a href="/tpl/ajax/online_order_rest<?=CITY_ID=="msk"||CITY_ID=="spb"?"":"_rgaurm"?>.php" class="booking">
                            <?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                        </a>
                    </div>

                    <div class="time-desc-to-call">Бронируем ежедневно с 10:00 до 22:00</div>
                    <span class="vert-border-line"></span>
                </div>
                <div class="footer-order-link-wrap">
                    <a href="/tpl/ajax/online_order_rest<?=CITY_ID=="msk"||CITY_ID=="spb"?"":"_rgaurm"?>.php" class="booking">
                        <span>Бронировать</span>
                        <span>бесплатно</span>
                    </a>
                </div>
                <div class="footer-app-text-about">
                    Приложение<br>Restoran.ru
                </div>
                <div class="footer-app-link-wrap">
                    <a href="https://appsto.re/ru/yYEdfb.i" target="_blank">
                        <img src="/tpl/images/footer-app-link-logo.png" alt="Установить приложение"/>
                    </a>
                </div>
            </div>
            <?
            $arSiteMenuIB = getArIblock("site_menu", CITY_ID);
            $APPLICATION->IncludeComponent("restoran:catalog.section.list", "bottom_site_menu-one-str", array(
                "IBLOCK_TYPE" => "site_menu",
                "IBLOCK_ID" => $arSiteMenuIB["ID"],
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "Y",
                "TOP_DEPTH" => "2",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "36000011",
                "CACHE_GROUPS" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                'SORT_FIELD'=>'UF_FOOTER_SORT',
                'SORT_ORDER'=>'ASC'
            ),
                false
            );?>

            <div class="footer-age-inf">
                <div class="sixteen-copyright">16+</div>
                <div class="copyright-line">
                    © <?=date("Y")?> Ресторан.Ru
                    <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("include_areas/footer_copyright_3-n.php"),
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
            </div>
            <div class="footer-social-block">
                <a href="https://www.instagram.com/restoran_ru/" class="footer-social-inst-link" target="_blank"></a>
                <a href="<?=CITY_ID=='spb'?'https://vk.com/restoran_ru':'https://vk.com/restoranru'?>" class="footer-social-vk-link" target="_blank"></a>
                <a href="<?=CITY_ID=='spb'?'https://www.facebook.com/restoranspb/':'https://www.facebook.com/Restoran.ru/'?>" class="footer-social-f-link" target="_blank"></a>
            </div>
            <div class="clearfix"></div>
            <div class="comp_count">
                <ul class="companies">
                    <li>
                        <noindex><a href="http://www.cakelabs.ru/portfolio/" rel="nofollow" target="_blank" class="cakelabs" alt="cakelabs.ru" title="<?=GetMessage('made_by_title')?>"><?=GetMessage('made_by')?></a>
                    </li>
<!--                    <li>-->
<!--                        <noindex><a rel="nofollow" target="_blank" class="sunmedia" href="http://www.sunmedia.ru" alt="sunmedia.ru" title="--><?//=GetMessage('seo_by_title')?><!--">--><?//=GetMessage('seo_by')?><!--</a></noindex>-->
<!--                    </li>-->
                </ul>
                <div class="counters">
                    <div class="pull-left" style="margin-top:5px;">
                        <noindex>
                            <!-- Rating@Mail.ru counter -->
                            <script type="text/javascript">
                                var _tmr = window._tmr || (window._tmr = []);
                                _tmr.push({id: "432665", type: "pageView", start: (new Date()).getTime()});
                                (function (d, w, id) {
                                    if (d.getElementById(id)) return;
                                    var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
                                    ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
                                    var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
                                    if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
                                })(document, window, "topmailru-code");
                            </script><noscript><div style="position:absolute;left:-10000px;">
                                    <img src="//top-fwz1.mail.ru/counter?id=432665;js=na" style="border:0;" height="1" width="1" alt="–ейтинг@Mail.ru" />
                                </div></noscript>
                            <!-- //Rating@Mail.ru counter -->
                            <!-- Rating@Mail.ru logo -->
                            <a href="http://top.mail.ru/jump?from=432665">
                                <img src="//top-fwz1.mail.ru/counter?id=432665;t=571;l=1"
                                     style="border:0;" height="40" width="88" alt="–ейтинг@Mail.ru" /></a>
                            <!-- //Rating@Mail.ru logo -->
                        </noindex>


                    </div>
                    <div class="pull-left" style="margin-left:10px;margin-top:5px;">
                        <noindex>
                            <div id="top100counter" style="float:left;"></div>
                            <p style="margin-bottom: 0; font-size:10px;"><a href="http://www.rambler.ru/" class="font10">Партнер «Рамблера»</a></p>
                            <script type="text/javascript">
                                var _top100q = _top100q || [];

                                _top100q.push(["setAccount", "2838823"]);
                                _top100q.push(["trackPageviewByLogo", document.getElementById("top100counter")]);


                                (function(){
                                    var top100 = document.createElement("script"); top100.type = "text/javascript";

                                    top100.async = true;
                                    top100.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/pack/pack.min.js?3600000";
                                    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(top100, s);
                                })();
                            </script>
                        </noindex>
                    </div>
                    <div class="pull-left" style="margin-left:10px;margin-top:5px;">
                        <noindex>
                            <!--LiveInternet counter--><script type="text/javascript"><!--
                                document.write("<a href='http://www.liveinternet.ru/click' "+
                                "target=_blank><img src='//counter.yadro.ru/hit?t41.5;r"+
                                escape(document.referrer)+((typeof(screen)=="undefined")?"":
                                ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                                ";"+Math.random()+
                                "' alt='' title='LiveInternet' "+
                                "border='0' width='31' height='31'><\/a>")
                                //--></script><!--/LiveInternet-->
                        </noindex>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?else:?>
    <div id="footer">
        <div class="container">

            <?
            $arSiteMenuIB = getArIblock("site_menu", CITY_ID);
            $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bottom_site_menu", array(
                "IBLOCK_TYPE" => "site_menu",
                "IBLOCK_ID" => $arSiteMenuIB["ID"],
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "Y",
                "TOP_DEPTH" => "2",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000011",
                "CACHE_GROUPS" => "N",
                "ADD_SECTIONS_CHAIN" => "N"
            ),
                false
            );?>


            <div class="pull-right">
                <div class="title"><?=GetMessage("FOR_USER")?></div>
                <?$APPLICATION->IncludeFile(
                    "/index_inc.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
                <br /><br />


                <?$APPLICATION->IncludeComponent("bitrix:news.list", "links_cloud", Array(
                    "DISPLAY_DATE" => "N",	// Выводить дату элемента
                    "DISPLAY_NAME" => "Y",	// Выводить название элемента
                    "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                    "AJAX_MODE" => "N",	// Включить режим AJAX
                    "IBLOCK_TYPE" => "site_menu",	// Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => "links_cloud",	// Код информационного блока
                    "NEWS_COUNT" => "20",	// Количество новостей на странице
                    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                    "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                    "SORT_BY2" => "",	// Поле для второй сортировки новостей
                    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                    "FILTER_NAME" => "",	// Фильтр
                    "FIELD_CODE" => "",	// Поля
                    "PROPERTY_CODE" => "",	// Свойства
                    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                    "PARENT_SECTION" => "",	// ID раздела
                    "PARENT_SECTION_CODE" => SITE_LANGUAGE_ID=='ru' ? CITY_ID : 'en-'.CITY_ID,	// Код раздела
                    "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
                    "CACHE_TYPE" => "A",	// Тип кеширования
                    "CACHE_TIME" => "36000001",	// Время кеширования (сек.)
                    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",	// Учитывать права доступа
                    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                    "PAGER_TITLE" => "Новости",	// Название категорий
                    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                ),
                    false
                );?>

            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="comp_count">
                <ul class="companies">
                    <li>
                        <noindex><a href="http://www.cakelabs.ru/portfolio/" rel="nofollow" target="_blank" class="cakelabs" alt="cakelabs.ru" title="<?=GetMessage('made_by_title')?>"><?=GetMessage('made_by')?></a>
                    </li>
<!--                    <li>-->
<!--                        <noindex><a rel="nofollow" target="_blank" class="sunmedia" href="http://www.sunmedia.ru" alt="sunmedia.ru" title="--><?//=GetMessage('seo_by_title')?><!--">--><?//=GetMessage('seo_by')?><!--</a></noindex>-->
<!--                    </li>-->
                </ul>
                <div class="counters">
                    <div class="pull-left" style="margin-top:5px;">
                        <noindex>
                            <!--Rating@Mail.ru COUNTER-->
                            <script language="JavaScript" type="text/javascript"><!--
                                d=document;var a="";a+=";r="+escape(d.referrer); js=10
                                //-->
                            </script>
                            <script language="JavaScript1.1" type="text/javascript">
                                <!--
                                a+=";j="+navigator.javaEnabled(); js=11
                                //-->
                            </script>
                            <script language="JavaScript1.2" type="text/javascript">
                                <!--
                                s=screen;a+=";s="+s.width+"*"+s.height; a+=";d="+(s.colorDepth?s.colorDepth:s.pixelDepth); js=12
                                //-->
                            </script>
                            <script language="JavaScript1.3" type="text/javascript">
                                <!--
                                js=13
                                //-->
                            </script>
                            <script language="JavaScript" type="text/javascript">
                                <!--
                                d.write("<a href=http://top.mail.ru/jump?from=432665"+" target=_top><img src=http://top.list.ru/counter"+"?id=432665;t=130;js="+js+a+";rand="+Math.random()+" alt=Рейтинг@Mail.ru"+" border=0 height=40 width=88/></a>");
                                if(11<js) d.write("<"+"!-- ")
                                //--></script>
                            <noscript><a target=_top href="http://top.mail.ru/jump?from=432665"><img src="http://top.list.ru/counter?js=na;id=432665;t=130" border=0 height=40 width=88 alt="Рейтинг@Mail.ru"/></a></noscript>
                            <script language="JavaScript" type="text/javascript"><!--
                                if(11<js)d.write("--"+">")
                                //-->
                            </script>
                        </noindex>


                    </div>
                    <div class="pull-left" style="margin-left:10px;margin-top:5px;">
                        <noindex>
                            <div id="top100counter" style="float:left;"></div>
                            <p style="margin-bottom: 0; font-size:10px;"><a href="http://www.rambler.ru/" class="font10">Партнер «Рамблера»</a></p>
                            <script type="text/javascript">
                                var _top100q = _top100q || [];

                                _top100q.push(["setAccount", "2838823"]);
                                _top100q.push(["trackPageviewByLogo", document.getElementById("top100counter")]);


                                (function(){
                                    var top100 = document.createElement("script"); top100.type = "text/javascript";

                                    top100.async = true;
                                    top100.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/pack/pack.min.js?3600000";
                                    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(top100, s);
                                })();
                            </script>
                        </noindex>
                    </div>
                    <div class="pull-left" style="margin-left:10px;margin-top:5px;">
                        <noindex>
                            <!--LiveInternet counter--><script type="text/javascript"><!--
                                document.write("<a href='http://www.liveinternet.ru/click' "+
                                "target=_blank><img src='//counter.yadro.ru/hit?t41.5;r"+
                                escape(document.referrer)+((typeof(screen)=="undefined")?"":
                                ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                                ";"+Math.random()+
                                "' alt='' title='LiveInternet' "+
                                "border='0' width='31' height='31'><\/a>")
                                //--></script><!--/LiveInternet-->
                        </noindex>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?endif?>
<div class="modal fade <?='new-free-form '.LANGUAGE_ID//=$USER->IsAdmin()?'new-free-form '.LANGUAGE_ID:'';?>" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&#9587; <!--&times;--></span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="information" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<?if($USER->GetID()=='16261'): //addblock modal?>
<!--<script src="--><?//=SITE_TEMPLATE_PATH?><!--/js/ads.js"></script>-->
<!--<script>-->
<!--    $(function(){-->
<!--        if( window.canRunAds === undefined ){-->
<!--            $.ajax({-->
<!--                type: "POST",-->
<!--                url: '/tpl/ajax/add_block_handler.php',-->
<!--                data: 'act=check'-->
<!--            })-->
<!--            .done(function (data) {-->
<!--                console.log(data,'data_a_b');-->
<!--                data = JSON.parse(data);-->
<!--                if (!data.freeze_a) {-->
<!--                    $('#a_block_win').modal('show');-->
<!---->
<!--                    $('#a_block_win').on('hidden.bs.modal', function (e) {-->
<!--                        $.ajax({-->
<!--                            type: "POST",-->
<!--                            url: '/tpl/ajax/add_block_handler.php'-->
<!--                        })-->
<!--                            .done(function (data) {-->
<!--                                console.log('a-b-closed');-->
<!--                            });-->
<!--                    })-->
<!--                }-->
<!--            })-->
<!--        }-->
<!--    });-->
<!--</script>-->
<!--<div class="modal fade" data-backdrop="static" id="a_block_win" tabindex="-1" role="dialog">-->
<!--    <div class="modal-dialog">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-close">-->
<!--                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&#9587; <!--&times;--><!--</span><span class="sr-only">Close</span></button>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<!--                <div class="a_b_text">-->
<!--                    <strong>--><?//=GetMessage('add_block_message')?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<?endif?>

<?
setSeo();
?>

<?php $APPLICATION->IncludeFile("/tpl/include_areas/counters-from-muslim.php",Array(),Array("MODE"=>"php","NAME"=>"Счетчики"));?>

<?
if ($_COOKIE["NEED_SELECT_CITY"]=="Y"):?>
    <script>
        $('#information .modal-body').load('/tpl/ajax/city_select2.php?<?=bitrix_sessid_get()?>', function(data) {
            $('#information').modal("show");
        });
    </script>
    <?unset($_COOKIE["NEED_SELECT_CITY"]);?>
<?endif;?>
<div id="system_loading"><?=GetMessage('popup_downloading_mess')?></div>

</body>
</html>