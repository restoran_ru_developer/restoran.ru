function sendMetric() {
    try{
        ga('send', 'event', 'order', 'sent');
    }
    catch(e) {
        console.log(e);
    }
}

if(window.addEventListener){
    window.addEventListener('load', function(){
        sendMetric();
    }, false);
} else {
    window.attachEvent('onload', function(){
        sendMetric();
    });
}
