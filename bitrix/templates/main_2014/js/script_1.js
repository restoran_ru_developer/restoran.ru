var ajax_load = false;
$(function(){
    //loading...
    $(document).bind("ajaxSend",function(){        
        $("#system_loading").show(); ajax_load=true;
    }).bind("ajaxComplete",function(){
        $("#system_loading").hide(); ajax_load=false;
    });
    //To booking
    $(document).on('click', 'a.booking', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var params = {"id":$(this).data("id"),"name":$(this).data("restoran")};
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),
            data: params,            
        })
        .done(function(data) {
            $('#booking_form .modal-body').html(data);
        });
        $('#booking_form .modal-dialog').removeClass("small");
        $('#booking_form').modal('show');           
        return false;        
    });
    //To auth
    $(document).on('click', 'a.auth', function (e) {
        e.stopPropagation();
        e.preventDefault();        
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),                      
        })
        .done(function(data) {
            $('#booking_form .modal-body').html(data);
        });        
        $('#booking_form .modal-dialog').addClass("small");           
        $('#booking_form').modal('show');           
        return false;        
    });
    //To show hide element on click
    $(document).on('click', '[data-toggle=toggle]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#"+$(this).data("target")).toggle();     
        if ($(this).data("class"))
        {
            if ($(this).next().hasClass($(this).data("class")))
                $(this).hide().next().show();
            else
                $(this).hide().prev().show();
        }
        return false;        
    });
    //Slide to anchor position
    $(document).on('click', '[data-toggle=anchor]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var pos=$($(this).attr("href")).offset().top-50;
        $('html,body').animate({scrollTop:pos},"300");
        return false;        
    });
    //detail_tab to anchor position
    $(document).on('click', '[data-toggle=detail_tabs]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".detail_tabs").hide();
        $($(this).attr("href")).show();
        $(".anchors-list a").removeClass("asc");
        $(this).addClass("asc");
        return false;        
    });
    //to favorites
    $(document).on('click', '[data-toggle=favorite]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var _this = $(this);
        if (!$(this).data("restoran"))
        {
            alert("Произошла ошибка, попробуйте позже")
            return false;
        }
        var params = {"id":$(this).data("restoran")};        
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),
            data: params,
            dataType: 'json'
        })        
        .done(function(data) {          
            console.log(data);
            if (data.MESSAGE.length>0)
                var o = data.MESSAGE;
            else
                var o = data.ERROR;
            _this.tooltip({'title':o,'trigger':"manual",'placement':'top'});
            _this.tooltip("show");            
            setTimeout(function(){_this.tooltip("hide")},1000);
        });
        return false;        
    });
    //send message to user
    $(document).on('click', 'a.send_message', function (e) {
        var id = $(this).data("id");
        var name = $(this).data("name");
        $.ajax({
            type:"POST",
            url:"/tpl/ajax/im.php",
            data:"USER_NAME="+name+"&USER_ID="+id,
            success:function(data){
                 $('#booking_form .modal-dialog').addClass("small");
                $('#booking_form .modal-body').html(data);
                $('#booking_form').modal("show");   
            }
        });
    });
    //Invite2rest
    $(document).on('click', 'a.invite2rest', function (e) {
        var params = $(this).data("params");
        $.ajax({
            type:"POST",
            url:"/tpl/ajax/invite2rest.php",
            data:params,
            success:function(data){
                $('#booking_form .modal-dialog').addClass("small");
                $('#booking_form .modal-body').html(data);
                $('#booking_form').modal("show"); 
            }
        });
    });    



    //form main scrollable content
    $('.nav-tabs a.news_scrollable').click(function(){
        $(".rest_news_cont").hide();
        $("#"+$(this).data("scrollable")).show();
    });
    //for ajax loading tabs       
    $(".nav-tabs a.ajax").click(function(e){
        e.preventDefault();                
        var p = $($(this).attr("href"));
        var _this = $(this);
        if (null!=p&&p.is(":empty"))
        {
            p.load($(this).data("href"),function(){
                _this.tab('show');
            });
            return false;
        }        
    });
     //for history tabs   
    if (null!=$(".nav-tabs.history"))
    {
        var anchor = window.location && window.location.hash;
        $('[data-toggle="tab"], [data-toggle="pill"]').each(function () {
            var $potentialTab = $(this);

            if(($potentialTab.attr('href') === anchor || $potentialTab.data('target') === anchor)) {
                $potentialTab.tab("show");
                var scrollListener = function resetAnchorScroll () {
                    window.removeEventListener('scroll', scrollListener);
                    window.scrollTo(0,0);
                };  
                window.addEventListener('scroll', scrollListener);
            }
        });
    }
    $(".nav-tabs.history a").click(function(e){
        e.preventDefault();   
        e.stopPropagation();

        var _this = $(this);
//        var a = new function() {
//            _this.tab('show');
//        }
        location.hash = $(this).attr("href");
        setTimeout(function(){_this.tab('show')},100);
        //$(window).scrollTop(a+"px");
    });
    //Scroll top top (animation?!) [always on page]
    $(".ontop").click(function(){
        //$("html,body").scrollTop(0);
        $('html,body').animate({scrollTop:0},"300");
    });
    //to send search form [always on page]
    $(".search_submit").click(function (){
        $(this).parents("form").submit();
    });
    //For filter
    if (null!=$(".filter"))
    {
        $('.filter_box .dropdown-menu input[type=hidden]').each(function(){
            var id = $(this).parents('.dropdown-menu').data('prop');
           
            if ($("#new_filter_results").is(":hidden"))
                    $("#new_filter_results").css("display","block");  
            $('<li class="fil_option" id="val'+$(this).attr("value")+'" data-val="'+$(this).attr("value")+'">' + $(this).data("val") + '<a href="javascript:void(0)"></a></li>').prependTo('#new_filter_results ul#multi'+id);        
            $('ul#multi'+id).show();
        });
        $(".filter .dropdown-menu").click(function(e){
            e.stopPropagation();
        });
        $(".filter .dropdown-menu a").click(function(){            
            var id = $(this).parents(".dropdown-menu").data("prop");
            var nfr = $("#new_filter_results");
            var code = $(this).parents('.dropdown-menu').data('code');
            if ($(this).data("code"))
                code = $(this).data("code");

            $(this).toggleClass("selected");
            if (!$("#val"+$(this).attr("id")).hasClass("fil_option"))
            {                
                $("#multi"+id).prepend('<li class="fil_option" id="val'+$(this).attr("id")+'" data-val="'+$(this).attr("id")+'">' + $(this).data("val") + '<a href="javascript:void(0)"></a></li>');            
                $('#multi'+id).show();
                
                if (code=="breakfast")
                    $(this).parents('.dropdown-menu').append("<input type='hidden' data-val='"+$(this).data("val")+"' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"]' value='"+$(this).attr("id")+"' />");
                else
                    $(this).parents('.dropdown-menu').append("<input type='hidden' data-val='"+$(this).data("val")+"' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"][]' value='"+$(this).attr("id")+"' />");

            }
            else
            {
                $("#val"+$(this).attr("id")).remove();
                $("#hid"+$(this).attr("id")).remove();
            }
            if (nfr.is(":hidden")||nfr.find(".fil_option").length>0)
                nfr.show();
            else
                nfr.hide();
            if (typeof map_fil !=="undefined")
            {
                map_fil = $("#map_fil").serialize();
                UpdateMarkers();
            }
        });
        $(".filter_button").click(function(){
            $(this).parents(".dropdown").removeClass("open");
        });
        $('#new_filter_results').on('click', 'a',function() {
            var value = $(this).parent().data("val");
            if ($(this).parent().parent().find(".fil_option").length==1)
                $(this).parent().parent().hide();
            if ($(this).parent().parent().parent().find(".fil_option").length==1)
                $("#new_filter_results").hide();
            $(this).parent().remove();       
            $("#"+value).removeClass("selected");
            $("#hid"+value).remove();
        });
        $('.title_box a').click(function(e){    
            $(this).parents(".dropdown-menu").find("input[type='hidden']").remove();
            $(this).parents(".dropdown-menu").find("ul a").removeClass("selected");
            //$("."+$(this).data("filid")+" input[type='hidden']").remove();
            //$("."+$(this).data("filid")+" a").removeClass("selected");
            $("ul#"+$(this).data("filid")).find("li").remove();        
            $("ul#"+$(this).data("filid")).hide();                
            if ($("#new_filter_results").find(".fil_option").length==0)
                $("#new_filter_results").hide();
            $(this).parents(".dropdown").removeClass("open");
        });
    }    
    //small galery on the best page of restaurants
    if (null!=$(".priority-slider")) 
    {
        $(".content").on("slide.bs.carousel",".priority-slider",function(e){        
            if (e.direction == "left")
            {
                var nextImage = $('.active.item', this).next('.item').find('img');
                if (null==nextImage.attr("src"))
                    nextImage.attr('src', nextImage.data('url'));
            }
            if (e.direction == "right")
            {
                if (!$('.active.item', this).prev('.item').length)
                {                
                    var nextImage = $('.item', this).last().find('img');
                    if (null==nextImage.attr("src"))
                        nextImage.attr('src', nextImage.data('url'));
                }
                else
                {
                    var nextImage = $('.active.item', this).prev('.item').find('img');
                    if (null==nextImage.attr("src"))
                        nextImage.attr('src', nextImage.data('url'));
                }
            }                
        });
    }
    //choose rating stars, when user choose rating need to focus text input
    if (null!=$(".choose-ratio"))
    {
        $(".choose-ratio span.glyphicon").click(
            function(){
                $("#review").focus();                               
                var count = $(this).index()+1;
                var i = 1;
                $("#input_ratio").attr("value",count);
                $(this).parent().find("span.glyphicon").each(function(){                
                    if (i<=count)
                        $(this).attr("class","glyphicon glyphicon-star");
                    else
                        $(this).attr("class","glyphicon glyphicon-star-empty");
                    i++;
                })
            }
        ); 
    }        
    
    
    //select where user would like to search by add hidden input
    if (null!=$(".search_box .dropdown-menu")) {
        $(".search_box .dropdown-menu a").click(function(){
            $("#search_in").val($(this).data("value"));
            $("#search_in_title .t").text($(this).text());
            return false;
        });
    }
});