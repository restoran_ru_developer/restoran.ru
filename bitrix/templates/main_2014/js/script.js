var page = 0;
var ajax_load = false;
var selectize;//    инициализация тут: bitrix/templates/main_2014/components/restoran/catalog.filter/filter_2014/template.php
var one_submit = false;
$(function(){
    $('.dont-show').hide();
    //loading...
    $(document).bind("ajaxSend",function(){        
        $("#system_loading").show(); ajax_load=true;
    }).bind("ajaxComplete",function(){
        $("#system_loading").hide(); ajax_load=false;
    });
    //To booking
    $(document).on('click', 'a.booking', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var params = {"id":$(this).data("id"),"name":$(this).data("restoran"),"network_list":$(this).data("network_list")};
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),
            data: params,            
        })
        .done(function(data) {
            $('#booking_form .modal-body').html(data);
        });
        $('#booking_form .modal-dialog').removeClass("small");
        $('#booking_form').modal('show');           
        return false;        
    });
    //To auth
    $(document).on('click', 'a.auth', function (e) {
        e.stopPropagation();
        e.preventDefault();        
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),                      
        })
        .done(function(data) {
            $('#booking_form .modal-body').html(data);
        });        
        $('#booking_form .modal-dialog').addClass("small");           
        $('#booking_form').modal('show');           
        return false;        
    });
    //To show hide element on click
    $(document).on('click', '[data-toggle=toggle]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#"+$(this).data("target")).toggle();     
        if ($(this).data("class"))
        {
            if ($(this).next().hasClass($(this).data("class")))
                $(this).hide().next().show();
            else
                $(this).hide().prev().show();
        }
        return false;        
    });
    //Slide to anchor position
    $(document).on('click', '[data-toggle=anchor]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if($(this).attr("href")=='#comment_form'){
            $('#comment_form').show();
        }
        var pos=$($(this).attr("href")).offset().top-50;
        $('html,body').animate({scrollTop:pos},"300");

        return false;
    });
    //detail_tab to anchor position
    $(document).on('click', '[data-toggle=detail_tabs]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(".detail_tabs").hide();
        $($(this).attr("href")).show();
        $(".anchors-list a").removeClass("asc");
        $(this).addClass("asc");
        return false;        
    });
    //to favorites
    $(document).on('click', '[data-toggle=favorite]', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var _this = $(this);
        if (!$(this).data("restoran"))
        {
            alert("Произошла ошибка, попробуйте позже")
            return false;
        }
        var params = {"id":$(this).data("restoran")};
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),
            data: params,
            dataType: 'json'
        })        
        .done(function(data) {          
            console.log(data);
            if (data.MESSAGE.length>0)
                var o = data.MESSAGE;
            else
                var o = data.ERROR;
            _this.tooltip({'title':o,'trigger':"manual",'placement':'top'});
            _this.tooltip("show");            
            setTimeout(function(){_this.tooltip("hide")},1000);
        });
        return false;        
    });
    //send message to user
    $(document).on('click', 'a.send_message', function (e) {
        var id = $(this).data("id");
        var name = $(this).data("name");
        $.ajax({
            type:"POST",
            url:"/tpl/ajax/im.php",
            data:"USER_NAME="+name+"&USER_ID="+id,
            success:function(data){
                 $('#booking_form .modal-dialog').addClass("small");
                $('#booking_form .modal-body').html(data);
                $('#booking_form').modal("show");   
            }
        });
    });

    //Invite2rest
    $(document).on('click', 'a.invite2rest', function (e) {
        var params = $(this).data("params");
        $.ajax({
            type:"POST",
            url:"/tpl/ajax/invite2rest.php",
            data:params,
            success:function(data){
                $('#booking_form .modal-dialog').addClass("small");
                $('#booking_form .modal-body').html(data);
                $('#booking_form').modal("show"); 
            }
        });
    });    



    //form main scrollable content
    $('.nav-tabs a.news_scrollable').click(function(){
        $(".rest_news_cont").hide();
        $("#"+$(this).data("scrollable")).show();
    });
    //for ajax loading tabs       
    $(".nav-tabs a.ajax").click(function(e){
        e.preventDefault();                
        var p = $($(this).attr("href"));
        var _this = $(this);
        if (null!=p&&p.is(":empty"))//&&_this.data('afisha')!='y'
        {
            p.load($(this).data("href"),function(){
                _this.tab('show');

            });
            return false;
        }        
    });

    $(".ajax_date_tabs a.ajax").click(function(e){
        e.preventDefault();
        var p = $($(this).attr("href"));
        var _this = $(this);
        if (null!=p&&p.is(":empty"))//&&_this.data('afisha')!='y'
        {
            p.load($(this).data("href"),function(){
                _this.tab('show');

            });
            return false;
        }
    });


    //  для главной - табов
    $(".nav-tabs.new-history a.ajax").each(function(e){
        //e.preventDefault();
        var anchor = window.location && window.location.hash;

        if(anchor===$(this).attr("href")){
            var p = $($(this).attr("href"));
            var _this = $(this);
            if (null!=p&&p.is(":empty"))//&&_this.data('afisha')!='y'
            {
                p.load($(this).data("href"),function(){
                    _this.tab('show');
                    _this.parents('.index-nav-tabs-wrapper').find('.link-to-all-from-journal').hide();
                    $('#link-to-all-'+_this.attr("href").replace(/#index_journal_/, '')).show();
                });
            }
        }
    });
    $(".nav-tabs.new-history a").click(function(e){
        location.hash = $(this).attr("href");
        $(this).parents('.index-nav-tabs-wrapper').find('.link-to-all-from-journal').hide();
        $('#link-to-all-'+$(this).attr("href").replace(/#index_journal_|#/, '')).show();
    });


     //for history tabs   
    //if (null!=$(".nav-tabs.history"))
    if ($(".nav-tabs.history").length>0)
    {
        var anchor = window.location && window.location.hash;
        $('[data-toggle="tab"], [data-toggle="pill"]').each(function () {
            var $potentialTab = $(this);

            if(($potentialTab.attr('href') === anchor || $potentialTab.data('target') === anchor)) {
                $potentialTab.tab("show");
                var scrollListener = function resetAnchorScroll () {
                    window.removeEventListener('scroll', scrollListener);
                    window.scrollTo(0,0);
                };  
                window.addEventListener('scroll', scrollListener);
            }
        });
    }
    $(".nav-tabs.history a").click(function(e){
        e.preventDefault();   
        e.stopPropagation();

        var _this = $(this);
//        var a = new function() {
//            _this.tab('show');
//        }
        location.hash = $(this).attr("href");
        setTimeout(function(){_this.tab('show')},100);
        //$(window).scrollTop(a+"px");
    });
    //Scroll top top (animation?!) [always on page]

    $(window).scroll(function() {
        if ($(this).scrollTop()>200){
            $('.to-top-btn-bg').fadeIn();
        }
        else {
            $('.to-top-btn-bg').fadeOut();
        }
    });

    $(".ontop, .to-top-btn-bg").click(function(){
        //$("html,body").scrollTop(0);
        $('html,body').animate({scrollTop:0},"300");
    });
    //to send search form [always on page]
    $(".search_submit").click(function (){
        if (!$(".search_input").val())
        {
            var a = $(".search_input").attr("placeholder").replace("Например, ресторан ","").replace("Example, restaurant ","");        
            $(".search_input").val(a);
        }
        $(this).parents("form").submit();
    });
    //For filter
    if (null!=$(".filter"))
    {
        $('.filter_box .dropdown-menu input[type=hidden]').each(function(){
            var id = $(this).parents('.dropdown-menu').data('prop');
           
            if ($("#new_filter_results").is(":hidden"))
                    $("#new_filter_results").css("display","block");  
            $('<li class="fil_option" id="val'+$(this).attr("value")+'" data-val="'+$(this).attr("value")+'">' + $(this).data("val") + '<a href="javascript:void(0)"></a></li>').prependTo('#new_filter_results ul#multi'+id);        
            $('ul#multi'+id).show();
        });
        $(".filter .dropdown-menu").click(function(e){
            e.stopPropagation();
        });
        //$(".filter .dropdown-menu.toc a").click(function(){
        //    var id = $(this).parents(".dropdown-menu").data("prop");
        //    var nfr = $("#new_filter_results");
        //    var code = $(this).parents('.dropdown-menu').data('code');
        //    if ($(this).data("code"))
        //        code = $(this).data("code");
        //
        //    $(this).toggleClass("selected");
        //    if (!$("#val"+$(this).attr("id")).hasClass("fil_option"))
        //    {
        //        $("#multi"+id).prepend('<li class="fil_option" id="val'+$(this).attr("id")+'" data-val="'+$(this).attr("id")+'">' + $(this).data("val") + '<a href="javascript:void(0)"></a></li>');
        //        $('#multi'+id).show();
        //
        //        if (code=="breakfast")
        //            $(this).parents('.dropdown-menu').append("<input type='hidden' data-val='"+$(this).data("val")+"' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"]' value='"+$(this).attr("id")+"' />");
        //        else
        //            $(this).parents('.dropdown-menu').append("<input type='hidden' data-val='"+$(this).data("val")+"' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"][]' value='"+$(this).attr("id")+"' />");
        //
        //    }
        //    else
        //    {
        //        $("#val"+$(this).attr("id")).remove();
        //        $("#hid"+$(this).attr("id")).remove();
        //    }
        //    if (nfr.is(":hidden")||nfr.find(".fil_option").length>0)
        //        nfr.show();
        //    else
        //        nfr.hide();
        //    if (typeof map_fil !=="undefined")
        //    {
        //        map_fil = $("#map_fil").serialize();
        //        UpdateMarkers();
        //    }
        //});
        $('.title_box a').click(function(e){
            $(this).parents(".dropdown-menu").find("input[type='hidden']").remove();
            $(this).parents(".dropdown-menu").find("ul a").removeClass("selected");
            //$("."+$(this).data("filid")+" input[type='hidden']").remove();
            //$("."+$(this).data("filid")+" a").removeClass("selected");
            $("ul#"+$(this).data("filid")).find("li").remove();
            $("ul#"+$(this).data("filid")).hide();
            if ($("#new_filter_results").find(".fil_option").length==0)
                $("#new_filter_results").hide();
            $(this).parents(".dropdown").removeClass("open");

            if($(this).attr('data-filter')=='filter_popup_409' || $(this).attr('data-filter')=='filter_popup_54'){
                $('.subway_station').each(function(ind,element){
                    $(element).removeClass('selected');
                });
                selectize.clear();
            }
        });
        $(".filter .dropdown-menu.toc a").click(function(){

            if($(this).parents('.title_box').length>0){
                return false;
            }

            $(this).parents(".dropdown").removeClass("open");

            var id = $(this).parents(".dropdown-menu").data("prop");
            var nfr = $("#new_filter_results");
            var code = $(this).parents('.dropdown-menu').data('code');

            if ($(this).data("code"))
                code = $(this).data("code");

            $(this).toggleClass("selected");
            if (!$("#val"+$(this).attr("id")).hasClass("fil_option"))
            {
                if(!one_submit){

                    $(this).parents('form').find('input[data-val]').remove();
                    $("#new_filter_results > ul > li").remove();

                    $("#multi"+id).prepend('<li class="fil_option" id="val'+$(this).attr("id")+'" data-val="'+$(this).attr("id")+'">' + $(this).data("val") + '<a href="javascript:void(0)"></a></li>');
                    $('#multi'+id).show();

                    if (code=="breakfast")
                        $(this).parents('.dropdown-menu').append("<input type='hidden' data-val='"+$(this).data("val")+"' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"]' value='"+$(this).attr("id")+"' />");
                    else
                        $(this).parents('.dropdown-menu').append("<input type='hidden' data-val='"+$(this).data("val")+"' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"][]' value='"+$(this).attr("id")+"' />");

                    $(this).parents('form').submit();
                    one_submit = true;
                }

            }
            else
            {
                $("#val"+$(this).attr("id")).remove();
                $("#hid"+$(this).attr("id")).remove();
            }
            if (nfr.is(":hidden")||nfr.find(".fil_option").length>0)
                nfr.show();
            else
                nfr.hide();
            if (typeof map_fil !=="undefined")
            {
                map_fil = $("#map_fil").serialize();
                UpdateMarkers();
            }

            if($(this).data('val')){
                return false;
            }
        });
        $(".filter_button").click(function(){
            $(this).parents(".dropdown").removeClass("open");
        });
        $('#new_filter_results').on('click', 'a',function() {

            //console.log('hide');
            if($('#YMapsID').length>0){
                location.href = $('#map-page').text();
            }

            var value = $(this).parent().data("val");
            if ($(this).parent().parent().find(".fil_option").length==1)
                $(this).parent().parent().hide();
            if ($(this).parent().parent().parent().find(".fil_option").length==1)
                $("#new_filter_results").hide();
            $(this).parent().remove();       
            $("#"+value).removeClass("selected");
            $("#hid"+value).remove();
        });

    }    
    //small galery on the best page of restaurants
    if (null!=$(".priority-slider")) 
    {
        $(".content").on("slide.bs.carousel",".priority-slider",function(e){        
            if (e.direction == "left")
            {
                var nextImage = $('.active.item', this).next('.item').find('img');
                if (null==nextImage.attr("src"))
                    nextImage.attr('src', nextImage.data('url'));
            }
            if (e.direction == "right")
            {
                if (!$('.active.item', this).prev('.item').length)
                {                
                    var nextImage = $('.item', this).last().find('img');
                    if (null==nextImage.attr("src"))
                        nextImage.attr('src', nextImage.data('url'));
                }
                else
                {
                    var nextImage = $('.active.item', this).prev('.item').find('img');
                    if (null==nextImage.attr("src"))
                        nextImage.attr('src', nextImage.data('url'));
                }
            }                
        });
    }
    //choose rating stars, when user choose rating need to focus text input
    if (null!=$(".choose-ratio"))
    {
        $(".choose-ratio span.glyphicon").click(
            function(){
                $("#review").focus();                               
                var count = $(this).index()+1;
                var i = 1;
                $("#input_ratio").attr("value",count);
                $(this).parent().find("span.glyphicon").each(function(){                
                    if (i<=count)
                        $(this).attr("class","glyphicon glyphicon-star");
                    else
                        $(this).attr("class","glyphicon glyphicon-star-empty");
                    i++;
                })
            }
        ); 
    }


    //select where user would like to search by add hidden input
    if (null!=$(".search_box .dropdown-menu")) {
        $(".search_box .dropdown-menu a").click(function(){
            $("#search_in").val($(this).data("value"));
            $("#search_in_title .t").text($(this).text());
            return false;
        });
    }


    $('.block').on('click','.js-restaurant-list-more',function(){
        trigger = $(this);
        if(!ajax_load){
            page = +$(this).attr('nav-page-num');
            page++;
            max_page = +$(this).attr('nav-page-count');

            if(page>=max_page){
                $(this).hide();
            }

            if(!ajax_load && page<=max_page){
                console.log('lets go to get 0');
                ajax_load = 1;
                $.ajax({
                    type: "GET",
                    url: location.pathname+location.search,
                    data: {
                        page:page,
                        PAGEN_1:page,
                        AJAX_REQUEST:'Y'
                    },
                    success: function(data) {
                        if(trigger.hasClass('for-news-list')){
                            $(".navigation").html();
                            $(".news-list .item").last().after(data);
                            navigation_body_new = $('.news-list').find('.navigation').html();
                            $('.news-list').find('.navigation').remove();
                            $(".navigation").html(navigation_body_new);
                        }
                        else if(trigger.hasClass('for-opinion-list')){
                            $(".navigation").remove();
                            $(".reviews-list .item").last().after(data);
                        }
                        else {
                            $(".priority-list .hr").last().after(data);
                            $(".navigation").html($(".navigation_temp").html());
                            $(".navigation_temp").remove();
                        }

                        ajax_load = 0;
                    }
                });
            }
        }
    })

});

//$(document).ready(function(){
    /** from old fight**/
    $(".modal_close").live('click',function(){
        $(this).parents(".popup_modal").css("display","none");
        hideOverflow();
    });
    $(".modal_close_galery").live('click',function(){
        $(".big_modal").css("display","none");
        hideOverflow();
    });

    function setCenter(obj)
    {
        var ow = obj.width();
        var oh = obj.height();
        var opt = obj.css("padding-top");
        if (opt)
            opt = opt.replace("px","");
        var opb = obj.css("padding-bottom");
        if (opb)
            opb = opb.replace("px","");
        var opl = obj.css("padding-left");
        if (opl)
            opl = opl.replace("px","");
        var opr = obj.css("padding-right");
        if (opr)
            opr = opr.replace("px","");
        if (opl=="auto")
            opl = 0;
        if (opr=="auto")
            opr = 0;
        if (opb=="auto")
            opb = 0;
        if (opt=="auto")
            opt = 0;

        var omt = obj.css("margin-top");
        if (omt)
            omt = omt.replace("px","");
        var omb = obj.css("margin-bottom");
        if (omb)
            omb = omb.replace("px","");
        var oml = obj.css("margin-left");
        if (oml)
            oml = oml.replace("px","");
        var omr = obj.css("margin-right");
        if (omr)
            omr = omr.replace("px","");
        if (oml=="auto")
            oml = 0;
        if (omr=="auto")
            omr = 0;
        if (omb=="auto")
            omb = 0;
        if (omt=="auto")
            omt = 0;
        oh = oh*1 + opt*1 + opb*1 + omt*1 + omb*1;
        ow = ow*1 + opl*1 + opr*1 + oml*1 + omr*1;
        if (obj.attr("class")=="big_modal")
        {
            obj.css("position","absolute");
        }
        //obj.css("position","absolute");
        obj.css("top",($(window).height()-oh)/2+"px");
        obj.css("left",($(window).width()-ow)/2+"px");
        obj.css("z-index",10000);
    }
    function showOverflow()
    {
        $("body").css("overflow","hidden");
        //$("#system_overflow").css("display","block");
        $("#system_overflow").fadeIn(300);
        //$("#system_overflow").css("opacity",.5);
        $("#system_overflow").css("height",$(document).height());
    }
    function hideOverflow()
    {
        //$("#system_overflow").css("display","none");
        $("#system_overflow").fadeOut(300);
        $("body").css("overflow","auto");
    }
    function send_message(id,name)
    {
        $.ajax({
            type:"POST",
            url:"/tpl/ajax/im.php",
            data:"USER_NAME="+name+"&USER_ID="+id,
            success:function(data){
                //data = eval(data);
                $('#booking_form .modal-dialog').addClass("small");
                $('#booking_form .modal-body').html(data);
                $('#booking_form').modal("show");

                $("#send_message").click(function(){
                    var form = $(this).parents('form');
                    console.log(form);
                    console.log(form.attr("action"));
                    console.log(form.serialize());
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize(),
                        success: function(data) {
                            $(".modal-body").html(data);
                        }
                    });
                });

            }
        });
        return false;
    }
    /** from old fight**/
//});
