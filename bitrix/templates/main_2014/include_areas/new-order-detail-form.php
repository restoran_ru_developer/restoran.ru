<?if(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='urm'||CITY_ID=='rga'):?>
<?global $element_id,$RESTORAN_NAME,$SHOW_REST_PHONE_IN_ORDER_BANNER, $restaurant_phone,$REST_NETWORK;
?>
<?if(LANGUAGE_ID!='en'):?>

<div class="detail-order-banner">
    <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
         <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">
    <?else:?>
        <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">
    <?endif;?>
            <div class="detail-order-banner-title"><?=$_REQUEST["CATALOG_ID"]=='restaurants'?"Бронировать столик":"Бронировать банкет"?></div>
            <div class="detail-order-banner-in-place-str">
            <?if(!$RESTORAN_NAME):?>
                <div>в ресторанах<br><?=CITY_ID=='msk'?'Москвы':''?><?=CITY_ID=='spb'?'Санкт-Петербурга':''?><?=CITY_ID=='urm'?'Юрмалы':''?><?=CITY_ID=='rga'?'Риги':''?></div>
            <?else:?>
                <div>в ресторане<br><?=$RESTORAN_NAME?></div>
            <?endif?>
            </div>
            <div class="detail-order-banner-addition-text-str">
                Быстро
                <br>
                Бесплатно
                <br>
                <?= ($_REQUEST["CATALOG_ID"] == "banket") ? "Оператор свяжется с Вами в течение 30 мин." : "СМС-подтверждение<br>через 5 минут" ?>
            </div>
            <div class="detail-order-banner-above-phone-str">на сайте или по телефону</div>

            <?if(!$SHOW_REST_PHONE_IN_ORDER_BANNER):?>
                <div class="detail-order-banner-phone-str"><?if(CITY_ID=='msk')echo "+7 (495) 988-26-56";elseif(CITY_ID=='spb')echo "+7 (812) 740-18-20";elseif(CITY_ID=='rga'||CITY_ID=='urm')echo "+371 661-031-06";?></div>
            <?else:?>
                <?

                if(preg_match('/,/',$restaurant_phone)){
                    $phone_str_arr = explode(',',$restaurant_phone);
                    $phone_str = $phone_str_arr[0];
                }
                elseif(preg_match('/;/',$restaurant_phone)){
                    $phone_str_arr = explode(';',$restaurant_phone);
                    $phone_str = $phone_str_arr[0];
                }
                else {
                    $phone_str = trim(preg_replace('/[^-+0-9() ]+/i','',$restaurant_phone));
                }
//                }

                ?>
                <div class="detail-order-banner-phone-str"><?=$phone_str?></div>
            <?endif;?>

            <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking detail-banner-order-button" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">Начать бронирование<span></span></a>
            <?else:?>
                <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking detail-banner-order-button" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">Начать бронирование<span></span></a>
            <?endif?>
        </a>
</div>
<?else:?>
    <div class="detail-order-banner en-banner">
        <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
            <a href="/tpl/ajax/online_order_rest.php" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">
        <?else:?>
            <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">
        <?endif;?>
                <div class="detail-order-banner-title">Book<br>a table</div>
                <div class="detail-order-banner-in-place-str">
<!--                    --><?//if(!$RESTORAN_NAME):?>
                        <div>at the restaurants<br><?=CITY_ID=='msk'?'at the Moscow':''?><?=CITY_ID=='spb'?'in St. Petersburg':''?><?=CITY_ID=='urm'?'in Jurmala':''?><?=CITY_ID=='rga'?'in Riga':''?></div>
<!--                    --><?//else:?>
<!--                        <div>at the restaurant<br>--><?//=$RESTORAN_NAME?><!--</div>-->
<!--                    --><?//endif?>
                </div>
                <div class="detail-order-banner-addition-text-str">
                    Fast
                    <br>
                    Free of charge
                    <br>
                    <?//= ($_REQUEST["CATALOG_ID"] == "banket") ? "Оператор свяжется с Вами в течение 30 мин." : "СМС-подтверждение<br>через 5 минут" ?>
                    <span>SMS-confirmation<br>in 5 minutes</span>
                </div>
                <div class="detail-order-banner-above-phone-str">by the web or by phone</div>

                <?if(!$SHOW_REST_PHONE_IN_ORDER_BANNER):?>
                    <div class="detail-order-banner-phone-str"><?if(CITY_ID=='msk')echo "+7 (495) 988-26-56";elseif(CITY_ID=='spb')echo "+7 (812) 740-18-20";elseif(CITY_ID=='rga'||CITY_ID=='urm')echo "+371 661-031-06";?></div>
                <?else:?>
                    <?

                    if(preg_match('/,/',$restaurant_phone)){
                        $phone_str_arr = explode(',',$restaurant_phone);
                        $phone_str = $phone_str_arr[0];
                    }
                    elseif(preg_match('/;/',$restaurant_phone)){
                        $phone_str_arr = explode(';',$restaurant_phone);
                        $phone_str = $phone_str_arr[0];
                    }
                    else {
                        $phone_str = trim(preg_replace('/[^-+0-9() ]+/i','',$restaurant_phone));
                    }
//                }

                    ?>
                    <div class="detail-order-banner-phone-str"><?=$phone_str?></div>
                <?endif;?>

                <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                    <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking detail-banner-order-button" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">Start booking<span></span></a>
                <?else:?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking detail-banner-order-button" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>' data-network_list="<?=implode('|',$REST_NETWORK)?>">Start booking<span></span></a>
                <?endif?>
            </a>
    </div>
<?endif?>
<?endif?>