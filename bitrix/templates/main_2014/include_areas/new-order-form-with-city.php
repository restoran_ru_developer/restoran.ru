<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
    <?if(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='urm'||CITY_ID=='rga'):?>
        <?if(LANGUAGE_ID!='en'):?>
            <div class="detail-order-banner <?=$arParams['BANNER_TYPE']?>">
                <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking" >
                    <?else:?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking" >
                        <?endif?>
                        <div class="detail-order-banner-title"><?=stristr($APPLICATION->GetCurDir(),'/banquet-service/')||stristr($APPLICATION->GetCurDir(),'/banket/')?"Бронировать банкет":"Бронировать столик";?></div>
                        <?if($arParams['BANNER_TYPE']=='ginza'):?>
                            <div class="detail-order-banner-in-place-str"><div>в ресторанах<br><span>Ginza Project</span></div></div>
                        <?elseif($arParams['BANNER_TYPE']=='bear'):?>
                            <div class="detail-order-banner-in-place-str"><div>в пивных<br><span>ресторанах</span></div></div>
                        <?elseif($arParams['BANNER_TYPE']=='panorama'):?>
                            <div class="detail-order-banner-in-place-str"><div>в панорамных<br><span>ресторанах</span></div></div>
                        <?else:?>
                            <div class="detail-order-banner-in-place-str"><div>в ресторанах<br><span><?=CITY_ID=='msk'?'Москвы':''?><?=CITY_ID=='spb'?'Санкт-Петербурга':''?><?=CITY_ID=='urm'?'Юрмалы':''?><?=CITY_ID=='rga'?'Риги/Юрмалы':''?></span></div></div>
                        <?endif?>
                        <div class="detail-order-banner-addition-text-str">
                            Быстро
                            <br>
                            Бесплатно
                            <br>
                            Профессионально
                        </div>
                        <div class="detail-order-banner-above-phone-str">на сайте или по телефону</div>
                        <div class="detail-order-banner-phone-str"><?if(CITY_ID=='msk')echo "+7 (495) 988-26-56";elseif(CITY_ID=='spb')echo "+7 (812) 740-18-20";elseif(CITY_ID=='rga'||CITY_ID=='urm')echo "+371 661-031-06";?></div>
                        <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                            <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking detail-banner-order-button" >Начать бронирование<span></span></a>
                        <?else:?>
                            <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking detail-banner-order-button" >Начать бронирование<span></span></a>
                        <?endif?>
                    </a>
            </div>
        <?else:?>
            <div class="detail-order-banner en-banner">
                <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                <a href="/tpl/ajax/online_order_rest.php" class="booking" >
                    <?else:?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking" >
                        <?endif?>
                        <div class="detail-order-banner-title">Book<br>a table</div>
                        <div class="detail-order-banner-in-place-str"><div>at the restaurants<br><?=CITY_ID=='msk'?'at the Moscow':''?><?=CITY_ID=='spb'?'in St. Petersburg':''?><?=CITY_ID=='urm'?'in Jurmala':''?><?=CITY_ID=='rga'?'in Riga':''?></div></div>
                        <div class="detail-order-banner-addition-text-str">
                            Fast
                            <br>
                            Free of charge
                            <br>
                            <span>SMS-confirmation<br>in 5 minutes</span>
                        </div>
                        <div class="detail-order-banner-above-phone-str">by the web or by phone</div>
                        <div class="detail-order-banner-phone-str"><?if(CITY_ID=='msk')echo "+7 (495) 988-26-56";elseif(CITY_ID=='spb')echo "+7 (812) 740-18-20";elseif(CITY_ID=='rga'||CITY_ID=='urm')echo "+371 661-031-06";?></div>
                        <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                            <a href="/tpl/ajax/online_order_rest.php" class="booking detail-banner-order-button" >Start booking<span></span></a>
                        <?else:?>
                            <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking detail-banner-order-button" >Start booking<span></span></a>
                        <?endif?>
                    </a>
            </div>
        <?endif?>
    <?endif?>
