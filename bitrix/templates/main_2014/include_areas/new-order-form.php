<?if(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='urm'||CITY_ID=='rga'):?>

    <div class="detail-order-banner">
        <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
        <a href="/tpl/ajax/online_order_rest.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>'>
            <?else:?>
            <a href="/tpl/ajax/online_order_rest_rgaurm.php<?= ($_REQUEST["CATALOG_ID"] == "restaurants") ? "" : "?banket=Y" ?>" class="booking" data-id='<?=$element_id?>' data-restoran='<?=rawurlencode($RESTORAN_NAME)?>'>
                <?endif?>
                <div class="detail-order-banner-title">Бронировать столик</div>
                <div class="detail-order-banner-in-place-str"><div>в ресторане<br></div></div>
                <div class="detail-order-banner-addition-text-str">
                    Быстро
                    <br>
                    Бесплатно
                    <br>
                    СМС-подтверждение<br>через 5 минут
                </div>
                <div class="detail-order-banner-above-phone-str">на сайте или по телефону</div>
                <div class="detail-order-banner-phone-str"><?if(CITY_ID=='msk')echo "+7 (495) 988-26-56";elseif(CITY_ID=='spb')echo "+7 (812) 740-18-20";elseif(CITY_ID=='rga'||CITY_ID=='urm')echo "+371 661-031-06";?></div>
                <?if(CITY_ID!='urm'&&CITY_ID!='rga'):?>
                    <a href="/tpl/ajax/online_order_rest.php" class="booking detail-banner-order-button" >Начать бронирование<span></span></a>
                <?else:?>
                    <a href="/tpl/ajax/online_order_rest_rgaurm.php" class="booking detail-banner-order-button" >Начать бронирование<span></span></a>
                <?endif?>
            </a>
    </div>

<?endif?>