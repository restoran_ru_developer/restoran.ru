<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$arResult["NAME"] = preg_replace("/&lt;br[ \/]+&gt;[0-9A-zА-ярР .]+/"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br/>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br />"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br /&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br/&gt;"," ",$arResult["NAME"]);


$APPLICATION->SetTitle('Новогодняя ночь в ресторане '.$arResult["NAME"]);
$APPLICATION->SetPageProperty("description", 'Предложения ресторана '.$arResult["NAME"].' на новогоднюю ночь');
$APPLICATION->SetPageProperty("keywords",  'новогодняя, ночь, ресторан, '.$arResult["NAME"]);

?>