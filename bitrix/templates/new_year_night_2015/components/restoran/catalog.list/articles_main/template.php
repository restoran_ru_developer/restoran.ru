<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>                  
<div id="spec_block_list">
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>

        <?
        if($arItem['CODE']=='river-palas'){
            $arItem["DETAIL_PAGE_URL"] = 'http://www.biletnariver.ru/';
        }
        ?>
    <?if($arItem['CODE']=='river-palas'):?>
        <div class="spec" onclick="window.open('<?= $arItem["DETAIL_PAGE_URL"] ?>')" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat">
    <?else:?>
        <div class="spec" onclick="location.href='<?= $arItem["DETAIL_PAGE_URL"] ?>'" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat">
    <?endif?>
                <div class="right spec_grad">
                    <a class="title" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"  <?=$arItem['CODE']=='river-palas'?'target="_blank"':''?>>
                        <?= $arItem["NAME"] ?>
                    </a>                    
                    <div class="text">                                                      
                        <?if ($arItem["adres"]):?>
                            <?=$arItem["adres"]?>
                        <?endif;?>                
                            <Br/>
                        <?if ($arItem["subway"]):?>
                            <p class="subway-icon"><?=$arItem["subway"]?></p>
                        <?endif;?>                                                
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" <?=$arItem['CODE']=='river-palas'?'target="_blank"':''?>><button class="btn-nb-empty">Подробнее</button></a>
                        <?if ($arItem["PREVIEW_TEXT"]):?>
                            <p><?=$arItem["PREVIEW_TEXT"]?></p>
                        <?endif;?>
                    </div>    
<!--                    <a href="<?//=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn-nb-empty">Подробнее</a>-->
                </div>            
                <div class="clear"></div>               
        </div>
    <? endforeach; ?>    
    <? /*if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <div class="navigation">
            <div class="pages"><?= $arResult["NAV_STRING"] ?></div>
        </div>
    <? endif; */?>
</div>