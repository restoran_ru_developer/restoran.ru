
<p class="MsoNormal"> </p>
 
<p class="MsoNormal"><span style="font-size: 9pt; line-height: 115%;">От того, какие кафе, банкетные залы или рестораны Санкт-Петербурга вы выберете для празднования новогодней ночи, зависит ваше впечатление от начавшегося года. Restoran.ru готов помочь вам встретить Новый 2017 год вкусно и красиво. В рубрике «Новогодняя ночь в ресторанах Санкт-Петербурга» собраны самые выгодные предложения от московских ресторанов, баров и кафе. Новогоднее меню, феерическая шоу-программа, запоминающиеся представления артистов и фокусников — это и многое другое вам готовы предложить рестораны Санкт-Петербурга в новогоднюю ночь 2017.</span>
  <br />
<span style="font-size: 9pt; line-height: 115%;"> Вы уже знаете, где провести Новый год? Если нет, позвоните нам </span><span style="font-size: 12px; line-height: 13.8px;">(812) 740-18-20</span><span style="font-size: 9pt; line-height: 115%;">. Служба заказа банкетов и столиков Ресторан.ру справится с любой задачей, подберет для вашего новогоднего торжества ресторан мечты!</span><span style="font-size: 12pt; line-height: 115%;"><o:p></o:p></span></p>
 
<p></p>
