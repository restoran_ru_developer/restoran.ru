<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
            <div class="ontop"> наверх</div>
            <?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
            <?$APPLICATION->IncludeFile(
                $APPLICATION->GetTemplatePath("include_areas/seotext_".CITY_ID.".php"),
                Array(),
                Array("MODE"=>"html")
            );?>                                 
            <Br /><Br />
        </div>
    </div>    
    <?
    setSeo();
    ?>
    <div class="modal fade" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content"> 
              <div class="modal-close">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&#9587; <!--&times;--></span><span class="sr-only">Close</span></button>
              </div>
              <div class="modal-body">                  
                  
              </div>           
          </div>
        </div>
    </div>
    <noindex>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33504724-1']);
  _gaq.push(['_setDomainName', 'restoran.ru']);


  _gaq.push (['_addOrganic', 'images.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'blogs.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'video.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'mail.ru', 'q']);
  _gaq.push (['_addOrganic', 'go.mail.ru', 'q']);
  _gaq.push (['_addOrganic', 'google.com.ua', 'q']);
  _gaq.push (['_addOrganic', 'images.google.ru', 'q']);
  _gaq.push (['_addOrganic', 'maps.google.ru', 'q']);
  _gaq.push (['_addOrganic', 'rambler.ru', 'words']);
  _gaq.push (['_addOrganic', 'nova.rambler.ru', 'query']);
  _gaq.push (['_addOrganic', 'nova.rambler.ru', 'words']);
  _gaq.push (['_addOrganic', 'gogo.ru', 'q']);
  _gaq.push (['_addOrganic', 'nigma.ru', 's']);
  _gaq.push (['_addOrganic', 'search.qip.ru', 'query']);
  _gaq.push (['_addOrganic', 'webalta.ru', 'q']);
  _gaq.push (['_addOrganic', 'sm.aport.ru', 'r']);
  _gaq.push (['_addOrganic', 'meta.ua', 'q']);
  _gaq.push (['_addOrganic', 'search.bigmir.net', 'z']);
  _gaq.push (['_addOrganic', 'search.i.ua', 'q']);
  _gaq.push (['_addOrganic', 'index.online.ua', 'q']);
  _gaq.push (['_addOrganic', 'web20.a.ua', 'query']);
  _gaq.push (['_addOrganic', 'search.ukr.net', 'search_query']);
  _gaq.push (['_addOrganic', 'search.com.ua', 'q']);
  _gaq.push (['_addOrganic', 'search.ua', 'q']);
  _gaq.push (['_addOrganic', 'poisk.ru', 'text']);
  _gaq.push (['_addOrganic', 'go.km.ru', 'sq']);
  _gaq.push (['_addOrganic', 'liveinternet.ru', 'ask']);
  _gaq.push (['_addOrganic', 'gde.ru', 'keywords']);
  _gaq.push (['_addOrganic', 'affiliates.quintura.com', 'request']);
  _gaq.push (['_addOrganic', 'akavita.by', 'z']);
  _gaq.push (['_addOrganic', 'search.tut.by', 'query']);
  _gaq.push (['_addOrganic', 'all.by', 'query']);


  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter17073367 = new Ya.Metrika({id:17073367,
    enableAll: true});
    } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") +
    "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: '432665', type: 'pageView', start: (new Date()).getTime()});
(function (d, w) {
   var ts = d.createElement('script'); ts.type = 'text/javascript'; ts.async = true;
   ts.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//top-fwz1.mail.ru/js/code.js';
   var f = function () {var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ts, s);};
   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window);
//]]></script>
<noscript>
<div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=432665;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
</div>
</noscript>
<!-- //Rating@Mail.ru counter -->                           	
<div id="top100counter" style="position:absolute;left:-10000px;"></div>
<div style="position:absolute;left:-10000px;">
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t41.5;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->
</div>
        <script type="text/javascript">
        var _top100q = _top100q || [];

        _top100q.push(["setAccount", "2838823"]);
        _top100q.push(["trackPageviewByLogo", document.getElementById("top100counter")]);


        (function(){
         var top100 = document.createElement("script"); top100.type = "text/javascript";

         top100.async = true;
         top100.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/pack/pack.min.js";
         var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(top100, s);
        })();
        </script>
    </noindex>
</body>
</html>