<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script src="<?=$templateFolder?>/script.js"></script>
<?
$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;

$tplFolder = str_replace("user", "editor", $templateFolder);
$APPLICATION->AddHeadScript($tplFolder.'/jq_redactor/redactor_user.js');
$APPLICATION->AddHeadScript($tplFolder.'/uploaderObject.js');
$APPLICATION->AddHeadScript($tplFolder.'/chosen.jquery.js');
$APPLICATION->AddHeadScript($tplFolder.'/jQuery.fileinput.js');
$APPLICATION->AddHeadScript($tplFolder.'/jquery.form.js');


    $APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
    $APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
    $APPLICATION->AddHeadString('<link href="/tpl/js/fu/fineuploader.css" rel="stylesheet" type="text/css"/>'); 
?>
<form id="editor_form" method="post" action="<?=$tplFolder?>/core.php" old-action="<?=$tplFolder?>/core.php" enctype="multipart/form-data">
<input type="hidden" name="act" value="edit_element" />
<input type="hidden" name="ELEMENT_ID" value="<?=$arParams["ELEMENT_ID"]?>" />
<input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
<input type="hidden" name="PARENT_SECTION" value="<?=$arParams["PARENT_SECTION"]?>" />
<input type="hidden" name="TAG_SECTION" value="<?=$arParams["TAG_SECTION"]?>" />
<?
//if($USER->IsAdmin())
?>
<div id="p_editor">	
	<div class="cntr">
	<div class="ttl"><?=($arParams["NEW_TEXT"])?$arParams["NEW_TEXT"]:"Описание"?></div>
	<?
	if($arResult["NAME"]!="") $APPLICATION->SetTitle("Редактирование: ".$arResult["NAME"]);
	else $APPLICATION->SetTitle("Новая публикация");
	?>
	<a name="ar_top"></a>
        
	<ul id="req_inp">
		<?
		//var_dump($arResult["PROPERTIES"]);
                $sele_new = 0;
		?>
		<?foreach($arParams["REQ"] as $R){?>
			<?
			if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && $R["ADMIN_ONLY"]=="Y" && $R["CODE"]=="ACTIVE")  $R["TYPE"]="hidden";
			if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin() && $R["ADMIN_ONLY"]=="Y" && $R["TYPE"]!="hidden") continue;
			?>
			<li <? if($R["TYPE"]=="hidden") echo 'style="display:none;"';?> <?if($R["TYPE"]=="select"&&$sele_new%2==0){$sele_new++; echo 'style="width:337px; display:block; float:left; margin-right:10px"';}elseif($R["TYPE"]=="select"&&$sele_new%2==1){$sele_new++; echo 'style="width:337px; display:block; float:left;"';}?>>
				<?
				
				
				if(is_array($R["VALUE_FROM"])){
					$val=array();
					foreach($R["VALUE_FROM"] as $VF){
						
						$addr = explode("__", $VF);
						if(count($addr)==1) $val[] = $arResult[$addr[0]];
						if(count($addr)==2) $val[] = $arResult[$addr[0]][$addr[1]];
						if(count($addr)==3) $val[] = $arResult[$addr[0]][$addr[1]][$addr[2]];
						if(count($addr)==4) $val[] = $arResult[$addr[0]][$addr[1]][$addr[2]][$addr[3]];
						
						//var_dump( $arResult[$addr[0]][$addr[1]]);
					}
				
				}else{
					$val = "";
					$addr = explode("__", $R["VALUE_FROM"]);
					if(count($addr)==1) $val = $arResult[$addr[0]];
					if(count($addr)==2) $val = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $val = $arResult[$addr[0]][$addr[1]][$addr[2]];
					if(count($addr)==4) $val = $arResult[$addr[0]][$addr[1]][$addr[2]][$addr[3]];
				}
				
				
				if($R["CODE"]=="ACTIVE" && $arParams["ALWAYS_ACTIVE"]) $val="Y";
				
				
				if($R["TYPE"]=="hidden"){
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/hidden.php",
           		        Array(
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
						),
						Array("MODE"=>"php")
					);
				}
				
				if($R["TYPE"]=="short_text"){
					//strip_tags(string str [, string allowable_tags])
					//var_dump($arResult["USER"]);
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/short_text_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>strip_tags($val),
                    		"LEN"=>250,
                			"SHOW_CO"=>"N",
                			"NOT_DELETE"=>"Y",
                			"REQUIRED"=>$R["REQUIRED"]
						),
						Array("MODE"=>"php")
					);
				}

				
				if($R["TYPE"]=="week"){
					if(is_array($R["VALUES_LIST"])){
						$values_list=array();
						foreach($R["VALUES_LIST"] as $VL){
						
							$addr = explode("__", $VL);
							if(count($addr)==1) $values_list[] = $arResult[$addr[0]];
							if(count($addr)==2) $values_list[] = $arResult[$addr[0]][$addr[1]];
							if(count($addr)==3) $values_list[] = $arResult[$addr[0]][$addr[1]][$addr[2]];
							if(count($addr)==4) $values_list[] = $arResult[$addr[0]][$addr[1]][$addr[2]][$addr[3]];
						}
					}else{
						$values_list = "";
						$addr = explode("__", $R["VALUES_LIST"]);
						if(count($addr)==1) $values_list = $arResult[$addr[0]];
						if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
						if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
					}
					
					
					//var_dump($values_list);
					
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/week.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}


				
				if($R["TYPE"]=="stars"){
					//strip_tags(string str [, string allowable_tags])
					//var_dump($val);
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/stars.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>strip_tags($val),
                    		"LEN"=>250,
                			"SHOW_CO"=>"N",
                			"NOT_DELETE"=>"Y",
                			"REQUIRED"=>$R["REQUIRED"]
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="vis_red2"){
					//var_dump($val);
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/vis_red2.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"LEN"=>250,
                			"SHOW_CO"=>"Y",
                			"NOT_DELETE"=>"Y",
                			"REQUIRED"=>$R["REQUIRED"]
						),
						Array("MODE"=>"php")
					);
				}
				
				
				
				
				
				if($R["TYPE"]=="short_text2"){
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/short_text_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                			"SHOW_CO"=>"N",
                			"NOT_DELETE"=>"Y",
                			"REQUIRED"=>$R["REQUIRED"]
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="multi_select"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==1) $values_list = $arResult[$addr[0]];
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/multi_select_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y",
                			"REQUIRED"=>$R["REQUIRED"]
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="select"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==1) $values_list = $arResult[$addr[0]];
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
					
					if($R["CODE"]=="ACTIVE"){
						//if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
							$APPLICATION->IncludeFile(
							$templateFolder."/blocks/select_block.php",
    	       		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
							),
							Array("MODE"=>"php")
							);
						//}else{
						/*
							$APPLICATION->IncludeFile(
							$tplFolder."/blocks/hidden.php",
    	       		        Array(
        	           		 	"BLOCK_NAME"=>$R["CODE"],
            	        		"BLOCK_VALUE" =>$val,
							),
							Array("MODE"=>"php")
							);
							*/
						//}
					}else{
					
						//var_dump($values_list);
						$APPLICATION->IncludeFile(
						$templateFolder."/blocks/select_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y",
                			"REQUIRED"=>$R["REQUIRED"]
						),
						Array("MODE"=>"php")
						);
					}
				}
				
				
				if($R["TYPE"]=="photo"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==1) $values_list = $arResult[$addr[0]];
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					
					
				//	var_dump($val);
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/photo.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				
				if($R["TYPE"]=="photos"){
				
					//var_dump($val);
					foreach($val as $v){
						if($v>0) $ARR[]=array($v, "");
					}
					
					
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/photos_new.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>array($ARR),
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="video"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==1) $values_list = $arResult[$addr[0]];
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
					
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/video_block.php",
						Array(
							"BLOCK_TITTLE"=>$R["NAME"],
        				    "BLOCK_VALUE" =>array("300", "400", $val),
			        	    "BLOCK_NAME"=>$R["CODE"]
						),
						Array("MODE"=>"php")
        			);

				}
				
				
				if($R["TYPE"]=="video_code"){
					$APPLICATION->IncludeFile(
						$tplFolder."/blocks/video_code_2.php",
						Array(
							"BLOCK_TITTLE"=>$R["NAME"],
			        	    "BLOCK_VALUE" =>$val,
			        	    "BLOCK_NAME"=>$R["CODE"]
						),
						Array("MODE"=>"php")
        			);
				}
				
				?>
			</li>
                        <?if($sele_new%2==0):?>
                            <li class="clear" style="background:none; padding: 0px; border:0px; margin:0px;"></li>
                        <?endif;?>
		<?}?>
	</ul>
	
	<ul id="block_list">
	<?
	$STEP=1;
	$k=0;
	//echo $_SERVER["DOCUMENT_ROOT"].$tplFolder.'/phpQuery-onefile.php';
	require($_SERVER["DOCUMENT_ROOT"].$tplFolder.'/phpQuery-onefile.php');
	
	$document = phpQuery::newDocument($arResult["DETAIL_TEXT"]);
	$blocks = $document->find('.bl');
	$VIDEO_BLOCK =0;
        $igr_block = 0;
	foreach($blocks as $el){
		$BL = pq($el);
		
		
		echo '<li>';
		//ПРЯМАЯ РЕЧЬ
		if(substr_count($BL->attr("class"), "direct_speech")>0){
			
			//var_dump($BL->html());
			
			$txt1 = $BL->find(".txt1")->text();
			$txt2 = $BL->find(".txt2")->text();
			$txt3 = $BL->find(".txt3")->text();
			$txt3 = str_replace("«",'',$txt3);
			$txt3 = str_replace("»",'',$txt3);
			
			
			$img = $BL->find(".pic")->attr("src");
			$img = str_replace("#FID_PR",'',$img);
			$img = str_replace("#",'',$img);
			
			//var_dump();
			
			$APPLICATION->IncludeFile(
				$tplFolder."/blocks/pr_rech.php",
				Array(
					"BLOCK_TITTLE"=>"Прямая речь",
					"BLOCK_VALUE" =>array($txt1,$txt2,$txt3,$img),
					"LEN"=>500,
					"BLOCK_NAME"=>"bl".time().$k
				),
				Array("MODE"=>"php")
       		);
		}
		
		
		
		//ИНГРЕДИЕНТЫ
		if(substr_count($BL->attr("class"), "ingr")>0){
			
			/*$txt1 = $BL->find("h2")->text();
			
			$INs=array();
			foreach($BL->find("li") as $L){
				$Li = pq($L);
				$INs[]=array("NAME"=>$Li->find(".name")->text(),"CO"=>$Li->find(".amount")->text());
			}
			
			$txt1 =str_replace("Ингредиенты —", "", $txt1);
			$txt1 =str_replace("Ингредиенты:", "", $txt1);*/
                        $txt1 = $BL->find("h2")->text();
			$txt2 = $BL->find(".txt")->html();
			
			$APPLICATION->IncludeFile(
				$tplFolder."/blocks/vis_red.php",
				Array(
					"BLOCK_TITTLE"=>"Ингредиенты",
					"BLOCK_VALUE" =>array($txt1,$txt2),
                                    
					"LEN"=>500,
					"BLOCK_NAME"=>"bl".time().$k
				),
				Array("MODE"=>"php")
                        );
                        $igr_block++;
		}
		
		
		
		//ШАГИ
		if(substr_count($BL->attr("class"), "shag")>0){
                        if (!$igr_block)
                        {                            
                                $APPLICATION->IncludeFile(
                                            $tplFolder."/blocks/vis_red.php",
                                            Array(
                                                    "BLOCK_TITTLE"=>"Ингредиенты",
                                                    "BLOCK_VALUE" =>array("",""),
                                                    "BLOCK_NAME"=>"bl".time(),
                                            ),
                                            Array("MODE"=>"php")
                                );
                            echo '</li><li>';
                            $igr_block++;
                        }
			$txt = $BL->find(".txt")->text();
			
			
			$img = $BL->find(".pic")->attr("src");
			$img = str_replace("#FID_",'',$img);
			$img = str_replace("#",'',$img);
			
			
			
			$APPLICATION->IncludeFile(
				$tplFolder."/blocks/shag_block.php",
				Array(
					"BLOCK_TITTLE"=>"Шаг",
					"STEP"=>$STEP,
        	   	 	"BLOCK_VALUE" =>array($txt,$img),
        	    	"BLOCK_NAME"=>"bl".time().$k
				),
				Array("MODE"=>"php")
        	);

			
			$STEP++;
			
		}
		
		
		
		//ВСТАВКА ТЕКСТА
		if(substr_count($BL->attr("class"), "vis_red")>0){
			
			$txt1 = $BL->find("h2")->text();
			$txt2 = $BL->find(".txt")->html();
			
			//var_dump($txt2);
			$APPLICATION->IncludeFile(
				$tplFolder."/blocks/vis_red.php",
				Array(
					"BLOCK_TITTLE"=>"Вставка текста",
        			"BLOCK_VALUE" =>array($txt1,$txt2),
        			"BLOCK_NAME"=>"bl".time().$k,
				),
				Array("MODE"=>"php")
        	);
                        $igr_block++;
		}
		
		
		
		//ПРИВЯЗКА К РЕСТОРАНУ
		if(substr_count($BL->attr("class"), "restoran_text")>0){
			
			$txt1 = $BL->find(".article_rest")->text();
			$txt2 = $BL->find(".article_rest_text")->html();
			
			$tar = explode(" ", $txt1);
			$txt1=array();
			foreach($tar as $t){
				$t = str_replace("#REST_",'',trim($t));
				$t = str_replace("#",'',trim($t));
				$txt1[]=$t;
			}
			//var_dump($arResult["PROPS"]["RESTORAN"]);
			$APPLICATION->IncludeFile(
				$templateFolder."/blocks/restoran.php",
				Array(
					"BLOCK_TITTLE"=>"Упоминание о ресторане",
					"BLOCK_VALUE" =>array($txt1, $txt2),
					"VALUES_LIST"=>$arResult["PROPS"]["RESTORAN"]["LIST"],
					"SELECT_TITLE"=>"Упомянуть ресторан",
					"BLOCK_NAME"=>"bl".time().$k
				),
				Array("MODE"=>"php")
        	);
		}
		
		
		
		//ФОТОГРАФИИ
		if(substr_count($BL->attr("class"), "articles_photo")>0){
			
			$txt1 = $BL->find(".first_pic")->attr("src");
			
			$ARR=array();
			
			$img = $BL->find(".first_pic")->attr("src");
			$img = str_replace("#FID_",'',$img);
			$img = str_replace("#",'',$img);
			
			$first_id = $img;
			$descr =$BL->find(".first_pic")->parent("div")->next()->find("i")->text();
			
			if($img>0)$ARR[]=array($img, $descr);
			
			
			foreach($BL->find(".pic") as $P){
				$Pic = pq($P);
				$img = $Pic->attr("src");
				$img = str_replace("#FID_",'',$img);
				$img = str_replace("#",'',$img);
				$descr = $Pic->attr("alt");
				if($img!=$first_id){
					$ARR[]=array($img, $descr);
				}
			} 
			
			//var_dump($ARR);
	
			
			$APPLICATION->IncludeFile(
			$templateFolder."/blocks/photos_new.php",
			Array(
				"BLOCK_TITTLE"=>"Фотогалерея",
        	    "BLOCK_VALUE" =>array($ARR),
                            "NOT_DELETE" => "Y",
        	    "BLOCK_NAME"=>"bl".time().$k
			),
			Array("MODE"=>"php")
        );
		}
		
		
		
		
		if(substr_count($BL->attr("class"), "video")>0 && substr_count($BL->attr("class"), "video_code")==0 ){
			
			$fid = $BL->find(".myPlayer")->attr("href");
			$fid = str_replace("#FID_",'',$fid);
			$fid = str_replace("#",'',$fid);
	                        
			
			$APPLICATION->IncludeFile(
			$tplFolder."/blocks/video_block.php",
			Array(
				"BLOCK_TITTLE"=>"Видео",
        	    "BLOCK_VALUE" =>array("300", "400", $fid),
        	    "BLOCK_NAME"=>"bl".time().$k
			),
			Array("MODE"=>"php")
        	);
        	$VIDEO_BLOCK++;
		}
		
		
		if(substr_count($BL->attr("class"), "video_code")>0){
			
			$txt = $BL->html();
			
			
			$APPLICATION->IncludeFile(
			$tplFolder."/blocks/video_code.php",
			Array(
				"BLOCK_TITTLE"=>"Видео",
        	    "BLOCK_VALUE" =>$txt,
        	    "BLOCK_NAME"=>"bl".time().$k
			),
			Array("MODE"=>"php")
        	);
        	$VIDEO_BLOCK++;
		}
		
		
		
		$k++;
		echo '</li>';
	}
	
	if($VIDEO_BLOCK==0 && $arParams["SHOW_VBLOCK"]=="Y" && $arResult["DETAIL_TEXT"]=="" && count($arParams["CAN_ADD"])>0){
		echo '<li>';
			$APPLICATION->IncludeFile(
			$tplFolder."/blocks/video_block.php",
			Array(
				"BLOCK_TITTLE"=>"Видео",
        	    "BLOCK_VALUE" =>"",
        	    "BLOCK_NAME"=>"bl".time().$k
			),
			Array("MODE"=>"php")
        	);	
        echo '</li>';	
	}
	
	//var_dump($arResult["PROPS"]);
	
	if(($k==0 && $arResult["DETAIL_TEXT"]!="" && count($arParams["CAN_ADD"])>0) || ($arResult["DETAIL_TEXT"]=="" && $k==0  && count($arParams["CAN_ADD"])>0))
        {
		if($arParams["HIDE_RESTB"]!="Y")
                { 
                    echo '<li>';
                    $APPLICATION->IncludeFile(
			$templateFolder."/blocks/restoran.php",
			Array(
				"BLOCK_TITTLE"=>"Упоминание о ресторане",
				"BLOCK_VALUE" =>array($txt1, $txt2),
				"VALUES_LIST"=>$arResult["PROPS"]["RESTORAN"]["LIST"],
				"SELECT_TITLE"=>"Упомянуть ресторан",
				"BLOCK_NAME"=>"bl".time().$k
			),
			Array("MODE"=>"php")
                    );
                    $k++;
                    echo '</li>';
		}
                echo '<li>';
                $APPLICATION->IncludeFile(
			$templateFolder."/blocks/photos_new.php",
			Array(
				"BLOCK_TITTLE"=>"Фотогалерея",
                                "BLOCK_VALUE" =>array(),
                                "NOT_DELETE" => "Y",
                                "BLOCK_NAME"=>"bl".time().$k
			),
			Array("MODE"=>"php")
                );
                echo '</li>';
	}
        if ($k==0 && $arResult["DETAIL_TEXT"]=="")
        {            
            if ($arParams["IGR_SHOW"]=="Y")
            {
                echo '<li>';
                    $APPLICATION->IncludeFile(
				$tplFolder."/blocks/vis_red.php",
				Array(
                                        "BLOCK_TITTLE"=>"Ингредиенты",
                                        "BLOCK_VALUE" =>array("Ингредиенты",""),
                                        "BLOCK_NAME"=>"bl".time().$k,
				),
				Array("MODE"=>"php")
                    );
		echo '</li>';
                $k++;
            }
            if ($arParams["SHAG_SHOW"]=="Y")
            {
                echo '<li>';
                    $APPLICATION->IncludeFile(
				$templateFolder."/blocks/shag_block.php",
				Array(
                                        "BLOCK_TITTLE"=>"Шаг",
                                        "STEP"=>$_REQUEST["steps"]+1,
                                        "BLOCK_VALUE" =>array("",""),
                                        "BLOCK_NAME"=>"bl".time().$k
				),
				Array("MODE"=>"php")
                    );
		echo '</li>';                
                $k++;
            }
        }
        if ($arParams["SHAG_SHOW"]=="Y")
            echo '<li style="background:none;padding-left:0px; padding-top:0px;"><a href="javascript:void(0)" class="add_step" id="add_new_step">добавить шаг</a><br /></li>';
	?>
	</ul>
	<input type="hidden" name="ACTIVE" val="N" />
	
	
	
	
	<div class="buts">
		<div class="lb">
		
		<?/*	<a href="#" target="_blank" id="primenit">Применить</a>*/?>
			<a href="<?=$arParams["BACK_LINK"]?>"  id="chernovik" title="Сохранить, но не показывать на сайте">Черновик</a>
			<?if($arResult["ID"]>0){?>
				<a href="/content/view.php?ELEMENT_ID=<?=$arResult["ID"]?>&IBLOCK_TYPE=<?=$arResult["IBLOCK_TYPE"]?>&IBLOCK_ID=<?=$arResult["IBLOCK_ID"]?>" id="predprosmotr" target="_blank">Предпросмотр</a>
			<?}else{?>
				<a href="/content/view.php?IBLOCK_TYPE=<?=$arResult["IBLOCK_TYPE"]?>&IBLOCK_ID=<?=$_REQUEST["IB"]?>&PARENT_SECTION=<?=$arParams["PARENT_SECTION"]?>" id="predprosmotr" target="_blank">Предпросмотр</a>
			<?}?>
		</div>
		<div class="rb">
			<a href="<?=$arParams["BACK_LINK"]?>" id="public">Опубликовать</a>
		</div>
		
	</div>
	
	<div class="item-row error_li" id="error_li"></div>
	
	<div class="comments">
	Опубликованную запись вы сможете найти и редактировать<br/>
в разделе «<a href="#">Блог</a>» личного кабинета
	</div>
	
	</div>
	<div class="right">
		<?
		if($arParams["HIDE_TAGS"]!="Y"){
		//var_dump($arResult["TAGS"]);
		$EL_TAGS = explode(",", $arResult["TAGS"]);
		foreach($EL_TAGS as &$Tag){
			$Tag=trim($Tag);
			$ELEMENT_TAGS.='<a href="#">'.$Tag.'</a>';	
		}
		//var_dump($EL_TAGS);
		
		?>
		<div id="choose_tags">
			<div class="ttl">Теги</div>
			<div class="ln">
				<?
				global $USER;
				$arGroups = $USER->GetUserGroupArray();
				foreach($arGroups as $v) $GRs[]=$v;
				?>
				Поиск тега: &nbsp;&nbsp;&nbsp;<input name="new_tag" class="pole" id="new_tag"/> <?if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){?><a href="#" class="add_new_tag"></a><?}?>
				<div class="all_tags_lis"></div>
			</div>
			
			<div class="tag_list"><?=$ELEMENT_TAGS?></div>
			
			<input type="hidden" name="TAGS" value="<?=$arResult["TAGS"]?>" />
			
			<div class="sub_t">Теги раздела:</div>
			<div class="pop">
				<?
				//in_array()
				?>
				<?foreach($arResult["ALL_TAGS"] as $T){?>
					<?if(!in_array(trim($T), $EL_TAGS)){?> 
						<a href="#"><?=trim($T)?></a>
					<?}?>
				<?}?>
			</div>
			
			<?
			//<div class="all_tags"><a href="#">все теги</a></div>
			?>
		</div>
		<?}?>
	</div>
	<div class="clear"></div>
</div>
</form>


<?
//v_dump($arParams);
?>