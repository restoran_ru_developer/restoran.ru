<div class="content_block ph_gals">	
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][type]" value="add_photos"/>
	<div class="name"><?=$BLOCK_TITTLE?>  <?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
	<div id="imu_content_<?=$BLOCK_NAME?>" class="imu_content">
		<div class="upl-description">
                    <!--Перетащите фотографии в это поле <br />или<br />-->
			<p align="center">
				<!--<input type="file" name="block[<?=$BLOCK_NAME?>][file][]" value="" id="file-field_<?=$BLOCK_NAME?>" multiple="true" class="file-field" /><br/>
				<em>Выберите файлы</em>-->
                                <ul id="file_field_<?=$BLOCK_NAME?>" class="unstyled" style="width:120px; margin:0 auto;"></ul>
			</p>
		</div>
	</div>
						
	<div class="upl-imgs">
		<ul id="img-list_<?=$BLOCK_NAME?>" class="img-list">
			<? $i=0;?>
			<? foreach($BLOCK_VALUE[0] as $VALUE){?>
				<? $file = CFile::ResizeImageGet($VALUE[0], array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_EXACT, true);  ?>
				<li>
					<em><img src="<?=$file["src"]?>" width="100"></em>
					<textarea name="block[<?=$BLOCK_NAME?>][descr_new][]" <? if($VALUE[1]=="" || $VALUE[1]=="Добавьте описание"){ $VALUE[1]="";?> <?}?> placeholder="Добавьте описание" ><?=$VALUE[1]?></textarea>
					<p class="up"><a href="#">Поднять</a></p>
					<p class="dela"><a href="#">Удалить фото</a></p><div class="progress" rel="0"></div>
					<input type="hidden" name="block[<?=$BLOCK_NAME?>][fid_new][]" value="<?=$VALUE[0]?>" />
				</li>
				<? $i++;?>
			<?}?>
		</ul>
	</div>					
</div>

<!--<div class="content_block">	
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][type]" value="add_step"/>
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][fid]" value="<?=$BLOCK_VALUE[1]?>"/>
	<div class="name <?if($STEP>0) echo "step"; ?>"><?=$BLOCK_TITTLE?> <?=$STEP?> <?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
				
	<div class="short">
		<textarea name="block[<?=$BLOCK_NAME?>][txt]" class="pole" maxlength="<?=$LEN?>"><?=$BLOCK_VALUE[0]?></textarea>
	</div>
		
	<?if($SHOW_CO=="Y"){?><div class="simbol_co short" id="<?=$BLOCK_NAME?>_co">Осталось <span><?=$OST?></span> символа</div><?}?>
				
	<div class="autor_photo" style="top:33px;">
		<div class="image"><img src="<?=$file["src"]?>" alt=""/></div>
                <ul id="file_field_<?=$BLOCK_NAME?>" class="unstyled"></ul>		
	</div>		
</div>-->
<script>
    $(document).ready(function(){
        var errorHandler = function(event, id, fileName, reason) {
          qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
        $('#file_field_<?=$BLOCK_NAME?>').fineUploader({
            text: {
                uploadButton: "Обзор",
                cancelButton: "Отмена",
                waitingForResponse: ""
            },
            multiple: true,
            request: {
                endpoint: "/tpl/ajax/upload.php",
                params: {"generateError": true}
            },
            /*failedUploadTextDisplay: {
                mode: 'custom',
                maxChars: 5
            }*/
        })
        .on('error', errorHandler)
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('complete', function(event, id, fileName, response) {
            $('<li><em><img src="'+response.resized+'" height="111" /></em><textarea name="block[<?=$BLOCK_NAME?>][descr_new][]" placeholder="Добавьте описание"></textarea><p class="up"><a href="#">Поднять</a></p><p class="dela"><a href="#">Удалить фото</a></p><div class="progress" rel="0"></div><input value="'+response.id+'" type="hidden" name="block[<?=$BLOCK_NAME?>][fid_new][]" /></li>').appendTo($("#img-list_<?=$BLOCK_NAME?>"));
            $(".qq-upload-list").hide();
        }); 
    });
    
</script>			
<script type="text/javascript">
$('p.dela a').live("click", function() {
	$(this).parent("p").parent("li").hide("fast", function(){
		$(this).remove();
	});
	return false;
});
			
</script>			