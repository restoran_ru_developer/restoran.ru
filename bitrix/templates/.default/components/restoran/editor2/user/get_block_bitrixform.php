<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/
//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die();
CModule::IncludeModule("iblock");
$el = new CIBlockElement;

$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["SECTION_ID"];
$arLoadProductArray["IBLOCK_ID"]=$_REQUEST["IBLOCK_ID"];
$arLoadProductArray["ACTIVE"]="Y";


//var_dump($APPLICATION->GetCurDir());
switch ($_REQUEST["block_id"]) {
	case "add_video":
    	$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/video_block.php",
			Array(
				"BLOCK_TITTLE"=>"Видео",
        	    "BLOCK_VALUE" =>"",
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break; 
    
    
    case "add_video_code":
    	$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/video_code.php",
			Array(
				"BLOCK_TITTLE"=>"Код видео",
        	    "BLOCK_VALUE" =>"",
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break; 
    
    
    case "add_short_text":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/short_text_block.php",
			Array(
				"BLOCK_TITTLE"=>"Короткий текст",
                "BLOCK_VALUE" =>"",
                "LEN"=>250,
                "SHOW_CO"=>"Y",
                "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break;
    
    
    case "add_rest":
        //Нужно получить список всех ресторанов
		//Потом включить кэширование
                if (!$_REQUEST["city"])
                    $arRestIB = getArIblock("catalog", CITY_ID);
                else
                    $arRestIB = getArIblock("catalog", $_REQUEST["city"]);
		$arFilter = Array('IBLOCK_TYPE'=>"catalog","IBLOCK_ID"=>$arRestIB["ID"], 'ACTIVE'=>'Y',"ACTIVE_DATE"=>'Y');
  		$db_list = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false);
  		$VALUES =array(); 
  		while($ar_result = $db_list->GetNext()){
                    $ar_result["NAME"] = str_replace("`","",$ar_result["NAME"]);
                    $ar_result["NAME"] = str_replace("'","",$ar_result["NAME"]);
                    $ar_result["NAME"] = str_replace('"',"",$ar_result["NAME"]);
                    $sec = "";
                    $res = CIBlockSection::GetByID($ar_result["IBLOCK_SECTION_ID"]);
                    if($ar_res = $res->GetNext())
                        $sec = " [".$ar_res['NAME']."]";
                    $VALUES[]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"].$sec);
		}
		
		//если привязка к ресторану, то нужно запросить еще и фирмы
		//$arIIIB = getArIblock("firms", CITY_ID);
			
		$arFilter_prl = Array("IBLOCK_TYPE"=>"firms","ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res_prl = CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $arFilter_prl, false);
		while($ob_prl = $res_prl->GetNext()){
			$VALUES[]=array("ID"=>$ob_prl["ID"], "NAME"=>$ob_prl["NAME"]." [Фирмы]");
		}
					
				
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/restoran.php",
			Array(
				"BLOCK_TITTLE"=>"Упомянание о ресторане",
				"BLOCK_VALUE" =>array(""),
				"VALUES_LIST"=>$VALUES,
				"SELECT_TITLE"=>"Выберите ресторан",
				"BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break;
    
    
    case "add_step":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/shag_block.php",
			Array(
				"BLOCK_TITTLE"=>"Шаг",
				"STEP"=>$_REQUEST["steps"]+1,
        	    "BLOCK_VALUE" =>array("",""),
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break;
    
        
    case "add_text":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/vis_red.php",
			Array(
				"BLOCK_TITTLE"=>"Вставка текста",
        		"BLOCK_VALUE" =>array("","Текст"),
        		"BLOCK_NAME"=>"bl".time(),
			),
			Array("MODE"=>"php")
        );
    break;
    
    
    case "add_pr":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/pr_rech.php",
			Array(
				"BLOCK_TITTLE"=>"Прямая речь",
				"BLOCK_VALUE" =>array("","",""),
				"LEN"=>500,
				"BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
       	);
    break;
    
    
    case "add_photos":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/photos.php",
			Array(
				"BLOCK_TITTLE"=>"Фотогалерея",
        	    "BLOCK_VALUE" =>array(),
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
   	break;
   		
}


?>