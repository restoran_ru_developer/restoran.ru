var sgo = {};
$(document).ready(function(){

$("#req_inp input:first").focus();

$.tools.dateinput.localize("ru",  { months: 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря', 
                            shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
                            days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
                            shortDays:     'вс,пн,вт,ср,чт,пт,сб'});
$("input[name=ACTIVE_FROM]").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});    
$("input[name=ACTIVE_TO]").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});  
$("input[name=PROPERTY_EVENT_DATE]").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"}); 

 


	//ПОднимаем фотки
	$("#block_list .up a").live("click", function(){
		var bl = $(this).parent("p").parent("li");
		bl.after(bl.prev());
		return false;
	});

	/********ПРОКРУТКА ОКНА*************/
	function getBodyScrollTop(){
		return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
	}

	$(window).scroll(function(){
		var glob_top = $('#editor_form').offset().top;
		var gto = getBodyScrollTop();
		
		if(gto>glob_top) {
			gto = gto - glob_top+10;			
		}else{
			gto =0;
		}

		var pedit = $("#p_editor").height()+$("#p_editor").offset().top-getBodyScrollTop();
	
		if(pedit>$('#add_modules').height()){
			$('#add_modules').stop().animate({'top':gto},500);
			$('#choose_tags').stop().animate({'top':gto},500);
		}
	
	});
	
	
$("#add_modules ul li a").click(function(){
	return false;
});

$("#add_modules .block" ).draggable({
	connectToSortable: "#block_list",
	helper: "clone",
	revert: "invalid",
	cursorAt: { cursor: "move"},
	cancel: ".video_ln, input.pole",
	stop: function(event, ui) {
		var id = $(this).find("a").attr("id");

	}, 
	start: function(event, ui){
		
	}
});




$("#block_list").sortable({
	placeholder: "ui-state-highlight",
	cancel: ".video_ln, textarea, input, .redactor_box",
	update: function(event, ui) {
		get_editor_block(ui.item);

		//если в блоке есть редактор, то разрушаем его и создаем новый
		if(ui.item.find(".vredactor").length>0){
		
			var id = ui.item.find(".vredactor").attr("id");
			var code = redactors[id].getCode();
			
			//alert(id);
			
			redactors[id].destroyEditor();		
			redactors[id] = $('#'+id).redactor({ 
			path: '/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor', 
			buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|', 'link'], 
			css: 'redactor.css', 
			lang: 'ru',
			focus: false,
			fileUpload: '/bitrix/templates/.default/components/restoran/editor2/editor/file_upload.php',
			autoresize: false
		});
			//redactors[id].insertHtml(code);
		}
	},
	stop: function(event, ui) {
		
		//если в блоке есть редактор, то разрушаем его и создаем новый
		if(ui.item.find(".vredactor").length>0){
			console.log(redactors);
			var id = ui.item.find(".vredactor").attr("id");
			var code = redactors[id].getCode();

			
			redactors[id].destroyEditor();	
			redactors[id] = $('#'+id).redactor({ 
			path: '/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor', 
			buttons: ['bold', 'italic','underline', '|','unorderedlist', 'orderedlist', 'outdent', 'indent','|', 'link'], 
			css: 'redactor.css', 
			lang: 'ru',
			focus: false,
			fileUpload: '/bitrix/templates/.default/components/restoran/editor2/editor/file_upload.php',
			autoresize: false
		});
			//redactors[id].insertHtml(code);

			console.log(redactors);
		}
	},
	start: function(event, ui) {
		//делаем прозрачный блок высотой с перетаскиваемый
		$("#block_list .ui-state-highlight").height(ui.item.height());
		
	}
});

$(".video_ln").disableSelection();
$("#block_list li").addClass("loaded");


function get_editor_block(obj){
	if(!obj.hasClass("loaded") && !obj.hasClass("ui-state-default")){
		var id = obj.find("a").attr("id");
		var link = obj.find("a").attr("href");
		obj.find("a").remove();
		var steps = $(".step").length;
		
		var section_id = $("input[name=SECTION_ID]").val();
		var iblock_id = $("input[name=IBLOCK_ID]").val();
	
		
		$.post(link,{block_id: id, steps: steps, SECTION_ID: section_id, IBLOCK_ID: iblock_id},function(otvet){
			obj.html(otvet);
			obj.addClass("loaded");
		});
	}
}

/*********************УДАЛЯЕМ БЛОК **********************/
$("#block_list").on("click",".remove_block", function(){
	$(this).parent("div").parent("div").parent("li").hide("normal", function(){
		$(this).remove();	
	});
	return false;
});       
/********************************************************/

/***********************ТЭГИ******************************/
/********************************************************/
$("#choose_tags .tag_list").on("click", "a", function(event){
        
	$("#choose_tags .pop").append($(this));
	update_element_tags();
	return false
});

$("#choose_tags .pop").on("click", "a", function(event){
    if ($(".tag_list a").length>5)
    {
        if (!only5())
            return false;
    }
	$("#choose_tags .tag_list").append($(this));
	
	update_element_tags();
		
	return false;
});



function update_element_tags(){
	var tags = "";

	$("#choose_tags .tag_list a").each(function(){
		tags+=$(this).html()+",";
	});
	
	$("input[name=TAGS]").val(tags);	
}


$("#new_tag").keyup(function(){
	if($(this).val().length>2){
		$("#choose_tags .add_new_tag").show();
		var link = $("#editor_form").attr("action");
		var tag_section = $("input[name=TAG_SECTION]").val();
		var iblock_id = $("input[name=IBLOCK_ID]").val();
		var tags = "";
		
		$("#choose_tags .tag_list a").each(function(){
			tags+=$(this).html()+",";
		});
	
		$.post(link,{TAG_SECTION: tag_section, tag: $(this).val(), act:"get_tag_list", IBLOCK_ID:iblock_id, tags: tags},function(otvet){
			$("#choose_tags .all_tags_lis").html(otvet);
			$("#choose_tags .all_tags_lis").slideDown("fast");
			
			
		});
	}else{
		$("#choose_tags .add_new_tag").hide();
	}
});

$("body").click(function(){
	$("#choose_tags .all_tags_lis").slideUp("fast");
});



$("#choose_tags .all_tags_lis a").live("click", function(){
	$("#choose_tags .tag_list").append($(this));
	
	update_element_tags();

	$("#choose_tags .all_tags_lis").slideUp("fast", function(){
		$("#choose_tags .all_tags_lis").html();
	});
	return false;
});

$("#choose_tags .add_new_tag").click(function(){
	
	var link = $("#editor_form").attr("action");
	var tag_section = $("input[name=TAG_SECTION]").val();
	var iblock_id = $("input[name=IBLOCK_ID]").val();
	var new_tag = $("#new_tag").val();
	var tags = "";
	$("#choose_tags .tag_list a").each(function(){
		tags+=$(this).html()+",";
	});
		
	$.post(link, {TAG_SECTION: tag_section, new_tag: new_tag, act: "new_tag", IBLOCK_ID:iblock_id, tags: tags},function(otvet){
		$("#choose_tags .tag_list").append('<a href="#">'+new_tag+'</a>');
		$("#new_tag").val("");
		$("#choose_tags .all_tags_lis").slideUp("fast");
		
		update_element_tags();	
	});
	
	return false;
});

/********************************************************/

/*****Ингредиенты*****/
$("a.add_ingr").live("click", function(){
	var nl = $(this).parent("div").prev().clone();
	nl.find(".p1").val("");
	nl.find(".p2").val("");
	
	$(this).parent("div").before(nl);
	return false;
});


/******************ОТПРАВКА ФОРМЫ************************/
/********************************************************/
var act = "";
var go_to ="";

$("#public").click(function(){
	$('#editor_form').attr("action", $('#editor_form').attr("old-action"));
	
	act = "save_and_view";
	go_to = $(this).attr("href");
	
	$("input[name=ACTIVE]").val("Y");
	
	refresh_form();
	$('#editor_form').submit();
	return false;
});


$("#primenit").click(function(){
	$('#editor_form').attr("action", $('#editor_form').attr("old-action"));
	act = "save";
	
	$("input[name=ACTIVE]").val("Y");
	
	refresh_form();
	$('#editor_form').submit();
	return false;
});



$("#predprosmotr").click(function(){
	var lnk = $(this).attr("href");
	
	
	$('#editor_form').attr("old-action", $('#editor_form').attr("action"));
	$('#editor_form').attr("action", lnk);
	
	//alert(lnk);
	$('#editor_form').submit();
	
	return false;
});



$("#chernovik").click(function(){
	$('#editor_form').attr("action", $('#editor_form').attr("old-action"));
	
	act = "save";
	go_to = $(this).attr("href");
	
	
	$("select[name=ACTIVE]").val("N");
	$("input[name=ACTIVE]").val("N");
	
	refresh_form();
	$('#editor_form').submit();
	return false;
});





function refresh_form(){
	//проверка формы
	function check_editor_form(a,f,o){
		var ret=true;
		o.dataType = "html";
		
		$("#error_li").html();
		
		
		var error_string="Вы не заполнили следующие поля: ";
		var z =0;
		var co_errors=0;
		f.find("input[type=text]").each(function(){
			if($(this).hasClass("ob") && $(this).val()==""){
				ret = false;
				var name = $(this).parent("div").parent("div").find(".name").html();	
				name = name.replace('<i class="star">*</i>','');		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		f.find("textarea").each(function(){
			if($(this).hasClass("ob") && $(this).val()==""){
				ret = false;
				var name = $(this).parent("div").parent("div").find(".name").html();	
				name = name.replace('<i class="star">*</i>','');		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		f.find("select").each(function(){
			//alert($(this).val());
			if($(this).hasClass("ob") && ($(this).val()=="" || $(this).val()==null)){
				ret = false;
				var name = $(this).parent("div").parent("div").find(".name").html();	
				name = name.replace('<i class="star">*</i>','');	
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		
		error_string+=".";
		
		if(co_errors>0){
			$("#error_li").html(error_string);
		}
		
		if(ajax_load) ret = false;
		
		return ret;
	}

	//Отправка формы
	$('#editor_form').ajaxForm({
		beforeSubmit: check_editor_form,
		success: function(data) {
			console.log(data);
			var lnk = self.location.protocol+'//'+self.location.hostname+''+go_to;
			
			if(parseInt(data)>0) $("#editor_form input[name=ELEMENT_ID]").val(parseInt(data));
			
			
			if(act=="save_and_view" && go_to!="") {
				window.location.replace(go_to);
			}else{
				if(act=="predprosmotr"){
					var newWindow = window.open(go_to, '_blank');
					newWindow.focus();
				}else{
					if(parseInt(data)>0) window.location.replace(window.location.href+"&ID="+parseInt(data));
					//else window.location.reload();
					
					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
	       		   	$('#promo_modal').html("<div align=\"right\"><a class=\"modal_close uppercase white\" href=\"javascript:void(0)\"></a></div>Изменения сохранены");
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
				}
			}
			
		}
	});
}
/********************************************************/

$(".del_pic a").click(function(){
	$(this).hide();
	var pic = $(this).parent("div").prev().find(".pic");
	pic.find("img").hide();
	pic.append('<input type="hidden" name="del_prev_photo" value="Y" />');
	pic.hide();
	
	act = "save";
	go_to = $(this).attr("href");
	
	
	refresh_form();
	$('#editor_form').submit();
	
	
	return false;
});
});