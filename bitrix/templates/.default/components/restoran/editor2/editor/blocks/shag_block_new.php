<?
$OST = $LEN - trim(strlen($BLOCK_VALUE[0]));

$file = CFile::ResizeImageGet($BLOCK_VALUE[1], array('width'=>111, 'height'=>111), BX_RESIZE_IMAGE_EXACT, true); 
?>


<div class="content_block">	
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][type]" value="add_step"/>
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][fid]" value="<?=$BLOCK_VALUE[1]?>"/>
	<div class="name <?if($STEP>0) echo "step"; ?>"><?=$BLOCK_TITTLE?> <?=$STEP?> <?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
				
	<div class="short">
		<textarea name="block[<?=$BLOCK_NAME?>][txt]" class="pole" maxlength="<?=$LEN?>"><?=$BLOCK_VALUE[0]?></textarea>
	</div>
		
	<?if($SHOW_CO=="Y"){?><div class="simbol_co short" id="<?=$BLOCK_NAME?>_co">Осталось <span><?=$OST?></span> символа</div><?}?>
				
	<div class="autor_photo" style="top:33px;">
		<div class="image"><img src="<?=$file["src"]?>" alt=""/></div>
                <ul id="file_field_<?=$BLOCK_NAME?>" class="unstyled"></ul>		
	</div>		
</div>
<script>
    $(document).ready(function(){
        var errorHandler = function(event, id, fileName, reason) {
          qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
        $('#file_field_<?=$BLOCK_NAME?>').fineUploader({
            text: {
                uploadButton: "Обзор",
                cancelButton: "Отмена",
                waitingForResponse: ""
            },
            multiple: false,
            request: {
                endpoint: "/tpl/ajax/upload.php",
                params: {"generateError": true}
            },
            /*failedUploadTextDisplay: {
                mode: 'custom',
                maxChars: 5
            }*/
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            $("#file_field_<?=$BLOCK_NAME?>").prev().html("").html('<img src="'+response.resized+'" height="111" /><input value="'+response.id+'" type="hidden" name="block[<?=$BLOCK_NAME?>][fid_new]" />');
        }); 
    });
    
</script>