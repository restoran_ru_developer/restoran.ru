<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_code($CODE, $IB){
	CModule::IncludeModule("iblock");
	
	$arSelect = Array("ID");
	$arFilter = Array("IBLOCK_ID"=>$IB, "CODE"=>$CODE);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->Fetch()){
  		return $CODE."-".time();//rand(1, 100);
	}else return $CODE;
}

function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID() || CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_element(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("sale") || !CModule::IncludeModule("currency")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
	
	//Если не выбран раздел, то кидаем просто в корневой
	if($_REQUEST["SECTION_ID"]>0) $SECTION_ID=$_REQUEST["SECTION_ID"];
	else $SECTION_ID=$_REQUEST["PARENT_SECTION"];
	
	

	$PROP = array();
	
	
	
	//собираем массив со свойствами
	foreach($_REQUEST as $CODE=>$VAL){
		if(substr_count($CODE, "PROPERTY_")>0){
			$CODE = str_replace("PROPERTY_", "", $CODE);
			//выкидываем пустые значения из массива
			if(is_array($VAL)){
				for($i=0; $i<count($VAL);$i++){
					if($VAL[$i]=="") unset($VAL[$i]);
				}
			}
			$PROP[$CODE]=$VAL;
		}
		
		
		if(substr_count($CODE, "PRICE_")>0){
			$CODE = str_replace("PRICE_", "", $CODE);
			$PRICE=$VAL;
			//var_dump($PRICE);
		}
	}
	
	//var_dump($PROP);
	
	if($_REQUEST["DETAIL_TEXT"]=="") $_REQUEST["DETAIL_TEXT"] = make_detail_text();
	
	//Записываем ник пользователю, если унего нет ника
	//var_dump($_REQUEST["USER_NICK"]);
	if(isset($_REQUEST["USER_NICK"]) && $_REQUEST["USER_NICK"]!=""){
		$_REQUEST["NAME"]=$_REQUEST["USER_NICK"];
		$rsUser = CUser::GetByID($USER->GetID());
		$USR = $rsUser->Fetch();
		if($USR["PERSONAL_PROFESSION"]==""){
			$user = new CUser;
			$user->Update($USER->GetID(), array("PERSONAL_PROFESSION"=>$_REQUEST["USER_NICK"]));
			
		}
	}
	//if ($USER->IsAdmin())            
           // var_dump($_FILES);
	if (CSite::InGroup(9))
            $_REQUEST["TAGS"] = $_REQUEST["TAGS"].", Профессиональный блог";
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $SECTION_ID,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => trim($_REQUEST["NAME"]),
  		"SORT"         => $_REQUEST["SORT"],  
  		"ACTIVE"         => $_REQUEST["ACTIVE"],            
  		"PREVIEW_TEXT"   => strip_tags($_REQUEST["PREVIEW_TEXT"]),
  		"ACTIVE_FROM"   => $_REQUEST["ACTIVE_FROM"],
  		"ACTIVE_TO"   => $_REQUEST["ACTIVE_TO"],
  		"DETAIL_TEXT"	 => $_REQUEST["DETAIL_TEXT"],
  		"DETAIL_TEXT_TYPE"=>"html",
  		"PREVIEW_TEXT_TYPE"=>"text",
  		"TAGS"=>$_REQUEST["TAGS"],
  		"CODE"=>check_code(translitIt(trim($_REQUEST["NAME"])), $_REQUEST["IBLOCK_ID"])
  	);
	
	if($_REQUEST["DATE_CREATE"])
            $arLoadProductArray["DATE_CREATE"]=$_REQUEST["DATE_CREATE"];
	
	//var_dump($arLoadProductArray);
	
	//Основная фотка
	if(isset($_FILES["DETAIL_PICTURE"])){
		$arLoadProductArray["DETAIL_PICTURE"]=$_FILES["DETAIL_PICTURE"];
	}	
	
	if(isset($_FILES["PREVIEW_PICTURE"])){
		$arLoadProductArray["PREVIEW_PICTURE"]=$_FILES["PREVIEW_PICTURE"];
	}
	
	if(isset($_REQUEST["del_prev_photo"]) && $_REQUEST["del_prev_photo"]=="Y"){
		$arLoadProductArray["DETAIL_PICTURE"]=array('del' => 'Y') ;
		$arLoadProductArray["PREVIEW_PICTURE"]=array('del' => 'Y') ;
	}
	
	
	//ПРИВЯЗКА К РЕСТОРАНАМ
	$RESTORANS_IDs=array();
	foreach($_REQUEST["block"] as $BL_NAME=>$BL){
		if($BL["type"]=="add_rest"){
			
			if(is_array($BL["selected"])){
				for($i=0; $i<count($BL["selected"]);$i++){
					if($BL["selected"][$i]=="") unset($BL["selected"][$i]);
				}
			}
			
			$RESTORANS_IDs = array_merge($RESTORANS_IDs, $BL["selected"]);	
		}
	}
	//////////////////////////
	
	
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию	
		
		if($arLoadProductArray["ACTIVE"]=="") unset($arLoadProductArray["ACTIVE"]);
		
		//var_dump($arLoadProductArray);
		
		unset($arLoadProductArray["CODE"]);
		unset($arLoadProductArray["IBLOCK_ID"]);
		
		if(!check_element_perm($_REQUEST["ELEMENT_ID"]) && $_REQUEST["ELEMENT_ID"]>0) die("Permission denied");
		
		$el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray);
		
		if($PRICE>0) CPrice::SetBasePrice($_REQUEST["ELEMENT_ID"], $PRICE, "RUB");
		
		CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], false, array("RESTORAN" => $RESTORANS_IDs));
		
		//переиндексация
		CIBlockElement::UpdateSearch($_REQUEST["ELEMENT_ID"], true);

        //  пост критика
        $arGroups = $USER->GetUserGroupArray();
        if($_REQUEST["IBLOCK_ID"]==156&&count(array_intersect($arGroups, array(35))) > 0){
            CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], false, array("CRITIC_POST" => 2810));
        }
        elseif($_REQUEST["IBLOCK_ID"]==157&&count(array_intersect($arGroups, array(35))) > 0){
            CIBlockElement::SetPropertyValuesEx($_REQUEST["ELEMENT_ID"], false, array("CRITIC_POST" => 2809));
        }

	
	}else{
		//создаем новую публикацию
		
		//делаем ее активной
		if($_REQUEST["ACTIVE"]==""){
			$arLoadProductArray["ACTIVE"]="Y";
		}

		
		if($ELEMENT_ID = $el->Add($arLoadProductArray))
  			echo $ELEMENT_ID;
		else
  			echo "Error: ".$el->LAST_ERROR;
                $new_el = CIBlockElement::GetByID($ELEMENT_ID);
                if ($new_el_ar = $new_el->GetNext())
                {
                    $new_link = "http://restoran.ru".$new_el_ar["DETAIL_PAGE_URL"];
                }
                if (CSite::InGroup(Array(9))):
                    $arMessage = Array(
                        "USER_ID"=> $USER->GetID(),
                        "USER_NAME"=> $USER->GetFullName(),
                        "NAME"=> $arLoadProductArray["NAME"],
                        "LINK" => $new_link
                    );
                    $res = CEvent::Send("ADD_ARTICLE_RESTORATOR", "s1", $arMessage);	
                endif;
  		if($PRICEC>0) CPrice::SetBasePrice($ELEMENT_ID, $PRICE, "RUB");	
  		
  		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("RESTORAN" => $RESTORANS_IDs));

        //  пост критика
        $arGroups = $USER->GetUserGroupArray();
        if($_REQUEST["IBLOCK_ID"]==156&&count(array_intersect($arGroups, array(35))) > 0){
            CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("CRITIC_POST" => 2810));
        }
        elseif($_REQUEST["IBLOCK_ID"]==157&&count(array_intersect($arGroups, array(35))) > 0){
            CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array("CRITIC_POST" => 2809));
        }
	}
	

	$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_' . $_REQUEST["IBLOCK_ID"]);
	
}


/****ФОРМИРУЕМ ТЕКСТ ДЕТАЛЬНОГО ОПИСАНИЯ****/
function make_detail_text(){
	$EXIT = "";
	$SHAG=0;
	
	//var_dump($_REQUEST["block"]);
	//var_dump($_FILES);
	$RESTORANS_IDs=array();
	foreach($_REQUEST["block"] as $BL_NAME=>$BL){
		
		
		if($BL["type"]!="add_step" && $SHAG>1 && !$steps_closed){
			$steps_closed=true;
			$EXIT.='</div>';
		}
		
		//ШАГ
		if($BL["type"]=="add_step"){
			$SHAG++;
			if($SHAG==1) $EXIT.='<div class="instructions">';
                        global $USER;
			$FL=form_file_array($BL_NAME);
			if($FL["name"]!="")
                        {
                            $DIR=$_SERVER["DOCUMENT_ROOT"].'/images/_tmp';
                            $new_name = translateFilename($FL['name']);
                            copy($FL['tmp_name'], $DIR.'/'.$new_name);
        
                            $temp = explode(".",$new_name);        
                            $rasch = $temp[count($temp)-1];
                            unset($temp[count($temp)-1]);
                            $nn = implode("",$temp);
                            $new_file_name = $nn.substr(md5(mt_rand()), 0, 3)."_res.".$rasch;
                            $s = $DIR.'/'.$new_file_name;
                            CFile::ResizeImageFile($DIR.'/'.$new_name, $s, Array("width"=>728,"height"=>486),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,array(),60,false);
                            unlink($DIR.'/'.$new_name);
                            $nFile = CFile::MakeFileArray('/images/_tmp/'.$new_file_name);
                            
                            $fid = CFile::SaveFile($nFile);
                            unlink($s);
			}
                        elseif($BL["fid_new"])
                        {
                            $fid=$BL["fid_new"];
                        }
                        else 
                            $fid=$BL["fid"];
			global $USER;                            
			
			$EXIT.='<div class="recept_text shag bl" itemprop="recipeInstructions"><h2>Шаг '.$SHAG.' </h2><div class="left photos123" style="position:relative; width:238px;margin-bottom: 10px;"><img itemprop="image" id="pic'.$fid.'" src="#FID_'.$fid.'#" class="pic" width="232"/><img id="pic'.$fid.'big" style="display:none" src="#FID_'.$fid.'#" width="728"/></div><div style="" class="txt instruction">'.$BL["txt"].'</div><div class="clear"></div></div>';
 			
 			//$SHAG++;
		}
		
		
		
		
		//ПРЯМАЯ РЕЧЬ
		if($BL["type"]=="add_pr"){
			//формируем массив файла
			
			$FL=form_file_array($BL_NAME);
			if($FL["name"]!=""){
				$fid = CFile::SaveFile($FL);
			}else $fid=$BL["fid"];
			
			
			
			$EXIT.='<br/><table class="direct_speech bl"><tbody><tr> <td width="170" style="border-image: initial; "><div class="author"><img src="#FID_PR'.$fid.'#"  class="pic" width="150"/> </div></td> <td width="150" style="border-image: initial; "> <span class="uppercase txt1">'.$BL["txt1"].'</span><br /><i class="txt2">'.$BL["txt2"].'</i> </td> <td class="font14" style="border-image: initial; "> <i class="txt3">'.$BL["txt3"].'</i> </td> </tr></tbody></table>';
		}
		
		
		
		//ИНГРЕДИЕНТЫ
		if($BL["type"]=="ingr"){
			$INGRS="";
			$INGRS.='<ul>';
			foreach($BL["ingrname"] as $ikey => $INname){
				if(trim($INname)!=""){
					$INGRS.='<li class="ingredient" itemprop="ingredients"><span class="name">'.trim($INname).'</span>';
					if(trim($BL["ingrco"][$ikey])!='') $INGRS.=' - <span class="amount">'.trim($BL["ingrco"][$ikey]).'</span>';
					$INGRS.='</li>';
				}	
			}
			$INGRS.='</ul>';
			
			if($BL["zag"]!="") $ZG=trim($BL["zag"]);
			else $ZG='Ингредиенты:';
			
			$EXIT.='<div class="ingr bl"><h2>'.$ZG.'</h2><div style="" class="txt">'.$INGRS.'</div></div>';
		}
		
		
		

		//ВСТАВКА ТЕКСТА
		if($BL["type"]=="add_text"){
			if (trim($BL["zag"]))
                            $EXIT.='<div class="vis_red bl"><h2>'.$BL["zag"].'</h2><div style="" class="txt">'.$BL["val"].'</div></div>';
                        else
                            $EXIT.='<div class="vis_red bl"><div style="" class="txt">'.$BL["val"].'</div></div>';
		}
		
		//ВСТАВКА РЕСТОРАНА
		if($BL["type"]=="add_rest"){
			
			if(is_array($BL["selected"])){
				for($i=0; $i<count($BL["selected"]);$i++){
					if($BL["selected"][$i]=="") unset($BL["selected"][$i]);
				}
			}
			
			$RESTs="";
			foreach($BL["selected"] as $rid){
				$RESTs.="#REST_".$rid."# ";
			}
			
			
			$RESTORANS_IDs = array_merge($RESTORANS_IDs, $BL["selected"]);
                        $BL["txt"] = str_replace("<p></p>","",$BL["txt"]);
			$EXIT.='<div class="restoran_text bl"><div class="left article_rest">'.$RESTs.'</div><div class="right article_rest_text">'.$BL["txt"].'</div><div class="clear"></div></div>';
		}
		
		//ФОТОГАЛЕРЕЯ
		if($BL["type"]=="add_photos"){
		
			
			//нужно собрать массив
			//сначала добавляем уже загруженные картинки
			$PICS=array();
			//var_dump($BL["descr"]);
			if(isset($BL["file"]) && is_array($BL["file"])){
				for($i=0;$i<count($BL["file"]);$i++){
					$PICS[]=array("FID"=>$BL["file"][$i], "DESCR"=>$BL["descr"][$i]);
				}
			}
                        
                        if(isset($BL["fid_new"]) && is_array($BL["fid_new"])){
				for($i=0;$i<count($BL["fid_new"]);$i++){
					$PICS[]=array("FID"=>$BL["fid_new"][$i], "DESCR"=>$BL["descr_new"][$i]);
				}
			}
//                        global $USER;
//                        if ($USER->IsAdmin())
//                        {
//                            v_dump($BL);
//                            v_dump($PICS);
//                            exit;
//                        }
			$FL=form_file_array($BL_NAME);
			//var_dump($FL);
			if(is_array($FL) && is_array($FL[0])){
				$i=0;
			//	echo 1;
				foreach($FL as $F){
					$fid = CFile::SaveFile($F,"publications");
					$PICS[]=array("FID"=>$fid, "DESCR"=>$BL["descr_ja"][$i]);
					$i++;
				}
			}
                        else{
			//	echo 2;
				$i=0;
				if($FL["name"]!=""){
					$fid = CFile::SaveFile($FL[0],"publications");
					$PICS[]=array("FID"=>$fid, "DESCR"=>$BL["descr_ja"][$i]);
				}
			}
			
			
			//var_dump($PICS);
			//echo '<br/><br/>';	
			$EXIT.='<div class="articles_photo bl">';
			
				if(count($PICS)>1){
					$EXIT.='<div class="left scroll_arrows">';
					$EXIT.='<a class="prev browse left" ></a><span class="scroll_num">1</span>/'.count($PICS).'<a class="next browse right" ></a>';
					$EXIT.='</div><div class="clear"></div>';
  				}	
  				for($i=0;$i<count($PICS);$i++){
  					if($i==0){
  						//первая картинка
  						$EXIT.='<div class="img"><div class="images"><img src="#FID_'.$PICS[$i]["FID"].'#" height="512" class="first_pic" /></div><p><i>'.$PICS[$i]["DESCR"].'</i></p></div>';
  						if(count($PICS)>1){
  							$EXIT.='<div class="special_scroll"><div class="scroll_container">';
    						$EXIT.='<div class="item active"> <img src="#FID_'.$PICS[$i]["FID"].'#" alt="'.$PICS[$i]["DESCR"].'" align="bottom"  class="pic"/> </div>';	
  						}
  					}else{
  						$EXIT.='<div class="item"> <img src="#FID_'.$PICS[$i]["FID"].'#" alt="'.$PICS[$i]["DESCR"].'" align="bottom"  class="pic"/> </div>';
  					}
  				}
  				
  				if(count($PICS)>1) $EXIT.='</div></div>';
  				
  				$EXIT.='</div>';
		}
		
		
		
		//ВИДЕО
		if($BL["type"]=="add_video"){
			
			$FL=form_file_array($BL_NAME);
			if($FL["name"]!=""){
				$fid = CFile::SaveFile($FL,"publications");
			}else $fid=$BL["fid"];
			
			if($fid!="") $EXIT.='<div class="video bl"><a class="myPlayer" href="http://restoran.ru#FID_'.$fid.'#"></a></div><br/>';
 			
 			$SHAG++;
		}
		
		
		//ВИДЕО
		if($BL["type"]=="add_video_code"){
			
			$EXIT.='<div class="video_code bl">'.$BL["val"].'</div><br/>';
 			
		}
	
	}

	if(!$steps_closed && $SHAG>0){
			$steps_closed=true;
			$EXIT.='</div>';
	}

	return $EXIT;
}

/***********ФОРМИРУЕМ МАССИВ ФАЙЛА**************/
function form_file_array($BL_NAME){
	$FAR=array();

	foreach($_FILES["block"] as $A=>$C){		
		$VAL="";
		if(is_array($C[$BL_NAME])){
			foreach($C[$BL_NAME] as $V) $VAL=$V;
		}
		$FAR[$A]=$V;
	}
	
	if(is_array($FAR["name"])){
		$RET=array();
		foreach($FAR as $N=>$V){
			for($i=0;$i<count($V);$i++){
				$RET[$i][$N]=$V[$i];
			}
		}
		//var_dump($RET);
		return $RET;
	}else return $FAR;
}

function update_element_tags(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if(!check_section_perm($_REQUEST["SECTION_ID"])) die("Permission denied");
}

function get_tag_list(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}	
	
	$tar = tags_prepare($_REQUEST["tags"], "s1");
	//var_dump($tar);
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["TAG_SECTION"], "IBLOCK_ID"=>99, "NAME"=>"%".$_REQUEST["tag"]."%");
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	while($ar_fields = $res->GetNext()){
		if(!in_array($ar_fields["NAME"], $tar)) echo '<a href="#">'.$ar_fields["NAME"].'</a>';
	}

	
}


function new_tag(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}	
	
	if(!check_section_perm($_REQUEST["TAG_SECTION"])) die("Permission denied");
	
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["TAG_SECTION"], "IBLOCK_ID"=>99, "NAME"=>$_REQUEST["new_tag"]);
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	if($ar_fields = $res->GetNext()){
		
	}else{
		$el = new CIBlockElement;
		$arLoadProductArray["NAME"]=$_REQUEST["new_tag"];
		$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["TAG_SECTION"];
		$arLoadProductArray["IBLOCK_ID"]=99;
		$arLoadProductArray["ACTIVE"]="Y";
		$el->Add($arLoadProductArray);
	}
}

function parse_ingredients($INGR){
	//var_dump($INGR);
	
	$NEW_UL="<ul>";
	$regexp = '#<li.*?>(.*?)</li>#s';
	if (preg_match_all($regexp, $INGR, $matches)) {
		//var_dump($matches[1]);
		
		foreach($matches[1] as $li){
			$li=strip_tags($li);
			
			if(substr_count($li, "-")>0){
				$li_ar = explode("-", $li);
				
				$NEW_UL.='<li class="ingredient"><span class="name">'.trim($li_ar[0]).'</span> - <span class="amount">'.trim($li_ar[1]).'</span></li>';
			}else{
				$NEW_UL.='<li class="ingredient"><span class="name">'.$li.'</span></li>';
			}
			
		}
  		//$li = $matches[1];
  		//var_dump($li);	
	}
	$NEW_UL.="</ul>";
	
	
	
	$INGR2 = preg_replace('#<ul.*?>(.*?)</ul>#s',$NEW_UL, $INGR);
	//var_dump($INGR2);
	//$INGR = preg_replace('/(<li\b[^><]*)>/i', '$1 class="ingredient">', $INGR);

	return $INGR2;
}


function parse_ingredients2($INGR){
	//var_dump($INGR);
	
	$NEW_ULs=array();
	
	$regexp1 = '#<li.*?>(.*?)</li>#s';
	$regexp2 = '#<ul.*?>(.*?)</ul>#s';
	if (preg_match_all($regexp2, $INGR, $ULs)) {
		$i=0;
		foreach($ULs[1] as $ul){
			$NEW_ULs[$i]="<ul>";
			if (preg_match_all($regexp1, $ul, $LIs)) {
				foreach($LIs[1] as $li){
					$li=strip_tags($li);
			
					if(substr_count($li, "-")>0){
						$li_ar = explode("-", $li);
						$NEW_ULs[$i].='<li class="ingredient"><span class="name">'.trim($li_ar[0]).'</span> - <span class="amount">'.trim($li_ar[1]).'</span></li>';
					}else{
						$NEW_ULs[$i].='<li class="ingredient"><span class="name">'.$li.'</span></li>';
					}
				}
			}
			$NEW_ULs[$i].="</ul>";
			$i++;	
		}
	}
	
	$INGR = preg_replace(array("#<ul.*?>(.*?)</ul>#s","#<ul.*?>(.*?)</ul>#s"), $NEW_ULs, $INGR);
	
	return $INGR;
}


if($_REQUEST["act"]=="edit_element") edit_element();

if($_REQUEST["act"]=="get_tag_list") get_tag_list();
if($_REQUEST["act"]=="update_element_tags") update_element_tags();
if($_REQUEST["act"]=="new_tag") new_tag();

?>
