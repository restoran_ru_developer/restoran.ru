<?
$OST = $LEN - trim(strlen($BLOCK_VALUE[0]["VALUE"]));

$file = CFile::ResizeImageGet($BLOCK_VALUE[1]["VALUE"], array('width'=>111, 'height'=>111), BX_RESIZE_IMAGE_EXACT, true); 
?>


<div class="content_block">
	<input type="hidden" id="<?=$BLOCK_VALUE[2]["NAME"]?>" name="<?=$BLOCK_VALUE[2]["NAME"]?>" value="<?=$BLOCK_VALUE[2]["VALUE"]?>"/>
	<input type="hidden" name="bid__<?=$ELEMENT_ID?>__SORT" value="<?=$SORT?>"  class="sort"/>
	
	<div class="name <?if($STEP>0) echo "step"; ?>"><?=$BLOCK_TITTLE?> <?=$STEP?> <?if($NOT_DELETE!="Y"){?><a href="#<?=$ELEMENT_ID?>" class="remove_block">Удалить</a><?}?></div>
				
	<div class="short">
		<textarea name="<?=$BLOCK_VALUE[0]["NAME"]?>" class="pole" maxlength="<?=$LEN?>"><?=$BLOCK_VALUE[0]["VALUE"]?></textarea>
	</div>
		
	<?if($SHOW_CO=="Y"){?><div class="simbol_co short" id="<?=$BLOCK_VALUE[0]["NAME"]?>_co">Осталось <span><?=$OST?></span> символа</div><?}?>
				
	<div class="autor_photo" style="top:33px;">
		<div class="image"><img src="<?=$file["src"]?>" alt=""/></div>
		<div class="lnk"><input type="file" name="<?=$BLOCK_VALUE[1]["NAME"]?>" value="" id="file_field_<?=$BLOCK_VALUE[1]["NAME"]?>"  maxlength="<?=$LEN?>" class=""/><a href="#">Обзор</a></div>
		<input type="hidden" id="file_changed_<?=$BLOCK_VALUE[1]["NAME"]?>" name="file_changed_<?=$BLOCK_VALUE[1]["NAME"]?>_changed" value="N"/>
	</div>		
</div>



		

<script type="text/javascript">	
// Проверка поддержки File API в браузере
var fileInput = $('#file_field_<?=$BLOCK_VALUE[1]["NAME"]?>');

if(window.FileReader == null) {
	//alert('Ваш браузер не поддерживает File API!');
	fileInput.next().remove();
	fileInput.customFileInput();
	
	fileInput.bind({
		change: function() {
    	   $("#file_changed_<?=$BLOCK_VALUE[1]["NAME"]?>").val("Y");
		}
	});
	
}else{
	fileInput.addClass("file_field");

	fileInput.bind({
		change: function() {
    	    displayFiles_<?=$BLOCK_VALUE[1]["NAME"]?>(this.files, fileInput);
		
		}
	});

	// Отображение выбраных файлов и создание миниатюр
	function displayFiles_<?=$BLOCK_VALUE[1]["NAME"]?>(files, obj) {
		var imageType = /image.*/;
		var num = 0;
    	
    	$("#file_changed_<?=$BLOCK_VALUE[1]["NAME"]?>").val("Y");
    
     	$.each(files, function(i, file) {
		
			// Отсеиваем не картинки
			if (!file.type.match(imageType)) {
				alert("Ошибка")
				return true;
			}

       		num++;
            
	       	var img = obj.parent("div").prev().children("img");
   		    obj.parent("div").prev().get(0).file = file;
            
	        var reader = new FileReader();
        
   		    reader.onload = (function(aImg) {
				return function(e) {
					aImg.attr('src', e.target.result);
					aImg.attr('width', 111);           
				};
			})(img); 
		  
        	reader.readAsDataURL(file);         
	});    
}
}
</script>