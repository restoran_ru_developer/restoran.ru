<?
$file = CFile::ResizeImageGet($BLOCK_VALUE, array('width'=>130, 'height'=>130), BX_RESIZE_IMAGE_EXACT, true);
?>
<input type="hidden" name="bid__<?=$ELEMENT_ID?>__SORT" value="<?=$SORT?>" />
<div class="content_block">
<div class="name"><?=$BLOCK_TITTLE?><?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
	<div class=" one_file <? if($BLOCK_VALUE!="") echo 'pic_added';?>">
		<div class="pic"><img src="<?=$file["src"]?>"/></div>
		<div class="lnk"><input type="file" name="<?=$BLOCK_NAME?>" value="" id="file_field_<?=$BLOCK_NAME?>" class=""/><a href="#">Обзор</a></div>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">	
// Проверка поддержки File API в браузере
var fileInput_<?=$BLOCK_NAME?> = $('#file_field_<?=$BLOCK_NAME?>');

if(window.FileReader == null) {
	//alert('Ваш браузер не поддерживает File API!');
	fileInput_<?=$BLOCK_NAME?>.next().remove();
	fileInput_<?=$BLOCK_NAME?>.customFileInput();
	
	fileInput_<?=$BLOCK_NAME?>.bind({
		change_<?=$BLOCK_NAME?>: function() {
    	   $("#file_changed_<?=$BLOCK_NAME?>").val("Y");
		}
	});
	
}else{
	fileInput_<?=$BLOCK_NAME?>.addClass("file_field");

	fileInput_<?=$BLOCK_NAME?>.next().remove();
	fileInput_<?=$BLOCK_NAME?>.customFileInput();
	
	fileInput_<?=$BLOCK_NAME?>.bind({
		change: function() {
    	    displayFiles_<?=$BLOCK_NAME?>(this.files, fileInput_<?=$BLOCK_NAME?>);
		
		}
	});
	
	
	
	// Отображение выбраных файлов и создание миниатюр
	function displayFiles_<?=$BLOCK_NAME?>(files, obj) {
		var imageType = /image.*/;
		var num = 0;
    	
    	$("#file_changed_<?=$BLOCK_NAME?>").val("Y");
    
     	$.each(files, function(i, file) {
		
			// Отсеиваем не картинки
			if (!file.type.match(imageType)) {
				alert("Ошибка")
				return true;
			}

       		num++;
            
           
            obj.parent("div").parent("div").parent("div").addClass("pic_added");
	       	var img = obj.parent("div").parent("div").prev().children("img");
   		    obj.parent("div").parent("div").prev().get(0).file = file;
            
	        var reader = new FileReader();
        
        
        
        
        
   		    reader.onload = (function(aImg) {
				return function(e) {
					aImg.attr('src', e.target.result);
					aImg.attr('width', 130);  
					aImg.attr('height', 130);           
				};
			})(img); 
		  
        	reader.readAsDataURL(file);         
	});    
}
}
</script>