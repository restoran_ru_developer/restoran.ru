<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?$APPLICATION->IncludeComponent(
	"restoran:editor",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Шаг", "CODE"=>"add_step"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest")				
		),		
		"SECTION_ID"=>$_REQUEST["SECTION_ID"],
		"IBLOCK_ID"=>54,
		"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_NAME", "VALUE_FROM"=>"SECTION__NAME"),
			/*array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"SECTION_PICTURE", "VALUE_FROM"=>"SECTION__PICTURE"),
			array("NAME"=>"Кухня", "TYPE"=>"multi_select", "CODE"=>"UF_KITCHEN", "VALUE_FROM"=>"SECTION__UF_KITCHEN__VALUE", "VALUES_LIST"=>"SECTION__UF_KITCHEN__VALUES"),
			array("NAME"=>"Повод", "TYPE"=>"multi_select", "CODE"=>"UF_CAUSE", "VALUE_FROM"=>"SECTION__UF_CAUSE__VALUE", "VALUES_LIST"=>"SECTION__UF_CAUSE__VALUES"),
			array("NAME"=>"Предпочтения", "TYPE"=>"multi_select", "CODE"=>"UF_PREFERENCES", "VALUE_FROM"=>"SECTION__UF_PREFERENCES__VALUE", "VALUES_LIST"=>"SECTION__UF_PREFERENCES__VALUES"),
			array("NAME"=>"Время приготовления", "TYPE"=>"select", "CODE"=>"UF_TIME", "VALUE_FROM"=>"SECTION__UF_TIME__VALUE", "VALUES_LIST"=>"SECTION__UF_TIME__VALUES"),
			array("NAME"=>"Приготовление", "TYPE"=>"select", "CODE"=>"UF_PREPARATION", "VALUE_FROM"=>"SECTION__UF_PREPARATION__VALUE", "VALUES_LIST"=>"SECTION__UF_PREPARATION__VALUES"),
			array("NAME"=>"Категории", "TYPE"=>"select", "CODE"=>"UF_SECTION", "VALUE_FROM"=>"SECTION__UF_SECTION__VALUE", "VALUES_LIST"=>"SECTION__UF_SECTION__VALUES")*/
		),
		"TAG_SECTION"=>44203,
		"NEW_ARTICLE"=>$_REQUEST["NEW_ARTICLE"]
		
	),
false
);?> 