var RTOOLBAR = {
	h5: 		{exec: 'formatblock', name: 'h5', title: RLANG.header1, style: 'font-size: 28px;'},
	ed_bold: {exec: 'bold', name: 'bold', title: RLANG.bold, style: 'font-weight: bold;'},
	ed_italic: {exec: 'italic', name: 'italic', title: RLANG.italic, style: 'font-style: italic;'},
	ed_italic: {exec: 'italic', name: 'italic', title: RLANG.italic, style: 'font-style: italic;'},
	ed_strike:  {exec: 'StrikeThrough', name: 'StrikeThrough', title: RLANG.strikethrough, style: 'text-decoration: line-through !important;'},
	ed_ul: 	 {exec: 'insertunorderedlist', name: 'insertunorderedlist', title: '&bull; ' + RLANG.unorderedlist},
	ed_link: 	{name: 'link', title: RLANG.link_insert, func: 'showLink'}
}