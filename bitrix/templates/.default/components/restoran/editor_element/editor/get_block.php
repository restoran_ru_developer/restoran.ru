<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/
//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die();

$el = new CIBlockElement;

$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["SECTION_ID"];
$arLoadProductArray["IBLOCK_ID"]=$_REQUEST["IBLOCK_ID"];
$arLoadProductArray["ACTIVE"]="Y";


//var_dump($APPLICATION->GetCurDir());
switch ($_REQUEST["block_id"]) {
    case "add_short_text":
    	$arLoadProductArray["NAME"]="Короткий текст";
    	
    	$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Прямая речь"));
		if($enum_fields = $property_enums->GetNext()) $arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"] = $enum_fields["ID"];
		$BL_ID = $el->Add($arLoadProductArray);
		
        $APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/short_text_block.php",
			Array(
				"BLOCK_TITTLE"=>"Короткий текст",
				"BLOCK_NAME"=>"block_name",
                "BLOCK_VALUE" =>"",
                "LEN"=>250,
                "SHOW_CO"=>"Y",
                "ELEMENT_ID" => $BL_ID,
	            "SORT"=>500
			),
			Array("MODE"=>"php")
        );
        break;
    case "add_rest":
    	$arLoadProductArray["NAME"]="Привязка к ресторану";
    	
    	$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Привязка к ресторану"));
		if($enum_fields = $property_enums->GetNext()) $arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"] = $enum_fields["ID"];
		$BL_ID = $el->Add($arLoadProductArray);
		
		
        //Нужно получить список всех ресторанов
		//Потом включить кэширование
		$arRestIB = getArIblock("catalog", CITY_ID);
		$arFilter = Array('IBLOCK_ID'=>$arRestIB["ID"], 'ACTIVE'=>'Y');
  		$db_list = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false);
  		$VALUES =array(); 
  		while($ar_result = $db_list->GetNext()){
  			$VALUES[]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"]);
		}				
				//v_dump($EL["PROPERTIES"]["REST_BIND"]);
            	$APPLICATION->IncludeFile(
			 		$APPLICATION->GetCurDir()."/blocks/restoran.php",
					Array(
						"BLOCK_TITTLE"=>"Упомянание о ресторане",
       			        "BLOCK_VALUE" =>array(
        	        	array("NAME"=>"bid__".$BL_ID."__DETAIL_TEXT", "VALUE"=>$EL["DETAIL_TEXT"]),
    	            	array("NAME"=>"bid__".$BL_ID."__REST_BIND", "VALUE"=>$EL["PROPERTIES"]["REST_BIND"]["VALUE"],"VALUES"=>$VALUES),
    	            	array("NAME"=>"bid__".$BL_ID."__BLOCK_TYPE", "VALUE"=>$enum_fields["ID"])
	                ),
	                "ELEMENT_ID" => $BL_ID,
	                "SORT"=>500,
	                "SELECT_TITLE"=>"Выберите ресторан"
					),
					Array("MODE"=>"php")
        		);
        break;
    case "add_step":
    	$arLoadProductArray["NAME"]="Шаг";
    	
    	$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Шаг"));
		if($enum_fields = $property_enums->GetNext()) $arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"] = $enum_fields["ID"];
		if($BL_ID = $el->Add($arLoadProductArray)){
		
    		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/shag_block.php",
				Array(
					"BLOCK_TITTLE"=>"Шаг",
					"STEP"=>$_REQUEST["steps"]+1,
        	        "BLOCK_VALUE" =>array(
            	    	array("NAME"=>"bid__".$BL_ID."__DETAIL_TEXT", "VALUE"=>$EL["DETAIL_TEXT"]),
            	    	array("NAME"=>"bid__".$BL_ID."__DETAIL_PICTURE", "VALUE"=>$EL["DETAIL_PICTURE"]),
            	    	array("NAME"=>"bid__".$BL_ID."__BLOCK_TYPE", "VALUE"=>$enum_fields["ID"])
                	),
                	"ELEMENT_ID" => $BL_ID,
                	"SORT"=>500
			),
			Array("MODE"=>"php")
        	);
        }else{
       		echo "Error: ".$el->LAST_ERROR;
        }
        break;    
    case "add_text":
    	$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Визуальный редактор"));
		if($enum_fields = $property_enums->GetNext()) $arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"] = $enum_fields["ID"];
		$arLoadProductArray["NAME"]="Визуальный редактор";
		
       	if($BL_ID = $el->Add($arLoadProductArray)){	
			$APPLICATION->IncludeFile(
				$APPLICATION->GetCurDir()."/blocks/vis_red.php",
				Array(
					"BLOCK_TITTLE"=>"Вставка текста",
        	       	"BLOCK_VALUE" =>array(
                		array("NAME"=>"bid__".$BL_ID."__NAME", "VALUE"=>$EL["NAME"]),
               			array("NAME"=>"bid__".$BL_ID."__DETAIL_TEXT", "VALUE"=>$EL["DETAIL_TEXT"]),
               		),
               		"SORT"=>500
				),
				Array("MODE"=>"php")
        	);
		}
        break;
    case "add_pr":
    	$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Прямая речь"));
		if($enum_fields = $property_enums->GetNext()) $arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"] = $enum_fields["ID"];
		$arLoadProductArray["NAME"]="Прямая речь";
		
		if($BL_ID = $el->Add($arLoadProductArray)){
		
			$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/pr_rech.php",
			Array(
				"BLOCK_TITTLE"=>"Прямая речь",
				"BLOCK_VALUE" =>array(
					array("NAME"=>"bid__".$BL_ID."__RESPONDENT_NAME", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_NAME"]["VALUE"]),
    	           	array("NAME"=>"bid__".$BL_ID."__RESPONDENT_POST", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_POST"]["VALUE"]),
    	           	array("NAME"=>"bid__".$BL_ID."__RESPONDENT_SPEECH__TEXT", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"]),
    	           	array("NAME"=>"bid__".$BL_ID."__RESPONDENT_PHOTO", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"]),
    	           	array("NAME"=>"bid__".$BL_ID."__BLOCK_TYPE", "VALUE"=>$arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"])
	            ),
           		"SHOW_CO"=>"Y",
               	"LEN"=>250,
	            "ELEMENT_ID" => $BL_ID,
	            "SORT"=>500
			),
			Array("MODE"=>"php")
       		);
        }else{
        	 echo "Error: ".$el->LAST_ERROR;
        }
    	break;
    case "add_photos":
    	$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Фотогалерея"));
		if($enum_fields = $property_enums->GetNext()) $arLoadProductArray["PROPERTY_VALUES"]["BLOCK_TYPE"] = $enum_fields["ID"];
		$arLoadProductArray["NAME"]="Фотогалерея";
		if($BL_ID = $el->Add($arLoadProductArray)){
			$APPLICATION->IncludeFile(
					$APPLICATION->GetCurDir()."/blocks/photos.php",
					Array(
						"BLOCK_TITTLE"=>"Фотогалерея",
        	        	"BLOCK_VALUE" =>array(
                			array("NAME"=>"bid__".$BL_ID."__PICTURES", "VALUE"=>$EL["PROPERTIES"]["PICTURES"]["VALUE"]),
                			array("NAME"=>"bid__".$BL_ID."__PICTURES_DESCR", "VALUE"=>$EL["PROPERTIES"]["PICTURES"]["DESCRIPTION"]),
                			array("NAME"=>"bid__".$BL_ID."__BLOCK_TYPE", "VALUE"=>$enum_fields["ID"])
                		),
                		"ELEMENT_ID" => $BL_ID,
                		"SORT"=>500
					),
					Array("MODE"=>"php")
        		);
        }else{
        	 echo "Error: ".$el->LAST_ERROR;
        }
    	break;	
        
}


?>