$(document).ready(function(){
function getBodyScrollTop(){
	return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}

$(window).scroll(function(){
	var glob_top = $('#editor_form').offset().top;
	var gto = getBodyScrollTop();
		
	if(gto>glob_top) {
		gto = gto - glob_top+10;			
	}else{
		gto =0;
	}
	
	$('#add_modules').stop().animate({'top':gto},500);
	$('#choose_tags').stop().animate({'top':gto},500);
	
	
});
	
	
$("#add_modules ul li a").click(function(){
	return false;
});
$( "#add_modules .block" ).draggable({
	connectToSortable: "#block_list",
	helper: "clone",
	revert: "invalid",
	cursorAt: { cursor: "move"},
	stop: function(event, ui) {
		var id = $(this).find("a").attr("id");

	}, 
	start: function(event, ui){
		
	}
});

$("#block_list").sortable({
	placeholder: "ui-state-highlight",
	update: function(event, ui) {
		get_editor_block(ui.item);

		//если в блоке есть редактор, то разрушаем его и создаем новый
		if(ui.item.find(".vredactor").length>0){
			var id = ui.item.find(".vredactor").attr("id");
			var code = redactors[id].getCode();
			redactors[id].destroy();		
			redactors[id] = $('#'+id).redactor({ css: ['blank.css'], toolbar: 'my', resize:false });
			redactors[id].setHtml(code);
		}
	},
	stop: function(event, ui) {
		go_sort();
		//если в блоке есть редактор, то разрушаем его и создаем новый
		if(ui.item.find(".vredactor").length>0){
			var id = ui.item.find(".vredactor").attr("id");
			var code = redactors[id].getCode();
			redactors[id].destroy();		
			redactors[id] = $('#'+id).redactor({ css: ['blank.css'], toolbar: 'my', resize:false });
			redactors[id].setHtml(code);
		}
	},
	start: function(event, ui) {
		//делаем прозрачный блок высотой с перетаскиваемый
		$("#block_list .ui-state-highlight").height(ui.item.height());
	}
});

function go_sort(){
	var s = 10;
	var to_send = {};
	$(".sort").each(function(){
		$(this).val(s);
		s+= 10;
		to_send[$(this).attr("name")] = $(this).val();
	});
	
	to_send["act"] = "sort_update";
	
	var link = $("#editor_form").attr("action");
	$.post(link, to_send, function(otvet){
		
	});
}

function get_editor_block(obj){
	if(!obj.hasClass("loaded") && !obj.hasClass("ui-state-default")){
		var id = obj.find("a").attr("id");
		var link = obj.find("a").attr("href");
		obj.find("a").remove();
		var steps = $(".step").length;
		
		var section_id = $("input[name=SECTION_ID]").val();
		var iblock_id = $("input[name=IBLOCK_ID]").val();
	
		
		$.post(link,{block_id: id, steps: steps, SECTION_ID: section_id, IBLOCK_ID: iblock_id},function(otvet){
			obj.html(otvet);
			obj.addClass("loaded");
		});
	}
}


$("#block_list").on("click",".remove_block", function(){
	var link = $("#editor_form").attr("action");
	var id = $(this).attr("href");
	
	$.post(link,{act:"del_block", ELEMENT_ID: id},function(otvet){
		
	});
	
	$(this).parent("div").parent("div").parent("li").hide("normal", function(){
		$(this).remove();	
	});
	return false;
});       


///ТЭГИ
$("#choose_tags .tag_list").on("click", "a", function(event){
	$("#choose_tags .pop").append($(this));
	/*
	$(this).hide("fast", function(){
		$(this).remove();
		
	});
	*/
	update_element_tags();
	return false
});

$("#choose_tags .pop").on("click", "a", function(event){
	$("#choose_tags .tag_list").append($(this));
	
	update_element_tags();
		
	return false
});



function update_element_tags(){
	var section_id = $("input[name=SECTION_ID]").val();
	var iblock_id = $("input[name=IBLOCK_ID]").val();
	var tags = "";
	var link = $("#editor_form").attr("action");
	
	
	$("#choose_tags .tag_list a").each(function(){
		tags+=$(this).html()+",";
	});
	
	$.post(link,{SECTION_ID: section_id, tags: tags, act:"update_element_tags", IBLOCK_ID:iblock_id},function(otvet){
		
	});
}


$("#new_tag").keyup(function(){
	if($(this).val().length>2){
		$("#choose_tags .add_new_tag").show();
		var link = $("#editor_form").attr("action");
		var tag_section = $("input[name=TAG_SECTION]").val();
		var iblock_id = $("input[name=IBLOCK_ID]").val();
		var tags = "";
		
		$("#choose_tags .tag_list a").each(function(){
			tags+=$(this).html()+",";
		});
	
		$.post(link,{TAG_SECTION: tag_section, tag: $(this).val(), act:"get_tag_list", IBLOCK_ID:iblock_id, tags: tags},function(otvet){
			$("#choose_tags .all_tags_lis").html(otvet);
			$("#choose_tags .all_tags_lis").slideDown("fast");
			
			
		});
	}else{
		$("#choose_tags .add_new_tag").hide();
	}
});

$("body").click(function(){
	$("#choose_tags .all_tags_lis").slideUp("fast");
});



$("#choose_tags .all_tags_lis a").live("click", function(){
	$("#choose_tags .tag_list").append($(this));
	
	update_element_tags();

	$("#choose_tags .all_tags_lis").slideUp("fast", function(){
		$("#choose_tags .all_tags_lis").html();
	});
	return false;
});

$("#choose_tags .add_new_tag").click(function(){
	
	var link = $("#editor_form").attr("action");
	var tag_section = $("input[name=TAG_SECTION]").val();
	var iblock_id = $("input[name=IBLOCK_ID]").val();
	var new_tag = $("#new_tag").val();
	var tags = "";
	$("#choose_tags .tag_list a").each(function(){
		tags+=$(this).html()+",";
	});
		
	$.post(link, {TAG_SECTION: tag_section, new_tag: new_tag, act: "new_tag", IBLOCK_ID:iblock_id, tags: tags},function(otvet){
		$("#choose_tags .tag_list").append('<a href="#">'+new_tag+'</a>');
		$("#new_tag").val("");
		$("#choose_tags .all_tags_lis").slideUp("fast");
		
		update_element_tags();	
	});
	
	return false;
});

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////




var act = "";
$("#public").click(function(){
	act = "public";
	refresh_form();
	$('#editor_form').submit();
	return false;
});

function refresh_form(){
//проверка формы
function check_editor_form(a,f,o){
	var ret=true;
	o.dataType = "html";
	return ret;
}

//Отправка формы
$('#editor_form').ajaxForm({
	beforeSubmit: check_editor_form,
	success: function(data) {
		console.log(data);
	}
});
}

});