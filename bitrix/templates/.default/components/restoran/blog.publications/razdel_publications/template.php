<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="blog-items">
<?if(count($arResult["RAZDELS"])>0){?>
<div class="fltr">
	<select name="rzdel">
		<option value="">Все</option>
		<?foreach($arResult["RAZDELS"] as $R){?>
		<option value="<?=$R["ID"]?>" <? if($R["ID"]==$arParams["RAZDEL"]) echo 'selected="selected"';?>><?=$R["NAME"]?></option>
		<?}?>
	</select>
	
</div>
<?}?>
<a  class="light_button" href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?NEW_ARTICLE=Y" >+ Новая публикация</a>
<ul>
<?
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):?>
	<li class="blog-item">
				<p class="blog-top"><?if($arParams["NOT_SHOW_PARENT"]!="Y"){?><strong><?=$arSection["IBLOCK_SECTION_NAME"]?></strong>, <?}?><?=$arSection["DATE_CREATE"]?></p>
				<p class="blog-caption"><?=$arSection["NAME"]?></p>
				<p class="blog-text"><?=$arSection["DESCRIPTION"]?></p>
				
				<p class="blog-info">
					<span class="blog-comments">Комментарии: (<a href="#"><?=intval($arSection["UF_SECTION_COMM_CNT"])?></a>)</span>
					<span class="blog-tags">Теги: 
					<?if($arSection["TAGS"]!=""){?>
						<?
						$tar = explode(",", $arSection["TAGS"]);
						$i=0;
						?>
						<?foreach($tar as $t){?><a href="#"><?=$t?></a><?
						if($tar[$i+1]!="") echo ", ";
						$i++;
						?> 
						
						<?}?>
						
					<?}?>
					</span>
					<span class="blog-more"><a href="<?=$arSection["SECTION_PAGE_URL"]?>" target="_blank">Далее</a></span>
				</p>
				<ul class="blog-actions">
					<li class="blog-rborder"><a href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?SECTION_ID=<?=$arSection["ID"]?>">Редактировать</a></li>
					<li><a href="<?=$templateFolder?>/core.php?act=del_item&ID=<?=$arSection["ID"]?>" rel="nofollow" class="del_item">Удалить</a></li>	
					<li>
						<?if($arSection["ACTIVE"]=="Y"){?>
							<a href="<?=$templateFolder?>/core.php?act=active_item&ID=<?=$arSection["ID"]?>" rel="nofollow" class="active_item activate">Не показывать на сайте</a>
						<?}else{?>
							<a href="<?=$templateFolder?>/core.php?act=active_item&ID=<?=$arSection["ID"]?>" rel="nofollow" class="active_item activate">Показывать на сайте</a>
						<?}?>
					</li>					
		</ul>
	<?
	//v_dump($arSection);
	?>
	</li>
	
<?endforeach?>
</ul>
<div class="pager">
	<?=$arResult["NAV_STRING"]?>
</div>
</div>

 
<?

//v_dump($arResult["RAZDELS"]);
?>
