<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/
//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("auth_error");

function del_item(){
	global $DB;
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if(check_section_perm($_REQUEST["ID"])){
		echo "ok";
		$DB->StartTransaction();
		if(!CIBlockSection::Delete($_REQUEST["ID"])){
			$strWarning .= 'Error.';
			$DB->Rollback();
		}else
			$DB->Commit();
	}else echo "error";
}


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}


function active_item(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	if(!check_section_perm($_REQUEST["ID"])) die("Permission denied");
	
	
	$res = CIBlockSection::GetByID($_REQUEST["ID"]);
	if($ar_res = $res->GetNext()){
		$bs = new CIBlockSection;
			
		if($ar_res["ACTIVE"]=="Y"){
			$bs->Update($_REQUEST["ID"], Array("ACTIVE" =>"N"));
		}else{
			$bs->Update($_REQUEST["ID"], Array("ACTIVE" =>"Y"));
		}	
	}

}

if($_REQUEST["act"]=="del_item") del_item();
if($_REQUEST["act"]=="active_item") active_item();

?>
