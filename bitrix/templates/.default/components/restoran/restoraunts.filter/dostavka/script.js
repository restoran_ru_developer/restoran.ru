$(document).ready(function() {
    /* search input */
    // focus
    $(".search_block .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
    });
    // blur
    $(".search_block .placeholder").on("blur", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val().length <= 0)
            $(this).val(defVal);
    });

    // filter_button event click
    $('.filter_button').on('click', function() {
        var id = $(this).attr('filid');
        var filID = 'cuselMultiple-scroll-' + id;
        $('#' + filID + ' > span.cuselMultipleActive').each(function(index) {
            if($(this).text() && index == 0)
                $('.' + filID).empty();
            if(index <= 3)
                $('<li><a href="javascript:void(0)" class="white">' + $(this).text() + '</a></li>').appendTo('.' + filID);

            // set form hidden values
/*            var inpName = $('#' + id + '_sel_cont').attr('name');
            $('<input type="hidden" name="' + inpName + '[]" value="' + $(this).attr('value') + '" />').appendTo('#' + id + '_sel_cont');*/

        });
        $('.filter_popup').hide();
    });

    // XXX stupid hook
    // get restaurants type on search button
    $('form[name=rest_filter_form]').submit(function() {
/*        $('#cuselMultiple-scroll-multi2 > span.cuselMultipleActive').each(function(index) {
            var inpName = $('#multi2_sel_cont').attr('name');
            if($(this).attr('value').length > 0)
                $('<input type="hidden" name="' + inpName + '[]" value="' + $(this).attr('value') + '" />').appendTo('#multi2_sel_cont');
        });*/

        // check def search value
        var defVal = $(".search_block .placeholder").attr('defvalue');
        if($('input[name=q]').val() == defVal)
            $('input[name=q]').val('');
    });

    // send suggest
    $("#top_search_input").autocomplete("/search/index_suggest.php", {
        limit: 5,
        minChars: 3,
        formatItem: function(data, i, n, value) {
            return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        },
        extraParams: {
            search_in: function() { return $('#search_in').val(); },
            cuisine: function() {
                return $('input[name="cuisine[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            average_bill: function() {
                return $('input[name="average_bill[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            subway: function() {
                return $('input[name="subway[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            subway: function() {
                return $('#cuselMultiple-scroll-multi2 > span.cuselMultipleActive').map(function() {
                    return $(this).attr('value');
                }).get().join(',');
            }
        }
   	});

    // go to suggest result
    $("#top_search_input").result(function(event, data, formatted) {
        if (data) {
            location.href = value.split("###")[1];
        }
   	});

});