<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script>
    $(document).ready(function(){
   
    });
    function success_a(position) {
        link = "/bitrix/components/restoran/metro.list/ajax.php?q=near&template=checkbox&lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&<?=bitrix_sessid_get()?>";    
        $("#metro_list").load(link);                                                    
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success_a, error_a);
        } else {
            alert("Ваш браузер не поддерживает\nопределение местоположения");
            return false;
        }                            
    }
</script>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="title_main left"><?=GetMessage("WHAT_YOU_SEARCH_TEXT")?></div>  
<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
    <div class="search left">
        <div class="filter_box">
            <div class="left filter_block" id="filter4">
                <div class="title"><?=GetMessage("REST_TYPE_TITLE")?></div>
                <ul class="filter_category_block cuselMultiple-scroll-multi2">
                    <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                        <li><a href="javascript:void(0)" class="white"><?=$arResult["RESTAURANTS_TYPE"][$i]["NAME"]?></a></li>
                    <?endfor?>
                </ul>
                <div><a href="javascript:void(0)" class="filter_arrow js another">все типы</a></div>
                <script>
                    $(document).ready(function(){
                        $(".filter_popup_4").popup({"obj":"filter4","css":{"left":"-10px","top":"0px", "width":"315px"}});
                        var params = {
                            changedEl: "#multi2",
                            scrollArrows: true
                        }
                        cuSelMulti(params);
                    })
                </script>
                <div class="filter_popup filter_popup_4">
                    <div class="title"><?=GetMessage("REST_TYPE_TITLE")?></div>
                    <select multiple="multiple" class="asd" id="multi2" name="rest_type[]" size="10">
                        <?foreach($arResult["RESTAURANTS_TYPE"] as $restType):?>
                            <option value="<?=$restType["ID"]?>"><?=$restType["NAME"]?></option>
                        <?endforeach?>
                    </select>
                    <br /><br />
                    <div align="center"><input class="filter_button" filID="multi2" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                </div>
            </div>
            <div class="left filter_block" style="width:120px" id="filter3">
                <div class="title"><?=GetMessage("SUBWAY_TITLE")?></div>
                <!--<ul class="filter_category_block cuselMultiple-scroll-multi4">-->
                    <?/*for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                        <li><a href="javascript:void(0)" class="white"><?=$arResult["SUBWAY"][$i]["NAME"]?></a></li>
                    <?endfor*/?>                    
                <script>
                    var link = "";
                    function success(position) {
                        if (link != "/tpl/ajax/metro.php?q=near&lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&<?=bitrix_sessid_get()?>")
                        {
                            link = "/tpl/ajax/metro.php?q=near&lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&<?=bitrix_sessid_get()?>";
                            $(".filter_popup_3").css({'padding':'55px','padding-top':'0px','left':'0px','top':'165px', 'width':'962px','position':'absolute','z-index':10000,'background': '#3B414E'});
                            $(".filter_popup_3").load("/tpl/ajax/metro.php?q=near&lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&<?=bitrix_sessid_get()?>",function(){
                            $(".filter_popup_3").fadeIn(300);
                            });                                                    
                        }
                        else
                        {
                            $(".filter_popup_3").fadeIn(300);
                        }
                    }
                    function error()
                    {
                        alert("Невозможно определить местоположение");
                        return false;
                    }
                    function get_location()
                    {
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(success, error);
                        } else {
                            alert("Ваш браузер не поддерживает\nопределение местоположения");
                            return false;
                        }                            
                    }
                    function load_metro(obj)
                    {
                        if (link != $(obj).attr('href'))
                        {
                            link = $(obj).attr('href');
                            $(".filter_popup_3").css({'padding':'55px','padding-top':'0px','left':'0px','top':'165px', 'width':'962px','position':'absolute','z-index':10000,'background': '#3B414E'});
                            $(".filter_popup_3").load(link, function(){$(".filter_popup_3").fadeIn(300);});                                                      
                        }
                        else
                        {
                            $(".filter_popup_3").fadeIn(300);
                        }
                    }
                    $(document).ready(function(){
                        $(".filter_popup_3").click(function(e){                                
                            e.stopPropagation();
                            //return false;
                        });
                        $(".filter_category_block").find("a").click(function(e){
                            e.stopPropagation();
                            //return false;
                        });
                        $('html,body').click(function() {
                            $(".filter_popup_3").fadeOut(300);
                        });
                        $(window).keyup(function(event) {
                        if (event.keyCode == '27') {
                            $(".filter_popup_3").fadeOut(300);
                        }
                        });
                    });
                </script>
                    <ul class="filter_category_block">
                        <li><a href="/tpl/ajax/metro.php?q=abc&<?=bitrix_sessid_get()?>" class="white" onclick="load_metro(this); return false;">По алфавиту</a></li>
                        <li><a href="/tpl/ajax/metro.php?q=thread&<?=bitrix_sessid_get()?>" class="white" onclick="load_metro(this); return false;">По веткам</a></li>
                        <li><a id="near_metro" href="javascript:void(0)" class="ajax js another" onclick="get_location();">Ближайшие к вам</a></li>
                    </ul>                            
                <div class="filter_popup filter_popup_3 popup"></div>                    
                <?/*foreach($arResult["SUBWAY"] as $subway):?>
                    <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                <?endforeach*/?>
            </div>
            <div class="left filter_block" id="filter1">
                <div class="title"><?=GetMessage("CUISINE_TITLE")?></div>
                <ul class="filter_category_block cuselMultiple-scroll-multi3">
                    <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                        <li><a href="javascript:void(0)" class="white"><?=$arResult["CUISINES"][$i]["NAME"]?></a></li>
                    <?endfor?>
                </ul>
                <div><a href="javascript:void(0)" class="filter_arrow js another">все кухни</a></div>
                <script>
                    $(document).ready(function(){
                        $(".filter_popup_1").popup({"obj":"filter1","css":{"left":"-10px","top":"0px", "width":"315px"}});
                        var params = {
                            changedEl: "#multi3",
                            scrollArrows: true
                        }
                        cuSelMulti(params);
                    })
                </script>
                <div class="filter_popup filter_popup_1">
                    <div class="title"><?=GetMessage("CUISINE_TITLE")?></div>
                    <!--<div id="multi3_sel_cont" name="cuisine"></div>-->
                    <select multiple="multiple" class="asd" id="multi3" name="cuisine[]" size="10">
                        <?foreach($arResult["CUISINES"] as $cuisine):?>
                            <option value="<?=$cuisine["ID"]?>"><?=$cuisine["NAME"]?></option>
                        <?endforeach?>
                    </select>
                    <br /><br />
                    <div align="center"><input class="filter_button" filID="multi3" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                </div>
            </div>
            <div class="left filter_block end" id="filter2">
                <div class="title"><?=GetMessage("AVERAGE_BILL_TITLE")?></div>
                <ul class="filter_category_block cuselMultiple-scroll-multi1">
                    <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                        <li><a href="javascript:void(0)" class="white"><?=$arResult["AVERAGE_BILL"][$i]["NAME"]?></a></li>
                    <?endfor?>
                </ul>
                <div><a href="javascript:void(0)" class="filter_arrow js another">больше счет</a></div>
                <script>
                    $(document).ready(function(){
                        $(".filter_popup_2").popup({"obj":"filter2","css":{"left":"-10px","top":"0px", "width":"315px"}});
                        var params = {
                            changedEl: "#multi1",
                            scrollArrows: true
                        }
                        cuSelMulti(params);
                    })
                </script>
                <div class="filter_popup filter_popup_2">
                    <div class="title"><?=GetMessage("AVERAGE_BILL_TITLE")?></div>
                    <!--<div id="multi1_sel_cont" name="average_bill"></div>-->
                    <select multiple="multiple" class="asd" id="multi1" name="average_bill[]" size="10">
                        <?foreach($arResult["AVERAGE_BILL"] as $avBill):?>
                            <option value="<?=$avBill["ID"]?>"><?=$avBill["NAME"]?></option>
                        <?endforeach?>
                    </select>
                    <br /><br />
                    <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                </div>
            </div>                
            <div class="clear"></div>
        </div>
    </div>
    <div class="button right">
        <input style="width:168px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
        <?if($APPLICATION->GetCurDir() == "/" || substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/catalog/") || (substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/")&&!substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/afisha/"))):?>
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/include_areas/top_search_links.php",
                Array(),
                Array("MODE"=>"html")
            );?>
        <?endif;?>
    </div>  
</form>
<div class="clear"></div>
<?$APPLICATION->IncludeComponent(
            "bitrix:search.title",
            "main_search_suggest",
            Array(),
        false
    );?>
<div class="clear"></div>