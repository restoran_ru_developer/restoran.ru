<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script src="/tpl/js/jquery.checkbox.js"></script>
<script>
    $(document).ready(function(){
        $(".select_all").toggle(function(){
        setCheck($(this).parent().find(".niceCheck"));
        $(this).find("a").html("<?=GetMessage("UNSELECT_ALL")?>");
        },function(){
        unsetCheck($(this).parent().find(".niceCheck"));
        $(this).find("a").html("<?=GetMessage("SELECT_ALL")?>");
        });    
    });
    function success_a(position) {
        link = "/bitrix/components/restoran/metro.list/ajax.php?q=near&template=checkbox&lat="+position.coords.latitude+"&lon="+position.coords.longitude+"&<?=bitrix_sessid_get()?>";    
        $("#metro_list").load(link);                                                    
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success_a, error_a);
        } else {
            alert("Ваш браузер не поддерживает\nопределение местоположения");
            return false;
        }                            
    }
</script>
<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
    <input type="hidden" name="IBLOCK_SECTION_ID" value="<?=(int)$arParams['SECTION']?>" />
    <?//Restoran type?>
    <h2><?=GetMessage("REST_TYPE_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $rest_type_count = ceil(count($arResult["RESTAURANTS_TYPE"])/4);
        foreach($arResult["RESTAURANTS_TYPE"] as $rest_i=>$restType):
            if (($rest_i >= $rest_type_count && $rest_i%$rest_type_count == 0) || $rest_i == 0)
            {
                $aaa = ($rest_i>=($rest_type_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$restType["ID"].'" name="rest_type[]" id="type'.$restType["ID"].'" /></span></td><td class="option2"><label>'.$restType["NAME"].'</label></td>';       

            if ($rest_i%$rest_type_count == ($rest_type_count-1) && $rest_i>0) 
                echo '</table></div>';    
            if (end($arResult["RESTAURANTS_TYPE"]) == $restType && $rest_i%$rest_type_count != ($rest_type_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <?//Metro?>
    <h2><?=GetMessage("SUBWAY_TITLE")?></h2>
    <div class="extended_filter">
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>    
        <?/*
        $subway_count = ceil(count($arResult["SUBWAY"])/4);
        foreach($arResult["SUBWAY"] as $subw_i=>$subway):
            if (($subw_i >= $subway_count && $subw_i%$subway_count == 0) || $subw_i == 0)
                echo '<div class="left"><table cellpadding="0" cellspacing="0">';

            echo '<tr><td><span class="niceCheck"><input type="checkbox" value="'.$subway["ID"].'" name="rest_type[]" id="subw'.$subway["ID"].'" /></span></td><td class="option"><label>'.$subway["NAME"].'</label></td>';       

            if ($subw_i%$subway_count == ($subway_count-1) && $subw_i>0) 
                echo '</table></div>';    
            if (end($arResult["SUBWAY"]) == $subway && $subw_i%$subway_count != ($subway_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        */?>    
        <div id="metro_list">
        <?$APPLICATION->IncludeComponent(
                    "restoran:metro.list",
                    "checkbox",
                    Array(
                            "q" => 'abc',
                            "lat" => (double)$_REQUEST["lat"],
                            "lon" => (double)$_REQUEST["lon"],
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "3600"			
                    ),
                    false
            );?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <?//Zag type?>
    <h2><?=GetMessage("ZAG_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $zag_count = ceil(count($arResult["ZAG"])/4);
        foreach($arResult["ZAG"] as $zag_i=>$zag):
            if (($zag_i >= $zag_count && $zag_i%$zag_count == 0) || $zag_i == 0)
            {
                $aaa = ($zag_i>=($zag_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$zag["ID"].'" name="rest_type[]" id="type'.$zag["ID"].'" /></span></td><td class="option2"><label>'.$zag["NAME"].'</label></td>';       

            if ($zag_i%$zag_count == ($zag_count-1) && $zag_i>0) 
                echo '</table></div>';    
            if (end($arResult["ZAG"]) == $zag && ($zag_i%$zag_count) != ($zag_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <?//Cuisine type?>
    <h2><?=GetMessage("CUISINE_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $cuisine_count = ceil(count($arResult["CUISINES"])/4);
        foreach($arResult["CUISINES"] as $cuisine_i=>$cuisine):
            if (($cuisine_i >= $cuisine_count && $cuisine_i%$cuisine_count == 0) || $cuisine_i == 0)
            {
                $aaa = ($cuisine_i>=($cuisine_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$cuisine["ID"].'" name="rest_type[]" id="type'.$cuisine["ID"].'" /></span></td><td class="option2"><label>'.$cuisine["NAME"].'</label></td>';       

            if ($cuisine_i%$cuisine_count == ($cuisine_count-1) && $cuisine_i>0) 
                echo '</table></div>';    
            if (end($arResult["CUISINES"]) == $cuisine && $cuisine_i%$cuisine_count != ($cuisine_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>

    <div class="dotted"></div>
    <?//AVERAGE_BILL type?>
    <h2><?=GetMessage("AVERAGE_BILL_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $bill_count = ceil(count($arResult["AVERAGE_BILL"])/4);
        foreach($arResult["AVERAGE_BILL"] as $bill_i=>$bill):
            if (($bill_i >= $bill_count && $bill_i%$bill_count == 0) || $bill_i == 0)
            {
                $aaa = ($bill_i>=($bill_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$bill["ID"].'" name="rest_type[]" id="type'.$bill["ID"].'" /></span></td><td class="option2"><label>'.$bill["NAME"].'</label></td>';       

            if ($bill_i%$bill_count == ($bill_count-1) && $bill_i>0) 
                echo '</table></div>';    
            if (end($arResult["AVERAGE_BILL"]) == $bill && ($bill_i%$bill_count) != ($bill_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <?//CREDIT type?>
    <h2><?=GetMessage("CREDIT_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $credit_count = ceil(count($arResult["CREDIT"])/4);
        foreach($arResult["CREDIT"] as $credit_i=>$credit):
            if (($credit_i >= $credit_count && $credit_i%$credit_count == 0) || $credit_i == 0)
            {
                $aaa = ($credit_i>=($credit_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$credit["ID"].'" name="rest_type[]" id="type'.$credit["ID"].'" /></span></td><td class="option2"><label>'.$credit["NAME"].'</label></td>';       

            if ($credit_i%$credit_count == ($credit_count-1) && $credit_i>0) 
                echo '</table></div>';    
            if (end($arResult["CREDIT"]) == $credit && ($credit_i%$credit_count) != ($credit_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("IN_MENU_TITLE")?></h2>
    <div class="extended_filter">    

    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("CHILD_TITLE")?></h2>
    <div class="extended_filter">    

    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("OSOBENNOST_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $oso_count = ceil(count($arResult["OSOBENNOST"])/4);
        foreach($arResult["OSOBENNOST"] as $oso_i=>$oso):
            if (($oso_i >= $oso_count && $oso_i%$oso_count == 0) || $oso_i == 0)
            {
                $aaa = ($oso_i>=($oso_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$oso["ID"].'" name="rest_type[]" id="type'.$oso["ID"].'" /></span></td><td class="option2"><label>'.$oso["NAME"].'</label></td>';       

            if ($oso_i%$oso_count == ($oso_count-1) && $oso_i>0) 
                echo '</table></div>';    
            if (end($arResult["OSOBENNOST"]) == $oso && ($oso_i%$oso_count) != ($oso_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("ENTERTAIMENT_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $ent_count = ceil(count($arResult["ENTARTAIMENT"])/4);
        foreach($arResult["ENTARTAIMENT"] as $ent_i=>$ent):
            if (($ent_i >= $ent_count && $ent_i%$ent_count == 0) || $ent_i == 0)
            {
                $aaa = ($ent_i>=($ent_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$ent["ID"].'" name="rest_type[]" id="type'.$ent["ID"].'" /></span></td><td class="option2"><label>'.$ent["NAME"].'</label></td>';       

            if ($ent_i%$ent_count == ($ent_count-1) && $ent_i>0) 
                echo '</table></div>';    
            if (end($arResult["ENTARTAIMENT"]) == $ent && ($ent_i%$ent_count) != ($ent_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("IDEAL_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $ideal_count = ceil(count($arResult["IDEAL"])/4);
        foreach($arResult["IDEAL"] as $ideal_i=>$ideal):
            if (($ideal_i >= $ideal_count && $ideal_i%$ideal_count == 0) || $ideal_i == 0)
            {
                $aaa = ($ideal_i>=($ideal_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$ideal["ID"].'" name="rest_type[]" id="type'.$ideal["ID"].'" /></span></td><td class="option2"><label>'.$ideal["NAME"].'</label></td>';       

            if ($ideal_i%$ideal_count == ($ideal_count-1) && $ideal_i>0) 
                echo '</table></div>';    
            if (end($arResult["IDEAL"]) == $ideal && ($ideal_i%$ideal_count) != ($ideal_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("MUSIC_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $ideal_count = ceil(count($arResult["MUSIC"])/4);
        foreach($arResult["MUSIC"] as $ideal_i=>$ideal):
            if (($ideal_i >= $ideal_count && $ideal_i%$ideal_count == 0) || $ideal_i == 0)
            {
                $aaa = ($ideal_i>=($ideal_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$ideal["ID"].'" name="rest_type[]" id="type'.$ideal["ID"].'" /></span></td><td class="option2"><label>'.$ideal["NAME"].'</label></td>';       

            if ($ideal_i%$ideal_count == ($ideal_count-1) && $ideal_i>0) 
                echo '</table></div>';    
            if (end($arResult["MUSIC"]) == $ideal && ($ideal_i%$ideal_count) != ($ideal_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <h2><?=GetMessage("PARKOVKA_TITLE")?></h2>
    <div class="extended_filter">    
        <div class="select_all"><a href="javascript:void(0)" class="uppercase"><?=GetMessage("SELECT_ALL")?></a></div>
        <?
        $park_count = ceil(count($arResult["PARKOVKA"])/4);
        foreach($arResult["PARKOVKA"] as $park_i=>$park):
            if (($park_i >= $park_count && $park_i%$park_count == 0) || $park_i == 0)
            {
                $aaa = ($park_i>=($park_count*3))?"0px":"10px";
                echo '<div class="left" style="margin-right: '.$aaa.'"><table cellpadding="0" cellspacing="0">';
            }
            echo '<tr><td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck"><input type="checkbox" value="'.$park["ID"].'" name="rest_type[]" id="type'.$park["ID"].'" /></span></td><td class="option2"><label>'.$park["NAME"].'</label></td>';       

            if ($park_i%$park_count == ($park_count-1) && $park_i>0) 
                echo '</table></div>';    
            if (end($arResult["PARKOVKA"]) == $park && ($park_i%$park_count) != ($park_count-1)):    
                echo '</table></div>';
            endif;
        endforeach    
        ?>
    </div>
    <div class="clear"></div>
    <div class="dotted"></div>
    <div class="button right">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding: 5px 0px;padding-right:5px">
                    <span class="niceCheck"><input type="checkbox" value="1" name="wifi[]" /></span>
                </td>
                <td class="option2 font14"><label>Wi-fi</label></td>
            </tr>
            <tr>
                <td style="padding: 5px 0px;padding-right:5px">
                    <span class="niceCheck"><input type="checkbox" value="1" name="alltime[]" /></span>
                </td>
                <td class="option2 font14"><label>Круглосуточно</label></td>
            </tr>
        </table>
        <br />
        <input style="width:168px" class="light_button" type="submit" name="search_submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />            
    </div>          
</form>