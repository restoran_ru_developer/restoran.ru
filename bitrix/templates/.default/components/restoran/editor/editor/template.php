<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>


<?

$APPLICATION->AddHeadScript($templateFolder.'/jq_redactor/redactor.js');
$APPLICATION->AddHeadScript($templateFolder.'/uploaderObject.js');
$APPLICATION->AddHeadScript($templateFolder.'/chosen.jquery.js');
$APPLICATION->AddHeadScript($templateFolder.'/jQuery.fileinput.js');
$APPLICATION->AddHeadScript($templateFolder.'/jquery.form.js');

?>
<form id="editor_form" method="post" action="<?=$templateFolder?>/core.php" >
<?
if($arResult["SECTION"]["ID"]!="") echo '<input type="hidden" name="act" value="edit_section" />';
else echo '<input type="hidden" name="act" value="new_section" />';
?>

<div id="p_editor">
	<div class="left">
	
		<div id="add_modules">
			<div class="ttl">Добавить модуль</div>
			<ul>
			<?
			//строим список блоков
			foreach($arParams["CAN_ADD"] as $BL){
			?>
				<li class="block"><a href="<?=$templateFolder?>/get_block.php" id="<?=$BL["CODE"]?>"><?=$BL["NAME"]?></a></li>
			<?
			}
			?>
			</ul>
			<div class="help">
			Сначала заполните обязательные поля. Затем создайте запись, используя доступные модули. Например, к стандартным «дата и время проведения», «мето проведения» и «текстовое поле» мероприятия раздела «Афиша» можно добавить фотографии и видео, дать красивую ссылку на ресторан («Упомянуть ресторан»), добавить цитату с фотографией автора («Прямая речь»).  Дополнительне модули можно менять местами.
			</div>
		</div>
	
	</div>
	<div class="cntr">
	<div class="ttl">Описание</div>
	
	<input type="hidden" name="SECTION_ID" value="<?=$arResult["SECTION"]["ID"]?>" />
	<input type="hidden" name="IBLOCK_ID" value="<?=$arResult["SECTION"]["IBLOCK_ID"]?>" />
	<input type="hidden" name="TAG_SECTION" value="<?=$arParams["TAG_SECTION"]?>" />
	<?
	$APPLICATION->SetTitle("Редактирование: ".$arResult["SECTION"]["NAME"]);
	?>
	<ul id="req_inp">
		
		<?foreach($arParams["REQ"] as $R){?>
			<li>
				<?
				$val = "";
				$addr = explode("__", $R["VALUE_FROM"]);
				if(count($addr)==2) $val = $arResult[$addr[0]][$addr[1]];
				if(count($addr)==3) $val = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
				if($R["TYPE"]=="short_text"){
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/short_text_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"LEN"=>250,
                			"SHOW_CO"=>"Y",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="multi_select"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/multi_select_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="select"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/select_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="photo"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/photo.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				?>
			</li>
		<?}?>
	</ul>
	<?
	/////
	//Выводим элементы из раздела
	/////
	?>
	<ul id="block_list">
		<?
		$STEP=1;
		$SORT = 50;
		?>
		<?foreach($arResult["ELEMENTS"] as $EL){?>
			<?
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Тэги"){
				$tar_tags = explode(",", $EL["TAGS"]);
				$EL_TAGS = $EL["TAGS"];
				$ELEMENT_TAGS = ""; 	
				foreach($tar_tags as $T){
					$ELEMENT_TAGS.='<a href="#">'.$T.'</a>';
				}
				continue;
			} 
			
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="") continue;
			?>
		
		<li class="ui-state-default">
			<?
			//////Упомянание о ресторане
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Привязка к ресторану"){ 
				//Нужно получить список всех ресторанов
				//Потом включить кэширование
				$arFilter = Array('ACTIVE'=>'Y', "IBLOCK_TYPE"=>"catalog");
				
				if($EL["PROPERTIES"]["REST_BIND"]["LINK_IBLOCK_ID"]>0) $arFilter["IBLOCK_ID"]=$EL["PROPERTIES"]["REST_BIND"]["LINK_IBLOCK_ID"];
				
  				$db_list = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false);
  				$VALUES =array(); 
  				while($ar_result = $db_list->GetNext()){
  					$VALUES[]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"]);
				}				
				//v_dump($EL["PROPERTIES"]["REST_BIND"]);
            	$APPLICATION->IncludeFile(
			 		$templateFolder."/blocks/restoran.php",
					Array(
						"BLOCK_TITTLE"=>"Упомянание о ресторане",
       			        "BLOCK_VALUE" =>array(
        	        	array("NAME"=>"bid__".$EL["ID"]."__DETAIL_TEXT", "VALUE"=>$EL["DETAIL_TEXT"]),
    	            	array("NAME"=>"bid__".$EL["ID"]."__REST_BIND", "VALUE"=>$EL["PROPERTIES"]["REST_BIND"]["VALUE"],"VALUES"=>$VALUES),
    	            	array("NAME"=>"bid__".$EL["ID"]."__BLOCK_TYPE", "VALUE"=>$EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE_ENUM_ID"])
	                ),
	                "ELEMENT_ID" => $EL["ID"],
	                "SORT"=>$EL["SORT"],
	                "SELECT_TITLE"=>"Выберите ресторан"
					),
					Array("MODE"=>"php")
        		);
			}
			
			
			//////ПРЯМАЯ РЕЧЬ
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Прямая речь"){ 
            	$APPLICATION->IncludeFile(
			 		$templateFolder."/blocks/pr_rech.php",
					Array(
						"BLOCK_TITTLE"=>"Прямая речь",
       			        "BLOCK_VALUE" =>array(
        	        	array("NAME"=>"bid__".$EL["ID"]."__RESPONDENT_NAME", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_NAME"]["VALUE"]),
    	            	array("NAME"=>"bid__".$EL["ID"]."__RESPONDENT_POST", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_POST"]["VALUE"]),
    	            	array("NAME"=>"bid__".$EL["ID"]."__RESPONDENT_SPEECH__TEXT", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_SPEECH"]["VALUE"]["TEXT"]),
    	            	array("NAME"=>"bid__".$EL["ID"]."__RESPONDENT_PHOTO", "VALUE"=>$EL["PROPERTIES"]["RESPONDENT_PHOTO"]["VALUE"]),
    	            	array("NAME"=>"bid__".$EL["ID"]."__BLOCK_TYPE", "VALUE"=>$EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE_ENUM_ID"])
	                ),
           		    "SHOW_CO"=>"Y",
               		"LEN"=>250,
	                "ELEMENT_ID" => $EL["ID"],
	                "SORT"=>$EL["SORT"]
					),
					Array("MODE"=>"php")
        		);
			}
			
			/////ВИЗУАЛЬНЫЙ РЕДАКТОР
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Визуальный редактор"){
				$EL["DETAIL_TEXT"] = str_replace("<br />", "", $EL["DETAIL_TEXT"]);
				$APPLICATION->IncludeFile(
					$templateFolder."/blocks/vis_red.php",
					Array(
						"BLOCK_TITTLE"=>"Вставка текста",
        	        	"BLOCK_VALUE" =>array(
            	    		array("NAME"=>"bid__".$EL["ID"]."__NAME", "VALUE"=>$EL["NAME"]),
                			array("NAME"=>"bid__".$EL["ID"]."__DETAIL_TEXT", "VALUE"=>$EL["DETAIL_TEXT"]),
                			array("NAME"=>"bid__".$EL["ID"]."__BLOCK_TYPE", "VALUE"=>$EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE_ENUM_ID"])
                		),
                		"ELEMENT_ID" => $EL["ID"],
                		"SORT"=>$EL["SORT"]	
					),
					Array("MODE"=>"php")
        		);
			}
			
			
			////ШАГ
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Шаг"){
				$APPLICATION->IncludeFile(
					$templateFolder."/blocks/shag_block.php",
					Array(
						"BLOCK_TITTLE"=>"Шаг",
						"STEP"=>$STEP,
        	        	"BLOCK_VALUE" =>array(
            	    		array("NAME"=>"bid__".$EL["ID"]."__DETAIL_TEXT", "VALUE"=>$EL["DETAIL_TEXT"]),
            	    		array("NAME"=>"bid__".$EL["ID"]."__DETAIL_PICTURE", "VALUE"=>$EL["DETAIL_PICTURE"]),
            	    		array("NAME"=>"bid__".$EL["ID"]."__BLOCK_TYPE", "VALUE"=>$EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE_ENUM_ID"])
                		),
                		"ELEMENT_ID" => $EL["ID"],
                		"SORT"=>$EL["SORT"]
					),
					Array("MODE"=>"php")
        		);
        		$STEP++;
			}
			
			//ФОТОГАЛЕРЕЯ
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Фотогалерея"){
				//var_dump($EL["PROPERTIES"]["PICTURES"]);
				$APPLICATION->IncludeFile(
					$templateFolder."/blocks/photos.php",
					Array(
						"BLOCK_TITTLE"=>"Фотогалерея",
        	        	"BLOCK_VALUE" =>array(
                			array("NAME"=>"bid__".$EL["ID"]."__PICTURES", "VALUE"=>$EL["PROPERTIES"]["PICTURES"]["VALUE"]),
                			array("NAME"=>"bid__".$EL["ID"]."__PICTURES_DESCR", "VALUE"=>$EL["PROPERTIES"]["PICTURES"]["DESCRIPTION"]),
                			array("NAME"=>"bid__".$EL["ID"]."__BLOCK_TYPE", "VALUE"=>$EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE_ENUM_ID"])
                		),
                		"ELEMENT_ID" => $EL["ID"],
                		"SORT"=>$EL["SORT"]
					),
					Array("MODE"=>"php")
        		);
			}
			
			
			//НЕДЛИННЫЙ ТЕКСТ
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Недлинный текст"){
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/short_text_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>"Недлинный текст",
                   		 	"BLOCK_NAME"=>"bid__".$EL["ID"]."__PREVIEW_TEXT",
                    		"BLOCK_VALUE" =>$EL["PREVIEW_TEXT"],
                    		"LEN"=>250,
                			"SHOW_CO"=>"Y",
                			"ELEMENT_ID" => $EL["ID"],
                			"SORT"=>$EL["SORT"]
						),
						Array("MODE"=>"php")
					);
			}
			
			
			if($EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Видео"){
				$APPLICATION->IncludeFile(
					$templateFolder."/blocks/video_block.php",
					Array(
						"BLOCK_TITTLE"=>"Видео",
						"STEP"=>$STEP,
        	        	"BLOCK_VALUE" =>array(
            	    		array("NAME"=>"bid__".$EL["ID"]."__video", "VALUE"=>$EL["PROPERTIES"]["video"]["VALUE"]),
            	    		array("NAME"=>"bid__".$EL["ID"]."__BLOCK_TYPE", "VALUE"=>$EL["PROPERTIES"]["BLOCK_TYPE"]["VALUE_ENUM_ID"])
                		),
                		"ELEMENT_ID" => $EL["ID"],
                		"SORT"=>$EL["SORT"]
					),
					Array("MODE"=>"php")
        		);
        		$STEP++;
			}
			
			
			$SORT +=50;
			?>
		</li>
		<?}?>
	</ul>
	
	<?
	//var_dump($arResult["SECTION"]);
	?>
	
	<div class="buts">
		<div class="lb">
			<a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>" id="predprosmotr" target="_blank">Сохранить и посмотреть</a>
		</div>
		<div class="rb">
			<a href="#" id="public">Сохранить</a>
		</div>
		
	</div>
	
	<div class="comments">
	Опубликованную запись вы сможете найти и редактировать<br/>
в разделе «<a href="#">Блог</a>» личного кабинета
	</div>
	
	</div>
	<div class="right">
		
		<div id="choose_tags">
			<div class="ttl">Теги</div>
			<div class="ln">
				<?
				global $USER;
				$arGroups = $USER->GetUserGroupArray();
				foreach($arGroups as $v) $GRs[]=$v;
				?>
				<input name="new_tag" class="pole" id="new_tag"/> <?if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){?><a href="#" class="add_new_tag"></a><?}?>
				<div class="all_tags_lis"></div>
			</div>
			
			<div class="tag_list"><?=$ELEMENT_TAGS?></div>
		
			<div class="sub_t">Тэги раздела:</div>
			<div class="pop">
				<?
				
				?>
				<?foreach($arResult["TAGS"] as $T){?>
					<?if(substr_count($EL_TAGS, trim($T))==0){?> 
						<a href="#"><?=trim($T)?></a>
					<?}?>
				<?}?>
			</div>
			
			<?
			//<div class="all_tags"><a href="#">все теги</a></div>
			?>
		</div>
	
	</div>
	<div class="clear"></div>
</div>
</form>


<?
//v_dump($arResult);
?>