<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/
//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	//var_dump($GRs);

	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_section(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if(!check_section_perm($_REQUEST["SECTION_ID"])) die("Permission denied");
	
	//Сначала апдейтим секцию(название и родителя)
	$bs = new CIBlockSection;
	$arFields = Array("ACTIVE" => "Y", "NAME" => $_REQUEST["SECTION_NAME"], "CODE" => translitIt($_REQUEST["SECTION_NAME"]));
	
	if($_REQUEST["IBLOCK_SECTION_ID"]!="") $arFields["IBLOCK_SECTION_ID"] = $_REQUEST["IBLOCK_SECTION_ID"];
	else $arFields["IBLOCK_SECTION_ID"]=false;
	if($_REQUEST["SECTION_DESCRIPTION"]!="") $arFields["DESCRIPTION"] = $_REQUEST["SECTION_DESCRIPTION"];
	
	if($_FILES["SECTION_PICTURE"]){
		$arFields["PICTURE"]=$_FILES["SECTION_PICTURE"];
	}
	
	foreach($_REQUEST as $key => $val){
		if(substr_count($key, "UF_")>0){
			$arFields[$key]=$val;
		}
	}
	
	//var_dump($arFields);
	$bs->Update($_REQUEST["SECTION_ID"], $arFields, false);
	CIBlockSection::ReSort($_REQUEST["IBLOCK_ID"]);
	////////

	
	//теперь начинается самое интересное!
	//теперь нужно проапдейтить все элементы этой секции
	
	//разбиваем запрос на элементы и их свойства чтобы все было красиво
	$ELEMENTS=array();
	foreach($_REQUEST as $key => $val){
		$tar = explode("__", $key);
		//для обычного свойства
		if(count($tar)==3){
			$ELEMENTS[$tar[1]][$tar[2]]=$val;
		}
		
		//для больших текстовых полей
		if(count($tar)==4){
			$ELEMENTS[$tar[1]][$tar[2]]= array("VALUE"=>array("TYPE"=>$tar[3], "TEXT" =>$val));
		}
	}
	
	
	
	//обновляем свойства элементов
	foreach($ELEMENTS as $EL_ID=>$EL){
		
		$el = new CIBlockElement;
		
		$PROP = array();
		foreach($EL as $PR_CODE=>$PR_VAL){
			if($PR_CODE=="DETAIL_TEXT" || $PR_CODE=="NAME" || $PR_CODE=="PICTURES_DESCR" || $PR_CODE=="PICTURES_comment" || $PR_CODE=="SORT") continue;
			$PROP[$PR_CODE]=$PR_VAL;
		}
		
		unset($PROP["PICTURES"]);
		
		if($_FILES["new_file_bid__".$EL_ID."__video"]){
			$PROP["video"] = Array(
        		"VALUE" => Array (
           		 	"FILE" => $_FILES["new_file_bid__".$EL_ID."__video"],
            		"WIDTH" => 400,
            		"HEIGHT" => 300,
            		"TITLE" => "Заголовок видео",
            		"DURATION" => "00:30",
            		"AUTHOR" => "Автор видео",
            		"DATE" => "01.02.2011"
        		)
			);
		
		}else unset($PROP["video"]);
		
		
		
		
		//смотрим, не сменилась ли фотография в интервью
		if($EL["RESPONDENT_PHOTO_changed"]=="Y"){
			if ($_FILES && $_FILES["bid__".$EL_ID."__RESPONDENT_PHOTO"]){	
				$PROP["RESPONDENT_PHOTO"]=Array(
	    			"name" => $_FILES["bid__".$EL_ID."__RESPONDENT_PHOTO"]["name"],
	    			"size" => @filesize($_FILES["bid__".$EL_ID."__RESPONDENT_PHOTO"]["tmp_name"]),
	    			"tmp_name" => $_FILES["bid__".$EL_ID."__RESPONDENT_PHOTO"]["tmp_name"],
	    			"type" => $_FILES["bid__".$EL_ID."__RESPONDENT_PHOTO"]["type"],
    				"old_file" => "",
    				"del" => "N",
	 	   			"MODULE_ID" => "main"
	 	   		);	
			}	
		}
		
		
		
		$arLoadProductArray = Array("MODIFIED_BY"=> $USER->GetID(), "PROPERTY_VALUES"=> $PROP);
//		$arLoadProductArray[]
		
		if(count($PROP)==1 && isset($PROP["BLOCK_TYPE"])) unset($arLoadProductArray["PROPERTY_VALUES"]);
		
		if($EL["NAME"]!="") $arLoadProductArray["NAME"]=$EL["NAME"];
		if($EL["DETAIL_TEXT"]!="") {
			$arLoadProductArray["DETAIL_TEXT"]=$EL["DETAIL_TEXT"];
			$arLoadProductArray["DETAIL_TEXT_TYPE"]="html";
		}
		if($EL["DETAIL_PICTURE_changed"]=="Y"){
			if ($_FILES && $_FILES["bid__".$EL_ID."__DETAIL_PICTURE"]){	
				$arLoadProductArray["DETAIL_PICTURE"]=Array(
	    			"name" => $_FILES["bid__".$EL_ID."__DETAIL_PICTURE"]["name"],
	    			"size" => @filesize($_FILES["bid__".$EL_ID."__DETAIL_PICTURE"]["tmp_name"]),
	    			"tmp_name" => $_FILES["bid__".$EL_ID."__DETAIL_PICTURE"]["tmp_name"],
	    			"type" => $_FILES["bid__".$EL_ID."__DETAIL_PICTURE"]["type"],
    				"old_file" => "",
    				"del" => "N",
	 	   			"MODULE_ID" => "main"
	 	   		);	
			}	
		}
		
		//var_dump($arLoadProductArray);
		
		$res = $el->Update($EL_ID, $arLoadProductArray);
	
		
		if(count($EL["PICTURES"])>0 || $_FILES["new_file_bid__".$EL_ID."__PICTURES"]){
			//$PROP["PICTURES"]=array();
			//var_dump($EL["PICTURES"]);
			
			$i=0;
			foreach($EL["PICTURES"] as $fid){
				$F = CFile::GetFileArray($fid);
				$F["DESCRIPTION"] = $EL["PICTURES_DESCR"][$i];
				$PICTURES[] = array("VALUE"=>CFile::MakeFileArray($fid), "DESCRIPTION"=>$EL["PICTURES_DESCR"][$i]);
				$i++;
				//echo $fid;
			}
			
			
			//Нужно посмотреть, может есть новые файлы
			if($_FILES && $_FILES["new_file_bid__".$EL_ID."__PICTURES"]){
				
				
				
				for($i=0;$i<count($_FILES["new_file_bid__".$EL_ID."__PICTURES"]["tmp_name"]);$i++){
					$UP_FILES=array(
						"name" => $_FILES["new_file_bid__".$EL_ID."__PICTURES"]["name"][$i],
		    			"size" => $_FILES["new_file_bid__".$EL_ID."__PICTURES"]["size"][$i],
    					"tmp_name" => $_FILES["new_file_bid__".$EL_ID."__PICTURES"]["tmp_name"][$i],
    					"type"=>$_FILES["new_file_bid__".$EL_ID."__PICTURES"]["type"][$i]
					);
					
					
					$PICTURES[] = array("VALUE"=>$UP_FILES, "DESCRIPTION"=>$EL["PICTURES_comment"][$i]);	
				}
			}
			var_dump($PICTURES);
			CIBlockElement::SetPropertyValuesEx($EL_ID, $_REQUEST["IBLOCK_ID"], array("PICTURES" => $PICTURES));
		}
		
	
	}
	
	

}



function update_element_tags(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if(!check_section_perm($_REQUEST["SECTION_ID"])) die("Permission denied");
		
	$TAGS="";
	$tar = tags_prepare($_REQUEST["tags"], "s1");
	$i=1;
	foreach($tar as $t){
		$TAGS.= $t;
		if($i<count($tar)) $TAGS.=",";
		$i++;
	}
	
	$el = new CIBlockElement;
	$arLoadProductArray["TAGS"]=$TAGS;
		
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["SECTION_ID"], "PROPERTY_BLOCK_TYPE_VALUE"=>"Тэги","IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"]);
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	if($ar_fields = $res->GetNext()){

		$el->Update($ar_fields["ID"], $arLoadProductArray);
		
	}else{
		$arLoadProductArray["NAME"]="Тэги";
		$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["SECTION_ID"];
		$arLoadProductArray["IBLOCK_ID"]=$_REQUEST["IBLOCK_ID"];
		$arLoadProductArray["ACTIVE"]="Y";
		if($BL_ID = $el->Add($arLoadProductArray)){
			$property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$_REQUEST["IBLOCK_ID"], "CODE"=>"BLOCK_TYPE", "VALUE"=>"Тэги"));
			if($enum_fields = $property_enums->GetNext()) $PID = $enum_fields["ID"];
		
			CIBlockElement::SetPropertyValuesEx($BL_ID, $_REQUEST["IBLOCK_ID"], array("BLOCK_TYPE" => array("VALUE"=>$PID)));
		}else{
			 echo "Error: ".$el->LAST_ERROR;
		}
	}
}

function get_tag_list(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}	
	
	$tar = tags_prepare($_REQUEST["tags"], "s1");
	//var_dump($tar);
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["TAG_SECTION"], "IBLOCK_ID"=>99, "NAME"=>"%".$_REQUEST["tag"]."%");
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	while($ar_fields = $res->GetNext()){
		if(!in_array($ar_fields["NAME"], $tar)) echo '<a href="#">'.$ar_fields["NAME"].'</a>';
	}

	
}


function new_tag(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}	
	
	if(!check_section_perm($_REQUEST["TAG_SECTION"])) die("Permission denied");
	
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["TAG_SECTION"], "IBLOCK_ID"=>99, "NAME"=>$_REQUEST["new_tag"]);
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	if($ar_fields = $res->GetNext()){
		
	}else{
		$el = new CIBlockElement;
		$arLoadProductArray["NAME"]=$_REQUEST["new_tag"];
		$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["TAG_SECTION"];
		$arLoadProductArray["IBLOCK_ID"]=99;
		$arLoadProductArray["ACTIVE"]="Y";
		$el->Add($arLoadProductArray);
	}
}


function del_block(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$ELEMENT_ID = str_replace("#", "", $_REQUEST["ELEMENT_ID"]);

	if(!check_element_perm($ELEMENT_ID)) die("Permission denied");
	
	CIBlockElement::Delete($ELEMENT_ID);
}


function sort_update(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	$el = new CIBlockElement;
	
	$ELEMENTS=array();
	foreach($_REQUEST as $key => $val){
		$tar = explode("__", $key);
		//для обычного свойства
		if(count($tar)==3 && $tar[1]!=""){
			//$ELEMENTS[$tar[1]][$tar[2]]=$val;
			$arLoadProductArray["SORT"]=$val;
			if(check_element_perm($tar[1]))	$el->Update($tar[1], $arLoadProductArray);	
		}
	}
}

/*
function new_article(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	$bs = new CIBlockSection;
	$arFields = Array(
  		"ACTIVE" => "N",
  		"IBLOCK_SECTION_ID" => $IBLOCK_SECTION_ID,
  		"IBLOCK_ID" => $_REQUEST["IBLOCK_ID"],
  		"NAME" => "",
  	);

}
*/

if($_REQUEST["act"]=="sort_update") sort_update();
if($_REQUEST["act"]=="get_tag_list") get_tag_list();
if($_REQUEST["act"]=="edit_section") edit_section();
if($_REQUEST["act"]=="update_element_tags") update_element_tags();
if($_REQUEST["act"]=="new_tag") new_tag();
if($_REQUEST["act"]=="del_block") del_block();
if($_REQUEST["act"]=="new_article") new_article();
?>
