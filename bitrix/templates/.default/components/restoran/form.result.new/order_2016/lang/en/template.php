<?
$MESS["FORM_REQUIRED_FIELDS"] = "Required fields";
$MESS["FORM_NOT_REQUIRED_FIELDS"] = "not necessarily";
$MESS["FORM_APPLY"] = "Apply";
$MESS["FORM_ADD"] = "Add";
$MESS["FORM_ACCESS_DENIED"] = "Web-form access denied.";
$MESS["FORM_DATA_SAVED1"] = "Thank you. Your application form #";
$MESS["FORM_DATA_SAVED2"] = " was received.";
$MESS["FORM_MODULE_NOT_INSTALLED"] = "Web-form module is not installed.";
$MESS["FORM_NOT_FOUND"] = "Web-form is not found.";
$MESS["I_WANT"] = "I want ";
$MESS["IN_RESTORAN"] = "at a restaurant";
$MESS["NA"] = "Date";
$MESS["AT"] = "time";
$MESS["OUR_COMPANY"] = "Our company consists of";
$MESS["PEOPLE"] = "people";
$MESS["HOW_MUCH_BUY"] = "we plan to spend";
$MESS["BY_PERSON"] = "rub. per person";
$MESS["MY_NAME"] = "My name is";
$MESS["FOR_FEEDBACK"] = "For feedback I leave";
$MESS["MY_PHONE"] = "Phone:";
$MESS["MY_EMAIL"] = "E-mail:";
$MESS["MY_WISH"] = "Your special instructions";
$MESS["CAPTCHA"] = "I confirm that I am not a bot, the picture reads:";
$MESS["NON_SMOKING_HALL"] = "Non-smoking hall";
$MESS["SMOKING_HALL"] = "Smoking hall";
$MESS["YES"] = "Yes";
$MESS["NO"] = "No";
$MESS["RESERVE"] = "Reserve";
$MESS["RESERVE_TABLE"] = "Reserve";
$MESS["RESERVE_BANKET"] = "Reserve";
$MESS["SPEAK_ENGLISH"] = "Make restaurant reservations - we speak english";
$MESS["BRONE_TXT_spb"] = "book <span> free </ span>: +7 (812) 740-18-20";
$MESS["BRONE_TXT_msk"] = "book <span> free </ span>: +7 (495) 988-26-56";
$MESS["BRONE_TXT_KLD"] = "© Restoran.ru. <span> service <b> completely free </ b>. </ span> <Br /> <span class = 'phone'> You can also book by phone: <Br /> +7 (952) 110-75- 55 </ span>";
$MESS["BRONE_TXT_UFA"] = "© Restoran.ru. <span> Service <b> completely free </ b>. </ span> <Br /> <span class = 'phone'> You can also book by phone: <Br /> +7 (964) 955-55- 00 </ span>";
$MESS["BRONE_TXT_"] = "© Restoran.ru. <span> Service <b> completely free </ b>. </ span> <Br /> <span class = 'phone'> You can also book by phone: <Br /> +7 (495) 988-26- 56 </ span>";
$MESS["F_PER"] = "rub/person";
$MESS["F_ALC"] = "BYOB";
$MESS["F_EVENT"] = "Event-service";
$MESS["F_BUDGET"] = "Budget";
$MESS["CHEK_TIME"] = "Time";
$MESS["CHEK_PERSONS"] = "Persons";
$MESS["SERVICE_FREE"] = "This service is <b>free</b>";
$MESS["REST_NAME"] = "Restaurant name";
$MESS["BRONE_TXT_SPB"] = "<span class='phone'>+7 (812) 740-18-20</span>";
$MESS["BRONE_TXT_MSK"] = "<span class='phone'> +7 (495) 988-26-56</span>";


$MESS["CALLBACK_thanks_text"] = "Thank you. Contact with you just a couple of minutes.";
$MESS["CALLBACK_no_want_fill"] = "Do not want to perform?";
$MESS["CALLBACK_we_will_make"] = "We are happy to do it for you. <span> And reserve a restaurant, of course, for free!</span><br>
                        Booking confirmation by SMS in 5 minutes.";
$MESS["CALLBACK_bron_service"] = "Reservation service:";
$MESS["CALLBACK_our_phone_text"] = "<span>Your phone</span><br>
                            for feedback";
$MESS["CALLBACK_call_me"] = "call back";
$MESS["CALLBACK_enter_phone_num"] = "Enter your phone number";

$MESS["we-will-get-you"] = "Contact with you just in 5 minutes.";
?>