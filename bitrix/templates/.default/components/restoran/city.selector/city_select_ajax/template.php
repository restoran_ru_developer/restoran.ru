<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    <br /><br />
</div>
<p class="center"><?=GetMessage("WRONG_CITY")?></p>
<?
$cities = "";
foreach($arResult["ITEMS"] as $arItem):
    if($arItem["CODE"]!='spb'&&$arItem["CODE"]!='msk'&&$arItem["CODE"]!='rga'){
        continue;
    }
//    $add_class = '';
//    if($arItem["CODE"]=='ast'||$arItem["CODE"]=='ufa'||$arItem["CODE"]=='krd'||$arItem["CODE"]=="tmn"||$arItem["CODE"]=="kld"||$arItem["CODE"]=="nsk"||$arItem["CODE"]=="sch"){
//        $add_class = 'dont-show';
//    }
    if (CITY_ID!=$arItem["CODE"]):
        if ($arItem["CODE"]=="msk"){
            if (SITE_ID=="s2")
                $cities .= '<li><a href="https://en.restoran.ru/msk/">'.$arItem["NAME"].'</a></li>';
            else
                $cities .= '<li><a href="https://www.restoran.ru/?CITY_ID=msk" rel="nofollow">'.$arItem["NAME"].'</a></li>';
        }
        elseif ($arItem["CODE"]=="spb")
        {
            if (SITE_ID=="s2")
                $cities .= '<li><a href="https://en.restoran.ru/spb/">'.$arItem["NAME"].'</a></li>';
            else
                $cities .= '<li><a href="https://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
        }
        elseif ($arItem["CODE"]=="urm")
        {
            if (SITE_ID=="s2")
                $cities .= '<li><a href="https://en.restoran.ru/urm/">'.$arItem["NAME"].'</a></li>';
            else
                $cities .= '<li><a href="https://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="tln") {}
        elseif($arItem["CODE"]=="amt")
        {
            //$cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        }
        elseif($arItem["CODE"]=="vrn")
        {
            //$cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
        }
        else{
            if (SITE_ID=="s2"){
                $cities .= '<li class="'.$add_class.'"><a href="http://en.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
            }
            else {
                $cities .= '<li class="'.$add_class.'"><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
            }
        }
    else:
        $city = $arItem["NAME"];
    endif;
endforeach;
?>
<div style="width:175px; margin:0 auto;">
<div class="title" style="text-align: center"><?=$city?></div>
<ul>
    <?=$cities?>
</ul>
</div>