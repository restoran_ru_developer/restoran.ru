<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="city_select_block" class="popup" style="display:none">
    <?$cities = "";?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if (CITY_ID!=$arItem["CODE"]):
            /*if (substr_count($APPLICATION->GetCurPageParam(),CITY_ID)&&!substr_count($APPLICATION->GetCurPageParam(),"detailed")):
                $arItem["CODE"] = str_replace(CITY_ID,$arItem["CODE"],$APPLICATION->GetCurPageParam());
                if ($arItem["CODE"]=="spb"||$arItem["CODE"]=="msk")
                    $cities .= '<li><a href="'.$arItem["CODE"].'">'.$arItem["NAME"].'</a></li>';
                else
                    $cities .= '<li><a href="'.$arItem["CODE"].'">'.$arItem["NAME"].'</a></li>';
            else:*/
                if ($arItem["CODE"]=="msk")
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                elseif ($arItem["CODE"]=="spb")
                {
                    if (SITE_ID=="s2")
                        $cities .= '<li><a href="http://en.restoran.ru/spb/">'.$arItem["NAME"].'</a></li>';
                    else
                        $cities .= '<li><a href="http://'.$arItem["CODE"].'.restoran.ru">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="ast")
                {
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="tln")
                {
                    //$cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="ufa")
                {
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="kld")
                {
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="amt"&&CSite::InGroup(Array(1,23)))
                {
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="vrn"&&CSite::InGroup(Array(1)))
                {
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
                }
                elseif($arItem["CODE"]=="ast"||$arItem["CODE"]=="amt"||$arItem["CODE"]=="vrn")
                {
                    
                }
//                elseif($arItem["CODE"]=="rga"||$arItem["CODE"]=="urm")
//                {
//                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].' <sup>New</sup></a></li>';
//                }
                else
                    $cities .= '<li><a href="http://www.restoran.ru/'.$arItem["CODE"].'/">'.$arItem["NAME"].'</a></li>';
            //endif;
        else:
            $city = $arItem["NAME"];
        endif;?>
    <?endforeach;?>
    <?if (SITE_ID=="s2")
    {
        $cities = str_replace("www.","en.",$cities);
    }?>
    <div class="title"><?=$city?></div>
    <ul>
        <?=$cities?>
    </ul>
</div>
<ul class="city_select">
    <li id="city_select"><a href="javascript:void(0)"><?=$city?></a></li>
</ul>
