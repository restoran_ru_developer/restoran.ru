var sgo = {};
$(document).ready(function(){


function getBodyScrollTop(){
	return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}

$(window).scroll(function(){
	var glob_top = $('#editor_form').offset().top;
	var gto = getBodyScrollTop();
		
	if(gto>glob_top) {
		gto = gto - glob_top+10;			
	}else{
		gto =0;
	}
	//alert($("#p_editor").height()-getBodyScrollTop());
	var pedit = $("#p_editor").height()+$("#p_editor").offset().top-getBodyScrollTop();
	
	if(pedit>$('#add_modules').height()){
		$('#add_modules').stop().animate({'top':gto},500);
		$('#choose_tags').stop().animate({'top':gto},500);
	}
	
});
	

$.tools.dateinput.localize("ru",  { months: 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря', 
                            shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
                            days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
                            shortDays:     'вс,пн,вт,ср,чт,пт,сб'});
    $("input[name=ACTIVE_FROM]").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});      


///ТЭГИ
$("#choose_tags .tag_list").on("click", "a", function(event){
	$("#choose_tags .pop").append($(this));
	/*
	$(this).hide("fast", function(){
		$(this).remove();
		
	});
	*/
	update_element_tags();
	return false
});

$("#choose_tags .pop").on("click", "a", function(event){
	$("#choose_tags .tag_list").append($(this));
	
	update_element_tags();
		
	return false
});



function update_element_tags(){
	var element_id = $("input[name=ELEMENT_ID]").val();
	var iblock_id = $("input[name=IBLOCK_ID]").val();
	var tags = "";
	var link = $("#editor_form").attr("action");
	
	
	$("#choose_tags .tag_list a").each(function(){
		tags+=$(this).html()+",";
	});
	
	$.post(link,{ELEMENT_ID: element_id, tags: tags, act:"update_element_tags", IBLOCK_ID:iblock_id},function(otvet){
		
	});
}


$("#new_tag").keyup(function(){
	if($(this).val().length>2){
		$("#choose_tags .add_new_tag").show();
		var link = $("#editor_form").attr("action");
		var tag_section = $("input[name=TAG_SECTION]").val();
		var iblock_id = $("input[name=IBLOCK_ID]").val();
		var tags = "";
		
		$("#choose_tags .tag_list a").each(function(){
			tags+=$(this).html()+",";
		});
	
		$.post(link,{TAG_SECTION: tag_section, tag: $(this).val(), act:"get_tag_list", IBLOCK_ID:iblock_id, tags: tags},function(otvet){
			$("#choose_tags .all_tags_lis").html(otvet);
			$("#choose_tags .all_tags_lis").slideDown("fast");
			
			
		});
	}else{
		$("#choose_tags .add_new_tag").hide();
	}
});

$("body").click(function(){
	$("#choose_tags .all_tags_lis").slideUp("fast");
});



$("#choose_tags .all_tags_lis a").live("click", function(){
	$("#choose_tags .tag_list").append($(this));
	
	update_element_tags();

	$("#choose_tags .all_tags_lis").slideUp("fast", function(){
		$("#choose_tags .all_tags_lis").html();
	});
	return false;
});

$("#choose_tags .add_new_tag").click(function(){
	
	var link = $("#editor_form").attr("action");
	var tag_section = $("input[name=TAG_SECTION]").val();
	var iblock_id = $("input[name=IBLOCK_ID]").val();
	var new_tag = $("#new_tag").val();
	var tags = "";
	$("#choose_tags .tag_list a").each(function(){
		tags+=$(this).html()+",";
	});

	$(this).hide();
			
	$.post(link, {TAG_SECTION: tag_section, new_tag: new_tag, act: "new_tag", IBLOCK_ID:iblock_id, tags: tags},function(otvet){
		$("#choose_tags .tag_list").append('<a href="#">'+new_tag+'</a>');
		$("#new_tag").val("");
		$("#choose_tags .all_tags_lis").slideUp("fast");
		
		update_element_tags();	
	});
	
	return false;
});

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////




var act = "";
var go_to ="";
$("#public").click(function(){
	act = "save";
	refresh_form();
	$('#editor_form').submit();
	return false;
});

$("#predprosmotr").click(function(){
	act = "save_and_view";
	go_to = $(this).attr("href");
	refresh_form();
	$('#editor_form').submit();
	return false;
});


function refresh_form(){
	//проверка формы
	function check_editor_form(a,f,o){
		var ret=true;
		o.dataType = "html";
		return ret;
	}

	//Отправка формы
	$('#editor_form').ajaxForm({
		beforeSubmit: check_editor_form,
		success: function(data) {
			console.log(data);
			var lnk = self.location.protocol+'//'+self.location.hostname+''+go_to;
			//alert(lnk);
			if(act=="save_and_view" && go_to!="") window.open(lnk,'_blank','toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes'); 
		}
	});
}

});