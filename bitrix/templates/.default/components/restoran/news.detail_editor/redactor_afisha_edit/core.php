<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/
//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	//var_dump($GRs);

	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_element(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if(!check_element_perm($_REQUEST["ELEMENT_ID"])) die("Permission denied");
	
	
	//апдейтим элемент
	$el = new CIBlockElement;
	
	foreach($_REQUEST as $PR_CODE=>$PR_VAL){
		if($PR_CODE=="DETAIL_TEXT" || $PR_CODE=="NAME" || $PR_CODE=="SORT") continue;
		if(substr_count($PR_CODE, "PROPERTY_")>0){
			$PR_CODE = str_replace("PROPERTY_", "", $PR_CODE);
			$PROP[$PR_CODE]=$PR_VAL;
		}
	}
	
	$arLoadProductArray = Array("MODIFIED_BY"=> $USER->GetID(), "PROPERTY_VALUES"=> $PROP);

		
	
		
	if($_REQUEST["NAME"]!="") $arLoadProductArray["NAME"]=$_REQUEST["NAME"];
	if($_REQUEST["ACTIVE_FROM"]!="") $arLoadProductArray["ACTIVE_FROM"]=$_REQUEST["ACTIVE_FROM"];
	
	if($_REQUEST["DETAIL_TEXT"]!="") {
		$arLoadProductArray["DETAIL_TEXT"]=$_REQUEST["DETAIL_TEXT"];
		$arLoadProductArray["DETAIL_TEXT_TYPE"]="html";
	}	
	
	v_dump($arLoadProductArray);
	$res = $el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray);
	
}



function update_element_tags(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if(!check_element_perm($_REQUEST["ELEMENT_ID"])) die("Permission denied");
		
	$TAGS="";
	//$tar = tags_prepare($_REQUEST["tags"], "s1");
	$tar = explode(",", $_REQUEST["tags"]);
	$i=1;
	foreach($tar as $t){
		$TAGS.= $t;
		if($i<count($tar)) $TAGS.=",";
		$i++;
	}
	
	$el = new CIBlockElement;
	$arLoadProductArray["TAGS"]=$_REQUEST["tags"];
		
	//var_dump($arLoadProductArray);
	
	$el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray);
	
	
	
}

function get_tag_list(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}	
	
	$tar = tags_prepare($_REQUEST["tags"], "s1");
	//var_dump($tar);
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["TAG_SECTION"], "IBLOCK_ID"=>99, "NAME"=>"%".$_REQUEST["tag"]."%");
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	while($ar_fields = $res->GetNext()){
		if(!in_array($ar_fields["NAME"], $tar)) echo '<a href="#">'.$ar_fields["NAME"].'</a>';
	}

	
}


function new_tag(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}	
	
	if(!check_section_perm($_REQUEST["TAG_SECTION"])) die("Permission denied");
	
	$arFilter = Array("ACTIVE"=>"Y", "SECTION_ID"=>$_REQUEST["TAG_SECTION"], "IBLOCK_ID"=>99, "NAME"=>$_REQUEST["new_tag"]);
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false);
	if($ar_fields = $res->GetNext()){
		
	}else{
		$el = new CIBlockElement;
		$arLoadProductArray["NAME"]=$_REQUEST["new_tag"];
		$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["TAG_SECTION"];
		$arLoadProductArray["IBLOCK_ID"]=99;
		$arLoadProductArray["ACTIVE"]="Y";
		$el->Add($arLoadProductArray);
	}
}


function del_block(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$ELEMENT_ID = str_replace("#", "", $_REQUEST["ELEMENT_ID"]);

	if(!check_element_perm($ELEMENT_ID)) die("Permission denied");
	
	CIBlockElement::Delete($ELEMENT_ID);
}




/*
function new_article(){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	$bs = new CIBlockSection;
	$arFields = Array(
  		"ACTIVE" => "N",
  		"IBLOCK_SECTION_ID" => $IBLOCK_SECTION_ID,
  		"IBLOCK_ID" => $_REQUEST["IBLOCK_ID"],
  		"NAME" => "",
  	);

}
*/

if($_REQUEST["act"]=="sort_update") sort_update();
if($_REQUEST["act"]=="get_tag_list") get_tag_list();
if($_REQUEST["act"]=="edit_element") edit_element();
if($_REQUEST["act"]=="update_element_tags") update_element_tags();
if($_REQUEST["act"]=="new_tag") new_tag();
if($_REQUEST["act"]=="del_block") del_block();
if($_REQUEST["act"]=="new_article") new_article();
?>
