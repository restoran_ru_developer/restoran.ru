<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>


<?



$APPLICATION->AddHeadScript($templateFolder.'/chosen.jquery.js');
$APPLICATION->AddHeadScript($templateFolder.'/jquery.form.js');

?>
<form id="editor_form" method="post" action="<?=$templateFolder?>/core.php" >
<?
if($arResult["ID"]!="") echo '<input type="hidden" name="act" value="edit_element" />';
else echo '<input type="hidden" name="act" value="new_element" />';
?>

<div id="p_editor">
	<div class="left">
	
	
	</div>
	<div class="cntr">
	<div class="ttl">Описание</div>
	
	<input type="hidden" name="ELEMENT_ID" value="<?=$arResult["ID"]?>" />
	<input type="hidden" name="IBLOCK_ID" value="<?=$arResult["IBLOCK_ID"]?>" />
	<input type="hidden" name="TAG_SECTION" value="<?=$arParams["TAG_SECTION"]?>" />
	<?
	$APPLICATION->SetTitle("Редактирование: ".$arResult["NAME"]);
	?>
	<ul id="req_inp">
		
		<?foreach($arParams["REQ"] as $R){?>
			<li>
				<?
				$val = "";
				$addr = explode("__", $R["VALUE_FROM"]);
				if(count($addr)==1) $val = $arResult[$addr[0]];
				if(count($addr)==2) $val = $arResult[$addr[0]][$addr[1]];
				if(count($addr)==3) $val = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
				if($R["TYPE"]=="short_text"){
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/short_text_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"LEN"=>250,
                			"SHOW_CO"=>"Y",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="multi_select"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/multi_select_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="select"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/select_block.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				
				if($R["TYPE"]=="photo"){
					$values_list = "";
					$addr = explode("__", $R["VALUES_LIST"]);
					if(count($addr)==2) $values_list = $arResult[$addr[0]][$addr[1]];
					if(count($addr)==3) $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];
				
					
					$APPLICATION->IncludeFile(
						$templateFolder."/blocks/photo.php",
           		        Array(
                   		 	"BLOCK_TITTLE"=>$R["NAME"],
                   		 	"BLOCK_NAME"=>$R["CODE"],
                    		"BLOCK_VALUE" =>$val,
                    		"VALUES_LIST"=>$values_list,
                    		"SELECT_TITLE"=>" ",
                			"NOT_DELETE"=>"Y"
						),
						Array("MODE"=>"php")
					);
				}
				
				?>
			</li>
		<?}?>
	</ul>
	<?
	/////
	//Выводим элементы из раздела
	/////
	?>
	<ul id="block_list">
		<?		
		$tar_tags = explode(",", $arResult["TAGS"]);	
		foreach($tar_tags as $T){
			$ELEMENT_TAGS.='<a href="#">'.trim($T).'</a>';
		}
		?>
	</ul>
	
	<div class="buts">
		<div class="lb">
			<a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>" id="predprosmotr" target="_blank">Сохранить и посмотреть</a>
		</div>
		<div class="rb">
			<a href="#" id="public">Сохранить</a>
		</div>
		
	</div>
	
	<div class="comments">
	Опубликованную запись вы сможете найти и редактировать<br/>
в разделе «<a href="#">Блог</a>» личного кабинета
	</div>
	
	</div>
	<div class="right">
		
		<div id="choose_tags">
			<div class="ttl">Теги</div>
			<div class="ln">
				<?
				global $USER;
				$arGroups = $USER->GetUserGroupArray();
				foreach($arGroups as $v) $GRs[]=$v;
				?>
				<input name="new_tag" class="pole" id="new_tag"/> <?if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){?><a href="#" class="add_new_tag"></a><?}?>
				<div class="all_tags_lis"></div>
			</div>
			
			<div class="tag_list"><?=$ELEMENT_TAGS?></div>
		
			<div class="sub_t">Тэги раздела:</div>
			<div class="pop">
				<?
				
				?>
				<?foreach($arResult["ALL_TAGS"] as $T){?>
					<?if(substr_count($ELEMENT_TAGS, trim($T))==0){?> 
						<a href="#"><?=trim($T)?></a>
					<?}?>
				<?}?>
			</div>
			
			<?
			//<div class="all_tags"><a href="#">все теги</a></div>
			?>
		</div>
	
	</div>
	<div class="clear"></div>
</div>
</form>


<?
//v_dump($arResult);
?>