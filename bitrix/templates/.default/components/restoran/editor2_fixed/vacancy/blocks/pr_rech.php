<?
$OST = $LEN - trim(strlen($BLOCK_VALUE[2]));
$file = CFile::ResizeImageGet($BLOCK_VALUE[3], array('width'=>111, 'height'=>111), BX_RESIZE_IMAGE_EXACT, true); 
?>
<div class="content_block">
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][type]" value="add_pr"/>
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][fid]" value="<?=$BLOCK_VALUE[3]?>"/>
	<div class="name"><?=$BLOCK_TITTLE?> <?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
	<div class="iln short" style="margin-bottom: 12px;">
		<input type="text" class="pole" name="block[<?=$BLOCK_NAME?>][txt1]" value="<?=$BLOCK_VALUE[0]?>"/>
	</div>
				
	<div class="iln short">
		<input type="text" class="pole" name="block[<?=$BLOCK_NAME?>][txt2]" value="<?=$BLOCK_VALUE[1]?>"/>
	</div>
				
	<div class="iln2 short">
		<textarea name="block[<?=$BLOCK_NAME?>][txt3]" class="pole" maxlength="<?=$LEN?>"><?=$BLOCK_VALUE[2]?></textarea>
	</div>
		
	<?if($SHOW_CO=="Y"){?><div class="simbol_co short" id="<?=$BLOCK_NAME?>_co">Осталось <span><?=$OST?></span> символа</div><?}?>
				
	<div class="autor_photo">
		<div class="image"><img src="<?=$file["src"]?>" alt=""/></div>
		<div class="lnk"><input type="file" name="block[<?=$BLOCK_NAME?>][photo]" value="" id="file_field_<?=$BLOCK_NAME?>" class=""/><a href="#">Обзор</a></div>
	</div>		
</div>



<?if($SHOW_CO=="Y"){?>
<script type="text/javascript">
	if(sgo["<?=$BLOCK_NAME?>"]!=true){
	
		$("textarea[name='block[<?=$BLOCK_NAME?>][txt3]'").change(function(){
			var n = <?=$LEN?> - $(this).val().length;
			$("#<?=$BLOCK_NAME?>_co span").html(n);
		}).keyup(function(){
			var n = <?=$LEN?> - $(this).val().length;
			$("#<?=$BLOCK_NAME?>_co span").html(n);
		});
	
		sgo["<?=$BLOCK_NAME?>"]=true;
	}
</script>
<?}?>		

<script type="text/javascript">	
// Проверка поддержки File API в браузере
var fileInput = $('#file_field_<?=$BLOCK_NAME?>');


if(window.FileReader == null) {
	//alert('Ваш браузер не поддерживает File API!');
	if(sgo["<?=$BLOCK_NAME?>"]!=true){
	fileInput.next().remove();
	fileInput.customFileInput();
	
	fileInput.bind({
		change: function() {
    	   $("#file_changed_<?=$BLOCK_NAME?>").val("Y");
		}
	});
		sgo["<?=$BLOCK_NAME?>"]=true;
	}
}else{
if(sgo["<?=$BLOCK_NAME?>"]!=true){
	fileInput.addClass("file_field");
	

	
	fileInput.bind({
		change: function() {
    	    displayFiles_<?=$BLOCK_NAME?>(this.files, fileInput);
		
		}
	});


	// Отображение выбраных файлов и создание миниатюр
	function displayFiles_<?=$BLOCK_NAME?>(files, obj) {
		var imageType = /image.*/;
		var num = 0;
    
     	$.each(files, function(i, file) {
		
			// Отсеиваем не картинки
			if (!file.type.match(imageType)) {
				alert("Ошибка");
				return true;
			}

       		num++;
            
	       	var img = obj.parent("div").prev().children("img");
   		    obj.parent("div").prev().get(0).file = file;
            
	        var reader = new FileReader();
        
   		    reader.onload = (function(aImg) {
				return function(e) {
					aImg.attr('src', e.target.result);
					aImg.attr('width', 111);  
					aImg.attr('height', 130);         
				};
			})(img); 
		  
        	reader.readAsDataURL(file);         
	});    
}
sgo["<?=$BLOCK_NAME?>"]=true;
}
}
</script>