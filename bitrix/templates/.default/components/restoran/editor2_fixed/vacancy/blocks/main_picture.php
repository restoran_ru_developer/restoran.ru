<?
$OST = $LEN - trim(strlen($BLOCK_VALUE[2]["VALUE"]));
$file = CFile::ResizeImageGet($BLOCK_VALUE[3]["VALUE"], array('width'=>111, 'height'=>111), BX_RESIZE_IMAGE_EXACT, true); 
?>
<div class="content_block">
	
	
	<div class="name"><?=$BLOCK_TITTLE?> <?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
	<div class="iln short" style="margin-bottom: 12px;">
		<input type="text" class="pole" name="<?=$BLOCK_VALUE[0]["NAME"]?>" value="<?=$BLOCK_VALUE[0]["VALUE"]?>"/>
	</div>
				
	<div class="iln short">
		<input type="text" class="pole" name="<?=$BLOCK_VALUE[1]["NAME"]?>" value="<?=$BLOCK_VALUE[1]["VALUE"]?>"/>
	</div>
				
	<div class="iln2 short">
		<textarea name="<?=$BLOCK_VALUE[2]["NAME"]?>" class="pole" maxlength="<?=$LEN?>"><?=$BLOCK_VALUE[2]["VALUE"]?></textarea>
	</div>
		
	<?if($SHOW_CO=="Y"){?><div class="simbol_co short" id="<?=$BLOCK_VALUE[2]["NAME"]?>_co">Осталось <span><?=$OST?></span> символа</div><?}?>
				
	<div class="autor_photo">
		<div class="image"><img src="<?=$file["src"]?>" alt=""/></div>
		<div class="lnk"><input type="file" name="<?=$BLOCK_VALUE[3]["NAME"]?>" value="" id="file_field_<?=$BLOCK_VALUE[3]["NAME"]?>"  maxlength="<?=$LEN?>" class=""/><a href="#">Обзор</a></div>
		<input type="hidden" id="file_changed_<?=$BLOCK_VALUE[3]["NAME"]?>" name="file_changed_<?=$BLOCK_VALUE[3]["NAME"]?>_changed" value="N"/>
	</div>		
</div>



<?if($SHOW_CO=="Y"){?>
<script type="text/javascript">
	$("textarea[name=<?=$BLOCK_VALUE[2]["NAME"]?>]").change(function(){
		var n = <?=$LEN?> - $(this).val().length;
		$("#<?=$BLOCK_VALUE[2]["NAME"]?>_co span").html(n);
	}).keyup(function(){
		var n = <?=$LEN?> - $(this).val().length;
		$("#<?=$BLOCK_VALUE[2]["NAME"]?>_co span").html(n);
	});
</script>
<?}?>		

<script type="text/javascript">	
// Проверка поддержки File API в браузере
var fileInput = $('#file_field_<?=$BLOCK_VALUE[3]["NAME"]?>');

if(window.FileReader == null) {
	//alert('Ваш браузер не поддерживает File API!');
	fileInput.next().remove();
	fileInput.customFileInput();
	
	fileInput.bind({
		change: function() {
    	   $("#file_changed_<?=$BLOCK_VALUE[3]["NAME"]?>").val("Y");
		}
	});
	
}else{
	fileInput.addClass("file_field");

	fileInput.bind({
		change: function() {
    	    displayFiles_<?=$BLOCK_VALUE[3]["NAME"]?>(this.files, fileInput);
		
		}
	});

	// Отображение выбраных файлов и создание миниатюр
	function displayFiles_<?=$BLOCK_VALUE[3]["NAME"]?>(files, obj) {
		var imageType = /image.*/;
		var num = 0;
    	
    	$("#file_changed_<?=$BLOCK_VALUE[3]["NAME"]?>").val("Y");
    
     	$.each(files, function(i, file) {
		
			// Отсеиваем не картинки
			if (!file.type.match(imageType)) {
				alert("Ошибка")
				return true;
			}

       		num++;
            
	       	var img = obj.parent("div").prev().children("img");
   		    obj.parent("div").prev().get(0).file = file;
            
	        var reader = new FileReader();
        
   		    reader.onload = (function(aImg) {
				return function(e) {
					aImg.attr('src', e.target.result);
					aImg.attr('width', 111);           
				};
			})(img); 
		  
        	reader.readAsDataURL(file);         
	});    
}
}
</script>