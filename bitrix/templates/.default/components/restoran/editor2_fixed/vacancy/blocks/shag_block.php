<?
$OST = $LEN - trim(strlen($BLOCK_VALUE[0]));

$file = CFile::ResizeImageGet($BLOCK_VALUE[1], array('width'=>111, 'height'=>111), BX_RESIZE_IMAGE_EXACT, true); 
?>


<div class="content_block">	
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][type]" value="add_step"/>
	<input type="hidden" name="block[<?=$BLOCK_NAME?>][fid]" value="<?=$BLOCK_VALUE[1]?>"/>
	<div class="name <?if($STEP>0) echo "step"; ?>"><?=$BLOCK_TITTLE?> <?=$STEP?> <?if($NOT_DELETE!="Y"){?><a href="#" class="remove_block">Удалить</a><?}?></div>
				
	<div class="short">
		<textarea name="block[<?=$BLOCK_NAME?>][txt]" class="pole" maxlength="<?=$LEN?>"><?=$BLOCK_VALUE[0]?></textarea>
	</div>
		
	<?if($SHOW_CO=="Y"){?><div class="simbol_co short" id="<?=$BLOCK_NAME?>_co">Осталось <span><?=$OST?></span> символа</div><?}?>
				
	<div class="autor_photo" style="top:33px;">
		<div class="image"><img src="<?=$file["src"]?>" alt=""/></div>
		<div class="lnk"><input type="file" name="block[<?=$BLOCK_NAME?>][file]" value="" id="file_field_<?=$BLOCK_NAME?>"  maxlength="<?=$LEN?>" class=""/><a href="#">Обзор</a></div>
	</div>		
</div>



		

<script type="text/javascript">	

// Проверка поддержки File API в браузере
var fileInput = $('#file_field_<?=$BLOCK_NAME?>');

if(window.FileReader == null) {
	if(sgo["<?=$BLOCK_NAME?>"]!=true){
		fileInput.next().remove();
		fileInput.customFileInput();
	
		fileInput.bind({
			change: function() {
	   	 	   $("#file_changed_<?=$BLOCK_NAME?>").val("Y");
			}
		});
		sgo["<?=$BLOCK_NAME?>"] = true;
	}
}else{
	if(sgo["<?=$BLOCK_NAME?>"]!=true){
	fileInput.addClass("file_field");

	fileInput.bind({
		change: function() {
    	    displayFiles_<?=$BLOCK_NAME?>(this.files, fileInput);
		
		}
	});

	// Отображение выбраных файлов и создание миниатюр
	function displayFiles_<?=$BLOCK_NAME?>(files, obj) {
		var imageType = /image.*/;
		var num = 0;
    	
    	$("#file_changed_<?=$BLOCK_NAME?>").val("Y");
    
     	$.each(files, function(i, file) {
		
			// Отсеиваем не картинки
			if (!file.type.match(imageType)) {
				alert("Ошибка")
				return true;
			}

       		num++;
            
	       	var img = obj.parent("div").prev().children("img");
   		    obj.parent("div").prev().get(0).file = file;
            
	        var reader = new FileReader();
        
   		    reader.onload = (function(aImg) {
				return function(e) {
					aImg.attr('src', e.target.result);
					aImg.attr('width', 111);           
				};
			})(img); 
		  
        	reader.readAsDataURL(file);         
	}); 
	   
	}

	sgo["<?=$BLOCK_NAME?>"] = true;
	}
}
</script>