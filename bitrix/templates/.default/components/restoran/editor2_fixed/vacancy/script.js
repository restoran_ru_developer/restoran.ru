var sgo = {};
$(document).ready(function(){



    $.tools.dateinput.localize("ru",  {
        months: 'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь', 
                                   shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
                                   days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
                                   shortDays:     'вс,пн,вт,ср,чт,пт,сб'
    });
    $("input[name=ACTIVE_FROM]").dateinput({
        lang: 'ru', 
        firstDay: 1 , 
        format:"dd.mm.yyyy",
        selectors: true,        
        yearRange: [-60,1]
    });    
    $("input[name=ACTIVE_TO]").dateinput({
        lang: 'ru', 
        firstDay: 1 , 
        format:"dd.mm.yyyy",
        selectors: true,
        yearRange: [-60,1]
    });
    $("input[code=PROPERTY_EXP_WHEN_1]").dateinput({
        lang: 'ru', 
        selectors: true,
        firstDay: 1 , 
        format:"dd.mm.yyyy",        
        yearRange: [-60,1]
    });    
    $("input[code=PROPERTY_EXP_WHEN_2]").dateinput({
        lang: 'ru', 
        firstDay: 1 , 
        format:"dd.mm.yyyy",
        selectors: true,
        yearRange: [-60,1],        
    });
    //$("input[name=PROPERTY_CONTACT_PHONE]").maski("(999) 999-99-99");
    $("input[name=PROPERTY_WAGES_OF]").keyup(function(){       
       $(this).val(($(this).val().replace(/\D/g,"")));
    });
    $("input[name=PROPERTY_WAGES_TO]").keyup(function(){       
       $(this).val(($(this).val().replace(/\D/g,"")));
    });
    $("input[name=PROPERTY_AGE_FROM]").keyup(function(){       
       $(this).val(($(this).val().replace(/\D/g,"")));
    });
    $("input[name=PROPERTY_AGE_TO]").keyup(function(){       
       $(this).val(($(this).val().replace(/\D/g,"")));
    });
    $("input[name=PROPERTY_SUGG_WORK_ZP_FROM]").keyup(function(){       
       $(this).val(($(this).val().replace(/\D/g,"")));
    });
    $("input[name=PROPERTY_SUGG_WORK_ZP_TO]").keyup(function(){       
       $(this).val(($(this).val().replace(/\D/g,"")));
    });
	
    $(".add_block").click(function(){
        var id= $(this).attr("id");
        var btn = $(this);
	
        //alert(id);
        $("li[id="+id+"]").each(function(){
            t = $(this).clone();
            t.find("input").val("").parents("li").attr("id",id+"Y");
            var t = btn.before(t);
        //t.find("input").val("");
        //alert(t.html());
        });
	
        $("input[code=PROPERTY_EXP_WHEN_1]:last").dateinput({
            lang: 'ru', 
            firstDay: 1 , 
            format:"dd.mm.yyyy",
            selectors: true,
            yearRange: [-60,0]
        });    
        $("input[code=PROPERTY_EXP_WHEN_2]:last").dateinput({
            lang: 'ru', 
            firstDay: 1 , 
            format:"dd.mm.yyyy",
            selectors: true,
            yearRange: [-60,0]
        });
        return false;
    });


    /******************ОТПРАВКА ФОРМЫ************************/
    /********************************************************/
    var act = "";
    var go_to ="";
    $("#public").click(function(){
        act = "save_and_view";
        go_to = $(this).attr("href");
	
	
        refresh_form();
        $('#editor_form').submit();
        return false;
    });

    $("#predprosmotr").click(function(){
        act = "save";
	
        refresh_form();
        $('#editor_form').submit();
        return false;
    });


    function refresh_form(){
        //проверка формы
        
        function check_editor_form(a,f,o){
		var ret=true;
		o.dataType = "html";
		
		$("#error_li").html();
		
		
		var error_string="Вы не заполнили следующие поля: ";
		var z =0;
		var co_errors=0;
		f.find("input[type=text]").each(function(){
			if($(this).hasClass("ob") && $(this).val()==""){
				ret = false;
				var name = $(this).parent("div").parent("div").find(".name").html();	
				name = name.replace('<i class="star">*</i>','');		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		f.find("textarea").each(function(){
			if($(this).hasClass("ob") && $(this).val()==""){
				ret = false;
				var name = $(this).parent("div").parent("div").find(".name").html();	
				name = name.replace('<i class="star">*</i>','');		
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		f.find("select").each(function(){
			//alert($(this).val());
			if($(this).hasClass("ob") && ($(this).val()=="" || $(this).val()==null)){
				ret = false;
				var name = $(this).parent("div").parent("div").find(".name").html();	
				name = name.replace('<i class="star">*</i>','');	
				
				if(z>0) error_string+=", ";
				error_string+=name;
				
				co_errors+=1;
				z+=1;
			}
		});
		
		
		
		error_string+=".";
		
		if(co_errors>0){
			$("#error_li").html(error_string);
		}
		console.log(error_string);
		if(ajax_load) ret = false;
		
		return ret;
                
	}

        //Отправка формы
        $('#editor_form').ajaxForm({
            beforeSubmit: check_editor_form,
            success: function(data) {
                console.log(data);
                var lnk = self.location.protocol+'//'+self.location.hostname+''+go_to;
			
                if(parseInt(data)>0) $("#editor_form input[name=ELEMENT_ID]").val(parseInt(data));
			
                if(act=="save_and_view" && go_to!="") window.location.replace(go_to);
                else{
                    if(parseInt(data)>0) window.location.replace(window.location.href+"&ID="+parseInt(data));
                    else window.location.reload();
                } 
			
            }
        });
    }
/********************************************************/
});