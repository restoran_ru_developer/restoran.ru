<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>


<?
//$APPLICATION->AddHeadScript($templateFolder . '/jq_redactor/redactor.js');
//$APPLICATION->AddHeadScript($templateFolder . '/uploaderObject.js');
$APPLICATION->AddHeadScript($templateFolder . '/chosen.jquery.js');
$APPLICATION->AddHeadScript($templateFolder . '/jQuery.fileinput.js');
$APPLICATION->AddHeadScript($templateFolder . '/jquery.form.js');

//v_dump($arResult);
?>
<script>
    $(document).ready(function(){
        $.tools.dateinput.localize("ru",  { months: 'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь', 
            shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',   
            days:'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',                                       
            shortDays:     'вс,пн,вт,ср,чт,пт,сб'});
        //$("[name=ACTIVE_FROM]").dateinput({lang: 'ru', firstDay: 1 , format:"dd.mm.yyyy"});
    });
</script>
<form id="editor_form" method="post" action="<?= $templateFolder ?>/core.php"  class="my_rest">
    <input type="hidden" name="act" value="edit_element" />
    <input type="hidden" name="USER_BIND_APPLICANT" value="1" />
    <input type="hidden" name="ELEMENT_ID" value="<?= $arParams["ELEMENT_ID"] ?>" />
    <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
    <input type="hidden" id="NEW_IBLOCK_ID" name="NEW_IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
    <input type="hidden" name="PARENT_SECTION" value="<?= $arParams["PARENT_SECTION"] ?>" />


    <div class="wrap-div">
        <ul>
            <?
            $i = 0;

            $BL_INDEX = 0;
            $BL_START = $i;
            $BL_STOP = $i;

            foreach ($arParams["REQ"] as $R) {
                if ($R["CODE"]=="PROPERTY_EXP_WHEN_1"):
                    echo "<li class='item-row'><h2>Опыт работы</h2></li>";
                endif;
                ?>
                <li class="item-row <?
            if ($R["BL"] != "" && $R["BL"] != $arParams["REQ"][$i - 1]["BL"]) {
                echo 'blfirst';
                $BL_START = $i;
                $FBL = true;
            }
                ?>" <? if ($R["BL"] != "") echo 'id="block_' . $R["BL"] . '"'; ?>>
                        <?
                        $val = "";
                        $addr = explode("__", $R["VALUE_FROM"]);
                        if (count($addr) == 1)
                            $val = $arResult[$addr[0]];                        
                        if (count($addr) == 2)
                            $val = $arResult[$addr[0]][$addr[1]];
                        if (count($addr) == 3)
                            $val = $arResult[$addr[0]][$addr[1]][$addr[2]];
                        if (is_array($val)) {
                            $BLI = $BL_INDEX;
                        }else
                            $BLI = false;
                        if ($FBL) {
                            $CO_D = count($val);
                        }

                        if ($R["TYPE"] == "short_text") {
                            //var_dump($BLI);
                            $APPLICATION->IncludeFile(
                                    $templateFolder . "/blocks/short_text_block.php", Array(
                                "BLOCK_TITTLE" => $R["NAME"],
                                "BLOCK_NAME" => $R["CODE"],
                                "BLOCK_VALUE" => $val,
                                "LEN" => $R["LEN"],
                                "SHOW_CO" => $R["SHOW_CO"],
                                "NOT_DELETE" => "Y",
                                "BL" => $R["BL"],
                                        "REQUIRED"=>$R["REQUIRED"],
                                "IND" => $BLI
                                    ), Array("MODE" => "php")
                            );
                        }

                        if ($R["TYPE"] == "short_text2") {
                            $APPLICATION->IncludeFile(
                                    $templateFolder . "/blocks/short_text_block2.php", Array(
                                "BLOCK_TITTLE" => $R["NAME"],
                                "BLOCK_NAME" => $R["CODE"],
                                "BLOCK_VALUE" => $val,
                                "SHOW_CO" => "N",
                                "NOT_DELETE" => "Y",
                                "BL" => $R["BL"],
                                        "REQUIRED"=>$R["REQUIRED"],
                                "IND" => $BL_INDEX
                                    ), Array("MODE" => "php")
                            );
                        }


                        if ($R["TYPE"] == "multi_select") {
                            $values_list = "";
                            $addr = explode("__", $R["VALUES_LIST"]);
                            if (count($addr) == 1)
                                $values_list = $arResult[$addr[0]];
                            if (count($addr) == 2)
                                $values_list = $arResult[$addr[0]][$addr[1]];
                            if (count($addr) == 3)
                                $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];

                           
                            //v_dump($arResult);
                            
                            $APPLICATION->IncludeFile(
                                    $templateFolder . "/blocks/multi_select_block.php", Array(
                                "BLOCK_TITTLE" => $R["NAME"],
                                "BLOCK_NAME" => $R["CODE"],
                                "BLOCK_VALUE" => $val,
                                "VALUES_LIST" => $values_list,
                                "SELECT_TITLE" => " ",
                                        "REQUIRED"=>$R["REQUIRED"],
                                "NOT_DELETE" => "Y"
                                    ), Array("MODE" => "php")
                            );
                            //v_dump($val);
                            //v_dump($cityVal);
                            //v_dump($cityValList);
                        }


                        if ($R["TYPE"] == "select") {
                            $values_list = "";
                            $addr = explode("__", $R["VALUES_LIST"]);
                            if (count($addr) == 1)
                                $values_list = $arResult[$addr[0]];
                            if (count($addr) == 2)
                                $values_list = $arResult[$addr[0]][$addr[1]];
                            if (count($addr) == 3)
                                $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];                             
                            $APPLICATION->IncludeFile(
                                    $templateFolder . "/blocks/select_block.php", Array(
                                "BLOCK_TITTLE" => $R["NAME"],
                                "BLOCK_NAME" => $R["CODE"],
                                "BLOCK_VALUE" => $val,
                                "VALUES_LIST" => $values_list,
                                "SELECT_TITLE" => " ",
                                        "REQUIRED"=>$R["REQUIRED"],
                                "NOT_DELETE" => "Y"
                                    ), Array("MODE" => "php")
                            );
                        }


                        if ($R["TYPE"] == "photo") {
                            $values_list = "";
                            $addr = explode("__", $R["VALUES_LIST"]);
                            if (count($addr) == 1)
                                $values_list = $arResult[$addr[0]];
                            if (count($addr) == 2)
                                $values_list = $arResult[$addr[0]][$addr[1]];
                            if (count($addr) == 3)
                                $values_list = $arResult[$addr[0]][$addr[1]][$addr[2]];


                            $APPLICATION->IncludeFile(
                                    $templateFolder . "/blocks/photo.php", Array(
                                "BLOCK_TITTLE" => $R["NAME"],
                                "BLOCK_NAME" => $R["CODE"],
                                "BLOCK_VALUE" => $val,
                                "VALUES_LIST" => $values_list,
                                "SELECT_TITLE" => " ",
                                        "REQUIRED"=>$R["REQUIRED"],
                                "NOT_DELETE" => "Y"
                                    ), Array("MODE" => "php")
                            );
                        }
                        ?>
                </li>

                <?
                if ($R["BL"] != "" && $R["BL"] != $arParams["REQ"][$i + 1]["BL"]) {
                    $BL_STOP = $i;
                    $BL_INDEX++;
                        
                    for ($l = 1; $l < $CO_D; $l++) {
                        //Прогоняем еще раз блоки 
                        for ($k = $BL_START; $k <= $BL_STOP; $k++) {
                            ?>
                            <li class="item-row <? if ($k == $BL_START) echo "blfirst"; ?>">
                                <?
                                if ($arParams["REQ"][$k]["TYPE"] == "short_text2") {

                                    $val = "";
                                    $addr = explode("__", $arParams["REQ"][$k]["VALUE_FROM"]);
                                    if (count($addr) == 1)
                                        $val = $arResult[$addr[0]];
                                    if (count($addr) == 2)
                                        $val = $arResult[$addr[0]][$addr[1]];
                                    if (count($addr) == 3)
                                        $val = $arResult[$addr[0]][$addr[1]][$addr[2]];

                                    $APPLICATION->IncludeFile(
                                            $templateFolder . "/blocks/short_text_block2.php", Array(
                                        "BLOCK_TITTLE" => $arParams["REQ"][$k]["NAME"],
                                        "BLOCK_NAME" => $arParams["REQ"][$k]["CODE"],
                                        "BLOCK_VALUE" => $val,
                                        "LEN" => 250,
                                        "SHOW_CO" => "Y",
                                        "NOT_DELETE" => "Y",
                                                "REQUIRED"=>$R["REQUIRED"],
                                        "BL" => $R["BL"],
                                        "IND" => $BL_INDEX
                                            ), Array("MODE" => "php")
                                    );
                                }
                                ?>
                            </li>
                            <?
                        }
                        $BL_INDEX++;
                    }

                    echo '<li class="bllast"><div class="buts2"><a href="#" class="add_block" id="block_' . $R["BL"] . '" >+ Добавить еще</a></div></li>';
                    $BL_INDEX++;
                }
                $i++;
                ?>
            <? } ?>


        </ul>
        <div class="item-row error_li" id="error_li" style="color:red; font-size: 16px; margin-top: 10px;"></div>
        <i class="grey">* - Обязательно для заполнения</i>
        <div class="buts">            
            <a href="<?= $arParams["BACK_LINK"] ?>" id="public" class="sbut"><? if ($arParams["ELEMENT_ID"] > 0) echo 'Сохранить'; else echo 'Разместить'; ?></a>
            <?/*if ($_REQUEST["ID"]):?>
                <a class="sbut" href="/content/joblist/preview.php?IBLOCK_TYPE_ID=<?=$arResult["IBLOCK_TYPE_ID"]?>&ELEMENT_ID=<?=$_REQUEST["ID"]?>" target="_blank">Предпросмотр</a>
            <?endif;*/?>
        </div>
        
    </div>
</form>


<?
//v_dump($arResult);
?>