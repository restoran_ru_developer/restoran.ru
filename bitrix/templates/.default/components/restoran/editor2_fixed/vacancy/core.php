<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
                        $gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
                        if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;			
		}		
	}else return false;
}



function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}



function edit_element(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$el = new CIBlockElement;
	
	//Если не выбран раздел, то кидаем просто в корневой
	if($_REQUEST["SECTION_ID"]>0) $SECTION_ID=$_REQUEST["SECTION_ID"];
	else $SECTION_ID=$_REQUEST["PARENT_SECTION"];
	
	

	$PROP = array();

	//собираем массив со свойствами
	foreach($_REQUEST as $CODE=>$VAL){
		if(substr_count($CODE, "PROPERTY_")>0){
			$CODE = str_replace("PROPERTY_", "", $CODE);
			//выкидываем пустые значения из массива
			if(is_array($VAL)){
				for($i=0; $i<count($VAL);$i++){
					if($VAL[$i]=="") unset($VAL[$i]);
				}
			}
			$PROP[$CODE]=$VAL;
		}
	}
	
	
	

	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  		"IBLOCK_SECTION_ID" => $SECTION_ID,          // элемент лежит в корне раздела
  		"IBLOCK_ID"      => $_REQUEST["IBLOCK_ID"],
  		"PROPERTY_VALUES"=> $PROP,
  		"NAME"           => trim($_REQUEST["NAME"]),
  		"SORT"         => $_REQUEST["SORT"],  
  		"ACTIVE"         => "Y",            
  		"ACTIVE_FROM"   => $_REQUEST["ACTIVE_FROM"],
  		"ACTIVE_TO"   => date("d.m.Y",  strtotime($_REQUEST["ACTIVE_FROM"]." + 30 days")),
                "DETAIL_PICTURE"   => $_FILES["DETAIL_PICTURE"],
  		"CODE"=>translitIt(trim($_REQUEST["NAME"]))
  	);
	
	
	
	v_dump($arLoadProductArray);
	
	//Проверяем права на запись
	//if(!check_section_perm($SECTION_ID) && $SECTION_ID>0) die("Permission denied");
	
	if($_REQUEST["ELEMENT_ID"]>0){
		//Обновляем существующую публикацию	
	
		unset($arLoadProductArray["IBLOCK_ID"]);
		if(!check_element_perm($_REQUEST["ELEMENT_ID"]) && $_REQUEST["ELEMENT_ID"]>0) die("Permission denied");
		if($el->Update($_REQUEST["ELEMENT_ID"], $arLoadProductArray,false, true, true)) echo "ok";
		else echo "Error: ".$el->LAST_ERROR;
	
	}else{
		//создаем новую публикацию
		
		
		
		if($ELEMENT_ID = $el->Add($arLoadProductArray,false,true,true))
  			echo $ELEMENT_ID;
		else
  			echo "Error: ".$el->LAST_ERROR;
	}
	
}






if($_REQUEST["act"]=="edit_element") edit_element();

?>
