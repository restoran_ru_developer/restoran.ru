<?define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/
//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die();

$el = new CIBlockElement;

$arLoadProductArray["IBLOCK_SECTION_ID"]=$_REQUEST["SECTION_ID"];
$arLoadProductArray["IBLOCK_ID"]=$_REQUEST["IBLOCK_ID"];
$arLoadProductArray["ACTIVE"]="Y";


//var_dump($APPLICATION->GetCurDir());
switch ($_REQUEST["block_id"]) {
	case "add_video":
    	$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/video_block.php",
			Array(
				"BLOCK_TITTLE"=>"Видео",
        	    "BLOCK_VALUE" =>"",
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break; 
    
    
    case "add_short_text":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/short_text_block.php",
			Array(
				"BLOCK_TITTLE"=>"Короткий текст",
                "BLOCK_VALUE" =>"",
                "LEN"=>250,
                "SHOW_CO"=>"Y",
                "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break;
    
    
    case "add_rest":
        //Нужно получить список всех ресторанов
		//Потом включить кэширование
		$arRestIB = getArIblock("catalog", CITY_ID);
		$arFilter = Array('IBLOCK_TYPE'=>"catalog", 'ACTIVE'=>'Y');
  		$db_list = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false);
  		$VALUES =array(); 
  		while($ar_result = $db_list->GetNext()){
  			$VALUES[]=array("ID"=>$ar_result["ID"], "NAME"=>$ar_result["NAME"]);
		}				
				
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/restoran.php",
			Array(
				"BLOCK_TITTLE"=>"Упомянание о ресторане",
				"BLOCK_VALUE" =>array(""),
				"VALUES_LIST"=>$VALUES,
				"SELECT_TITLE"=>"Выберите ресторан",
				"BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break;
    
    
    case "add_step":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/shag_block.php",
			Array(
				"BLOCK_TITTLE"=>"Шаг",
				"STEP"=>$_REQUEST["steps"]+1,
        	    "BLOCK_VALUE" =>array("",""),
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
    break;
    
        
    case "add_text":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/vis_red.php",
			Array(
				"BLOCK_TITTLE"=>"Вставка текста",
        		"BLOCK_VALUE" =>array("Заголовок","Текст"),
        		"BLOCK_NAME"=>"bl".time(),
			),
			Array("MODE"=>"php")
        );
    break;
    
    
    case "add_pr":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/pr_rech.php",
			Array(
				"BLOCK_TITTLE"=>"Прямая речь",
				"BLOCK_VALUE" =>array("","",""),
				"LEN"=>500,
				"BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
       	);
    break;
    
    
    case "add_photos":
		$APPLICATION->IncludeFile(
			$APPLICATION->GetCurDir()."/blocks/photos.php",
			Array(
				"BLOCK_TITTLE"=>"Фотогалерея",
        	    "BLOCK_VALUE" =>array(),
        	    "BLOCK_NAME"=>"bl".time()
			),
			Array("MODE"=>"php")
        );
   	break;
   		
}


?>