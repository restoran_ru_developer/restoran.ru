<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["ORDERS"] as $val){
	if($STATUS[$val["ORDER"]["STATUS_ID"]]==""){
		$arStatus = CSaleStatus::GetByID($val["ORDER"]["STATUS_ID"]);
		$STATUS[$val["ORDER"]["STATUS_ID"]]=$arStatus["NAME"];
		
	}
	//if ($arStatus = CSaleStatus::GetByID($STATUS_ID))

}

//var_dump($arResult["ORDERS"]);

?>
<table class="profile-activity w100" id="my_bron">
		<tr>
			<th class="first">Место</th>
			<th>Заказ</th>
			<th>Статус</th>
			<th>Стоимость заказа</th>
			<th>Дата и время</th>
			<th></th>
		</tr>
		
	<?foreach($arResult["ORDERS"] as $val):?>
	<tr>
			<?	
			$PROPS=array();
			//нужно получить инфу по заказу
			$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("PERSON_TYPE_ID" => $val["ORDER"]["PERSON_TYPE_ID"]));
   			while ($arProps = $db_props->Fetch()){
   				$db_vals = CSaleOrderPropsValue::GetList(array("SORT" => "ASC"),array("ORDER_ID" => $val["ORDER"]["ID"], "ORDER_PROPS_ID" => $arProps["ID"]));
      			if ($arVals = $db_vals->Fetch()){
      				$PROPS[$arProps["CODE"]]=$arVals["VALUE"];
        			
        		}			
   			}
   
   			//запрашиваем инфу по ресторану
   			if($PROPS["RESTID"]>0){
				$res = CIBlockElement::GetByID($PROPS["RESTID"]);
				if($ob2 = $res->GetNextElement()){
	 				$arFields = $ob2->GetFields();  
 					$arProps = $ob2->GetProperties();

 					$file = CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width'=>73, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);
 			
 					$REST_NAME=$arFields["NAME"];
 				}
	
   			
   			}
				
			?>
			<td class="first w-new">
			<img src="<?=$file["src"]?>" width="73" height="60" />
			<p class="name"><a href="<?=$arFields["DETAIL_PAGE_URL"]?>" target="_blank"><?=$REST_NAME?></a></p>
            <p class="profile-activity-rating">
            	<div class="rating" style="padding:0px;">
           			<?for($i = 1; $i <= 5; $i++):?>
               			<div class="small_star<?if($i<=round($arProps["ratio"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
           			<?endfor?>
           			<div class="clear"></div>
      			</div>
            </p>
            <p class="upcase">ресторан</p>
         </td>
         <td>
         	<?
			foreach($val["BASKET_ITEMS"] as $vval){
				if (strlen($vval["DETAIL_PAGE_URL"])>0) 
					echo '<a href="'.$vval["DETAIL_PAGE_URL"].'" target="_blank">';
				echo $vval["NAME"];
				if (strlen($vval["DETAIL_PAGE_URL"])>0) 
					echo '</a>';
			}
			?>         
         </td>
		 <td>
		 	<p class="status">
		 	<?
		 	if($val["ORDER"]["CANCELED"]=="Y") echo 'Отменен';
		 	else echo $STATUS[$val["ORDER"]["STATUS_ID"]];
		 	?>
		 	</p>
		</td>
		<td>
			<p class="type"><?=CurrencyFormat($val["ORDER"]["PRICE"], "RUB");?></p>
		</td>
		<td>
			<ul>
				<li><?=$val["ORDER"]["DATE_INSERT_FORMAT"]?></li>
			</ul>
		</td>
		
		<td>
		<?
		//var_dump($val["ORDER"]);
		?>
		    	<div class="activity-actions w-small">
		    		<?if($val["ORDER"]["STATUS_ID"]=="F"){?>
		    		<a class="icon-continue" href="<?=$templateFolder?>/core.php?act=repeat&ID=<?=$val["ORDER"]["ID"]?>">Повторить</a> 
		    		<?}?>
		    		
		    		
		    		
		    		<?if($val["ORDER"]["STATUS_ID"]=="N"){?>
		    			<a class="icon-delete" href="<?=$templateFolder?>/core.php?act=cancel&ID=<?=$val["ORDER"]["ID"]?>">Отменить</a> 
		    		<?}?>
		    		
		    		
		    	
			</div>
		</td>
	</tr>		
	<?endforeach;?>
</table>
<?if(strlen($arResult["NAV_STRING"]) > 0):?>
	<?=$arResult["NAV_STRING"]?>
<?endif?>