<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/***
НУЖНО ДОБАВИТЬ ПРОВЕРКУ НА АВТОРИЗАЦИЮ И ВСЕ ТАКОЕ
****/

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function check_section_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;
	
	$res = CIBlockSection::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			//var_dump($ar_res["IBLOCK_ID"]);
			
			$gr_res = CIBlock::GetGroupPermissions($ar_res["IBLOCK_ID"]);
			//var_dump($gr_res);
			//echo CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID());
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"], $USER->GetID())>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}




function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ar_res = $res->GetNext()){
		//если пользователь редактор или админ
		if(in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
			if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') return true;
			else return false;
		}else{
			//если обычный юзер
			if($ar_res["CREATED_BY"]==$USER->GetID()) return true;
			else return false;
		}		
	}else return false;
}


function repeat(){
	global $USER;
	//Copy order
	if(!CModule::IncludeModule("sale")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	
	$ID = IntVal($_REQUEST["ID"]);
	
	$dbOrder = CSaleOrder::GetList(Array("ID"=>"DESC"), Array("ID"=>$ID, "USER_ID"=>IntVal($USER->GetID()), "LID" => SITE_ID));
	if ($arOrder = $dbOrder->Fetch())
	{
		$dbBasket = CSaleBasket::GetList(Array("ID"=>"ASC"), Array("ORDER_ID"=>$arOrder["ID"]));
		while ($arBasket = $dbBasket->Fetch())
		{
			$arFields=Array();
			$arProps = array();
			$dbBasketProps = CSaleBasket::GetPropsList(
					array("SORT" => "ASC"),
					array("BASKET_ID" => $arBasket["ID"]),
					false,
					false,
					array("ID", "BASKET_ID", "NAME", "VALUE", "CODE", "SORT")
			);

			if ($arBasketProps = $dbBasketProps->Fetch())
			{
				do
				{
					$arProps[] = array(
						"NAME" => $arBasketProps["NAME"],
						"CODE" => $arBasketProps["CODE"],
						"VALUE" => $arBasketProps["VALUE"]
					);						
				}
				while ($arBasketProps = $dbBasketProps->Fetch());
			}
			
			$SUMM+=$arBasket["PRICE"]*$arBasket["QUANTITY"];
			
			$arFields = array(
				"PRODUCT_ID"			=> $arBasket["PRODUCT_ID"],
				"PRODUCT_PRICE_ID"		=> $arBasket["PRODUCT_PRICE_ID"],
				"PRICE"					=> $arBasket["PRICE"],
				"CURRENCY"				=> $arBasket["CURRENCY"],
				"WEIGHT"				=> $arBasket["WEIGHT"],
				"QUANTITY"				=> $arBasket["QUANTITY"],
				"LID"					=> $arBasket["LID"],
				"DELAY"					=> "N",
				"CAN_BUY"				=> "Y",
				"NAME"					=> $arBasket["NAME"],
				"CALLBACK_FUNC"			=> $arBasket["CALLBACK_FUNC"],
				"MODULE"				=> $arBasket["MODULE"],
				"NOTES"					=> $arBasket["NOTES"],
				"CANCEL_CALLBACK_FUNC"	=> $arBasket["CANCEL_CALLBACK_FUNC"],
				"ORDER_CALLBACK_FUNC"	=> $arBasket["ORDER_CALLBACK_FUNC"],
				"PAY_CALLBACK_FUNC"		=> $arBasket["PAY_CALLBACK_FUNC"],
				"DETAIL_PAGE_URL"		=> $arBasket["DETAIL_PAGE_URL"],
				"CATALOG_XML_ID" 		=> $arBasket["CATALOG_XML_ID"],
				"PRODUCT_XML_ID" 		=> $arBasket["PRODUCT_XML_ID"],
				"PROPS"					=> $arProps,
				);

			CSaleBasket::Add($arFields);
		}
	
		
		echo "ok";
	
	
	}


}




function del_item(){
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	$ELEMENT_ID = str_replace("#", "", $_REQUEST["ID"]);
	if(!check_element_perm($ELEMENT_ID)) die("Permission denied");
	//CIBlockElement::Delete($ELEMENT_ID);
}

if($_REQUEST["act"]=="repeat") repeat();


?>
