<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>

<?if($arResult["bDescPageNumbering"] === false):?>
        <?if(!$_REQUEST["page"]):?>
            <span class="num">&nbsp;<?=GetMessage("TOP_SPEC")?>&nbsp;</span>
        <?else:?>
            <a class="another nav" href="<?=$arResult["sUrlPath"]?>"><?=GetMessage("TOP_SPEC")?></a>
        <?endif?>
	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
		<?if($arResult["nStartPage"] == $arResult["NavPageNomer"] && $_REQUEST["page"]):?>
            <span class="num">&nbsp;&nbsp;<?=$arResult["nStartPage"]?>&nbsp;&nbsp;</span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false && $_REQUEST["page"]):?>
			<a class="another nav" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>?page=<?=$arResult["nStartPage"]?><?=($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "")?><?=($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "")?>"><?=$arResult["nStartPage"]?></a>
		<?else:?>
			<a class="another nav" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>page=<?=$arResult["nStartPage"]?><?=($_REQUEST["pageRestCnt"] ? "&pageRestCnt=".$_REQUEST["pageRestCnt"] : "")?><?=($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "")?>"><?=$arResult["nStartPage"]?></a>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

<?endif?>