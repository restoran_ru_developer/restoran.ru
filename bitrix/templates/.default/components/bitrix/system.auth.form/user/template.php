<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["FORM_TYPE"] == "login"):?>
    <?
    $arParamsToDelete = array(
	"login",
	"logout",
	"register",
	"forgot_password",
	"change_password",
	"confirm_registration",
	"confirm_code",
	"confirm_user_id",
	"logout_butt",
	"auth_service_id",
        "CITY_ID"
    );
    $currentUrl = $APPLICATION->GetCurPageParam("", $arParamsToDelete);
    ?>
    <ul class="small" style="margin-top:2px;">        
        <li class="left" id="enter" style="margin-right:15px;"><a href="/tpl/ajax/auth.php?backurl=<?=$currentUrl?>" id="main_auth_form_auth" style="text-decoration:none" class="blueb"><?=GetMessage("ENTER_LINK")?></a></li>
        <li class="left" style="line-height:16px;"><a style="color:#24A6CF" href="/auth/register.php"><?=GetMessage("REGISTER_LINK")?></a></li>
    </ul>
<?
//if($arResult["FORM_TYPE"] == "login")
else:
?>
<ul class="small" style="margin-top:25px; width:64px">
    <li align="right">
    	<?if($arResult["UNREAD"]>0){?><span class="unread"><?=$arResult["UNREAD"];?></span><?}?>
        <div class="radius" style="background:url(<?=$arResult["USER_PHOTO"]['src']?>) left top no-repeat">
            <?if (CSite::InGroup(Array(9))):?>
                <a href="/restorator/" /></a>
            <?else:?>
                <a href="/users/id<?=$USER->GetID()?>/?CITY_ID=<?=CITY_ID?>" /></a>
            <?endif;?>
        </div>
    </li>
</ul>
<ul class="small2">    
    <li align="right" style="text-align: center; width:83px;overflow:hidden">
        <?if (CSite::InGroup(Array(9))):?>
            <a href="/restorator/"><b><? if($arResult["USER_LOGIN"]!="") echo  $arResult["USER_LOGIN"]; else echo $arResult["USER_NAME"]?></b></a>
        <?else:?>
            <a href="/users/id<?=$USER->GetID()?>/?CITY_ID=<?=CITY_ID?>"><b><? if($arResult["USER_LOGIN"]!="") echo  $arResult["USER_LOGIN"]; else echo $arResult["USER_NAME"]?></b></a>
        <?endif;?>
    </li>
    <li align="center" style="width:83px; overflow:hidden">
		<div class="logout-button-wr">
			<div class="logout-button-data">
				<a style="position:relative" class="greyb" href="<?=$APPLICATION->GetCurPageParam("logout=yes");?>"><?=GetMessage("LOGOUT")?></a>
			</div>
		</div>
	</li>
</ul>
<?endif?>

<?
//var_dump($arResult);
?>