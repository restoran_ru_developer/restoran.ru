<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div align="right">
    <a class="modal_close uppercase white" href="javascript:void(0)"></a>
</div>
<?if($arResult["FORM_TYPE"] == "login"):?>
    <div class="ajax_form">
    <script>
        function success_authorize(a,b)
        {
            if (!b)
            {                
                $(".popup").fadeOut(300);
                location.reload();                
            }
        }
    </script>
    <!--<div class="title" style="border:0px"><?echo GetMessage("AUTH_TITLE")?></div>-->
    <?
    if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
            ShowMessage($arResult['ERROR_MESSAGE']);
    ?>

    <form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="/auth/?login=yes">
    <?/*if($arResult["BACKURL"] <> ''):?>
            <input type="hidden" name="backurl" value="/" />
    <?endif*/?>
    <?if($_REQUEST["backurl"]):?>
            <input type="hidden" name="backurl" value="<?=$_REQUEST["backurl"]?>" />
    <?endif?>
    <?foreach ($arResult["POST"] as $key => $value):?>
            <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
    <?endforeach?>
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <div class="question">
                    <?=GetMessage("AUTH_LOGIN")?><br />
                    <input class="inputtext" type="text" placeholder="E-mail" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" style="width:280px" />
            </div>
            <div class="question">
                <?=GetMessage("AUTH_PASSWORD")?><br />
                <input class="inputtext" type="password" name="USER_PASSWORD" maxlength="255"  size="40" style="width:280px" />
            </div>
            <div class="">
                <table>
                    <tr>
                        <td><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" /></td>
                        <td><label for="USER_REMEMBER">&nbsp;<?=GetMessage("AUTH_REMEMBER_ME")?></label></td>
                    </tr>
                </table>                
            </div>     
            <?if ($arResult["CAPTCHA_CODE"]):?>
            <div class="question">
                <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
                <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                <div style="margin-bottom:5px;"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                <input class="inputtext" type="text" name="captcha_word" maxlength="50" value="" style="width:250px" />
            </div>
            <?endif?>
            <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                <noindex>
                    <div class="left">
                        <p><a class="another font14" style="margin:0px;font-size:14px;" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></p>
                    </div>
                    <div class="right">
                        <p><a class="another font14" style="margin:0px;font-size:14px;" href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a></p>
                    </div>
                    <div class="clear"></div>
                </noindex>
            <?endif?>
            <div align="center">
                <input type="hidden" value="<?=GetMessage("AUTH_AUTHORIZE")?>" name="Login" /> 
                <input type="submit"  class="light_button" style="width:150px" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
            </div>
    </form>
    <script type="text/javascript">
    <?if (strlen($arResult["LAST_LOGIN"])>0):?>
    try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
    <?else:?>
    try{document.form_auth.USER_LOGIN.focus();}catch(e){}
    <?endif?>
    </script>
    <!--<div class="bx-auth-title">Войти как пользователь</div>-->
    <div class="bx-auth-note" style="line-height:16px;margin:10px 0px;" align="center"><?=GetMessage("AUTH_ALSO")?></div>
    <?$APPLICATION->AddHeadString('<link href="/bitrix/components/restoran/user.vkontakte_auth/style.css"  type="text/css" rel="stylesheet" />', true)?>
    <?$APPLICATION->AddHeadScript('/bitrix/components/restoran/user.vkontakte_auth/script.js')?>
    <div style="float: left; margin-bottom:5px">
        <?$APPLICATION->IncludeComponent("restoran:user.vkontakte_auth", ".default", array(
            ),
            false
        );?>
    </div>
    <?$APPLICATION->AddHeadString('<link href="/bitrix/components/restoran/user.facebook_auth/style.css"  type="text/css" rel="stylesheet" />', true)?>
    <?$APPLICATION->AddHeadScript('/bitrix/components/restoran/user.facebook_auth/script.js')?>
    <div style="float: right;margin-bottom:5px">
        <?$APPLICATION->IncludeComponent("restoran:user.facebook_auth", ".default", array(
            ),
            false
        );?>
    </div>
    <div class="clear"></div>
    </div>
<?endif;?>