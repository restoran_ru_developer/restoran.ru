<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $current,$last?>
<?if (!empty($arResult)):?>
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
    ?>
    <?if ($arItem["PARAMS"]["bull"]=="Y"):?>
        <li class="bull"></li>
    <?endif;?>
        <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
      <?if ($arItem==end($arResult)):?>
        <li class="bull"></li>
    <?endif;?>          
    <?endforeach?>
<?endif?>