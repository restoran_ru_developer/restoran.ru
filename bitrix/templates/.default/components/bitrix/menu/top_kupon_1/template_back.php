<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $current,$last?>
<?if (!empty($arResult)):?>
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
    ?>
    <?if ($arItem["PARAMS"]["bull"]=="Y"):?>
        <li class="bull"></li>
    <?endif;?>
        <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?><sup>
                    <?if ($arItem["PARAMS"]["time"]=="current"):
                        echo $current;
                    endif;?>
                    <?if ($arItem["PARAMS"]["time"]=="last"):
                        echo $last;
                    endif;?>
                </sup></a></li>
      <?if ($arItem==end($arResult)):?>
        <li class="bull"></li>
    <?endif;?>          
    <?endforeach?>
<?endif?>