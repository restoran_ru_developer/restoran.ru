<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<ul class="another_menu">
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
    ?>
        <li><a class="font18 another <?=($arItem["SELECTED"]=="Y")?"active":''?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
    <?endforeach?>
</ul>
<?endif?>