<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="clear"></div>
    <div class="left" style="width:150px;">
<ul class="left_menu_tabs" ajax="ajax" history="true" ajax_url="/content/personal/<?=RestUsers::getPersonalLink()?>/blog/">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
        $arItem["LINK"]=str_replace("/content/personal/".RestUsers::getPersonalLink()."/blog/","",$arItem["LINK"])
?>
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>

     </div>
    <div class="left" style="width:828px;">
        <div class="left_menu_panes">
        	<?foreach($arResult as $arItem):?>
            <div class="left_menu_pane"></div>
            <?endforeach?>
        </div>
    </div>
    <div class="clear"></div>
    
<?endif?>