<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <li class="bull"></li>
    <?
    foreach($arResult as $key=>$arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
    ?>
        <li <?=($arItem["SELECTED"])?"class='selected'":""?> <?=($key==0)?"style='margin:0px 10px;'":"style='margin:0px 7px;'"?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
    <?endforeach?>
    <li class="bull"></li>
<?endif?>