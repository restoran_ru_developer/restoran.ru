<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<noindex>
    <form action="/<?=CITY_ID?>/articles/8marta/search/" method="get" name="rest_filter_form">       
        <div class="search_block_new left">        
            <div class="left" id="title-search">
                <?if (CITY_ID=="spb"):?>
                    <input id="search_input" x-webkit-speech="" speech="" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" />
                <?else:?>
                    <input id="search_input" x-webkit-speech="" speech="" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q']:"")?>" />
                <?endif;?>            
            </div>            
            <div class="clear"></div>        
        </div>    
        <div class="right">
                <input id="search_submit_new" type="submit" value=""/>
            </div>
        <div class="clear"></div>
    </form>
</noindex>