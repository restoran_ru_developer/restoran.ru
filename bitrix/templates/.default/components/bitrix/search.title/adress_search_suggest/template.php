<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="" method="get" name="rest_filter_form" id="map_adres">
    <div id="adress_search_block" class="search_block left">
        <div class="left" id="title-search">
            <input id="top_search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : GetMessage("BSF_T_SEARCH_EXMP_STRING"))?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
        </div>
        <div class="left">
            <select id="search_in" name="search_in" class="search_in">
                <option selected="selected" value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></option>
                <option value="adres"><?=GetMessage("BSF_T_WHERE_SEARCH_ADRES")?></option>
            </select>
        </div>
        <div class="right">
            <input id="search_submit" type="image" src="/tpl/images/search_button.png" />
        </div>
        <div class="clear"></div>
    </div>    
    <!--<div class="right search_button">
        <input type="submit" class="dark_button" value="Поиск" />
    </div>-->
    <div class="clear"></div>
</form>