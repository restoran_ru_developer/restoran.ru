var new_x;
var new_y;
$(document).ready(function() {
    // focus
    $(".search_block .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
        $(this).removeClass("placeholder");
        $(this).addClass("placeholder2");
    });
    // blur
    $(".search_block .placeholder").on("blur", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val().length <= 0)
            $(this).val(defVal);
        $(this).removeClass("placeholder2");
        $(this).addClass("placeholder"); 
    });
    
    // send suggest
    $("#top_search_input").autocomplete("/search/map_suggest.php", {
        delay:600,
        limit: 5,
        minChars: 3,
        cacheLength:5,
        width:970,
        selectOnly:true,
        formatItem: function(data, i, n, value) {            
            var a = value.split("###")[1];
            var x = a.split(" ")[0];
            var y = a.split(" ")[1];
            return "<a class='suggest_res_url' href='javascript:void(0)' onclick='go_adres("+x+","+y+")'> " + value.split("###")[0] + "</a>";
        },
        onItemSelect: function(value) {
              var a = value.split("###")[1];
            new_x = a.split(" ")[0];
            new_y = a.split(" ")[1];
        },
        formatResult: function(data, value) {          
            return value.split("###")[0];
        },
        extraParams: {
            search_in: function() {return $('#search_in').val();}          
        }
   	});
    $("#map_adres").submit(function(){
        if (new_x&&new_y)
        {
            if ($('#search_in').val()=="adres")
                go_adres(new_x,new_y,1);
            else
                go_adres(new_x,new_y,0);
        }
        return false;
    })
});