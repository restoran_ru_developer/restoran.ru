<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<noindex>
<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
    <div class="search_block left">
        <div class="left" id="title-search">
            <?if (CITY_ID=="spb"):?>
                <input id="top_search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : GetMessage("BSF_T_SEARCH_EXMP_STRING_spb"))?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_spb")?>" />
            <?else:?>
                <input id="top_search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : GetMessage("BSF_T_SEARCH_EXMP_STRING"))?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
            <?endif;?>
        </div>
       
        <div class="left" id="search_where">
            <select id="search_in" name="search_in" class="search_in">
                <option<?if($_REQUEST["search_in"] == 'all' || !$_REQUEST["search_in"]):?> selected="selected"<?endif?> value="all"><?=GetMessage("BSF_T_WHERE_ALL")?></option>                
                <option<?if($_REQUEST["search_in"] == 'rest'):?> selected="selected"<?endif?> value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></option>
                <!--<option<?if($_REQUEST["search_in"] == 'dostavka'):?> selected="selected"<?endif?> value="dostavka"><?=GetMessage("BSF_T_WHERE_SEARCH_DOSTAVKA")?></option>-->
                <option<?if($_REQUEST["search_in"] == 'reviews'):?> selected="selected"<?endif?> value="reviews"><?=GetMessage("BSF_T_WHERE_SEARCH_REVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'blog'):?> selected="selected"<?endif?> value="blog"><?=GetMessage("BSF_T_WHERE_SEARCH_BLOG")?></option>                                
                <option<?if($_REQUEST["search_in"] == 'news'):?> selected="selected"<?endif?> value="news"><?=GetMessage("BSF_T_WHERE_SEARCH_NEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'afisha'):?> selected="selected"<?endif?> value="afisha"><?=GetMessage("BSF_T_WHERE_SEARCH_AFISHA")?></option>
                <option<?if($_REQUEST["search_in"] == 'recipe'):?> selected="selected"<?endif?> value="recipe"><?=GetMessage("BSF_T_WHERE_SEARCH_RECIPE")?></option>
                <!--<option<?if($_REQUEST["search_in"] == 'master_classes'):?> selected="selected"<?endif?> value="master_classes"><?=GetMessage("BSF_T_WHERE_SEARCH_MASTER_CLASS")?></option>                
                <option<?if($_REQUEST["search_in"] == 'overviews'):?> selected="selected"<?endif?> value="overviews"><?=GetMessage("BSF_T_WHERE_SEARCH_OVERVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'interview'):?> selected="selected"<?endif?> value="interview"><?=GetMessage("BSF_T_WHERE_SEARCH_INTERVIEWS")?></option>                   -->
                <!--<option<?if($_REQUEST["search_in"] == 'kupons'):?> selected="selected"<?endif?> value="kupons"><?=GetMessage("BSF_T_WHERE_SEARCH_KUPONS")?></option>-->
            </select>
        </div>
         <div class="right">
            <input id="search_submit" type="image" src="/tpl/images/search_button.png" />
        </div>
        <div class="clear"></div>
        
    </div>    
    <div id="chec" class="left" style="width:160px; height:28px; background: url(/tpl/images/filter_border.png) right center no-repeat; background:#F2F2F2; border: 5px solid #F2F2F2; padding-top:1px">
        <table>
            <tr>
                <td>
                    <span class="niceCheck2">
                        <input type="checkbox" name="by_name" value="Y" <?=($_REQUEST["by_name"])?"checked":""?> id="by_name" />
                    </span>                    
                </td>
                <td>
                    <label class="niceCheck2Label" for="by_name"> Искать по названию</label>
                </td>
            </tr>
        </table>                        
    </div>
    <!--<div class="right search_button">
        <input type="submit" class="dark_button" value="Поиск" />
    </div>-->
    <div class="clear"></div>
</form>
    </noindex>