$(document).ready(function() {
        // focus
    $(".search_block .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
        $(this).removeClass("placeholder");
        $(this).addClass("placeholder2");
    });
    // blur
    $(".search_block").on("blur",".placeholder2", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val()=="")
            $(this).attr("value",defVal);
        $(this).removeClass("placeholder2");
        $(this).addClass("placeholder");        
    });
    
    // send suggest
    $("#top_search_input").autocomplete("/search/index_suggest.php", {
        limit: 10,
        minChars: 3,
        selectFirst:false,
        formatItem: function(data, i, n, value) {
            return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        },
        extraParams: {
            search_in: function() { return $('#search_in').val(); }
            /*cuisine: function() {
                return $('input[name="cuisine[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            average_bill: function() {
                return $('input[name="average_bill[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            subway: function() {
                return $('input[name="subway[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            subway: function() {
                return $('#cuselMultiple-scroll-multi2 > span.cuselMultipleActive').map(function() {
                    return $(this).attr('value');
                }).get().join(',');
            }*/
        }
   });
      // go to suggest result
    $("#top_search_input").result(function(event, data, formatted) {
        if (data) {
            location.href = value.split("###")[1];
        }
   	});
});


jQuery(document).ready(function(){
                jQuery("body").on('click',".niceCheck2", function() {
                    changeCheck2(jQuery(this));
                });

                jQuery("body").on('click','.niceCheck2Label', function() {
                    changeCheck2(jQuery(this).parent().parent().find(".niceCheck2"));
                });
                jQuery(".niceCheck2").each(
                function() {

                    changeCheckStart2(jQuery(this));

                });

            });

            function changeCheck2(el)
            {
                var el = el,
                    input = el.find("input").eq(0);
                    if(!input.attr("checked")) {
                            el.css("background-position","0 -19px");	
                            input.attr("checked", true)
                    } else {
                            el.css("background-position","0 0");	
                            input.attr("checked", false)
                    }
                return true;
            }

            function changeCheckStart2(el)
            {
                var el = el,
                            input = el.find("input").eq(0);
                if(input.attr("checked")) {
                            el.css("background-position","0 -19px");	
                            }
                return true;
            }