$(document).ready(function(){
    $("#top_search_input2").autocomplete("/search/rest_bron.php", {
        limit: 5,
        minChars: 3,
        formatItem: function(data, i, n, value) {
            return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        },
        onItemSelect: function(value) {
            
              var a = value.split("###")[1];
              $("#top_search_rest2").attr("value",a);
        },
        extraParams: {
            //search_in: function() { return $('#search_in').val(); },            
        }
   	});
});