<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
    <div class="search_block left">
        <div class="left" id="title-search">
            <input id="top_search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : GetMessage("BSF_T_SEARCH_EXMP_STRING"))?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
        </div>
        <!--<div class="right">
            <select id="search_in" name="search_in" class="search_in">
                <option<?if($_REQUEST["search_in"] == 'rest' || !$_REQUEST["search_in"]):?> selected="selected"<?endif?> value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></option>
                <option<?if($_REQUEST["search_in"] == 'kupons'):?> selected="selected"<?endif?> value="kupons"><?=GetMessage("BSF_T_WHERE_SEARCH_KUPONS")?></option>
                <option<?if($_REQUEST["search_in"] == 'afisha'):?> selected="selected"<?endif?> value="afisha"><?=GetMessage("BSF_T_WHERE_SEARCH_AFISHA")?></option>
                <option<?if($_REQUEST["search_in"] == 'news'):?> selected="selected"<?endif?> value="news"><?=GetMessage("BSF_T_WHERE_SEARCH_NEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'recipe'):?> selected="selected"<?endif?> value="recipe"><?=GetMessage("BSF_T_WHERE_SEARCH_RECIPE")?></option>
                <option<?if($_REQUEST["search_in"] == 'master_classes'):?> selected="selected"<?endif?> value="master_classes"><?=GetMessage("BSF_T_WHERE_SEARCH_MASTER_CLASS")?></option>
                <option<?if($_REQUEST["search_in"] == 'blog'):?> selected="selected"<?endif?> value="blog"><?=GetMessage("BSF_T_WHERE_SEARCH_BLOG")?></option>
                <option<?if($_REQUEST["search_in"] == 'overviews'):?> selected="selected"<?endif?> value="overviews"><?=GetMessage("BSF_T_WHERE_SEARCH_OVERVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'interview'):?> selected="selected"<?endif?> value="interview"><?=GetMessage("BSF_T_WHERE_SEARCH_INTERVIEWS")?></option>   
                <option<?if($_REQUEST["search_in"] == 'all'):?> selected="selected"<?endif?> value="all"><?=GetMessage("BSF_T_WHERE_ALL")?></option>
            </select>
        </div>-->
        <div class="clear"></div>
    </div>    
    <!--<div class="right search_button">
        <input type="submit" class="dark_button" value="Поиск" />
    </div>-->
    <div class="clear"></div>
</form>