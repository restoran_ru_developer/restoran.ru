$(document).ready(function() {
    // focus
    $(".search_block .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
        $(this).removeClass("placeholder");
        $(this).addClass("placeholder2");
    });
    // blur
    $(".search_block .placeholder2").on("blur", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val().length <= 0)
            $(this).val(defVal);
        $(this).removeClass("placeholder2");
        $(this).addClass("placeholder");        
    });
    
    // send suggest
    $("#top_search_input").autocomplete("/search/index_suggest.php", {
        limit: 5,
        minChars: 3,
        formatItem: function(data, i, n, value) {
            return "<a class='suggest_res_url' href='" + value.split("###")[1] + "'> " + value.split("###")[0] + "</a>";
        },
        formatResult: function(data, value) {
            return value.split("###")[0];
        },
        extraParams: {
            search_in: function() { return $('#search_in').val(); },
            /*cuisine: function() {
                return $('input[name="cuisine[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            average_bill: function() {
                return $('input[name="average_bill[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            subway: function() {
                return $('input[name="subway[]"]').map(function() {
                    return $(this).val();
                }).get().join(',');
            },
            subway: function() {
                return $('#cuselMultiple-scroll-multi2 > span.cuselMultipleActive').map(function() {
                    return $(this).attr('value');
                }).get().join(',');
            }*/
        }
   });
   // go to suggest result
    $("#top_search_input").result(function(event, data, formatted) {
        if (data) {
            location.href = value.split("###")[1];
        }
   	});
});
