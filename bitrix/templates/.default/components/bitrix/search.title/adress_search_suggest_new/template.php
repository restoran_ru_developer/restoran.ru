<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<noindex>
    <form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form" id="map_adres"> 
        <div id="adress_search_block" class="search_block_new left">        
            <div class="left" id="search_by">
                <select id="by_name" name="search_in" class="by_name">
                    <option selected="selected" value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></option>
                    <option value="adres"><?=GetMessage("BSF_T_WHERE_SEARCH_ADRES")?></option>
                </select>
            </div>
            <div class="left" id="title-search">
                <?if (CITY_ID=="spb"):?>
                    <input id="search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : '')?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING_spb")?>" />
                <?else:?>
                    <input id="search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : '')?>" placeholder="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
                <?endif;?>            
            </div>              
            <div class="right">
                <input id="search_submit_new" type="submit" value="поиск"/>
            </div>
            <div class="clear"></div>

        </div>            
        <div class="clear"></div>
    </form>
</noindex>