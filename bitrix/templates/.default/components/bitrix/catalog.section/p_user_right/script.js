$(document).ready(function() {
	$(".right_side .tbs li:not(.active) a").live("click", function(){
		var id = $(this).attr("name");
		
		$(".right_side .tbs li").removeClass("active");
		$(this).parent("li").addClass("active");
		
		$(".tabs_cont ul").hide();
		
		$("#"+id).show();
			
		return false;
	});

});
