<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="tlist">
<?
$i=0;
?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement){?>			
<?
$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array('width'=>232, 'height'=>169), BX_RESIZE_IMAGE_PROPORTIONAL, true);    
	//var_dump($arElement);
?>			
			
<div class="bl <? if($i==count($arResult["ITEMS"])-1) echo 'last';?>" id="<?=$this->GetEditAreaId($arElement['ID']);?>" >
	<div class="name"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a> <div class="stars"></div></div>
	
	<div class="pic">
		<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$file["src"]?>" alt="<?=$arElement["NAME"]?>"/></a>
		<div class="restics_bg"><span><?=intval($arElement["PRICES"]["BASE"]["VALUE"])?></span></div>
	</div>
					
	<div class="anons">
		<div class="txt">
			<?=$arElement["PREVIEW_TEXT"]?>
		</div>
		<?if($arParams["MY_SCHET"]>=$arElement["PRICES"]["BASE"]["VALUE"]){?>
			<a href="<?=$templateFolder?>/core.php?act=add2basket&ID=<?=$arElement["ID"]?>" class="buy_lnk">приобрести</a>
		<?}else{?>
		<?
		$razn = $arElement["PRICES"]["BASE"]["VALUE"]- $arParams["MY_SCHET"];
		?>
		<span class="buy_lnk">Вам не хватает <i><?=$razn?></i> restиков</span>
		<?}?>
	</div>	
	<div class="clear"></div>
</div>
<?
$i++;
?>			
<?}?>						
</div>