$(document).ready(function() {
	$("a.buy_lnk").click(function(){
		
		if (!$("#promo_modal").size()){
			$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
		}

		var lnk = $(this).attr("href");
		
		var str ='<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div><div class="ln">Вы уверены, что хотите приобрести этот подарок?</div>';
		str +='<input class="light_button buy_prezent" type="submit" name="web_form_submit" value="Да" href="'+lnk+'"><input class="dark_button popup_close" type="button" value="Нет">';
		
					
		$('#promo_modal').html(str);
		showOverflow();
		setCenter($("#promo_modal"));
		$("#promo_modal").fadeIn("300"); 
		
		return false;
	});
	
	
	$(".popup_close").live("click", function(){
		$(this).parent(".popup_modal").fadeOut(300);
        hideOverflow();
		return false;
	});
	
	
	$(".buy_prezent").live("click", function(){
		var lnk = $(this).attr("href");                
		$.post(lnk,function(html){
                    $('#promo_modal').html(html);
                    setTimeout("window.location.reload()",1500);
		});
		return false;
	});
});
