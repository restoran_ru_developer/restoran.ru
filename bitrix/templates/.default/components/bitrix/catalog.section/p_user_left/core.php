<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");


function add2basket_prize(){
	global $USER;
	global $DB;
	
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("sale")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//Очищаем корзину
	CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
	
	//Добавляем подарок в корзину
	Add2BasketByProductID($_REQUEST["ID"]);
        echo "Ваша заявка отправлена. С вами свяжется наш администратор.";
	
	//Считаем сумму заказа
	$arSelFields = array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "NAME", "CURRENCY", "CATALOG_XML_ID", "VAT_RATE", "NOTES", "DISCOUNT_PRICE");
	$dbBasketItems = CSaleBasket::GetList(
		array("NAME" => "ASC"),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		$arSelFields
	);
	while ($arBasketItems = $dbBasketItems->GetNext()){
		$arBasketItems["PRICE"] = roundEx($arBasketItems["PRICE"], SALE_VALUE_PRECISION);
		$arBasketItems["QUANTITY"] = DoubleVal($arBasketItems["QUANTITY"]);
		
		$arResult["ORDER_PRICE"] += $arBasketItems["PRICE"] * $arBasketItems["QUANTITY"];
	}

	//Получаем личный счет пользователя
	$dbUserAccount = CSaleUserAccount::GetList(array(),array("USER_ID" => $USER->GetID(),"CURRENCY" => "RUB"));
	$arUserAccount = $dbUserAccount->GetNext();
	$arResult["USER_ACCOUNT"] = $arUserAccount;
	
	$withdrawSum = 0.0;
	if(DoubleVal($arResult["USER_ACCOUNT"]["CURRENT_BUDGET"]) >= DoubleVal($arResult["ORDER_PRICE"])){
		//если у пользователя достаточно денег, то создаем заказ
		$arFields = array(
			"LID" => SITE_ID,
			"PERSON_TYPE_ID" => 4,
			"PAYED" => "N",
			"CANCELED" => "N",
			"STATUS_ID" => "N",
			"PRICE" => $arResult["ORDER_PRICE"],
			"CURRENCY" => "RUB",
			"USER_ID" => IntVal($USER->GetID()),
			"PAY_SYSTEM_ID" => 8
		);
	
	
		$arResult["ORDER_ID"] = CSaleOrder::Add($arFields);
		$arResult["ORDER_ID"] = IntVal($arResult["ORDER_ID"]);

		if ($arResult["ORDER_ID"]>0){
			CSaleBasket::OrderBasket($arResult["ORDER_ID"], CSaleBasket::GetBasketUserID(), SITE_ID, false);		
		}
	
	
	
		//Оплачиваем подарок с личного счета
		$withdrawSum = CSaleUserAccount::Withdraw(
			$USER->GetID(),
			$arResult["ORDER_PRICE"],
			"RUB",
			$arResult["ORDER_ID"]
		);

		if($withdrawSum > 0){
			$arFields = array("SUM_PAID" => $withdrawSum,"USER_ID" => $USER->GetID());

			CSaleOrder::Update($arResult["ORDER_ID"], $arFields);
			if ($withdrawSum == $arResult["ORDER_PRICE"]) CSaleOrder::PayOrder($arResult["ORDER_ID"], "Y", False, False);
		}
	}
			
	
	
	//Отправка сообщения 
	if ($arResult["ORDER_ID"]>0){
		$strOrderList = "";
		$dbBasketItems = CSaleBasket::GetList(
			array("NAME" => "ASC"),
			array("ORDER_ID" => $arResult["ORDER_ID"]),
			false,
			false,
			array("ID", "NAME", "QUANTITY", "PRICE", "CURRENCY")
		);
		
		while ($arBasketItems = $dbBasketItems->Fetch()){
			$strOrderList .= $arBasketItems["NAME"]." - ".$arBasketItems["QUANTITY"]." ".GetMessage("SOA_SHT").": ".SaleFormatCurrency($arBasketItems["PRICE"], $arBasketItems["CURRENCY"]);
			$strOrderList .= "\n";
		}

		
		$arFields = Array(
			"ORDER_ID" => $arResult["ORDER_ID"],
			"ORDER_DATE" => Date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT", SITE_ID))),
			"ORDER_USER" => ( (strlen($arUserResult["PAYER_NAME"]) > 0) ? $arUserResult["PAYER_NAME"] : $USER->GetFullName() ),
			"PRICE" => SaleFormatCurrency($totalOrderPrice, $arResult["BASE_LANG_CURRENCY"]),
			"BCC" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
			"EMAIL" => (strlen($arUserResult["USER_EMAIL"])>0 ? $arUserResult["USER_EMAIL"] : $USER->GetEmail()),
			"ORDER_LIST" => $strOrderList,
			"SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
			"DELIVERY_PRICE" => $arResult["DELIVERY_PRICE"],
		);
		
		$eventName = "SALE_NEW_ORDER";

		$bSend = true;
		$db_events = GetModuleEvents("sale", "OnOrderNewSendEmail");
		while ($arEvent = $db_events->Fetch())
			if (ExecuteModuleEventEx($arEvent, Array($arResult["ORDER_ID"], &$eventName, &$arFields))===false)
				$bSend = false;

			if($bSend){
				$event = new CEvent;
				$event->Send($eventName, SITE_ID, $arFields, "N");
			}
	}	
}



if($_REQUEST["act"]=="add2basket") add2basket_prize();
?>