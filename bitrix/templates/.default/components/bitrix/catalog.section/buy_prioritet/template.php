<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $cell=>$arElement){?>
<?
$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));


?>
<div class="block-premium-bottom" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
    <p class="upcase"><?=$arElement["NAME"]?></p>
    <p class="colorize"><?=$arElement["PREVIEW_TEXT"]?> <span><?=$arElement["PRICES"]["BASE"]["VALUE"]?></span> Руб./месяц</p>
    <input type="button" class="light_button" value="+ Приобрести" />
</div>

<?
//v_dump($arElement["PRICES"]["BASE"]);
?>
<?}?>