$(document).ready(function(){
    $.tools.validator.localize("ru", {
        '*'	: 'Поле обязательно для заполнения',
    	'[req]'	: 'Поле обязательно для заполнения'
    });
    $.tools.validator.fn("[req=req]", "Поле обязательно для заполнения", function(input, value) { 
        if (!value)
            return false;
        return true;
    });
    $(".phone").maski("(999) 999-99-99");
    $(".ajax_form > form").validator({ lang: 'ru',messageClass: 'error_text_message' }).submit(function(e) {
        var form = $(this);
        if (!e.isDefaultPrevented()) {
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $('.ok').fadeIn(500);
                    form.parent().html(data);                    
                    setTimeout("$('.ok').fadeOut(500)",10000);
                }
            });
            e.preventDefault();
        }
    });
    $(".ajax_form > form").bind("onFail", function(e, errors)  {
	if (e.originalEvent.type == 'submit') {
		$.each(errors, function()  {
			var input = this.input;	                        
		});
	}
    });
});