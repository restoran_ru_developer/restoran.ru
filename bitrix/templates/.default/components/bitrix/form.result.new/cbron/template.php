<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<style>
    .ajax_form .question
    {
        margin-bottom:5px!important;
    }
</style>
<?=$arResult["FORM_HEADER"]?>
<?if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y"):?>
    <?if ($arResult["isFormTitle"]):?>
        <div class="title" style="text-align: right; border:0px">
            <?//=$arResult["FORM_TITLE"]?>
        </div>
    <?endif;?>
<?endif;?>
<?//v_dump($arResult["QUESTIONS"])?>
<div class="ok">
    <?=$arResult["FORM_NOTE"]?>
</div>
<?if ($arResult["isFormErrors"] == "Y"):?>
    <?=$arResult["FORM_ERRORS_TEXT"];?>
<?endif;?>

<?if(!$arResult["FORM_NOTE"]):?>

    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion): ?>
        <div class="question">
            <?=$arQuestion["CAPTION"]?> <?=$arResult["REQUIRED_SIGN"];?><br />
            <?=$arQuestion["HTML_CODE"]?><br />
        </div>
    <?endforeach;?>
    <?if($arResult["isUseCaptcha"] == "Y"): ?>
        <!--<b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b><br />-->
        <?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?><br />
        <table>
            <tr>
                <td><input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" />        </td>
                <td><input type="text" name="captcha_word" size="20" maxlength="50" value="" class="inputtext" /><br /></td>
            </tr>
        </table>                
    <?endif; ?>
    <input type="hidden" name="web_form_apply" value="Y" /><br />
    <div align="center"><input class="light_button" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"];?>" /></div>
    <!--<input class="dark_button popup_close" type="button" value="<?=GetMessage("FORM_CLOSE")?>"/>-->

<?endif?>

<?=$arResult["FORM_FOOTER"]?>
