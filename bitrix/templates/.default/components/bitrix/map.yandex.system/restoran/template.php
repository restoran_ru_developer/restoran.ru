<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<script type="text/javascript">
if (!window.GLOBAL_arMapObjects)
	window.GLOBAL_arMapObjects = {};

function init_<?echo $arParams['MAP_ID']?>(context) 
{
	if (null == context)
		context = window;

	//if (!context.ymaps)
	//	return;
	//console.log(context.ymaps.Map);
	//window.GLOBAL_arMapObjects['<?echo $arParams['MAP_ID']?>'] = new context.ymaps.Map(context.document.getElementById("BX_YMAP_<?echo $arParams['MAP_ID']?>"));
	if (null == ymaps.Map)
        {
            //ymaps.load(init_<?echo $arParams['MAP_ID']?>);
            console.log("yandex fail");
            setTimeout("init_rest_<?=$_REQUEST["ID"]?>()",1000);
            return false;
        }
        
	<? if($arParams['PL']["LON"]!=""){ ?>
	window.GLOBAL_arMapObjects['<?echo $arParams['MAP_ID']?>'] = new ymaps.Map("BX_YMAP_<?echo $arParams['MAP_ID']?>", {
        // Центр карты
        center: [<?echo $arParams['INIT_MAP_LON']?>, <?echo $arParams['INIT_MAP_LAT']?> ],
        // Коэффициент масштабирования
        zoom: <?echo $arParams['INIT_MAP_SCALE']?>,
        // Тип карты
    	}
	);
	<?}else{?>
	
	window.GLOBAL_arMapObjects['<?echo $arParams['MAP_ID']?>'] = new ymaps.Map("BX_YMAP_<?echo $arParams['MAP_ID']?>", {
        // Центр карты
        center: [<?echo $arParams['INIT_MAP_LAT']?>, <?echo $arParams['INIT_MAP_LON']?> ],
        // Коэффициент масштабирования
        zoom: <?echo $arParams['INIT_MAP_SCALE']?>,
        // Тип карты
    	}
	);
	
	<?}?>
	var map = window.GLOBAL_arMapObjects['<?echo $arParams['MAP_ID']?>'];
	
	
	map.controls.add("zoomControl").add("typeSelector");
	map.behaviors.enable("scrollZoom");
	map.behaviors.disable("dblClickZoom");
	
	map.bx_context = context;
	
	
	//map.setCenter(new context.ymaps.GeoPoint(<?echo $arParams['INIT_MAP_LON']?>, <?echo $arParams['INIT_MAP_LAT']?>), <?echo $arParams['INIT_MAP_SCALE']?>, context.ymaps.MapType.<?echo $arParams['INIT_MAP_TYPE']?>);
	
	
	
	map.events.add("dblclick",
   		function(e) {
   		


   		var pts = e.get("coordPosition");
   		var ID = $(".tabs a.current").attr("rel");
    	var lnk = $("form#rest_"+ID).attr("action");
    	var IB_ID = $("form#rest_"+ID).find("input[name=IBLOCK_ID]").val();
    	//alert(ID);
    	if(ID=="undefined" || ID==null || !ID){
    		var ID = $("input[name=ELEMENT_ID]").val();
		   	var lnk = $("form#rest_"+ID).attr("action");
			var IB_ID = $("form#rest_"+ID).find("input[name=IBLOCK_ID]").val();
	    	
    	}
    	
    	//alert(pts);    	
   		
   		
   		
   		
   		var obPlacemark = new ymaps.Placemark(pts, {iconContent: "", balloonContent: ""}, {draggable: true, hideIconOnBallon: false,
			iconImageHref: '/tpl/images/map/ico_rest.png', // картинка иконки
			iconImageSize: [27, 32], // размеры картинки
			iconImageOffset: [-15, -32] // смещение картинки
		});
		
		
		
		obPlacemark.events.add("dragend",
   			function(e) {
    			
    	    	
  				var pts = [];
		   		map.geoObjects.each(function (geoObject) {
   					pts[pts.length]=geoObject.geometry.getCoordinates();
    			});
    			if(!ajax_load){ 
                                    if (!lnk)
                                        lnk = $("#cont_form").attr("action");
                                    $.post(lnk, {pts: pts, ELEMENT_ID: ID, act:"save_dots", IBLOCK_ID: IB_ID}, function(data){
    		
    				});
    			}  		
    		},obPlacemark);	
		
                obPlacemark.events.add("dblclick",function(e) {
   			map.geoObjects.remove(obPlacemark);			
			var ID = $("input[name=ELEMENT_ID]").val();
                        var lnk = $("form#rest_"+ID).attr("action");
                        if (!lnk)
                                lnk = $("#cont_form").attr("action");
                                var IB_ID = $("form#rest_"+ID).find("input[name=IBLOCK_ID]").val();

                        var pts = [];
                                map.geoObjects.each(function (geoObject) {
                                        pts[pts.length]=geoObject.geometry.getCoordinates();	
                        });
                        if(!ajax_load){ 
                                        $.post(lnk, {pts: pts, ELEMENT_ID: ID, act:"save_dots", IBLOCK_ID: IB_ID}, function(data){


                                }); 
                                }			   		
                },obPlacemark);
                
    		map.geoObjects.add(obPlacemark);
   		
   			var pts = [];
	   		map.geoObjects.each(function (geoObject) {
   				pts[pts.length]=geoObject.geometry.getCoordinates();	
    		});
   			if (!lnk)
                                lnk = $("#cont_form").attr("action");
   			if(!ajax_load){ 
   				$.post(lnk, {pts: pts, ELEMENT_ID: ID, act:"save_dots", IBLOCK_ID: IB_ID}, function(data){
    		
    			});
    		}
    });   		
    
	
<?
foreach ($arResult['ALL_MAP_OPTIONS'] as $option => $method)
{
	if (in_array($option, $arParams['OPTIONS'])):
?>
	//map.enable<?echo $method?>();
<?
	else:
?>
	//map.disable<?echo $method?>();
<?
	endif;
}
foreach ($arResult['ALL_MAP_CONTROLS'] as $control => $method)
{
	if (in_array($control, $arParams['CONTROLS'])):
?>
	//map.addControl(new context.ymaps.<?echo $method?>());
	//map.controls.add("mapTools")
<?	
	endif;
}
if ($arParams['DEV_MODE'] == 'Y'):
?>
	context.bYandexMapScriptsLoaded = true;
<?
endif;

if ($arParams['ONMAPREADY']):
?>
	if (window.<?echo $arParams['ONMAPREADY']?>)
	{
		<?
		if ($arParams['ONMAPREADY_PROPERTY']):
		?>
		//alert(map.bx_context);
		<?echo $arParams['ONMAPREADY_PROPERTY']?> = map;
		//alert(window.<?echo $arParams['ONMAPREADY']?>);
		window.<?echo $arParams['ONMAPREADY']?>();
		<?
		else:
		?>
		//alert('qq');
		window.<?echo $arParams['ONMAPREADY']?>(map);
		<?
		endif;
		?>
	}
<?
endif;
?>	
}
<?
if ($arParams['DEV_MODE'] == 'Y'):
?>
function BXMapLoader_<?echo $arParams['MAP_ID']?>(MAP_KEY)
{
	if (null == MAP_KEY || typeof MAP_KEY == 'object')
		MAP_KEY = '<?echo $arParams['KEY']?>';		
//	if (null == window.bYandexMapScriptsLoaded)
//	{                        
//		var obMapContainer = document.getElementById("BX_YMAP_<?echo $arParams['MAP_ID']?>");
//
//		var obFrame = document.createElement('IFRAME');                
//		//obFrame.src = "/bitrix/components/bitrix/map.yandex.system/blank.php";
//		
//		obFrame.style.height = 0;
//		obFrame.style.width = 0;
//		obFrame.style.border = 'none';
//		obFrame.setAttribute('frameBorder', '0');
//		
//		obMapContainer.innerHTML = '';
//		obMapContainer.appendChild(obFrame);
//		
//		var iframeWindow = obFrame.contentWindow;
//
//		if (obFrame.contentDocument)
//			var iframeDocument = obFrame.contentDocument;
//		else
//		{
//			//alert('ee');
//			var iframeDocument = obFrame.contentWindow.document;
//			//alert('rr');
//		}
//		
//		var strOnload = 
//			('\v'=='v') ? 
//			'onreadystatechange="if(this.readyState==\'complete\'&&null!=window.ymaps){window.ymaps.load(function(){parent.init_<?echo $arParams['MAP_ID']?>(window);});}"' :
//			'onload="if(null!=window.ymaps){window.ymaps.load(function(){parent.init_<?echo $arParams['MAP_ID']?>(window);});}"';
//		
//		obFrame.style.height = '<?echo $arParams['MAP_HEIGHT'];?>';
//		obFrame.style.width = '<?echo $arParams['MAP_WIDTH'];?>';
//		
//		iframeDocument.write('<html><head><style type="text/css">body{margin:0;padding:0;overflow:hidden;font-family: Arial; font-size: 11px;}</style>' + 
//			'<'+'!--[if IE]><style type="text/css">vml\\:shape,vml\\:group{behavior: url(#default#VML);display:inline-block;}</style><'+'![endif]--'+'>'+
//			'<script type="text/javascript" charset="utf-8" src="http://api-maps.yandex.ru/<?echo $arParams['YANDEX_VERSION'];?>/?key=' + MAP_KEY + '&loadByRequire=1&wizard=bitrix&lang=ru&rnd=' + Math.random() + '" ' + strOnload + '></s' + 'cript></head><body><div id="BX_YMAP_<?echo $arParams['MAP_ID']?>" style="height: <?echo $arParams['MAP_HEIGHT'];?>; width: <?echo $arParams['MAP_WIDTH']?>;"><?echo GetMessage('MYS_LOADING');?></div></body></html>');
//		iframeDocument.close();
//	}
//	else
//	{
		ymaps.load(init_<?echo $arParams['MAP_ID']?>);
	//}
}
<?
	if (!$arParams['WAIT_FOR_EVENT']):
?>
if (window.attachEvent) // IE
	window.attachEvent("onload", BXMapLoader_<?echo $arParams['MAP_ID']?>);
else if (window.addEventListener) // Gecko / W3C
	window.addEventListener('load', BXMapLoader_<?echo $arParams['MAP_ID']?>, false);
else
	window.onload = BXMapLoader_<?echo $arParams['MAP_ID']?>;
<?
	else:
		echo CUtil::JSEscape($arParams['WAIT_FOR_EVENT']),' = BXMapLoader_',$arParams['MAP_ID'],';';
	endif;
else: // $arParams['DEV_MODE'] == 'Y'
?>
//if (window.attachEvent) // IE
//	window.attachEvent("onload", function(){init_<?echo $arParams['MAP_ID']?>()});
//else if (window.addEventListener) // Gecko / W3C
//	window.addEventListener('load', function(){init_<?echo $arParams['MAP_ID']?>()}, false);
//else
	window.onload = function(){init_<?echo $arParams['MAP_ID']?>()};
<?
endif; // $arParams['DEV_MODE'] == 'Y'
?>
</script>
<div id="BX_YMAP_<?echo $arParams['MAP_ID']?>" class="bx-yandex-map" style="height: <?echo $arParams['MAP_HEIGHT'];?>; width: <?echo $arParams['MAP_WIDTH']?>;"></div>


<?
//var_dump($arParams);
?>