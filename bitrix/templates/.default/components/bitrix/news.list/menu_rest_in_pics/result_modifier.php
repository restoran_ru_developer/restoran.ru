<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

foreach($arResult["ITEMS"] as $key=>$arItem) {
    $arFileTmp = CFile::ResizeImageGet(
        $arItem['PREVIEW_PICTURE'],
        array("width" => 235, "height" => 235),
        BX_RESIZE_IMAGE_EXACT,
        true
    );
    $arResult["ITEMS"][$key]['MENU_PREVIEW'] = $arFileTmp;
}

?>