<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CT_BNL_ELEMENT_SEE_ALL_MENU"] = "ALL MENUS";
$MESS["CT_BNL_IN_MENU"] = "The menu";
$MESS["CT_BNL_LAST_UPDATE"] = "Last update ";
?>