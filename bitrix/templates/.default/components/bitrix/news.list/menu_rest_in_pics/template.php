<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/PhotoSwipe-master/dist/photoswipe.css">
<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/PhotoSwipe-master/dist/default-skin/default-skin.css">
<script src="<?=SITE_TEMPLATE_PATH?>/js/PhotoSwipe-master/dist/photoswipe.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/PhotoSwipe-master/dist/photoswipe-ui-default.min.js"></script>

<script>
    $(function(){
        var pswpElement = document.querySelectorAll('.pswp')[0];

        // build items array
        var items = [
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            {
                src: 'http://www.restoran.ru<?=$arItem['PREVIEW_PICTURE']['SRC']?>',
                w: <?=$arItem['PREVIEW_PICTURE']['WIDTH']?>,
                h: <?=$arItem['PREVIEW_PICTURE']['HEIGHT']?>
            },
            <?endforeach;?>
        ];

        // define options (if needed)


        // Initializes and opens PhotoSwipe

//
        $('.menu-pics-wrapper a').on('click', function(){

            var options = {
                // optionName: 'option value'
                // for example:
                index: $(this).parent('.menu-pics-wrapper').index(), // start at first slide
                history: false,
                closeOnScroll: false
            };
            options.mainClass = 'pswp--minimal--dark';
            options.barsSize = {top:0,bottom:0};
            options.captionEl = false;
            options.fullscreenEl = false;
            options.shareEl = false;
            options.bgOpacity = 0.85;
            options.tapToClose = true;
            options.tapToToggleControls = false;

            var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
            return false;
        })
    })

</script>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>



<div class="in-menu-pics">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="menu-pics-wrapper">
            <a href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="fancy-menu">
                <img src="<?=$arItem['MENU_PREVIEW']['src']?>" width="<?=$arItem['MENU_PREVIEW']['width']?>" height="<?=$arItem['MENU_PREVIEW']['height']?>" alt="<?=$arItem['NAME']?>">
            </a>
        </div>
    <?endforeach;?>
</div>