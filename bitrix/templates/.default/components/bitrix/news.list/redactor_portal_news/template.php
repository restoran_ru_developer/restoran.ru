<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="blog-items">
<a  class="light_button" href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?NEW_ARTICLE=Y" >+ Написать статью</a>

<ul>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$tar = explode(" ", $arItem["DISPLAY_ACTIVE_FROM"]);
$arItem["DISPLAY_ACTIVE_FROM"] = "<em>".$tar[0]."</em> ".$tar[1]." ".$tar[2];
?>
<li class="blog-item">
				<p class="blog-top"><?if($arParams["NOT_SHOW_PARENT"]!="Y"){?><strong><?=$arSection["IBLOCK_SECTION_NAME"]?></strong>, <?}?><?=$arItem["DISPLAY_ACTIVE_FROM"]?></p>
				<p class="blog-caption"><?=$arItem["NAME"]?></p>
				<p class="blog-text"><?=$arItem["DESCRIPTION"]?></p>
				
				<p class="blog-info">
					<span class="blog-comments">Комментарии: (<a href="#"><?=intval($arSection["UF_SECTION_COMM_CNT"])?></a>)</span>
					<span class="blog-tags">Теги: 
					<?if($arItem["TAGS"]!=""){?>
						<?
						$tar = explode(",", $arItem["TAGS"]);
						$i=0;
						?>
						<?foreach($tar as $t){?><a href="#"><?=$t?></a><?
						if($tar[$i+1]!="") echo ", ";
						$i++;
						?> 
						<?}?>
					<?}?>
					</span>
					<span class="blog-more"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank">Далее</a></span>
				</p>
				<ul class="blog-actions">
					<li class="blog-rborder"><a href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?ELEMENT_ID=<?=$arItem["ID"]?>">Редактировать</a></li>
					<li><a href="<?=$templateFolder?>/core.php?act=del_item&ID=<?=$arItem["ID"]?>" rel="nofollow" class="del_item">Удалить</a></li>					
				</ul>
</li>
<?endforeach?>
</ul>
</div>
<?
///var_dump($arResult["ITEMS"]);
?>