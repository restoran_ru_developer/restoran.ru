<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="blog-items">
	<a  class="light_button" href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?IB=<?=$arParams["IBLOCK_ID"]?>" >+ Новая публикация</a>
	<ul>
	<?foreach($arResult["ITEMS"] as $arSection):?>
	<li class="blog-item">
				<p class="blog-top"><?=$arSection["DISPLAY_ACTIVE_FROM"]?></p>
				<p class="blog-caption"><?=$arSection["NAME"]?></p>
				<p class="blog-text"><?=$arSection["DESCRIPTION"]?></p>
                                <?if ($arSection["CREATED_BY"]==$USER->GetID()):?>
				<ul class="blog-actions">
					<li class="blog-rborder"><a href="<?=$arParams["ARTICLE_EDOTOR_PAGE"]?>?ID=<?=$arSection["ID"]?>&IB=<?=$arParams["IBLOCK_ID"]?>&SECTION=0">Редактировать</a></li>
					<li><a href="<?=$templateFolder?>/core.php?act=del_item&ID=<?=$arSection["ID"]?>" rel="nofollow" class="del_item">Удалить</a></li>					
                                </ul>
                                <?endif;?>
	</li>
<?endforeach?>
	
	</ul>
	
	<div class="pager">
	<?=$arResult["NAV_STRING"]?>
	</div>
</div>


<?
//echo "<pre>";
//var_dump($arResult);
//echo "</pre>";
?>