<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="top_restoraunt<?if($key == 3):?> top_restoraunt_end<?endif?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="image">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" /><br />
        </div>
        <?endif;?>
        <div class="title"><?=$arItem["NAME"]?></div>
        <div class="rating">
            <?for($i = 1; $i <= 5; $i++):?>
                <div class="small_star<?if($i <= round($arItem["REVIEWS"]["RATIO"])):?>_a<?endif?>" alt="<?=$i?>"></div>
            <?endfor?>
            <div class="clear"></div>
       </div>
       <div class="clear"></div>
       <p><?=$arItem["PREVIEW_TEXT"]?></p>
       <div class="left"><?=$arItem["REVIEWS"]["COUNT"]?> <?=pluralForm(intval($arItem["REVIEWS"]["COUNT"]), GetMessage("CT_RS_REVIEWS1"), GetMessage("CT_RS_REVIEWS2"), GetMessage("CT_RS_REVIEWS3"))?></div>
       <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_ALL_REVIEWS")?></a></div>
       <div class="clear"></div>
    </div>
    <?if ($arItem!=end($arResult["ITEMS"])):?>
            <div class="dotted"></div>
    <?endif;?>
<?endforeach;?>