if (!window.BX_YMapAddPlacemark)
{
	window.BX_YMapAddPlacemark = function(map, arPlacemark)
	{
		if (null == map)
			return false;
		
		if(!arPlacemark.LAT || !arPlacemark.LON)
			return false;
		
		var obPlacemark = new ymaps.Placemark([ arPlacemark.LON ,arPlacemark.LAT], {iconContent: "", balloonContent: arPlacemark.TEXT.replace(/\n/g, '<br />')}, {draggable: true, hideIconOnBallon: false,
                    iconImageHref: '/tpl/images/map/ico_rest.png', // картинка иконки
                    iconImageSize: [27, 32], // размеры картинки
                    iconImageOffset: [-15, -32] // смещение картинки
                });
		
		obPlacemark.events.add("dragend",
   		function(e) {
   		
			var pts =this.geometry.getCoordinates();
    		
    		var ID = $("input[name=ELEMENT_ID]").val();
    		var lnk = $("form#rest_"+ID).attr("action");
    		var IB_ID = $("form#rest_"+ID).find("input[name=IBLOCK_ID]").val();
    		//alert(pts);
    		$.post(lnk, {pts: ''+pts+'', ELEMENT_ID: ID, act:"save_dots", IBLOCK_ID: IB_ID}, function(data){
    		
    		});   		
    	},obPlacemark);	
		
    	map.geoObjects.add(obPlacemark);
		
		return obPlacemark;
	}
}

if (!window.BX_YMapAddPolyline)
{
	window.BX_YMapAddPolyline = function(map, arPolyline)
	{
		if (null == map)
			return false;
		
		if (null != arPolyline.POINTS && arPolyline.POINTS.length > 1)
		{
			var arPoints = [];
			for (var i = 0, len = arPolyline.POINTS.length; i < len; i++)
			{
				arPoints[i] = new map.bx_context.YMaps.GeoPoint(arPolyline.POINTS[i].LON, arPolyline.POINTS[i].LAT);
			}
		}
		else
		{
			return false;
		}
		
		if (null != arPolyline.STYLE)
		{
			var obStyle = new map.bx_context.YMaps.Style();
			obStyle.lineStyle = new map.bx_context.YMaps.LineStyle();
			obStyle.lineStyle.strokeColor = arPolyline.STYLE.lineStyle.strokeColor;
			obStyle.lineStyle.strokeWidth = arPolyline.STYLE.lineStyle.strokeWidth;
			
			var style_id = "bitrix#line_" + Math.random();
			
			map.bx_context.YMaps.Styles.add(style_id, obStyle);
		}
		
		var obPolyline = new map.bx_context.YMaps.Polyline(
			arPoints,
			{style: style_id, clickable: true}
		);
		obPolyline.setBalloonContent(arPolyline.TITLE);
		
		map.addOverlay(obPolyline);
		
		return obPolyline;
	}
}