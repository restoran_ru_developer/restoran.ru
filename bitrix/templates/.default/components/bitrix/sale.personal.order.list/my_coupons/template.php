<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<table class="profile-activity w100 lh24" id="my_coupons">
	<tr>
		<th>Купон</th>
		<th>Стоимость</th>
		<th>Срок действия</th>
		<th class="w-auto">&nbsp;</th>
	</tr>
	<?foreach($arResult["ORDERS"] as $val):?>		
	<tr>
		<td class="name">
			<?
			$res = CIBlockElement::GetByID($val["BASKET_ITEMS"][0]["PRODUCT_ID"]);
			if($ar_res = $res->GetNext()){
				//var_dump($ar_res);
				
				$file = CFile::ResizeImageGet($ar_res["PREVIEW_PICTURE"], array('width'=>73, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true); 
				
			}
			?>
			
			<a href="<?=$val["BASKET_ITEMS"][0]["DETAIL_PAGE_URL"]?>"><img src="<?=$file["src"]?>" width="73" height="60" /></a>	
			<?
			foreach($val["BASKET_ITEMS"] as $vval){
				if (strlen($vval["DETAIL_PAGE_URL"])>0) 
					echo '<a href="'.$vval["DETAIL_PAGE_URL"].'" target="_blank">';
				echo $vval["NAME"];
				if (strlen($vval["DETAIL_PAGE_URL"])>0) 
					echo '</a>';
			}
			?>
		</td>
		<td class="nowrap"><?=$val["BASKET_ITEMS"][0]["QUANTITY"]?> x <?=CurrencyFormat($val["BASKET_ITEMS"][0]["PRICE"], "RUB");?> </td>
		<td>3 дня</td>
		<td class="w-auto">
			<div class="activity-actions w-small">
				<a class="icon-continue" href="/content/kupon/order.php?ID=<?=$val["BASKET_ITEMS"][0]["PRODUCT_ID"]?>" target="_blank">Повторить</a>
				<? /*<a class="icon-print" href="<?=$templateFolder?>/core.php?act=print&ID=<?=$val["ORDER"]["ID"]?>">Распечатать</a> */?>
			</div>
		</td>
	</tr>				
	<?endforeach;?>		
</table>
<?if(strlen($arResult["NAV_STRING"]) > 0):?>
	<?=$arResult["NAV_STRING"]?>
<?endif?>