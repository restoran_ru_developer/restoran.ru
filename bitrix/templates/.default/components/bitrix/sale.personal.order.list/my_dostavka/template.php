<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["ORDERS"] as $val){
	if($STATUS[$val["ORDER"]["STATUS_ID"]]==""){
		$arStatus = CSaleStatus::GetByID($val["ORDER"]["STATUS_ID"]);
		$STATUS[$val["ORDER"]["STATUS_ID"]]=$arStatus["NAME"];
		
	}
	//if ($arStatus = CSaleStatus::GetByID($STATUS_ID))

}

?>
		
<table class="profile-activity w100" id="my_bron">
		<tr>
			<th class="first">Место</th>
			<th>Статус</th>
			<th>Стоимость заказа</th>
			<th>Дата и время</th>
			<th></th>
		</tr>
		
	<?foreach($arResult["ORDERS"] as $val):?>
	<tr>
		<td class="first w-new">
			<?
		//	var_dump($val);
			?>
			
			<img src="<?=$file["src"]?>" width="73" height="60" />
			<p class="name"><a href="<?=$arFields["DETAIL_PAGE_URL"]?>" target="_blank"><?=$REST_NAME?></a></p>
            <p class="profile-activity-rating">
            	<div class="rating" style="padding:0px;">
           			<?for($i = 1; $i <= 5; $i++):?>
               			<div class="small_star<?if($i<=round($arProps["ratio"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
           			<?endfor?>
           			<div class="clear"></div>
      			</div>
            </p>
            <p class="upcase">ресторан</p>
         </td>
		 <td>
		 	<p class="status"><?=$STATUS[$val["ORDER"]["STATUS_ID"]]?></p>
		</td>
		<td>
			<p class="type"><?=CurrencyFormat($val["ORDER"]["PRICE"], "RUB");?></p>
		</td>
		<td>
			<ul>
				<li><?=$val["ORDER"]["DATE_INSERT_FORMAT"]?></li>
			</ul>
		</td>
		
		<td>
		    	<div class="activity-actions w-small">
		    		<?if($arElement["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен" || $arElement["PROPERTIES"]["status"]["VALUE"]=="гости пришли"){?>
		    		<a class="icon-continue" href="<?=$templateFolder?>/core.php?act=repeat&ID=<?=$arElement["ID"]?>">Повторить</a> 
		    		<?}?>
		    		
		    		<?if(($arElement["PROPERTIES"]["guest"]["VALUE"]=="" || $DATE=="" || $TIME==":") && $arElement["PROPERTIES"]["status"]["VALUE"]!="гости отменили заказ"){?>
		    			<a class="icon-save" href="<?=$templateFolder?>/core.php?act=save&ID=<?=$arElement["ID"]?>">Сохранить</a> 
		    		<?}?>
		    		
		    		<?if($arElement["PROPERTIES"]["status"]["VALUE_ENUM_ID"]==1418 || $arElement["PROPERTIES"]["status"]["VALUE_ENUM_ID"]==1504){?>
		    			<a class="icon-delete" href="<?=$templateFolder?>/core.php?act=cancel&ID=<?=$arElement["ID"]?>">Отменить</a> 
		    		<?}?>
		    		
		    		
		    	
			</div>
		</td>
	</tr>		
	<?endforeach;?>		
</table>
<?if(strlen($arResult["NAV_STRING"]) > 0):?>
	<?=$arResult["NAV_STRING"]?>
<?endif?>