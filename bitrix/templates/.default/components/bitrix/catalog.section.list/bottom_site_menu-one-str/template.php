<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$inc_key=0;
$inner_key = 2;
?>
<div class="pull-left bottom-menu-block-wrap">
<?foreach($arResult["SECTIONS"] as $key=>$arSection):?>
    <div class="footer-menu-col">
    <?if($arSection["NAME"]=='Портал'):?>
        <noindex>
    <?endif?>
    <ul>
        <li id="<?//=$this->GetEditAreaId($arSection['ID']);?>" <?//=($arSection["ELEMENT_CNT"]?'class="dropdown"':"")?>>
            <?if(!empty($arSection["CODE"])):?>
                <a href="<?=$arSection["CODE"]?>" <?if(!empty($arSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])):?>title="<?=$arSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']?>"<?endif;?> >
                    <?=$arSection["NAME"]?>
                </a>
            <?else:?>
                <?=$arSection["NAME"]?>
            <?endif?>
            <?if ($arSection["ELEMENT_CNT"]):?>
                <ul>
                    <?foreach ($arSection["ELEMENTS"] as $item):?>
                        <li><a href="<?=$item["CODE"]?>" <?if(!empty($item['DETAIL_TEXT'])){echo 'title="'.$item['DETAIL_TEXT'].'"';}?> ><?=$item["NAME"]?></a></li>
                    <?endforeach;?>
                </ul>
            <?endif;?>
        </li>
    </ul>
    <?if($arSection["NAME"]=='Портал'):?>
        </noindex>
    <?endif?>
    </div>
<?endforeach?>
</div>