<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(is_array($arResult["SEARCH"]) && !empty($arResult["SEARCH"])):
    ?>
    <?if ($arParams["PAGE_ELEMENTS"]<=20):?>
        <div class="title"><?=GetMessage("TAGS_TITLE")?></div>
    <?endif;?>
    <noindex>
        <?foreach ($arResult["SEARCH"] as $key => $res):?>
            <?if ($arParams["PAGE_ELEMENTS"]<=20 && $key>20)break;?>
            <a href="<?=$res["URL"]?>&search_in=<?=$arParams["SEARCH_IN"]?>" style="font-size: <?=$res["FONT_SIZE"]?>px; color: #<?=$res["COLOR"]?>;px" rel="nofollow" class="another"><?=$res["NAME"]?></a>
        <?endforeach?>
    </noindex>
    <?if ($arParams["PAGE_ELEMENTS"]<=20):?>
        <br />
        <br />
        <div class="black_hr"></div>
        <div align="right">
            <?if ($arParams["SEARCH_IN"]=="recepts"):?>
                <a href="/content/tags/<?=$arParams["SEARCH_IN"]?>/" class="uppercase" style="font-style:normal"><?=GetMessage("ALL_TAGS")?></a>
            <?else:?>
                <a href="/<?=CITY_ID?>/tags/<?=$arParams["SEARCH_IN"]?>/" class="uppercase" style="font-style:normal"><?=GetMessage("ALL_TAGS")?></a>
            <?endif;?>
        </div>
        <br />
    <?endif;?>
<?endif?>