<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if(count($arResult["ITEMS"])>0){?>
		
<table class="profile-activity w100" id="my_bron">
		<tr>
			<th class="first">Место</th>
			<th>Статус</th>
			<th>Расположение</th>
			<th>Дата</th>
			<th>Время</th>
			<th>Персон</th>
		</tr>
<?
$now_date="";
$balls = getUserActionBalls("bronirovanie-stolika", $USER->GetID());

?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?
	$tar=explode(" ", $arElement["ACTIVE_FROM"]);
 	$tar2=explode(":", $tar[1]);
 	$DATE = $tar[0];
 	$TIME = $tar2[0].":".$tar2[1];
	
	if($TIME==":") $TIME="";
	
	//Берем инфу по клиенту
	$res = CIBlockElement::GetByID($arElement["PROPERTIES"]["client"]["VALUE"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps = $ob->GetProperties();
 		$CLIENT_NAME=$arProps["NAME"]["VALUE"];
 		$CLIENT_SURNAME=$arProps["SURNAME"]["VALUE"];
 	}
 	
 	//Берем инфу по ресторану
	if($arElement["PROPERTIES"]["rest"]["VALUE"]==0) $REST_NAME="подбор";
	else{
		$res = CIBlockElement::GetByID($arElement["PROPERTIES"]["rest"]["VALUE"]);
		if($ob2 = $res->GetNextElement()){
 			$arFields = $ob2->GetFields();  
 			$arProps = $ob2->GetProperties();

 			$file = CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width'=>73, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);
 			
 			$REST_NAME=$arFields["NAME"];
 		}
	}
	?>
	<tr>
		<td class="first w-new">
			<a href="<?=$arFields["DETAIL_PAGE_URL"]?>" target="_blank"><img src="<?=$file["src"]?>" width="73" height="60" /></a>
			<p class="name"><a href="<?=$arFields["DETAIL_PAGE_URL"]?>" target="_blank"><?=$REST_NAME?></a></p>
            <p class="profile-activity-rating">
            	<div class="rating" style="padding:0px;">
           			<?for($i = 1; $i <= 5; $i++):?>
               			<div class="small_star<?if($i<=round($arProps["ratio"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
           			<?endfor?>
           			<div class="clear"></div>
      			</div>
            </p>
            <p class="upcase">ресторан</p>
         </td>
		 <td>
		 	<p class="status"><?=$arElement["PROPERTIES"]["status"]["VALUE"]?></p>
			<?
			if(	($arElement["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен" ||
				$arElement["PROPERTIES"]["status"]["VALUE"]=="гости пришли") && $balls ) echo '<p>начислено<br />'.$balls.' баллов</p>';
			?>
		</td>
		<td>
			<p class="type"><?=$arElement["PROPERTIES"]["type"]["VALUE"]?></p>
			<p>«Антиквариат»<?if($arElement["PROPERTIES"]["stol"]["VALUE"]){?><br />Столик <?=$arElement["PROPERTIES"]["stol"]["VALUE"]?><?}?></p>
		</td>
		<td>
			<ul>
				<li><? if($DATE=="" && $arElement["PROPERTIES"]["status"]["VALUE"]!="гости отменили заказ"){?><input type="text" name="date" value="" class="pole pl1" /><?}else echo $DATE;?></li>
			</ul>
		</td>
		<td><?if($TIME=="" && $arElement["PROPERTIES"]["status"]["VALUE"]!="гости отменили заказ"){?><input type="text" name="time" value="" class="pole pl2 time" /><?}else echo $TIME;?></td>
		<td>
              	<p class="guests">
              		<?if($arElement["PROPERTIES"]["guest"]["VALUE"]=="" && $arElement["PROPERTIES"]["status"]["VALUE"]!="гости отменили заказ"){?>
              		<input type="text" name="guest" value="" class="pole pl3" />
              		<?}else echo $arElement["PROPERTIES"]["guest"]["VALUE"];?> 
              	</p>
		    	<div class="activity-actions w-small">
		    		<?if($arElement["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен" || $arElement["PROPERTIES"]["status"]["VALUE"]=="гости пришли"){?>
		    		<a class="icon-continue" href="<?=$templateFolder?>/core.php?act=repeat&ID=<?=$arElement["ID"]?>">Повторить</a> 
		    		<?}?>
		    		
		    		<?if(($arElement["PROPERTIES"]["guest"]["VALUE"]=="" || $DATE=="" || $TIME==":") && $arElement["PROPERTIES"]["status"]["VALUE"]!="гости отменили заказ"){?>
		    			<a class="icon-save" href="<?=$templateFolder?>/core.php?act=save&ID=<?=$arElement["ID"]?>">Сохранить</a> 
		    		<?}?>
		    		
		    		<?if($arElement["PROPERTIES"]["status"]["VALUE_ENUM_ID"]==1418 || $arElement["PROPERTIES"]["status"]["VALUE_ENUM_ID"]==1504){?>
		    			<a class="icon-delete" href="<?=$templateFolder?>/core.php?act=cancel&ID=<?=$arElement["ID"]?>">Отменить</a> 
		    		<?}?>
		    		
		    		
		    	
			</div>
		</td>
	</tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
</table>					
</div>
<?}?>

<?
//var_dump($arResult["DISPLAY_PROPERTIES"]);

?>