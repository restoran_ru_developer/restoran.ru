<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//если не авторизован сразу убиваем
if (!$USER->IsAuthorized()) die("Permission denied");

//проверяем права на запись, если права есть, то возвращаем элемент
function check_element_perm($ID){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	//собрали группы в массив
	$arGroups = $USER->GetUserGroupArray();
	foreach($arGroups as $v) $GRs[]=$v;

	$res = CIBlockElement::GetByID($ID);
	if($ob = $res->GetNextElement()){
		$ar_res = $ob->GetFields();  
 		$ar_res["PROPERTIES"] = $ob->GetProperties();
		
		//если пользователь оператор или админ
		if(in_array(16, $GRs) || $USER->IsAdmin()){
			//if(CIBlock::GetPermission($ar_res["IBLOCK_ID"])>='W') 
			//else return false;
                        return $ar_res;
		}else{                    
			//если обычный юзер
			if($ar_res["PROPERTIES"]["user"]["VALUE"]==$USER->GetID()) return $ar_res;
			else return false;
		}		
	}else return false;
}

//функция получает ID брони и копирует запись
function repeat(){
	global $USER;
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	$res = CIBlockElement::GetByID($_REQUEST["ID"]);
	if($ob = $res->GetNextElement()){
  		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		
		
		//var_dump($arProps);
		
		
		//теперь нужно сформировать массив для вставки нового элемента
		$el = new CIBlockElement;
	
		$PROP2 = array();
		$PROP2["rest"] = $arProps["rest"]["VALUE"]; 
		$PROP2["status"] = 1418; 
		$PROP2["client"] = $arProps["client"]["VALUE"]; 	
		$PROP2["source"] = 1464; 	
		$PROP2["user"] = $arProps["user"]["VALUE"]; 
		$PROP2["type"] = $arProps["type"]["VALUE_ENUM_ID"]; 
		
		
		$arLoadProductArray2 = Array(
  			"IBLOCK_SECTION" => false,  
  			"IBLOCK_ID" => 105,         
  			"PROPERTY_VALUES"=> $PROP2,
  			"NAME"           => $arFields["NAME"],
  			"ACTIVE"         => "Y",      
  			"PREVIEW_TEXT"   => $arFields["PREVIEW_TEXT"],
  		);
		
		$EXIT["ORDER_ID"] = $el->Add($arLoadProductArray2);
		//var_dump($arFields);	
		

	}
}


function cancel(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	if($EL = check_element_perm($_REQUEST["ID"])){
		if($EL["PROPERTIES"]["status"]["VALUE_ENUM_ID"]==1418 || $EL["PROPERTIES"]["status"]["VALUE_ENUM_ID"]==1504){
			
			//ставим статус "гости отменили заказ"
			CIBlockElement::SetPropertyValuesEx($_REQUEST["ID"], 105, array("status" => 1501));
			
			
			CModule::IncludeModule("sale");
			//теперь нужно списать баллы со счета пользователя
			$balls = getUserActionBalls("bronirovanie-stolika", $USER->GetID());
			
			CSaleUserAccount::Withdraw($USER->GetID(), $balls, "RUB", "");
			
			echo "гости отменили заказ";
		}
	}
}

function save(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	
	if($EL = check_element_perm($_REQUEST["ID"])){
		
		$tar=explode(" ", $EL["ACTIVE_FROM"]);
 		$tar2=explode(":", $tar[1]);
 		$DATE = $tar[0];
 		$TIME = $tar2[0].":".$tar2[1];
 		
 		if($TIME==":") $TIME="";
 	
 		if($_REQUEST["date"]!="") $DATE=$_REQUEST["date"];
		if($_REQUEST["time"]!="") $TIME=" ".$_REQUEST["time"].":00";
	
	
		$el = new CIBlockElement;
		$arLoadProductArray2 = Array("ACTIVE_FROM"=>$DATE.$TIME);
		$el->Update($_REQUEST["ID"], $arLoadProductArray2);
		
		CIBlockElement::SetPropertyValuesEx($_REQUEST["ID"], 105, array("guest" => $_REQUEST["guest"]));
	
	}
}

function hide(){
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}
	if($EL = check_element_perm($_REQUEST["ID"])){                  
            CIBlockElement::SetPropertyValueCode($_REQUEST["ID"], "hide", "Y");
            echo "Не показывать";
	}
}

if($_REQUEST["act"]=="save") save();
if($_REQUEST["act"]=="repeat") repeat();
if($_REQUEST["act"]=="cancel") cancel();
if($_REQUEST["act"]=="hide") hide();


?>
