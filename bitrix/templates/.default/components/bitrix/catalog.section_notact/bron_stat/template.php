<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["ITEMS"])>0){?>

<!--<a href="#" class="but_sverka">Распечатать сверку</a>-->
<table class="profile-activity compact-table">
		<tr>
			<th class="tc1 ac">Дата</th>
			<th class="tc2">Тип</th>
			<th class="tc3">Персон</th>
			<th class="tc4">Расположение</th>
			<th class="tc5">Гость</th>
			<th class="tc6">Принял</th>
			<th class="tc7">Подтвердил</th>
			<th class="tc8">Статус</th>
			<th class="tc9">Счет</th>
			<th class="tc10">Комиссия</th>
		</tr>

		

<?
$i=1;
foreach($arResult["ITEMS"] as $arElement):?>
	<?
	//echo '<pre>';
//	var_dump($arElement);
//	echo '</pre>';

	if($arElement["PROPERTIES"]["type"]["VALUE"]=="Бронирование столика") $arElement["PROPERTIES"]["type"]["VALUE"]="Столик";
	
	
	
	$arElement["ACTIVE_FROM"]=explode(" ", $arElement["ACTIVE_FROM"]);
	?>
	<tr id="br<?=$arElement["ID"]?>" <? if($i==count($arResult["ITEMS"])) echo 'class="last"'; ?>>
		<td><?=$arElement["ACTIVE_FROM"][0]?></td>
		<td><?=$arElement["PROPERTIES"]["type"]["VALUE"]?></td>
		<td class="ac"><?=$arElement["PROPERTIES"]["guest"]["VALUE"]?></td>
		<td><?if($arElement["PROPERTIES"]["stol"]["VALUE"]!=""){?>Столик <?=$arElement["PROPERTIES"]["stol"]["VALUE"]?><?}?></td>

		<td class="user-friends nofloat">
			<ul>
				<li class="nobg">
					<?if($arElement["PROPERTIES"]["user"]["VALUE"]>0){?>
						<?
						$rsUser = CUser::GetByID($arElement["PROPERTIES"]["user"]["VALUE"]);
						$arUser = $rsUser->Fetch();
						$file = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width'=>73, 'height'=>73), BX_RESIZE_IMAGE_EXACT, true);
						?>
						<?if($file["src"]!=""){?><img src="<?=$file["src"]?>" /><?}?>
						<div class="user-friend">
							<p><a href="/users/id<?=$arUser["ID"]?>/" target="_blank"><?=$arUser["NAME"]?> <?=$arUser["LAST_NAME"]?></a><br/>
							<i><?=$arUser["PERSONAL_CITY"]?></i>
							</p>
							<p class="user-friend-mail"><a href="/users/id<?=$arUser["ID"]?>/im/" target="_blank">Сообщение</a></p>
						</div>
					<?}else{?>
						<?if($arElement["PROPERTIES"]["client"]["VALUE"]>0){?>
						<?
						$res = CIBlockElement::GetByID($arElement["PROPERTIES"]["client"]["VALUE"]);
						if($ob = $res->GetNextElement()){
							$arFields = $ob->GetFields();  
							$arProps = $ob->GetProperties();
 
							
						}
						?>
						<div class="user-friend">
							<? if($arProps["EMAIL"]["VALUE"]!="" && substr_count($arProps["EMAIL"]["VALUE"], "@")>0){?><p><a href="mailto:<?=$arProps["EMAIL"]["VALUE"]?>" target="_blank"><?=$arProps["EMAIL"]["VALUE"]?></a><?}?>
							<? if($arProps["TELEPHONE"]["VALUE"]!=""){?><br/><?=$arProps["TELEPHONE"]["VALUE"];?><?}?>
							<br/><i><?=$arProps["NAME"]["VALUE"]?> <?=$arProps["SURNAME"]["VALUE"]?></i>
							</p>					
						</div>
						<?}?>
					<?}?>
				</li>
			</ul>
        </td>
		<td><?=$arElement["PROPERTIES"]["prinyal"]["VALUE"]?></td>
		<td><?=$arElement["PROPERTIES"]["podtv"]["VALUE"]?></td>
		<td><?=$arElement["PROPERTIES"]["status"]["VALUE"]?></td>
		<td><?if($arElement["PROPERTIES"]["summ_sch"]["VALUE"]!=""){?><?=$arElement["PROPERTIES"]["summ_sch"]["VALUE"]?> руб.<?}?></td>
		<td></td>
	</tr>
	<?
	$i++;
	?>
<?endforeach?>
</table>
<div class="pager">
	<?=$arResult["NAV_STRING"]?>
</div>
<?}else{?>
	Бронирования отсутствуют
<?}?>