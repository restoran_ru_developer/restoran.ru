<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<noindex>
<div style="display: none;"><img src="<?= $arResult["ASD_PICTURE_NOT_ENCODE"]?>" alt="" /></div>
<?if (in_array("VK_LIKE", $arParams["ASD_INCLUDE_SCRIPTS"]) ||
	in_array("TWITTER", $arParams["ASD_INCLUDE_SCRIPTS"]) ||
	in_array("FB_LIKE", $arParams["ASD_INCLUDE_SCRIPTS"]) ||
	in_array("GOOGLE", $arParams["ASD_INCLUDE_SCRIPTS"]) ||
	in_array("ASD_FAVORITE", $arParams["ASD_INCLUDE_SCRIPTS"])):?>
		<?if (in_array("FB_LIKE", $arParams["ASD_INCLUDE_SCRIPTS"])):?>
		<div class="left" style="padding-top:4px; margin-right:0px;">
			<?if ($arParams['SCRIPT_IN_HEAD'] != "Y"){?><script type="text/javascript" src="http://connect.facebook.net/<?='en_US'?>/all.js#xfbml=1"></script><?}?>
			<div id="fb-root"></div>
			<fb:like href="<?= $arResult["ASD_URL_NOT_ENCODE"]?>" layout="button_count" action="<?= strtolower($arParams["LIKE_TYPE"])?>" data-width="75"></fb:like>
		</div>
		<?endif;?>
		<?if (in_array("TWITTER", $arParams["ASD_INCLUDE_SCRIPTS"])):?>
		<div class="left"  style="padding-top:4px;">
			<?if ($arParams['SCRIPT_IN_HEAD'] != "Y"){?><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script><?}?>
			<a rel="nofollow" href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="<?= SITE_SERVER_NAME?>" data-lang="en">Tweet</a>
		</div>
		<?endif;?>
		<?if (in_array("GOOGLE", $arParams["ASD_INCLUDE_SCRIPTS"])):?>
		<div class="left" style="padding-top:4px;">
			<?if ($arParams['SCRIPT_IN_HEAD'] != "Y"){?><script type="text/javascript" src="http://apis.google.com/js/plusone.js">{"lang": "<?='en-US'?>"}</script><?}?>
			<div style="width: 65px;"><g:plusone href="<?= CUtil::JSescape($arResult["ASD_URL_NOT_ENCODE"])?>" size="medium"></g:plusone></div>
		</div>
		<?endif;?>
		<?if (in_array("VK_LIKE", $arParams["ASD_INCLUDE_SCRIPTS"])):?>
		<div class="left" style="padding-top:3px;">
			<?if ($arParams['SCRIPT_IN_HEAD'] != "Y"){?><script type="text/javascript" src="http://userapi.com/js/api/openapi.js?17"></script><?}?>
			<div id="vk_like"></div>
			<script type="text/javascript">
				VK.init({apiId: <?= $arParams["VK_API_ID"]?>, onlyWidgets: true});
				VK.Widgets.Like('vk_like', {
					type: '<?= $arParams["VK_LIKE_VIEW"]?>',
					width: 80,
					verb: <?= $arParams["LIKE_TYPE"]=="RECOMMEND" ? '1' : '0'?>,
					pageUrl: '<?= CUtil::JSescape($arResult["ASD_URL_NOT_ENCODE"])?>'
				});
			</script>
		</div>
		<?endif;?>
	</tr>
</table>
<?endif;?>
</noindex>