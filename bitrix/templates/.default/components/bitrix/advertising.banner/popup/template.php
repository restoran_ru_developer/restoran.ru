<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["BANNER"]):?>
    <div class="popup_baner">    
        <div>
            <div class="close_baner"></div>
            <?=$arResult["BANNER"]?>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            setTimeout("showMeBaner()",1000);
            $(".close_baner").click(function(){
                hideOverflow();
                $(".popup_baner").remove();
            });
            $(".popup_baner").click(function(){
                hideOverflow();
                $(".popup_baner").remove();
            });
        });

        function showMeBaner()
        {
            showOverflow();
            $(".popup_baner").fadeIn(300);
        }
    </script>
<? endif; ?>
