<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>                  
<div id="spec_block_list">
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <div class="spec" onclick="location.href='<?= $arItem["DETAIL_PAGE_URL"] ?>'"> 
<!--            <div class="left pic">
                
                <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" />
            </div>-->
            <div class="left pic" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat"> 
                <div class="title_box">
                    <a class="title <?=(strlen($arItem["NAME"])>23||  substr_count($arItem["NAME"], "br"))?"big":""?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>">
                        <?= $arItem["NAME"] ?>
                    </a>
                </div>        
            </div>
            <div class="right text">                
                <?if ($arItem["adres"]):?>
                    <p><?=$arItem["adres"]?></p>
                <?endif;?>
                <?if ($arItem["subway"]):?>
                    <p class="metro_<?=CITY_ID?>"><?=$arItem["subway"]?></p>
                <?endif;?>                
                    <Br />
                <?if ($arItem["PREVIEW_TEXT"]):?>
                    <?=$arItem["PREVIEW_TEXT"]?>
                <?endif;?>                
            </div>            
            <div class="clear"></div>            
        </div>
    <? endforeach; ?>    
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>