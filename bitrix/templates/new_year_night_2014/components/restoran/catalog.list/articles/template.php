<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>   
<div id="spec_block_list">
    <?if (count($arResult["ITEMS"])>0):?>
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <div class="spec left" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat" onclick="location.href='<?= $arItem["DETAIL_PAGE_URL"] ?>'"> 
                <div class="title_box">
                    <a class="title <?=(strlen($arItem["NAME"])>23||  substr_count($arItem["NAME"], "br"))?"big":""?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>">
                        <?= $arItem["NAME"] ?>
                    </a>
                </div>        
            </div>
        <? endforeach; ?>
        <div class="clear"></div>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <br /><?= $arResult["NAV_STRING"] ?>
        <? endif; ?>
    <?else:?>
    <?    
        echo "<h3>Дорогие друзья!</h3>
<p>
Листья на деревьях еще зеленые, солнышко припекает, но рестораторы уже трудятся над созданием самых интересных программ и самых вкусных меню Новогодней Ночи 2014.
Совсем скоро мы начнем выкладывать новогодние предложения!
Следите за обновлениями!</p>";
    ?>
    <?endif;?>
</div>