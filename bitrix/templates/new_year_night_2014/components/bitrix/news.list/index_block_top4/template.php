<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>    

<?

//$arResult["ELEMENTS"][3] = $arResult["ELEMENTS"][2];
?>
<div id="spec_block_list">
    <? foreach ($arResult["ELEMENTS"] as $key => $arItem): ?>
        <div class="spec left<? if ($key == 3): ?> end<? endif ?>" style="background:url('<?= $arItem["IMAGE"]["src"] ?>') left top no-repeat"> 
            <div class="title_box grad-new">
                <a class="title <?=(strlen($arItem["NAME"])>23)?"big":""?>" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>">
                    <?= $arItem["NAME"] ?>
                </a>
            </div>        
        </div>
    <? endforeach; ?>
    <div class="clear"></div>
</div>