$(document).ready(function(){
	$("body").append("<div class=big_modal><div class='modal_close_galery'></div><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
    //setCenter($(".big_modal"));                                

    $(".articles_photo").find("img").click(function(){
        var img = new Image();
        img.src = $(this).attr("src");
        $(".big_modal img").attr("src",img.src);
        $(".big_modal img").attr("alt",$(this).attr("id"));
        showOverflow();
        if ($(window).height()<img.height)
        {
            var old_height = img.height;
            img.height = $(window).height()*0.85;
            img.width = img.height*img.width/old_height;
        }
        $(".big_modal img").css({"width":"0px","height":"0px"});
        img.onload = function(){
            $(".big_modal img").css({"width":+img.width,"height":+img.height});
            $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});
            $(".big_modal").fadeIn(300);   
        }            
    });
    $(window).resize(function(){
        if (!$(".big_modal").is("hidden"))
        {
            var img = new Image();
            img.src = $(".big_modal").find("img").attr("src");
            if ($(window).height()<img.height)
            {
                var old_height = img.height;
                img.height = $(window).height()*0.85;
                img.width = img.height*img.width/old_height;
            }
            $(".big_modal img").css({"width":img.width,"height":img.height});
            $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
        }
    });   
	
	
	$(".subscribe-user2user").click(function(){
		var id = $(this).attr('id');
		var ids = id.substr(7);
		var title;
		//alert(id); 
		$.ajax({
			url : '/bitrix/components/restoran/restoraunts.detail/templates/.default/subscribe_user2user.php',
			type : 'post',
			data: {
				'uid' : ids,
				'action' : 'subscribe'
			},
			success: function(result)
			{
				if(result === '2')
				{
					title = 'Подписаться';
				}
				if(result === '1')
				{
					title = 'Отписаться';
				}
				
				$("#" + id).val(title);
				
			}
		
		});
	});
	
	

	
});

$(window).load(function(){

	var id = $(".subscribe-user2user").attr('id');
	var ids = id.substr(7);
	var title;
	$.ajax({
			url : '/bitrix/components/restoran/restoraunts.detail/templates/.default/subscribe_user2user.php',
			type : 'post',
			data: {
				'uid' : ids,
				'action' : 'check'
			},
			success: function(result)
			{
				if(result === '2')
				{
					title = 'Подписаться';
				}
				if(result === '1')
				{
					title = 'Отписаться';
				}
				
				$("#" + id).val(title);
				
			}
		
		});
	
});
function users_onpage(data)
{
    if (data)
        $("#users_onpage").html(data);
    else
        $("#users_onpage").remove();
}