<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js')?>
</head>
<body>
    <script>
        $(document).ready(function(){
            window.print()
        });        
    </script>
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="container">
        <div id="header" style="width: 960px;">
            <div id="logo" class="left"><a href="/"><img src="/tpl/images/logo.png" /></a></div>            
            <div class="phones right" style="margin-top:20px;">                
                <div class="font18">Заказ банкетов и столиков</div>
                <div align="right">
                    <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/top_phones_".CITY_ID.".php"),
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </div>
            </div>
            <!--<div class="phone_block right">
                <?=GetMessage("ORDER_BUBBLE_TEXT")?>
            </div>            -->
            <div class="clear"></div>
        </div>