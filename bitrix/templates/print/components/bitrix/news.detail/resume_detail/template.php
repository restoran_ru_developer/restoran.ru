<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
<script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />

<script type="text/javascript">
    $(document).ready(function(){
        $('.QapTcha').QapTcha({
            txtLock : 'Сдвиньте слайдер вправо.',
            txtUnlock : 'Готово. Теперь можно отправлять',
            disabledSubmit : true,
            autoRevert : true,
            PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
            autoSubmit : false});
                    
                    
        $(".fancy").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200, 
            'speedOut'		:	200, 
            //'overlayShow'	:	false
        });
    });
</script>

<?
//v_dump($arResult); 
$rsUser = CUser::GetByID($arResult["CREATOR_ID"]);
$arUser = $rsUser->Fetch();
?>
<script>
    $(document).ready(function(){
        $("#user_mail_send").click(function(){
            var _this = $(this);
            $.ajax({
                type: "POST",
                url: _this.attr("href"),
                data: "USER_NAME=<?= $arUser["NAME"] . " " . $arUser["LAST_NAME"] ?>&USER_ID=<?= intval($arUser["ID"]) ?>&<?= bitrix_sessid_get() ?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                    }
                    $('#mail_modal').html(data);
                    showOverflow();
                    setCenter($("#mail_modal"));
                    $("#mail_modal").fadeIn("300"); 
                }
            });
            return false;
        });
    });
</script>
<div class="content-wrapper">
    <div class="content-content">
            <h1 class="content-h nopadding-bottom"><?= $arResult["NAME"] ?></h1>
            <p><span class="date-field"><span class="date-day"><?= $arResult["DISPLAY_ACTIVE_FROM_1"] ?></span> <?= $arResult["DISPLAY_ACTIVE_FROM_2"] ?> <?= $arResult["DISPLAY_ACTIVE_FROM_3"] ?></span></p>
        <div class="clear"></div>
        <div class="info-detail-wrapper info-detail-wrapper-resume">
            <div class="info-detail-small info-detail-small-resume">
                <div class="big_avatar">
                    <? if ($arResult["CREATOR_PHOTO"]): ?>
                        <img src="<?= $arResult["CREATOR_PHOTO"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" />
                    <? endif; ?>
                </div>
            </div>
            <div class="info-detail-big info-detail-big-resume">
                <dl class="resume-dl">
                    <dt>Имя:</dt>
                    <dd><?= $arResult["CREATOR_NAME"] ?></dd>
                    <dt>Пол:</dt>
                    <dd><?= GetMessage("GENDER_" . $arResult["CREATOR_GENDER"]) ?></dd>
                    <dt>Дата рождения:</dt>
                    <dd><?= $arResult["CREATOR_BIRTHDAY"] ?></dd>
                    <dt>Город:</dt>
                    <dd><?= $arResult["IBLOCK"]["NAME"] ?><? if (strlen($arResult["PROPERTIES"]["SUGG_WORK_MOVE"]["VALUE"]) > 0): ?><span class="italic em0_83"> + <?= $arResult["PROPERTIES"]["SUGG_WORK_MOVE"]["VALUE"] ?><? endif ?></span></dd>
                    <? if ($USER->IsAuthorized()) { ?>
                        <dt>Телефон:</dt>
                        <dd><?= $arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"] ?></dd>
                    <? } ?>
                    <dt>Образвание:</dt>
                    <dd><?= $arResult["PROPERTIES"]["EDUCATION"]["VALUE"] ?></dd>
                    <dt>Опыт работы:</dt>
                    <dd><?= $arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></dd>
                    <dt class="last">О себе:</dt>
                    <dd class="last"><span class="italic em0_83"><?= $arResult["PROPERTIES"]["ADD_USER_INFO"]["VALUE"] ?></span></dd>
                </dl>
            </div>
        </div>
        <div class="resume-params">
            <h3 class="content-h">Пожелания к месту работы</h3>
            <table class="table-info">
                <tr>
                    <th>Заработная плата</th>
                    <th>График работы</th>
                    <th>Метро</th>
                </tr>
                <tr>
                    <td>
                        <?= $arResult["PROPERTIES"]["SUGG_WORK_ZP_FROM"]["VALUE"] ?> &mdash; <?= $arResult["PROPERTIES"]["SUGG_WORK_ZP_TO"]["VALUE"] ?> <span class="rouble font12">e</span>
                    </td>
                    <td>
                        <?= implode(" / ", $arResult["PROPERTIES"]["SUGG_WORK_GRAFIK"]["VALUE"]) ?>
                    </td>
                    <td>
                        <div class="metro_<?= CITY_ID ?>"><?= implode(', ', $arResult['PROPERTIES']["SUGG_WORK_SUBWAY"]["VALUE"]) ?></div>
                    </td>
                </tr>
            </table>
        </div>
        <hr class="content-separator">
        <br class="br-content-blocks" style="margin-top: 1.5em;">
        <div class="resume-params">
            <? if($arResult["PROPERTIES"]["EXP_WHEN_1"]["VALUE"] && $arResult["PROPERTIES"]["EXP_WHEN_2"]["VALUE"]):?>
            <h3 class="content-h">Опыт работы</h3>
            <div class="resume-params-experience">
                <table class="table-info">
                    <? foreach ($arResult["PROPERTIES"]["EXP_WHEN_1"]["VALUE"] as $keyWork => $work): ?>
                        <tr>
                            <th>Когда?</th>
                            <th>Где?</th>
                            <th>Должность</th>
                            <th>Обязанности</th>
                        </tr>
                        <tr>
                            <td><?= $work ?> — <?= $arResult["PROPERTIES"]["EXP_WHEN_2"]["VALUE"][$keyWork] ?></td>
                            <td><?= $arResult["PROPERTIES"]["EXP_WHERE"]["VALUE"][$keyWork] ?></td>
                            <td><?= $arResult["PROPERTIES"]["EXP_POST"]["VALUE"][$keyWork] ?></td>
                            <td width="35%"><?= $arResult["PROPERTIES"]["EXP_RESPONS"]["VALUE"][$keyWork] ?></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr class="content-row-separator">
                            </td>
                        </tr>
                    <? endforeach ?>
                </table>
            </div>
            <? endif; ?>
        </div>
        
    </div>
</div>

