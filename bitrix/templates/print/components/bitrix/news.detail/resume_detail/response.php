<?

define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>

<?

global $USER;
if ($USER->IsAuthorized()) {
    
//отправка письма создателю резюме
    CModule::IncludeModule("iblock");
    $pr_res = CIBlockElement::GetByID($_REQUEST["resume_id"]);
    if ($pr_res = $pr_res->GetNext()) {
        $arResult["VACANCY"] = $pr_res;
        $arResult["CREATOR_ID"] = $pr_res["CREATED_BY"];
        $arResult["DETAIL_PAGE_URL"] = $pr_res["DETAIL_PAGE_URL"];
    }

   // var_dump($arResult);
    
    $rsUser = CUser::GetByID($arResult["CREATOR_ID"]);
    $creator = $rsUser->Fetch();
    $arResult["CREATOR"] = $creator;
    $arResult["EMAIL_TO"] = $creator['EMAIL'];

    $event = new CEvent;
    $arFields = Array(
        "USER_NAME" => $USER->GetFullName(),
        "USER_ID" => $USER->GetID(),
        "EMAIL_TO" => $creator['EMAIL'],
        "LINK_TO_VACANCY" => $arResult["DETAIL_PAGE_URL"]
    );
    $event->Send($eventName, SITE_ID, $arFields, "N", 125);
    
    
    if ($_REQUEST["resume_id"] && check_bitrix_sessid()) {
        $arResult["ERROR"] = "";
        $arIB = getArIblock('resume', CITY_ID);

        $arResponseUsers = Array();
        $curUserID = $USER->GetID();
        $rsResponse = CIBlockElement::GetProperty($arIB["ID"], $_REQUEST["resume_id"], array("sort" => "asc"), Array("CODE" => "RESPOND_USERS_REG"));
        while ($arResponse = $rsResponse->Fetch()) {
            $arResponseUsers[] = $arResponse["VALUE"];
        }
        if (!in_array($curUserID, $arResponseUsers)) {
            $arResponseUsers[] = $curUserID;
            CIBlockElement::SetPropertyValuesEx($_REQUEST["resume_id"], $arIB["ID"], array("RESPOND_USERS_REG" => $arResponseUsers));
            $arResult["RESULT"] = "Вы откликнулись!";
        } else {
            $arResult["RESULT"] = "Вы уже откликались на резюме!";
        }
    } else {
        $arResult["ERROR"] = "Ошибка сессии!";
    }
} else {
    $arResult["ERROR"] = "Для отклика на вакансию необходимо авторизоваться";
}
echo json_encode($arResult);
?>