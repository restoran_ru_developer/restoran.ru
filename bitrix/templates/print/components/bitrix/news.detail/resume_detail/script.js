$(document).ready(function() {
    $('#response_resume').on('click', function() {
        var formID = $('#response_resume_form');
        var formData = formID.serialize();
        $.ajax({
            type: 'POST',
            url: formID.attr('action'),
            cache: false,
            data: formData,
            dataType: 'json',
            success: function (data) {	
                var toAdd = '<div align="right"> <a class="modal_close uppercase white" href="javascript:void(0)"></a></div>';
                if(data.ERROR){                    
                    if (!$("#promo_modal").size())
                    {
                        $("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
                    }
                    $('#promo_modal').html(toAdd + data.ERROR);
                    showOverflow();
                    setCenter($("#promo_modal"));
                    $("#promo_modal").fadeIn("300");
                }

                if(data.RESULT){
                    //alert(data.RESULT);
                    if (!$("#promo_modal").size())
                    {
                        $("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
                    }
                    $('#promo_modal').html(toAdd + data.RESULT);
                    showOverflow();
                    setCenter($("#promo_modal"));
                    $("#promo_modal").fadeIn("300");
                }	
            }
        });
    });
    
    function addFavouriteHandler (data) {
        var toAdd = '<div align="right"> <a class="modal_close uppercase white" href="javascript:void(0)"></a></div>';
        if(data.STATUS === 1)
        {
            if (!$("#promo_modal").size())
            {
                $("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
            }
            $('#promo_modal').html(toAdd + 'Резюме добавлено в избранное!');
            showOverflow();
            setCenter($("#promo_modal"));
            $("#promo_modal").fadeIn("300");
        }
			
    }
});