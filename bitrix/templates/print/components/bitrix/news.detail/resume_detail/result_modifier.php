<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?

/*
  // parse active from date
  $tmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_FROM"]);
  $arResult["DISPLAY_ACTIVE_FROM_1"] = $tmpDate[0];
  $arResult["DISPLAY_ACTIVE_FROM_2"] = $tmpDate[1];
  $arResult["DISPLAY_ACTIVE_FROM_3"] = $tmpDate[2];

  // get person info
  $rsUser = CUser::GetByID($arResult["PROPERTIES"]["USER_BIND_APPLICANT"]["VALUE"]);
  $arUser = $rsUser->Fetch();

  $arResult["APPLICANT_PERSON"]["FIO"] = $arUser["LAST_NAME"]." ".$arUser["NAME"];
  $arResult["APPLICANT_PERSON"]["PHONE"] = $arUser["PERSONAL_PHONE"];
  $arResult["APPLICANT_PERSON"]["CITY"] = $arUser["PERSONAL_CITY"];
  $arResult["APPLICANT_PERSON"]["PHOTO"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
  $arResult["APPLICANT_PERSON"]["GENDER"] = $arUser["PERSONAL_GENDER"];
  $arResult["APPLICANT_PERSON"]["BIRTHDAY"] = $arUser["PERSONAL_BIRTHDAY"];
  $arResult["APPLICANT_PERSON"]["LOGIN"] = $arUser["LOGIN"];
  $arResult["APPLICANT_PERSON"]["ID"] = $arUser["ID"];

  // resto subway
  foreach($arResult["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"] as $keySub=>$subway) {
  $rsRestSubway = CIBlockElement::GetByID($subway);
  $arRestSubway = $rsRestSubway->Fetch();

  $arResult["RESTAURANT"]["SUBWAY"] .= $arRestSubway["NAME"];
  if($arResult["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"][$keySub+1])
  $arResult["RESTAURANT"]["SUBWAY"] .= ", ";
  }

  // calculate diff between dates
  foreach($arResult["PROPERTIES"]["EXP_WHEN_1"]["VALUE"] as $keyWork=>$work) {
  if(!strtotime($arResult["PROPERTIES"]["EXP_WHEN_2"]["VALUE"][$keyWork])) {
  $arResult["PROPERTIES"]["EXP_WHEN_2"]["VALUE"][$keyWork] = date("d.m.Y");
  }
  $diff = abs(strtotime($arResult["PROPERTIES"]["EXP_WHEN_1"]["VALUE"][$keyWork]) - strtotime($arResult["PROPERTIES"]["EXP_WHEN_2"]["VALUE"][$keyWork]));
  $years = floor($diff / (365*60*60*24));
  $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
  if($years > 0)
  $arResult["PROPERTIES"]["EXP_DIFF"]["VALUE"][$keyWork] = $years." ".pluralForm($years, "год ", "года ", "лет ");
  $arResult["PROPERTIES"]["EXP_DIFF"]["VALUE"][$keyWork] .= $months." ".pluralForm($months, "месяц", "месяца", "месяцев");
  } */

$res = CIBlockElement::GetByID($arResult["ID"]);
if ($ar_res = $res->GetNext()) {
    $arResult["ACTIVE_TO"] = $ar_res["ACTIVE_TO"];
    $arResult["CREATED_BY"] = $ar_res["CREATED_BY"];
}
$creator = $USER->getById($arResult["CREATED_BY"])->fetch();
//v_dump($arResult);
if ($arResult["DETAIL_PICTURE"]) {
    $arResult["CREATOR_PHOTO"] = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']["ID"], array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_EXACT, true);
} else {
    $arResult["CREATOR_PHOTO"] = CFile::ResizeImageGet($creator['PERSONAL_PHOTO'], array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_EXACT, true);
    if (!$arResult["CREATOR_PHOTO"]) {
        if ($arResult["CREATOR_GENDER"] == "M")
            $arResult["CREATOR_PHOTO"]["src"] = "/tpl/images/noname/man_nnm.png";
        elseif ($arResult["CREATOR_GENDER"] == "F")
            $arResult["CREATOR_PHOTO"]["src"] = "/tpl/images/noname/woman_nnm.png";
        else
            $arResult["CREATOR_PHOTO"]["src"] = "/tpl/images/noname/unisx_nnm.png";
    }
}

//v_dump($creator);
$arResult["CREATOR_NAME"] = $creator["NAME"] . ' ' . $creator["LAST_NAME"];
$arResult["CREATOR_ID"] = $creator["ID"];
$arResult["CREATOR_GENDER"] = $creator["PERSONAL_GENDER"];
$arResult["CREATOR_BIRTHDAY"] = $creator["PERSONAL_BIRTHDAY"];
// parse active from date
$tmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_FROM"]);
//v_dump($arResult["ACTIVE_FROM"]);
$arResult["DISPLAY_ACTIVE_FROM_1"] = $tmpDate[0];
$arResult["DISPLAY_ACTIVE_FROM_2"] = $tmpDate[1];
$arResult["DISPLAY_ACTIVE_FROM_3"] = $tmpDate[2];
foreach ($arResult["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"] as $key => $value) {
    $res = CIBlockElement::GetByID($value);
    if ($ar_res = $res->GetNext()) {
        $arResult["PROPERTIES"]["SUGG_WORK_SUBWAY"]["VALUE"][$key] = $ar_res["NAME"];
    }
}
?>