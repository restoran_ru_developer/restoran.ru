<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?



  
  
$res = CIBlockElement::GetByID($arResult["ID"]);
if ($ar_res = $res->GetNext()) {
    $arResult["ACTIVE_TO"] = $ar_res["ACTIVE_TO"];
    $arResult["CREATED_BY"] = $ar_res["CREATED_BY"];
}

$creator = $USER->getById($arResult["CREATED_BY"])->fetch();
$arResult["CREATOR_PHOTO"] = CFile::ResizeImageGet($creator["PERSONAL_PHOTO"], array('width' => 64, 'height' => 64), BX_RESIZE_IMAGE_PROPORTIONAL, true);
$arResult["CREATOR_NAME"] = $creator["NAME"] . ' ' . $creator["LAST_NAME"];
$arResult["CREATOR_ID"] = $creator["ID"];
$arResult["CREATOR_GENDER"] = $creator["PERSONAL_GENDER"];
$arResult["CREATOR_BIRTHDAY"] = $creator["PERSONAL_BIRTHDAY"];

if(!$arResult["CREATOR_PHOTO"]){
    if ($arResult["CREATOR_GENDER"] == "M")
    $arResult["CREATOR_PHOTO"]["src"] = "/tpl/images/noname/man_nnm.png";
elseif ($arResult["CREATOR_GENDER"] == "F")
    $arResult["CREATOR_PHOTO"]["src"] = "/tpl/images/noname/woman_nnm.png";
else
    $arResult["CREATOR_PHOTO"]["src"] = "/tpl/images/noname/unisx_nnm.png";
}
//v_dump($arResult);
//v_dump($creator);
$arResult["CREATOR_NAME"] = $creator["NAME"] . ' ' . $creator["LAST_NAME"];
$arResult["CREATOR_ID"] = $creator["ID"];
// parse active from date
$tmpDate = explode(" ", $arResult["DISPLAY_ACTIVE_FROM"]);
//v_dump($arResult["ACTIVE_FROM"]);
$arResult["DISPLAY_ACTIVE_FROM_1"] = $tmpDate[0];
$arResult["DISPLAY_ACTIVE_FROM_2"] = $tmpDate[1];
$arResult["DISPLAY_ACTIVE_FROM_3"] = $tmpDate[2];
foreach ($arResult["PROPERTIES"]["SUBWAY_BIND"]["VALUE"] as $key => $value) {
    $res = CIBlockElement::GetByID($value);
    if ($ar_res = $res->GetNext()) {
        $arResult["PROPERTIES"]["SUBWAY_BIND"]["VALUE"][$key] = $ar_res["NAME"];
    }
}
?>