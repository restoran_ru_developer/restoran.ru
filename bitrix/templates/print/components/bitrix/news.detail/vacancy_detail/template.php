<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
<script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
<script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />

<script type="text/javascript">
    $(document).ready(function(){
        $('.QapTcha').QapTcha({
            txtLock : 'Сдвиньте слайдер вправо.',
            txtUnlock : 'Готово. Теперь можно отправлять',
            disabledSubmit : true,
            autoRevert : true,
            PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
            autoSubmit : false});
                    
                    
        $(".fancy").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200, 
            'speedOut'		:	200, 
            //'overlayShow'	:	false
        });
    });
</script>
<? //v_dump($arResult["IBLOCK"]["NAME"]);    ?>
<?
$rsUser = CUser::GetByID($arResult["CREATOR_ID"]);
$arUser = $rsUser->Fetch();
//v_dump($arUser);
?>

<script>
    $(document).ready(function(){
        $("#user_mail_send").click(function(){
            var _this = $(this);
            $.ajax({
                type: "POST",
                url: _this.attr("href"),
                data: "USER_NAME=<?= $arUser["NAME"] . " " . $arUser["LAST_NAME"] ?>&USER_ID=<?= intval($arUser["ID"]) ?>&<?= bitrix_sessid_get() ?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='mail_modal'></div>").appendTo("body");                                                               
                    }
                    $('#mail_modal').html(data);
                    showOverflow();
                    setCenter($("#mail_modal"));
                    $("#mail_modal").fadeIn("300"); 
                }
            });
            return false;
        });
    });
</script>
<div class="content-wrapper">
    <!-- CONTENT -->
    <div class="content-content">
        <h1 class="content-h nopadding-bottom"><?= $arResult["NAME"] ?></h1>
        <p><span class="date-field"><span class="date-day"><?= $arResult["DISPLAY_ACTIVE_FROM_1"] ?></span> <?= $arResult["DISPLAY_ACTIVE_FROM_2"] ?> <?= $arResult["DISPLAY_ACTIVE_FROM_3"] ?></span></p>
        <div class="info-detail-wrapper">
            <div class="info-detail-big">
                <? if (!empty($arResult['PROPERTIES']["COMPANY_NAME"]["VALUE"])) { ?>
                    <h3 class="content-h"><?= $arResult['PROPERTIES']["COMPANY_NAME"]["VALUE"] ?></h3>
                <? } ?>

                <? if (!empty($arResult['PROPERTIES']["COMPANY_FIO"]["VALUE"])) { ?>
                    <div class="block-param">
                        <span class="block-param-title">Контактное лицо:</span> <span class="block-param-content"><?= $arResult['PROPERTIES']["COMPANY_FIO"]["VALUE"] ?></span>
                    </div>
                <? } ?>
                <? if (!empty($arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"])) { ?>
                    <? if ($USER->IsAuthorized()) { ?>
                        <div class="block-param">
                            <span class="block-param-title">Телефон:</span> <span class="block-param-content"><?= $arResult['PROPERTIES']["CONTACT_PHONE"]["VALUE"] ?></span>
                        </div>
                    <? } ?>
                <? } ?>
                <br class="block-param-break">
                <? if (!empty($arResult['PROPERTIES']["ADDRESS"]["VALUE"])) { ?>
                    <div class="block-param">
                        <span class="block-param-title">Адрес:</span> <span class="block-param-content"><?= $arResult['PROPERTIES']["ADDRESS"]["VALUE"] ?></span>
                    </div>
                <? } ?>

                <? if (!empty($arResult['PROPERTIES']["SUBWAY_BIND"]["VALUE"])) { ?>
                    <div class="metro_<?= CITY_ID ?>"><?= implode(', ', $arResult['PROPERTIES']["SUBWAY_BIND"]["VALUE"]) ?></div>
                <? } ?>


                <br class="block-param-break">
                <? if (!empty($arResult['PROPERTIES']["site"]["VALUE"])) { ?>
                    <div class="block-param-url italic">
                        <?= $arResult['PROPERTIES']["site"]["VALUE"] ?>
                    </div>
                <? } ?>


            </div>
            <div class="info-detail-small">
                <div class="avatar vacancy-detail-image">
                    <? if ($arResult["CREATOR_PHOTO"]): ?>
                        <img src="<?= $arResult["CREATOR_PHOTO"]["src"] ?>" alt="<?= $arItem["USER"]["FIO"] ?>" width="64"/>
                    <? endif; ?>
                </div>
                <div class="vacancy-detail-info">
                    <div class="vacancy-detail-info-name">
                        <?= $arResult["CREATOR_NAME"] ?>
                    </div>
                    <div class="vacancy-detail-info-city"><?= $arResult["IBLOCK"]["NAME"] ?></div>
                </div>
            </div>
        </div>
        <div class="vacancy-params">

            <?
            $dontShowParams = empty($arResult["PROPERTIES"]["WAGES_OF"]["VALUE"]) && empty($arResult["PROPERTIES"]["WAGES_TO"]["VALUE"])
                    && empty($arResult["PROPERTIES"]["SHEDULE"]["VALUE"]) && empty($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"])
                    && empty($arResult["PROPERTIES"]["EDUCATION"]["VALUE"]) && empty($arResult["PROPERTIES"]["RESPONSIBILITY"]["VALUE"])
                    && empty($arResult["PROPERTIES"]["AGE_FROM"]["VALUE"]) && empty($arResult["PROPERTIES"]["AGE_TO"]["VALUE"]);

            if (!$dontShowParams) {
                ?>
                <div class="vacancy-params-left">
                    <h3 class="content-h">Условия и требования</h3>
                    <dl>

                        <? if (!empty($arResult["PROPERTIES"]["WAGES_OF"]["VALUE"]) || !empty($arResult["PROPERTIES"]["WAGES_TO"]["VALUE"])) { ?>
                            <dt>Заработная плата</dt>
                            <dd><?= $arResult["PROPERTIES"]["WAGES_OF"]["VALUE"] ?> &mdash; <?= $arResult["PROPERTIES"]["WAGES_TO"]["VALUE"] ?> <span class="rouble font12">e</span></dd>
                        <? } ?>

                        <? if (!empty($arResult["PROPERTIES"]["SHEDULE"]["VALUE"])) { ?>
                            <dt>График работы</dt>
                            <dd><?= implode(" / ", $arResult["PROPERTIES"]["SHEDULE"]["VALUE"]) ?></dd>
                        <? } ?>


                        <? if (!empty($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"])) { ?>
                            <dt>Опыт работы</dt>
                            <dd><?= $arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"] ?></dd>
                        <? } ?>

                        <? if (!empty($arResult["PROPERTIES"]["EDUCATION"]["VALUE"])) { ?>
                            <dt>Образование</dt>
                            <dd><?= $arResult["PROPERTIES"]["EDUCATION"]["VALUE"] ?></dd>
                        <? } ?>

                        <? if (!empty($arResult["PROPERTIES"]["AGE_FROM"]["VALUE"]) || !empty($arResult["PROPERTIES"]["AGE_TO"]["VALUE"])) { ?>
                            <dt>Возраст</dt>
                            <dd><?= $arResult["PROPERTIES"]["AGE_FROM"]["VALUE"] ?> — <?= $arResult["PROPERTIES"]["AGE_TO"]["VALUE"] ?> лет</dd>
                        <? } ?>

                        <? if (!empty($arResult["PROPERTIES"]["RESPONSIBILITY"]["VALUE"])) { ?>
                            <dt>Обязанности</dt>
                            <dd><?= $arResult["PROPERTIES"]["RESPONSIBILITY"]["VALUE"] ?></dd>
                        <? } ?>
                    </dl>
                </div>
            <? } ?>
            <? if (!empty($arResult["PROPERTIES"]["ADD_INFO"]["VALUE"])) { ?>
                <div class="vacancy-params-right">
                    <h3 class="content-h">Дополнительно</h3>
                    <div class="vacancy-params-content italic"><?= $arResult["PROPERTIES"]["ADD_INFO"]["VALUE"] ?></div>
                </div>
            <? } ?>
        </div>

    </div>
</div>

