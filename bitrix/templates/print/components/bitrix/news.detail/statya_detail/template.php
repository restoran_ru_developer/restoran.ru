<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/templates/main/js/flowplayer.js"></script>
<script>
$(document).ready(function(){
   flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
        clip: {
                autoPlay: false, 
                autoBuffering: true,
                accelerated: true                
        }
    });
    $(".articles_photo").galery(); 
});
</script>
<script>
$(document).ready(function(){
    $(".photos123").each(function(){
       var wid = $(this).width();
       //var hei = $(this).height();
       $(this).append("<div class='block'>Увеличить</div>");
       $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
    });
    $(".photos123").hover(function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        console.log(wid);
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"}); */
        $(this).find(".block").show();
    },
    function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"});*/
        $(this).find(".block").hide();
    });
    $(".photos123 .block").toggle(function(){
        $(this).parent().find("img:hidden").show().prev().hide();        
        $(this).parent().removeClass('left');        
        $(this).html("Уменьшить");
        $(this).parent().css("width","728px");
        var wid = $(this).parent().width();
        //var hei = $(this).parent().height();
        $(this).css({"left":wid/2-50+"px"});
    },
    function(){
        $(this).parent().find("img:hidden").show().next().hide();
        $(this).parent().addClass('left');        
        $(this).html("Увеличить");
        $(this).parent().css("width","238px");
        var wid = $(this).parent().width();
        //var hei = $(this).parent().height();
        $(this).css({"left":wid/2-50+"px"});
    });    
})
</script> 
<?$resto = array();?>
<div id="content">
    <div class="left" style="width:720px;">
       
        <h1><?=$arResult["NAME"]?></h1>
        <a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <?if($arResult["PREVIEW_TEXT"]):?>
            <i><?=$arResult["PREVIEW_TEXT"]?></i>
            <br /> <br />
        <?endif;?>
        <div class="left recept_prop_block1" style="background-color:#FFFFFF; color: #000000;">
            Время приготовления — <span><?=$arResult["DISPLAY_PROPERTIES"]["prig_time"]["DISPLAY_VALUE"]?></span><br />
            Сложность: <span><?=$arResult["DISPLAY_PROPERTIES"]["slognost"]["DISPLAY_VALUE"]?></span><br />
            <?
                $params = array();
                $params["id"] = $arResult["ID"];
                $params["section"] = 1;
                $params = addslashes(json_encode($params));            
            ?>

        </div>
        <div class="right recept_prop_block2">
            <div class="left" style="margin-right:20px;">
                Основной ингридиент: <span><?=$arResult["DISPLAY_PROPERTIES"]["osn_ingr"]["DISPLAY_VALUE"]?></span><br />
                Категория: <span><?=$arResult["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]?></span><Br />
                Кухня: <span><?=$arResult["DISPLAY_PROPERTIES"]["cook"]["DISPLAY_VALUE"]?></span>
            </div>
            <div class="left" style="width:180px">
                Повод: <span><?=$arResult["DISPLAY_PROPERTIES"]["povod"]["DISPLAY_VALUE"]?></span><Br />
                <!--Предпочтения: <span><?=$arResult["DISPLAY_PROPERTIES"]["predp"]["DISPLAY_VALUE"]?></span><br />-->
                Приготовление: <span><?=$arResult["DISPLAY_PROPERTIES"]["prig"]["DISPLAY_VALUE"]?></span>
            </div>
            <div class="clear"></div>
        </div>

        <?=$arResult["DETAIL_TEXT"]?>



		<br/>
		<br/>

    </div>
 
    <div class="clear"></div>
</div>