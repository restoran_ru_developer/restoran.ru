<?
if ($arParams["IBLOCK_ID"])
{
    $res = CIBlock::GetByID($arParams["IBLOCK_ID"]);
    $ar = $res->Fetch();
    $arResult["DATE"] = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($ar["TIMESTAMP_X"], CSite::GetDateFormat()));
}
?>
