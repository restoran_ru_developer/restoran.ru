$(function() 
{
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/ajax_loader.gif"></div>');
    $("#system_loading").css("left",$(window).width()/2-104/2+"px");
    $("#system_loading").css("top",$(window).height()/2-104/2+"px"); 
    $("#system_loading").bind("ajaxSend", function(){        
        $(this).show();
    }).bind("ajaxComplete", function(){
        $(this).hide();
    });
    
});
function send_ajax(url,params)
{
    params = eval('('+params+')');
    $.ajax({
      url: url,
      type: 'post',
      dataType: 'json',
      data: params,
      statusCode: {
        404: function() {
            alert('Page not found');
        }
      },
      success: function(data)
      {
          console.log(data);
      }
    });
}
function sumbit_form(form)
{
    var params = $(form).serialize();
    var url = $(form).attr("action");
    $.ajax({
      url: url,
      type: 'post',      
      data: params,
      statusCode: {
        404: function() {
            alert('Page not found');
        }
      },
      beforeSend: function()
      {
          if (typeof(before_submit_form)=="function")
              before_submit_form(form);    
          return false;
      },
      success: function(data)
      {
          $(form).parent().html(data);
          if (typeof(success_submit_form)=="function")
              success_submit_form();
      }
    });
    return false;
}