$(document).ready(function(){
    
    $("#reviews > form").submit(function(e) {
        var form = $(this);
        if (!e.isDefaultPrevented()) {
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function(data) {
                    $("#rating_overlay").html(data);
                    //$(".error").hide();
                     $('#rating_overlay').overlay({
                        top: 260,                    
                        closeOnClick: false,
                        closeOnEsc: false,
                        api: true
                    }).load();
                    $(".add_review").html("");
                }
            });
            e.preventDefault();
        }
    });
    $("#reviews > form").bind("onFail", function(e, errors)  {
	if (e.originalEvent.type == 'submit') {
		$.each(errors, function()  {
			var input = this.input;	                        
		});
	}
    });
});
