<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

function calculateTheDistance($LAT1, $LON1, $LAT2, $LON2) {

    // перевести координаты в радианы
    $lat1 = $LAT1 * M_PI / 180;
    $lat2 = $LAT2 * M_PI / 180;
    $long1 = $LON1 * M_PI / 180;
    $long2 = $LON2 * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

    $ad = atan2($y, $x);
    $dist = $ad * 6371000;

    return $dist;
}

$lat1 = (float) $arResult["PROPERTIES"]['lat']["VALUE"][0];
$lon1 = (float) $arResult["PROPERTIES"]['lon']["VALUE"][0];
$lat2 = (float) $_SESSION['lat'];
$lon2 = (float) $_SESSION['lon'];
$dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
if ($dist < 1000) {
    $distance = $dist . ' м<br>';
} elseif (($dist > 1000 && $dist < 10000)) {
    $distance = substr($dist, 0, 1) . '.' . substr($dist, 1, 1) . ' км<br>';
} elseif (($dist > 10000 && $dist < 100000)) {
    $distance = substr($dist, 0, 2) . '.' . substr($dist, 2, 1) . ' км<br>';
}
?>

<div class="detail-page">
    <? //v_dump($arResult["PROPERTIES"]["features"]["VALUE"]); ?>
    <div class="name-block">
        <?
        if ($arResult["SECTION"]["PATH"][0]["CODE"] == 'restaurants') {
            $what = "Ресторан";
        } else {
            $what = "Банкетный зал";
        }
        ?>
        <div class="restaurant-name"><?= $what ?> <?= $arResult["NAME"] ?></div>        
    </div>
    <a href="/gallery/?RESTOURANT=<?= $_REQUEST['RESTOURANT'] ?>" data-ajax="false">
        <div class="main-image-block">
            <div class="rating-block">
                <div class="rating-block-background"></div>
                <div class="rating-container">
                    <? for ($i = 1; $i <= round($arResult["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                        <div class="star-good"></div>
                    <? endfor ?>
                    <? for ($i = round($arResult["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                        <div class="star-bad"></div>
                    <? endfor ?>
                </div>

                <div class="comment-count-block" onclick="$('html,body').animate({scrollTop: $('#comments').offset().top},'slow');">
                    <div class="circle">
                        <div class="comment-count"><?= (int) $arResult["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"] ?></div>
                    </div>
                    <div class="comment-count-text"><?= pluralForm((int) $arResult["DISPLAY_PROPERTIES"]["COMMENTS"]["DISPLAY_VALUE"], 'ОТЗЫВ', 'ОТЗЫВА', 'ОТЗЫВОВ') ?></div>
                </div>
            </div>
            <? if ($arResult["PROPERTIES"]["photos"]["VALUE2"] !== null): ?>
                <img class="main-image" src="<?= $arResult["PROPERTIES"]["photos"]["VALUE2"][0]["src"] ?>"/>
            <? else: ?>
                <? if ($arResult["PREVIEW_PICTURE"]["SRC"] !== null): ?>
                    <img class="main-image" src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"/>
                <? else: ?>
                    <img class="main-image" src="/tpl/images/noname/rest_nnm.png"/>
                <? endif; ?>
            <? endif; ?>

        </div>
    </a>
    
    <? if (count($arResult["PROPERTIES"]["photos"]["VALUE2"]) > 1 ): ?>
        <div class="clear"></div>
        <div class="image-block">
            <div class="images-background">
                <a href="/gallery/?RESTOURANT=<?= $_REQUEST['RESTOURANT'] ?>" class="button button-right all-photos-link" data-ajax="false"></a>
            </div>
            <div class="images-container">
                <?
                for ($i = 1; $i <= 50; $i++):
                    if ($arResult["PROPERTIES"]["photos"]["VALUE2"][$i]["src"] !== null):
                        ?>
                        <div class="image">
                            <a href="/gallery/?RESTOURANT=<?= $_REQUEST['RESTOURANT'] ?>" data-ajax="false">
                                <img src="<?= $arResult["PROPERTIES"]["photos"]["VALUE2"][$i]["src"] ?>"/>
                            </a>

                        </div>
                        <?
                    endif;
                endfor
                ?> 
            </div>
            <div class="all-photos-link-block">
                <a href="/gallery/?RESTOURANT=<?= $_REQUEST['RESTOURANT'] ?>" style="color:white !important;" class="button button-right all-photos-link" data-ajax="false">Все фотографии ></a>
            </div>
        </div>
    <? endif; ?>
    <div class="clear"></div>

    <?
    //$arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"] = array();
    //$arResult["DISPLAY_PROPERTIES"]["features"]["DISPLAY_VALUE"] = array();
    //$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = array();
    //$arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"] = array();
    //$arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"] = array();
    $address = explode(', ', $arResult["PROPERTIES"]['address']['VALUE'][0]);
    unset($address[0]);
    $address = implode(', ', $address);
    ?>
    <div class="on-map-block" style="margin-right: 10px; float:left; ">
        <a data-role="button" data-theme="b" data-ajax="false" href="/map.php?lat=<?= $arResult["PROPERTIES"]['lat']['VALUE'][0] ?>&lon=<?= $arResult["PROPERTIES"]['lon']['VALUE'][0] ?>" class="button button-left on-map-link"><?= $distance ?>На карте</a>
    </div>
    <div class="on-map-right-block" style="color: #6E88BF;">
        <?= $address ?><br>
        <?= $arResult["DISPLAY_PROPERTIES"]["subway"]["NAME"] ?>: <?= $arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0] ?>
    </div>

    <div class="clear"></div>
    <a data-role="button" data-theme="e" data-ajax="false" href="/online_order.php?RESTOURANT=<?= $arResult['ID'] ?>&what=<?= $what ?>" class="no-margin-top">Забронировать</a>
    <div class="clear"></div>
    <?
    $n = 0;
    if (!empty($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])):
        ?>
        <div class="opening-hours-block">
            <div class="block-name"><?= $arResult["DISPLAY_PROPERTIES"]["kitchen"]["NAME"] ?></div>
            <?
            if (!is_array($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])) {
                $arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"] = explode(", ", $arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]);
            }
            foreach ($arResult["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"] as $value) {
                echo '<a style="font-size:12px" class="block-value">' . $value . '</a>';
                echo '<div class="clear"></div>';
            }
            ?>
        </div>
        <?
        $n++;
    endif;
    ?>
<? if ($n == 2): ?>
        <div class="clear"></div>
        <div class="line2"></div>
        <?
        $n = 0;
    endif;
    ?>
<? if (!empty($arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"])): ?>
        <div class="opening-hours-block">
            <div class="block-name"><?= $arResult["DISPLAY_PROPERTIES"]["average_bill"]["NAME"] ?></div>
            <div class="block-value" style="font-size:12px"><?= $arResult["DISPLAY_PROPERTIES"]["average_bill"]["DISPLAY_VALUE"][0] ?></div>
        </div>
        <?
        $n++;
    endif;
    ?>
<? if ($n == 2): ?>
        <div class="clear"></div>
        <div class="line2"></div>
        <?
        $n = 0;
    endif;
    ?>
    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"])):
        ?>
        <div class="opening-hours-block">
            <div class="block-name"><?= $arResult["DISPLAY_PROPERTIES"]["opening_hours"]["NAME"] ?></div>
            <div class="block-value" style="font-size:12px"><?= str_replace("0,", "0<br>", $arResult["DISPLAY_PROPERTIES"]["opening_hours"]["DISPLAY_VALUE"][0]) ?></div>
        </div>
        <?
        $n++;
    endif;
    ?>
<? if ($n == 2): ?>
        <div class="clear"></div>
        <div class="line2"></div>
        <?
        $n = 0;
    endif;
    ?>
    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])):
        ?>
        <div class="opening-hours-block">
            <div class="block-name">Телефон</div>
            <?
            if (!is_array($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"])) {
                $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] = explode(", ", $arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]);
            }
            foreach ($arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"] as $value) {                
                if ($value[0]=="8"&&$value[1]=="1")
                    $value = "+7 ".$value;
                if ($value[0]=="(")
                    $value = "+7 ".$value;
                echo '<a class="" style="font-size:12px" data-ajax="true" href="tel:' . $value . '">' . $value . '</a>';
                echo '<div class="clear"></div>';
            }
            ?>            
        </div>
        <?
        $n++;
    endif;
    ?>
<? if ($n == 2): ?>
        <div class="clear"></div>
        <div class="line2"></div>
        <?
        $n = 0;
    endif;
    ?>
<? if (!empty($arResult["DISPLAY_PROPERTIES"]["features"]["DISPLAY_VALUE"])): ?>
        <div class="opening-hours-block">
            <div class="block-name"><?= $arResult["DISPLAY_PROPERTIES"]["features"]["NAME"] ?></div>
            <?
            foreach ($arResult["DISPLAY_PROPERTIES"]["features"]["DISPLAY_VALUE"] as $value) {
                echo '<a style="font-size:12px" class="block-value">' . $value . '</a>';
                echo '<div class="clear"></div>';
            }
            ?>
        </div>
        <?
        $n++;
    endif;
    ?>
    <div class="clear"></div>
    <a data-role="button" data-theme="b" data-ajax="false" href="/add_review.php?RESTOURANT=<?= $_REQUEST['RESTOURANT'] ?>&CITY_ID=<?=CITY_ID?>" class="">Добавить отзыв</a>
    <div class="line2"></div>

    <div class="clear"></div>




    <?
    $arReviewsIB = getArIblock("reviews", CITY_ID);
    global $arrFilter;
    $arrFilter = array();
    $arrFilter["PROPERTY_ELEMENT"] = $arResult["ID"];
    $APPLICATION->IncludeComponent(
            "restoran:catalog.list", "reviews", Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "reviews",
        "IBLOCK_ID" => $arReviewsIB["ID"],
        "NEWS_COUNT" => "3",
        "SORT_BY1" => "created_date",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PAGE_URL"),
        "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "COMMENTS"),
        "CHECK_DATES" => "N",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "search_rest_list",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "URL" => $_REQUEST["url"],
            ), false
    );
    ?>



</div>


<? //v_dump($arResult["PROPERTIES"]["photos"]["VALUE2"])          ?> 