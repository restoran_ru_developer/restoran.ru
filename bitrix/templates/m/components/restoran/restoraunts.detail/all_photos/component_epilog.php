<?php
/*$count = count($_COOKIE["RECENTLY_VIEWED"]["catalog"]);
if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["catalog"]))
    setcookie ("RECENTLY_VIEWED[catalog][".$count."]", $arResult["ID"], time() + 3600*12, "/");*/

global $APPLICATION;
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult["IMAGE"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult["TEXT"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);

if($_REQUEST["CONTEXT"]=="Y"){
	//$_SESSION["CONTEXT"]="Y";
	//var_dump($APPLICATION->get_cookie("CONTEXT"));
	if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
}
?>
<script>
$(document).ready(function(){
    <?if ($_REQUEST["bron"]=="Y"):?>
        $('html,body').animate({scrollTop:600}, "300");
        $.ajax({
                type: "POST",
                url: "/tpl/ajax/online_order_rest.php",
                data: "what="+$("#order_what").val()+"&date="+$(".whose_date").val()+"&name=<?=$arResult["NAME"]?>&id=<?=$arResult["ID"]?>&time="+$(".time").val()+"&person="+$("#person").val()+"&<?=bitrix_sessid_get()?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                    }
                    $('#bron_modal').html(data);
                    showOverflow();
                    setCenter($("#bron_modal"));
                    $("#bron_modal").fadeIn("300"); 
                }
            });
    <?endif;?>
    <?if ($_REQUEST["bron_banket"]=="Y"):?>
        $('html,body').animate({scrollTop:600}, "300");
        $.ajax({
                type: "POST",
                url: "/tpl/ajax/online_order_rest.php",
                data: "what=2&date="+$(".whose_date").val()+"&name=<?=$arResult["NAME"]?>&id=<?=$arResult["ID"]?>&time="+$(".time").val()+"&person="+$("#person").val()+"&<?=bitrix_sessid_get()?>",
                success: function(data) {
                    if (!$("#mail_modal").size())
                    {
                        $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
                    }
                    $('#bron_modal').html(data);
                    showOverflow();
                    setCenter($("#bron_modal"));
                    $("#bron_modal").fadeIn("300"); 
                }
            });
    <?endif;?>
});
</script>