<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="all-photos-page">
    <div class="image-block">
        <? for ($i = 1; $i <= count($arResult["PROPERTIES"]["photos"]["VALUE2"]); $i++): ?>
            <div class="image">
                <? $src = CFile::ResizeImageGet($arResult["PROPERTIES"]["photos"]["VALUE"][$i], Array("width" => 300, "height" => 200), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                <a href="/gallery.php?RESTOURANT=<?=$_REQUEST['RESTOURANT']?>&value=<?=$arResult["PROPERTIES"]["photos"]["VALUE"][$i]?>"><img src="<?= $src['src'] ?>"/></a>
            </div>
        <? endfor ?>
    </div>
    <div class="clear"></div>
</div>
