<br />
<table class="properties more_props" cellpadding="5" cellspacing="0" style="display:none;">
        <?$i=0;?>
        <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
            <?if($arProperty["DISPLAY_VALUE"]&&$arProperty["CODE"]!="add_props"&&$arProperty["CODE"]!="landmarks"&&$arProperty["CODE"]!="number_of_rooms"&&$arProperty["CODE"]!="phone"&&$arProperty["CODE"]!="subway"&&$arProperty["CODE"]!="average_bill"&&$arProperty["CODE"]!="kitchen"&&$arProperty["CODE"]!="area"&&$arProperty["CODE"]!="opening_hours"&&$arProperty["CODE"]!="d_tours"):?>
                <?if ($pid!="map"&&$pid!="RATIO"&&$pid!="COMMENTS"):?>
                    <?=($i%2==0)?"<tr>":""?>
                        <?if($pid=="address"&&$arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"][0]&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):?>
                        <?else:?>
                        <td style="vertical-align:top"><div class="dotted" style="height:12px; margin:0px;"><span class="name"><?=GetMessage("R_PROPERTY_".$arProperty["CODE"])?>:&nbsp; </span></div></td>
                        <?endif;?>
                        <td width="28%" style="vertical-align:top">
                            <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                    <?if ($pid=="site"):?>
                                        <?foreach($arProperty["DISPLAY_VALUE"] as &$value):
                                            $pattern = "/(<a href=\"\/bitrix\/redirect.php?)([^>]*)>/";
                                            $replacement = "$1$2 onclick=\"this.target='_blank'\">";
                                            $site[] = preg_replace($pattern, $replacement, $value);
                                        endforeach;?>                                                                
                                        <?=implode(", ", $site);?>
                                    <?elseif($pid=="address"&&is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):?>

                                    <?else:?>      
                                        <?foreach($arProperty["DISPLAY_VALUE"] as &$value):
                                                $value = strip_tags($value);
                                        endforeach;?>
                                        <?=implode(", ", $arProperty["DISPLAY_VALUE"]);?>
                                    <?endif;?>
                            <?else:?>
                                <?if ($pid=="phone"&&$_REQUEST["CONTEXT"]=="Y"):?>
                                    <?if (CITY_ID=="spb"):?>
                                                <a href="tel:7401820" class="<? echo MOBILE;?>">(812) 740-18-20</a>
                                                    <?else:?>
                                                <a href="tel:9882656" class="<? echo MOBILE;?>">(495) 988-26-56</a>
                                            <?endif;?>  
                                <?elseif ($pid=="email"):?>                                                                  
                                        <?
                                        $email_temp = explode("@",$arProperty["DISPLAY_VALUE"])?>
                                        <script>
                                            document.write("<a href='mailto:"+"<?=$email_temp[0]?>"+"@"+"<?=$email_temp[1]?>"+"'>"+"<?=$email_temp[0]?>"+"@"+"<?=$email_temp[1]?>"+"</a>");
                                        </script> 
                                <?elseif($pid=="address"&&is_array($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])&&count($arResult["DISPLAY_PROPERTIES"]["subway"]["DISPLAY_VALUE"])>5):?>

                                <?else:?>
                                    <?if ($pid=="site"):?>
                                        <?
                                        $site = "";
                                        $link_value = $arProperty["DISPLAY_VALUE"];
                                        $pattern = "/(<a href=\"\/bitrix\/redirect.php?)([^>]*)>/";
                                        $replacement = "$1$2 onclick=\"this.target='_blank'\">";
                                        $site = preg_replace($pattern, $replacement, $link_value);
                                        ?>
                                        <?=$site?>
                                    <?else:?>
                                        <?=strip_tags($arProperty["DISPLAY_VALUE"]);?>
                                    <?endif;?>
                                <?endif;?>
                            <?endif?>
                        </td>
                    <?=($i%2==1)?"</tr>":""?>
                        <?$i++?>
                <?endif;?>
            <?endif;?>                                    
        <?endforeach;?>
        <?if ($arResult["DISPLAY_PROPERTIES"]["landmarks"]["DISPLAY_VALUE"]):?>
            <tr>
                <td style="vertical-align:top"><div class="dotted" style="height:12px; margin:0px;"><span class="name"><?=GetMessage("R_PROPERTY_".$arResult["DISPLAY_PROPERTIES"]["landmarks"]["CODE"])?>:&nbsp; </span></div></td>
                <td colspan="3"><?=$arResult["DISPLAY_PROPERTIES"]["landmarks"]["DISPLAY_VALUE"]?></td>
            </tr>
        <?endif;?>
            <?if ($arResult["DISPLAY_PROPERTIES"]["add_props"]["DISPLAY_VALUE"]):?>
            <tr>
                <td style="vertical-align:top"><div class="dotted" style="height:12px; margin:0px;"><span class="name"><?=GetMessage("R_PROPERTY_".$arResult["DISPLAY_PROPERTIES"]["add_props"]["CODE"])?>:&nbsp; </span></div></td>
                <td colspan="3"><?=$arResult["DISPLAY_PROPERTIES"]["add_props"]["DISPLAY_VALUE"]?></td>
            </tr>
        <?endif;?>
</table>                                            
<br />  