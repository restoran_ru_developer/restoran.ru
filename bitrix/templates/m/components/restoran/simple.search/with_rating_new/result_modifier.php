<?
foreach($arResult["COMMENTS"] as $key=>$arItem) {
    // explode date
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arResult["COMMENTS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["COMMENTS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    $arUser = $rsUser->Fetch();
    if ($arUser["PERSONAL_PROFESSION"])
        $arResult["COMMENTS"][$key]["NAME"] = $arUser["PERSONAL_PROFESSION"];
    else
        $arResult["COMMENTS"][$key]["NAME"] = $arItem["NAME"];  
    $arResult["COMMENTS"][$key]["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL, true);    
    if (!$arResult["COMMENTS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="M")
        $arResult["COMMENTS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/man_nnm.png";
    elseif (!$arResult["COMMENTS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="F")
        $arResult["COMMENTS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/woman_nnm.png";
    elseif (!$arResult["COMMENTS"][$key]["AVATAR"]["src"])
        $arResult["COMMENTS"][$key]["AVATAR"]['src'] = "/tpl/images/noname/unisx_nnm.png";
    
    // get rest name
    $rsSec = CIBlockElement::GetByID($arItem["PROPERTIES"]["ELEMENT"]);
    if($arSec = $rsSec->GetNext()) {
        $pic = CFile::ResizeImageGet($arSec['PREVIEW_PICTURE'], array('width'=>70, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);   
        if (!$pic["src"])
            $pic = CFile::ResizeImageGet($arSec['DETAIL_PICTURE'], array('width'=>60, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);   
        $arResult["COMMENTS"][$key]["RESTORAN"] = Array("ID"=>$arSec["ID"],"NAME"=>$arSec["NAME"],"ACTIVE"=>$arSec["ACTIVE"],"IBLOCK_NAME"=>$arSec["IBLOCK_NAME"],"SRC"=>$pic["src"],"DETAIL_PAGE_URL"=>$arSec["DETAIL_PAGE_URL"],"IBLOCK_TYPE_ID"=>$arSec["IBLOCK_TYPE_ID"]);       
    }
    
    /*$arResult["COMMENTS"][$key]["PREVIEW_TEXT"] = stripslashes($arResult["COMMENTS"][$key]["PREVIEW_TEXT"]);
    $arResult["COMMENTS"][$key]["PREVIEW_TEXT2"] = TruncateText($arResult["COMMENTS"][$key]["PREVIEW_TEXT"], 400);
    
    $arResult["COMMENTS"][$key]["PROPERTIES"]["plus"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["plus"]["TEXT"]);
    $arResult["COMMENTS"][$key]["PROPERTIES"]["plus"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["plus"]["TEXT"], 400);
    
    $arResult["COMMENTS"][$key]["PROPERTIES"]["minus"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["minus"]["TEXT"]);
    $arResult["COMMENTS"][$key]["PROPERTIES"]["minus"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["minus"]["TEXT"], 400);*/
    
    
        $arResult["COMMENTS"][$key]["PREVIEW_TEXT"] = stripslashes($arResult["COMMENTS"][$key]["PREVIEW_TEXT"]);
    $arResult["COMMENTS"][$key]["PREVIEW_TEXT2"] = TruncateText($arResult["COMMENTS"][$key]["PREVIEW_TEXT"], 400);
    $arResult["COMMENTS"][$key]["PROPERTIES"]["pluss"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["plus"]);
    $arResult["COMMENTS"][$key]["PROPERTIES"]["pluss"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["plus"], 400);
    
    $arResult["COMMENTS"][$key]["PROPERTIES"]["minuss"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["minus"]);
    $arResult["COMMENTS"][$key]["PROPERTIES"]["minuss"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["minus"], 400);
        
    foreach ($arResult["COMMENTS"][$key]["PROPERTIES"]["photos"] as &$photo)
    {
        $id = $photo;
        $photo = array();
        $photo["ID"] = $id;
        $photo["SRC"] = CFile::ResizeImageGet($id,array("height"=>124,"width"=>137),BX_RESIZE_IMAGE_EXACT,true);
        $photo["SRC"] = $photo["SRC"]["src"];
        $photo["ORIGINAL_SRC"] = CFile::GetPath($id);
    }
}
?>