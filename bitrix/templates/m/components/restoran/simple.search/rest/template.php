<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

function calculateTheDistance($LAT1, $LON1, $LAT2, $LON2) {

    // перевести координаты в радианы
    $lat1 = $LAT1 * M_PI / 180;
    $lat2 = $LAT2 * M_PI / 180;
    $long1 = $LON1 * M_PI / 180;
    $long2 = $LON2 * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

    $ad = atan2($y, $x);
    $dist = $ad * 6371000;

    return $dist;
}
?>

<div class="search-page">
    <input type="hidden" id="q" value="<?= $_REQUEST["q"] ?>">
    <input type="hidden" id="processing" value="0">
    <input type="hidden" id="maxpage" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
    <? if (isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
        ?>
        <?
    endif;
    if (count($arResult["ITEMS"]) == 1) {
        LocalRedirect('/detail.php?RESTOURANT=' . $arResult["ITEMS"][0]["ELEMENT"]["CODE"]);
    }
    if (count($arResult["ITEMS"]) > 0):
        ?>
        <?
        foreach ($arResult["ITEMS"] as $key => $arItem):
            $lat1 = (float) $arItem["ELEMENT"]["PROPERTIES"]['lat']["VALUE"][0];
            $lon1 = (float) $arItem["ELEMENT"]["PROPERTIES"]['lon']["VALUE"][0];
            $lat2 = (float) $_SESSION['lat'];
            $lon2 = (float) $_SESSION['lon'];
            $dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);
            if ($dist < 1000) {
                $distance = $dist . ' м';
            } elseif (($dist > 1000 && $dist < 10000)) {
                $distance = substr($dist, 0, 1) . '.' . substr($dist, 1, 1) . ' км';
            } elseif (($dist > 10000 && $dist < 100000)) {
                $distance = substr($dist, 0, 2) . '.' . substr($dist, 2, 1) . ' км';
            }
            $address = explode(', ', $arItem["ELEMENT"]["PROPERTIES"]['address']['VALUE'][0]);
            unset($address[0]);
            $address = implode(', ', $address);
            ?>
            <div class="restaurant-block">
                <a class="block-link" data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["ELEMENT"]["CODE"] ?>"></a>
                <div class="image-container">
                    <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["ELEMENT"]["CODE"] ?>"><img src="<?= $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"] ?>" alt="<?= $arItem["ELEMENT"]["NAME"] ?>" title="<?= $arItem["ELEMENT"]["NAME"] ?>" /></a>
                </div>
                <div class="title-container">
                    <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["ELEMENT"]["CODE"] ?>"><?= $arItem["ELEMENT"]["NAME"] ?><? if (substr_count($arItem["URL"], "restaurants")):
                ?>
                            [<?= GetMessage("R_RESTORAN") ?>]
                        <? elseif (substr_count($arItem["URL"], "banket")): ?>
                            [<?= GetMessage("R_BANKET") ?>]
                        <? endif; ?></a>
                    <div class="rating-container">
                        <? for ($i = 1; $i <= round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                            <div class="star-good-small"></div>
                        <? endfor ?>
                        <? for ($i = round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                            <div class="star-bad-small"></div>
                        <? endfor ?>
                        <div class="clear"></div>
                        <div class="distance"><?= $distance ?></div>
                    </div>
                    <div class="address">
                        <?= $address ?>
                    </div>
                    <!--<div class="address">
                        Кухня: <?= implode(", ", $arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]) ?>
                    </div>-->
                </div>
            </div>
            <div class="clear"></div>
            <div class="new-line"></div>
            <div class="clear"></div>
        <? endforeach; ?>

        <div class="clear"></div>
        <div class="rest-ajax"></div>
        <div class="ajax-container hidden">
            <div class="loading2">
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>         
            </div>
        </div>

        <div class="clear"></div>
        <div class="to-top-link-container" onclick="$('html,body').animate({scrollTop: 0 },'slow');">
            <div class="to-top-link">Наверх</div>
        </div>
        <div class="clear"></div>
        <?
        global $search_result;
        $search_result++;
        else:
            echo 'К сожалению, по вашему запросу ничего не найдено.';
    endif;
    ?>                
</div>
