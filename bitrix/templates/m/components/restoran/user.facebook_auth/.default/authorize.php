<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
global $USER;
$arUserData = json_decode($_REQUEST["user_data"]);
//exit;
$step = $_REQUEST["step"];

// save to session and post form


$user = new CUser;

if(!$step) {
        
    $arUserInfo["confirm_code"] = randString(5);
    $arUserInfo["email"] = $arUserData->email;
    $arUserInfo["id"] = $arUserData->id;
    $arUserInfo["phone"] = $_POST["fb_phone"];
    $arUserInfo["name"] = $arUserData->first_name;
    $arUserInfo["last_name"] = $arUserData->last_name;
    $arUserInfo["gender"] = ($arUserData->gender == "male" ? "M" : "F");
    $arUserInfo["city"] = $arUserData->location->name;
    $arUserInfo["fb_user_id"] = $arUserData->id;
    $arUserInfo["picture"] = $arUserData->picture->data->url;
    $arUserInfo["birthday"] = $DB->FormatDate($arUserData->birthday, "MM/DD/YYYY", "DD.MM.YYYY");

    session_start();
    $_SESSION['form_serialize'] = $arUserInfo;
    
    if ($arUserData->gender!="male"&&$arUserData->gender!="female")
        $arUserInfo["gender"] = "";

    // check user fo exist
    $rsUser = CUser::GetByLogin($arUserData->email);
    $arUser = $rsUser->Fetch();

    if ($arUserInfo["picture"])
        {
            $a = CFile::MakeFileArray($arUserInfo["picture"]);
            $a["MODULE_ID"] = "main";
        }
    
    if($arUser) {
        // user VK User ID prop update
        $arUserFields = Array(
            "UF_FB_USER_ID" => $arUserData->id,
            "PERSONAL_PHOTO" => $a
        );
        $user->Update($arUser["ID"], $arUserFields);
        // user authorize
        $USER->Authorize($arUser["ID"]);
        $arResult["TYPE"] = "AUTH";
    } else {
        $arResult["TYPE"] = "NO";
        $new_password = randString(7);

        
        //echo "http://graph.facebook.com/".$arUserInfo["fb_user_id"]."/picture?type=large";
        
        $arUserFields = Array(
            "NAME" => $_SESSION["form_serialize"]["name"],
            "LAST_NAME" => $arUserInfo["last_name"],
            "PERSONAL_PHONE" => $arUserInfo["phone"],
            "EMAIL" => $arUserInfo["email"],
            "LOGIN" => $arUserInfo["email"],
            "PASSWORD" => $new_password,
            "CONFIRM_PASSWORD" => $new_password,
            "CONFIRM_CODE" => $new_password,
            "PERSONAL_GENDER" => $arUserInfo["gender"],
            "PERSONAL_PHOTO" => $a,
            "PERSONAL_CITY" => $arUserInfo["city"],
            "PERSONAL_BIRTHDAY" => $arUserInfo["birthday"],
            "UF_FB_USER_ID" => $arUserInfo["fb_user_id"],
            "GROUP_ID"      => array(5),
        );
        $ID = $user->Add($arUserFields);
        // user authorize
        if($ID) {
            $USER->Authorize($ID);
            $arResult["TYPE"] = "ADD";
        } else {
            $arResult["ERROR"] = $user->LAST_ERROR;
        }
    }
}

if($step == 1 && $_POST["fb_phone"]) {
    // send sms with code via http://sms-kontakt.ru/
    if (CModule::IncludeModule("sozdavatel.sms")) {
        $_SESSION['form_serialize']["phone"] = $_POST["fb_phone"];
        $message = "Код для подтверждения регистрации: ".$_SESSION["form_serialize"]["confirm_code"];
        CSMS::Send($message, $_POST["fb_phone"], "UTF-8");
    }

    $arResult["HTML"] = '<div class="question">Код:<br/>
                    <input id="fb_confirm_code" name="fb_confirm_code" type="text" class="inputtext" required="required" style="width:380px" />
                <input type="hidden" id="step" name="step" value="2" /><br />';
    $arResult["TYPE"] = "REQ_CONFIRM_CODE";
} elseif($step == 2) {
    // check confirm code
    if($_POST["fb_confirm_code"] != $_SESSION["form_serialize"]["confirm_code"]) {
        $arResult["ERROR"]["fb_confirm_code"] = "Неверный код";
    } else {
        // user add
        $new_password = randString(7);

        $arUserFields = Array(
            "NAME" => $_SESSION["form_serialize"]["name"],
            "LAST_NAME" => $_SESSION["form_serialize"]["last_name"],
            "PERSONAL_PHONE" => $_SESSION["form_serialize"]["phone"],
            "EMAIL" => $_SESSION["form_serialize"]["email"],
            "LOGIN" => $_SESSION["form_serialize"]["email"],
            "PASSWORD" => $new_password,
            "CONFIRM_PASSWORD" => $new_password,
            "CONFIRM_CODE" => $new_password,
            "PERSONAL_GENDER" => $_SESSION["form_serialize"]["gender"],
            "PERSONAL_CITY" => $_SESSION["form_serialize"]["city"],
            "UF_FB_USER_ID" => $_SESSION["form_serialize"]["fb_user_id"],
            "GROUP_ID"      => array(5),
        );
        $ID = $user->Add($arUserFields);
        // user authorize
        if($ID) {
            $USER->Authorize($ID);
            $arResult["TYPE"] = "ADD";
        } else {
            $arResult["ERROR"] = $user->LAST_ERROR;
        }
        // clear garbage
        unset($_SESSION['form_serialize']);
    }
}

echo json_encode($arResult);
?>