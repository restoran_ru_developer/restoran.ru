
function getNextP(thisPageNum){
    maxPage = $('#maxpage').val();
    
    $.ajax({
        type: "POST",
        url: "/ajax-filter.php",
        data: {
            PAGEN_1:thisPageNum, 
            CITY_ID:request.CITY_ID, 
            IBLOCK_SECTION_ID:request.IBLOCK_SECTION_ID, 
            set_filter:request.set_filter, 
            arrFilter_pf:request.arrFilter_pf
        },
        success: function(data) {
            $('#processing').val(0);
            $('.ajax-container').hide();
            $(".priorities-ajax").append(data);
            if (thisPageNum == maxPage){
                $('.to-top-link-container').fadeIn();
            }
            
        }
    });
}

$(document).ready(function(){
    var scrH = $(window).height();
    var scrHP = $(".rest-container").height();
    var thisPageNum = 1;
    var thisWork = 1;
    var maxPage = $('#maxpage').val();
    
    $(window).scroll(function(){
        var scro = $(this).scrollTop();
        var scrHP = $(".rest-container").height();
        var scrH2 = 0;
        scrH2 = scrH + scro;
        var leftH = scrHP - scrH2;
        if(leftH < -50){
            if (thisPageNum < maxPage) {
                if($('#processing').val() == 0){
                    $('#processing').val(1);
                    $('.ajax-container').show();
                    thisPageNum = thisPageNum+1;
                    getNextP(thisPageNum);
                }
            }            
        }
    });
});
