<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>
    $(document).ready(function(){    
        request = <?= json_encode($_REQUEST) ?>;
    });
</script>
<? if (count($arResult["ITEMS"]) > 0): ?>    
    <input type="hidden" id="processing" value="0">
    <input type="hidden" id="maxpage" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
    <div class="rest-container">
        <? if ($_REQUEST["page"] || $_REQUEST["PAGEN_1"] || $_REQUEST["letter"]): ?>
            <?
            foreach ($arResult["ITEMS"] as $cell => $arItem):
                $address = explode(', ', $arItem["PROPERTIES"]['address']['VALUE'][0]);
                unset($address[0]);
                $address = implode(', ', $address);
                ?>
                <div class="restaurant-block">
                    <a class="block-link" data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"></a>
                    <div class="image-container">
                        <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" title="<?= $arItem["NAME"] ?>" /></a>
                    </div>
                    <div class="title-container">
                        <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><?= $arItem["NAME"] ?></a>
                        <div class="rating-container">
                            <? for ($i = 1; $i <= round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                                <div class="star-good-small"></div>
                            <? endfor ?>
                            <? for ($i = round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                                <div class="star-bad-small"></div>
            <? endfor ?>
                        </div>
                        
                        <div class="address">
            <?= $address ?>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="new-line"></div>
                <div class="clear"></div>

            <? endforeach; ?> 
    <? endif; ?>
        <div class="priorities-ajax"></div>
        <div class="ajax-container">
            <div class="loading2">
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>         
            </div>
        </div>
        <div class="clear"></div>
        <div class="to-top-link-container" onclick="$('html,body').animate({scrollTop: 0 },'slow');">
            <div class="to-top-link">Наверх</div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
<? endif; ?>
