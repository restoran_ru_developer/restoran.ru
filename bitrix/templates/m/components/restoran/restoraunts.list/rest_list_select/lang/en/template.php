<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["SHOW_CNT_TITLE"] = "List by";
$MESS["SORT_NEW_TITLE"] = "by newest";
$MESS["SORT_PRICE_TITLE"] = "by bill";
$MESS["SORT_RATIO_TITLE"] = "by rating";
$MESS["SORT_ALPHABET_TITLE"] = "by name";
$MESS["R_ADD2FAVORITES"] = "In favorites";
$MESS["BOOK_BUTTON"] = "RESERVE";
$MESS["NOT_FOUND"] = "Unfortunately, the selected filter options did not match";

$MESS["R_PROPERTY_kitchen"] = "Kitchen";
$MESS["R_PROPERTY_average_bill"] = "Average bill";
$MESS["R_PROPERTY_subway"] = "Subway";
$MESS["R_PROPERTY_area"] = "Area";
$MESS["R_PROPERTY_phone"] = "Phone";
$MESS["R_PROPERTY_address"] = "Address";
$MESS["R_PROPERTY_number_of_rooms"] = "Number of halls";
$MESS["R_PROPERTY_opening_hours"] = "Working time";

$MESS["R_PROPERTY_kolichestvochelovek"] = "Persons";
$MESS["R_PROPERTY_out_city"] = "Country";
$MESS["R_PROPERTY_credit_cards"] = "Credit cards";
$MESS["R_PROPERTY_children"] = "For children";
$MESS["R_PROPERTY_features"] = "Features";
$MESS["R_PROPERTY_entertainment"] = "Entertainment";
$MESS["R_PROPERTY_ideal_place_for"] = "Ideal place for";
$MESS["R_PROPERTY_music"] = "Music";
$MESS["R_PROPERTY_parking"] = "Parking";
$MESS["R_PROPERTY_site"] = "Site";
$MESS["R_PROPERTY_breakfast"] = "Breakfast";
$MESS["R_PROPERTY_breakfasts"] = "Breakfast";
$MESS["R_PROPERTY_business_lunch"] = "Business lunch";
$MESS["R_PROPERTY_branch"] = "Brunch";
$MESS["R_PROPERTY_rent"] = "Rent (from)";
$MESS["R_PROPERTY_my_alcohol"] = "Your alcohol";
$MESS["R_PROPERTY_bankets"] = "Banquets";
$MESS["R_PROPERTY_catering"] = "Catering";
$MESS["R_PROPERTY_food_delivery"] = "Food delivery";
$MESS["R_PROPERTY_wi_fi"] = "Wi-fi";
$MESS["R_PROPERTY_administrative_distr"] = "Adm. district";
$MESS["R_PROPERTY_email"] = "Email";
$MESS["R_PROPERTY_proposals"] = "Proposals";
$MESS["R_PROPERTY_add_props"] = "Additional information";
$MESS["R_PROPERTY_landmarks"] = "Landmarks";
$MESS["R_PROPERTY_type"] = "Type";
?>