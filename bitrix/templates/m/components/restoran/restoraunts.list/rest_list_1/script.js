
function getNextP(thisPageNum){
    maxPage = $('#maxpage').val();
    $.ajax({
        type: "POST",
        url: "/mobile/ajax-restaurants.php",
        data: {
            PAGEN_1:thisPageNum
        },
        success: function(data) {
            $('#processing').val(0);
            $('.ajax-container').hide();
            $(".priorities-ajax").append(data);
            if (thisPageNum == maxPage){
                $('.to-top-link-container').fadeIn();
            }
        }
    });
}

$(document).ready(function(){
    var scrH = $(window).height();
    var scrHP = $(".rest-container").height();
    nextPage = $(".priorities").html();
    var thisPageNum = 1;
    var thisWork = 1;
    var maxPage = $('#maxpage').val();
    
    $(window).scroll(function(){
        var scro = $(this).scrollTop();
        var scrHP = $(".rest-container").height();
        var scrH2 = 0;
        scrH2 = scrH + scro;
        var leftH = scrHP - scrH2;
        if(leftH < -20){
            if (thisPageNum < maxPage) {
                if($('#processing').val() == 0){
                    $('#processing').val(1);
                    $('.ajax-container').show();
                    thisPageNum = thisPageNum+1;
                    getNextP(thisPageNum);
                }
            }            
        }
    });
});
