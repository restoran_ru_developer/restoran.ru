<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="priorities">
    <?
    foreach ($arResult["ITEMS"] as $cell => $arItem):
        
        $address = explode(', ', $arItem["PROPERTIES"]['address']['VALUE'][0]);
        unset($address[0]);
        $address = implode(', ', $address);
        ?>
        <div class="restaurant-block">
            <a class="block-link" data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"></a>
            <div class="image-container">
                <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" title="<?= $arItem["NAME"] ?>" /></a>
            </div>
            <div class="title-container">
                <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><?= $arItem["NAME"] ?></a>
                <div class="rating-container">
                    <? for ($i = 1; $i <= round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                        <div class="star-good-small"></div>
                    <? endfor ?>
                    <? for ($i = round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                        <div class="star-bad-small"></div>
    <? endfor ?>
                </div>

                <div class="address">
    <?= $address ?>
                </div>                
            </div>
        </div>
        <div class="clear"></div>
        <div class="new-line"></div>
        <div class="clear"></div>
<? endforeach; ?>
</div>