<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

function calculateTheDistance($LAT1, $LON1, $LAT2, $LON2) {

    // перевести координаты в радианы
    $lat1 = $LAT1 * M_PI / 180;
    $lat2 = $LAT2 * M_PI / 180;
    $long1 = $LON1 * M_PI / 180;
    $long2 = $LON2 * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

    $ad = atan2($y, $x);
    $dist = $ad * 6371000;

    return $dist;
}
?>
<? if (count($arResult["ITEMS"]) > 0): ?>    
    <?
    foreach ($arResult["ITEMS"] as $cell => $arItem):
        $lat1 = (float) $arItem["PROPERTIES"]['lat']["VALUE"][count($arItem["PROPERTIES"]['lat']["VALUE"])-1];
        $lon1 = (float) $arItem["PROPERTIES"]['lon']["VALUE"][count($arItem["PROPERTIES"]['lon']["VALUE"])-1];                
        $lat2 = (float) $_SESSION['lat'];
        $lon2 = (float) $_SESSION['lon'];
        if ($_REQUEST["pageRestSort"]=="distance")
            $dist = $arItem["DISTANCE"];
        else
            $dist = (int) calculateTheDistance($lat1, $lon1, $lat2, $lon2);

        if ($dist < 1000)
                    $distance = round($dist,0) . ' м';
                else
                    $distance = round($dist/1000,2).' км';
        $address = explode(', ', $arItem["PROPERTIES"]['address']['VALUE'][0]);
        unset($address[0]);
        $address = implode(', ', $address);
        ?>
        <div class="restaurant-block">
            <a class="block-link" data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"></a>
            <div class="image-container">
                <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" title="<?= $arItem["NAME"] ?>" /></a>
            </div>
            <div class="title-container">
                <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><?= $arItem["NAME"] ?></a>
                <div class="rating-container">
                    <? for ($i = 1; $i <= round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                        <div class="star-good-small"></div>
                    <? endfor ?>
                    <? for ($i = round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                        <div class="star-bad-small"></div>
                    <? endfor ?>
                    <div class="clear"></div>
                    <div class="distance"><?= $distance ?></div>
                </div>

                <div class="address">
                    <?= $address ?>
                    <br>
                    <div style="padding-left: 110px;">
                        <? if ($arItem["PROPERTIES"]["average_bill"]["VALUE"][0] == "1997"): ?>
                            <div class="bill-icon"></div>
                            <div class="bill-icon-opacity"></div>
                            <div class="bill-icon-opacity"></div>
                            <div class="bill-icon-opacity"></div>
                            <div class="bill-icon-opacity"></div>
                        <? endif; ?>
                        <? if ($arItem["PROPERTIES"]["average_bill"]["VALUE"][0] == "1925"): ?>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon-opacity"></div>
                            <div class="bill-icon-opacity"></div>
                            <div class="bill-icon-opacity"></div>
                        <? endif; ?>
                        <? if ($arItem["PROPERTIES"]["average_bill"]["VALUE"][0] == "1953"): ?>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon-opacity"></div>
                            <div class="bill-icon-opacity"></div>
                        <? endif; ?>
                        <? if ($arItem["PROPERTIES"]["average_bill"]["VALUE"][0] == "1998"): ?>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon-opacity"></div>
                        <? endif; ?>
                        <? if ($arItem["PROPERTIES"]["average_bill"]["VALUE"][0] == "6703"): ?>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                            <div class="bill-icon"></div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>        
        <div class="line2"></div>
    <? endforeach; ?>
<? endif; ?>