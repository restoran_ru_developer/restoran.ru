<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
foreach ($arResult["ITEMS"] as $key => $arItem): //v_dump($arItem["ID"]);

    $restArray = explode('/', $arItem["RESTORAN"]["DETAIL_PAGE_URL"]);
    $restName = $restArray[count($restArray) - 2];
    ?>

    <div class="one-opinion">
        <? if (!$_REQUEST["CODE"] && !$_REQUEST["id"]): ?>
            <div class="clear"></div>
            <div class="dotted"></div>
        <? endif; ?>

        <span class="name"><a style="font-weight: bold; color:#009BDC; text-decoration: none;" class="" data-ajax="true" href="/users/id<?= $arItem["CREATED_BY"] ?>/"><?= $arItem["NAME"] ?></a></span>                
        <div class="name right"><a style="font-weight: bold; text-decoration: none;" class="another" data-ajax="true" href="/detail.php?RESTOURANT=<?= $restName ?>"><?= $arItem["PROPERTIES"]["ELEMENT"][0] ?></a></div>
        <div class="clear"></div>
        <div class="comment_date"><?= $arItem["DISPLAY_ACTIVE_FROM_DAY"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_MONTH"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_YEAR"] ?>, <?= $arItem["DISPLAY_ACTIVE_FROM_TIME"] ?></div>                

        <? if ((int) $arItem["DETAIL_TEXT"] > 0): ?>
            <div class="rating-container right-imp no-margin">                        
                <? for ($i = 1; $i <= $arItem["DETAIL_TEXT"]; $i++): ?>
                    <div class="star-good-small"></div>
                <? endfor ?>
                <? for ($i = $arItem["DETAIL_TEXT"]; $i <= 4; $i++): ?>
                    <div class="star-bad-small"></div>
                <? endfor ?>
            </div>
        <? endif; ?>
        <div class="clear"></div>
        <div class="left" style="margin-top:5px;">
            <div class="preview_text<?= $arItem["ID"] ?>">
                <?=$arItem["PREVIEW_TEXT2"] ?>
            </div>
            <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                <div class="preview_text<?= $arItem["ID"] ?>" style="display:none;">
                    <?=$arItem["PREVIEW_TEXT"] ?>
                </div>
            <? endif; ?>
        </div>
        <div class="clear"></div>
        <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
            <div class="right">                
                <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();"><a class="razvernut"></a></div>
                <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();" style="display:none"><a class="svernut"></a></div>
            </div>
        <? endif; ?>
        <div class="left">                
            <a class="answer" data-ajax="false" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="color:#2489CE;">Ответить</a>
            <a class="active-answer" data-ajax="false" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="color:#2489CE;">Ответить</a>
        </div>
        <div class="clear"></div>
        <?
        foreach ($arItem["PROPERTIES"]["photos"] as $value) {
            if ($value["SRC"] !== null) {
                ?>
                <a data-ajax="false" href="/opinion/index.php?tid=<?= $arItem["ID"] ?>">
                    <img class="review-photo" src="<?= $value["SRC"] ?>">
                </a>
                <?
            }
        }
        ?>
        <div class="clear"></div>
        <div class="line2"></div>
    </div>
<? endforeach; ?>

    
