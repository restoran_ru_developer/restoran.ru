<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "отзыв";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "отзыва";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "отзывов";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Далее";
$MESS["SHOW_CNT_TITLE"] = "Выводить по";
$MESS["SORT_NEW_TITLE"] =" по новизне";
$MESS["SORT_POPULAR_TITLE"] =" по популярности";
$MESS["ALL_REVIEWS_catalog"] ="Все отзывы";
$MESS["ALL_REVIEWS_kupons"] ="Все отзывы купона";
$MESS["REVIEWS_ANSWERS"] ="Ответов: ";
$MESS["REVIEWS_ANSWER"] ="Ответить";
$MESS["RE_RAZVER"] ="Развернуть";
$MESS["RE_SVER"] ="Свернуть";
$MESS["R_PLUS"] = "ДОСТОИНСТВА";
$MESS["R_MINUS"] = "НЕДОСТАТКИ";
$MESS["NO_REVIEWS"] = "Не добавлено ни одного отзыва. Вы можете быть первыми.";
?>