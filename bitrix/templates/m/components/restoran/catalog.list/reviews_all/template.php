<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<!--<script src="<?= SITE_TEMPLATE_PATH ?>/js/flowplayer.js"></script>-->
<script>
    $(document).ready(function(){    
        //$("#photogalery").galery({});
        $("body").append("<div class=big_modal><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        //setCenter($(".big_modal"));
        $(".slider_left").on("click",function(){
            change_big_modal_prev();
            //setTimeout("change_big_modal_prev()",100);
        });
        
        $(".razvernut").on("click",function(){
            $(this).parent().parent().parent().addClass('razver-background');
            $(this).parent().parent().parent().find('.answer').hide();
            $(this).parent().parent().parent().find('.active-answer').show();
            //return false;
        });
        
        $(".svernut").on("click",function(){
            $(this).parent().parent().parent().removeClass('razver-background');
            $(this).parent().parent().parent().find('.answer').show();
            $(this).parent().parent().parent().find('.active-answer').hide();
            //return false;
        });
        
        $(".slider_right").on("click",function(){
            change_big_modal_next();
            //setTimeout("change_big_modal_next()",100);
            //change_big_modal_next();
        });
        $(".big_modal").hover(function(){
            $(".big_modal .slider_left").fadeIn(300);
            $(".big_modal .slider_right").fadeIn(300);
        },function(){
            $(".big_modal .slider_left").fadeOut(300);
            $(".big_modal .slider_right").fadeOut(300);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_modal img").attr("src",img.src);
            $(".big_modal img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_modal img").css({"width":"0px","height":"0px"});
            img.onload = function(){
                $(".big_modal img").css({"width":img.width,"height":img.height});
                $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                
                $(".big_modal").fadeIn(300);   
            }            
        });
        $(window).resize(function(){
            if (!$(".big_modal").is("hidden"))
            {
                var img = new Image();
                img.src = $(".big_modal").find("img").attr("src");
                if ($(window).width()<1100||$(window).height()<700)
                {
                    img.width = img.width*0.8;
                    img.height = img.height*0.8;
                }
                $(".big_modal img").css({"width":img.width,"height":img.height});
                $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
            }
        });
        var img2 = new Array()
        $(".photog img").each(function(n){
            img2[n] = new Image();
            img2[n].src = $(this).attr("alt");
        });
    });
</script>
<div id="comments" class="review-container">
    <? //v_dump($arResult);?>
    <? if ($_REQUEST["RESTOURANT"] && $arResult["ITEMS"][0]["PROPERTIES"]["ELEMENT"][0]): ?>
        <h2 style="margin: 0px 0px 0px 0px;">Отзывы о <?= $arResult["ITEMS"][0]["PROPERTIES"]["ELEMENT"][0] ?></h2>
    <? endif; ?>
    <? if ($_REQUEST["RESTOURANT"]): ?>    
    <a data-role="button" data-theme="b" data-ajax="false" href="/add_review.php<? if ($_REQUEST["RESTOURANT"]) {
        echo '?RESTOURANT=' . $_REQUEST["RESTOURANT"];
    } ?>" class="">Добавить отзыв</a>
        <? endif; ?>
    <input type="hidden" id="maxpagereview" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
    <input type="hidden" id="photos" value="<?= $_REQUEST["photos"] ?>">
    <? if ($arResult["ITEMS"][0]["ID"]): ?>

        <?
        foreach ($arResult["ITEMS"] as $key => $arItem): //v_dump($arItem["ID"]);

            $restArray = explode('/', $arItem["RESTORAN"]["DETAIL_PAGE_URL"]);
            $restName = $restArray[count($restArray) - 2];
            ?>

            <div class="one-opinion">
                <? if (!$_REQUEST["CODE"] && !$_REQUEST["id"]): ?>
                    <div class="clear"></div>
                    <div class="dotted"></div>
                <? endif; ?>

                <span class="name"><a class="" data-ajax="true" href="/user.php?USER_ID=<?= $arItem["CREATED_BY"] ?>"><?= $arItem["NAME"] ?></a></span>     
                <? if (!$_REQUEST["RESTOURANT"]): ?>
                    <div class="name right"><a class="another" data-ajax="true" href="/detail.php?RESTOURANT=<?= $restName ?>"><?= $arItem["PROPERTIES"]["ELEMENT"][0] ?></a></div>
        <? endif; ?>
                <div class="clear"></div>
                <div class="comment_date"><?= $arItem["DISPLAY_ACTIVE_FROM_DAY"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_MONTH"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_YEAR"] ?>, <?= $arItem["DISPLAY_ACTIVE_FROM_TIME"] ?></div>                

                    <? if ((int) $arItem["DETAIL_TEXT"] > 0): ?>
                    <div class="rating-container right-imp no-margin">                        
                        <? for ($i = 1; $i <= $arItem["DETAIL_TEXT"]; $i++): ?>
                            <div class="star-good-small"></div>
                        <? endfor ?>
                        <? for ($i = $arItem["DETAIL_TEXT"]; $i <= 4; $i++): ?>
                            <div class="star-bad-small"></div>
                    <? endfor ?>
                    </div>
        <? endif; ?>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="left" style="margin-top:5px;">
                    <div class="preview_text<?= $arItem["ID"] ?>">
                    <?= $arItem["PREVIEW_TEXT2"] ?>
                    </div>
                        <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                        <div class="preview_text<?= $arItem["ID"] ?>" style="display:none;">
                        <?= $arItem["PREVIEW_TEXT"] ?>
                        </div>
                <? endif; ?>
                </div>
                <div class="clear"></div>
        <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                    <div class="right">                
                        <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();"><a class="razvernut"></a></div>
                        <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();" style="display:none"><a class="svernut"></a></div>
                    </div>
        <? endif; ?>    
                <!--<div class="left">                
                    <div class="answers"><?= GetMessage("REVIEWS_ANSWERS") ?> <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments" class="another"><?= ($arItem["PROPERTIES"]["COMMENTS"]) ? $arItem["PROPERTIES"]["COMMENTS"] : "0" ?></a></div>
                </div>-->
                <div class="left">                
                    <a class="answer" data-ajax="false" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="color:#2489CE;">Ответить</a>
                    <a class="active-answer" data-ajax="false" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="color:#2489CE;">Ответить</a>
                </div>
                <div class="clear"></div>
                <?
                foreach ($arItem["PROPERTIES"]["photos"] as $value) {
                    if ($value["SRC"] !== null) {
                        ?>
                        <a data-ajax="false" href="/opinion/index.php?tid=<?= $arItem["ID"] ?>">
                            <img class="review-photo" src="<?= $value["SRC"] ?>">
                        </a>
                        <?
                    }
                }
                ?>
                <div class="clear"></div>
            </div>
            <? if (end($arResult["ITEMS"]) != $arItem): ?>
                <div class="line2"></div>
            <? endif; ?>
        <? endforeach; ?>  
    <? else: ?>
        <p style="font-style:italic"><?= GetMessage("NO_REVIEWS") ?></p>
<? endif; ?>
    <? if ($arParams["URL"]): ?>
        <Br />
        <p align="right"><a data-ajax="true" href="<?= str_replace("detailed", "opinions", $arParams["URL"]) ?>" class="uppercase"><?= GetMessage("ALL_REVIEWS_catalog") ?></a></p>
    <? endif; ?>
    <div class="clear"></div>
    <div class="comments-ajax"></div>
<? if ((int) $arResult["NAV_RESULT"]->NavPageCount > 1): ?>
        <div class="ajax-container" style="text-align: center; padding: 0; font-size: 24px;">
            Показать ещё
        </div>
<? endif; ?>

    <div class="clear"></div>
    <div class="to-top-link-container" onclick="$('html,body').animate({scrollTop: 0 },'slow');">
        <div class="to-top-link">Наверх</div>
    </div>
    <div class="clear"></div>
</div>
