<?
//v_dump($arResult["ITEMS"][0]["PROPERTIES"]["photos"]);
if (count($arResult["ITEMS"][0]["PROPERTIES"]["photos"]) == 1) {
    foreach ($arResult["ITEMS"][0]["PROPERTIES"]["photos"] as $value) :
        $srcBig = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 1000, "height" => 650), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $srcSmall = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 200, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($srcBig) && !empty($srcSmall)) :
            ?>
            <li><a href="<?= $srcBig['src'] ?>" rel="external"><img src="<?= $srcSmall['src'] ?>" alt="" /></a></li>
            <?
        endif;
    endforeach;
    foreach ($arResult["ITEMS"][0]["PROPERTIES"]["photos"] as $value) :
        $srcBig = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 1000, "height" => 650), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $srcSmall = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 200, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($srcBig) && !empty($srcSmall)) :
            ?>
            <li><a href="<?= $srcBig['src'] ?>" rel="external"><img src="<?= $srcSmall['src'] ?>" alt="" /></a></li>
            <?
        endif;
    endforeach;
    foreach ($arResult["ITEMS"][0]["PROPERTIES"]["photos"] as $value) :
        $srcBig = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 1000, "height" => 650), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $srcSmall = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 200, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($srcBig) && !empty($srcSmall)) :
            ?>
            <li><a href="<?= $srcBig['src'] ?>" rel="external"><img src="<?= $srcSmall['src'] ?>" alt="" /></a></li>
            <?
        endif;
    endforeach;
}

if (count($arResult["ITEMS"][0]["PROPERTIES"]["photos"]) == 2) {
    foreach ($arResult["ITEMS"][0]["PROPERTIES"]["photos"] as $value) :
        $srcBig = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 1000, "height" => 650), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $srcSmall = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 200, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($srcBig) && !empty($srcSmall)) :
            ?>
            <li><a href="<?= $srcBig['src'] ?>" rel="external"><img src="<?= $srcSmall['src'] ?>" alt="" /></a></li>
            <?
        endif;
    endforeach;
    foreach ($arResult["ITEMS"][0]["PROPERTIES"]["photos"] as $value) :
        $srcBig = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 1000, "height" => 650), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $srcSmall = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 200, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($srcBig) && !empty($srcSmall)) :
            ?>
            <li><a href="<?= $srcBig['src'] ?>" rel="external"><img src="<?= $srcSmall['src'] ?>" alt="" /></a></li>
            <?
        endif;
    endforeach;
}

if (count($arResult["ITEMS"][0]["PROPERTIES"]["photos"]) > 2) {
    foreach ($arResult["ITEMS"][0]["PROPERTIES"]["photos"] as $value) :
        $srcBig = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 1000, "height" => 650), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $srcSmall = CFile::ResizeImageGet((int) $value["ID"], Array("width" => 200, "height" => 130), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($srcBig) && !empty($srcSmall)) :
            ?>
            <li><a href="<?= $srcBig['src'] ?>" rel="external"><img src="<?= $srcSmall['src'] ?>" alt="" /></a></li>
            <?
        endif;
    endforeach;
}
?>