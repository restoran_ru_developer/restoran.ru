<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--<script src="<?= SITE_TEMPLATE_PATH ?>/js/flowplayer.js"></script>-->
<script>    
    $(document).ready(function(){                    
        $(".razvernut").on("click",function(){
            $(this).parent().parent().parent().addClass('razver-background');
            $(this).parent().parent().parent().find('.answer').hide();
            $(this).parent().parent().parent().find('.active-answer').show();
            //return false;
        });
        
        $(".svernut").on("click",function(){
            $(this).parent().parent().parent().removeClass('razver-background');
            $(this).parent().parent().parent().find('.answer').show();
            $(this).parent().parent().parent().find('.active-answer').hide();
            //return false;
        });                
    });
</script>
<div id="comments">
    <? if ($arResult["ITEMS"][0]["ID"]): ?>
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        
            <div class="one-opinion">
                <? if (!$_REQUEST["CODE"] && !$_REQUEST["id"]): ?>
                    <div class="clear"></div>
                    <div class="dotted"></div>
                <? endif; ?>

                <span class="name"><a class="another" data-ajax="true" href="/user.php?USER_ID=<?= $arItem["CREATED_BY"] ?>"><?= $arItem["NAME"] ?></a></span>
                <div class="clear"></div>
                <div class="comment_date"><?= $arItem["DISPLAY_ACTIVE_FROM_DAY"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_MONTH"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_YEAR"] ?>, <?= $arItem["DISPLAY_ACTIVE_FROM_TIME"] ?></div>
                <? if ((int) $arItem["DETAIL_TEXT"] > 0): ?>
                    <div class="rating-container right-imp no-margin">
                        <? for ($i = 1; $i <= $arItem["DETAIL_TEXT"]; $i++): ?>
                            <div class="star-good-small"></div>
                        <? endfor ?>
                        <? for ($i = $arItem["DETAIL_TEXT"]; $i <= 4; $i++): ?>
                            <div class="star-bad-small"></div>
                        <? endfor ?>
                    </div>
                <? endif; ?>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="left review_text" >
                    <div class="preview_text<?= $arItem["ID"] ?>">
                        <?=$arItem["PREVIEW_TEXT2"] ?>
                    </div>
                    <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                        <div class="preview_text<?= $arItem["ID"] ?>" style="display:none;">
                            <?=$arItem["PREVIEW_TEXT"] ?>
                        </div>
                    <? endif; ?>
                </div>
                <div class="clear"></div>
                <? //v_dump($arItem["PREVIEW_TEXT"])?>
                <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                    <div class="right review_text">                
                        <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();"><a class="razvernut"></a></div>
                        <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();" style="display:none"><a class="svernut"></a></div>
                    </div>
                <? endif; ?>    
                <!--<div class="left">                
                    <div class="answers"><?= GetMessage("REVIEWS_ANSWERS") ?> <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>#comments" class="another"><?= ($arItem["PROPERTIES"]["COMMENTS"]) ? $arItem["PROPERTIES"]["COMMENTS"] : "0" ?></a></div>
                </div>-->
                <div class="left">              
                    <a class="answer" data-ajax="false" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" style="color:#2489CE;">Ответить</a>
                    <a class="active-answer" data-ajax="false" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" data-ajax="false" style="color:#2489CE;">Ответить</a>
                </div>
                <div class="clear"></div>
            </div>
            <?if (end($arResult["ITEMS"])!=$arItem):?>
                <div class="line2"></div>
            <?endif;?>
        <? endforeach; ?>
    <? else: ?>
        <p style="font-style:italic"><?= GetMessage("NO_REVIEWS") ?></p>
    <? endif; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"] == "Y"): ?>
        <hr class="bold" />        
        <div class="left">
            <?= GetMessage("SHOW_CNT_TITLE") ?>&nbsp;
            <?= ($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" data-ajax="true" href="?page=' . $_REQUEST["PAGEN_1"] . ($_REQUEST["pageSort"] ? "&pageSort=" . $_REQUEST["pageSort"] : "") . '">20</a>') ?>
            <?= ($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" data-ajax="true" href="?page=' . $_REQUEST["PAGEN_1"] . '&pageCnt=40' . ($_REQUEST["pageSort"] ? "&pageSort=" . $_REQUEST["pageSort"] : "") . '">40</a>') ?>
            <?= ($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" data-ajax="true" href="?page=' . $_REQUEST["PAGEN_1"] . '&pageCnt=60' . ($_REQUEST["pageSort"] ? "&pageSort=" . $_REQUEST["pageSort"] : "") . '">60</a>') ?>
        </div>
        <div class="right">            
            <?= $arResult["NAV_STRING"] ?>            
        </div>    
        <div class="clear"></div>
        <Br /><Br />
    <? endif; ?>
    <? if ($arParams["URL"]): ?>
        <p align="right"><a data-ajax="true" href="<?= str_replace("detailed", "opinions", $arParams["URL"]) ?>" class="uppercase"><?= GetMessage("ALL_REVIEWS_catalog") ?></a></p>
    <? endif; ?>
    <div class="clear"></div>
    <Br />
    <div class="end-page">        
        <? if ($arResult["ITEMS"][$key]["RESTORAN"]["ACTIVE"] == "Y"): ?>
            <a style="width:100%" data-role="button" data-theme="b" data-mini="false" class="right" data-ajax="false" href="/opinions.php?RESTOURANT=<?= $_REQUEST["RESTOURANT"] ?>">Все отзывы ресторана</a>
        <? endif; ?>
    </div>
    <div class="clear"></div>
</div>
