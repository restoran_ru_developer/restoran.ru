<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--<script src="<?= SITE_TEMPLATE_PATH ?>/js/flowplayer.js"></script>-->
<script>
    var img = '';
    function change_big_modal_next()
    {
        $(".big_modal img").fadeOut(300,function(){
            img = new Image();                
            if ($("#"+$(this).attr("alt")).parent().next().hasClass("left"))
            {
                img.src = $("#"+$(this).attr("alt")).parent().next().find("img").attr("alt");                                            
                $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().next().find("img").attr("id"));
            }
            else
            {
                img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("alt");                                            
                $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left").eq(0).find("img").attr("id"));                    
            }
            $(".big_modal img").attr("src",img.src);
            img.onload = new function(){                                        
                $(".big_modal img").css({"width":"0px","height":"0px"});
                $(".big_modal img").css({"width":img.width,"height":img.height});
                $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                $(".big_modal img").fadeIn(300); 
            }
            //showOverflow();               

        });                                            

    }
    function change_big_modal_prev()
    {       
        $(".big_modal img").fadeOut(300,function(){
            var img = new Image();
            if ($("#"+$(this).attr("alt")).parent().prev().hasClass("left"))
            {
                img.src = $("#"+$(this).attr("alt")).parent().prev().find("img").attr("alt");                                            
                $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().prev().find("img").attr("id"));
            }
            else
            {
                img.src = $("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("alt");                                            
                $(".big_modal img").attr("alt",$("#"+$(this).attr("alt")).parent().parent().find(".left:last").find("img").attr("id"));                    
            }
            $(".big_modal img").attr("src",img.src);
            //showOverflow();
            img.onload = new function(){
                //$(".big_modal img").css({"width":"0px","height":"0px"});
                $(".big_modal img").css({"width":img.width,"height":img.height});
                $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                $(".big_modal img").fadeIn(300); 
            }
        });                                          

    }
    $(document).ready(function(){    
        //$("#photogalery").galery({});
        $("body").append("<div class=big_modal><div class='slider_left'><a></a></div><img src='' /><div class='slider_right'><a></a></div></div>");
        //setCenter($(".big_modal"));
        $(".slider_left").on("click",function(){
            change_big_modal_prev();
            //setTimeout("change_big_modal_prev()",100);
        });
        
        $(".razvernut").on("click",function(){
            $(this).parent().parent().parent().addClass('razver-background');
            $(this).parent().parent().parent().find('.answer').hide();
            $(this).parent().parent().parent().find('.active-answer').show();
            //return false;
        });
        
        $(".svernut").on("click",function(){
            $(this).parent().parent().parent().removeClass('razver-background');
            $(this).parent().parent().parent().find('.answer').show();
            $(this).parent().parent().parent().find('.active-answer').hide();
            //return false;
        });
        
        $(".slider_right").on("click",function(){
            change_big_modal_next();
            //setTimeout("change_big_modal_next()",100);
            //change_big_modal_next();
        });
        $(".big_modal").hover(function(){
            $(".big_modal .slider_left").fadeIn(300);
            $(".big_modal .slider_right").fadeIn(300);
        },function(){
            $(".big_modal .slider_left").fadeOut(300);
            $(".big_modal .slider_right").fadeOut(300);
        });
        $(".photog").find("img").click(function(){
            var img = new Image();
            img.src = $(this).attr("alt");
            $(".big_modal img").attr("src",img.src);
            $(".big_modal img").attr("alt",$(this).attr("id"));
            showOverflow();
            $(".big_modal img").css({"width":"0px","height":"0px"});
            img.onload = function(){
                $(".big_modal img").css({"width":img.width,"height":img.height});
                $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
                
                $(".big_modal").fadeIn(300);   
            }            
        });
        $(window).resize(function(){
            if (!$(".big_modal").is("hidden"))
            {
                var img = new Image();
                img.src = $(".big_modal").find("img").attr("src");
                if ($(window).width()<1100||$(window).height()<700)
                {
                    img.width = img.width*0.8;
                    img.height = img.height*0.8;
                }
                $(".big_modal img").css({"width":img.width,"height":img.height});
                $(".big_modal").css({"width":+img.width,"height":+img.height,"top":+eval($(window).height()/2-img.height/2),"left":+eval($(window).width()/2 - img.width/2)});        
            }
        });
        var img2 = new Array()
        $(".photog img").each(function(n){
            img2[n] = new Image();
            img2[n].src = $(this).attr("alt");
        });
        
        ajax_load = 0;
    });
</script>
<div id="comments">
    
    <? if ($arResult["ITEMS"][0]["ID"]): ?>
        <? foreach ($arResult["ITEMS"] as $key => $arItem): //v_dump($arItem);?>
            <div class="one-opinion">
                <? if (!$_REQUEST["CODE"] && !$_REQUEST["id"]): ?>
                    <div class="clear"></div>
                    <div class="dotted"></div>
                <? endif; ?>

                <span class="name"><a class="another" data-ajax="true" href="/users/id<?= $arItem["CREATED_BY"] ?>/"><?= $arItem["NAME"] ?></a></span>
                <div class="clear"></div>
                <div class="comment_date"><?= $arItem["DISPLAY_ACTIVE_FROM_DAY"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_MONTH"] ?> <?= $arItem["DISPLAY_ACTIVE_FROM_YEAR"] ?>, <?= $arItem["DISPLAY_ACTIVE_FROM_TIME"] ?></div>
                <? if ((int) $arItem["DETAIL_TEXT"] > 0): ?>
                    <div class="rating-container right-imp no-margin">
                        <? for ($i = 1; $i <= $arItem["DETAIL_TEXT"]; $i++): ?>
                            <div class="star-good-small"></div>
                        <? endfor ?>
                        <? for ($i = $arItem["DETAIL_TEXT"]; $i <= 4; $i++): ?>
                            <div class="star-bad-small"></div>
                        <? endfor ?>
                    </div>
                <? endif; ?>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="left">
                    <div class="preview_text<?= $arItem["ID"] ?>">
                        <?=$arItem["PREVIEW_TEXT2"] ?>
                    </div>
                    <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                        <div class="preview_text<?= $arItem["ID"] ?>" style="display:none;">
                            <?=$arItem["PREVIEW_TEXT"] ?>
                        </div>
                    <? endif; ?>
                </div>
                <div class="clear"></div>
                <? if (strlen($arItem["PREVIEW_TEXT2"]) != strlen($arItem["PREVIEW_TEXT"])): ?>
                    <div class="right">                
                        <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();"><a class="razvernut"></a></div>
                        <div class="razver razver<?= $arItem["ID"] ?>" onclick="$('.preview_text<?= $arItem["ID"] ?>').toggle(); $('.razver<?= $arItem["ID"] ?>').toggle();" style="display:none"><a class="svernut"></a></div>
                    </div>
                <? endif; ?>    
                
                <div class="clear"></div>
            </div>
            <div class="new-line"></div>
        <? endforeach; ?>  
    <? endif; ?>

    <div class="clear"></div>
</div>
