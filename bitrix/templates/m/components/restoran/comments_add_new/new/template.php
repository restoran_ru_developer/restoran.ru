<?
$APPLICATION->AddHeadString('<link href="' . $templateFolder . '/fineuploader.css" rel="stylesheet" type="text/css"/>');
?>
<? if (!$USER->IsAuthorized()): ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "no_auth", Array(
        "REGISTER_URL" => "",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "",
        "SHOW_ERRORS" => "N"
            ), false
    );
    ?> 
    <br /><br />
<? endif; ?>
<? if ($arParams["OPEN"] == "Y"): ?>
    <div align="right">
        <input type="button" id="wr_cm" class="light_button" value="<?= GetMessage("LEAVE_COMMENT") ?>"/>        
    </div>
    <div id="write_comment" <?= ($arParams["OPEN"] == "Y") ? 'style="display:none"' : '' ?>> 
        <h2><?= GetMessage("YOUR_COMMENT") ?></h2>
    <? endif; ?>    
    <form name="attach" action="/bitrix/components/restoran/comments_add_new/ajax.php" method="post" id="comment_form<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>" enctype="multipart/form-data">
        <div class="comment-form">
            <?= bitrix_sessid_post() ?>
            <input type="hidden" name="IBLOCK_TYPE" value="<?= $arParams["IBLOCK_TYPE"] ?>"/>
            <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>"/>
            <input type="hidden" name="ELEMENT_ID" value="<?= $arParams["ELEMENT_ID"] ?>"/>
            <input type="hidden" name="PARENT" value="<?= $arParams["PARENT"] ?>"/>
            <? if (!$USER->IsAuthorized()): ?>
                <div class="uppercase font10 ls1" style="padding-left:6px;">E-mail:</div>
                <input class="inputtext font14" value="" name="email" req="req" style="width:100%" data-theme="d" /><br />
                <div class="uppercase font10 ls1" style="padding-left:6px;padding-top:5px;"><?= GetMessage("R_COMMENT") ?>:</div>        
            <? endif ?>        
            <textarea data-theme="d" rows="4" cols="50" name="review" class="comment_textarea"></textarea>
            <br />
            <div class="img-container" id="img-container<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>">
                <div class="clear"></div>
            </div>
            <? if (!$USER->IsAuthorized()): ?>
                <div>            
                    <div class="QapTcha"></div>
                    <? /* <link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css" />
                      <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
                      <script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
                      <script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
                      <script type="text/javascript">
                      $(document).ready(function(){
                      $('.QapTcha').QapTcha({
                      txtLock : '<?=GetMessage("MOVE_SLIDER")?>',
                      txtUnlock : '<?=GetMessage("MOVE_SLIDER_DONE")?>',
                      disabledSubmit : true,
                      autoRevert : true,
                      PHPfile : '/tpl/js/quaptcha/Qaptcha.jquery.php',
                      autoSubmit : false});
                      });
                      </script> */ ?> 
                    <div class="clear"></div>
                    <br />
                </div>    
            <? endif; ?>




            <div class="right">
                <div class="left attach" style="color: black;font-size: 14px;font-weight: bold; margin-top: 7px;">Прикрепить:</div>
                <div id="attach_photo<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>" class="left photo_icon"></div>
                <!--<div id="attach_video<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>" class="left video_icon"></div>        -->
            </div>
            <div class="clear"></div>
            <input class="light_button add-comm-mobile" <? if (!$USER->IsAuthorized()) {
                echo 'disabled="disabled"';
            } ?> type="submit" value="<?= GetMessage("COMMENT") ?>">
        </div>    
    </form>
    <? if ($arParams["OPEN"] == "Y"): ?>
    </div>
<? endif; ?>
<script>
    $(document).ready(function(){
        var files = new Array();
        var errorHandler = function(event, id, fileName, reason) {
            qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
        $('#attach_photo<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>').fineUploader({
            text: {
                uploadButton: "",
                cancelButton: "",
                waitingForResponse: ""
            },
            multiple: true,
            disableCancelForFormUploads: true,
            request: {
                endpoint: "<?= $templateFolder ?>/upload.php",
                params: {"generateError": true,"f":"images"}
            },
            validation:{
                allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
                typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: jpg, gif, png"
            }
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push(response.resized);            
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?> .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
                $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));                        
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        }); 
        $("#wr_cm").click(function(){
            $(this).hide();
            $("#write_comment").show("500");                
        }); 
        /*$('#attach_video<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>').fineUploader({
            text: {
                uploadButton: "",
                cancelButton: "",
                waitingForResponse: ""
            },
            multiple: true,
            disableCancelForFormUploads : true,
            request: {
                endpoint: "<?= $templateFolder ?>/upload.php",
                params: {"generateError": true, "f":"video"}
            },
             validation:{
                allowedExtensions : ["m4v","avi","mov","flv","3gp"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
               typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: m4v, avi, mov, flv, 3gp"
            }                
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push("video");                        
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?> .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="<?= $templateFolder ?>/images/video_file.png" height="70">').appendTo($('#img'+files.length));
                $('<div class="name">'+response.uploadName+'</div>').appendTo($('#img'+files.length));
                $('<input value="'+response.path+'" type="hidden" name="video[]" />').appendTo($('#img'+files.length));                            
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        });*/
        
        $(".img-container").on("click",".remove-link",function(){
            var file = $(this).next().attr("src");
            $(this).parents(".image-block").remove();                
            return false;
        });
        
        $("#comment_form<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>").keypress(function(e){
            e = e || window.event;    
            //for chrome & safari
            if (e.ctrlKey) {
                if(e.keyCode == 10){
                    $("#comment_form<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>").submit();
                    return false;
                }
            };
            //for firefox
            if (e.keyCode == 13 && e.ctrlKey) {
                $("#comment_form<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>").submit();
                return false;
            };
        });
        $("#comment_form<?= $arParams["ELEMENT_ID"] ?><?= $arParams["PARENT"] ?>").submit(function(){
            if (!$(this).find(".comment_textarea").val())
            {
                alert("Введите комментарий!");
                return false;
            }
            var params = $(this).serialize();
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: params,
                success: function(data) {
                    if(data)
                    {                       
                        data = eval('('+data+')');
                        if (data.ERROR=="1")
                        {
                            alert(data.MESSAGE);
                        }
                        if (data.STATUS=="0")
                        {
                            alert(data.MESSAGE);
                        }
                        if (data.STATUS=="1"){
                            alert('Спасибо! Ваш комментарий добавлен.')
                            location.reload();
                        }
                    }
                }
            });
            return false; 
        });
    });
    
</script>