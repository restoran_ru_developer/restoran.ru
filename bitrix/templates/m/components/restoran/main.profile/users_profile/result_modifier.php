<?
if ($arResult["arUser"]['PERSONAL_PHOTO'])
{
    $file = CFile::ResizeImageGet($arResult["arUser"]['PERSONAL_PHOTO'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);                        
    $arResult["arUser"]['PERSONAL_PHOTO'] = $file;
}        
else
{
    if ($arResult["arUser"]["PERSONAL_GENDER"]=="M")
        $arResult["arUser"]['PERSONAL_PHOTO'] = Array("src"=>"/tpl/images/noname/new/man_nnm_150.png","width"=>150,"height"=>150);
    elseif ($arResult["arUser"]["PERSONAL_GENDER"]=="F")
        $arResult["arUser"]['PERSONAL_PHOTO'] = Array("src"=>"/tpl/images/noname/new/woman_nnm_150.png","width"=>150,"height"=>150);
    else
        $arResult["arUser"]['PERSONAL_PHOTO'] = Array("src"=>"/tpl/images/noname/new/unisx_nnm_150.png","width"=>150,"height"=>150);
    //$arResult["arUser"]['PERSONAL_PHOTO'] = Array("src"=>"/tpl/images/user.jpg","width"=>165,"height"=>165);
}
$date = $DB->FormatDate($arResult["arUser"]["DATE_REGISTER"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
$arTmpDate = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($date, CSite::GetDateFormat()));
$arTmpDate = explode(" ", $arTmpDate);
$arResult["arUser"]["DATE_REGISTER"] = $arTmpDate[0]." ".$arTmpDate[1]." ".$arTmpDate[2];
$arGroups = CUser::GetUserGroup($arResult["ID"]);
if(in_array(RESTORATOR_GROUP, $arGroups))
{
    $arResult["IS_RESTORATOR"] = true;
    /*$res = CIBlockElement::GetList(Array("NAME"=>"ASC"),Array("IBLOCK_TYPE"=>"catalog","!PROPERTY_user_bind"=>false,"PROPERTY_user_bind_VALUE"=>$arResult["ID"]),false,false,Array("ID","NAME","DETAIL_PAGE_URL"));
    $res->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
    while ($ar = $res->GetNext())
        $arResult["RESTORAN"][] = $ar;*/
}
?>