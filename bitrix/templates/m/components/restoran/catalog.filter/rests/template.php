<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<link rel="stylesheet" href="<?=$templateFolder?>/style.css" />
<div class="filter-page">
    <?if ($arParams["NO_NAME"]!="Y"):?>
        <div class="new font18 center">Подобрать ресторан</div>
        <Br />
    <?endif;?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" data-ajax="true" action="/catalog.php" method="GET">
    <input type="hidden" name="IBLOCK_SECTION_ID" value="32<?=(int)$arParams['SECTION']?>" />
    <input type="hidden" name="page" value="1" />
    <div class="question">
        <div class="left">Сортировать</div>
        <div class="input">
            <select name="pageRestSort" data-theme="e"  data-mini="true">
                <option value="NAME" <? if($_REQUEST['pageRestSort'] == 'NAME') echo 'selected="selected"'?>>По алфавиту</option>
                <option value="distance" <? if($_REQUEST['pageRestSort'] == 'distance') echo 'selected="selected"'?>>По удаленности</option>
                <!--<option value="ration" <? if($_REQUEST['pageRestSort'] == 'PROPERTY_RATIO') echo 'selected="selected"'?>>По рейтингу</option>-->
                <option value="popular" <? if($_REQUEST['pageRestSort'] == 'popular') echo 'selected="selected"'?>>По популярности</option>
            </select>
        </div>
    </div>    
    <input type="hidden" name="by" value="asc" />
    <div class="line2"></div>
    <?
    $mskSelected = '';
    $spbSelected = '';
    if (isset($_REQUEST['CITY_ID'])){
        if ($_REQUEST['CITY_ID'] == 'msk'){
            $mskSelected = 'selected="selected"';
            $spbSelected = '';
        } else {
            $mskSelected = '';
            $spbSelected = 'selected="selected"';
        }
    } else {
        if (CITY_ID == 'msk'){
            $mskSelected = 'selected="selected"';
            $spbSelected = '';
        } else {
            $mskSelected = '';
            $spbSelected = 'selected="selected"';
        }
    }
    ?>
    <script>
        function chan(obj)
        {
            var city = obj.val();
            location.href = "<?=$APPLICATION->GetCurPage()?>?CITY_ID="+city;
        }
    </script>
    <div class="question">
        <div class="left">Город</div>
        <div class="input">
<!--            <select data-theme="d" name="CITY_ID" data-mini="true" onchange="changeFormAction($(this));">-->
            <select data-theme="d" name="CITY_ID" data-mini="true" onchange="chan($(this));">
                <option value="msk" <?=$mskSelected?>>Москва</option>
                <option value="spb" <?=$spbSelected?>>Санкт-Петербург</option>            
            </select>
        </div>
        <div class="clear"></div>
    </div>
    <?
    foreach($arResult["arrProp"] as $prop_id => $arProp)
    {
            $res = "";
            $arResult["arrInputNames"][$FILTER_NAME."_pf"]=true;
            if ($arProp["CODE"]!="wi_fi"&&$arProp["CODE"]!='subway_dostavka'&&$arProp["CODE"]!="hrs_24")
            {
                if ($arProp["PROPERTY_TYPE"]=="L" || $arProp["PROPERTY_TYPE"]=="E")
                {
                    $name = $FILTER_NAME."_pf[".$arProp["CODE"]."]";
                            $value = $arrPFV[$arProp["CODE"]];
                            $res = "";
                            if ($arProp["MULTIPLE"]=="Y")
                            {
                                    if (is_array($value) && count($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
                            else
                            {
                                    if (!is_array($value) && strlen($value) > 0)
                                            ${$FILTER_NAME}["PROPERTY"][$arProp["CODE"]] = $value;
                            }
            ?>
  
                            <div class="question">
                                <div class="left"><?=$arProp["NAME"]?></div>
                                <div class="input">
                                    <?if ($arProp["CODE"]=="subway"):?>
                                        <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" data-theme="d" multiple="multiple" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>                                    
                                            <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val["NAME"]?></option>
                                            <? endforeach; ?>
                                        </select>
                                    <?else:?>
                                        <select <? if(MOBILE ==''){ echo ' data-native-menu="false"'; }?> data-mini="true" multiple="multiple" data-theme="d" name="<?=$arParams["FILTER_NAME"].'_pf['.$arProp["CODE"].'][]'?>">
                                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>                                    
                                                <option <?=(in_array($key, $_REQUEST[$arParams["FILTER_NAME"].'_pf'][$arProp["CODE"]]))?"selected":""?> value="<?= htmlspecialchars($key) ?>"><?=$val?></option>
                                            <? endforeach; ?>
                                        </select>
                                    <?endif;?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            
            <?             
                    
                }
            }
    }
        ?>        
        <div style="width:70%; margin:0 auto; margin-top:20px">
            <input data-theme="b" data-mini="" class="light_button" type="submit" name="set_filter" value="Найти" />    
        </div>
        <input type="hidden" name="set_filter" value="Y" />          
</form>
</div>
