<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <? 
    //v_dump($arResult["ITEMS"][0]['CODE']);
    foreach($arResult["ITEMS"] as $cell=>$arItem):
        $address = explode(', ', $arItem["PROPERTIES"]['address']['VALUE'][0]); unset($address[0]); $address = implode(', ', $address);        
        if (is_array($arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']))
            $subway = implode(', ', $arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE']);
        else
            $subway = $arItem["DISPLAY_PROPERTIES"]['subway']['DISPLAY_VALUE'];
        ?>
            <div class="restaurant-block">
                <a data-ajax="true" class="block-link" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"></a>
                <div class="image-container">
                    <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" title="<?=$arItem["NAME"]?>" /></a>
                </div>
                <div class="title-container h80">
                    <a data-ajax="true" href="/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>"><?=$arItem["NAME"]?></a>
                    <div class="rating-container">
                        <?for($i = 1; $i <= round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i++):?>
                            <div class="star-good-small"></div>
                        <?endfor?>
                        <?for($i = round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++):?>
                            <div class="star-bad-small"></div>
                        <?endfor?>
                            <div class="clear"></div>                        
                    </div>
                    
                    <div class="address">
                        <?=$address?>
                    </div>
                    <div class="address">
                        м. <?=$subway?>
                    </div>
                    <div class="advert">                        
                        <div class="new">Реклама</div>
                    </div>
                    <!--<div class="address">
                        Кухня: <?=implode(", ", $arItem["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"])?>
                    </div>-->
                </div>
            </div>
            <div class="clear"></div>
            <div class="new-line"></div>
            <div class="clear"></div>
    <?endforeach;?>