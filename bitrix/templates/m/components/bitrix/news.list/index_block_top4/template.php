<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["ITEMS"][0]["ELEMENT"]["CODE"]):?>
<div class="rest-container1 ad-rest-block" style="margin-bottom:10px;">
    <div class="swipe-block">
        <div class="ad-text">РЕКЛАМА</div>
    </div>
    <div class="slides_container">
        <div class="one-slide" href="/detail.php?RESTOURANT=<?=$arResult["ITEMS"][0]["ELEMENT"]["CODE"]?>">
            <a href="/detail.php?RESTOURANT=<?=$arResult["ITEMS"][0]["ELEMENT"]["CODE"]?>" data-ajax="true" class="block-link ui-link"></a>
            <div class="image-container">
                <a href="/detail.php?RESTOURANT=<?=$arResult["ITEMS"][0]["ELEMENT"]["CODE"]?>" data-ajax="true" class="ui-link"><img title="<?=$arResult["ITEMS"][0]["ELEMENT"]["NAME"]?>" src="<?=$arResult["ITEMS"][0]["ELEMENT"]["PICTURE"]["src"]?>"></a>
            </div>
            <div class="title-container">
                <a href="/detail.php?RESTOURANT=<?=$arResult["ITEMS"][0]["ELEMENT"]["CODE"]?>" data-ajax="true" class="ui-link"><?=$arResult["ITEMS"][0]["ELEMENT"]["NAME"]?></a>
                <div class="address">
                    <?=$arResult["ITEMS"][0]["ELEMENT"]["ADDRESS"]?>
                </div>
            </div>
        </div>
    </div>
</div>
<?endif;?>