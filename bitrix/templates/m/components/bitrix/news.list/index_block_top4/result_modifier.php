<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach ($arResult["ITEMS"] as $key=>&$item)
{
    foreach ($item["PROPERTIES"]["ELEMENTS"]["VALUE"] as $value)
    {
        //v_dump($value);
        if ($value)
        {
            CModule::IncludeModule("iblock");
            $res = CIBlockElement::GetByID($value);
            if ($ar = $res->Fetch())
            {                
                $res = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], "sort", "asc", array("CODE" => "address"));
                if ($ob = $res->GetNext())
                {
                    $adr = $ob['VALUE'];
                }                
                if ($ar["PREVIEW_PICTURE"])
                    $pic = CFile::ResizeImageGet($ar['PREVIEW_PICTURE'], array('width'=>100, 'height'=>73), BX_RESIZE_IMAGE_EXACT, true, Array());
                elseif ($ar["DETAIL_PICTURE"])
                    $pic = CFile::ResizeImageGet($ar['DETAIL_PICTURE'], array('width'=>100, 'height'=>73), BX_RESIZE_IMAGE_EXACT, true, Array());
                else
                    $pic["src"] = "/tpl/images/noname/rest_nnm.png";
                    $item["ELEMENT"] = Array("ID"=>$ar["ID"],"NAME"=>$ar["NAME"],"ADDRESS"=>$adr, "PICTURE"=>$pic, "CODE"=>$ar["CODE"]);
            }
        }
    }
    
}?>