$(document).ready(function(){
    $("#no_auth_form_auth").click(function(){
        var _this = $(this);
        $.ajax({
            type: "POST",
            url: _this.attr("href"),
            data: "",
            success: function(data) {
                if (!$("#auth_modal").size())
                {
                    $("<div class='popup popup_modal' id='auth_modal' style='font-size:12px'></div>").appendTo("body");                                                               
                }
                $('#auth_modal').html(data);
                showOverflow();
                setCenter($("#auth_modal"));
                $("#auth_modal").fadeIn("300"); 
            }
        });
        return false;
    });
});