<?
CModule::IncludeModule("iblock");
$arReviewsIB = getArIblock("reviews", CITY_ID);
global $search;
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $search =1;
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            
                $el["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($el["ACTIVE_FROM"], CSite::GetDateFormat()));
                $arTmpDate = explode(" ", $el["DISPLAY_ACTIVE_FROM"]);
                $el["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
                $el["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
                $el["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
            foreach ($el["PROPERTIES"] as $key=>$prop)
                $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            //v_dump($el["DISPLAY_PROPERTIES"]);
            $arItem["ELEMENT"] = $el;
            if ($el["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true);
            
            $rsUser = CUser::GetByID($el["CREATED_BY"]);
            $arUser = $rsUser->Fetch();
            $arItem["ELEMENT"]["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT, true);    
            if (!$arItem["ELEMENT"]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="M")
                $arItem["ELEMENT"]["AVATAR"]["src"] = "/tpl/images/noname/man_nnm.png";
            elseif (!$arItem["ELEMENT"]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="F")
                $arItem["ELEMENT"]["AVATAR"]["src"] = "/tpl/images/noname/woman_nnm.png";
            elseif (!$arItem["ELEMENT"]["AVATAR"]["src"])
                $arItem["ELEMENT"]["AVATAR"]['src'] = "/tpl/images/noname/unisx_nnm.png";
            
            // get rest name
            $rsSec = CIBlockElement::GetByID($el["PROPERTIES"]["ELEMENT"]["VALUE"]);
            if($arSec = $rsSec->GetNext()) {
                $pic = CFile::ResizeImageGet($arSec['PREVIEW_PICTURE'], array('width'=>70, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);   
                if (!$pic["src"])
                    $pic = CFile::ResizeImageGet($arSec['DETAIL_PICTURE'], array('width'=>60, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);   
                $arItem["ELEMENT"]["RESTORAN"] = Array("ID"=>$arSec["ID"],"ACTIVE"=>$arSec["ACTIVE"],"NAME"=>$arSec["NAME"],"IBLOCK_NAME"=>$arSec["IBLOCK_NAME"],"SRC"=>$pic["src"],"DETAIL_PAGE_URL"=>$arSec["DETAIL_PAGE_URL"],"IBLOCK_TYPE_ID"=>$arSec["IBLOCK_TYPE_ID"]);       
            }

            $arItem["ELEMENT"]["PREVIEW_TEXT"] = stripslashes($el["PREVIEW_TEXT"]);
            $arItem["ELEMENT"]["PREVIEW_TEXT2"] = TruncateText($el["PREVIEW_TEXT"], 400);
            foreach ($el["PROPERTIES"]["photos"]["VALUE"] as &$photo)
            {
                $id = $photo;
                $photo = array();
                $photo["ID"] = $id;
                $photo["SRC"] = CFile::ResizeImageGet($id,array("height"=>124,"width"=>137),BX_RESIZE_IMAGE_EXACT,true);
                $photo["SRC"] = $photo["SRC"]["src"];
                $photo["ORIGINAL_SRC"] = CFile::GetPath($id);
                $arItem["ELEMENT"]["PHOTOS"][] = $photo; 
            }
         }                        
    endforeach;?>
<?endif;?>
