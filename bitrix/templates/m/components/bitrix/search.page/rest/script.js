
function getNextP(thisPageNum){
    q = $('#q').val();
    maxPage = $('#maxpage').val();
    
    $.ajax({
                type: "POST",
                url: "/ajax-search.php",
                data: { PAGEN_1:thisPageNum, q:q },
                success: function(data) {
                    $('#processing').val(0);
                    $('.ajax-container').hide();
                    $(".rest-ajax").append(data);
                    if (thisPageNum == maxPage){
                        $('.to-top-link-container').fadeIn();
                    }
                }
            });
}

$(document).ready(function(){
    //alert(2);
    var scrH = $(window).height();
    var scrHP = $(".search-page").height();
    var thisPageNum = 1;
    var thisWork = 1;
    var maxPage = $('#maxpage').val();
    
    //alert(restoran);
    
    $(window).scroll(function(){
        var scro = $(this).scrollTop();
        var scrHP = $(".search-page").height();
        var scrH2 = 0;
        scrH2 = scrH + scro;
        var leftH = scrHP - scrH2;
        //console.log(leftH);
        if(leftH < -50){
            //alert(2);
            if (thisPageNum < maxPage) {
                if($('#processing').val() == 0){
                    $('#processing').val(1);
                    $('.ajax-container').show();
                    thisPageNum = thisPageNum+1;
                    getNextP(thisPageNum);
                }
            }
        }
    });
});
