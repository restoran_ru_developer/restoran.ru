<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page">
<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>
<?if(count($arResult["SEARCH"])>0):?>
        <h1><?=$arParams["SECTION_NAME"]?></h1>
	<?foreach($arResult["SEARCH"] as $key=>$arItem):?>
		<!--<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
		<p><?echo $arItem["BODY_FORMATED"]?></p>
		<small><?=GetMessage("SEARCH_MODIFIED")?> <?=$arItem["DATE_CHANGE"]?></small><br /><?
		if($arItem["CHAIN_PATH"]):?>
			<small><?=GetMessage("SEARCH_PATH")?>&nbsp;<?=$arItem["CHAIN_PATH"]?></small><?
		endif;
		?><hr />-->
                <div class="new_restoraunt left<?if($key%3==2):?> end<?endif?>">
                    <?if ($arItem["ELEMENT"]["PREVIEW_PICTURE"]):?>
                    <div class="image">
                        <a href="<?=$arItem["URL"]?>"><img src="<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]?>" width="232" height="153" /></a><br />
                    </div>
                    <?endif;?>                    
                    <div class="title">
                        <a href="<?=$arItem["URL"]?>"><?=$arItem["ELEMENT"]["NAME"]?></a>
                    </div>
                    <?//v_dump($arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["RESTORAUNT"])?>
                    <p><?=$arItem["ELEMENT"]["PREVIEW_TEXT"]?></p>
                    <p>
                        <span class="datedate"><?=$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_DAY"]."</span> ".$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_MONTH"]." ".$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_YEAR"]?>
                    </p>
                    <div class="clear"></div>
                </div>
                <?if($key%3==2):?><div class="clear"></div><?endif?>
	<?endforeach;?>
        <div class="clear"></div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&& count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]):?>
            <div align="right"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>
        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div align="right"><a href="<?=$APPLICATION->GetCurPageParam("search_in=".$arParams["SEARCH_IN"], array("search_in","x","y")); ?>" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?> <?=$arParams["SECTION_NAME"]?></a></div>
        <?endif;?>
                    <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                 
</div>
