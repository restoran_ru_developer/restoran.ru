<?
CModule::IncludeModule("iblock");
$arReviewsIB = getArIblock("reviews", CITY_ID);
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            $el = $obElement->GetFields();
            $el["PROPERTIES"] = $obElement->GetProperties();
            
                $el["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($el["ACTIVE_FROM"], CSite::GetDateFormat()));
                $arTmpDate = explode(" ", $el["DISPLAY_ACTIVE_FROM"]);
                $el["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
                $el["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
                $el["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
            foreach ($el["PROPERTIES"] as $key=>$prop)
                $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            //v_dump($el["DISPLAY_PROPERTIES"]);
            $arItem["ELEMENT"] = $el;
            if ($el["PREVIEW_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
         }
    endforeach;?>
<?endif;?>
