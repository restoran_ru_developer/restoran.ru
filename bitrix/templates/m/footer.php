<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<input type="hidden" id="processing" value="0">
</div>
<? if ($APPLICATION->GetCurPage() != "/map.php"): ?> 
    <div data-role="footer" id="footer" data-position="fixed" >                            
        <div class="footer-left">© 2013 Ресторан.Ru</div>
        <div class="footer-right"><a href="http://restoran.ru/<?=CITY_ID?>/?from_mobile=Y" data-ajax="false">Полная версия</a></div>         
    </div>
<? endif; ?>
<div data-role="panel" id="defaultpanel" data-theme="a" data-position-fixed="true">        
    <?if ($APPLICATION->GetCurPage() != "/" && $APPLICATION->GetCurPage() != "/index.php"): ?> 
    <div id="mysearch">
<!--        <form name="rest_filter_form" method="get" data-ajax="true" action="/search.php">        
            <input type="hidden" value="rest" name="search_in" id="by_name">                                                
            <input type="text" class="search_input" placeholder="Поиск на Restoran.ru" data-theme="b" value="" name="q">                                                                                            
            <div class="search_button-container" onclick="$(this).parents('form').submit()">
                <div class="search_button"></div>
            </div>            
            <div class="clear"></div>
        </form>-->
        <ul id="autocomplete3" data-corners="false" data-shadow="false" data-iconshadow="false" data-role="listview" data-inset="true" data-filter="true" data-filter-placeholder="Поиск ресторана..." data-filter-theme="b"></ul>
    </div>        
    <? endif; ?>
    <!--<ul data-role="listview" class="nav-search" data-inset="true"  data-theme="e">            
        <li><a data-ajax="true" href="/mobile/map.php" data-transition="fade" >Ближайшие на карте</a></li>        
    </ul>-->
    <? if ($APPLICATION->GetCurPage() != "/" && $APPLICATION->GetCurPage() != "/index.php"): ?> 
        <!--<div id="onmap">
            <div id="map2"></div>
            <div id="map_link2" onclick="location.href='map.php'">
                <div>
                    <input type="button" value="Ближайшие рестораны"  data-theme="e" onclick="location.href='map.php'"/>    
                </div>
            </div>
        </div>-->      
    <? endif; ?>
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d" data-icon="false">
        <li <?
    if ($APPLICATION->GetCurPage() == "/catalog.php") {
        echo 'class="ui-btn-active"';
    }
    ?>><a data-ajax="true" data-textonly="true" data-textvisible="true" data-msgtext="Text only loader" href="/catalog.php" data-theme="b" data-transition="fade" >Рестораны</a></li>
            <? if ($_SESSION['lat']): ?>
            <li><a data-ajax="true" data-textonly="true" data-textvisible="true" data-msgtext="Text only loader" href="/catalog.php?page=1&pageRestSort=distance&by=asc&CITY_ID=<?=CITY_ID?>&set_filter=Y" data-theme="b" data-transition="fade" >Ближайшие рестораны</a></li>
            <? endif; ?>
        <li <?
            if ($APPLICATION->GetCurPage() == "/map.php") {
                echo 'class="ui-btn-active"';
            }
            ?>><a data-ajax="false" data-textonly="true" data-textvisible="true" href="/map.php" data-theme="b" data-transition="fade" >Найти на карте</a></li>
        <li><a data-ajax="true" data-textonly="true" data-textvisible="true" href="/filter.php" data-theme="b" data-transition="fade" >Найти по параметрам</a></li>
        <!--<li <?
            if ($APPLICATION->GetCurPage() == "/banket.php") {
                echo 'class="ui-btn-active"';
            }
            ?>><a href="/mobile/banket.php" data-transition="fade" >Банкетные залы</a></li>-->
        <!--<li <?
        /* if ($APPLICATION->GetCurPage() == "/mobile/opinions.php") {
          echo 'class="ui-btn-active"';
          } */
            ?>><a data-ajax="false" href="/mobile/opinions.php" data-transition="fade" >Отзывы</a></li>   -->         
        <li data-theme="e" <?
        if ($APPLICATION->GetCurPage() == "/online_order.php") {
            echo 'class="ui-btn-active"';
        }
            ?>><a data-ajax="false" href="/online_order.php" data-transition="fade">Забронировать столик</a></li>            
    </ul>       
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d">
        <li <?
            if ($APPLICATION->GetCurPage() == "/auth/") {
                echo 'class="ui-btn-active"';
            }
            ?>><a data-ajax="false" href="/auth/" data-transition="fade"><?
            if ($USER->IsAuthorized()) {
                echo 'Личный кабинет';
            } else {
                echo 'Авторизоваться';
            }
            ?></a></li>
    </ul>
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d">
        <li><a data-ajax="false" href="http://restoran.ru/<?=CITY_ID?>/?from_mobile=Y" data-transition="fade">Перейти на сайт</a></li>
    </ul>
</div>  
<? if ($APPLICATION->GetCurPage() == "/catalog.php"): ?> 
    <div data-role="panel" data-position="right" data-position-fixed="true" data-swipe-close="false" data-dismissible="false" data-theme="a" id="filterform" style="color:white;">            
        <?
        $arIB = getArIblock('catalog', CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.filter", "rests", Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arIB["ID"],
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array("subway", "out_city", "average_bill", "kitchen", "features"),
            "PRICE_CODE" => array(),
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "CACHE_NOTES" => "mobile",
            "LIST_HEIGHT" => "5",
            "TEXT_WIDTH" => "20",
            "NUMBER_WIDTH" => "5",
            "SAVE_IN_SESSION" => "N"
                )
        );
        ?>
    </div>
<? endif; ?>
<? if ($APPLICATION->GetCurPage() == "/opinions.php"): ?> 
    <div data-role="panel" data-position="right" data-position-fixed="true" data-swipe-close="false" data-dismissible="false" data-theme="a" id="filterform" style="color:white;">            
        <?
        $arIB = getArIblock('reviews', CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.filter", "opinions", Array(
            "IBLOCK_TYPE" => "reviews",
            "IBLOCK_ID" => $arIB["ID"],
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array(""),
            "PRICE_CODE" => array(),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "CACHE_NOTES" => "mobile",
            "LIST_HEIGHT" => "5",
            "TEXT_WIDTH" => "20",
            "NUMBER_WIDTH" => "5",
            "SAVE_IN_SESSION" => "N"
                )
        );
        ?>
    </div>
<? endif; ?>

</div>
<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter17073367 = new Ya.Metrika({id:17073367,
    enableAll: true});
    } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") +
    "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: '432665', type: 'pageView', start: (new Date()).getTime()});
(function (d, w) {
   var ts = d.createElement('script'); ts.type = 'text/javascript'; ts.async = true;
   ts.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//top-fwz1.mail.ru/js/code.js';
   var f = function () {var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ts, s);};
   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window);
//]]></script><noscript><div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=432665;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->
</body>
</html>