<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="HandheldFriendly" content="True" />
        <? $APPLICATION->ShowHead(); ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadScript('http://code.jquery.com/jquery-1.9.1.min.js'); ?>                
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
        <link rel="stylesheet" href="/bitrix/templates/m/themes/restoran.css" />
        <link rel="stylesheet" href="/bitrix/templates/m/ad_slider.css" />
        <script src="/bitrix/templates/m/js/jquery.ez-bg-resize.js"></script>   
        <script src="/bitrix/templates/m/js/ad_slider.js"></script>
        <script src="/bitrix/templates/m/script.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>

        <script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>
        <style>.panel-content { padding:15px; }	.ui-panel-dismiss {
                display: none;
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 1002;
            }</style>        
    </head>
    <body style="overflow: auto;">        
        <div  data-role="page" data-theme="a" id="page" class="ui-responsive-panel">
            <div data-role="header" id="header" data-position="fixed" data-tap-toggle="false">                            
                <? if ($APPLICATION->GetCurPage() == "/" || $APPLICATION->GetCurPage() == "/index.php"): ?>
                    <!--<div class="left-full"><a href="/" data-ajax="true">Полная версия</a></div>-->
                    <div class="header-menu-button"><a class="menu" href="#defaultpanel"></a></div>
                <? else: ?>  
                    <div class="header-menu-button"><a class="menu" href="#defaultpanel"></a></div>

                    <!--<a href="#defaultpanel" data-icon="bars" data-theme="b" style="margin-top:5px;">Меню</a>-->
                <? endif; ?>

                <div class="logo_box">
                    <div class="left-logo" onclick="location.href='/'"></div>
                </div>            
                <? if ($APPLICATION->GetCurPage() == "/" || $APPLICATION->GetCurPage() == "/index.php"): ?> 
                    <div class="search_ico-container">
                        <div class="search_ico se"></div>
                    </div>
                    <div class="search_cancel se">
                        <input type="button" data-mini="true" data-theme="b" value="Отмена" />
                    </div>
                    <div class="clear"></div>    
                    <div class="search_box">                                
<!--                        <form name="rest_filter_form" method="get" data-transition="fade" action="/search.php">        
                            <input type="hidden" value="rest" name="search_in" id="by_name">                                                
                            <input type="text" class="search_input" placeholder="Поиск на Restoran.ru" data-theme="b" value="" name="q">  
                            <div class="search_button-container" onclick="$(this).parents('form').submit()">
                                <div class="search_button2"></div>
                            </div>                                                        
                            <div class="clear"></div>
                        </form>-->
<script>
$( document ).on( "pageinit", "#page", function() {
    $( "#autocomplete2" ).on( "listviewbeforefilter", function ( e, data ) {
        var $ul = $( this ),
            $input = $( data.input ),
            value = $input.val(),
            html = "";
        $ul.html( "" );
        if ( value && value.length > 2 ) {
            $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
            $ul.listview( "refresh" );
            $.ajax({
                url: "/suggest.php",
                dataType: "json",                
                data: {
                    q: $input.val()
                }
            })
            .then( function ( response ) {
                $.each( response, function ( i, val ) {
                    html += "<li onclick='location.href=\"/detail.php?RESTOURANT="+val.CODE+"\"'>" + val.NAME + "</li>";
                });
                $ul.html( html );
                $ul.listview( "refresh" );
                $ul.trigger( "updatelayout");
            });
        }
    });
});
</script>
                        <ul id="autocomplete2" style="margin-top:10px;" data-corners="false" data-shadow="false" data-iconshadow="false" data-role="listview" data-inset="true" data-filter="true" data-filter-placeholder="Поиск ресторана..." data-filter-theme="b"></ul>
                    </div>
                    <? if(CITY_ID == 'spb'): ?>
                    <div class="rest-phone-box"><a href="tel:+78127401820" data-ajax="false" style="text-decoration: none; color: #3C414E"><span style="font-size:12px;">812</span> 740 18 20<span class="phone-icon"></span></a></div>
                    <? endif; ?>
                    <? if(CITY_ID == 'msk'): ?>
                    <div class="rest-phone-box"><a href="tel:+74959882656" data-ajax="false" style="text-decoration: none; color: #3C414E"><span style="font-size:12px;">495</span> 988 26 56<span class="phone-icon"></span></a></div>
                    <? endif; ?>
                <? else: ?>
                    <? if ($APPLICATION->GetCurPage() != "/filter.php" && $APPLICATION->GetCurPage() != "/auth/"&& $APPLICATION->GetCurPage() != "/online_order.php" && $APPLICATION->GetCurPage() != "/map.php" && $APPLICATION->GetCurPage() != "/search.php" && !$_REQUEST["RESTOURANT"]): ?>
                        <div class="header-menu-button"><a class="search_options" href="#filterform"></a></div>                                    
                    <? endif; ?>
                <? endif; ?>
            </div>
            <div data-role="content" id="content" data-theme="a" <?= ($APPLICATION->GetCurPage() == "/map.php") ? "style=padding:0px!important;margin:0px!important" : "" ?>>                                                                                    


