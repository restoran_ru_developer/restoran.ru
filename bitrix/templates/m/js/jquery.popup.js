jQuery.fn.popup = function(options){
    // настройки по умолчанию
    var options = jQuery.extend({    
        url:"",
        bg_color:"#2d323b",
        opacity:".97",
        obj:"",
        css:{"top":"-20px","right":"-10px"},
        center:false,
        speed:500,
        right:false
    },options);
    this.makeCenter = function() {
            var a = ( (($(window).height() / 2) - ( (jQuery(this).height()) / 2 ) ));
            if (a<0)
                a = 0;
            jQuery(this).css ({
                top: a + 'px',
                right: ( (($(window).width() / 2) - ( (jQuery(this).width()) / 2 ) )) + 'px',
                position:"fixed"
            });
    };
     
    this.init = function() {
        var _this = this;              
        //Дополнительно для IE в стилях необходимо указать behavior c css3pie.htc
        _this.css({"background-color":options.bg_color,"opacity":options.opacity,"display":"none","position":"absolute","z-index":"10000"});
        _this.css(options.css);           
        _this.parent().css("position","relative");        
        $("#"+options.obj).on('click','a',function(e){
            $(".popup").fadeOut(options.speed);
            e.stopPropagation();            
            if (options.url)
            {                
                if (!$(_this).html())
                {
                    if (options.url==true) options.url = $(this).attr("href");
                    _this.load(options.url,function(){
                        if (options.center)
                            _this.makeCenter();
                        _this.fadeIn(options.speed);
                    });
                }
                else
                {
                    if (options.center)
                        _this.makeCenter();
                    _this.fadeIn(options.speed);
                }                
                return false;
            }
            else
            {
                if (options.center)
                    _this.makeCenter();                
                _this.fadeIn(options.speed);
            }
        });
        $(_this).on('click','.popup_close',function(e){
            _this.fadeOut(options.speed);
        });
        _this.click(function(e){
            e.stopPropagation();
            //return false;
        });
        $('html,body').click(function() {
            _this.fadeOut(options.speed);
        });
        $(window).keyup(function(event) {
          if (event.keyCode == '27') {
            _this.fadeOut(options.speed);
           }
        });
        if (options.center)
        {
            $(window).resize(function() {            
                _this.makeCenter();
            });
        }
        return _this;
    };
    return this.init();
}