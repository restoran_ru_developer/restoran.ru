jQuery(document).ready(function() {
    // ajax loader
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/144.gif" align="left"> Загрузка...</div>');
    /*$("#system_loading").css("left", $(window).width() / 2 - 104 / 2 + "px");
    $("#system_loading").css("top", $(window).height() / 2 - 104 / 2 + "px");*/
    $("#system_loading").bind("ajaxSend",
        function() {
            $(this).show();
        }).bind("ajaxComplete", function() {
            $(this).hide();
        });

    var params = {
        changedEl:"#search_in",
        visRows:5
        /*scrollArrows: true*/
    }
    cuSel(params);
    $("ul.tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
            $(this).tabs("div.panes > .pane", {                
                effect: 'fade',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        else
            $(this).tabs("div.panes > .pane",{effect: 'fade'});
    });    
    //$("ul#poster_tabs").tabs("div.poster_panes > .poster_pane");
    $("ul#poster_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.poster_panes > .poster_pane", {                
                effect: 'fade',  
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.poster_panes > .poster_pane",{effect: 'fade'});
    }); 
    $("ul.history_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.panes > .pane", {                
                effect: 'fade',  
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.panes > .pane",{effect: 'fade'});
    }); 
    $(".to_top").on('click', function() {
        $('html,body').animate({scrollTop:"0"}, 500);
    });    
    $("a.anchor").click(function(){
        var pos = $($(this).attr("href")).offset().top-10;
        $('html,body').animate({scrollTop:pos}, "300");
    });
    $("input[type^='radio']").each(
        function() {
             changeRadioStart($(this));
    });
});

function showExpose() {
    $(document).mask({ color: '#1a1a1a', loadSpeed: 200, closeSpeed:0, opacity: 0.5, closeOnEsc: false, closeOnClick: false });
}
function closeExpose() {
    $.mask.close();
}

function send_ajax(url, dataType, params, successFunc) {
    //showExpose();
    if(!dataType)
        dataType = 'json';
    params = eval('(' + params + ')');
    $.ajax({
        url:url,
        type: 'post',
        dataType: dataType,
        data: params,
        statusCode: {
            404:function() {
                alert('Page not found');
            }
        },
        success:function(data) {
            if (successFunc)
                successFunc(data);
            //closeExpose();
        }
    });
}

function sumbit_form(form, beforeFunc,successFunc) {
    var params = $(form).serialize();
    var url = $(form).attr("action");
    console.log(url);
    $.ajax({
        url:url,
        type:'post',
        data:params,
        statusCode:{
            404:function() {
                alert('Page not found');
            }
        },
        beforeSend:function() {
            /*if (typeof(before_submit_form) == "function")
                before_submit_form(form);
            return false;*/
            if (beforeFunc)
                beforeFunc(form);
        },
        success:function(data) {
            $(form).parent().html(data);
            /*if (typeof(success_submit_form) == "function")
                success_submit_form();*/
            if (successFunc)
                successFunc(form,data);
        }
    });
    return false;
}

function show_popup(obj,options)
{
    if(!options)
        options = {"obj":$(obj).parent().attr("id")};
    else
        options.obj = $(obj).parent().attr("id");    
    if (!$(obj).parent().find("div").hasClass("popup"))
        $("<div>").addClass("popup").appendTo($(obj).parent()).popup(options);
}