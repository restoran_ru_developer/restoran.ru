<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<input type="hidden" id="processing" value="0">
</div>
<? if ($APPLICATION->GetCurPage() != "/mobile/map.php"): ?> 
    <div data-role="footer" id="footer" data-position="fixed" >                            
        <div class="footer-left">© 2013 Ресторан.Ru</div>
        <div class="footer-right"><a href="http://restoran.ru/" data-ajax="false">Полная версия</a></div>         
    </div>
<? endif; ?>
<div data-role="panel" id="defaultpanel" data-theme="a" data-position-fixed="true">        
    <? //if ($APPLICATION->GetCurPage() != "/mobile/" && $APPLICATION->GetCurPage() != "/mobile/index.php"): ?> 
    <div id="mysearch">
        <form name="rest_filter_form" method="get" data-ajax="true" action="/mobile/search.php">        
            <input type="hidden" value="rest" name="search_in" id="by_name">                                                
            <input type="text" class="search_input" placeholder="Поиск на Restoran.ru" data-theme="b" value="" name="q">                                                                                            
            <div class="search_button-container" onclick="$(this).parents('form').submit()">
                <div class="search_button"></div>
            </div>
            <!--<a href="javascript:void(0)" data-role="button" data-iconshadow="true" data-transition="fade" data-icon="search" data-theme="a" data-iconpos="notext" onclick="$(this).parents('form').submit()" ></a>-->

            <div class="clear"></div>
        </form>
    </div>        
    <? //endif; ?>
    <!--<ul data-role="listview" class="nav-search" data-inset="true"  data-theme="e">            
        <li><a data-ajax="true" href="/mobile/map.php" data-transition="fade" >Ближайшие на карте</a></li>        
    </ul>-->
    <? if ($APPLICATION->GetCurPage() != "/mobile/" && $APPLICATION->GetCurPage() != "/mobile/index.php"): ?> 
        <!--<div id="onmap">
            <div id="map2"></div>
            <div id="map_link2" onclick="location.href='map.php'">
                <div>
                    <input type="button" value="Ближайшие рестораны"  data-theme="e" onclick="location.href='map.php'"/>    
                </div>
            </div>
        </div>-->      
    <? endif; ?>
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d" data-icon="false">
        <li <?
    if ($APPLICATION->GetCurPage() == "/mobile/catalog.php") {
        echo 'class="ui-btn-active"';
    }
    ?>><a data-ajax="true" data-textonly="true" data-textvisible="true" data-msgtext="Text only loader" href="/mobile/catalog.php" data-theme="b" data-transition="fade" >Рестораны</a></li>
            <? if ($_SESSION['lat']): ?>
            <li><a data-ajax="true" data-textonly="true" data-textvisible="true" data-msgtext="Text only loader" href="/mobile/catalog.php?page=1&pageRestSort=distance&by=asc&CITY_ID=spb&set_filter=Y" data-theme="b" data-transition="fade" >Ближайшие рестораны</a></li>
            <? endif; ?>
        <li <?
            if ($APPLICATION->GetCurPage() == "/mobile/map.php") {
                echo 'class="ui-btn-active"';
            }
            ?>><a data-ajax="false" data-textonly="true" data-textvisible="true" href="/mobile/map.php" data-theme="b" data-transition="fade" >Найти на карте</a></li>
        <li><a data-ajax="true" data-textonly="true" data-textvisible="true" href="/mobile/filter.php" data-theme="b" data-transition="fade" >Найти по параметрам</a></li>
        <!--<li <?
            if ($APPLICATION->GetCurPage() == "/mobile/banket.php") {
                echo 'class="ui-btn-active"';
            }
            ?>><a href="/mobile/banket.php" data-transition="fade" >Банкетные залы</a></li>-->
        <!--<li <?
        /* if ($APPLICATION->GetCurPage() == "/mobile/opinions.php") {
          echo 'class="ui-btn-active"';
          } */
            ?>><a data-ajax="false" href="/mobile/opinions.php" data-transition="fade" >Отзывы</a></li>   -->         
        <li data-theme="e" <?
        if ($APPLICATION->GetCurPage() == "/mobile/online_order.php") {
            echo 'class="ui-btn-active"';
        }
            ?>><a data-ajax="false" href="/mobile/online_order.php" data-transition="fade">Забронировать столик</a></li>            
    </ul>       
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d">
        <li <?
            if ($APPLICATION->GetCurPage() == "/mobile/auth/") {
                echo 'class="ui-btn-active"';
            }
            ?>><a data-ajax="false" href="/mobile/auth/" data-transition="fade"><?
            if ($USER->IsAuthorized()) {
                echo 'Личный кабинет';
            } else {
                echo 'Авторизоваться';
            }
            
            ?></a></li>
    </ul>
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d">
        <li><a data-ajax="false" href="http://restoran.ru/" data-transition="fade">Перейти на сайт</a></li>
    </ul>
</div>  
<? if ($APPLICATION->GetCurPage() == "/mobile/catalog.php"): ?> 
    <div data-role="panel" data-position="right" data-position-fixed="true" data-swipe-close="false" data-dismissible="false" data-theme="a" id="filterform" style="color:white;">            
        <?
        $arIB = getArIblock('catalog', CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.filter", "rests", Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arIB["ID"],
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array("subway", "out_city", "average_bill", "kitchen", "features"),
            "PRICE_CODE" => array(),
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "N",
            "LIST_HEIGHT" => "5",
            "TEXT_WIDTH" => "20",
            "NUMBER_WIDTH" => "5",
            "SAVE_IN_SESSION" => "N"
                )
        );
        ?>
    </div>
<? endif; ?>
<? if ($APPLICATION->GetCurPage() == "/mobile/opinions.php"): ?> 
    <div data-role="panel" data-position="right" data-position-fixed="true" data-swipe-close="false" data-dismissible="false" data-theme="a" id="filterform" style="color:white;">            
        <?
        $arIB = getArIblock('reviews', CITY_ID);
        $APPLICATION->IncludeComponent(
                "restoran:catalog.filter", "opinions", Array(
            "IBLOCK_TYPE" => "reviews",
            "IBLOCK_ID" => $arIB["ID"],
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array(""),
            "PRICE_CODE" => array(),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "LIST_HEIGHT" => "5",
            "TEXT_WIDTH" => "20",
            "NUMBER_WIDTH" => "5",
            "SAVE_IN_SESSION" => "N"
                )
        );
        ?>
    </div>
<? endif; ?>

</div>
</body>
</html>