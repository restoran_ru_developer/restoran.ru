<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="HandheldFriendly" content="True" />
        <? $APPLICATION->ShowHead(); ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadScript('http://code.jquery.com/jquery-1.9.1.min.js'); ?>                
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
        <link rel="stylesheet" href="/bitrix/templates/mobile/themes/restoran.css" />
        <link rel="stylesheet" href="/bitrix/templates/mobile/ad_slider.css" />
        <script src="/bitrix/templates/mobile/js/jquery.ez-bg-resize.js"></script>   
        <script src="/bitrix/templates/mobile/js/ad_slider.js"></script>
        <script src="/bitrix/templates/mobile/script.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>

        <script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>
        <style>.panel-content { padding:15px; }	.ui-panel-dismiss {
                display: none;
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 1002;
            }</style>
    </head>
    <body style="overflow: auto;">
        <div  data-role="page" data-theme="a" id="page" class="ui-responsive-panel">
            <div data-role="header" id="header" data-position="fixed" data-tap-toggle="false">                            
                <? if ($APPLICATION->GetCurPage() == "/mobile/" || $APPLICATION->GetCurPage() == "/mobile/index.php"): ?>
                    <!--<div class="left-full"><a href="/" data-ajax="true">Полная версия</a></div>-->
                    <div class="header-menu-button"><a class="menu" href="#defaultpanel"></a></div>
                <? else: ?>  
                    <div class="header-menu-button"><a class="menu" href="#defaultpanel"></a></div>

                    <!--<a href="#defaultpanel" data-icon="bars" data-theme="b" style="margin-top:5px;">Меню</a>-->
                <? endif; ?>

                <div class="logo_box">
                    <div class="left-logo" onclick="location.href='/mobile'"></div>
                </div>            
                <? if ($APPLICATION->GetCurPage() == "/mobile/" || $APPLICATION->GetCurPage() == "/mobile/index.php"): ?> 
                    <div class="search_ico-container">
                        <div class="search_ico se"></div>
                    </div>
                    <div class="search_cancel se">
                        <input type="button" data-mini="true" data-theme="b" value="Отмена" />
                    </div>
                    <div class="clear"></div>    
                    <div class="search_box">                                
                        <form name="rest_filter_form" method="get" data-transition="fade" action="/mobile/search.php">        
                            <input type="hidden" value="rest" name="search_in" id="by_name">                                                
                            <input type="text" class="search_input" placeholder="Поиск на Restoran.ru" data-theme="b" value="" name="q">  
                            <div class="search_button-container" onclick="$(this).parents('form').submit()">
                                <div class="search_button2"></div>
                            </div>
                            
                            <!--<a href="javascript:void(0)" data-role="button" data-iconshadow="false" data-transition="fade" data-icon="search" data-theme="a" data-iconpos="notext" onclick="$(this).parents('form').submit()" ></a>-->

                            <div class="clear"></div>
                        </form>
                    </div>
                    <? if(CITY_ID == 'spb'): ?>
                    <div class="rest-phone-box"><a href="tel:+78127401820" data-ajax="false" style="text-decoration: none; color: #3C414E"><span style="font-size:12px;">812</span> 740 18 20<span class="phone-icon"></span></a></div>
                    <? endif; ?>
                    <? if(CITY_ID == 'msk'): ?>
                    <div class="rest-phone-box"><a href="tel:+74959882656" data-ajax="false" style="text-decoration: none; color: #3C414E"><span style="font-size:12px;">495</span> 988 26 56<span class="phone-icon"></span></a></div>
                    <? endif; ?>
                <? else: ?>
                    <? if ($APPLICATION->GetCurPage() != "/mobile/filter.php" && $APPLICATION->GetCurPage() != "/mobile/auth/"&& $APPLICATION->GetCurPage() != "/mobile/online_order.php" && $APPLICATION->GetCurPage() != "/mobile/map.php" && $APPLICATION->GetCurPage() != "/mobile/search.php" && !$_REQUEST["RESTOURANT"]): ?>
                        <div class="header-menu-button"><a class="search_options" href="#filterform"></a></div>                                    
                    <? endif; ?>
                <? endif; ?>
            </div>
            <div data-role="content" id="content" data-theme="a" <?= ($APPLICATION->GetCurPage() == "/mobile/map.php") ? "style=padding:0px!important;margin:0px!important" : "" ?>>                                                                                    


