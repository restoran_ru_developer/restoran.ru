function changeFormAction(select){
        if (select.val() == 'msk'){
            select.parent().parent().parent().parent().attr('action', 'http://www.restoran.ru/mobile/catalog.php');
        }
        if (select.val() == 'spb'){
            select.parent().parent().parent().parent().attr('action', 'http://spb.restoran.ru/mobile/catalog.php');
        }
    }

function getNextP(thisPageNum, type){
    if (type == 'catalog'){
        maxPage = parseInt($(".rest-container:visible").find('#maxpagecatalog').val());
        console.log(thisPageNum, maxPage);
        $.ajax({
            type: "POST",
            url: "/mobile/ajax-restaurants.php?"+$('[name=arrFilter_form]:visible').serialize(),
            data: {
                PAGEN_1:thisPageNum
            },
            success: function(data) {
                
                //console.log(maxPage);
                $('#processing').val(0);
                $('.ajax-container').hide();
                $(".priorities-ajax:visible").append(data);
                if (thisPageNum == maxPage){
                    $(".rest-container:visible").parent().find('.to-top-link-container').fadeIn();
                }
            }
        });
    }
    if (type == 'review'){
        maxPage = $('#maxpagereview').val();
        if ($('#photos').val() == 'Y'){
            photos = "Y";
        } else {
            photos = "N";
        }
        
        $.ajax({
            type: "POST",
            url: "/mobile/ajax-opinions.php",
            data: {
                PAGEN_1:thisPageNum,
                RESTOURANT:restoran,
                photos:photos
            },
            success: function(data) {
                $('#processing').val(0);
                $('.ajax-container').hide();
                $(".comments-ajax").append(data);
                if (thisPageNum == maxPage){
                    $(".review-container").find('.to-top-link-container').fadeIn();
                }
            }
        });
    }
    
}

$( document ).on( "mobileinit", function() {
    $.mobile.loader.prototype.options.textonly = true;
    $.mobile.loader.prototype.options.text = "Загрузка";
    $.mobile.loader.prototype.options.textVisible = true;
    $.mobile.loader.prototype.options.theme = "a";
    $.mobile.loader.prototype.options.html = '<div class="new-loader">Загрузка</div>';
    $.event.special.tap.tapholdThreshold = 150;
});

$(document).ready(function(){
    
    var ReviewThisPageNum = 1;
    var CatalogThisPageNum = 1;
    $(window).scroll(function(){
        if($(".rest-container:visible").height() > 0){
            thisPage = parseInt($(".rest-container:visible").find('[name=page]').val());
            maxPage = parseInt($(".rest-container:visible").find('#maxpagecatalog').val());
            scrH = $(window).height();
            scro = $(this).scrollTop();
            scrHP = $(".rest-container:visible").height();
            scrH2 = 0;
            scrH2 = scrH + scro;
            var leftH = scrHP - scrH2;
            //console.log(thisPage, maxPage);
            //console.log(thisPage);
            if(leftH < 1400){
                if (thisPage < maxPage) {
                    if($('#processing').val() == 0){
                        $('#processing').val(1);
                        $('.ajax-container').show();
                        $(".rest-container:visible").find('[name=page]').val(thisPage+1)
                        getNextP(thisPage+1, 'catalog');
                    }
                }            
            }
        }
        if($(".review-container").height() > 0){
            restoran = $('#rest').val();
            maxPage = $('#maxpagereview').val();
            scrH = $(window).height();
            scro = $(this).scrollTop();
            scrHP = $(".review-container").height();
            scrH2 = 0;
            scrH2 = scrH + scro;
            var leftH = scrHP - scrH2;
            if(leftH < 500){
                if (ReviewThisPageNum < maxPage) {
                    if($('#processing').val() == 0){
                        $('#processing').val(1);
                        $('.ajax-container').show();
                        ReviewThisPageNum = ReviewThisPageNum+1;
                        getNextP(ReviewThisPageNum, 'review');
                    }
                }            
            }
        }
        
    });
    
    $(".ajax-container").on("click",function(){
        //alert($(".rest-container:visible").find('[name=page]').val());
        //alert($(".rest-container:visible").find('#maxpagecatalog').val());
        if($(".rest-container:visible").height() > 0){
            if(parseInt($(".rest-container:visible").find('[name=page]').val()) < parseInt($(".rest-container:visible").find('#maxpagecatalog').val())){
                if($('#processing').val() == 1){
                    $(".rest-container:visible").find('[name=page]').val(thisPage+1)
                    getNextP(thisPage, 'catalog');
                }
            }
        }
        if($(".review-container").height() > 0){
            if(parseInt($('#rest').val()) < parseInt($('#maxpagereview').val())){
                if($('#processing').val() == 1){
                    getNextP(ReviewThisPageNum-1, 'review');
                }
            }
        }
    });
    
    $(".search_ico-container").on("click",function(){
        //alert(2);
        $(".search_box").toggle(0); 
        $(".se").toggle(0); 
    });
    $(".search_cancel").on("click",function(){
        $(".search_box").toggle(0); 
        $(".se").toggle(0);  
    });
    
    $(".autocomplete-question li").on("click",function(){
        $(".ui-input-search").find('input').val($(this).find('.ui-link-inherit').text());
        $("#autocomplete-restaurant-name").val($(this).find('.ui-link-inherit').text());
        $("#autocomplete-restaurant-id").val($(this).find('.ui-link-inherit').attr('rest-id'));
        $(".ui-listview li").addClass('ui-screen-hidden');
    });
    
    $(document).on("click", ".menu", function(){
        if ( $.mobile.activePage.jqmData( "panel" ) !== "open" )
            $( "#defaultpanel" ).panel("open");
        else
        {
            $( "#defaultpanel" ).panel("close");
            $( "#filterform" ).panel("close");
        }
    }); 
    
    /*$( ".search_options" ).on("click",function(){
        if ( $.mobile.activePage.jqmData( "panel" ) !== "open" )
            $( "#filterform" ).panel("open");
        else
        {
            $( "#filterform" ).panel("close");
            $( "#defaultpanel" ).panel("close");
        }
    });*/ 
    
    $( document ).on( "swipeleft swiperight", ".ui-page", function( e ) {
        if ( $.mobile.activePage.jqmData( "panel" ) !== "open" ) {

        }
        else
        {
            if ( e.type === "swipeleft"  ) {
                $( "#defaultpanel" ).panel( "close" );
            }
            if ( e.type === "swiperight"  ) {
                $( "#filter-form" ).panel( "close" );
            }
        }
    });  
});

function open_default_panel()
{
    if ( $.mobile.activePage.jqmData( "panel" ) !== "open" )
        $( "#defaultpanel" ).panel("open");
    else
        $( "#defaultpanel" ).panel("close");
}

function open_filter_panel()
{
    if ( $.mobile.activePage.jqmData( "panel" ) !== "open" )
        $( "#filterform" ).panel("open");
    else
        $( "#filterform" ).panel("close");
}
