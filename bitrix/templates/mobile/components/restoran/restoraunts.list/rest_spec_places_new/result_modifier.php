<?
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill"
);

foreach($arResult["ITEMS"] as $cell=>$arItem) {
    if($arItem["PREVIEW_PICTURE"])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 100, 'height' => 73), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["SRC"] = $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"];        
    }
    elseif($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => 100, 'height' => 73), BX_RESIZE_IMAGE_EXACT, true, Array());
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["SRC"] = $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"];        
    }
    else
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["SRC"] = "/tpl/images/noname/rest_nnm.png";
    }
    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        // remove links from subway and kitchen name
        if(in_array($pid, $arFormatProps)) {
            if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                }
            } else {
                $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
            }
        }
    }
    
    foreach ($arItem["PROPERTIES"]["photos"]["VALUE"] as $key=>$photo)
    {
        $arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"][$key] = CFile::ResizeImageGet($photo, array('width' => 100, 'height' => 73), BX_RESIZE_IMAGE_EXACT, true, Array());
    }
}
?>