<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="rest-container">
<ul class="menu_list" data-role="listview" data-inset="true" data-theme="b" data-shadow="false" data-corners ="false">
    <?foreach ($arResult["ITEMS"] as $cell => $arItem):
        $address = explode(', ', $arItem["PROPERTIES"]['address']['VALUE'][0]);
        unset($address[0]);
        $address = implode(', ', $address);
        ?>
    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" >
        <a href="/mobile/detail.php?RESTOURANT=<?= $arItem["CODE"] ?>">
        <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>">
        <div class="title-container">
            <?= $arItem["NAME"] ?>
            <div class="rating-container">
                <? for ($i = 1; $i <= round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                    <div class="star-good-small"></div>
                <? endfor ?>
                <? for ($i = round($arItem["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                    <div class="star-bad-small"></div>
                <? endfor ?>
            </div>
            <div class="address">
                <?= $address ?>
            </div>
        </div>
        <div class="clear"></div>        
        </a>
        <div class="line2"></div>
    </li>
    <?endforeach?>        
</ul>

<input type="hidden" id="processing" value="0">
<input type="hidden" id="maxpage" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
 <div class="priorities-ajax"></div>
<div class="ajax-container">
    <div class="loading2">
        <div></div>             
        <div></div>             
        <div></div>             
        <div></div>             
        <div></div>             
        <div></div>             
        <div></div>             
        <div></div>         
    </div>
</div>

<div class="clear"></div>
<div class="to-top-link-container" onclick="$('html,body').animate({scrollTop: 0 },'slow');">
    <div class="to-top-link">Наверх</div>
</div>
<div class="clear"></div>


</div>