<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?= ShowError($arResult["strProfileError"]); ?>
<h3 style="margin: 0 0 10px 0;"><?= GetMessage("PROFILE") ?></h3>
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center" valign="top" style="width:170px">
            <div class="userpic" style="background:url(<?= $arResult["arUser"]["PERSONAL_PHOTO"]["src"] ?>) left top no-repeat">
                <!--<img src="<?= $arResult["arUser"]["PERSONAL_PHOTO"]["src"] ?>" width="<?= $arResult["arUser"]["PERSONAL_PHOTO"]["width"] ?>"  height="<?= $arResult["arUser"]["PERSONAL_PHOTO"]["height"] ?>" />-->
            </div>
        </td>
        <td valign="top" style="position:relative">
            <div class="read" style="position:relative">
                <table cellpadding="2" class="user_params">
                    <tr>
                        <td width="100" class="name"><?= GetMessage('NAME') ?></td>
                        <td class="value"><?= $arResult["arUser"]["NAME"] ?> <?= $arResult["arUser"]["LAST_NAME"] ?></td>
                    </tr>                 
                    <? if ($arResult["arUser"]["PERSONAL_GENDER"]): ?>
                        <tr>
                            <td class="name"><?= GetMessage('USER_GENDER') ?></td>
                            <td class="value"><?= GetMessage("SEX_" . $arResult["arUser"]["PERSONAL_GENDER"]) ?></td>
                        </tr>
                    <? endif; ?>
                    <? if ($arResult["arUser"]["PERSONAL_BIRTHDAY"]): ?>
                        <tr>
                            <td class="name"><?= GetMessage('USER_BIRTHDAY') ?></td>
                            <td class="value"><?= $arResult["arUser"]["PERSONAL_BIRTHDAY"] ?></td>
                        </tr>
                    <? endif; ?>
                    <? if ($arResult["arUser"]["PERSONAL_CITY"]): ?>
                        <tr>
                            <td class="name"><?= GetMessage('USER_CITY') ?></td>
                            <td class="value"><?= $arResult["arUser"]["PERSONAL_CITY"] ?></td>
                        </tr>       
                    <? endif; ?>
                    <? if (($arResult["arUser"]["PERSONAL_PHONE"] && (!$arResult["arUser"]["UF_HIDE_CONTACTS"] || $arResult["ID"] == $USER->GetID())) || ($arResult["arUser"]["PERSONAL_PHONE"] && $USER->IsAdmin())): ?>
                        <tr>
                            <td class="name"><?= GetMessage('USER_PHONE') ?></td>
                            <td class="value"><?= $arResult["arUser"]["PERSONAL_PHONE"] ?></td>
                        </tr>       
                    <? endif; ?>
                    <? if (($arResult["arUser"]["EMAIL"] && !$arResult["arUser"]["UF_HIDE_CONTACTS"] || $arResult["ID"] == $USER->GetID()) || ($arResult["arUser"]["EMAIL"] && $USER->IsAdmin())): ?>
                        <tr>
                            <td class="name"><?= GetMessage('EMAIL') ?></td>
                            <td class="value"><?= $arResult["arUser"]["EMAIL"] ?></td>
                        </tr> 
                    <? endif; ?>
                    <? if ($arResult["arUser"]["PERSONAL_NOTES"]): ?>
                        <tr>
                            <td class="name"><?= GetMessage('KITCHEN') ?></td>
                            <td class="value"><i><?= $arResult["arUser"]["PERSONAL_NOTES"] ?></i></td>
                        </tr>
                    <? endif; ?>
                    <? if ($arResult["arUser"]["WORK_NOTES"]): ?>
                        <tr>
                            <td class="name"><?= GetMessage('ABOUT') ?></td>
                            <td class="value font12"><i><?= $arResult["arUser"]["WORK_NOTES"] ?></i></td>
                        </tr>    
                    <? endif; ?>
                </table>
            </div>
        </td>
    </tr>
</table> 


