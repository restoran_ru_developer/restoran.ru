<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // explode date
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"]." G:i", MakeTimeStamp($arResult["ITEMS"][$key]["DATE_CREATE"], CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_TIME"] = $arTmpDate[3];
    $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = '/mobile/opinions.php?tid='.$arItem["ID"];
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    $arUser = $rsUser->Fetch();
    if ($arUser["PERSONAL_PROFESSION"])
        $arResult["ITEMS"][$key]["NAME"] = $arUser["PERSONAL_PROFESSION"];
    else
        $arResult["ITEMS"][$key]["NAME"] = $arUser["LAST_NAME"]." ".$arUser["NAME"]; 
    $arResult["ITEMS"][$key]["AVATAR"] = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT, true);    
    //$arResult["ITEMS"][$key]["AVATAR"]["src"] = CFile::GetPath($arUser["PERSONAL_PHOTO"]);    
    if (!$arResult["ITEMS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="M")
        $arResult["ITEMS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/man_nnm.png";
    elseif (!$arResult["ITEMS"][$key]["AVATAR"]["src"]&&$arUser["PERSONAL_GENDER"]=="F")
        $arResult["ITEMS"][$key]["AVATAR"]["src"] = "/tpl/images/noname/woman_nnm.png";
    elseif (!$arResult["ITEMS"][$key]["AVATAR"]["src"])
        $arResult["ITEMS"][$key]["AVATAR"]['src'] = "/tpl/images/noname/unisx_nnm.png";
    
    // get rest name
    $rsSec = CIBlockElement::GetByID($arItem["PROPERTIES2"]["ELEMENT"][0]);
    if($arSec = $rsSec->GetNext()) {
        $pic = array();
        //$pic = CFile::ResizeImageGet($arSec['PREVIEW_PICTURE'], array('width'=>70, 'height'=>60), BX_RESIZE_IMAGE_EXACT, true);           
        //if (!$pic["src"])
          //  $pic = CFile::ResizeImageGet($arSec['DETAIL_PICTURE'], array('width'=>60, 'height'=>50), BX_RESIZE_IMAGE_EXACT, true);
        $pic["src"] = CFile::GetPath($arSec['PREVIEW_PICTURE']);   
        if (!$pic["src"])
            $pic["src"] = CFile::GetPath($arSec['DETAIL_PICTURE']);   
        $arResult["ITEMS"][$key]["RESTORAN"] = Array("ID"=>$arSec["ID"],"NAME"=>$arSec["NAME"],"ACTIVE"=>$arSec["ACTIVE"],"IBLOCK_NAME"=>$arSec["IBLOCK_NAME"],"SRC"=>$pic["src"],"DETAIL_PAGE_URL"=>$arSec["DETAIL_PAGE_URL"],"IBLOCK_TYPE_ID"=>$arSec["IBLOCK_TYPE_ID"]);       
    }
    
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = "<p>".strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"])."</p>";
    if ($arItem["PROPERTIES"]["plus"])
    {
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] .="<p><b>".GetMessage("R_PLUS").":</b>";
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] .=" ".strip_tags($arItem["PROPERTIES"]["plus"]."</p>");
    }
    if ($arItem["PROPERTIES"]["minus"])
    {
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] .="<p><b>".GetMessage("R_MINUS").":</b>";
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] .=" ".strip_tags($arItem["PROPERTIES"]["minus"]."</p>");
    }
    
    $obParser = new CTextParser;
    $arResult["ITEMS"][$key]["PREVIEW_TEXT2"] = $obParser->html_cut($arResult["ITEMS"][$key]["PREVIEW_TEXT"], 200);
    
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT2"] = TruncateText($arResult["ITEMS"][$key]["PREVIEW_TEXT"], 400);
    /*$arResult["ITEMS"][$key]["PROPERTIES"]["pluss"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["plus"]);
    $arResult["ITEMS"][$key]["PROPERTIES"]["pluss"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["plus"], 400);
    
    $arResult["ITEMS"][$key]["PROPERTIES"]["minuss"]["TEXT"] = stripslashes($arItem["PROPERTIES"]["minus"]);
    $arResult["ITEMS"][$key]["PROPERTIES"]["minuss"]["TEXT2"] = TruncateText($arItem["PROPERTIES"]["minus"], 400);*/
    foreach ($arResult["ITEMS"][$key]["PROPERTIES"]["photos"] as &$photo)
    {
        $id = $photo;
        $photo = array();
        $photo["ID"] = $id;
        $photo["SRC"] = CFile::ResizeImageGet($id,array("height"=>124,"width"=>137),BX_RESIZE_IMAGE_EXACT,true);
        $photo["SRC"] = $photo["SRC"]["src"];
        //TODO:Сделать, чтобы фотки уменьшалис уже при загрузке
        $photo["ORIGINAL_SRC"] = CFile::ResizeImageGet($id,array("height"=>600,"width"=>800),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
    }
}

global $arrFilter;
if ($arrFilter["PROPERTY_ELEMENT"])
{
    global $APPLICATION;
    $cp = $this->__component; // объект компонента
    if (is_object($cp))
    {
            // добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
            $cp->arResult['RESTORAN_URL'] = $arResult["ITEMS"][0]["RESTORAN"]["DETAIL_PAGE_URL"];
            $cp->arResult['RESTORAN'] = $arResult["ITEMS"][0]["RESTORAN"]["NAME"];
            //Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
            $cp->SetResultCacheKeys(array('RESTORAN','RESTORAN_URL'));
            // сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
            if (!isset($arResult['RESTORAN']))
            {
                    $arResult['RESTORAN'] = $cp->arResult['RESTORAN'];
                    $arResult['RESTORAN_URL'] = $cp->arResult['RESTORAN_URL'];
            }
    }
}
?>