<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/*foreach($arResult["ITEMS"] as $key=>$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
  //  $arResult["SECTION"]["PATH"]
}*/
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arTmpDate);
    $arResult["ITEMS"][$key]["CREATED_DATE_FORMATED_1"] = $arTmpDate[0]." ".$arTmpDate[1]." ".$arTmpDate[2];
    
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arResult["ITEMS"][$key]["DETAIL_PICTURE"], array('width' => 200, 'height' => 146), BX_RESIZE_IMAGE_EXACT, true);
    if(!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"])
                    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["LINK_IBLOCK_TYPE"] = $arItem["IBLOCK_TYPE_ID"];
    if ($arItem['IBLOCK_TYPE_ID']=="comment"||$arItem['IBLOCK_TYPE_ID']=="reviews")
    {
        $db_props = CIBlockElement::GetProperty($arItem["IBLOCK_ID"], $arItem["ID"], array("sort" => "asc"), Array("CODE"=>"ELEMENT"));
        if($ar_props = $db_props->Fetch())
        {
            $element_id = IntVal($ar_props["VALUE"]);
            $res = CIBlockElement::GetList(Array(),Array("ID"=>$element_id),false,false,Array("ID","IBLOCK_TYPE_ID","DETAIL_PICTURE","NAME","DETAIL_PAGE_URL"));
            $res->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
            if ($ar = $res->GetNext())
            {
                $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($ar["DETAIL_PICTURE"], array('width' => 200, 'height' => 146), BX_RESIZE_IMAGE_EXACT, true);
                if(!$arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"])
                    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
                $arResult["ITEMS"][$key]["NAME"] = change_quotes($ar["NAME"]);
                $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = $ar["DETAIL_PAGE_URL"];
                $arResult["ITEMS"][$key]["LINK_IBLOCK_TYPE"] = $ar["IBLOCK_TYPE_ID"];
            }
        }
        
    }
}

?>