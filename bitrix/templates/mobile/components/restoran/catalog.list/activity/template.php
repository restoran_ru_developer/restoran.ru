<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
    <h2>Последние отзывы</h2>
<? endif; ?>
<? foreach ($arResult["ITEMS"] as $key => $arItem): ?> 
    <div class="one-opinion">
        <div class="clear"></div>
        <div style="width: 120px; float: left">
            <div class="comment_date" style="margin-top: 3px; margin-bottom: 4px;"><?= $arItem["CREATED_DATE_FORMATED_1"] ?></div>
            <div style="float: left;position: relative;width: 110px;">
                <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" width="110" />
            </div>
        </div>
        <a class="another" style="text-decoration: none;" href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
        <div style="">
            <?= $arItem["PREVIEW_TEXT"] ?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="line2"></div>  
<? endforeach; ?>