<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["NO_COMMENTS"] = "Здесь еще никто не писал, Вы можете быть первым";
$MESS ["ALL_REVIEWS"] = "ВСЕ ОТЗЫВЫ";
$MESS ["ONLY_AUTHORIZED"] = "Только авторизованные пользователи могут писать отзывы.";
$MESS ["CLICK_TO_RATE"] = "Нажмите, чтобы оценить ресторан";
?>