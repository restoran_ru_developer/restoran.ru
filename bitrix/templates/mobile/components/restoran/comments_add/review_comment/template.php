<div id="write_comment"> 
    <? if (!$USER->IsAuthorized()): ?>
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:system.auth.form", "no_auth", Array(
            "REGISTER_URL" => "",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "",
            "SHOW_ERRORS" => "N"
                ), false
        );
    endif;
    ?>


    <form action="/bitrix/components/restoran/comments_add/ajax.php" class="submit_comment_form" id="comment_form" method="post" novalidate="novalidate">
        <? if (!$USER->IsAuthorized()): ?>
            <div class="uppercase font10 ls1" style="padding-left:6px;">E-mail:</div>
            <input class="" value="" name="email" req="req" style="width:100%" data-theme="d"/>
            <div class="uppercase font10 ls1" style="padding-left:6px;padding-top:5px;"><?= GetMessage("R_COMMENT") ?>:<input id="ch" name="ch" type="checkbox" value="1" /></div>
        <? endif ?>
        <h2><?= GetMessage("YOUR_COMMENT") ?></h2>
        <?= bitrix_sessid_post() ?>
        <textarea data-theme="d" class="add_review" id="review" name="review" req="req" style="width:100%; margin-bottom:0px;"></textarea>            
        <div class="grey_block" style="padding:10px;">                
            <div class="clear"></div>
        </div> 

        <input type="hidden" name="IBLOCK_TYPE" value="<?= $arParams["IBLOCK_TYPE"] ?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
        <input type="hidden" name="ELEMENT_ID" value="<?= $arParams["ELEMENT_ID"] ?>" />
        <input type="hidden" name="IS_SECTION" value="<?= $arParams["IS_SECTION"] ?>" />
        <input type="hidden" name="PARENT" value="<?= $arParams["PARENT"] ?>" />
        <input type="hidden" name="cpt" value="<?= $arParams["MY_CAPTCHA"] ?>" />
        <div class="clear"></div>
    </form>
    <div class="grey_block">
        <? if (!$USER->IsAuthorized()): ?>
            <div class="QapTcha"></div>
            <link rel="stylesheet" href="/tpl/js/quaptcha_mobile_1/QapTcha.jquery.css" type="text/css" />
            <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
            <script type="text/javascript" src="/tpl/js/quaptcha_mobile_1/jquery.ui.touch.js"></script>
            <script type="text/javascript" src="/tpl/js/quaptcha_mobile_1/QapTcha.jquery.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.QapTcha').QapTcha({
                        txtLock : '<?= GetMessage("MOVE_SLIDER") ?>',
                        txtUnlock : '<?= GetMessage("MOVE_SLIDER_DONE") ?>',
                        disabledSubmit : true,
                        autoRevert : true,
                        PHPfile : '/tpl/js/quaptcha_mobile/Qaptcha.jquery.php',
                        autoSubmit : false});
                });
            </script>
            <div class="clear"></div>
            <input disabled="disabled" type="submit" class="add_commm light_button" value="<?= GetMessage("R_COMMENT_ADD") ?>"/> 
        <? else: ?>
            <input type="submit" class="add_commm light_button" value="<?= GetMessage("R_COMMENT_ADD") ?>"/> 
        <? endif; ?>

        <div class="clear"></div>
    </div>  
</div>