<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
$MESS ["YOUR_REVIEW"] = "Your review";
$MESS ["NO_COMMENTS"] = "No one's writing, you can be the first";
$MESS ["ALL_REVIEWS"] = "ALL REVIEWS";
$MESS ["ONLY_AUTHORIZED"] = "Only authorized users can write reviews";
$MESS ["CLICK_TO_RATE"] = "Click to rate restaurant";
$MESS ["ADD_PHOTO"] = "Add Photo";
$MESS ["ADD_VIDEO"] = "Add Video";
$MESS ["ADD_PROS_CONS"] = "Add pros and cons of place";
$MESS ["REVIEW_TEXT"] = "Review";
$MESS ["PROS"] = "PROS";
$MESS ["CONS"] = "CONS";
$MESS ["MORE"] = "MORE";
$MESS ["YOUTUBE_LINK"] = "Youtube Link";
$MESS ["MOVE_SLIDER"] = "Move the slider to the right";
$MESS ["MOVE_SLIDER_DONE"] = "Done. You can send now";
$MESS ["REVIEWS_PUBLISH"] = "Publish";
$MESS ["ER_RATING"] = "Not selected rating";
$MESS ["ER_TEXT"] = "Comment is empty";
$MESS ["R_TERMS"] = "<p>Dear friends! Remember that the editor will remove:</p>
        <p>1. Comments with profanity and rough<br />
        2. Direct or indirect insults hero post or readers<br />
        3. Short evaluation comments («awful», «class», «sucks»)<Br />
        4. Comments that incite ethnic and social strife<br />
        <p><a href='#'>Full terms of Restoran.ru</a></p>";
?>