<script src="<?= $templateFolder ?>/jquery.form.js"></script>
<script src="<?= $templateFolder ?>/jQuery.fileinput.js"></script>
<h2><?= GetMessage("YOUR_REVIEW") ?></h2>
<style>
    .TxtStatus {
    color: black !important;
    font-style: normal !important;
    font-weight: bold !important;
    font-size: 14px !important;
    margin-left: 0px !important;
}

.QapTcha .bgSlider {
    width: 100% !important;
}
    
</style>
<script>
    $(document).ready(function(){
        $('#file-field').customFileInput();
        $('#video-field').customFileInput();
        $("#add_photo").click(function(){
            var count = $(".file-field").length;
            $(this).prev().after('<input type="file" name="file[]" value="" id="file-field'+count+'" class="file-field" />');
            $('#file-field'+count).customFileInput();
        });
            
        $("#restoraunt").autocomplete("/tpl/ajax/afisha_suggest.php", {
            limit: 5,
            minChars: 3,
            width: 685,
            formatItem: function(data, i, n, value) {
                return value.split("###")[0];
            },
            formatResult: function(data, value) {
                return value.split("###")[0];
            }        
        });  
        $("#restoraunt").result(function(event, data, formatted) {
            if (data) {
                $("#restoran222").val(formatted.split("###")[1]);
            }
        });
        $('#comment_form').ajaxForm({
            beforeSubmit: check_editor_form,
            success: function(data) {
                if(data)
                {
                    data = eval('('+data+')');                        
                    if (!$("#comment_modal").size())
                    {
                        $("<div class='popup popup_modal' id='comment_modal'></div>").appendTo("body");
                    }
                    var html = '<div align="right"><a class="modal_close uppercase black" href="javascript:void(0)"></a></div><div class="center">';
                    var html2 = '</div>';                    
                    $('#comment_modal').html(html+data.MESSAGE+html2);
                    showOverflow();
                    setCenter($("#comment_modal"));
                    $("#comment_modal").fadeIn("300");
                    if (data.ERROR=="1")
                    {
                        $("#add_commm").attr("disabled",false);
                    }
                    if (data.STATUS=="1")
                        setTimeout("location.reload()","3500");
                }
                return false;
            }                
        });
        function check_editor_form()
        {
            if (ajax_load)
                return false;
            if (!$("#input_ratio").val()) {
                alert("<?= GetMessage("NO_RATING") ?>");
                return false;
            }
            if (!$("#review").val() && parseInt($("#input_ratio").val()) > 0) {
                alert("<?= GetMessage("NO_COMMENT") ?>");
                return false;
            }
            if (!$("#restoran222").val())
            {
                alert("<?= GetMessage("NO_REST") ?>");
                return false;
            }
            $("#add_commm").attr("disabled","disabled");
        }
    });
</script>
<? if (!$USER->IsAuthorized()): ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "no_auth", Array(
        "REGISTER_URL" => "",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "",
        "SHOW_ERRORS" => "N"
            ), false
    );
    ?> 
    <br /><br />
<? endif; ?>
<form id="comment_form" action="/bitrix/components/restoran/comments_add_new/ajax.php" method="post" novalidate="novalidate">
    <?= bitrix_sessid_post() ?>
    <div class="active_rating">
        <? for ($i = 1; $i <= 5; $i++): ?>
            <div class="star" alt="<?= $i ?>"></div>
        <? endfor; ?>
    </div>
    <div class="grey left" style="margin-left:10px;"><i>&ndash; <?= GetMessage("CLICK_TO_RATE") ?></i></div>
    <div class="clear"></div> 
    <br />
    <div class="grey_block" style="margin-bottom:10px">
        <div class="uppercase"><?= GetMessage("CHOOSE_REST") ?>: <span class="grey" style="letter-spacing: 0px; font-size:11px; text-transform: lowercase"> <i><?= GetMessage("ONLY_REST") ?></i></span></div>
        <input type="text" id="restoraunt" class="inputtext-with_border font14" style="width:665px"  req="req"/>
        <input type="hidden" id="restoran222" value="" size="30" name="ELEMENT_ID"/>
    </div>


    <? if (!$USER->IsAuthorized()): ?>
        <div class="grey_block" style="margin-bottom:10px">
            <div class="uppercase">E-mail:</div>
            <input class="inputtext-with_border font14" value="" name="email" req="req" style="width:665px" /><br />           
        </div>
    <? endif ?>
    <div class="grey_block" id="write_com" style="margin-bottom:10px;">
        <div class="uppercase left"><?= GetMessage("REVIEW_TEXT") ?></div>
        <div class="right">
            <a href="javascript:void(0)" class="button" onclick="$('#add_videos').toggle(300)">+ <?= GetMessage("ADD_VIDEO") ?></a>
        </div>
        <div class="right">
            <a href="javascript:void(0)" class="button" onclick="$('#add_photos').toggle(300)">+ <?= GetMessage("ADD_PHOTO") ?></a> 
        </div>                        
        <div class="clear"></div>
        <textarea class="add_review" id="review" name="review" req="req" style="width:687px;height:180px"></textarea>
        <input type="hidden" value="" id="input_ratio" name="ratio" />
        <div style="text-align:center"><a href="javascript:void(0)" class="js another" onclick="$('#plus_minus').toggle(300)"><?= GetMessage("ADD_PROS_CONS") ?></a></div>
    </div>
    <div class="grey_block" id="plus_minus">
        <div class="left">
            <div class="uppercase"><?= GetMessage("PROS") ?></div>
            <textarea class="add_review plus" name="plus" req="req" style="width:315px"></textarea>
        </div>
        <div class="right">
            <div class="uppercase"><?= GetMessage("CONS") ?></div>
            <textarea class="add_review" name="minus" req="req" style="width:315px"></textarea>
        </div>
        <div class="clear"></div>
    </div>
    <div class="grey_block" id="add_photos">
        <div class="uppercase"><?= GetMessage("ADD_PHOTO") ?></div>
        <input type="file" name="file[]" value="" id="file-field" class="file-field" />
        <a class="uppercase" id="add_photo" style="color:#1a1a1a; border-bottom:1px dotted #1a1a1a">ЕЩЕ</a>
    </div>
    <div class="grey_block" id="add_videos">
        <div class="uppercase"><?= GetMessage("ADD_VIDEO") ?></div>
        <input type="file" name="video" value="" id="video-field" class="file-field" />
        <div class="uppercase"><?= GetMessage("YOUTUBE_LINK") ?></div>
        <input type="text" name="video_youtube" value="" class="inputtext-with_border font14" style="width:665px"  />
    </div>

    <? if (!$USER->IsAuthorized()): ?>
        <div class="QapTcha"></div>
        <link rel="stylesheet" href="/tpl/js/quaptcha_mobile_1/QapTcha.jquery.css" type="text/css" />
        <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
        <script type="text/javascript" src="/tpl/js/quaptcha_mobile_1/jquery.ui.touch.js"></script>
        <script type="text/javascript" src="/tpl/js/quaptcha_mobile_1/QapTcha.jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.QapTcha').QapTcha({
                    txtLock : '<?= GetMessage("MOVE_SLIDER") ?>',
                    txtUnlock : '<?= GetMessage("MOVE_SLIDER_DONE") ?>',
                    disabledSubmit : true,
                    autoRevert : true,
                    PHPfile : '/tpl/js/quaptcha_mobile/Qaptcha.jquery.php',
                    autoSubmit : false});
            });
        </script>
        <div class="clear"></div>
    <? endif; ?>
    <div class="right" style="margin-top:5px;">
        <input type="submit" id="add_commm" class="light_button" value="+ <?= GetMessage("REVIEWS_PUBLISH") ?>">       
    </div>
    <div class="clear"></div>

    <div>                    
        <Br />            
        <!--<div class="figure_link" style="margin-left:0px;">
            <input type="submit" value="+ Комментировать">
        </div>-->
        <input type="hidden" name="IBLOCK_TYPE" value="<?= $arParams["IBLOCK_TYPE"] ?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
        <input type="hidden" name="IS_SECTION" value="<?= $arParams["IS_SECTION"] ?>" />
    </div>
    <div id="rating_overlay">
        <div class="close"></div>                           
    </div>
    <div class="clear"></div>
    <div class="grey left">
        <?= GetMessage("R_TERMS") ?> 
    </div>
    <div class="clear"></div>
</form>
