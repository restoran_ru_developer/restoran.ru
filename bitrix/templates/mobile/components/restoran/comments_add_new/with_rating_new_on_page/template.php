1<?
$APPLICATION->AddHeadString('<link href="' . $templateFolder . '/fineuploader.css" rel="stylesheet" type="text/css"/>');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
$APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
?>
<? if (!$USER->IsAuthorized()): ?>
<div style="margin-bottom:15px;color: black;font-size: 14px;">
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "no_auth", Array(
        "REGISTER_URL" => "",
        "FORGOT_PASSWORD_URL" => "", 
        "PROFILE_URL" => "",
        "SHOW_ERRORS" => "N"
            ), false
    );
    ?> 
    </div>
<? endif; ?>
    <div class="left" style="margin-bottom:5px;color: black;font-size: 14px;font-weight: bold;"><?= GetMessage("CLICK_TO_RATE") ?></div>
    <div class="clear"></div> 
<form name="attach" action="/bitrix/components/restoran/comments_add_new/ajax.php" method="post" id="comment_form" enctype="multipart/form-data">   
    <?= bitrix_sessid_post() ?>
    <div class="active_rating">
        <? for ($i = 1; $i <= 5; $i++): ?>
            <div class="star" alt="<?= $i ?>"></div>
        <? endfor; ?>
    </div>
    
    <div class="clear"></div> 
    <h1 style="text-align:left;color:black; margin-bottom:0px; font-size: 14px">Выберите ресторан</h1>
    <div class="question autocomplete-question">
        <?
        $APPLICATION->IncludeComponent("restoran:restoraunts.list", "rest_list_select_review", Array(
            "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => CITY_ID, // Код информационного блока
            "PARENT_SECTION_CODE" => "restaurants", // Код раздела
            "NEWS_COUNT" => 10000, // Количество ресторанов на странице
            "SORT_BY1" => "NAME", // Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "ASC", // Направление для первой сортировки ресторанов
            "SORT_BY2" => "SORT", // Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "ASC", // Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arrFilter", // Фильтр
            "PROPERTY_CODE" => array(),
            "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "N", // Тип кеширования
            //"CACHE_TIME" => "21600", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y", // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
            "SET_TITLE" => "Y", // Устанавливать заголовок страницы
            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
            "PAGER_TITLE" => "Рестораны", // Название категорий
            "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y", // Выводить дату элемента
            "DISPLAY_NAME" => "Y", // Выводить название элемента
            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                ), $component
        );
        ?>
    </div>
    <? if (!$USER->IsAuthorized()): ?>
        <div class="grey_block">
            <div class="uppercase" style="text-align: left; color: black;font-size: 14px;font-weight: bold;">E-mail:</div>
            <input class="inputtext-with_border font14" value="" data-theme="d" name="email" req="req" style="width:100%" /><br />           
        </div>
    <? endif ?>
    <div class="" id="write_com">
        <div class="uppercase" style="text-align: left; color: black;font-size: 14px;font-weight: bold;"><?= GetMessage("REVIEW_TEXT") ?>:</div>            
        <div class="clear"></div>
        <textarea class="add_review" id="review" name="review" req="req" style="width:100%;height:80px" data-theme="d"></textarea>
        <input type="hidden" value="" id="input_ratio" name="ratio" />

        <div class="right">
            <div class="left attach" style="color: black;font-size: 14px;font-weight: bold; margin-top: 7px;">Прикрепить:</div>
            <div id="attach_photo" class="left photo_icon"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="img-container" id="img-container">
        <div class="clear"></div>
    </div>
    <div class="grey_block">            
        <? if (!$USER->IsAuthorized()): ?>
            <div class="clear"></div>
            <div class="QapTcha"></div>
            <link rel="stylesheet" href="/tpl/js/quaptcha_mobile_1/QapTcha.jquery.css" type="text/css" />
            <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
            <script type="text/javascript" src="/tpl/js/quaptcha_mobile_1/jquery.ui.touch.js"></script>
            <script type="text/javascript" src="/tpl/js/quaptcha_mobile_1/QapTcha.jquery.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.QapTcha').QapTcha({
                        txtLock : '<?= GetMessage("MOVE_SLIDER") ?>',
                        txtUnlock : '<?= GetMessage("MOVE_SLIDER_DONE") ?>',
                        disabledSubmit : true,
                        autoRevert : true,
                        PHPfile : '/tpl/js/quaptcha_mobile/Qaptcha.jquery.php',
                        autoSubmit : false});
                });
            </script>
            <div class="clear"></div>
        <? endif; ?>

        <div class="clear"></div>
        <input style="width: 100%;" <? if (!$USER->IsAuthorized()){ echo 'disabled="disabled"';} ?> type="submit" id="add_commm" class="light_button add-comm-mobile" value="<?= GetMessage("REVIEWS_PUBLISH") ?>">       

        <div class="clear"></div>
    </div>
    <div>                    
        <Br />            
        <input type="hidden" name="IBLOCK_TYPE" value="<?= $arParams["IBLOCK_TYPE"] ?>" />
        <input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>" />
        <input type="hidden" name="IS_SECTION" value="<?= $arParams["IS_SECTION"] ?>" />
    </div>
    <div id="rating_overlay">
        <div class="close"></div>                           
    </div>
    <div class="clear"></div>
    <div style="margin-bottom:15px;color: black;font-size: 14px;">
        <?= GetMessage("R_TERMS") ?> 
    </div>
    <div class="clear"></div>
</form>
<a id="open-dialog-link" href="#review-dialog-page" data-rel="dialog"></a>
<script>
    $(document).ready(function(){        
            
        var files = new Array();
        var errorHandler = function(event, id, fileName, reason) {
            qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
        };
        $('#attach_photo').fineUploader({
            text: {
                uploadButton: "",
                cancelButton: "",
                waitingForResponse: ""
            },
            multiple: false,
            disableCancelForFormUploads: true,
            request: {
                endpoint: "<?= $templateFolder ?>/upload.php",
                params: {"generateError": true,"f":"images"}
            },
            validation:{
                allowedExtensions : ["jpeg","jpg","bmp","gif","png"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
                typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: jpg, gif, png"
            }
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push(response.resized);            
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="'+response.resized+'" height="70">').appendTo($('#img'+files.length));
                $('<input value="'+response.resized+'" type="hidden" name="image[]" />').appendTo($('#img'+files.length));                        
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        }); 
        $('#attach_video').fineUploader({
            text: {
                uploadButton: "",
                cancelButton: "",
                waitingForResponse: ""
            },
            multiple: true,
            disableCancelForFormUploads : true,
            request: {
                endpoint: "<?= $templateFolder ?>/upload.php",
                params: {"generateError": true, "f":"video"}
            },
            validation:{
                allowedExtensions : ["m4v","avi","mov","flv","3gp"]
            },
            failedUploadTextDisplay: {                
                maxChars: 5
            },
            messages: {
                typeError: "Неверный тип файла. Поддерживается загрузка следующих форматов: m4v, avi, mov, flv, 3gp"
            }                
        })
        .on('upload', function(id, fileName){
            $(".qq-upload-list").show();
        })
        .on('error', errorHandler)
        .on('complete', function(event, id, fileName, response) {
            if (response.success)
            {
                files.push("video");                        
                $('<div id="img'+files.length+'" class="image-block">').insertBefore($('#img-container .clear'));
                $('<a href="#" class="remove-link">').appendTo($('#img'+files.length));
                $('<img src="<?= $templateFolder ?>/images/video_file.png" height="70">').appendTo($('#img'+files.length));
                $('<div class="name">'+response.uploadName+'</div>').appendTo($('#img'+files.length));
                $('<input value="'+response.path+'" type="hidden" name="video[]" />').appendTo($('#img'+files.length));                            
                $(".qq-upload-list").hide();
                $(".qq-upload-success").remove();
            }
        });
        $("#wr_cm").click(function(){
            $(this).hide();
            $("#write_comment").show("500");                
        });                 
        $(".img-container").on("click",".remove-link",function(){
            var file = $(this).next().attr("src");
            $(this).parents(".image-block").remove();                
            return false;
        });
        
        $("#comment_form").keypress(function(e){
            e = e || window.event;    
            //for chrome & safari
            if (e.ctrlKey) {
                if(e.keyCode == 10){
                    $("#comment_form").submit();
                    return false;
                }
            };
            //for firefox
            if (e.keyCode == 13 && e.ctrlKey) {
                $("#comment_form").submit();
                return false;
            };
        });
        $("#comment_form").submit(function(){
            if (!$(this).find("#review").val())
            {
                alert("Введите комментарий!");
                return false;
            }
            if (!$(this).find("#input_ratio").val())
            {
                alert("Оцените ресторан!");
                return false;
            }
            var params = $(this).serialize();
            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: params,
                success: function(data) {
                    if(data.trim() !== '')
                    {
                        data = eval('('+data+')');
                        if (data.ERROR=="1")
                        {
                            alert(data.MESSAGE);
                            $("#add_commm").attr("disabled",false);
                        }
                        if (data.STATUS=="0")
                        {
                            alert(data.MESSAGE);
                            $("#add_commm").attr("disabled",false);
                        }
                        if (data.STATUS=="1"){
                            alert('Спасибо! Ваш комментарий добавлен.')
                            setTimeout("location.reload()","100");
                        }
                    }
                }
            });
            return false; 
        });
    });
    
</script>