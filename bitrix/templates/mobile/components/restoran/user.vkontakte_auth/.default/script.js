function showRegpromptVK() {
    /*$(document).ready(function() {
        // show prompt_vk for additional data
        $('#prompt_vk').overlay({
            top: 260,
            onBeforeLoad: function(){
                $('#prompt_vk > p:eq(0)').show();
                $('#prompt_vk > p:eq(1)').hide();
            },
            onBeforeClose: function() {
                $('div.error').hide();
            },
            mask: {
                color: '#fff',
                loadSpeed: 200,
                closeSpeed:0,
                opacity: 0.5
            },
            closeOnClick: false,
            closeOnEsc: false,
            api: true
        }).load();
    });*/
    if (!$("#auth_modal2").size())
    {
        $("<div class='popup popup_modal' id='auth_modal2' style='width:400px;'></div>").appendTo("body");
        setCenter($("#auth_modal2"));
        $("#auth_modal2").html($('#prompt_vk').html());
    }
    showOverflow();
    $("#auth_modal2").fadeIn("300");
 
    $("#auth_modal2  form").submit(function(e) {
    	var form = $(this);
    	// client-side validation OK.
    	if (!e.isDefaultPrevented()) {
    		// submit with AJAX
            $.ajax({
                type: "POST",
                url: "/bitrix/components/restoran/user.vkontakte_auth/templates/.default/authorize.php",
                data: form.serialize(),
                beforeSend: function(data)
                {
                    /*if (!$("#vk_email").attr("value"))
                    {
                        alert("Введите E-mail");
                        return false;
                    }
                    if (!$("#vk_phone").attr("value"))
                    {
                        alert("Введите номер телефона");
                        return false;
                    }*/
                    
                    return true;
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);
                    // if result OK show confirm code field
                    if (obj.ERROR)
                    {
                        $('#auth_modal2 #ajax_object_response').html(obj.ERROR);
                        return false;
                    }
                    if(obj.TYPE == "AUTH") {
                        location.href = "/";
                    } else if(obj.TYPE == "REQ_CONFIRM_CODE") {
                        $('#auth_modal2 > .center > p:eq(0)').hide();
                        $('#auth_modal2 > .center > p:eq(1)').hide();
                        $('#auth_modal2 > .center > p:eq(2)').show();
                        $('#auth_modal2 > form > div:eq(0)').html(obj.HTML);
                        // reinitialize validator
                       // $("#auth_modal > form").validator({ lang: 'ru' });
                    } else if(obj.TYPE == "ADD") {
                        location.href = "/";
                    }
                }
            });
    		// prevent default form submission logic
    		e.preventDefault();
    	}
        return false;
    });
}

$(document).ready(function() {
    

    // validate form
    
});