<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
//XXX don't forget set correct path to backend authorize file in script.js
?>
<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.min.js')?>
<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.validator.js')?>
<style>
    #ajax_object_response {color:red}
</style>
<div id="login_button<?=$arParams["PREFIX"]?>" onclick="VK.Auth.login(authInfo);"></div>

<div id="vk_api_transport"></div>
<script language="javascript">
window.vkAsyncInit = function() {
  VK.init({
    apiId: <?=$arParams["APP_ID"]?>
  });
  VK.UI.button('login_button<?=$arParams["PREFIX"]?>');
};

setTimeout(function() {
  var el = document.createElement("script");
  el.type = "text/javascript";
  el.src = "http://vk.com/js/api/openapi.js";
  el.async = true;
  document.getElementById("vk_api_transport").appendChild(el);
}, 0);

function authInfo(response) {
    // if success authorize require email
    if (response.session) {
        // authorize if user exist and VK ID == user id
        //showExpose();
        $.ajax({
            type: "POST",
            url: "<?=$templateFolder?>/getUserVK.php",
            data: "vk_user_id=" + response.session.mid,
            success: function(data) {
                var obj = jQuery.parseJSON(data);
                if(obj.TYPE == "AUTH") {
                	location.href = "/";
                } else {
                    $('#vk_user_id').val(response.session.mid);
                    showRegpromptVK();
                }
            }
        });
    }
}
//VK.Auth.getLoginStatus(authInfo);

</script>

<div class="modal_vk" id="prompt_vk">
    <div align="right">
        <a class="modal_close uppercase white" href="javascript:void(0)"></a>
    </div>
    <div id="ajax_object_response"></div>
        <h3 style="color:#FFF;margin:10px 0px">Заполните поля ниже</h3>
        <div class="center">
            <p>- E-mail необходим для регистрации в системе (Вконтакте не передает e-mail)</p>
            <!--<p>- На указанный номер мобильного телефона, Вам должна прийти СМС с кодом подтверждения регистрации (формат: 79XXXXX...)</p>
            <p style="display: none;">
                - Введите проверочный код из Email
            </p>-->
        </div>
        <form>
            <div>
                <div class="question">
                    E-mail:<Br />
                    <input id="vk_email" class="inputtext" name="vk_email" type="email" required="required" style="width:380px" />
                </div>
                <!--<div class="question">
                    Телефон:<Br />
                    <input id="vk_phone" class="inputtext" name="vk_phone" type="phone" required="required" size="72" />
                </div>-->
                <input type="hidden" id="vk_user_id" name="vk_user_id" />
                <input type="hidden" id="vk_step" name="vk_step" value="1" />
                <br />
            </div>
            <div align="right">
                <input type="submit" value="Продожить" class="light_button">
            </div>
            <!--<button type="button" class="close"> Отмена </button>-->
        </form>
    <br />
 </div>