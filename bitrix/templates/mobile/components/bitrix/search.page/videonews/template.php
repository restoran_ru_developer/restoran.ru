<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page">
<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>
<?if(count($arResult["SEARCH"])>0):?>
        <h1><?=GetMessage("SR_TITLE")?></h1>
	<?foreach($arResult["SEARCH"] as $key=>$arItem):?>
		<!--<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
		<p><?echo $arItem["BODY_FORMATED"]?></p>
		<small><?=GetMessage("SEARCH_MODIFIED")?> <?=$arItem["DATE_CHANGE"]?></small><br /><?
		if($arItem["CHAIN_PATH"]):?>
			<small><?=GetMessage("SEARCH_PATH")?>&nbsp;<?=$arItem["CHAIN_PATH"]?></small><?
		endif;
		?><hr />-->
                <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
                    <?if ($arItem["ELEMENT"]["PROPERTIES"]["VIDEO"]["VALUE"]["path"]):?>
                    <div class="image">
                        <a class="myPlayer" href="http://<?=SITE_SERVER_NAME.$arItem["ELEMENT"]["PROPERTIES"]["VIDEO"]["VALUE"]["path"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                }
                            });
                        </script>
                        <br />
                    </div>
                    <?endif;?>                    
                    <div class="title">
                        <a href="<?=$arItem["URL"]?>"><?=$arItem["ELEMENT"]["NAME"]?></a>
                    </div>
                    <p>
                        <span class="datedate"><?=$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_DAY"]."</span> ".$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_MONTH"]." ".$arItem["ELEMENT"]["DISPLAY_ACTIVE_FROM_YEAR"]?>
                    </p>
                    <div class="clear"></div>
                </div>
	<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"];?>
        <div class="clear"></div>
        <?if(count($arResult["SEARCH"])>=3):?>
            <div align="right"><a href="#" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?></a></div>
        <?endif;?>
        <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    global $search_result;
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N" ||!$search_result)
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                 
</div>
