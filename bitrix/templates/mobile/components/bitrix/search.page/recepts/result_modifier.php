<?
CModule::IncludeModule("iblock");
$arReviewsIB = getArIblock("reviews", CITY_ID);
global $search;
if (is_array($arResult["SEARCH"])):?>
    <?//v_dump($arResult["SEARCH"])?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $search = 1;
        //$el[] = substr($arItem["ITEM_ID"], 1);
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($el = $res->GetNext())
        {
            $db_props = CIBlockElement::GetProperty($el["IBLOCK_ID"], $el["ID"], array(), Array("CODE"=>"ratio"));
            if($ar_props = $db_props->Fetch())
                    $ratio = IntVal($ar_props["VALUE"]);
            $arItem["ELEMENT"] = $el;
            $arItem["ELEMENT"]["RATIO"] = $ratio;
            //$arItem["ELEMENT"]["DETAIL_PAGE_URL"] = "/content/cookery/".RestIBlock::getCookerySection($el["IBLOCK_SECTION_ID"])."/".$el["CODE"]."/";

            //if ($el["DETAIL_PICTURE"])
            //    $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
            if ($el["PREVIEW_PICTURE"])
            {
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true, Array());
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"];
            }
            else
            {
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 153), BX_RESIZE_IMAGE_EXACT, true, Array());
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"];
            }
            $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["PREVIEW_TEXT"]), 200);
            if (!$arItem["ELEMENT"]["PREVIEW_TEXT"])
                $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["DETAIL_TEXT"]), 200);
            
         }
    endforeach;
    ?>
<?endif;?>
