<?
if (!CModule::IncludeModule("catalog"))
    return false;
if (!CModule::IncludeModule("sale"))
    return false;
$arReviewsIB = getArIblock("reviews", CITY_ID);
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($obElement = $res->GetNextElement())
        {
            $el = array();
            
            $el = $obElement->GetFields();
            //v_dump($el);
            $el["PROPERTIES"] = $obElement->GetProperties();
            //foreach ($el["PROPERTIES"] as $key=>$prop)
            //    $el["DISPLAY_PROPERTIES"][$key] = CIBlockFormatProperties::GetDisplayValue($el, $prop, "news_out");
            $arItem["ELEMENT"] = $el;
            if (is_array($el["PREVIEW_PICTURE"]))
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["PREVIEW_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            else
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);           
            
            $arItem["ELEMENT"]["PRICE"] = CPrice::GetBasePrice($arItem["ITEM_ID"]);
            $arItem["ELEMENT"]["PRICE"]["PRICE"] = sprintf("%01.0f", $arItem["ELEMENT"]["PRICE"]["PRICE"]); 
         }
    endforeach;?>
<?endif;?>
