<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page kupons">
    <?if(count($arResult["SEARCH"])>0):?>
            <h1><?=GetMessage("SR_TITLE")?></h1>
            <?foreach($arResult["SEARCH"] as $key=>$arItem):?>
                    <!--<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
                    <p><?echo $arItem["BODY_FORMATED"]?></p>
                    <small><?=GetMessage("SEARCH_MODIFIED")?> <?=$arItem["DATE_CHANGE"]?></small><br /><?
                    if($arItem["CHAIN_PATH"]):?>
                            <small><?=GetMessage("SEARCH_PATH")?>&nbsp;<?=$arItem["CHAIN_PATH"]?></small><?
                    endif;
                    ?><hr />-->
                    <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
                        <?if ($arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"]):?>
                        <div class="image">
                            <a href="<?=$arItem["URL"]?>"><img src="<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"]?>" width="232" height="153" /></a><br />
                        </div>
                        <?endif;?>
                        <div class="title">
                            <a href="<?=$arItem["URL"]?>"><?=$arItem["ELEMENT"]["NAME"]?></a>
                        </div>
                        <div class="rating">
                            <?for($i = 1; $i <= 5; $i++):?>
                                <div class="small_star<?if($i <= round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"])):?>_a<?endif?>" alt="<?=$i?>"></div>
                            <?endfor?>
                            <div class="clear"></div>
                        </div>
                        <?//v_dump($arItem["ELEMENT"]["DISPLAY_PROPERTIES"])?>
                        <div class="price">
                            <?=GetMessage("R_SALE")?><span> <?=$arItem["ELEMENT"]["PROPERTIES"]["sale"]["VALUE"]?>%</span> <?=GetMessage("R_ZA")?> <span><?=$arItem["ELEMENT"]["PRICE"]["PRICE"]?></span> <span class="rouble">e</span>
                        </div>  
                        <div class="clear"></div>
                    </div>
            <?endforeach;?>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&& count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]) echo '<div class="clear"></div><div align="right">'.$arResult["NAV_STRING"].'</div>'?>
            <div class="clear"></div>
            <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div align="right"><a href="<?=$APPLICATION->GetCurPageParam("search_in=kupons", array("search_in","x","y")); ?>" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?></a></div>
            <?endif;?>
                    <?
        global $search_result;
        $search_result++;
        ?>            
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>        
</div>