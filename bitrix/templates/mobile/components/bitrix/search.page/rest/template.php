<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="search-page">
    <input type="hidden" id="q" value="<?= $_REQUEST["q"] ?>">
    <input type="hidden" id="processing" value="0">
    <input type="hidden" id="maxpage" value="<?= $arResult["NAV_RESULT"]->NavPageCount ?>">
    <? if (isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
        ?>
    <? endif;
    if (count($arResult["SEARCH"]) == 1){
        LocalRedirect('/mobile/detail.php?RESTOURANT='.$arResult["SEARCH"][0]["ELEMENT"]["CODE"]);
    }
    if (count($arResult["SEARCH"]) > 0): ?>
        <?
        foreach ($arResult["SEARCH"] as $key => $arItem):
            $address = explode(', ', $arItem["ELEMENT"]["PROPERTIES"]['address']['VALUE'][0]);
            unset($address[0]);
            $address = implode(', ', $address);
            ?>
            <div class="restaurant-block">
                <a class="block-link" data-ajax="true" href="/mobile/detail.php?RESTOURANT=<?= $arItem["ELEMENT"]["CODE"] ?>"></a>
                <div class="image-container">
                    <a data-ajax="true" href="/mobile/detail.php?RESTOURANT=<?= $arItem["ELEMENT"]["CODE"] ?>"><img src="<?= $arItem["ELEMENT"]["PREVIEW_PICTURE"] ?>" alt="<?= $arItem["ELEMENT"]["NAME"] ?>" title="<?= $arItem["ELEMENT"]["NAME"] ?>" /></a>
                </div>
                <div class="title-container">
                    <a data-ajax="true" href="/mobile/detail.php?RESTOURANT=<?= $arItem["ELEMENT"]["CODE"] ?>"><?= $arItem["ELEMENT"]["NAME"] ?><? if (substr_count($arItem["URL"], "restaurants")):
                ?>
                            [<?= GetMessage("R_RESTORAN") ?>]
                        <? elseif (substr_count($arItem["URL"], "banket")): ?>
                            [<?= GetMessage("R_BANKET") ?>]
                        <? endif; ?></a>
                    <div class="rating-container">
                        <? for ($i = 1; $i <= round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"]); $i++): ?>
                            <div class="star-good-small"></div>
                        <? endfor ?>
                        <? for ($i = round($arItem["ELEMENT"]["PROPERTIES"]["RATIO"]["VALUE"]); $i <= 4; $i++): ?>
                            <div class="star-bad-small"></div>
                        <? endfor ?>
                    </div>
                    <div class="address">
                        <?= $address ?>
                    </div>
                    <!--<div class="address">
                        Кухня: <?= implode(", ", $arItem["ELEMENT"]["DISPLAY_PROPERTIES"]["kitchen"]["DISPLAY_VALUE"]) ?>
                    </div>-->
                </div>
            </div>
            <div class="clear"></div>
            <div class="new-line"></div>
            <div class="clear"></div>
        <? endforeach; ?>

        <div class="clear"></div>
        <div class="rest-ajax"></div>
        <div class="ajax-container hidden">
            <div class="loading2">
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>             
                <div></div>         
            </div>
        </div>

        <div class="clear"></div>
        <div class="to-top-link-container" onclick="$('html,body').animate({scrollTop: 0 },'slow');">
            <div class="to-top-link">Наверх</div>
        </div>
        <div class="clear"></div>
        <?
        global $search_result;
        $search_result++;
    endif;
    ?>                
</div>
