<?
$arFormatProps = Array(
    "subway", "kitchen", "type", "average_bill"
);
CModule::IncludeModule("iblock");
global $TopSpec;
foreach($arResult["ITEMS"] as $cell=>$arItem) {
    $TopSpec[] = $arItem["ID"];
    // resize images
    foreach($arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["VALUE"] as $pKey=>$photoID) {
        $arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["DISPLAY_VALUE"][$pKey] = CFile::GetFileArray($photoID);
        //$arResult["ITEMS"][$cell]["PROPERTIES"]["photos"]["DISPLAY_VALUE"][$pKey] = CFile::ResizeImageGet($photoID, array('width' => 397, 'height' => 271), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array());
    }
    if($arParams["PREVIEW_TRUNCATE_LEN"]>0)
    {
            $end_pos = $arParams["PREVIEW_TRUNCATE_LEN"];
            $arItem["PREVIEW_TEXT"] = strip_tags($arItem["~DETAIL_TEXT"]);
            while(substr($arItem["PREVIEW_TEXT"],$end_pos,1)!=" " && $end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $end_pos++;
            if($end_pos<strlen($arItem["PREVIEW_TEXT"]))
                    $arItem["PREVIEW_TEXT"] = substr($arItem["PREVIEW_TEXT"], 0, $end_pos)."...";
            $arResult["ITEMS"][$cell]["PREVIEW_TEXT"] = $arItem["PREVIEW_TEXT"];
    }
    if ($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']["ID"], array('width'=>100, 'height'=>73), BX_RESIZE_IMAGE_EXACT, true, Array());
    }
    else
    {
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']["ID"], array('width'=>100, 'height'=>73), BX_RESIZE_IMAGE_EXACT, true, Array());
    }
    if(!$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PROPERTIES"]["photos"]["VALUE"][0], array('width'=>100, 'height'=>73), BX_RESIZE_IMAGE_EXACT, true, Array());        
    if(!$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"])
        $arResult["ITEMS"][$cell]["PREVIEW_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    
    // remove links from subway name
    foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty) {
        if($arProperty["DISPLAY_VALUE"])
        {
            // remove links from subway name
            if(in_array($pid, $arFormatProps)) {
                if(is_array($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"])) {
                    foreach($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] as $key=>$subway) {
                        $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"][$key]);
                    }
                } else {
                    $arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"] = strip_tags($arResult["ITEMS"][$cell]["DISPLAY_PROPERTIES"][$pid]["DISPLAY_VALUE"]);
                }
            }
        }
    }
}
?>