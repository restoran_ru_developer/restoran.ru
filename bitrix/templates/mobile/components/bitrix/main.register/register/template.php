<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


if ($arResult["VALUES"]["PERSONAL_CITY"] == "")
    $arResult["VALUES"]["PERSONAL_CITY"] = getCityByIP();
?>

<div class="register_form">
    <h1><?= GetMessage("AUTH_REGISTER") ?></h1>
    <? if ($USER->IsAuthorized()): ?>
        <p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>
        <div style="margin-bottom:400px;"></div>
    <? else: ?>
        <?
        if (count($arResult["ERRORS"]) > 0):
            foreach ($arResult["ERRORS"] as $key => $error)
                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
            ShowError(implode("<br />", $arResult["ERRORS"]));

        elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
            ?>
                                                                        <!--<p><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>-->
        <? endif ?>

        <form id="formm" method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" enctype="multipart/form-data">
            <? if ($arResult["BACKURL"] <> ''): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
            <? endif; ?>
            <div class="question-block">
                <div class="question-input">
                    <input  type="text" class="placeholder" name="REGISTER[LAST_NAME]" placeholder="Иванов" value="<?= $_REQUEST["REGISTER"]["LAST_NAME"] ?>" autocomplete="off" data-theme="b" />
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_LAST_NAME") ?>:</div>
            </div>
            <div class="question-block">
                <div class="question-input">
                    <input  type="text" class="placeholder" name="REGISTER[NAME]" placeholder="Иван" value="<?= $_REQUEST["REGISTER"]["NAME"] ?>" autocomplete="off" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_NAME") ?>:</div>
            </div>
            <div class="question-block">
                <div class="question-input">
                    <input  type="text" class="placeholder" id="nicknameinput" name="REGISTER[PERSONAL_PROFESSION]" onkeyup="nickChanged();" placeholder="nickname" value="<?= $_REQUEST["REGISTER"]["PERSONAL_PROFESSION"] ?>" autocomplete="off" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_PERSONAL_PROFESSION") ?>:</div>
            </div>
            <div class="question-block">
                <div class="question-input">
                    <input type="text" class="placeholder phone" name="REGISTER[PERSONAL_PHONE]" value="<?= $_REQUEST["REGISTER"]["PERSONAL_PHONE"] ?>" autocomplete="off" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_PERSONAL_PHONE") ?>:</div>
            </div>    
            <div class="question-block">
                <div class="question-input">
                    <input id="EMAIL" type="text" class="placeholder" name="REGISTER[EMAIL]" <?= ($_REQUEST["invite_email"]) ? "readonly='readonly'" : "" ?> value="<?= ($_REQUEST["invite_email"]) ? $_REQUEST["invite_email"] : $arResult["VALUES"]["EMAIL"] ?>"  autocomplete="off" data-theme="b"/>
                    <input id="LOGIN" type="hidden" class="placeholder" name="REGISTER[LOGIN]" value="<?= $_REQUEST["REGISTER"]["LOGIN"] ?>"  autocomplete="off" data-theme="b"/>
                    <input type="hidden" name="invite" value="1" />
                    <? if ($_REQUEST["co"]): ?>
                        <input type="hidden" name="REGISTER[PERSONAL_PAGER]" value="<?= $_REQUEST["co"] ?>" />
                    <? endif; ?>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_EMAIL") ?>:</div>
            </div> 

            <div class="question-block">
                <div class="question-input">
                    <input type="text" class="placeholder" name="REGISTER[PERSONAL_CITY]" value="<?= $_REQUEST["REGISTER"]["PERSONAL_CITY"] ?>"  autocomplete="off" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_PERSONAL_CITY") ?>:</div>
            </div>  
            <div class="question-block">
                <div class="question-input">
                    <input type="password" class="placeholder" name="REGISTER[PASSWORD]"  autocomplete="off" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_PASSWORD") ?>:</div>
            </div> 
            <div class="question-block">
                <div class="question-input">
                    <input type="password" class="placeholder" name="REGISTER[CONFIRM_PASSWORD]"   autocomplete="off" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD") ?>:</div>
            </div>

            <div class="clear"></div>

            <!--<div class="question">
            <?= GetMessage("REGISTER_FIELD_PODT") ?>: <a href="javascript:void(0)" val="sms" class="sms active no_border"><i><?= GetMessage("USER_SMS") ?></i></a> / <a class="sms no_border" val="email" href="javascript:void(0)"><i><?= GetMessage("USER_EMAIL") ?></i></a>
                <input type="hidden" id="sms" name="REGISTER[SMS]" value="email"/>
            </div>-->
            <input type="hidden" id="sms" name="REGISTER[SMS]" value="email"/>
            <div class="clear"></div>
            <div class="center-text"><?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>:</div>
            <div class="clear"></div>
            <? if ($arResult["USE_CAPTCHA"] == "Y"): ?>
                <div class="question-block">
                    <div class="question-input30">
                        <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>" />
                        <input class="placeholder" type="text" name="captcha_word" maxlength="10" value="" size="15" data-theme="b" />
                    </div>
                    <div class="question-label"><img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" align="left" class="captcha" /></div>
                </div>
            <? endif; ?>

            <div class="clear"></div>
            <div class="question-block">
                <div class="question-input">
                    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" style="width:100%;">
                        <input type="radio" name="REGISTER[PERSONAL_GENDER]" id="form_checkbox_smokea" value="M" data-theme="d">
                        <label for="form_checkbox_smokea"><?= GetMessage("USER_MALE") ?></label>
                        <input type="radio" name="REGISTER[PERSONAL_GENDER]" id="form_checkbox_smokeb" value="F" data-theme="d">
                        <label for="form_checkbox_smokeb"><?= GetMessage("USER_FEMALE") ?></label>
                    </fieldset>
                </div>
                <div class="question-label" style="line-height: 40px;"><?= GetMessage("REGISTER_FIELD_PERSONAL_GENDER") ?>:</div>
            </div>
            <div class="clear"></div>
            <div class="question-block">
                <div class="question-input50">
                    <input type="date" class="datew placeholder" name="REGISTER[PERSONAL_BIRTHDAY]" value="<?= $_REQUEST["REGISTER"]["PERSONAL_BIRTHDAY"] ?>" size="25" data-theme="b"/>
                </div>
                <div class="question-label"><?= GetMessage("REGISTER_FIELD_PERSONAL_BIRTHDAY") ?>:</div>
            </div>
            <div class="clear"></div>
            <a id="link_agreement" href="/mobile/auth/user_license_agreement.php" data-theme="e">Прочтите условия пользовательского соглашения</a>
            <div class="clear"></div>
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <div class="ui-checkbox">
                                <input type="checkbox" value="Y" name="agreement" id="USER_REMEMBER" data-mini="true" data-theme="c">
                                <label for="USER_REMEMBER" data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="span" data-icon="checkbox-off" data-theme="c" data-mini="true" class="ui-checkbox-off ui-btn ui-btn-up-c ui-btn-corner-all ui-mini ui-btn-icon-left">Я принимаю условия</label>
                                    
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table> 
            <div class="clear"></div>

            <div align="right">
                <input class="light_button disabled" id="register_submit_button" type="submit" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>" />
            </div>


        </form>
    <? endif ?>
</div>
