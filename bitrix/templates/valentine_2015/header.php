<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <meta name="author" content="Restoran.ru">    
        <meta property="fb:app_id" content="297181676964377" />    
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">         
        <link href="http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/multiselect.css"  type="text/css" rel="stylesheet" />        
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->    
<!--        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>       -->

        <link href='http://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>

        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-multiselect.js')?>        
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?> 
        <?$APPLICATION->ShowHead()?>
        <?
        CModule::IncludeModule("advertising");
        CAdvBanner::SetRequiredKeywords(array(CITY_ID));
        ?>
    </head>
    <body style="background:#daf0dd url(<?=SITE_TEMPLATE_PATH?>/img/valentine_bg.jpg) center top no-repeat;">               
        <div id="panel"><? $APPLICATION->ShowPanel() ?></div>
        <div class="container">     
            <div class="bg1" style="background: url(<?=SITE_TEMPLATE_PATH?>/img/valentine_bg_left.png) left top no-repeat"></div>            
            <div class="bg2" style="background: url(<?=SITE_TEMPLATE_PATH?>/img/valentine_bg_right.png) right top no-repeat; z-index:1"></div>            
            <div class="t_b_980">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "top_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
            <div class="page-header block" style="background: url(<?=SITE_TEMPLATE_PATH?>/img/valentine_bg_heart.png) 646px 50px no-repeat">
                <div class="heart" style="background: url(<?=SITE_TEMPLATE_PATH?>/img/valentine_bg_center.png) center center no-repeat;  z-index:2; position:relative;">                    
                    <a class="new_year_night_logo" href="/<?=CITY_ID?>/articles/valentine/"></a>
                    <a class="restoran_logo" href="/"></a>
                    <h1><?=$APPLICATION->ShowTitle("h1");?></h1>
                </div>
            </div>
            <div class="filter-box block" style="margin-top:20px;">
                <noindex>
                    <form class="form-inline" role="form" action="/<?=CITY_ID?>/articles/valentine/" method="get" id="rest_filter_form">                
                        <div class="form-group">
                            <label for="name">Название ресторана</label>
                            <input type="text" class="form-control" id="name" name="arrFilter_ff[NAME]" value="<?=$_REQUEST["arrFilter_ff"]["NAME"]?>" placeholder="Название ресторана">
                        </div>
                        <?                    
                        $_REQUEST["SECTION_CODE"] = 'valentine';
                        $arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");                
                        CModule::IncludeModule("iblock");
                        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arIB["ID"]));
                        while ($prop_fields = $properties->Fetch())
                        {
                            if ($prop_fields["CODE"]!="RESTORAN"&&$prop_fields["CODE"]!="offers")
                                $filter[] = $prop_fields["CODE"];                        
                        }                    
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter", "restoran", 
                            Array(
                                "IBLOCK_TYPE" => "special_project",
                                "IBLOCK_ID" => $arIB["ID"],
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array(),   
                                "PROPERTY_CODE" => $filter,
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N"
                            )
                        );
                        ?><script type="text/javascript">
//                            $(document).ready(function() {
//                                $('.multiselect').multiselect({
//                                    numberDisplayed:1,
//                                    nonSelectedText: '',
//                                    nSelectedText: "Выбрано",   
//                                    maxHeight: 250,
//                                    buttonWidth: "140px"
//                                });
//                            });
                        </script>                                       
                    </form>  
                </noindex>
            </div>
            <div class="content block">                                 