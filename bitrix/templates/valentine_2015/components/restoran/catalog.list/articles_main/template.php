<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>                  
<div id="spec_block_list">
    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
        <div class="spec" onclick="location.href='<?= $arItem["DETAIL_PAGE_URL"] ?>'" style="background:url('<?= $arItem["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat">                             
                <div class="right spec_grad">                                                        
                    <a class="title" href="<?= $arItem["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>">
                        <?= $arItem["NAME"] ?>
                    </a>                    
                    <div class="text">                                                      
                        <?if ($arItem["adres"]):?>
                            <?=$arItem["adres"]?>
                        <?endif;?>                
                            <Br/>
                        <?if ($arItem["subway"]):?>
                            <p class="subway-icon"><?=$arItem["subway"]?></p>
                        <?endif;?>                
                        <Br />                        
                        <?if ($arItem["PREVIEW_TEXT"]):?>
                            <p><?=$arItem["PREVIEW_TEXT"]?></p>
                        <?endif;?>
                    </div>    
<!--                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="btn btn-nb-empty">Подробнее</a>-->
                </div>            
                <div class="clear"></div>               
        </div>
    <? endforeach; ?>    
    <? /*if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <div class="navigation">
            <div class="pages"><?= $arResult["NAV_STRING"] ?></div>
        </div>
    <? endif; */?>
</div>