<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
    $(function(){
        $(".articles_photo").galery();
    });
</script>
<div id="detail_rest">       
        <h1>День Святого Валентина в ресторане "<?=HTMLToTxt($arResult["~NAME"])?>"</h1>
        <?if($arResult["PREVIEW_TEXT"]):?>
            <i><?=$arResult["PREVIEW_TEXT"]?></i>
            <br />
        <?endif;?>
        <?=$arResult["DETAIL_TEXT"]?>
        <div class="clear"></div>        
        <div class="statya_section_name">            
            <?if ($arResult["PREV_ARTICLE"]):?>
                <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                    <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                </div>
            <?endif;?>
            <?if ($arResult["NEXT_ARTICLE"]):?>
                <div class="right">
                    <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                </div>
            <?endif;?>
            <div class="clear"></div>            
        </div>
</div>