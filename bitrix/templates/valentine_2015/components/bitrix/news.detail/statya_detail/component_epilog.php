<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$arResult["NAME"] = preg_replace("/&lt;br[ \/]+&gt;[0-9A-zА-ярР .]+/"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br/>"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("<br />"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br /&gt;"," ",$arResult["NAME"]);
$arResult["NAME"] = str_replace("&lt;br/&gt;"," ",$arResult["NAME"]);

$APPLICATION->SetTitle('День Святого Валентина в ресторане '.$arResult["NAME"]);
$APPLICATION->SetPageProperty("description", 'Предложения ресторана '.$arResult["NAME"].' в день Святого Валентина');
$APPLICATION->SetPageProperty("keywords",  'день, святого, валентина, ресторан, '.$arResult["NAME"]);

?>