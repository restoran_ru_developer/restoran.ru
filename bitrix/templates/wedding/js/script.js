jQuery(document).ready(function() {
    // ajax loader
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/144.gif" align="left"> Загрузка...</div>');
    /*$("#system_loading").css("left", $(window).width() / 2 - 104 / 2 + "px");
    $("#system_loading").css("top", $(window).height() / 2 - 104 / 2 + "px");*/
    $("#system_loading").bind("ajaxSend",
        function() {
            $(this).show();
        }).bind("ajaxComplete", function() {
            $(this).hide();
        });

    var params = {
        changedEl:"#search_in",
        visRows:5
        /*scrollArrows: true*/
    }
    cuSel(params);
    $("ul.tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
            $(this).tabs("div.panes > .pane", {                
                effect: 'fade',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        else
            $(this).tabs("div.panes > .pane",{effect: 'fade'});
    });    
    $("ul#poster_tabs").tabs("div.poster_panes > .poster_pane");
    $(".to_top").on('click', function() {
        $('html,body').animate({scrollTop:"0"}, 500);
    });    
        var SoapBubbleMachineNumber1 = $('fn').BubbleEngine({
          particleSizeMin:            10,
          particleSizeMax:            20,
          particleSourceX:            $(window).width()-20,
          particleSourceY:            300,
          particleAnimationDuration:  3000,
          particleDirection:          'left',
          particleAnimationVariance:  5000,
          particleScatteringX:        $(window).width()/1.5,
          particleScatteringY:        $(window).height()/2,
          imgSource:                  '/bitrix/templates/wedding/js/bubles/images/heart.png',
          gravity:                    0
        });
        var SoapBubbleMachineNumber2 = $('fn').BubbleEngine({
          particleSizeMin:            10,
          particleSizeMax:            40,
          particleSourceX:            0,
          particleSourceY:            300,
          particleAnimationDuration:  3000,
          particleDirection:          'right',
          particleAnimationVariance:  5000,
          particleScatteringX:         $(window).width()/1.2,
          particleScatteringY:        $(window).height()/2,
          imgSource:                  '/bitrix/templates/wedding/js/bubles/images/bubble.png',
          gravity:                    0
        });
        //SoapBubbleMachineNumber1.addBubbles(10);
        //SoapBubbleMachineNumber2.addBubbles(30);
});

function showExpose() {
    $(document).mask({ color: '#1a1a1a', loadSpeed: 200, closeSpeed:0, opacity: 0.5, closeOnEsc: false, closeOnClick: false });
}
function closeExpose() {
    $.mask.close();
}

function send_ajax(url, dataType, params, successFunc) {
    //showExpose();
    if(!dataType)
        dataType = 'json';
    params = eval('(' + params + ')');
    $.ajax({
        url:url,
        type: 'post',
        dataType: dataType,
        data: params,
        statusCode: {
            404:function() {
                alert('Page not found');
            }
        },
        success:function(data) {
            if (successFunc)
                successFunc(data);
            //closeExpose();
        }
    });
}

function sumbit_form(form, beforeFunc,successFunc) {
    var params = $(form).serialize();
    var url = $(form).attr("action");
    $.ajax({
        url:url,
        type:'post',
        data:params,
        statusCode:{
            404:function() {
                alert('Page not found');
            }
        },
        beforeSend:function() {
            /*if (typeof(before_submit_form) == "function")
                before_submit_form(form);
            return false;*/
            if (beforeFunc)
                beforeFunc(form);
        },
        success:function(data) {
            $(form).parent().html(data);
            /*if (typeof(success_submit_form) == "function")
                success_submit_form();*/
            if (successFunc)
                successFunc(form);
        }
    });
    return false;
}

function show_popup(obj,options)
{
    if(!options)
        options = {"obj":$(obj).parent().attr("id")};
    else
        options.obj = $(obj).parent().attr("id");    
    if (!$(obj).parent().find("div").hasClass("popup"))
        $("<div>").addClass("popup").appendTo($(obj).parent()).popup(options);
}