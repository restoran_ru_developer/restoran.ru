<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript("/tpl/js/jquery.checkbox.js")?>
<noindex>
<form action="/<?=CITY_ID?>/articles/wedding/" method="get" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search">
            <div class="filter_box">
            <?
            $p=0;
            $ar_cusel = Array(2,3,1);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
                if ($arProp["PROPERTY_TYPE"]!="L"):?>
                <div class="filter_block" id="filter<?=$prop_id?>">
                    <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                    <div class="title"><a class="another" href="javascript:void(0)"><?=$arProp["NAME"]?></a></div>                                      
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"right":"70px","top":"2px", "width":"355px"}});
                            var params = {
                                changedEl: "#multi<?=$ar_cusel[$p]?>",
                                scrollArrows: true,
                                visRows:10
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                        <div class="title" align="right"><?=$arProp["NAME"]?></div>
                        <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="10">
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>        
                <?$p++;
                else:?>
                    <table cellpadding="0" cellspacing="0">                                                                        
                        <tr class="select_all">
                            <td style="padding: 5px 0px;padding-right:5px"><span class="niceCheck" style="background-position: 0px 0px; "><input name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>]" type="checkbox" value="Да" <?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]=="Да")?"checked":""?> name="" id=""></span></td>
                            <td class="option2"><label class="niceCheckLabel"><?=$arProp["NAME"]?></label></td>                                       
                        </tr>
                    </table>
                <?endif;          
            }
            ?>      
            <br /><Br />
            <input style="width:128px" class="new_filter_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
        </div>
    </div>
    <div class="clear"></div>
</form>
</noindex>