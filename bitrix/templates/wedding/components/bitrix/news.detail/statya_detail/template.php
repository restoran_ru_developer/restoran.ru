<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/templates/main/js/flowplayer.js"></script>
<script>
$(document).ready(function(){
   flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
        clip: {
                autoPlay: false, 
                autoBuffering: true,
                accelerated: true                
        }
    });
    $(".articles_photo").galery(); 
});
</script>
<script>
$(document).ready(function(){
    $(".photos123").each(function(){
       var wid = $(this).width();
       //var hei = $(this).height();
       $(this).append("<div class='block'>Увеличить</div>");
       $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
    });
    $(".photos123").hover(function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        console.log(wid);
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"}); */
        $(this).find(".block").show();
    },
    function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"});*/
        $(this).find(".block").hide();
    });
    $(".photos123 .block").toggle(function(){
        $(this).parent().find("img:hidden").show().prev().hide();        
        $(this).parent().removeClass('left');        
        $(this).html("Уменьшить");
        $(this).parent().css("width","728px");
        var wid = $(this).parent().width();
        //var hei = $(this).parent().height();
        $(this).css({"left":wid/2-50+"px"});
    },
    function(){
        $(this).parent().find("img:hidden").show().next().hide();
        $(this).parent().addClass('left');        
        $(this).html("Увеличить");
        $(this).parent().css("width","238px");
        var wid = $(this).parent().width();
        //var hei = $(this).parent().height();
        $(this).css({"left":wid/2-50+"px"});
    });    
})
</script>  
<?//$resto = array();?>
<?
//global $resto;
?>
        <div class="statya_section_name">
            <?$temp = explode("-",$arResult["IBLOCK"]["NAME"]);?>            
            <h4><?=$temp[0]?></h4>
            <div class="statya_nav">
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1>Свадебный переполох в ресторане "<?=$arResult["~NAME"]?>"</h1>
        <a class="another no_border" href="/users/id<?=$arResult["CREATED_BY"]?>/"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <?if($arResult["PREVIEW_TEXT"]):?>
            <i><?=$arResult["PREVIEW_TEXT"]?></i>
            <br />
        <?endif;?>
        <?=$arResult["DETAIL_TEXT"]?>
            <div class="clear"></div>
            <br />
        <?/*?>
        <p><?=GetMessage("TAGS_TITLE")?>: <a href="#" class="another">название тега 1</a>, <a href="#" class="another">название тега 2</a>, <a href="#" class="another">название тега 3</a></p>
        <?*/?>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#comments" class="anchor another" onclick="return false;"><?=$arResult["PROPERTIES"]["COMMENTS"]["VALUE"]?></a>)</div>
            <!--<div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <div class="right" style="padding-right:15px;"><input type="button" class="subscribe-user2user button" id="subscri<?=$arResult["CREATED_BY"]?>" value="<?=GetMessage("SUBSCRIBE")?>" /></div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
            <div class="clear"></div>
        </div>
        <?if(count($resto)):?>
            <div id="order" style="width:700px">
                <div class="left">
                    <script>

                        $(document).ready(function(){
                            var params = {
                                changedEl: "#order_what",
                                visRows: 5
                            }
                            cuSel(params);
                            var params = {
                                changedEl: "#order_many",
                                visRows: 5
                            }
                            cuSel(params);
                            var params = {
                                changedEl: "#rest",
                                visRows: 5
                            }
                            cuSel(params);
                            $.tools.dateinput.localize("ru",  {
                            months:        '<?=GetMessage("MONTHS_FULL")?>',
                            shortMonths:   '<?=GetMessage("MONTHS_SHORT")?>',
                            days:          '<?=GetMessage("WEEK_DAYS_FULL")?>',
                            shortDays:     '<?=GetMessage("WEEK_DAYS_SHORT")?>'
                            });
                            $(".whose_date").dateinput({lang: 'ru', firstDay: 1 , format:"dd/mm/yy",trigger:true, css: { trigger: 'caltrigger'}, change: function(e, date)  {
                                $(".caltrigger").html(this.getValue("dd mmmm"));
                            } });
                        $(".caltrigger").html("<?=$arResult["TODAY_DATE"]?>");
                        $.maski.definitions['~']='[0-2]';
                        $.maski.definitions['!']='[0-5]';
                        $(".time").maski("~9:!9");
                        });
                    </script>
                    Бронирование
                    <select id="order_what" name="order_what">
                        <option selected="selected" value="1">столика</option>
                        <option  value="2">банкета</option>
                        <option value="3">фуршета</option>
                    </select>
                    в ресторане 
                    <select id="rest" name="rest">
                        <?foreach($resto as $res):?>
                            <option selected="selected" value="<?=$res["ID"]?>"><?=$res["NAME"]?></option>
                        <?endforeach;?>
                    </select>
                    <br />
                    <span style="position: relative; display: inline">
                        <input class="whose_date" style="" type="date" style="visibility:hidden" />
                    </span>
                    в <input class="time" type="text" size="4" value="1230"/> на
                    <select id="order_many" name="order_many">
                        <option selected="2" value="2">2</option>
                        <option  value="4">4</option>
                        <option value="6">6</option>
                        <option value="8">8</option>
                    </select>
                    персоны
                    <div class="phone">
                        <sub>Заказ столика или банкета:<br /> </sub>
                        +7 (495) 988-26-56, +7 (495) 506-00-33
                    </div>
                </div>
                <div class="right">
                    <input type="button" class="dark_button" value="ЗАБРОНИРОВАТЬ" />
                    <div class="coupon">
                        -50% <span>скидка</span><br />
                        <a href="#">Купить купон</a>
                    </div>

                </div>
            </div>
        <?endif;?>
        <br /><br />
        <div id="tabs_block7">
            <ul class="tabs">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block">
                   <?$APPLICATION->IncludeComponent(
                   	"restoran:comments",
                   	"",
                   	Array(
                            "IBLOCK_TYPE" => "comment",
                            "ELEMENT_ID" => $arResult["ID"],
                            "IS_SECTION" => "N"
                   	),
                   false
                   );?>
               </div>
            </div>            
        </div>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2881483, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <hr class="black" />
        <div class="statya_section_name">
            <h4><?=$temp[0]?></h4>
            <div class="statya_nav">
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "content_article_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>       
    