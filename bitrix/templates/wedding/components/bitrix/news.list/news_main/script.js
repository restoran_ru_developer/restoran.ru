$(document).ready(function(){
   $("#spec_news .scroll_arrows .prev").click(function(){
       go2prev_news();
   });
   $("#spec_news .scroll_arrows .next").click(function(){
       go2next_news();
   });
});

function go2next_news()
{
    var a = $("#spec_news .galery_news:visible");
    if (a.next().hasClass("galery_news"))
    {
        a.hide().next().show();
        $("#spec_news  .scroll_num").html($("#spec_news  .scroll_num").html()*1+1);
    }
    else
    {
        $("#spec_news .galery_news:visible").hide();
        $("#spec_news .galery_news:first").show();
        $("#spec_news  .scroll_num").html(1);
    }
}
function go2prev_news()
{
    var a = $("#spec_news .galery_news:visible");
    if (a.prev().hasClass("galery_news"))
    {
        a.hide().prev().show();
        $("#spec_news  .scroll_num").html($("#spec_news  .scroll_num").html()*1-1);
    }
    else
    {
        $("#spec_news .galery_news:visible").hide();
        $("#spec_news .galery_news:last").show();
        $("#spec_news  .scroll_num").html($("#spec_news  .all_num").html());
        
    }
}