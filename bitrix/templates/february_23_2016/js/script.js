$(function(){    
    //Scroll top top (animation?!) [always on page]
    $(".ontop").click(function(){
        //$("html,body").scrollTop(0);
        $('html,body').animate({scrollTop:0},"300");
    });

    $(document).on('click', 'a.booking', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var params = {"id":$(this).data("id"),"name":$(this).data("restoran")};
        $.ajax({
            type: "POST",
            url: $(this).attr("href"),
            data: params,
        })
            .done(function(data) {
                $('#booking_form .modal-body').html(data);
            });
        $('#booking_form .modal-dialog').removeClass("small");
        $('#booking_form').modal('show');
        return false;
    });

    $(window).scroll(function() {
        if ($(this).scrollTop()>200){
            $('.to-top-btn-bg').fadeIn();
        }
        else {
            $('.to-top-btn-bg').fadeOut();
        }
    });

    $(".ontop, .to-top-btn-bg").click(function(){
        //$("html,body").scrollTop(0);
        $('html,body').animate({scrollTop:0},"300");
    });
});