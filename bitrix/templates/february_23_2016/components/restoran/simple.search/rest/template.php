<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div id="spec_block_list">
<?if(count($arResult["ITEMS"])>0):?>
        <h1>Поиск по запросу «<?=$_REQUEST["q"]?>»</h1>
	<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
            <div class="spec left" style="background:url('<?= $arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"] ?>') left top no-repeat" onclick="location.href='<?= $arItem["ELEMENT"]["DETAIL_PAGE_URL"] ?>'"> 
                <div class="title_box">
                    <a class="title <?=(strlen($arItem["NAME"])>23||  substr_count($arItem["NAME"], "br"))?"big":""?>" href="<?= $arItem["ELEMENT"]["DETAIL_PAGE_URL"] ?>" alt="<?= $arItem["ELEMENT"]["NAME"] ?>" title="<?= $arItem["ELEMENT"]["NAME"] ?>">
                        <?=$arItem["ELEMENT"]["~NAME"]?>
                    </a>
                </div>        
            </div>               
            <?if($key%3==2):?><div class="clear"></div><?endif?>
	<?endforeach;?>
        <div class="clear"></div>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&& count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]):?>            
            <div align="right"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>                
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                
</div>