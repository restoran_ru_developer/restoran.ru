<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div id="spec_block_list">
<?if(count($arResult["ITEMS"])>0):?>
        <h1><?=GetMessage("SR_TITLE")?></h1>
	<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
               <div class="spec left<?if($key % 3 == 2):?> end<?endif?>" style="background:url(<?=$arItem["ELEMENT"]["PREVIEW_PICTURE"]["src"]?>) left top no-repeat"> 
                    <a href="<?=$arItem["ELEMENT"]["DETAIL_PAGE_URL"]?>" alt="<?=$arItem["ELEMENT"]["~NAME"]?>" title="<?=$arItem["ELEMENT"]["~NAME"]?>">
                        <div class="dotted_s"></div>        
                        <div class="title_box grad<?=($key%3)?>">
                            <div class="title">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td><?=$arItem["ELEMENT"]["~NAME"]?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>        
                    </a>
                </div>
                <?if($key%3==2):?><div class="clear"></div><?endif?>
	<?endforeach;?>
        <div class="clear"></div>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N"&& count($arResult["SEARCH"]) >= $arParams["PAGE_RESULT_COUNT"]):?>
            <div class="clear"></div>
            <div align="right"><?=$arResult["NAV_STRING"];?></div>
        <?endif;?>
        <?if(count($arResult["SEARCH"])>=3 && $arParams["DISPLAY_BOTTOM_PAGER"] == "N"):?>
            <div align="right"><a href="#" class="uppercase"><?=GetMessage("SR_ALL_ITEMS")?></a></div>
        <?endif;?>
        <?
        global $search_result;
        $search_result++;
        ?>
<?else:
    if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N")
	ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));           
endif;?>                
</div>
