<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <meta name="author" content="Restoran.ru">    
        <meta property="fb:app_id" content="297181676964377" />    
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">         
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="/bitrix/templates/main_2014/css/datepicker.css"  type="text/css" rel="stylesheet" />

        <link href="<?=SITE_TEMPLATE_PATH?>/css/multiselect.css"  type="text/css" rel="stylesheet" />
        <link href="/bitrix/templates/main_2014/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->    
<!--        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.min.js"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>       -->

        <link href='https://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>

        <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-multiselect.js')?>        
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/bootstrap-datepicker.js?141145344014054')?>
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/maskedinput.js?14085357603362')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
        <?$APPLICATION->AddHeadScript('/bitrix/templates/main_2014/js/jquery.autocomplete.min.js?140836218021406')?>
        <?$APPLICATION->ShowHead()?>
        <?
        CModule::IncludeModule("advertising");
        CAdvBanner::SetRequiredKeywords(array(CITY_ID));
        ?>
    </head>
    <body style="background:#ffffff url(<?=SITE_TEMPLATE_PATH?>/img/top-8marta-bg.jpg) center top no-repeat;">
<!--        <div id="panel">--><?// $APPLICATION->ShowPanel() ?><!--</div>-->
        <div class="container">     
            <div class="t_b_980">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "top_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
            <div class="page-header block" style="">
                <a class="mobile-book-phone" href="tel:<?if(CITY_ID=='spb'):?>+78127401820<?else:?>+74959882656<?endif?>"><span></span></a>
                <a class="mobile-book-trigger booking" href="/tpl/ajax/online_order_rest.php">бронировать</a>
                <a class="new_year_night_logo-to-root" href="/"></a>
                <a class="new_year_night_logo" href="/<?=CITY_ID?>/articles/8marta/"></a>

                <div class="phone serif">
                    <a class="serif city">
                        <?if(CITY_ID=='spb'):?>Санкт-Петербург<?else:?>Москва<?endif?>
                    </a>
                    <span class="header-phone-wrap"><?if(CITY_ID=='spb'):?>+7 (812) <span>740 18 20</span><?else:?>+7 (495) <span>988 26 56</span><?endif?></span>
<!--                    <div class="order_link">-->
<!--                        <a href="/tpl/ajax/online_order_rest.php" class="booking">Бронировать <span>Бесплатно</span></a>-->
<!--                    </div>-->
                </div>
                <a href="/tpl/ajax/online_order_rest.php" class="booking pink-booking-btn">Бронировать Бесплатно</a>
            </div>

            <div style="background: #2c2d34;float: left;width: 100%;margin-bottom: 20px;padding-bottom: 20px;">
                <div class="filter-box block" style="    margin-top: 20px;">
                <noindex>
                    <form class="form-inline" role="form" action="/<?=CITY_ID?>/articles/8marta/" method="get" id="rest_filter_form">
                        <div class="form-group">
                            <label for="name">Название ресторана</label>
                            <input type="text" class="form-control" id="name" name="arrFilter_ff[NAME]" value="<?=$_REQUEST["arrFilter_ff"]["NAME"]?>" placeholder="Название ресторана">
                        </div>
                        <?                    
//                        $_REQUEST["SECTION_CODE"] = 'valentine';
                        $arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");
//                        CModule::IncludeModule("iblock");
//                        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arIB["ID"]));
//                        while ($prop_fields = $properties->Fetch())
//                        {
//                            if ($prop_fields["CODE"]!="RESTORAN")
//                                $filter[] = $prop_fields["CODE"];
//                        }
//                        FirePHP::getInstance()->info($filter);

                        $filter = array("bill","subway", "zagorod", 'offers');

                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter", "restoran", 
                            Array(
                                "IBLOCK_TYPE" => "special_project",
                                "IBLOCK_ID" => $arIB["ID"],
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array(),   
                                "PROPERTY_CODE" => $filter,
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "Y",//y
                                "CACHE_TIME" => "36000002",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N"
                            )
                        );
                        ?>
                    </form>  
                </noindex>
            </div>
            </div>
            <div class="content block">                                 