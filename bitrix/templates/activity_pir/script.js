$(function() 
{
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/ajax_loader.gif"></div>');
    $("#system_loading").css("left",$(window).width()/2-104/2+"px");
    $("#system_loading").css("top",$(window).height()/2-104/2+"px"); 
    $("#system_loading").bind("ajaxSend", function(){        
        $(this).show();
    }).bind("ajaxComplete", function(){
        $(this).hide();
    });
    $(".to_top").click(function() {
        $('html,body').animate({scrollTop:"0"}, 500);
    }); 
});
