<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="menu_menu">
<?
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	if($CURRENT_DEPTH<$arSection["DEPTH_LEVEL"])
		echo "<ul>";
	elseif($CURRENT_DEPTH>$arSection["DEPTH_LEVEL"])
		echo str_repeat("</ul>", $CURRENT_DEPTH - $arSection["DEPTH_LEVEL"]);
	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
?>
	<li id="<?=$this->GetEditAreaId($arSection['ID']);?>" <?=($_REQUEST["SECTION_ID"]==$arSection["ID"])?"class='active'":""?>><a href="<?=$arSection["SECTION_PAGE_URL"]?>&PARENT_SECTION_ID=<?=$_REQUEST["PARENT_SECTION_ID"]?>&<?=bitrix_sessid_get()?>"><?=$arSection["NAME"]?></a><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;<sup><?=$arSection["ELEMENT_CNT"]?></sup><?endif;?>
	
	<a href="<?=$templateFolder?>/core.php?act=edit_rzdel&SECTION_ID=<?=$arSection["ID"]?>" class="edite_section"></a>
	<a href="<?=$templateFolder?>/core.php?act=del_rzdel&SECTION_ID=<?=$arSection["ID"]?>" class="del_section"></a>
	</li>
<?endforeach?>
</ul>


<?
//v_dump($_REQUEST["SECTION_ID"]);
//v_dump($arResult["SECTION"]);
?>