<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?=$APPLICATION->ShowProperty("title");?></title>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/script.js")?>
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
</head>
<body>    
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="container">
        <div class="wrap">
        <?if ($APPLICATION->GetCurPage()=="/activities/pir/"||$APPLICATION->GetCurPage()=="/activities/pir/index.php"):?>
            <div id="header">                            
                <div id="icons_block">   
                    <a href="/" style="position:absolute; left:0px; top:-387px; display: block; width:285px; height:100px;"></a>
                    <div id="time">
                        1 — 4 октября 2013 года<br />
                        <a href="#">Москва, «Крокус Экспо»</a>
                    </div>
                    <a class="icon1 icons" href="#">Программа мероприятий</a>
                    <a class="icon2 icons" href="http://pirfood.pir.ru/#scheme" target="_blank">План выставки</a>
                    <a class="icon3 icons" href="/activities/pir_back/news.php">Новости</a>                
                    <a class="icon4 icons" href="#">Фотоотчеты</a>
                </div>
            </div>
        <?else:?>
            <div id="header2">                 
                <div id="icons_block">      
                    <a href="/" style="position:absolute; left:190px; top:-134px; display: block; width:222px; height:100px;"></a>
                    <div id="time">
                        1 — 4 октября 2013 года<br />
                        <a href="#">Москва, «Крокус Экспо»</a>
                    </div>
                    <a class="icon1 icons" href="#">Программа мероприятий</a>
                    <a class="icon2 icons" href="http://pirfood.pir.ru/#scheme" target="_blank">План выставки</a>
                    <a class="icon3 icons" href="/activities/pir_back/news.php">Новости</a>                
                    <a class="icon4 icons" href="#">Фотоотчеты</a>
                </div>
            </div>
        <?endif;?>
        <div id="content">
            <div class="left" style="width: 220px;"> 
                <h1><?=$APPLICATION->ShowTitle(false);?></h1>
                <br />
                <a href="http://pirexpo.com/ticket/" taget="_blank"><img src="<?=SITE_TEMPLATE_PATH?>/images/ticket.png" width="260" /></a>
            </div>
            <div class="right" style="width: 700px;">