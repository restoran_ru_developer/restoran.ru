<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--    <div>-->
<?if (count($arResult["ITEMS"])>0):?>
    <?foreach($arResult["ITEMS"] as $key=>$arItem):
        ?>
        <div class="pull-left" lat="<?=$arItem['PROPERTIES']['lat']['VALUE']?>" lon="<?=$arItem['PROPERTIES']['lon']['VALUE']?>" this-rest-id="<?=$arItem['PROPERTIES2']['RESTORAN'][0]?>">
            <div class="distance-place"><span></span> КМ</div>
            <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
                <div class="pic">
                    <a href="http://restoran.ru<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
                </div>
            <?endif;?>
            <div class="text">
                <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><?=$arItem["NAME"]?></a></h2>
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"]['VALUE'])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>
                </div>
                <div class="props">
                    <div class="prop">
                        <?
                        $clear_city_arr = array(
                            'msk'=>"/( +|)Москва[, ]+/i",
                            'spb'=>"/( +|)Санкт-Петербург[, ]+/i",
                            'rga'=>"/( +|)Рига[, ]+/i",
                            'urm'=>"/( +|)Юрмала[, ]+/i",
                            'kld'=>"/( +|)Калиниград[, ]+/i",
                            'nsk'=>"/( +|)Новосибирск[, ]+/i",
                            'sch'=>"/( +|)Сочи[, ]+/i"
                        );
                        ?>
                        <div class="value"><?=is_array($arItem["PROPERTIES"]["address"]["VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arItem["PROPERTIES"]["address"]["VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arItem["PROPERTIES"]["address"]["VALUE"])?></div>
                    </div>
                    <div class="prop wsubway">
                        <div class="subway">M</div>
                        <div class="value"><?=implode(', ',$arItem["PROPERTIES"]["SUBWAY"]["VALUE"])?></div>
                    </div>
                </div>
            </div>
        </div>
        <!--            --><?//if(($key+1)%3==0&&$key!=0):?>
        <!--                </div><div>-->
        <!--            --><?//endif?>
    <?endforeach;?>
    <!--        </div>-->

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]&&$arResult["NAV_STRING"]&&$arParams['AJAX']!='Y'): ?>
        <div class="clear"></div>
        <div class="navigation">
            <div class="pages"><?= $arResult["NAV_STRING"] ?></div>
        </div>
    <?endif;?>
<?else:?>
    <?
    echo "<p>На данный момент мы не располагаем информацией о предложениях ресторана. Воспользуйтесь поиском или обратитесь в нашу службу бронирования, и мы с удовольствием подберем для вас ресторан или банкетный зал с учётом ваших пожеланий!</p>";
    ?>
<?endif;?>
