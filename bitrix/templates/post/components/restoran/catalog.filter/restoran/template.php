<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddHeadScript("/tpl/js/jquery.checkbox.js")?>
<noindex>
<form action="/<?=CITY_ID?>/articles/post/" method="get" name="rest_filter_form">
    <input type="hidden" name="page" value="1">
    <div class="search">
            <div class="filter_box" style="width:428px;">
                <div class="left" style="width:372px">
            <?
            $p=0;$t=0;
            $ar_cusel = Array(2,3,1);
            foreach($arResult["arrProp"] as $prop_id => $arProp)
            {
                if ($arProp["PROPERTY_TYPE"]!="L"):?>
                <div class="filter_block" id="filter<?=$prop_id?>">
                    <?$arProp["NAME"]= str_replace(" - Доставка","",$arProp["NAME"])?>
                    <div class="title"><a class="another" href="javascript:void(0)"><?=$arProp["NAME"]?></a></div>                                      
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_<?=$prop_id?>").popup({"obj":"filter<?=$prop_id?>","css":{"right":"0px","top":"2px", "width":"355px"}});
                            var params = {
                                changedEl: "#multi<?=$ar_cusel[$p]?>",
                                scrollArrows: true,
                                visRows:10
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_<?=$prop_id?>" style="display:none">
                        <div class="title" align="right"><?=$arProp["NAME"]?></div>
                        <select multiple="multiple" class="asd" id="multi<?=$ar_cusel[$p]?>" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]" size="10">
                            <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                                <?if ($arProp["CODE"]=="subway"):?>
                                    <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val["NAME"]?></option>
                                <?else:?>
                                    <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>
                                <?endif;?>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi<?=$ar_cusel[$p]?>" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>        
                <?$p++;
                else:?>
                    <table cellpadding="0" cellspacing="0" class="right" style="width:225px; margin-right:0px;  margin-bottom:10px; margin-top:1px;">                                                                        
                        <tr class="select_all">
                            <td style="width:20px; padding: 5px 0px;padding-right:5px"><span class="niceCheck" style="background-position: 0px 0px; "><input name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>]" type="checkbox" value="Да" <?=($_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]=="Да")?"checked":""?> name="" id=""></span></td>
                            <td class="option2" style="font-size:16px;"><label class="niceCheckLabel"><?=$arProp["NAME"]?></label></td>                                       
                        </tr>
                    </table>
                <?$t++;endif;
                
            }
            ?>    
            </div>
            <div style="margin-left:25px;">
                <input style="width:86px" class="new_filter_button" type="submit" name="search_submit" value="<?=GetMessage("IBLOCK_SET_FILTER");?>" />
            </div>
                <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</form>
</noindex>