<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true)?>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js')?>

    <?$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/cusel.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/styles.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js')?>            
    <?$APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js')?>             
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js')?>    
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/radio.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js')?>
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
    <?
        CModule::IncludeModule("advertising");    
        CAdvBanner::SetRequiredKeywords (array(CITY_ID));
    ?>
</head>
<body style="background:#FFF">
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <!--<div id="cupid">
                <img src="/tpl/images/spec/wedding/cupid.png" />
            </div>-->    
    <div id="container" style="background:url(/tpl/images/spec/post/post_bg.jpg) center top no-repeat">    
        <div id="wrapp">
        <div id="header">
            <div class="baner" style="height:90px;">
                <?$APPLICATION->IncludeComponent(
                	"bitrix:advertising.banner",
                	"",
                	Array(
                		"TYPE" => "top_main_page",
                		"NOINDEX" => "Y",
                		"CACHE_TYPE" => "A",
                		"CACHE_TIME" => "600"
                	),
                false
                );?>
            </div>      
            <div id="logo"><a href="/"><img src="/bitrix/templates/post/images/logo_post.png" /></a></div>  
            <div id="slogan">
                    <p class="s3">
                        <?
                        $APPLICATION->IncludeFile(
                            $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </p>
            </div>            
            <div id="logo_spec"><a href="/<?=CITY_ID?>/articles/post/"></a></div>
            <?$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");?>
            <?/*$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news_main",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "special_projects",
                        "IBLOCK_ID" => $arIB["ID"],
                        "NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "20"),
                        "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "ACTIVE_FROM"),
                        "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "DESC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array("ACTIVE_FROM"),
                        "PROPERTY_CODE" => array("ratio","reviews_bind"),
                        "CHECK_DATES" => "N",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "300",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "news",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "search_rest_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );*/
            ?> 
            <div id="filter">
                <div class="">                    
                    <?$APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "restoran",
                            Array(
                                    "IBLOCK_TYPE" => "special_project",
                                    "IBLOCK_ID" => $arIB["ID"],
                                    "FILTER_NAME" => "arrFilter",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => array("bill","subway"),
                                    "PRICE_CODE" => array(),
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_GROUPS" => "Y",
                                    "LIST_HEIGHT" => "5",
                                    "TEXT_WIDTH" => "20",
                                    "NUMBER_WIDTH" => "5",
                                    "SAVE_IN_SESSION" => "N"
                            )
                    );?>
                    <div class="clear"></div>
                </div>
                <Br /><Br />
                <?
                    $APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "post",
                            Array(),
                        false
                    );    
                ?>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news_main",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "special_projects",
                        "IBLOCK_ID" => $arIB["ID"],
                        "NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "20"),
                        "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
                        "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "DESC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array("ACTIVE_FROM"),
                        "PROPERTY_CODE" => array("ratio","reviews_bind"),
                        "CHECK_DATES" => "N",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "300",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "news",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "search_rest_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );
            ?>    
        </div>                
        <div id="content">            