<?
define("NO_KEEP_STATISTIC", true);

define("SITE_TEMPLATE_PATH", "/bitrix_personal/templates/Booking");
define("SITE_TEMPLATE_ID", "Booking");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/*Добавить проверку на авторизацию и все такое*/
/*нужно сделать дву универсальные функциидля запроса инфы по заказу и по клиенту*/


function user_auth(){
	global $USER;
	if (!is_object($USER)) $USER = new CUser;
	$arAuthResult = $USER->Login($_REQUEST["USER_LOGIN"], $_REQUEST["USER_PASSWORD"], "N");
	
	if($arAuthResult["TYPE"]=="ERROR"){
		echo $arAuthResult["MESSAGE"];
	}else{
		echo "ok";

	}
}

function save_win_pos(){
	global $USER;
	$W_ID = $_REQUEST["w_id"];
	$RAZDEL = $_REQUEST["razdel"];

	unset($_REQUEST["act"]);
	unset($_REQUEST["w_id"]);
	unset($_REQUEST["razdel"]);
	//v_dump($_REQUEST);
	
	$USER->Update($USER->GetID(), Array("UF_WIN_POS"=> serialize($_REQUEST)));
	
	if($W_ID=="w2"){
		$_REQUEST["razdel"]=$RAZDEL;
		//Пересчитываем высоту окна
		$C_HEIGHT = $_REQUEST["w2"]["height"] - 30 - 20-10 -22;
		$_SESSION["CLIENTS_PAGE_CO"]=round($C_HEIGHT/28);
		
		change_razdel("Y");
	
	}
	
}

function clear_pos(){
	global $USER;
	$USER->Update($USER->GetID(), Array("UF_WIN_POS"=> ""));
	echo "ok";
}


function resort_clients(){
	global $APPLICATION;
	
	if($_REQUEST["order"]=="#ID" || !isset($_SESSION["CLIENTS_ORDER"])) {
		$_SESSION["CLIENTS_ORDER"]="ID";
		$_SESSION["CLIENTS_BY"] = "DESC";
	}
	
	if($_REQUEST["order"]=="#SURNAME") {
		$_SESSION["CLIENTS_ORDER"]="PROPERTY_SURNAME";
		$_SESSION["CLIENTS_BY"] = "ASC";
	}
	
	if($_REQUEST["order"]=="#NAME") {
		$_SESSION["CLIENTS_ORDER"]="PROPERTY_NAME";
		$_SESSION["CLIENTS_BY"] = "ASC";
	}
	
	
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/clients.php"),
		Array(),
		Array("MODE"=>"php")
	);
	
}

function get_client_info(){

	global $APPLICATION;
	$_REQUEST["id"] = str_replace("client_", "", $_REQUEST["id"]);
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$EXIT = array();
	$res = CIBlockElement::GetByID($_REQUEST["id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps = $ob->GetProperties();
 		
 		$EXIT["CLIENT_ID"] = $arFields["ID"];
 		
 		$EXIT["NAME"] = $arProps["NAME"]["VALUE"];
 		$EXIT["SURNAME"] = $arProps["SURNAME"]["VALUE"];
 		$EXIT["EMAIL"] = $arProps["EMAIL"]["VALUE"];
 		$EXIT["TELEPHONE"] = $arProps["TELEPHONE"]["VALUE"];
 		$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
 		
 		$EXIT["PRIM"] = str_replace("<br />", "", $EXIT["PRIM"]);
 		
 		//$EXIT["ORDERS"]
 		
 		$EXIT["ORDERS"]='<div class="tbl">';
 		$EXIT["ORDERS"].='<table cellpadding="0" cellspacing="0">';
 		$EXIT["ORDERS"].='<tr class="hd">';
		$EXIT["ORDERS"].='<th class="c11">ресторан</th>';
		$EXIT["ORDERS"].='<th class="c12">гостей</th>';
		$EXIT["ORDERS"].='<th class="c13">визит</th>';
		$EXIT["ORDERS"].='<th class="c14">статус</th>';
		$EXIT["ORDERS"].='</tr>';
 		
 		//Теперь нужно запросить все заказы для этого пользователя
 		//Эту часть нужно будет закэшировать
 		$HO=false;
		$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_client"=>$arFields["ID"]);
		$res_zak = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter, false, false, false);
		while($ob_zak= $res_zak->GetNextElement()){
  			$arFields_zak = $ob_zak->GetFields();
  			$arProps_zak = $ob_zak->GetProperties();
  			
  			$arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", MakeTimeStamp($arFields_zak["ACTIVE_FROM"], CSite::GetDateFormat()));
  			
  			$resr = CIBlockElement::GetByID($arProps_zak["rest"]["VALUE"]);
			if($arFields_rest = $resr->GetNext()){
				$REST_NAME = $arFields_rest["NAME"];
		
			}else $REST_NAME="Подбор";

  			
  			
  			$EXIT["ORDERS"].='<tr id="order_'.$arFields_zak["ID"].'">';
			$EXIT["ORDERS"].='<td class="c11"><div>&nbsp;'.$REST_NAME.'</div></td>';
			$EXIT["ORDERS"].='<td class="c12"><div>&nbsp;'.$arProps_zak["guest"]["VALUE"].'</div></td>';
			$EXIT["ORDERS"].='<td class="c13"><div>&nbsp;'.$arFields_zak["ACTIVE_FROM"].'</div></td>';
			$EXIT["ORDERS"].='<td class="c14"><div>&nbsp;'.$arProps_zak["status"]["VALUE"].'</div></td>';
			$EXIT["ORDERS"].='</tr>';
			
			$HO=true;		
	
		}
		
		$EXIT["ORDERS"].='</table>';
		$EXIT["ORDERS"].='</div>';
		
		if(!$HO) $EXIT["ORDERS"]="";
		
	}	
	
	echo json_encode($EXIT);
}


function change_razdel($T1){
	global $APPLICATION;
	
	

	if($_REQUEST["razdel"]=="#clients"){
		
		if($_REQUEST["order"]=="#ID" || !isset($_SESSION["CLIENTS_ORDER"])) {
			$_SESSION["CLIENTS_ORDER"]="ID";
			$_SESSION["CLIENTS_BY"] = "DESC";
		}
	
		if($_REQUEST["order"]=="#SURNAME") {
			$_SESSION["CLIENTS_ORDER"]="PROPERTY_SURNAME";
			$_SESSION["CLIENTS_BY"] = "ASC";
		}
	
		if($_REQUEST["order"]=="#NAME") {
			$_SESSION["CLIENTS_ORDER"]="PROPERTY_NAME";
			$_SESSION["CLIENTS_BY"] = "ASC";
		}
	
	
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/clients.php"),
			Array(),
			Array("MODE"=>"php")
		);
	}
	
	if($_REQUEST["razdel"]=="#orders"){
		if($_REQUEST["order"]=="#ACTIVE_FROM" || !isset($_SESSION["ORDERS_ORDER"])) {
			$_SESSION["ORDERS_ORDER"]="ACTIVE_FROM";
			$_SESSION["ORDERS_BY"] = "ASC";
		}
	
		if($_REQUEST["order"]=="#ID") {
			$_SESSION["ORDERS_ORDER"]="ID";
			$_SESSION["ORDERS_BY"] = "DESC";
		}
		
		global $arrFilter;
		$arrFilter["PROPERTY_status_VALUE"]=array("заказ принят", "забронирован", "банкет в работе", "внесена предоплата");

	
		$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/orders.php"),
		Array(),
		Array("MODE"=>"php")
		);
	}
	
	
	if($_REQUEST["razdel"]=="#all_orders"){
		
		if($_REQUEST["order"]=="#ACTIVE_FROM" || !isset($_SESSION["ORDERS_ORDER"])) {
			$_SESSION["ORDERS_ORDER"]="ACTIVE_FROM";
			$_SESSION["ORDERS_BY"] = "ASC";
		}
	
		if($_REQUEST["order"]=="#ID") {
			$_SESSION["ORDERS_ORDER"]="ID";
			$_SESSION["ORDERS_BY"] = "DESC";
		}
		
		global $arrFilter;
		
		if($_SESSION["ALL_ORDERS_FLTR"]) $arrFilter=$_SESSION["ALL_ORDERS_FLTR"];	
	
		$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/all_orders.php"),
		Array(),
		Array("MODE"=>"php")
		);
	}
	
	
	if($_REQUEST["razdel"]=="#new_orders"){
		global $arrFilter;
		$arrFilter["PROPERTY_new_VALUE"]="Y";	
	
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/all_orders.php"),
			Array(),
			Array("MODE"=>"php")
		);
	}
	
	
	
	


	if($_REQUEST["razdel"]=="#reports"){
		$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/reports.php"),
		Array(),
		Array("MODE"=>"php")
		);
	}


	if($_REQUEST["razdel"]=="#restorans"){
		if(isset($_REQUEST["PAGEN_1"]) || $T1=="Y") $SHOW_FLTR="N";
		else $SHOW_FLTR="Y";
		
		$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/restorans.php"),
		Array("SHOW_FLTR"=>$SHOW_FLTR),
		Array("MODE"=>"php")
		);
	}else{
		unset($_SESSION["REST_FLTR"]);
	}

	if($_REQUEST["razdel"]=="#sms"){
		$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/sms.php"),
		Array(),
		Array("MODE"=>"php")
		);
	}
		
}

//нужно проверить права на изменение
//нужно вернуть строку с новыми данными и заменяем старую в w2
function update_client(){
	global $APPLICATION;
	global $USER;
	
	$arGroups = CUser::GetUserGroup($USER->GetID());
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	      
	$EXIT2 =$EXIT2 =array();
	
	//сначала смотрим, апдэйтим ли мы клиента, или создаем нового
	if($_REQUEST["client_id"]>0){
		//апдейтим клиента
		$el = new CIBlockElement;
	
		$PROP = array();
		$PROP["NAME"] = $_REQUEST["name"]; 
		$PROP["SURNAME"] = $_REQUEST["surname"]; 
		$PROP["TELEPHONE"] = $_REQUEST["telephone"]; 
		$PROP["EMAIL"] = $_REQUEST["email"]; 
		
		$arLoadProductArray = Array(
  			"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем 
  			"PROPERTY_VALUES"=> $PROP,
  			"NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"],
  			"ACTIVE"         => "Y",            // активен
  			"PREVIEW_TEXT"   => $_REQUEST["prim"],
  		);
		
		$res = $el->Update($_REQUEST["client_id"], $arLoadProductArray);	
		$EXIT["message"]="client_updated";
		
		$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
	}else{
		//добавляем нового
		$EXIT = add_new_client();
	
		$_REQUEST["client_id"]=$EXIT["client"];
		
		$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
	}
	
	
	
	if($_REQUEST["rastoran_id"]=="") $_REQUEST["rastoran_id"]=0;
	
	
	//далее смотрим, на бронь
	if($_REQUEST["order_id"]>0){
		
		//апдэйтим существующую бронь
		//Нужно получить текущий статус заказа
		$ro = CIBlockElement::GetByID($_REQUEST["order_id"]);
		if($ar_res_ro = $ro->GetNextElement()){
			$orPrps = $ar_res_ro->GetProperties();
			
		}
		
		//если заказ меняет оператор и у заказа статус отчет
		if(!in_array(17, $arGroups) && $orPrps["status"]["VALUE"]=="отчет" ){
			$EXIT["message"] ="permission denied";
		}else{
		
		$el = new CIBlockElement;
	
		$PROP2 = array();
		$PROP2["rest"] = $_REQUEST["rastoran_id"]; 
		$PROP2["guest"] = $_REQUEST["guest"]; 
		
		$PROP2["client"] = $_REQUEST["client_id"]; 	
		$PROP2["prinyal"] = $_REQUEST["prinyal"]; 
		$PROP2["source"] = $_REQUEST["source"]; 
		$PROP2["podtv"] = $_REQUEST["podtv"]; 
		$PROP2["summ_sch"] = $_REQUEST["summ_sch"];
		$PROP2["podtv"] = $_REQUEST["podtv"]; 
		$PROP2["summ_sch"] = $_REQUEST["summ_sch"]; 	
		$PROP2["stol"] = $_REQUEST["stol"]; 
		$PROP2["podtv2"] = $_REQUEST["podtv2"]; 
		$PROP2["type"] = $_REQUEST["type"];
		
		$PROP2["procent"] = $_REQUEST["procent"];
		
		$PROP2["format"] = $_REQUEST["format"];
		
		$PROP2["status"] = $_REQUEST["status"];
		
		$PROP2["control_call"] = $_REQUEST["control_call"];
		
		//деньги получены
		if(in_array(17, $arGroups)){
			if($_REQUEST["dengi"]=="Y") $PROP2["dengi"]=1553;
			else $PROP2["dengi"]="";
		}
		
		$DT="";
		if($_REQUEST["date"]!=""){
			$DT=$_REQUEST["date"];
		}
	
		if($_REQUEST["time"]!="" && $DT!=""){
			$tr = explode(":", $_REQUEST["time"]);
			if($tr[0]<24 && $tr[1]<61){
				$DT.=" ".$_REQUEST["time"].":00";
			} 
		}
		
		$arLoadProductArray2 = Array(
  			"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  			"PROPERTY_VALUES"=> $PROP2,
  			"NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"]." ".$_REQUEST["rastoran_id"],
  			"ACTIVE"         => "Y",            // активен
  			"ACTIVE_FROM"=>$DT,
  			"PREVIEW_TEXT"   => htmlspecialchars_decode($_REQUEST["prim_order"], ENT_NOQUOTES),
  		);
  		
  		//htmlspecialchars_decode(string string [, int quote_style])
		
		//var_dump($arLoadProductArray2);
		
		if($_REQUEST["client_id"]>0) {
			if($res = $el->Update($_REQUEST["order_id"], $arLoadProductArray2)) $EXIT["message"] ="order_saved";
			else $EXIT["message"]=$el->LAST_ERROR;
		}
		
		
		$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_105');
		}
	}else{
		//создаем новую бронь
		//if($_REQUEST["rastoran_id"]>0 || $_REQUEST["rastoran"]!="") 
		if($_REQUEST["type"]!="" && $_REQUEST["client_id"]!="") 
			$EXIT2 = add_new_order($_REQUEST["client_id"]);
		
		$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_105');
	
	}
	
	//var_dump($EXIT2);
	echo json_encode(array_merge($EXIT,$EXIT2));
	
}

function add_new_order($CLIENT_ID){
	global $APPLICATION;
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	if($_REQUEST["status"]=="") $_REQUEST["status"]=1418;
	
	$EXIT=array();

	$el = new CIBlockElement;
	
	$PROP2 = array();
	$PROP2["rest"] = $_REQUEST["rastoran_id"]; 
	$PROP2["guest"] = $_REQUEST["guest"]; 
	$PROP2["status"] = $_REQUEST["status"];
	$PROP2["client"] = $CLIENT_ID; 	
	$PROP2["prinyal"] = $_REQUEST["prinyal"]; 
	$PROP2["source"] = $_REQUEST["source"]; 
	$PROP2["podtv"] = $_REQUEST["podtv"]; 
	$PROP2["summ_sch"] = $_REQUEST["summ_sch"];
	$PROP2["podtv"] = $_REQUEST["podtv"]; 
	$PROP2["summ_sch"] = $_REQUEST["summ_sch"]; 	
	$PROP2["stol"] = $_REQUEST["stol"]; 
	$PROP2["podtv2"] = $_REQUEST["podtv2"]; 
	$PROP2["user"] = $_REQUEST["podtv2"]; 
	$PROP2["type"] = $_REQUEST["type"];
	$PROP2["procent"] = $_REQUEST["procent"];
	$PROP2["control_call"] = $_REQUEST["control_call"];

	if($_SESSION["CITY"]=="spb") $SECTION=83660;
	else $SECTION=83659;
	
	$DT="";
	if($_REQUEST["date"]!=""){
		$DT=$_REQUEST["date"];
	}
	
	if($_REQUEST["time"]!="" && $DT!=""){
		$tr = explode(":", $_REQUEST["time"]);
		if($tr[0]<24 && $tr[1]<61){
			$DT.=" ".$_REQUEST["time"].":00";
		} 
	}
		
	$arLoadProductArray2 = Array(
  		"IBLOCK_SECTION_ID" => $SECTION,  
  		"IBLOCK_ID" => 105,         
  		"PROPERTY_VALUES"=> $PROP2,
  		"NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"]." ".$_REQUEST["rastoran_id"],
  		"ACTIVE"         => "Y",     
  		"ACTIVE_FROM"=>$DT,  
  		"PREVIEW_TEXT"   => $_REQUEST["prim_order"],
  	);
  	
  	
		
	if($ORID = $el->Add($arLoadProductArray2)) $EXIT["ORDER_ID"]=$ORID;
	else echo "Error: ".$el->LAST_ERROR;
	
	return $EXIT;
}

function search_client_by_surname(){
	global $APPLICATION;
	global $USER;
	global $arrFilter;
	if($_REQUEST["surname"]!="") $arrFilter["PROPERTY_surname"]="%".$_REQUEST["surname"]."%";
	
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/clients.php"),
		Array("arrFilter"=>$arrFilter),
		Array("MODE"=>"php")
	);
}


function search_client_by_name(){
	global $APPLICATION;
	global $USER;
	global $arrFilter;
	if($_REQUEST["name"]!="") $arrFilter["PROPERTY_name"]="%".$_REQUEST["name"]."%";
	
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/clients.php"),
		Array("arrFilter"=>$arrFilter),
		Array("MODE"=>"php")
	);
}


function search_client_by_telephone(){
	global $APPLICATION;
	global $USER;
	global $arrFilter;
	
	$_REQUEST["telephone"] = str_replace("_", "", $_REQUEST["telephone"]);
	
	if($_REQUEST["telephone"]!="") $arrFilter["PROPERTY_telephone"]="%".$_REQUEST["telephone"]."%";
	
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/clients.php"),
		Array("arrFilter"=>$arrFilter),
		Array("MODE"=>"php")
	);
}

function search_client_by_email(){
	global $APPLICATION;
	global $USER;
	global $arrFilter;
	if($_REQUEST["email"]!="") $arrFilter["PROPERTY_email"]="%".$_REQUEST["email"]."%";
	
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/clients.php"),
		Array("arrFilter"=>$arrFilter),
		Array("MODE"=>"php")
	);
}


function add_new_client(){
	global $APPLICATION;
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	if($_SESSION["CITY"]=="spb") $SECTION=83823;
	else $SECTION=83822;
	
	$EXIT=array();
	//сначала посмотрим, есть ли в базе клиент с таким телефон или emailом или фамилией
	//если клиент есть, то покажем алерт
	$arrFilter=array(
		array(
			"LOGIC"=>"OR",
			"PROPERTY_EMAIL"=>$_REQUEST["email"],
			"PROPERTY_TELEPHONE"=>$_REQUEST["telephone"],
			"PROPERTY_SURNAME"=>$_REQUEST["surname"]
		),
		"IBLOCK_SECTION"=>$SECTION,
		"IBLOCK_ID"=>104
	);
	
	if($_REQUEST["email"]=="") unset($arrFilter[0]["PROPERTY_EMAIL"]);
	
	//var_dump($arrFilter);
	$res = CIBlockElement::GetList(Array(), $arrFilter, false, false, false);
	if($ob = $res->GetNextElement()){
		$arProps = $ob->GetProperties();
		
		//такой клиент есть
		if($_REQUEST["no_alert"]=="Y"){
			//все равно добавляем
			$el = new CIBlockElement;

			$PROP = array();
			
			if($_REQUEST["name"]=="" && $_REQUEST["surname"]=="") $_REQUEST["surname"]="Аноним"; 
			
			$PROP["NAME"] = $_REQUEST["name"]; 
			$PROP["SURNAME"] = $_REQUEST["surname"]; 
			$PROP["TELEPHONE"] = $_REQUEST["telephone"]; 
			$PROP["EMAIL"] = $_REQUEST["email"];       
			
			$arLoadProductArray = Array(
		  		"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
		  		"IBLOCK_ID"=>104,
		  		"IBLOCK_SECTION" => $SECTION,          // элемент лежит в корне раздела
		  		"PROPERTY_VALUES"=> $PROP,
		  		"NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"],
		  		"ACTIVE"         => "Y",            // активен
  				"PREVIEW_TEXT"   => $_REQUEST["prim"],
  			);

			if($CLIENT_ID = $el->Add($arLoadProductArray)){
  				$EXIT["message"]="client_edded";
  				$EXIT["client"]=$CLIENT_ID;
  				
  				$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
			}else
  				$EXIT["message"]=$el->LAST_ERROR;
			
			
		}else{
			$EXIT["message"]="client_find";
			
			if($arProps["SURNAME"]["VALUE"]==$_REQUEST["surname"] && $_REQUEST["surname"]!="") $EXIT["message2"]='<span class="quest_icon"></span>Клиент с такой фамилией уже есть. Продолжить?';
			if($arProps["TELEPHONE"]["VALUE"]==$_REQUEST["telephone"] && $_REQUEST["telephone"]!="") $EXIT["message2"]='<span class="quest_icon"></span>Клиент с таким телефоном уже есть. Продолжить?';
			if($arProps["EMAIL"]["VALUE"]==$_REQUEST["email"] && $_REQUEST["email"]!="") $EXIT["message2"]='<span class="quest_icon"></span>Клиент с таким E-mailом уже есть. Продолжить?';	
			
			if($EXIT["message2"]=="") $EXIT["message2"]='<span class="quest_icon"></span>Вы не заполнили некоторые поля. Продолжить?';
		}
	}else{
		//такого клиента нет, смело добавляем
		
		$el = new CIBlockElement;

		$PROP = array();
		$PROP["NAME"] = $_REQUEST["name"]; 
		$PROP["SURNAME"] = $_REQUEST["surname"]; 
		$PROP["TELEPHONE"] = $_REQUEST["telephone"]; 
		$PROP["EMAIL"] = $_REQUEST["email"];       

		if($_SESSION["CITY"]=="spb") $SECTION=83823;
		else $SECTION=83822;

		$arLoadProductArray = Array(
		  	"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
		  	"IBLOCK_ID"=>104,       // элемент лежит в корне раздела
		  	"PROPERTY_VALUES"=> $PROP,
		  	"IBLOCK_SECTION" => $SECTION,
		  	"NAME"           => $_REQUEST["surname"]." ".$_REQUEST["name"],
		  	"ACTIVE"         => "Y",            // активен
  			"PREVIEW_TEXT"   => $_REQUEST["prim"],
  		);

		if($CLIENT_ID = $el->Add($arLoadProductArray)){
  			$EXIT["message"]="client_edded";
  			$EXIT["client"]=$CLIENT_ID;
  			
  			$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_104');
		}else
  			$EXIT["message"]=$el->LAST_ERROR;
	}
	
	return $EXIT;
	
}


function search_restoran(){
	global $APPLICATION;
	global $USER;
	if($_REQUEST["name"]=="") die("");
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
	if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
	
	//var_dump(urldecode($_REQUEST["name"]));
	
	//urldecode(string str)
	
	$arrFilter=array(
		"NAME"=>"%".urldecode($_REQUEST["name"])."%",
		"IBLOCK_ID"=>$IBLOCK_ID
	);
	
	$arrFilter["ACTIVE"]="Y";
	//$arrFilter["SECTION_CODE"]="restaurants";
	
	$now_rest="";
	$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arrFilter, false, false, false);
	while($ob= $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps_rest = $ob->GetProperties();
 		
 		$ADDR="";
 		foreach($arProps_rest["address"]["VALUE"] as $T){
 			$ADDR.=$T;
 			if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
 			$i++;
 		}
 		
 		$ADDR = str_replace("Москва, ","", $ADDR);
 		$ADDR = str_replace("Санкт-Петербург, ","", $ADDR);
 		
 		
  		echo '<a href="#'.$arFields["ID"].'">'.$arFields["NAME"];
  		echo ' ('.$ADDR.')';
  		echo '</a>';
  		$now_rest=$arFields["NAME"];
  	}	
}

function get_restoran_info(){
	global $APPLICATION;
	global $USER;
	if($_REQUEST["restoran_id"]=="") die("");
	$_REQUEST["restoran_id"] = str_replace("#", "", $_REQUEST["restoran_id"]);
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$EXIT=array();
	$res = CIBlockElement::GetByID($_REQUEST["restoran_id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps_rest = $ob->GetProperties();
		
		$EXIT["ID"]=$arFields["ID"];
		$EXIT["ADDRESS"]=$arProps_rest["address"]["VALUE"];
		
		foreach($arProps_rest["phone"]["VALUE"] as $T){
 				$TEL.=$T;
 				if($arProps_rest["phone"]["VALUE"][$i+1]!="") $TEL.=", ";
 				$i++;
 			}

			$i=0;
 			foreach($arProps_rest["address"]["VALUE"] as $T){
 				$ADDR.=$T;
 				if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
 				$i++;
 			}
 			
 			$i=0;
 			foreach($arProps_rest["opening_hours"]["VALUE"] as $T){
 				$OPEN.=$T;
 				if($arProps_rest["opening_hours"]["VALUE"][$i+1]!="") $OPEN.=", ";
 				$i++;
 			}
 			
 			$i=0;
 			foreach($arProps_rest["subway"]["VALUE"] as $T){
 				$res_metro = CIBlockElement::GetByID($T);
				if($ob_metro = $res_metro->GetNext()){
					$tar=explode("(", $ob_metro["NAME"]);
					$METRO.=trim($tar[0]);
				}
 				if($arProps_rest["subway"]["VALUE"][$i+1]!="") $METRO.=", ";
 				$i++;
 			}
		
		if($arProps_rest["rabotaem"]["VALUE"]=="Y") $arProps_rest["rabotaem"]["VALUE"]="Работаем";
		else $arProps_rest["rabotaem"]["VALUE"]="НЕ Работаем";
		
		$EXIT["RESTORAN_INFO"]='';
 		$EXIT["RESTORAN_INFO"].='<div class="il"><b>Телефон:</b> '.$TEL.'</div>';
 		if($arProps_rest["real_tel"]["VALUE"]!="") $EXIT["RESTORAN_INFO"].='<div class="il"><b>Реальный телефон:</b> '.$arProps_rest["real_tel"]["VALUE"].'</div>';
		$EXIT["RESTORAN_INFO"].='<div class="il"><b>Адрес:</b> '.$ADDR.' </div>';
		$EXIT["RESTORAN_INFO"].='<div class="il"><b>Метро:</b> '.$METRO.'</div>';
		$EXIT["RESTORAN_INFO"].='<div class="il spec_color"><b>Сотрудничество:</b> '.$arProps_rest["rabotaem"]["VALUE"].'</div>';		
		$EXIT["RESTORAN_INFO"].='<div class="il"><b>Время работы:</b> '.$OPEN.'</div>';
		$EXIT["RESTORAN_INFO"].='<div class="il2"><b>Пометки:</b><br/> '.$arProps_rest["pometki"]["VALUE"].'</div>';
		
	}

	echo json_encode($EXIT);

}

function get_rest_detailed_info(){
	global $APPLICATION;
	global $USER;
	if($_REQUEST["id"]=="") die("");
	$_REQUEST["restoran_id"] = str_replace("rest_", "", $_REQUEST["id"]);
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	
	$res = CIBlockElement::GetByID($_REQUEST["restoran_id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arFields["PROPERTIES"] = $ob->GetProperties();
			
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/restoran_detailed_info.php"),
			Array("REST"=>$arFields),
			Array("MODE"=>"php")
		);	

	}


}

function get_order_info(){
	global $APPLICATION;
	global $USER;
	$_REQUEST["id"] = str_replace("order_", "", $_REQUEST["id"]);
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$EXIT = array();
	$res = CIBlockElement::GetByID($_REQUEST["id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps = $ob->GetProperties();
 		
 		
 		$EXIT["DATE_CREATE"] = $arFields["DATE_CREATE"];
 		
 		$EXIT["ID"] = $arFields["ID"];
 		//$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
 		
 		$EXIT["PRIM"] = addslashes(html_entity_decode($arFields["PREVIEW_TEXT"]));
 		
 		
 		//addslashes()
 		//$EXIT["PRIM"] =rawurlencode($EXIT["PRIM"]);
 		
 		//var_dump($EXIT["PRIM"]);
 		
 		//htmlspecialchars_decode(string string [, int quote_style])
 		
 		$tar=explode(" ", $arFields["ACTIVE_FROM"]);
 		$tar2=explode(":", $tar[1]);
 		$EXIT["DATE"] = $tar[0];
 		$EXIT["TIME"] = $tar2[0].":".$tar2[1];
 		
 		if($EXIT["TIME"]==":") $EXIT["TIME"]="";
 		
 		if($arProps["status"]["VALUE"]=="") $arProps["status"]["VALUE"]="&nbsp;";
 		if($arProps["source"]["VALUE"]=="") $arProps["source"]["VALUE"]="&nbsp;";
 		
 		
 		
 		$EXIT["GUEST"] = $arProps["guest"]["VALUE"];
 		$EXIT["STATUS"] = $arProps["status"]["VALUE"];
 		$EXIT["STATUS_ID"] = $arProps["status"]["VALUE_ENUM_ID"];
 		
 		
 		if($arProps["status"]["VALUE"]=="заказ принят") $EXIT["STATUS_COLOR"] = "yello";
		if($arProps["status"]["VALUE"]=="забронирован") $EXIT["STATUS_COLOR"]  = "sv_zel";
		if($arProps["status"]["VALUE"]=="гости пришли") $EXIT["STATUS_COLOR"]  = "zel";
		if($arProps["status"]["VALUE"]=="гости отменили заказ") $EXIT["STATUS_COLOR"]  = "red";
		if($arProps["status"]["VALUE"]=="ошибочный заказ") $EXIT["STATUS_COLOR"]  = "black";
		if($arProps["status"]["VALUE"]=="банкет в работе") $EXIT["STATUS_COLOR"]  = "yello";
		if($arProps["status"]["VALUE"]=="внесена предоплата") $EXIT["STATUS_COLOR"]  = "yello";
		if($arProps["status"]["VALUE"]=="отчет") $EXIT["STATUS_COLOR"]  = "blue";
		if($arProps["status"]["VALUE"]=="заказ оплачен") $EXIT["STATUS_COLOR"]  = "blue";
 		
 		
 		$EXIT["CONTROL_CALL"] = $arProps["control_call"]["VALUE"];
 		
 		$EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];
 		
 		$EXIT["FORMAT"] = $arProps["format"]["VALUE"];
 		
 		$EXIT["TYPE"] = $arProps["type"]["VALUE"];
 		$EXIT["TYPE_ID"] = $arProps["type"]["VALUE_ENUM_ID"];
 		$EXIT["DENGI"] = $arProps["dengi"]["VALUE"];
 		
 		$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
 		$EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];
 		$EXIT["SUMM_SCH"] = $arProps["summ_sch"]["VALUE"];
 		$EXIT["PROCENT"] = $arProps["procent"]["VALUE"];

 		$EXIT["STOL"] = $arProps["stol"]["VALUE"];
 		$EXIT["PODTV2"] = $arProps["podtv2"]["VALUE"];
		$EXIT["PODTV"] = $arProps["podtv"]["VALUE"];
 		
 		
 		$EXIT["SOURCE"] = $arProps["source"]["VALUE"];
 		$EXIT["SOURCE_ID"] = $arProps["source"]["VALUE_ENUM_ID"];
 		
 		
 		$rsUser = CUser::GetByID($arFields["CREATED_BY"]);
		$arUser = $rsUser->Fetch();
 		
 		$EXIT["OPERATOR"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];
 		
 		//теперь нужно получить инфу по ресторану
 		$res_rest = CIBlockElement::GetByID($arProps["rest"]["VALUE"]);
		if($ob_rest = $res_rest->GetNextElement()){
			$arFields_rest = $ob_rest->GetFields();  
 			$arProps_rest = $ob_rest->GetProperties();
 		
 			$EXIT["RESTORAN"]=$arFields_rest["NAME"];
 			$EXIT["RESTORAN_ID"]=$arFields_rest["ID"];
 			
 			
 			$i=0;
 			foreach($arProps_rest["phone"]["VALUE"] as $T){
 				$TEL.=$T;
 				if($arProps_rest["phone"]["VALUE"][$i+1]!="") $TEL.=", ";
 				$i++;
 			}

			$i=0;
 			foreach($arProps_rest["address"]["VALUE"] as $T){
 				$ADDR.=$T;
 				if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
 				$i++;
 			}
 			
 			$i=0;
 			foreach($arProps_rest["opening_hours"]["VALUE"] as $T){
 				$OPEN.=$T;
 				if($arProps_rest["opening_hours"]["VALUE"][$i+1]!="") $OPEN.=", ";
 				$i++;
 			}
 			
 			$i=0;
 			foreach($arProps_rest["subway"]["VALUE"] as $T){
 				$res_metro = CIBlockElement::GetByID($T);
				if($ob_metro = $res_metro->GetNext()){
					$tar=explode("(", $ob_metro["NAME"]);
					$METRO.=trim($tar[0]);
				}
 				if($arProps_rest["subway"]["VALUE"][$i+1]!="") $METRO.=", ";
 				$i++;
 			}

 			
 			
			
			
			$EXIT["RESTORAN_INFO"]='';
 			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Телефон:</b> '.$TEL.'</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Адрес:</b> '.$ADDR.' </div>';
			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Метро:</b> '.$METRO.'</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il cpec_color"><b>Сотрудничество:</b> '; 
			
			if($arProps_rest["rabotaem"]["VALUE"]=="Y") $EXIT["RESTORAN_INFO"].='Работаем';
			else  $EXIT["RESTORAN_INFO"].='НЕ Работаем';
			
			$EXIT["RESTORAN_INFO"].='</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Время работы:</b> '.$OPEN.'</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il2"><b>Пометки:</b><br/> '.$arProps_rest["operator_comment"]["VALUE"].'</div>';
 			
 			
 		}else{
 			$EXIT["RESTORAN"]="Подбор";
 		}
 		
		
	}	
	
	
	echo json_encode($EXIT);


}

function get_client_info_by_order(){
	global $APPLICATION;
	$_REQUEST["id"] = str_replace("order_", "", $_REQUEST["id"]);
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$EXIT = array();	
	
	//сначала нужно получить инфу по заказу
	$res = CIBlockElement::GetByID(intval($_REQUEST["id"]));
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps = $ob->GetProperties();
 		
		$EXIT["DATE_CREATE"] = $arFields["DATE_CREATE"];
		
 		$EXIT["ID"] = $arFields["ID"];
 		$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
 		
 		
 		$EXIT["PRIM"]=str_replace("<br />", "", $EXIT["PRIM"]);
 		
 		$tar=explode(" ", $arFields["ACTIVE_FROM"]);
 		$tar2=explode(":", $tar[1]);
 		$EXIT["DATE"] = $tar[0];
 		$EXIT["TIME"] = $tar2[0].":".$tar2[1];
 		
 		if($EXIT["TIME"]==":") $EXIT["TIME"]="";
 		
 		if($arProps["status"]["VALUE"]=="") $arProps["status"]["VALUE"]="&nbsp;";
 		if($arProps["source"]["VALUE"]=="") $arProps["source"]["VALUE"]="&nbsp;";
 		
 		
 		
 		
 		if($arProps["status"]["VALUE"]=="заказ принят") $EXIT["STATUS_COLOR"] = "yello";
		if($arProps["status"]["VALUE"]=="забронирован") $EXIT["STATUS_COLOR"]  = "sv_zel";
		if($arProps["status"]["VALUE"]=="гости пришли") $EXIT["STATUS_COLOR"]  = "zel";
		if($arProps["status"]["VALUE"]=="гости отменили заказ") $EXIT["STATUS_COLOR"]  = "red";
		if($arProps["status"]["VALUE"]=="ошибочный заказ") $EXIT["STATUS_COLOR"]  = "black";
		if($arProps["status"]["VALUE"]=="банкет в работе") $EXIT["STATUS_COLOR"]  = "yello";
		if($arProps["status"]["VALUE"]=="внесена предоплата") $EXIT["STATUS_COLOR"]  = "yello";
		if($arProps["status"]["VALUE"]=="отчет") $EXIT["STATUS_COLOR"]  = "blue";
		if($arProps["status"]["VALUE"]=="заказ оплачен") $EXIT["STATUS_COLOR"]  = "blue";
 		
 		$EXIT["CONTROL_CALL"] = $arProps["control_call"]["VALUE"];
 		
 		
 		$EXIT["GUEST"] = $arProps["guest"]["VALUE"];
 		$EXIT["STATUS"] = $arProps["status"]["VALUE"];
 		$EXIT["STATUS_ID"] = $arProps["status"]["VALUE_ENUM_ID"];
 		$EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];
 		
 		$EXIT["PRIM_ORDER"] = $arFields["PREVIEW_TEXT"];
 		$EXIT["PRINYAL"] = $arProps["prinyal"]["VALUE"];
 		$EXIT["SUMM_SCH"] = $arProps["summ_sch"]["VALUE"];
 		$EXIT["STOL"] = $arProps["stol"]["VALUE"];
 		$EXIT["PROCENT"] = $arProps["procent"]["VALUE"];
 		$EXIT["PODTV2"] = $arProps["podtv2"]["VALUE"];
		$EXIT["PODTV"] = $arProps["podtv"]["VALUE"];
 		
 		
 		$EXIT["SOURCE"] = $arProps["source"]["VALUE"];
 		$EXIT["SOURCE_ID"] = $arProps["source"]["VALUE_ENUM_ID"];
 		
		$EXIT["TYPE"] = $arProps["type"]["VALUE"];
 		$EXIT["TYPE_ID"] = $arProps["type"]["VALUE_ENUM_ID"];
 		
 		
 		$EXIT["FORMAT"] = $arProps["format"]["VALUE"];
 		
 		$rsUser = CUser::GetByID($arFields["CREATED_BY"]);
		$arUser = $rsUser->Fetch();
 		
 		$EXIT["OPERATOR"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];
 		
 		CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], 105, array("new" => false));
 		//удаляем файл
 		@unlink($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders/".$_REQUEST["id"]);
 		
 		
 		//теперь нужно получить инфу по ресторану
 		$res_rest = CIBlockElement::GetByID($arProps["rest"]["VALUE"]);
		if($ob_rest = $res_rest->GetNextElement()){
			$arFields_rest = $ob_rest->GetFields();  
 			$arProps_rest = $ob_rest->GetProperties();
 		
 			$EXIT["RESTORAN"]=$arFields_rest["NAME"];
 			$EXIT["RESTORAN_ID"]=$arFields_rest["ID"];
 			
 			$i=0;
 			foreach($arProps_rest["phone"]["VALUE"] as $T){
 				$TEL.=$T;
 				if($arProps_rest["phone"]["VALUE"][$i+1]!="") $TEL.=", ";
 				$i++;
 			}

			$i=0;
 			foreach($arProps_rest["address"]["VALUE"] as $T){
 				$ADDR.=$T;
 				if($arProps_rest["address"]["VALUE"][$i+1]!="") $ADDR.=", ";
 				$i++;
 			}
 			
 			$i=0;
 			foreach($arProps_rest["opening_hours"]["VALUE"] as $T){
 				$OPEN.=$T;
 				if($arProps_rest["opening_hours"]["VALUE"][$i+1]!="") $OPEN.=", ";
 				$i++;
 			}
 			
 			$i=0;
 			foreach($arProps_rest["subway"]["VALUE"] as $T){
 				$res_metro = CIBlockElement::GetByID($T);
				if($ob_metro = $res_metro->GetNext()){
					$tar=explode("(", $ob_metro["NAME"]);
					$METRO.=trim($tar[0]);
				}
 				if($arProps_rest["subway"]["VALUE"][$i+1]!="") $METRO.=", ";
 				$i++;
 			}

 			
 			$EXIT["RESTORAN_INFO"]='';
 			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Телефон:</b> '.$TEL.'</div>';
 			if($arProps_rest["real_tel"]["VALUE"]!="") $EXIT["RESTORAN_INFO"].='<div class="il"><b>Реальный телефон:</b> '.$arProps_rest["real_tel"]["VALUE"].'</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Адрес:</b> '.$ADDR.' </div>';
			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Метро:</b> '.$METRO.'</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il spec_color"><b>Сотрудничество:</b> '; 
			
			if($arProps_rest["rabotaem"]["VALUE"]=="Y") $EXIT["RESTORAN_INFO"].='Работаем';
			else  $EXIT["RESTORAN_INFO"].='НЕ Работаем';
			
			$EXIT["RESTORAN_INFO"].='</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il"><b>Время работы:</b> '.$OPEN.'</div>';
			$EXIT["RESTORAN_INFO"].='<div class="il2"><b>Пометки:</b><br/> '.$arProps_rest["operator_comment"]["VALUE"].'</div>';
 		}
 		
 		//теперь берем инфу по клиенту
 		if($arProps["client"]["VALUE"]>0){
 			$res = CIBlockElement::GetByID($arProps["client"]["VALUE"]);
			if($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();  
		 		$arProps = $ob->GetProperties();
 		
 				$EXIT["CLIENT_ID"] = $arFields["ID"];
	 			//var_dump($arProps);
 				$EXIT["NAME"] = $arProps["NAME"]["VALUE"];
 				$EXIT["SURNAME"] = $arProps["SURNAME"]["VALUE"];
 				$EXIT["EMAIL"] = $arProps["EMAIL"]["VALUE"];
 				$EXIT["TELEPHONE"] = $arProps["TELEPHONE"]["VALUE"];
		 		$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
 				
 				$EXIT["PRIM"]=str_replace("<br />", "", $EXIT["PRIM"]);
 		
		 		$EXIT["ORDERS"]='<div class="tbl">';
 				$EXIT["ORDERS"].='<table cellpadding="0" cellspacing="0">';
		 		$EXIT["ORDERS"].='<tr class="hd">';
				$EXIT["ORDERS"].='<th class="c11">ресторан</th>';
				$EXIT["ORDERS"].='<th class="c12">гостей</th>';
				$EXIT["ORDERS"].='<th class="c13">визит</th>';
				$EXIT["ORDERS"].='<th class="c14">статус</th>';
				$EXIT["ORDERS"].='</tr>';
 		
 				//Теперь нужно запросить все заказы для этого пользователя
 				//Эту часть нужно будет закэшировать
 				$HO=false;
				$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_client"=>$arFields["ID"], "SECTION_CODE" => $_SESSION["CITY"]);
				$res_zak = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter, false, false, false);
				while($ob_zak= $res_zak->GetNextElement()){
  					$arFields_zak = $ob_zak->GetFields();
  					$arProps_zak = $ob_zak->GetProperties();
  			
  					$arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", MakeTimeStamp($arFields_zak["ACTIVE_FROM"], CSite::GetDateFormat()));
  			
  					$resr = CIBlockElement::GetByID($arProps_zak["rest"]["VALUE"]);
					if($arFields_rest = $resr->GetNext()){
						$REST_NAME = $arFields_rest["NAME"];
		
					}else $REST_NAME="Подбор";

  			
  			
  					$EXIT["ORDERS"].='<tr id="order_'.$arFields_zak["ID"].'">';
					$EXIT["ORDERS"].='<td class="c11"><div>&nbsp;'.$REST_NAME.'</div></td>';
					$EXIT["ORDERS"].='<td class="c12"><div>&nbsp;'.$arProps_zak["guest"]["VALUE"].'</div></td>';
					$EXIT["ORDERS"].='<td class="c13"><div>&nbsp;'.$arFields_zak["ACTIVE_FROM"].'</div></td>';
					$EXIT["ORDERS"].='<td class="c14"><div>&nbsp;'.$arProps_zak["status"]["VALUE"].'</div></td>';
					$EXIT["ORDERS"].='</tr>';
			
					$HO=true;		
	
				}
		
				$EXIT["ORDERS"].='</table>';
				$EXIT["ORDERS"].='</div>';
		
				if(!$HO) $EXIT["ORDERS"]="";
			
			}	
 		}else{
 			//Посмотрим, может быть это пользователь с сайта
 			if($arProps["user"]["VALUE"]>0){
 				$rsUser = CUser::GetByID($arProps["user"]["VALUE"]);
				$arUser = $rsUser->Fetch();
				
				

 				$EXIT["CLIENT_ID"] = "";
	 			//var_dump($arUser);
	 			
 				$EXIT["NAME"] = $arUser["NAME"];
 				$EXIT["SURNAME"] = $arUser["LAST_NAME"];
 				$EXIT["EMAIL"] = $arUser["EMAIL"];
 				$EXIT["TELEPHONE"] = $arUser["PERSONAL_PHONE"];
		 		$EXIT["PRIM"] = "Пользователь сайта. Зарегистрирован ".$arUser["DATE_REGISTER"].". \n".$arUser["WORK_NOTES"];
 				$EXIT["PRIM"] = str_replace("<br />", "", $EXIT["PRIM"]);
 				$EXIT["ORDERS"]="";
 			}
 		
 		}	
		
	}
	
	

	echo json_encode($EXIT);
}

function load_rest_filter(){
	global $APPLICATION;

	if($_REQUEST["fltr"]=="#type"){
		
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/rest_fltr_type.php"),
			Array(),
			Array("MODE"=>"php")
		);
	}
	
	if($_REQUEST["fltr"]=="#props"){
		
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/rest_fltr_props.php"),
			Array(),
			Array("MODE"=>"php")
		);
	}
	
	if($_REQUEST["fltr"]=="#location"){
		
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/rest_fltr_location.php"),
			Array(),
			Array("MODE"=>"php")
		);
	}
	
	if($_REQUEST["fltr"]=="#intertaim"){
		
		$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/rest_fltr_intertaim.php"),
			Array(),
			Array("MODE"=>"php")
		);
	}
}

//нужно сделать проверку на разрешение удаления отчета
function remove_report(){
	global $APPLICATION;
	$ELEMENT_ID = str_replace("#", "", $_REQUEST["report_id"]);
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	if(CIBlockElement::Delete($ELEMENT_ID)) echo "ok";
	

}


function search_rest_by_name(){
	global $APPLICATION;
	global $arrFilter;
	
	$arrFilter["ACTIVE"]="Y";
	
	$arrFilter["NAME"]="%".urldecode($_REQUEST["name"])."%";
	$arrFilter["IBLOCK_CODE"]=$_SESSION["CITY"];
	//$arrFilter["SECTION_CODE"]="restaurants";
	
	//var_dump($arrFilter);

	$APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "restorans_list", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "",
	"SECTION_ID" => "",
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "ID",
	"ELEMENT_SORT_ORDER" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-2,
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "phone",
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "Y",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "win_pager",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);

}



function filtr_rest(){
	global $arrFilter;
	global $APPLICATION;
	
	$arrFilter=array();
	
	foreach($_REQUEST as $CODE=>$V){
		//Фильтр по метро
		if(substr_count($CODE, "metro")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_subway"][]=$t[1];
		}
		
		if(substr_count($CODE, "type")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_type"][]=$t[1];
		}
		
		if(substr_count($CODE, "ideal")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_ideal_place_for"][]=$t[1];
		}
		
		if(substr_count($CODE, "music")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_music"][]=$t[1];
		}
		
		if(substr_count($CODE, "parking")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_parking"][]=$t[1];
		}
		
		
		if(substr_count($CODE, "kitchen")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_kitchen"][]=$t[1];
		}
		
		if(substr_count($CODE, "average_bill")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_average_bill"][]=$t[1];
		}
		
		
		if(substr_count($CODE, "children")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_children"][]=$t[1];
		}
		
		
		if(substr_count($CODE, "features")){
			$t = explode("_", $CODE);
			$arrFilter["PROPERTY_features"][]=$t[1];
		}
	
	}
	
	
	
	//$arrFilter["IBLOCK_CODE"] = $_SESSION["CITY"];
	//$arrFilter["PROPERTY_SUBWAY"] = 184378;
	
	if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
	if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
	
	
	
	if($_REQUEST["rzdl"]=="#location"){
		$_SESSION["REST_FLTR"]["PROPERTY_subway"] = $arrFilter["PROPERTY_subway"];
	}
	
	if($_REQUEST["rzdl"]=="#type"){
		$_SESSION["REST_FLTR"]["PROPERTY_type"] = $arrFilter["PROPERTY_type"];
		$_SESSION["REST_FLTR"]["PROPERTY_ideal_place_for"] = $arrFilter["PROPERTY_ideal_place_for"];
		$_SESSION["REST_FLTR"]["PROPERTY_music"] = $arrFilter["PROPERTY_music"];
		$_SESSION["REST_FLTR"]["PROPERTY_parking"] = $arrFilter["PROPERTY_parking"];
	}
	
	if($_REQUEST["rzdl"]=="#props"){
		$_SESSION["REST_FLTR"]["PROPERTY_kitchen"] = $arrFilter["PROPERTY_kitchen"];
		$_SESSION["REST_FLTR"]["PROPERTY_average_bill"] = $arrFilter["PROPERTY_average_bill"];
	}
	
	if($_REQUEST["rzdl"]=="#intertaim"){
		$_SESSION["REST_FLTR"]["PROPERTY_features"] = $arrFilter["PROPERTY_features"];
		$_SESSION["REST_FLTR"]["PROPERTY_children"] = $arrFilter["PROPERTY_children"];
	}
	
	foreach($_SESSION["REST_FLTR"] as $M=>$V) if($V=="") unset($_SESSION["REST_FLTR"][$M]);
	
	
	$arrFilter=$_SESSION["REST_FLTR"];
	$arrFilter["SECTION_CODE"]="restaurants";
	
	
	$arrFilter["ACTIVE"]="Y";
	//var_dump($arrFilter);
	
	$APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "restorans_list", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $IBLOCK_ID,
	"SECTION_ID" => "",
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "ID",
	"ELEMENT_SORT_ORDER" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-2,
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "phone",
		1=> "subway"
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "Y",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "win_pager",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);


}

function check_new(){
	//$_SERVER["DOCUMENT_ROOT"];
	

	$EXIT=array();
	$EXIT["SMS"]=0;
	$EXIT["ORDERS"]=0;
	
	
	$files = scandir($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders");    //сканируем (получаем массив файлов)
    array_shift($files); // удаляем из массива '.'
   	array_shift($files);
     	
    
    //if($_SESSION["CITY"]=="msk")
	//if($_SESSION["CITY"]=="spb") 	
	$CO_NEW=0;
    foreach($files as $f){
    	
    	$file_array = file($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/orders/".$f);
    	if($file_array[0]==$_SESSION["CITY"]){
    		$CO_NEW++;
    	}
    	//var_dump($file_array[0]);
    } 	
     	
	$EXIT["ORDERS"]=$CO_NEW;
	
	echo json_encode($EXIT);

}


function add_rest_form(){
	global $APPLICATION;
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/add_rest_form.php"),
		Array(),
		Array("MODE"=>"php")
	);
}


function edite_rest_form(){
	global $APPLICATION;
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}


	$ELEMENT_ID = str_replace("#", "", $_REQUEST["id"]);

	$res = CIBlockElement::GetByID($ELEMENT_ID);
	if($ob = $res->GetNextElement()){
  		$arProps = $ob->GetProperties();
  		//var_dump($arProps["rabotaem"]);
	}

	global $APPLICATION;
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/edite_rest_form.php"),
		Array("VNINFO"=>$arProps["vn_info"]["VALUE"], "POMETKI"=>$arProps["pometki"]["VALUE"], "ID"=>$ELEMENT_ID, "RABOTAEM"=>$arProps["rabotaem"]["VALUE"],"REAL_TEL"=>$arProps["real_tel"]["VALUE"],),
		Array("MODE"=>"php")
	);
}


function edite_rest(){
	global $APPLICATION;
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("pometki" => $_REQUEST["pometki"]));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("vn_info" => $_REQUEST["vninfo"]));
	CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("real_tel" => $_REQUEST["real_tel"]));
	
	if($_REQUEST["rabotaem"]!=""){
		if($_SESSION["CITY"]=="msk") $PROP["rabotaem"] = 1547;
		if($_SESSION["CITY"]=="spb") $PROP["rabotaem"] = 1557;
	
		CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], false, array("rabotaem" => $PROP["rabotaem"]));
	}else 
		CIBlockElement::SetPropertyValueCode($_REQUEST["id"], "rabotaem", false);	
}

/************ДОБАВЛЯЕМ НОВЫЙ РЕСТОРАН***************/
function add_new_rest(){
	global $APPLICATION;
	global $USER;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	//нужно получить id инфоблока с городом
	$arrFilter["IBLOCK_CODE"]=$_SESSION["CITY"];
	
	
	$res = CIBlock::GetList(Array(), Array('TYPE'=>'catalog', 'SITE_ID'=>SITE_ID, 'ACTIVE'=>'Y', "CODE"=>$_SESSION["CITY"]), true);
	if($REST_IB = $res->Fetch()){
		
	}
	$PROP = array();
	
	if($_SESSION["CITY"]=="msk") $PROP["rabotaem"] = 1547;
	if($_SESSION["CITY"]=="spb") $PROP["rabotaem"] = 1557;
	
	
	if($_SESSION["CITY"]=="msk") $PROP["operator_add"] = 1924;
	if($_SESSION["CITY"]=="spb") $PROP["operator_add"] = 1923;
	
	
	if($_SESSION["CITY"]=="msk") $SID = 32;
	if($_SESSION["CITY"]=="spb") $SID = 103;

	
	$el = new CIBlockElement;

	
	$PROP["address"] = $_REQUEST["address"];       
	$PROP["phone"] = $_REQUEST["telephone"]; 
	$PROP["subway"] = $_REQUEST["metro"]; 
	$PROP["kitchen"] = $_REQUEST["cook"]; 
	$PROP["opening_hours"] = $_REQUEST["time"]; 
	$PROP["pometki"] = $_REQUEST["pometki"]; 
	$PROP["vn_info"] = $_REQUEST["vninfo"]; 
	

	//$arrFilter["SECTION_CODE"]="restaurants";
	
	$arLoadProductArray = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"IBLOCK_ID"=>$REST_IB["ID"],
		"IBLOCK_SECTION_ID" => $SID,         
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $_REQUEST["name"],
		"ACTIVE"         => "Y",            // активен
  		"CODE"         => translitIt($_REQUEST["name"])."_operator",  
  	);
  	
  	
  	//var_dump($arLoadProductArray);
  	
	
	if($REST_ID = $el->Add($arLoadProductArray))
  		echo "ok";
	else
  		echo "Error: ".$el->LAST_ERROR;
	

}


function sms_otvet_form(){
	global $APPLICATION;
	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/sms_otvet_form.php"),
		Array("ID"=>$_REQUEST["id"]),
		Array("MODE"=>"php")
	);
}


function send_sms_otvet(){
	global $APPLICATION;
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$res = CIBlockElement::GetByID($_REQUEST["id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps = $ob->GetProperties();
 		
 		
 		$TXT='<?xml version="1.0" encoding="UTF-8" ?>
		<data>
		<authorization>
			<login>restoran_1223</login>
			<password>d3Csa23w94</password>
		</authorization>
		<type>send</type>
		<request>
			<transaction_id>'.$arProps["transaction_id"]["VALUE"].'</transaction_id>
			<message>'.$_REQUEST["otvet_text"].'</message>
		</request>
		</data>';
 		
 		//var_dump($TXT);
 		
 		//инициализируем сеанс
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://gate.ias.su/baseapi/');
		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $TXT);
		$res = curl_exec($curl);
 			
 		var_dump($res);
 		
 		
 		CIBlockElement::SetPropertyValuesEx($_REQUEST["id"], 149, array("new" => ""));
 		//удаляем файл
 		@unlink($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/sms/".$ID);
 		
	}
}


function get_client_by_sms(){
	global $APPLICATION;
	$_REQUEST["id"] = str_replace("sms_", "", $_REQUEST["id"]);
	$res = CIBlockElement::GetByID($_REQUEST["id"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$SMSn = trim(str_replace("+7", "", $arFields["NAME"]));
 	}
	
	
	
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	//var_dump($SMSn);
	
	$EXIT = array();
	$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y", "PROPERTY_telephone"=>"%".$SMSn."%");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();  
 		$arProps = $ob->GetProperties();
 		
 		$EXIT["CLIENT_ID"] = $arFields["ID"];
 		
 		$EXIT["NAME"] = $arProps["NAME"]["VALUE"];
 		$EXIT["SURNAME"] = $arProps["SURNAME"]["VALUE"];
 		$EXIT["EMAIL"] = $arProps["EMAIL"]["VALUE"];
 		$EXIT["TELEPHONE"] = $arProps["TELEPHONE"]["VALUE"];
 		$EXIT["PRIM"] = $arFields["PREVIEW_TEXT"];
 		
 		$EXIT["ORDERS"]='<div class="tbl">';
 		$EXIT["ORDERS"].='<table cellpadding="0" cellspacing="0">';
 		$EXIT["ORDERS"].='<tr class="hd">';
		$EXIT["ORDERS"].='<th class="c11">ресторан</th>';
		$EXIT["ORDERS"].='<th class="c12">гостей</th>';
		$EXIT["ORDERS"].='<th class="c13">визит</th>';
		$EXIT["ORDERS"].='<th class="c14">статус</th>';
		$EXIT["ORDERS"].='</tr>';
 		
 		//Теперь нужно запросить все заказы для этого пользователя
 		//Эту часть нужно будет закэшировать
 		$HO=false;
		$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_client"=>$arFields["ID"]);
		$res_zak = CIBlockElement::GetList(Array("ACTIVE_FROM"=>"DESC"), $arFilter, false, false, false);
		while($ob_zak= $res_zak->GetNextElement()){
  			$arFields_zak = $ob_zak->GetFields();
  			$arProps_zak = $ob_zak->GetProperties();
  			
  			$arFields_zak["ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat("d.m.Y в H:i", MakeTimeStamp($arFields_zak["ACTIVE_FROM"], CSite::GetDateFormat()));
  			
  			$resr = CIBlockElement::GetByID($arProps_zak["rest"]["VALUE"]);
			if($arFields_rest = $resr->GetNext()){
				$REST_NAME = $arFields_rest["NAME"];
		
			}else $REST_NAME="&nbsp;";

  			
  			
  			$EXIT["ORDERS"].='<tr id="order_'.$arFields_zak["ID"].'">';
			$EXIT["ORDERS"].='<td class="c11"><div>&nbsp;'.$REST_NAME.'</div></td>';
			$EXIT["ORDERS"].='<td class="c12"><div>&nbsp;'.$arProps_zak["guest"]["VALUE"].'</div></td>';
			$EXIT["ORDERS"].='<td class="c13"><div>&nbsp;'.$arFields_zak["ACTIVE_FROM"].'</div></td>';
			$EXIT["ORDERS"].='<td class="c14"><div>&nbsp;'.$arProps_zak["status"]["VALUE"].'</div></td>';
			$EXIT["ORDERS"].='</tr>';
			
			$HO=true;		
	
		}
		
		$EXIT["ORDERS"].='</table>';
		$EXIT["ORDERS"].='</div>';
		
		if(!$HO) $EXIT["ORDERS"]="";
		
	}	
	
	echo json_encode($EXIT);



}


function clear_rest_filter(){
	global $APPLICATION;
	unset($_SESSION["REST_FLTR"]);

	if($_SESSION["CITY"]=="spb") $IBLOCK_ID=12;
	if($_SESSION["CITY"]=="msk") $IBLOCK_ID=11;
	
	$APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "restorans_list", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $IBLOCK_ID,
	"SECTION_ID" => "",
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "ID",
	"ELEMENT_SORT_ORDER" => "DESC",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-2,
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "phone",
		1=> "subway"
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "Y",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "win_pager",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);


}



function clear_orders_filter(){
	global $APPLICATION;

	unset($_SESSION["ALL_ORDERS_FLTR"]);

	$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/all_orders.php"),
		Array(),
		Array("MODE"=>"php")
	);
	
}

function filter_orders(){
	
	global $arrFilter;
	global $APPLICATION;
	
	$arrFilter=array();

	//unset($_SESSION["ALL_ORDERS_FLTR"]);
	if($_REQUEST["date_po"]!="") $_REQUEST["date_po"] = date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po"])));
	if($_REQUEST["date_po2"]!="") $_REQUEST["date_po2"] = date( 'd.m.Y' ,strtotime('+1 day', strtotime($_REQUEST["date_po2"])));
	
	if($_REQUEST["restoran_id"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"] && $_REQUEST["restoran_id"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]=$_REQUEST["restoran_id"];
	if($_REQUEST["source"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_source"] && $_REQUEST["source"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_source"]=$_REQUEST["source"];
	
	if($_REQUEST["status"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"] && $_REQUEST["status"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]=$_REQUEST["status"];
	if($_REQUEST["operator"]!=$_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"] && $_REQUEST["operator"]!="") $_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]=$_REQUEST["operator"];
	
	if($_REQUEST["type"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_type"] && $_REQUEST["type"]!="") $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_type"]=$_REQUEST["type"];
	
	if($_REQUEST["date_s"]!=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_ACTIVE_FROM"] && $_REQUEST["date_s"]!="") $_SESSION["ALL_ORDERS_FLTR"][">=DATE_ACTIVE_FROM"]=$_REQUEST["date_s"];
	if($_REQUEST["date_po"]!=$_SESSION["ALL_ORDERS_FLTR"]["<=DATE_ACTIVE_FROM"] && $_REQUEST["date_po"]!="") $_SESSION["ALL_ORDERS_FLTR"]["<=DATE_ACTIVE_FROM"]=$_REQUEST["date_po"];
	
	if($_REQUEST["date_s2"]!=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"] && $_REQUEST["date_s2"]!="") $_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]=$_REQUEST["date_s2"];
	if($_REQUEST["date_po2"]!=$_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"] && $_REQUEST["date_po2"]!="") $_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]=$_REQUEST["date_po2"];
	
	if($_REQUEST["control_call"]!=$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_control_call"] && $_REQUEST["control_call"]!=""){
		$_SESSION["ALL_ORDERS_FLTR"][">=PROPERTY_control_call"]=ConvertDateTime($_REQUEST["control_call"], "YYYY-MM-DD")." 00:00:00";
		$_SESSION["ALL_ORDERS_FLTR"]["<=PROPERTY_control_call"]=ConvertDateTime($_REQUEST["control_call"], "YYYY-MM-DD")." 23:59:59";
	}
	
	
	//$newdate = strtotime ('-1 day', strtotime($_REQUEST["date_po"])) ;
	
	//$newdate = date( 'd.m.Y' ,strtotime('-1 day', strtotime($_REQUEST["date_po"])));
	
	foreach($_SESSION["ALL_ORDERS_FLTR"] as $M=>$V) if($V=="") unset($_SESSION["ALL_ORDERS_FLTR"][$M]);
	
	
	
		
	if($_SESSION["ALL_ORDERS_FLTR"]) $arrFilter=$_SESSION["ALL_ORDERS_FLTR"];

//var_dump($arrFilter);
$APPLICATION->IncludeComponent("bitrix:catalog.section_notact", "orders_list_all", array(
	"IBLOCK_TYPE" => "booking_service",
	"IBLOCK_ID" => "105",
	"SECTION_ID" => "",
	"SECTION_CODE" => $_SESSION["CITY"],
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $_SESSION["ORDERS_ORDER"],
	"ELEMENT_SORT_ORDER" => $_SESSION["ORDERS_BY"],
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"PAGE_ELEMENT_COUNT" => $_SESSION["CLIENTS_PAGE_CO"]-4,
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "guest",
		1 => "rest",
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "Y",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "win_pager",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"CBY_F"=>"Y"
	),
	false
);
}

if($_REQUEST["act"]=="clear_orders_filter") clear_orders_filter();
if($_REQUEST["act"]=="filter_orders") filter_orders();
if($_REQUEST["act"]=="clear_rest_filter")  clear_rest_filter();
if($_REQUEST["act"]=="get_client_by_sms") get_client_by_sms();
if($_REQUEST["act"]=="send_sms_otvet") send_sms_otvet();
if($_REQUEST["act"]=="sms_otvet_form") sms_otvet_form();

if($_REQUEST["act"]=="check_new") check_new();

if($_REQUEST["act"]=="get_rest_detailed_info") get_rest_detailed_info();
if($_REQUEST["act"]=="load_rest_filter") load_rest_filter();

if($_REQUEST["act"]=="get_client_info_by_order") get_client_info_by_order();
if($_REQUEST["act"]=="get_order_info") get_order_info();
if($_REQUEST["act"]=="get_restoran_info") get_restoran_info();
if($_REQUEST["act"]=="search_restoran") search_restoran();
if($_REQUEST["act"]=="add_new_client") add_new_client();
if($_REQUEST["act"]=="search_client_by_email") search_client_by_email();
if($_REQUEST["act"]=="search_client_by_telephone") search_client_by_telephone();
if($_REQUEST["act"]=="search_client_by_name") search_client_by_name();
if($_REQUEST["act"]=="search_client_by_surname") search_client_by_surname();
if($_REQUEST["act"]=="update_client") update_client();
if($_REQUEST["act"]=="change_razdel") change_razdel();
if($_REQUEST["act"]=="get_client_info") get_client_info();
if($_REQUEST["act"]=="change_page") change_razdel(); 
if($_REQUEST["act"]=="resort_clients") change_razdel(); 
if($_REQUEST["act"]=="clear_pos") clear_pos(); 
if($_REQUEST["act"]=="save_win_pos") save_win_pos();
if($_REQUEST["act"]=="user_auth") user_auth();

if($_REQUEST["act"]=="remove_report") remove_report();
if($_REQUEST["act"]=="search_rest_by_name") search_rest_by_name();
if($_REQUEST["act"]=="filtr_rest") filtr_rest();


if($_REQUEST["act"]=="add_new_rest") add_new_rest();
if($_REQUEST["act"]=="add_rest_form") add_rest_form();


if($_REQUEST["act"]=="edite_rest_form") edite_rest_form();
if($_REQUEST["act"]=="edite_rest") edite_rest();
?>