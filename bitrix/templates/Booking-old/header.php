<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>

	<script type="text/javascript">
		google.load("jquery", "1");
		google.load("jqueryui", "1");
	</script>
	
	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle()?></title>

	<? 
	if($USER->IsAuthorized()){
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.maskedinput-1.3.min.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.ui.datepicker-ru.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jQuery.fileinput.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.form.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/cookies.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/jquery.typewatch.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/java.js');
		
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w1.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w2.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w3.js');
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/w4.js');

		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/inputs.js');
	}else{
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/java/auth.js');
	}
	?>
	
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
<div id="conteiner">
	<div id="ajax_loader"></div>
	<?if ($USER->IsAuthorized()){?>
		<div class="center" id="content">
		<div class="logo"></div>
		<div class="user">Здравствуйте, <?=$USER->GetFullName()?>!   <a href="/bs/?logout=yes">Выйти</a></div>
		<div id="position">
			<a href="#" class="clear">Сбросить расположение окон</a>
		</div>
		
		<?
		//if($_SESSION["CITY"]==""){
			$rsUser = CUser::GetByID($USER->GetID());
			$arUser = $rsUser->Fetch();
			
			$_SESSION["CITY"]=$arUser["UF_CITY"];
			if($_SESSION["CITY"]=="") $_SESSION["CITY"]="msk";
		//}
		//var_dump($_SESSION["CITY"]);
		$arGroups = CUser::GetUserGroup($USER->GetID());
		if(in_array(19, $arGroups)>0 || $USER->IsAdmin()){

		if($_REQUEST["new_city"]!="" && $_SESSION["CITY"] != $_REQUEST["new_city"]) $_SESSION["CITY"] = $_REQUEST["new_city"];
		
		if($_SESSION["CITY"]=="msk") $CTY="Москва";
		if($_SESSION["CITY"]=="spb") $CTY="Санкт-Петербург";
		
		//var_dump($_SESSION["CITY"]);
		?>
		<form id="city_pos" action="/bs/" method="post">
			<input type="hidden" name="city" value=""/>
			<div class="select select_off" id="city" style="z-index: 800;">
				<div class="selected_element"><?=$CTY?></div>
				<div class="all_variants">
					<a href="msk" rel="nofollow" class="">Москва</a>
					<a href="spb" rel="nofollow" class="">Санкт-Петербург</a>
				</div>
			</div>
		</form>
		<?}?>
		<div id="dialog-confirm" title="">
			
		</div>
		
	<?}?>