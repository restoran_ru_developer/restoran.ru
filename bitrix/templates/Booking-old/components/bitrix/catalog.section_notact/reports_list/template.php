<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if(count($arResult["ITEMS"])>0){?>				
<div class="tbl reports">
<table cellpadding="0" cellspacing="0">
<?
$now_date="";
?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
	<?
	$rsUser = CUser::GetByID($arElement["CREATED_BY"]);
	$arUser = $rsUser->Fetch();
	///var_dump($arUser);
	$arElement["CREATED_BY"]=$arUser["LAST_NAME"]." ".$arUser["NAME"];
	
	$tar = explode(" ", $arElement["DATE_CREATE"]);
	$arElement["DATE_CREATE"]=$tar[0];
	
	//var_dump($arElement["PROPERTIES"]);
	?>
	<tr id="report_<?=$arElement["ID"]?>">
		<td class="c000"><div><a href="<?=CFile::GetPath($arElement["PROPERTIES"]["xls"]["VALUE"])?>" target="_blank" class="xls_icon" title="Скачать"></a></div></td>
		<td class="c111"><div>&nbsp;<?=$arElement["NAME"]?></div></td>
		<td class="c222"><div>&nbsp;<?=$arElement["DATE_CREATE"]?></div></td>
		<td class="c333"><div>&nbsp;<?=$arElement["CREATED_BY"]?></div></td>
		<td class="c444"><div><a href="#<?=$arElement["ID"]?>" class="remove_report" title="Удалить отчет"></a></div></td>
	</tr>
<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>
</table>					
</div>
<?}?>
<?if($arResult["NAV_STRING"]!=""){?>		
<div class="paginator">
	<?=$arResult["NAV_STRING"]?>
</div>
<?}?>