<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>

	
	
	<div class="auth_frm_cont">
	<span class="auth_frm_logo"></span>
	<form action="<?=$arResult["AUTH_URL"]?>" id="auth_form">
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>
		
		<div class="c">
			<div class="ln"><div class="zag">Логин</div><div class="inpc"><input type="text" name="USER_LOGIN" class="pole" value="<?=$arResult["LAST_LOGIN"]?>"/></div></div>
			<div class="ln"><div class="zag">пароль</div><div class="inpc"><input type="password" name="USER_PASSWORD" class="pole"/></div></div>
			<?if($arResult["CAPTCHA_CODE"]):?>
				<div class="ln"><div class="zag">Код</div>
					<div class="inpc">
						<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
						<input type="text" name="captcha_word" class="pole" />
					</div>
					
				</div>
				
				<div class="ln"><div class="zag"></div>
					<div class="inpc">
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					</div>
					
				</div>
			<?endif;?>
			<div class="but">
				<input type="submit" class="btn_enter" value="Войти"/>
			</div>
		</div>	
	</form>
	<div class="otvet"></div>
	</div>
	
<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>

