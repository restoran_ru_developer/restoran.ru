<?
if(is_array($POS)){
	$CSS = "";
	foreach($POS as $key=>$val) if($val!="") $CSS.=$key.':'.$val.'px;';
}

function build_list($code, $z_index, $selected, $COLORS){
	$ELS=array();
	$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC","ID"=>"ASC"), Array("IBLOCK_ID"=>105, "CODE"=>$code));
	while($enum_fields = $property_enums->GetNext()){
  		$ELS[]=array("ID"=>$enum_fields["ID"], "VALUE"=>$enum_fields["VALUE"]);
	}
	
	if($selected==""){
	 	$selected=$ELS[0]["VALUE"];
		$sid=$ELS[0]["ID"];
	}
	
	?>
	<input type="hidden" name="<?=$code?>" value="<?=$sid?>"/>
	<div class="select select_off" id="<?=$code?>" style="z-index: <?=$z_index?>;">
		<div class="selected_element <?=$COLORS[0]?>"><?=$selected?></div>
		<div class="all_variants">
		<?$c=0;?>
		<?foreach($ELS as $E){?>
			<a href="<?=$E["ID"]?>" rel="nofollow" class="<?=$COLORS[$c]?>"><?=$E["VALUE"]?></a>
			<?
			$c++;
			?>
		<?}?>
		</div>
	</div>
<?}?>

<?
$arGroups = CUser::GetUserGroup($USER->GetID());

//var_dump($arGroups);
?>

<div class="window pos3 left_col"  id="w3" style="<?=$CSS?>">
			<div class="head">
				<div class="menu">Заказ</div>
				<span class="drag_icon"></span>
			</div>
			<div class="content">
				
				<div class="ln ln_sel" style="z-index:3000;">
					<div class="zg">Тип</div>
					<div class="inpc">
						<? build_list("type", 800, "&nbsp;");?>
					</div>
				</div>
			
				<div class="ln" style="z-index:1000;">
					<div class="zg">ресторан</div>
					<div class="inpc">
						<input type="hidden"  name="rastoran_id" value=""/>
						<input type="text" class="pole ugolok" name="rastoran" />
						<div class="variants"></div>
					</div>
				</div>
				<div class="rinfo" style="z-index:990;">

				</div>
				<input type="hidden"  name="order_id" value=""/>
				<input type="hidden" name="order_changed" value="N" id="order_changed"/>
				<div class="ln" style="z-index:980;">
					<div class="zg">дата</div>
					<div class="inpc"><input type="text" class="pole ugolok" name="date" id="order_date"/></div>
				</div>
				
				<div class="ln" style="z-index:980;">
					<div class="zg2 zg">контрольный звонок</div>
					<div class="inpc"><input type="text" class="pole ugolok" name="control_call" id="control_call"/></div>
				</div>
				
				<div class="ln ln4">
					<div class="bl_l">
						<div class="zg">время</div>
						<div class="inpc"><input type="text" class="pole" name="time" id="time"/></div>
					</div>
					
					<div class="bl_r">
						<div class="zg">гостей</div>
						<div class="inpc"><input type="text" class="pole" name="guest" /></div>
					</div>
					
				</div>
				
				<div class="ln" style="display:none;" id="format_m">
					<div class="zg zg2">формат<br/>мероприятия</div>
					<div class="inpc"><input type="text" class="pole" name="format" /></div>
				</div>
				
				<div class="ln ln3">
					<div class="zg">примечание</div>
				</div>
				
				<div class="ln2">
					<textarea name="prim_order" class="pole"></textarea>
				
				</div>
				
				<div class="ln ln5" style="display:none;">
					<b>Оператор:</b> <span id="operator"></span>
				</div>
				
				<div class="ln">
					<div class="zg">принял заказ</div>
					<div class="inpc"><input type="text" class="pole" name="prinyal" /></div>
				</div>
				
				<div class="ln ln_sel" style="z-index:2000;">
					<div class="zg">источник</div>
					<div class="inpc">
						<? build_list("source", 800, "&nbsp;");?>
					</div>
				</div>
				
				
				<div class="ln ln_sel"  style="z-index:1000;">
					<div class="zg">статус</div>
					<div class="inpc">
						<? build_list("status", 700, "", array("yello", "sv_zel","zel","red","red","black","yello","blue", "blue","blue","red","red", "grey"));?>
					</div>
				</div>
				
				
				<div class="ln" style="z-index:980;">
					<div class="zg">Подтвердил заказ</div>
					<div class="inpc"><input type="text" class="pole" name="podtv" /></div>
				</div>
				
				<div class="ln ln4" style="z-index:970;">
					<div class="bl_l">
						<div class="zg ">Сумма счета</div>
						<div class="inpc"><input type="text" class="pole" name="summ_sch"/></div>
					</div>
					
					<div class="bl_r" style="z-index:960;">
						<div class="zg">Номер стола</div>
						<div class="inpc"><input type="text" class="pole" name="stol" /></div>
					</div>
					
				</div>
				
				<div class="ln ln4" style="z-index:920;">
					
					<div class="bl_l">
						<div class="zg ">Проценты</div>
						<div class="inpc"><input type="text" class="pole" name="procent"/></div>
					</div>
					
					<?if(in_array(17, $arGroups)>0){?>
					<div class="bl_r">
						<div class="zg">Деньги получены</div>
						<div class="inpc"><input type="checkbox" class="" name="dengi" value="Y"/></div>
					</div>
					<?}?>
				</div>
				
				<div class="ln">
					<div class="zg zg2">Подтвердил<br/>сумму/№ стола</div>
					<div class="inpc"><input type="text" class="pole" name="podtv2" /></div>
				</div>
				
				<div class="ln">
					<div class="zg">Создан</div>
					<div class="inpc"><span id="sozdan"></span></div>
				</div>
			</div>
		</div>