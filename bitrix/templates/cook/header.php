<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xmlns:og="http://ogp.me/ns#">
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <?//$APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true)?>
    <?//$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/cusel.css"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css?1"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadString('<link href="/tpl/css/styles.css?14376"  type="text/css" rel="stylesheet" />',true)?>
    <?//$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/datepicker.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="http://restoran.ru/min/f=tpl/css/autocomplete/jquery.autocomplete.css,bitrix/templates/cook/css/cusel.css,tpl/css/cusel_m.css,tpl/css/styles.css,bitrix/templates/cook/css/datepicker.css";  type="text/css" rel="stylesheet" />', true)?>    
    <?$APPLICATION->AddHeadScript('http://restoran.ru/min/f=tpl/js/jquery.js,tpl/js/detail_galery.js,bitrix/templates/cook/js/script.js,tpl/js/jquery.popup.js,bitrix/templates/cook/js/maskedinput.js,bitrix/templates/cook/js/tools.js,tpl/js/jquery.mousewheel.js,tpl/js/jScrollPane.js,tpl/js/cusel-multiple-min-0.9.js,tpl/js/cusel-min.js,tpl/js/radio.js,tpl/js/jquery.autocomplete.min.js')?>
    
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/detail_galery.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js')?>

    <?$APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js')?>            
    <?//$APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js')?>             
    <?//$APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js')?>    
    <?//$APPLICATION->AddHeadScript('/tpl/js/cusel-min.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/radio.js')?>
    <?//$APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js')?>
    
    
    
    
    
    <?//$APPLICATION->AddHeadScript('http://cdn.jquerytools.org/1.2.6/all/jquery.tools.min.js')?>
    <?//$APPLICATION->AddHeadScript('/bitrix/templates/kupon/js/cusel-min.js')?>
    
    <!--[if IE 7]><link href="/bitrix/templates/main/ie.css?2" rel="stylesheet" media="all" /><![endif]-->
    <!--[if IE 8]><link href="/bitrix/templates/main/ie.css?4" rel="stylesheet" media="all" /><![endif]-->
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
    <?        CModule::IncludeModule("advertising");    
    if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
        $page = "others";
    else
        $page = "main";
    CAdvBanner::SetRequiredKeywords (array(CITY_ID));
    ?>
</head>
<body>
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '297181676964377', // App ID
                channelURL : '//<?=SITE_SERVER_NAME?>/channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                oauth      : true, // enable OAuth 2.0
                xfbml      : true  // parse XFBML
            });
        };

        (function(d){
            var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            d.getElementsByTagName('head')[0].appendChild(js);
        }(document));
    </script>

    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div id="container">
        <div id="wrapp">            
            <div id="header">
                <div class="i_b_r">
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "top_main_page_".CITY_ID,
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                    false
                    );?>
                </div>            
                <div id="logo" class="left"><a href="/<?=CITY_ID?>/"><img src="/tpl/images/logo.png" /></a></div>                
                <div class="city_select left" style="margin-top:20px; margin-right:10px;width:140px">  
                    <div style="margin-right:15px; margin-bottom:16px;">
                        <?$APPLICATION->IncludeComponent(
                            "restoran:city.selector",
                            "city_select",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_URL" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "new2323",
                                "CACHE_GROUPS" => "N"
                            ),
                        false
                        );?>      
                        <div class="clear"></div>
                    </div>                    
                    <div style="margin-right:20px">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.site.selector",
                            "lang_selector",
                            Array(
                                "SITE_LIST" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "360000000",
                                "CACHE_NOTES" => ""
                            ),
                        false
                        );?>                    
                        <div class="clear"></div>
                    </div>                    
                </div>
                <div class="left" style="margin-top:20px; text-align: center; margin-right:12px;">
                        <div class="phones" style="line-height:22px;margin-bottom:12px;font-size:32px; width:130px;text-align:left;<?=(CITY_ID=="msk"||CITY_ID=="spb")?"background:url(/tpl/images/phone_min.png) left top no-repeat; ":""?>">
                            <?
                            $APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath("include_areas/top_phones_".CITY_ID.".php"),
                                Array(),
                                Array("MODE"=>"html")
                            );
                            ?>                            
                        </div>                        
                        <div>
                            
                        </div>
                </div>
                <div class="left" style="margin-top:22px;">
                   <?if (CITY_ID=="spb"||CITY_ID=="msk"):?>                        
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient2"  onclick="ajax_bron('<?=bitrix_sessid_get()?>')"><?=GetMessage("TABLE_BOOK")?></a>
                        </div>
                        <div class="stolik_shadow">
                            <a href="javascript:void(0)" class="bron_button_gradient1"  onclick="ajax_bron2('<?=bitrix_sessid_get()?>&banket=Y',1)"><?=GetMessage("BANKET_BOOK")?></a>
                        </div>
                    <?endif;?>
                </div>
                <div class="right" style="<?=($USER->IsAuthorized())?"":"margin-top:20px"?> width:150px; overflow:hidden; position:relative">   
                    <div class="top_middle_menu" style="margin-right:0px;">
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:system.auth.form",
                                "user",
                                Array(
                                        "REGISTER_URL" => "",
                                        "FORGOT_PASSWORD_URL" => "",
                                        "PROFILE_URL" => "",
                                        "SHOW_ERRORS" => "N"
                                ),
                        false
                        );?> 
                        <div class="clear"></div>
                        <?/*if (CITY_ID=="ast"||CITY_ID=="krd"||CITY_ID=="tmn"||CITY_ID=="ufa"||CITY_ID=="kld"):?>
                                <script>
                                function cities_bron(params)
                                {
                                    $.ajax({
                                        type: "POST",
                                        url: "/tpl/ajax/bron_<?=CITY_ID?>.php",
                                        data: params,
                                        success: function(data) {
                                            if (!$("#cbron_form").size())
                                            {
                                                $("<div class='popup popup_modal' id='cbron_form' style='width:335px'></div>").appendTo("body");                                                               
                                            }
                                            $('#cbron_form').html(data);
                                            showOverflow();
                                            setCenter($("#cbron_form"));
                                            $("#cbron_form").css("display","block"); 
                                        }
                                    });
                                }
                                </script>
                                <a href="javascript:void(0)" onclick="cities_bron('<?=bitrix_sessid_get()?>')" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                            <?else:*/?>
                        <div style="margin-top:5px;">
                                <?if ($USER->IsAdmin()||CSite::InGroup(Array(9,15,16))):?>
<!--                                    <a href="javascript:void(0)" id="add_rest" class="greyb"><?=GetMessage("ADD_PLACE")?></a>-->
                                    <a href="/restorator/" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?elseif ($USER->IsAuthorized()):?>
                                    <a href="/auth/register_restorator.php#restorator" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?else:?>
                                    <a href="/auth/register_restorator.php#restorator" class="greyb"><?=GetMessage("ADD_PLACE")?></a>
                                <?endif;?>
                            <?//endif;?>
                        </div>
                    </div>                                                                             
                </div>                            
                <?/*else:?>
                    <div class="phone_block right" onclick="ajax_bron('<?=bitrix_sessid_get()?>')"  style="margin-right:20px;">
                        <?=GetMessage("ORDER_BUBBLE_TEXT")?>
                    </div>
                <?endif;*/?>
                <div class="clear"></div>          
            </div>
            <div id="block960">
                    <?if (SITE_ID=="s1"):?>
                    <div class="left" style="<?=(CITY_ID=="tmn"||CITY_ID=="ufa")?'width:820px':'width:740px'?>">     
                    <?endif;?>                       
                        <div id="main_menu" <?if (SITE_ID=="s1"):?>style="<?=(CITY_ID=="tmn")?'width:820px':'width:740px'?>"<?endif;?>>
                                <ul>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top_1",
                                        Array(
                                            "ROOT_MENU_TYPE" => "top_1_new_".CITY_ID,
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "top_1",
                                            "USE_EXT" => "N",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_TIME" => "36000000",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                            "MENU_CACHE_GET_VARS" => array(CITY_ID,1)
                                        ),
                                    false
                                    );?>
                                </ul>
                        </div>
                        <div id="toptop_menu_new" style="margin:0px;<?=(CITY_ID=="tmn"||CITY_ID=="ufa")?'width:820px':'width:740px'?><?if (SITE_ID!="s1"):?>margin-bottom:10px<?endif;?>">
                            <ul>
                                <?$APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "top_small_2_new_2_11",
                                        Array(
                                                "ROOT_MENU_TYPE" => "art_".CITY_ID,
                                                "MAX_LEVEL" => "1",
                                                "CHILD_MENU_TYPE" => "",
                                                "USE_EXT" => "N",
                                                "DELAY" => "N",
                                                "ALLOW_MULTI_SELECT" => "N",
                                                "MENU_CACHE_TYPE" => "A",
                                                "MENU_CACHE_TIME" => "36000000",
                                                "MENU_CACHE_USE_GROUPS" => "N",
                                                "MENU_CACHE_GET_VARS" => array(CITY_ID,1)
                                        ),
                                false
                                );?> 
                            </ul>
                        </div>
                    <?if (SITE_ID=="s1"):?>
                    </div>
                        <div class="right"  style="<?=(CITY_ID=="tmn"||CITY_ID=="ufa")?"width:120px":"width:220px"?>">
                            <!--<div style="width:112px; height:90px; margin-top: -20px;">
                                <a id="ny_ded" href="javascript:void(0)" style="position:relative; text-decoration:none;cursor:default">    
                                    <div class="ny_round_64"></div>
                                    <div class="cur_s"></div>
                                    <div class="appetite appetite_ny_15">Новый Год!</div>
                                    <div id="ny_dm_92"></div>
                                </a>
                            </div>-->
                            <?if (CITY_ID!="tmn"):?>
                                <?if (CITY_ID=="msk"||CITY_ID=="spb"||CITY_ID=="kld"):?>
                                <div style="width:240px; height:90px; margin-top: -22px; margin-left:15px">
                                    <?/*if (CITY_ID=="msk"):?>
                                        <a href="/msk/catalog/restaurants/rubric/fashion/" title="лучшие рестораны Москвы"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/rest_best.png" alt="лучшие рестораны москвы" title="лучшие рестораны москвы" /></a>
                                    <?endif;?>
                                    <?if (CITY_ID=="spb"):?>
                                        <a href="/spb/catalog/restaurants/rubric/fashion/" title="лучшие рестораны Санкт-Петербурга"><img style="margin-top: 10px; margin-left: -10px;" src="/tpl/images/rest_best_spb.jpg" alt="лучшие рестораны Санкт-Петербурга" title="лучшие рестораны Санкт-Петербурга" /></a>
                                    <?endif;*/?>
                                    <?if (CITY_ID=="spb"||CITY_ID=="msk"||CITY_ID=="kld"):?>
                                            <?                                       
                                            $arBanIB = getArIblock("banner_in_header", CITY_ID);
                                            $APPLICATION->IncludeComponent(    
                                                "bitrix:news.list",
                                                "banner_in_header",
                                                Array(
                                                    "DISPLAY_DATE" => "Y",
                                                    "DISPLAY_NAME" => "Y",
                                                    "DISPLAY_PICTURE" => "N",
                                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                                    "AJAX_MODE" => "N",
                                                    "IBLOCK_TYPE" => "banner_in_header",
                                                    "IBLOCK_ID" => $arBanIB["ID"],
                                                    "NEWS_COUNT" => "10",
                                                    "SORT_BY1" => "SORT",
                                                    "SORT_ORDER1" => "ASC",
                                                    "SORT_BY2" => "",
                                                    "SORT_ORDER2" => "",
                                                    "FILTER_NAME" => "",
                                                    "FIELD_CODE" => array(),
                                                    "PROPERTY_CODE" => array(),
                                                    "CHECK_DATES" => "Y",
                                                    "DETAIL_URL" => "",
                                                    "PREVIEW_TRUNCATE_LEN" => "120",
                                                    "ACTIVE_DATE_FORMAT" => "j F Y",
                                                    "SET_TITLE" => "N",
                                                    "SET_STATUS_404" => "N",
                                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                    "ADD_SECTIONS_CHAIN" => "N",
                                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                    "PARENT_SECTION" => "",
                                                    "PARENT_SECTION_CODE" => "",
                                                    "CACHE_TYPE" => "Y",
                                                    "CACHE_TIME" => "14400",
                                                    "CACHE_FILTER" => "Y",
                                                    "CACHE_GROUPS" => "N",
                                                    "CACHE_NOTES" => "n",            
                                                    "DISPLAY_TOP_PAGER" => "N",
                                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                                    "PAGER_TITLE" => "Новости",
                                                    "PAGER_SHOW_ALWAYS" => "N",
                                                    "PAGER_TEMPLATE" => "",
                                                    "PAGER_DESC_NUMBERING" => "N",
                                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                    "PAGER_SHOW_ALL" => "N",
                                                    "AJAX_OPTION_JUMP" => "N",
                                                    "AJAX_OPTION_STYLE" => "Y",
                                                    "AJAX_OPTION_HISTORY" => "N"
                                                ),
                                                false
                                                );                                            
                                            ?>
                                    <?endif;?>                                           
<!--                                    <a onclick="ajax_bron('<?=bitrix_sessid_get()?>')" id="bron_round" href="javascript:void(0)" style="position:relative; text-decoration:none;">    
                                        <div id="bron_round_phone"></div>
                                        <?if (CITY_ID=="spb"):?>
                                            <div id="rounded2" class="bron_round_blue"></div>
                                        <?else:?>
                                            <div id="rounded2" class="bron_round_red"></div>
                                        <?endif;?>                                    
                                        <div id="rounded1" class="bron_round_orange"></div>                                    
                                        <div id="rounded_first" class="appetite appetite_bron_15">Хочу заказать столик<?//=GetMessage("NEW_YEAR")?></div>                                   
                                        <?if (CITY_ID=="spb"):?>
                                            <div id="rounded_second" class="appetite appetite_bron_15_2">(812)<br />740-1820<?//=GetMessage("NEW_YEAR")?></div>                                                                           
                                        <?else:?>
                                            <div id="rounded_second" class="appetite appetite_bron_15_2">(495)<br />988-2656<?//=GetMessage("NEW_YEAR")?></div>
                                        <?endif;?>
                                    </a>-->
                                     <?/*<a id="bron_round" href="/<?=CITY_ID?>/articles/letnie_verandy/" style="position:relative; text-decoration:none;">    
                                        <div class="left" style="width:100px; margin-bottom:2px;"><img src="/tpl/images/letn.png" /></div>
                                        <div class="left letn_title" style="width:140px; padding-top: 10px;margin-bottom:2px;">
                                            <div style="font-size:14px; line-height:24px; text-transform: uppercase"><span style="font-size:20px">Л</span>етние веранды</div>
                                            <div style="font-style:italic">Мы подобрали для Вас более 50 ресторанов с отличным видом!</div>
                                        </div>
                                        <div class="clear"></div>
                                    </a?>
                                    <a id="bron_round" href="/<?=CITY_ID?>/articles/new_year/" style="position:relative; text-decoration:none;">    
                                            <div class="left" style="width:100px;margin-bottom:2px;margin-right: 5px" align="center"><img src="/tpl/images/new_year/ny_head2.jpg" /></div>
                                            <div class="left letn_title" style="width:130px; padding-top: 5px;margin-bottom:2px;">
                                                <div style="font-size:17px; line-height:18px; color:#24a5cf; font-family: appetite">Новый год</div>
                                                <div style="font-size:17px; line-height:18px; color:#24a5cf; font-family: appetite">совсем скоро!</div>
                                                <div style="font-style:italic; line-height:18px;">Веселый корпоратив?<br/>Мы найдем ресторан!</div>
                                            </div>
                                            <div class="clear"></div>
                                        </a>
                                    <?*//*<div style="position: absolute; z-index: 100; margin: 0px auto;">
                                        <a href="/<?=CITY_ID?>/articles/8marta/"><img src="/bitrix/images/1.gif" width="157" height="116" border="0" alt=""></a>                                
                                    </div>
                                    <object classid="clsid:D27CDB6E-AE6D-11CF-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="157" height="116">
                                        <param name="movie" value="<?=SITE_TEMPLATE_PATH?>/images/8mart3.swf">
                                        <param name="quality" value="high">
                                        <param name="bgcolor" value="#FFFFFF">
                                        <param name="wmode" value="opacue">
                                        <embed src="<?=SITE_TEMPLATE_PATH?>/images/8mart3.swf" quality="high" bgcolor="#FFFFFF" wmode="opacue" width="157" height="116" name="banner" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">
                                    </object>
                                     * 
                                     
                                    <a href="/<?=CITY_ID?>/articles/8marta/"><img src="<?=SITE_TEMPLATE_PATH?>/images/8m.png" /></a>
                                    <!--<a href="/<?=CITY_ID?>/articles/valentine/"><img src="/tpl/images/valentine.png" /></a>-->*/?>
                                </div>
                                <?endif;?>
                            <?endif;?>
                        </div>
                        <div class="clear"></div>
                    <?endif;?>
                </div> 
        <div id="filter_bg">
            <div id="filter">
                <div class="filter">
                    <?$_REQUEST["search_in"] = 'recipe';?>
                    <?$APPLICATION->IncludeComponent(
                                "bitrix:search.title",
                                "main_search_suggest_new",
                                Array(),
                            false
                    );?>
                    <noindex>
                    <?$APPLICATION->IncludeComponent(
                                "restoran:catalog.filter",
                                "cook_new2",
                                Array(
                                        "IBLOCK_TYPE" => "catalog",
                                        "IBLOCK_ID" => 139,
                                        "SPEC_IBLOCK" => 72,
                                        "FILTER_NAME" => "arrFilter",
                                        "FIELD_CODE" => array(),
                                        "PROPERTY_CODE" => array("cat", "osn_ingr", "prig_time", "cook", "povod", "prig"),
                                        "PRICE_CODE" => array(),
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "N",
                                        "LIST_HEIGHT" => "5",
                                        "TEXT_WIDTH" => "20",
                                        "NUMBER_WIDTH" => "5",
                                        "SAVE_IN_SESSION" => "N"
                                )
                    );?>  
                        </noindex>
                </div>
            </div>
        </div>
            <div align="center">    
        <a href="javascript:void(0)" class="ajax font18" onclick="show_review_form()">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "top_content_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
        </a>
                <Br />
            </div>
        <?/*$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "inc_top",
                "EDIT_TEMPLATE" => ""
            ),
        false
        );*/?>