<?
CModule::IncludeModule("iblock");
if (is_array($arResult["SEARCH"])):?>
    <?foreach($arResult["SEARCH"] as &$arItem):
        $res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
        if($el = $res->GetNext())
        {
            /*$db_props = CIBlockElement::GetProperty($el["IBLOCK_ID"], $el["ID"], array(), Array("CODE"=>"ratio"));
            if($ar_props = $db_props->Fetch())
                    $ratio = IntVal($ar_props["VALUE"]);
            
            $arItem["ELEMENT"]["RATIO"] = $ratio;*/
            $arItem["ELEMENT"] = $el;
            $arItem["ELEMENT"]["PREVIEW_TEXT"] = TruncateText(strip_tags($arItem["ELEMENT"]["PREVIEW_TEXT"]), 200);		
            if ($el["DETAIL_PICTURE"])
                $arItem["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($el["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_PROPORTIONAL, true);  
         }
    endforeach;
    ?>
<?endif;?>
