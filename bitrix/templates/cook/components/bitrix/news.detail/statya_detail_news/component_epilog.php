<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$APPLICATION->SetTitle($arResult["SECTION"]["PATH"][0]["NAME"]);
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult['IMAGE'].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult['TEXT'].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true);
?>
<div id="comments_temp">
    <br /><Br />
    <?
    $APPLICATION->IncludeComponent(
                "restoran:comments_add",
                "review_comment",
                Array(
                        "IBLOCK_TYPE" => "comment",
                        "IBLOCK_ID" => 2438,
                        "ELEMENT_ID" => $arResult["ID"],
                        "IS_SECTION" => "N",       
                        "OPEN" => "Y"
                ),false
    );?>
    <br />
    <?$APPLICATION->IncludeComponent(
            "restoran:comments",
            "review_comment",
            Array(
                "IBLOCK_TYPE" => "comment",
                "ELEMENT_ID" => $arResult["ID"],
                "IBLOCK_ID" => 2438,
                "IS_SECTION" => "N",
                "ADD_COMMENT_TEMPLATE" => "",
                "COUNT" => 999,
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
            ),
        false
    );?>
    <Br />
</div>