<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
// set title
$APPLICATION->SetTitle($arResult["SECTION"]["PATH"][0]["NAME"]);
$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult["NAME"].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />',true);            
$APPLICATION->AddHeadString('<meta property="og:url" content="http://www.restoran.ru'.$APPLICATION->GetCurPage().'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:image" content="http://www.restoran.ru'.$arResult['IMAGE'].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult['TEXT'].'" />',true);            
$APPLICATION->AddHeadString('<meta property="og:site_name" content="&#x420;&#x435;&#x441;&#x442;&#x43e;&#x440;&#x430;&#x43d;.&#x440;&#x443;" />',true);            
$APPLICATION->AddHeadString('<meta property="fb:app_id" content="297181676964377" />',true); 
//$count = count($_COOKIE["RECENTLY_VIEWED"]["cookery"]);
//if (!in_array($arResult["ID"], $_COOKIE["RECENTLY_VIEWED"]["cookery"]))
  //  setcookie ("RECENTLY_VIEWED[cookery][".$count."]", $arResult["ID"], time() + 3600*12, "/");
//$APPLICATION->SetPageProperty("keywords", "рецепт, салат");

$temp = explode(" ",$arResult["NAME"]);
$tt = array();
foreach ($temp as $t)
{
    if (strlen($t)>3)
        $tt[] = strtolower($t);
}
$title = "Рецепт: ".$arResult["NAME"];
$key = "рецепт, ".implode(", ",$tt);
$desc = 'Рецепт: '.$arResult["NAME"].'. Подробный рецепт приготовления блюда "'.$arResult["NAME"].'"';

$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetPageProperty("keywords", $key);
$APPLICATION->SetPageProperty("description",$desc);
?>
<div id="comments_temp">
    <br /><Br />
    <?
    $APPLICATION->IncludeComponent(
                "restoran:comments_add",
                "review_comment",
                Array(
                        "IBLOCK_TYPE" => "comment",
                        "IBLOCK_ID" => (SITE_ID=="s1")?2438:2641,
                        "ELEMENT_ID" => $arResult["ID"],
                        "IS_SECTION" => "N",       
                        "OPEN" => "Y"
                ),false
    );?>
    <br />
    <?$APPLICATION->IncludeComponent(
            "restoran:comments",
            "review_comment",
            Array(
                "IBLOCK_TYPE" => "comment",
                "ELEMENT_ID" => $arResult["ID"],
                "IBLOCK_ID" => (SITE_ID=="s1")?2438:2641,
                "IS_SECTION" => "N",
                "ADD_COMMENT_TEMPLATE" => "",
                "COUNT" => 999,
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
            ),
        false
    );?>
    <Br />
</div>