<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/bitrix/templates/main/js/flowplayer.js"></script>
<script>
$(document).ready(function(){
   flowplayer("a.myPlayer", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
        clip: {
                autoPlay: false, 
                autoBuffering: true,
                accelerated: true                
        }
    });
    $(".articles_photo").galery(); 
});
</script>
<script>
$(document).ready(function(){
    $(".photos123").each(function(){
       var wid = $(this).width();
       //var hei = $(this).height();
       $(this).append("<div class='block'><?=GetMessage("R_ZOOM_UP")?></div>");
       $(this).find(".block").css({"display":"none","position":"absolute","left":wid/2-50+"px","width":"100px","height":"30px","top":"48%","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
    });
    $(".photos123").hover(function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        console.log(wid);
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"}); */
        $(this).find(".block").show();
    },
    function(){
        /*var wid = $(this).width();
        var hei = $(this).height();
        $(this).find(".block").css({"left":wid/2-50+"px","top":hei/2-15+"px"});*/
        $(this).find(".block").hide();
    });
//    $(".photos123 .block").toggle(function(){
//        $(this).parent().find("img:hidden").show().prev().hide();        
//        $(this).parent().removeClass('left');        
//        $(this).html("<?=GetMessage("R_ZOOM_DOWN")?>");
//        $(this).parent().css("width","728px");
//        var wid = $(this).parent().width();
//        //var hei = $(this).parent().height();
//        $(this).css({"left":wid/2-50+"px"});
//    },
//    function(){
//        $(this).parent().find("img:hidden").show().next().hide();
//        $(this).parent().addClass('left');        
//        $(this).html("<?=GetMessage("R_ZOOM_UP")?>");
//        $(this).parent().css("width","238px");
//        var wid = $(this).parent().width();
//        //var hei = $(this).parent().height();
//        $(this).css({"left":wid/2-50+"px"});
//    });    
    
    $(".photos123").toggle(function(){
        $(this).find("img:hidden").show().prev().hide();        
        $(this).removeClass('left');        
        $(this).find(".block").html("<?=GetMessage("R_ZOOM_DOWN")?>");
        $(this).css("width","728px");       
        var wid = $(this).width();        
        $(this).find(".block").css({"left":wid/2-50+"px"});
    },
    function(){
        $(this).find("img:hidden").show().next().hide();
        $(this).addClass('left');        
        $(this).find(".block").html("<?=GetMessage("R_ZOOM_UP")?>");
        $(this).css("width","238px");                
        var wid = $(this).width();
        $(this).find(".block").css({"left":wid/2-50+"px"});
    });  
    
    //minus_plus_buts 
    
    $("#minus_plus_buts").on("click",".plus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"plus"}, function(otvet){
  			if(parseInt(otvet)){
                                $(".all_plus_minus").html($(".all_plus_minus").html()*1+1);
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				
  			}
  		});
		return false;
	});
	
	$("#minus_plus_buts").on("click",".minus_but a", function(event){
		var obj = $(this).parent("div");
		$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"minus"}, function(otvet){
  			if(parseInt(otvet)){
                                $(".all_plus_minus").html($(".all_plus_minus").html()*1-1);
  				obj.html('<span></span>'+otvet);
  			}else{
  				
  					if (!$("#promo_modal").size()){
						$("<div class='popup popup_modal' id='promo_modal'></div>").appendTo("body");                                                               
					}
			
       		   		$('#promo_modal').html(otvet);
					showOverflow();
					setCenter($("#promo_modal"));
					$("#promo_modal").fadeIn("300");
  				}
  			
  		});
		return false;
	});
  	
  	$.post("<?=$templateFolder."/plus_minus.php"?>",{ID: <?=$arResult["ID"]?>, act:"generate_buts"}, function(otvet){
  		$("#minus_plus_buts").html(otvet);
  	});  
    
   
    
    
});
</script> 
<?$resto = array();?>
<div id="content">
    <div class="hrecipe left" style="width:720px;" itemscope itemtype="http://schema.org/Recipe">
        <div class="statya_section_name">
            <a class="uppercase another font14" href=<?=$arResult["SECTION_PAGE_URL"]?>><?=$arResult["IBLOCK_TYPE_NAME"]?></a>
            <div class="statya_nav">
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <hr class="black" />
        <h1  class="fn" itemprop="name"><?=$arResult["NAME"]?></h1>
        <?/*<a class="another no_border" href="/users/id<?=$arResult['CREATED_BY']?>/"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>*/?>
        <?if (SITE_ID!="s2"):?>
            <a itemprop="author" class="another author no_border" href="/users/id<?=$arResult['CREATED_BY']?>/"><?=$arResult["AUTHOR_NAME"]?></a>
            <br /><br />
        <?endif;?>
        <?if($arResult["PREVIEW_TEXT"]):?>
            <i itemprop="description" class="summary"><?=$arResult["PREVIEW_TEXT"]?></i>
            <br /> <br />
        <?endif;?>
        <div class="left recept_prop_block1" alt="blue-a">
            <?=GetMessage("R_COOKING_TIME")?> — <span itemprop="cookTime" class="duration"><?=$arResult["DISPLAY_PROPERTIES"]["prig_time"]["DISPLAY_VALUE"]?></span><br />
            <?=GetMessage("R_COMPLICATION")?> — <span><?=$arResult["DISPLAY_PROPERTIES"]["slognost"]["DISPLAY_VALUE"]?></span><br />
            <?
                $params = array();
                $params["id"] = $arResult["ID"];
                $params["section"] = 0;
                $params = addslashes(json_encode($params));            
            ?>
            <div class="recept_book">
                <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>
            </div>
            <div class="print">
                <a href="?print=Y" target="_blank"><?=GetMessage("R_PRINT")?></a>
            </div>
        </div>
        <div class="right recept_prop_block2">
            <div class="left" style="margin-right:20px; width:180px">
                <?if ($arResult["DISPLAY_PROPERTIES"]["osn_ingr"]["DISPLAY_VALUE"]):?>
                    <?=GetMessage("R_MAIN_INGRIDIENT")?>: <span><?=$arResult["DISPLAY_PROPERTIES"]["osn_ingr"]["DISPLAY_VALUE"]?></span><br />
                <?endif;?>
                <?if ($arResult["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]):?>
                    <?=GetMessage("R_CATEGORY")?>: <span itemprop="recipeCategory" class="category"><?=$arResult["DISPLAY_PROPERTIES"]["cat"]["DISPLAY_VALUE"]?></span><Br />
                <?endif;?>
                <?if ($arResult["DISPLAY_PROPERTIES"]["cook"]["DISPLAY_VALUE"]):?>
                    <?=GetMessage("R_KITCHEN")?>: <span class="cuisine-type"><?=$arResult["DISPLAY_PROPERTIES"]["cook"]["DISPLAY_VALUE"]?></span>
                <?endif;?>
            </div>
            <div class="left" style="width:170px">
                <?if ($arResult["DISPLAY_PROPERTIES"]["povod"]["DISPLAY_VALUE"]):?>
                    <?=GetMessage("R_POVOD")?>: <span><?=$arResult["DISPLAY_PROPERTIES"]["povod"]["DISPLAY_VALUE"]?></span><Br />
                <?endif;?>
                <!--Предпочтения: <span><?=$arResult["DISPLAY_PROPERTIES"]["predp"]["DISPLAY_VALUE"]?></span><br />-->
                 <?if ($arResult["DISPLAY_PROPERTIES"]["prig"]["DISPLAY_VALUE"]):?>
                    <?=GetMessage("R_PREPARATION")?>: <span><?=$arResult["DISPLAY_PROPERTIES"]["prig"]["DISPLAY_VALUE"]?></span>
                <?endif;?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <br /><br />
        <div style="position:relative">
            <?if ($arResult["PROPERTIES"]["porc"]["VALUE"]):?>
                <div itemprop="recipeYield" class="yield portion">
                    <div><?=$arResult["PROPERTIES"]["porc"]["VALUE"]?></div>
                    <?=pluralForm($arResult["PROPERTIES"]["porc"]["VALUE"],GetMessage("R_PORTION1"),GetMessage("R_PORTION2"),GetMessage("R_PORTION3"))?>
                </div>
            <?endif;?>
            <?if ($arResult["DETAIL_PICTURE"]["width"]>=720):?>
                <img itemprop="image" class="photo" src="<?=$arResult["DETAIL_PICTURE"]["src"]?>" width="720" />
            <?else:?>
                <img itemprop="image" class="photo" src="<?=$arResult["DETAIL_PICTURE"]["src"]?>" />
            <?endif;?>
        </div>
        <br />
        <?=$arResult["DETAIL_TEXT"]?>
        <br />
        <div class="bon_appetite"><?=GetMessage("R_BON_APPETI")?></div>
        <?
            $arrTags = explode(',', $arResult["TAGS"]);
            $count = count($arrTags);
            $i = 0;
            $t="";
            foreach($arrTags as $value):
               $i++;
               $value = trim($value);
               $t .= '<a class="another" href="/search/?tags='.str_replace(' ', '+', $value).'&search_in=recepts">'.$value.'</a>';
               if($i != $count) $t .= ', ';
            endforeach;
        ?>
        <br />
        <p><?=GetMessage("TAGS_TITLE")?>: <?=$t?></p>
        <div class="staya_likes">
            <div class="left"><?=GetMessage("R_COMMENTS")?>: (<a href="#comments" class="anchor another" onclick="return false;"><?=$arResult["PROPERTIES"]["COMMENTS"]["VALUE"]?></a>)</div>
            <!--<div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <?/*<div class="right" style="padding-right:15px;"><input type="button" class="button" value="<?=GetMessage("SUBSCRIBE")?>" /></div>*/?>
            <div id="minus_plus_buts"></div>
            
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2881483",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
            <div class="clear"></div>
        </div>
        <?if(count($resto)):?>
            <div id="order" style="width:700px">
                <div class="left">
                    <script>

                        $(document).ready(function(){
                            var params = {
                                changedEl: "#order_what",
                                visRows: 5
                            }
                            cuSel(params);
                            var params = {
                                changedEl: "#order_many",
                                visRows: 5
                            }
                            cuSel(params);
                            var params = {
                                changedEl: "#rest",
                                visRows: 5
                            }
                            cuSel(params);
                            $.tools.dateinput.localize("ru",  {
                            months:        '<?=GetMessage("MONTHS_FULL")?>',
                            shortMonths:   '<?=GetMessage("MONTHS_SHORT")?>',
                            days:          '<?=GetMessage("WEEK_DAYS_FULL")?>',
                            shortDays:     '<?=GetMessage("WEEK_DAYS_SHORT")?>'
                            });
                            $(".whose_date").dateinput({lang: 'ru', firstDay: 1 , format:"dd/mm/yy",trigger:true, css: { trigger: 'caltrigger'}, change: function(e, date)  {
                                $(".caltrigger").html(this.getValue("dd mmmm"));
                            } });
                        $(".caltrigger").html("<?=$arResult["TODAY_DATE"]?>");
                        $.maski.definitions['~']='[0-2]';
                        $.maski.definitions['!']='[0-5]';
                        $(".time").maski("~9:!9");
                        });
                    </script>
                    Бронирование
                    <select id="order_what" name="order_what">
                        <option selected="selected" value="1">столика</option>
                        <option  value="2">банкета</option>
                        <option value="3">фуршета</option>
                    </select>
                    в ресторане 
                    <select id="rest" name="rest">
                        <?foreach($resto as $res):?>
                            <option selected="selected" value="<?=$res["ID"]?>"><?=$res["NAME"]?></option>
                        <?endforeach;?>
                    </select>
                    <br />
                    <span style="position: relative; display: inline">
                        <input class="whose_date" style="" type="date" style="visibility:hidden" />
                    </span>
                    в <input class="time" type="text" size="4" value="1230"/> на
                    <select id="order_many" name="order_many">
                        <option selected="2" value="2">2</option>
                        <option  value="4">4</option>
                        <option value="6">6</option>
                        <option value="8">8</option>
                    </select>
                    персоны
                    <div class="phone">
                        <sub>Заказ столика или банкета:<br /> </sub>
                        +7 (495) 988-26-56, +7 (495) 506-00-33
                    </div>
                </div>
                <div class="right">
                    <input type="button" class="dark_button" value="ЗАБРОНИРОВАТЬ" />
                    <div class="coupon">
                        -50% <span>скидка</span><br />
                        <a href="#">Купить купон</a>
                    </div>

                </div>
            </div>
        <?endif;?>
        <div id="comments"></div> 
        <!--<div id="tabs_block7">
            <ul class="tabs" ajax="ajax">
                <li>
                    <a href="<?=$templateFolder."/comments.php?id=".$arResult["ID"]?>">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block"></div>
            </div>             
        </div>-->
        
        <div id="tabs_block8">
            <ul class="tabs">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_VK")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_FACEBOOK")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block">
                   <!-- Put this script tag to the <head> of your page -->
                    <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

                    <script type="text/javascript">
                    VK.init({apiId: 2881483, onlyWidgets: true});
                    </script>

                    <!-- Put this div tag to the place, where the Comments block will be -->
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                    VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                    </script>
               </div>
               <div class="pane">
                    <div class="fb-comments" data-href="http://www.restoran.ru<?=$APPLICATION->GetCurPage()?>" data-num-posts="20" data-width="728"></div>
               </div>
            </div>            
        </div>
        <hr class="black" />
        <div class="statya_section_name">
            <a class="uppercase another font14" href=<?=$arResult["SECTION_PAGE_URL"]?>><?=$arResult["IBLOCK_TYPE_NAME"]?></a>
            <div class="statya_nav">
                <?if ($arResult["PREV_ARTICLE"]):?>
                    <div class="<?=($arResult["NEXT_ARTICLE"])?"left":"right"?>">
                        <a class="statya_left_arrow no_border" href="<?=$arResult["PREV_ARTICLE"]?>"><?=GetMessage("NEXT_POST")?></a>
                    </div>
                <?endif;?>
                <?if ($arResult["NEXT_ARTICLE"]):?>
                    <div class="right">
                        <a class="statya_right_arrow no_border" href="<?=$arResult["NEXT_ARTICLE"]?>"><?=GetMessage("PREV_POST")?></a>
                    </div>
                <?endif;?>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        <Br /><Br />
    </div>
    <div class="right" style="width:240px">
        <a class="add_recipe" href="/content/cookery/add_recipe.php">+ <?=GetMessage("R_ADD_RECEPT")?></a>
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        <br /><br />
        <!--<div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <div id="users_onpage"></div>
            </div>
            <div class="bottom"></div>
        </div>   
        <br /><br />-->        
        <div class="top_block"><?=GetMessage("R_SIMILAR")?></div>
        <?
        global $arrFilterTop4;
        if (is_array($arResult["PROPERTIES"]["similar_recepts"]["VALUE"]))
        {
            $arrFilterTop4 = Array(
                "ID"=>$arResult["PROPERTIES"]["similar_recepts"]["VALUE"]
            );
        }
        else
        {
            $arrFilterTop4 = Array(
                "!ID"=>$arResult["ID"],
                "PROPERTY_cat" => $arResult["PROPERTIES"]["cat"]["VALUE"],
                Array("LOGIC"=>"OR",
                    Array( "PROPERTY_osn_ingr" => $arResult["PROPERTIES"]["osn_ingr"]["VALUE"]),
                    Array("!PROPERTY_osn_ingr" => false)
                ),            
            );
        }
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => (SITE_ID=="s1")?139:2606,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                ?> 
        <br /><br />
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        <br /><Br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
        <br /><Br/>
         <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <br /><br />
    </div>
    <div class="clear"></div>
    <br />
    <div id="yandex_direct">
                    <script type="text/javascript"> 
                    //<![CDATA[
                    yandex_partner_id = 47434;
                    yandex_site_bg_color = 'FFFFFF';
                    yandex_site_charset = 'utf-8';
                    yandex_ad_format = 'direct';
                    yandex_font_size = 1;
                    yandex_direct_type = 'horizontal';
                    yandex_direct_limit = 4;
                    yandex_direct_title_color = '24A6CF';
                    yandex_direct_url_color = '24A6CF';
                    yandex_direct_all_color = '24A6CF';
                    yandex_direct_text_color = '000000';
                    yandex_direct_hover_color = '1A1A1A';
                    document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                    //]]>
                    </script>
                </div>
    <br />
</div>