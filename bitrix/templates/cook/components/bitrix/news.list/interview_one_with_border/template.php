<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="read_more" <?=(end($arResult["ITEMS"])==$arItem)?"style='border:none'":""?>>
        <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" width="232" />
        <div class="interview_name">
            <h3><?=$arItem["NAME"]?></h3>
        </div>
        
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
        
        <div class="left">Комментарии: (<a class="another" href="/<?=CITY_ID?>/afisha/<?=$arItem["CODE"]?>#comments"><?=intval($arItem["REVIEWS_CNT"])?></a>)</div>
        <div class="right"><a class="another" href="/<?=CITY_ID?>/afisha/<?=$arItem["CODE"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
<?endforeach;?>