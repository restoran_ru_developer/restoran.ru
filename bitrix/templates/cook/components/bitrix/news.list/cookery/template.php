<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
            <div class="sorting">
                <div class="left"><span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span></div>
                <div class="right"><?=$arResult["NAV_STRING"]?></div>                    
                <div class="clear"></div>
            </div>
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>    
                <div class="new_restoraunt left<?if($key % 3 == 2):?> end<?endif?>">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />
                    <div class="interview_name">
                        <h3><?=$arItem["NAME"]?></h3>
                    </div>
                    <div class="interview_text">
                        <p><?=$arItem["PREVIEW_TEXT"]?></p>
                    </div>
                    <div class="left">Комментарии: (<a class="another" href="#"><?=intval($arItem["REVIEWS_CNT"])?></a>)</div>
                    <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                    <div class="clear"></div>
                </div>
            <?endforeach;?>          
            <div class="clear"></div>        
            <hr class="bold" />        
            <div class="left">
                <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
                <?=($_REQUEST["pageRestCnt"] == 20 || !$_REQUEST["pageRestCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">20</a>')?>
                <?=($_REQUEST["pageRestCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=40'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">40</a>')?>
                <?=($_REQUEST["pageRestCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageRestCnt=60'.($_REQUEST["pageRestSort"] ? "&pageRestSort=".$_REQUEST["pageRestSort"] : "").'">60</a>')?>
            </div>
            <div class="right">
                <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                    <?=$arResult["NAV_STRING"]?>
                <?endif;?>
            </div>
            <div class="clear"></div>        
 