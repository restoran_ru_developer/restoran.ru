$(document).ready(function(){
   setTimeout("ch_spec_block()",5000);
});

function ch_spec_block()
{
    if ($("#spec_block_image img.active").next().hasClass("chim"))
        $("#spec_block_image img.active").removeClass("active").fadeOut(2000).next().addClass("active").fadeIn(2000);
    else
    {
        $("#spec_block_image img.active").removeClass("active").fadeOut(2000);
        $("#spec_block_image").find("img:eq(0)").addClass("active").fadeIn(2000);
    }
    setTimeout("ch_spec_block()",5000);
}