<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
$(document).ready(function(){
    $(".photos123").each(function(){
       var c = new Image;
       c.src = $(this).find("img:visible").attr("src");
       $(this).append("<div class='block'>Увеличить</div>");
       $(this).find(".block").css({"display":"none","position":"absolute","left":c.width/2-50+"px","width":"100px","height":"30px","top":c.height/2-15+"px","background":"#000","opacity":"0.7","color":"#FFF","text-transform":"uppercase","text-align":"center","line-height":"30px","cursor":"pointer","padding":"0px 10px"}); 
       //$(this).append("<div class='block_text'>Увеличить</div>").find(".block_text").css({"position":"absolute","left":($(this).width()/2-50)+"px","width":"100px","height":"30px","top":(this.offsetHeight/2-15)+"px","text-transform":"uppercase","color":"#FFF"}); ;                
    });
    $(".photos123").hover(function(){
        var c = new Image;
        c.src = $(this).find("img:visible").attr("src");
        $(this).find(".block").css({"left":c.width/2-50+"px","top":c.height/2-15+"px"}); 
        $(this).find(".block").show();
    },
    function(){
        var c = new Image;
        c.src = $(this).find("img:visible").attr("src");
        $(this).find(".block").css({"left":c.width/2-50+"px","top":c.height/2-15+"px"});
        $(this).find(".block").hide();
    });
    $(".photos123 .block").toggle(function(){
        $(this).parent().find("img:hidden").show().prev().hide();        
        $(this).parent().removeClass('left');        
        $(this).html("Уменьшить");
        var c = new Image;
        c.src = $(this).parent().find("img:visible").attr("src");
        $(this).css({"left":c.width/2-50+"px","top":c.height/2-15+"px"}); 
    },
    function(){
        $(this).parent().find("img:hidden").show().next().hide();
        $(this).parent().addClass('left');        
        $(this).html("Увеличить");
        var c = new Image;
        c.src = $(this).parent().find("img:visible").attr("src");
        $(this).css({"left":c.width/2-50+"px","top":c.height/2-15+"px"}); 
    });    
})
</script>  

<div id="content">
    <div class="left" style="width:720px;">
        <div class="statya_section_name">
            <h4><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h4>
            <div class="statya_nav">
                <div class="left">
                    <?if ($arResult["RECEPT"]["PREV_POST"]):?>
                        <a class="statya_left_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["PREV_POST"]["CODE"]?>/"><?=GetMessage("PREV_POST")?></a>
                    <?else:?>
                        <a class="statya_left_arrow no_border" href="javascript:void(0)" title="Предыдущих рецептов нет"><?=GetMessage("PREV_POST")?></a>
                    <?endif;?>                    
                </div>
                <div class="right">
                    <?if ($arResult["RECEPT"]["NEXT_POST"]):?>
                        <a class="statya_right_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["NEXT_POST"]["CODE"]?>/"><?=GetMessage("NEXT_POST")?></a>
                    <?else:?>
                        <a class="statya_right_arrow no_border" href="javascript:void(0)" title="Следующих рецептов нет"><?=GetMessage("NEXT_POST")?></a>                         
                    <?endif;?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="black_hr margin15"></div>
        <h1><?=$arResult["SECTION"]["PATH"][2]["NAME"]?></h1>
        <div class="rating" style="margin-bottom:10px;">
            <?for($i==0;$i<$arResult["RECEPT"]["RATIO"]["RATIO"];$i++):?>
                <div class="star_a"></div>
            <?endfor?>
            <?for($i;$i<5;$i++):?>
                <div class="star"></div>
            <?endfor;?>
        </div>
        <a class="another no_border" href="#"><?=$arResult["AUTHOR_NAME"]?></a>, <span class="statya_date"><?=$arResult["CREATED_DATE_FORMATED_1"]?></span> <?=$arResult["CREATED_DATE_FORMATED_2"]?>
        <br /><br />
        <?
            // short text
            if($arResult["RECEPT"]["DESCRIPTION"]):?>
                <i><?=$arResult["RECEPT"]["DESCRIPTION"]?></i>
                <br /> <br />
                <div class="left recept_prop_block1">
                    Время приготовления — <span><?=$arResult["RECEPT"]["PROPERTIES"]["TIME"]?></span><br />
                    Сложность: <span><?=$arResult["RECEPT"]["PROPERTIES"]["SLOG"]?></span><br />
                    <?
                      $params = array();
                      $params["id"] = $arResult["SECTION"]["PATH"][2]["ID"];
                      $params["section"] = 1;
                      $params = addslashes(json_encode($params));            
                    ?>
                    <div class="recept_book">
                        <a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "json","<?=$params?>",favorite_success)'><?=GetMessage("R_ADD2FAVORITES")?></a>
                    </div>
                    <div class="print">
                        <a href="?print=Y" target="_blank"><?=GetMessage("R_PRINT")?></a>
                    </div>
                </div>
                <div class="right recept_prop_block2">
                    <div class="left">
                        Основной ингридиент: <span><?=$arResult["RECEPT"]["PROPERTIES"]["OSN_ING"]?></span><br />
                        Категория: <span><?=$arResult["RECEPT"]["PROPERTIES"]["SECTION"]?></span><Br />
                        Кухня: <span><?=implode(", ",$arResult["RECEPT"]["PROPERTIES"]["KITCHEN"])?></span>
                    </div>
                    <div class="right">
                        Повод: <span><?=implode(", ",$arResult["RECEPT"]["PROPERTIES"]["CAUSE"])?></span><Br />
                        Предпочтения: <span><?=implode(", ",$arResult["RECEPT"]["PROPERTIES"]["PREFERENCES"])?></span><br />
                        Приготовление: <span><?=implode(", ",$arResult["RECEPT"]["PROPERTIES"]["PREPARATION"])?></span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <br /><br />
            <?
            // prev type for layout
            $prevType = 'shortText';
            endif?>
        <?foreach($arResult["ITEMS"] as $key=>$block):?>
            
            <?
            // rest bind
            if(is_array($block["REST_BIND_ARRAY"])):?>
                <div class="left" style="width:165px;">
                    <?foreach($block["REST_BIND_ARRAY"] as $rest):?>
                        <img class="indent" src="<?=$rest["REST_PICTURE"]["src"]?>" width="148" /><br />
                            <a class="font14" href="#"><?=$rest["NAME"]?></a>,<br />
                            <?=$rest["CITY"]?>
                        <p>
                            <div class="rating">
                                <?for($i = 0; $i < 5; $i++):?>
                                    <div class="small_star<?if($i < $rest["RATIO"]):?>_a<?endif?>" alt="<?=$i?>"></div>
                                <?endfor?>
                                <div class="clear"></div>
                            </div>
                            <b><?=GetMessage("CUISINE")?>:</b>
                            <?foreach($rest["CUISINE"] as $cuisine):?>
                                <?=$cuisine?><?if(end($rest["CUISINE"]) != $cuisine) echo ", ";?>
                            <?endforeach?>
                            <br />
                            <b><?=GetMessage("AVERAGE_BILL")?>:</b> <?=$rest["AVERAGE_BILL"]?><br />
                        </p>
                        <?if(end($block["REST_BIND_ARRAY"]) != $rest):?>
                            <hr class="dotted" />
                        <?endif?>
                    <?endforeach?>
                </div>
            <?
            $prevType = 'rest';
            endif?>
            <?if($arResult["RECEPT"]["DETAIL_PICTURE"]&&$key==0):?>
                <div class="left" style="position:relative;margin-bottom:10px ">
                    <?if ($arResult["RECEPT"]["QUANTITY"]):?>
                        <div class="portion">
                            <div><?=$arResult["RECEPT"]["QUANTITY"]?></div>
                            <?=pluralForm($arResult["RECEPT"]["QUANTITY"],"порция","порции","порций")?>
                        </div>
                    <?endif;?>
                    <img src="<?=$arResult["RECEPT"]["DETAIL_PICTURE"]["src"]?>" />
                </div>
                <div class="clear"></div>
            <?endif;?>
            <?
            //  text
            if ($block["DETAIL_PICTURE"]["SRC"]):?>
                <div class="recept_text">                  
                    <?if ($block["PROPERTIES"]["SHOW_NAME"]["VALUE"]=="Да"):?>
                        <h2><?=$block["NAME"]?></h2>
                    <?endif;?>
                    <?if ($block["PROPERTIES"]["BIG_PICTURE"]["VALUE"]!="Да"):?>
                        <div class="left photos123" style="position:relative; width:238px;margin-bottom: 10px;">                            
                            <img id="pic<?=$block["PREVIEW_PICTURE"]["ID"]?>" src="<?=$block["PREVIEW_PICTURE"]["src"]?>" />
                            <img id="pic<?=$block["PREVIEW_PICTURE"]["ID"]?>big" style="display:none" src="<?=$block["DETAIL_PICTURE"]["SRC"]?>" />
                        </div>
                    <?else:?>
                        <div class="left" style="position:relative">
                            <?if ($block["PROPERTIES"]["QUANTITY"]["VALUE"]):?>
                                <div class="portion">
                                    <div><?=$block["PROPERTIES"]["QUANTITY"]["VALUE"]?></div>
                                    <?=pluralForm($block["PROPERTIES"]["QUANTITY"]["VALUE"],"порция","порции","порций")?>
                                </div>
                            <?endif;?>
                            <img src="<?=$block["DETAIL_PICTURE"]["SRC"]?>" />
                        </div>
                    <?endif;?>
                    <div style="">
                        <?=$block["DETAIL_TEXT"]?>
                    </div>
                        <div class="clear"></div>
                </div>
                <br /><br />                                            
            <?elseif($block["DETAIL_TEXT"]):?>
                <div class="recept_text">    
                    <?if ($block["PROPERTIES"]["BLOCK_TYPE"]["VALUE"]=="Визуальный редактор"):?>
                        <h2><?=$block["NAME"]?></h2>
                    <?endif;?>            
                    <?=$block["DETAIL_TEXT"]?>                
                </div>
                <br /><br />                                
            <?endif?>                
            <?            
            // speech
            if($block["RESPONDENT"]):?>
                <div class="bon_appetite">Приятного аппетита!</div>
                <table class="direct_speech">
                    <tr>
                        <td width="170">
                            <div class="author">
                                <?if (!$block["RESPONDENT"]["PHOTO"]["src"]):?>
                                    <img src="/tpl/images/user.jpg" />
                                <?else:?>
                                    <img src="<?=$block["RESPONDENT"]["PHOTO"]["src"]?>" />
                                <?endif;?>
                            </div>
                        </td>
                        <td width="150">
                            <span class="uppercase"><?=$block["RESPONDENT"]["NAME"]?></span><br/>
                            <i><?=$block["RESPONDENT"]["POST"]?></i>
                        </td>
                        <td>
                            <i>«<?=$block["RESPONDENT"]["SPEECH"]?>»</i>
                        </td>
                    </tr>
                </table>
                <br />
            <?
            $prevType = 'speech';
            endif?>
            <?
            //  photos
            if(is_array($block["PICTURES"])):?>
                <div class="left articles_photo">
                    <?if($block["CNT_PICTURES"] == 1):?>
                        <div class="img">
                            <img src="<?=$block["PICTURES"][0]["src"]?>" width="715" />
                            <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                        </div>
                    <?else:?>
                        <script type="text/javascript">
                            $(document).ready(function(){
                               $(".articles_photo").galery();
                            });
                        </script>
                        <div class="left scroll_arrows"><a class="prev browse left"></a><span class="scroll_num">1</span>/<?=$block["CNT_PICTURES"]?><a class="next browse right"></a></div>
                        <div class="clear"></div>
                        <div class="img">
                            <img src="<?=$block["PICTURES"][0]["src"]?>" width="715" />
                            <p><i><?=$block["PICTURES"][0]["description"]?></i></p>
                        </div>
                        <div class="special_scroll">
                            <div class="scroll_container">
                                <?foreach($block["PICTURES"] as $keyPic=>$pic):?>
                                    <div class="item<?if($keyPic == 0):?> active<?endif?>">
                                        <img src="<?=$pic["src"]?>" alt="<?=$pic["description"]?>" align="bottom" />
                                    </div>
                                <?endforeach?>
                            </div>
                        </div>
                    <?endif?>
                </div>
                <div class="clear"></div>
            <?
            $prevType = 'photos';
            endif?>
        <?endforeach?>
        <?
            $arrTags = explode(',', $arResult["TAGS"]);
            $count = count($arrTags);
            $i = 0;
            $t="";
            foreach($arrTags as $value):
               $i++;
               $value = trim($value);
               $t .= '<a class="another" href="/search/?tags='.str_replace(' ', '+', $value).'&search_in=recipe">'.$value.'</a>';
               if($i != $count) $t .= ', ';
            endforeach;
        ?>
        <p><?=GetMessage("TAGS_TITLE")?>: <?=$t?></p>
        <div class="staya_likes">
            <div class="left">Комментарии: (<a href="#comments" class="anchor another"><?=$arResult["RECEPT"]["RATIO"]["COUNT"]?></a>)</div>
            <!--<div class="right"><a href="javascript:void(0)" onclick='send_ajax("/bitrix/components/restoran/favorite.add/ajax.php", "<?=$params?>")'><?=GetMessage("R_ADD2FAVORITES")?></a></div>-->
            <div class="right" style="padding-right:15px;"><input type="button" class="button" value="<?=GetMessage("SUBSCRIBE")?>" /></div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:asd.share.buttons",
                "likes",
                Array(
                    "ASD_TITLE" => $APPLICATION->GetTitle(false),
                    "ASD_URL" => "http://".SITE_SERVER_NAME.$APPLICATION->GetCurUri(),
                    "ASD_PICTURE" => "",
                    "ASD_TEXT" => ($block["PREVIEW_TEXT"] ? $block["PREVIEW_TEXT"] : $APPLICATION->GetTitle(false)),
                    "ASD_INCLUDE_SCRIPTS" => array(0=>"FB_LIKE", 1=>"VK_LIKE", 2=>"TWITTER",),
                    "LIKE_TYPE" => "LIKE",
                    "VK_API_ID" => "2628160",
                    "VK_LIKE_VIEW" => "mini",
                    "SCRIPT_IN_HEAD" => "N"
                )
            );?>
            <div class="clear"></div>
        </div>
        <br /><Br />
        <div id="tabs_block7">
            <ul class="tabs">
                <li>
                    <a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name"><?=GetMessage("R_COMMENTS")?></div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>
                </li>
            </ul>
            <div class="panes">
               <div class="pane" style="display:block">
                   <?$APPLICATION->IncludeComponent(
                   	"restoran:main.comments",
                   	"with_rating",
                   	Array(
                   		"IBLOCK_TYPE" => "reviews",
                   		"IBLOCK_ID" => 88,
                   		"SECTION_ID" => $arResult["RECEPT"]["PROPERTIES"]["COMMENTS"],
                   		"PROPERTY_PARENT_BIND_CODE" => "PARENT_ID",
                   		"PROPERTY_USER_BIND_CODE" => "USER_BIND",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => array("PROPERTY_PARENT_ID", "PROPERTY_USER_BIND"),
                                "ITEMS_LIMIT" => "10",
                                "ADD_COMMENT_TEMPLATE" => "with_rating",
                   		"CACHE_TYPE" => "A",
                   		"CACHE_TIME" => "3600",
                   		"CACHE_NOTES" => ""
                   	),
                   false
                   );?>
               </div>
            </div>            
        </div>
        
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="http://userapi.com/js/api/openapi.js?45"></script>

        <script type="text/javascript">
          VK.init({apiId: 2709409, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 20, width: "720", attach: false});
        </script>
        <hr class="black" />
        <div class="statya_section_name">
            <h4><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h4>
            <div class="statya_nav">
                <div class="left">
                    <?if ($arResult["RECEPT"]["PREV_POST"]):?>
                        <a class="statya_left_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["PREV_POST"]["CODE"]?>/"><?=GetMessage("PREV_POST")?></a>
                    <?else:?>
                        <a class="statya_left_arrow no_border" href="javascript:void(0)" title="Предыдущих рецептов нет"><?=GetMessage("PREV_POST")?></a>
                    <?endif;?>                    
                </div>
                <div class="right">
                    <?if ($arResult["RECEPT"]["NEXT_POST"]):?>
                        <a class="statya_right_arrow no_border" href="/content/cookery/<?=$arResult["SECTION"]["PATH"][0]["CODE"]?>/<?=$arResult["RECEPT"]["NEXT_POST"]["CODE"]?>/"><?=GetMessage("NEXT_POST")?></a>
                    <?else:?>
                        <a class="statya_right_arrow no_border" href="javascript:void(0)" title="Следующих рецептов нет"><?=GetMessage("NEXT_POST")?></a>                         
                    <?endif;?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <br /><br />
        <img alt="" title="" src="/upload/rk/aa3/middle_baner.png" width="728" height="90" border="0">
    </div>
    <div class="right" style="width:240px">        
        <img src="/bitrix_personal/templates/main/images/right_baner1.png" />
        <br /><br />
        <div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <div id="users_onpage"></div>
            </div>
            <div class="bottom"></div>
        </div>   
        <br /><br />
        <div class="top_block">Похожие рецепты</div>
        <?$APPLICATION->IncludeComponent(
            "restoran:article.list",
            "interview_one_top",
            Array(
                    "IBLOCK_TYPE" => "cookery",
					"IBLOCK_ID" => "70",
					"SECTION_ID" => Array(0=>"234",1=>"235"),
					"SECTION_CODE" => "",
					"PAGE_COUNT" => 5,
					"SECTION_URL" => "/content/cookery/detail.php?SECTION_ID=#ID#",
					"PICTURE_WIDTH" => 232,
					"PICTURE_HEIGHT" => 127,
					"DESCRIPTION_TRUNCATE_LEN" => 200,
					"COUNT_ELEMENTS" => "Y",
					"TOP_DEPTH" => "3",
					"SECTION_FIELDS" => "",
					"SECTION_USER_FIELDS" => Array("UF_COUNT","UF_COMMENTS"),
					"SORT_FIELDS" => "ID",
					"SORT_BY" => "DESC",
					"ADD_SECTIONS_CHAIN" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_NOTES" => "",
					"CACHE_GROUPS" => "Y",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "kupon_list",	
					"PAGER_DESC_NUMBERING" => "N",	
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
            ),
            false
        );?> 
        <?/*$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "interview_one_with_border",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "interview",
                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                        "NEWS_COUNT" => 6,
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrOtherInterview",
                        "FIELD_CODE" => Array("ACTIVE_TO"),
                        "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "kupon_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "PRICE_CODE" => "BASE"
                ),
            false
        );*/?>
        <div align="right">
            <img src="/bitrix_personal/templates/main/images/right_baner2.png">
        </div>
        <br /><br />
        <div class="phone_block2">
            <div>Закажите столик<br /> по телефонам: </div>
            <span>812</span> 338 38 58<br>
            <span>812</span> 338 38 25
        </div>
    </div>
    <div class="clear"></div>
</div>