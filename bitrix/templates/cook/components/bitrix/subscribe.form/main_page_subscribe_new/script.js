$(document).ready(function() {
    /* subscribe input */
    // focus
    $(".subscribe_block .placeholder").on("focus", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val() == defVal)
            $(this).val('');
    });
    // blur
    $(".subscribe_block .placeholder").on("blur", function(event){
        var defVal = $(this).attr('defvalue');
        if($(this).val().length <= 0)
            $(this).val(defVal);
    });
});