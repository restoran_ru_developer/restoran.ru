<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
    <div class="title_main left"><?=GetMessage("WHAT_YOU_SEARCH_TEXT")?></div>        
        <?if($APPLICATION->GetCurDir() == "/" || substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/catalog/") || (substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/")&&!substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/afisha/"))):?>
        <form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
		<div class="search left">
            <div class="filter_box">
                <div class="left filter_block" id="filter1">
                    <div class="title"><?=GetMessage("CUISINE_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi3">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["CUISINES"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow"></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_1").popup({"obj":"filter1","css":{"left":"-10px","top":"0px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi3",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_1">
                        <div class="title"><?=GetMessage("CUISINE_TITLE")?></div>
                        <!--<div id="multi3_sel_cont" name="cuisine"></div>-->
                        <select multiple="multiple" class="asd" id="multi3" name="cuisine[]" size="10">
                            <?foreach($arResult["CUISINES"] as $cuisine):?>
                                <option value="<?=$cuisine["ID"]?>"><?=$cuisine["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi3" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="left filter_block" id="filter2">
                    <div class="title"><?=GetMessage("AVERAGE_BILL_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi1">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["AVERAGE_BILL"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow"></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_2").popup({"obj":"filter2","css":{"left":"-10px","top":"0px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi1",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_2">
                        <div class="title"><?=GetMessage("AVERAGE_BILL_TITLE")?></div>
                        <!--<div id="multi1_sel_cont" name="average_bill"></div>-->
                        <select multiple="multiple" class="asd" id="multi1" name="average_bill[]" size="10">
                            <?foreach($arResult["AVERAGE_BILL"] as $avBill):?>
                                <option value="<?=$avBill["ID"]?>"><?=$avBill["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="left filter_block" id="filter3">
                    <div class="title"><?=GetMessage("SUBWAY_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi4">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["SUBWAY"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow"></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_3").popup({"obj":"filter3","css":{"left":"-279px","top":"0px", "width":"830px"}});
                            var params = {
                                changedEl: "#multi4",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_3">
                        <div class="title"><?=GetMessage("SUBWAY_TITLE")?></div>
                        <!--<div id="multi4_sel_cont" name="subway"></div>-->
                        <select multiple="multiple" class="asd" id="multi4" name="subway[]" size="10">
                            <?foreach($arResult["SUBWAY"] as $subway):?>
                                <option value="<?=$subway["ID"]?>"><?=$subway["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi4" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="left filter_block end" id="filter4">
                    <div class="title"><?=GetMessage("REST_TYPE_TITLE")?></div>
                    <ul class="filter_category_block cuselMultiple-scroll-multi1">
                        <?for($i = 0; $i < $arParams["ITEMS_LIMIT"]; $i++):?>
                            <li><a href="javascript:void(0)" class="white"><?=$arResult["RESTAURANTS_TYPE"][$i]["NAME"]?></a></li>
                        <?endfor?>
                    </ul>
                    <div><a href="javascript:void(0)" class="filter_arrow"></a></div>
                    <script>
                        $(document).ready(function(){
                            $(".filter_popup_4").popup({"obj":"filter4","css":{"left":"-10px","top":"0px", "width":"315px"}});
                            var params = {
                                changedEl: "#multi2",
                                scrollArrows: true
                            }
                            cuSelMulti(params);
                        })
                    </script>
                    <div class="filter_popup filter_popup_4">
                        <div class="title"><?=GetMessage("REST_TYPE_TITLE")?></div>
                        <select multiple="multiple" class="asd" id="multi2" name="rest_type[]" size="10">
                            <?foreach($arResult["RESTAURANTS_TYPE"] as $restType):?>
                                <option value="<?=$restType["ID"]?>"><?=$restType["NAME"]?></option>
                            <?endforeach?>
                        </select>
                        <br /><br />
                        <div align="center"><input class="filter_button" filID="multi1" type="button" value="<?=GetMessage("CHOOSE_BUTTON")?>"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
		</div>
	</form>
    <div class="button right">
            <input class="filter_button" type="submit" name="search_submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
            <?if ($APPLICATION->GetCurDir()=="/" || substr_count($APPLICATION->GetCurDir(), "/".CITY_ID."/catalog/")):?>
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/include_areas/top_search_links.php",
                    Array(),
                    Array("MODE"=>"html")
                );?>
            <?endif;?>
    </div>            
    <?endif;?>              
    <div class="clear"></div>
	<form action="/<?=CITY_ID?>/search/" method="get" name="rest_filter_form">
    <div class="search_block left">
	
        <div class="left" id="title-search">
            <?/*$APPLICATION->IncludeComponent(
                    "bitrix:search.title",
                    "main_search_suggest",
                    Array(
                            "SHOW_INPUT" => "N",
                            "INPUT_ID" => "title-search-input",
                            "CONTAINER_ID" => "title-search",
                            "PAGE" => "#SITE_DIR#search/index.php?search_in=rest",
                            "NUM_CATEGORIES" => "1",
                            "TOP_COUNT" => "5",
                            "ORDER" => "rank",
                            "USE_LANGUAGE_GUESS" => "Y",
                            "CHECK_DATES" => "N",
                            "SHOW_OTHERS" => "N",
                            "CATEGORY_0_TITLE" => "",
                            "CATEGORY_0" => array("iblock_catalog"),
                            "CATEGORY_0_iblock_catalog" => array("11")
                    ),
            false
            );*/?>
            <input <?/*?>id="title-search-input"<?*/?> id="top_search_input" class="placeholder" type="text" name="q" value="<?=($_REQUEST['q'] ? $_REQUEST['q'] : GetMessage("BSF_T_SEARCH_EXMP_STRING"))?>" defvalue="<?=GetMessage("BSF_T_SEARCH_EXMP_STRING")?>" />
        </div>
        <div class="right">
            <select id="search_in" name="search_in" class="search_in">
                <option<?if($_REQUEST["search_in"] == 'rest' || !$_REQUEST["search_in"]):?> selected="selected"<?endif?> value="rest"><?=GetMessage("BSF_T_WHERE_SEARCH_REST")?></option>
                <option<?if($_REQUEST["search_in"] == 'news'):?> selected="selected"<?endif?> value="news"><?=GetMessage("BSF_T_WHERE_SEARCH_NEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'recipe'):?> selected="selected"<?endif?> value="recipe"><?=GetMessage("BSF_T_WHERE_SEARCH_RECIPE")?></option>
                <option<?if($_REQUEST["search_in"] == 'blog'):?> selected="selected"<?endif?> value="blog"><?=GetMessage("BSF_T_WHERE_SEARCH_BLOG")?></option>
                <option<?if($_REQUEST["search_in"] == 'overviews'):?> selected="selected"<?endif?> value="overviews"><?=GetMessage("BSF_T_WHERE_SEARCH_OVERVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'interview'):?> selected="selected"<?endif?> value="interview"><?=GetMessage("BSF_T_WHERE_SEARCH_INTERVIEWS")?></option>
                <option<?if($_REQUEST["search_in"] == 'master_classes'):?> selected="selected"<?endif?> value="master_classes"><?=GetMessage("BSF_T_WHERE_SEARCH_MASTER_CLASS")?></option>
            </select>
        </div>
        <div class="clear"></div>
    </div>
	
	<div class="right search_button">
		<input type="submit" class="light_button" value="Поиск" />
	</div>
	</form>
    <div class="clear"></div>
