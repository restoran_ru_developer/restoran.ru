<?
$MESS ['BSF_T_SEARCH_BUTTON'] = "НАЙТИ";
$MESS ['BSF_T_WHERE_SEARCH_REST'] = "по ресторанам";
$MESS ['BSF_T_WHERE_SEARCH_NEWS'] = "по новостям";
$MESS ['BSF_T_WHERE_SEARCH_RECIPE'] = "по рецептам";
$MESS ['BSF_T_WHERE_SEARCH_BLOG'] = "по блогам";
$MESS ['BSF_T_WHERE_SEARCH_OVERVIEWS'] = "по обзорам";
$MESS ['BSF_T_WHERE_SEARCH_INTERVIEWS'] = "по интервью";
$MESS ['BSF_T_WHERE_SEARCH_MASTER_CLASS'] = "по мастер-классам";
$MESS ['BSF_T_SEARCH_EXMP_STRING'] = "Например, ресторан Москва";
$MESS ['CUISINE_TITLE'] = "Кухня";
$MESS ['AVERAGE_BILL_TITLE'] = "Средний счет";
$MESS ['SUBWAY_TITLE'] = "Метро";
$MESS ['REST_TYPE_TITLE'] = "Тип";
$MESS ['CHOOSE_BUTTON'] = "ВЫБРАТЬ";
?>