<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?> 
<?
if (LANGUAGE_ID=="en")
    $arCookID = 2606;
else
    $arCookID = 139;
?>
<div id="content">
    <div class="left" style="width:730px">
        <?if (sizeof($arResult["ITEMS"])>0):?>
                <h1>
                    <?if ($arParams["SET_TITLE"]=="Y"):?>
                        <a href="/content/cookery" style="text-decoration:none"><?=$arResult["IBLOCK_TYPE"]?></a>
                    <?else:?>
                        <?=$APPLICATION->GetTitle(false);?>
                    <?endif;?>
                </h1>        
                <?
                $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE","clear_cache","CITY_ID");
                $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=new&".(($_REQUEST["pageSort"]=="new"&&$_REQUEST["by"]=="desc"||!$_REQUEST["pageSort"]) ? "by=asc": "by=desc"), $excUrlParams);                
                $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=PROPERTY_summa_golosov&".(($_REQUEST["pageSort"]=="PROPERTY_summa_golosov"&&$_REQUEST["by"]=="asc"||!$_REQUEST["pageSort"]) ? "by=desc": "by=asc"), $excUrlParams);
                ?>                
                <div class="sorting">
                    <div class="left">
                        <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                        <?$by = ($_REQUEST["by"]=="asc")?"a":"z";?>
                        <?if ($_REQUEST["pageSort"] == "new" || !$_REQUEST["pageSort"]):                                                   
                            echo "<span class='".$by."'><a class='another' href='".$sortNewPage."'>".GetMessage("SORT_NEW_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a></span>';
                        endif;?>
                            / 
                        <?if ($_REQUEST["pageSort"] == "PROPERTY_summa_golosov"):
                            echo "<span class='".$by."'><a class='another' href='".$sortPopularPage."'>".GetMessage("SORT_POPULAR_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortPopularPage.'">'.GetMessage("SORT_POPULAR_TITLE").'</a></span>';
                        endif;?>
                    </div>
                    <div class="right"><?//=$arResult["NAV_STRING"]?></div>                    
                    <div class="clear"></div>
                </div>        
            <?foreach($arResult["ITEMS"] as $key=>$arItem):?>        
                <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-3)):?>
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:advertising.banner",
                                "",
                                Array(
                                        "TYPE" => "content_article_list",
                                        "NOINDEX" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                ),
                                false
                            );?>   
                <?endif;?>
                <div class="new_restoraunt left<?if($key % 3 == 2):?> end<?endif?>">
                    <div style="margin-bottom:10px;">
                        <a href="/users/id<?=$arItem["CREATED_BY"]?>/" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
                    </div>
                    <?if($arItem["VIDEO"]):?>
                        <script src="/tpl/js/flowplayer.js"></script>
                        <a class="myPlayer1" href="http://<?=SITE_SERVER_NAME.$arItem["VIDEO"]["path"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer1", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                },
                                plugins:  {
                                        controls:  {
                                                volume: false,
                                                time: false
                                        }
                                }
                            });
                        </script>
                    <?else:?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a>
                    <?endif?>                                        
                    <div class="interview_name">
                        <a class="no_border" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>                        
                        <div class="clear"></div>
                    </div>
                    <p><?=$arItem["PREVIEW_TEXT"]?></p>
                    <div class="left"><?=GetMessage("R_COMMENTS")?>: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
                    <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                    <div class="clear"></div>
                </div>
                <?if(($key % 3 == 2)):?>
                    <div class="clear"></div>
                    <?if (end($arResult["ITEMS"])!=$arItem):?>
                        <div class="border_line"></div>        
                    <?endif;?>
                <?endif?>
            <?endforeach;?>    
    <?else:?>
            <h4><?=GetMessage("NO_RESULTS")?></h4>
    <?endif;?>
    </div>
    <div class="right" style="width:240px">      
        <a class="add_recipe" href="/content/cookery/add_recipe.php">+ <?=GetMessage("R_ADD_RECEPT")?></a>
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>    
        <?if (LANGUAGE_ID!="en"):?>
        <br />
        <div class="tags">
            <?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                    "SORT" => "CNT",
                    "PAGE_ELEMENTS" => "20",
                    "PERIOD" => "",
                    "URL_SEARCH" => "/search/index.php",
                    "TAGS_INHERIT" => "Y",
                    "CHECK_DATES" => "Y",
                    "FILTER_NAME" => "",
                    "arrFILTER" => array(
                            0 => "iblock_cookery",
                    ),
                    "arrFILTER_iblock_cookery" => array(
                            0 => $arCookID,
                    ),
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "FONT_MAX" => "24",
                    "FONT_MIN" => "12",
                    "COLOR_NEW" => "24A6CF",
                    "COLOR_OLD" => "24A6CF",
                    "PERIOD_NEW_TAGS" => "",
                    "SHOW_CHAIN" => "Y",
                    "COLOR_TYPE" => "N",
                    "WIDTH" => "100%",
                    "SEARCH_IN" => "recepts"
                    ),
                    $component
            );?>  
        </div>
        
        <!--<br />
        <br />
        <div align="center">
            <div class="top_block">Видео-рецепты</div>
        </div>
        <?
           /* $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "cook_one",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "cookery",
                        "IBLOCK_ID" => 139,
                        "NEWS_COUNT" => 3,
                        "SORT_BY1" => "ID",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("COMMENTS"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "200",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => 57429,
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );*/
        ?>
        <br />
        <div align="right"><a href="/content/cookery/videorecepti/">ВСЕ ВИДЕО</a></div>-->
        <br /><br />
        <?endif;?>
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        <br /><Br />
        <div align="center">
            <div class="top_block"><?=GetMessage("R_TOP3")?></div>
        </div>
        <?
                    $obCache = new CPHPCache; 
                    $life_time = 8*60*60; 
                    $cache_id = "cookerytop3"; 
                    if($obCache->InitCache($life_time, $cache_id, "/")) :
                        $vars = $obCache->GetVars();
                        $top3 = $vars["TOP3"];
                    else :
                        CModule::IncludeModule("iblock");
                        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arCookID,"CODE"=>"top3ru","ACTIVE"=>"Y","ACTIVE_DATE"=>"Y"),false,Array("nTopCount"=>1));
                        if ($ar = $res->Fetch())
                        {
                            $db_props = CIBlockElement::GetProperty($arCookID, $ar["ID"], "", "", Array("CODE"=>"recepts"));
                            while ($ob = $db_props->Fetch())
                            {
                                $top3[] = $ob["VALUE"];
                            }                            
                        }
                    endif;                    
                    if($obCache->StartDataCache()):
                        $obCache->EndDataCache(array(
                            "TOP3"    => $top3
                        )); 
                    endif;
                ?>
        <?
        if (count($top3)>1)
        {
            global $arrFilterTop3;
            $arrFilterTop3["ID"] = $top3; 
        }
        $_REQUEST["arrFilter_pf"] = array();
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "cook_one",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "cookery",
                        "IBLOCK_ID" => $arCookID,
                        "NEWS_COUNT" => (count($top3)>1) ? 9 : 3,
                        "SORT_BY1" => "shows",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop3",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "200",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );
        ?>
        <!--<hr />
        <div align="right"><a href="#">ВСЕ НОВЫЕ</a></div>
        <br /><br />-->
        <Br /><Br />
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
    </div>
    <div class="clear"></div>  
</div>
<div id="content">
    <hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?$excUrlParams=Array("pageCnt","page","PAGEN_1")?>
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageCnt=20", $excUrlParams).'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageCnt=40", $excUrlParams).'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageCnt=60", $excUrlParams).'">60</a>')?>
        </div>
        <div class="right">            
            	<?=$arResult["NAV_STRING"]?>            
        </div>
    <div class="clear"></div>
    <br /><br />
    <div id="yandex_direct">
        <script type="text/javascript"> 
        //<![CDATA[
        yandex_partner_id = 47434;
        yandex_site_bg_color = 'FFFFFF';
        yandex_site_charset = 'utf-8';
        yandex_ad_format = 'direct';
        yandex_font_size = 1;
        yandex_direct_type = 'horizontal';
        yandex_direct_limit = 4;
        yandex_direct_title_color = '24A6CF';
        yandex_direct_url_color = '24A6CF';
        yandex_direct_all_color = '24A6CF';
        yandex_direct_text_color = '000000';
        yandex_direct_hover_color = '1A1A1A';
        document.write('<sc'+'ript type="text/javascript" src="http://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
        //]]>
        </script>
    </div>
    <br /><br />
    <div align="center">
    <?$APPLICATION->IncludeComponent(
	"bitrix:advertising.banner",
	"",
	Array(
		"TYPE" => "bottom_cook_list",
		"NOINDEX" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "0"
	),
    false
    );?>
    </div>
    <br /><br />    
</div>