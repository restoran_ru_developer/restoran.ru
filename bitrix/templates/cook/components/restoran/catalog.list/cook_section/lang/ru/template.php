<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_1"] = "отзыв";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_2"] = "отзыва";
$MESS["CT_BNL_ELEMENT_PLURAL_REVIEW_3"] = "отзывов";
$MESS["CT_BNL_ELEMENT_SEE_ALL_REVIEWS"] = "Далее";
$MESS["SHOW_CNT_TITLE"] = "Выводить по";
$MESS["SORT_NEW_TITLE"] =" по новизне";
$MESS["SORT_POPULAR_TITLE"] =" по популярности";
$MESS["NO_RESULTS"] ="Ничего не найдено";
$MESS["R_COMMENTS"] ="Комментарии";
$MESS["R_ADD_RECEPT"] ="Добавить рецепт";
$MESS["R_TOP3"] ="Топ-3 рецептов";
?>