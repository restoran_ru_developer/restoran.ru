<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // truncate text
    //$arItem["DETAIL_PAGE_URL"] = str_replace("content",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
   // $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
   // $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);

    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    //$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::GetPath($arItem["DETAIL_PICTURE"]);
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
    {
        if ($arUser["PERSONAL_PROFESSION"])
            $arItem["AUTHOR_NAME"] = $arUser["PERSONAL_PROFESSION"];
        else
            $arItem["AUTHOR_NAME"] = ($arUser["NAME"])?$arUser["NAME"]:$arUser["LAST_NAME"];
    }

    // format date
    //if (!$arItem["DISPLAY_ACTIVE_FROM"]):
        $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    //else:
      //  $arTmpDate = $arItem["DISPLAY_ACTIVE_FROM"];
    //endif;
    $arTmpDate = explode(" ", $arTmpDate);
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
    
    /*$id_section = $DB->ForSql($arItem["IBLOCK_SECTION_ID"]);
    $sql = "SELECT b.CODE as CODE FROM b_iblock_section a
            JOIN b_iblock_section b ON b.ID = a.IBLOCK_SECTION_ID
            WHERE a.ID = ".$id_section." LIMIT 1";
    $r = $DB->Query($sql);
    if ($ar = $r->Fetch())
        $arResult["ITEMS"][$key]["IBLOCK_SECTION"] = $ar["CODE"];*/
    
    
    if ($arParams["IBLOCK_TYPE"]=="videonews")
    {
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$arItem["ID"],"PROPERTY_BLOCK_TYPE_VALUE"=>"Видео"),false,false,Array("PROPERTY_VIDEO"));
        if ($ar = $res->GetNext())
            $arItem["VIDEO"] = $ar["PROPERTY_VIDEO_VALUE"];        
    }
    
}
?>