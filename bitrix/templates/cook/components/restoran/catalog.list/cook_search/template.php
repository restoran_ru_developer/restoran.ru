<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?> 
    <?if (sizeof($arResult["ITEMS"])>0):?>                 
        <?foreach($arResult["ITEMS"] as $key=>$arItem):?>        
            <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-3)):?>
                    <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "content_article_list",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                        );?>   
            <?endif;?>
            <div class="new_restoraunt left<?if($key % 3 == 2):?> end<?endif?>">
                <div style="margin-bottom:10px;">
                    <a href="/users/id<?=$arItem["CREATED_BY"]?>/" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
                </div>
                <?if($arItem["VIDEO"]):?>
                    <script src="/tpl/js/flowplayer.js"></script>
                    <a class="myPlayer1" href="http://<?=SITE_SERVER_NAME.$arItem["VIDEO"]["path"]?>"></a> 
                    <script>
                        flowplayer("a.myPlayer1", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                            clip: {
                                    autoPlay: false, 
                                    autoBuffering: true
                            },
                            plugins:  {
                                    controls:  {
                                            volume: false,
                                            time: false
                                    }
                            }
                        });
                    </script>
                <?else:?>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a>
                <?endif?>                                        
                <div class="interview_name">
                    <a class="no_border" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><h3><?=$arItem["NAME"]?></h3></a>                        
                    <div class="clear"></div>
                </div>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
                <div class="left">Комментарии: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
                <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                <div class="clear"></div>
            </div>
            <?if(($key % 3 == 2)):?>
                <div class="clear"></div>
                <?if (end($arResult["ITEMS"])!=$arItem):?>
                    <div class="border_line"></div>        
                <?endif;?>
            <?endif?>
        <?endforeach;?>    
<?else:?>
        <h4><?=GetMessage("NO_RESULTS")?></h4>
<?endif;?>    
        <div class="clear"></div>
    <hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?$excUrlParams=Array("pageCnt","page","PAGEN_1")?>
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageCnt=20", $excUrlParams).'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageCnt=40", $excUrlParams).'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="'.$APPLICATION->GetCurPageParam("pageCnt=60", $excUrlParams).'">60</a>')?>
        </div>
        <div class="right">            
            	<?=$arResult["NAV_STRING"]?>            
        </div>
    <div class="clear"></div>
    <br /><br />