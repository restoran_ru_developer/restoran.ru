<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="left <?=($i%3==2)?"end":""?>" style="width:232px; margin-right:12px">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /> </a>       
            <div class="interview_name" style="height:auto;">
                <div  class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>                
                <div class="clear"></div>
            </div>            
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <div style="margin-top:10px">
            <div class="left"><?=GetMessage("R_COMMENTS")?>: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
            <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>                
        </div>
    </div>
    <?if ($i%3==2):?>
        <div class="clear"></div>
    <?endif;?>
    <?if ($i%3==2):?>
        <?if (end($arResult["ITEMS"])!=$arItem):?>
            <div class="border_line"></div>
        <?endif;?>
    <?endif;?>
    <?$i++;?>
<?endforeach;?>
<div class="clear"></div>
<?if ($arParams["ALL"]):?>
    <Br />
    <div align="right"><a class="uppercase" href="<?=$arParams["ALL"]?>"><?=$arParams["ALL_D"]?></a></div>
<? endif; ?>
