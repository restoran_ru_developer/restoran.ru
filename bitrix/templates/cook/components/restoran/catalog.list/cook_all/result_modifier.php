<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    //$arItem["DETAIL_PAGE_URL"] = str_replace("content",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 232, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    //$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::GetPath($arItem["DETAIL_PICTURE"]);
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
    {
        $arItem["AUTHOR_NAME"] = ($arUser["NAME"])?$arUser["NAME"]:$arUser["LAST_NAME"];
    }

    // format date
    if (!$arItem["DISPLAY_ACTIVE_FROM"]):
        $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    else:
        $arTmpDate = $arItem["DISPLAY_ACTIVE_FROM"];
    endif;
    $arTmpDate = explode(" ", $arTmpDate);
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
    
    //$arResult["ITEMS"][$key]["IBLOCK_SECTION"] = RestIBlock::getCookerySection($arItem["IBLOCK_SECTION_ID"]);
    
    if ($arParams["IBLOCK_TYPE"]=="videonews")
    {
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$arItem["ID"],"PROPERTY_BLOCK_TYPE_VALUE"=>"Видео"),false,false,Array("PROPERTY_VIDEO"));
        if ($ar = $res->GetNext())
            $arItem["VIDEO"] = $ar["PROPERTY_VIDEO_VALUE"];        
    }
    //$arItem["DETAIL_PAGE_URL"] = str_replace("content", CITY_ID, $arItem["DETAIL_PAGE_URL"]);
}
?>