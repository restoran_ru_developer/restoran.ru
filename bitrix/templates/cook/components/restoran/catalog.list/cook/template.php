<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="left <?=($i%3==2)?"end":""?>" style="width:<?=($key==0)?"476px":"232px"?>; margin-right:12px">
        <?if ($key==0):?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="478" /></a>
            <div class="interview_name">
                <div  class="title left"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>  
                <!--<div class="rating left" style="padding-top:17px;padding-left:30px;">
                        <?/*for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                            <div class="small_star_a"></div>                                        
                        <?endfor?>
                        <?for($p;$p<5;$p++):?>
                            <div class="small_star"></div>
                        <?endfor;*/?>
                </div>-->
                <div class="clear"></div>                
            </div>            
            <!--<p><?=$arItem["PREVIEW_TEXT"]?></p>             -->
            <?$i++;?>
        <?else:?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /> </a>       
            <div class="interview_name">
                <div  class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>                
                <!--<div class="rating">
                    <?/*for($p=0;$p<$arItem["PROPERTIES"]["RATIO"];$p++):?>
                        <div class="small_star_a"></div>                                        
                    <?endfor?>
                    <?for($p;$p<5;$p++):?>
                        <div class="small_star"></div>
                    <?endfor;*/?>
                </div>-->
                <div class="clear"></div>
            </div>            
            <p <?=($key==1)?"style='display:block;'":""?>><?=$arItem["PREVIEW_TEXT"]?></p>
        <?endif;?>
        <div style="margin-top:10px">
            <div class="left"><?=GetMessage("R_COMMENTS")?>: (<a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>#comments"><?=intval($arItem["PROPERTIES"]["COMMENTS"])?></a>)</div>
            <div class="right"><a class="another" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>                
        </div>
    </div>
    <?if ($key==1||$i%3==2):?>
        <div class="clear"></div>
    <?endif;?>
    <?if ($i%3==2):?>
        <?if (end($arResult["ITEMS"])!=$arItem):?>
            <div class="border_line"></div>
        <?endif;?>
    <?endif;?>
    <?$i++;?>
<?endforeach;?>
<div class="clear"></div>