<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    
    // truncate text
    //$arItem["DETAIL_PAGE_URL"] = str_replace("content",CITY_ID,$arItem["DETAIL_PAGE_URL"]);
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    
    //$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::GetPath($arItem["DETAIL_PICTURE"]);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 240, 'height' => 127), BX_RESIZE_IMAGE_EXACT, true);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"];
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
        $arItem["AUTHOR_NAME"] = ($arUser["NAME"])?$arUser["NAME"]:$arUser["LAST_NAME"];

    // format date
    if (!$arItem["DISPLAY_ACTIVE_FROM"]):
        $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    else:
        $arTmpDate = $arItem["DISPLAY_ACTIVE_FROM"];
    endif;
    $arTmpDate = explode(" ", $arTmpDate);
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
    
    /*$id_section = $DB->ForSql($arItem["IBLOCK_SECTION_ID"]);
    $sql = "SELECT b.CODE as CODE FROM b_iblock_section a
            JOIN b_iblock_section b ON b.ID = a.IBLOCK_SECTION_ID
            WHERE a.ID = ".$id_section." LIMIT 1";
    $r = $DB->Query($sql);
    if ($ar = $r->Fetch())
        $arResult["ITEMS"][$key]["IBLOCK_SECTION"] = $ar["CODE"];
    */
    /*if ($arParams["IBLOCK_TYPE"]=="videonews")
    {
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$arItem["ID"],"PROPERTY_BLOCK_TYPE_VALUE"=>"Видео"),false,false,Array("PROPERTY_VIDEO"));
        if ($ar = $res->GetNext())
            $arItem["VIDEO"] = $ar["PROPERTY_VIDEO_VALUE"];        
    }*/
    if ($arItem["IBLOCK_SECTION_ID"]=="57429"):
        preg_match("/#FID_([0-9]+)#/",$arItem["DETAIL_TEXT"],$video1);
        if ($video1[1])
            $arResult["ITEMS"][$key]["VIDEO"] = "http://".SITE_SERVER_NAME."".CFile::GetPath($video1[1]);
        /*preg_match("/<iframe src=\"([a-z0-9&;:?.=\/]+)\" width=\"([0-9]+)\" height=\"([0-9]+)\"/",$arItem["DETAIL_TEXT"],$video2);
        if ($video2[1]&&$video2[2]&&$video2[3])
        {
            $height = 232*$video2[3]/$video2[2];
            $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$video2[1].'" width="232" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        }
        preg_match("/<iframe width=\"([0-9]+)\" height=\"([0-9]+)\" src=\"([a-zA-Z:.=\/]+)\"/",$arItem["DETAIL_TEXT"],$video3);    
        if ($video3[1]&&$video3[2]&&$video3[3])
        {
            $height = 232*$video3[2]/$video3[1];
            $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$video3[3].'" width="232" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        }*/
        preg_match("/<iframe ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\"/",$arItem["DETAIL_TEXT"],$video2);
        if ($video2[1])
        {
            for ($i=1;$i<count($video2);$i++)
            {
                if ($video2[$i]=="width")
                    $width = $video2[++$i];
                elseif ($video2[$i]=="height")
                    $height = $video2[++$i];
                elseif($video2[$i]=="src")
                    $src = $video2[++$i];                
            }
        }
        if ($width&&$height&&$src)
        {            
            $height = 232*$height/$width;
            $arResult["ITEMS"][$key]["VIDEO_PLAYER"] = '<iframe src="'.$src.'" width="232" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        }
    endif;
}
?>