<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="left <?=($i%3==2)?"end":""?>" style="width:<?=($key==0)?"481px":"232px"?>; margin-right:15px">
        <?if ($key==0):?>
            <a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" width="480" /></a>
            <div class="interview_name">
                <div  class="title left"><a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=$arItem["NAME"]?></a></div>  
                <div class="rating left" style="padding-top:17px;padding-left:30px;">
                        <?for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                            <div class="small_star_a"></div>                                        
                        <?endfor?>
                        <?for($p;$p<5;$p++):?>
                            <div class="small_star"></div>
                        <?endfor;?>
                </div>
                <div class="clear"></div>                
            </div>            
            <?$i++;?>
        <?else:?>
            <a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /> </a>       
            <div class="interview_name">
                <div  class="title"><a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=$arItem["NAME"]?></a></div>                
                <div class="rating">
                    <?for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                        <div class="small_star_a"></div>                                        
                    <?endfor?>
                    <?for($p;$p<5;$p++):?>
                        <div class="small_star"></div>
                    <?endfor;?>
                </div>
                <div class="clear"></div>
            </div>            
            <div class="interview_text">
                <p><?=$arItem["DESCRIPTION"]?></p>
            </div>
        <?endif;?>
        <div style="margin-top:10px">
            <div class="left">Комментарии: (<a class="another" href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/#comments"><?=intval($arItem["UF_SECTION_COMM_CNT"])?></a>)</div>
            <div class="right"><a class="another" href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>                
        </div>
    </div>
    <?if ($key==1||$i%3==2):?>
        <div class="clear"></div>
    <?endif;?>
    <?if ($i%3==2):?>
        <?if (end($arResult["ITEMS"])!=$arItem):?>
            <div class="border_line"></div>
        <?endif;?>
    <?endif;?>
    <?$i++;?>
<?endforeach;?>
<div class="clear"></div>