<?php
if (!CModule::IncludeModule("iblock"))
    return false;
foreach ($arResult["ITEMS"] as &$arItem)
{
    if ($arItem["UF_COMMENTS"])
    {
        $arItem["RATIO"] = getReviewsRatio(88,$arItem["UF_COMMENTS"]);
    }    
    $arSelect = Array("ID", "NAME", "PROPERTY_video");
    $arFilter = Array("IBLOCK_ID"=>$arItem["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_video"=>false, "SECTION_ID"=>$arItem["ID"]);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    if($ob = $res->GetNext())
    {  
        $arItem["VIDEO"] = $ob["PROPERTY_VIDEO_VALUE"];
    }
    if ($arItem["PARENT_SECTION"]["SECTION_ID"])
    {
        $r = CIBlockSection::GetById($arItem["PARENT_SECTION"]["SECTION_ID"]);
        if ($ar = $r->Fetch())
        {
            $arItem["IBLOCK_SECTION_CODE"] = $ar["CODE"];
        }   
    }
    if ($arItem["PARENT_SECTION"]["SECTION_ID"])
    {
        $r = CIBlockSection::GetById($arItem["PARENT_SECTION"]["SECTION_ID"]);
        if ($ar = $r->Fetch())
        {
            $arItem["IBLOCK_SECTION_CODE"] = $ar["CODE"];
        }   
    }
}
?>
