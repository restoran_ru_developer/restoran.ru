<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
<div class="cook_block">
        <div align="center">
            <?if($arItem["VIDEO"]):?>
                <script src="/tpl/js/flowplayer.js"></script>
                <a class="myPlayer1" href="http://<?=SITE_SERVER_NAME.$arItem["VIDEO"]["path"]?>"></a> 
                <script>
                    flowplayer("a.myPlayer1", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                        clip: {
                                autoPlay: false, 
                                autoBuffering: true
                        },
                        plugins:  {
                                controls:  {
                                        volume: false,
                                        time: false
                                }
                        }
                    });
                </script>
            <?else:?>
                <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" />
            <?endif?>
        </div>
        <div class="interview_name" style="height:auto">
            <a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/" class="no_border"><h3><?=$arItem["NAME"]?></h3></a>
            <div class="rating">
                    <?for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                        <div class="small_star_a"></div>                                        
                    <?endfor?>
                    <?for($p;$p<5;$p++):?>
                        <div class="small_star"></div>
                    <?endfor;?>
            </div>
            <div class="clear"></div>
        </div>
</div>    
        <?if (end($arResult["ITEMS"])!=$arItem):?>
            <div class="dotted"></div>
        <?endif;?>
<?endforeach;?>