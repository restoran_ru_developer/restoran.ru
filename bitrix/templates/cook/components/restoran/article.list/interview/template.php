<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>     
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <?if ($key==0):?>
        <div id="content">
            <div class="left" style="width:730px">
                <h1><?=$arItem["IBLOCK_SECTION_NAME"]?></h1>
                <?
                $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE","clear_cache");
                $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=new&".(($_REQUEST["pageSort"]=="new"&&$_REQUEST["by"]=="desc"||!$_REQUEST["pageSort"]) ? "by=asc": "by=desc"), $excUrlParams);                
                $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "")."pageSort=popular&".(($_REQUEST["pageSort"]=="popular"&&$_REQUEST["by"]=="asc"||!$_REQUEST["pageSort"]) ? "by=desc": "by=asc"), $excUrlParams);
                ?>                
                <div class="sorting">
                    <div class="left">
                        <!--<span class="z">по новизне</span> / <span><a href="#" class="another">по популярности</a></span>-->
                        <?$by = ($_REQUEST["by"]=="asc")?"a":"z";?>
                        <?if ($_REQUEST["pageSort"] == "new" || !$_REQUEST["pageSort"]):                                                   
                            echo "<span class='".$by."'><a class='another' href='".$sortNewPage."'>".GetMessage("SORT_NEW_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortNewPage.'">'.GetMessage("SORT_NEW_TITLE").'</a></span>';
                        endif;?>
                         / 
                        <?if ($_REQUEST["pageSort"] == "popular"):
                            echo "<span class='".$by."'><a class='another' href='".$sortPopularPage."'>".GetMessage("SORT_POPULAR_TITLE")."</a></span>";
                        else:
                            echo '<span><a class="another" href="'.$sortPopularPage.'">'.GetMessage("SORT_POPULAR_TITLE").'</a></span>';
                        endif;?>
                    </div>
                    <div class="right"><?//=$arResult["NAV_STRING"]?></div>                    
                    <div class="clear"></div>
                </div>
               <?endif;?>
            <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-3)):?>
                <div class="left" style="width:730px">
                    <img src="/bitrix_personal/templates/kupon/images/middle_baner.png" />                    
            <?endif;?>
                <div class="new_restoraunt left<?if($key % 3 == 2):?> end<?endif?>">
                    <div style="margin-bottom:10px;">
                        <a href="#" class="another"><?=$arItem["AUTHOR_NAME"]?></a>,  <span class="statya_date"><?=$arItem["CREATED_DATE_FORMATED_1"]?></span> <?=$arItem["CREATED_DATE_FORMATED_2"]?>
                    </div>
                    <?if($arItem["VIDEO"]):?>
                        <script src="/tpl/js/flowplayer.js"></script>
                        <a class="myPlayer1" href="http://<?=SITE_SERVER_NAME.$arItem["VIDEO"]["path"]?>"></a> 
                        <script>
                            flowplayer("a.myPlayer1", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
                                clip: {
                                        autoPlay: false, 
                                        autoBuffering: true
                                },
                                plugins:  {
                                        controls:  {
                                                volume: false,
                                                time: false
                                        }
                                }
                            });
                        </script>
                    <?else:?>
                        <a href="/content/cookery/<?=$arParams["SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /></a>
                    <?endif?>                                        
                    <div class="interview_name paddingbottom10">
                        <a class="no_border" href="/content/cookery/<?=$arParams["SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><h3><?=$arItem["NAME"]?></h3></a>
                        <div class="rating">
                                <?for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                                    <div class="small_star_a"></div>                                        
                                <?endfor?>
                                <?for($p;$p<5;$p++):?>
                                    <div class="small_star"></div>
                                <?endfor;?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="interview_text">
                        <p><?=$arItem["DESCRIPTION"]?></p>
                    </div>
                    <div class="left">Комментарии: (<a class="another" href="<?=$arItem["SECTION_PAGE_URL"]?>#comments"><?=intval($arItem["UF_SECTION_COMM_CNT"])?></a>)</div>
                    <div class="right"><a class="another" href="/content/cookery/<?=$arParams["SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
                    <div class="clear"></div>
                </div>
            <?if(($key % 3 == 2) && !(count($arResult["ITEMS"])>=20 && $key > (count($arResult["ITEMS"])-5))&&$key!=8):?>
                <div class="clear"></div>
                <div class="border_line"></div>        
            <?endif?>
    <?if ($key==8 || (count($arResult["ITEMS"])<9 && $arItem == end($arResult["ITEMS"]))):?>
            </div>
            <div class="right" style="width:240px">
                <div id="search_article">
                    <form action="<?=$arResult["FORM_ACTION"]?>">
                        <div class="title">Рассылка</div>
                        <a href="#" class="js another">кулинария</a><br />
                        <div class="subscribe_block left">
                            <input class="placeholder" type="text" name="sf_EMAIL" value="<?=($arResult["EMAIL"] ? $arResult["EMAIL"] : "Email")?>" defvalue="Email" />
                        </div>
                        <div class="right">
                            <input class="subscribe" type="image" src="<?=SITE_TEMPLATE_PATH?>/images/subscribe_submit.png" name="OK" value="" title="<?=GetMessage("subscr_form_button")?>" />
                        </div>
                        <div class="clear"></div>
                    </form>            
                </div>
                <div align="right">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/right_baner1.png" />
                </div>
                <br />
                <div class="tags">
                    <?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                            "SORT" => "CNT",
                            "PAGE_ELEMENTS" => "20",
                            "PERIOD" => "",
                            "URL_SEARCH" => "/search/index.php",
                            "TAGS_INHERIT" => "Y",
                            "CHECK_DATES" => "Y",
                            "FILTER_NAME" => "",
                            "arrFILTER" => array(
                                    0 => "iblock_cookery",
                            ),
                            "arrFILTER_iblock_cookery" => array(
                                    0 => "70",
                            ),
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "FONT_MAX" => "24",
                            "FONT_MIN" => "12",
                            "COLOR_NEW" => "24A6CF",
                            "COLOR_OLD" => "24A6CF",
                            "PERIOD_NEW_TAGS" => "",
                            "SHOW_CHAIN" => "Y",
                            "COLOR_TYPE" => "N",
                            "WIDTH" => "100%"
                            ),
                            $component
                    );?>  
                </div>
                <br />
                <div align="right">
                    <a href="#" class="uppercase">ВСЕ ТЕГИ</a>
                </div>
                <br />
                <br />
		<div class="top_block">Видео-рецепты</div>
		<?$APPLICATION->IncludeComponent(
					"restoran:article.list",
					"interview_one_top",
					Array(
						"IBLOCK_TYPE" => "cookery",
						"IBLOCK_ID" => "70",
						"SECTION_ID" => 240,
						"SECTION_CODE" => "",
						"PAGE_COUNT" => 3,
						"SECTION_URL" => "/content/cookery/detail.php?SECTION_ID=#ID#",
						"PICTURE_WIDTH" => 232,
						"PICTURE_HEIGHT" => 127,
						"DESCRIPTION_TRUNCATE_LEN" => 200,
						"COUNT_ELEMENTS" => "Y",
						"TOP_DEPTH" => "3",
						"SECTION_FIELDS" => "",
						"SECTION_USER_FIELDS" => Array("UF_COUNT","UF_COMMENTS"),
						"SORT_FIELDS" => "SORT",
						"SORT_BY" => "DESC",
						"ADD_SECTIONS_CHAIN" => "Y",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_NOTES" => "",
						"CACHE_GROUPS" => "Y",
						"PAGER_SHOW_ALWAYS" => "Y",
						"PAGER_TEMPLATE" => "kupon_list",	
						"PAGER_DESC_NUMBERING" => "N",	
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",					
					),
					false
				);?>
		<hr />
		<div align="right"><a href="#">ВСЕ ВИДЕО</a></div>
		<br /><br />
		<div class="top_block">Топ-3 рецептов</div>
		<?$APPLICATION->IncludeComponent(
					"restoran:article.list",
					"interview_one_top",
					Array(
						"IBLOCK_TYPE" => "cookery",
						"IBLOCK_ID" => "70",
						"SECTION_ID" => Array(0=>"234",1=>"235"),
						"SECTION_CODE" => "",
						"PAGE_COUNT" => 3,
						"SECTION_URL" => "/content/cookery/detail.php?SECTION_ID=#ID#",
						"PICTURE_WIDTH" => 232,
						"PICTURE_HEIGHT" => 127,
						"DESCRIPTION_TRUNCATE_LEN" => 200,
						"COUNT_ELEMENTS" => "Y",
						"TOP_DEPTH" => "3",
						"SECTION_FIELDS" => "",
						"SECTION_USER_FIELDS" => Array("UF_COUNT","UF_COMMENTS"),
						"SORT_FIELDS" => "UF_COUNT",
						"SORT_BY" => "DESC",
						"ADD_SECTIONS_CHAIN" => "Y",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_NOTES" => "",
						"CACHE_GROUPS" => "Y",
						"PAGER_SHOW_ALWAYS" => "Y",
						"PAGER_TEMPLATE" => "kupon_list",	
						"PAGER_DESC_NUMBERING" => "N",	
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",					
					),
					false
				);?> 
		<!--<hr />
		<div align="right"><a href="#">ВСЕ НОВЫЕ</a></div>
		<br /><br />-->
            </div>
            <div class="clear"></div>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])>=20 && $key == (count($arResult["ITEMS"])-1)):?>
            </div>
            <div class="right">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/right_baner2.png" />
            </div>
            <div class="clear"></div>
    <?endif;?>                
    <?if (count($arResult["ITEMS"])>9 && ($key+1) == count($arResult["ITEMS"])):?>
        </div>
    <?endif;?>
    <?if (count($arResult["ITEMS"])<9 && end($arResult["ITEMS"])==$arItem):?>
        </div></div>
    <?endif;?>
<?endforeach;?>          
<div class="clear"></div>        
<div id="content">
<hr class="bold" />        
        <div class="left">
            <?=GetMessage("SHOW_CNT_TITLE")?>&nbsp;
            <?=($_REQUEST["pageCnt"] == 20 || !$_REQUEST["pageCnt"] ? "20" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">20</a>')?>
            <?=($_REQUEST["pageCnt"] == 40 ? "40" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=40'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">40</a>')?>
            <?=($_REQUEST["pageCnt"] == 60 ? "60" : '<a class="another" href="?page='.$_REQUEST["PAGEN_1"].'&pageCnt=60'.($_REQUEST["pageSort"] ? "&pageSort=".$_REQUEST["pageSort"] : "").'">60</a>')?>
        </div>
        <div class="right">            
            	<?=$arResult["NAV_STRING"]?>            
        </div>
    <div class="clear"></div>
    <br /><br />
    <img src="/tpl/images/direct.jpg" />
    <br /><br />
    <div align="center">
    <?$APPLICATION->IncludeComponent(
	"bitrix:advertising.banner",
	"",
	Array(
		"TYPE" => "bottom_cook_list",
		"NOINDEX" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "0"
	),
    false
    );?>
    </div>
    <br /><br />    
</div>