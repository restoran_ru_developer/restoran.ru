<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("blog"))
    return false;

foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
        $arItem["AUTHOR_NAME"] = $arUser["NAME"];

    // format date
    $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
    $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    $arTmpDate = explode(" ", $arTmpDate);
    
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];

    $arSelect = Array("ID", "NAME", "PROPERTY_video");
    $arFilter = Array("IBLOCK_ID"=>$arItem["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_video"=>false, "SECTION_ID"=>$arItem["ID"]);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    if($ob = $res->GetNext())
    {  
        $arItem["VIDEO"] = $ob["PROPERTY_VIDEO_VALUE"];
    }
    if ($arItem["UF_COMMENTS"])
    {
        $arItem["RATIO"] = getReviewsRatio(88,$arItem["UF_COMMENTS"]);
    }
    if ($arItem["PARENT_SECTION"]["SECTION_ID"])
    {
        $r = CIBlockSection::GetById($arItem["PARENT_SECTION"]["SECTION_ID"]);
        if ($ar = $r->Fetch())
        {
            $arItem["IBLOCK_SECTION_CODE"] = $ar["CODE"];
            $arItem["IBLOCK_SECTION_NAME"] = $ar["NAME"];
        }   
    }

}
?>