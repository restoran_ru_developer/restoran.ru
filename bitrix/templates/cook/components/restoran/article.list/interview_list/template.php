<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="left <?=($key%3==2)?"end":""?>" style="width:232px; margin-right:15px">    
            <a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" /> </a>       
            <div class="interview_name">
                <div class="title"><a href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=$arItem["NAME"]?></a></div>
                <div class="rating">
                    <?for($p=0;$p<$arItem["RATIO"]["RATIO"];$p++):?>
                        <div class="small_star_a"></div>                                        
                    <?endfor?>
                    <?for($p;$p<5;$p++):?>
                        <div class="small_star"></div>
                    <?endfor;?>
                </div>
                <div class="clear"></div>
            </div>            
            <div class="interview_text">
                <p><?=$arItem["DESCRIPTION"]?></p>
            </div>
        <div class="left">Комментарии: (<a class="another" href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/#comments"><?=intval($arItem["UF_SECTION_COMM_CNT"])?></a>)</div>
        <div class="right"><a class="another" href="/content/cookery/<?=$arItem["IBLOCK_SECTION_CODE"]?>/<?=$arItem["CODE"]?>/"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>                
    </div>
    <?if ($key%3==2):?>
        <div class="clear"></div>
        <?if (end($arResult["ITEMS"])!=$arItem):?>
            <div class="border_line"></div>
        <?endif;?>
    <?endif;?>
<?endforeach;?>