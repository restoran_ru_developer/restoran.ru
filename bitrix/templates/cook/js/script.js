var ajax_load = false;
jQuery(document).ready(function() {
    //snow
    //$('body').snowFall({'color':'#24A6CF','total':'30','interval':'5'});
    // ajax loader
    setInterval("start_rounded()",3000);
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/144.gif" align="left"> Загрузка...</div>');
    $('body').append('<div id="system_overflow"></div>');
    /*$("#system_loading").css("left", $(window).width() / 2 - 104 / 2 + "px");
    $("#system_loading").css("top", $(window).height() / 2 - 104 / 2 + "px");*/
    $("#system_loading").bind("ajaxSend",
        function() {
            $(this).show();
            ajax_load = true;
        }).bind("ajaxComplete", function() {
            $(this).hide();
            ajax_load = false;
        });

    var params = {
        changedEl:"#search_in",
        visRows:12
        /*scrollArrows: true*/
    }
    cuSel(params);
    $("ul.tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
            $(this).tabs("div.panes > .pane", {                
                effect: 'fade',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        else
            $(this).tabs("div.panes > .pane");
    });    
    //$("ul#poster_tabs").tabs("div.poster_panes > .poster_pane");
    $("ul#poster_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.poster_panes > .poster_pane", {                
                effect: 'fade',  
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.poster_panes > .poster_pane");
    }); 
    $("ul.history_tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
        {
            var url = "";
            if ($(this).attr("ajax_url"))
                url = $(this).attr("ajax_url");
            $(this).tabs("div.panes > .pane", {                
                effect: 'fade',  
                history: 'true',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(url+this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        }
        else
            $(this).tabs("div.panes > .pane");
    }); 
    $(".to_top").on('click', function() {
        $('html,body').animate({scrollTop:"0"}, 500);
    });    
    $("a.anchor").click(function(){
        var pos = $($(this).attr("href")).offset().top-10;
        $('html,body').animate({scrollTop:pos}, "300");
    });
    $("input[type^='radio']").each(
        function() {
             changeRadioStart($(this));
    });
     $(".modal_close").live('click',function(){
        $(this).parents(".popup_modal").fadeOut(300);
        hideOverflow();
    });
});

function showExpose() {
    $(document).mask({ color: '#1a1a1a', loadSpeed: 200, closeSpeed:0, opacity: 0.5, closeOnEsc: false, closeOnClick: false });
}
function closeExpose() {
    $.mask.close();
}

function send_ajax(url, dataType, params, successFunc) {
    //showExpose();
    if(!dataType)
        dataType = 'json';
    params = eval('(' + params + ')');
    $.ajax({
        url:url,
        type: 'post',
        dataType: dataType,
        data: params,
        statusCode: {
            404:function() {
                alert('Page not found');
            }
        },
        success:function(data) {
            if (successFunc)
                successFunc(data);
            //closeExpose();
        }
    });
}

function sumbit_form(form, beforeFunc,successFunc) {
    var params = $(form).serialize();
    var url = $(form).attr("action");
    console.log(url);
    $.ajax({
        url:url,
        type:'post',
        data:params,
        statusCode:{
            404:function() {
                alert('Page not found');
            }
        },
        beforeSend:function() {
            /*if (typeof(before_submit_form) == "function")
                before_submit_form(form);
            return false;*/
            if (beforeFunc)
                beforeFunc(form);
        },
        success:function(data) {
            $(form).parent().html(data);
            /*if (typeof(success_submit_form) == "function")
                success_submit_form();*/
            if (successFunc)
                successFunc(form,data);
        }
    });
    return false;
}

function show_popup(obj,options)
{
    if(!options)
        options = {"obj":$(obj).parent().attr("id")};
    else
        options.obj = $(obj).parent().attr("id");    
    if (!$(obj).parent().find("div").hasClass("popup"))
        $("<div>").addClass("popup").appendTo($(obj).parent()).popup(options);
}
function setCenter(obj)
{
    var ow = obj.width();
    var oh = obj.height();
    var opt = obj.css("padding-top");
    if (opt)
        opt = opt.replace("px","");
    var opb = obj.css("padding-bottom");
    if (opb)
        opb = opb.replace("px","");    
    var opl = obj.css("padding-left");
    if (opl)
        opl = opl.replace("px","");  
    var opr = obj.css("padding-right");
    if (opr)
        opr = opr.replace("px","");  
    
    var omt = obj.css("margin-top");
    if (omt)
        omt = omt.replace("px","");
    var omb = obj.css("margin-bottom");
    if (omb)
        omb = omb.replace("px","");    
    var oml = obj.css("margin-left");
    if (oml)
        oml = oml.replace("px","");  
    var omr = obj.css("margin-right");
    if (omr)
        omr = omr.replace("px","");
    oh = oh*1 + opt*1 + opb*1 + omt*1 + omb*1;
    ow = ow*1 + opl*1 + opr*1 + oml*1 + omr*1;
    //obj.css("position","absolute");
    obj.css("top",($(window).height()-oh)/2+"px");
    obj.css("left",($(window).width()-ow)/2+"px");
    obj.css("z-index",10000);
}
function showOverflow()
{
    $("#system_overflow").fadeIn(300);
    $("#system_overflow").css("height",$(document).height());
}
function hideOverflow()
{
    $("#system_overflow").fadeOut(300);
}
function ajax_bron(params)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_rest.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:400px'></div>").appendTo("body");                                                               
            }
            $(".popup_modal").css("background-color","#2D323B");
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").fadeIn("300"); 
        }
    });
}
function call_me(params)
{
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/call_me.php",
        data: params,
        success: function(data) {
            if (!$("#call_me_form").size())
            {
                $("<div class='popup popup_modal' id='call_me_form' style='width:335px'></div>").appendTo("body");                                                               
            }
            $('#call_me_form').html(data);
            showOverflow();
            setCenter($("#call_me_form"));
            $("#call_me_form").fadeIn("300"); 
        }
    });
}
function favorite_success(data)
{
    //data = eval('(' + data + ')');
    if (!$("#favorite_form").size())
    {
        $("<div class='popup popup_modal' id='favorite_form' style='width:335px'></div>").appendTo("body");                                                               
    }
    $('#favorite_form').html(data);
    showOverflow();
    setCenter($("#favorite_form"));
    $("#favorite_form").fadeIn("300"); 
    if (!data.ERROR)
    {        
        if (data.MESSAGE)
            $("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.MESSAGE);
    }
    else
            $("#favorite_form").html('<div align="right"><a class="modal_close uppercase white" href="javascript:void(0)"></a></div>'+data.ERROR);
}

function ajax_bron(params,a)
{
    var b = "400px";
    if (a)
        b = "800px";
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_rest.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");                                                               
            }
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").css("display","block"); 
        }
    });
}

function ajax_bron2(params,a)
{
    var b = "400px";
    if (a)
        b = "800px";
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_banket.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");                                                               
            }
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").css("display","block"); 
        }
    });
}
function start_rounded()
{
    if ($("#rounded1").is(":hidden"))
    {
        $("#rounded1").show();
        $("#rounded_first").show();
        $("#rounded2").hide();
        $("#rounded_second").hide();
    }
    else
    {
        $("#rounded1").hide();
        $("#rounded_first").hide();
        $("#rounded2").show();
        $("#rounded_second").show();
    }
}
jQuery.fn.system_message=function(options)
{
    this.init=function(){
        $(this).append('<div id="system_message"></div>');
        var _this=$("#system_message");
        $("#system_message").on("click","#sys_mes_close",function(){
            $("#system_message").fadeOut(300);
        });
        return _this;
    };
    return this.init();
}
$(document).ready(function(){$("body").system_message();});