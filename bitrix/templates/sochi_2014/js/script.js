jQuery(document).ready(function() {
    // ajax loader
    $('body').append('<div id="system_loading"><img src="/bitrix/templates/main/images/144.gif" align="left"> Загрузка...</div>');
    $('body').append('<div id="system_overflow"><div class="modal_close_galery"></div></div>');    
    /*$("#system_loading").css("left", $(window).width() / 2 - 104 / 2 + "px");
    $("#system_loading").css("top", $(window).height() / 2 - 104 / 2 + "px");*/
    $("#system_loading").bind("ajaxSend",
        function() {
            $(this).show();
        }).bind("ajaxComplete", function() {
            $(this).hide();
        });

    /*var params = {
        changedEl:"#search_in",
        visRows:5
    }
    cuSel(params);*/
    $("ul.tabs").each(function(){
        if ($(this).attr("ajax")=="ajax")            
            $(this).tabs("div.panes > .pane", {                
                effect: 'fade',
                onBeforeClick: function(event, i) {
                    var pane = this.getPanes().eq(i);
                    if (pane.is(":empty")) {
                        pane.load(this.getTabs().eq(i).attr("href"));			
                    }			
                }	
            });
        else
            $(this).tabs("div.panes > .pane",{effect: 'fade'});
    });    
    $("ul#poster_tabs").tabs("div.poster_panes > .poster_pane");
    $(".to_top").on('click', function() {
        $('html,body').animate({scrollTop:"0"}, 500);
    });    
    $(".modal_close").live('click',function(){
        $(this).parents(".popup_modal").css("display","none");
        hideOverflow();
    });
    $("#system_overflow").click(function(){
        $(".big_modal").css("display","none");
        hideOverflow();
        $(".popup").css("display","none");
    });
    $(".modal_close_galery").live('click',function(){
        $(".big_modal").css("display","none");
        hideOverflow();
    });
});

function showExpose() {
    $(document).mask({ color: '#1a1a1a', loadSpeed: 200, closeSpeed:0, opacity: 0.5, closeOnEsc: false, closeOnClick: false });
}
function closeExpose() {
    $.mask.close();
}

function send_ajax(url, dataType, params, successFunc) {
    //showExpose();
    if(!dataType)
        dataType = 'json';
    params = eval('(' + params + ')');
    $.ajax({
        url:url,
        type: 'post',
        dataType: dataType,
        data: params,
        statusCode: {
            404:function() {
                alert('Page not found');
            }
        },
        success:function(data) {
            if (successFunc)
                successFunc(data);
            //closeExpose();
        }
    });
}

function sumbit_form(form, beforeFunc,successFunc) {
    var params = $(form).serialize();
    var url = $(form).attr("action");
    $.ajax({
        url:url,
        type:'post',
        data:params,
        statusCode:{
            404:function() {
                alert('Page not found');
            }
        },
        beforeSend:function() {
            /*if (typeof(before_submit_form) == "function")
                before_submit_form(form);
            return false;*/
            if (beforeFunc)
                beforeFunc(form);
        },
        success:function(data) {
            $(form).parent().html(data);
            /*if (typeof(success_submit_form) == "function")
                success_submit_form();*/
            if (successFunc)
                successFunc(form);
        }
    });
    return false;
}

function show_popup(obj,options)
{
    if(!options)
        options = {"obj":$(obj).parent().attr("id")};
    else
        options.obj = $(obj).parent().attr("id");    
    if (!$(obj).parent().find("div").hasClass("popup"))
        $("<div>").addClass("popup").appendTo($(obj).parent()).popup(options);
}
function ajax_bron2(params,a)
{
    var b = "400px";
    if (a)
        b = "800px";
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_banket.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");                                                               
            }
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").css("display","block"); 
            $(".modal_close").click(function(){
               $(this).parent().parent().hide();
            });
        }
    });
}
function showOverflow()
{
    //$("body").css("overflow","hidden");
    //$("#system_overflow").css("display","block");
    $("#system_overflow").fadeIn(300);
    //$("#system_overflow").css("opacity",.5);
    $("#system_overflow").css("height",$(document).height());
}
function hideOverflow()
{
    $("#system_overflow").css("display","none");
    $("body").css("overflow","auto");
}
function setCenter(obj)
{
    var ow = obj.width();
    var oh = obj.height();
    var opt = obj.css("padding-top");
    if (opt)
        opt = opt.replace("px","");
    var opb = obj.css("padding-bottom");
    if (opb)
        opb = opb.replace("px","");    
    var opl = obj.css("padding-left");
    if (opl)
        opl = opl.replace("px","");  
    var opr = obj.css("padding-right");
    if (opr)
        opr = opr.replace("px","");  
    if (opl=="auto")
        opl = 0;
    if (opr=="auto")
        opr = 0;
    if (opb=="auto")
        opb = 0;
    if (opt=="auto")
        opt = 0;
    
    var omt = obj.css("margin-top");
    if (omt)
        omt = omt.replace("px","");
    var omb = obj.css("margin-bottom");
    if (omb)
        omb = omb.replace("px","");    
    var oml = obj.css("margin-left");
    if (oml)
        oml = oml.replace("px","");  
    var omr = obj.css("margin-right");
    if (omr)
        omr = omr.replace("px","");
    if (oml=="auto")
        oml = 0;
    if (omr=="auto")
        omr = 0;
    if (omb=="auto")
        omb = 0;
    if (omt=="auto")
        omt = 0;
    oh = oh*1 + opt*1 + opb*1 + omt*1 + omb*1;
    ow = ow*1 + opl*1 + opr*1 + oml*1 + omr*1;
    if (obj.attr("class")=="big_modal")
    {
      obj.css("position","absolute");  
    }
    //obj.css("position","absolute");
    obj.css("top",($(window).height()-oh)/2+"px");
    obj.css("left",($(window).width()-ow)/2+"px");
    obj.css("z-index",10000);
}

function ajax_bron2(params,a)
{
    var b = "400px";
    if (a)
        b = "800px";
    alert(1);
    $.ajax({
        type: "POST",
        url: "/tpl/ajax/online_order_banket.php",
        data: params,
        success: function(data) {
            if (!$("#mail_modal").size())
            {
                $("<div class='popup popup_modal' id='bron_modal' style='width:"+b+"'></div>").appendTo("body");                                                               
            }
            $('#bron_modal').html(data);
            showOverflow();
            setCenter($("#bron_modal"));
            $("#bron_modal").css("display","block"); 
        }
    });
}