<?
if ($_REQUEST["f"])
{    
    $date = $_REQUEST["f"];
    $day = date("w",strtotime($date));
    $days = array("воскресение","понедельник","вторник","среда","четверг","пятница","суббота");    
    
    $day = $days[$day];
    $da = date("d.m",strtotime($date));
    $date = str_replace(".", "",$date);
    $file = file_get_contents("../include_areas/schedule/".$date.".json");
    $file = json_decode($file,true);             
}
?>
<div class="sch_box">
<table class="schedule" cellpadding="3">
    <tr>
        <th class="sch_date"><?=$da?></th>
        <th class="sch_day"><?=$day?></th>
    </tr>
    <?foreach ($file as $hour):        
    
        ?>
    
        <?if($hour["SOR"]):
            foreach ($hour["SOR"] as $time):?>
            <tr>
                <td><?=$time["TIME"]?></td>
                <td>
                    <span><?=$hour["TITLE"]?></span><br />
                    <?=str_replace("_","",$time["SOR"])?>
                </td>
            </tr>
        <?endforeach;
        else:?>
            <tr>
                <td ></td>
                <td >В выбранный день соревнований нет</td>
            </tr>
            
            <?endif;?>
    <?endforeach;?>
</table>
</div>