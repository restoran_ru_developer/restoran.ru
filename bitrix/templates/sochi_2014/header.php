<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? require_once($BX_DOC_ROOT . SITE_TEMPLATE_PATH . "/lang/" . SITE_LANGUAGE_ID . "/header.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <? $APPLICATION->ShowHead() ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/detail_galery.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/script.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/maskedinput.js') ?>
        <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/tools.js') ?>

        <? $APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/cusel.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/datepicker.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadString('<link href="/tpl/css/styles.css"  type="text/css" rel="stylesheet" />', true) ?>
        <? $APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js') ?>            
        <? $APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js') ?>             
        <? $APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js') ?>  
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.system_message.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/cusel-min.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/radio.js') ?>
        <? $APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js') ?>
        
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
            <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
                <?
                CModule::IncludeModule("advertising");
                CAdvBanner::SetRequiredKeywords(array(CITY_ID));
                ?>
                </head>
                <body style="background:#FFF">
                    <div id="panel"><? $APPLICATION->ShowPanel() ?></div>
                    <?if (CITY_ID=="msk"):?>
                        <div id="container" style="background: #fff url(/tpl/images/spec/sochi2014/sochi2014_bg.jpg) center top no-repeat">
                    <?elseif(CITY_ID=="spb"):?>
                        <div id="container" style="background: #fff url(/tpl/images/spec/sochi2014/sochi2014_bg_spb.jpg) center top no-repeat">
                    <?endif;?>
                        <div id="wrapp">    
<!--                            <div class="baner" style="height:90px; background: none; border-radius: 15px; width:980px; margin:0 auto; padding-top:20px;">                
                                <?$APPLICATION->IncludeComponent(
                                         "bitrix:advertising.banner",
                                         "",
                                         Array(
                                                 "TYPE" => "top_main_page",
                                                 "NOINDEX" => "Y",
                                                 "CACHE_TYPE" => "A",
                                                 "CACHE_TIME" => "600"
                                         ),
                                 false
                                 );?>
                            </div>-->
                            <div id="content" style="padding-top: 511px;">         
                                <a id="new_year_night_logo" href="/<?=CITY_ID?>/articles/sochi2014/"></a>
                                <?/*if (CITY_ID!="tmn"):?>
                                <a id="new_year_back" href="/<?=CITY_ID?>/articles/new_year/"></a>
                                <?endif;*/?>
                                <a id="new_year_logo" href="/"></a>
                                <?//if ($USER->IsAdmin()):?>
                                    <div id="schedule_box">
                                        
                                    </div>
                                    <div id="schedule_str"></div>
                                    <div id="datebox">
                                        <div class="intro vday">Выбери день</div>
                                        <div class="strl strl1"></div>
                                        <div class="strl strl2"></div>
                                        <div class="strl strl3"></div>
                                        <input type="text" class="date" />
                                        <script>                                        
                                            $(function(){                                            
                                                $.tools.dateinput.localize("ru",  {
                                                    months:        'январь,февраль,март,апрель,май,июнь,июль,август,сентябрь,октябрь,ноябрь,декабрь',
                                                    shortMonths:   'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек',
                                                    days:          'восресенье,понедельник,вторник,среда,четверг,пятница,суббота',
                                                    shortDays:     'вс,пн,вт,ср,чт,пт,сб'
                                                 });    
                                                $(".date").dateinput( {
                                                        lang: 'ru', 
                                                        firstDay: 1 , 
                                                        format:"dd.mm.yyyy",
                                                        min:"2014-02-06",
                                                        max:"2014-02-23",
                                                        // closing is not possible
                                                        onHide: function()  {
                                                                return false;
                                                        },                                                   
                                                        change: function(e, date)  {
                                                            $("#schedule_box").html(this.getValue("dmmyyyy"));
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?=SITE_TEMPLATE_PATH?>/ajax/schedule.php",
                                                                data: { f: this.getValue("d.mm.yyyy")}
                                                            }).done(function( msg ) {
                                                                $("#schedule_box").html(msg);
                                                            });                                                            
                                                        }                                            
                                                }).data("dateinput").setValue(0).show();
                                            });
                                        </script>
                                    </div>
                                <? //endif; ?>
