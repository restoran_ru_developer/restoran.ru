<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/footer.php");?>
            <div class="clear"></div>            
        </div>       
    </div>
    </div>
    <div id="footer">            
        <div class="footer_content">
            <div class="to_top"><?=GetMessage("GO_TOP")?></div>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom_menu",
                    Array(
                            "ROOT_MENU_TYPE" => "bottom_menu_".CITY_ID,
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array()
                    ),
            false
            );?>
            <div class="clear"></div>
            <br /><br />
            <div class="left">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("/tpl/include_areas/lang/".SITE_LANGUAGE_ID."/footer_copyright_1.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="left">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("/tpl/include_areas/lang/".SITE_LANGUAGE_ID."/footer_copyright_2.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="right">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("/tpl/include_areas/lang/".SITE_LANGUAGE_ID."/footer_dev_company.php"),
                    Array(),
                    Array("MODE"=>"html")
                );?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
        <?
    setSeo();
    ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33504724-1']);
  _gaq.push(['_setDomainName', 'restoran.ru']);


  _gaq.push (['_addOrganic', 'images.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'blogs.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'video.yandex.ru', 'text']);
  _gaq.push (['_addOrganic', 'mail.ru', 'q']);
  _gaq.push (['_addOrganic', 'go.mail.ru', 'q']);
  _gaq.push (['_addOrganic', 'google.com.ua', 'q']);
  _gaq.push (['_addOrganic', 'images.google.ru', 'q']);
  _gaq.push (['_addOrganic', 'maps.google.ru', 'q']);
  _gaq.push (['_addOrganic', 'rambler.ru', 'words']);
  _gaq.push (['_addOrganic', 'nova.rambler.ru', 'query']);
  _gaq.push (['_addOrganic', 'nova.rambler.ru', 'words']);
  _gaq.push (['_addOrganic', 'gogo.ru', 'q']);
  _gaq.push (['_addOrganic', 'nigma.ru', 's']);
  _gaq.push (['_addOrganic', 'search.qip.ru', 'query']);
  _gaq.push (['_addOrganic', 'webalta.ru', 'q']);
  _gaq.push (['_addOrganic', 'sm.aport.ru', 'r']);
  _gaq.push (['_addOrganic', 'meta.ua', 'q']);
  _gaq.push (['_addOrganic', 'search.bigmir.net', 'z']);
  _gaq.push (['_addOrganic', 'search.i.ua', 'q']);
  _gaq.push (['_addOrganic', 'index.online.ua', 'q']);
  _gaq.push (['_addOrganic', 'web20.a.ua', 'query']);
  _gaq.push (['_addOrganic', 'search.ukr.net', 'search_query']);
  _gaq.push (['_addOrganic', 'search.com.ua', 'q']);
  _gaq.push (['_addOrganic', 'search.ua', 'q']);
  _gaq.push (['_addOrganic', 'poisk.ru', 'text']);
  _gaq.push (['_addOrganic', 'go.km.ru', 'sq']);
  _gaq.push (['_addOrganic', 'liveinternet.ru', 'ask']);
  _gaq.push (['_addOrganic', 'gde.ru', 'keywords']);
  _gaq.push (['_addOrganic', 'affiliates.quintura.com', 'request']);
  _gaq.push (['_addOrganic', 'akavita.by', 'z']);
  _gaq.push (['_addOrganic', 'search.tut.by', 'query']);
  _gaq.push (['_addOrganic', 'all.by', 'query']);


  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter17073367 = new Ya.Metrika({id:17073367,
    enableAll: true});
    } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") +
    "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>