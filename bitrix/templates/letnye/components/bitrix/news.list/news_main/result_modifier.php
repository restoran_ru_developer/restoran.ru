<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    // explode date
    $arTmpDate = explode(" ", $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM"]);
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
    $arResult["ITEMS"][$key]["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["NAME"] = htmlspecialchars_decode($arResult["ITEMS"][$key]["NAME"]);

    if ($arItem["DETAIL_PICTURE"])
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>480, 'height'=>340), BX_RESIZE_IMAGE_EXACT, true);
    }
    else
    {
        $arResult["ITEMS"][$key]["DETAIL_PICTURE"]["src"] = "/tpl/images/noname/rest_nnm.png";
    }
    // get rest name
    $rsSec = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
    if($arSec = $rsSec->GetNext()) {
        $arResult["ITEMS"][$key]["RESTAURANT_NAME"] = htmlspecialchars_decode($arSec["NAME"]);
    }
    
    //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);   
    //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
}
?>