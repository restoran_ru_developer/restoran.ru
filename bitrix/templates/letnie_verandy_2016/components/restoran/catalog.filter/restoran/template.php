<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
    $(function(){
        $(".subza_a").click(function(){
            if (!$(this).hasClass('active'))
                $(".subza").toggle();
        })        
    })
</script>

<?foreach($arResult["arrProp"] as $prop_id => $arProp):?>
    <?if(!in_array($arProp['CODE'],$arParams['PROPERTY_CODE']))
        continue;?>
    <div class="form-group <?=($arProp['CODE']=="subway"||$arProp['CODE']=="out_city")?"subza":""?>" <?=($arProp['CODE']=="out_city")?"style='display:none;'":""?>  <?=($arProp["CODE"]=="with_dm")?"style='top:-20px;position:relative;'":""?> <?=($arProp["CODE"]=="compliment")?"style='top:24px;left:-145px;position:relative;'":""?>>
        <?if ($arProp['CODE']=="subway"):?>
            <label for="name"><a class="subza_a active" href="javascript:void(0)"><?=$arProp["NAME"]?></a> / <a class="subza_a" href="javascript:void(0)">Загородные</a></label>
        <?elseif ($arProp['CODE']=="out_city"):?>
            <label for="name"><a class="subza_a" href="javascript:void(0)">Метро</a> / <a class="subza_a active" href="javascript:void(0)">Загородные</a></label>
        <?elseif($arProp["PROPERTY_TYPE"]!="L"||$arProp['CODE']=='features'):?>
            <label for="name" for-prop="<?=$arProp['CODE']?>"><?=$arProp["NAME"]?></label>
        <?endif;?>
        <?if ($arProp["PROPERTY_TYPE"]!="L"||$arProp['CODE']=='features'):?>
            <select multiple class="multiselect-<?=$arProp["CODE"]?> form-control" name="<?=$arParams["FILTER_NAME"]?>_pf[<?=$arProp["CODE"]?>][]">                            
                <?foreach($arProp["VALUE_LIST"] as $key=>$val):?>
                    <?if ($arProp["CODE"]!="subway"):?>
                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val?></option>                
                    <?else:?>
                        <option value="<?=$key?>" <?=(in_array($key,$_REQUEST[$arParams["FILTER_NAME"]."_pf"][$arProp["CODE"]]))?"selected":""?>><?=$val["NAME"]?></option>                
                    <?endif;?>
                <?endforeach?>
            </select> 
            <script type="text/javascript">                                        
                $(function() {
                    $('.multiselect-<?=$arProp["CODE"]?>').multiselect({
                        numberDisplayed:1,
                        nonSelectedText: '<?=GetMessage('multiselect-'.$arProp["CODE"])?>',
                        nSelectedText: "Выбрано",   
                        maxHeight: 250,
                        buttonWidth: "180px"
                    });
                });
            </script>
        <?endif;?>        
    </div>        
<?endforeach;?>        
<div class="form-group" style="">
    <label>&nbsp;</label>
    <input type="submit" class="form-control btn-light" value="Найти">
</div>
<script type="text/javascript">                                        
                $(function() {
                    $(".checkbox-box div").click(function(){                        
                        console.log($(this).hasClass("active"));
                        if (!$(this).hasClass("active"))
                        {
                            $(this).next().val("Да");
                            $(this).addClass("active");
                        }
                        else
                        {
                            $(this).next().val("");                        
                            $(this).removeClass("active");
                        }
                    });
                });
            </script>