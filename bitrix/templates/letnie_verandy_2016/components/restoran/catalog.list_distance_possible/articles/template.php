<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (count($arResult["ITEMS"])>0):?>
<?if($arParams['SEE_ALSO']=='Y'):?>
<div class="left" style="">
    <div class="pt-serif like-h1-in-bottom">Смотрите также</div>
    <div id="spec_block_list_other">
<?endif?>
    <?
    foreach($arResult["ITEMS"] as $key=>$arItem):
        ?>
        <?if($key==2&&$_REQUEST['PAGEN_1']<2&&$arParams['SEE_ALSO']!='Y'):?>
            <div class="pull-left banner-in-list-place" >
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
        <?endif?>
        <div class="pull-left <?=$arParams['SEE_ALSO']!='Y'?'':'see-also-in-detail'?>" lat="<?=$arItem['PROPERTIES']['lat']['VALUE']?>" lon="<?=$arItem['PROPERTIES']['lon']['VALUE']?>" this-rest-id="<?=$arItem['PROPERTIES2']['RESTORAN'][0]?>">
            <div class="distance-place"><span></span> КМ</div>
            <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
                <div class="pic">
                    <a href="http://restoran.ru<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" /></a>
                </div>
            <?endif;?>
            <div class="text">
                <h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" target="_blank"><?=$arItem["~NAME"]?></a></h2>
                <div class="ratio">
                    <?for($z=1;$z<=5;$z++):?>
                        <span class="<?=(round($arItem["PROPERTIES"]["RATIO"]['VALUE'])>=$z)?"icon-star":"icon-star-empty"?>"></span>
                    <?endfor?>
                </div>
                <div class="props">
                    <div class="prop">
                        <?
                        $clear_city_arr = array(
                            'msk'=>"/( +|)Москва[, ]+/i",
                            'spb'=>"/( +|)Санкт-Петербург[, ]+/i",
                            'rga'=>"/( +|)Рига[, ]+/i",
                            'urm'=>"/( +|)Юрмала[, ]+/i",
                            'kld'=>"/( +|)Калиниград[, ]+/i",
                            'nsk'=>"/( +|)Новосибирск[, ]+/i",
                            'sch'=>"/( +|)Сочи[, ]+/i"
                        );
                        ?>
                        <div class="value"><?=is_array($arItem["PROPERTIES"]["address"]["VALUE"])?preg_replace($clear_city_arr[CITY_ID],'',$arItem["PROPERTIES"]["address"]["VALUE"][0]):preg_replace($clear_city_arr[CITY_ID],'',$arItem["PROPERTIES"]["address"]["VALUE"])?></div>
                    </div>
                    <div class="prop wsubway">
                        <div class="subway">M</div>
                        <div class="value"><?=implode(', ',$arItem["PROPERTIES"]["SUBWAY"]["VALUE"])?></div>
                    </div>
                    <?if($arParams['SEE_ALSO']!='Y'):?>
                    <div class="prop phone">
                        <div class="value"><?=$arItem["PROPERTIES"]["phone"]["VALUE"]?></div>
                    </div>
                    <?//if($USER->IsAdmin()):?>
                        <a href="/tpl/ajax/online_order_rest.php" class="booking book-trigger-in-list" data-id="<?=$arItem['PROPERTIES2']['RESTORAN'][0]?>" data-restoran="<?=rawurlencode($arItem['PROPERTIES']['RESTORAN'][0])?>">забронировать</a>
                    <?//endif?>
                    <?endif;?>
                </div>
            </div>
        </div>
    <?endforeach;?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]&&$arResult["NAV_STRING"]&&$arParams['AJAX']!='Y'): ?>
        <div class="clear"></div>
        <div class="navigation">
            <div class="pages"><?= $arResult["NAV_STRING"] ?></div>
        </div>
    <?endif;?>
<?elseif($arParams['NEWS_COUNT']!=3&&$arParams['SEE_ALSO']!='Y'):?>
    <?
    echo "<p>На данный момент мы не располагаем информацией о предложениях ресторана. Воспользуйтесь поиском или обратитесь в нашу службу бронирования, и мы с удовольствием подберем для вас ресторан или банкетный зал с учётом ваших пожеланий!</p>";
    ?>
<?endif;?>
<?if($arParams['SEE_ALSO']=='Y'):?>
    </div>
    </div>
<?endif?>