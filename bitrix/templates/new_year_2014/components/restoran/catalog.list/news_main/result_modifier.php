<?
foreach($arResult["ITEMS"] as $key=>$arItem) {
    $elems = array();
    foreach ($arItem["PROPERTIES2"]["ELEMENTS"] as $el)
    {
        $res = CIBlockElement::GetByID($el);
        if ($ar = $res->GetNext())
        {
            // explode date
            $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($ar["ACTIVE_FROM"], CSite::GetDateFormat()));
            $arTmpDate = explode(" ", $arTmpDate);
            $elem["DISPLAY_ACTIVE_FROM_DAY"] = $arTmpDate[0];
            $elem["DISPLAY_ACTIVE_FROM_MONTH"] = $arTmpDate[1];
            $elem["DISPLAY_ACTIVE_FROM_YEAR"] = $arTmpDate[2];
            $elem["PREVIEW_TEXT"] = htmlspecialchars_decode($ar["PREVIEW_TEXT"]);
            $elem["NAME"] = htmlspecialchars_decode($ar["NAME"]);

            if ($ar["DETAIL_PICTURE"])
            {
                $elem["DETAIL_PICTURE"] = CFile::ResizeImageGet($ar['DETAIL_PICTURE'], array('width'=>480, 'height'=>340), BX_RESIZE_IMAGE_EXACT, false, Array());
                $elem["DETAIL_PICTURE"] = $elem["DETAIL_PICTURE"]["src"];
                //$arResult["ITEMS"][$key]["DETAIL_PICTURE"] = CFile::GetPath($arItem['DETAIL_PICTURE']);
            }
            else
            {
                $elem["DETAIL_PICTURE"] = "/tpl/images/noname/rest_nnm.png";
            }
            // get rest name
        /* $rsSec = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
            if($arSec = $rsSec->GetNext()) {
                $arResult["ITEMS"][$key]["RESTAURANT_NAME"] = htmlspecialchars_decode($arSec["NAME"]);
            }*/

            //$arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);   
            //$arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
            preg_match("/#FID_([0-9]+)#/",$ar["DETAIL_TEXT"],$video1);
            if ($video1[1])
                $elem["VIDEO"] = "http://".SITE_SERVER_NAME."".CFile::GetPath($video1[1]);
            preg_match("/<iframe ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\" ([a-zA-Z0-9&;:?.=\/]+)=\"([a-zA-Z0-9&;:?.=\/]+)\"/",$ar["DETAIL_TEXT"],$video2);
            if ($video2[1])
            {
                for ($i=1;$i<count($video2);$i++)
                {
                    if ($video2[$i]=="width")
                        $width = $video2[++$i];
                    elseif ($video2[$i]=="height")
                        $height = $video2[++$i];
                    elseif($video2[$i]=="src")
                        $src = $video2[++$i];                
                }
            }
            if ($width&&$height&&$src)
            {            
                $height = 470*$height/$width;
                $elem["VIDEO_PLAYER"] = '<iframe src="'.$src.'" width="470" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            }
            $elem["DETAIL_PAGE_URL"] = $ar["DETAIL_PAGE_URL"];
        }
        $elems[]=$elem;
    }
    $arResult["ELEMENTS"] = $elems;
}
?>