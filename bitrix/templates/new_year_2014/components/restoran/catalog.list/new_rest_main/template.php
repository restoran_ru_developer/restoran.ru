<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <div class="new_restoraunt left<?if($key == 2):?> end<?endif?>">
        <?if ($arItem["PREVIEW_PICTURE"]["src"]):?>
        <div class="image">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]?>" width="232" /></a><br />
        </div>
        <?endif;?>
        <div>
            <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=HtmlToTxt($arItem["~NAME"])?></a></div>
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
        </div>
        <div class="left">
            <i><?=intval($arItem["PROPERTIES"]["COMMENTS"])?> <?=pluralForm(intval($arItem["PROPERTIES"]["COMMENTS"]), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_1"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_2"), GetMessage("CT_BNL_ELEMENT_PLURAL_REVIEW_3"))?></i>
        </div>
        <div class="right"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("CT_BNL_ELEMENT_SEE_ALL_REVIEWS")?></a></div>
        <div class="clear"></div>
    </div>
<?endforeach;?>
<div class="clear"></div>
<?if ($arParams["SORT_BY1"]=="PROPERTY_RATIO")
    $aa = "ratio";
elseif($arParams["SORT_BY1"]=="show_counter")
    $aa = "popular";
elseif($arParams["SORT_BY1"]=="PROPERTY_restoran_ratio")
    $aa = "recomended";
else
    $aa = "";
if ($aa):
?>
    <div align="right"><a class="uppercase" href="/<?=CITY_ID?>/ratings/#<?=$aa?>"><?=GetMessage("ALL_NEWS")?></a></div>
<?elseif($arItem["IBLOCK_TYPE_ID"] == "firms_news"):?>
    <div align="right"><a class="uppercase" href="/<?=CITY_ID?>/news/restoratoram/"><?=GetMessage("ALL_NEWS")?></a></div>
<?elseif(substr_count($arResult["SECTION"]["PATH"][0]["CODE"], "newplace")):?>
    <div align="right"><a class="uppercase" href="<?=$arResult["SECTION"]["PATH"][0]["SECTION_PAGE_URL"]?>"><?=GetMessage("ALL_NEWS1")?></a></div>    
<?endif;?>
<?//v_dump($arResult["ITEMS"][0])?>