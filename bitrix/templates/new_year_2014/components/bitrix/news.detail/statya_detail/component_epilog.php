<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
// set title
$arResult["NAME"] = preg_replace("/&lt;br[ \/]+&gt;[0-9A-zА-ярР .]+/","",$arResult["NAME"]);

$APPLICATION->SetTitle('Новый год 2015 в ресторане '.$arResult["NAME"]);
$APPLICATION->SetPageProperty("description", 'Предложения ресторана '.$arResult["NAME"].' в новый год 2015');
$APPLICATION->SetPageProperty("keywords",  'новый, год, 2015, '.$arResult["NAME"]);
?>