<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$p=0?>
<?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
            "TYPE" => "up_728_90",
            "NOINDEX" => "Y",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
    ),
false
);?>
<?foreach($arResult["NEW_ITEMS"] as $cell=>$arI):?>
    <?if ($p==4):?>
        <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "first_728_90",
            		"NOINDEX" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
            );?>
            <br />
    <?endif;?>
    <?if ($p==5):?>
        <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "second_728_90",
            		"NOINDEX" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
            );?>
            <br />
    <?endif;?>
    <div id="tabs_block<?=$cell?>">
        <ul class="tabs">
            <?foreach($arI as $key=>$arItem):
                //plate_scrollable holiday_scrollable news_scrollable
                ?>
            <li>
                <a href="#">
                    <div class="left tab_left"></div>
                    <div class="left name"><?=$arItem["NAME"]?></div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <?endforeach;?>
        </ul>
        <!-- tab "panes" -->
        <div class="panes">
        <?foreach($arI as $key=>$arItem):?>
        <div class="pane" main="main_page">
            <?
            $filter = "arrNoFilter_".$arItem["CODE"];
            if (is_array($arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]))
            {    
                    global $$filter;
                    $$filter = Array("ID"=>$arItem["PROPERTIES"]["ELEMENTS"]["VALUE"]);
            }
            ?>
            <?
            switch($arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"])
            {
                case "news":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "new_rest_main",
                        Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                            "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => ($$filter)?$filter:"",
                            "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "special_projects":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "new_rest_main",
                        Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                            "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => ($$filter)?$filter:"",
                            "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "firms_news":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "new_rest_main",
                        Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                            "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => ($$filter)?$filter:"",
                            "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                            "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "ratings":
                    if ($arItem["CODE"]=='ratings1')
                    {
                        $arIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "new_rest_main",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "PROPERTY_RATIO",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "PROPERTY_COMMENTS",
                                "SORT_ORDER2" => "DESC",
                                "SORT_BY3" => "NAME",
                                "SORT_ORDER3" => "DESC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );
                        echo "<div class='clear'></div>";
                    }
                    elseif ($arItem["CODE"]=='ratings2')
                    {
                        $arIB = getArIblock("catalog", CITY_ID);
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "new_rest_main",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "show_counter",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );
                        echo "<div class='clear'></div>";
                    }
                    if ($arItem["CODE"]=='ratings3')
                    {
                        $arIB = getArIblock("catalog", CITY_ID);
                        global $arrfil;
                        $arrfil["!PROPERTY_restoran_ratio"] = false;
                        $APPLICATION->IncludeComponent(
                            "restoran:catalog.list",
                            "new_rest_main",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "PROPERTY_restoran_ratio",
                                "SORT_ORDER1" => "asc,nulls",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => "arrfil",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );
                        echo "<div class='clear'></div>";
                    }
                break;
                case "videonews":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "videonews_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => "2",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("LIST_PAGE_URL"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );   
                break;
                case "kupons":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "kupons",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("ACTIVE_TO"),
                                "PROPERTY_CODE" => array("RATIO", "COMMENTS","subway"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "100",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "overviews":             
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("RATIO"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "reviews":
                case "comment":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "reviews_main",
                        Array(
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "DATE_CREATE",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DATE_CREATE","CREATED_BY"),
                                "PROPERTY_CODE" => array("COMMENTS","ELEMENT"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "150",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                    );
                break;
                case "cookery":   
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("comments"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "150",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => $arItem["PROPERTIES"]["SECTION"]["VALUE"],
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
            case "photoreports":
            ?>
                <div id="photogalery" class="photogalery">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "photoreports",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                                "PROPERTY_CODE" => array("PHOTOS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                        false
                        );?>
                    </div>
                <?echo "<div class='clear'></div>";
                break;
                case "interview":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("ratio"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                    echo "<div class='clear'></div>";
                break;
                case "afisha":
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "afisha_list_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                                "NEWS_COUNT" => "99",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER2" => "ASC",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("EVENT_TYPE","TIME"),
                                "CHECK_DATES" => "N",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                        false
                        );
                    echo "<div class='clear'></div>";
                break;
                case "blogs":
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "article_main",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => $arItem["PROPERTIES"]["IBLOCK_TYPE"]["VALUE"],
                                "IBLOCK_ID" => $arItem["PROPERTIES"]["IBLOCK_ID"]["VALUE"],
                            "NEWS_COUNT" => ($arItem["PROPERTIES"]["COUNT"]["VALUE"])?$arItem["PROPERTIES"]["COUNT"]["VALUE"]:"3",
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => ($$filter)?$filter:"",
                                "FIELD_CODE" => array("DATE_CREATE","CREATED_BY","DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("COMMENTS"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "120",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "search_rest_list",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                break;
                default:

                break;
            }


            ?>
        </div>
        <?endforeach;?>
        </div>
    </div>
<?$p++;?>
<?endforeach;?>