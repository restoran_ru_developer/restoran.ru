<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/autocomplete/jquery.autocomplete.css";  type="text/css" rel="stylesheet" />', true)?>
    <?$APPLICATION->AddHeadScript('http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js')?>

    <?$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/css/cusel.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/cusel_m.css"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadString('<link href="/tpl/css/styles.css?1"  type="text/css" rel="stylesheet" />',true)?>
    <?$APPLICATION->AddHeadScript('http://vkontakte.ru/js/api/share.js?11')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.mousewheel.js')?>            
    <?$APPLICATION->AddHeadScript('/tpl/js/jScrollPane.js')?>             
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-multiple-min-0.9.js')?>  
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.system_message.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/cusel-min.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/radio.js')?>
    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.autocomplete.min.js')?>
    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon"> 
    <?
        CModule::IncludeModule("advertising");    
        CAdvBanner::SetRequiredKeywords (array(CITY_ID));
    ?>
</head>
    <body style="background:#FFF">
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <h1 style="position: absolute;z-index: -1;"><?=$APPLICATION->ShowTitle("h1")?></h1>
    <div id="container" style="background:#032d50 url(<?=SITE_TEMPLATE_PATH?>/img/new_year_bg_1.jpg) center top no-repeat">            
        <div id="wrapp">
        <div id="header">
            <div class="baner" style="height:90px; background: none; border-radius: 15px;">                
                <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "new_year_top_980_90",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
            </div>
            <a href="/<?=CITY_ID?>/articles/new_year_corp/" class="" style="margin-left: 86px;height: 80px; top: 318px; position:absolute; width: 235px; line-height: 47px; text-align: center; margin-bottom: 0px;"></a>
            <a href="/<?=CITY_ID?>/articles/new_year_corp/" class="appetite light_button" style="margin-left: 86px; margin-top:280px; float: left; width: 235px; line-height: 47px; text-align: center; margin-bottom: 0px;">Организовать банкет</a>
            <a href="/<?=CITY_ID?>/articles/new_year_night/" class="" style="right: 86px; height: 80px; top: 318px; position:absolute; width: 235px; line-height: 47px; text-align: center; margin-bottom: 0px;"></a>
            <a href="/<?=CITY_ID?>/articles/new_year_night/" class="appetite light_button" style="margin-top:280px; margin-right:128px; float: right; width: 180px; line-height: 47px; text-align: center; margin-bottom: 0px;">Выбрать ресторан</a>
            <a id="new_year_logo" href="/"></a>
            <?$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");?>            
        </div>                
        <div id="content">