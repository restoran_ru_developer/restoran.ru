<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["ITEMS"] as $key=>&$arItem) {
    // truncate text
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = TruncateText(strip_tags($arResult["ITEMS"][$key]["PREVIEW_TEXT"]), $arParams["PREVIEW_TRUNCATE_LEN"]);
    $arResult["ITEMS"][$key]["NAME"] = change_quotes($arResult["ITEMS"][$key]["NAME"]);
    $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = change_quotes($arResult["ITEMS"][$key]["PREVIEW_TEXT"]);
    $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 314, 'height' => 200), BX_RESIZE_IMAGE_EXACT, true,Array(),false,75);
    $rsUser = CUser::GetByID($arItem["CREATED_BY"]);
    if($arUser = $rsUser->GetNext())
        $arItem["AUTHOR_NAME"] = $arUser["NAME"];

    // format date
    if (!$arItem["DISPLAY_ACTIVE_FROM"]):
        $date = $DB->FormatDate($arItem["DATE_CREATE"], 'DD-MM-YYYY HH:MI:SS', 'DD.MM.YYYY HH:MI:SS');
        $arTmpDate = CIBlockFormatProperties::DateFormat('j F Y G:i', MakeTimeStamp($date, CSite::GetDateFormat()));
    else:
        $arTmpDate = $arItem["DISPLAY_ACTIVE_FROM"];
    endif;    
    $arTmpDate = explode(" ", $arTmpDate);
    $arItem["CREATED_DATE_FORMATED_1"] = $arTmpDate[0];
    $arItem["CREATED_DATE_FORMATED_2"] = $arTmpDate[1]." ".$arTmpDate[2].", ".$arTmpDate[3];
    
    /*if ($arParams["IBLOCK_TYPE"]=="videonews")
    {
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$arItem["ID"],"PROPERTY_BLOCK_TYPE_VALUE"=>"Видео"),false,false,Array("PROPERTY_VIDEO"));
        if ($ar = $res->GetNext())
            $arItem["VIDEO"] = $ar["PROPERTY_VIDEO_VALUE"];        
    }*/
    $arItem["DETAIL_PAGE_URL"] = str_replace("#CITY_ID#", CITY_ID, $arItem["DETAIL_PAGE_URL"]);
    if ($arItem["PROPERTIES2"]["RESTORAN"][0])
    {
        $r = CIBlockElement::GetByID($arItem["PROPERTIES2"]["RESTORAN"][0]);
        if ($a = $r->GetNext())
            $arItem["REST"] = array("DETAIL_PAGE_URL"=>$a["DETAIL_PAGE_URL"],"IBLOCK_ID"=>$a["IBLOCK_ID"]);        
        
        $db_props = CIBlockElement::GetProperty($arItem["REST"]["IBLOCK_ID"], $arItem["PROPERTIES2"]["RESTORAN"][0], array("sort" => "asc"), Array("CODE"=>"address"));
        if($ar_props = $db_props->Fetch())
            $arItem["adres"]= $ar_props["VALUE"];
        $db_props = CIBlockElement::GetProperty($arItem["REST"]["IBLOCK_ID"], $arItem["PROPERTIES2"]["RESTORAN"][0], array("sort" => "asc"), Array("CODE"=>"subway"));
        if($ar_props = $db_props->Fetch())
        {
            $subway = $ar_props["VALUE"];
            $res = CIBlockElement::GetByID($subway);
            if ($ar = $res->Fetch())
                $arItem["subway"] = $ar["NAME"];
        }
//        $db_props = CIBlockElement::GetProperty($arItem["REST"]["IBLOCK_ID"], $arItem["PROPERTIES2"]["RESTORAN"][0], array("sort" => "asc"), Array("CODE"=>"number_of_rooms"));
//        if($ar_props = $db_props->Fetch())
//            $arItem["rooms"] = $ar_props["VALUE"];
        
        $db_props = CIBlockElement::GetProperty($arItem["REST"]["IBLOCK_ID"], $arItem["PROPERTIES2"]["RESTORAN"][0], array("sort" => "asc"), Array("CODE"=>"banket_average_bill"));
        if($ar_props = $db_props->Fetch())
            $arItem["ch"] = $ar_props["VALUE"];        
        
        $db_props = CIBlockElement::GetProperty($arItem["REST"]["IBLOCK_ID"], $arItem["PROPERTIES2"]["RESTORAN"][0], array("sort" => "asc"), Array("CODE"=>"average_bill"));
        if($ar_props = $db_props->Fetch())
        {
            $r = CIBlockElement::GetByID($ar_props["VALUE"]);
            $a = $r->Fetch();
            $arItem["average_bill"] = $a["NAME"];        
        }
        
        $db_props = CIBlockElement::GetProperty($arItem["REST"]["IBLOCK_ID"], $arItem["PROPERTIES2"]["RESTORAN"][0], array("sort" => "asc"), Array("CODE"=>"opening_hours"));
        if($ar_props = $db_props->Fetch())
            $arItem["opening_hours"] = $ar_props["VALUE"];        
        
        
    }
    if ($key>5)
    {
        unset($arResult["ITEMS"][$key]);        
    }
}
?>