<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                  
<div id="spec_block_list">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
        <div class="spec left<?if($key % 3 == 2):?> end<?endif?>" style="background:url(<?=$arItem["PREVIEW_PICTURE"]["src"]?>) left top no-repeat"> 
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>">
            <div class="dotted_s"></div>        
            <div class="title_box grad<?=($key%3)?>">
                <div class="title">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td><?=$arItem["NAME"]?></td>
                        </tr>
                    </table>
                </div>
            </div>        
            </a>
        </div>
    <?endforeach;?>
    <div class="clear"></div>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>