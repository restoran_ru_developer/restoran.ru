<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>                  
<div id="spec_block_list">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>        
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a>
        <Br />
        <?=$arItem["PREVIEW_TEXT"]?>
    <?endforeach;?>
    <div class="clear"></div>
</div>