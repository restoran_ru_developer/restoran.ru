<?
if (isset($_REQUEST['work_start']))
{
	define("NO_AGENT_STATISTIC", true);
	define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule("iblock");
IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm("Доступ запрещен");

$limit = 50;
$BID = 105;

if($_REQUEST['work_start'] && check_bitrix_sessid())
{
    $deletedCount = intval($_REQUEST['deletedCount']);
    $allCountWillDelete = intval($_REQUEST['allCountWillDelete']);
//    $rsEl = CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => $BID, ">ID" => $_REQUEST["lastid"], '<DATE_CREATE'=>'01.01.2015 00:00:00', "!PROPERTY_status"=>1808), false, array("nTopCount" => $limit));
//    while ($arEl = $rsEl->Fetch())
//    {
//        /*
//         * do something
//         */
//
//
//        $DB->StartTransaction();
//        if(!CIBlockElement::Delete($arEl["ID"]))
//        {
//            FirePHP::getInstance()->info('deleting error with id='.$arEl["ID"]);
//            AddMessage2Log($arEl["ID"],'deleting bs error');
//            $DB->Rollback();
//            die();
//        }
//        else
//            $DB->Commit();
//
//        AddMessage2Log($arEl["ID"],'deleting bs');
//
//        $lastID = intval($arEl["ID"]);
//        $deletedCount++;
//    }

//    $rsLeftBorder = CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => $BID, "<=ID" => $lastID, '<DATE_CREATE'=>'01.01.2015 00:00:00'));
//    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();

    $rsAll = CIBlockElement::GetList(array("ID" => "ASC"), array("IBLOCK_ID" => $BID, '<DATE_CREATE'=>'01.01.2015 00:00:00', "!PROPERTY_status"=>1808));
    $allCnt = $rsAll->SelectedRowsCount();

    FirePHP::getInstance()->info($allCnt,'$allCnt');


    if(!$allCountWillDelete){
        $allCountWillDelete = $allCnt;
    }


    FirePHP::getInstance()->info($deletedCount,'$deletedCount');
    FirePHP::getInstance()->info($allCountWillDelete,'$allCountWillDelete');

    $p = round(100*$deletedCount/$allCountWillDelete, 2);//$leftBorderCnt

    echo 'CurrentStatus = Array('.$p.', "'.($p < 100 && $allCnt!=0 ? '&lastid='.$lastID.'&deletedCount='.$deletedCount.'&allCountWillDelete='.$allCountWillDelete : '').'", "Обрабатываю запись с ID #'.$lastID.'", "Осталось '.$allCnt.' записей");';

    die();
}

$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
						'<tr class="heading">'.
							'<td>Текущее действие</td>'.
							'<td width="1%">&nbsp;</td>'.
						'</tr>'.
					'</table>';

$aTabs = array(array("DIV" => "edit1", "TAB" => "Обработка"));
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle("Обработка элементов инфоблока");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
<script type="text/javascript">

	var bWorkFinished = false;
	var bSubmit;

	function set_start(val)
	{
		document.getElementById('work_start').disabled = val ? 'disabled' : '';
		document.getElementById('work_stop').disabled = val ? '' : 'disabled';
		document.getElementById('progress').style.display = val ? 'block' : 'none';

		if (val)
		{
			ShowWaitWindow();
			document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
			document.getElementById('status').innerHTML = 'Работаю...';

			document.getElementById('percent').innerHTML = '0%';
			document.getElementById('indicator').style.width = '0%';

			CHttpRequest.Action = work_onload;
			CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
		}
		else
			CloseWaitWindow();
	}

	function work_onload(result)
	{
		try
		{
			eval(result);

			iPercent = CurrentStatus[0];
			strNextRequest = CurrentStatus[1];
			strCurrentAction = CurrentStatus[2]+', '+CurrentStatus[3];

			document.getElementById('percent').innerHTML = iPercent + '%';
			document.getElementById('indicator').style.width = iPercent + '%';

			document.getElementById('status').innerHTML = 'Работаю...';

			if (strCurrentAction != 'null')
			{
				oTable = document.getElementById('result_table');
				oRow = oTable.insertRow(-1);
				oCell = oRow.insertCell(-1);
				oCell.innerHTML = strCurrentAction;
				oCell = oRow.insertCell(-1);
				oCell.innerHTML = '';
			}

			if (strNextRequest && document.getElementById('work_start').disabled)
				CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
			else
			{
				set_start(0);
				bWorkFinished = true;
			}

		}
		catch(e)
		{
			CloseWaitWindow();
			document.getElementById('work_start').disabled = '';
			alert('Сбой в получении данных');
		}
	}

</script>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
<?
echo bitrix_sessid_post();

$tabControl->Begin();
$tabControl->BeginNextTab();
?>
	<tr>
		<td colspan="2">

			<input type=button value="Старт" id="work_start" onclick="set_start(1)" />
			<input type=button value="Стоп" disabled id="work_stop" onclick="bSubmit=false;set_start(0)" />
			<div id="progress" style="display:none;" width="100%">
			<br />
				<div id="status"></div>
				<table border="0" cellspacing="0" cellpadding="2" width="100%">
					<tr>
						<td height="10">
							<div style="border:1px solid #B9CBDF">
								<div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
							</div>
						</td>
						<td width=30>&nbsp;<span id="percent">0%</span></td>
					</tr>
				</table>
			</div>
			<div id="result" style="padding-top:10px"></div>

		</td>
	</tr>
<?
$tabControl->End();
?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>