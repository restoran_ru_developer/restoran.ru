<?
$arHTMLPagesOptions = array(
	"~INCLUDE_MASK" => array(
		"0" => "'^/m/.*?\$'",
	),
	"~EXCLUDE_MASK" => array(
		"0" => "'^/bitrix/.*?\$'",
		"1" => "'^/404\\.php\$'",
	),
	"~FILE_QUOTA" => "104857600",
	"COMPRESS" => "1",
	"STORE_PASSWORD" => "Y",
	"COOKIE_LOGIN" => "BITRIX_SM_LOGIN",
	"COOKIE_PASS" => "BITRIX_SM_UIDH",
	"INCLUDE_MASK" => "/m/*",
	"EXCLUDE_MASK" => "/bitrix/*;/404.php",
	"FILE_QUOTA" => "100",
);
?>
