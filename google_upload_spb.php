<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

CModule::IncludeModule("iblock");

$lastEl = false;
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);
$lastElID = ($_REQUEST["lastElID"] ? $_REQUEST["lastElID"] : 0);
//$curIblockID = ($_REQUEST["curIblockID"] ? $_REQUEST["curIblockID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// start execution
$time_start = microtime_float();

    file_put_contents("google_upload_spb.xml", '', LOCK_EX);

    $str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    $str .= "<listings xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://local.google.com/local_feed.xsd\">\n";

        $str .= "\t<language>ru</language>\n";
        $str .= "\t<datum>WGS84</datum>\n";
    file_put_contents("google_upload_spb.xml", $str, FILE_APPEND | LOCK_EX);
/*
$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
		//">ID" => $curIblockID
        "ID" => 11
	),
	false
);
*/

//while($arIblock = $rsIblock->Fetch()) {
	//$curStepID = 0;

	$rsRest = CIBlockElement::GetList(
		Array("ID" => "ASC"),
		Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => 12,
			//">ID" => $curStepID,
			"!SECTION_ID" => 44170,
			"!PROPERTY_google_photo" => false
		),
		false,
		false,
		Array("ID", "NAME", "DETAIL_PAGE_URL", "DATE_CREATE", "PROPERTY_*")
	);
	$ch = 0;
	while($arRest = $rsRest->GetNext()) {
		$curStepID = $arRest["ID"];
		//$curIblockID = $arRest["IBLOCK_ID"];
	
		// get photo
		$photoKey = 0;
		$strPhoto = '';
                $str = "";
		$rsProp = CIBlockElement::GetProperty(
			$arRest["IBLOCK_ID"],
			$arRest["ID"],
			Array(),
			Array(
				"CODE" => "google_photo"
			)
		);
		while($arProp = $rsProp->GetNext()) {
			if(intval($arProp["VALUE"]) > 0) {
				$arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array('width' => 2000, 'height' => 1500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	
				$strPhoto .= "\t\t\t<image type=\"photo\" url=\"http://".SITE_SERVER_NAME.$arPhoto["src"]."\" width=\"".$arPhoto["width"]."\" height=\"".$arPhoto["height"]."\">\n";
                $strPhoto .= "\t\t\t\t<link>http://".SITE_SERVER_NAME.$arRest["DETAIL_PAGE_URL"]."</link>\n";
				$strPhoto .= "\t\t\t\t<title>Restaurant inside</title>\n";
				$strPhoto .= "\t\t\t\t<author>Restoran.ru</author>\n";
				$strPhoto .= "\t\t\t</image>\n";
	
				$photoKey++;
				if($photoKey > 3)
					break;
			}
		}
	
		if($photoKey > 0) {
	
			// tmp date
			$arTmpDate = explode(' ', $arRest["DATE_CREATE"]);
			$tmpDate = explode('.', $arTmpDate[0]);
	
			$str .= "\t<listing>\n";
				$str .= "\t\t<id>".$arRest["ID"]."</id>\n";
                                $temp_name = explode("(",$arRest["NAME"]);
                                $arRest["NAME"] = trim($temp_name[0]);
				$str .= "\t\t<name>".htmlspecialchars($arRest["NAME"])."</name>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "address"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"]) {
						$address = str_replace($arIblock["NAME"].",", "", $arProp["VALUE"]);
						$address = $arIblock["NAME"].", ".$address;
						$str .= "\t\t<address>".$address."</address>\n";
					}	
	
				$str .= "\t\t<country>RU</country>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lat"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<latitude>".$arProp["VALUE"]."</latitude>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lon"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<longitude>".$arProp["VALUE"]."</longitude>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "phone"
					)
				);
				if($arProp = $rsProp->GetNext())
                                if($arProp["VALUE"])
                                    {
                                        $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                        preg_match_all($reg, $arProp["VALUE"], $matches);
                                        $TELs=array();
                                        for($p=1;$p<5;$p++){
                                                foreach($matches[$p] as $key=>$v){
                                                        //if($p==1 && $v!="") $TELs[$key].="+7";
                                                        $TELs[$key].=$v;
                                                }	
                                        }
                                        foreach($TELs as $key => $T){
                                                if(strlen($T)>7) { 
                                                        $TELs[$key] = "+7 (".substr($T, 0,3).") ".substr($T,3,3)."-".substr($T,6,2)."-".substr($T,8,2);                                                        
                                                }
                                                else {unset($TELs[$key]);}
                                        }
//                                            foreach($TELs as $t)
//                                            {
//                                                $str .= "\t\t<phone>\n";
//                                                $str .= "\t\t\t<ext/>\n\t\t\t<type>phone</type>\n\t\t\t<number>".$t."</number>\n\t\t\t<info/>\n";
//                                                $str .= "\t\t</phone>\n";
//                                            }
                                            $str .= "\t\t<phone type=\"main\">".$TELs[0]."</phone>\n";
                                    }
	
				$str .= "\t\t<category>Restaurants</category>\n";
				$str .= "\t\t<date month=\"".$tmpDate[1]."\" day=\"".$tmpDate[0]."\" year=\"".$tmpDate[2]."\"/>\n";
				$str .= "\t\t<content>\n";
	
				$str .= $strPhoto;
	
					$str .= "\t\t\t<attributes>\n";
						$str .= "\t\t\t\t<title>Restaurant Details</title>\n";
						$str .= "\t\t\t\t<author>Restoran.ru</author>\n";
						$str .= "\t\t\t\t<email>redaktor@restoran.ru</email>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "site"
							)
						);
						if($arProp = $rsProp->GetNext())
							if($arProp["VALUE"]) {
                                if(strstr($arProp["VALUE"], ','))
                                    $tmpWebsite = explode(",", $arProp["VALUE"]);
                                elseif(strstr($arProp["VALUE"], ';'))
                                    $tmpWebsite = explode(";", $arProp["VALUE"]);
                                else
                                    $tmpWebsite[0] = $arProp["VALUE"];

                                $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);

                                $str .= "\t\t\t\t<website>"."http://".$tmpWebsite[0]."</website>\n";
								//$str .= "\t\t\t\t<website>".$arProp["VALUE"]."</website>\n";
                            }

						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "opening_hours_google"
							)
						);
						if($arProp = $rsProp->GetNext())
							if($arProp["VALUE"])
								$str .= "\t\t\t\t<attr name=\"Hours\">".$arProp["VALUE"]."</attr>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "credit_cards"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->GetNext())
								$str .= "\t\t\t\t<attr name=\"Payment accepted\">".$arPayment["NAME"]."</attr>\n";
						}
	
						//$str .= "\t\t\t\t<attr name=\"Attire\">Casual</attr>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kolichestvochelovek"
							)
						);
						if($arProp = $rsProp->GetNext())
							$rsSeating = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arSeating = $rsSeating->GetNext())
								$str .= "\t\t\t\t<attr name=\"Seating\">".str_replace("&nbsp;", "", $arSeating["NAME"])."</attr>\n";
	
						//$str .= "\t\t\t\t<attr name=\"Phone Orders\">No</attr>\n";
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "average_bill"
							)
						);
						if($arProp = $rsProp->GetNext())
                                                {
							if($arProp["VALUE"]) {
                                                            $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                            if($arPayment = $rsPayment->GetNext())
                                                            {
                                                                if ($arPayment["NAME"]=="до 1000р")
                                                                    $arPayment["NAME"] = "Inexpensive";
                                                                if ($arPayment["NAME"]=="1000-1500р.")
                                                                    $arPayment["NAME"] = "Inexpensive - Moderate";
                                                                if ($arPayment["NAME"]=="1500-2000р")
                                                                    $arPayment["NAME"] = "Moderate";
                                                                if ($arPayment["NAME"]=="2000-3000р")
                                                                    $arPayment["NAME"] = "Moderate - Expensive";
                                                                if ($arPayment["NAME"]=="от 3000р")
                                                                    $arPayment["NAME"] = "Expensive";
								$str .= "\t\t\t\t<attr name=\"Price Level\">".$arPayment["NAME"]."</attr>\n";
                                                            }
                                                        }
                                                }
						//$str .= "\t\t\t\t<attr name=\"Phone Orders\">No</attr>\n";
                                                $str .= "\t\t\t\t<attr name=\"Reservations\">http://".SITE_SERVER_NAME.$arRest["DETAIL_PAGE_URL"]."?bron=Y</attr>\n";
                                                $str .= "\t\t\t\t<attr name=\"Menu\">http://".SITE_SERVER_NAME.$arRest["DETAIL_PAGE_URL"]."menu/</attr>\n";  
						$str .= "\t\t\t\t<date month=\"".$tmpDate[1]."\" day=\"".$tmpDate[0]."\" year=\"".$tmpDate[2]."\"/>\n";
					$str .= "\t\t\t</attributes>\n";
	
				$str .= "\t\t</content>\n";
			$str .= "\t</listing>\n";
	
			//v_dump($arRest["IBLOCK_ID"]);
			//v_dump($arRest["ID"]);
	
		}
                file_put_contents("google_upload_spb.xml", $str, FILE_APPEND | LOCK_EX);
                $ch++;		
	
	}
//}
v_dump($ch);
$str = "</listings>";        
file_put_contents("google_upload_spb.xml", $str, FILE_APPEND | LOCK_EX);
?>
