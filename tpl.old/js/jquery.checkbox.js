jQuery(document).ready(function(){
    jQuery("body").on('click',".niceCheck", function() {
        changeCheck(jQuery(this));
    });
    
    jQuery("body").on('click','.niceCheckLabel', function() {
        changeCheck(jQuery(this).parent().parent().find(".niceCheck"));
    });
    jQuery(".niceCheck").each(
    /* при загрузке страницы нужно проверить какое значение имеет чекбокс и в соответствии с ним выставить вид */
    function() {

        changeCheckStart(jQuery(this));

    });

});

function changeCheck(el)
/* 
	функция смены вида и значения чекбокса
	el - span контейнер дял обычного чекбокса
	input - чекбокс
*/
{
     var el = el,
          input = el.find("input").eq(0);
   	 if(!input.attr("checked")) {
		el.css("background-position","0 -19px");	
		input.attr("checked", true);
                if(input.attr("id")=="agreement")
                {
                    $("#register_submit_button").removeClass("disabled");
                    $("#register_submit_button").attr("disabled",false);
                }
	} else {
		el.css("background-position","0 0");	
		input.attr("checked", false);
                if(input.attr("id")=="agreement")
                {
                    $("#register_submit_button").addClass("disabled");
                    $("#register_submit_button").attr("disabled",true);
                }
	}
     return true;
}

function setCheck(el)
{
    var el = el,
    input = el.find("input").eq(0);
    el.css("background-position","0 -19px");	
    input.attr("checked", true);
    
    return true;
}

function unsetCheck(el)
{
    var el = el,
    input = el.find("input").eq(0);
    el.css("background-position","0 0");	
    input.attr("checked", false);
    return true;
}


function changeCheckStart(el)
/* 
	если установлен атрибут checked, меняем вид чекбокса
*/
{
var el = el,
		input = el.find("input").eq(0);
      if(input.attr("checked")) {
		el.css("background-position","0 -19px");	
		}
     return true;
}