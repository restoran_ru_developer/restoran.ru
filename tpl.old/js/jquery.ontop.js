jQuery.fn.ontop = function(options){
    // настройки по умолчанию
    var options = jQuery.extend({    
        text:"наверх"
    },options);    
     
    this.init = function() {
        $(this).append("<div id='ontop'><div class='ontop_arrow'>"+options.text+"</div></div>");
        var _this = $("#ontop");
        var a = $(window).height();
        $(window).scroll(function(){
            if ($(window).scrollTop()>$(window).height()+300)            
                _this.fadeIn(300);            
            else
               _this.fadeOut(300);            
        });
        _this.click(function(){
             $('html,body').animate({scrollTop:"0"}, 500);
        });
        return _this;
    };
    return this.init();
}
$(document).ready(function(){
    if ($(window).width()>1024)
    {
        $("#container").ontop();
    }
});