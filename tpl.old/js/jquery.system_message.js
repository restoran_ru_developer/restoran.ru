jQuery.fn.system_message = function(options){
    this.init = function() {
        $(this).append('<div id="system_message"></div>');
        var _this = $("#system_message");        
        $("#system_message").on("click","#sys_mes_close",function(){
             $("#system_message").fadeOut(300);
        });         
        return _this;
    };
    return this.init();
}
$(document).ready(function(){
    $("body").system_message();
});