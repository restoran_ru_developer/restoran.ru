<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if ($_REQUEST["ID"])
{
    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    $ar_res = $res->GetNext();    
    $db_props = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"], $ar_res["ID"], array("sort" => "asc"), Array("CODE"=>"rating_date"));
    if($ar_props = $db_props->Fetch())
    {
        $old_value = 0;
        if ($ar_props["VALUE"]!=date("d.m.Y"))
        {
            CIBlockElement::SetPropertyValueCode($ar_res["ID"], "rating_date", Array("VALUE"=>date("d.m.Y"),"DESCRIPTION"=>($ar_res["SHOW_COUNTER"]-1)));
            $old_value = $ar_res["SHOW_COUNTER"]-1;
        }
        else
            $old_value = $ar_props["DESCRIPTION"];
        CIBlockElement::SetPropertyValueCode($ar_res["ID"], "stat_day", Array("VALUE"=>($ar_res["SHOW_COUNTER"]-$old_value)));
    }
    
    if (is_file($_SERVER["DOCUMENT_ROOT"].'/tpl/images/counter.png'))
    {
        $img = ImageCreateFromPNG($_SERVER["DOCUMENT_ROOT"].'/tpl/images/counter.png');
    

        // Назначаем черный цвет
        $blue = ImagecolorAllocate($img,36,166,207);
        $black = ImagecolorAllocate($img,59,65,78);
        // Выводим счет на изображение
        $str1 = 125 - strlen(($ar_res["SHOW_COUNTER"]-$old_value))*7;
        $str2 = 125 - strlen($ar_res["SHOW_COUNTER"])*7;
        
        Imagestring($img,3,$str1,31,$ar_res["SHOW_COUNTER"]-$old_value,$blue);
        Imagestring($img,3,$str2,42,$ar_res["SHOW_COUNTER"],$black);
        
        
        
        $text = $ar_res["SHOW_COUNTER"];
        $font = $_SERVER["DOCUMENT_ROOT"].'/tpl/images/arialbd.ttf';
        //imagettftext($img, 8, 0, 92, 41, $blue, $font, "1356");
        //imagettftext($img, 8, 0, 92, 52, $black, $font, "123678");

        // Выводим изображение в стандартный поток вывода
        Header("Content-type: image/png");
        ImagePng($img);
    }
}
?>
