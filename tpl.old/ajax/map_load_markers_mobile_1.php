<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (check_bitrix_sessid()):
    if (CModule::IncludeModule("iblock")):
        $arRestIB = getArIblock("catalog", CITY_ID);
        if ($_REQUEST["arrFilter_pf"])
        {
            foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
            {
                //if ($key=="type")
                    //$arrFilter["?PROPERTY_".$key] = implode(" || ",$ar);
                //else
                    $arrFilter["PROPERTY_".$key] = $ar;
            }
        }
        $arFilter = Array("ACTIVE"=>"Y","IBLOCK_ID"=>$arRestIB["ID"],"!PROPERTY_lat"=>false,"!PROPERTY_lon" => false,"!PROPERTY_sleeping_rest_VALUE"=>"Да");
        if (is_array($arrFilter))
        {
            $arFilter = array_merge($arFilter,$arrFilter);
        }
        $res = CIBlockElement::GetList(Array(),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","DETAIL_PAGE_URL","CODE","PROPERTY_lat","PROPERTY_lon"));
        //$res->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
        while($ar = $res->GetNext())
        {
            $lat = array();
            $lon = array();
            $adr = array();
            $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lat"));
            while($ar_props = $db_props->Fetch())
                $lat[] = $ar_props["VALUE"];
            $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"lon"));
            while($ar_props = $db_props->Fetch())
                $lon[] = $ar_props["VALUE"];
            $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
            while($ar_props = $db_props->Fetch())
                $adr[] = $ar_props["VALUE"];
            
            foreach($lat as $key=>$l)
            {
//                if ($lat[$key]>=(double)$_REQUEST["xl"]&&$lat[$key]<=(double)$_REQUEST["xr"]&&$lon[$key]>=(double)$_REQUEST["yl"]&&$lon[$key]<=(double)$_REQUEST["yr"])
//                {
                    $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$lat[$key],"lon"=>$lon[$key],"adres"=>stripslashes($adr[$key]),"url"=> "/detail.php?RESTOURANT=".$ar["CODE"]);
//                }
            }
        }
        if (count($arResult)>0)
        {   
            echo json_encode($arResult);
        }
    endif;
endif;
?>