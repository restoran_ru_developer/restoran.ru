<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$_REQUEST["ELEMENT"] = intval($_REQUEST["ELEMENT"]);
?>
<?if (check_bitrix_sessid()):?> 
    <?if ($_REQUEST["AJAX"]!="Y"):?>
        <script src="/tpl/js/jquery.form.js"></script>
        <script src="/tpl/js/jQuery.fileinput.js"></script>
        <script>
            $(document).ready(function(){
                $('#csv_file').customFileInput();
                $('#csv_form').ajaxForm({
                    success: function(data) {
                        var obj = jQuery.parseJSON(data);

                        if(obj.TEXT.length > 0)
                            $("#ajax_csv").html(obj.TEXT);
                        if (obj.STEP) {
                            setTimeout("next_step("+obj.STEP+",'"+obj.FILE+"', 0, "+obj.IBLOCK+", 0, 0, 0)" ,1000);
                        }
                    }
                });
            });
            function next_step(step, file, seek, iblock, sectionID, section_sort, element_sort)
            {
                if (!seek)
                    seek=0;
                $.ajax({
                    url: '/tpl/ajax/csv.php?<?=bitrix_sessid_get()?>',
                    data: 'AJAX=Y&step='+step+'&file='+file+'&seek='+seek+'&iblock='+iblock+'&section='+sectionID+'&section_sort='+section_sort+'&element_sort='+element_sort,
                    success: function(data) {
                        var obj = jQuery.parseJSON(data);
                        if(obj.END == 'end') {
                            $("#ajax_csv").html(obj.TEXT);
                            //setTimeout("window.location.reload()", 2000);
                        }
                        if(obj.STEP && obj.END != 'end') {
                            $("#ajax_csv").html(obj.TEXT);
                            console.debug(obj);
                            setTimeout("next_step("+obj.STEP+",'"+obj.FILE+"','"+obj.SEEK+"', "+obj.IBLOCK+", "+obj.SECTION+", "+obj.SECTION_SORT+", "+obj.ELEMENT_SORT+")", 1000);
                        }
                    }
                });
            }
        </script>
        <div align="right">
            <a class="modal_close uppercase white" href="javascript:void(0)"></a>
        </div>
        <form action="/tpl/ajax/csv.php" id="csv_form" method="POST" enctype="multipart/form-data" >
            <?=bitrix_sessid_post()?>
            <p align="center"><i>Выберите файл и нажмите кнопку "Загрузить"</i></p>
            <br /><Br />
            <input type="file" id="csv_file" name="csv" />
            <input type="checkbox" value="1" name="delete_old" id="delete_old" />
            <label for="delete_old">Удалить старое меню</label>
            <input type="hidden" value="Y" name="AJAX" />
            <input type="hidden" value="1" name="step" />
            <input type="hidden" value="<?=$_REQUEST["ELEMENT"]?>" name="ELEMENT" />
            <input type="hidden" value="<?=$_REQUEST["IBLOCK"]?>" name="IBLOCK" />
            <br /><br />
            <div id="ajax_csv">
                
            </div>
            <br /><br />
            <div align=center><input type="submit" class="light_button" value="Загрузить" /></div>
        </form>
    <?else:?>
        <?
        if ($_REQUEST["step"] == 1) {
            // check file ext
            $rsFile = CFile::CheckFile($_FILES['csv'], 0, false, 'csv');
            if(strlen($rsFile) > 0) $result["TEXT"] = "<span style='color: red;'>Не верное расширение файла</span>";

            if(isset($_FILES['csv']) && strlen($rsFile) <= 0/*&&(int)$_REQUEST["ELEMENT"]*/) {

                $folder = $_SERVER["DOCUMENT_ROOT"].'/upload/file_upload_csv/';
                $uploadedFile = $folder.$_REQUEST["ELEMENT"].basename($_FILES['csv']['name']);
                if(is_uploaded_file($_FILES['csv']['tmp_name']))
                {
                    if(move_uploaded_file($_FILES['csv']['tmp_name'], $uploadedFile))
                    {
                        $result["TEXT"] = "<span style='color: green;'>Файл загружен</span>";
                        $result["FILE"] = $folder.$_REQUEST["ELEMENT"].$_FILES['csv']['name'];
                        $result["STEP"] = 2;

                        CModule::IncludeModule("iblock");
                        CModule::IncludeModule("catalog");
                        $_REQUEST["ELEMENT"] = $DB->ForSql($_REQUEST["ELEMENT"]);
                        $strSql = "SELECT * FROM b_iblock WHERE DESCRIPTION = ".$_REQUEST["ELEMENT"];
                        $res = $DB->Query($strSql, false, $err_mess.__LINE__);
                        if($ar_res = $res->Fetch()) {
                            // remove old sections and elements
                            if($_REQUEST["delete_old"]) {
                                $rsSecDel = CIBlockSection::GetList(
                                    Array("SORT"=>"ASC"),
                                    Array(
                                        "IBLOCK_ID" => $ar_res["ID"],
                                    ),
                                    false
                                );
                                while($arSecDel = $rsSecDel->Fetch()) {
                                    $DB->StartTransaction();
                                    if(!CIBlockSection::Delete($arSecDel["ID"]))
                                        $DB->Rollback();
                                    else
                                        $DB->Commit();
                                }
                            }
                            $result["IBLOCK"] = $ar_res["ID"];
                            $result["ELEMENT"] = (int)$_REQUEST["ELEMENT"];
                        } else {
                            // get rest name
                            $arRestIB = getArIblock("catalog", CITY_ID);
                            $rsRest = CIBlockElement::GetList(
                                Array("SORT"=>"ASC"),
                                Array(
                                    "IBLOCK_ID" => $arRestIB["ID"],
                                    "ID" => $_REQUEST["ELEMENT"],
                                ),
                                false,
                                false,
                                Array("ID", "NAME")
                            );
                            $arRest = $rsRest->GetNext();
                            $NAME = $arRest["NAME"];

                            $ib = new CIBlock;
                            $arFields = Array(
                                "ACTIVE" => "Y",
                                "NAME" => $NAME,
                                "CODE" => '',
                                "LIST_PAGE_URL" => '',
                                "DETAIL_PAGE_URL" => '',
                                "IBLOCK_TYPE_ID" => 'rest_menu_ru',
                                "SITE_ID" => Array(SITE_ID),
                                "SORT" => 500,
                                "DESCRIPTION" => $arRest["ID"],
                                "DESCRIPTION_TYPE" => 'text',
                                "LIST_MODE" => 'C',
                                "GROUP_ID" => Array("2"=>"R", "14"=>"W", "15"=>"W")
                            );
                            if(!$ID = $ib->Add($arFields)) {
                                $result["TEXT"] = $ib->LAST_ERROR;
                                $result["ERROR"] = 1;
                            } else {
                                $result["IBLOCK"] = $ID;
                                $result["ELEMENT"] = (int)$_REQUEST["ELEMENT"];
                                if($ID)
                                    CCatalog::Add(
                                        Array(
                                            "IBLOCK_ID" => $ID,
                                            "YANDEX_EXPORT" => "N",
                                            "SUBSCRIPTION" => "N",
                                        )
                                    );
                            }
                        }
                    }
                    else {
                        $result["TEXT"] = "<span style='color: red;'>Во  время загрузки файла произошла ошибка</span>";
                        $result["ERROR"] = 1;
                    }
                }
                else {
                    $result["TEXT"] = "<span style='color: red;'>Файл не загружен</span>";
                    $result["ERROR"] = 1;
                }
            }
            echo json_encode($result);
        }
        if ($_REQUEST["step"] > 1) {
            $result = Array();
            $bs = new CIBlockSection;
            $fp = fopen($_REQUEST["file"],"r");
            if ($_REQUEST["seek"])
                fseek($fp, (int)$_REQUEST["seek"]);
            // get parent section
            $iblock = $_REQUEST["iblock"];
            $parentSection = $_REQUEST["section"];

            if(fgets($fp, 4096) === false) {
                $result["END"] = 'end';
                $result["TEXT"] = "<span style='color: green;'>Импорт завершен</span>";
                echo json_encode($result);
                return;
            } else {
                fseek($fp, (int)$_REQUEST["seek"]);
            }

            $secSort = intval($_REQUEST["section_sort"]);
            $elSort = intval($_REQUEST["element_sort"]);

            while (($buffer = fgets($fp, 4096)) !== false) {
                $arFields = Array();

                $ar = explode("|", $buffer);
                foreach($ar as $arKey=>$arVal)
                    $ar[$arKey] = str_replace(Array("<br>", "<br/>", "<br />"), "", iconv("windows-1251", "utf-8", $ar[$arKey]));

                // if section, add
                if(strlen(trim(($ar[2]))) <= 0) {
                    // check section exist
                    $rsCheckSec = CIBlockSection::GetList(
                        Array("SORT"=>"ASC"),
                        Array(
                            "IBLOCK_ID" => $iblock,
                            //"IBLOCK_ID" => 151,
                            "SECTION_ID" => $parentSection,
                            "NAME" => trim($ar[0])
                        ),
                        false
                    );
                    if(!$arCheckSec = $rsCheckSec->GetNext()) {
                        // add to sec sort
                        $secSort = $secSort + 10;

                        $arFields = Array();
                        $arFields = Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $iblock,
                            //"IBLOCK_ID" => 151,
                            "IBLOCK_SECTION_ID" => $parentSection,
                            "NAME" => trim($ar[0]),
                            "SORT" => $secSort
                        );
                        $ID = $bs->Add($arFields);
                        $parentSection = $ID;
                        $result["SECTION"] = $ID;
                    } else {
                        $parentSection = $arCheckSec["ID"];
                        $result["SECTION"] = $ID;
                    }

                    $result["SECTION_SORT"] = $secSort;
                }

                // add description to parent section
                if(trim($ar[2]) == '#') {
                    if($parentSection) {
                        $arFields = Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $iblock,
                            "DESCRIPTION" => trim($ar[0])
                        );
                        $bs->Update($parentSection, $arFields);
                    }
                }

                // add element
                if(trim($ar[2]) != '/' && trim($ar[2]) != '#' && trim($ar[0]) && trim($ar[2]) && CModule::IncludeModule("catalog")) {
                    $el = new CIBlockElement;
                    // check element exist
                    /*$rsCheckEl = CIBlockElement::GetList(
                        Array("SORT"=>"ASC"),
                        Array(
                            "IBLOCK_ID" => $iblock,
                            "SECTION_ID" => $parentSection,
                            "NAME" => trim($ar[0])
                        ),
                        false,
                        false,
                        Array()
                    );
                    if(!$arCheckEl = $rsCheckEl->GetNext()) {*/
                        // add to el sort
                        $elSort = $elSort + 10;

                        $arLoadElementArray = Array();
                        $arLoadElementArray = Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $iblock,
                            "IBLOCK_SECTION_ID" => $parentSection,
                            "NAME" => trim($ar[0]),
                            "PREVIEW_TEXT" => trim($ar[1]),
                            "PREVIEW_TEXT_TYPE" => "html",
                            "SORT" => $elSort
                        );
                        $PRODUCT_ID = $el->Add($arLoadElementArray);
                        $result["ELEMENT_ID"] = $PRODUCT_ID;

                        // add to catalog
                        $arCatalogFields = Array(
                            "ID" => $PRODUCT_ID,
                            "VAT_ID" => 1,
                            "VAT_INCLUDED" => "N",
                        );
                        $resCatalog = CCatalogProduct::Add($arCatalogFields);
                        //$result["CATALOG"] = $resCatalog;
                    /*} else {
                        // update element
                        $PRODUCT_ID = $arCheckEl["ID"];
                        $arLoadElementArray = Array();
                        $arLoadElementArray = Array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $iblock,
                            "PREVIEW_TEXT" => trim($ar[1]),
                            "PREVIEW_TEXT_TYPE" => "html",
                        );                        );

                        $el->Update($PRODUCT_ID, $arLoadElementArray);
                        $result["ELEMENT_ID"] = $PRODUCT_ID;
                    }*/
                    $result["ELEMENT_SORT"] = $elSort;
                    // set base price
                    if (CITY_ID=="rga"||CITY_ID=="urm")
                    {
                        $ar[2] = str_replace(",",".",$ar[2]);
                        $resPrice = CPrice::SetBasePrice($PRODUCT_ID, floatval($ar[2]), "EUR");
                    }
                    else
                        $resPrice = CPrice::SetBasePrice($PRODUCT_ID, intval($ar[2]), "RUB");
                    //$result["PRICE"] = $resPrice;
                }


                // if close set get parent section ID
                if(trim(($ar[0])) == '/') {
                    $rsParSec = CIBlockSection::GetByID($parentSection);
                    if($arParSec = $rsParSec->GetNext())
                        $result["SECTION"] = $arParSec["IBLOCK_SECTION_ID"];
                }

                $result["IBLOCK"] = $iblock;
                $result["SECTION_SORT"] = $secSort;
                $result["ELEMENT_SORT"] = $elSort;

                if ($ar[0] == "/") {
                    $result["FILE"] = $_REQUEST["file"];
                    $result["STEP"] = $_REQUEST["step"] + 1;
                    $result["SEEK"] = ftell($fp);
                    $result["TEXT"] = "<span style='color: green;'>Импорт в процессе</span>";
                    echo json_encode($result);
                    break;
                }
            }

            fclose($fp);
        }
        ?>
    <?endif;?>
<?endif; ?>
