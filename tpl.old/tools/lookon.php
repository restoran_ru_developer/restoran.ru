<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
$i=0;
$array = array();
$all = simplexml_load_file($_SERVER["DOCUMENT_ROOT"]."/lookoncity.xml");
foreach ($all as $simple_node)  
{
    //v_dump($simple_node);
    $id = (array)$simple_node->attributes()->id;
    $id = $id[0];
    $add = (array)$simple_node->adress;
    $add = $add[0];
    $name = $simple_node->name;
    $temp = explode("(",$name);
    $name = $DB->ForSql(trim($temp[0]));
    $name2 = str_replace(")","",$temp[1]);
    $name2 =$DB->ForSql($name2);
    
//    $preview = array();
//    $tur = array();
//    $widget = array();
//    $widgetPath = array();
//    $title = array();
    
    foreach ($simple_node->tours->tour as $tour)
    {
        $tour_id = (array)$tour->attributes()->id;
        $tour_id = $tour_id[0];
        $preview_temp = (array)$tour->preview->attributes()->url;
        $preview = $preview_temp[0];
        $tur_temp = (array)$tour->links->attributes()->tour;
        $tur = $tur_temp[0];
        $widget_temp = (array)$tour->links->attributes()->widget;
        $widget = $widget_temp[0];
        $widgetPath_temp = (array)$tour->links->attributes()->widgetPath;
        $widgetPath = $widgetPath_temp[0];
        $title_temp = (array)$tour->title;
        $title = $title_temp[0];
        $array[] = Array("id"=>$id,"tour_id"=>$tour_id,"name"=>$name, "add"=>$add,"preview"=>$preview,"tour"=>$tur,"widget"=>$widget,"widgetPath"=>$widgetPath,"title"=>$title);        
    }      
}
//v_dump($array);
$el = new CIBlockElement;
foreach ($array as $ar)
{
    $PROP = array();
    $PROP[2563] = $ar["tour"];  
    $PROP[2564] = $ar["widget"];
    $PROP[2565] = $ar["widgetPath"];
    $PROP[2566] = $ar["id"];
    $PROP[2569] = $ar["tour_id"];

    // tour 2563 									
    // widget 2564									
    // widgetPath 2565									
    // old_id 2566
    
    $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID"      => 2644,
        "PROPERTY_VALUES"=> $PROP,
        "NAME"           => $ar["name"]." (".$ar["add"].") [".$ar['title']."]",
        "ACTIVE"         => "Y",
        "DETAIL_PICTURE" => CFile::MakeFileArray($ar["preview"])
    );
    //v_dump($arLoadProductArray);
    if($PRODUCT_ID = $el->Add($arLoadProductArray,false,false,true))
        echo "New ID: ".$PRODUCT_ID;
    else
        echo "Error: ".$el->LAST_ERROR;
    //exit;
}
?>
