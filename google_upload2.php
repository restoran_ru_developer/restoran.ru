<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// start execution
$time_start = microtime_float();

// get rest list
$rsRestList = CIBlockElement::GetList(
    Array("ID"=>"DESC"),
    Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => "11",
        "IBLOCK_SECTION_ID" => 32,
        ">ID" => $curStepID
    ),
    false,
    Array("nPageSize" => 300),// false,
    Array("ID", "NAME", "IBLOCK_ID", "IBLOCK_CODE", "CODE", "DETAIL_PAGE_URL", "PROPERTY_address", "PROPERTY_phone", "PROPERTY_site", "PREVIEW_PICTURE")
);
while($arRestList = $rsRestList->GetNext()) {
    $curStepID = $arRestList["ID"];

    $arRestList["PROPERTY_ADDRESS_VALUE"][0] = str_replace("Москва, ", "", $arRestList["PROPERTY_ADDRESS_VALUE"][0]);
    $arRestList["PROPERTY_ADDRESS_VALUE"][0] = str_replace(Array(",", ";", ":"), "", $arRestList["PROPERTY_ADDRESS_VALUE"][0]);

    // name
    $str = "\n".trim(htmlspecialchars_decode(stripcslashes($arRestList["NAME"]))).",";
    // address
    $str .= ($arRestList["PROPERTY_ADDRESS_VALUE"][0] ? $arRestList["PROPERTY_ADDRESS_VALUE"][0] : "-").",";
    // city
    $arIblock = getArIblock("catalog", $arRestList["IBLOCK_CODE"]);
    $str .= $arIblock["NAME"].",";
    // country
    $str .= "Россия,";
    // phone
    $str .= ($arRestList["PROPERTY_PHONE_VALUE"][0] ? trim(htmlspecialchars_decode(stripcslashes($arRestList["PROPERTY_PHONE_VALUE"][0]))) : "-").",";
    // website
    $str .= ($arRestList["PROPERTY_SITE_VALUE"] ? trim(htmlspecialchars_decode(stripcslashes($arRestList["PROPERTY_SITE_VALUE"]))) : "-").",";
    // category
    $str .= "ресторан,";
    // image_category
    $str .= "-,";
    // image_url
    $str .= ($arRestList["PREVIEW_PICTURE"] ? "http://".SITE_SERVER_NAME.CFile::GetPath($arRestList["PREVIEW_PICTURE"]) : "-");

    file_put_contents("google_upload2.csv", $str, FILE_APPEND | LOCK_EX);

    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;
    if($time > 30) {
        echo "<script>setTimeout(function() {location.href = '/google_upload2.php?curStepID=".$curStepID."'}, 5000);</script>";
        break;
    }
}
?>