<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${msg:chat.window.title.user}</title>
<link rel="shortcut icon" href="${webimroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />
<style type="text/css">
#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
</style>
</head>
<body class="m" style="margin:0px">
    <form name="mailThreadForm" method="post" action="${webimroot}/mail.php"><input type="hidden" name="style" value="${styleid}"/>
        <input type="hidden" name="thread" value="${page:ct.chatThreadId}"/><input type="hidden" name="token" value="${page:ct.token}"/><input type="hidden" name="level" value="${page:level}"/>
    <div id="main">
        <div class="left logo" ${if:user}style="margin-left:15px;"${endif:user}>
            ${if:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                        </a>
                    ${else:webimHost}
                    <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                    ${endif:webimHost}
                ${else:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                    </a>
                        ${else:webimHost}
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                        ${endif:webimHost}
            ${endif:ct.company.chatLogoURL}
        </div>
        <div class="right" align="right">
            <a class="uppercase" href="javascript:window.close();" title="${msg:leavemessage.close}">${msg:leavemessage.close}</a><Br />
        </div>
        <div class="clear"></div>
        <div class="question">${msg:mailthread.title}</div>
        <div class="question">${msg:mailthread.enter_email} <input type="text" name="email" size="20" value="${form:email}" class="username" />
        <a href="javascript:document.mailThreadForm.submit();"
                                    class="but" id="sndmessagelnk"><input class="light_button" type="button" value="${msg:mailthread.perform}" /></a>
        </div>
        
        
    </div>
        </form>
</body>
</html>
