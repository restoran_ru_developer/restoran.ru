<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${msg:chat.window.title.user}</title>
<link rel="shortcut icon" href="${webimroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />
</head>
<body class="m" style="margin:0px">
    <div id="main">
        <div class="left logo" ${if:user}style="margin-left:15px;"${endif:user}>
            ${if:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                        </a>
                    ${else:webimHost}
                    <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                    ${endif:webimHost}
                ${else:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                    </a>
                        ${else:webimHost}
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                        ${endif:webimHost}
            ${endif:ct.company.chatLogoURL}
        </div>
        <div class="right" align="right">
            <a class="uppercase" href="javascript:window.close();" title="${msg:leavemessage.close}">${msg:leavemessage.close}</a><Br />
        </div>
        <div class="clear"></div>
        <div class="question">${msg:chat.mailthread.sent.content,email}</div>
    </div>
</body>
</html>