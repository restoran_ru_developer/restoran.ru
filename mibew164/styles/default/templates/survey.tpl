<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${msg:presurvey.title}</title>
<link rel="shortcut icon" href="${webimroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />
<style type="text/css">
/*#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}
.form td{
	background-color:#f4f4f4;
	color:#525252;
}
.but{
	font-family:Verdana !important;
	font-size:11px;
	font-weight: bold;
	background:url(${tplroot}/images/wmchat.png) top left no-repeat;
	background-position:0px -25px;
	display:block;
	text-align:center;
	padding-top:2px;
	color:white;
	width:186px;
	height:18px;
	text-decoration:none;
}*/
</style>

</head>
<body style="margin:0px;" class="m">
<form name="surveyForm" method="post" action="${webimroot}/client.php">
    <input type="hidden" name="style" value="${styleid}"/>
    <input type="hidden" name="info" value="${form:info}"/>
    <input type="hidden" name="referrer" value="${page:referrer}"/>
    <input type="hidden" name="survey" value="on"/>
    ${ifnot:showemail}<input type="hidden" name="email" value="${form:email}"/>${endif:showemail}
    ${ifnot:groups}${if:formgroupid}<input type="hidden" name="group" value="${form:groupid}"/>${endif:formgroupid}${endif:groups}
    ${ifnot:showmessage}<input type="hidden" name="message" value="${form:message}"/>${endif:showmessage}
    <div id="main">
        <div class="left logo">
            ${if:ct.company.chatLogoURL}
                                    ${if:webimHost}
                                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                                            <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                                        </a>
                                    ${else:webimHost}
                                    <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                                    ${endif:webimHost}
                                ${else:ct.company.chatLogoURL}
                                    ${if:webimHost}
                                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                                    </a>
                                        ${else:webimHost}
                                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                                        ${endif:webimHost}
                            ${endif:ct.company.chatLogoURL}
        </div>
        <div class="right">
            <a class="uppercase" href="javascript:window.close();" title="${msg:leavemessage.close}">${msg:leavemessage.close}</a>
        </div>
        <div class="clear"></div>
        <br />
        <p class="font14">${msg:presurvey.intro}</p>
        <br />
        ${if:errors}
            <p class="font12 another">${errors}</p>
        ${endif:errors}
        ${if:groups}
            <div class="question">${msg:presurvey.department}</div>
            <select name="group" style="min-width:200px;">${page:groups}</select>
        ${endif:groups}
        <div class="question">${msg:presurvey.name}</div>
        <input type="text" name="name" size="50" value="${form:name}" class="username" ${ifnot:showname}disabled="disabled"${endif:showname} />
        ${if:showemail}
            div class="question">${msg:presurvey.mail}</div>
            <input type="text" name="email" size="50" value="${form:email}" class="username" />
        ${endif:showemail}       
        ${if:showmessage}
            <div class="question">${msg:presurvey.question}</div>
            <textarea name="message" tabindex="0" cols="45" rows="2" style="border:1px solid #878787; overflow:auto">${form:message}</textarea>
        ${endif:showmessage}	
        <div align="left"><input class="light_button" type="submit" onclick="document.surveyForm.submit(); return false;" id="sndmessagelnk" value="${msg:presurvey.submit}" /></div>
        <div id="poweredByTD" align="center" class="copyr">
                ${msg:chat.window.poweredby} <a id="poweredByLink" href="http://mibew.org" title="Mibew Community" target="_blank">mibew.org</a>
        </div>
    </div>
</form>
</body>
</html>
