<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${msg:leavemessage.title}</title>
<link rel="shortcut icon" href="${webimroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />
<style type="text/css">
/*#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}
.form td{
	background-color:#f4f4f4;
	color:#525252;
}
.but{
	font-family:Verdana !important;
	font-size:11px;
	background:url(${tplroot}/images/butbg.gif) no-repeat top left;
	display:block;
	text-align:center;
	padding-top:2px;
	color:white;
	width:80px;
	height:18px;
	text-decoration:none;
	position:relative;top:1px;
}*/
</style>
</head>
<body style="margin:0px;" class="m">
<form name="leaveMessageForm" method="post" action="${webimroot}/leavemessage.php">
    <input type="hidden" name="style" value="${styleid}"/>
    <input type="hidden" name="info" value="${form:info}"/>
    <input type="hidden" name="referrer" value="${page:referrer}"/>
    ${if:formgroupid}<input type="hidden" name="group" value="${form:groupid}"/>${endif:formgroupid}
    <div id="main">
        <div class="left userphoto">

        </div>
        <div class="left logo">
            ${if:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                        </a>
                    ${else:webimHost}
                    <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                    ${endif:webimHost}
                ${else:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                    </a>
                        ${else:webimHost}
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                        ${endif:webimHost}
            ${endif:ct.company.chatLogoURL}
        </div>
        <div class="right">
            <a class="uppercase" href="javascript:window.close();" title="${msg:leavemessage.close}">${msg:leavemessage.close}</a>
        </div>
        <div class="clear"></div>
        <p class="font14"><i>${msg:leavemessage.descr}</i></p>
        ${if:errors}
            <p class="font12 another">${errors}</p>
        ${endif:errors}
        <div class="question">${msg:form.field.email}</div>
        <input type="text" name="email" size="50" value="${form:email}" class="username"/>
        <div class="question">${msg:form.field.name}</div>
        <input type="text" name="name" size="50" value="${form:name}" class="username"/>
        <div class="question">${msg:form.field.message}</div>
        <textarea name="message" tabindex="0" cols="40" rows="5" style="border:1px solid #878787; overflow:auto">${form:message}</textarea>
        ${if:showcaptcha}
            <img src="captcha.php"/>
            <input type="text" name="captcha" size="50" maxlength="15" value="" class="username"/>
        ${endif:showcaptcha}
        <div align="right"><input class="light_button" type="submit" onclick="document.leaveMessageForm.submit(); return false;" id="sndmessagelnk" value="${msg:mailthread.perform}" /></div>
        <div id="poweredByTD" align="center" class="copyr">
                ${msg:chat.window.poweredby} <a id="poweredByLink" href="http://mibew.org" title="Mibew Community" target="_blank">mibew.org</a>
        </div>
    </div>
</form>
</body>
</html>
