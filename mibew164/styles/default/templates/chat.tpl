<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>${msg:chat.window.title.agent}</title>
<link rel="shortcut icon" href="${webimroot}/images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css">
<script type="text/javascript" language="javascript" src="${webimroot}/js/${jsver}/common.js"></script>
<script type="text/javascript" language="javascript" src="${webimroot}/js/${jsver}/brws.js"></script>
<script type="text/javascript" language="javascript"><!--
var threadParams = { servl:"${webimroot}/thread.php",wroot:"${webimroot}",frequency:${page:frequency},${if:user}user:"true",${endif:user}threadid:${page:ct.chatThreadId},token:${page:ct.token},cssfile:"${tplroot}/chat.css",ignorectrl:${page:ignorectrl} };
//-->
</script>
<script type="text/javascript" language="javascript" src="${webimroot}/js/${jsver}/chat.js"></script>
<style type="text/css">
#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}
.but{
	font-family:Verdana !important;
	font-size:11px;
	background:url(${tplroot}/images/wmchat.png) top left no-repeat;
	background-position:0px -25px;
	display:block;
	text-align:center;
	padding-top:2px;
	color:white;
	width:186px;
	height:18px;
	text-decoration:none;
}
.tplimage {
	background: transparent url(${tplroot}/images/wmchat.png) no-repeat scroll 0px 0px;
	width: 25px; height: 25px;
	-moz-background-clip: -moz-initial; 
	-moz-background-origin: -moz-initial; 
	-moz-background-inline-policy: -moz-initial;
}
.irefresh { background-position:-72px 0px; }
.iclose { background-position:-24px 0px; }
.iexec { background-position:-48px 0px; }
.ihistory, .ichangeuser { background-position:-96px 0px; }
.isend { background-position:-120px 0px; }
.issl { background-position:-144px 0px; }
.isound { background-position:-168px 0px; }
.inosound { background-position:-192px 0px; }
.iemail { background-position:0px 0px; }
.iclosewin { background-position:-187px -27px; width: 15px; height: 15px; }
.tplimageloc {
	background: transparent url(${webimroot}${url:image.chat.sprite}) no-repeat scroll 0px 0px;
	-moz-background-clip: -moz-initial; 
	-moz-background-origin: -moz-initial; 
	-moz-background-inline-policy: -moz-initial;
}
.ilog { background-position: 0px 0px;width: 20px; height: 80px; }
.imessage { background-position: 0px -82px;width: 20px; height: 85px; }
div.sound {background: url(${tplroot}/images/icon_sound.png) left top no-repeat; height:35px; width:35px;}
div.sound a {height:35px; width:35px; display:block}
div.refresh {background: url(${tplroot}/images/icon_refresh.png) left top no-repeat; height:35px; width:35px;}
div.refresh a {height:35px; width:35px; display:block}
div.mail {background: url(${tplroot}/images/icon_mail.png) left top no-repeat; height:35px; width:35px;}
div.mail a {height:35px; width:35px; display:block}
</style>

</head>
<body class="m" style="margin:0px;">
<div id="main">
        ${if:user}
        <div class="left userphoto">
            <div class="avatar" id="avatarwnd"></div>
        </div>
        ${endif:user}
        <div class="left logo" ${if:user}style="margin-left:15px;"${endif:user}>
            ${if:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                        </a>
                    ${else:webimHost}
                    <img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
                    ${endif:webimHost}
                ${else:ct.company.chatLogoURL}
                    ${if:webimHost}
                    <a onclick="window.open('${page:webimHost}');return false;" href="${page:webimHost}">
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                    </a>
                        ${else:webimHost}
                            <img src="${webimroot}/images/webimlogo.gif" border="0" alt=""/>
                        ${endif:webimHost}
            ${endif:ct.company.chatLogoURL}
            <br />
            <p class="font16">
            <i class="font14">${msg:chat.client.changename}</i><Br />
            <span id="agentwnd" class="agent"></span></p>
        </div>
        <div class="right" align="right">
            <a class="uppercase" href="javascript:window.close();" title="${msg:leavemessage.close}">${msg:leavemessage.close}</a><Br />
            <div id="typingdiv" style="display:none;">${msg:typing.remote}</div>
            <br /><br /><br />
            ${if:agent}
                    <div class="left"><a class="closethread" href="javascript:void(0)" onclick="return false;" title="${msg:chat.window.close_title}">
                    <img class="tplimage iclose" src="${webimroot}/images/free.gif" border="0" alt="${msg:chat.window.close_title}"/></a></div>
            ${endif:agent}
            ${if:agent}
                <!--${if:canpost}
                    <div class="redirect left"><a href="${page:redirectLink}&amp;style=${styleid}" title="${msg:chat.window.toolbar.redirect_user}"><img class="tplimage isend" src="${webimroot}/images/free.gif" border="0" alt="Redirect&nbsp;" /></a></div>
                ${endif:canpost}-->
                ${if:historyParams}
                    <div class="redirect left"><a href="${page:historyParamsLink}" target="_blank" title="${msg:page.analysis.userhistory.title}" onclick="this.newWindow = window.open('${page:historyParamsLink}', 'UserHistory', 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,width=720,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><img class="tplimage ihistory" src="${webimroot}/images/free.gif" border="0" alt="History&nbsp;"/></a></div>
                ${endif:historyParams}
            ${endif:agent}
            ${if:user}
                <div class="mail left"><a href="${page:mailLink}&amp;style=${styleid}" target="_blank" title="${msg:chat.window.toolbar.mail_history}" onclick="this.newWindow = window.open('${page:mailLink}&amp;style=${styleid}', 'ForwardMail', 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,width=603,height=254,resizable=0'); if (this.newWindow != null) {this.newWindow.focus();this.newWindow.opener=window;}return false;"></a></div>
            ${endif:user}
            <div class="sound left" style="margin-left:10px;">
                <a id="togglesound" href="javascript:void(0)" onclick="return false;" title="Turn off sound"></a>
            </div>
            <div class="refresh left" style="margin-left:10px;">
                <a id="refresh" href="javascript:void(0)" onclick="return false;" title="${msg:chat.window.toolbar.refresh}"></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <br />
        <div style="width:100%; height:30%" id="chatwndtd">
            <iframe id="chatwnd" width="100%" height="100%" src="${webimroot}/images/blank.html#bottom" frameborder="0" style="overflow:auto;">
                Sorry, your browser does not support iframes; try a browser that supports W3 standards.
            </iframe>
        </div>
        <div class="question">Ваше сообщение</div>
        <div style="width:100%; height:70px">
            <textarea id="msgwnd" class="ss" tabindex="0" style="width:100%"></textarea>
        </div>
        <div class="left" style="margin-top:10px;">
            <!--${if:agent}${if:canpost}
            <div>
                <select id="predefined" size="1" class="answer">
                <option>${msg:chat.window.predefined.select_answer}</option>
                ${page:predefinedAnswers}
                </select>
            </div>
            ${endif:canpost}${endif:agent}-->
            <div id="poweredByTD" align="center" class="copyr left" >
                    ${msg:chat.window.poweredby} <a id="poweredByLink" href="http://mibew.org" title="Mibew Community" target="_blank">mibew.org</a>
            </div>
        </div>
        ${if:canpost}
            <div id="postmessage" class="right">
               <a href="#bottom" onclick="return false;" id="sndmessagelnk"><input class="light_button" type="submit" value="${msg:chat.window.send_message_short,send_shortcut}" /></a>
            </div>
        ${endif:canpost}
        <div class="clear"></div>
        <br />
</div>
</body>
</html>
