<?
$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");


$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_news_by_categories_generate_spb", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83653",
    "ID" => 41,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_news_by_categories_generate_msk", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83649",
    "ID" => 40,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_new_restaurants_by_categories_generate_spb", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83832",
    "ID" => 41,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_new_restaurants_by_categories_generate_msk", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "news",
    "SECTION_ID" => "83831",
    "ID" => 40,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_cookery_by_categories_generate_spb", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "cookery",
    "ID" => 139,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
    "PREVIEW_TRUNCATE_LEN" => "",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_cookery_by_categories_generate_msk", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "cookery",
    "ID" => 139,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
    "PREVIEW_TRUNCATE_LEN" => "",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_overviews_by_categories_generate_spb", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "overviews",
    "ID" => 117,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_overviews_by_categories_generate_msk", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "overviews",
    "ID" => 51,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_photoreports_by_categories_generate_spb", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "photoreports",
    "ID" => 119,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
        "restoran:subscribe.news", "subscribe_photoreports_by_categories_generate_msk", Array(
    "SITE_ID" => "s1",
    "IBLOCK_TYPE" => "photoreports",
    "ID" => 55,
    "SORT_BY" => "ACTIVE_FROM",
    "SORT_ORDER" => "DESC",
        ), null, array(
    "HIDE_ICONS" => "Y",
        )
);

$res = CIBlock::GetList(
                Array(), Array(
            'TYPE' => 'blogs'
                ), true
);
while ($ar_res = $res->Fetch()) {
    $result["ID"][] = $ar_res['ID'];
}

$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "subscribe_blogs_by_categories_generate_spb", Array(
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "blogs",
    "IBLOCK_ID" => $result["ID"],
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

$APPLICATION->IncludeComponent(
        "restoran:catalog.list_multi", "subscribe_blogs_by_categories_generate_msk", Array(
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "N",
    "AJAX_MODE" => "N",
    "IBLOCK_TYPE" => "blogs",
    "IBLOCK_ID" => $result["ID"],
    "LID1" => "s1",
    "NEWS_COUNT" => "3",
    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    "SORT_BY2" => "",
    "SORT_ORDER2" => "",
    "FILTER_NAME" => "",
    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
    "PROPERTY_CODE" => array("COMMENTS", "summa_golosov"),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "PREVIEW_TRUNCATE_LEN" => "120",
    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "N",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => "search_rest_list",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N"
        ), false
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>