<?
$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";

define("NO_KEEP_STATISTIC", true);
ini_set("memory_limit","4048M");
error_reporting(E_ALL);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


if(!CModule::IncludeModule("iblock")) die("iblock error");

$OUTPUT="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
$OUTPUT.="<reviews xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"otz.xsd\">\n";
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/xmler/otz.xml", "w");
fwrite($fp, $OUTPUT);
fclose($fp);
$OUTPUT = '';
//Выбираем все отзывы
$OTZ=array();
$I=0;
$arFilter = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"reviews");
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter);
//echo $res->SelectedRowsCount();
//exit;
while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();  
	$arFields["PROPERTIES"] = $ob->GetProperties();
	
	$REST_IDs[]=$arFields["PROPERTIES"]["ELEMENT"]["VALUE"];
        if (!in_array($arFields["CREATED_BY"], $USERs))
            $USERs[]=$arFields["CREATED_BY"];	
	
	$I++;
	//var_dump($arFields);
	
	$arFields["DATE_CREATE"] = CIBlockFormatProperties::DateFormat("Y-m-d\TH:i:00", $arFields["DATE_CREATE_UNIX"]); 
	//var_dump($arFields["CREATED_BY"]);
	
	
	
	$OTZ[]=array(
		"ID"=>$arFields["ID"],                
		"NAME"=>$arFields["NAME"],
		"DATE_CREATE"=>$arFields["DATE_CREATE"],
		"CREATED_BY"=>$arFields["CREATED_BY"],
		"PREVIEW_TEXT"=>strip_tags($arFields["PREVIEW_TEXT"]),
		"RATIO"=>strip_tags($arFields["DETAIL_TEXT"]),
		"ELEMENT"=>$arFields["PROPERTIES"]["ELEMENT"]["VALUE"],
		"PLUS"=>strip_tags($arFields["PROPERTIES"]["plus"]["VALUE"]),
		"MINUS"=>strip_tags($arFields["PROPERTIES"]["minus"]["VALUE"]),
		"CITY"=>$arFields["IBLOCK_CODE"],
		"DETAIL_PAGE_URL"=>$arFields["DETAIL_PAGE_URL"]
	);
	
	//if($I>200) break;
}


//Выбираем рестораны для отзывов
$arFilter = Array("ACTIVE_DATE"=>"Y", "IBLOCK_TYPE"=>"catalog", "ID"=>$REST_IDs);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, false);
while($ob = $res->GetNextElement()){
	$arFields = $ob->GetFields();  
	$arFields["PROPERTIES"] = $ob->GetProperties();
	
	//echo '<pre>';
	//var_dump($arFields);
	//echo '</pre>';
	
	$arFields["NAME"] = str_replace("&", "и", $arFields["NAME"]);
	
	if(!isset($RESTS[$arFields["ID"]])){
		$RESTS[$arFields["ID"]]=array(
			"NAME"=>$arFields["NAME"],
                        "IBLOCK_ID"=>$arFields["IBLOCK_ID"],
			"CODE"=>$arFields["CODE"],
			"DETAIL_PAGE_URL"=>$arFields["DETAIL_PAGE_URL"],
			"TELS"=>$arFields["PROPERTIES"]["phone"]["VALUE"],
			"ADDR"=>$arFields["PROPERTIES"]["address"]["VALUE"],
			"SITE"=>$arFields["PROPERTIES"]["site"]["VALUE"][0],
		);
	}        
}

//выбираем пользователей
$resUsers = CUser::GetList(
	($by=array("last_name"=>"asc")), ($order="asc"),
	array("ID" => implode(" | ", $USERs)),
	array("SELECT" => array("UF_WORK_OFFICE"))
);

while($arUser = $resUsers->Fetch()){
	if(!empty($arUser["PERSONAL_PHOTO"])){
		$file = CFile::ResizeImageGet($arUser["PERSONAL_PHOTO"], array('width'=>144, 'height'=>185), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true); 
		$arr=$file["src"];	
	}else $arr="";
				
	
				
	$USRs[$arUser['ID']] = array(
		"NAME" => $arUser['NAME'],
		"LAST_NAME" => $arUser['LAST_NAME'],
		"SECOND_NAME" => $arUser['SECOND_NAME'],
		"NICK" => $arUser['PERSONAL_PROFESSION'],
		"AVATAR" => $arr,
		"EMAIL" => $arUser['EMAIL'],
	);
				
				
}
	

//echo '<pre>';
//var_dump($RESTS);
//echo '</pre>';

//echo '<pre>';
//var_dump($OTZ);
//echo '</pre>';

foreach($OTZ as $O){
$OUTPUT ="<review>\n";
	$OUTPUT.="<locale>ru</locale>\n";
   	$OUTPUT.="<type>biz</type>\n";
        if ($RESTS[$O["ELEMENT"]]["IBLOCK_ID"]==12)
            $OUTPUT.="<url>http://spb.restoran.ru".$O["DETAIL_PAGE_URL"]."</url>\n";
        else
            $OUTPUT.="<url>http://www.restoran.ru".$O["DETAIL_PAGE_URL"]."</url>\n";
   
   	if($O["PLUS"]!="" ) $OUTPUT.="<pros><pro>".$O["PLUS"]."</pro></pros>\n";
   	if($O["MINUS"]!="" ) $OUTPUT.="<contras><contra>".$O["MINUS"]."</contra></contras>\n";
   	if($O["PREVIEW_TEXT"]!="" ) {
   		$O["PREVIEW_TEXT"] = str_replace("&nbsp;", "",$O["PREVIEW_TEXT"]);
	//htmlentities(string string [, int quote_style [, string charset]])
	//htmlspecialchars()
                $O["PREVIEW_TEXT"] = html_entity_decode($O["PREVIEW_TEXT"]);
                $O["PREVIEW_TEXT"] = htmlspecialchars_decode($O["PREVIEW_TEXT"]);
   		$OUTPUT.="<description>".htmlspecialchars($O["PREVIEW_TEXT"])."</description>\n";   		
   	}
        $OUTPUT.="<rating>".intval($O["RATIO"])."</rating>\n";

   	$OUTPUT.="<reviewer>\n";
 	   	$OUTPUT.="<vcard>\n";
 	   		if($USRs[$O["CREATED_BY"]]["NICK"]!=""){
 	   			$UNAME=$USRs[$O["CREATED_BY"]]["NICK"];
 	   		}else{
 	   			if($USRs[$O["CREATED_BY"]]["NAME"]!="" && $USRs[$O["CREATED_BY"]]["LAST_NAME"]!="") $UNAME=$USRs[$O["CREATED_BY"]]["NAME"]." ".$USRs[$O["CREATED_BY"]]["LAST_NAME"];
 	   			else {
 	   				$tar=explode("@",$USRs[$O["CREATED_BY"]]["EMAIL"]);
 	   				$UNAME=$tar[0];
 	   			}
 	   		}
 	   		
			$OUTPUT.="<fn>".htmlspecialchars($UNAME)."</fn>\n";
		    $OUTPUT.="<url>http://www.restoran.ru/users/id".$O["CREATED_BY"]."/</url>\n";
		    if($USRs[$O["CREATED_BY"]]["AVATAR"]!="") $OUTPUT.="<avatar>http://www.restoran.ru".$USRs[$O["CREATED_BY"]]["AVATAR"]."</avatar>\n";
		//    $OUTPUT.="<authcomments></authcomments>\n";
		//    $OUTPUT.="<karma></karma>\n";
		$OUTPUT.="</vcard>\n";
	$OUTPUT.="</reviewer>";
   	
   	
   	$OUTPUT.="<item>\n";
		
		$OUTPUT.="<vcard>\n";
		
		$OUTPUT.="<category>restaurant</category>\n";
		
		$OUTPUT.="<fn>".htmlspecialchars_decode($RESTS[$O["ELEMENT"]]["NAME"])."</fn>\n";
		
                if ($RESTS[$O["ELEMENT"]]["IBLOCK_ID"]==12)
                    $OUTPUT.="<localurl>http://spb.restoran.ru".$RESTS[$O["ELEMENT"]]["DETAIL_PAGE_URL"]."</localurl>\n";
                else
                    $OUTPUT.="<localurl>http://www.restoran.ru".$RESTS[$O["ELEMENT"]]["DETAIL_PAGE_URL"]."</localurl>\n";		
		$OUTPUT.="<url>".$RESTS[$O["ELEMENT"]]["SITE"]."</url>\n";
		
		if(count($RESTS[$O["ELEMENT"]]["TELS"])>0 && $RESTS[$O["ELEMENT"]]["TELS"][0]!=""){
			$OUTPUT.="<tels>";
			foreach($RESTS[$O["ELEMENT"]]["TELS"] as $T){
                                $T = htmlspecialchars_decode($T);
                                $T = strip_tags($T);
				$OUTPUT.="<tel>".trim($T)."</tel>\n";
			}
			$OUTPUT.="</tels>";
		}
		
		
		if(count($RESTS[$O["ELEMENT"]]["ADDR"])>0 && $RESTS[$O["ELEMENT"]]["ADDR"][0]!=""){
		$OUTPUT.="<adrs>";
			if($O["CITY"]=="spb") $CITY_RU="Санкт-Петербург";
			elseif($O["CITY"]=="msk") $CITY_RU="Москва";
			foreach($RESTS[$O["ELEMENT"]]["ADDR"] as $A){
				$OUTPUT.="<adr>\n";
				$OUTPUT.="<country-name>Россия</country-name>\n";
				$OUTPUT.="<locality>".$CITY_RU."</locality>\n";
				$OUTPUT.="<street-address>".$A."</street-address>\n";
				$OUTPUT.="</adr>\n";
			}
		$OUTPUT.="</adrs>\n";
		}
		
		
		
		$OUTPUT.="</vcard>\n";
 	
 	$OUTPUT.="</item>\n";
 	
   	$OUTPUT.="<reviewsurl>http://www.restoran.ru".$RESTS[$O["ELEMENT"]]["DETAIL_PAGE_URL"]."</reviewsurl>\n";
	
	$OUTPUT.="<dtreviewed>".$O["DATE_CREATE"]."</dtreviewed>";
$OUTPUT.="</review>\n";
    $fp = fopen($_SERVER["DOCUMENT_ROOT"]."/xmler/otz.xml", "a");
    fwrite($fp, $OUTPUT);
    fclose($fp);
}

$OUTPUT="</reviews>";


//


$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/xmler/otz.xml", "a"); // Открываем файл в режиме записи 
if(!fwrite($fp, $OUTPUT)) echo 'Ошибка при записи в файл.';
fclose($fp); //Закрытие файла

echo "done";

?>