<?
$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";

define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


if(!CModule::IncludeModule("iblock")) die("iblock error");
require($_SERVER["DOCUMENT_ROOT"].'/bitrix/templates/.default/components/restoran/editor2/editor/phpQuery-onefile.php');


$OUTPUT="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
$OUTPUT.="<entities>\n";

global $photos;

function get_file($matches)
{
    global $photos;
    //return "http://".SITE_SERVER_NAME."".CFile::GetPath($matches[1]);
    $watermark = Array(
        Array( 'name' => 'watermark',
        'position' => 'br',
        'size'=>'real',
        'type'=>'image',
        'alpha_level'=>'30',
        'file'=>$_SERVER['DOCUMENT_ROOT'].'/tpl/images/watermark.png', 
        ),
    );
    //$photo = array();
    $photos[] = CFile::ResizeImageGet($matches[1],Array(),BX_RESIZE_IMAGE_PROPORTIONAL,true,$watermark);
    return true;
}

//Выбираем все отзывы
$OTZ=array();
$I=0;
$arFilter = Array("ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_TYPE"=>"cookery", "IBLOCK_ID"=>"139", "SECTION_ID"=>Array(57432,57431));
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, Array("ID","NAME","DETAIL_TEXT","DETAIL_PAGE_URL","PROPERTY_cat","PROPERTY_prig_time","PROPERTY_porc"));
while($arFields = $res->GetNext()){		
        
        $end_photo = $category = $prig = $kitchen = "";
        $photos = array();
        $INs = $INst = Array();
        
        $r = CIBlockElement::GetByID($arFields["PROPERTY_CAT_VALUE"]);
        if ($ar = $r->GetNext())
            $category = $ar["NAME"];
        
        $r = CIBlockElement::GetByID($arFields["PROPERTY_PRIG_TIME_VALUE"]);
        if ($ar = $r->GetNext())
            $prig = $ar["NAME"];
        
        $r = CIBlockElement::GetByID($arFields["PROPERTY_COOK_VALUE"]);
        if ($ar = $r->GetNext())
            $kitchen = $ar["NAME"];
        
        
        $arFields["DETAIL_TEXT"] = preg_replace_callback("/#FID_([0-9]+)#/","get_file",$arFields["DETAIL_TEXT"]);
        $photos1  = array();
        $photos1 = $photos;
        $photos  = array();
        foreach ($photos1 as $key=>&$photo)
        {
            if ($key%2)                
                $photos[] = $photo;
        }
        $end_photo = end($photos);               
	
	$document = phpQuery::newDocument($arFields["DETAIL_TEXT"]);
	$blocks = $document->find('.ingr');
	foreach($blocks as $el)
        {
		$BL = pq($el);						
                foreach($BL->find("li") as $L){
                        $Li = pq($L);
                        $INs[]=array("NAME"=>$Li->find(".name")->text(),"CO"=>$Li->find(".amount")->text());
                }                               
        }        
        
        $blocks = $document->find('.instruction');
        foreach($blocks as $el)
        {
            $BL = pq($el);						
            $INst[]=$BL->text();
        } 
        if (count($INs)<1)
        {
            $blocks = $document->find('.vis_red')->find('ul')->find('li');
            foreach($blocks as $el)
            {
                $BL = pq($el);						
                $INs[]["NAME"]=$BL->text();
            }
        }
        if (count($INs)>=1)
        {
            $REC[]=array(
                    "ID"=>$arFields["ID"],
                    "NAME"=>  htmlspecialchars($arFields["NAME"]),
                    "CREATED_BY"=>$arFields["CREATED_BY"],
                    "DETAIL_PAGE_URL"=>$arFields["DETAIL_PAGE_URL"],
                    "CATEGORY" => $category,
                    "PRIG" => $prig,
                    "KITCHEN" => $kitchen,
                    "PORC" => htmlspecialchars($arFields["PROPERTY_PORC_VALUE"]),
                    "PHOTOS" => $photos,
                    "FINAL_PHOTO" => $end_photo,
                    "INGRIDIENTS" => $INs,
                    "INSTRUCTIONS" => $INst
            );
        }
}



//echo '<pre>';
//var_dump($RESTS);
//echo '</pre>';

//echo '<pre>';
//var_dump($OTZ);
//echo '</pre>';

foreach($REC as $O){
$OUTPUT.="\t<recipe>\n";
        $OUTPUT.="\t\t<name>".$O["NAME"]."</name>\n";
	$OUTPUT.="\t\t<url>http://www.restoran.ru".$O["DETAIL_PAGE_URL"]."</url>\n";
        if ($O["CATEGORY"])
            $OUTPUT.="\t\t<type>".$O["CATEGORY"]."</type>\n";
        if ($O["KITCHEN"])
            $OUTPUT.="\t\t<cuisine-type>".$O["KITCHEN"]."</cuisine-type>\n";
                
        foreach ($O["INGRIDIENTS"] as $ingr)
        {
            $OUTPUT.= "\t\t<ingredient>\n";
            $OUTPUT.= "\t\t\t<name>".$ingr["NAME"]." ".$ingr["CO"]."</name>\n";
            $OUTPUT.= "\t\t</ingredient>\n";
        }                
        foreach ($O["INSTRUCTIONS"] as $inst)
        {
            $OUTPUT.= "\t\t<instruction>".$inst."</instruction>\n";
        } 
        foreach ($O["PHOTOS"] as $photo)
        {
            $OUTPUT.= "\t\t<photo>http://www.restoran.ru".$photo["src"]."</photo>\n";
        }
        if ($O["FINAL_PHOTO"])
            $OUTPUT.= "\t\t<final-photo>http://www.restoran.ru".$O["FINAL_PHOTO"]["src"]."</final-photo>\n";
        
        if ($O["PORC"])
            $OUTPUT.="\t\t<yield>".$O["PORC"]."</yield>\n";
        if ($O["PRIG"])
            $OUTPUT.="\t\t<duration>".$O["PRIG"]."</duration>\n";

   		
   	
$OUTPUT.="\t</recipe>\n";
}

$OUTPUT.="</entities>";


//
$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/xmler/recipes.xml", "w+"); // Открываем файл в режиме записи 
if(!fwrite($fp, $OUTPUT)) echo 'Ошибка при записи в файл.';
fclose($fp); //Закрытие файла

echo "done";

?>