<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;


define("ADD_IBLOCK_ID", 151);
// depends from city id
//$arIBProp["BLOCK_TYPE"] = 1455;

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

$el = new CIBlockElement;
$bs = new CIBlockSection;
global $DB;

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "gamekonezimo";

$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);
$curMenuElID = ($_REQUEST["curMenuElID"] ? $_REQUEST["curMenuElID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function forAr($arr, $restSecID, $restID = 0, $arID = Array()) {
    //v_dump($arr);
    $el = new CIBlockElement;
    $bs = new CIBlockSection;
    $obPrice = new CPrice();

    foreach($arr as $a) {
        //v_dump($a["id"]);
        if (is_array($a['childs'])) {
            $secID = $bs->Add(Array(
                "ACTIVE" => ($a["is_enable"] ? "Y" : "N"),
                "IBLOCK_ID" => ADD_IBLOCK_ID,
                "IBLOCK_SECTION_ID" => (array_key_exists(intval($a["tid"]), $arID) ? $arID[$a["tid"]] : $restSecID),
                "NAME" => trim(htmlspecialchars_decode(stripcslashes($a["name"]))),
                "CODE" => translitIt($a["name"]),
                //"UF_RESTORAN" => $restID,
            ));
            $arID[$a["id"]] = $secID;


            forAr($a['childs'], $restSecID, $restID, $arID);
        }else{
            $arEl = Array();

            // parser text
            $tmpText = explode("<br />", $a["description"]);
            $arEl = Array(
                "ACTIVE" => ($a["is_enable"] ? "Y" : "N"),
                "IBLOCK_ID" => ADD_IBLOCK_ID,
                "IBLOCK_SECTION_ID" => $arID[$a["tid"]],
                "NAME" => trim(htmlspecialchars_decode(stripcslashes($a["name"]))),
                "CODE" => translitIt($a["name"]),
                "PREVIEW_TEXT" => $tmpText[0],
                //"PREVIEW_PICTURE" => $fileArray,
            );
            //v_dump($arEl);
            if(!$elID = $el->Add($arEl))
                echo "Error: ".$el->LAST_ERROR;
            // set catalog product
            $arProductFields = Array(
                "ID" => $elID,
            );
            CCatalogProduct::Add(
                $arProductFields
            );
            // set price
            $arPriceFields = Array();
            $arPriceFields = Array(
                "PRODUCT_ID" => $elID,
                "CATALOG_GROUP_ID" => 1,
                "PRICE" => intval($a["price"]),
                "CURRENCY" => "RUB ",
            );
            $obPrice->Add($arPriceFields, true);

            //v_dump($tmpText[0]);
        }
    }
}

// start execution
$time_start = microtime_float();

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

// get our rest list
$rsRest = CIBlockElement::GetList(
    Array("ID"=>"ASC"),
    Array(
        "IBLOCK_ID" => 11,
        "PROPERTY_old_item_id" => 5593,
        ">ID" => $curStepID
    ),
    false,
    false,
    Array("ID", "IBLOCK_ID", "CODE", "NAME", "PROPERTY_old_item_id")
);
while($arRest = $rsRest->Fetch()) {
    //check rest menu exist
    $rsMenuRestCheck = CIBlockSection::GetList(
        Array("ID"=>"ASC"),
        Array(
            "IBLOCK_ID" => ADD_IBLOCK_ID,
            "UF_RESTORAN" => $arRest["ID"]
        ),
        false,
        false,
        Array("ID", "CODE", "NAME")
    );
    if(!$arMenuRestCheck = $rsMenuRestCheck->Fetch()) {
        $curStepID = $arRest["ID"];
        // add rest folder

        $restSecID = $bs->Add(Array(
            "IBLOCK_ID" => ADD_IBLOCK_ID,
            "NAME" => $arRest["NAME"],
            "CODE" => $arRest["CODE"],
            "UF_RESTORAN" => $arRest["ID"],
        ));

        mysql_select_db($DBName1, $link1);

        $strSql = "
            SELECT
                ru_items_menu.*
            FROM
                ru_items_menu
            WHERE
                ru_items_menu.item_id = {$arRest["PROPERTY_OLD_ITEM_ID_VALUE"]}
            ORDER BY
                ru_items_menu.id ASC
        ";

        $res = mysql_query($strSql, $link1);

        $tree=array(0=>array('id'=>0, 'tid'=>0, 'value'=>'root'));
        $temp=array(0=>&$tree[0]);

        while($val = mysql_fetch_assoc($res))
        {
            $parent = &$temp[ $val['tid'] ];
            if (!isset($parent['childs'])) {
                $parent['childs'] = array();
            }
            $parent['childs'][$val['id']] = $val;
            $temp[$val['id']] = &$parent['childs'][$val['id']];
        }
        unset($rs,$temp,$val,$parent);
        //v_dump($tree[0]['childs']);

        //forAr($tree[0]['childs'], $restSecID, $arRest["ID"], array());

        // execution time
        $time_end = microtime_float();
        $time = $time_end - $time_start;
        if($time > 30) {
            echo "<script>setTimeout(function() {location.href = '/parser_menu.php?curStepID=".$curStepID."'}, 5000);</script>";
            break;
        }

    }
}
?>