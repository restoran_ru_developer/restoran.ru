<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление купонов");
?>

<?
$APPLICATION->AddHeadString('<link href="/tpl/css/chosen/chosen.css"  type="text/css" rel="stylesheet" />', true);
$APPLICATION->AddHeadScript('/tpl/css/chosen/chosen.jquery.js');
$APPLICATION->AddHeadScript('/tpl/js/jquery.checkbox_2.js');
?>

<div id="content">
    <?if ($_REQUEST["REST_ID"]):?>
	 <?$APPLICATION->IncludeComponent(
        "restoran:restoran.edit_form",
        "",
        Array(
            "REST_ID" => $_REQUEST["REST_ID"],
        ),
        false
    );?>
	
	<div class="clear"></div>
    <?else:?>
        <?ShowError("Не выбран ресторан")?>
    <?endif;?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>