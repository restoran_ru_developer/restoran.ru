<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление купонов");
?>
<div id="content">
<?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),					
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Навание купона", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Активность", "TYPE"=>"hidden", "CODE"=>"ACTIVE", "VALUE_FROM"=>"ACTIVE"),
			array("NAME"=>"Анонс купона", "TYPE"=>"short_text", "CODE"=>"PREVIEW_TEXT", "VALUE_FROM"=>"PREVIEW_TEXT"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"PREVIEW_PICTURE", "VALUE_FROM"=>"PREVIEW_PICTURE"),
			array("NAME"=>"Показывать с", "TYPE"=>"short_text2", "CODE"=>"ACTIVE_FROM", "VALUE_FROM"=>"ACTIVE_FROM"),
			array("NAME"=>"Показывать до", "TYPE"=>"short_text2", "CODE"=>"ACTIVE_TO", "VALUE_FROM"=>"ACTIVE_TO"),
			
			array("NAME"=>"Метро", "TYPE"=>"select", "CODE"=>"PROPERTY_subway", "VALUE_FROM"=>"PROPERTIES__subway__VALUE", "VALUES_LIST"=>"PROPS__subway__LIST"),
			
			array("NAME"=>"Купон дня", "TYPE"=>"select", "CODE"=>"PROPERTY_kupon_dnya", "VALUE_FROM"=>"PROPERTIES__kupon_dnya__VALUE", "VALUES_LIST"=>"PROPS__kupon_dnya__LIST"),
			array("NAME"=>"Новый", "TYPE"=>"select", "CODE"=>"PROPERTY_new", "VALUE_FROM"=>"PROPERTIES__new__VALUE", "VALUES_LIST"=>"PROPS__new__LIST"),
			array("NAME"=>"Популярное", "TYPE"=>"select", "CODE"=>"PROPERTY_popular", "VALUE_FROM"=>"PROPERTIES__popular__VALUE", "VALUES_LIST"=>"PROPS__popular__LIST"),
			array("NAME"=>"Рекомендуемый купон", "TYPE"=>"select", "CODE"=>"PROPERTY_RECOMENDED", "VALUE_FROM"=>"PROPERTIES__RECOMENDED__VALUE", "VALUES_LIST"=>"PROPS__RECOMENDED__LIST"),
			array("NAME"=>"Привязка к ресторану", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_REST_BIND", "VALUE_FROM"=>"PROPERTIES__REST_BIND__VALUE", "VALUES_LIST"=>"PROPS__REST_BIND__LIST"),
			array("NAME"=>"Категория", "TYPE"=>"select", "CODE"=>"PROPERTY_category", "VALUE_FROM"=>"PROPERTIES__category__VALUE", "VALUES_LIST"=>"PROPS__category__LIST"),
			
			array("NAME"=>"Размер скидки", "TYPE"=>"select", "CODE"=>"PROPERTY_sale_value", "VALUE_FROM"=>"PROPERTIES__sale_value__VALUE", "VALUES_LIST"=>"PROPS__sale_value__LIST"),
			array("NAME"=>"Стоимость", "TYPE"=>"select", "CODE"=>"PROPERTY_price", "VALUE_FROM"=>"PROPERTIES__price__VALUE", "VALUES_LIST"=>"PROPS__price__LIST"),
			array("NAME"=>"Цена", "TYPE"=>"short_text", "CODE"=>"PRICE_BASE", "VALUE_FROM"=>"PRICES__BASE"),
			
			array("NAME"=>"Скидка", "TYPE"=>"short_text", "CODE"=>"PROPERTY_sale", "VALUE_FROM"=>"PROPERTIES__sale__VALUE"),
			array("NAME"=>"Поправка купленных купонов", "TYPE"=>"short_text", "CODE"=>"PROPERTY_delta", "VALUE_FROM"=>"PROPERTIES__delta__VALUE"),
			
		),
		"TAG_SECTION"=>79133,
		"BACK_LINK"=>"/kup/"
		
	),
false
);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>