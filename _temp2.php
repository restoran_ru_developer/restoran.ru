<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Песочница");
?>

<style>
body
{

}


.ap
{
	font-family: 'appetite', appetite, "Monotype Corsiva", Georgia, Times New Roman, serif;
}
#slider-data
{
		position: relative;
}

#slider-div
{

	/*background-image: url(<?=SITE_TEMPLATE_PATH?>/images/_bg.jpg);	*/
	background-image: url(<?=SITE_TEMPLATE_PATH?>/images/arrows_bg.png);/*	*/
	background-repeat: no-repeat;
    background-position: 335px 336px;
	
}
.bbl
{
	position: absolute;
	background-repeat: no-repeat;
	background-position: center center;
	opacity: 0.75;
}




#bb1
{
	width: 341px;
	height:328px;
	top: 536px;
	left: 100px;
	background-image: url(<?=SITE_TEMPLATE_PATH?>/images/b_cloud_01.png);
}

#bb2
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_cloud_02.png");
   height: 242px;
    left: 2210px;
    top: 223px;
    width: 251px;
}
#bb3
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_cloud_03.png");
    height: 266px;
    left: 643px;
    top: 166px;
    width: 282px;
}
#bb4
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_cloud_04.png");
    height: 266px;
    left: 1844px;
    top: 543px;
    width: 282px;
}
#bb5
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_cloud_05.png");
    height: 412px;
    left: 1181px;
    top: 467px;
    width: 374px;
}





#crcl1
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_circle_01.png");
    height: 324px;
    left: 2608px;
    top: 617px;
    width: 331px;
}

#crcl2
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_circle_02.png");
    height: 412px;
    left: 2825px;
    top: 138px;
    width: 340px;
}


/*
SMALL BUBBLES
*/
#b_small_ord_01
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_ord_01.png");
    height: 78px;
    left: 297px;
    top: 259px;
    width: 164px;
}


#b_small_ord_02
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_ord_02.png");
    height: 76px;
    left: 536px;
    top: 483px;
    width: 169px;
}


#b_small_ord_03
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_ord_03.png");
    height: 77px;
    left: 504px;
    top: 771px;
    width: 187px;
}

#b_small_reg_01
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_reg_01.png");
    height: 90px;
    left: 1418px;
    top: 151px;
    width: 210px;
}


#b_small_reg_02
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_reg_02.png");
    height: 90px;
    left: 1022px;
    top: 191px;
    width: 225px;
}


#b_small_reg_03
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_reg_03.png");
    height: 76px;
    left: 817px;
    top: 567px;
    width: 190px;
}


#b_small_reg_04
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_reg_04.png");
    height: 76px;
    left: 899px;
    top: 872px;
    width: 190px;
}


#b_small_reg_05
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_reg_05.png");
    height: 68px;
    left: 1250px;
    top: 985px;
    width: 182px;
}


#b_small_reg_06
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/b_gray_reg_06.png");
   height: 77px;
    left: 1629px;
    top: 715px;
    width: 182px;
}

/* WHITE CIRCLES */

#b_circle_white_01
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_01.png");
    height: 234px;
    left: 3032px;
    top: 450px;
    width: 234px;
}

#b_circle_white_02
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_02.png");
    height: 252px;
    left: 3204px;
    top: 100px;
    width: 252px;
}

#b_circle_white_03
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_03.png");
    height: 263px;
    left: 3298px;
    top: 358px;
    width: 263px;
}

#b_circle_white_04
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_04.png");
    height: 217px;
    left: 2583px;
    top: 316px;
    width: 217px;
}

#b_circle_white_05
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_05.png");
    height: 210px;
    left: 3222px;
    top: 626px;
    width: 210px;
}

#b_circle_white_06
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_06.png");
    height: 256px;
    left: 3085px;
    top: 834px;
    width: 256px;
}

#b_circle_white_07
{
    background-image: url("<?=SITE_TEMPLATE_PATH?>/images/circle_white_07.png");
    height: 254px;
    left: 1982px;
    top: 800px;
    width: 254px;
}


#text_01
{
    color: #0093D0;
    font-size: 61px;
    left: 1764px;
    letter-spacing: -0.015em;
    line-height: 1.1em;
    text-align: center;
    top: 250px;
}

#text_02
{
    color: #3B424E;
    font-size: 61px;
    left: 2161px;
    letter-spacing: -0.021em;
    line-height: 1.1em;
    text-align: center;
    top: 606px;
}



.textblock
{
	position: absolute;
}

.textblock-title
{
    font-family: Georgia,Times New Roman,serif;
    font-size: 23px;
    margin: 0 auto 18px;
    text-align: center;
}
.textblock-title a
{
	text-decoration: none;
	color: #231f20;
}
.textblock-title a:hover
{
	
	color: #0093d0;
}

.textblock-title a .q
{
    background-color: #231f20;
    color: #FFFFFF;
    font-size: 12px;
    padding: 2px 6px;
    position: relative;
    top: -2px;
}
.textblock-title a:hover .q
{
    background-color: #0093d0;
}
.corr
{
    padding-left: 2px;
    padding-right: 10px;
}
.textblock-content
{
    font-family: Georgia,Times New Roman,serif;
    font-size: 15px;
    line-height: 1.3em;
    margin: 0 auto;
    text-align: center;
}


#tb_reg_01
{
    left: 1369px;
    top: 256px;
    width: 284px;
	
}
#tb_reg_02
{
    left: 992px;
    top: 303px;
    width: 242px;
	
}

#tb_reg_03
{
    left: 762px;
    top: 662px;
    width: 239px;
	
}
#tb_reg_04
{
    left: 832px;
    top: 970px;
    width: 274px;
	
}
#tb_reg_05
{
    left: 1191px;
    top: 1072px;
    width: 257px;
	
}
#tb_reg_06
{
    left: 1570px;
    top: 813px;
    width: 244px;
	
}


#tb_ord_01
{
    left: 237px;
    top: 357px;
    width: 244px;
	
}

#tb_ord_02
{
    left: 472px;
    top: 578px;
    width: 280px;
	
}
#tb_ord_03
{
    left: 450px;
    top: 874px;
    width: 244px;
	
}

#tb_new
{
    left: 1803px;
    top: 404px;
    width: 272px;
	
}


#bb1 .bcont{
	text-align: center;
	padding-top: 160px;
}
#bb1 .bcont-text{
    margin-left: 60px;
    margin-right: 90px;
}


#bb2 .bcont{
    padding-top: 80px;
    text-align: center;
}
#bb2 .bcont-text{
    font-size: 13px;
    line-height: 1.45em;
}
#bb2 .bcont-title{
 	margin-bottom: 12px;
}


#bb3 .bcont{
    padding-top: 99px;
    text-align: center;
}
#bb3 .bcont-text{
    font-size: 13px;
    line-height: 1.45em;
}
#bb3 .bcont-title{
 	margin-bottom: 12px;
}


#bb4 .bcont{
    padding-top: 112px;
    text-align: center;
}
#bb4 .bcont-text{
    margin-left: 40px;
    margin-right: 40px;
}
#bb4 .bcont-title{
 	margin-bottom: 12px;
}




#bb5 .bcont{
    padding-top: 161px;
    text-align: center;
}
#bb5 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 95px;
    margin-right: 95px;
}
#bb5 .bcont-title{
    margin-bottom: 18px;
}





#crcl1 .bcont{
    padding-top: 127px;
    text-align: center;
}
#crcl1 .bcont-text{
    font-size: 14px;
    line-height: 1.4em;
    margin-left: 35px;
    margin-right: 45px;
}
#crcl1 .bcont-title{
    margin-bottom: 10px;
}
#crcl1 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
}




#crcl2 .bcont{
    padding-right: 50px;
    padding-top: 146px;
    text-align: center;
}
#crcl2 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 55px;
    margin-right: 55px;
}
#crcl2 .bcont-title{
   margin-bottom: 8px;
}
#crcl2 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
}


#b_circle_white_01 .bcont{
    padding-top: 101px;
    text-align: center;
}
#b_circle_white_01 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 55px;
    margin-right: 55px;
}
#b_circle_white_01 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_01 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}


#b_circle_white_02 .bcont{
    padding-top: 84px;
    text-align: center;
}
#b_circle_white_02 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 55px;
    margin-right: 55px;
}
#b_circle_white_02 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_02 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}


#b_circle_white_03 .bcont{
    padding-top: 94px;
    text-align: center;
}
#b_circle_white_03 .bcont-text{
    color: #000000;
    font-size: 14px;
    line-height: 1.55em;
    margin-left: 35px;
    margin-right: 35px;
}
#b_circle_white_03 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_03 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}


#b_circle_white_04 .bcont{
    padding-top: 95px;
    text-align: center;
}
#b_circle_white_04 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 55px;
    margin-right: 55px;
}
#b_circle_white_04 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_04 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}

#b_circle_white_05 .bcont{
    padding-top: 88px;
    text-align: center;
}
#b_circle_white_05 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 55px;
    margin-right: 55px;
}
#b_circle_white_05 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_05 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}


#b_circle_white_06 .bcont{
    padding-top: 95px;
    text-align: center;
}
#b_circle_white_06 .bcont-text{
    font-size: 13px;
    line-height: 1.4em;
    margin-left: 55px;
    margin-right: 55px;
}
#b_circle_white_06 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_06 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}




#b_circle_white_07 .bcont{
    padding-top: 85px;
    text-align: center;
}
#b_circle_white_07 .bcont-text{
    color: #000000;
    font-size: 14px;
    line-height: 1.55em;
    margin-left: 35px;
    margin-right: 35px;
}
#b_circle_white_07 .bcont-title{
   margin-bottom: 8px;
   color: #3B424E;
}
#b_circle_white_07 .bcont-title a{
    font-size: 27px;
    line-height: 1.25em;
	color: #3B424E !important;
}




.bcont-title 
{
	margin-bottom: 10px;
}
.bcont-title a
{
    color: #FFFFFF;
    font-size: 30px;
    text-decoration: none;
}
.bcont-title a .q
{
    background-color: #FFFFFF;
    color: #0093D0;
    font-size: 12px;
    padding: 1px 6px;
    position: relative;
    text-decoration: none;
    top: -4px;
}

.bcont-text
{
	color: #FFFFFF;
	    line-height: 1.4em;
}

.wc a .q
{
	color: #FFFFFF !important;
	background-color: #3B424E !important;
}
</style>

<div id="slider-data">
	<div id="slider-div">
	
		<div id="bb1" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87632/"><span class="ap">Бронирование столиков</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Простые способы бронирования
на любой вкус.</div>
			</div>
		</div>
		
		<div id="bb2" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87646/"><span class="ap">Купоны</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Выбери предложение<br/>
					Оплати Online<br/>
					Воспользуйся скидкой.</div>
				</div>
		</div>
		<div id="bb3" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87636/"><span class="ap">Отзывы <br/>о ресторанах</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Оставляй отзывы, загружай фото!</div>
			</div>
		</div>
		<div id="bb4" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87645/"><span class="ap">Кулинария</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Отличные рецепты в новом формате!</div>
			</div>		
		</div>
		<div id="bb5" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87643/"><span class="ap">Преимущества зарегистрированных пользователей</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Регистрация на Restoran.ru стала невероятно простой.</div>
			</div>		
		</div>
		
		
		
		<div id="crcl1" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><span class="ap"><a href="/<?=CITY_ID?>/articles/help/87649/">Размещаем <br/><span class="corr"></span> ресторан</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Заполни данные <br/> Преобрети пакет размещения <br/> Размещай ресторан.</div>
			</div>			
		</div>		
		<div id="crcl2" class="bbl">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87653/"><span class="ap">Размещаем <br/> сетевой <br/> ресторан</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Единые фотография и описание ресторана сети.</div>
			</div>		
		</div>

		
		
		<div id="b_small_ord_01" class="bbl"></div>
		<div id="b_small_ord_02" class="bbl"></div>
		<div id="b_small_ord_03" class="bbl"></div>
		<div id="b_small_reg_01" class="bbl"></div>
		<div id="b_small_reg_02" class="bbl"></div>
		<div id="b_small_reg_03" class="bbl"></div>
		<div id="b_small_reg_04" class="bbl"></div>
		<div id="b_small_reg_05" class="bbl"></div>
		<div id="b_small_reg_06" class="bbl"></div>
		
		
		
		<div id="b_circle_white_01" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87650/"><span class="ap">Размещаем<br/> доставку<br/><span class="corr"></span> еды</span> <span class="q">?</span></a></div>
				
			</div>		
		</div>
		<div id="b_circle_white_02" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87654/"><span class="ap">Размещаем<br/> компанию-<br/><span class="corr"></span>поставщика</span> <span class="q">?</span></a></div>
				
			</div>			
		</div>
		<div id="b_circle_white_03" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87655/"><span class="ap">Приобретаем<br/> баннеры</span> <span class="q">?</span></a></div>
				<div class="bcont-text">Выбери раздел размещения <br/>
Выбери тип баннера <br/>
Оплати</div>
			</div>			
		</div>
		<div id="b_circle_white_04" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87648/"><span class="ap">Публикуем<br/><span class="corr"></span> вакансии</span> <span class="q">?</span></a></div>
				
			</div>			
		</div>
		<div id="b_circle_white_05" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87651/"><span class="ap">Добавляем<br/><span class="corr"></span> купоны</span> <span class="q">?</span></a></div>
				
			</div>			
		</div>
		<div id="b_circle_white_06" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87652/"><span class="ap">Отслеживаем<br/><span class="corr"></span> бронирования</span> <span class="q">?</span></a></div>
				
			</div>			
		</div>
		<div id="b_circle_white_07" class="bbl wc">
			<div class="bcont">
				<div class="bcont-title"><a href="/<?=CITY_ID?>/articles/help/87647/"><span class="ap">Общайтесь</span> <span class="q">?</span></a></div>
				<div class="bcont-text">
				Общайтесь с коллегами,<br/> комментируйте блоги,<br/>  приглашайте <br/> 
в ресторан!
				</div>
			</div>			
		</div>
		
		
		<div id="text_01" class="bbl ap">Новый <br/>Ресторан.ру</div>
		<div id="text_02" class="bbl ap">Для <br/>рестораторов</div>
		
		
		<div class="textblock" id="tb_reg_01">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87642/">Пригласи в ресторан <span class="q">?</span></a></div>
			<div class="textblock-content">Теперь ты можешь пригласить 
в ресторан, как понравившихся
пользователей, так и своих друзей, незарегистрированных на портале.</div>
		</div>		
		
		<div class="textblock" id="tb_reg_02">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87641/">Добавить в друзья <span class="q">?</span></a></div>
			<div class="textblock-content">Теперь ты можешь подписаться
на интересных пользователей,
добавив их в друзья.</div>
		</div>
				
		<div class="textblock" id="tb_reg_03">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87637/">Твой личный блог <span class="q">?</span></a></div>
			<div class="textblock-content">Отличная площадка 
для общения Online.</div>
		</div>
		
							
		<div class="textblock" id="tb_reg_04">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87638/">Обмен сообщениями <span class="q">?</span></a></div>
			<div class="textblock-content">Можешь вести переписку
с понравившимися пользователями.</div>
		</div>
		
			
									
		<div class="textblock" id="tb_reg_05">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87639/">Твое резюме <span class="q">?</span></a></div>
			<div class="textblock-content">Ищешь работу в ресторнной сфере?</div>
		</div>
		
			
					
									
		<div class="textblock" id="tb_reg_06">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87640/">Еда с доставкой <span class="q">?</span></a></div>
			<div class="textblock-content">Теперь ты можешь заказать еду 
с доставкой в любое удобное место!</div>
		</div>
		
			
		
					
					
									
		<div class="textblock" id="tb_ord_01">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87633/">По телефону <span class="q">?</span></a></div>
			<div class="textblock-content">Теперь ты можешь заказать еду 
с доставкой в любое удобное место!</div>
		</div>					
									
		<div class="textblock" id="tb_ord_02">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87634/">Бронирование Online <span class="q">?</span></a></div>
			<div class="textblock-content">Теперь ты можешь заказать еду 
с доставкой в любое удобное место!</div>
		</div>									
		<div class="textblock" id="tb_ord_03">
			<div class="textblock-title"><span class="corr"></span><a href="/<?=CITY_ID?>/articles/help/87632/">По SMS <span class="q">?</span></a></div>
			<div class="textblock-content">Теперь ты можешь заказать еду 
с доставкой в любое удобное место!</div>
		</div>
		
			
		
		<div class="textblock" id="tb_new">
			<div class="textblock-title" style="font-weight: bold;">Что изменилось?</div>
			<div class="textblock-content">Ура! Свершилось! Теперь Ресторан.ру 
не только стильный и красивый, 
но и невероятно удобный!</div>
		</div>
		
			
		
		

		
		
		
		
	</div>
	
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>