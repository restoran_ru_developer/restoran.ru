<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");


$rsUser = $USER->GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$podpiska = unserialize($arUser["WORK_STREET"]);
$selectedRubrics = array();
$city = $podpiska['CITY_ID'];
foreach ($podpiska["rubriki"] as $key => $value) {
    $selectedRubrics[$value] = $value;
}


$body = '';
$body .= '<table cellpadding="0" cellspacing="0" width="100%" style="background:#2c323c;  padding:10px 40px; padding-top:0px;"><tr><td style="padding-bottom:0px;"><table align="center" cellpadding="0" cellspacing="0"  width="730" height="167" style="position:relative; overflow:hidden; background-image:url(\'http://restoran.ru/tpl/images/mail/rassilka/rassilka_head.jpg\')"><tr><td height="167" style="color:#FFF; text-align:center; font-size:14px; font-family: Georgia; "><a href="http://www.restoran.ru/" style="width:100%; height: 167px; display:block"></a></td></tr></table><table align="center" cellpadding="0" cellspacing="0"  width="730" style="position:relative;">';

if (isset($selectedRubrics['news'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/news_" . $city . ".php");
}
if (isset($selectedRubrics['receipts'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/receipts_" . $city . ".php");
}
if (isset($selectedRubrics['new_rests'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/new_rests_" . $city . ".php");
}
if (isset($selectedRubrics['reviews'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/reviews_" . $city . ".php");
}
if (isset($selectedRubrics['photoreports'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/photoreports_" . $city . ".php");
}
if (isset($selectedRubrics['blogs'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/blogs_" . $city . ".php");
}
if (isset($selectedRubrics['afisha'])) {
    $body .= file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/subscribe/templates/rubrics/afisha_" . $city . ".php");
}
$body .= '<tr><td style="background:#FFFFFF; padding:30px 37px; padding-bottom:30px; text-align: center;">Управление рассылками доступно только зарегистированным пользователям в личном кабинете. Пожалуйста, пройдите процедуру регистрации <a style="line-height: 13px; text-decoration: none; border-bottom: 1px solid #0097D6; color:#0097D6; font-size:12px; font-family: Georgia;" href="http://www.restoran.ru/auth/?forgot_password=yes">здесь</a></td></tr>';
$body .= '</table></td></tr></table>';
echo $body;
//mail("ti@cakelabs.com", "My Subject", $body);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>