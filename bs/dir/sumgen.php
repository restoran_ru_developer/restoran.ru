<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$SEC_CODE="msk";

if(!CModule::IncludeModule("iblock")){
	$this->AbortResultCache();
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}


$REST_IDS=array();
$DOLGI=array();
$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_status"=>1808, "SECTION_CODE"=>$SEC_CODE);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
while($ob = $res->GetNextElement()){ 
 	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties();
	
	if(!in_array($arProps["rest"]["VALUE"], $REST_IDS)){
		$REST_IDS[]=$arProps["rest"]["VALUE"];
	}
	$DOLGI[$arProps["rest"]["VALUE"]]+=$arProps["procent"]["VALUE"];
	

}

$arFilter = Array("IBLOCK_TYPE"=>"catalog", "ACTIVE"=>"Y", "ID"=>$REST_IDS);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
while($ob = $res->GetNextElement()){ 
 	$arFields = $ob->GetFields();  
	$RESTS[$arFields["ID"]]=$arFields["NAME"];
}


$arFilter = Array("IBLOCK_ID"=>2541, "ACTIVE"=>"Y", "SECTION_CODE"=>$SEC_CODE);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
while($ob = $res->GetNextElement()){ 
 	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties();
	
	$DOLGI_BTRX[$arProps["rest"]["VALUE"]]=array("ID"=>$arFields["ID"], "DOLG"=>$arProps["dolg"]["VALUE"]);
}


foreach($DOLGI as $REST_ID => $D){
	if(isset($DOLGI_BTRX[$REST_ID])){
		//должник уже есть
		if($DOLGI_BTRX[$REST_ID]["ID"]!=$D) CIBlockElement::SetPropertyValuesEx($DOLGI_BTRX[$REST_ID]["ID"], false, array("dolg" => $D));
	}else{
		//создаем нового должника
		if($RESTS[$REST_ID]!=""){
			$el = new CIBlockElement;
		
			if($SEC_CODE=="spb") $SECID=185097;
  			else $SECID=185096;
		
		
			$PROP = array();
			$PROP["dolg"] = $D;  
			$PROP["rest"] = $REST_ID;      

			$arLoadProductArray = Array(
  				"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
  				"IBLOCK_SECTION_ID" => $SECID,          // элемент лежит в корне раздела
  				"IBLOCK_ID"      => 2541,
  				"PROPERTY_VALUES"=> $PROP,
  				"NAME"           => $RESTS[$REST_ID],
  				"ACTIVE"         => "Y",            // активен
  			);
		
			var_dump($arLoadProductArray);
			echo '<br/><br/>';
			
			$el->Add($arLoadProductArray);
		}
		
	}

}

/*
echo '<pre>';
var_dump($DOLGI_BTRX);
var_dump($DOLGI);
echo '</pre>';
*/	

?>