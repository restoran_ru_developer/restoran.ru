<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


//var_dump($_REQUEST);
function print_orders(){
    if(count($_REQUEST["ids"])==0) die("Нет элементов для печати!");

    //далее формируем фильтр и выбираем все элементы, которые подпадают под условия
    if(!CModule::IncludeModule("iblock")){
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $RDOLG = array();
    $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_status"=>1808);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("PROPERTY_rest","PROPERTY_procent"));
    while($arFields = $res->Fetch()){
        //var_dump($arFields);

        $RDOLG[$arFields["PROPERTY_REST_VALUE"]]+=$arFields["PROPERTY_PROCENT_VALUE"];
    }


    $arFilter = Array("IBLOCK_ID"=>2541, "ACTIVE"=>"Y", "ID"=>$_REQUEST["ids"]);

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Сводная таблица</title>
        <style type="text/css">
            HTML,BODY{margin:0px;padding:0px;height:100%;width:100%;}
            BODY{font-family: Arial;font-size:12px;color:#1a1a1a;line-height: 18px;background-color: #fff;}

            table{border-collapse: collapse;border:1px solid #000;width:900px;margin:0 auto;margin-top:20px;}
            table tr{}
            table tr th{line-height: 13px;padding-top: 4px;padding-bottom: 4px;font-size: 13px;padding-left: 10px;vertical-align: middle;border:1px solid #000;text-align: left;background-color: #edebea;padding-right: 4px;}
            table tr td{line-height: 13px;padding-top: 4px;padding-bottom: 4px;font-size: 13px;padding-left: 10px;vertical-align: middle;border:1px solid #000;padding-right: 4px;}
            table tr.itog td{background-color: #edebea;}
            table tr .c1{width:300px;}
            table tr .c2{width:100px;}
            table tr .c3{width:100px;}
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
    <table cellpadding="0" cellspacing="0">
        <tr class="headd">
            <th>Название</th>
            <!--			<th>Общая сумма</th>-->
            <th>Сумма долга по директорской программе</th>
            <th>Сумма долга по программе бронирования</th>
            <th>Комментарии</th>
        </tr>
        <?
        $res = CIBlockElement::GetList(Array("NAME"=>"ASC","date_active_from"=>"ASC"), $arFilter, false, false, false);
        while($ob = $res->GetNextElement()){
            $arElement = $ob->GetFields();
            $arElement["PROPERTIES"] = $ob->GetProperties();

            $ELEMENTS[]=$arElement;

        }

        foreach($ELEMENTS as $arElement) $Dolgsids[]=$arElement["ID"];

        $arFilter = Array("IBLOCK_ID"=>2542, "ACTIVE"=>"Y", "PROPERTY_dolg"=>$Dolgsids);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();

            $VIPLATI[$arProps["dolg"]["VALUE"]]+=$arProps["summa"]["VALUE"];
        }
        ?>
        <?foreach($ELEMENTS as $cell=>$arElement):?>
            <tr id="dolg_<?=$arElement["ID"]?>">
                <td class="c1">
                    <?if(!$arElement["DISPLAY_PROPERTIES"]['rest']['LINK_ELEMENT_VALUE'][$arElement["DISPLAY_PROPERTIES"]['rest']['VALUE']]['NAME']):?>
                        <?
                        $res = CIBlockElement::GetByID($arElement["DISPLAY_PROPERTIES"]['rest']['VALUE']);
                        if($ar_res = $res->Fetch()){
                            echo $ar_res['NAME'];
                        }
                        else {
                            echo $arElement['NAME'];
                        }
                        ?>
                    <?else:?>
                        <?=$arElement["DISPLAY_PROPERTIES"]['rest']['LINK_ELEMENT_VALUE'][$arElement["DISPLAY_PROPERTIES"]['rest']['VALUE']]['NAME']//$arElement['NAME']?>
                    <?endif?>
                </td>
                <td class="c2"><?//=$arElement["PROPERTIES"]["dolg"]["VALUE"]?>
                    <?

                    $DOLG = 0;
                    $DOLG = $arElement["PROPERTIES"]["dolg"]["VALUE"]-$VIPLATI[$arElement["ID"]];
                    echo $DOLG;

                    $SUMMA1+=$arElement["PROPERTIES"]["dolg"]["VALUE"];
                    $SUMMA2+=$DOLG;
                    ?>
                </td>
                <td class="c3">
                    <?=$RDOLG[$arElement["PROPERTIES"]["rest"]["VALUE"]];
                    $SUMMA3+=$RDOLG[$arElement["PROPERTIES"]["rest"]["VALUE"]];
                    ?>
                </td>
                <td class="c4"><?=$arElement["PREVIEW_TEXT"]?></td>
            </tr>
        <?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>

        <tr class="itog">
            <td ><b>Итого:</b></td>
            <td><?=$SUMMA2//$SUMMA1?></td>
            <td><?=$SUMMA3?></td>
            <td ></td>
        </tr>
    </table>
    </body>
    </html>
<?
}
print_orders();
?>