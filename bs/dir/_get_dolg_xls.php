<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function create_report(){
	if(count($_REQUEST["ids"])==0) die("Нет элементов для печати!");
	//var_dump($_REQUEST["ids"]);
	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	global $USER;
	require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel.php';
  	    
    $objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"]."/bs/dolg_report_new.xlsx");
 
	
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
  
    //Делаем у ячеек рамки
	$styleArray = array(
	'borders' => array(
		'inside' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb'=>'FF000000')
		),
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		)
	),
	);

        $RDOLG = array();
    $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "PROPERTY_status"=>1808);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("PROPERTY_rest","PROPERTY_procent"));
    while($arFields = $res->Fetch()){
        //var_dump($arFields);

        $RDOLG[$arFields["PROPERTY_REST_VALUE"]]+=$arFields["PROPERTY_PROCENT_VALUE"];
    }


	$arFilter = Array("IBLOCK_ID"=>2541, "ACTIVE"=>"Y", "ID"=>$_REQUEST["ids"]);
	$res = CIBlockElement::GetList(Array("NAME"=>"ASC","date_active_from"=>"ASC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){ 
 		$arElement = $ob->GetFields();  
 		$arElement["PROPERTIES"] = $ob->GetProperties();
 		
 		$ELEMENTS[]=$arElement;
 		
	}
	
	foreach($ELEMENTS as $arElement) $Dolgsids[]=$arElement["ID"]; 
	
	$arFilter = Array("IBLOCK_ID"=>2542, "ACTIVE"=>"Y", "PROPERTY_dolg"=>$Dolgsids);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
  
		$VIPLATI[$arProps["dolg"]["VALUE"]]+=$arProps["summa"]["VALUE"];
	}
	

 	
 	
// 	$FILE_NAME2="dolg_otchet.xls";
 	$FILE_NAME2="dolg_otchet_new.xls";

 	
 
 	$i=3;
 	if(count($ELEMENTS)==0){
 		$EXIT["message"]="not_dolg";
 	}else{
 		foreach($ELEMENTS as $cell=>$arElement){
 			$DOLG = 0;
			$DOLG = $arElement["PROPERTIES"]["dolg"]["VALUE"]-$VIPLATI[$arElement["ID"]];


 			$objPHPExcel->setActiveSheetIndex(0)
   			->setCellValue('A'.$i, $arElement["NAME"])
   			->setCellValue('B'.$i, $DOLG)
   			->setCellValue('C'.$i, $RDOLG[$arElement["PROPERTIES"]["rest"]["VALUE"]])
   			->setCellValue('D'.$i, $arElement["PREVIEW_TEXT"]);	
   			$i++;
   			
//   			$SUMMA1+=$arElement["PROPERTIES"]["dolg"]["VALUE"];
            $SUMMA2+=$DOLG;
            $SUMMA3+=$RDOLG[$arElement["PROPERTIES"]["rest"]["VALUE"]];
 		}
 		
 		$i++;
 		
 		$objPHPExcel->setActiveSheetIndex(0)
   			->setCellValue('A'.$i, "Итого")
   			->setCellValue('B'.$i, $SUMMA2)
   			->setCellValue('C'.$i, $SUMMA3)
   			->setCellValue('D'.$i, "");
 		
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$FILE_NAME2.'"');
		header('Cache-Control: max-age=0');
		$objWriter->save("php://output");
		
	}
}

create_report();
?>