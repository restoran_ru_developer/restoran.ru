<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");




cidlookup($_REQUEST["number"], $_REQUEST["lang"]);


function cidlookup($NUMBER, $LANG){
	//header("Content-type: text/xml");
	
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
		
	if(strlen($NUMBER)>10) $NUMBER = substr($NUMBER, -10);
	
	
	
	//$dom->appendChild($root);
	
//	$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y","PROPERTY_TELEPHONE"=>$NUMBER);
//	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
//	if($ob = $res->GetNextElement()){
//  		$arFields = $ob->GetFields();
//  		$arFields["PROPERTIES"] = $ob->GetProperties();
//  		//print_r($arFields);
//  		if($LANG=="en"){
//
//  			$arFields["PROPERTIES"]["SURNAME"]["VALUE"] = translitIt($arFields["PROPERTIES"]["SURNAME"]["VALUE"]);
//  			$arFields["PROPERTIES"]["NAME"]["VALUE"] = translitIt($arFields["PROPERTIES"]["NAME"]["VALUE"]);
//  		}
//
//  		echo $arFields["PROPERTIES"]["SURNAME"]["VALUE"]." ".$arFields["PROPERTIES"]["NAME"]["VALUE"];
//
//	}
    $arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y","PROPERTY_TELEPHONE"=>$NUMBER);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('PROPERTY_SURNAME','PROPERTY_NAME'));
    if($ob = $res->Fetch()){
        if($LANG=="en"){
            $ob["PROPERTY_SURNAME_VALUE"] = translitIt($ob["PROPERTY_SURNAME_VALUE"]);
            $ob["PROPERTY_NAME_VALUE"] = translitIt($ob["PROPERTY_NAME_VALUE"]);
        }
        echo $ob["PROPERTY_SURNAME_VALUE"]." ".$ob["PROPERTY_NAME_VALUE"];
    }
}

?>