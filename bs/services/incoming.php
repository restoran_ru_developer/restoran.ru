<?
//http://ip/popup.php?number=9251234567&operator=201

define("NO_KEEP_STATISTIC", true);

define("SITE_TEMPLATE_PATH", "/bitrix_personal/templates/Booking");
define("SITE_TEMPLATE_ID", "Booking");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

incoming ($_REQUEST["number"], $_REQUEST["operator"]);


function incoming($NUMBER, $OPERATOR){
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$memcache_obj = new Memcache;
	$memcache_obj->connect('127.0.0.1', 11211) or die("Could not connect");
		
	if(strlen($NUMBER)>10) $NUMBER = substr($NUMBER, -10);
	
	
	
//	$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y","PROPERTY_TELEPHONE"=>array($NUMBER, "8".$NUMBER, "9".$NUMBER));
//	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, false);
//	if($ob = $res->GetNextElement()){
//  		$arFields = $ob->GetFields();
//  		$arFields["PROPERTIES"] = $ob->GetProperties();
//
//		$EXIT=array("client"=>$arFields["ID"], "telephone"=>$NUMBER, "name"=>$arFields["PROPERTIES"]["SURNAME"]["VALUE"]." ".$arFields["PROPERTIES"]["NAME"]["VALUE"]);
//		//var_dump($EXIT);
//  		if($memcache_obj->set('operator_'.$OPERATOR, json_encode($EXIT), false, 600)) echo 'done';
//  		$memcache_obj->set('operator_666', json_encode($EXIT), false, 600);
//  	}

    $arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y","PROPERTY_TELEPHONE"=>array($NUMBER, "8".$NUMBER, "9".$NUMBER));
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID','PROPERTY_SURNAME','PROPERTY_NAME'));
    if($ob = $res->Fetch()){
        $EXIT=array("client"=>$ob["ID"], "telephone"=>$NUMBER, "name"=>$ob["PROPERTY_SURNAME_VALUE"]." ".$ob["PROPERTY_NAME_VALUE"]);
        if($memcache_obj->set('operator_'.$OPERATOR, json_encode($EXIT), false, 600)) echo 'done';
        $memcache_obj->set('operator_666', json_encode($EXIT), false, 600);
    }
    else{
  		$EXIT=array("client"=>"NEW", "telephone"=>$NUMBER, "name"=>"");
  		if($memcache_obj->set('operator_'.$OPERATOR, json_encode($EXIT), false, 600)) echo 'done';
  		$memcache_obj->set('operator_666', json_encode($EXIT), false, 600);
  	}
  	
  	$memcache_obj->close();
}

?>