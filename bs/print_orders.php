<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


//var_dump($_REQUEST);
function print_orders(){
	if(count($_REQUEST["ids"])==0) die("Нет элементов для печати!");

	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "ID"=>$_REQUEST["ids"]);
	
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Печать заказов</title>
 	<style type="text/css">
		HTML,BODY{margin:0px;padding:0px;height:100%;width:100%;}
		BODY{font-family: Arial;font-size:12px;color:#1a1a1a;line-height: 18px;background-color: #fff;}
		
		table{border-collapse: collapse;border:1px solid #000;width:900px;margin:0 auto;margin-top:20px;}
		table tr{}
		table tr th{line-height: 13px;padding-top: 4px;padding-bottom: 4px;font-size: 13px;padding-left: 10px;vertical-align: middle;border:1px solid #000;text-align: left;background-color: #edebea;padding-right: 4px;}	
		table tr td{line-height: 13px;padding-top: 4px;padding-bottom: 4px;font-size: 13px;padding-left: 10px;vertical-align: middle;border:1px solid #000;padding-right: 4px;}	
		table tr.itog td{background-color: #edebea;}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
	<table cellpadding="0" cellspacing="0">
		<tr class="headd">
		<th>ID</th>
		<th>Статус</th>
		<th>Дата создания</th>
		<th>Дата мероприятия</th>
		<th>Ресторан</th>
		<th>Сумма счета</th>
		<th>Сумма выплат</th>
		<th>Гостей</th>
		<th>Тип</th>
		<th>Примечание</th>
	</tr>
	<?
	$NOW_REST="";
	$COORD=0;
	$res = CIBlockElement::GetList(Array("PROPERTY_rest.NAME"=>"ASC","date_active_from"=>"ASC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){ 
 		$COORD++;
 		$arElement = $ob->GetFields();  
 		$arElement["PROPERTIES"] = $ob->GetProperties();
 		
 		$CLIENT_NAME="";
		$CLIENT_SURNAME="";
		$DATE="";
		$TIME="";
	
		$tar=explode(" ", $arElement["ACTIVE_FROM"]);
 		$tar2=explode(":", $tar[1]);
 		$DATE = $tar[0];
 		$TIME = $tar2[0].":".$tar2[1];
	
		if($TIME==":") $TIME="";

		$tar=explode(" ", $arElement["DATE_CREATE"]);	
		$arElement["DATE_CREATE"]=$tar[0];
 	
 		//Берем инфу по ресторану
		if($arElement["PROPERTIES"]["rest"]["VALUE"]==0) $REST_NAME="подбор";
		else{
			$res_rest = CIBlockElement::GetByID($arElement["PROPERTIES"]["rest"]["VALUE"]);
			if($arFields_rest = $res_rest->GetNext()){
 				$REST_NAME=$arFields_rest["NAME"];
 			}
		}
		
		if($NOW_REST=="") $NOW_REST = $arFields_rest["NAME"];
		
		if($NOW_REST!=$arFields_rest["NAME"]){
			//if($REST_SUMM>0){
			?>
			<tr class="itog">
				<td class="cr0"></td>
				<td class="cr1"></td>
				<td class="cr2"></td>
				<td class="cr2"></td>
				<td class="cr3"></td>
				<td class="cr4"><?=$REST_SUMM?></td>
				<td class="cr5"><?=$REST_PROCENT?></td>
				<td class="cr0"></td>
				<td class="cr6"></td>
				<td class="cr7"></td>
			</tr>
			<?	
			//}
 			$REST_SUMM=$REST_PROCENT=0;
 			$NOW_REST=$arFields_rest["NAME"];
 			
 						
 		}
 		
 		$PROCENT_SUMMA+=$arElement["PROPERTIES"]["procent"]["VALUE"];
 		$SCH_SUMMA+=$arElement["PROPERTIES"]["summ_sch"]["VALUE"];
 		
 		$REST_SUMM+=$arElement["PROPERTIES"]["summ_sch"]["VALUE"];
 		$REST_PROCENT+=$arElement["PROPERTIES"]["procent"]["VALUE"];
 		//var_dump($arElement);
 		
 		
 		
 		
 		?>
 		
 		
 		<tr>
		<td class="cr0"><?=$arElement["ID"]?></td>
		<td class="cr1"><?=$arElement["PROPERTIES"]["status"]["VALUE"]?></td>
		<td class="cr2"><?=$arElement["DATE_CREATE"]?></td>
		<td class="cr2"><?=$DATE?></td>
		<td class="cr3"><?=$REST_NAME?></td>
		<td class="cr4"><?=$arElement["PROPERTIES"]["summ_sch"]["VALUE"]?></td>
		<td class="cr5"><?=$arElement["PROPERTIES"]["procent"]["VALUE"]?></td>
		<td ><?=$arElement["PROPERTIES"]["guest"]["VALUE"]?></td>
		<td class="cr0"><?=$arElement["PROPERTIES"]["type"]["VALUE"]?></td>
		<td class="cr7"><?=$arElement["PROPERTIES"]["prim_oplata"]["VALUE"]?></td>
		</tr>
 		<?
 		
 		
 	}
 	?>
 	<tr class="itog">
 		<td colspan="5"><b>Всего заказов: <?=$COORD?></b></td>
 		<td><?=$SCH_SUMMA?></td>
 		<td><?=$PROCENT_SUMMA?></td>
 		<td colspan="3"></td>
 	</tr>
</table>	
</body>
</html>
<?	    
}
print_orders();
?>