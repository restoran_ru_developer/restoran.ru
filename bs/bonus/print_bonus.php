<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


//var_dump($_REQUEST);
function print_orders(){
	global $DB;
	if(count($_REQUEST["clients_ids"])==0) die("Нет элементов для печати!");

	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	
	//выбираем заказы и группируем по пользователям	
	$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_status"=>array(1808,1506,1810), "SECTION_CODE"=>$_SESSION["CITY"],"PROPERTY_CLIENT"=>$_REQUEST["clients_ids"]);
	if($_SESSION["CITY"]=="spb") $arFilter[">DATE_CREATE"]=date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,3,2,2014));

	$res = CIBlockElement::GetList(Array("CNT"=>"DESC"), $arFilter, array("PROPERTY_client"), false, array("ID"));
	while($ob = $res->GetNextElement()){
	    $arFields = $ob->GetFields();
	    if($arFields["CNT"]>1 || ($_SESSION["CITY"]=="spb" && $arFields["CNT"]==1)){
	        $CLIDs[]=$arFields["PROPERTY_CLIENT_VALUE"];
	        $CL[$arFields["PROPERTY_CLIENT_VALUE"]]=$arFields["CNT"];
	    }
	}
	
	
	
	//выбираем инфу по пользователям
	$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$CLIDs);
	$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){
	    $arFields = $ob->GetFields();
	    $arProperties = $ob->GetProperties();
	
	    $CLIENTS[$arFields["ID"]]=array(
	        "ID"=>$arFields["ID"],
	        "NAME"=>trim($arProperties["NAME"]["VALUE"]." ".$arProperties["SURNAME"]["VALUE"]),
	        "TELEPHONE"=>$arProperties["TELEPHONE"]["VALUE"]
	    );
	}
		
	//Выбираем истории выплат по пользователям
	$arFilter = Array("IBLOCK_ID"=>2964, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){
	    $arFields = $ob->GetFields();
	    $arProperties = $ob->GetProperties();
	
	    $PAYS[$arProperties["client"]["VALUE"]][]=array(
	        "ID"=>$arFields["ID"],
	        "DATE_CREATE"=>$arFields["DATE_CREATE"],
	        "SUMM"=>$arProperties["summ"]["VALUE"]
	    );
	    $CLIENTS[$arProperties["client"]["VALUE"]]["ALLSUMM"]+=$arProperties["summ"]["VALUE"];
	}	
		
	if(count($CLIDs)==0) die();

	
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Печать заказов</title>
 	<style type="text/css">
		HTML,BODY{margin:0px;padding:0px;height:100%;width:100%;}
		BODY{font-family: Arial;font-size:12px;color:#1a1a1a;line-height: 18px;background-color: #fff;}
		
		table{border-collapse: collapse;border:1px solid #000;width:900px;margin:0 auto;margin-top:20px;}
		table tr{}
		table tr th{line-height: 13px;padding-top: 4px;padding-bottom: 4px;font-size: 13px;padding-left: 10px;vertical-align: middle;border:1px solid #000;text-align: left;background-color: #edebea;padding-right: 4px;}	
		table tr td{line-height: 13px;padding-top: 4px;padding-bottom: 4px;font-size: 13px;padding-left: 10px;vertical-align: middle;border:1px solid #000;padding-right: 4px;}	
		table tr.itog td{background-color: #edebea;}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
	<table cellpadding="0" cellspacing="0">
		<tr class="headd">
			<th>#</th>
			<th>ФИО</th>
			<th>Номер телефона</th>
			<th>Сумма</th>
			<th>Отправлено</th>
		</tr>
		
		<?foreach($CL as $clid=>$cnt){?>
            <?
			$i++;
            if($CLIENTS[$clid]["ID"]=="") continue;
            $NEED_PAY=false;
            $SUM=0;

            $FIRST_BONUS=300;
            $SECOND_BONUS=500;



            $Class="cltr";
            $Class.=" orders".$cnt;
            if($cnt>4) $Class.=" orders4";


            $CITY_CODES=array(812,495,499);
            $TCODE = substr($CLIENTS[$clid]["TELEPHONE"], 0, 3);

            if(($cnt>=3) && $CLIENTS[$clid]["TELEPHONE"]!="" && !in_array($TCODE, $CITY_CODES)){
                if(count($PAYS[$clid])==0){
                    $NEED_PAY=true;
                    $SUM=$FIRST_BONUS;
                    $Class.=" need_pay";
                }else{
                    if($cnt%5==0 || $cnt>10 || ($cnt>=5 && $cnt<=10)){
                        $v=intval($cnt/5);//$SECOND_BONUS за 5 заказ
                        if($CLIENTS[$clid]["ALLSUMM"]<$v*$SECOND_BONUS+$FIRST_BONUS+$SECOND_BONUS){
                            $NEED_PAY=true;
                            $SUM=$SECOND_BONUS;
                            $Class.=" need_pay v".$v;
                        }
                    }
                }
            }
			$ALL_SUM+=$SUM;
            ?>	
	 		<tr>
				<td class="cr0"><?=$i?></td>
				<td class="cr1"><?=$CLIENTS[$clid]["NAME"]?></td>
				<td class="cr2"><?=$CLIENTS[$clid]["TELEPHONE"]?></td>
				<td class="cr2"><?if($NEED_PAY){?><?=$SUM?><?}?></td>
				<td class="cr3"></td>
			</tr>
 		<?
	 	}
	 ?> 		
 	<tr class="itog">
 		<td colspan="3"><b>Всего:</b></td>
 		<td><?=$ALL_SUM?></td>
 		<td></td>
 	</tr>
</table>	
</body>
</html>
<?	    
}
print_orders();
?>