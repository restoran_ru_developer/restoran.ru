<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function create_report(){
	if(count($_REQUEST["ids"])==0) die("Нет элементов для печати!");

	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	global $USER;
	require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel.php';
  	    
    $objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"]."/bs/oparator_report_procent.xls");
 
	
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(14);
	
	
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.2);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.2);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.2);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.2);
	
  
    //Делаем у ячеек рамки
	$styleArray = array(
	'borders' => array(
		'inside' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb'=>'FF000000')
		),
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		)
	),
	'font'=> array(
		'size'=> 12
	)
	);
	
	$arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", "ID"=>$_REQUEST["ids"]);
	$res = CIBlockElement::GetList(Array("PROPERTY_rest.NAME"=>"ASC","date_active_from"=>"ASC"), $arFilter, false, false, false);
	while($ob = $res->GetNextElement()){ 
 		$arFields = $ob->GetFields();  
 		$arFields["PROPERTIES"] = $ob->GetProperties();
 		
 		$ORDERS[]=$arFields;
 		$CO++;
 	}
 	
 	
 	$FILE_NAME2="otchet.xls";
 	
 	$NOW_REST="";
 
 	$i=3;
 	if($CO==0){
 		$EXIT["message"]="not_orders";
 	}else{
 		$ALL_SUMM=0;
 		foreach($ORDERS as $key=>$O){
 	
 			//Ресторан
 			$res_rest = CIBlockElement::GetByID($O["PROPERTIES"]["rest"]["VALUE"]);
			if($ob_rest = $res_rest->GetNextElement()){
				$arFields_rest = $ob_rest->GetFields();  
 			}else{
 				$arFields_rest["NAME"]="подбор";
 			}
 			
 			if($NOW_REST=="") $NOW_REST = $arFields_rest["NAME"];
 			
 			if($NOW_REST!=$arFields_rest["NAME"]){
				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $REST_SUMM);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $REST_PROCENT);
				
				$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
				
 				$REST_SUMM=0;
 				$REST_PROCENT=0;
 				$NOW_REST=$arFields_rest["NAME"];
 				
 				$i++;
 			}
 			
 			//Клиент
 			$res_cl = CIBlockElement::GetByID($O["PROPERTIES"]["client"]["VALUE"]);
			if($ob_cl = $res_cl->GetNextElement()){
				$arFields_cl = $ob_cl->GetFields();
				$arProps_cl = $ob_cl->GetProperties();  
 			}
 			
 			$tar=explode(" ", $O["ACTIVE_FROM"]);
 			$tar2=explode(":", $tar[1]);
	 		$DATE = $tar[0];
 			$TIME = $tar2[0].":".$tar2[1];
 			
 			if($TIME==":") $TIME="";
 			
 			$tar=explode(" ", $O["DATE_CREATE"]);
 			$O["DATE_CREATE"] = $tar[0];
 			
 			$rsUser = CUser::GetByID($O["CREATED_BY"]);
			$arUser = $rsUser->Fetch();
 		
 			$O["CREATED_BY"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];
 			
 			if($O["PROPERTIES"]["dengi"]["VALUE"]=="Y") $dengi="Да";
 			else $dengi="Нет";
 		
 			$objPHPExcel->setActiveSheetIndex(0)
   			->setCellValue('A'.$i, $arFields_rest["NAME"])
   			->setCellValue('B'.$i, $arFields_cl["NAME"])
   			->setCellValue('C'.$i, $O["PROPERTIES"]["status"]["VALUE"])
   			->setCellValue('D'.$i, $DATE)
   			->setCellValue('E'.$i, $TIME)
   			->setCellValue('F'.$i, $O["PROPERTIES"]["guest"]["VALUE"])
   			->setCellValue('G'.$i, $O["PROPERTIES"]["summ_sch"]["VALUE"])
   			->setCellValue('H'.$i, $O["PROPERTIES"]["procent"]["VALUE"]);

			
			$ALL_SUMM+=$O["PROPERTIES"]["summ_sch"]["VALUE"];
			$REST_SUMM+=$O["PROPERTIES"]["summ_sch"]["VALUE"];
			
			$REST_PROCENT+=$O["PROPERTIES"]["procent"]["VALUE"];
			$ALL_PROCENT+=$O["PROPERTIES"]["procent"]["VALUE"];
			
			
			$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
			$i++;	
 		}
 		
 		
 		
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $REST_SUMM);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $REST_PROCENT);
		
		$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
					
 		$objPHPExcel->getActiveSheet()->getStyle('A2:H'.$i)->applyFromArray($styleArray);
 		
 	
 		
 		
 		$i++;
 		
 		$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
 		//Всего заказов
 		$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, "Заказов всего: ".count($ORDERS));
 		
 		
 		//ИТОГОВАЯ СУММА
 		$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getFont()->setBold(true);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $ALL_SUMM);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $ALL_PROCENT);
 		
 		
 		$objPHPExcel->getActiveSheet()->getStyle('A2:H'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
 		$objPHPExcel->getActiveSheet()->getStyle('A2:H'.$i)->getAlignment()->setWrapText(true);
		
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$FILE_NAME2.'"');
		header('Cache-Control: max-age=0');
		$objWriter->save("php://output");
		
	}
 
    echo json_encode($EXIT);
    
    
}

create_report();
?>