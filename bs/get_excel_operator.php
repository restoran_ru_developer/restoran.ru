<?
define("NO_KEEP_STATISTIC", true);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function create_report(){
	if(count($_SESSION["ALL_ORDERS_FLTR"])==0) die("Нет параметров для фильтрации!");
    
	global $USER;
	require_once $_SERVER["DOCUMENT_ROOT"].'/bs/Classes/PHPExcel.php';
  	
//  	if($USER->IsAdmin()){
        $objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"]."/bs/oparator_report.xlsx");
//    }
//    else {
//        $objPHPExcel = PHPExcel_IOFactory::load($_SERVER["DOCUMENT_ROOT"]."/bs/oparator_report.xls");
//    }



	
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(14);
  
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	

	
	
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.2);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.2);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.2);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.2);
	//$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(100);
	
  
    //Делаем у ячеек рамки
	$styleArray = array(
        'borders' => array(
            'inside' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb'=>'FF000000')
            ),
            'outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
            )
        ),
        'font'=> array(
            'size'=> 12
        )
	);
	

	//далее формируем фильтр и выбираем все элементы, которые подпадают под условия
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$ONAME="(";
	$i=4;
	$CO=0;
	
	$arFilter = Array("IBLOCK_TYPE"=>"booking_service","IBLOCK_ID"=>105, "ACTIVE"=>"Y", "SECTION_CODE"=>$_SESSION["CITY"]);
	
	
	//var_dump($_SESSION["ALL_ORDERS_FLTR"]);
	$arFilter = array_merge($arFilter, $_SESSION["ALL_ORDERS_FLTR"]);

    //  получение связанного ресторана, добавление в фильтр
    $res = CIBlockElement::GetByID($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]);
    if($ob = $res->GetNextElement()){
        $additional_report_rest_ids_arr = $ob->GetProperty('SAME_NOT_ACTIVE_REST');
    }
    if($additional_report_rest_ids_arr['VALUE']>0){
        $additional_report_rest_ids_arr['VALUE'][] = $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"];
        $arFilter["PROPERTY_rest"] = $additional_report_rest_ids_arr['VALUE'];
    }

	//СТАТУС
	if($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]>0 && $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]){
		$db_enum_list = CIBlockProperty::GetPropertyEnum("status", Array(), Array("IBLOCK_ID"=>105, "ID"=>$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_status"]));
		if($ar_enum_list = $db_enum_list->GetNext()){
  			$ONAME.=$ar_enum_list["VALUE"].",";
		}
	} 
	
	//РЕСТОРАН
	if($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]>0 && $_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]){
		$res_rest = CIBlockElement::GetByID($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]);
		if($ob_rest = $res_rest->GetNextElement()){
			$arFields_rest = $ob_rest->GetFields();  
 		}
 		
 		$ONAME.=$arFields_rest["NAME"].",";	
	} 
	
	//ОПЕРАТОР
	if($_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]>0 && $_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]){
		
		$rsUser = CUser::GetByID($_SESSION["ALL_ORDERS_FLTR"]["CREATED_BY"]);
		$OPERATOR = $rsUser->Fetch();
		
		$ONAME.=$OPERATOR["LAST_NAME"]." ".$OPERATOR["NAME"].",";
	} 
	
	
	if($_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]!="" && $_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]){		
		$ONAME.=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]."-".date( 'd.m.Y' ,strtotime('-1 day', strtotime($_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]))).",";
	} 
	
	
	
	
	if($_REQUEST["date_s2"]!=$_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"] && $_REQUEST["date_s2"]!="") $_SESSION["ALL_ORDERS_FLTR"][">=DATE_CREATE"]=$_REQUEST["date_s2"];
	if($_REQUEST["date_po2"]!=$_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"] && $_REQUEST["date_po2"]!="") $_SESSION["ALL_ORDERS_FLTR"]["<=DATE_CREATE"]=$_REQUEST["date_po2"];


    $arSelect=array("ID",'PROPERTY_source',"PROPERTY_rest","PROPERTY_client","ACTIVE_FROM","DATE_CREATE","CREATED_BY","PROPERTY_summ_sch","PROPERTY_podtv","PROPERTY_prinyal","PROPERTY_guest","PROPERTY_dengi",'PROPERTY_procent');
    $res = CIBlockElement::GetList(Array("date_active_from"=>"ASC"), $arFilter, false, false, $arSelect);//
    while($arFields = $res->Fetch()){
        $ORDERS[]=$arFields;
        $CO++;
    }
//
//
//    }
//    else {
//        //source
//        $arSelect=array("ID",'PROPERTY_source',"PROPERTY_rest","PROPERTY_client","ACTIVE_FROM","DATE_CREATE","CREATED_BY","PROPERTY_summ_sch","PROPERTY_podtv","PROPERTY_prinyal","PROPERTY_guest","PROPERTY_dengi");
//        $res = CIBlockElement::GetList(Array('property_source'=>"ASC","date_active_from"=>"ASC","PROPERTY_rest.NAME"=>"ASC"), $arFilter, false, false, $arSelect);//
//        while($arFields = $res->Fetch()){
//            //$arFields = $ob->GetFields();
//            //$arFields["PROPERTIES"] = $ob->GetProperties();
//
//            $ORDERS[]=$arFields;
//            $CO++;
//        }
//    }

 	
 	$ONAME.=")";
 	$ONAME = str_replace(",)", ")", $ONAME);

 	if($ONAME=="()") $ONAME="";
 

 	
 	if($CO==0){
 		$EXIT["message"]="not_orders";
 	}else{
 		$ALL_SUMM=0;
 		$ALL_PERCENT_SUMM=0;
 		foreach($ORDERS as $order_key=>$O){

 			//Ресторан
 			$res_rest = CIBlockElement::GetByID($O["PROPERTY_REST_VALUE"]);
			if($ob_rest = $res_rest->GetNextElement()){
				$arFields_rest = $ob_rest->GetFields();
				$arFields_rest["PROPERTIES"] = $ob_rest->GetProperties();  
				$PROCENT = $arFields_rest["PROPERTIES"]["procent"]["VALUE"];
 			}else{
 				$arFields_rest["NAME"]="подбор";
 			}
 			
 			//Клиент
 			$res_cl = CIBlockElement::GetByID($O["PROPERTY_CLIENT_VALUE"]);
			if($ob_cl = $res_cl->GetNextElement()){
				$arFields_cl = $ob_cl->GetFields();
				$arProps_cl = $ob_cl->GetProperties();  
 			}
 			
 			$tar=explode(" ", $O["ACTIVE_FROM"]);
 			$tar2=explode(":", $tar[1]);
	 		$DATE = $tar[0];
 			$TIME = $tar2[0].":".$tar2[1];
 			
 			if($TIME==":") $TIME="";
 			
 			$tar=explode(" ", $O["DATE_CREATE"]);
 			$O["DATE_CREATE"] = $tar[0];
 			
 			$rsUser = CUser::GetByID($O["CREATED_BY"]);
			$arUser = $rsUser->Fetch();
 		
 			$O["CREATED_BY"] =$arUser["LAST_NAME"]." ".$arUser["NAME"];
 			
 			if($O["PROPERTY_DENGI_VALUE"]=="Y") $dengi="Да";
 			else $dengi="Нет";

            $ADDR = str_replace("Москва, ","", $arFields_rest["PROPERTIES"]['address']['VALUE'][0]);
            $ADDR = str_replace("Санкт-Петербург, ","", $ADDR);

 			$objPHPExcel->setActiveSheetIndex(0)
   			->setCellValue('A'.$i, $arFields_rest["NAME"].' ('.$ADDR.')')
   			->setCellValue('B'.$i, trim($arFields_cl["NAME"]))
   			->setCellValue('C'.$i, $DATE)
   			->setCellValue('D'.$i, $TIME)
   			->setCellValue('E'.$i, $O["PROPERTY_GUEST_VALUE"])
   			->setCellValue('F'.$i, $O["PROPERTY_SUMM_SCH_VALUE"])
   			->setCellValue('G'.$i, $O["PROPERTY_PRINYAL_VALUE"])
   			->setCellValue('H'.$i, $O["PROPERTY_PODTV_VALUE"])
   			->setCellValue('I'.$i, $O["PROPERTY_SOURCE_VALUE"])
   			->setCellValue('J'.$i, $O["PROPERTY_PROCENT_VALUE"]);

			
			$ALL_SUMM+=	$O["PROPERTY_SUMM_SCH_VALUE"];
            $ALL_PERCENT_SUMM+=	intval($O["PROPERTY_PROCENT_VALUE"]);
			/*
			$COLOR="";
			
			if($O["PROPERTIES"]["status"]["VALUE"]=="заказ принят") $COLOR = "FFfbf205";
			if($O["PROPERTIES"]["status"]["VALUE"]=="забронирован") $COLOR = "FFB5F562";
			if($O["PROPERTIES"]["status"]["VALUE"]=="гости пришли") $COLOR = "FF71C207";
			if($O["PROPERTIES"]["status"]["VALUE"]=="гости отменили заказ") $COLOR = "FFfb2d05";
			if($O["PROPERTIES"]["status"]["VALUE"]=="ошибочный заказ") $COLOR = "FF3b3b3b";
			if($O["PROPERTIES"]["status"]["VALUE"]=="банкет в работе") $COLOR = "FFFBFB61";
			if($O["PROPERTIES"]["status"]["VALUE"]=="внесена предоплата") $COLOR = "FFFBFB61";
			if($O["PROPERTIES"]["status"]["VALUE"]=="заказ оплачен") $COLOR = "FF48C7EF";
	
			
			
			if($COLOR!=""){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getFill()->getStartColor()->setARGB($COLOR);
			}
			*/
			$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
			$i++;	
 		}
 		
 		$i--;
 		$objPHPExcel->getActiveSheet()->getStyle('A3:J'.$i)->applyFromArray($styleArray);
 		$objPHPExcel->getActiveSheet()->getStyle('A3:J'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
 		
 		$objPHPExcel->getActiveSheet()->getStyle('A3:J'.$i)->getAlignment()->setWrapText(true);
 		
 		
 		//$objPHPExcel->getActiveSheet()->setBreak( 'A3:H'.$i , PHPExcel_Worksheet::BREAK_COLUMN );
 		
 		$i++;
 		
 		$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
 		//Всего заказов
 		$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, "Заказов всего: ".count($ORDERS));
 		
 		
 		//ИТОГОВАЯ СУММА
 		$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getFont()->setBold(true);
 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $ALL_SUMM);

        if($PROCENT=="") $PROCENT=10;
        $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getFont()->setBold(true);
//        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, ceil($ALL_SUMM/100*$PROCENT).' '."(".$PROCENT."%)");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, ceil($ALL_PERCENT_SUMM));



        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->applyFromArray(array('font'=> array('size'=> 11)));

//        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);

// 		$i++;
 		
 		//ПРОЦЕНТЫ

 		
// 		$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getFont()->setBold(true);
// 		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, "(".$PROCENT."%)");
 		

 		
		$FILE_NAME2 = $_SESSION["ALL_ORDERS_FLTR"][">=DATE_ACTIVE_FROM"]." - ".$_SESSION["ALL_ORDERS_FLTR"]["<=DATE_ACTIVE_FROM"]." ".$ONAME;
		if($_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"]!="") $FILE_NAME2.=" ".$_SESSION["ALL_ORDERS_FLTR"]["PROPERTY_rest"];
		
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$FILE_NAME2.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save("php://output");

		
	}
 
    echo json_encode($EXIT);
    
    
}

create_report();
?>