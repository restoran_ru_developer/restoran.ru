<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Служба бронирования");
?>

<?
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
	

if($arUser["UF_WIN_POS"]!=""){
	$WIN_POS = unserialize($arUser["UF_WIN_POS"]);
}


?>

<?if(!CSite::InGroup(array(36))):?>
<?$APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include_areas/window1.php"),
	Array("POS"=>$WIN_POS["w1"]),
	Array("MODE"=>"php")
);?>
<?endif?>

<?$APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include_areas/window2.php"),
	Array("POS"=>$WIN_POS["w2"]),
	Array("MODE"=>"php")
);?>

<?if(!CSite::InGroup(array(36))):?>
<?$APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include_areas/window3.php"),
	Array("POS"=>$WIN_POS["w3"], "REST_INFO"=>$WIN_POS["rest_info"]),
	Array("MODE"=>"php")
);?>
<?endif?>

<?if(!CSite::InGroup(array(36))):?>
<?$APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("include_areas/window4.php"),
	Array("POS"=>$WIN_POS["w4"]),
	Array("MODE"=>"php")
);?>
<?endif?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>