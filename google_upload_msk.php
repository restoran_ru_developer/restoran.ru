<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

CModule::IncludeModule("iblock");

$lastEl = false;
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);
$lastElID = ($_REQUEST["lastElID"] ? $_REQUEST["lastElID"] : 0);
//$curIblockID = ($_REQUEST["curIblockID"] ? $_REQUEST["curIblockID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// start execution
$time_start = microtime_float();

// set last el id
if($lastElID <= 0) {
    /*
	$rsIblock = CIBlock::GetList(
		Array("ID" => "ASC"),
		Array(
			"SITE_ID" => "s1",
			"TYPE" => "catalog",
			">ID" => $curIblockID		
		),
		false
	);
	while($arIblockLast = $rsIblock->Fetch()) {
    */
    $rsLastEl = CIBlockElement::GetList(
        Array("ID"=>"DESC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 11,
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
            "!PROPERTY_google_photo" => false
        ),
        false,
        false,
        Array()
    );
    if($arLastEl = $rsLastEl->GetNext())
        $lastElID = $arLastEl["ID"];
    //}
}

if($curStepID == 0 /*&& $curIblockID <= 0*/) {
    file_put_contents("google_upload_msk.xml", '', LOCK_EX);

    $str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    $str .= "<listings xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://local.google.com/local_feed.xsd\">\n";

        $str .= "\t<language>ru</language>\n";
        $str .= "\t<datum>WGS84</datum>\n";
}
/*
$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
		//">ID" => $curIblockID
        "ID" => 11
	),
	false
);
*/

//while($arIblock = $rsIblock->Fetch()) {
	//$curStepID = 0;

	$rsRest = CIBlockElement::GetList(
		Array("ID" => "ASC"),
		Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => 11,
			">ID" => $curStepID,
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
			"!PROPERTY_google_photo" => false
		),
		false,
		false,
		Array("ID", "NAME", "DETAIL_PAGE_URL", "DATE_CREATE", "PROPERTY_*")
	);
	
	while($arRest = $rsRest->GetNext()) {
		$curStepID = $arRest["ID"];
		//$curIblockID = $arRest["IBLOCK_ID"];
	
		// get photo
		$photoKey = 0;
		$strPhoto = '';
		$rsProp = CIBlockElement::GetProperty(
			$arRest["IBLOCK_ID"],
			$arRest["ID"],
			Array(),
			Array(
				"CODE" => "google_photo"
			)
		);
		while($arProp = $rsProp->GetNext()) {
			if(intval($arProp["VALUE"]) > 0) {
				$arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array('width' => 2000, 'height' => 1500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	
				$strPhoto .= "\t\t\t<image type=\"photo\" url=\"http://".SITE_SERVER_NAME.$arPhoto["src"]."\" width=\"".$arPhoto["width"]."\" height=\"".$arPhoto["height"]."\">\n";
                $strPhoto .= "\t\t\t\t<link>http://".SITE_SERVER_NAME.$arRest["DETAIL_PAGE_URL"]."</link>\n";
				$strPhoto .= "\t\t\t\t<title>Restaurant inside</title>\n";
				$strPhoto .= "\t\t\t\t<author>Restoran.ru</author>\n";
				$strPhoto .= "\t\t\t</image>\n";
	
				$photoKey++;
				if($photoKey > 3)
					break;
			}
		}
	
		if($photoKey > 0) {
	
			// tmp date
			$arTmpDate = explode(' ', $arRest["DATE_CREATE"]);
			$tmpDate = explode('.', $arTmpDate[0]);
	
			$str .= "\t<listing>\n";
				$str .= "\t\t<id>".$arRest["ID"]."</id>\n";
				$str .= "\t\t<name>".htmlspecialchars($arRest["NAME"])."</name>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "address"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"]) {
						$address = str_replace($arIblock["NAME"].",", "", $arProp["VALUE"]);
						$address = $arIblock["NAME"].", ".$address;
						$str .= "\t\t<address>".$address."</address>\n";
					}	
	
				$str .= "\t\t<country>RU</country>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lat"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<latitude>".$arProp["VALUE"]."</latitude>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lon"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<longitude>".$arProp["VALUE"]."</longitude>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "phone"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<phone type=\"main\">".$arProp["VALUE"]."</phone>\n";
	
				$str .= "\t\t<category>Restaurants</category>\n";
				$str .= "\t\t<date month=\"".$tmpDate[1]."\" day=\"".$tmpDate[0]."\" year=\"".$tmpDate[2]."\"/>\n";
				$str .= "\t\t<content>\n";
	
				$str .= $strPhoto;
	
					$str .= "\t\t\t<attributes>\n";
						$str .= "\t\t\t\t<title>Restaurant Details</title>\n";
						$str .= "\t\t\t\t<author>Restoran.ru</author>\n";
						$str .= "\t\t\t\t<email>redaktor@restoran.ru</email>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "site"
							)
						);
						if($arProp = $rsProp->GetNext())
							if($arProp["VALUE"]) {
                                if(strstr($arProp["VALUE"], ','))
                                    $tmpWebsite = explode(",", $arProp["VALUE"]);
                                elseif(strstr($arProp["VALUE"], ';'))
                                    $tmpWebsite = explode(";", $arProp["VALUE"]);
                                else
                                    $tmpWebsite[0] = $arProp["VALUE"];

                                $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);

                                $str .= "\t\t\t\t<website>"."http://".$tmpWebsite[0]."</website>\n";
								//$str .= "\t\t\t\t<website>".$arProp["VALUE"]."</website>\n";
                            }

						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "opening_hours_google"
							)
						);
						if($arProp = $rsProp->GetNext())
							if($arProp["VALUE"]) {
                                // replace russian characters
                                //$ar1 = Array('С', 'с', ' до ', ' до последнего посетителя', ' до утра', 'Пт.', 'пт.', 'пт', 'Пт', 'Пятница', 'пятница', 'пятницу', 'сб.', 'Сб.', 'Сб', 'сб', 'СБ', 'суббота', 'суббота', ' (до последнего посетителя)', ' до последнего гостя.', 'Круглосуточно', 'Пн.', 'пн.', 'пн', 'Пн', 'Понедельник', 'понедельник', 'понедельника', 'вс.', 'Вс.', 'вс', 'Вс', 'Воскресенье', 'ВС', 'в воскресенье', 'завтраки с ', ' и', ' (или до последнего клиента)', 'чт.', 'Чт.', 'чт', 'четверг', 'Четверг', 'ЧТ', '(до последнего гостя)', ': до ', ', пивной бар работает круглосуточно', ' (до последнего клиента)', ' (кроме праздников)', 'Газетный пер.: ', 'Будни, кроме субботы, воскресенья и праздничных дней с ', 'Ресторан-веранда: ', 'ср.', 'ср', 'Ср.', 'Ср', 'СР', 'праздничные дни', 'Ресторан', 'Будни: ', '24 часа', "'Гарден' - 7:00-23:00. 'Биргарден' - с 13:00 до последнего гостя. 'АРТ Лобби-бар' и банкетные залы - круглосуточно", 'Real food Restaurant - ', ' (вход до 23.30).', ' или дольше по желанию гостя', 'Ежедневно, ', 'круглосуточно', ' – до последнего гостя', ' (возможно продление)', ' ежедневно', 'Вт', 'Вт.', 'вт', 'вт.', 'Вторник', 'вторник', 'Бар – ', 'Radio City Sport bar: ', 'До последнего клиента', ' утра', ' - до ', ', возможен технический перерыв 6:00 до 9:00', '1. ', ' по ', 'Ежедневно ', ' Под банкеты: до 00.00', 'Ресторан: 24 часа; Караоке-клуб: с 20.00 до 07.00', ' – ежедневно', '1) ', ' Работаем до последнего гостя', ' до первых петухов', 'Будни и воскресенье с ', 'бар', '&lt;p&gt;Ресторан: ', ' Бильярд&lt;strong&gt;:&lt;/strong&gt; вс.-чт. с 14.00 до 02.00; пт.-сб. с 14.00 до 05.00&lt;/p&gt;', 'Банкеты и вечеринки до последнего гостя', ' (на банкеты)', 'Ресторан:', 'Банкеты всю ночь', ' (в летнее время до 21.00).', '; Летняя веранда - ежедневно с 12.00 до 23.00', '. Суббота, воскресенье в банкетном режиме', ' (ул. Новый Арбат);', ' Вс., пн. - выходной', 'вс. - пн. по предзаказу', 'Тверская: ', 'Ресторация', '. Выходные 10:00 — 24:00', 'По заказу клиента', 'В обычном режиме:', 'Каждый день', 'На банкеты - круглосуточно.', '- до последнего', 'до последнего клиента', 'Кафе - круглосуточно, ресторан:');
                                //$ar2 = Array('From', 'From', ':', '', '', 'Fri', 'Fri', 'Fri', 'Fri', 'Fri', 'Fri', 'Fri', 'Sat', 'Sat', 'Sat', 'Sat', 'Sat', 'Sat', 'Sat', '', '', 'around the clock', 'Mon', 'Mon', 'Mon', 'Mon', 'Mon', 'Mon', 'Mon', 'Sun', 'Sun', 'Sun', 'Sun', 'Sun', 'Sun', 'Sun', 'Breakfast', '', '', 'Thu', 'Thu', 'Thu', 'Thu', 'Thu', 'Thu', '', ':', '', '', '', '', '', '', 'Wed', 'Wed', 'Wed', 'Wed', 'Wed', 'holidays', '', 'weekdays: ', 'around the clock', 'around the clock', '. Бар - круглосуточно', '', '', '', 'around the clock', '', '', '', 'Tue', 'Tue', 'Tue', 'Tue', 'Tue', 'Tue', '', '', 'around the clock', '', ':', '', '', ' and ', '', '', 'around the clock', '', '', '', '', '', 'bar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'around the clock', '', '', '', '', '', '');
                                //$arProp["VALUE"] = str_replace($ar1, $ar2, $arProp["VALUE"]);
                                $arProp["VALUE"] = preg_replace ("/\b(с)\b/i", "", $arProp["VALUE"]);
                                $arProp["VALUE"] = preg_replace ("/\b(С)\b/i", "", $arProp["VALUE"]);
                                $arProp["VALUE"] = preg_replace ("/\b(до)\b/i", ":", $arProp["VALUE"]);

								$str .= "\t\t\t\t<attr name=\"Hours\">".$arProp["VALUE"]."</attr>\n";
                            }
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "credit_cards"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->GetNext())
								$str .= "\t\t\t\t<attr name=\"Payment accepted\">".$arPayment["NAME"]."</attr>\n";
						}
	
						//$str .= "\t\t\t\t<attr name=\"Attire\">Casual</attr>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kolichestvochelovek"
							)
						);
						if($arProp = $rsProp->GetNext())
							$rsSeating = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arSeating = $rsSeating->GetNext())
								$str .= "\t\t\t\t<attr name=\"Seating\">".str_replace("&nbsp;", "", $arSeating["NAME"])."</attr>\n";
	
						//$str .= "\t\t\t\t<attr name=\"Phone Orders\">No</attr>\n";
						$str .= "\t\t\t\t<date month=\"".$tmpDate[1]."\" day=\"".$tmpDate[0]."\" year=\"".$tmpDate[2]."\"/>\n";
					$str .= "\t\t\t</attributes>\n";
	
				$str .= "\t\t</content>\n";
			$str .= "\t</listing>\n";
	
			v_dump($arRest["IBLOCK_ID"]);
			v_dump($arRest["ID"]);
	
		}
	
		// check last element id
		if($lastElID == $arRest["ID"]) {
			$str .= "</listings>";
			$lastEl = true;
		}
	
		// execution time
		$time_end = microtime_float();
		$time = $time_end - $time_start;
		if($time > 30) {
			file_put_contents("google_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);
	
			echo "<script>setTimeout(function() {location.href = '/google_upload_msk.php?curStepID=".$curStepID."&lastElID=".$lastElID."&curIblockID=".$curIblockID."'}, 5000);</script>";
			break;
		}
	
		if($lastEl) {
			v_dump("Last EL");
			file_put_contents("google_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);
		}
	}
//}
?>
