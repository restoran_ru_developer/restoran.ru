<!DOCTYPE html>
<html style="background:#0089e7 url(/globo/kupon2.jpg) top center no-repeat;">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0" />
  		<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
        <style>
	@font-face{font-family:'appetite';src:url('/tpl/fonts/appetite-italic-webfont.eot');src:url('/tpl/fonts/appetite-italic-webfont.eot?#iefix') format('embedded-opentype'),
url('/tpl/fonts/appetite-italic-webfont.woff') format('woff'),
url('/tpl/fonts/appetite-italic-webfont.ttf') format('truetype'),
url('/tpl/fonts/appetite-italic-webfont.svg#appetite') format('svg');font-weight:normal;font-style:normal}
</style>
        <title>Бесплатный коктейль "Ресторан.ру" от ресторана Globo</title>
            </head>
<body style="text-align:center;">
<p style="font-family: appetite; font-size: 22px; color: #fff; text-shadow: 1px 1px 1px rgba(0, 0, 0, 1.0);">Лето с Globo и Ресторан.ру</p>
<p style="font-family: appetite; font-size: 16px; color: #fff; text-shadow: 1px 1px 1px rgba(0, 0, 0, 1.0);">Покажи официанту или бармену эту страничку и получи бесплатный коктейль "Ресторан.ру"*</p>
<p style="font-family: appetite; font-size: 12px; color: #fff; text-shadow: 1px 1px 1px rgba(0, 0, 0, 1.0);">* но только 1 коктейль в одни руки ;)</p>
<p style="font-family: appetite; font-size: 18px; color: #fff; text-shadow: 1px 1px 1px rgba(0, 0, 0, 1.0); position:absolute; bottom:0;">Дата: <?php echo date("d F Y"); ?></p>
</body>
</html>