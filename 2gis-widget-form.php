<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
<!DOCTYPE html>
<html lang="en" style="min-height:inherit">
<head>
    <link href="http://fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>

    <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/selectivizr-1.0.2/selectivizr-min.js"></script>
    <link href="<?=SITE_TEMPLATE_PATH?>/cap-ie-style.css" rel="stylesheet">
    <script>
        $(function(){
            $('.cap-wrapper').css('display','block');
            $('.cap-overlay').css('display','block');
            $('.container').remove();
        });
    </script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script>
        $(function(){
            $("*:first-child").addClass("firstChild");
        });
    </script>
    <![endif]-->

    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>

    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?$APPLICATION->ShowHead()?>

    <?$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);?>

    <link href="<?=SITE_TEMPLATE_PATH?>/css/2gis-widget.css"  type="text/css" rel="stylesheet" />
</head>
<body class="gis-form">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-50714926-1', '2gis.com');
        ga('send', 'pageview');

    </script>
<!--    <div class="modal fade in --><?//=$_REQUEST['CITY_ID']?><!--" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog" aria-hidden="false" style="display: block;" >-->
<!--        -->
<!--    </div>-->
    <div class="modal-dialog gis-widget">
        <div class="modal-content">

            <div class="modal-body">
                <noindex>
                    <?if (CITY_ID=="ast"):
                        ShowError("Услуга временно не работает");
                        die;
                    endif;?>
                    <?
                    global $USER;
                    $APPLICATION->IncludeComponent(
                        ($_REQUEST['CITY_ID']=='urm'||$_REQUEST['CITY_ID']=='rga')?"bitrix:form.result.new2":"restoran:form.result.new",
                        //($USER->IsAdmin())?"ajax_form_bron_rest_sms_new":"ajax_form_bron_rest_sms",
//                            ($_REQUEST["banket"]=="Y")?"banket_2014":"order_2014",
                        "order_2014",
                        Array(
                            "SEF_MODE" => "N",
                            "WEB_FORM_ID" => "3",
                            "LIST_URL" => "",
                            "EDIT_URL" => "",
                            "SUCCESS_URL" => "",
                            "CHAIN_ITEM_TEXT" => "",
                            "CHAIN_ITEM_LINK" => "",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS" => "N",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "3600",
                            "MY_CAPTCHA" => "Y",
                            "VARIABLE_ALIASES" => Array(
                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                "RESULT_ID" => "RESULT_ID"
                            )
                        ),
                        false
                    );?>
                </noindex>
            </div>
        </div>
    </div>
</body>
</html>