<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
@define("ERROR_404", "Y");
CHTTP::SetStatus("404 Not Found");
//header("Content-type: application/xml; charset=utf-8");

//CModule::IncludeModule("iblock");


//CModule::IncludeModule("search");
//$strTime = TimeEncodeZeroZone(time()+2*60);
//$strTime = CSiteMap::TimeEncode(time()+60*2);



//print_r($strTime);
//CModule::IncludeModule("sozdavatel.sms");
//$message = 'тест 3';
//if(CSMS::Send($message, '79523969381', "UTF-8",false,$strTime)){
//    echo 'отправили';
//}
//else {
//    echo 'не отправили';
//}


//$arFile = CFile::GetFileArray(1234177);
//print_r($arFile);


//$ctx = stream_context_create(array(
//        'http' => array(
//            'timeout' => 1
//        )
//    )
//);

//$ip = getUserIP();

//print_r($ip);
//$city = $city[0];

//$ip = "77.38.220.50";
//$urm_array = array(77,38);
//$search_in = explode('.',$ip);
//print_r($search_in);
//if($search_in[0]==$urm_array[0] && $search_in[1]==$urm_array[1]){
//    echo 'this is urmala';
//}



//getSetStreets('restaurants');
//getSetStreets('banket');
//$arIB = getArIblock("catalog",CITY_ID);
//$db_props = CIBlockElement::GetProperty(12, 1820018, array("sort" => "asc"), Array("CODE"=>"user_bind"));
//if($ar_props = $db_props->Fetch()){
//
//    $restorator_obj = CUser::GetByID($ar_props['VALUE']);
//    if($restorator_email_arr = $restorator_obj->Fetch()){
//        print_r($restorator_obj);
//    }
//}


//
//$res = CIBlockElement::GetByID(385039);
//if($ar_res = $res->GetNext()){
//    print_r($ar_res);
//}

/*INNER JOIN ( SELECT DISTINCT BSE.IBLOCK_ELEMENT_ID FROM b_iblock_section_element BSE */

/*INNER JOIN b_iblock_section BS ON BSE.IBLOCK_SECTION_ID = BS.ID WHERE ((BS.ID IN (83659))) ) */


function getSetStreets($section_code){
    $catalog_iblock_id_temp = getArIblock("catalog",'spb');
    $street_iblock_id_temp = getArIblock("streets",'spb');

    $catalog_iblock_id = $catalog_iblock_id_temp['ID'];
    $street_iblock_id = $street_iblock_id_temp['ID'];

    $arFilter = Array('IBLOCK_ID'=>$street_iblock_id, 'CODE'=>"$section_code");
    $db_list = CIBlockSection::GetList(Array(), $arFilter, false, array('ID'));  //  берем id текущего раздела в улицах
    if($ar_result = $db_list->Fetch()){

//        if(!$_REQUEST['iNumPage']){
//            $iNumPage = 1;
//        }
//        else {
//            $iNumPage = $_REQUEST['iNumPage'];
//        }

        $el = new CIBlockElement;

        $arSelect = Array("ID",'NAME','IBLOCK_ID');
        $arFilter = Array("IBLOCK_ID" => $catalog_iblock_id, "ACTIVE" => "Y",'SECTION_CODE'=>$section_code,'PROPERTY_STREET'=>false);
        $res = CIBlockElement::GetList(Array('NAME' => 'ASC'), $arFilter, false, false, $arSelect);//Array("nPageSize" => 100,'iNumPage' => $iNumPage)
        while ($ob = $res->Fetch()) {
            $db_props = CIBlockElement::GetProperty($catalog_iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"lat"));
            if($ar_props = $db_props->Fetch())
                $ob['MY_PROPS']['lat'] = $ar_props["VALUE"];
            $db_props = CIBlockElement::GetProperty($catalog_iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"lon"));
            if($ar_props = $db_props->Fetch())
                $ob['MY_PROPS']['lon'] = $ar_props["VALUE"];
            $db_props = CIBlockElement::GetProperty($catalog_iblock_id, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch())
                $ob['MY_PROPS']['address'] = $ar_props["VALUE"];

            if($steet_name = Geocode::yaGeocode($ob['MY_PROPS']['lon'],$ob['MY_PROPS']['lat'],$ob['MY_PROPS']['address'])) {
//                'SECTION_CODE'=>$section_code
                $arParams = array("replace_space"=>"-","replace_other"=>"-");
                $street_translit = Cutil::translit($steet_name,"ru",$arParams);
                $street_res = CIBlockElement::GetList(Array('NAME' => 'ASC'), array('IBLOCK_ID'=>$street_iblock_id,array("LOGIC" => "OR","NAME"=>$steet_name,'CODE'=>$street_translit)), false, Array("nTopCount" => 1), array('ID'));
                if ($street_ob = $street_res->Fetch()) {

                    // добавить к группе улицу, если не в текущей группе
                    $db_old_groups = CIBlockElement::GetElementGroups($street_ob['ID'], true, array('CODE','ID'));
                    $this_group_code = array();
                    while($ar_group = $db_old_groups->Fetch()){
                        $this_group_code['CODE'][] = $ar_group['CODE'];
                        $this_group_code['ID'][] = $ar_group['ID'];
                    }


                    if(!in_array($section_code,$this_group_code['CODE'])){
                        CIBlockElement::SetElementSection($street_ob['ID'], array($this_group_code['ID'][0],$ar_result['ID']));
                    }
                    //  установить улицу элементу каталога
                    CIBlockElement::SetPropertyValuesEx($ob['ID'], $catalog_iblock_id, array('STREET' => $street_ob['ID']));
                }
                else {
                    //  добавить и установить элементу каталога

                    $arLoadProductArray = Array(
                        "IBLOCK_SECTION" => $ar_result['ID'],
                        "IBLOCK_ID"      => $street_iblock_id,
                        "NAME"           => "$steet_name",
                        'CODE'           => Cutil::translit($steet_name,"ru",$arParams),
                        "ACTIVE"         => "Y"
                    );

                    if($PRODUCT_ID = $el->Add($arLoadProductArray)){
                        CIBlockElement::SetPropertyValuesEx($ob['ID'], $catalog_iblock_id, array('STREET' => $PRODUCT_ID));
                    }
                    else
                        echo "Error: ".strip_tags($el->LAST_ERROR).': '.Cutil::translit($steet_name,"ru",$arParams).' - '.$steet_name.'; ресторан: '.$ob['NAME'].'<br>';
                }
            }
            else {
                echo $ob['NAME'].': no yandex street - '.$ob['ID'].'<br>';
            }
        }

//        if ($res->NavPageCount!=$iNumPage && $iNumPage<25) {
//            LocalRedirect('/citylinks-test.php?iNumPage='.++$iNumPage, true);
//        }
//        else {
            echo '!_finished_!';
//        }
    }
    else {
        die();
    }


}

class Geocode {
    static function yaGeocode($lat,$lon,$steet_name=''){
        if(!$lat||!$lon && $steet_name!=''){
            $answer = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&results=1&kind=street&geocode='.$steet_name),true);
        }
        elseif($lat && $lon) {
            $answer = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?format=json&results=1&kind=street&geocode='.$lat.','.$lon),true);       //  обратное геокодирование
        }

        if($answer['response']['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found']){
            if($answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName']){
                return $answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName'];
            }
            elseif($answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName']) {
                return $answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['Thoroughfare']['ThoroughfareName'];
            }
            elseif($answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['Thoroughfare']['ThoroughfareName']) {
                return $answer['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']['Thoroughfare']['ThoroughfareName'];
            }
        }
        else {
            return false;
        }
    }
}


//RestIBlock::newsXMLNow('spb');
//  DIRECT адреса второго уровня
//CModule::IncludeModule("iblock");
//$arIB = getArIblock("catalog", 'msk');
//
//$arSelect = Array("ID", "IBLOCK_ID", "NAME", "CODE");
//$arFilter = Array("IBLOCK_ID"=>$arIB['ID']);
//$res = CIBlockElement::GetList(Array('ID'=>'ASC'), $arFilter, false, false, $arSelect);
//echo $res->SelectedRowsCount();
//while($ob = $res->Fetch()){
//
//    $correct_code = preg_replace('/_/','-',$ob['CODE']);
//
//    echo $ob['NAME'].': '.$ob['CODE'].'@@@'.$correct_code.'<br>';
//
//    CIBlockElement::SetPropertyValuesEx($ob['ID'], $arIB['ID'], array('LIKE_CODE_FIELD' => $correct_code));
//}

//
//CModule::IncludeModule('iblock');
//$arSelect = Array("ID");
//$arFilter = Array("IBLOCK_ID" => 11, "ACTIVE" => "Y", 'PROPERTY_STREET'=>false);
//$res = CIBlockElement::GetList(Array('NAME' => 'ASC'), $arFilter, false, false, $arSelect);
//while ($ob = $res->Fetch()) {
//    $some[] =  $ob['ID'].'<br>';
//}
//echo count($some);//391 433



//TODO  получение старых сетевых
//CModule::IncludeModule('iblock');
//$arSelect = Array("ID",'NAME','IBLOCK_ID');
//$arFilter = Array("IBLOCK_ID" => 12, "ACTIVE" => "Y", 'SECTION_ID'=>103,'!PROPERTY_sleeping_rest_VALUE'=>'Да');//32, 103
//$res = CIBlockElement::GetList(Array('NAME' => 'ASC'), $arFilter, false, false, $arSelect);
//while ($ob = $res->Fetch()) {
//    $db_props = CIBlockElement::GetProperty(12, $ob['ID'], array("sort" => "asc"), Array("CODE"=>"ADDRESS"));
//    while($ar_props = $db_props->Fetch()){
//        if($ar_props['VALUE']){
//            $restorants[$ob['ID']][] = $ob;
//        }
//    }
//}
//foreach($restorants as $rest){
//    if(count($rest)>1){
//        echo $rest[0]['NAME'].'<br>';
////        print_r($rest);
//    }
//}


?>