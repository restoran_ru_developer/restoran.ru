<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--        <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="Restoran.ru">
    <meta property="fb:app_id" content="297181676964377" />
    <meta property="fb:admins" content="100004709941783" />
    <?if (!$_REQUEST["CODE"]):?>
        <meta property="og:image" content="http://www.restoran.ru/images/logo.png" />
    <?endif;?>

    <?if (CITY_ID=="msk"&&$APPLICATION->GetCurDir()=='/'):?>
        <link rel="canonical" href="http://www.restoran.ru/" />
    <?endif?>

    <?if (CITY_ID=="spb"&&$APPLICATION->GetCurDir()=='/'):?>
        <link rel="canonical" href="http://spb.restoran.ru/" />
    <?endif?>

    <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">
    <link href="http://fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>

    <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/selectivizr-1.0.2/selectivizr-min.js"></script>
    <link href="<?=SITE_TEMPLATE_PATH?>/cap-ie-style.css" rel="stylesheet">
    <script>
        $(function(){
            $('.cap-wrapper').css('display','block');
            $('.cap-overlay').css('display','block');
            $('.container').remove();
        });
    </script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script>
        $(function(){
            $("*:first-child").addClass("firstChild");
        });
    </script>
    <![endif]-->

    <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>

    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>
    <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
    <?$APPLICATION->ShowHead()?>
    <?

    $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);

    CModule::IncludeModule("advertising");
    if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
        $page = "others";
    else
        $page = "main";
    CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));
    global $brand1;
    ?>


    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/ny-interiors/style.css");?>

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.js"></script>
    <script type="text/javascript">
        var slider_inf;
        $(".fancy").fancybox({
            afterShow: function() {
                var src = $('.fancybox-image').attr('src');
                var rest_obj_id = $('#'+$('a[href="'+src+'"]').attr('rel'));
                console.log(rest_obj_id);
                if(rest_obj_id){
                    if(rest_obj_id.attr('link-to-all-photos')!=''){
                        $("<div class='rest-inf-in-slider'><div class='rest-slider-name'>"+rest_obj_id.text()+"</div><a class='watch-all-photos-slider-link' href='"+rest_obj_id.attr('link-to-all-photos')+"'>Смотреть все фото ресторана</a></div>").prependTo('.fancybox-inner');
                    }
                    else {
                        $("<div class='rest-inf-in-slider'><div class='rest-slider-name'>"+rest_obj_id.text()+"</div></div>").prependTo('.fancybox-inner');
                    }

                    //<div class="rest-inf-in-slider"></div>
                }
            },
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200,
            'speedOut'		:	200
        });
        $(function(){
            $('.one-news-title-wrap').on('click',function(){
                $(this).prev('.ny-slider-trigger').click();
                return false;
            })
        })
    </script>
</head>
<body>

<div id="panel"><?$APPLICATION->ShowPanel()?></div>

<!--[if lt IE 9]>
<div class="cap-wrapper">
    <div class="cap-top-title">Ваш браузер устарел!</div>
    <div class="cap-text-about">
        Вы пользуетесь устаревшей версией браузера Internet Explorer. Данная версия браузера не поддерживает многие
        современные технологии,<br>из-за чего многие страницы отображаются некорректно
    </div>
    <div class="browser-icons-wrapper">
        <ul>
            <li><a href="http://www.apple.com/safari/download/" target="_blank">safari</a></li>
            <li><a href="http://www.mozilla.com/firefox/" target="_blank">firefox</a></li>
            <li><a href="http://www.opera.com/download/" target="_blank">opera</a></li>
            <li><a href="http://www.google.com/chrome/" target="_blank">chrome</a></li>
        </ul>
    </div>
    <div class="cap-company-logo"></div>
</div>
<div class="cap-overlay"></div>
<![endif]-->

<div class="container">
    <div class="page-header block">
        <div class="phone-wrapper">
            <div class="special-links-wrap">
                <a href="/msk/articles/new_year_night/">Новогодняя ночь</a>
                <a href="/msk/articles/new_year_corp/">Новогодние корпоративы</a>
            </div>
            <span class="city">
                Москва
            </span>
            <span class="detail-phone-wrap">
                +7 (495) <strong>988-26-56</strong>
            </span>
        </div>
        <a class="ny-interiors-restoran-logo" href="/"></a>
        <a class="ny-interiors-center-text" href="/ny-interiors/"></a>
    </div>
    <div class="content">
        <div class="block">
            <div class="ny-news-wrap">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "ny-interiors",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "special_projects",
                        "IBLOCK_ID" => "4851",
                        "NEWS_COUNT" => "300",
                        "SORT_BY1" => "NAME",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => array('REST','PHOTOS'),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "N",
                        "CACHE_TYPE" => $USER->IsAdmin()?'N':"A",
                        "CACHE_TIME" => "36000002",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N"
                    ),
                    false
                );?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&#9587; <!--&times;--></span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="information" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<?
setSeo();
?>


<?if(!$USER->IsAdmin()):?>
    <noindex>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-33504724-1']);
            _gaq.push(['_setDomainName', 'restoran.ru']);


            _gaq.push (['_addOrganic', 'images.yandex.ru', 'text']);
            _gaq.push (['_addOrganic', 'blogs.yandex.ru', 'text']);
            _gaq.push (['_addOrganic', 'video.yandex.ru', 'text']);
            _gaq.push (['_addOrganic', 'mail.ru', 'q']);
            _gaq.push (['_addOrganic', 'go.mail.ru', 'q']);
            _gaq.push (['_addOrganic', 'google.com.ua', 'q']);
            _gaq.push (['_addOrganic', 'images.google.ru', 'q']);
            _gaq.push (['_addOrganic', 'maps.google.ru', 'q']);
            _gaq.push (['_addOrganic', 'rambler.ru', 'words']);
            _gaq.push (['_addOrganic', 'nova.rambler.ru', 'query']);
            _gaq.push (['_addOrganic', 'nova.rambler.ru', 'words']);
            _gaq.push (['_addOrganic', 'gogo.ru', 'q']);
            _gaq.push (['_addOrganic', 'nigma.ru', 's']);
            _gaq.push (['_addOrganic', 'search.qip.ru', 'query']);
            _gaq.push (['_addOrganic', 'webalta.ru', 'q']);
            _gaq.push (['_addOrganic', 'sm.aport.ru', 'r']);
            _gaq.push (['_addOrganic', 'meta.ua', 'q']);
            _gaq.push (['_addOrganic', 'search.bigmir.net', 'z']);
            _gaq.push (['_addOrganic', 'search.i.ua', 'q']);
            _gaq.push (['_addOrganic', 'index.online.ua', 'q']);
            _gaq.push (['_addOrganic', 'web20.a.ua', 'query']);
            _gaq.push (['_addOrganic', 'search.ukr.net', 'search_query']);
            _gaq.push (['_addOrganic', 'search.com.ua', 'q']);
            _gaq.push (['_addOrganic', 'search.ua', 'q']);
            _gaq.push (['_addOrganic', 'poisk.ru', 'text']);
            _gaq.push (['_addOrganic', 'go.km.ru', 'sq']);
            _gaq.push (['_addOrganic', 'liveinternet.ru', 'ask']);
            _gaq.push (['_addOrganic', 'gde.ru', 'keywords']);
            _gaq.push (['_addOrganic', 'affiliates.quintura.com', 'request']);
            _gaq.push (['_addOrganic', 'akavita.by', 'z']);
            _gaq.push (['_addOrganic', 'search.tut.by', 'query']);
            _gaq.push (['_addOrganic', 'all.by', 'query']);


            _gaq.push(['_trackPageview']);
            setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])",15000);


            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter17073367 = new Ya.Metrika({id:17073367,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/17073367" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    </noindex>
    <!-- /Yandex.Metrika counter -->
<?endif?>
<?
if ($_COOKIE["NEED_SELECT_CITY"]=="Y"):?>
    <script>
        $('#information .modal-body').load('/tpl/ajax/city_select2.php?<?=bitrix_sessid_get()?>', function(data) {
            $('#information').modal("show");
        });
    </script>
    <?unset($_COOKIE["NEED_SELECT_CITY"]);?>
<?endif;?>
<div id="system_loading"><?=GetMessage('popup_downloading_mess')?></div>

</body>
</html>