<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
if(!$USER->IsAdmin())
    return false;

CModule::IncludeModule("iblock");

$lastEl = false;
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);
$lastElID = ($_REQUEST["lastElID"] ? $_REQUEST["lastElID"] : 0);
//$curIblockID = ($_REQUEST["curIblockID"] ? $_REQUEST["curIblockID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// start execution
$time_start = microtime_float();

// set last el id
if($lastElID <= 0) {
    /*
	$rsIblock = CIBlock::GetList(
		Array("ID" => "ASC"),
		Array(
			"SITE_ID" => "s1",
			"TYPE" => "catalog",
			">ID" => $curIblockID		
		),
		false
	);
	while($arIblockLast = $rsIblock->Fetch()) {
    */
    $rsLastEl = CIBlockElement::GetList(
        Array("ID"=>"DESC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 15,
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
            "!PROPERTY_google_photo" => false
        ),
        false,
        false,
        Array()
    );
    if($arLastEl = $rsLastEl->GetNext())
        $lastElID = $arLastEl["ID"];
    //}
}

if($curStepID == 0 /*&& $curIblockID <= 0*/) {
    file_put_contents("google_upload_anp.xml", '', LOCK_EX);

    $str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    $str .= "<listings xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://local.google.com/local_feed.xsd\">\n";

        $str .= "\t<language>ru</language>\n";
        $str .= "\t<datum>WGS84</datum>\n";
}
/*
$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
		//">ID" => $curIblockID
        "ID" => 11
	),
	false
);
*/

//while($arIblock = $rsIblock->Fetch()) {
	//$curStepID = 0;

	$rsRest = CIBlockElement::GetList(
		Array("ID" => "ASC"),
		Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => 15,
			">ID" => $curStepID,
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
			"!PROPERTY_google_photo" => false
		),
		false,
		false,
		Array("ID", "NAME", "DETAIL_PAGE_URL", "DATE_CREATE", "PROPERTY_*")
	);
	
	while($arRest = $rsRest->GetNext()) {
		$curStepID = $arRest["ID"];
		//$curIblockID = $arRest["IBLOCK_ID"];
	
		// get photo
		$photoKey = 0;
		$strPhoto = '';
		$rsProp = CIBlockElement::GetProperty(
			$arRest["IBLOCK_ID"],
			$arRest["ID"],
			Array(),
			Array(
				"CODE" => "google_photo"
			)
		);
		while($arProp = $rsProp->GetNext()) {
			if(intval($arProp["VALUE"]) > 0) {
				$arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array('width' => 2000, 'height' => 1500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	
				$strPhoto .= "\t\t\t<image type=\"photo\" url=\"http://".SITE_SERVER_NAME.$arPhoto["src"]."\" width=\"".$arPhoto["width"]."\" height=\"".$arPhoto["height"]."\">\n";
				$strPhoto .= "\t\t\t\t<link>------------------------------</link>\n";
				$strPhoto .= "\t\t\t\t<title>Restaurant inside</title>\n";
				$strPhoto .= "\t\t\t\t<author>Restoran.ru</author>\n";
				$strPhoto .= "\t\t\t</image>\n";
	
				$photoKey++;
				if($photoKey > 3)
					break;
			}
		}
	
		if($photoKey > 0) {
	
			// tmp date
			$arTmpDate = explode(' ', $arRest["DATE_CREATE"]);
			$tmpDate = explode('.', $arTmpDate[0]);
	
			$str .= "\t<listing>\n";
				$str .= "\t\t<id>".$arRest["ID"]."</id>\n";
				$str .= "\t\t<name>".htmlspecialchars($arRest["NAME"])."</name>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "address"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"]) {
						$address = str_replace($arIblock["NAME"].",", "", $arProp["VALUE"]);
						$address = $arIblock["NAME"].", ".$address;
						$str .= "\t\t<address>".$address."</address>\n";
					}	
	
				$str .= "\t\t<country>RU</country>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lat"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<latitude>".$arProp["VALUE"]."</latitude>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lon"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<longitude>".$arProp["VALUE"]."</longitude>\n";
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "phone"
					)
				);
				if($arProp = $rsProp->GetNext())
					if($arProp["VALUE"])
						$str .= "\t\t<phone type=\"main\">".$arProp["VALUE"]."</phone>\n";
	
				$str .= "\t\t<category>Restaurants</category>\n";
				$str .= "\t\t<date month=\"".$tmpDate[1]."\" day=\"".$tmpDate[0]."\" year=\"".$tmpDate[2]."\"/>\n";
				$str .= "\t\t<content>\n";
	
				$str .= $strPhoto;
	
					$str .= "\t\t\t<attributes>\n";
						$str .= "\t\t\t\t<title>Restaurant Details</title>\n";
						$str .= "\t\t\t\t<author>Restoran.ru</author>\n";
						$str .= "\t\t\t\t<email>redaktor@restoran.ru</email>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "site"
							)
						);
						if($arProp = $rsProp->GetNext())
							if($arProp["VALUE"]) {
                                if(strstr($arProp["VALUE"], ','))
                                    $tmpWebsite = explode(",", $arProp["VALUE"]);
                                elseif(strstr($arProp["VALUE"], ';'))
                                    $tmpWebsite = explode(";", $arProp["VALUE"]);
                                else
                                    $tmpWebsite[0] = $arProp["VALUE"];

                                $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);

                                $str .= "\t\t\t\t<website>"."http://".$tmpWebsite[0]."</website>\n";
								//$str .= "\t\t\t\t<website>".$arProp["VALUE"]."</website>\n";
                            }

						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "opening_hours_google"
							)
						);
						if($arProp = $rsProp->GetNext())
							if($arProp["VALUE"])
								$str .= "\t\t\t\t<attr name=\"Hours\">".$arProp["VALUE"]."</attr>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "credit_cards"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->GetNext())
								$str .= "\t\t\t\t<attr name=\"Payment accepted\">".$arPayment["NAME"]."</attr>\n";
						}
	
						//$str .= "\t\t\t\t<attr name=\"Attire\">Casual</attr>\n";
	
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kolichestvochelovek"
							)
						);
						if($arProp = $rsProp->GetNext())
							$rsSeating = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arSeating = $rsSeating->GetNext())
								$str .= "\t\t\t\t<attr name=\"Seating\">".str_replace("&nbsp;", "", $arSeating["NAME"])."</attr>\n";
	
						//$str .= "\t\t\t\t<attr name=\"Phone Orders\">No</attr>\n";
						$str .= "\t\t\t\t<date month=\"".$tmpDate[1]."\" day=\"".$tmpDate[0]."\" year=\"".$tmpDate[2]."\"/>\n";
					$str .= "\t\t\t</attributes>\n";
	
				$str .= "\t\t</content>\n";
			$str .= "\t</listing>\n";
	
			v_dump($arRest["IBLOCK_ID"]);
			v_dump($arRest["ID"]);
	
		}
	
		// check last element id
		if($lastElID == $arRest["ID"]) {
			$str .= "</listings>";
			$lastEl = true;
		}
	
		// execution time
		$time_end = microtime_float();
		$time = $time_end - $time_start;
		if($time > 30) {
			file_put_contents("google_upload_anp.xml", $str, FILE_APPEND | LOCK_EX);
	
			echo "<script>setTimeout(function() {location.href = '/google_upload_anp.php?curStepID=".$curStepID."&lastElID=".$lastElID."&curIblockID=".$curIblockID."'}, 5000);</script>";
			break;
		}
	
		if($lastEl) {
			v_dump("Last EL");
			file_put_contents("google_upload_anp.xml", $str, FILE_APPEND | LOCK_EX);
		}
	}
//}
?>
