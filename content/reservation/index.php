<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Служба бронирования");
$APPLICATION->SetTitle("Служба бронирования");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?$APPLICATION->ShowTitle()?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Restoran.ru">
        <meta property="fb:app_id" content="297181676964377" />
        <?if (CITY_ID=="tmn"):?>
            <meta property="fb:admins" content="100005128267295,100004709941783" />
        <?else:?>
            <meta property="fb:admins" content="100004709941783" />
        <?endif;?>
        <?if (!$_REQUEST["CODE"]):?>
            <meta property="og:image" content="http://www.restoran.ru/images/logo.png" />
        <?endif;?>
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link href="http://fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->



        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>



        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>


        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script-new.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>

        <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/index.js')?>


        <?$APPLICATION->ShowHead()?>
        <?
        //Строки от Антона
        if(substr_count($_SERVER["REQUEST_URI"], "blog")>0 || substr_count($_SERVER["REQUEST_URI"], "kup")>0 || substr_count($_SERVER["REQUEST_URI"], "restoran_edit")>0 || substr_count($_SERVER["REQUEST_URI"], "restorator")>0 || substr_count($_SERVER["REQUEST_URI"], "businessman")>0 || substr_count($_SERVER["REQUEST_URI"], "rest_edit")>0)
        {
            if (!CSite::InGroup(Array(1,15,16,23)))
            {
                //$APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
            }
            else
                $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/style.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadString('<link href="/bitrix/templates/.default/components/restoran/editor2/editor/jq_redactor/css/redactor.css"  type="text/css" rel="stylesheet" />',true);
            $APPLICATION->AddHeadScript('/tpl/js/jquery-ui-1.8.17.custom.min.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/redactor_list_script.js');
        }
        $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);

        CModule::IncludeModule("advertising");
        if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
            $page = "others";
        else
            $page = "main";
        CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));

        //unset($_SESSION["CONTEXT"]);
        //unset($_REQUEST["CONTEXT"]);
        //        FirePHP::getInstance()->info();
        ?>


        <link href="<?=SITE_TEMPLATE_PATH?>/css/reservation.css"  type="text/css" rel="stylesheet" />

        <script>
            $(function(){
//                $('#booking_form').modal('show');
            })
        </script>

    </head>
<body>


    <div id="panel"><?$APPLICATION->ShowPanel()?></div>

    <div class="container">
        <div class="content content-indent">

            <div class="block">

                <div class="section-title-wrapper">
                    <h1>Служба бронирования</h1><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/white-logo.png" alt="Ресторан.ру" width="" height="" alt="" ></a>
                </div>
                <div class="this-section-description">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/content/reservation/inc_top_desc_".CITY_ID.".php",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    );?>
                    
                    <br>
                    <strong>
                        быстро, бесплатно и с удовольтвием!
                    </strong>
                </div>

                <div class="modal fade in" data-backdrop="static" id="booking_form" tabindex="-1" role="dialog" aria-hidden="false" style="display: block;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <noindex>
                                    <?if (CITY_ID=="ast"):
                                        ShowError("Услуга временно не работает");
                                        die;
                                    endif;?>
                                    <?
                                    $APPLICATION->IncludeComponent(
                                        "restoran:form.result.new",
                                        //($USER->IsAdmin())?"ajax_form_bron_rest_sms_new":"ajax_form_bron_rest_sms",
                                        "order_2014",
                                        Array(
                                            "SEF_MODE" => "N",
                                            "WEB_FORM_ID" => "3",
                                            "LIST_URL" => "",
                                            "EDIT_URL" => "",
                                            "SUCCESS_URL" => "",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "CHAIN_ITEM_LINK" => "",
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "USE_EXTENDED_ERRORS" => "N",
                                            "CACHE_TYPE" => "N",
                                            "CACHE_TIME" => "3600",
                                            "MY_CAPTCHA" => "Y",
                                            "VARIABLE_ALIASES" => Array(
                                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                                "RESULT_ID" => "RESULT_ID"
                                            ),
                                            'SHOW_THIS_FORM_TITLE'=>'N'
                                        ),
                                        false
                                    );?>
                                </noindex>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="this-section-detail-text">
                    <div class="left-block">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/content/reservation/inc_left_bottom_desc_".CITY_ID.".php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>

                    </div>
                    <div class="right-block">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/content/reservation/inc_left_bottom_desc_".CITY_ID.".php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>

                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>



</body>
</html>
<?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>