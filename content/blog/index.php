<?
if($_REQUEST['AJAX_REQUEST']!='Y'):
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Блоги на Ресторан.ру");
$APPLICATION->SetPageProperty("description", "Блоги на Ресторан.ру");
$APPLICATION->SetTitle("Блоги на Ресторан.ру");
else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
endif;
if ($_REQUEST["blog"]=="all")
    $arIB = getArIblock2("blogs");
else
    $arIB = getArIblock("blogs", CITY_ID);
?>
<?

if ($_REQUEST["USER_ID"])
{
    global $arrFilterTop4;
    $arrFilterTop4 = array();
    if((int)$_REQUEST["USER_ID"]==65||(int)$_REQUEST["USER_ID"]==93){
        $arrFilterTop4["PROPERTY_CRITIC_POST_VALUE"] = 'Y';
    }
//    else {
//        $arrFilterTop4["CREATED_BY"] = (int)$_REQUEST["USER_ID"];
//    }


}
?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
<div class="block">
    <h1><?=$APPLICATION->GetTitle("h1");?></h1>
    <?
    $excUrlParams = array("page", "pageSort", "PAGEN_1", "by", "SECTION_CODE", "clear_cache");
    $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "") . "pageSort=ACTIVE_FROM&" . (($_REQUEST["pageSort"] == "ACTIVE_FROM" && $_REQUEST["by"] == "desc" || !$_REQUEST["pageSort"]) ? "by=asc" : "by=desc"), $excUrlParams);
    $sortPopularPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=" . $_REQUEST["PAGEN_1"] . "&" : "") . "pageSort=PROPERTY_summa_golosov&" . (($_REQUEST["pageSort"] == "PROPERTY_summa_golosov" && $_REQUEST["by"] == "asc" || !$_REQUEST["pageSort"]) ? "by=desc" : "by=asc"), $excUrlParams);
    ?>                
    <div class="sort">                        
            <? $by = ($_REQUEST["by"] == "asc") ? "asc" : "desc"; ?>
            <?
            if ($_REQUEST["pageSort"] == "ACTIVE_FROM" || !$_REQUEST["pageSort"]):
                echo "<a class='" . $by . "' href='" . $sortNewPage . "'>по новизне</a>";
            else:
                echo '<a href="' . $sortNewPage . '">по новизне</a>';
            endif;
            ?>            
            <?
            if ($_REQUEST["pageSort"] == "PROPERTY_summa_golosov"):
                echo "<a class='" . $by . "' href='" . $sortPopularPage . "'>по популярности</a>";
            else:
                echo '<a href="' . $sortPopularPage . '">по популярности</a>';
            endif;
            ?>                        
    </div>
    <div class="sort">
        <noindex>
            <?if ($_REQUEST["blog"]=="all"):?>
                <?if (CITY_ID=="msk"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Москва</a> 
                <?elseif(CITY_ID=="spb"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Петербург</a> 
                <?elseif(CITY_ID=="anp"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Анапа</a> 
                <?elseif(CITY_ID=="sch"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Сочи</a> 
                <?elseif(CITY_ID=="krd"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Краснодар</a> 
                <?elseif(CITY_ID=="kld"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Калининград</a> 
                <?elseif(CITY_ID=="nsk"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Новосибирск</a> 
                <?elseif(CITY_ID=="urm"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Юрмала</a> 
                <?elseif(CITY_ID=="rga"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Рига</a> 
                <?elseif(CITY_ID=="ufa"):?>
                    <a class="another" href="/<?=CITY_ID?>/blogs/">только Уфа</a> 
                <?endif;?>
                / 
                по всем городам
            <?else:?>
                только 
                <?if (CITY_ID=="msk"):?>
                    Москва
                <?elseif(CITY_ID=="spb"):?>
                    Петербург
                <?elseif(CITY_ID=="anp"):?>
                    Анапа
                <?elseif(CITY_ID=="sch"):?>
                    Сочи
                <?elseif(CITY_ID=="krd"):?>
                    Краснодар
                <?elseif(CITY_ID=="kld"):?>
                    Калининград
                <?elseif(CITY_ID=="nsk"):?>
                    Новосибирск
                <?elseif(CITY_ID=="urm"):?>
                    Юрмала
                <?elseif(CITY_ID=="rga"):?>
                    Рига
                <?elseif(CITY_ID=="ufa"):?>
                    Уфа
                <?endif;?>
                / <a class="another" href="/all/blogs/">по всем городам</a>
            <?endif;?>
        </noindex>
    </div>
    <div class="clearfix"></div>
    <div class="left-side">
<?endif?>
        <?
        $APPLICATION->IncludeComponent(
                "restoran:catalog.list_multi",
                "news",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "blogs",
                    "IBLOCK_ID" => $arIB["ID"],
                    "NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "21"),
                    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "created_date"),
                    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFilterTop4",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE",'SHOW_COUNTER'),
                    "PROPERTY_CODE" => array("COMMENTS","summa_golosov"),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000009",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    'AJAX_REQUEST' => $_REQUEST['AJAX_REQUEST'],
                ),
            false
            );
        ?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
    </div>
    <div class="right-side">
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_2_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
        <?$APPLICATION->IncludeComponent(
                "bitrix:subscribe.form",
                "main_page_subscribe_new",
                Array(
                        "USE_PERSONALIZATION" => "Y",
                        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                        "SHOW_HIDDEN" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_NOTES" => $USER->GetID()
                ),
        false
        );?>

        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_1_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
        <? if (SITE_ID != "s2"&&CITY_ID!="ufa"&&CITY_ID!="nsk"): ?>
        <?
            if (CITY_ID=="tmn")
                $anons = Array("overviews", "photoreports", "cookery", "news","interview");
            elseif (CITY_ID=="ufa"||CITY_ID=="kld")
                $anons = Array();
            elseif (CITY_ID=="rga"||CITY_ID=="urm")
                    $anons = Array("cookery", "news");
            else
                $anons = Array("overviews","photoreports","cookery","news","interview");
            $iblock_type = Array($arResult["ITEMS"][0]["IBLOCK_TYPE_ID"]);
            $diff = array_diff($anons,$iblock_type);
//            FirePHP::getInstance()->info($diff,'$diff');
            $p=0;
            foreach ($diff as $d)
            {
                if ($p<4)
                    $dif[] = $d;
                $p++;
            }


//            if($USER->IsAdmin()){
//                FirePHP::getInstance()->info($dif,'$dif');
//                $dif = array("overviews", "photoreports", "cookery", "news");
//            }

            foreach ($dif as $as)
            {
                $arAnons[] = getArIblock($as, CITY_ID);
            }
//            FirePHP::getInstance()->info($arAnons,'$arAnons');
            $MESS1 = array();
            $MESS1["ANONS_overviews"] = "Обзоры";
            $MESS1["ANONS_news_overviews"] = "Обзоры";
            $MESS1["ANONS_news_photo"] = "Фотоотчеты";
            $MESS1["ANONS_photoreports"] = "Фотоотчеты";
            $MESS1["ANONS_cookery"] = "Рецепты от шефа";
            $MESS1["ANONS_news"] = "Новости";
            $MESS1["ANONS_interview"] = "Интервью";
            $MESS1["ANONS_blogs"] = "Критика";
        ?>
            <?
            foreach ($arAnons as $key => $anons):
                ?>
                <?if ($dif[$key]=="blogs"&&CITY_ID=="kld"):?>
                    <div class="title" align="">Блоги</div>
                <?else:?>
                    <div class="title" align="<?if($dif[$key]=='cookery'):?>center<?endif;?>"><?= $MESS1["ANONS_" . $dif[$key]]?></div>
                <?endif;?>
                <?
                $arrAddFilter = array();
                if ($dif[$key] == "blogs") {
                    global $arrAddFilter;
                    if (CITY_ID == "spb")
                        $arrAddFilter = Array("CREATED_BY" => 65);
                    elseif (CITY_ID == "msk")
                        $arrAddFilter = Array("CREATED_BY" => 93);
                    else
                    {

                    }
                }
                if ($dif[$key] == "news_overviews") {
                    global $arrAddFilter;
                    $arrAddFilter = Array("SECTION_ID" => 215534);
                    $dif[$key] = "news";
                }
                if ($dif[$key] == "news_photo") {
                    global $arrAddFilter;
                    $arrAddFilter = Array("SECTION_ID" => 215535);
                    $dif[$key] = "news";
                }
                ?>
                <?
//                FirePHP::getInstance()->info($anons["ID"],'check');
                $APPLICATION->IncludeComponent(
                        "restoran:catalog.list", "interview_one_with_border", Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $dif[$key],
                    "IBLOCK_ID" => ($dif[$key] == "cookery") ? 145 : $anons["ID"],
                    "NEWS_COUNT" => 1,
                    "SORT_BY1" => "ID",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrAddFilter",
                    "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("ratio"),//, "COMMENTS"
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                        ), false
                );
                ?>
            <? endforeach; ?>
        <? endif; ?>
        <div class="tags">
            <?
            if (LANGUAGE_ID == 'ru') {
                if (CITY_ID=="msk"||CITY_ID=="spb")
                        $ci = Array(156,157);
                    else
                        $ci =  Array($arIB["ID"]);

                $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                    "SORT" => "CNT",
                    "PAGE_ELEMENTS" => "20",
                    "PERIOD" => "",
                    "URL_SEARCH" => "/search/index.php",
                    "TAGS_INHERIT" => "Y",
                    "CHECK_DATES" => "Y",
                    "FILTER_NAME" => "",
                    "arrFILTER" => array(
                            0 => "iblock_blogs",
                    ),
                    "arrFILTER_iblock_blogs" => $ci,
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "86401",
                    "FONT_MAX" => "24",
                    "FONT_MIN" => "12",
                    "COLOR_NEW" => "30c5f0",
                    "COLOR_OLD" => "24A6CF",
                    "PERIOD_NEW_TAGS" => "",
                    "SHOW_CHAIN" => "Y",
                    "COLOR_TYPE" => "N",
                    "WIDTH" => "100%",
                    'SEARCH_IN'=>'blogs'
                    ),
                    false
                );
            }
            ?>
        </div>
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_3_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
    </div>
    <div class="preview_seo_text" style="clear: both;">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
    <div class="clearfix"></div>

    <?
    $APPLICATION->IncludeComponent(
        "bitrix:advertising.banner", "", Array(
        "TYPE" => "bottom_rest_list",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
            ), false
    );
    ?>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?endif?>