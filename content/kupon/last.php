<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("kupon.restoran.ru");
$arKuponsIB = getArIblock("kupons", CITY_ID);
global $arrFilter;
$arrFilter["<=DATE_ACTIVE_TO"] = date("d.m.Y H:i:s");
?>
<h1>Прошедшие акции</h1>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "last_kupons",
    Array(
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "kupons",
            "IBLOCK_ID" => $arKuponsIB["ID"],
            "NEWS_COUNT" => ($_REQUEST["pageKuponCnt"] ? $_REQUEST["pageKuponCnt"] : "20"),
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => Array("ACTIVE_FROM","ACTIVE_TO"),
            "PROPERTY_CODE" => array("ratio", "reviews", "subway","kupon_dnya","new","popular"),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "150",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Купоны",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "kupon_list",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "PRICE_CODE" => "BASE"
    ),
false
);?>
<div class="clear"></div>
<div class="kupon_long_baner">
    <img src="<?=SITE_TEMPLATE_PATH?>/images/top_baner.png" />
</div>
<br /><br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>