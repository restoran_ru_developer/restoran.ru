<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Покупка скидки");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Покупка скидки");
$arKuponsIB = getArIblock("kupons", CITY_ID);
?>
<div id="content">	
	<div class="left" style="width:720px;">
		<h1>Покупка скидки</h1>
		<div class="grey_block">
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.detail",
				"kupon_order",
				Array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"USE_SHARE" => "N",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "kupons",
					"IBLOCK_ID" => $arKuponsIB["ID"],
					"ELEMENT_ID" => (int)$_REQUEST["ID"],
					"ELEMENT_CODE" => "",
					"CHECK_DATES" => "Y",
					"FIELD_CODE" => Array("PREVIEW_PICTURE","ACTIVE_TO"),
					"PROPERTY_CODE" => Array("PHOTOS","RESTORAN_PREVIEW","subway"),
					"IBLOCK_URL" => "",
					"META_KEYWORDS" => "-",
					"META_DESCRIPTION" => "-",
					"BROWSER_TITLE" => "-",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"ACTIVE_DATE_FORMAT" => "j F Y",
					"USE_PERMISSIONS" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_NOTES" => "",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Страница",
					"PAGER_TEMPLATE" => "",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				),
			false
			);?> 
			<div class="dotted"></div>
			<br />
			<?if (!$_REQUEST["ORDER_ID"]):?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket",
					"basket",
					Array(
						"PATH_TO_ORDER" => "/personal/order.php",
						"HIDE_COUPON" => "Y",
						"COLUMNS_LIST" => array("NAME", "PRICE", "QUANTITY"),
						"QUANTITY_FLOAT" => "N",
						"PRICE_VAT_SHOW_VALUE" => "N",
						"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
						"SET_TITLE" => "N"
					),
				false
				);?>
				<br />
				<div class="dotted"></div>			
			<?endif;?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:sale.order.ajax",
				//"order",
                                "order2",
                                //"",
				Array(
					"PATH_TO_BASKET" => "",
					"PATH_TO_PERSONAL" => "index.php",
					"PATH_TO_PAYMENT" => "payment.php",
					"PATH_TO_AUTH" => "/auth/",
					"PAY_FROM_ACCOUNT" => "Y",
					"COUNT_DELIVERY_TAX" => "N",
					"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
					"ONLY_FULL_PAY_FROM_ACCOUNT" => "Y",
					"ALLOW_AUTO_REGISTER" => "N",
					"SEND_NEW_USER_NOTIFY" => "N",
					"DELIVERY_NO_AJAX" => "Y",
					"SET_TITLE" => "N",
                                        "FIELD_CODE" => Array("payment_type","present_email"),   
					"PROPERTY_CODE" => array("payment_type", "present_email"),
					"PROP_1" => array("1", "2", "4", "5", "6", "7", "21", "22"),
					//"PROP_2" => array("3", "8", "9", "10", "11", "12", "13", "14", "15", "16")
				),
			false
			);?>
		</div>
                <br /><br />
                <div id="tabs_block2">
                    <ul class="tabs">
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Количество</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Способы оплаты</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Перевод денег</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Распечатать скидку</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                    </ul>
                    <!-- tab "panes" -->
                    <div class="panes">
                        <div class="pane" style="display:block">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                    </div>
                </div>
	</div>
	<div class="right" style="width:240px">
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>	
		<br /><br />
                <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest.php"),
                        Array(),
                        Array("MODE"=>"html")
                );?>
		<br /><br />
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
	</div>
	<div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>