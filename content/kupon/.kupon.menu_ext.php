<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
$arKuponIB = getArIblock("kupons", CITY_ID);
$aMenuLinksExt=$APPLICATION->IncludeComponent(
	"bitrix:menu.sections",
	"",
	Array(
		"IS_SEF" => "N",
		"ID" => "",
		"IBLOCK_TYPE" => "kupons",
		"IBLOCK_ID" => $arKuponIB["ID"],
		"SECTION_URL" => "",
		"DEPTH_LEVEL" => "1",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000"
	)
);
$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
?>