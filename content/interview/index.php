<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Интервью");
$arInterviewIB = getArIblock("interview", CITY_ID);
?>
<?$APPLICATION->IncludeComponent(
	"restoran:article.list",
	"news",
	Array(
		"IBLOCK_TYPE" => "interview",
		"IBLOCK_ID" => $arInterviewIB["ID"],
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
                "PAGE_COUNT" => 20,
		"SECTION_URL" => "/content/interview/detail.php?SECTION_ID=#ID#",
                "PICTURE_WIDTH" => 232,
                "PICTURE_HEIGHT" => 127,
                "DESCRIPTION_TRUNCATE_LEN" => 200,
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => "",
		"SECTION_USER_FIELDS" => Array("UF_SECTION_BIND", "UF_SECTION_COMM_CNT"),
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "kupon_list",	
                "PAGER_DESC_NUMBERING" => "N",	
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		
	),
false
);?> 

<?/*$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "interview",
    Array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "interview",
        "IBLOCK_ID" => $arInterviewIB["ID"],
        "NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "20"),
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => Array("ACTIVE_TO"),
        "PROPERTY_CODE" => array("ratio", "reviews", "subway"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "150",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Купоны",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "kupon_list",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "PRICE_CODE" => "BASE"
    ),
false
);*/?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>