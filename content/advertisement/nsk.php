<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Реклама на сайте");
$APPLICATION->SetPageProperty("description", "Реклама на сайте");
$APPLICATION->SetTitle("Реклама на сайте");
?> 
<div id="content"> 
  <h1><?=$APPLICATION->GetTitle()?></h1>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">VIP</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">30 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города. </p>
 
  <p class="MsoNormal">2. Авторская статья на русском и английском языках. </p>
 
  <p class="MsoNormal">3. Меню и винная карта на русском и английском языках. </p>
 
  <p class="MsoNormal">4. Фотографии (до 30 штук). </p>
 
  <p class="MsoNormal">5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик). </p>
 
  <p class="MsoNormal">6. Субдомен www.название ресторана.restoran.ru , почтовый ящик: название ресторана@restoran.ru. </p>
 
  <p class="MsoNormal">7. Контекстная реклама по названию ресторана в Яндексе. </p>
 
  <p class="MsoNormal">8. Баннер 240х400 в разделе Рестораны сроком на 1 месяц в ротации! (кроме октября-декабря). </p>
 
  <p class="MsoNormal">9. Бесплатное размещение новостей и фоторепортажей на главной странице (до 4-х в месяц); на собственной странице &ndash; без ограничений. </p>
 
  <p class="MsoNormal">10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). </p>
 
  <p class="MsoNormal">11. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;. </p>
 
  <p class="MsoNormal">12. Бесплатное обновление меню 1 раз в месяц (только на русском языке). </p>
 
  <p class="MsoNormal">13. Cкидка 50% на размещение в других рубриках (&quot;Банкетные залы&quot;).   </p>
 
  <p class="MsoNormal">14. Возможно изготовление 3D-тура (оплачивается отдельно).</p>
 
  <p class="MsoNormal"> <hr/></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p style="text-align: center;" class="MsoNormal"><font size="4"><b>PREMIUM 
        <br />
       </b><b>20 000 рублей в год</b></font></p>
 
  <p style="text-align: center;" class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт. </p>
 
  <p class="MsoNormal">2. Авторская статья на русском и английском языках. </p>
 
  <p class="MsoNormal">3. Меню и винная карта на русском и английском языках. </p>
 
  <p class="MsoNormal">4. Фотографии (до 30 штук).   </p>
 
  <p class="MsoNormal">5. Видеопанорамы залов.  </p>
 
  <p class="MsoNormal">6. Есть возможность размещать видео (предоставляет заказчик       </p>
 
  <p class="MsoNormal">7. Субдомен www. название Dресторана.restoran.ru, почтовый ящик названиересторана@restoran.ru.    8. Бесплатное рекламное место сроком на 1 месяц (кроме сентября-октября). </p>
 
  <p class="MsoNormal">9. Высокая степень прописки по названию в поисковых системах www.yandex.ru или www.google.ru (1-3 место). </p>
 
  <p class="MsoNormal">10. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений. </p>
 
  <p class="MsoNormal">11. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). </p>
 
  <p class="MsoNormal">12. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;. </p>
 
  <p class="MsoNormal">13. Бесплатное обновление меню 1 раз в месяц (только на русском языке). </p>
 
  <p class="MsoNormal">14. Cкидка 50% на размещение в других рубриках.   </p>
 
  <p class="MsoNormal">15. Возможно изготовление 3D-тура (оплачивается отдельно).</p>
 
  <p class="MsoNormal"> <hr/></p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"><b><font size="4">BUSINESS</font></b></div>
 <b><font size="4"> </font> 
    <div style="text-align: center;"><b><font size="4">15 000 рублей в год</font></b></div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города. </p>
 
  <p class="MsoNormal">2. Меню и винная карта на русском языке. </p>
 
  <p class="MsoNormal">3. Фотографии (до 20 штук). </p>
 
  <p class="MsoNormal">4. Видеопанорамы залов.   </p>
 
  <p class="MsoNormal">5. Статья о ресторане (предоставляется заказчиком). </p>
 
  <p class="MsoNormal">6. Субдомен www. название ресторана.restoran.ru, почтовый ящик &laquo;название ресторана@restoran.ru. </p>
 
  <p class="MsoNormal">7. Бесплатное размещение новостей и фоторепортажей на главной странице (до 4-х в месяц); на собственной странице – без ограничений. </p>
 
  <p class="MsoNormal">8. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). </p>
 
  <p class="MsoNormal">9. Бесплатное участие в рубрике «Мастер-класс от шеф-повара&raquo;. </p>
 
  <p class="MsoNormal">10. Бесплатное обновление меню 1 раз в месяц. </p>
 
  <p class="MsoNormal">11. Скидка 50% на размещение в других рубриках («Банкетные залы»). <hr/></p>
 
  <p style="text-align: center;" class="MsoNormal"><b><font size="4">LIGHT 
        <br />
       12 000 рублей в год</font></b></p>
 
  <p style="text-align: center;" class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт. </p>
 
  <p class="MsoNormal">2. Фотографии (до 10 штук). </p>
 
  <p class="MsoNormal">3. 20 пунктов меню. </p>
 
  <p class="MsoNormal">4. Субдомен www. название ресторана.restoran.ru, почтовый ящик названиересторана@restoran.ru. </p>
 
  <p class="MsoNormal">5. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений; </p>
 
  <p class="MsoNormal">6. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.)<hr/></p>
 
  <p class="MsoNormal"> 
    <br />
   </p>

  <p class="MsoNormal" style="text-align: center;"><b><font size="4">Баннерная реклама</font></b></p>

  <p class="MsoNormal">Верхний баннер в шапке сайта 960х90 - 15000руб/месяц</p>

  <p class="MsoNormal">Верхний боковой баннер и баннер №2 240х400 - 15000руб/месяц</p>

  <p class="MsoNormal">Нижний боковой баннер 204х400 - 10000руб/месяц</p>

  <p class="MsoNormal">Нижний баннер в футере сайта 728х90 - 10000руб/месяц</p>

  <p class="MsoNormal"></p>

  <p class="MsoNormal">Приоритетные места в каталоге ресторанов и банкетных залов - 5000руб/месяц</p>

  <p class="MsoNormal">
    <br />
  </p>
 
  <p class="MsoNormal"><b>Дополнительные услуги:</b></p>
 
  <p class="MsoNormal">Репортажная фотосъемка мероприятий.</p>
 
  <p class="MsoNormal">Съемка 3д туров для заведений.</p>
 
  <p class="MsoNormal"><b>НДС не облагается.</b></p>
 
  <p class="MsoNormal"><b>Мы с удовольствием ответим на все Ваши вопросы:</b></p>
 
  <p class="MsoNormal">+7 (383) 287-48-15 (Николай Константинович)</p>
 
  <p class="MsoNormal"><b>e-mail: </b>nsk@restoran.ru</p>
 
  <p class="MsoNormal"><b>С уважением к вам и вашему бизнесу.</b></p>
 
  <p class="MsoNormal"><b>Команда портала Restoran.ru Новосибирск</b></p>
 
  <p class="MsoNormal"><b>        
      <br />
     </b> 
    <br />
   
    <br />
   </p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>