<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Реклама на сайте");
$APPLICATION->SetPageProperty("description", "Реклама на сайте");
$APPLICATION->SetTitle("Реклама на сайте");
?> 
<div id="content"> 
  <h1><?=$APPLICATION->GetTitle()?></h1>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"> 
    <div><b><font size="4">BUSINESS</font></b></div>
   <b style="text-align: start;"><font size="4"></font> 
      <div style="text-align: center;"><b><font size="4">33 000 рублей в год</font></b></div>
     </b></div>
 <b> 
    <div style="text-align: center;"> 
      <p class="MsoNormal" style="font-weight: normal; text-align: start;">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города.  
        <br />
       2. Меню и винная карта на русском языке.  
        <br />
       3. Фотографии (до 20 штук).           
        <br />
       4. Описательная статья.   
        <br />
       5. Субдомен www. название ресторана.restoran.ru, почтовый ящик «название ресторана@restoran.ru.  
        <br />
       6. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений.  
        <br />
       7. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.).  
        <br />
       8. Бесплатное обновление меню 1 раз в месяц.  
        <br />
       9. Скидка 50% на размещение в других рубриках («Банкетные залы»). </p>
     
      <p class="MsoNormal" style="text-align: left; font-weight: normal;"><b>Пример:  </b></p>
     
      <p class="MsoNormal" style="text-align: left;"><a href="https://spb.restoran.ru/spb/detailed/restaurants/pacman/" target="_blank" >ресторан «Pacman»</a></p>
     </div>
   </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><hr/></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal" style="text-align: center;"><font size="4"><b>PREMIUM 
        <br />
       </b><b>44 000 рублей в год</b></font></p>
 
  <p class="MsoNormal" style="text-align: center;"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города, ссылка на сайт. 
    <br />
   2. Описательная статья на русском и английском языках. 
    <br />
   3. Меню и винная карта на русском и английском языках. 
    <br />
   4. Фотографии (до 30 штук).   
    <br />
   5. Видеопанорамы залов.  
    <br />
   6. Есть возможность размещать видео (предоставляет заказчик).             
    <br />
   7. Субдомен www. название ресторана.restoran.ru, почтовый ящик название ресторана@restoran.ru.     
    <br />
   8. Бесплатное размещение на странице Best на 1 месяц (кроме сентября-октября). 
    <br />
   9. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений. 
    <br />
   10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). 
    <br />
   11. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;. 
    <br />
   12. Бесплатное обновление меню 1 раз в месяц (только на русском языке). 
    <br />
   13. Cкидка 50% на размещение в других рубриках.   
    <br />
   14. Возможно изготовление 3D-тура (оплачивается отдельно).</p>
 
  <p class="MsoNormal"><b>Пример: </b></p>
 
  <p class="MsoNormal"><a href="https://spb.restoran.ru/spb/detailed/restaurants/res-dom/" ><b>ресторан «Дом»</b></a></p>
 
  <p class="MsoNormal"> <hr/></p>
 
  <p class="MsoNormal"> </p>
 
  <div style="text-align: center;"> 
    <div><b><font size="4">VIP</font></b></div>
   <b style="text-align: start;"><font size="4"></font> 
      <div style="text-align: center;"><b><font size="4">66 000 рублей в год</font></b></div>
     </b></div>
 
  <div style="text-align: center;"> 
    <br />
   </div>
 <b> </b> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города.  
    <br />
   2. Описательная статья на русском и английском языках.  
    <br />
   3. Меню и винная карта на русском и английском языках.  
    <br />
   4. Фотографии (до 40 штук).  
    <br />
   5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик).  
    <br />
   6. Субдомен www.название ресторана.restoran.ru, почтовый ящик: название ресторана@restoran.ru.  
    <br />
   7. Баннер 240х400 или 980x90 сроком на 1 месяц в ротации! (кроме октября-декабря). 
    <br />
   8. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений.  
    <br />
   9. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.).  
    <br />
   10. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;.  
    <br />
   11. Бесплатное обновление меню 1 раз в месяц (только на русском языке).  
    <br />
   12. Cкидка 50% на размещение в других рубриках (&quot;Банкетные залы&quot;).    
    <br />
   13. Возможно изготовление 3D-тура (оплачивается отдельно).</p>
 
  <p class="MsoNormal"><b>Пример: </b><a href="https://spb.restoran.ru/spb/detailed/banket/shusha/" ><b>ресторан «Шуша»</b></a></p>
 
  <div> 
    <br />
   </div>
 
  <p style="text-align: center;" class="MsoNormal"><hr/></p>
 
  <div style="text-align: center;" yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><font size="4"><b>FOREVER</b></font></div>
 
  <div style="text-align: center;" yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><b><font size="4">99 000 рублей</font></b></div>
 
  <div style="text-align: center;" yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><font size="4"><b>Однократная оплата. Бессрочное размещение в каталоге &quot;Рестораны&quot; и &quot;Банкетные залы&quot;</b></font></div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px; text-align: center;" st=""><font size="4"><b> 
        <br />
       </b></font></div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">2. Описательная статья на русском и английском языках.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">3. Меню и винная карта на русском и английском языках.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">4. Фотографии (до 50 штук).  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">5. Видеопанорамы залов. Есть возможность размещать видео (предоставляет заказчик).  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">6. Субдомен www.название ресторана.restoran.ru, почтовый ящик: название ресторана@restoran.ru.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">7. Баннер 240х400 или 980x90 сроком на 1 месяц (кроме октября-декабря) в разделах: </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st=""> 
    <ul> 
      <li>Каталог ресторанов или</li>
     
      <li>Внутренние страницы ресторанов или</li>
     
      <li>Каталог банкетных залов или</li>
     
      <li>Внутренние страницы банкетных залов или</li>
     
      <li>Все остальные страницы сайта.</li>
     </ul>
   8. Бесплатное размещение на странице Best на 1 месяц (кроме сентября-декабря). </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">9. Бесплатное размещение новостей и фоторепортажей на главной странице (на усмотрение редакции); на собственной странице – без ограничений.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">10. Бесплатное участие в обзорах (Новый год, 8 марта, Летние веранды и т.д.). </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">11. Бесплатное участие в рубрике &quot;Мастер-класс от шеф-повара&quot;.  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">12. Бесплатное обновление меню 1 раз в месяц (только на русском языке).  </div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st="">13. Cкидка 50% на размещение в других рубриках (&quot;Банкетные залы&quot;).   
    <br />
   14. Возможно изготовление 3D-тура (оплачивается отдельно).</div>
 
  <div yle="color: rgb(39, 39, 39); font-family: Georgia; font-size: 12px; line-height: 18px;" st=""> 
    <br />
   </div>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>Дополнительные услуги:</b><o:p></o:p></p>
 
  <p class="MsoNormal"> </p>
 
  <ul> 
    <li>Дополнительный выезд фотографа – 5000 руб. ( в пределах КАД)</li>
   
    <li>Дополнительный выезд фотографа – 7000 руб. (за пределами КАД)</li>
   
    <li>Дополнительный выезд журналиста – 1000 руб. (в пределах КАД)</li>
   
    <li>Дополнительный выезд журналиста – 1300 руб. (за пределами КАД)</li>
   </ul>
 <o:p></o:p> 
  <p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"><b>НДС не облагается.</b></p>
 
  <p class="MsoNormal"><b>Наши менеджеры с удовольствием ответят на все Ваши вопросы: 
      <br />
     </b> 
    <br />
   <b>В Санкт-Петербурге:  
      <br />
     </b>Контакты:  
    <br />
   +7-921-941-15-48, Олег (oleg@restoran.ru)     
    <br />
   
    <br />
   </p>
 
  <p class="MsoNormal"><b>В Москве:</b> 
    <br />
   (495) 745-05-66 
    <br />
   E-mail: manager@restoran.ru</p>
 
  <p class="MsoNormal"><o:p></o:p></p>
 
  <p class="MsoNormal"> 
    <br />
   </p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>