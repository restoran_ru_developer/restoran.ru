<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Реклама на сайте");
$APPLICATION->SetPageProperty("description", "Реклама на сайте");
$APPLICATION->SetTitle("Реклама на сайте");
?> 
<div id="content"> 
  <h1><?=$APPLICATION->GetTitle()?></h1>
 
  <div align="center"> 
    <br />
   <b><b> 
        <div align="center">Ресторанный портал Ресторан.ру Калининград дарит рестораторам </div>
       
        <div align="center">20% на первое размещение по любому пакету!  <b><b> </b></b></div>
       
        <br />
       
        <br />
       ПАКЕТ &laquo;RESTAURANT&raquo;</b></b> 
    <br />
   
    <br />
   </div>
 
  <div align="center"> 
    <div align="center">40 000 рублей в год, 25 000 рублей за 6 месяцев, 15 000 рублей за 3 месяца.</div>
   
    <div align="center"><span style="text-decoration: line-through;">32 000 рублей в год, 20 000 рублей за 6 месяцев, 12 000 рублей за 3 месяца.</span></div>
   
    <div align="center">Размещение в категории «Банкетные залы» + 5 000 рублей</div>
   
    <div align="center">Размещение заведения в рубрике «Свадьба» - 5 000 рублей за год.</div>
   
    <div> 
      <br />
     </div>
   
    <br />
   </div>
 
  <div id="content"> 
    <div id="content">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города в категории «Рестораны»; </div>
   
    <div id="content">2. Описание заведения на русском и английском языках;</div>
   
    <div id="content">3. Личный кабинет ресторатора (с доступом к редактированию и добавлением информации и новостной ленте на своей странице заведения);</div>
   
    <div id="content">4. Меню и карта бара на русском языке и английском языке (на английском языке предоставляет заказчик);</div>
   
    <div id="content">5. Фотогалерея интерьера и экстерьера заведения (до 30 штук);</div>
   
    <div id="content">6. 3д тур по заведению (В рамках проекта Ресторан.ру);</div>
   
    <div id="content">7. Размещение приложения с 3д туром «Вконтакте» в группе заказчика;</div>
   
    <div id="content">8. Размещение видео (предоставляет заказчик);</div>
   
    <div id="content">9. Субдомен www.название ресторана.restoran.ru;</div>
   
    <div id="content">10. Корпоративный почтовый ящик: название ресторана@restoran.ru;</div>
   
    <div id="content">11. Размещение в мобильной версии сайта m.restoran.ru;</div>
   
    <div id="content">12. Размещение в категории «Ланчи», «Афиша» и «Фотоотчёты»;</div>
   
    <div id="content">13. Бесплатное размещение новостей и фоторепортажей на главной странице (до 4-х в месяц); на собственной странице &ndash; без ограничений;</div>
   
    <div id="content">14. Бесплатное участие в рубрике «Мастер-класс от шеф-повара»;</div>
   
    <div id="content">15. Бесплатное обновление меню 1 раз в месяц;</div>
   
    <div id="content">16. Бесплатное размещение информации в рубрике «Работа» и «Черный список»; </div>
   
    <div id="content">17. Размещение заведения на первых страницах в категории «Рестораны»;</div>
   
    <div id="content">18. Получение информации о бронировании столиков и банкетов, через он-лайн заявку на сайте;</div>
   
    <div id="content">19. Перепост новостей, афиши и фотоотчетов заведения в соцсетях «Вконтакте», «Facebook», «Instagram»;</div>
   
    <div id="content">20. Верхний баннер 240х400 в разделе «Рестораны» сроком на 1 месяц в ротации! (кроме октября-декабря, баннер предоставляется заказчиком).</div>
   </div>
 
  <div id="content"> 
    <br />
   </div>
 <b>Пример: <a href="http://www.restoran.ru/kld/detailed/restaurants/uno_italyanskiy_restoran/" target="_blank" >ресторан &quot;UNO&quot; </a></b></div>
 
<div id="content"><span style="font-family: Georgia;">При размещении двух и более заведений одной компании, предоставляется скидка на размещение.</span></div>
 
<div id="content"> 
  <br />
 
  <div align="center"><b>ПАКЕТ «</b><b><span lang="EN-US" st="" yle="font-size: 12pt; font-family: Georgia;">BANQUET</span></b><b>»</b> 
    <br />
   
    <br />
   </div>
 
  <div align="center"> 
    <div align="center">35 000 рублей в год, 22 500 рублей за 6 месяцев, 13 500 рублей за 3 месяца.</div>
   
    <div align="center">28 000 рублей в год, 18 000 рублей за 6 месяцев, 10 800 рублей за 3 месяца.</div>
   
    <div align="center">Размещение заведения в рубрике «Свадьба» - 5 000 рублей за год.</div>
   
    <div> 
      <br />
     </div>
   
    <br />
   </div>
 
  <div id="content"> 
    <div id="content">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города в категории «Банкетные залы»;</div>
   
    <div id="content">2. Описание заведения на русском и английском языках;</div>
   
    <div id="content">3. Личный кабинет ресторатора (с доступом к редактированию и добавлением информации и новостной ленте на своей странице заведения);</div>
   
    <div id="content">4. Меню и карта бара на русском языке и английском языке (на английском языке предоставляет заказчик);</div>
   
    <div id="content">5. Фотогалерея интерьера и экстерьера заведения (до 30 штук);</div>
   
    <div id="content">6. 3д тур по заведению (В рамках проекта Ресторан.ру);</div>
   
    <div id="content">7. Размещение приложения с 3д туром «Вконтакте» в группе заказчика;</div>
   
    <div id="content">8. Размещение видео (предоставляет заказчик);</div>
   
    <div id="content">9. Субдомен www.название ресторана.restoran.ru;</div>
   
    <div id="content">10. Корпоративный почтовый ящик: название ресторана@restoran.ru;</div>
   
    <div id="content">11. Размещение в мобильной версии сайта m.restoran.ru;</div>
   
    <div id="content">12. Размещение в категории «Афиша» и «Фотоотчёты»;</div>
   
    <div id="content">13. Бесплатное размещение новостей и фоторепортажей на главной странице (до 4-х в месяц); на собственной странице – без ограничений;</div>
   
    <div id="content">14. Бесплатное участие в рубрике «Мастер-класс от шеф-повара»;</div>
   
    <div id="content">15. Бесплатное обновление меню 1 раз в месяц;</div>
   
    <div id="content">16. Бесплатное размещение информации в рубрике «Работа» и «Черный список»; </div>
   
    <div id="content">17. Размещение заведения на первых страницах в категории «Банкетные залы»;</div>
   
    <div id="content">18. Получение информации о бронировании столиков и банкетов, через он-лайн заявку на сайте;</div>
   
    <div id="content">19. Перепост новостей, афиши и фотоотчетов заведения в соцсетях «Вконтакте», «Facebook», «Instagram»;</div>
   
    <div id="content">20. Верхний баннер 240х400 в разделе «Рестораны» сроком на 1 месяц в ротации! (кроме октября-декабря, баннер предоставляется заказчиком).</div>
   </div>
 
  <br />
 <b>Пример: </b><a href="http://www.restoran.ru/kld/detailed/restaurants/logerie/" target="_blank" ><b>ресторан &quot;Мадам де Логерье&quot;</b></a><b> </b> </div>
 
<div id="content">При размещении двух и более заведений одной компании, предоставляется скидка на размещение.  </div>
 
<div id="content"> 
  <br />
 
  <br />
 
  <div align="center"><b>ПАКЕТ «</b><b><span lang="EN-US" st="" yle="font-size: 12pt; font-family: Georgia;">CAFE &amp; BAR</span></b><b>» </b></div>
 
  <div align="center"><b> 
      <br />
     </b></div>
 
  <div align="center"> 
    <div align="center">30 000 рублей в год, 20 000 рублей за 6 месяцев, 12 500 рублей за 3 месяца.</div>
   
    <div align="center">24 000 рублей в год, 16 000 рублей за 6 месяцев, 10 000 рублей за 3 месяца.</div>
   
    <div align="center">Размещение в категории «Банкетные залы» + 5 000 рублей</div>
   
    <div align="center">Размещение заведения в рубрике «Свадьба» - 5 000 рублей за год.</div>
   
    <div> 
      <br />
     </div>
   </div>
 
  <div align="center"> 
    <br />
   
    <div align="left"> 
      <div align="left"> 
        <div align="left">1. Визитная карточка ресторана: название, адрес, телефон, часы работы, кухня, средняя стоимость счета, скидки, особые предложения, размещение на карте города в категории «Рестораны»;</div>
       
        <div align="left">2. Описание заведения на русском и английском языках;</div>
       
        <div align="left">3. Личный кабинет ресторатора (с доступом к редактированию и добавлением информации и новостной ленте на своей странице заведения);</div>
       
        <div align="left">4. Меню и карта бара на русском языке и английском языке (на английском языке предоставляет заказчик);</div>
       
        <div align="left">5. Фотогалерея интерьера и экстерьера заведения (до 30 штук);</div>
       
        <div align="left">6. 3д тур по заведению (В рамках проекта Ресторан.ру);</div>
       
        <div align="left">7. Размещение приложения с 3д туром «Вконтакте» в группе заказчика;</div>
       
        <div align="left">8. Размещение видео (предоставляет заказчик);</div>
       
        <div align="left">9. Субдомен www.название ресторана.restoran.ru;</div>
       
        <div align="left">10. Корпоративный почтовый ящик: название ресторана@restoran.ru;</div>
       
        <div align="left">11. Размещение в мобильной версии сайта m.restoran.ru;</div>
       
        <div align="left">12. Размещение в категории «Ланчи», «Афиша» и «Фотоотчёты»;</div>
       
        <div align="left">13. Бесплатное размещение новостей и фоторепортажей на главной странице (до 4-х в месяц); на собственной странице – без ограничений;</div>
       
        <div align="left">14. Бесплатное участие в рубрике «Мастер-класс от шеф-повара»;</div>
       
        <div align="left">15. Бесплатное обновление меню 1 раз в месяц;</div>
       
        <div align="left">16. Бесплатное размещение информации в рубрике «Работа» и «Черный список»; </div>
       
        <div align="left">17. Размещение заведения на первых страницах в категории «Рестораны»;</div>
       
        <div align="left">18. Получение информации о бронировании столиков и банкетов, через он-лайн заявку на сайте;</div>
       
        <div align="left">19. Перепост новостей, афиши и фотоотчетов заведения в соцсетях «Вконтакте», «Facebook», «Instagram»;</div>
       
        <div align="left">20. Верхний баннер 240х400 в разделе «Рестораны» сроком на 1 месяц в ротации! (кроме октября-декабря, баннер предоставляется заказчиком).</div>
       
        <div align="left"> 
          <br />
         </div>
       
        <div align="left"><b>Пример: </b><b><a href="http://www.restoran.ru/kld/detailed/restaurants/stereocafe/" target="_blank" >Кафе «Stereo Cafe»</a></b></div>
       
        <div align="left"> 
          <br />
         </div>
       
        <div align="left">При размещении двух и более заведений одной компании, предоставляется скидка на размещение.</div>
       </div>
     </div>
   </div>
 <span style="text-align: -webkit-center;"> 
    <div st="" yle="font-weight: bold; text-align: center;"><b> 
        <br />
       </b></div>
   
    <div st="" yle="font-weight: bold; text-align: center;"><b>ПАКЕТ «</b><b><span lang="EN-US" st="" yle="font-size: 12pt; font-family: Georgia;">BUSINESS</span></b><b>» </b></div>
   
    <div st="" yle="font-weight: bold; text-align: center;"><b> 
        <br />
       </b></div>
   
    <div style="text-align: center;"> 
      <div>10 000 рублей в год, 6 000 рублей за 6 месяцев, 4 000 рублей за 3 месяца.</div>
     
      <div>8 000 рублей в год, 4 800 рублей за 6 месяцев, 3 200 рублей за 3 месяца.</div>
     
      <div> 
        <br />
       </div>
     
      <div style="text-align: left;"> 
        <div>1. Визитная карточка компании: название, адрес, телефон, сайт, почтовый ящик;</div>
       
        <div>2. Описание компании на русском и английском языках;</div>
       
        <div>3. Субдомен www.название компании.restoran.ru;</div>
       
        <div>4. Бесплатное размещение новостей на главной странице (до 4-х в месяц);</div>
       
        <div>5. Перепост новостей компании в соцсетях «Вконтакте», «Facebook», «Instagram».</div>
       
        <div>6. Размещение информации в рубрике «Работа».</div>
       </div>
     
      <div style="text-align: left;"> 
        <br />
       </div>
     </div>
   </span> 
  <div align="center"><b>БАННЕРНАЯ РЕКЛАМА</b> 
    <br />
   
    <br />
   
    <div align="left"> 
      <div align="left">Растяжка баннер вверху страницы 960х90px (статика в ротации до 5 баннеров)</div>
     
      <div align="left">1 500 рублей неделя / 5 000 рублей в месяц</div>
     
      <div align="left"> 
        <br />
       </div>
     
      <div align="left">Баннер справа вверху 240х400px  (статика в ротации до 10 баннеров)</div>
     
      <div align="left">1 250 рублей неделя / 4 000 рублей в месяц</div>
     
      <div align="left"> 
        <br />
       </div>
     
      <div align="left">Баннер справа внизу 240х400px  (статика в ротации до 10 баннеров)</div>
     
      <div align="left">1 000 рублей неделя / 3 000 рублей в месяц</div>
     
      <div align="left"> 
        <br />
       </div>
     
      <div align="left">Растяжка баннер внизу страницы  728х90px  (статика в ротации до 5 баннеров)</div>
     
      <div align="left">1 000 рублей неделя / 3 000 рублей в месяц</div>
     
      <div> 
        <br />
       </div>
     </div>
   </div>
 
  <div align="center"><b>Дополнительные услуги: 
      <br />
     
      <br />
     </b></div>
 
  <div id="content">Агентская комиссия за бронирование столов или банкетов через сайт Ресторан.ру, составляет 10% от суммы счета заказа клиента.</div>
 
  <div id="content">Репортажная Фото и видеосъемка мероприятий и событий &mdash; 1 500 рублей</div>
 
  <div id="content">Участие в авторской статье — 1 000 рублей</div>
 
  <div id="content">Изготовление флэш-баннеров — от 500 рублей</div>
 
  <div id="content">Съемка 3д тура заведения — от 3 000 рублей</div>
 
  <div id="content"> 
    <br />
   </div>
 
  <div id="content">НДС не облагается. </div>
 
  <div> 
    <br />
   </div>
 <b>Мы с удовольствием ответим на все Ваши вопросы: </b> 
  <br />
 
  <div id="content">По вопросам рекламы:</div>
 
  <div id="content">+7 (911) 456-72-79 (Дмитрий)</div>
 
  <div id="content">e-mail: prkld@restoran.ru</div>
 
  <div id="content"> 
    <br />
   </div>
 
  <div id="content">+7 (952) 110-75-55 (Константин) </div>
 
  <div id="content">e-mail: adminkld@restoran.ru </div>
 
  <div id="content"> 
    <br />
   </div>
 
  <div id="content">С уважением к Вам и Вашему бизнесу. </div>
 
  <div id="content">Команда портала Restoran.ru Калининград   </div>
 
  <div> 
    <br />
   </div>
 
  <br />
 </div>
 
<div mouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(event)" mouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" on="" style="display: none;" id="skype_c2c_menu_container" class="skype_c2c_menu_container"> 
  <div class="skype_c2c_menu_click2call"><a class="skype_c2c_menu_click2call_action" id="skype_c2c_menu_click2call_action" >Позвонить</a></div>
 
  <div class="skype_c2c_menu_click2sms"><a class="skype_c2c_menu_click2sms_action" id="skype_c2c_menu_click2sms_action" >Отправить SMS</a></div>
 
  <div class="skype_c2c_menu_add2skype"><a class="skype_c2c_menu_add2skype_text" id="skype_c2c_menu_add2skype_text" >Добавить в Skype</a></div>
 
  <div class="skype_c2c_menu_toll_info"><span class="skype_c2c_menu_toll_callcredit">Call with Skype credits</span><span class="skype_c2c_menu_toll_free">БЕСПЛАТНО в Skype</span></div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>