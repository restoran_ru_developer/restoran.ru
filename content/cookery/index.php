<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Кулинария");
//s$arInterviewIB = getArIblock("interview", CITY_ID);
?>    
<div class="block">
    <div class="left-side">
        <!--<h1>Кулинария</h1>-->
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"spec_block",
			Array(
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "cookery",
				"IBLOCK_ID" => 72,
				"NEWS_COUNT" => "1",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "ID",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array("DETAIL_PICTURE","TAGS"),
				"PROPERTY_CODE" => array(
					"RECEPTS",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j F Y G:i",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "Y",
				"CACHE_TIME" => "7200",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			),
		false
		);

        ?>
        <div class="restoran-detail network-design">
            <div class="tab-str-wrapper">
                <div class="nav-tab-str-title">Рецепты редактора</div>
<!--                <ul class="nav nav-tabs">-->
<!--                    <li class="active"><a href="#editor-recipes" data-toggle="tab">Рецепты редактора</a></li>-->
<!--                </ul>-->

                <div class="tabs-center-line"></div>
            </div>
        </div>

        <div class="tab-content">            
            <div class="tab-pane active sm" id="editor-recipes"> 
                <?$APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "new_rest_main",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "cookery",
                            "IBLOCK_ID" => 139,
                            "NEWS_COUNT" => 4,
                            "SORT_BY1" => "created_date",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "arrFilterTop4",
                            "FIELD_CODE" => array("CREATED_BY"),
                            "PROPERTY_CODE" => array("ratio","reviews_bind","COMMENTS",'prig_time','SHOW_COUNTER'),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "200",
                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "57432",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",//a
                            "CACHE_TIME" => "36000004",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "LINK_NAME" => "Рецепты редактора",
                            "LINK" => "/content/cookery/editor/",
                            "A" => "a",
                            "WITH_LAST_DOUBLE_NEWS"=>'Y'
                    ),
                false
                );
                ?>
            </div>
        </div>


        <div class="restoran-detail network-design">
            <div class="tab-str-wrapper">
                <div class="nav-tab-str-title">Рецепты пользователей</div>
<!--                <ul class="nav nav-tabs">-->
<!--                    <li class="active"><a href="#editor-recipes" data-toggle="tab">Рецепты пользователей</a></li>-->
<!--                </ul>-->

                <div class="tabs-center-line"></div>
            </div>
        </div>



        <div class="tab-content">            
            <div class="tab-pane active sm" id="users-recipes"> 
                <?$APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "new_rest_main",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "cookery",
                            "IBLOCK_ID" => 139,
                            "NEWS_COUNT" => 3,
                            "SORT_BY1" => "created_date",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "arrFilterTop4",
                            "FIELD_CODE" => array("CREATED_BY"),
                            "PROPERTY_CODE" => array("ratio","reviews_bind","COMMENTS",'SHOW_COUNTER'),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "57431",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000003",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "LINK_NAME" => "Рецепты пользователей", 
                            "LINK" => "/content/cookery/users/",
                            "A" => "a"
                    ),
                false
                );
                ?>                
            </div>
        </div>        


        <div class="restoran-detail network-design">
            <div class="tab-str-wrapper">
                <div class="nav-tab-str-title">Советы</div>
<!--                <ul class="nav nav-tabs">-->
<!--                    <li class="active"><a href="#tips" data-toggle="tab">Советы</a></li>-->
<!--                </ul>-->

                <div class="tabs-center-line"></div>
            </div>
        </div>

        <div class="tab-content">            
            <div class="tab-pane active sm" id="tips"> 
                <?$APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "new_rest_main",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "cookery",
                            "IBLOCK_ID" => 227,
                            "NEWS_COUNT" => 3,
                            "SORT_BY1" => "date_created",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "arrFilterTop4",
                            "FIELD_CODE" => array("CREATED_BY",'SHOW_COUNTER'),
                            "PROPERTY_CODE" => array("comments"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000003",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "LINK" => "/content/cookery/sovety/",
                            "LINK_NAME" => "ВСЕ СОВЕТЫ",
                        "A" => "a"
                    ),
                false
                );
                ?>
            </div>
        </div>

        <div class="restoran-detail network-design">
            <div class="tab-str-wrapper">
                <div class="nav-tab-str-title">Диеты</div>
<!--                <ul class="nav nav-tabs">-->
<!--                    <li class="active"><a href="#diets" data-toggle="tab">Диеты</a></li>-->
<!--                </ul>-->

                <div class="tabs-center-line"></div>
            </div>
        </div>


        <div class="tab-content">            
            <div class="tab-pane active sm" id="diets"> 
                <?$APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "new_rest_main",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "cookery",
                            "IBLOCK_ID" => 228,
                            "NEWS_COUNT" => 3,
                            "SORT_BY1" => "date_created",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "arrFilterTop4",
                            "FIELD_CODE" => array("CREATED_BY",'SHOW_COUNTER'),
                            "PROPERTY_CODE" => array("comments"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000003",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "LINK" => "/content/cookery/diety/",
                            "LINK_NAME" => "ВСЕ ДИЕТЫ",
                        "A" => "a"
                    ),
                false
                );
                ?>
            </div>
        </div>      		
    </div>    
    <div class="right-side">
        <!--        <a class="add_recipe" href="/content/cookery/add_recipe.php">+ Добавить рецепт</a>-->            
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
            false
        );?>
        <div id="subscribe">
            <?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "main_page_subscribe_new", array(
                "USE_PERSONALIZATION" => "Y",
                "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                "SHOW_HIDDEN" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_NOTES" => ""
            ),
                false,
                array(
                    "ACTIVE_COMPONENT" => "Y"
                )
            );?>
        </div>

        <div class="title text-center"><a href="/<?=CITY_ID?>/news/mcfromchif/">Рецепты от шеф-повара</a></div>
                <?$APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "interview_one_with_border",
                    Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "cookery",
                            "IBLOCK_ID" => 145,
                            "NEWS_COUNT" => 3,
                            "SORT_BY1" => "ID",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "",
                            "SORT_ORDER2" => "",
                            "FILTER_NAME" => "",
                            "FIELD_CODE" => array("CREATED_BY"),
                            "PROPERTY_CODE" => array("COMMENTS"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "120",
                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",//a
                            "CACHE_TIME" => "36000007",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "ALL" => "/".CITY_ID."/news/mcfromchif/",
                            "ALL_D" => "ВСЕ Рецепты"
                    ),
                false
                );
                ?>

        <div class="tags">
            <?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                "SORT" => "CNT",
                "PAGE_ELEMENTS" => "20",
                "PERIOD" => "",
                "URL_SEARCH" => "/search/index.php",
                "TAGS_INHERIT" => "Y",
                "CHECK_DATES" => "Y",
                "FILTER_NAME" => "",
                "arrFILTER" => array(
                                0 => "iblock_cookery",
                ),
                "arrFILTER_iblock_cookery" => array(
                                0 => "139",
                ),
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "FONT_MAX" => "24",
                "FONT_MIN" => "12",
                "COLOR_NEW" => "24A6CF",
                "COLOR_OLD" => "24A6CF",
                "PERIOD_NEW_TAGS" => "",
                "SHOW_CHAIN" => "Y",
                "COLOR_TYPE" => "N",
                "WIDTH" => "100%",
                "SEARCH_IN" => "recepts"
                ),
                $component
            );?> 
        </div>        			
                <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
    </div>
    <div class="clearfix"></div>
    <div class="preview_seo_text" style="clear: both;">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
    false
    );?>      
</div>   
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>