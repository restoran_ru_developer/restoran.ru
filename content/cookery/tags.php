<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Популярные теги");
?>
<div class="block">
        <div class="left-side">
        <h1>Популярные теги</h1>
        <form action="/search/index.php" />
        <div class="question">
<!--            <span class="uppercase">Поиск по тегам: </span>-->
            <input name="tags" type="text" class="inputtext" size="90" placeholder="Например, капуста" style="font-style:italic" />
            <input type="hidden" value="recepts" name="search_in" />
            <input type="submit" class="light_button" value="Поиск"/>
        </div>         
        </form>
        <br />
        <i>
        <?
            $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                "SORT" => "CNT",
                "PAGE_ELEMENTS" => "150",
                "PERIOD" => "",
                "URL_SEARCH" => "/search/index.php",
                "TAGS_INHERIT" => "Y",
                "CHECK_DATES" => "Y",
                "FILTER_NAME" => "",
                "arrFILTER" => array(
                                0 => "iblock_cookery",
                ),
                "arrFILTER_iblock_".$_REQUEST["IBLOCK_TYPE_ID"] => array(
                                0 => 139,
                                0 => 145,
                ),
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "FONT_MAX" => "40",
                "FONT_MIN" => "12",
                "COLOR_NEW" => "24A6CF",
                "COLOR_OLD" => "24A6CF",
                "PERIOD_NEW_TAGS" => "",
                "SHOW_CHAIN" => "Y",
                "COLOR_TYPE" => "N",
                "SEARCH_IN" => "recepts",
                "WIDTH" => "100%"
                ),
                $component
            );?> 
            </i>
        <div class="clear"></div>
    </div>
    <div class="right-side">
        <a class="add_recipe" href="/content/cookery/add_recipe.php">+ Добавить рецепт</a>
    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
       <!-- <div id="search_article">
            <?/*$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.form",
                    "main_page_subscribe",
                    Array(
                            "USE_PERSONALIZATION" => "Y",
                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                            "SHOW_HIDDEN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => ""
                    ),
            false
            );*/?>           
        </div>-->
        <!--<br />
		<div class="top_block">Видео-рецепты</div>		
		<?
                   /* $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 139,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => 57429,
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );*/
                ?>
		<div class="black_hr"></div>
		<div align="right"><a class="uppercase" href="/content/cookery/videoretsepty/">ВСЕ ВИДЕО</a></div>-->
        <div class="title">Топ-3 рецептов</div>
        <?
        $obCache = new CPHPCache;
        $life_time = 8*60*60;
        $cache_id = "cookerytop3";
        if($obCache->InitCache($life_time, $cache_id, "/")) :
            $vars = $obCache->GetVars();
            $top3 = $vars["TOP3"];
        else :
            CModule::IncludeModule("iblock");
            $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>139,"CODE"=>"top3ru","ACTIVE"=>"Y","ACTIVE_DATE"=>"Y"),false,Array("nTopCount"=>1));
            if ($ar = $res->Fetch())
            {
                $db_props = CIBlockElement::GetProperty($arCookID, $ar["ID"], "", "", Array("CODE"=>"recepts"));
                while ($ob = $db_props->Fetch())
                {
                    $top3[] = $ob["VALUE"];
                }
            }
        endif;
        if($obCache->StartDataCache()):
            $obCache->EndDataCache(array(
                "TOP3"    => $top3
            ));
        endif;
        ?>
        <?
        if (count($top3)>1)
        {
            global $arrFilterTop3;
            $arrFilterTop3["ID"] = $top3;
        }
        $_REQUEST["arrFilter_pf"] = array();
        $APPLICATION->IncludeComponent(
            "restoran:catalog.list",
            "interview_one_with_border",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "cookery",
                "IBLOCK_ID" => 139,
                "NEWS_COUNT" => (count($top3)>1) ? 9 : 3,
                "SORT_BY1" => "shows",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arrFilterTop3",
                "FIELD_CODE" => array("CREATED_BY"),
                "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "200",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
        );
        ?>
        <?
//                    $APPLICATION->IncludeComponent(
//                        "restoran:catalog.list",
//                        "cook_one",
//                        Array(
//                                "DISPLAY_DATE" => "N",
//                                "DISPLAY_NAME" => "Y",
//                                "DISPLAY_PICTURE" => "Y",
//                                "DISPLAY_PREVIEW_TEXT" => "N",
//                                "AJAX_MODE" => "N",
//                                "IBLOCK_TYPE" => "cookery",
//                                "IBLOCK_ID" => 139,
//                                "NEWS_COUNT" => 3,
//                                "SORT_BY1" => "ID",
//                                "SORT_ORDER1" => "DESC",
//                                "SORT_BY2" => "",
//                                "SORT_ORDER2" => "",
//                                "FILTER_NAME" => "arrFilterTop4",
//                                "FIELD_CODE" => array("CREATED_BY"),
//                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
//                                "CHECK_DATES" => "Y",
//                                "DETAIL_URL" => "",
//                                "PREVIEW_TRUNCATE_LEN" => "200",
//                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
//                                "SET_TITLE" => "N",
//                                "SET_STATUS_404" => "N",
//                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                                "ADD_SECTIONS_CHAIN" => "N",
//                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                                "PARENT_SECTION" => "",
//                                "PARENT_SECTION_CODE" => "",
//                                "CACHE_TYPE" => "A",
//                                "CACHE_TIME" => "36000000",
//                                "CACHE_FILTER" => "Y",
//                                "CACHE_GROUPS" => "Y",
//                                "DISPLAY_TOP_PAGER" => "N",
//                                "DISPLAY_BOTTOM_PAGER" => "N",
//                                "PAGER_TITLE" => "Новости",
//                                "PAGER_SHOW_ALWAYS" => "N",
//                                "PAGER_TEMPLATE" => "",
//                                "PAGER_DESC_NUMBERING" => "N",
//                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                                "PAGER_SHOW_ALL" => "N",
//                                "AJAX_OPTION_JUMP" => "N",
//                                "AJAX_OPTION_STYLE" => "Y",
//                                "AJAX_OPTION_HISTORY" => "N"
//                        ),
//                    false
//                    );
                ?>	
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>