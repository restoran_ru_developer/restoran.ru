<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расширенный поиск");
?>
<div class="block">	    
    <div class="left-side">
	<h1>Расширенный поиск</h1>
        <div class="clearfix"></div>
            <div id="tabs_block2">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#rest" data-toggle="tab">
                        Рестораны                        
                    </a>
                </li>
                <li>
                    <a href="#banket" class="ajax" data-toggle="tab" data-href="/bitrix/components/restoran/catalog.filter/templates/.default/banket.php?CITY_ID=<?=CITY_ID?>">                        
                        Банкетные залы                        
                    </a>
                </li>
            </ul>            
            <div class="tab-content">
                <div class="tab-pane active">
                    <?
                    $arRestIB = getArIblock("catalog", CITY_ID);                    
                    if (CITY_ID=="msk")
                        $prop = array("type", "subway", "out_city", "administrative_distr", "average_bill", "kitchen","menu","children", "proposals", "features","entertainment","music","ideal_place_for","wi_fi","hrs_24");
                    else
                        $prop = array("type", "subway", "out_city", "area", "average_bill", "kitchen","menu","children", "proposals", "features","entertainment","music","ideal_place_for","wi_fi","hrs_24");
                    $_REQUEST["CATALOG_ID"]="restaurants";
                    $APPLICATION->IncludeComponent(
                            "restoran:catalog.filter",
                            "",
                            Array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arRestIB["ID"],
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array(),
                                "PROPERTY_CODE" => $prop,
                                "PRICE_CODE" => array(),
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "360000",
                                "CACHE_GROUPS" => "N",
                                "LIST_HEIGHT" => "5",
                                "TEXT_WIDTH" => "20",
                                "NUMBER_WIDTH" => "5",
                                "SAVE_IN_SESSION" => "N",
                                "SECTION" => 32
                            )
                    );?>
                </div>
               <div class="tab-pane"></div>                
            </div>
        </div> 	
	</div>
	<div class="right-side">                        
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_2_main_page",
                                "NOINDEX" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
            );?>
            <div class="title">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);                       
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arPFilter["!PREVIEW_PICTURE"] = false;
            $arPFilter["!PROPERTY_restoran_ratio"] = false;  
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "PROPERTY_restoran_ratio",
                    "SORT_ORDER1" => "asc,nulls",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "10",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "recomended",
                    "REST_PROPS" => "Y"
                ),
            false
            );?>            
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>       
        <div class="title">Популярные</div>
            <?
            unset($arPFilter["!PROPERTY_restoran_ratio"]);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "PROPERTY_rating_date",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "PROPERTY_stat_day",
                    "SORT_ORDER2" => "DESC",
                    "SORT_BY3" => "NAME",
                    "SORT_ORDER3" => "ASC",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "popular",
                    "REST_PROPS" => "Y"
                ),
            false
            );?>                
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?> 
        </div>
    </div>
    <div class="clearfix"></div>    
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
    false
    );?>      
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>