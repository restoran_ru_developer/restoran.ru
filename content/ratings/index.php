<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Рейтинги");
$arRestIB = getArIblock("catalog", CITY_ID);
?>
<div class="block">	
    <div class="left-side">

        <div class="restoran-detail network-design">
            <div class="tab-str-wrapper">
                <div class="nav-tab-str-title"><h1><?=$APPLICATION->ShowTitle(false)?></h1></div>
                <ul class="nav nav-tabs history">
                    <li ><a href="#recomended" rel="nofollow" data-toggle="tab">Рекомендуем</a><span>|</span></li>
                    <li ><a href="#ratio" rel="nofollow" data-toggle="tab">Банкетные залы</a><span>|</span></li>
                    <li class="active"><a href="#popular" rel="nofollow" data-toggle="tab">Просматриваемые</a><span>|</span></li>

                </ul>
                <div class="tabs-center-line"></div>
            </div>
        </div>

<!--            <ul class="nav nav-tabs history">-->
<!--                <li class="active"><a href="#popular" rel="nofollow" data-toggle="tab">Самые просматриваемые</a></li>-->
<!--                <li><a href="#ratio" rel="nofollow" data-toggle="tab">Банкетные залы</a></li>-->
<!--                <li><a href="#recomended" rel="nofollow" data-toggle="tab">Рекомендуем</a></li>-->
<!--            </ul>-->
            <div class="tab-content">
                <div class="tab-pane sm active" id="popular">            
                    <?
                    $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
                    $APPLICATION->IncludeComponent("restoran:catalog.list", "ratings", array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arRestIB["ID"],
                            "PARENT_SECTION_CODE" => "restaurants",
                            "NEWS_COUNT" => "19",
                            "SORT_BY1" => "PROPERTY_rating_date",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "PROPERTY_stat_day",
                            "SORT_ORDER2" => "DESC",
                            "SORT_BY3" => "NAME",
                            "SORT_ORDER3" => "ASC",
                            "FILTER_NAME" => "arPFilter",
                            "PROPERTY_CODE" => array(
                                    0 => "phone",
                                    1 => "address",
                                    2 => "subway",
                                    3 => "COMMENTS",
                                    4 => "photos",
                                    5 => "rating_date",
                                    6 => "stat_day",
                                    7 => "ratio",
                                    8 => "STREET"
                            ),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "Y",
                            "AJAX_OPTION_SHADOW" => "N",
                            "AJAX_OPTION_JUMP" => "Y",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "Y",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "3602",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "PREVIEW_TRUNCATE_LEN" => "100",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Рестораны",
                            "PAGER_SHOW_ALWAYS" => "Y",
                            "PAGER_TEMPLATE" => "search_rest_list",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_OPTION_ADDITIONAL" => ""
                            ),
                            false
                    );?>
                </div>
                <div class="tab-pane sm" id="ratio">  
                    <?
                    $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
                    //$arPFilter["!SECTION_ID"] = false;
                    $APPLICATION->IncludeComponent("restoran:catalog.list", "ratings", array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arRestIB["ID"],
                            "PARENT_SECTION_CODE" => "banket",
                            "NEWS_COUNT" => "19",
                            "SORT_BY1" => "PROPERTY_rating_date",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "PROPERTY_stat_day",
                            "SORT_ORDER2" => "DESC",
                            "SORT_BY3" => "NAME",
                            "SORT_ORDER3" => "ASC",
                            "FILTER_NAME" => "arPFilter",
                            "PROPERTY_CODE" => array(
                                    0 => "phone",
                                    1 => "address",
                                    2 => "subway",
                                    4 => "photos",
                                    5 => "ratio",
                                    6 => "COMMENTS",
                                8 => "STREET"
                            ),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "Y",
                            "AJAX_OPTION_SHADOW" => "Y",
                            "AJAX_OPTION_JUMP" => "Y",
                            "AJAX_OPTION_STYLE" => "N",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "3602",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "PREVIEW_TRUNCATE_LEN" => "100",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Рестораны",
                            "PAGER_SHOW_ALWAYS" => "Y",
                            "PAGER_TEMPLATE" => "search_rest_list",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_OPTION_ADDITIONAL" => ""
                            ),
                            false
                    );?>

                </div>                
                <div class="tab-pane sm" id="recomended"> 
                    <?$APPLICATION->IncludeComponent("restoran:catalog.list", "ratings", array(
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "PARENT_SECTION_CODE" => "restaurants",
                        "NEWS_COUNT" => "19",
                        "SORT_BY1" => "PROPERTY_restoran_ratio",
                        "SORT_ORDER1" => "asc,nulls",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arPFilter",
                        "PROPERTY_CODE" => array(
                                0 => "phone",
                                1 => "address",
                                2 => "subway",
                                3 => "COMMENTS",
                                4 => "photos",
                                5 => "ratio",
                            8 => "STREET"
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_SHADOW" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000004",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "PREVIEW_TRUNCATE_LEN" => "100",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Купоны",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_OPTION_ADDITIONAL" => ""
                        ),
                        false
                    );?>
                </div>
            </div>                        
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "bottom_content_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
            );?>
	</div>
	<div class="right-side">


            <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_2_main_page",
                            "NOINDEX" => "Y",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
            );?>
            <div class="title">Новые на сайте</div>
            <?$arIB = getArIblock("catalog", CITY_ID);
                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "recomended",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arIB["ID"],
                        "NEWS_COUNT" => "5",
                        "SORT_BY1" => "CREATED_DATE",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("RATIO","COMMENTS"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "NEWEST" => "Y",
                        "REST_PROPS"=>"Y"
                    ),
                false
                );
        ?>
             <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
        <br>
<!--        --><?//if(CITY_ID=='spb'||CITY_ID=='msk'||CITY_ID=='rga'||CITY_ID=='urm'):?>
<!--            --><?//$APPLICATION->IncludeComponent(
//                "bitrix:main.include",
//                "",
//                Array(
//                    "AREA_FILE_SHOW" => "file",
//                    "PATH" => SITE_TEMPLATE_PATH."/include_areas/new-order-form-with-city.php",
//                    "EDIT_TEMPLATE" => ""
//                ),
//                false
//            );?>
<!--        --><?//endif?>
<!--        --><?//$APPLICATION->IncludeFile(
//		$APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
//		Array(),
//		Array("MODE"=>"html")
//	);?>
             <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_3_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
            false
            );?>
    </div>
    <div class="clearfix"></div>
    <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
    false
    );?>
    <div class="preview_seo_text">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>