<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.min.js')?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.tools.validator.js')?>
<?$APPLICATION->IncludeComponent("restoran:restoraunts.detail", ".default", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => CITY_ID,
	"ELEMENT_ID" => "",
	"ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
	"CHECK_DATES" => "Y",
	"PROPERTY_CODE" => array(
		0 => "type",
		1 => "average_bill",
		2 => "kitchen",
		3 => "opening_hours",
		4 => "phone",
		5 => "administrative_distr",
		6 => "area",
		7 => "address",
		8 => "subway",
		9 => "number_of_rooms",
		10 => "music",
                11 => "clothes",
		12 => "proposals",
		13 => "credit_cards",
		14 => "discouncards",
		15 => "bankets",
		16 => "touristgroups_1",
		17 => "banketnayasluzhba",
		18 => "kolichestvochelovek",
		19 => "stoimostmenyu",
		20 => "max_check",
		21 => "entertainment",
		22 => "wi_fi",
		23 => "hrs_24",
		24 => "parking",
		25 => "features",
		26 => "out_city",
		27 => "min_check",
		//28 => "menu",
		29 => "children",
		30 => "ideal_place_for",
		31 => "offers",
		32 => "email",
		33 => "discounts",
		34 => "landmarks",
		35 => "map",
		36 => "ratio",
		//37 => "videopanoramy",
		//38 => "photos",
		39 => "okrugdel",
		40 => "kuhnyadostavki",
		41 => "viduslug",
		42 => "add_props",
		//43 => "reviews_bind",
		//44 => "user_bind",
		//45 => "news_bind",
		//46 => "videonews_bind",
		//47 => "afisha_bind",
		//48 => "blog_bind",
		//49 => "top_spec_place",
		//50 => "str_spec_place",
		//51 => "overviews_bind",
		//52 => "interviews_bind",
		//53 => "photoreports_bind",
	),
	"ADD_REVIEWS" => "Y",
	"REVIEWS_BLOG_ID" => "3",
	"SIMILAR_OUTPUT" => "Y",
	"SIMILAR_PROPERTIES" => array(
		0 => "kitchen",
		1 => "average_bill",
		2 => "",
	),
	"IBLOCK_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"USE_PERMISSIONS" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Страница",
	"PAGER_TEMPLATE" => "",
	"PAGER_SHOW_ALL" => "Y",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"USE_SHARE" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?> 
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>