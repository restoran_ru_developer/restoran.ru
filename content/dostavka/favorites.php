<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
?>
<div id="news">	
    <div class="left">
		<h1>Избранное</h1>
        <ul id="poster_tabs" ajax="ajax" ajax_url="/bitrix/templates/main/components/restoran/favorite.list/.default/ajax.php?part=">
            <li><a class="big" href="catalog">Рестораны</a></li>
            <li><a class="big" href="kupons">Купоны</a></li>
            <li><a class="big" href="recipes">Книга рецептов</a></li>
			<li><a class="big" href="birzha">Биржа труда</a></li>
        </ul>
        <!-- tab "panes" -->
        <div class="poster_panes">
           <div class="poster_pane"></div>
		   <div class="poster_pane"></div>
		   <div class="poster_pane"></div>
		   <div class="poster_pane"></div>
		 </div>
		 <div class="clear"></div>
	</div>
	<div class="right">
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
	</div>
	<div class="clear"></div>
	<img src="/bitrix_personal/templates/main/images/top_baner.png">
	<br /><Br />
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>