<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
$arOverviewsIB = getArIblock("reviews", CITY_ID);
?><?/*$APPLICATION->IncludeComponent("restoran:blog.new_comments", ".default", array(
	"GROUP_ID" => "9",
	"BLOG_URL" => "",
	"COMMENT_COUNT" => "20",
	"MESSAGE_LENGTH" => "100",
	"POST_ID" => $_REQUEST["POST_ID"],
	"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
	"PATH_TO_BLOG" => "",
	"PATH_TO_POST" => "",
	"PATH_TO_USER" => "",
	"PATH_TO_GROUP_BLOG_POST" => "",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "86400",
	"PATH_TO_SMILE" => "",
	"BLOG_VAR" => "",
	"POST_VAR" => "",
	"USER_VAR" => "",
	"PAGE_VAR" => "",
	"SEO_USER" => "N"
	),
	false
);*/?>
<div id="content">
	<div class="left" style="width:720px;">
		<h1><?=$APPLICATION->ShowTitle(false)?></h1>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"reviews",
			Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "reviews",
				"IBLOCK_ID" => $arOverviewsIB["ID"],
				"NEWS_COUNT" => "10",
				"SORT_BY1" => "DATE_CREATE",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array("DATE_CREATE","CREATED_BY"),
				"PROPERTY_CODE" => array("ratio"),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j M Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			),
		   false
		);?>
	</div>
	<div class="right">
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>
	</div>
	<div class="clear"></div>
	<img src="/bitrix_personal/templates/main/images/top_baner.png">
	<br /><Br />
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>