<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");


?>
<script>
    $(document).ready(function(){
        $(".baner2 object").css("z-index","9998");
    });
</script>
<style>
    .baner2 object
    {
        z-index:9999;
    }
</style>


<?
global $USER;
//if($USER->IsAdmin()):?>
    <script>
        $(function(){
            var sessionLat = '<?= $_SESSION['lat'] ?>';
            var sessionLon = '<?= $_SESSION['lon'] ?>';
            var options_for_get_position = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            };
            var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';

            setTimeout(get_location_a, 1000);

            function get_location_a()
            {
                if (navigatorOn != 'Y') {
                    console.log('navigatorOff');
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(success_a, error_a, options_for_get_position);
                    } else {
                        console.log("Ваш браузер не поддерживает\nопределение местоположения");
                        return false;
                    }
                }
                else {
                    var lat_lon_list = new Object();
                    $('.pull-left').each(function(indx, element){
                        lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                    })
//                    console.log(lat_lon_list);
                    $.ajax({
                        type: "POST",
                        url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                        data: {
                            lat_lon_list: lat_lon_list
//                            who: '321'
                        },
                        dataType:'json',
                        success: function(data) {
                            $('.pull-left').each(function(indx, element){
                                $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                            })
                            $('.distance-place').show();
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }
            }
            function success_a(position) {
                sessionLat = position.coords.latitude;
                sessionLon = position.coords.longitude;

                console.log(sessionLat,'sessionLat');
                console.log(sessionLon,'sessionLon');
                $.ajax({
                    type: "POST",
                    url: "<?=SITE_TEMPLATE_PATH?>/ajax/ajax-coordinates.php",
                    data: {
                        lat:sessionLat,
                        lon:sessionLon
                    },
                    success: function(data) {
                        var lat_lon_list = new Object();
                        $('.pull-left').each(function(indx, element){
                            lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                        })
                        console.log(lat_lon_list);
                        $.ajax({
                            type: "POST",
                            url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                            data: {
                                lat_lon_list: lat_lon_list
//                            who: '321'
                            },
                            dataType:'json',
                            success: function(data) {
                                $('.pull-left').each(function(indx, element){
                                    $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                                })
                                $('.distance-place').show();
                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });

                    }
                });
            }
            function error_a()
            {
                console.log("Невозможно определить местоположение");
                return false;
            }
        })
    </script>
<?//endif?>
<?
global $USER;
$arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");
FirePHP::getInstance()->info($_REQUEST["SECTION_CODE"]);
FirePHP::getInstance()->info($arIB["ID"]);
if ($arIB["ID"]) {
    ?>    
    <div class="left" style="">
        <?                           
        $APPLICATION->IncludeComponent("restoran:catalog.list",
            "articles", 
            array(
            "IBLOCK_TYPE" => "special_projects",           
            "IBLOCK_ID" => $arIB["ID"],
            "PARENT_SECTION_CODE" => "articles",
            "NEWS_COUNT" => 12,
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "NAME",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "PROPERTY_CODE" => array(
                0 => "RESTORAN",
                1 => "",
                2 => "",
                3 => "",
            ),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",//a
            "CACHE_TIME" => "36000003",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => "120",
            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "search_rest_list1",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
                ), false
        );
        ?> 
        <div class="preview_seo_text">
            <? $APPLICATION->ShowViewContent("preview_seo_text"); ?>
        </div>
    </div>

<div class="clear"></div>
    <?
} else {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
}
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>