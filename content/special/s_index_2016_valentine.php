<?if($_REQUEST['ajax']!='Y'):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?endif?>

<?if($_REQUEST['ajax']!='Y'):?>
<script>
    $(document).ready(function(){
        $(".baner2 object").css("z-index","9998");
    });
</script>
<style>
    .baner2 object
    {
        z-index:9999;
    }
</style>
<?endif?>



<?
global $USER;
//if($USER->IsAdmin()):?>
    <script>
        $(function(){
            var sessionLat = '<?= $_SESSION['lat'] ?>';
            var sessionLon = '<?= $_SESSION['lon'] ?>';
            var options_for_get_position = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            };
            var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';

//            setTimeout(get_location_a, 1000);

            function get_location_a()
            {
                if (navigatorOn != 'Y') {
                    console.log('navigatorOff');
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(success_a, error_a, options_for_get_position);
                    } else {
                        console.log("Ваш браузер не поддерживает\nопределение местоположения");
                        return false;
                    }
                }
                else {
                    var lat_lon_list = new Object();
                    $('.pull-left:not(.banner-in-list-place)').each(function(indx, element){
                        lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                    })
                    console.log(lat_lon_list);
                    $.ajax({
                        type: "POST",
                        url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                        data: {
                            lat_lon_list: lat_lon_list
//                            who: '321'
                        },
                        dataType:'json',
                        success: function(data) {
                            console.log(data);
                            if(data){
                                $('.pull-left').each(function(indx, element){
                                    $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                                })
                                $('.distance-place').show();
                            }
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }
            }
            function success_a(position) {
                sessionLat = position.coords.latitude;
                sessionLon = position.coords.longitude;

//                console.log(sessionLat,'sessionLat');
//                console.log(sessionLon,'sessionLon');
                $.ajax({
                    type: "POST",
                    url: "<?=SITE_TEMPLATE_PATH?>/ajax/ajax-coordinates.php",
                    data: {
                        lat:sessionLat,
                        lon:sessionLon
                    },
                    success: function(data) {
                        var lat_lon_list = new Object();
                        $('.pull-left').each(function(indx, element){
                            lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                        })
                        console.log(lat_lon_list);
                        $.ajax({
                            type: "POST",
                            url: "<?=SITE_TEMPLATE_PATH?>/ajax/get-distance.php",
                            data: {
                                lat_lon_list: lat_lon_list
//                            who: '321'
                            },
                            dataType:'json',
                            success: function(data) {
                                if(data) {
                                    $('.pull-left').each(function (indx, element) {
                                        $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                                    })
                                    $('.distance-place').show();
                                }
                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });

                    }
                });
            }
            function error_a(error)
            {
                console.log(error);
                return false;
            }
        })
    </script>
<?//endif?>


<?

$arIB = getArIblock("special_projects", CITY_ID, $_REQUEST["SECTION_CODE"]."_");
if ($arIB["ID"]) {
    ?>
    <?if($_REQUEST['ajax']!='Y'):?>
        <div class="" style="clear: both;">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "_980",
                Array(
                    "TYPE" => "top_main_page_above",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="" style="clear: both;">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "_980",
                Array(
                    "TYPE" => "top_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    <div class="banner-left-side">
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_left_1",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_left_2",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_left_3",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_left_4",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>

    </div>
    <div class="left center-left" style="">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "_980",
            Array(
                "TYPE" => "top_main_page_480",
                "NOINDEX" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "0"
            ),
            false
        );?>
        <div id="spec_block_list_other" class="index-list">
    <?endif?>
        <?
        $APPLICATION->IncludeComponent("restoran:catalog.list_distance_possible",
            "articles", 
            array(
            "IBLOCK_TYPE" => "special_projects",
            "IBLOCK_ID" => $arIB["ID"],
            "PARENT_SECTION_CODE" => "articles",
            "NEWS_COUNT" => 14,
            "SORT_BY1" => 'SORT',
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "NAME",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "PROPERTY_CODE" => array(
                0 => "RESTORAN",
                1 => "",
                2 => "",
                3 => "",
            ),
            "CHECK_DATES" => "N",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => $USER->IsAdmin()?'N':"A",//a
            "CACHE_TIME" => "3600007",//0017
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => "120",
            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => 'rest_list_arrows',
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            'AJAX'=>$_REQUEST['ajax'],
            'NO_NEED_DISTANCE' => 'Y'
                ), false
        );
        ?>

<?if($_REQUEST['ajax']!='Y'):?>
        </div>
        <div class="clear"></div>

    </div>
    <div class="banner-right-side">
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_right_1",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_right_2",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_right_3",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="like-br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "new_year_night_right_4",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    </div>
<div class="clear"></div>

        <div class="preview_seo_text">
            <? $APPLICATION->ShowViewContent("preview_seo_text"); ?>
        </div>
<?endif?>

<?
} else {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
}
?>
<?if($_REQUEST['ajax']!='Y'):?>
    <?//if($USER->IsAdmin()):?>
    <script>
        $(function(){
            var page = 1;
            var ajax_load = 0;
            var max_page = $('.navigation .nav-end').text();


            $('.block').on('click','.js-restaurant-list-more',function(){
                trigger = $(this);
                if(!ajax_load){
                    page = +$(this).attr('nav-page-num');
                    page++;
                    max_page = +$(this).attr('nav-page-count');

                    if(page>=max_page){
                        $(this).hide();
                    }

                    if(!ajax_load && page<=max_page){
                        console.log('lets go to get 0');
                        ajax_load = 1;
                        $.ajax({
                            type: "GET",
                            url: location.pathname+location.search,
                            data: {
                                PAGEN_1:page,
                                ajax:'Y'
                            },
                            success: function(data) {

                                //console.log(data,'data');

                                $(".navigation").remove();
                                $("#spec_block_list_other .pull-left").last().after(data);

                                ajax_load = 0;
                            }
                        });
                    }
                }
            })

            function scroll_load() {
                post_num = page*<?=12?>-6;
                if($('#spec_block_list_other .pull-left:nth-child('+post_num+')').length>0){
                    last_top = $('#spec_block_list_other .pull-left:nth-child('+post_num+')').offset().top-200;//.catalogLine:nth-child(2n+1)
                    if ($(window).scrollTop()>=last_top&&!ajax_load&&max_page>page)
                    {
                        load_catalog_ajax();
                    }
                }
            }


            function load_catalog_ajax(){
                console.log('lets go to get 0');
                ajax_load = 1;
                $.ajax({
                    type: "POST",
                    url: "<?=$APPLICATION->GetCurPage()?>"+location.search,
                    data: {
                        PAGEN_1:++page,
                        ajax:'Y'
                    },
                    success: function(data) {
                        $("#spec_block_list_other .pull-left").last().after(data);
                        ajax_load = 0;
                    }
                });
            }
        })
    </script>
    <?//endif?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?endif?>