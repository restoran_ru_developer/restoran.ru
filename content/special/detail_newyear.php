<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Спецпроекты");
//$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
//$arIB = getArIblock($type, CITY_ID);
$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");

$arIB1 = getArIblock("special_projects", CITY_ID,"new_year_");

//var_dump($arIB);
?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/lightbox.js"></script>
<link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css" type="text/css" rel="stylesheet" />

<script>
    $(document).ready(function(){
        $(".baner2 object").css("z-index","9998");
        $(".first_pic").click(function(){           
            if ($(this).parents(".img").next().find("img").length)
                $(this).parents(".img").next().find("[src='"+$(this).attr("src")+"']").click();
            else 
                $(this).ekkoLightbox();
        })
        $(".scroll_container").delegate('img', 'click', function(event) {
            event.preventDefault();
            //$(this).parents(".img").next().find("[src="+$(this).src+"]").ekkoLightbox();
            $(this).ekkoLightbox();
        }); 
    });
</script>
	
	
    <div class="left" style="width:230px;">
    <?
            $arTopBlock["CODE"] = "popular";
            $APPLICATION->IncludeComponent(
                    "bitrix:news.list", "index_block_top4", Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "special_projects",
                "IBLOCK_ID" => $arIB1["ID"],
                "NEWS_COUNT" => "4",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arTopBlock",
                "FIELD_CODE" => "",
                "PROPERTY_CODE" => array(
                    0 => "ELEMENTS",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
                    ), false
            );
            ?>
    
    
    

        </div>
    <div class="right" style="width:750px">
        
        <?
 $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"statya_detail",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "special_projects",
		"IBLOCK_ID" => $arIB["ID"],
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"CHECK_DATES" => "N",
		"FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
		"PROPERTY_CODE" => Array("COMMENTS","RESTORAN"),
		"IBLOCK_URL" => "",
		"META_KEYWORDS" => "keywords",
		"META_DESCRIPTION" => "description",
		"BROWSER_TITLE" => "title",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
		"USE_PERMISSIONS" => "N",
		"CACHE_TYPE" => "Y",//y
		"CACHE_TIME" => "43203",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);
 ?>
        
        
            </div>
    <div class="clear"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>