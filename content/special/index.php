<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Где провести свадьбу в Москве");
// get IB params from iblock code
//$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
global $USER;
$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");
if ($arIB["ID"])
{   
?>

<div class="left" style="width:728px;">
    <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
    <?$APPLICATION->IncludeComponent("restoran:catalog.list", "articles", array(
	"IBLOCK_TYPE" => "special_projects",
	"IBLOCK_ID" => $arIB["ID"],
	"PARENT_SECTION_CODE" => "articles",
	"NEWS_COUNT" => ($_REQUEST["pageCnt"]?$_REQUEST["pageCnt"]:"21"),
	"SORT_BY1" => ($_REQUEST["SECTION_CODE"]=="wedding"||$_REQUEST["SECTION_CODE"]=="new_year_corp"||$_REQUEST["SECTION_CODE"]=="new_year_night"||$_REQUEST["SECTION_CODE"]=="valentine"||$_REQUEST["SECTION_CODE"]=="8marta"||$_REQUEST["SECTION_CODE"]=="post"||$_REQUEST["SECTION_CODE"]=="letnie_verandy")?"NAME":"rand",
	"SORT_ORDER1" => ($_REQUEST["SECTION_CODE"]=="wedding"||$_REQUEST["SECTION_CODE"]=="new_year_corp"||$_REQUEST["SECTION_CODE"]=="new_year_night"||$_REQUEST["SECTION_CODE"]=="valentine"||$_REQUEST["SECTION_CODE"]=="8marta"||$_REQUEST["SECTION_CODE"]=="post"||$_REQUEST["SECTION_CODE"]=="letnie_verandy")?"ASC":"",
	"SORT_BY2" => "",
	"SORT_ORDER2" => "",
	"FILTER_NAME" => "arrFilter",
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "ratio",
		2 => "reviews_bind",
		3 => "",
	),
	"CHECK_DATES" => "N",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "120",
	"ACTIVE_DATE_FORMAT" => "j F Y G:i",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "search_rest_list",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
    ?>   
    <div class="preview_seo_text">
            <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
</div>
<div class="right" style="width:240px;">
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
    
    <br /><Br />
    <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
    <br /><br />
    <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("/bitrix/templates/main/include_areas/order_rest_".CITY_ID.".php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    <br /><br />
    <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_3_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
        );?>
    <br /><br />
</div>
<div class="clear"></div>
<?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
            "TYPE" => "bottom_rest_list",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "600"
    ),
false
);?>
<Br /><Br />
<?}
else
{
    @define("ERROR_404", "Y");	
    CHTTP::SetStatus("404 Not Found");    
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>