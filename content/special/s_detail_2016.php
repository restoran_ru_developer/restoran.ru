<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Спецпроекты");
//$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
//$arIB = getArIblock($type, CITY_ID);
$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");

//$arIB1 = getArIblock("special_projects", CITY_ID,"new_year_");

//var_dump($arIB);
?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/lightbox.js"></script>
<link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css" type="text/css" rel="stylesheet" />

<script>
    $(document).ready(function(){
        $(".baner2 object").css("z-index","9998");
        $(".first_pic").click(function(){           
            if ($(this).parents(".img").next().find("img").length)
                $(this).parents(".img").next().find("[src='"+$(this).attr("src")+"']").click();
            else 
                $(this).ekkoLightbox();
        })
        $(".scroll_container").delegate('img', 'click', function(event) {
            event.preventDefault();
            //$(this).parents(".img").next().find("[src="+$(this).src+"]").ekkoLightbox();
            $(this).ekkoLightbox();
        }); 
    });
</script>		    
    <div class="left" style="width:<?=$_REQUEST["SECTION_CODE"]=='letnie_verandy'||$_REQUEST["SECTION_CODE"]=='new_year_corp'||$_REQUEST["SECTION_CODE"]=='new_year_night'||$_REQUEST["SECTION_CODE"]=='valentine'||$_REQUEST["SECTION_CODE"]=='february23'?650:750?>px">
        <?
            $ELEMENT_ID = $APPLICATION->IncludeComponent(
                "bitrix:news.detail",
                "statya_detail",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "USE_SHARE" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "special_projects",
                    "IBLOCK_ID" => $arIB["ID"],
                    "ELEMENT_ID" => "",
                    "ELEMENT_CODE" => $_REQUEST["CODE"],
                    "CHECK_DATES" => "N",
                    "FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
                    "PROPERTY_CODE" => Array("COMMENTS","RESTORAN"),
                    "IBLOCK_URL" => "",
                    "META_KEYWORDS" => "keywords",
                    "META_DESCRIPTION" => "description",
                    "BROWSER_TITLE" => "title",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "USE_PERMISSIONS" => "N",
                    "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",
                    "CACHE_TIME" => "43210",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Страница",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_SHOW_ALL" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                ),
        false
        );
        ?>
    </div>

    <div class="right right-banners" style="230px;">

        <?if ($_REQUEST["SECTION_CODE"]=="letnie_verandy"||($_REQUEST["SECTION_CODE"]=="new_year_corp")||($_REQUEST["SECTION_CODE"]=='new_year_night')||($_REQUEST["SECTION_CODE"]=='valentine')||$_REQUEST["SECTION_CODE"]=='february23'):?>
        <div class="modal-dialog floating-bron-form" <?//if($USER->IsAdmin()):?> <?//endif     data-spy="affix" data-offset-top="225"  data-offset-bottom="700"?>>
            <div class="modal-content">
                <div class="modal-body">
                    <noindex>
                        <?
                        global $REST_ID;
                        $_REQUEST['id'] = $REST_ID;
                        if($REST_ID){
                            $APPLICATION->IncludeComponent(
                                "restoran:form.result.new",
                                "order_2016",
                                Array(
                                    "SEF_MODE" => "N",
                                    "WEB_FORM_ID" => "3",
                                    "LIST_URL" => "",
                                    "EDIT_URL" => "",
                                    "SUCCESS_URL" => "",
                                    "CHAIN_ITEM_TEXT" => "",
                                    "CHAIN_ITEM_LINK" => "",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "USE_EXTENDED_ERRORS" => "N",
                                    "CACHE_TYPE" => "N",
                                    "CACHE_TIME" => "3600",
                                    "MY_CAPTCHA" => "Y",
                                    "VARIABLE_ALIASES" => Array(
                                        "WEB_FORM_ID" => "WEB_FORM_ID",
                                        "RESULT_ID" => "RESULT_ID"
                                    )
                                ),
                                false
                            );
                        }
                        ?>
                    </noindex>
                </div>
            </div>
        </div>
        <?endif;?>
        <?if ($_REQUEST["SECTION_CODE"]=="letnie_verandy"):?>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_TEMPLATE_PATH."/include_areas/new-order-detail-form.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>
        <?endif;?>
        <div class="br-1">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_2_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>

        <?if ($_REQUEST["SECTION_CODE"]!="letnie_verandy"):?>
        <div class="br-1">
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_1_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
        </div>
        <div class="br-1">
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_3_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
        </div>
        <?endif?>

    </div>
    <div class="clear"></div>

<?
FirePHP::getInstance()->info($_REQUEST["SECTION_CODE"],'$_REQUEST["SECTION_CODE"]');
if($_REQUEST["SECTION_CODE"]=='letnie_verandy'||($_REQUEST["SECTION_CODE"]=='new_year_corp')||($_REQUEST["SECTION_CODE"]=='new_year_night')||($_REQUEST["SECTION_CODE"]=='valentine')||($_REQUEST["SECTION_CODE"]=='february23')):?>
        <?
        $GLOBALS['arrFilter']['>ID'] = $ELEMENT_ID;
        $APPLICATION->IncludeComponent("restoran:catalog.list",
            "articles",
            array(
                "IBLOCK_TYPE" => "special_projects",
                "IBLOCK_ID" => $arIB["ID"],
                "PARENT_SECTION_CODE" => "articles",
                "NEWS_COUNT" => 3,
                "SORT_BY1" => 'SORT',
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrFilter",
                "PROPERTY_CODE" => array(
                    0 => "RESTORAN",
                    1 => "",
                    2 => "",
                    3 => "",
                ),
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => $USER->IsAdmin()?'N':"A",//a
                "CACHE_TIME" => "36000009",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => '',//$USER->IsAdmin()?'':"search_rest_list1",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                'AJAX'=>$_REQUEST['ajax'],
                'SEE_ALSO' => 'Y'
            ), false
        );
        ?>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>