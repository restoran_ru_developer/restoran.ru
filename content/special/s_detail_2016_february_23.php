<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Спецпроекты");
//$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
//$arIB = getArIblock($type, CITY_ID);
$arIB = getArIblock("special_projects", CITY_ID,$_REQUEST["SECTION_CODE"]."_");

//$arIB1 = getArIblock("special_projects", CITY_ID,"new_year_");

//var_dump($arIB);
?>
<script src="<?=SITE_TEMPLATE_PATH?>/js/lightbox.js"></script>
<link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css" type="text/css" rel="stylesheet" />

<script>
    $(document).ready(function(){
        $(".baner2 object").css("z-index","9998");
        $(".first_pic").click(function(){           
            if ($(this).parents(".img").next().find("img").length)
                $(this).parents(".img").next().find("[src='"+$(this).attr("src")+"']").click();
            else 
                $(this).ekkoLightbox();
        })
        $(".scroll_container").delegate('img', 'click', function(event) {
            event.preventDefault();
            //$(this).parents(".img").next().find("[src="+$(this).src+"]").ekkoLightbox();
            $(this).ekkoLightbox();
        }); 
    });
</script>		    
    <div class="left" style="width:750px">        
        <?        
            $APPLICATION->IncludeComponent(
                "bitrix:news.detail",
                "statya_detail",
                Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "USE_SHARE" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "special_projects",
                        "IBLOCK_ID" => $arIB["ID"],
                        "ELEMENT_ID" => "",
                        "ELEMENT_CODE" => $_REQUEST["CODE"],
                        "CHECK_DATES" => "N",
                        "FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
                        "PROPERTY_CODE" => Array("COMMENTS","RESTORAN"),
                        "IBLOCK_URL" => "",
                        "META_KEYWORDS" => "keywords",
                        "META_DESCRIPTION" => "description",
                        "BROWSER_TITLE" => "title",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                        "USE_PERMISSIONS" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "43201",
                        "CACHE_NOTES" => "",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Страница",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_SHOW_ALL" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                ),
        false
        );
        ?>
    </div>
    <div class="right right-banners" style="width:230px;">
        <?if ($_REQUEST["SECTION_CODE"]=="letnie_verandy"):?>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_TEMPLATE_PATH."/include_areas/new-order-detail-form.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>
        <?endif;?>
        <div class="br-1">
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_2_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
        </div>
        <div class="br-1">
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_1_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
        </div>
        <div class="br-1">
            <?$APPLICATION->IncludeComponent(
                        "bitrix:advertising.banner",
                        "",
                        Array(
                                "TYPE" => "right_3_main_page",
                                "NOINDEX" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "0"
                        ),
                        false
                );?>
        </div>
    </div>
    <div class="clear"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>