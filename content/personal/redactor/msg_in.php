<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обмен сообщениями");
?>

<?
// get current user ID
$userID = $USER->GetID();
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:socialnetwork.messages_input",
    "user_in_msg",
    Array(
        "MESSAGE_LENGTH" => "150",
        "SET_NAVCHAIN" => "N",
        "MESSAGE_VAR" => "",
        "PATH_TO_USER" => "",
        "PATH_TO_MESSAGE_FORM" => "",
        "PATH_TO_MESSAGE_FORM_MESS" => "",
        "PATH_TO_MESSAGES_INPUT" => "",
        "PATH_TO_MESSAGES_INPUT_USER" => "",
        "PAGE_VAR" => "",
        "USER_VAR" => "",
        "USER_ID" => $userID,
        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/",
        "ITEMS_COUNT" => "30"
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>