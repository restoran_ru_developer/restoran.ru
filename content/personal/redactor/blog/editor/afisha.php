<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	

<?$APPLICATION->IncludeComponent(
	"restoran:editor2",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
								
		),		
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$_REQUEST["IB"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Дата", "TYPE"=>"short_text", "CODE"=>"ACTIVE_FROM", "VALUE_FROM"=>"ACTIVE_FROM", "ADD_CLASS"=>"datew"),
			array("NAME"=>"Время", "TYPE"=>"short_text", "CODE"=>"PROPERTY_TIME", "VALUE_FROM"=>"PROPERTIES__TIME__VALUE"),
			array("NAME"=>"Тип события", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EVENT_TYPE", "VALUE_FROM"=>"PROPERTIES__EVENT_TYPE__VALUE"),
			array("NAME"=>"Адрес", "TYPE"=>"short_text", "CODE"=>"PROPERTY_ADRES", "VALUE_FROM"=>"PROPERTIES__ADRES__VALUE"),
			array("NAME"=>"Ресторан", "TYPE"=>"select", "CODE"=>"PROPERTY_RESTORAUNT", "VALUE_FROM"=>"PROPERTIES__RESTORAUNT__VALUE", "VALUES_LIST"=>"PROPS__RESTORAUNT__LIST"),
			array("NAME"=>"Тип заведения", "TYPE"=>"select", "CODE"=>"PROPERTY_TYPE", "VALUE_FROM"=>"PROPERTIES__TYPE__VALUE", "VALUES_LIST"=>"PROPS__TYPE__LIST")
		),
		"TAG_SECTION"=>44504,
		"CAN_ADD_TEXT"=>"Сначала заполните обязательные поля. Затем создайте запись, используя доступные модули. Например, к стандартным «дата и время проведения», «мето проведения» и «текстовое поле» мероприятия раздела «Афиша» можно добавить фотографии и видео, дать красивую ссылку на ресторан («Упомянуть ресторан»), добавить цитату с фотографией автора («Прямая речь»).  Дополнительне модули можно менять местами.",
		"BACK_LINK"=>"/content/personal/redactor/blog/#afisha.php"
	),
false
);?>
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>