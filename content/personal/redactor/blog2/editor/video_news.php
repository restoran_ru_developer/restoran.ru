<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	
      
     <?$APPLICATION->IncludeComponent(
	"restoran:editor",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Фотографии", "CODE"=>"add_photos"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),
			array("NAME"=>"Видео", "CODE"=>"add_video"),				
		),		
		"SECTION_ID"=>$_REQUEST["SECTION_ID"],
		"IBLOCK_ID"=>47,
		"PARENT_SECTION"=>"",
		"DEFAULT_SECTION"=>44492,
		"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_NAME", "VALUE_FROM"=>"SECTION__NAME"),
			array("NAME"=>"Родительский раздел", "TYPE"=>"select", "CODE"=>"IBLOCK_SECTION_ID", "VALUE_FROM"=>"SECTION__IBLOCK_SECTION_ID", "VALUES_LIST"=>"IBLOCK__SECTIONS"),
			array("NAME"=>"Анонс публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_DESCRIPTION", "VALUE_FROM"=>"SECTION__DESCRIPTION")
		),
		"TAG_SECTION"=>44495,
		"NEW_ARTICLE"=>$_REQUEST["NEW_ARTICLE"]
		
	),
false
);?>  
      
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>