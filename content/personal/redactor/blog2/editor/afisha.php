<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	
<?$APPLICATION->IncludeComponent("restoran:news.detail_editor", "redactor_afisha_edit", Array(
	"IBLOCK_TYPE" => "afisha",	// Тип информационного блока (используется только для проверки)
	"IBLOCK_ID" => "48",	// Код информационного блока
	"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],	// ID новости
	"ELEMENT_CODE" => "",	// Код новости
	"CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
	"FIELD_CODE" => array(	// Поля
		0 => "TAGS",
		1 => "",
	),
	"PROPERTY_CODE" => array(	// Свойства
		0 => "TIME",
		1 => "",
	),
	"IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "N",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"SET_TITLE" => "N",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
	"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
	"USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
	"PAGER_TITLE" => "Страница",	// Название категорий
	"PAGER_TEMPLATE" => "",	// Название шаблона
	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
	"DISPLAY_DATE" => "Y",	// Выводить дату элемента
	"DISPLAY_NAME" => "Y",	// Выводить название элемента
	"DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
	"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
	"USE_SHARE" => "N",	// Отображать панель соц. закладок
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Дата", "TYPE"=>"short_text", "CODE"=>"ACTIVE_FROM", "VALUE_FROM"=>"ACTIVE_FROM", "ADD_CLASS"=>"datew"),
			array("NAME"=>"Время", "TYPE"=>"short_text", "CODE"=>"PROPERTY_TIME", "VALUE_FROM"=>"PROPERTIES__TIME__VALUE"),
			array("NAME"=>"Тип события", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EVENT_TYPE", "VALUE_FROM"=>"PROPERTIES__EVENT_TYPE__VALUE", "VALUES_LIST"=>"PROPERTIES__EVENT_TYPE__VARIANTS"),
			array("NAME"=>"Адрес", "TYPE"=>"short_text", "CODE"=>"PROPERTY_ADRES", "VALUE_FROM"=>"PROPERTIES__ADRES__VALUE"),
			array("NAME"=>"Ресторан", "TYPE"=>"select", "CODE"=>"PROPERTY_RESTORAUNT", "VALUE_FROM"=>"PROPERTIES__RESTORAUNT__VALUE", "VALUES_LIST"=>"PROPERTIES__RESTORAUNT__VARIANTS"),
			array("NAME"=>"Тип заведения", "TYPE"=>"select", "CODE"=>"PROPERTY_TYPE", "VALUE_FROM"=>"PROPERTIES__TYPE__VALUE_ENUM_ID", "VALUES_LIST"=>"PROPERTIES__TYPE__VARIANTS")
	),
	"TAG_SECTION"=>44504
	),
	false
);?>
</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>