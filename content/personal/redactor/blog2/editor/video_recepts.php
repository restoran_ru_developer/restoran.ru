<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/lang/".SITE_LANGUAGE_ID."/index.php");
?>
<div id="content">	
      
     <?$APPLICATION->IncludeComponent(
	"restoran:editor",
	"editor",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",
		"CAN_ADD"=>array(
			array("NAME"=>"Прямая речь", "CODE"=>"add_pr"),
			array("NAME"=>"Шаг", "CODE"=>"add_step"),
			array("NAME"=>"Вставка текста", "CODE"=>"add_text"),
			array("NAME"=>"Упомянуть ресторан", "CODE"=>"add_rest"),		
			array("NAME"=>"Видео", "CODE"=>"add_video")				
		),		
		"SECTION_ID"=>$_REQUEST["SECTION_ID"],
		"IBLOCK_ID"=>70,
		"PARENT_SECTION"=>240,
		"DEFAULT_SECTION"=>44235,
		"REQ"=>array(
			array("NAME"=>"Навание публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_NAME", "VALUE_FROM"=>"SECTION__NAME"),
			array("NAME"=>"Родительский раздел", "TYPE"=>"select", "CODE"=>"IBLOCK_SECTION_ID", "VALUE_FROM"=>"SECTION__IBLOCK_SECTION_ID", "VALUES_LIST"=>"IBLOCK__SECTIONS"),
			array("NAME"=>"Анонс публикации", "TYPE"=>"short_text", "CODE"=>"SECTION_DESCRIPTION", "VALUE_FROM"=>"SECTION__DESCRIPTION"),
			array("NAME"=>"Основная фотография", "TYPE"=>"photo", "CODE"=>"SECTION_PICTURE", "VALUE_FROM"=>"SECTION__PICTURE"),
			array("NAME"=>"Порций", "TYPE"=>"short_text", "CODE"=>"UF_QUANTITY", "VALUE_FROM"=>"SECTION__UF_QUANTITY__VALUE"),
			array("NAME"=>"Кухня", "TYPE"=>"multi_select", "CODE"=>"UF_KITCHEN", "VALUE_FROM"=>"SECTION__UF_KITCHEN__VALUE", "VALUES_LIST"=>"SECTION__UF_KITCHEN__VALUES"),
			array("NAME"=>"Повод", "TYPE"=>"multi_select", "CODE"=>"UF_CAUSE", "VALUE_FROM"=>"SECTION__UF_CAUSE__VALUE", "VALUES_LIST"=>"SECTION__UF_CAUSE__VALUES"),
			array("NAME"=>"Предпочтения", "TYPE"=>"multi_select", "CODE"=>"UF_PREFERENCES", "VALUE_FROM"=>"SECTION__UF_PREFERENCES__VALUE", "VALUES_LIST"=>"SECTION__UF_PREFERENCES__VALUES"),
			array("NAME"=>"Время приготовления", "TYPE"=>"select", "CODE"=>"UF_TIME", "VALUE_FROM"=>"SECTION__UF_TIME__VALUE", "VALUES_LIST"=>"SECTION__UF_TIME__VALUES"),
			array("NAME"=>"Приготовление", "TYPE"=>"select", "CODE"=>"UF_PREPARATION", "VALUE_FROM"=>"SECTION__UF_PREPARATION__VALUE", "VALUES_LIST"=>"SECTION__UF_PREPARATION__VALUES"),
			array("NAME"=>"Категории", "TYPE"=>"select", "CODE"=>"UF_SECTION", "VALUE_FROM"=>"SECTION__UF_SECTION__VALUE", "VALUES_LIST"=>"SECTION__UF_SECTION__VALUES"),
			array("NAME"=>"Основной ингридиент", "TYPE"=>"select", "CODE"=>"UF_OSN_ING", "VALUE_FROM"=>"SECTION__UF_OSN_ING__VALUE", "VALUES_LIST"=>"SECTION__UF_OSN_ING__VALUES"),
			array("NAME"=>"Сложность", "TYPE"=>"select", "CODE"=>"UF_SLOG", "VALUE_FROM"=>"SECTION__UF_SLOG__VALUE", "VALUES_LIST"=>"SECTION__UF_SLOG__VALUES")
		),
		"TAG_SECTION"=>44203,
		"NEW_ARTICLE"=>$_REQUEST["NEW_ARTICLE"]
		
	),
false
);?>  
      
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>