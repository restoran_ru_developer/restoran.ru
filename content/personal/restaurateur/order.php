<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
if($_REQUEST["action"] == "extend_account")
    $title = "Продление аккаунта";
if($_REQUEST["action"] == "extend_priority")
    $title = "Продление приоритета";
elseif($_REQUEST["action"] == "buy_priority")
    $title = "Покупка приоритетов";
else
    $title = "Покупка";
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetTitle($title);
$arSaleIB = getArIblock("sale", CITY_ID);
?>

<script type="text/javascript">
    $(document).ready(function(){
        submitForm();

        $(".payment .left").live("click", function(event) {
            $(".payment .left").removeClass("active");
            $(this).addClass("active");
            var payVal = $(this).attr("pay");
            $("#payment_type").val(payVal);
            if(payVal == 5) {
                $("#PAY_CURRENT_ACCOUNT").attr('checked', 'checked');
                $("#PAY_SYSTEM_ID").val("");
                submitForm();
            } else {
                $("#PAY_SYSTEM_ID").val("7");
            }
        });
    });
</script>

<div id="content">	
	<div class="left" style="width:720px;">
		<h1><?=$APPLICATION->GetTitle(false)?></h1>
		<div class="grey_block">
            <?if($_REQUEST["action"] != "buy_priority"):?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.detail",
                    "account_order",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "USE_SHARE" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "sale",
                        "IBLOCK_ID" => $arSaleIB["ID"],
                        "ELEMENT_ID" => (int)$_REQUEST["ID"],
                        "ELEMENT_CODE" => "",
                        "CHECK_DATES" => "Y",
                        "FIELD_CODE" => Array("PREVIEW_PICTURE", "PREVIEW_TEXT"),
                        "PROPERTY_CODE" => Array(),
                        "IBLOCK_URL" => "",
                        "META_KEYWORDS" => "-",
                        "META_DESCRIPTION" => "-",
                        "BROWSER_TITLE" => "-",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "USE_PERMISSIONS" => "N",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_NOTES" => "",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Страница",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_SHOW_ALL" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                    ),
                false
                );?>
            <?elseif($_REQUEST["action"] == "buy_priority"):?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "buy_priority",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "sale",
                        "IBLOCK_ID" => "91",
                        "NEWS_COUNT" => "5",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "ACTIVE_FROM",
                        "SORT_ORDER2" => "DESC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => array("period_of_validity"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                    ),
                    false
                );?>
            <?endif?>
			<div class="clear"></div>
			<br />
            <?if (!$_REQUEST["ORDER_ID"] && $_REQUEST["action"] != "buy_priority"):?>
			    <div class="dotted"></div>
			    <br />
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket",
					"basket",
					Array(
						"PATH_TO_ORDER" => "/personal/order.php",
						"HIDE_COUPON" => "Y",
						"COLUMNS_LIST" => array("NAME", "PRICE", "QUANTITY"),
						"QUANTITY_FLOAT" => "N",
						"PRICE_VAT_SHOW_VALUE" => "N",
						"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
						"SET_TITLE" => "N"
					),
				false
				);?>
				<br />
				<div class="dotted"></div>
            <?endif?>
            <?if($_REQUEST["action"] != "buy_priority"):?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:sale.order.ajax",
                    "order",
                    Array(
                        "PATH_TO_BASKET" => "tip.php",
                        "PATH_TO_PERSONAL" => "index.php",
                        "PATH_TO_PAYMENT" => "payment.php",
                        "PATH_TO_AUTH" => "/auth/",
                        "PAY_FROM_ACCOUNT" => "Y",
                        "COUNT_DELIVERY_TAX" => "N",
                        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                        "ONLY_FULL_PAY_FROM_ACCOUNT" => "Y",
                        "ALLOW_AUTO_REGISTER" => "N",
                        "SEND_NEW_USER_NOTIFY" => "N",
                        "DELIVERY_NO_AJAX" => "Y",
                        "SET_TITLE" => "N",
                    ),
                false
                );?>
            <?endif;?>
		</div>
                <br /><br />
                <div id="tabs_block2">
                    <ul class="tabs">
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Количество</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Способы оплаты</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Перевод денег</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="left tab_left"></div>
                                <div class="left name">Распечатать купон</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                    </ul>
                    <!-- tab "panes" -->
                    <div class="panes">
                        <div class="pane" style="display:block">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                        <div class="pane">Оплата производится одним из предложенных способов через надежный дружественный сервис Robokassa. Через несколько минут после оплаты вы получите подтверждение об оплате на вашу электронную почту, а в вашем личном кабинете появится купон. Купон необходимо распечатать или сфотографировать. Предъявите купон в заведении, чтобы воспользоваться услугой.</div>
                    </div>
                </div>
	</div>
	<div class="right" style="width:240px">
		<img alt="" title="" src="/upload/rk/926/right_baner1.png" width="240" height="400" border="0">		
		<br /><br />
        <div class="phone_block2">
            <div>Закажите столик<br /> по телефонам: </div>
            <span>812</span> 338 38 58<br>
            <span>812</span> 338 38 25
        </div>
		<br /><br />
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
	</div>
	<div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>