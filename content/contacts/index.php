<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Контакты");
$arRestIB = getArIblock("catalog", CITY_ID);
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tools.js');
            $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/j.js');
            $APPLICATION->AddHeadScript('/tpl/js/jquery.popup.js');
?>
<script>
    $(function () {
        $("ul.tabs").each(function () {
            if ($(this).attr("ajax") == "ajax") {
                if ($(this).attr("history") == "true")
                    var history = true; else
                    var history = false;
                var url = "";
                if ($(this).attr("ajax_url"))
                    url = $(this).attr("ajax_url");
                $(this).tabs("div.panes > .pane", {
                    history: history, onBeforeClick: function (event, i) {
                        var pane = this.getPanes().eq(i);
                        if (pane.is(":empty")) {
                            pane.load(url + this.getTabs().eq(i).attr("href"));
                        }
                    }
                });
            }
            else {
                if ($(this).attr("history") == "true")
                    var history = true; else
                    var history = false;
                $(this).tabs("div.panes  .pane", {
                    history: history, onClick: function (event, i) {
                        $(".bx-yandex-map").each(function () {
                            var i = $(this).attr("id");
                            i = i.split('BX_YMAP_');
                            m = window.GLOBAL_arMapObjects[i[1]];
                            if (m)m.container.fitToViewport();
                        });
                    }
                });
            }
        });
    })
    </script>
<style>
    .panes {min-height:150px;}
    .panes div.pane { display:none; min-height:150px; padding:15px 0px;  margin-top:-3px; width:728px; line-height: 20px}
    .panes div span.date_size{ font-size: 1.6em}
    .panes div img{ margin-right:10px;margin-bottom:10px;}
    .poster_panes div img{ margin-right:10px;margin-bottom:10px;}
    .panes div a { color:#24a6cf; text-decoration: underline;}
    .panes div a:hover { color:#000; text-decoration: none;}
    .panes div a.uppercase { color:#1a1a1a; letter-spacing: 1px; text-decoration: none; }
    .panes div a.uppercase:hover { color:#24a6cf; text-decoration: none; }
    
ul.tabs { margin:0 !important; padding:0; height:41px; width:728px;border-bottom:1px solid #000;}
ul.tabs li { float:left; padding:0; margin:0; list-style-type:none;}
ul.tabs li a {
            padding: 8px 36px;
            font-size: 16px;
            font-family: 'futurafuturiscregular';
            -webkit-text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
            -moz-text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
            text-shadow: 1px 0px 1px rgba(0, 0, 0, 0.175);
            margin-right: 2px;
            line-height: 1.42857143;
            border: 1px solid transparent;
            background: #f3f6f7;
            position: relative;
            display: block;
}
ul.tabs a.current { 
    color: #000;
    background-color: #ffffff;
    border: 1px solid #000;
    border-top-width: 3px;
    border-bottom-color: transparent;
    border-bottom: 0px;
    cursor: default;
}
.more_cities {
            padding:0px 10px;
}
</style>
<div class="block">	
	<div class="left-side">
            <h1><?=$APPLICATION->ShowTitle(false)?></h1>
            <div class="clearfix"></div>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "more_cities_contact",
                    Array(
                            "IBLOCK_TYPE" => "contacts",
                            "IBLOCK_ID" => "101",
                            "SECTION_ID" => "",
                            "SECTION_CODE" => "",
                            "SECTION_URL" => "",
                            "COUNT_ELEMENTS" => "N",
                            "TOP_DEPTH" => "1",
                            "SECTION_FIELDS" => "",
                            "SECTION_USER_FIELDS" => "",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "3600003",
                            "CACHE_NOTES" => CITY_ID,
                            "CACHE_GROUPS" => "N"
                    ),
            false
            );?>
            <br /><br />
            <?$APPLICATION->IncludeComponent(
                    "bitrix:form.result.new",
                    "questions",
                    Array(
                            "SEF_MODE" => "N",
                            "WEB_FORM_ID" => "8",
                            "LIST_URL" => "",
                            "EDIT_URL" => "",
                            "SUCCESS_URL" => "",
                            "CHAIN_ITEM_TEXT" => "",
                            "CHAIN_ITEM_LINK" => "",
                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                            "USE_EXTENDED_ERRORS" => "N",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "3600",
                            "VARIABLE_ALIASES" => Array(
                                    "WEB_FORM_ID" => "WEB_FORM_ID",
                                    "RESULT_ID" => "RESULT_ID"
                            )
                    ),
            false
            );?>            
            <br /><Br />
	</div>
	<div class="right-side">        
                <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_2_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            <div class="title">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            //$arrFilterSim["PROPERTY_RECOMENDED_VALUE"] = "Да";
            $arrFilterTop4["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arrFilterTop4["!PREVIEW_PICTURE"] = false;
            $arrFilterTop4["!PROPERTY_restoran_ratio"] = false;  
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "rand",
                        "SORT_ORDER1" => "",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilterTop4",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("comments","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "10",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended"
                ),
            false
            );?>
            
        <br />
        <div align="right">
             <div class="baner2">
                <?$APPLICATION->IncludeComponent(
                            "bitrix:advertising.banner",
                            "",
                            Array(
                                    "TYPE" => "right_1_main_page",
                                    "NOINDEX" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "0"
                            ),
                            false
                    );?>
            </div>
        </div>
        <br /><br />
        <?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
	<div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>