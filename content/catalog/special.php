<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("События");
// get IB params from iblock code
?>
<div class="block">
    <div class="left-side">
        <h1>
                <?=$APPLICATION->GetTitle("h1");?>
        </h1>
        <div class="clearfix"></div>
<?
$rest = RestIBlock::GetRestIDByCode($_REQUEST["RESTOURANT"]);
    $ar1 = getArIblock("special_projects", CITY_ID,"easter_");
    $ar2 = getArIblock("special_projects", CITY_ID,"8marta_");
    $ar3 = getArIblock("special_projects", CITY_ID,"valentine_");
    $ar4 = getArIblock("special_projects", CITY_ID,"post_");
    $ar5 = getArIblock("special_projects", CITY_ID,"new_year_night_");
    $ar6 = getArIblock("special_projects", CITY_ID,"new_year_corp_");
    $ar7 = getArIblock("special_projects", CITY_ID,"letnie_verandy_");    
    $ar8 = getArIblock("special_projects", CITY_ID,"sport_");    
    $ar_spec = Array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],$ar6["ID"],$ar7["ID"],$ar8["ID"]);
    global $arrNewsFilter;
    $arrNewsFilter = Array("PROPERTY_RESTORAN"=>(int)$rest);

    $APPLICATION->IncludeComponent(
    	"restoran:catalog.list_multi",
    	"special",
    	Array(
    		"DISPLAY_DATE" => "N",
    		"DISPLAY_NAME" => "Y",
    		"DISPLAY_PICTURE" => "Y",
    		"DISPLAY_PREVIEW_TEXT" => "N",
    		"AJAX_MODE" => "N",
    		"IBLOCK_TYPE" => "special_projects",
    		"IBLOCK_ID" => $ar_spec,
    		"NEWS_COUNT" => 20,
    		"SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "timestamp_x"),
    		"SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
    		"SORT_BY2" => "",
    		"SORT_ORDER2" => "",
    		"FILTER_NAME" => "arrNewsFilter",
    		"FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
    		"PROPERTY_CODE" => array("COMMENTS","summa_golosov"),
    		"CHECK_DATES" => "N",
    		"DETAIL_URL" => "",
    		"PREVIEW_TRUNCATE_LEN" => "120",
    		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
    		"SET_TITLE" => "Y",
    		"SET_STATUS_404" => "N",
    		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    		"ADD_SECTIONS_CHAIN" => "N",
    		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
    		"PARENT_SECTION" => "",
    		"PARENT_SECTION_CODE" => "",
    		"CACHE_TYPE" => "A",
    		"CACHE_TIME" => "36000002",
    		"CACHE_FILTER" => "Y",
    		"CACHE_GROUPS" => "N",
    		"DISPLAY_TOP_PAGER" => "N",
    		"DISPLAY_BOTTOM_PAGER" => "Y",
    		"PAGER_TITLE" => "Новости",
    		"PAGER_SHOW_ALWAYS" => "N",
    		"PAGER_TEMPLATE" => "search_rest_list",
    		"PAGER_DESC_NUMBERING" => "N",
    		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    		"PAGER_SHOW_ALL" => "N",
    		"AJAX_OPTION_JUMP" => "N",
    		"AJAX_OPTION_STYLE" => "Y",
    		"AJAX_OPTION_HISTORY" => "N"
    	),
    false
    );
?>
                </div>
        <div class="right-side">        
            <div align="right">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_2_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
            <br />
            <div align="right">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                            "TYPE" => "right_1_main_page",
                            "NOINDEX" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
    </div>
    <div class="clearfix"></div>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>