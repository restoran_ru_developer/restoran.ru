<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?require_once($BX_DOC_ROOT.SITE_TEMPLATE_PATH."/lang/".SITE_LANGUAGE_ID."/header.php");?>
    <!DOCTYPE html>
<html lang="en">
    <head>
        <title><?$APPLICATION->ShowTitle()?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Restoran.ru">
        <meta property="fb:app_id" content="297181676964377" />
        <meta name='yandex-verification' content='6b320b6dff8af5fc' />
        <?if (CITY_ID=="tmn"):?>
            <meta property="fb:admins" content="100005128267295,100004709941783" />
        <?else:?>
            <meta property="fb:admins" content="100004709941783" />
        <?endif;?>
        <?if (!$_REQUEST["CODE"]):?>
            <meta property="og:image" content="http://www.restoran.ru/images/logo.png" />
        <?endif;?>
        <link rel="icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/tpl/favicon.ico" type="image/x-icon">
        <link href="http://fonts.googleapis.com/css?family=Playfair+Display:900,700,400,400italic,700italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic"  type="text/css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=PT+Serif:400italic,700italic' rel='stylesheet' type='text/css'>

        <link href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/lightbox.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/jquery.autocomplete.css"  type="text/css" rel="stylesheet" />
        <link href="<?=SITE_TEMPLATE_PATH?>/css/datepicker.css"  type="text/css" rel="stylesheet" />
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?$APPLICATION->AddHeadScript('/tpl/js/jquery.js')?>
        <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.min.js')?>
        <?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/script.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jScrollPane.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mousewheel.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/mwheelIntent.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/detail_galery.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/lightbox.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.autocomplete.min.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-datepicker.js')?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/maskedinput.js')?>
        <?$APPLICATION->ShowHead()?>
        <?


        $APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/anton.css"  type="text/css" rel="stylesheet" />',true);

        CModule::IncludeModule("advertising");
        if ($APPLICATION->GetCurPage()!="/"&&$APPLICATION->GetCurPage()!="/index.php")
            $page = "others";
        else
            $page = "main";
        CAdvBanner::SetRequiredKeywords (array(CITY_ID),array($page));

        ?>
    </head>
<body>
    <div id="panel"><?$APPLICATION->ShowPanel()?></div>
    <div class="container in-detail-photos">
        <div class="content">
            <div class="page-header block">
                <div class="logo in-detail-photos">
                    <?if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/".CITY_ID."/"):?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="Ресторан.ру" />
                    <?else:?>
                        <a href="/<?=CITY_ID?>/" alt="Ресторан.ру"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" /></a>
                    <?endif;?>
                    <?$APPLICATION->IncludeComponent(
                        "restoran:city.selector",
                        "city_select_2014",
                        Array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_URL" => "",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "36000000000",
                            "CACHE_NOTES" => "new321236624328",
                            "CACHE_GROUPS" => "N"
                        ),
                        false
                    );?>
                </div>
            </div>

            <div class="t_b_980">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                        "TYPE" => "top_content_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                    false
                );?>
            </div>
            <?
            $arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
            ?>
            <div class="block">
                <div class="restoran_name_view_content">
                    <h1 style="margin-top: -55px;">
                        Фотографии ресторана
                        <br>
                        <span class="photos-header"><?$APPLICATION->ShowViewContent("restoran_name_view_content");?></span>
                    </h1>
                </div>
                       <?$APPLICATION->IncludeComponent("restoran:restoraunts.detail", "photos", array(
                                "IBLOCK_TYPE" => "catalog",
                                "IBLOCK_ID" => $arIB["ID"],
                                "ELEMENT_ID" => "",
                                "ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
                                "CHECK_DATES" => "Y",
                                "PROPERTY_CODE" => array(
                                    'photos'
                                ),
                                "ADD_REVIEWS" => "Y",
                                "REVIEWS_BLOG_ID" => "3",
                                "IBLOCK_URL" => "",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "36000006",
                                "CACHE_GROUPS" => ($templ=="sleep")?"N":"N",
                                "META_KEYWORDS" => "-",
                                "META_DESCRIPTION" => "-",
                                "BROWSER_TITLE" => "-",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "Y",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "USE_PERMISSIONS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Страница",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_SHOW_ALL" => "Y",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "USE_SHARE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => ""
                                ),
                                false
                        );?>
            </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>