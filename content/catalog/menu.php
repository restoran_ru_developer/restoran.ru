<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" />
<?


if ($_REQUEST["page"])
    $_REQUEST["PAGEN_1"] = (int)$_REQUEST["page"];
    if (!$_REQUEST["PARENT_SECTION_ID"]):        
        CModule::IncludeModule("iblock");        
        $arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arIB["ID"],"CODE"=>$_REQUEST["RESTOURANT"],'ACTIVE'=>'Y'),false,Array("nTopCount"=>1),Array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_SKYDISH_ID"));
        if ($ar = $res->GetNext())
        {
            $id = $ar["ID"];
            $name = $ar["NAME"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
            $url = $ar["DETAIL_PAGE_URL"];
            
            $SKY_DISH_ID=$ar["PROPERTY_SKYDISH_ID_VALUE"];

            if($id){
                global $DB;
                $sql = "SELECT ID FROM b_iblock WHERE DESCRIPTION='".$DB->ForSql($id)."'";
                $db_list = $DB->Query($sql);
                if($ar_result = $db_list->GetNext())
                {
                    $menu = $ar_result['ID'];
                }
            }
            
            //if($USER->isAdmin()) var_dump($ar);
        }

    endif;
    ?>
<div class="block">
    <h1 class="with_link" menu_ib_id="<?=$menu?>">Меню ресторана «<a href="<?=$url?>" alt="<?=$ar["NAME"]?>" title="<?=$ar["NAME"]?>"><?=$ar["NAME"]?></a>»</h1>
    <div class="clearfix"></div>
<?
if ($menu)
{
	if($SKY_DISH_ID && $SKY_DISH_ID>0){
		
		if($_REQUEST["all"]=="Y") $TPL = "show_all";
	    else $TPL = "";
	    if($_REQUEST["print"]=="Y") $TPL = "print";
	    	
	    $APPLICATION->IncludeComponent(
			"restoran:skydish.menu",
			$TPL,
			Array("DIR"=>$APPLICATION->GetCurDir(),"REST_ID"=>$SKY_DISH_ID,"ACTIVE"=>$_REQUEST["SECTION_ID"]),
			false
		);	
		
	}else{
	
    if ($_REQUEST["all"]!="Y"):
        global $arrFilMenu;
        $arrFilMenu['SECTION_CODE']='foto';
        //$arrFilMenu["TAGS"] = "Y";
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "menu_rest_in_pics",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "rest_menu_ru",
                "IBLOCK_ID" => $menu,
                "NEWS_COUNT" => 1000,
                "SORT_BY1" => "sort",
                "SORT_ORDER1" => "asc",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "asc",
                "FILTER_NAME" => "arrFilMenu",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array(),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "150",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",//a
                "CACHE_TIME" => "36000003",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
        );
    endif;
    if(!$menuInPics) {
        $APPLICATION->IncludeComponent(
            "bitrix:catalog",
            "dostavka",
            Array(
                "AJAX_MODE" => "Y",
                "SEF_MODE" => "N",
                "IBLOCK_TYPE" => "rest_menu_ru",
                "IBLOCK_ID" => $menu,
                "USE_FILTER" => "N",
                "USE_REVIEW" => "N",
                "USE_COMPARE" => "N",
                "SHOW_TOP_ELEMENTS" => "Y",
                "PAGE_ELEMENT_COUNT" => "12",
                "LINE_ELEMENT_COUNT" => "3",
                "ELEMENT_SORT_FIELD" => "SORT",//sort
                "ELEMENT_SORT_ORDER" => "ASC",//asc
                "ELEMENT_SORT_FIELD2" => 'ID',
                "ELEMENT_SORT_ORDER2" => 'ASC',
                "LIST_PROPERTY_CODE" => array("map"),
                "INCLUDE_SUBSECTIONS" => "Y",
                "LIST_META_KEYWORDS" => "-",
                "LIST_META_DESCRIPTION" => "-",
                "LIST_BROWSER_TITLE" => "-",
                "DETAIL_PROPERTY_CODE" => array("map"),
                "DETAIL_META_KEYWORDS" => "map",
                "DETAIL_META_DESCRIPTION" => "map",
                "DETAIL_BROWSER_TITLE" => "map",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "7204",
                "CACHE_NOTES" => "",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "PRICE_CODE" => array("BASE"),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "LINK_IBLOCK_TYPE" => "",
                "LINK_IBLOCK_ID" => "",
                "LINK_PROPERTY_SID" => "",
                "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                "USE_ALSO_BUY" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "TOP_ELEMENT_COUNT" => "9",
                "TOP_LINE_ELEMENT_COUNT" => "3",
                "TOP_ELEMENT_SORT_FIELD" => "sort",
                "TOP_ELEMENT_SORT_ORDER" => "asc",

                "TOP_PROPERTY_CODE" => array("map"),
                "VARIABLE_ALIASES" => Array(
                    "SECTION_ID" => "SECTION_ID",
                    "ELEMENT_ID" => "ELEMENT_ID"
                ),
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );
    }
    
    }
}
else
{
    ShowError("Меню не найдено");
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");
}?>
</div>    
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>