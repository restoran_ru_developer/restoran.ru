<?
/**
 * 404 for not correct work of urlrewrite
 **/
if(preg_match('/\//',$_REQUEST['PROPERTY_VALUE2'])||preg_match('/\//',$_REQUEST['PROPERTY_VALUE'])){
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");
}

if($_REQUEST['AJAX_REQUEST']!='Y'):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("");?>
<?else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

    $arRestIB = getArIblock("catalog", CITY_ID);
    $APPLICATION->IncludeComponent(
        "restoran:catalog.filter",
        (substr_count($APPLICATION->GetCurDir(),"/map")>0)?"map_2014":"filter_2014",
        Array(
            "IBLOCK_TYPE" => "catalog",
            "IBLOCK_ID" => $arRestIB["ID"],
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => $prop_fil,
            "PRICE_CODE" => array(),
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "36000013",
            "CACHE_GROUPS" => "N",
            "LIST_HEIGHT" => "5",
            "TEXT_WIDTH" => "20",
            "NUMBER_WIDTH" => "5",
            "SAVE_IN_SESSION" => "N"
        )
    );

    $letter = trim($_REQUEST["letter"]);
    if ($letter)
    {
        if ($letter=="09")
        {
            $arrFilter[">=NAME"] = "0";
            $arrFilter["<=NAME"] = "9";
        }
        else
            $arrFilter["NAME"] = iconv("windows-1251","utf-8",chr($letter))."%";
    }

endif?>
<?


if (CITY_ID!="spb"&&CITY_ID!="msk"&&CITY_ID!="rga"&&!$_REQUEST["page"])
{
    $_REQUEST["page"] = 1;
    $_REQUEST["PAGEN_1"] = 1;
}
$arIB_best = getArIblock("best", $_REQUEST["CITY_ID"]);
$arIB_first_20 = getArIblock("first_20", $_REQUEST["CITY_ID"]);
$BEST_CACHE_TIME = 36000013;
if($arIB_best['ID'] && !$_REQUEST["page"] && !$_REQUEST["PAGEN_1"] && !$_REQUEST["letter"] && $_REQUEST["CATALOG_ID"]=='restaurants'&&LANGUAGE_ID!='en'&&$_REQUEST["PROPERTY"]&&!$_REQUEST["PROPERTY2"]&&(!$_REQUEST['pageRestSort']||($_REQUEST['pageRestSort']=='alphabet'&&$_REQUEST['by']=='asc'))&&$_REQUEST['set_filter']!='Y'&&!$_REQUEST['NEAR']){

    $APPLICATION->IncludeComponent(
        "restoran:best.update_checker",
        "",
        Array(
            'IBLOCK_TYPE'=>'best',
            "IBLOCK_ID" => $arIB_best['ID'],
            "CACHE_TYPE" => $USER->IsAdmin()?"N":"A",//
            "CACHE_TIME" => $BEST_CACHE_TIME
        ),
        false
    );

    if(!$First20SectionId){
        FirePHP::getInstance()->info('not-best');
        $APPLICATION->IncludeComponent(
            "restoran:best.update_checker",
            "",
            Array(
                'IBLOCK_TYPE'=>'first_20',
                "IBLOCK_ID" => $arIB_first_20['ID'],
                "CACHE_TYPE" => $USER->IsAdmin()?"N":"A",//a
                "CACHE_TIME" => $BEST_CACHE_TIME
            ),
            false
        );
        $_REQUEST["page"] = 1;
        $_REQUEST["PAGEN_1"] = 1;
        $_REQUEST['NO_BEST'] = true;
    }
    //$BestIblockUpdateNote для кеша

    FirePHP::getInstance()->info($RestListExceptionFirst20Ids,'$RestListExceptionFirst20Ids');
    FirePHP::getInstance()->info($First20SectionId,'$First20SectionId');
}
elseif(!$_REQUEST["letter"]&&($_REQUEST["PAGEN_1"]||$_REQUEST["page"])&&$arIB_first_20['ID']&&$_REQUEST["CATALOG_ID"]=='restaurants'&&LANGUAGE_ID!='en'&&!$_REQUEST["PROPERTY2"]&&(!$_REQUEST['pageRestSort']||($_REQUEST['pageRestSort']=='alphabet'&&$_REQUEST['by']=='asc'))&&$_REQUEST['set_filter']!='Y'&&!$_REQUEST['NEAR']){//&&$_REQUEST["PROPERTY"]
//    $_REQUEST["page"] = 1;
//    $_REQUEST["PAGEN_1"] = 1;
    $APPLICATION->IncludeComponent(
        "restoran:best.update_checker",
        "",
        Array(
            'IBLOCK_TYPE'=>'first_20',
            "IBLOCK_ID" => $arIB_first_20['ID'],
            "CACHE_TYPE" => $USER->IsAdmin()?"N":"A",//a
            "CACHE_TIME" => $BEST_CACHE_TIME
        ),
        false
    );
    if(array_key_exists('!ID',$RestListExceptionFirst20Ids)&&($_REQUEST["page"]!=1||$_REQUEST["PAGEN_1"]!=1)){
        $arrFilter = array_merge($arrFilter, $RestListExceptionFirst20Ids);
    }
    FirePHP::getInstance()->info($RestListExceptionFirst20Ids,'$RestListExceptionFirst20Ids');
    FirePHP::getInstance()->info($First20SectionId,'$First20SectionId');
}

$_REQUEST['bFirst20SectionId'] = $First20SectionId ? true : false;

//  для страниц с отсутствием top наборов
if((!array_key_exists('!ID',$RestListExceptionFirst20Ids)&&!$_REQUEST["page"]&&$_REQUEST["PROPERTY"])||(!$_REQUEST["page"]&&$_REQUEST['NEAR'])){
    $_REQUEST["page"] = 1;
    $_REQUEST["PAGEN_1"] = 1;
}


if($_REQUEST["CONTEXT"]=="Y"){
    if($APPLICATION->get_cookie("CONTEXT")!="Y") $APPLICATION->set_cookie("CONTEXT", "Y", time()+60*30, "/","restoran.ru",false,true);
}
if ($_REQUEST["PROPERTY"] == 'prigorody') {
    $REQUEST_PROPERTY = "out_city";
    $_REQUEST["PROPERTY"] = "out_city";
}
else $REQUEST_PROPERTY = $_REQUEST["PROPERTY"];




$arIB = getArIblock("catalog",CITY_ID);
//  TODO проверить, работает ли код в условие, т.к. фильтр выше
if ($_REQUEST["PROPERTY"]&&$_REQUEST["PROPERTY_VALUE"])//&&($USER->IsAdmin()?$_REQUEST["PROPERTY_VALUE"]=='all':'')
{
    $arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
    $properties = CIBlockProperty::GetList(Array(), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arRestIB["ID"], "CODE"=>$REQUEST_PROPERTY));
    if ($prop_fields = $properties->GetNext())
    {
        if ($prop_fields["LINK_IBLOCK_ID"])
        {
            if ($_REQUEST["PROPERTY_VALUE"]!="all")
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y","CODE"=>$_REQUEST["PROPERTY_VALUE"]),false,false,Array("ID", "NAME"));
                if($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]] = $ar["ID"];
//                    if ($_REQUEST["PROPERTY"]=="kitchen")
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_kitchen"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_kitchen"]=$ar["ID"];
//                        }
//                    }
//                    elseif($_REQUEST["PROPERTY"]=="type")
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_type"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_type"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif($_REQUEST["PROPERTY"]=="subway")
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_subway"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_subway"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif($_REQUEST["PROPERTY"]=="area" || $_REQUEST["PROPERTY"]=="rajon")
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_area"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_area"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="out_city") || ($_REQUEST["PROPERTY"]=="prigorody"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_out_city"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_out_city"]=$ar["ID"];
//                            //$_REQUEST["PRIORITY_prigorody"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="average_bill") || ($_REQUEST["PROPERTY"]=="srednij_schet"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_average_bill"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_average_bill"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="children") || ($_REQUEST["PROPERTY"]=="detyam"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_children"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_children"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="proposals") || ($_REQUEST["PROPERTY"]=="predlozheniya"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_proposals"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_proposals"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="features") || ($_REQUEST["PROPERTY"]=="osobennosti"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_features"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_features"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="entertainment") || ($_REQUEST["PROPERTY"]=="razvlecheniya"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_entertainment"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_entertainment"]=$ar["ID"];
//                        }
//                    }
//
//                    elseif(($_REQUEST["PROPERTY"]=="ideal_place_for") || ($_REQUEST["PROPERTY"]=="idealnoe_mesto_dlya"))
//                    {
//                        $r = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"PROPERTY_priority_ideal_place_for"=>$ar["ID"]),false,Array("nTopCount"=>1),Array("ID"));
//                        if ($r->SelectedRowsCount())
//                        {
//                            $_REQUEST["PRIORITY_ideal_place_for"]=$ar["ID"];
//                        }
//                    }

                }
            }
            else
            {
                $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>$prop_fields["LINK_IBLOCK_ID"],"ACTIVE"=>"Y"),false,false,Array("ID"));
                while($ar = $res->Fetch())
                {
                    $arrFilter["PROPERTY_".$prop_fields["CODE"]][] = $ar["ID"];
                }
            }
        }
    }
}
else
{
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["d_tours"])==1&&$_REQUEST["arrFilter_pf"]["d_tours"][0])
    {
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/d_tours/".$_REQUEST["arrFilter_pf"]["d_tours"][0].'/');
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["kitchen"])==1&&$_REQUEST["arrFilter_pf"]["kitchen"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["kitchen"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/kitchen/".$ar1["CODE"]);
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["type"])==1&&$_REQUEST["arrFilter_pf"]["type"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["type"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/type/".$ar1["CODE"]);
    }

    // метро
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["subway"])==1&&$_REQUEST["arrFilter_pf"]["subway"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["subway"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/metro/".$ar1["CODE"]);
    }

    // район города
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["area"])==1&&$_REQUEST["arrFilter_pf"]["area"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["area"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/rajon/".$ar1["CODE"]);
    }

    // Пригороды
    if (is_array($_REQUEST["arrFilter_pf"])&&((count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["out_city"])==1&&$_REQUEST["arrFilter_pf"]["out_city"][0])))
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["out_city"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/prigorody/".$ar1["CODE"]);
    }

    // средний счёт average_bill
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["average_bill"])==1&&$_REQUEST["arrFilter_pf"]["average_bill"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["average_bill"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/srednij_schet/".$ar1["CODE"]);
    }

    // музыка
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["music"])==1&&$_REQUEST["arrFilter_pf"]["music"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["music"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/music/".$ar1["CODE"]);
    }
    // Детям
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["children"])==1&&$_REQUEST["arrFilter_pf"]["children"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["children"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/detyam/".$ar1["CODE"]);
    }

    // Предложения
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["proposals"])==1&&$_REQUEST["arrFilter_pf"]["proposals"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["proposals"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/predlozheniya/".$ar1["CODE"]);
    }

    // Особенности
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["features"])==1&&$_REQUEST["arrFilter_pf"]["features"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["features"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/osobennosti/".$ar1["CODE"]);
    }

    // Особенности Банкет
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["FEATURES_B_H"])==1&&$_REQUEST["arrFilter_pf"]["FEATURES_B_H"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["FEATURES_B_H"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/osobennostibanket/".$ar1["CODE"]);
    }

    // Свой спирт
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["ALLOWED_ALCOHOL"])==1&&$_REQUEST["arrFilter_pf"]["ALLOWED_ALCOHOL"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["ALLOWED_ALCOHOL"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/razreshenospirtnoe/".$ar1["CODE"]);
    }

    // Специальное оборудование
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["BANKET_SPECIAL_EQUIPMENT"])==1&&$_REQUEST["arrFilter_pf"]["BANKET_SPECIAL_EQUIPMENT"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["BANKET_SPECIAL_EQUIPMENT"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/specoborudovanie/".$ar1["CODE"]);
    }

    // Развлечения
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["entertainment"])==1&&$_REQUEST["arrFilter_pf"]["entertainment"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["entertainment"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/razvlecheniya/".$ar1["CODE"]);
    }

    // ресторанные группы
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["rest_group"])==1&&$_REQUEST["arrFilter_pf"]["rest_group"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["rest_group"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/rest_group/".$ar1["CODE"]);
    }

    // парковка
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["parking"])==1&&$_REQUEST["arrFilter_pf"]["parking"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["parking"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/parking/".$ar1["CODE"]);
    }

    // Идеальное место для
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&count($_REQUEST["arrFilter_pf"]["ideal_place_for"])==1&&$_REQUEST["arrFilter_pf"]["ideal_place_for"][0])
    {
        $r = CIBlockElement::GetByID($_REQUEST["arrFilter_pf"]["ideal_place_for"][0]);
        $ar1 = $r->Fetch();
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/idealnoe_mesto_dlya/".$ar1["CODE"]);
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&$_REQUEST["arrFilter_pf"]["wi_fi"])
    {
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/wi_fi/y/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==1&&$_REQUEST["arrFilter_pf"]["breakfast"])
    {
        LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/breakfast/y/");
    }
    if (is_array($_REQUEST["arrFilter_pf"])&&count($_REQUEST["arrFilter_pf"])==2)
    {
        $url_str = "";
        if ($key == 'kitchen' || $key == 'type') {
            // добавил проверку на тип и кухню (первоначально редиректы делались только под эти параметры.
            // а вообще - вроде всё правильно, но ломается, если выбрать, например 2 типа и одно метро - в выдаче происходит неведомая ересь)
            foreach ($_REQUEST["arrFilter_pf"] as $key=>$filter)
            {
                if (count($filter)==1)
                {
                    $r = CIBlockElement::GetByID($filter[0]);
                    if ($ar1 = $r->Fetch())
                        $url_str .= "".$key."/".$ar1["CODE"]."/";
                    else
                        $url_str .= "".$key."/Y/";
                    //if (count($key)==1) $redirect = true;
                }
            }
            LocalRedirect("/".$_REQUEST["CITY_ID"]."/catalog/".$_REQUEST["CATALOG_ID"]."/".$url_str);
        }
    }

}

/**DISTANCE OUTPUT**/
if($_REQUEST["PROPERTY"]=='all'&&$_REQUEST["PROPERTY_VALUE"]=='distance'&&!$_REQUEST['pageRestSort']){
    $DISTANCE = true;
}
else {
    $DISTANCE = false;
}
?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
    <div class="block">
    <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
    <?if(!$DISTANCE&&!$_REQUEST['NEAR']):?>
        <?if(LANGUAGE_ID!='en'&&$_REQUEST['arrFilter_pf']):?>
            <?$APPLICATION->ShowViewContent("found_num_rest");?>
        <?endif?>
        <div class="sort">
            <?
            // set rest sort links
            $excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1","by","index_php?page","index_php");
            $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=new&".(($_REQUEST["pageRestSort"]=="new"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
            $sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=price&".(($_REQUEST["pageRestSort"]=="price"&&$_REQUEST["by"]=="asc") ? "by=desc": "by=asc"), $excUrlParams);
            //        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=ratio&".(($_REQUEST["pageRestSort"]=="ratio"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
            $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=popular&".(($_REQUEST["pageRestSort"]=="popular"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
            $sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=alphabet&".((($_REQUEST["pageRestSort"]=="alphabet"&&$_REQUEST["by"]=="asc")|| !$_REQUEST["pageRestSort"]) ? "by=desc": "by=asc"), $excUrlParams);
            ?>
            <?$by = ($_REQUEST["by"]=="desc")?"asc":"desc";?>
            <?
            if ($_REQUEST["pageRestSort"] == "new")
            {
                echo '<a class="'.$by.'" href="'.$sortNewPage.'">Новое на сайте</a>';
            }
            else
            {
                echo '<a href="'.$sortNewPage.'">Новое на сайте</a>';
            }
            if ($_REQUEST["pageRestSort"] == "price")
            {
                echo '<a class="'.$by.'" href="'.$sortPricePage.'">по стоимости</a>';
            }
            else
            {
                echo '<a href="'.$sortPricePage.'">по стоимости</a>';
            }
            //            if ($_REQUEST["pageRestSort"] == "ratio")
            if ($_REQUEST["pageRestSort"] == "popular")
            {
                echo '<a class="'.$by.'" href="'.$sortRatioPage.'">по рейтингу</a>';
            }
            else
            {
                echo '<a href="'.$sortRatioPage.'">по рейтингу</a>';
            }
            if ($_REQUEST["pageRestSort"] == "alphabet" || !$_REQUEST["pageRestSort"])
            {
                echo '<a class="'.$by.'" href="'.$sortAlphabetPage.'">по алфавиту</a>';
            }
            else
            {
                echo '<a href="'.$sortAlphabetPage.'">по алфавиту</a>';
            }
            ?>
        </div>
    <?endif?>
    <div class="clearfix"></div>
    <?if (!$_REQUEST["letter"]&&!$_REQUEST["RUBRIC"]&&!$_REQUEST['NEAR'])://!$_REQUEST["arrFilter_pf"]&&?>
        <div class="left-side">
            <div class="special">
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".photogalery_top_spec").galery({});
                    });
                </script>
                <?
                if(!$_REQUEST["PROPERTY"]):
                    global $arrFilterTop1;
//                    if (intval($_REQUEST["PRIORITY_kitchen"]))
//                    {
//                        $arrFilterTop1 = Array(
//                            "PROPERTY_priority_kitchen" => intval($_REQUEST["PRIORITY_kitchen"]),
//                            "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
//                        );
//                    }
//                    elseif(intval($_REQUEST["PRIORITY_type"]))
//                    {
//                        $arrFilterTop1 = Array(
//                            "PROPERTY_priority_type" => intval($_REQUEST["PRIORITY_type"]),
//                            "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
//                        );
//                    }
//                    else
//                    {
                    $arrFilterTop1 = Array(
                        "PROPERTY_top_spec_place_1_VALUE" => Array("Да"),
//                        "PROPERTY_priority_type" => false,
//                        "PROPERTY_priority_kitchen" => false,
                        "!PROPERTY_sleeping_rest_VALUE" => "Да"
                    );
//                    }
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "rest_list_top_spec",
                        Array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arIB["ID"],
                            "NEWS_COUNT" => "1",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFilterTop1",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "opening_hours", "phone", "address", "subway","RATIO"),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "160",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "86409",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "CACHE_NOTES" => "Nww",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CONTEXT"=>$_REQUEST["CONTEXT"]
                        ),
                        false
                    );
                else:
                    $arIB = getArIblock("top_bottom_priorities",CITY_ID);
                    global $arrTopBottomFilter;
                    $arrTopBottomFilter = array('PROPERTY_TOP_PRIORITY_VALUE'=>'Да');
                    if($_REQUEST['CATALOG_ID']=='banket'){
                        $arrTopBottomFilter = array_merge($arrTopBottomFilter,array('PROPERTY_BANKET_VALUE'=>'Да'));
                    }
                    else {
                        $arrTopBottomFilter = array_merge($arrTopBottomFilter,array('!PROPERTY_BANKET_VALUE'=>'Да'));
                    }
                    $APPLICATION->IncludeComponent(
                        "restoran:top_bottom.priority",
                        "",
                        Array(
                            "IBLOCK_ID" => $arIB['ID'],
                            "CACHE_TYPE" => "A",//a
                            "CACHE_TIME" => "36000009",
                            'FILTER_NAME' => 'arrTopBottomFilter',
                            'PROPERTIES' => array(
                                'photos',
                                'subway',
                                'kitchen',
                                'type',
                                'average_bill',
                                'address',
                                'opening_hours',
                                'phone',
                                'RATIO'
                            )
                        ),
                        false
                    );
                endif;
                ?>
            </div>
        </div>
        <div class="right-side">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
        <div class="clearfix"></div>
    <?endif;?>
<?endif?>
        <?
        //  NEAR BLOCK
        if($_REQUEST['NEAR']){
            $res = CIBlockElement::GetList(Array(), Array('IBLOCK_ID'=>$arIB['ID'], "CODE" => $_REQUEST['RESTOURANT'], "ACTIVE" => "Y"), false, Array("nTopCount" => 1), Array("ID",'IBLOCK_ID','NAME'));
            if ($obElement = $res->GetNextElement()) {
                $arAboutNearRest = $obElement->GetFields();

                $PROPERTY_CODE_LIST = array('LAT','LON');
                foreach ($PROPERTY_CODE_LIST as $prop_name) {
                    $arAboutNearRest["PROPERTIES"][$prop_name] = $obElement->GetProperty($prop_name);
                }

                $DISTANCE = true;
                $_REQUEST['lat'] = current($arAboutNearRest["PROPERTIES"]["LAT"]['VALUE']);
                $_REQUEST['lon'] = current($arAboutNearRest["PROPERTIES"]["LON"]['VALUE']);

                global $arrFilter;

                $arrFilter["!ID"] = $arAboutNearRest['ID'];

                if(CITY_ID=='spb'){
                    $city_name_first = 'Санкт-Петербурга';
                    $city_name_second = 'Санкт-Петербурге';
                    $city_name_third = 'санкт-петербург';
                }
                elseif(CITY_ID=='msk'){
                    $city_name_first = 'Москвы';
                    $city_name_second = 'Москве';
                    $city_name_third = 'москва';
                }
                elseif(CITY_ID=='rga'){
                    $city_name_first = 'Риги';
                    $city_name_second = 'Риге';
                    $city_name_third = 'рига';
                }
                $APPLICATION->SetPageProperty("title",  "Ближайшие заведения к ресторану ".$arAboutNearRest['NAME']." - Рестораны $city_name_first.");
                $APPLICATION->SetPageProperty("description",  "Каталог ближайших заведений к ресторану ".$arAboutNearRest['NAME']." в $city_name_second.");
                $APPLICATION->SetPageProperty("keywords",  "рестораны, ближайшие, ".strtolower($arAboutNearRest['NAME']).", $city_name_third");
                $APPLICATION->SetPageProperty("H1",  "Ближайшие заведения к ресторану ".$arAboutNearRest['NAME']);
            }
        }

        FirePHP::getInstance()->info($_REQUEST);
        //FirePHP::getInstance()->info($arrFilter);

        $APPLICATION->IncludeComponent("restoran:restoraunts.list_optimized", "rest_list_full_size", Array(//$DISTANCE&&!$USER->IsAdmin()?"rest_list":
            "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => $_REQUEST["CITY_ID"],	// Код информационного блока
            "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],	// Код раздела
            "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
            "SORT_BY1" => $DISTANCE?"distance":"NAME",	// Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
            "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arrFilter",	// Фильтр
            "PROPERTY_CODE" => array(	// Свойства
                0 => "type",
                1 => "kitchen",
                2 => $_REQUEST["CATALOG_ID"]=='banket'?"banket_average_bill":"average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "RATIO",
                9 => "REST_NETWORK",
                10 => "FEATURES_IN_PICS",//
                11 => "STREET",
                12 => "number_of_rooms",
            ),
            "CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "CACHE_TYPE" => $USER->IsAdmin()?"N":'Y',	// Тип кеширования//Y       //
            "CACHE_TIME" => "86441",	// Время кеширования (сек.)
            "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "CACHE_NOTES" => "new3".$BestIblockUpdateNote,
            "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => $_REQUEST['PROPERTY']&&!$_REQUEST['arrFilter_pf']&&!$DISTANCE?"N":"Y",	// Выводить под списком
            "PAGER_TITLE" => "Рестораны",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36002",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "BEST_SECTION_ID" => $First20SectionId,
            //            "PLUS_PAGE" => $PLUS_PAGE,
            'NO_SLEEP_TO_NEAR'=>'Y', // using only for distance sort rests
            'AJAX_REQUEST' => $_REQUEST['AJAX_REQUEST'],
            'SHOW_REST_COUNT' => 'Y',
            'DISTANCE_VALUE'=>$_REQUEST['NEAR']?1000:'',
            'INCLUDE_SUBSECTIONS'=>'N'
        ),
            false
        );


    ?>
    <script>
        $(function(){
            var sessionLat = '<?= $_SESSION['lat'] ?>';
            var sessionLon = '<?= $_SESSION['lon'] ?>';
            var options_for_get_position = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            };
            var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';

            setTimeout(get_location_a, 1000);

            function get_location_a()
            {
                if (navigatorOn != 'Y') {
                    console.log('navigatorOff');
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(success_a, error_a, options_for_get_position);
                    } else {
                        console.log("Ваш браузер не поддерживает\nопределение местоположения");
                        return false;
                    }
                }
                else {
                    var lat_lon_list = new Object();
                    $('.pull-left:not(.banner-in-list-place)').each(function(indx, element){
                        lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                    })
                    $.ajax({
                        type: "POST",
                        url: "/tpl/ajax/get-distance.php",
                        data: {
                            lat_lon_list: lat_lon_list
//                            who: '321'
                        },
                        dataType:'json',
                        success: function(data) {
                            if(data){
                                $('.pull-left').each(function(indx, element){
                                    if(data[$(element).attr('this-rest-id')]){
                                        $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                                        $(element).find('.distance-place').show();
                                    }
                                })
                            }
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }
            }
            function success_a(position) {
                sessionLat = position.coords.latitude;
                sessionLon = position.coords.longitude;

//                console.log(sessionLat,'sessionLat');
//                console.log(sessionLon,'sessionLon');
                $.ajax({
                    type: "POST",
                    url: "/tpl/ajax/ajax-coordinates.php",
                    data: {
                        lat:sessionLat,
                        lon:sessionLon
                    },
                    success: function(data) {
                        <?if($DISTANCE&&!$_REQUEST['NEAR']&&$APPLICATION->get_cookie("COORDINATES")!='Y'):?>
                        location.reload();
                        <?else:?>
                        var lat_lon_list = new Object();
                        $('.pull-left').each(function(indx, element){
                            lat_lon_list[$(element).attr('this-rest-id')] = $(element).attr('lat')+','+$(element).attr('lon');
                        })
                        $.ajax({
                            type: "POST",
                            url: "/tpl/ajax/get-distance.php",
                            data: {
                                lat_lon_list: lat_lon_list
                                //                            who: '321'
                            },
                            dataType:'json',
                            success: function(data) {
                                if(data) {
                                    $('.pull-left').each(function(indx, element){
                                        if(data[$(element).attr('this-rest-id')]){
                                            $(element).find('.distance-place span').html(data[$(element).attr('this-rest-id')]);
                                            $(element).find('.distance-place').show();
                                        }
                                    })
                                }
                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });
                        <?endif?>
                    }
                });
            }
            function error_a(error)
            {
                console.log('ERROR(' + error.code + '): ' + error.message);
                <?if($DISTANCE&&!$_REQUEST['NEAR']):?>
                alert("Невозможно определить местоположение");
                <?endif?>
                return false;
            }
            $('[data-toggle="tooltip-pic"]').tooltip()
        })
    </script>

<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>

    <?if (!$_REQUEST["letter"]&&!$_REQUEST["RUBRIC"]&&!$_REQUEST['NEAR'])://!$_REQUEST["arrFilter_pf"]&&?>
    <div class="left-side">
        <div class="special">
            <?
            if(!$_REQUEST["PROPERTY"]):
                global $arrFilterTop;
                global $TopSpec;
//                    if (intval($_REQUEST["PRIORITY_kitchen"]))
//                    {
//                        $arrFilterTop = Array(
//                            "!ID" => $TopSpec[0],
//                            "PROPERTY_priority_kitchen" => intval($_REQUEST["PRIORITY_kitchen"]),
//                            "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
//                        );
//                    }
//                    elseif(intval($_REQUEST["PRIORITY_type"]))
//                    {
//                        $arrFilterTop = Array(
//                            "!ID" => $TopSpec[0],
//                            "PROPERTY_priority_type" => intval($_REQUEST["PRIORITY_type"]),
//                            "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
//                        );
//                    }
//                    else
//                    {
                $arrFilterTop = Array(
                    "!ID" => $TopSpec[0],
                    "PROPERTY_top_spec_place_2_VALUE" => Array("Да"),
                    //"PROPERTY_priority_type" => false,
                    //"PROPERTY_priority_kitchen" => false,
                    "!PROPERTY_sleeping_rest_VALUE" => "Да"
                );
//                    }
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "rest_list_top_spec",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arIB["ID"],
                        "NEWS_COUNT" => "1",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arrFilterTop",
                        "FIELD_CODE" => array(),
                        "PROPERTY_CODE" => array("type", "kitchen", "average_bill", "opening_hours", "phone", "address", "subway"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "160",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => $_REQUEST["CATALOG_ID"],
                        "CACHE_TYPE" => "Y",//Y
                        "CACHE_TIME" => "86409",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "CACHE_NOTES" => "Nw",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CONTEXT"=>$_REQUEST["CONTEXT"]
                    ),
                    false
                );
            else:
                $arIB = getArIblock("top_bottom_priorities",CITY_ID);
                global $arrTopBottomFilter;
                $arrTopBottomFilter = array('PROPERTY_BOTTOM_PRIORITY_VALUE'=>'Да');
                if($_REQUEST['CATALOG_ID']=='banket'){
                    $arrTopBottomFilter = array_merge($arrTopBottomFilter,array('PROPERTY_BANKET_VALUE'=>'Да'));
                }
                else {
                    $arrTopBottomFilter = array_merge($arrTopBottomFilter,array('!PROPERTY_BANKET_VALUE'=>'Да'));
                }
                $APPLICATION->IncludeComponent(
                    "restoran:top_bottom.priority",
                    "",
                    Array(
                        "IBLOCK_ID" => $arIB['ID'],
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000009",
                        'FILTER_NAME' => 'arrTopBottomFilter',
                        'PROPERTIES' => array(
                            'photos',
                            'subway',
                            'kitchen',
                            'type',
                            'average_bill',
                            'address',
                            'opening_hours',
                            'phone',
                            'RATIO'
                        )
                    ),
                    false
                );
            endif;
            ?>
        </div>
    </div>
    <div class="right-side">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                "TYPE" => "right_1_main_page",
                "NOINDEX" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
    <?endif;?>
    <div class="clearfix"></div>
    <div class="navigation <?if($_REQUEST["CATALOG_ID"]=='banket'||$_REQUEST["NEAR"]):?>no-map-link<?endif?>" style="margin-top: 70px;<?if($_REQUEST['PROPERTY']&&!$_REQUEST['arrFilter_pf']&&!$DISTANCE):?>display: none;<?endif?>"></div>

    <script>
        $(function(){
            $(".navigation").html($(".navigation_temp").html());
            $(".navigation_temp").remove();
//                $(".navigation_temp").show();
        })
    </script>

    <div class="footer-prop-desc"><?=$APPLICATION->ShowProperty("catalog_prop_description"); ?></div>
    <div id="for_seo"></div>

<?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
        "TYPE" => "bottom_rest_list",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
    ),
    false
);?>

    <?
    if($_REQUEST['PROPERTY']&&!$_REQUEST['PROPERTY2']&&LANGUAGE_ID!='en'){//$USER->IsAdmin()&&
        $items = $APPLICATION->IncludeComponent(
            "restoran:features-random.list",
            "",
            Array(
                "CACHE_TYPE" => "N",//
                "CACHE_TIME" => "36000000",
                "PROPERTY_CODE" => '',//array('eng_name')
                "NEWS_COUNT" => 10,
            ),
            false
        );
    }
?>

    </div>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?endif?>