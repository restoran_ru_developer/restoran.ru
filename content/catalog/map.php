<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
        <meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="HandheldFriendly" content="True" />
        <script src="/tpl/js/jquery.js"></script>
        <script src="//api-maps.yandex.ru/1.1/index.xml?key=AGRMQlABAAAAfPS8dgIANYILzsW2M4zTDBnwri3gU6y8xLIAAAAAAAAAAABs2XC5NTwB6BZL_vpFO9lBNUndeg==" type="text/javascript"></script>
        <style>
            html,body {margin:0px ;padding: 0px; width:100%; height:100%; font-size:6px;}
            a {font-size:150%;}
            .map_ballon {position:absolute; z-index:998; color:#FFF; cursor:pointer;}
            .map_ballon a {text-decoration: none; border: none;}
            .map_ballon .balloon_content { background: #3B414E;padding:10px; z-index:99999; color:#FFF; cursor:pointer;}
            .map_ballon .balloon_tail { background: url(/tpl/images/map/g_c.png) center top no-repeat; height:24px;}
            .map_ballon:hover {z-index:999}
            .map_ballon:hover .balloon_content { background: #24A6CF;padding:10px; z-index:99999; color:#FFF;}
            .map_ballon:hover .balloon_tail { background: url(/tpl/images/map/b_c.png) center top no-repeat; height:24px;}
        </style>
    </head>
    <body>
<?
if ((int)$_REQUEST["ID"])
{
    $res = CIBlockElement::GetByID((int)$_REQUEST["ID"]);
    if ($ar = $res->Fetch())
    {
        if ($ar["IBLOCK_ID"]==11)
            $ci="msk";
        if ($ar["IBLOCK_ID"]==12)
            $ci="spb";
        $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"map"));
        if($ar_props = $db_props->Fetch())
        {
            $mmm = $ar_props["VALUE"];        
            $m[] = $ar_props["VALUE"];
        }
        $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"subway"));
        while($ar_props = $db_props->Fetch())
        {
            $r = CIBlockElement::GetByID($ar_props["VALUE"]);
            if ($a = $r->Fetch())                
                $subway[] = $a["NAME"];  
        }
        $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
        while($ar_props = $db_props->Fetch())
        {
                $address[] = $ar_props["VALUE"];  
        }
    }    
}
?>
<div align="center">    
    <script type="text/javascript">
        function GetMap ()
        {
            YMaps.jQuery ( function () {
                map = new YMaps.Map ( YMaps.jQuery ( "#map_area" )[0] );
                var MTmap = new YMaps.MapType ( YMaps.MapType.MAP.getLayers(), "Карта" );
                var MTsat = new YMaps.MapType ( YMaps.MapType.SATELLITE.getLayers(), "Спутник" );
                var typeControl = new YMaps.TypeControl ( [] );
                typeControl.addType ( MTmap );
                typeControl.addType ( MTsat );

                map.setMinZoom (12);
                map.addControl (typeControl);
                map.addControl(new YMaps.Zoom());
                map.enableScrollZoom();
                YMaps.Events.observe ( map, map.Events.Update, function () {
                        ShowMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.TypeChange, function () {
                        hideMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.MoveEnd, function () {
                    ShowMyBalloon (0);
                } );
                YMaps.Events.observe ( map, map.Events.MoveStart, function () {
                    hideMyBalloon ();
                } );
                YMaps.Events.observe ( map, map.Events.ZoomRangeChange, function () {
                    hideMyBalloon ();
                } );
                    YMaps.Events.observe ( map, map.Events.Click, function () {
                    if(map.getZoom()<=16)
                        hideMyBalloon ();
                });
            })
            my_style = new YMaps.Style();
               my_style.iconStyle = new YMaps.IconStyle();
               my_style.iconStyle.href = "/tpl/images/map/ico_rest.png";
               my_style.iconStyle.size = new YMaps.Point(27, 32);
               my_style.iconStyle.offset = new YMaps.Point(-15, -32);
        }

        function SetMapCenter ( lat, lng, zoom_i )
        {
            zoom = zoom_i ? zoom_i : 16;
            map.setCenter ( new YMaps.GeoPoint ( lng, lat ), zoom );
        }

        function ShowMyBalloon (id)
        {
            if (typeof ( map_c[0] ) == 'undefined')
            {
                hideMyBalloon ();
                <?if($subway):
                    $metro123 = "<p class='metro_".CITY_ID."' style=margin-bottom:0px;>".(is_array($subway) ? implode(", ",$subway) : $subway)."</p>";
                endif;?>
                var point = map.converter.coordinatesToLocalPixels(map_markers[id].getGeoPoint () );
                jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'>Ресторан <?=$ar["NAME"]?><br /><?=$address[0]?><br /><?=$metro123?></div><div class='balloon_tail'> </div></div>");
                jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-10+"px");
                jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
            }
            else
            {
                hideMyBalloon ();

                var point = map.converter.coordinatesToLocalPixels(map_c[id].getGeoPoint () );
                jQuery("#map_area").append("<div id='baloon"+id+"' class='map_ballon'><div class='balloon_content'>Ресторан <?=$rest_name.$ar["NAME"]?><br />"+markers_data[id].adres+"</div><div class='balloon_tail'> </div></div>");
                jQuery("#baloon"+id).css("left",point.getX() - jQuery("#baloon"+id).width()/2-4+"px");
                jQuery("#baloon"+id).css("top",point.getY() - jQuery("#baloon"+id).height()+18+"px");
            }
        }

        function hideMyBalloon ()
        {
            jQuery(".map_ballon").remove();
        }
        function showMeRest(point, id)
        {
            console.log(point);
            point = point.split(",");     
            map.setZoom(15,{smooth:true,position:new YMaps.GeoPoint(point[1], point[0]),centering:true,callback:function(){
                    map.setZoom(16,{smooth:true,position:new YMaps.GeoPoint(point[1], point[0]),centering:true});
            }});
            //map.panTo(new YMaps.GeoPoint(point[1], point[0]), {flying:0,callback:function(a){
                    //map.panTo(new YMaps.GeoPoint(point[1], point[0]), {flying:1});
                //      map.setZoom(16,{smooth:true});
                //}
            //});


            //SetMapCenter ( point[0], point[1], 16 );
            ShowMyBalloon(id);
        }
        function count_prs(obj)
        {
           var count = 0; 
           for(var prs in obj) 
           { 
        count++;
           } 
           return count; 
        }
    </script>
    <script type="text/javascript">
        var map = null;
        var map_markers = {};
        var markers_data = {};
        var map_filter = {};
        var hidden_marker = null;
        var sat_map_type = false;
        var map_type = 'default';
        var to_show = null;
        var cur_user = 0;
        var my_style = {};
        var map_c = {};

        window.onload = OnPageLoad;
        function OnPageLoad (){
            GetMap();                            
            <?if (count($m>1)):?>
                <?foreach ($m as $key=>$coords):
                    if ($key==0)
                        $temp2 = explode(",",$coords);
                    $temp = explode(",",$coords);
                    ?>                                
                    map_c[<?=$key?>] = new YMaps.Placemark ( new YMaps.GeoPoint ( <?=$temp[1]?>, <?=$temp[0]?> ), { style: my_style, hasBalloon: false, db_id: <?=$key?>,hideIcon: false } );
                    map_c[<?=$key?>].id = <?=$key?>;
                    var data = {};
                    data.name = '<?=$ar["NAME"]?>';
                    data.adres = '<?=$address[$key]?>';
                    markers_data[<?=$key?>] = data;

                <?endforeach;?>
                    console.log(markers_data);
            <?endif;?>
            if (typeof ( map_c[0] ) == 'undefined')
            {
                var coord = "<?=$mmm?>";
                coord = coord.split(",");
                SetMapCenter ( coord[0], coord[1], 16 );
                map_markers[0] = new YMaps.Placemark ( new YMaps.GeoPoint ( coord[1], coord[0] ), { style: my_style, hasBalloon: false, db_id: 0,hideIcon: false } );
                map.addOverlay ( map_markers[0] );
                ShowMyBalloon(0);                
            }
            else
            {
                SetMapCenter ( '<?=$temp2[0]?>', '<?=$temp2[1]?>', 15 );
                for (var i=0;i<count_prs(map_c);i++)
                {
                    map.addOverlay (map_c[i]);
                    YMaps.Events.observe ( map_c[i], map_c[i].Events.Click, function ( m, e ) {
                        hideMyBalloon();
                        ShowMyBalloon(m.id);
                    } );
                    ShowMyBalloon(i);
                }
                //hideMyBalloon();
            }
        }
    </script>
    <div style="position: absolute; top: 10px; left:10px; z-index:1000">
        <a href="/"><img src="/tpl/images/logo.png" width="100"></a>
        <div class="phones" style="font-size:200%; padding-left:30px; font-family: Georgia; margin-top:0px;">
            <?if ($ci == "spb"):?>
                <span style="font-size:150%;">812</span> 740 18 20
            <?elseif ($ci == "msk"):?>
                <span>495</span> 988 26 56
            <?endif;?>
        </div>
    </div>
    <div id="map_area" style="width: 100%;margin: 0 auto;height: 100%;"></div>
</div>
        </body>
</html>