<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
LocalRedirect(preg_replace('/\/group\//','/restaurants/group/',$APPLICATION->GetCurDir()),false,'301 Moved permanently');

if ($_REQUEST["CODE"]):
        global $DB;
        $arIB = getArIblock("rest_group", CITY_ID);
        $sql = "SELECT ID,NAME FROM b_iblock_element WHERE IBLOCK_ID=".$arIB["ID"]." AND CODE='".$DB->ForSql($_REQUEST["CODE"])."' LIMIT 1";
        $res = $DB->Query($sql);
        if ($ar = $res->Fetch())
        {            
            $id = $ar["ID"];
            $name = $ar["NAME"];
        }        
endif;
$arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
?>
<div class="block">    
    <?$APPLICATION->SetTitle("Ресторанная группа «".$ar["NAME"]."»")?>
        <h1>
            Ресторанная группа «<?=$ar["NAME"]?>»            
        </h1>
        <div class="pull-right" style="width:400px;margin-top:7px;">
            <a href="/tpl/ajax/online_order_rest.php" class="booking pull-left" style="margin-right:10px;" data-id='<?=$arResult["ID"]?>' data-restoran='<?=$arResult["NAME"]?>'><img src="/tpl/images/bronn.png" /></a>
            <?if (CITY_ID=="msk"):?>
                <div class="gphone" style="font-size:24px;">• <span style="color:#CCC" class="code">495</span> <span class="tel">988-26-56</span> •</div>
            <?elseif(CITY_ID=="spb"):?>
                <div class="gphone" style="font-size:24px;">• <span style="color:#CCC" class="code">812</span> <span class="tel">740-18-20</span> •</div>
            <?endif;?>
        </div>
        <div class="clearfix"></div>
        <div class="left-side">             
            <?if (!$_REQUEST["PAGEN_1"]||$_REQUEST["PAGEN_1"]==1):?>
                <div class="preview_seo_text">
                    <?$APPLICATION->ShowViewContent("preview_seo_text");?>
                </div>
            <?endif;?>
            <div class='news-list detail_article'>
            <?
            if ($id)
            {   
                $_REQUEST["page"] = 1;
                $_REQUEST["PAGEN_1"] = 1;
                global $arSimilar;        
                $arSimilar["PROPERTY_rest_group"] = $id;     
                    $APPLICATION->IncludeComponent("restoran:catalog.list", "rest3list", array(
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arIB["ID"],
                            "PARENT_SECTION_CODE" => "",//$_REQUEST["CATALOG_ID"],
                            "NEWS_COUNT" => "50",
                            "SORT_BY1" => "NAME",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arSimilar",
                            "PROPERTY_CODE" => Array("rest_group","type","kitchen","average_bill","RATIO"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_SHADOW" => "Y",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "Y",
                            "CACHE_TIME" => "3600000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "N",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Рестораны",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "REST_PROPS"=>"Y"
                            ),
                            false
                    );
            }
            else
            {
                ShowError("Группа не найдена");
            }?>
            </div>
        </div>
        <div class="right-side">

<!--            --><?//$APPLICATION->IncludeComponent(
//                "bitrix:main.include",
//                "",
//                Array(
//                    "AREA_FILE_SHOW" => "file",
//                    "PATH" => SITE_TEMPLATE_PATH."/include_areas/new-order-form-with-city.php",
//                    "EDIT_TEMPLATE" => ""
//                ),
//                false
//            );?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>                    
            <div class="title">Рекомендуем</div>
                <?
                $arRestIB = getArIblock("catalog", CITY_ID);
                $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
                $arPFilter["!PREVIEW_PICTURE"] = false;
                $arPFilter["!PROPERTY_restoran_ratio"] = false;            
                ?>
                <?
                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list",
                    "recomended",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "rand",
                        "SORT_ORDER1" => "",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended",
                        "REST_PROPS"=>"Y"
                    ),
                false
                );?>                                
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                    ),
                    false
                );?>        
            <div class="title">Популярные</div>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "show_counter",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "rand",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "popular",
                    "REST_PROPS"=>"Y"
                ),
            false
            );?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_3_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>    
        <div class="clearfix"></div>        
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>      
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>