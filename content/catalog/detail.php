<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.js"></script>
<script type="text/javascript">
    var slider_phone;
    $(function(){
        $(".fancy").fancybox({
            afterShow: function() {
                if(slider_phone){
                    $("<div class='phone-in-detail-photo'>"+slider_phone+"</div>").prependTo('.fancybox-inner');
                }
            },
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'cyclic'            :       true,
            'overlayColor'      :       '#1a1a1a',
            'speedIn'		:	200,
            'speedOut'		:	200
        });
    });
</script>

<?

$arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);

if (RestIBlock::IsSleeping($_REQUEST["RESTOURANT"]))    
    $templ = "sleep";

if (RestIBlock::IsClosed($_REQUEST["RESTOURANT"]))    
    $templ2 = "closed";

if ($templ2 == "closed"&&$templ == "sleep")
    $templ = "closed";

if(!$templ){
    if(!RestIBlock::IsOldNetwork($_REQUEST["RESTOURANT"])){
        $templ='_.default';
    }

    if(!RestIBlock::IsNetwork($_REQUEST["RESTOURANT"])){
        $SHOW_YA_SCHEME = true;
    }
//    $templ = '.default';
}

?>
<?

FirePHP::getInstance()->info($templ);
if($templ=='_.default'):?>
    <div class="block">
        <div class="restoran-detail network-design" <?if($SHOW_YA_SCHEME):?>itemscope itemtype="http://schema.org/Organization"<?endif?>>
            <div class="restoran_name_view_content">
                <?$APPLICATION->ShowViewContent("restoran_name_view_content");?>
            </div>

            <?$APPLICATION->ShowViewContent("restoran_counter_content");?>

            <?$APPLICATION->IncludeComponent("restoran:restoraunts.detail", $templ, array(
//                'REST_PREVIEW'=>'Y',
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arIB["ID"],
                "ELEMENT_ID" => "",
                "ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
                "CHECK_DATES" => "Y",
                "PROPERTY_CODE" => array(
                    0 => "type",
                    1 => "average_bill",
                    2 => "kitchen",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "administrative_distr",
                    6 => "area",
                    7 => "address",
                    8 => "subway",
                    9 => "number_of_rooms",
                    10 => "music",
                    11 => "clothes",
                    12 => "proposals",
                    13 => "credit_cards",
                    14 => "discouncards",
                    15 => "bankets",
                    16 => "touristgroups_1",
                    17 => "banketnayasluzhba",
//                            18 => "kolichestvochelovek",
                    19 => "stoimostmenyu",
                    20 => "max_check",
                    21 => "entertainment",
                    22 => "wi_fi",
                    23 => "hrs_24",
                    24 => "parking",
                    25 => "features",
                    26 => "out_city",
                    27 => "min_check",
                    28 => "site",
                    29 => "children",
                    30 => "ideal_place_for",
                    31 => "offers",
                    32 => "email",
                    33 => "discounts",
                    34 => "landmarks",
                    35 => "map",
                    36 => "RATIO",
                    37 => "COMMENTS",
                    38 => "okrugdel",
                    39 => "kuhnyadostavki",
                    40 => "viduslug",
                    41 => "add_props",
                    42 => "network",
                    43 => "rest_group",
                    44 => "breakfasts",
                    45 => "business_lunch",
                    46 => "branch",
                    47 => "rent",
                    48 => "my_alcohol",
                    49 => "catering",
                    50 => "food_delivery",
                    51 => "proposals",
                    52 => "d_tours",
                    53 => "sale10",
                    54 => "banket_average_bill",
                    55 => "place_new_rest",
                    56=>"FOURSQUARE_USER_LOGIN",
                    57=>"INSTAGRAM_USER_LOGIN",
                    58=>"without_reviews",
                    59=>$_REQUEST['CATALOG_ID']=='banket'?"RENTING_HALLS_COST":'',
                    60=>$_REQUEST['CATALOG_ID']=='banket'?"ALLOWED_ALCOHOL":'',
                    61=>$_REQUEST['CATALOG_ID']=='banket'?"BANKET_SPECIAL_EQUIPMENT":'',
                    62=>$_REQUEST['CATALOG_ID']=='banket'?"BANKET_ADDITIONAL_OPTION":'',
                    63=>$_REQUEST['CATALOG_ID']=='banket'?"BANKET_MENU_SUM":'',
                    64=>'STREET',
                    65=>'FEATURES_IN_PICS',
                    66=>'opening_hours_google',
                    67=>'REST_NETWORK',
                    68=>'NETWORK_REST'
                ),
                "ADD_REVIEWS" => "Y",
                "REVIEWS_BLOG_ID" => "3",
                "SIMILAR_OUTPUT" => "Y",
                "SIMILAR_PROPERTIES" => array(
                    0 => "kitchen",
                    //1 => "average_bill",
                ),
                "IBLOCK_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//
                "CACHE_TIME" => "36000127",
                "CACHE_GROUPS" => "N",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Страница",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                'TO_LINKS_CATALOG_ID' => $_REQUEST['CATALOG_ID']
            ),
                false
            );?>


            <?$APPLICATION->ShowViewContent("restoran_menu");?>


            <div class="clearfix"></div>
        <?if(ERROR_404!='Y'):?>
            <?$en_ru_element_id = $en_ru_element_id?array($element_id,$en_ru_element_id):array($element_id)?>
            <?unset($INSTAGRAM_USER_LOGIN);?>
            <?if ($FOURSQUARE_USER_LOGIN||$INSTAGRAM_USER_LOGIN||$count['PHOTO_REVIEWS_COUNTER']):?>
                <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
                <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/source/jquery.fancybox.js"></script>
                <script>
                    $(function(){
                        $(".fancybox").fancybox();
                    });
                </script>
                <?//$count["PHOTO_COUNT"]?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        <?//TODO заменить lang?>
                        Фото пользователей
                    </div>
                    <ul class="nav nav-tabs">
                        <?if($count['PHOTO_REVIEWS_COUNTER']):?>
                            <li class=""><a href="#reviews-photos" id="reviews_photos_link" data-toggle="tab" class="ajax" data-href="/tpl/ajax/get_reviews_photos.php?ID=<?=implode('|',$en_ru_element_id)?>&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>&CITY_ID=<?=CITY_ID?>">Restoran.ru</a><span>|</span></li>
                            <?if(!$FOURSQUARE_USER_LOGIN&&!$INSTAGRAM_USER_LOGIN):?>
                            <script>
                                $(function(){
                                    $("#reviews_photos_link").click();
                                })
                            </script>
                            <?endif?>
                        <?endif;?>
                        <?if($FOURSQUARE_USER_LOGIN):?>
                            <li class="<?if(!$INSTAGRAM_USER_LOGIN)echo "active";?>"><a href="#forsquare-widget" data-toggle="tab">Foursquare</a><span>|</span></li>
                        <?endif?>
                        <?if($INSTAGRAM_USER_LOGIN):?>
                            <li class="active"><a href="#instagram-widget" data-toggle="tab">Instagram</a><span>|</span></li>
                        <?endif;?>

                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content">
                    <?if($FOURSQUARE_USER_LOGIN):?>
                        <div class="tab-pane sm <?=(!$INSTAGRAM_USER_LOGIN)?"active":""?>" id="forsquare-widget">
                            <script>
                                $(function(){
                                    var iteration = 0;
                                    var remote_data_sq;
                                    var params = {"sq_place_id":$('.get-sq-photos-trigger').attr("restoran-fq-id")};
                                    $.ajax({
                                        type: "POST",
                                        url: $('.get-sq-photos-trigger').attr("href"),
                                        data: params,
                                        dataType: 'json'
                                    })
                                        .done(function(data) {
                                            console.log(data);
                                            remote_data_sq = data;
                                            if(data.meta.code==200){
                                                $('.sq-photos-count').text(data.response.photos.count);

                                                for(key in data.response.photos.items){
                                                    if(key>9)
                                                        break;
                                                    one_pic_url = data.response.photos.items[key].prefix+'width81'+data.response.photos.items[key].suffix;
                                                    full_pic_url = data.response.photos.items[key].prefix+'width'+data.response.photos.items[key].width+data.response.photos.items[key].suffix;
                                                    $('.sq-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                }
                                            }
                                            else {
                                                $('.sq-photos-container').text('ошибка доступа, попробуйте обновить страницу или зайдите позже');
                                                $('a.get-sq-photos-trigger').addClass('no-active');
                                            }
                                        });

                                    $('.clear-ig-fs-result.sq-photos-link').on('click', function(){
                                        if(!$(this).hasClass('no-active')){
                                            $('a.get-sq-photos-trigger').removeClass('no-active');
                                            $('.sq-photos-container ul li').each(function(indx, element){
                                                if(indx>9){
                                                    $(element).remove();
                                                }
                                            });
                                            $(this).addClass('no-active');

                                            $('html, body').stop().animate({
                                                scrollTop: $('#forsquare-widget').offset().top-60
                                            }, 500);
                                        }
                                        return false;
                                    });

                                    $(document).on('click', 'a.get-sq-photos-trigger', function (e) {
                                        if($(this).hasClass('no-active'))
                                            return false;
                                        iteration++;

                                        count_photos = $('.sq-photos-container ul li').length;

                                        if(remote_data_sq.meta.code==200){
                                            for(key in remote_data_sq.response.photos.items){
                                                key = parseInt(key)+count_photos;

                                                module_key = 20*iteration;
                                                if(key>=remote_data_sq.response.photos.count){
                                                    $('a.get-sq-photos-trigger').addClass('no-active');
                                                    break;
                                                }
                                                if(key%module_key==0)
                                                    break;
                                                one_pic_url = remote_data_sq.response.photos.items[key].prefix+'width232'+remote_data_sq.response.photos.items[key].suffix;
                                                full_pic_url = remote_data_sq.response.photos.items[key].prefix+'width'+remote_data_sq.response.photos.items[key].width+remote_data_sq.response.photos.items[key].suffix;
                                                $('.sq-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                $('.clear-ig-fs-result.sq-photos-link').removeClass('no-active');
                                            }
                                        }
                                        else {
                                            $('.sq-photos-container').text('ошибка доступа, попробуйте обновить страницу или зайдите позже');
                                        }

                                        return false;
                                    });
                                })
                            </script>
                            <div class="sq-photos-container">
                                <ul></ul>
                            </div>
                            <noindex>
                                <a href="/tpl/ajax/get_sq_photos.php" restoran-fq-id="<?=$FOURSQUARE_USER_LOGIN?>" class="get-sq-photos-trigger" >Показать еще фотографии</a>
                                <a href="#" class="clear-ig-fs-result no-active sq-photos-link" >Скрыть</a>
                            </noindex>
                        </div>
                    <?endif?>
                    <?if($INSTAGRAM_USER_LOGIN):?>
                        <div class="tab-pane active sm" id="instagram-widget">
                            <script>
                                $(function(){
                                    var iteration = 0;
                                    var remote_data_instagram;
                                    var params = {"USER_LOGIN":$('.get-instagram-photos-trigger').attr("restoran-instagram-id")};

                                    $.ajax({
                                        type: "POST",
                                        url: $('.get-instagram-photos-trigger').attr("href"),
                                        data: params,
                                        dataType: 'json'
                                    })
                                        .done(function(data) {

//                                            console.log(data,'inst data');
                                            remote_data_instagram = data;
                                            if(data.meta.code==200){

                                                $('.instagram-photos-count').text(data.data.length);
                                                for(key in data.data){
                                                    if(key>9)
                                                        break;
                                                    one_pic_url = data.data[key].images.thumbnail.url;
                                                    full_pic_url = data.data[key].images.standard_resolution.url;
                                                    $('.instagram-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                }
                                            }
                                            else {
                                                $('.instagram-photos-container').text('ошибка доступа, попробуйте обновить страницу или зайдите позже');
                                                $('a.get-instagram-photos-trigger').addClass('no-active');
                                            }
                                        });

                                    $('.clear-ig-fs-result.instagram-photos-link').on('click', function(){
                                        if(!$(this).hasClass('no-active')){
                                            $('a.get-instagram-photos-trigger').removeClass('no-active');
                                            $('.instagram-photos-container ul li').each(function(indx, element){
                                                if(indx>9){
                                                    $(element).remove();
                                                }
                                            });
                                            $(this).addClass('no-active');

                                            $('html, body').stop().animate({
                                                scrollTop: $('#instagram-widget').offset().top-60
                                            }, 500);
                                        }
                                        return false;
                                    });

                                    $('a.get-instagram-photos-trigger').on('click', function (e) {
                                        if($(this).hasClass('no-active'))
                                            return false;

                                        iteration++;

                                        count_photos = $('.instagram-photos-container ul li').length;
                                        //console.log(count_photos);

                                        if(remote_data_instagram.meta.code==200){
                                            for(key in remote_data_instagram.data){
                                                key = parseInt(key)+count_photos;
                                                module_key = 20*iteration;

                                                if(key>=remote_data_instagram.data.length){
                                                    $('a.get-instagram-photos-trigger').addClass('no-active');
                                                }
                                                if(key%module_key==0 || key>=remote_data_instagram.data.length)
                                                    break;
                                                one_pic_url = remote_data_instagram.data[key].images.thumbnail.url;
                                                full_pic_url = remote_data_instagram.data[key].images.standard_resolution.url;
                                                $('.instagram-photos-container ul').append('<li><a class="fancybox" rel="group" href="'+full_pic_url+'" ><img src="'+one_pic_url+'" width="81" height="" alt=""></a></li>');
                                                $('.clear-ig-fs-result.instagram-photos-link').removeClass('no-active');
                                            }
                                        }
                                        else {
                                            $('.instagram-photos-container').text('ошибка доступа, попробуйте обновить страницу или зайдите позже');
                                        }


                                        return false;
                                    });
                                });
                            </script>
                            <div class="instagram-photos-container">
                                <ul></ul>
                            </div>
                            <noindex>
                                <a href="/tpl/ajax/get_instagram_photos.php" restoran-instagram-id="<?=$INSTAGRAM_USER_LOGIN?>" class="get-instagram-photos-trigger" >Показать еще фотографии</a>
                                <a href="#" class="clear-ig-fs-result no-active instagram-photos-link" >Скрыть</a>
                            </noindex>
                        </div>
                    <?endif?>
                    <?if($count['PHOTO_REVIEWS_COUNTER']):?>
                        <div class="tab-pane sm" id="reviews-photos"></div>
                    <?endif;?>
                </div>
                <?unset($FOURSQUARE_USER_LOGIN, $INSTAGRAM_USER_LOGIN)?>
            <?endif;?>

<!--            <div class="left-side">-->
            <?
            if ($without_reviews!="Да"):?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        <?//TODO заменить lang?>
                        Отзывы
                    </div>
                    <ul class="nav nav-tabs">
                        <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <script>
                            $(function(){
                                console.log('/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/reviews.php?id=<?=implode('|',$en_ru_element_id)?>&wr=<?=$without_reviews?>&url=<?=$APPLICATION->GetCurPage()?>&NEW_DESIGN=Y&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>','get-adress');
                                ajax_load = 1;
                                $.ajax({
                                    <?//if($USER->IsAdmin()):?>
                                    'url':'/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/reviews.php?id=<?=implode('|',$en_ru_element_id)?>&wr=<?=$without_reviews?>&url=<?=$APPLICATION->GetCurPage()?>&NEW_DESIGN=Y&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>'
                                    <?//else:?>
//                                    'url':'/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/reviews.php?id=<?//=$element_id?>//&wr=<?//=$without_reviews?>//&url=<?//=$APPLICATION->GetCurPage()?>//&NEW_DESIGN=Y&REST_NETWORK=<?//=implode('|',$REST_NETWORK)?>//'
                                    <?//endif?>
                                })
                                .done(function(data){
                                    $("#reviews").html(data);
                                    ajax_load = 0;
                                });
                            });
                        </script>

                        <li class=""><a href="#so34c" data-toggle="tab">Facebook</a></li>
                        <li class=""><a href="#so3c" data-toggle="tab">Вконтакте</a><span>|</span></li>
                        <li class="active"><a href="#reviews" data-toggle="tab">Restoran</a><span>|</span></li>
                    </ul>
                    <div class="tabs-center-line"></div>
                </div>


                <div class="tab-content float-review-block-wrapper" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                    <?$APPLICATION->ShowViewContent("AggregateRating");?>

                    <div class="tab-pane active sm reviews network-design-reviews" id="reviews"></div>
                    <div class="tab-pane sm" id="so3c">
                        <!-- Put this script tag to the <head> of your page -->
                        <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?45"></script>
                        <script type="text/javascript">
                            VK.init({apiId: <?=(CITY_ID=="tmn")?"3559173":"2881483"?>, onlyWidgets: true});
                        </script>
                        <!-- Put this div tag to the place, where the Comments block will be -->
                        <div id="vk_comments"></div>
                        <script type="text/javascript">
                            VK.Widgets.Comments("vk_comments", {limit: 20, width: "728", attach: false});
                        </script>
                    </div>
                    <div class="tab-pane sm" id="so34c">
                        <div class="fb-comments" data-href="http://<?=SITE_SERVER_NAME?><?=$APPLICATION->GetCurPage()?>" data-numposts="20" data-colorscheme="light" data-width="728"></div>
                    </div>
                </div>
            <?else:?>
                <div class="tab-str-wrapper" >
                    <div class="nav-tab-str-title">
                        Отзывы
                    </div>
                    <ul class="nav nav-tabs">
                        <?$url = str_replace("detailed","opinions",$arResult["DETAIL_PAGE_URL"]);?>
                        <script>
                            $(function(){
                                ajax_load = 1;
                                $.ajax({
                                    'url':'/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/reviews.php?id=<?=$element_id?>&wr=<?=$without_reviews?>&url=<?=$APPLICATION->GetCurPage()?>&NEW_DESIGN=Y&REST_NETWORK=<?=implode('|',$REST_NETWORK)?>'
                                })
                                    .done(function(data){
                                        $("#reviews").html(data);
                                        ajax_load = 0;
                                    });
                            });
                        </script>
                        <li class="active"><a href="#reviews" data-toggle="tab">Отзывы</a></li>
                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content float-review-block-wrapper" >
                    <div class="tab-pane active sm reviews network-design-reviews" id="reviews"></div>
                </div>
            <?endif;?>
            <div class="right-side">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "",
                    Array(
                        "TYPE" => "right_1_main_page",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0",
                        'FROM_RIGHT_OF_REVIEWS' => 'Y'
                    ),
                    false
                );?>
            </div>
            <div class="clearfix"></div>
            <?

            if ($count["AFISHA_COUNT"]):
                ?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        Афиша
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane sm active afisha poster network-design-afisha" id="afisha">
                        <?  $arAfishaIB = getArIblock("afisha", CITY_ID);
                        global $arrFilter;
                        $arrFilter["PROPERTY_RESTORAN"] =(int)$element_id;
                        //$arrFilter["!PROPERTY_RESTORAN"] = false;
                        $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "afisha_list_main_rest_network_design",
                            Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "afisha",
                                "IBLOCK_ID" => $arAfishaIB["ID"],
                                "NEWS_COUNT" => "60",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "property_EVENT_DATE",
                                "SORT_ORDER2" => "DESC",
                                "FILTER_NAME" => "arrFilter",
                                "FIELD_CODE" => array("DETAIL_PICTURE"),
                                "PROPERTY_CODE" => array("EVENT_TYPE","TIME","RESTORAN","EVENT_DATE"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j M Y",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "Y",
                                "CACHE_TIME" => "3600016",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                            ),
                            false
                        );
                        ?>
                        <div class="more_links text-right">
                            <a href="/<?=CITY_ID?>/afisha/?restoran=<?=$element_id?>" class="btn btn-light">ВСЯ АФИША ресторана</a>
                        </div>
                    </div>
                </div>
            <?endif;?>


            <div class="clearfix"></div>


            <?if($count["NEWS_COUNT"]||$count["VIDEO_NEWS_COUNT"]||$count["OVERVIEWS_COUNT"]||$count["PHOTO_COUNT"]||$count["BLOG_COUNT"]||$count["MC_COUNT"]||$count["INTERVIEW_COUNT"])://?>
                <div class="tab-str-wrapper">
                    <div class="nav-tab-str-title">
                        Новости ресторана
                    </div>
                    <ul class="nav nav-tabs">
                        <?if ($count["VIDEO_NEWS_COUNT"]):?>
                            <li>
                                <a href="#videonews" id="videonews_link" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/videonews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Видеоновости</a><span>|</span>
                            </li>
                            <?if(!$count["NEWS_COUNT"]&&!$count["BLOG_COUNT"]&&!$count["MC_COUNT"]&&!$count["INTERVIEW_COUNT"]&&!$count["PHOTO_COUNT"]&&!$count["OVERVIEWS_COUNT"]):?>
                                <script>
                                    $(function(){
                                        $("#videonews_link").click();
                                    })
                                </script>
                            <?endif;?>
                        <?endif?>

                        <?if ($count["OVERVIEWS_COUNT"]):?>
                            <li >
                                <a id="overviews_link" href="#overviews" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/overviews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Обзоры</a><span>|</span>
                            </li>
                            <?if(!$count["NEWS_COUNT"]&&!$count["BLOG_COUNT"]&&!$count["MC_COUNT"]&&!$count["INTERVIEW_COUNT"]&&!$count["PHOTO_COUNT"]):?>
                                <script>
                                    $(function(){
                                        $("#overviews_link").click();
                                    })
                                </script>
                            <?endif;?>
                        <?endif;?>

                        <?if ($count["PHOTO_COUNT"]):?>
                            <li class="">
                                <a id="photo_link" href="#photoreviews" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/photoreviews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Фотоотчеты</a><span>|</span>
                            </li>
                            <?if(!$count["NEWS_COUNT"]&&!$count["BLOG_COUNT"]&&!$count["MC_COUNT"]&&!$count["INTERVIEW_COUNT"]):?>
                                <script>
                                    $(function(){
                                        $("#photo_link").click();
                                    })
                                </script>
                            <?endif;?>
                        <?endif;?>

                        <?if ($count["INTERVIEW_COUNT"]):?>
                            <li >
                                <a id="interview_link" href="#interview" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/interviews.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Интервью</a><span>|</span>
                            </li>
                            <?if(!$count["BLOG_COUNT"]&&!$count["MC_COUNT"]&&!$count["NEWS_COUNT"]):?>
                                <script>
                                    $(function(){
                                        $("#interview_link").click();
                                    })
                                </script>
                            <?endif;?>
                        <?endif;?>

                        <?if ($count["MC_COUNT"]):?>
                            <li>
                                <a href="#masterclass" id="master_link" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/.default/masterclass.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Мастер-классы</a><span>|</span>
                            </li>
                            <?if(!$count["BLOG_COUNT"]&&!$count["NEWS_COUNT"]):?>
                                <script>
                                    $(function(){
                                        $("#master_link").click();
                                    })
                                </script>
                            <?endif;?>
                        <?endif;?>

                        <?if ($count["BLOG_COUNT"]):?>
                            <li ><a href="#blogs" id="blogs_link" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/blogs.php?id=<?=$element_id?>&url=<?=$APPLICATION->GetCurPage()?>" class="ajax" data-toggle="tab">Блоги</a><span>|</span></li>
                            <?if(!$count["NEWS_COUNT"]):?>
                                <script>
                                    $(function(){
                                        $("#blogs_link").click();
                                    })
                                </script>
                            <?endif;?>
                        <?endif;?>

                        <?if ($count["NEWS_COUNT"]):?>
                            <li class="active" ><a href="#news" id="news_link" data-toggle="tab">Новости</a><span>|</span></li>
                        <?endif;?>

                    </ul>
                    <div class="tabs-center-line"></div>
                </div>
                <div class="tab-content">
                    <?if ($count["NEWS_COUNT"]):?>
                        <div class="tab-pane medium active" id="news">
                            <?  $arNewsIB = getArIblock("news", CITY_ID);
                            global $arrNewsFilter;
                            if ($arNewsIB["ID"]):
                                $arrNewsFilter = Array("PROPERTY_RESTORAN"=>(int)$element_id);
                                $APPLICATION->IncludeComponent(
                                    "bitrix:news.list",
                                    "detailed_news_rest",
                                    Array(
                                        "DISPLAY_DATE" => "Y",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "Y",
                                        "AJAX_MODE" => "N",
                                        "IBLOCK_TYPE" => "news",
                                        "IBLOCK_ID" => $arNewsIB["ID"],
                                        "NEWS_COUNT" => "2",
                                        "SORT_BY1" => "ACTIVE_FROM",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "arrNewsFilter",
                                        "FIELD_CODE" => array("DETAIL_PICTURE"),
                                        "PROPERTY_CODE" => array("COMMENTS","RESTORAN"),
                                        "CHECK_DATES" => "N",
                                        "DETAIL_URL" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "150",
                                        "ACTIVE_DATE_FORMAT" => "j F Y",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => ($_REQUEST["letn"]=="Y")?"letnie_verandy":"",
                                        "CACHE_TYPE" => "Y",
                                        "CACHE_TIME" => "14402",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "Новости",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => "",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "REST_ID" => (int)$element_id,
                                        "WITHOUT_LINK" => ($_REQUEST["letn"]=="Y")?"Y":""
                                    ),
                                    false
                                );
                            endif;?>
                        </div>
                    <?endif;?>
                    <?if ($count["VIDEO_NEWS_COUNT"]):?>
                        <div class="tab-pane medium" id="videonews"></div>
                    <?endif?>
                    <?if ($count["OVERVIEWS_COUNT"]):?>
                        <div class="tab-pane medium" id="overviews"></div>
                    <?endif;?>
                    <?if ($count["PHOTO_COUNT"]):?>
                        <div class="tab-pane medium" id="photoreviews"></div>
                    <?endif;?>

                    <?//блоги?>
                    <?if ($count["BLOG_COUNT"]):?>
                        <div class="tab-pane medium" id="blogs"></div>
                    <?endif;?>
                    <?if ($count["MC_COUNT"]):?>
                        <div class="tab-pane medium" id="masterclass"></div>
                    <?endif;?>

                    <?if ($count["INTERVIEW_COUNT"]):?>
                        <div class="tab-pane medium" id="interview"></div>
                    <?endif;?>
                </div>
            <?endif?>


            <?
            // TODO заменить в шаблоне проверку
            if ($count["SPEC_COUNT"]):
                $ar1 = getArIblock("special_projects", CITY_ID,"easter_");
                $ar2 = getArIblock("special_projects", CITY_ID,"8marta_");
                $ar3 = getArIblock("special_projects", CITY_ID,"valentine_");
                $ar4 = getArIblock("special_projects", CITY_ID,"post_");
                $ar5 = getArIblock("special_projects", CITY_ID,"new_year_night_");
                $ar6 = getArIblock("special_projects", CITY_ID,"new_year_corp_");
                $ar7 = getArIblock("special_projects", CITY_ID,"letnie_verandy_");
                $ar8 = getArIblock("special_projects", CITY_ID,"sport_");
                $ar9 = getArIblock("special_projects", CITY_ID,"february23_");
                $ar_spec = Array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],$ar6["ID"],$ar7["ID"],$ar8["ID"],$ar9["ID"]);
                global $arrNewsFilter;
                $arrNewsFilter = Array("PROPERTY_RESTORAN"=>(int)$element_id);

                $APPLICATION->IncludeComponent(
                    "restoran:catalog.list_multi",
                    "special_projects_network_design",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "special_projects",
                        "IBLOCK_ID" => $ar_spec,
                        "NEWS_COUNT" => 2,
                        "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "timestamp_x"),
                        "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrNewsFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","summa_golosov"),
                        "CHECK_DATES" => "N",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",//Y
                        "CACHE_TIME" => "3610",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "search_rest_list",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                    ),
                    false
                );
            endif;?>


            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "bottom_rest_list",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>




            <?if((($count["CONTACTS"]==1&&$count["LAT"]&&$count["LON"])||$similar_rest||$RGROUP["ID"])):?>
                <div class="main_2015_summer_content">
                    <div class="index-journal-wrapper ">
                        <div class="index-journal-bg-line"></div>
                        <div class="index-journal-title-wrapper"><div class="index-journal-title">Смотрите также</div></div>

                        <div class="index-nav-tabs-wrapper">
                            <?if($count["CONTACTS"]==1&&$count["LAT"]&&$count["LON"]):?>
                                <a href="/<?=CITY_ID?>/detailed/<?=$_REQUEST['CATALOG_ID']?>/<?=$_REQUEST["RESTOURANT"]?>/near/" class="btn btn-info btn-nb-empty link-to-all-from-journal" id="link-to-all-near-rests">Смотреть все</a>
                            <?endif?>
                            <?if ($RGROUP["CODE"]):?><a href="/<?=CITY_ID?>/catalog/restaurants/group/<?=$RGROUP["CODE"]?>/" class="btn btn-info btn-nb-empty display-none link-to-all-from-journal" id="link-to-all-near-rests-rest-group" style="display: none;">Смотреть все</a><?endif?>
                            <ul class="nav nav-tabs new-history">
                                <?if($count["CONTACTS"]==1&&$count["LAT"]&&$count["LON"]):?>
                                    <li class="active"><a href="#near-rests" data-toggle="tab" class="ajax">Ближайшие рестораны</a></li>
                                <?elseif($similar_rest):?>
                                    <li class="active"><a href="#near-rests" data-toggle="tab" class="ajax">Похожие рестораны</a></li>
                                <?endif?>

                                <?if ($RGROUP["ID"]):?><li><a href="#near-rests-rest-group" data-href="/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/rest-group.php?id=<?=$RGROUP["ID"]?>&restoran=<?=$element_id?>&catalog_id=<?=$_REQUEST['CATALOG_ID']?>" data-toggle="tab" class="ajax">Ресторанная группа <?=$RGROUP["NAME"]?></a></li><?endif?>
                            </ul>
                        </div>
                        <?if(($count["CONTACTS"]!=1||!$count["LAT"]||!$count["LON"])&&!$similar_rest&&$RGROUP['ID']):?>
                            <script src="<?=SITE_TEMPLATE_PATH?>/js/bxslider-4-master/dist/jquery.bxslider.min.js"></script>
                            <script>
                                $(function(){
                                    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                                        console.log('was shown');
                                        $('.near-rest-slider.group-slider:not(.no-four-slide)').bxSlider({
                                            pager:false,
                                            prevSelector:'.group-prev-slide',
                                            nextSelector:'.group-next-slide',
                                            slideWidth:200,
                                            moveSlides:1,
                                            minSlides:4,
                                            maxSlides:4,
                                            slideMargin: 30,
                                            hideControlOnEnd: true,
                                            infiniteLoop: false
                                        });
                                    })
                                    $('a[href="#near-rests-rest-group"]').click();
                                })
                            </script>
                        <?endif?>
                        <div class="tab-content">
                            <?if(($count["CONTACTS"]==1&&$count["LAT"]&&$count["LON"])||$similar_rest):?>
                            <div class="tab-pane active sm" id="near-rests">
                                <?
                                global $arrFilter;
                                $arrFilter = array();
                                if($count["CONTACTS"]==1&&$count["LAT"]&&$count["LON"]):
                                    $_REQUEST["pageRestSort"] = "distance";
                                    $_REQUEST["by"] = "ASC";
                                    $_REQUEST["set_filter"] = "Y";
                                    $_REQUEST['lat'] = $count["LAT"];
                                    $_REQUEST['lon'] = $count["LON"];

                                    $arrFilter["!ID"] = $element_id;
                                    $arrFilter["!PROPERTY_sleeping_rest_VALUE"] = "Да";
                                    $arrFilter["PROPERTY_REST_NETWORK"] = false;    //  сеть ресторанов
                                else:
                                    $_REQUEST["pageRestSort"] = "rand";
                                    $_REQUEST["by"] = "DESC";

                                    $arrFilter = Array("ID"=>$similar_rest);
                                endif;
                                //FirePHP::getInstance()->info($arrFilter,'$arrFilter');
                                //FirePHP::getInstance()->info($_REQUEST,'$_REQUEST');
                                $APPLICATION->IncludeComponent("restoran:restoraunts.list", "simular_in_detail_bottom_slider", Array(
                                    "main" => "Y",
                                    "NO_SLEEP_TO_NEAR" => "Y",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID),
                                    "PARENT_SECTION_CODE" => "restaurants", // Код раздела
                                    "NEWS_COUNT" => 20,
                                    "SORT_BY1" => ($_REQUEST["pageRestSort"] ? $_REQUEST["pageRestSort"] : "NAME"),
                                    "SORT_ORDER1" => "ASC",
                                    "SORT_BY2" => 'SORT',
                                    "SORT_ORDER2" => 'ASC',
                                    "FILTER_NAME" => "arrFilter",
                                    "PROPERTY_CODE" => array(
                                        1 => "kitchen",
                                        2 => "average_bill",
                                        4 => "phone",
                                        5 => "address",
                                        7 => "RATIO",
                                    ),
                                    "CHECK_DATES" => "N", // Показывать только активные на данный момент элементы
                                    "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                    "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
                                    "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
                                    "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
                                    "AJAX_MODE" => "N", // Включить режим AJAX
                                    "AJAX_OPTION_SHADOW" => "Y",
                                    "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
                                    "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
                                    "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
                                    "CACHE_TYPE" => "Y",//$USER->IsAdmin()?"N":
                                    "CACHE_TIME" => "36000079", // Время кеширования (сек.)
                                    "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
                                    "CACHE_GROUPS" => "N", // Учитывать права доступа
                                    "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
                                    "SET_TITLE" => "N", // Устанавливать заголовок страницы
                                    "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
                                    "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
                                    "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
                                    "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
                                    "PAGER_TITLE" => "Рестораны", // Название категорий
                                    "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
                                    "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
                                    "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
                                    "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
                                    "DISPLAY_DATE" => "Y", // Выводить дату элемента
                                    "DISPLAY_NAME" => "Y", // Выводить название элемента
                                    "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
                                    "DISPLAY_PREVIEW_TEXT" => "N", // Выводить текст анонса
                                    "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                                    "REST_PROPS"=>"Y",
                                    'SHOW_REST_COUNT' => 'Y',
                                    'DISTANCE_VALUE'=>1000,
                                    'HAS_SIMILAR_ARR'=>!$similar_rest&&!$RGROUP["ID"]?'N':'Y'
                                    ), false
                                );?>
                            </div>
                            <?endif?>
                            <div class="tab-pane sm" id="near-rests-rest-group"></div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            <?endif?>

        <?endif//404?>
        </div>
        <?//$APPLICATION->ShowViewContent("ratingValue");?>
    </div>
<?else:?>
    <?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/main_2014/components/restoran/restoraunts.detail/_.default/old_detail_page.php");?>
<?endif;?>
    <br /><br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>