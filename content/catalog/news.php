<?
if($_REQUEST['AJAX_REQUEST']!='Y'):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
endif;
$APPLICATION->SetTitle("Новости");
// get IB params from iblock code
if (!$_REQUEST["SECTION_CODE"])
    $_REQUEST["SECTION_CODE"] = "news";


$arIB = getArIblock($_REQUEST["SECTION_CODE"], $_REQUEST["CITY_ID"]);

$rest = RestIBlock::GetRestIDByCode($_REQUEST["RESTOURANT"]);
FirePHP::getInstance()->info($rest,'$rest');
global $arrFilterTop4;
$arrFilterTop4 = Array("PROPERTY_RESTORAN"=>$rest);
$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
if ($ttt)
{
    $type = $ttt;
}
else
{
    $type = str_replace($_REQUEST["CITY_ID"], "", $_REQUEST["SECTION_CODE"]);

    if ($type=="restoransnews")
        $type = "news";
    elseif ($type=="restoratoram")
        $type = "firms_news";
    elseif($type=="restvew"||$type=="obzor_")
        $type = "overviews";
    elseif($type=="pryamayarech"||$type=="intervew")
        $type = "interview";
    elseif($type=="photos")
        $type = "photoreports";
    elseif($type=="videonovosti")
        $type = "videonews";
    elseif($type=="moskvanatarelke"||$type=="onplate")
        $type = "on_plate";
    elseif($type=="mcfromchif")
    {
        $APPLICATION->SetTitle("Рецепты от шефа");
        $type = "cookery";
    }    
    else
    {
        $type = "news";
    }
}
unset($_REQUEST["SECTION_CODE"]);
?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
<div class="block">
    <div class="left-side">
<?endif?>
            <?$APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "news",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => $type,
                    "IBLOCK_ID" => $arIB["ID"],
                    "NEWS_COUNT" => ($_REQUEST["pageCnt"] ? $_REQUEST["pageCnt"] : "21"),
                    "SORT_BY1" => ($_REQUEST["pageSort"] ? $_REQUEST["pageSort"] : "ACTIVE_FROM"),
                    "SORT_ORDER1" => ($_REQUEST["by"] ? $_REQUEST["by"] : "desc"),
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFilterTop4",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RESTORAN"),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                    "SET_TITLE" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3601",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "REST_ID" => $rest
                ),
            false
            );        
        ?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>

    </div>
    <div class="right-side">
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_2_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>                               
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_1_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>   
        <? if (SITE_ID != "s2"&&CITY_ID!="ufa"&&CITY_ID!="nsk"): ?>
        <?
               
                if (CITY_ID=="tmn")
                    $anons = Array("overviews", "photoreports", "cookery", "news","interview");
                elseif (CITY_ID=="kld")
                    $anons = Array("news_overviews","blogs", "cookery", "news","news_photo");
                elseif (CITY_ID=="nsk")
                    $anons = Array("news_restvew","blogs", "cookery", "news","news_photo");
                elseif (CITY_ID=="ufa")
                    $anons = Array();
                elseif (CITY_ID=="rga" || CITY_ID=="urm")
                    $anons = Array("cookery","news","blogs");
                else
                    $anons = Array("overviews", "photoreports", "cookery", "news", "blogs");
                $iblock_type = Array($arResult["ITEMS"][0]["IBLOCK_TYPE_ID"]);
                $diff = array_diff($anons, $iblock_type);
                $p = 0;
                foreach ($diff as $d) {
                    if ($p < 4)
                        $dif[] = $d;
                    $p++;
                }
                foreach ($dif as $as) {
                    if ($as == "news_overviews"||$as == "news_photo"||$as == "news_restvew")
                        $as = "news";
                    $arAnons[] = getArIblock($as, CITY_ID);
                }

                $MESS1 = array();
                $MESS1["ANONS_overviews"] = "Обзоры";
                $MESS1["ANONS_news_overviews"] = "Обзоры";
                $MESS1["ANONS_news_photo"] = "Фотоотчеты";
                $MESS1["ANONS_photoreports"] = "Фотоотчеты";
                $MESS1["ANONS_cookery"] = "Рецепты от шефа";
                $MESS1["ANONS_news"] = "Новости";
                $MESS1["ANONS_interview"] = "Интервью";
                $MESS1["ANONS_blogs"] = "Критика";
                ?>                        
                                    <? foreach ($arAnons as $key => $anons): ?>                                                                    
                                        <?if ($dif[$key]=="blogs"&&CITY_ID=="kld"):?>
                                            <div class="title" align="center">Блоги</div>  
                                        <?else:?>
                                            <div class="title" align="center"><?= $MESS1["ANONS_" . $dif[$key]]?></div>  
                                        <?endif;?>
                                        <?
                                        $arrAddFilter = array();
                                        if ($dif[$key] == "blogs") {
                                            global $arrAddFilter;
                                            if (CITY_ID == "spb")
                                                $arrAddFilter = Array("CREATED_BY" => 65);
                                            elseif (CITY_ID == "msk")
                                                $arrAddFilter = Array("CREATED_BY" => 93);
                                            else
                                            {
                                                
                                            }
                                        }
                                        if ($dif[$key] == "news_overviews") {
                                            global $arrAddFilter;                                            
                                            $arrAddFilter = Array("SECTION_ID" => 215534);
                                            $dif[$key] = "news";                                            
                                        }
                                        if ($dif[$key] == "news_photo") {
                                            global $arrAddFilter;                                            
                                            $arrAddFilter = Array("SECTION_ID" => 215535);
                                            $dif[$key] = "news";
                                        }
                                        ?>
                                        <?
                                        $APPLICATION->IncludeComponent(
                                                "restoran:catalog.list", "interview_one_with_border", Array(
                                            "DISPLAY_DATE" => "N",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "N",
                                            "AJAX_MODE" => "N",
                                            "IBLOCK_TYPE" => $dif[$key],
                                            "IBLOCK_ID" => ($dif[$key] == "cookery") ? 145 : $anons["ID"],
                                            "NEWS_COUNT" => 1,
                                            "SORT_BY1" => "ID",
                                            "SORT_ORDER1" => "DESC",
                                            "SORT_BY2" => "",
                                            "SORT_ORDER2" => "",
                                            "FILTER_NAME" => "arrAddFilter",
                                            "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
                                            "PROPERTY_CODE" => array("ratio", "COMMENTS"),
                                            "CHECK_DATES" => "Y",
                                            "DETAIL_URL" => "",
                                            "PREVIEW_TRUNCATE_LEN" => "120",
                                            "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                            "SET_TITLE" => "N",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_FILTER" => "Y",
                                            "CACHE_GROUPS" => "N",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "Новости",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_TEMPLATE" => "search_rest_list",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N"
                                                ), false
                                        );
                                        ?>                                    
                                <? endforeach; ?>                                                
                        <? endif; ?>
            <div class="tags">
            <?          
            if (LANGUAGE_ID == 'ru') {
                $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                        "SORT" => "CNT",
                        "PAGE_ELEMENTS" => "20",
                        "PERIOD" => "",
                        "URL_SEARCH" => "/search/index.php",
                        "TAGS_INHERIT" => "Y",
                        "CHECK_DATES" => "Y",
                        "FILTER_NAME" => "",
                        "arrFILTER" => array(
                                0 => "iblock_".$type,
                        ),
                        "arrFILTER_iblock_".$type => array(
                                0 => $arIB["ID"],
                        ),
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "86400",
                        "FONT_MAX" => "24",
                        "FONT_MIN" => "12",
                        "COLOR_NEW" => "30c5f0",
                        "COLOR_OLD" => "24A6CF",
                        "PERIOD_NEW_TAGS" => "",
                        "SHOW_CHAIN" => "Y",
                        "COLOR_TYPE" => "N",
                        "WIDTH" => "100%"
                        ),
                        $component
                );
            }
            ?> 
        </div> 
        <?
        $APPLICATION->IncludeComponent(
                "bitrix:advertising.banner", "", Array(
            "TYPE" => "right_3_main_page",
            "NOINDEX" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0"
                ), false
        );
        ?>
    </div>
    <div class="clearfix"></div>

    <?
    $APPLICATION->IncludeComponent(
            "bitrix:advertising.banner", "", Array(
        "TYPE" => "bottom_rest_list",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
            ), false
    );
    ?>  
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?endif?>