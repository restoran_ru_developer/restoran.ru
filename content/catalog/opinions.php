<?
if($_REQUEST['AJAX_REQUEST']!='Y'):
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Отзывы");
else:
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
endif?>
<?
$arOverviewsIB = getArIblock("reviews", $_REQUEST["CITY_ID"]);


if ($_REQUEST["CODE"]):
    $id = RestIBlock::GetRestIDByCode($_REQUEST["CODE"]);
    if (!$id)
        $id = 1;

endif;

?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
<div class="block">
	<div class="left-side">
            <h1>
                <?if (!$_REQUEST["tid"]):?>
                    <?=$APPLICATION->ShowTitle(false)?>
                <?else:?>
                    <?=$APPLICATION->ShowTitle(false)?>
                <?endif;?>
            </h1>    
            <?if (!$_REQUEST["tid"]):?>                    
                <div class="sort">                        
                        <?                        
                        $excUrlParams = array("video", "photos","?PAGEN_1");
                        $with_photo = $APPLICATION->GetCurPageParam("photos=Y", $excUrlParams);
                        $with_video = $APPLICATION->GetCurPageParam("video=Y", $excUrlParams);       
                        ?>
                        <?if ($_REQUEST["photos"]!="Y"):?>
                            <a class="" href="<?=$with_photo?>"> с фото</a>
                        <?else:?>
                            <a> с фото</a>    
                        <?endif;?>                            
                        <?if ($_REQUEST["video"]!="Y"):?>
                            <a class="" href="<?=$with_video?>"> с видео</a> 
                        <?else:?>
                            <a> с видео</a>                    
                        <?endif;?>   
                        <?if ((!CSite::InGroup(Array(9))||CSite::InGroup(Array(1))))://&&$id!=2022840?>
                            <a tabindex="-1" data-toggle="toggle" data-target="comment_form" class="btn btn-info btn-nb-empty">Добавить отзыв</a>
                        <?endif;?>
                </div>            
            <?endif;?>
            <div class="clearfix"></div>
<?endif?>
		<?
        if (!$_REQUEST["tid"]):




            $arReviewsIB = getArIblock("reviews", $_REQUEST["CITY_ID"]);

            if($_REQUEST['AJAX_REQUEST']!='Y')://&&$id!=2022840
                $APPLICATION->IncludeComponent(
                "restoran:comments_add_new",
                "with_rating_new_on_page",
                Array(
                    "IBLOCK_TYPE" => "reviews",
                    "IBLOCK_ID" => $arReviewsIB["ID"],
                    "ELEMENT_ID" => $id,
                    "IS_SECTION" => "N",
                    'ELEMENT_ID_BY_CODE' => $id
                ), false
            );
            endif;

            if($_REQUEST['REST_NETWORK']){
                $REST_NETWORK = explode('|',$_REQUEST['REST_NETWORK']);
                if($REST_NETWORK[0]){

                    global $arrFilter;
                    $arrFilter = array(
                        array(
                            "LOGIC" => "OR",
                        ),
                    );

                    $arrFilter[0][0] = array("PROPERTY_ELEMENT"=>$_REQUEST["id"]);
                    foreach ($REST_NETWORK as $rest_network) {
                        $arrFilter[0][] = array("PROPERTY_ELEMENT"=>$rest_network);
                    }
                }
                else {
                    if ($id) {
                        global $arrFilter;
                        $arrFilter = array();
                        $arrFilter["PROPERTY_ELEMENT"] = $id;
                    }
                }
            }
            else {
                if ($id) {
                    global $arrFilter;
                    $arrFilter = array();
                    $arrFilter["PROPERTY_ELEMENT"] = $id;
                }
            }



            if ($_REQUEST["photos"] == "Y") {
                global $arrFilter;
                $arrFilter["!PROPERTY_photos"] = false;
            }
            if ($_REQUEST["video"] == "Y") {
                global $arrFilter;
                $arrFilter[0] = Array("LOGIC" => "OR",
                    Array("!PROPERTY_video" => false),
                    Array("!PROPERTY_video_youtube" => false)
                );
            }
            if (!$id && ($_REQUEST["CITY_ID"] == "msk" || $_REQUEST["CITY_ID"] == "spb")) {
                $arCatalogIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
                $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arCatalogIB["ID"], "PROPERTY_without_reviews_VALUE" => "Да"));
                while ($ar = $res->Fetch())
                    $wr[] = $ar["ID"];
                //$wrr = array_merge($wr,array(1305186,387793));
                $arrFilter["!PROPERTY_ELEMENT"] = $wr;
            }


            if($_REQUEST['AJAX_REQUEST']!='Y'):?>
            <script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
            <link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css"/>
            <?endif;?>
            <script>
                $(function() {
                    $(".fancy").fancybox({
                        'transitionIn': 'elastic',
                        'transitionOut': 'elastic',
                        'cyclic': true,
                        'overlayColor': '#1a1a1a',
                        'speedIn': 200,
                        'speedOut': 200
                    });
                });
            </script>
            <?

            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "opinions",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "reviews",
                    "IBLOCK_ID" => $arOverviewsIB["ID"],
                    "NEWS_COUNT" => ($_REQUEST["pageCnt"]) ? $_REQUEST["pageCnt"] : "20",
                    "SORT_BY1" => "created_date",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PAGE_URL",'SHOW_COUNTER'),
                    "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "video_youtube", "COMMENTS",'FAMOUS_REVIEW','FAMOUS_PIC'),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",//a
                    "CACHE_TIME" => "3600007",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "NO_CACHE_TEMPLATE" => "Y",
                    'AJAX_REQUEST' => $_REQUEST['AJAX_REQUEST'],
                ),
                false
            );
        else:?>
            <div class="detail-review">
            <?
            global $arrFilter;
            $arrFilter = array();
            $old_id = RestIBlock::getOldOpinionID((int)$_REQUEST["tid"]);
            if ($old_id) {
                LocalRedirect($APPLICATION->GetCurPageParam("tid=" . $old_id, array("tid", "CITY_ID")), true, "301 Moved permanently");
                $arrFilter["ID"] = $old_id;
            } else
                $arrFilter["ID"] = $_REQUEST["tid"];
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "opinion",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "reviews",
                    "IBLOCK_ID" => $arOverviewsIB["ID"],
                    "NEWS_COUNT" => "1",
                    "SORT_BY1" => "created_date",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY"),
                    "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "video_youtube", "COMMENTS",'FAMOUS_REVIEW'),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => $USER->IsAdmin()?'N':"A",//a
                    "CACHE_TIME" => "360000008",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
            );?>
            </div>
            <? if (ERROR_404 != "Y"): ?>
                <div id="comments">
                    <?
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/header.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/util.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/button.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.base.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.form.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/handler.xhr.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.basic.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/dnd.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/uploader.js');
                    $APPLICATION->AddHeadScript('/tpl/js/fu/js/jquery-plugin.js');
                    ?>
                    <link rel="stylesheet" href="/tpl/js/quaptcha/QapTcha.jquery.css" type="text/css"/>
                    <script type="text/javascript" src="/tpl/js/quaptcha/jquery-ui.js"></script>
                    <script type="text/javascript" src="/tpl/js/quaptcha/jquery.ui.touch.js"></script>
                    <script type="text/javascript" src="/tpl/js/quaptcha/QapTcha.jquery.js"></script>
                    <script type="text/javascript" src="/tpl/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
                    <link rel="stylesheet" href="/tpl/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css"/>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('.QapTcha').QapTcha({
                                txtLock: 'Сдвиньте слайдер вправо.',
                                txtUnlock: 'Готово. Теперь можно отправлять',
                                disabledSubmit: true,
                                autoRevert: true,
                                PHPfile: '/tpl/js/quaptcha/Qaptcha.jquery.php',
                                autoSubmit: false
                            });


                            $(".fancy").fancybox({
                                'transitionIn': 'elastic',
                                'transitionOut': 'elastic',
                                'cyclic': true,
                                'overlayColor': '#1a1a1a',
                                'speedIn': 200,
                                'speedOut': 200,
                                //'overlayShow'	:	false
                            });
                        });
                    </script>
                    <?
                    $APPLICATION->IncludeComponent(
                        "restoran:comments",
                        "review_comment_new",
                        Array(
                            "IBLOCK_TYPE" => "comment",
                            "ELEMENT_ID" => $arrFilter["ID"],
                            "IBLOCK_ID" => 2438,
                            "IS_SECTION" => "N",
                            "ADD_COMMENT_TEMPLATE" => "",
                            "COUNT" => 999,
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000004",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "PROPERTY_CODE" => Array("photos", "video")
                        ),
                        false
                    );
                    //endif;
                    ?>
                    <?
                    echo '<div  id="comments">';?>

                    <div class="h1_style">Ваш комментарий</div>

                    <?$APPLICATION->IncludeComponent(
                        "restoran:comments_add_new",
                        "new",
                        Array(
                            "IBLOCK_TYPE" => "comment",
                            "IBLOCK_ID" => (SITE_ID == "s1") ? "2438" : "2641",
                            "ELEMENT_ID" => $arrFilter["ID"],
                            "IS_SECTION" => "N",
                            "OPEN" => "Y"
                        ), false
                    );
                    echo "</div><br />";?>
                </div>
            <? endif; ?>
        <?endif; ?>
<?if($_REQUEST['AJAX_REQUEST']!='Y'):?>
	</div>

	<div class="right-side">
<!--        --><?//if(stristr($APPLICATION->GetCurDir(),'/spb/opinions/')):?>
<!--            --><?//$APPLICATION->IncludeComponent(
//                "bitrix:main.include",
//                "",
//                Array(
//                    "AREA_FILE_SHOW" => "file",
//                    "PATH" => SITE_TEMPLATE_PATH."/include_areas/new-order-form-with-city.php",
//                    "EDIT_TEMPLATE" => ""
//                ),
//                false
//            );?>
<!--        --><?//endif?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>                
            <div class="title">Популярные</div>
            <?
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "5",
                        "SORT_BY1" => "PROPERTY_rating_date",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "PROPERTY_stat_day",
                        "SORT_ORDER2" => "DESC",
                        "SORT_BY3" => "NAME",
                        "SORT_ORDER3" => "ASC",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                    "REST_PROPS"=>"Y"
                ),
            false
            );?>                
            <div class="title">Рекомендуем</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);           
            //$arrFilterSim["PROPERTY_RECOMENDED_VALUE"] = "Да";
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arPFilter["!PREVIEW_PICTURE"] = false;
            $arPFilter["!PROPERTY_restoran_ratio"] = false;  
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => $arRestIB["ID"],
                        "NEWS_COUNT" => "3",
                        "SORT_BY1" => "PROPERTY_restoran_ratio",
                        "SORT_ORDER1" => "asc,nulls",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "arPFilter",
                        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "10",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "A" => "recomended",
                    "REST_PROPS"=>"Y"
                ),
            false
            );?>                
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_1_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>                
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_3_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>


	</div>
	<div class="clearfix"></div>
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "bottom_rest_list",
                        "NOINDEX" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
        false
        );?>


    <div class="preview_seo_text">
        <?$APPLICATION->ShowViewContent("preview_seo_text");?>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?endif;?>