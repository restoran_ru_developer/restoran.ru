<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(CITY_ID=='spb'?'список улиц СПб':"список улиц Москвы");
$arIB = getArIblock("streets",CITY_ID);
if(!$arIB){
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");?>
    <script>location.href = 'http://www.restoran.ru/404.php';</script>
<?}

?>
<!--    Задача Константину:
1. на страницах Москвы в подвале необходимо поправить ссылку: http://www.restoran.ru/spb/catalog/restaurants/street/
заменить на http://www.restoran.ru/msk/catalog/restaurants/street/

Задача Татьяне Усмановой и, возможно, Константину :

2. Все выборки ресторанов по улицам необходимо переименовать с учетом склонений наименований улиц.
http://www.restoran.ru/msk/catalog/restaurants/street/ (список улиц Москвы)  http://spb.restoran.ru/spb/catalog/restaurants/street/ (список улиц СПб)
Например:  страница http://spb.restoran.ru/spb/catalog/restaurants/street/zagrebskiy-bulvar/
Должна иметь:
title: Рестораны на Загребском бульваре - Рестораны Санкт-Петербурга.
keywords: рестораны, загребский бульвар, санкт-петербург
description: Каталог ресторанов на Загребском бульваре, Санкт-Петербург.
H1: Рестораны на Загребском бульваре


Второй пример:   страница http://spb.restoran.ru/spb/catalog/restaurants/street/pochtamtskiy-pereulok/ Должна иметь:
title: Рестораны в Почтамтском переулке - Рестораны Санкт-Петербурга.
keywords: рестораны, почтамтский переулок, санкт-петербург  description: Каталог ресторанов в Почтамтском переулке, Санкт-Петербург.
H1: Рестораны в Почтамтском переулке-->
<div class="block">
    <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
    <div class="clearfix"></div>
<!--    <div class="left-side">-->
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "city-link-list",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "streets",
                "IBLOCK_ID" => $arIB['ID'],
                "NEWS_COUNT" => "10000",
                "SORT_BY1" => "NAME",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array(),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => $_REQUEST['CATALOG_ID'],
                "INCLUDE_SUBSECTIONS" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000004",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Улицы",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
        );?>
<!--    </div>-->
<!--    <div class="right-side">-->
<!---->
<!--    </div>-->
    <div class="clearfix"></div>
    <noindex>
        <div id="yandex_direct">
            <script type="text/javascript"> 
                //<![CDATA[
                yandex_partner_id = 47434;
                yandex_site_bg_color = 'FFFFFF';
                yandex_site_charset = 'utf-8';
                yandex_ad_format = 'direct';
                yandex_font_size = 1;
                yandex_direct_type = 'horizontal';
                yandex_direct_limit = 4;
                yandex_direct_title_color = '24A6CF';
                yandex_direct_url_color = '24A6CF';
                yandex_direct_all_color = '24A6CF';
                yandex_direct_text_color = '000000';
                yandex_direct_hover_color = '1A1A1A';
                document.write('<sc'+'ript type="text/javascript" src="https://an.yandex.ru/resource/context.js?rnd=' + Math.round(Math.random() * 100000) + '"></sc'+'ript>');
                //]]>
            </script>
        </div>        
    </noindex>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>