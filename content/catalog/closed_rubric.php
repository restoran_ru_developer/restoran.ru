<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");?>
<div id="catalog">
    <h1><?=$APPLICATION->ShowTitle("h1")?></h1>
    <?if (!$_REQUEST["PAGEN_1"]||$_REQUEST["PAGEN_1"]==1):?>
        <div class="preview_seo_text">
            <?$APPLICATION->ShowViewContent("preview_seo_text");?>
        </div>
    <?endif;?>
    <br />
    <div class="sort">
        <?
        $arIB = getArIblock("closed_rubrics", CITY_ID);
        if($arIB['ID']&&$_REQUEST["RUBRIC"]):
            if(CModule::IncludeModule("iblock")) {
                $obCache = new CPHPCache;
                $life_time = 60*60*24;
                $life_time = 0;
                global $arRestLinks;
                $cache_id = date("m.d.y")."closed_rubric";
                if($obCache->InitCache($life_time, $cache_id, "/")) {
                    $vars = $obCache->GetVars();
                    $arRestLinks = $vars["REST"];
                } else {    
                    $rsLinks = CIBlockElement::GetList(
                        Array("SORT"=>"ASC"),
                        Array(
                            "ACTIVE" => "Y",
                            "CODE" => $_REQUEST["RUBRIC"],
                            "IBLOCK_ID"=>$arIB['ID']
                        ),
                        false,
                        false,
                        Array("ID", "NAME")
                    );
                    if($arLinks = $rsLinks->GetNext()) {
                        $db_props = CIBlockElement::GetProperty($arIB['ID'], $arLinks["ID"], array("sort" => "asc"), Array("CODE"=>"RESTORAN"));
                        while($ar_props = $db_props->Fetch())
                        {
                            $arRestLinks["ID"][] = $ar_props["VALUE"];
                        }
                    }
                    if($obCache->StartDataCache()) {
                        $obCache->EndDataCache(array(
                            "REST" => $arRestLinks
                        ));
                    }
                }                
            }
            //v_dump($arRestLinks);
        endif;
        if (CITY_ID!="spb"&&CITY_ID!="msk"&&!$_REQUEST["page"])
        {
            $_REQUEST["page"] = 1;
            $_REQUEST["PAGEN_1"] = 1;
        }
        ?>               
    </div>
    <?$APPLICATION->IncludeComponent("restoran:restoraunts.list", "rest_list", Array(
            "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => $_REQUEST["CITY_ID"],	// Код информационного блока
            "PARENT_SECTION_CODE" => false,	// Код раздела
            "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "20"),	// Количество ресторанов на странице
            "SORT_BY1" => "NAME",	// Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки ресторанов
            "SORT_BY2" => "SORT",	// Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arRestLinks",	// Фильтр
            "PROPERTY_CODE" => array(	// Свойства
                    0 => "type",
                    1 => "kitchen",
                    2 => "average_bill",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "address",
                    6 => "subway",
                    7 => "ratio",
            ),
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "Y",	// Тип кеширования
            "CACHE_TIME" => "21600",	// Время кеширования (сек.)
            "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150",	// Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "PAGER_TITLE" => "Рестораны",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows",	// Название шаблона
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            ),
            false
    );?>    
</div>
<div id="baner_inv_block">
    <div id="baner_right_2_main_page">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
    <div id="baner_right_1_main_page">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>