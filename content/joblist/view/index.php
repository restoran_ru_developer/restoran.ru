<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
// get IB params from iblock code
$arIB = getArIblock($_REQUEST["IBLOCK_TYPE_ID"], CITY_ID);
global $arrFilter;
foreach ($_REQUEST["arrFilter_pf"] as $key=>$prop)
{
    $arrFilter['PROPERTY_'.$key] = $prop;
}
$zp = implode("_",$_REQUEST["arrFilter_pf"]["WAGES_OF"]);
if ($arrFilter["PROPERTY_WAGES_OF"]&&$_REQUEST["IBLOCK_TYPE_ID"]=="vacancy")
{
        $temp = array();
        $temp = explode("_",$zp);
        sort($temp);        
        $arrFilter["<=PROPERTY_WAGES_TO"] = $temp[(count($temp)-1)];
        $arrFilter[">=PROPERTY_WAGES_TO"] = $temp[0];
        //$arrFilter[">=PROPERTY_WAGES_OF"] = $temp[0];
        //$arrFilter["<=PROPERTY_WAGES_OF"] = $temp[(count($temp)-1)];
        $arrFilter["!PROPERTY_WAGES_OF"] = false;
        unset($arrFilter["PROPERTY_WAGES_OF"]);           
}
$zp = implode("_",$_REQUEST["arrFilter_pf"]["SUGG_WORK_ZP_FROM"]);
if ($arrFilter["PROPERTY_SUGG_WORK_ZP_FROM"]&&$_REQUEST["IBLOCK_TYPE_ID"]=="resume")
{
        $temp = array();
        $temp = explode("_",$zp);
        sort($temp);        
        $arrFilter["<=PROPERTY_SUGG_WORK_ZP_TO"] = $temp[(count($temp)-1)];
        $arrFilter[">=PROPERTY_SUGG_WORK_ZP_TO"] = $temp[0];
        //$arrFilter[">=PROPERTY_SUGG_WORK_ZP_FROM"] = $temp[0];
        //$arrFilter["<=PROPERTY_SUGG_WORK_ZP_FROM"] = $temp[(count($temp)-1)];
        $arrFilter["!PROPERTY_SUGG_WORK_ZP_FROM"] = false;        
        unset($arrFilter["PROPERTY_SUGG_WORK_ZP_FROM"]);   
}
?>
<div id="content">
    <div class="left">
        <div class="content-wrapper">
            <div class="content-content">
                <?if($_REQUEST["IBLOCK_TYPE_ID"] == "vacancy"):?>
                    <?$APPLICATION->SetTitle("Список вакансий");?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "vacancy_list",
                        Array(
                            "BLOCK_HEADER_POPULAR" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "vacancy",
                            "IBLOCK_ID" => $arIB["ID"],
                            "NEWS_COUNT" => "18",
                            "SORT_BY1" => "ID",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFilter",
                            "FIELD_CODE" => array("CREATED_BY"),
                            "PROPERTY_CODE" => array("WAGES_OF", "WAGES_TO", "EXPERIENCE", "SUBWAY_BIND","POSITION", "SHEDULE"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Вакансии",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "search_rest_list",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ),
                        false
                    );?>
                <?elseif($_REQUEST["IBLOCK_TYPE_ID"] == "resume"):?>
                    <?$APPLICATION->SetTitle("Список резюме");?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "resume_list",
                        Array(
                            "BLOCK_HEADER_POPULAR" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_MODE" => "N",
                            "IBLOCK_TYPE" => "resume",
                            "IBLOCK_ID" => $arIB["ID"],
                            "NEWS_COUNT" => "18",
                            "SORT_BY1" => "ID",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arrFilter",
                            "FIELD_CODE" => array("CREATED_BY"),
                            "PROPERTY_CODE" => array("SUGG_WORK_ZP_FROM", "SUGG_WORK_ZP_TO", "EXPERIENCE", "SUGG_WORK_GRAFIK", "SUGG_WORK_SUBWAY","POSITION"),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Резюме",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "search_rest_list",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N"
                        ),
                        false
                    );?>
                <?endif?>
            </div>
        </div>
    </div>
    <div class="right" style="width:240px;">
<!--        <div id="search_article">
            <?$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.form",
                    "main_page_subscribe",
                    Array(
                            "USE_PERSONALIZATION" => "Y",
                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                            "SHOW_HIDDEN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => ""
                    ),
            false
            );?>           
        </div>-->
        <div align="center">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
        <br /><br />
        <div class="top_block">Популярные</div>
        <?
        global $arrVacFilter;
        //$arrVacFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            //$arrVacFilter["!PREVIEW_PICTURE"] = false;
          //  $arrVacFilter["!PROPERTY_restoran_ratio"] = false;  
        ?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            $_REQUEST["IBLOCK_TYPE_ID"]."_right",
            Array(
                "BLOCK_HEADER_POPULAR" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => $_REQUEST["IBLOCK_TYPE_ID"],
                "IBLOCK_ID" => $arIB["ID"],
                "NEWS_COUNT" => "3",
                "SORT_BY1" => "shows",
                "SORT_ORDER1" => "desc",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arrVacFilter",
                "FIELD_CODE" => array("CREATED_BY"),
                "PROPERTY_CODE" => array("WAGES_OF", "WAGES_TO", "EXPERIENCE", "SUBWAY_BIND","SHEDULE","POSITION","SUGG_WORK_ZP_FROM", "SUGG_WORK_ZP_TO", "EXPERIENCE", "SUGG_WORK_GRAFIK", "SUGG_WORK_SUBWAY"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "10",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Вакансии",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
        );?>
        <br /><br />
	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
</div>
<div class="clear"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>