<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
$arIB = getArIblock($_REQUEST["IBLOCK_TYPE_ID"], CITY_ID);
//v_dump($_REQUEST);
?>

<div id="content">
    <div class="left">
        <?if($_REQUEST["IBLOCK_TYPE_ID"] == "vacancy"):
            ?>
            <?$APPLICATION->IncludeComponent("bitrix:news.detail", "vacancy_detail", Array(
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "N",	// Выводить детальное изображение
                "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                "USE_SHARE" => "N",	// Отображать панель соц. закладок
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "IBLOCK_TYPE" => "vacancy",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => $arIB["ID"],	// Код информационного блока
                "ELEMENT_ID" => $_REQUEST["ID"],	// ID новости
                "ELEMENT_CODE" => $_REQUEST["CODE"],	// Код новости
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "FIELD_CODE" => "",	// Поля
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "AGE_FROM",
                    1 => "AGE_TO",
                    2 => "EDUCATION",
                    3 => "EXPERIENCE",
                    4 => "WAGES_OF",
                    5 => "WAGES_TO",
                    6 => "GENDER",
                    7 => "SHEDULE",
                    8 => "RESPONSIBILITY",
                    9 => "ADD_INFO",
                    10 => "COMPANY_FIO",
                    11 => "CONTACT_PHONE",
                    12 => "USER_CONTACT_BIND",
                    13 => "EMAIL",
                    14 => "RES_USERS_UNR_EMAIL",
                    15 => "RESPOND_USERS_REG",
                    16 => "RES_USERS_UNR_FIO",
                    17 => "COMMENTS"
                ),
                "IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                "ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
                "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
                "CACHE_TYPE" => "A",//a	// Тип кеширования
                "CACHE_TIME" => "36000002",	// Время кеширования (сек.)
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                "PAGER_TITLE" => "Страница",	// Название категорий
                "PAGER_TEMPLATE" => "",	// Название шаблона
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                ),
                false
            );?>
        <?elseif($_REQUEST["IBLOCK_TYPE_ID"] == "resume"):?>
        <?$APPLICATION->IncludeComponent("bitrix:news.detail", "resume_detail", Array(
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "N",	// Выводить детальное изображение
                "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                "USE_SHARE" => "N",	// Отображать панель соц. закладок
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "IBLOCK_TYPE" => "resume",	// Тип информационного блока (используется только для проверки)
                "IBLOCK_ID" => $arIB["ID"],	// Код информационного блока
                "ELEMENT_ID" => $_REQUEST["ID"],	// ID новости
                "ELEMENT_CODE" => $_REQUEST["CODE"],	// Код новости
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "FIELD_CODE" => "",	// Поля
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "AGE_FROM",
                    1 => "AGE_TO",
                    2 => "EDUCATION",
                    3 => "EXPERIENCE",
                    4 => "WAGES_OF",
                    5 => "WAGES_TO",
                    6 => "GENDER",
                    7 => "SHEDULE",
                    8 => "RESPONSIBILITY",
                    9 => "ADD_INFO",
                    10 => "COMPANY_FIO",
                    11 => "CONTACT_PHONE",
                    12 => "USER_CONTACT_BIND",
                    13 => "EMAIL",
                    14 => "RES_USERS_UNR_EMAIL",
                    15 => "RESPOND_USERS_REG",
                    16 => "RES_USERS_UNR_FIO",
                    17 => "COMMENTS"
                ),
                "IBLOCK_URL" => "",	// URL страницы просмотра списка элементов (по умолчанию - из настроек инфоблока)
                "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                "ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
                "USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                "PAGER_TITLE" => "Страница",	// Название категорий
                "PAGER_TEMPLATE" => "",	// Название шаблона
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            ),
            false
        );?>
        <?endif?>
    </div>
</div>
<div class="clear"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>