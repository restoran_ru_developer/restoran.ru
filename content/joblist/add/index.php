<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
// get IB params from iblock code
$arIB = getArIblock($_REQUEST["IBLOCK_TYPE_ID"], CITY_ID);
?>

<?$APPLICATION->IncludeComponent("bitrix:iblock.element.add.form", "joblist_add_form", Array(
	"SEF_MODE" => "N",	// Включить поддержку ЧПУ
	"IBLOCK_TYPE" => $_REQUEST["IBLOCK_TYPE_ID"],	// Тип инфо-блока
	"IBLOCK_ID" => $arIB["ID"],	// Инфо-блок
	"PROPERTY_CODES" => array(	// Свойства, выводимые на редактирование
		0 => "109",
		1 => "110",
		2 => "111",
		3 => "112",
		4 => "113",
		5 => "115",
		6 => "116",
		7 => "117",
		8 => "118",
		9 => "119",
		10 => "120",
		11 => "121",
		12 => "122",
		/*13 => "125",*/
		14 => "94",
		15 => "95",
		16 => "96",
		17 => "97",
		18 => "98",
		19 => "101",
		20 => "99",
		21 => "100",
		22 => "102",
		23 => "103",
		24 => "104",
		25 => "105",
		26 => "106",
		27 => "107",
		/*28 => "126",*/
		29 => "NAME",
		30 => "IBLOCK_SECTION",
		31 => "DATE_ACTIVE_TO",
	),
	"PROPERTY_CODES_REQUIRED" => array(	// Свойства, обязательные для заполнения
		0 => ($_REQUEST["IBLOCK_TYPE_ID"] == "vacancy" ? "120" : "105"),
		1 => ($_REQUEST["IBLOCK_TYPE_ID"] == "vacancy" ? "121" : "106"),
		2 => "NAME",
	),
	"GROUPS" => array(	// Группы пользователей, имеющие право на добавление/редактирование
		0 => "5",
	),
	"STATUS_NEW" => "2",	// Статус после сохранения
	"STATUS" => array(	// Редактирование возможно для статуса
		0 => "2",
	),
	"LIST_URL" => "/".CITY_ID."/joblist/view/".$_REQUEST["IBLOCK_TYPE_ID"]."/",	// Страница со списком своих элементов
	"ELEMENT_ASSOC" => "CREATED_BY",	// Привязка к пользователю
	"MAX_USER_ENTRIES" => "100000",	// Ограничить кол-во элементов для одного пользователя
	"MAX_LEVELS" => "100000",	// Ограничить кол-во рубрик, в которые можно добавлять элемент
	"LEVEL_LAST" => "Y",	// Разрешить добавление только на последний уровень рубрикатора
	"USE_CAPTCHA" => "N",	// Использовать CAPTCHA
	"USER_MESSAGE_EDIT" => "",	// Сообщение об успешном сохранении
	"USER_MESSAGE_ADD" => "",	// Сообщение об успешном добавлении
	"DEFAULT_INPUT_SIZE" => "30",	// Размер полей ввода
	"RESIZE_IMAGES" => "N",	// Использовать настройки инфоблока для обработки изображений
	"MAX_FILE_SIZE" => "0",	// Максимальный размер загружаемых файлов, байт (0 - не ограничивать)
	"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",	// Использовать визуальный редактор для редактирования текста анонса
	"DETAIL_TEXT_USE_HTML_EDITOR" => "N",	// Использовать визуальный редактор для редактирования подробного текста
	"CUSTOM_TITLE_NAME" => "Должность",	// * наименование *
	"CUSTOM_TITLE_TAGS" => "",	// * теги *
	"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",	// * дата начала *
	"CUSTOM_TITLE_DATE_ACTIVE_TO" => "Срок хранения",	// * дата завершения *
	"CUSTOM_TITLE_IBLOCK_SECTION" => "",	// * раздел инфоблока *
	"CUSTOM_TITLE_PREVIEW_TEXT" => "",	// * текст анонса *
	"CUSTOM_TITLE_PREVIEW_PICTURE" => "",	// * картинка анонса *
	"CUSTOM_TITLE_DETAIL_TEXT" => "",	// * подробный текст *
	"CUSTOM_TITLE_DETAIL_PICTURE" => "",	// * подробная картинка *
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>