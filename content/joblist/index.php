<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список вакансий и резюме");
// get IB params from iblock code
$arVacancyIB = getArIblock("vacancy", CITY_ID);
$arResumeIB = getArIblock("resume", CITY_ID);

?>
<div class="block">
    <div class="left-side">
        <div class="content-wrapper">
            <div class="content-content">
                <div class="job_baner">
                    <img src="/tpl/images/work_baner_1.jpg" />
                    <a href="/content/joblist/add_resume.php" class="resume"></a>
                    <a href="/content/joblist/add_vacancy.php" class="vacancy"></a>
                </div>
                <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "vacancy_list",
                Array(
                    "BLOCK_HEADER_POPULAR" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "vacancy",
                    "IBLOCK_ID" => $arVacancyIB["ID"],
                    "NEWS_COUNT" => "6",
                    "SORT_BY1" => "ID",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array("CREATED_BY"),
                    "PROPERTY_CODE" => array("WAGES_OF", "WAGES_TO", "EXPERIENCE", "SUBWAY_BIND","COMMENTS","POSITION"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
            );?>

                <hr class="content-separator" />
                <br class="br-content-blocks" />

                <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "resume_list",
                Array(
                    "BLOCK_HEADER_POPULAR" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "resume",
                    "IBLOCK_ID" => $arResumeIB["ID"],
                    "NEWS_COUNT" => "6",
                    "SORT_BY1" => "ID",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array("CREATED_BY"),
                        "PROPERTY_CODE" => array("SUGG_WORK_ZP_FROM", "SUGG_WORK_ZP_TO", "EXPERIENCE", "SUGG_WORK_GRAFIK", "SUGG_WORK_SUBWAY","POSITION"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
            );?>
            </div>
        </div>
    </div>
    <div class="right-side">   
        <?//if (CSite::InGroup(Array(1,14,15,20))):?>
            <a class="add_recipe" href="/content/joblist/add_vacancy.php">Добавить вакансию</a>
            <a class="add_recipe" href="/content/joblist/add_resume.php">Добавить резюме</a>
        <?//endif;?>
        <div align="center">
            <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
        </div>
        <br />
<!--        <div id="search_article">
            <?
//            $APPLICATION->IncludeComponent(
//                    "bitrix:subscribe.form",
//                    "main_page_subscribe",
//                    Array(
//                            "USE_PERSONALIZATION" => "Y",
//                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
//                            "SHOW_HIDDEN" => "N",
//                            "CACHE_TYPE" => "A",
//                            "CACHE_TIME" => "3600",
//                            "CACHE_NOTES" => ""
//                    ),
//            false
//            );
            ?>           
        </div>-->
        <br />
        <?
        global $arrVacFilter;
        //$arrVacFilter = Array("PROPERTY_RECOMEND_VALUE"=>"Да","!PROPERTY_RECOMEND_VALUE"=>false);
        ?>
        <div class="title">Популярные</div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "vacancy_right",
            Array(
                "BLOCK_HEADER_POPULAR" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "vacancy",
                "IBLOCK_ID" => $arVacancyIB["ID"],
                "NEWS_COUNT" => "3",
                "SORT_BY1" => "SHOWS",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arrVacFilter",
                "FIELD_CODE" => array("CREATED_BY"),
                "PROPERTY_CODE" => array("WAGES_OF", "WAGES_TO", "EXPERIENCE", "SUBWAY_BIND"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Вакансии",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "search_rest_list",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
        );?>
        <br /><br />
	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("include_areas/order_rest.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
    </div>
</div>
<div class="clear"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>