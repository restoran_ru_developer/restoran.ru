<html>
 <head>
  <title>#SITE_NAME#: 8 Марта</title>
   <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
 </head>
 <body style="margin:0px; padding:0px; margin:auto; padding:10px; font-family: Georgia; font-size:12px;background:#f2f2f2; ">
  <table cellpadding="0" cellspacing="0" width="100%" style="background:#f2f2f2;  padding:10px 40px; padding-top:0px;">
   <tr>
     <td>
       <table align="center" height="148" cellpadding="0" cellspacing="0"  width="860" style="position:relative;height:148px; overflow:hidden; max-height:148px;">
         <tr>
             <td height="148" style=""><a href="http://www.restoran.ru"><img src="http://www.restoran.ru/tpl/images/mail/8mart/header.jpg" alt="8 Марта с Restoran.ru" /></a></td>
         </tr>
       </table>
       <table align="center" cellpadding="0" cellspacing="10" height="659" width="860" style="position:relative; background-image:url(http://www.restoran.ru/tpl/images/mail/8mart/main.jpg); background-position: left top; background-repeat: no-repeat; height:659px; max-height:659px">
         <tr>
            <td height="601" style="padding:0px 20px; vertical-align:top; width:282px; height:601px;">
                <a href="http://www.restoran.ru" style="text-decoration: none; display: block">
                <p style="font-size:26px; color:#FFF; line-height: 34px; font-family: Georgia;"><i>Поздравляем</i><br /> с 8 марта!</p>
                <Br />
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Желаем яркой весны ярким женщинам и всем, кто их любит!</p>
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>                              
                </a>
            </td>
            <td height="601" style="padding:0px 20px; vertical-align:top; width:282px; height:601px;">
                <a href="http://www.restoran.ru/msk/articles/8marta/" style="text-decoration: none; display: block">
                    <p style="font-size:26px; color:#FFF; line-height: 34px; font-family: Georgia;">Определились,<br /><i>где отмечаете</i><br />8 марта?</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Если еще нет, не паникуйте, мы знаем, в каких ресторанах еще есть места! Поможем определиться в выбором.</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Звоните: +7 (495) 988-26-56</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Желаем яркой весны ярким женщинам и всем, кто их любит!</p>
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>              
                <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>                              
                </a>
            </td>
            <td height="601" style="padding:0px 10px; vertical-align:top; width:282px; height:601px;">
                <a href="http://www.restoran.ru/content/cookery/spec/vesna_na_poroge/" style="text-decoration: none; display: block">
                    <p style="font-size:26px; color:#FFF; line-height: 34px; font-family: Georgia;">Весна идет!<br /><i>Весне дорогу!</i></p>                
                    <Br />
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">Отличные рецепты в spring-коллекции!</p>
                </a>
                <ul style="margin:0px;">
                    <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; color:#FFF "><a href="http://www.restoran.ru/content/cookery/editor/salat_iz_ogurtsa_i_seldereya/" style="color:#0c5831; text-decoration: none">Салат из огурца и сельдерея</a></li>
                    <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; color:#FFF"><a href="http://www.restoran.ru/content/cookery/editor/forel_v_apelsinovom_souse_s_zelenyim_salatom/" style="color:#0c5831; text-decoration: none">Форель в апельсиовом соусе</a></li>
                    <li style="font-size:13px; line-height:24px; padding-left:10px; font-family: Georgia; color:#FFF"><a href="http://www.restoran.ru/content/cookery/editor/rikotta_s_klubnikoy/" style="color:#0c5831; text-decoration: none">Рикотта с клубникой</a></li>
                </ul>     
                <a href="http://www.restoran.ru/content/cookery/spec/vesna_na_poroge/" style="text-decoration: none; display: block">
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>
                    <p style="font-size:16px; color:#FFF; line-height: 22px; font-family: Georgia;">&nbsp;</p>                    
                </a>
            </td>
         </tr>
         <tr>
            <td align="center"><a href="http://restoran.ru" style="font-size:16px; font-family: Georgia; color:#FFF; text-decoration: underline; font-style:italic; line-height:30px;">Restoran.ru</a></td>
            <td align="center"><a href="http://www.restoran.ru/msk/articles/8marta/" style="font-size:16px; font-family: Georgia; color:#FFF; text-decoration: underline; font-style:italic; line-height:30px;">Подобрать ресторан</a></td>
            <td align="center"><a href="http://www.restoran.ru/content/cookery/spec/vesna_na_poroge/" style="font-size:16px; font-family: Georgia; color:#FFF; text-decoration: underline; font-style:italic; line-height:30px;">Весенние рецепты</a></td>
         </tr>
       </table>       
     </td>
   </tr>
   <tr>
       <td colspn="3" height="25"></td>
   </tr>
  </table>    
 </body>
</html>