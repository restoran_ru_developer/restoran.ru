<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?

if(!$USER->IsAdmin())
    return false;

define("ADD_IBLOCK_ID", 119);
define("REST_IBLOCK_ID", 12);

CModule::IncludeModule("iblock");
$el = new CIBlockElement;

global $DB;

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "gamekonezimo";

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// stepping
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

// start execution
$time_start = microtime_float();

mysql_select_db($DBName1, $link1);

$strSql = "
    SELECT
        ru_photos.*
    FROM
        ru_photos
    WHERE
        ru_photos.city_id = 2 AND ru_photos.tid = 0 AND ru_photos.id > {$curStepID}
    ORDER BY
        ru_photos.id DESC
";
$res = mysql_query($strSql, $link1);
while($ar = mysql_fetch_array($res)) {
    //mysql_select_db($DBName1, $link1);
    $arFields = Array();

    $curStepID = $ar["id"];

    mysql_select_db($DB->DBName, $DB->db_Conn);
    $rsRestBind  = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            "IBLOCK_ID" => REST_IBLOCK_ID,
            "PROPERTY_old_item_id" => $ar["item_id"]
        ),
        false,
        false,
        Array()
    );
    if($arRestBind = $rsRestBind->Fetch())
        $arFields["PROPERTY_VALUES"]["RESTORAN"] = $arRestBind["ID"];

    $arFields["IBLOCK_ID"] = ADD_IBLOCK_ID;
    $arFields["ACTIVE"] = ($ar["is_enable"] ? "Y" : "N");
    $arFields["NAME"] = html_entity_decode(htmlspecialchars_decode(stripcslashes($ar["title"])));
    $arFields["CODE"] = $ar["path"];
    $arFields["ACTIVE_FROM"] = $DB->FormatDate($ar["date"], "YYYY-MM-DD HH:MI:SS", "DD.MM.YYYY HH:MI:SS");
    $detText = strip_tags(html_entity_decode(htmlspecialchars_decode(stripcslashes($ar["text"]))));
    $arFields["DETAIL_TEXT"] = '<div class="vis_red bl"><div style="" class="txt">'.$detText.'</div></div>';
    $arFields["DETAIL_TEXT_TYPE"] = "html";

    mysql_select_db($DBName1, $link1);
    $strSql = "
        SELECT
            ru_photos.*
        FROM
            ru_photos
        WHERE
            ru_photos.tid = {$ar["id"]}
        ORDER BY
            ru_photos.id ASC
    ";

    $resPhotos = mysql_query($strSql, $link1);

    $photosCnt = mysql_num_rows($resPhotos);
    $photosTxt = '<div class="articles_photo bl"><div class="left scroll_arrows"><a class="prev browse left" ></a><span class="scroll_num">1</span>/'.$photosCnt.'<a class="next browse right" ></a>
	</div>';
    $photosTxt .= '<div class="clear"></div>';

    $keyCnt = 0;
    while($arPhotos = mysql_fetch_array($resPhotos)) {

        mysql_select_db($DBName1, $link1);
        if($arPhotos["is_enable"]) {
            $arTmpImg = Array();
            mysql_select_db($DB->DBName, $DB->db_Conn);
            $arTmpImg = CFile::MakeFileArray("http://www.restoran.ru/uploads/galery/".$arPhotos["path"]);
            $fid = CFile::SaveFile($arTmpImg, "iblock");
        }

        if($keyCnt == 0 && $arPhotos["is_enable"]) {
            $photosTxt .= '<div class="img">';
                $photosTxt .= '<img src="#FID_'.$fid.'#" width="720" class="first_pic" />';
                $photosTxt .= '<p><i></i></p>';
            $photosTxt .= '</div>';
            $photosTxt .= '<div class="special_scroll">';
            $photosTxt .= '<div class="scroll_container">';

            $arFields["DETAIL_PICTURE"] = CIBlock::ResizePicture($arTmpImg, array(
                "WIDTH" => 232,
                "HEIGHT" => 127,
                "METHOD" => "resample",
            ));

            $keyCnt++;
        }

        if($arPhotos["is_enable"] && $keyCnt > 0) {
            $active = "";
            if($keyCnt == 1) {
                $active = ' active';
            }
                $photosTxt .= '<div class="item'.$active.'">';
                    $photosTxt .= '<img src="#FID_'.$fid.'#" alt="" align="bottom"  class="pic"/>';
                $photosTxt .= '</div>';

            $keyCnt++;
        }
    }

    $photosTxt .= '</div>';
    $photosTxt .= '</div>';
    $photosTxt .= '</div>';
    $arFields["DETAIL_TEXT"] .= $photosTxt;

    /*
    if($EL_ID = $el->Add($arFields))
        echo "New ID: ".$EL_ID."<br />";
    else
        echo "Error: ".$el->LAST_ERROR."<br />";
    */

    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;

    if($time > 35) {
        echo "<script>setTimeout(function() {location.href = '/parser_photo.php?curStepID=".$curStepID."'}, 5000);</script>";
        break;
    }

    //v_dump($arFields);
    //v_dump($ar);
    //break;
}
?>