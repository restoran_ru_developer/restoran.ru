
<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
/*if(!$USER->IsAdmin())
    return false;


CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

$el = new CIBlockElement;
$bs = new CIBlockSection;
global $DB;

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "Hr55RW.C";

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

define("ADD_IBLOCK_TYPE_ID", 'rest_menu_ru');

$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function RecursiveTree2(&$rs, $parent)
{
    $out = array();
    if (!isset($rs[$parent]))
    {
        return $out;
    }
    foreach ($rs[$parent] as $row)
    {
        $chidls = RecursiveTree2($rs, $row['id']);
        if ($chidls) $row['childs'] = $chidls;
        $out[] = $row;
    }
    return $out;
}


function forAr($arr, $restSecID, $addIblockID, $arID = Array()) {
    //v_dump($addIblockID);
    global $DB;
    mysql_select_db($DB->DBName, $DB->db_Conn);
    $el = new CIBlockElement;
    $bs = new CIBlockSection;
    $obPrice = new CPrice();

    foreach($arr as $a) {
        if (is_array($a['childs'])) {
            $secID = $bs->Add(Array(
                "ACTIVE" => ($a["is_enable"] ? "Y" : "N"),
                "IBLOCK_ID" => $addIblockID,
                "IBLOCK_SECTION_ID" => (array_key_exists(intval($a["tid"]), $arID) ? $arID[$a["tid"]] : false),
                "NAME" => trim(htmlspecialchars_decode(stripcslashes($a["name"]))),
                "CODE" => translitIt($a["name"]),
            ));
            $arID[$a["id"]] = $secID;

            forAr($a['childs'], $restSecID, $addIblockID, $arID);
        }else{
            $arEl = Array();
            // parser text
            $tmpText = explode("<br />", $a["description"]);
            $arEl = Array(
                "ACTIVE" => ($a["is_enable"] ? "Y" : "N"),
                "IBLOCK_ID" => $addIblockID,
                "IBLOCK_SECTION_ID" => $arID[$a["tid"]],
                "NAME" => trim(htmlspecialchars_decode(stripcslashes($a["name"]))),
                "CODE" => translitIt($a["name"]),
                "PREVIEW_TEXT" => $tmpText[0],
            );
            if(!$elID = $el->Add($arEl))
                echo "Error: ".$el->LAST_ERROR;
            // set catalog product
            $arProductFields = Array(
                "ID" => $elID,
            );
            CCatalogProduct::Add(
                $arProductFields
            );
            // set price
            $arPriceFields = Array();
            $arPriceFields = Array(
                "PRODUCT_ID" => $elID,
                "CATALOG_GROUP_ID" => 1,
                "PRICE" => intval($a["price"]),
                "CURRENCY" => "RUB ",
            );
            $obPrice->Add($arPriceFields, true);
        }
    }
}

mysql_select_db($DBName1, $link1);

$strSql = "
    SELECT
        ru_items_menu.*
    FROM
        ru_items_menu
    WHERE
        ru_items_menu.item_id > {$curStepID}
    AND
        ru_items_menu.tid = 0
    GROUP BY
        ru_items_menu.item_id
    ORDER BY
        ru_items_menu.item_id ASC
";
$res = mysql_query($strSql, $link1);
while($ar = mysql_fetch_array($res)) {
    $curStepID = $ar["item_id"];

    // check existing in rests
    $exsRestID = 0;
    $arExRestIblock = Array("11", "12", "13", "15", "16");
    foreach($arExRestIblock as $restIblock) {
        $rsRestCheck = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "IBLOCK_ID" => $restIblock,
                "PROPERTY_old_item_id" => $ar["item_id"]
            ),
            false,
            false,
            Array()
        );
        if($arRestCheck = $rsRestCheck->Fetch()) {
            $exsRest["ID"] = $arRestCheck["ID"];
            $exsRest["NAME"] = $arRestCheck["NAME"];
            break;
        }
    }

    // get tree
    if($exsRest["ID"]) {
        v_dump($exsRest["NAME"]);
        // add iblock
        $rsTmp = mysql_query("SELECT ru_items_menu.* FROM ru_items_menu WHERE ru_items_menu.item_id = {$ar["item_id"]}");
        while($arTmp = mysql_fetch_array($rsTmp)) {
            $rs2[$arTmp['tid']][] = $arTmp;
        }
        $arr = RecursiveTree2($rs2, 0);

        mysql_select_db($DB->DBName, $DB->db_Conn);
        $ib = new CIBlock;
        $arIblockFields = Array(
            "ACTIVE" => ($ar["is_enable"] ? "Y" : "N"),
            "NAME" => $exsRest["NAME"],
            "CODE" => $ar["item_id"],
            "LIST_PAGE_URL" => "",
            "DETAIL_PAGE_URL" => "",
            "IBLOCK_TYPE_ID" => ADD_IBLOCK_TYPE_ID,
            "SITE_ID" => Array("s1"),
            "SORT" => "500",
            "DESCRIPTION" => $exsRest["ID"],
            "LIST_MODE" => "C",
            "INDEX_SECTION" => "N",
            "INDEX_ELEMENT" => "N",
            "GROUP_ID" => Array("2"=>"R", "14"=>"W", "15" => "W")
        );
        if(!$iblockID = $ib->Add($arIblockFields))
            echo "Error: ".$ib->LAST_ERROR;
        // XXX tmp
        // add to catalog
        if($iblockID)
            CCatalog::Add(
                Array(
                    "IBLOCK_ID" => $iblockID,
                    "YANDEX_EXPORT" => "N",
                    "SUBSCRIPTION" => "N",
                )
            );

        forAr($arr, false, $iblockID, array());
    }

    if($exsRest["ID"]) {
        echo "<script>setTimeout(function() {location.href = '/parser_menu.php?curStepID=".$curStepID."'}, 3000);</script>";
        break;
    }
}*/
?>