<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
//include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
@define("ERROR_404", "Y");
CHTTP::SetStatus("404 Not Found");
?>
<?
CModule::IncludeModule("iblock");
function iCopyIbElement($elementToCopy){
    $resource = CIBlockElement::GetByID($elementToCopy);
    if ($ob = $resource->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arFields['PROPERTIES'] = $ob->GetProperties();

        $arFieldsCopy = $arFields;
        unset($arFieldsCopy['ID'], $arFieldsCopy['TMP_ID'], $arFieldsCopy['WF_LAST_HISTORY_ID'], $arFieldsCopy['SHOW_COUNTER'], $arFieldsCopy['SHOW_COUNTER_START']);
        $arFieldsCopy['PROPERTY_VALUES'] = array();

        foreach ($arFields['PROPERTIES'] as $property)
        {
            if ($property['PROPERTY_TYPE']=='L'){
                if ($property['MULTIPLE']=='Y'){
                    $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = array();
                    foreach($property['VALUE_ENUM_ID'] as $enumID){
                        $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']][] = array(
                            'VALUE' => $enumID
                        );
                    }
                } else {
                    $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = array(
                        'VALUE' => $property['VALUE_ENUM_ID']
                    );
                }
            }
            elseif ($property['PROPERTY_TYPE']=='F')
            {
                if ($property['MULTIPLE']=='Y') {
                    if (is_array($property['VALUE']))
                    {
                        foreach ($property['VALUE'] as $key => $arElEnum)
                            $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']][$key]=CFile::CopyFile($arElEnum);
                    }
                }
                else $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = CFile::CopyFile($property['VALUE']);
            }
            else {
                $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = $property['VALUE'];
            }
        }

        $el = new CIBlockElement();
        $NEW_ID = $el->Add($arFieldsCopy);

        echo 'Элемент скопирован. ID нового элемента: '.$NEW_ID;
    }
}


//if($q = $_REQUEST['q']){
//    $arRestGroupIB = getArIblock("rest_group", CITY_ID);
//    $iblock_group = $arRestGroupIB["ID"];
//
//    $arClosedGroupIB = getArIblock("closed_rubrics_rest", CITY_ID);
//    $iblock_closed = $arClosedGroupIB["ID"];
//
//    $arRestIB = getArIblock("catalog", CITY_ID);
//    $iblock = $arRestIB["ID"];
//
//    $q = $DB->ForSQL($q);
//
//    $q2 = decode2anotherlang_ru($q);
//    $q2 = $DB->ForSQL($q2);
//
//    FirePHP::getInstance()->info($q);
//    FirePHP::getInstance()->info(CITY_ID);
//
//    $arSelect = Array("ID", "NAME", "IBLOCK_ID");
//    $arFilter = Array(
//        array(
//            "LOGIC" => "OR",
//            array("IBLOCK_ID"=>IntVal($iblock)),
//            array("IBLOCK_ID"=>IntVal($iblock_group)),
//            array("IBLOCK_ID"=>IntVal($iblock_closed))
//        ),
//        "ACTIVE"=>"Y",
//        array(
//            "LOGIC" => "OR",
//            array('NAME'=>$q.'%'),
//            array('NAME'=>$q2.'%'),
//            array('NAME'=>'%'.$q.'%'),
//            array('NAME'=>'%'.$q2.'%'),
//
//            array('TAGS'=>$q.'%'),
//            array('TAGS'=>$q2.'%'),
//            array('TAGS'=>'%'.$q.'%'),
//            array('TAGS'=>'%'.$q2.'%')
//        )
//    );
//    $res = CIBlockElement::GetList(Array('property_sleeping_rest'=>'ASC','NAME'=>'ASC'), $arFilter, false, Array("nTopCount"=>10), $arSelect);
//    while($ob = $res->Fetch())
//    {
//        FirePHP::getInstance()->info($ob);
//    }
//}


//
//    $city_array = array('spb','msk','rga','urm','tmn','kld','nsk','krd','sch','ufa','ast');

$city_str_name = 'rga';
$arIB = getArIblock("catalog", $city_str_name);

//restaurants
//banket

//RestIBlock::MakeCitySiteMapJson($arIB['ID'],'banket',$city_str_name);
//RestIBlock::MakeCitySiteMap($arIB['ID'],$city_str_name);
//RestIBlock::GlueAllCities();


//$e = CUser::GetByID(16823);
//if ($u = $e->Fetch())
//{
//    print_r($u);
//}
//RestIBlock::GlueGisOrganizationJson();
//RestIBlock::GlueGisExternalPhotosJson();

//echo date('Y-m-d\TH:i:sP',time());

//$rrest = CIBlockElement::GetByID(1052273);
//if ($ar_rest = $rrest->Fetch())
//{
//    print_r($ar_rest);
//}

//if($_REQUEST['CITY_ID']){
//    CModule::IncludeModule("search");
//    $cCustomRank = new CSearchCustomRank;
//
//    $arCatalogIB = getArIblock("catalog", $_REQUEST['CITY_ID']);
//    $rest_iblock = $arCatalogIB["ID"];
//
//    $arFilter = Array(
//        "PROPERTY_sleeping_rest_VALUE" => "Да",
//        'IBLOCK_ID'=>$rest_iblock,
//        'ACTIVE'=>'Y'
//    );
//    $res = CIBlockElement::GetList(Array('NAME'=>'ASC'), $arFilter, false, false, array('ID'));
//    while($ob = $res->Fetch())
//    {
//        $arFields = array(
//            'ID'=>$ob['ID'],
//            "SITE_ID"=>"s1",
//            "MODULE_ID"=>"iblock",
//            "PARAM1"=>"catalog",
//            "PARAM2"=>$rest_iblock,
//            "ITEM_ID"=>$ob['ID'],
//            'RANK'=>'-1',
//            'APPLIED'=>'Y'
//        );
//        if($custom_rank_id = $cCustomRank->Add($arFields))
//        {
//            echo $custom_rank_id.'<br>';
//        }
//        else {
//            echo 'fail<br>';
//        }
//    }
//}





//$sql = "SELECT * FROM show_counter_statistic WHERE rest_id=385357 AND stat_date='2015-11-17'";
//$db_list = $DB->Query($sql);
//if($ar_result = $db_list->Fetch())
//{
//    //update
//    FirePHP::getInstance()->info($ar_result);
//}



//if($USER->IsAdmin()&&1==2):
//    //  02.02.2016 15:55:30
//    $city_code = 'jurmala';
//    $arSelect = Array("ID", "NAME",'PROPERTY_rest','PROPERTY_procent');
//    $arFilter = Array("IBLOCK_ID"=>105, "ACTIVE"=>"Y", 'PROPERTY_status' => array(1506,1808),'SECTION_CODE'=>$city_code,'>TIMESTAMP_X'=>'02.02.2016 15:55:30');
//    $res = CIBlockElement::GetList(Array('ID'=>'ASC'), $arFilter, false, false, $arSelect);
//    while($ob = $res->Fetch())
//    {
//        //2254351 skip
////        if($ob['ID']==2254351)
////            continue;
//
//        $arItems[] = $ob;
////        dolg2restNEW($ob['PROPERTY_PROCENT_VALUE'],$ob['PROPERTY_REST_VALUE'],$city_code);
////        echo $ob['PROPERTY_PROCENT_VALUE'].'_'.$ob['PROPERTY_REST_VALUE'].'_'.$city_code.'<br>';
////        echo $ob['ID'];
////        AddMessage2Log($ob['ID'],'dolg_log'.$city_code);
////        break;
//    }
////
////    print_r($arItems);
//    echo '<br>'.count($arItems);
//endif;


//function dolg2restNEW($SUMMA, $RESTID, $CITY_CODE){
//    global $APPLICATION;
//    global $USER;
//
//    //смотрим сначала, есть ли запись для этого ресторана
//    $arFilter = Array("IBLOCK_ID"=>2541, "SECTION_CODE"=>$CITY_CODE, "PROPERTY_rest"=>$RESTID);
//    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false);
//    if($ob = $res->GetNextElement()){
//        $arFields = $ob->GetFields();
//        $arProps = $ob->GetProperties();
//
//        $DOLG = $arProps["dolg"]["VALUE"] + $SUMMA;
//
//        CIBlockElement::SetPropertyValuesEx($arFields["ID"], 2541, array("dolg" => $DOLG));
//    }else{
//        $el = new CIBlockElement;
//
//        //возьмем название ресторана
//        $res = CIBlockElement::GetByID($RESTID);
//        if($ar_res = $res->Fetch()){
//            $RESTNAME = $ar_res['NAME'];
//        }
//
//        if($CITY_CODE=="spb") $SECID=185097;
//        if($CITY_CODE=="msk") $SECID=185096;
//        if($CITY_CODE=="jurmala") $SECID=274886;
//
//        if($CITY_CODE=="nsk") $SECID=283286;
//
//        $PROP = array();
//        $PROP["dolg"] = $SUMMA;
//        $PROP["rest"] = $RESTID;
//
//        $arLoadProductArray = Array(
//            "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
//            "IBLOCK_SECTION_ID" => $SECID,          // элемент лежит в корне раздела
//            "IBLOCK_ID"      => 2541,
//            "PROPERTY_VALUES"=> $PROP,
//            "NAME"           => $RESTNAME,
//            "ACTIVE"         => "Y",            // активен
//        );
//
//        $el->Add($arLoadProductArray);
//
//    }
//
//
//}
//$NUMBER = '79523969381';

//$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y","PROPERTY_TELEPHONE"=>array($NUMBER, "8".$NUMBER, "9".$NUMBER));
//$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID','PROPERTY_SURNAME','PROPERTY_NAME'));
//if($ob = $res->Fetch()){
//    $EXIT=array("client"=>$ob["ID"], "telephone"=>$NUMBER, "name"=>$ob["PROPERTY_SURNAME_VALUE"]." ".$ob["PROPERTY_NAME_VALUE"]);
//}



//$arFilter = Array("IBLOCK_ID"=>104, "ACTIVE"=>"Y","PROPERTY_TELEPHONE"=>$NUMBER);
//$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('PROPERTY_SURNAME','PROPERTY_NAME'));
//if($ob = $res->Fetch()){
//    if($LANG=="en"){
//        $ob["PROPERTY_SURNAME_VALUE"] = translitIt($ob["PROPERTY_SURNAME_VALUE"]);
//        $ob["PROPERTY_NAME_VALUE"] = translitIt($ob["PROPERTY_NAME_VALUE"]);
//    }
//    echo $ob["PROPERTY_SURNAME_VALUE"]." ".$ob["PROPERTY_NAME_VALUE"];
//}

//$_REQUEST['email'] = 'konst@cakelabs.ru';
//$rsUsers = CUser::GetList(($by="timestamp_x"), ($order="desc"), Array("EMAIL" => trim($_REQUEST["email"]))); // выбираем пользователей
//if($rsUsers_arr = $rsUsers->Fetch()) {
//    FirePHP::getInstance()->info($rsUsers_arr,'$rsUsers_arr');
//}



?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>