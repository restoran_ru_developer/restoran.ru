<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
$arReviewsIB = getArIblock("reviews", CITY_ID);
$arRestIB = getArIblock("catalog", CITY_ID);
?> 	




<div id="container"> 			
  <div id="content"> 
    <div class="opinions">
      <div class="opinions-items"></div>
    
      <div class="opinions-items"> <?$APPLICATION->IncludeComponent("restoran:news.list", "opinions", array(
	"IBLOCK_TYPE" => "reviews",
	"IBLOCK_ID" => $arReviewsIB["ID"],
	"NEWS_COUNT" => "5",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "ELEMENT",
		2 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "140",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>  </div>
     </div>
   
    <div class="top-five"> 					 
      <div class="top-five-tizer"> 						Топ 5 <span>Restoran.ru</span> </div>
     <?     
//     $APPLICATION->IncludeComponent(
//	"restoran:news.list",
//	"top-5",
//	Array(
//		"IBLOCK_TYPE" => "catalog",
//		"IBLOCK_ID" => $arRestIB["ID"],
//		"NEWS_COUNT" => "5",
//		"SORT_BY1" => "PROPERTY_restoran_ratio",
//		"SORT_ORDER1" => "asc,nulls",
//		"SORT_BY2" => "SORT",
//		"SORT_ORDER2" => "ASC",
//		"FILTER_NAME" => "arPFilter",
//		"FIELD_CODE" => array(0=>"",1=>"",2=>"",),
//		"PROPERTY_CODE" => array(0=>"",1=>"address",2=>"",),
//		"CHECK_DATES" => "Y",
//		"DETAIL_URL" => "",
//		"AJAX_MODE" => "N",
//		"AJAX_OPTION_JUMP" => "N",
//		"AJAX_OPTION_STYLE" => "Y",
//		"AJAX_OPTION_HISTORY" => "N",
//		"CACHE_TYPE" => "A",
//		"CACHE_TIME" => "36000000",
//		"CACHE_FILTER" => "N",
//		"CACHE_GROUPS" => "Y",
//		"PREVIEW_TRUNCATE_LEN" => "",
//		"ACTIVE_DATE_FORMAT" => "d.m.Y",
//		"SET_TITLE" => "Y",
//		"SET_STATUS_404" => "N",
//		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
//		"ADD_SECTIONS_CHAIN" => "Y",
//		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
//		"PARENT_SECTION" => "",
//		"PARENT_SECTION_CODE" => "",
//		"DISPLAY_TOP_PAGER" => "N",
//		"DISPLAY_BOTTOM_PAGER" => "Y",
//		"PAGER_TITLE" => "Новости",
//		"PAGER_SHOW_ALWAYS" => "Y",
//		"PAGER_TEMPLATE" => "",
//		"PAGER_DESC_NUMBERING" => "N",
//		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//		"PAGER_SHOW_ALL" => "Y",
//		"DISPLAY_DATE" => "Y",
//		"DISPLAY_NAME" => "Y",
//		"DISPLAY_PICTURE" => "Y",
//		"DISPLAY_PREVIEW_TEXT" => "Y",
//		"AJAX_OPTION_ADDITIONAL" => ""
//	)
//);
//        else:
            global $arTopBlock;        
        $arIndexIB = getArIblock("index_page", CITY_ID);
      $arTopBlock["CODE"] = "top4";      
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "index_block_top4",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "index_page",
                "IBLOCK_ID" => $arIndexIB["ID"],
                "NEWS_COUNT" => "1",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arTopBlock",
                "FIELD_CODE" => "",
                "PROPERTY_CODE" => array(
                    0  => "ELEMENTS",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
            false
            );                 
     ?>  
        </div>
    </div>

<!-- #content-->
 		</div>

<!-- #container-->
 		<aside id="sideRight"> 			 
 <?$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"wo_features2",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"subway",1=>"average_bill",2=>"kitchen",),
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SPEC_IBLOCK" => "N",
		"SAVE_IN_SESSION" => "N",
		"PRICE_CODE" => ""
	)
);?> 		</aside>
<!-- #sideRight -->
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>