// Hide input text on click
$.fn.placeholder = function() {
    return $(this).each(function(){
      var labelclass = 'placheolder-active';
        var value = $(this).val();
        var input = $(this).prev();
        input
        .addClass(labelclass)
        .focus(function() {
            if ($(this).val() == value) {
                $(this).val('');
                $(this).removeClass(labelclass);
            }
        })
        .blur(function() {
            if($(this).val() == '') {
                $(this).val(value);
                $(this).addClass(labelclass);
            }
        })
        .closest('form').submit(function() {
            if (input.val() == value) {
                input.val('');
                $(this).removeClass(labelclass);
            }
        });
        if(input.val() == '') {
            input.val(value);
            $(this).addClass(labelclass);
        }
    })
}