<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
$arRestIB = getArIblock("catalog", CITY_ID);
if (!$_REQUEST["CITY_ID"])
    $_REQUEST["CITY_ID"] = CITY_ID;
?> 	
<div id="container"> 			
  <div id="content"> 
    <div class="catalog"> 					
      <div class="catalog-items"> 						
        <h2>Рестораны</h2>


<div class="catalog-serv">
<ul>
        <?
        // set rest sort links
        $excUrlParams = array("page", "pageRestSort", "CITY_ID", "CATALOG_ID", "PAGEN_1","by","index_php?page","index_php");
        $sortNewPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=new&".(($_REQUEST["pageRestSort"]=="new"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortPricePage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=price&".(($_REQUEST["pageRestSort"]=="price"&&$_REQUEST["by"]=="asc") ? "by=desc": "by=asc"), $excUrlParams);
        $sortRatioPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=ratio&".(($_REQUEST["pageRestSort"]=="ratio"&&$_REQUEST["by"]=="desc") ? "by=asc": "by=desc"), $excUrlParams);
        $sortAlphabetPage = $APPLICATION->GetCurPageParam(($_REQUEST["PAGEN_1"] ? "page=".$_REQUEST["PAGEN_1"]."&" : "page=1&")."pageRestSort=alphabet&".((($_REQUEST["pageRestSort"]=="alphabet"&&$_REQUEST["by"]=="asc")|| !$_REQUEST["pageRestSort"]) ? "by=desc": "by=asc"), $excUrlParams);
        ?>
        <?//if($_REQUEST["page"] || $_REQUEST["PAGEN_1"]):?>
            <?$by = ($_REQUEST["by"]=="desc")?"z":"a";?>
            <?
           if ($_REQUEST["pageRestSort"] == "alphabet" || !$_REQUEST["pageRestSort"])
            {
                echo '<li><span class="'.$by.'"><a class="another" href="'.$sortAlphabetPage.'">По названию</a></span></li>';
            }
            else
            {
                echo '<li><a class="another" href="'.$sortAlphabetPage.'">По названию</a></li>';
            }
            if ($_REQUEST["pageRestSort"] == "price")
            {
                echo '<li><span class="'.$by.'"><a class="another" href="'.$sortPricePage.'">По стоимости</a></span></li>';
            }
            else
            {
                echo '<li><a class="another" href="'.$sortPricePage.'">По стоимости</a></li>';
            }
            if ($_REQUEST["pageRestSort"] == "ratio")
            {
                echo '<li><span class="'.$by.'"><a class="another" href="'.$sortRatioPage.'">По популярности</a></span></li>';
            }
            else
            {
                echo '<li><a class="another" href="'.$sortRatioPage.'">По популярности</a></li>';
            }
            ?>
</ul>        
    </div>

<?$APPLICATION->IncludeComponent("restoran:restoraunts.list", "restoran_list", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $_REQUEST["CITY_ID"],
	"PARENT_SECTION_CODE" => "restaurants",
	"NEWS_COUNT" => ($_REQUEST["pageRestCnt"]?$_REQUEST["pageRestCnt"]:"6"),
	"SORT_BY1" => "NAME",
	"SORT_ORDER1" => "ASC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "arrFilter",
	"PROPERTY_CODE" => array(
		0 => "address",
		1 => "subway",
		2 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "Y",
	"CACHE_TIME" => "86400",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_PICTURE_MAX_WIDTH" => "278",
	"PREVIEW_PICTURE_MAX_HEIGHT" => "185",
	"PREVIEW_TRUNCATE_LEN" => "150",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Рестораны",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "facebook",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "N",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>    
       </div>
     </div>
   	</div>

<!-- #content-->
 		</div>

<!-- #container-->
 		<aside id="sideRight"> 			 <?$arRestIB = getArIblock("catalog", CITY_ID);?> <?$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"wo_features2",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"subway",1=>"average_bill",2=>"kitchen",),
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SPEC_IBLOCK" => "N",
		"SAVE_IN_SESSION" => "N",
		"PRICE_CODE" => ""
	)
);?> 		</aside>
<!-- #sideRight -->
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>