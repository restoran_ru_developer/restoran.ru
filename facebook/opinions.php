<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
$arReviewsIB = getArIblock("reviews", CITY_ID);
$arRestIB = getArIblock("catalog", CITY_ID);
?> 	 
<div id="container"> 			 
  <div id="content"> 
    <div class="catalog-opinions"> 					 
      <div class="catalog-items"> 						 
        <h2>Отзывы</h2>

<?$APPLICATION->IncludeComponent("restoran:news.list", "full-opinions", array(
	"IBLOCK_TYPE" => "reviews",
	"IBLOCK_ID" => $arReviewsIB["ID"],
	"NEWS_COUNT" => "10",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "ELEMENT",
		2 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "facebook",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

       </div>
     </div>
   	</div>
 
<!-- #content-->
 		</div>
 
<!-- #container-->
 		<aside id="sideRight"> 			 <?$arRestIB = getArIblock("catalog", CITY_ID);?> <?$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"wo_features2",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"subway",1=>"average_bill",2=>"kitchen",),
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SPEC_IBLOCK" => "N",
		"SAVE_IN_SESSION" => "N",
		"PRICE_CODE" => ""
	)
);?> 		</aside> 
<!-- #sideRight -->
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>