<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результаты поиска");
$arRestIB = getArIblock("catalog", CITY_ID);
?>

<div id="container"> 			
  <div id="content"> 

<?$APPLICATION->IncludeComponent("bitrix:search.page", "poisk", array(
	"RESTART" => "Y",
	"NO_WORD_LOGIC" => "Y",
	"CHECK_DATES" => "Y",
	"USE_TITLE_RANK" => "Y",
	"DEFAULT_SORT" => "rank",
	"FILTER_NAME" => "",
	"arrFILTER" => array(
		0 => "iblock_catalog",
	),
	"arrFILTER_iblock_catalog" => array(
		0 => $arRestIB["ID"],
	),
	"SHOW_WHERE" => "N",
	"SHOW_WHEN" => "N",
	"PAGE_RESULT_COUNT" => "6",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Результаты поиска",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "facebook",
	"USE_LANGUAGE_GUESS" => "N",
	"USE_SUGGEST" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
</div></div>
<aside id="sideRight"> 			 <?$arRestIB = getArIblock("catalog", CITY_ID);?> <?$APPLICATION->IncludeComponent(
	"restoran:catalog.filter",
	"wo_features2",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => $arRestIB["ID"],
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"PROPERTY_CODE" => array(0=>"subway",1=>"average_bill",2=>"kitchen",),
		"LIST_HEIGHT" => "5",
		"TEXT_WIDTH" => "20",
		"NUMBER_WIDTH" => "5",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SPEC_IBLOCK" => "N",
		"SAVE_IN_SESSION" => "N",
		"PRICE_CODE" => ""
	)
);?> 		</aside>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>