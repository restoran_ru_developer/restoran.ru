<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?

if(!$USER->IsAdmin())
    return false;

define("OLD_CITY_ID", 144);
define("ADD_IBLOCK_ID_REVIEW", 205);
define("ADD_IBLOCK_ID_COMMENTS", 2438);
define("REST_IBLOCK_ID", 16);
define("USER_DEFAULT_ID", 130);

CModule::IncludeModule("iblock");
$el = new CIBlockElement;

global $DB;

$DBHost1 = "localhost";
$DBName1 = "test";
$DBLogin1 = "root";
$DBPassword1 = "Hr55RW.C";

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// stepping
$curStepID = ($_REQUEST["curStepID"] ? $_REQUEST["curStepID"] : 0);

// check connect
if(!$link1 = mysql_connect($DBHost1, $DBLogin1, $DBPassword1)) {
    echo "Connect Error!" . mysql_error();
    die();
}

// start execution
$time_start = microtime_float();

mysql_select_db($DBName1, $link1);

$strSql = "
    SELECT
        ru_items_opinions.*
    FROM
        ru_items_opinions
    WHERE
        ru_items_opinions.city_id = ".OLD_CITY_ID." AND ru_items_opinions.item_id > 0 AND ru_items_opinions.tid = 0 AND ru_items_opinions.id > {$curStepID}
    ORDER BY
        ru_items_opinions.id ASC
";

/*
$strSql = "
    SELECT
        ru_items_opinions.*
    FROM
        ru_items_opinions
    WHERE
        ru_items_opinions.city_id = ".OLD_CITY_ID." AND ru_items_opinions.tid > 0 AND ru_items_opinions.id > {$curStepID}
    ORDER BY
        ru_items_opinions.id ASC
";
*/
$res = mysql_query($strSql, $link1);
while($ar = mysql_fetch_array($res)) {

    $curStepID = $ar["id"];
    $bindEl = 0;

    $rsRest = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array(
            //"ACTIVE" => "Y",
            "IBLOCK_ID" => REST_IBLOCK_ID,
            "PROPERTY_old_item_id" => $ar["item_id"]
        ),
        false,
        false,
        Array("ID", "NAME")
    );
    if($arRest = $rsRest->Fetch()) {
        $bindEl = $arRest["ID"];
    } else {
        $rsRest = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                //"ACTIVE" => "Y",
                "IBLOCK_ID" => ADD_IBLOCK_ID_REVIEW,
                //"PROPERTY_old_item_id" => $ar["item_id"]
                "PROPERTY_OLD_OPINION_ID" => $ar["tid"]
            ),
            false,
            false,
            Array("ID", "NAME")
        );
        if($arRest = $rsRest->Fetch())
            $bindEl = $arRest["ID"];
    }

    if($bindEl) {

        $PROP = array();
        $PROP["OLD_OPINION_ID"] = $ar["id"];
        //$PROP["ELEMENT"] = $bindEl;
        $PROP["PARENT"] = $bindEl;

        $arAddField = Array(
            "ACTIVE" => ($ar["is_enable"] ? "Y" : "N"),
            "ACTIVE_FROM" => $DB->FormatDate($ar["date"], "YYYY-MM-DD HH:MI:SS", "DD.MM.YYYY HH:MI:SS"),
            "DATE_CREATE" => $DB->FormatDate($ar["date"], "YYYY-MM-DD HH:MI:SS", "DD.MM.YYYY HH:MI:SS"),
            "IBLOCK_ID" => ADD_IBLOCK_ID_REVIEW,
            "CREATED_USER_ID" => USER_DEFAULT_ID,
            "MODIFIED_USER_ID" => USER_DEFAULT_ID,
            "NAME" => "old.restoran.ru",
            "PREVIEW_TEXT" => html_entity_decode(htmlspecialchars_decode(stripcslashes($ar["text"]))),
            "PROPERTY_VALUES" => $PROP
        );

        $rsOpinion = CIBlockElement::GetList(
            Array("SORT"=>"ASC"),
            Array(
                "IBLOCK_ID" => ADD_IBLOCK_ID_REVIEW,
                "PROPERTY_OLD_OPINION_ID" => $ar["id"]
            ),
            false,
            false,
            Array("ID", "NAME")
        );
        if($arOpinion = $rsOpinion->Fetch()) {
            //$el->Update($arOpinion["ID"], $arAddField);
            v_dump("UPDATE: ".$arOpinion["ID"]);
        } else {
            //$opinionID = $el->Add($arAddField);
            v_dump("NEW: ".$opinionID);
            //break;
        }

    }

//    break;

    // execution time
    $time_end = microtime_float();
    $time = $time_end - $time_start;

    if($time > 35) {
        echo "<script>setTimeout(function() {location.href = '/parser_opinions.php?curStepID=".$curStepID."'}, 5000);</script>";
        break;
    }
}
?>
