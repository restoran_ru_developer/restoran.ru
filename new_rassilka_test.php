<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>

<?
global $USER;
if (isset($_POST["RUBRIC_ID"])) {
    $resultArray["RUBRIC_ID"] = $_POST["RUBRIC_ID"];
    $resultArray["EMAIL"] = $_POST["EMAIL"];
    $resultArray["CITY_ID"] = $_POST["CITY_ID"];
    $resultArray["rubriki"] = $_POST["rubriki"];
    $rsUser = $USER->GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();
    $podpiska = unserialize($arUser["WORK_STREET"]);
    $USER->update($_POST["USER_ID"], array("WORK_STREET" => serialize($resultArray)));
    $oldRubricId = $podpiska["RUBRIC_ID"];
    $newRubricId = $resultArray["RUBRIC_ID"];

    if ($oldRubricId !== $newRubricId) {
        if (CModule::IncludeModule("subscribe")) {
            $subscr = new CSubscription;
            $rsSubscription = $subscr->GetList(array(), array("USER_ID" => $USER->GetID()));
            if ($arSubscription = $rsSubscription->GetNext())
                $ID = intval($arSubscription["ID"]);
            else
                $ID = 0;
            $oldRubricArray = $subscr->GetRubricArray($ID);
            $oldRubricArray[] = $_POST["RUBRIC_ID"];
            for ($i = 0; $i < count($oldRubricArray); $i++) {
                if ($oldRubricArray[$i] == $oldRubricId) {
                    unset($oldRubricArray[$i]);
                }
            }
            $arFieldsSub = Array(
                "USER_ID" => $_POST["USER_ID"],
                "FORMAT" => "html",
                "EMAIL" => $resultArray["EMAIL"],
                "ACTIVE" => "Y",
                "RUB_ID" => $oldRubricArray,
                "CONFIRMED" => "Y",
                "SEND_CONFIRM" => "N"
            );

            $ID = $subscr->Update($ID, $arFieldsSub);
            if ($ID)
                $subscr->Authorize($ID);
        }
    }
}
?>
<div id="content">
    <h2>Рассылки</h2>
    <?
    $APPLICATION->IncludeComponent(
            "restoran:subscribe.select_category", "with_period", Array(
        "AJAX_MODE" => "N",
        "SHOW_HIDDEN" => "N",
        "ALLOW_ANONYMOUS" => "N",
        "SHOW_AUTH_LINKS" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "SET_TITLE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
            ), false
    );
    ?>

    <div class="clear"></div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>