<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<h1>Мы любим вас!</h1>
<i>Дорогие наши клиенты! Гурманы, открыватели новых вкусных мест, любители встретится с друзьями за бокалом-другим красного, Ресторан.ру объявляет акцию! </i>
<br>                

<?if (CITY_ID=="spb"):?>
<p>Бронируйте столики в рестораны Питера* через нашу Службу +7(812) 740-18-20! Сделайте 3 бронирования, и мы с благодарностью &nbsp;переведем на ваш мобильный номер 300 рублей. За 10 - 500 рублей. Мы будем только рады, если вы будете звонить нам чаще!&nbsp;</p>
<?elseif(CITY_ID=="msk"):?>
<p>Бронируйте столики в рестораны Москвы* через нашу Службу +7(495)988-2656! Сделайте 3 бронирования, и мы с благодарностью &nbsp;переведем на ваш мобильный номер 300 рублей. За 10 - 500 рублей. Мы будем только рады, если вы будете звонить нам чаще!&nbsp;</p>
<?endif;?>

<p>Резервация столиков в любые рестораны города совершенно бесплатна!</p>

<p><i>* акция действует при условии бронирования и оплаты счета в ресторанах-партнерах сайта Ресторан.ру</i></p>
<br />
<img src="/upload/resize_cache/editor/0a1/632_486_11439d68753681822a7c36537878e9c3f/rassylka977.png" width="100%" class="first_pic">           
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>