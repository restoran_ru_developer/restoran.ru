<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<?if ($APPLICATION->get_cookie("COORDINATES")=="Y"):    
    LocalRedirect("/index1.php");
else:?>
    <script>
        var mainnavigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';
        var mainsessionLat = '<?= $_SESSION['lat'] ?>';
        var mainsessionLon = '<?= $_SESSION['lon'] ?>';  
        function success_main(position) {
            $.ajax({
                type: "POST",
                url: "/ajax-coordinates.php",
                data: {
                    lat:position.coords.latitude,
                    lon:position.coords.longitude
                },
                success: function(data) {             
                    location.reload();
                }
            });
        }
        function error_main()
        {
            alert("Невозможно определить местоположение");
            return false;
        }
        function get_location_main()
        {
            if (mainnavigatorOn != 'Y') {
                if (navigator.geolocation) {                
                    navigator.geolocation.getCurrentPosition(success_main, error_main);
                } else {
                    alert("Ваш браузер не поддерживает\nопределение местоположения");
                    return false;
                }
            }
        }
        get_location_main();                       
    </script>
<?endif;?>