<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>    
<meta charset="utf-8">
<?$APPLICATION->ShowHead();?>

<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
<link rel="stylesheet" href="/bitrix/templates/mobile/themes/restoran.css" />	              

<link href="jquery-mobile.css" type="text/css" rel="stylesheet" />
<link href="photoswipe.css" type="text/css" rel="stylesheet" />

	
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>
<script type="text/javascript" src="lib/klass.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.0rc2/jquery.mobile-1.0rc2.min.js"></script>
<script type="text/javascript" src="code.photoswipe.jquery-3.0.5.js"></script>


<script type="text/javascript">

    $(document).ready(function(){
        var myPhotoSwipe = $(".Gallery a").photoSwipe({ enableMouseWheel: false , enableKeyboard: false });
        $(".Gallery a").click();
    });
    
</script>
<style>.panel-content { padding:15px; }	.ui-panel-dismiss {
    display: none;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 1002;
}
.ui-panel-inner li {
    padding:0 !important;
}
.ui-panel-inner .ui-li a.ui-link-inherit, .ui-li-static.ui-li {
    padding: 6px -1px 6px 6px !important;
}
.ui-panel-inner .ui-btn-inner {
    padding: 0px !important;
    background: none !important;
    border: 0 !important;
}
.ui-panel-inner .ui-btn-text {
    background: none !important;
}
.ui-body-c, .ui-overlay-c {
    background: none !important;
    border: 0 !important;
}
input { 
    border: 0 !important;
    box-shadow: none !important;
}

.ui-shadow-inset { 
    border: 0 !important;
    box-shadow: none !important;
}
.backlink {
    z-index: 10000;
}
.gallery-close-link {
    line-height: 200px;
    font-size: 100px;
}
</style>
<div data-role="page" data-add-back-btn="true" id="Gallery2" class="gallery-page" style="background: url(/bitrix/templates/mobile/image/mobile_bg2.png) right top no-repeat #414858!important;    ">
    <!--<div data-role="header" id="header" data-position="inline" style="height:45px;">                                                          
        <a href="#defaultpanel" data-icon="bars" data-theme="b" style="margin-top:5px;">Меню</a>
        <div class="left-logo"></div>
        <div class="clear"></div>
    </div>-->
    <div data-role="content">
        
        <ul class="gallery" style='display:none'>
            <?
            $arIB = getArIblock("catalog", CITY_ID);
            ?>

            <?
            $APPLICATION->IncludeComponent("restoran:restoraunts.detail", "gallery_new", array(
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arIB["ID"],
                "ELEMENT_ID" => "",
                "ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
                "CHECK_DATES" => "Y",
                "PROPERTY_CODE" => array(
                    0 => "type",
                    1 => "average_bill",
                    2 => "kitchen",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "administrative_distr",
                    6 => "area",
                    7 => "address",
                    8 => "subway",
                    9 => "number_of_rooms",
                    10 => "music",
                    11 => "clothes",
                    12 => "proposals",
                    13 => "credit_cards",
                    14 => "discouncards",
                    15 => "bankets",
                    16 => "touristgroups_1",
                    17 => "banketnayasluzhba",
                    18 => "kolichestvochelovek",
                    19 => "stoimostmenyu",
                    20 => "max_check",
                    21 => "entertainment",
                    22 => "wi_fi",
                    23 => "hrs_24",
                    24 => "parking",
                    25 => "features",
                    26 => "out_city",
                    27 => "min_check",
                    28 => "site",
                    29 => "children",
                    30 => "ideal_place_for",
                    31 => "offers",
                    32 => "email",
                    33 => "discounts",
                    34 => "landmarks",
                    35 => "map",
                    36 => "RATIO",
                    37 => "COMMENTS",
                    38 => "okrugdel",
                    39 => "kuhnyadostavki",
                    40 => "viduslug",
                    41 => "add_props",
                    42 => "network",
                    43 => "rest_group",
                    44 => "breakfasts",
                    45 => "business_lunch",
                    46 => "branch",
                    47 => "rent",
                    48 => "my_alcohol",
                    49 => "catering",
                    50 => "food_delivery",
                    51 => "proposals",
                    52 => "d_tours"
                ),
                "ADD_REVIEWS" => "Y",
                "REVIEWS_BLOG_ID" => "3",
                "SIMILAR_OUTPUT" => "Y",
                "SIMILAR_PROPERTIES" => array(
                    0 => "kitchen",
                    1 => "average_bill",
                ),
                "IBLOCK_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "60",
                "CACHE_GROUPS" => "Y",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Страница",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
                    ), false
            );
            ?>

        </ul>
    </div>
    <div data-role="panel" id="defaultpanel" data-theme="a">
    <form name="rest_filter_form" method="get" data-transition="fade" action="/mobile/search.php">        
        <input type="hidden" value="rest" name="search_in" id="by_name">        
        <div class="left">
            <input type="text" placeholder="Поиск" data-theme="b" value="" name="q">            
        </div>
        <div class="left" style="margin-left:5px;padding-top:5px;">            
            <a href="javascript:void(0)" data-role="button" data-iconshadow="false" data-transition="fade" data-icon="search" data-theme="a" data-iconpos="notext" onclick="$(this).parents('form').submit()" ></a>
        </div>        
        <div class="clear"></div>
    </form>
    
    <ul data-role="listview" class="nav-search" data-inset="true"  data-theme="e">
        <li><a  data-ajax="false" href="/mobile/map.php" data-transition="fade" >Ближайшие на карте</a></li>        
    </ul>
    
    <ul data-role="listview" class="nav-search" data-inset="true" data-theme="d">
        <li <? if ($APPLICATION->GetCurPage() == "/mobile/catalog.php"){ echo 'class="ui-btn-active"';} ?>><a href="/mobile/catalog.php" data-transition="fade" >Рестораны</a></li>
        <li <? if ($APPLICATION->GetCurPage() == "/mobile/banket.php"){ echo 'class="ui-btn-active"';} ?>><a href="/mobile/banket.php" data-transition="fade" >Банкетные залы</a></li>
        <li <? if ($APPLICATION->GetCurPage() == "/mobile/opinions.php"){ echo 'class="ui-btn-active"';} ?>><a href="/mobile/opinions.php" data-transition="fade" >Отзывы</a></li>
        <li <? if (substr($APPLICATION->GetCurPage(), 0, 14) == "/mobile/search" || (substr($APPLICATION->GetCurPage(), 0, 14)) == "/mobile/filter"){ echo 'class="ui-btn-active"';} ?>><a href="/mobile/search.php" data-transition="fade" >Поиск</a></li>
        <li <? if ($APPLICATION->GetCurPage() == "/mobile/online_order.php"){ echo 'class="ui-btn-active"';} ?>><a href="/mobile/online_order.php" data-transition="fade" >Забронировать столик</a></li>
        <li <? if ($APPLICATION->GetCurPage() == "/mobile/auth/"){ echo 'class="ui-btn-active"';} ?>><a href="/mobile/auth/" data-transition="fade" data-ajax="false">Личный кабинет</a></li>
    </ul>

</div>
</div>