<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>    
<link href="http://code.jquery.com/mobile/1.0rc2/jquery.mobile-1.0rc2.min.css" rel="stylesheet" />
<link href="jquery-mobile.css" type="text/css" rel="stylesheet" />
<link href="../photoswipe.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/bitrix/templates/mobile/themes/restoran.css" />
<script type="text/javascript" src="../lib/klass.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.0rc2/jquery.mobile-1.0rc2.min.js"></script>
<script type="text/javascript" src="../code.photoswipe.jquery-3.0.5.min.js"></script>


<script type="text/javascript">

    $(document).ready(function(){
        var myPhotoSwipe = $(".Gallery a").photoSwipe({ enableMouseWheel: false , enableKeyboard: false });
    });
    
</script>

<div data-role="page" data-add-back-btn="true" id="Gallery2" class="gallery-page">
    <div data-role="content">
        <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
        <ul class="gallery">
            <?
            $arIB = getArIblock("catalog", CITY_ID);
            ?>

            <?
            $APPLICATION->IncludeComponent("restoran:restoraunts.detail", "gallery_new", array(
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => $arIB["ID"],
                "ELEMENT_ID" => "",
                "ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
                "CHECK_DATES" => "Y",
                "PROPERTY_CODE" => array(
                    0 => "type",
                    1 => "average_bill",
                    2 => "kitchen",
                    3 => "opening_hours",
                    4 => "phone",
                    5 => "administrative_distr",
                    6 => "area",
                    7 => "address",
                    8 => "subway",
                    9 => "number_of_rooms",
                    10 => "music",
                    11 => "clothes",
                    12 => "proposals",
                    13 => "credit_cards",
                    14 => "discouncards",
                    15 => "bankets",
                    16 => "touristgroups_1",
                    17 => "banketnayasluzhba",
                    18 => "kolichestvochelovek",
                    19 => "stoimostmenyu",
                    20 => "max_check",
                    21 => "entertainment",
                    22 => "wi_fi",
                    23 => "hrs_24",
                    24 => "parking",
                    25 => "features",
                    26 => "out_city",
                    27 => "min_check",
                    28 => "site",
                    29 => "children",
                    30 => "ideal_place_for",
                    31 => "offers",
                    32 => "email",
                    33 => "discounts",
                    34 => "landmarks",
                    35 => "map",
                    36 => "RATIO",
                    37 => "COMMENTS",
                    38 => "okrugdel",
                    39 => "kuhnyadostavki",
                    40 => "viduslug",
                    41 => "add_props",
                    42 => "network",
                    43 => "rest_group",
                    44 => "breakfasts",
                    45 => "business_lunch",
                    46 => "branch",
                    47 => "rent",
                    48 => "my_alcohol",
                    49 => "catering",
                    50 => "food_delivery",
                    51 => "proposals",
                    52 => "d_tours"
                ),
                "ADD_REVIEWS" => "Y",
                "REVIEWS_BLOG_ID" => "3",
                "SIMILAR_OUTPUT" => "Y",
                "SIMILAR_PROPERTIES" => array(
                    0 => "kitchen",
                    1 => "average_bill",
                ),
                "IBLOCK_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "60",
                "CACHE_GROUPS" => "Y",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Страница",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
                    ), false
            );
            ?>

        </ul>
    </div>
</div>