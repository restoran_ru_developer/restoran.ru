<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
	LocalRedirect($backurl);

$APPLICATION->SetTitle("Авторизация");

$APPLICATION->IncludeComponent("restoran:main.profile", "users_profile", Array(
    "ID" => (int) $_REQUEST["USER_ID"],
    "USER_PROPERTY_NAME" => "", // Название закладки с доп. свойствами
    "SET_TITLE" => "N", // Устанавливать заголовок страницы
    "AJAX_MODE" => "N", // Включить режим AJAX
    "USER_PROPERTY" => Array("UF_HIDE_CONTACTS"), // Показывать доп. свойства
    "SEND_INFO" => "N", // Генерировать почтовое событие
    "CHECK_RIGHTS" => "N", // Проверять права доступа
    "AJAX_OPTION_SHADOW" => "Y", // Включить затенение
    "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        ), false
);
?>
<div class="clear"></div>
<a style="width:100%;" data-role="button" data-theme="e" data-mini="false" data-ajax="false" href="?logout=yes&USER_ID=<?=$USER->getId()?>">Выйти</a>
    <?
    global $arrFilterTop4;
    $arrFilterTop4["CREATED_BY"] = (int) $_REQUEST["USER_ID"];
    $arrFilterTop4["IBLOCK_TYPE"] = Array("reviews");   
    ?>
    <?
    $APPLICATION->IncludeComponent(
            "restoran:catalog.list", "activity", Array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "reviews",
        "IBLOCK_ID" => "",
        "NEWS_COUNT" => 10,
        "SORT_BY1" => "DATE_CREATE",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "arrFilterTop4",
        "FIELD_CODE" => array("CREATED_BY", "DETAIL_PICTURE"),
        "PROPERTY_CODE" => array("ratio", "reviews_bind"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "120",
        "ACTIVE_DATE_FORMAT" => "j F Y G:i",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "ACTIVITY" => "Y"
            ), false
    );
    ?>
    <div class="clear"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>