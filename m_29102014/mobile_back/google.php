<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<style>
    #partnerNotice
    {
        max-width:200px;
        line-heigth:100%;
    }
    #partnerNotice i
    {
        font-size:80%;        
        line-heigth:80%;
    }
    #partnerNotice a
    {
        color: #000;
        
    }
</style>
<script>
    var infoWindow = [];
    var infoString = [];
    function addInfoWindow(i,marker, message, map, link) {
        infoString[i] = '<div id="partnerNotice">' +
            '<a href="'+ link +'">' + message + '</a>' +            
            '</div>';        
        infoWindow[i] = new google.maps.InfoWindow({
            content: infoString[i],         
            //disableAutoPan: true
        });
        
        google.maps.event.addListener(marker, 'click', function () {
            for(var p=0; p<infoWindow.length;p++)
            {
                infoWindow[p].close();
            }
            infoWindow[i].open(map, marker);
        });
        
    }
        
    function getinfo()
    {
        $("#map_overflow").show();
        var bounds = map.getBounds().toUrlValue();
        var c = [];
        c = bounds.split(",");        
        var map_filter = "xl="+c[0]+"&yl="+c[1]+"&xr="+c[2]+"&yr="+c[3];
        var image = new google.maps.MarkerImage(
            '/tpl/images/map/ico_rest.png',        
            new google.maps.Size(27, 32),        
            new google.maps.Point(0, 0),        
            new google.maps.Point(0, 32)
        );
        var shape = {
            coord: [1, 1, 27, 32],
            type: 'rect'
        };
        jQuery.get ( requestMarkersUrl, map_filter, function ( data ) {            
            if (data)
            {                
                var coords = [];
                var marker = [];
                for ( var i in data )
                {       
                    if ( i != 'length' && typeof ( map_markers[data[i].id] ) == 'undefined')
                    {   
                        coords.push([parseFloat(data[i].lat), parseFloat(data[i].lon)]);
                    }                
                }
                
                for (var i = 0; i < map_markers.length; i++) {
                    map_markers[i].setMap(null);
                }
                map_markers = [];
                
                for (var i = 0; i<data.length; i++) {
                    marker[i] = new google.maps.Marker({
                        map:map,
                        draggable:false,
                        icon: image,
                        shape: shape,
                        animation: false,
                        position: new google.maps.LatLng(coords[i][0], coords[i][1])
                    });
                    addInfoWindow(i,marker[i], "<a href='"+data[i]["url"]+"'>"+data[i]["name"]+"</a><br /><i>"+data[i]["adres"]+"</i>", map, data[i]["url"]);                    
                }
                map_markers = marker;
                
            }
                $("#map_overflow").hide();
        }, 'json' );  
    }
    
    function success_a(position) {        
        /*var img = new Image();
        var h = $(window).height();
        var w = $(window).width();        
        img.src = "http://maps.googleapis.com/maps/api/staticmap?center="+position.coords.latitude+","+position.coords.longitude+"&zoom=13&size="+w+"x80&sensor=false";
        $("<img src="+img.src+" />").appendTo("#map");            */
        map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));   
        var my_marker = new google.maps.Marker({
            map:map,
            draggable:false,                        
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
        });
        $.ajax({
            type: "POST",
            url: "/ajax-coordinates.php",
            data: {
                lat:position.coords.latitude,
                lon:position.coords.longitude
            },
            success: function(data) {
                if (!$("#near").html())
                    $("#near").append('<a style="width:100%; float: left;" data-role="button" data-theme="b" data-mini="false" data-ajax="true" href="/catalog.php?page=1&pageRestSort=distance&by=asc&CITY_ID=<?=CITY_ID?>&set_filter=Y">Ближайшие рестораны</a>');
            }
        });
        setTimeout("getinfo()",300);
    }
    function error_a()
    {
        alert("Невозможно определить местоположение");
        return false;
    }
    function get_location_a()
    {
        if (navigatorOn !== 'Y') {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }
        } else {
            map.setCenter(new google.maps.LatLng(sessionLat, sessionLon));   
            var my_marker = new google.maps.Marker({
                map:map,
                draggable:false,                        
                animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng(sessionLat, sessionLon)
            });
            setTimeout("getinfo()",300);
        }                           
    }
      
</script>
<script type="text/javascript">
    var map = "";
    var requestMarkersUrl = "/tpl/ajax/map_load_markers_mobile.php?<?= bitrix_sessid_get() ?>";
    var map_markers = [];
    var oldCenter = [];
    var differnce =1;
    var aj_load = 0;
    function initialize() {   
        //$("#map").css("height",$(window).height()/2+"px");
        //$("#reviews").css("margin-top",$(window).height()/2+"px");
        if ($.browser.safari==true)
        {                                               
            //$("#map").css("height",($(window).height()-($(window).height()*0.3))+"px"); 
            $("#map").css("height",$(window).height()+"px"); 
            $("#map").css("width",$("#page").width()+"px");
            $("#buttons").css("margin-top",($(window).height()-($(window).height()*0.42))+"px");
        }
        else
        {
            //$("#map").css("height",($(window).height()-($(window).height()*0.35))+"px");            
            $("#map").css("height",$(window).height()+"px"); 
            $("#map").css("width",$("#page").width()+"px");
            $("#buttons").css("margin-top",($(window).height()-($(window).height()*0.45))+"px");
        }
        $( ".swipe-block" ).on( 'click', clickHandler );
        function clickHandler( event ) {
            href = $(".one-slide:visible").attr('href');
            location.replace(href);
        }
        var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
        coord = coord.split(",");
        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(coord[1], coord[0]),
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            //draggable: false,
            //scrollwheel: false
        }        
//        var styles = [{"stylers": [{ "gamma": 0.75 },{ "lightness": 5 }]}];
//        var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
                                                               
        map = new google.maps.Map(document.getElementById("map"), mapOptions);         
//        map.mapTypes.set('map_style', styledMap);
//        map.setMapTypeId('map_style');
        
        
        google.maps.event.addListener(map, 'bounds_changed', function() { 
            differnce = 0;            
            var c = [];
            c = map.getBounds().toUrlValue().split(",");                  
            var newCenter = c;
            if (!oldCenter[0])
                oldCenter = c;
            if (Math.abs(oldCenter[0]-newCenter[0])>0.01||Math.abs(oldCenter[1]-newCenter[1])>0.01)
                differnce = 1;            
            //console.log(newCenter);
            //console.log(differnce);
            if (differnce==1&&aj_load==0){
                aj_load = 1;
                setTimeout("getinfo(); aj_load=0;",400);
                oldCenter = c;
            }
            
            //clearInterval(interval);
            //interval = setInterval("getinfo()",500);            
        });
        get_location_a(); 
    }

    function loadScript() {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBIPD9UKtX4nflE-8ktUp62E8DVReinvUo&sensor=true&callback=initialize&sensor=TRUE";
        document.body.appendChild(script);
    }
    navigatorOn = '<?=$APPLICATION->get_cookie("COORDINATES")  ?>';
    sessionLat = '<?=$_SESSION['lat']  ?>';
    sessionLon = '<?=$_SESSION['lon']  ?>';
    window.onload = loadScript;
</script>
<div id="map" style="position:absolute; width: 100%; height: 300px; overflow: hidden;"></div>
<div id="map_overflow" style="position:absolute; z-index:99; top:0px; right:0px; width:120px; text-align:center; font-size:11px; height:20px;padding-top: 3px; border-bottom-left-radius: 10px; background:#000; opacity:0.8; color:#FFF;">Загрузка меток...</div>
<div id="buttons" style="z-index:100; padding:10px; padding-top:40px; position:relative; margin-left:-10px; margin-right:-10px; 
     background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjM1JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjkyJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
background: -moz-linear-gradient(top,  rgba(255,255,255,0) 0%, rgba(255,255,255,1) 35%, rgba(255,255,255,1) 92%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0)), color-stop(35%,rgba(255,255,255,1)), color-stop(92%,rgba(255,255,255,1)));
background: -webkit-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 35%,rgba(255,255,255,1) 92%);
background: -o-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 35%,rgba(255,255,255,1) 92%);
background: -ms-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 35%,rgba(255,255,255,1) 92%);
background: linear-gradient(to bottom,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 35%,rgba(255,255,255,1) 92%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=0 );">
    <div id="current_position" style="position:absolute; top:0px; right:10px;"><a href="#" data-theme="a"><img src="glyphicons_233_direction.png" /></a></div>
<div class="button-font13">
    <a style="width:49%; float: left; margin-right: 2%;" data-role="button" data-theme="b" data-mini="false" data-ajax="false" href="/online_order.php">Забронировать</a>
    <a style="width:49%; float: left;" data-role="button" data-theme="b" data-mini="false" data-ajax="true" href="/filter.php">Фильтр</a>
</div>
<div id="near">    
    <? if ($_SESSION['lat']): ?>
        <a style="width:100%; float: left;" data-role="button" data-theme="b" data-mini="false" data-ajax="true" href="/catalog.php?page=1&pageRestSort=distance&by=asc&CITY_ID=<?=CITY_ID?>&set_filter=Y">Ближайшие рестораны</a>
    <? endif; ?>
</div>
<div class="clear"></div>
<?
$arIndexIB = getArIblock("index_page", CITY_ID);
global $arTopBlock;
$arTopBlock["CODE"] = "mobile_main";
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "index_block_top4",
    Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "index_page",
        "IBLOCK_ID" => $arIndexIB["ID"],
        "NEWS_COUNT" => "1",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "arTopBlock",
        "FIELD_CODE" => "",
        "PROPERTY_CODE" => array(
            0  => "ELEMENTS",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "120",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
    false
    );
?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>			