<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
global $arIB;
$arIB = getArIblock("catalog", CITY_ID);
//v_dump($templ);
?>
<?if ($_REQUEST["formresult"]=="addok"):?>
<?
$r = $APPLICATION->get_cookie("MY_BRONS");
if (!substr_count($r, $_REQUEST["ID"]))
{
    if ($r)
        $r = $_REQUEST["ID"]."###".$r;
    else
        $r = $_REQUEST["ID"];
}
        
$r = explode("###", $r);
foreach ($r as $key=>$rr)
{
    if ($key>2)
    {
        unset($r[$key]);
    }
}
$APPLICATION->set_cookie("MY_BRONS", implode("###",$r), time()+60*60*24*7*4, "/","restoran.ru",false,true);
?>
<script>
    $(function(){
        alert("Спасибо! Мы подтвердим ваше бронирование буквально через пару минут.");
    })
</script>
<?endif;?>
<?
if ($_REQUEST["ID"]):
$APPLICATION->IncludeComponent("restoran:restoraunts.detail", "rest_detail", Array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $arIB["ID"],
	"ELEMENT_ID" => $_REQUEST["ID"],
	"ELEMENT_CODE" => $_REQUEST["RESTOURANT"],
	"CHECK_DATES" => "Y",
	"PROPERTY_CODE" => array(
		0 => "type",
		1 => "average_bill",
		2 => "kitchen",
		3 => "opening_hours",
		4 => "phone",
		5 => "administrative_distr",
		6 => "area",
		7 => "address",
		8 => "subway",
		9 => "number_of_rooms",
		10 => "music",
		11 => "clothes",
		12 => "proposals",
		13 => "credit_cards",
		14 => "discouncards",
		15 => "bankets",
		16 => "touristgroups_1",
		17 => "banketnayasluzhba",
		18 => "kolichestvochelovek",
		19 => "stoimostmenyu",
		20 => "max_check",
		21 => "entertainment",
		22 => "wi_fi",
		23 => "hrs_24",
		24 => "parking",
		25 => "features",
		26 => "out_city",
		27 => "min_check",
		28 => "site",
		29 => "children",
		30 => "ideal_place_for",
		31 => "offers",
		32 => "email",
		33 => "discounts",
		34 => "landmarks",
		35 => "map",
		36 => "RATIO",
		37 => "COMMENTS",
		38 => "okrugdel",
		39 => "kuhnyadostavki",
		40 => "viduslug",
		41 => "add_props",
		42 => "network",
		43 => "rest_group",
		44 => "breakfasts",
		45 => "business_lunch",
		46 => "branch",
		47 => "rent",
		48 => "my_alcohol",
		49 => "catering",
		50 => "food_delivery",
		51 => "proposals",
		52 => "d_tours",
	),
	"ADD_REVIEWS" => "Y",
	"REVIEWS_BLOG_ID" => "3",
	"SIMILAR_OUTPUT" => "Y",
	"SIMILAR_PROPERTIES" => array(
		0 => "kitchen",
		1 => "average_bill",
	),
	"IBLOCK_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "Y",
	"CACHE_TIME" => "3600075",
	"CACHE_GROUPS" => "N",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"USE_PERMISSIONS" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Страница",
	"PAGER_TEMPLATE" => "",
	"PAGER_SHOW_ALL" => "Y",
	"DISPLAY_DATE" => "Y",	// Выводить дату элемента
	"DISPLAY_NAME" => "Y",	// Выводить название элемента
	"DISPLAY_PICTURE" => "Y",	// Выводить детальное изображение
	"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
	"USE_SHARE" => "N",	// Отображать панель соц. закладок
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
    
?> 
<?
$arReviewsIB = getArIblock("reviews", CITY_ID);
global $arrFilter;
$arrFilter = array();
$arrFilter["PROPERTY_ELEMENT"] = $_REQUEST["ID"];
$APPLICATION->IncludeComponent(
    "restoran:catalog.list", "popup_reviews", Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "reviews",
        "IBLOCK_ID" => $arReviewsIB["ID"],
        "NEWS_COUNT" => "999",
        "SORT_BY1" => "created_date",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY", "DETAIL_PAGE_URL"),
        "PROPERTY_CODE" => array("ELEMENT", "minus", "plus", "photos", "video", "COMMENTS"),
        "CHECK_DATES" => "N",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600000",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "search_rest_list",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "URL" => $_REQUEST["url"],
    ), true
);
endif;
?>       
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>