<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Restoran.ru");
?>
<script type="text/javascript">
    var navigatorOn = '<?= $APPLICATION->get_cookie("COORDINATES") ?>';
    var sessionLat = '<?= $_SESSION['lat'] ?>';
    var sessionLon = '<?= $_SESSION['lon'] ?>';
    var coord = "<?= COption::GetOptionString("main", CITY_ID . "_center") ?>";
    var city_id = "<?= CITY_ID ?>";
    var page = 1;
    var ajax_load = 0;
    coord = coord.split(",");
    function success_a(position) {        
        $.ajax({
            type: "POST",
            url: "/ajax/ajax-coordinates.php",
            data: {
                lat:position.coords.latitude,
                lon:position.coords.longitude
            },
            success: function(data) {
                //location.reload();
//                $("#bubbling_text").text("Загружаем ближайшие рестораны...");                                
//                $( "#main" ).load("/" +" #ajax", {ajax:"Y"},function (data){                    
//                    $(".bubblingG").hide();
//                    history.pushState({foo: 'bar'}, $("#title").val(), "/");                                                        
//                    $("title").html($(data).find("#title").val());
//                });
            }
        });
    }

    function error_a()
    {
        //alert("Невозможно определить местоположение");
        //return false;
    }
    function get_location_a()
    {        
        if (navigatorOn != 'Y') {
            if (navigator.geolocation) {
               navigator.geolocation.getCurrentPosition(success_a, error_a);
            } else {
                alert("Ваш браузер не поддерживает\nопределение местоположения");
                return false;
            }
        } else {
            $(".bubblingG").hide(); 
            $("#bubbling_text").text("");
            //setTimeout("myMap.setCenter([sessionLat, sessionLon],15)", 500);
        }


    }    
    $(function(){
        //$(".bubblingG").show();
        //$("#bubbling_text").text("Определяем местоположение...");
       // get_location_a();        
        $(window).scroll(function(){            
            scroll_load();           
        });
    })
</script>
<?

if ($_REQUEST["arrFilter_pf"])
    {
        foreach ($_REQUEST["arrFilter_pf"] as $key=>&$fil)
        {
            if ($fil[0]==0)
                unset($_REQUEST["arrFilter_pf"][$key]);
            else {
                foreach ($fil as $key=>$f)
                {
                    if ($f==0)
                        unset($fil[$key]);
                }
            }
        }
    }
         
    if (!$_REQUEST["page"])
        $_REQUEST["page"] = 1;
    if (!$_REQUEST["PAGEN_1"])
        $_REQUEST["PAGEN_1"] = 1;
    $_REQUEST["by"] = "asc";
    $_REQUEST["pageRestSort"] = "popular";
    $_REQUEST["set_filter"] = "Y";
    global $arrFilter;
    echo "<input type='hidden' id='sort' value='popular' />";

    $arrFilter["!PROPERTY_no_mobile_VALUE"] = "Да";
    $APPLICATION->IncludeComponent("restoran:restoraunts.list", "main_rest", Array(
        "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID), // Код информационного блока
        "PARENT_SECTION_CODE" => "restaurants", // Код раздела
        "NEWS_COUNT" => ($_REQUEST["pageRestCnt"] ? $_REQUEST["pageRestCnt"] : "10"), // Количество ресторанов на странице
        "SORT_BY1" => "PROPERTY_rating_date",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "PROPERTY_stat_day",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "arrFilter", // Фильтр
        "PROPERTY_CODE" => array(// Свойства
            0 => "type",
            1 => "kitchen",
            2 => "average_bill",
            3 => "opening_hours",
            4 => "phone",
            5 => "address",
            6 => "subway",
            7 => "ratio",
        ),
        "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
        "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
        "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
        "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        "CACHE_TYPE" => "Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
        "CACHE_TIME" => "36000000001", // Время кеширования (сек.)
        "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "N", // Учитывать права доступа
        "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
        "SET_TITLE" => "Y", // Устанавливать заголовок страницы
        "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
        "PAGER_TITLE" => "Рестораны", // Название категорий
        "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
        "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
        "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
        "DISPLAY_DATE" => "Y", // Выводить дату элемента
        "DISPLAY_NAME" => "Y", // Выводить название элемента
        "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
            ), false
    );
?>
<?if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/index.php"||$APPLICATION->GetCurDir()=="/popular/"):?>
    <a class='btn filter'>Фильтры</a>
<?endif;?>
<div class='popup filter'>
<?
if ($_REQUEST["arrFilter_pf"])
    $a = "Y";
else
    $a = "";
$arIB = getArIblock("catalog", CITY_ID);
$APPLICATION->IncludeComponent("restoran:catalog.filter", "rests", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $arIB["ID"],
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => "",
	"PROPERTY_CODE" => array(
		0 => "subway",
		1 => "out_city",
		2 => "average_bill",
		3 => "kitchen",
		4 => "features",
	),
	"PRICE_CODE" => "",
	"CACHE_TYPE" => "Y",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "N",
	"CACHE_NOTES" => "mobile".$a.$APPLICATION->get_cookie("COORDINATES"),
	"LIST_HEIGHT" => "5",
	"TEXT_WIDTH" => "20",
	"NUMBER_WIDTH" => "5",
	"SAVE_IN_SESSION" => "N",
	"NO_NAME" => "Y"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
</div>
<?if ($_REQUEST["CITY_ID"]&&$_COOKIE["NEED_SELECT_CITY"]=="Y"):
                            unset($_COOKIE["NEED_SELECT_CITY"]);
                        endif?>
                        <?if ($_COOKIE["NEED_SELECT_CITY"]=="Y"):?>
                            <div class="popup review need_select_city">
                                <div class="inner">
                                    <div class="head">
                                        <h2>Выберите город</h2>
                                    </div>
                                    <div class="body">
                                        <br /><Br />
                                        <?
                                        $APPLICATION->IncludeComponent(
                                                    "restoran:city.selector",
                                                    "city_select_2014",
                                                    Array(
                                                        "IBLOCK_TYPE" => "catalog",
                                                        "IBLOCK_URL" => "",
                                                        "CACHE_TYPE" => "Y",
                                                        "CACHE_TIME" => "36000000000",
                                                        "CACHE_NOTES" => "new321223662438",
                                                        "CACHE_GROUPS" => "Y"
                                                    ),
                                                    false
                                                );
                                        ?>
                                    </div>
                                </div>
                                <div class="close"></div>
                            </div>
                            <script>
                                $(function(){
                                   $(".need_select_city").show(); 
                                });
                            </script>
                        <?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>