<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск ресторанов");
?>
<?
$oldDate = explode('-', $_REQUEST['form_date_33']);
if (count($oldDate) == 3) {
    $newDate = $oldDate[2] . '.' . $oldDate[1] . '.' . $oldDate[0];
    $_REQUEST['form_date_33'] = $newDate;
}
$APPLICATION->IncludeComponent(
    "restoran:form.result.new", "booking", Array(
        "SEF_MODE" => "Y",
        "WEB_FORM_ID" => "3",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "/detail.php?ID=".$_REQUEST["form_text_44"],
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "CACHE_TYPE" => "N",        
        "USE_CAPTCHA" => "Y",
        "VARIABLE_ALIASES" => Array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID"
        )
    ), false
);
?>
<script>
    $(function(){
        $(".menu").unbind("click");
        $(".logo").html("<h1>Бронирование</h1><h4>").attr("href","");
        //$(".logo h1").css("line-height",$('header').height()+"px");
        $(".app-bar-actions").remove();
        $(".menu").after($(".menu").clone().removeClass("menu").addClass("arrow_r"));//.removeClass("menu").addClass("arrow_r")));                
        $(".arrow_r").find("img").attr("src",$(".arrow_r img").attr("src").replace("menu.png","arrow_r.png"));
        $(".menu").hide();
        //$(".menu").addClass("arrow_r").removeClass("menu");
        $(".arrow_r").click(function(){
            history.go(-1);
        });
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>