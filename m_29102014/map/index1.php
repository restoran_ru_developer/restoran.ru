<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по карте");
?>
<style>
    .map {
padding: 0!important;
margin: 0!important;
width: 100%;
height: 100%;
}
</style>
<a href='#' class='locate'></a>
<!-- build:js scripts/main.min.js -->
	
	<script src="http://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
	<script type="text/javascript">
	 ymaps.ready(function () {
	    var map;
	    goToMe();
	    
	    function goToMe(){
	    	var map;
		    ymaps.geolocation.get().then(function (res) {
		        var mapContainer = $('#map'),
		            bounds = res.geoObjects.get(0).properties.get('boundedBy'),
		            mapState = ymaps.util.bounds.getCenterAndZoom(
		                bounds,
		                [mapContainer.width(), mapContainer.height()]
		            );
		        createMap(mapState);
		    }, function (e) {
		        createMap({
		            center: [59.955, 30.35],
		            zoom: 13,
		            controls: []
		        });
		    });
	    }

	    function createMap (state) {
	        map = new ymaps.Map('map', state);
	        	pointImgSrc = '/images/map_point.png'
	            	icon = {
	            		iconLayout: 'default#image',
		            iconImageHref: pointImgSrc,
		            iconImageSize: [15, 21],
		            iconImageOffset: [-3, -42]
		};
	        	var myPlacemark = new ymaps.Placemark([59.955, 30.35],{},icon);
	        	var myPlacemark2 = new ymaps.Placemark([59.959, 30.35],{},icon);
	        	var myPlacemark3 = new ymaps.Placemark([59.962, 30.359],{},icon);
	        	var myPlacemark4 = new ymaps.Placemark([59.954, 30.361],{},icon);
	        	var myPlacemark5 = new ymaps.Placemark([59.947, 30.359],{},icon);
		map.geoObjects.add(myPlacemark).add(myPlacemark2).add(myPlacemark3).add(myPlacemark4).add(myPlacemark5);
	    }
	});
	    </script>


        <!-- endbuild -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>