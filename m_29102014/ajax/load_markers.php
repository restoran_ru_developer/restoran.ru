<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if (check_bitrix_sessid()):
    $obCache = new CPHPCache; 
    $life_time = 60*60*24*7; 
    //$life_time = 0; 

    $cache_id = "map1332".CITY_ID.$_REQUEST["id"]; 

    if ($obCache->InitCache($life_time, $cache_id, "/")):
        // получаем закешированные переменные
        $vars = $obCache->GetVars();
        $markers = $vars["MARKERS"];
    else :
        if (CModule::IncludeModule("iblock")):
            $arRestIB = getArIblock("catalog", CITY_ID);
            if ($_REQUEST["arrFilter_pf"])
            {
                foreach($_REQUEST["arrFilter_pf"] as $key=>$ar)
                {               
                    $arrFilter["PROPERTY_".$key] = $ar;
                }
            }
            $arFilter = Array("ACTIVE"=>"Y","IBLOCK_ID"=>$arRestIB["ID"],"!PROPERTY_lat"=>false,"!PROPERTY_lon" => false);
            if ((int)$_REQUEST["id"])
            {
                $a = array("ID"=>(int)$_REQUEST["id"]);
                $arFilter = array_merge($a,$arFilter);
            }
            if (is_array($arrFilter))
            {
                $arFilter = array_merge($arFilter,$arrFilter);
            }
            $arFilter["!PROPERTY_no_mobile_VALUE"] = "Да";
            $res = CIBlockElement::GetList(Array(),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","CODE"));               
            while($ar = $res->Fetch())
            {                
                $map = array();
                $lon = array();
                $adr = array();
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"map"));
                while($ar_props = $db_props->Fetch())
                    $map[] = $ar_props["VALUE"];                                                
                $db_props = CIBlockElement::GetProperty($ar["IBLOCK_ID"], $ar["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
                while($ar_props = $db_props->Fetch())
                    $adr[] = $ar_props["VALUE"];
                $temp = array();
                foreach($map as $key=>$l)
                {
                    $temp = explode(",",$l);
                    $arResult[] = Array("id"=>$ar["ID"],"name"=>stripslashes($ar["NAME"]),"lat"=>$temp[0],"lon"=>$temp[1],"adres"=>stripslashes($adr[$key]),"url"=> "/detail.php?ID=".$ar["ID"]);            
                }
            }
            
        endif;
    endif;

    // начинаем буферизирование вывода
    if($obCache->StartDataCache()):
        if (count($arResult)>0)
        {   
            echo json_encode($arResult);
        }
        $obCache->EndDataCache(array(
            "SECTION_TITLE"    => $arResult
            )); 
    endif;   
endif;
?>