<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(!$_REQUEST["q"] || strlen(trim($_REQUEST["q"])) <= 0)
    return;

$APPLICATION->IncludeComponent(
    "restoran:mobile.search",
    (!$_REQUEST["page"])?"suggest":"suggest_link",
    Array(
        "COUNT" => 5
    )
);
?>