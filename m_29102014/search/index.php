<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск ресторанов");
?>
<div class='search'>
    <form id="search_form" action="/search/">
        <input id="restoran" autocomplete="off" placeholder='Поиск по названию' name='q' value="<?=$_REQUEST["q"]?>" type="text" />
        <input type="submit" value='Найти' />
        <div class="restoran_suggest"></div>   
    </form>
</div>
<script>
    $(function(){
        $("#restoran").keyup(function(){        
            if ($("#restoran").length&&$("#restoran").val().length>2)
            {
                if (null!=$(".restoran_suggest"))
                {
                    var a = "";
                    if (location.pathname.indexOf("/search/")>=0)
                        a = "search";
                    else
                        a = "";
                    $( ".restoran_suggest" ).load("/ajax/suggest.php", {"q":$("#restoran").val(),"sessid":$("#sessid").val(),"page":a}, function (data) {
                        if (data) {
                            $(".restoran_suggest").show();
                        }
                    });
                }
            }
        });
    });
</script>
<?
$APPLICATION->IncludeComponent(
        "restoran:mobile.search", "rest", Array(
        "COUNT" => 50,
        "SECTION"=>"restaurants",
        "CACHE_TYPE"=>"Y",
	"CACHE_TIME" => 3600000000
    )
);
?>
<?
if (!$_REQUEST["q"]):    
    $r = $APPLICATION->get_cookie("RECENTLY_VIEWED");
    $r = explode("###",$r);       
    if (is_array($r))
    {
        global $arrFilter;
        $arrFilter["ID"] = $r;        
        $APPLICATION->IncludeComponent("restoran:restoraunts.list", "main_rest", Array(    
            "IBLOCK_TYPE" => "catalog", // Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => ($_REQUEST["CITY_ID"] ? $_REQUEST["CITY_ID"] : CITY_ID), // Код информационного блока
            "PARENT_SECTION_CODE" => "restaurants", // Код раздела
            "NEWS_COUNT" => 3, // Количество ресторанов на странице
            "SORT_BY1" => "", // Поле для первой сортировки ресторанов
            "SORT_ORDER1" => "", // Направление для первой сортировки ресторанов
            "SORT_BY2" => "", // Поле для второй сортировки ресторанов
            "SORT_ORDER2" => "", // Направление для второй сортировки ресторанов
            "FILTER_NAME" => "arrFilter", // Фильтр
            "PROPERTY_CODE" => array(// Свойства
                0 => "type",
                1 => "kitchen",
                2 => "average_bill",
                3 => "opening_hours",
                4 => "phone",
                5 => "address",
                6 => "subway",
                7 => "ratio",
            ),
            "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "PREVIEW_PICTURE_MAX_WIDTH" => "231", // максимальная ширина превью изображения
            "PREVIEW_PICTURE_MAX_HEIGHT" => "163", // максимальная высота превью изображения
            "PREVIEW_PICTURE_ADDITIONAL_PROP_CODE" => "photos", // код доп фото
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "Y",//($_REQUEST["pageRestSort"]=="distance")?"N":"Y",
            "CACHE_TIME" => "36000010", // Время кеширования (сек.)
            "CACHE_FILTER" => "Y", // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N", // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "150", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
            "SET_TITLE" => "Y", // Устанавливать заголовок страницы
            "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "Y", // Выводить под списком
            "PAGER_TITLE" => "Рестораны", // Название категорий
            "PAGER_SHOW_ALWAYS" => "Y", // Выводить всегда
            "PAGER_TEMPLATE" => "rest_list_arrows", // Название шаблона
            "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "Y", // Показывать ссылку "Все"
            "DISPLAY_DATE" => "Y", // Выводить дату элемента
            "DISPLAY_NAME" => "Y", // Выводить название элемента
            "DISPLAY_PICTURE" => "Y", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
            "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
                ), false
        );
    }
endif;
?>
<?//if ($APPLICATION->GetCurPage()=="/"||$APPLICATION->GetCurPage()=="/index.php"||$APPLICATION->GetCurDir()=="/popular/"):?>
    <a class='btn filter'>Фильтры</a>
<?//endif;?>
<div class='popup filter'>
<?
if ($_REQUEST["arrFilter_pf"])
    $a = "Y";
else
    $a = "";
$arIB = getArIblock("catalog", CITY_ID);
$APPLICATION->IncludeComponent("restoran:catalog.filter", "rests", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => $arIB["ID"],
	"FILTER_NAME" => "arrFilter",
	"FIELD_CODE" => "",
	"PROPERTY_CODE" => array(
		0 => "subway",
		1 => "out_city",
		2 => "average_bill",
		3 => "kitchen",
		4 => "features",
	),
	"PRICE_CODE" => "",
	"CACHE_TYPE" => "Y",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "N",
	"CACHE_NOTES" => "mobile".$a.$APPLICATION->get_cookie("COORDINATES"),
	"LIST_HEIGHT" => "5",
	"TEXT_WIDTH" => "20",
	"NUMBER_WIDTH" => "5",
	"SAVE_IN_SESSION" => "N",
	"NO_NAME" => "Y"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>