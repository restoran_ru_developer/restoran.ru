<?
$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>
<?
//if(!$USER->IsAdmin())
  //  return false;

CModule::IncludeModule("iblock");

//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex_upload_msk.xml", '', LOCK_EX);

$str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$str .= "<companies xmlns:xi='http://www.w3.org/2001/XInclude' version='2.1'>\n";

//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);

/*$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
                "ID" => Array(11,12)
	),
	false
);
while($arIblock = $rsIblock->Fetch()) {*/
	$rsRest = CIBlockElement::GetList(
		Array("NAME" => "ASC"),
		Array(
			//"ACTIVE" => "Y",
			"IBLOCK_ID" => Array(11,12),
			"!SECTION_ID" => Array("226", "44170", "44509", "44510"),
		)
                /*false,
                Array("nTopCount"=>100)*/
	);	
	while($arRest = $rsRest->GetNext()) {            
                $tmpDate = strtotime($arRest["TIMESTAMP_X"])*1000;

                $str .= "\t<company>\n";
                        $str .= "\t\t<id>".$arRest["ID"]."</id>\n";
                        if ($arRest["IBLOCK_ID"]==11)
                            $str .= "\t\t<region>Москва</region>\n";
                        elseif ($arRest["IBLOCK_ID"]==12)
                            $str .= "\t\t<region>Санкт-Петербург</region>\n";
                        if ($arRest["IBLOCK_SECTION_ID"]==32||$arRest["IBLOCK_SECTION_ID"]==103)
                            $str .= "\t\t<section>Рестораны</section>\n";
                        if ($arRest["IBLOCK_SECTION_ID"]==33||$arRest["IBLOCK_SECTION_ID"]==104)
                            $str .= "\t\t<section>Банкетные залы</section>\n";
                        $str .= "\t\t<name>".htmlspecialchars($arRest["NAME"])."</name>\n";
                        if ($arRest["TAGS"])
                            $str .= "\t\t<alias>".htmlspecialchars($arRest["TAGS"])."</alias>\n";
                        $str .= "\t\t<active>".$arRest["ACTIVE"]."</active>\n";                        
                        $rsProp = CIBlockElement::GetProperty(
                                        $arRest["IBLOCK_ID"],
                                        $arRest["ID"],
                                        Array(),
                                        Array(
                                                "CODE" => "sleeping_rest"
                                        )
                        );
                        if($arProp = $rsProp->Fetch())
                        {
                                if($arProp["VALUE_ENUM"]=="Да")	
                                    $str .= "\t\t<sleep>Y</sleep>\n";
                                else
                                    $str .= "\t\t<sleep>N</sleep>\n";
                        }
                        $str .= "\t\t<url>http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."</url>\n";
                $str .= "\t</company>\n";                                       
	}
        //sleep(5);
//}
$str .= "</companies>";
echo $str;
header('Content-type: application/xml');
header("Content-Disposition: attachment; filename=seo_xml.xml");
//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex_upload_msk.xml", $str, FILE_APPEND | LOCK_EX);
?>
