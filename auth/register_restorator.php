<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<script>
    $(document).ready(function(){
        $(".tabs").next();
    })
</script>
<div id="content">
    <div id="tabs_block2">
        <ul class="nav nav-tabs">
            <li class="">
                <a href="/auth/register.php" onclick="location.href=$(this).attr('href')">                    
                    Зарегистрироваться как пользователь                    
                </a>
            </li>
            <li class="active">
                <a href="#restorator">
                    Для ресторатора
                </a>
            </li>
        </ul>
        <!-- tab "panes" -->
        <div class="tab-content">
            <div class="tab-pane">
            </div>
            <div class="tab-pane active">
<!--                <ul class="nav nav-pills nav-justified poster_tabs" >-->
<!--                    <li><a href="#a1" data-toggle="tab" style="font-size:14px;">Добавление <br />ресторанов</a></li>-->
<!--                    --><?//if (CITY_ID!="kld"):?>
<!--                        <li><a href="#a2" data-toggle="tab" style="font-size:14px;">Добавление <br />доставки, еды, фирмы </a></li>                        -->
<!--                    --><?//endif;?>
<!--                    <li><a href="#a3" data-toggle="tab" style="font-size:14px;">Рекламные места,<Br /> баннеры</a></li>                    -->
<!--                </ul>-->
                <div class="tab-content">
                    <div class="tab-pane active" id="a1">
                        <?
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc_add_rest_".CITY_ID,
                                        "AREA_FILE_RECURSIVE" => "N",
                                        "EDIT_TEMPLATE" => ""
                                )
                            );
                        //}
                        ?>
                    </div>
                    <?if (CITY_ID!="kld"):?>
                        <div class="tab-pane" id="a2">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc_add_food",
                                        "AREA_FILE_RECURSIVE" => "N",
                                        "EDIT_TEMPLATE" => ""
                                )
                            );?>
                        </div>
                    <?endif;?>
                    <div class="tab-pane" id="a3">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc_add_baner",
                                    "AREA_FILE_RECURSIVE" => "N",
                                    "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    </div>
                </div>                               
            <div class="clearfix"></div>
                <?$APPLICATION->IncludeComponent("restoran:main.register", "register_form_restorator", Array(
                        "USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
                        "SHOW_FIELDS" => array(	// Поля, которые показывать в форме
                                0 => "LAST_NAME",
                                1 => "NAME",
                                2 => "EMAIL",                                
                                3 => "PERSONAL_PHONE",
                                4 => "PERSONAL_NOTES",
                                5 => "PERSONAL_GENDER",
                                6 => "PERSONAL_BIRTHDAY",
                                7 => "WORK_COMPANY",
                                8 => "WORK_POSITION",                                
                                9 => "PERSONAL_PROFESSION",
                                10 => "PERSONAL_CITY",
                                11 => "PERSONAL_PAGER",
                                12 => "WORK_PROFILE"
                        ),
                        "REQUIRED_FIELDS" => array(	// Поля, обязательные для заполнения
                                0 => "LAST_NAME",
                                1 => "NAME",
                                2 => "EMAIL",
                                3 => "PERSONAL_PROFESSION",
                                4 => "PERSONAL_PHONE",
                                5 => "WORK_PROFILE"
                        ),
                        "AUTH" => "N",	// Автоматически авторизовать пользователей
                        "USE_BACKURL" => "N",	// Отправлять пользователя по обратной ссылке, если она есть
                        "SUCCESS_PAGE" => "/auth/register_confirm.php",	// Страница окончания регистрации
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "USER_PROPERTY" => array(	// Показывать доп. свойства
                                0 => "UF_USER_AGREEMENT",
                                1 => "UF_KITCHEN"
                        )
                        ),
                        false
                );?>
            </div>    
        </div> 
        <div class="clear"></div>
</div>    
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>