<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<div id="content">
    <div id="tabs_block2">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#user" onclick="$('.register_form #group_u').val('')">                    
                    Зарегистрироваться как пользователь                    
                </a>
            </li>
            <li>
                <a href="/auth/register_restorator.php#restorator" onclick="location.href=$(this).attr('href')">
                    Для ресторатора
                </a>
            </li>
        </ul>
        <!-- tab "panes" -->
        <div class="tab-content">
            <div class="tab-pane active">
                <ul class="nav nav-pills nav-justified poster_tabs" >
                    <li><a href="#a1" data-toggle="tab" style="font-size:14px;">Бронирование<br />столиков online</a></li>
                    <li><a href="#a2" data-toggle="tab" style="font-size:14px;">Ведение<br />блога </a></li>
                    <li><a href="#a3" data-toggle="tab" style="font-size:14px;">Рецепты<br />и диеты</a></li>
                    <li><a href="#a4" data-toggle="tab" style="font-size:14px;">Добавление<br /> отзывов</a></li>                   
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="a1">
                        <p>Забронировать столик или банкет в любом ресторане города – это очень просто! Вы можете оформить бронь с главной страницы сайта, можете из каталога, а можете и со страницы понравившегося ресторана! Просто нажмите кнопку, и через 5 минут оператор сообщит подробности вашего резерва. А наши зарегистрированные пользователи, помимо приятно проведённого вечера в любимом ресторане, ещё получают Рестики, которые потом можно обменять на <a href="/shop/" taget="_blank">классные подарки</a>, и имеют возможность следить за бронью в личном кабинете. </p>
                        <p>Нет никакой возможности заполнить форму или связаться с нами? Просто кликните на «Позвоните мне», и наши операторы немедленно свяжутся с вами! </p>
                       <br/>
                    </div>
                    <div class="tab-pane"  id="a2">
                        <p>Знаете много интересного про еду? Любите рестораны? Много путешествуете и можете рассказать про тайный ингредиент священного напитка индейцев танана? Поделиться рецептом, полностью восстановленным по одной из книг Джорджа Мартина? Может, рецептом простого, но безумно вкусного бутерброда? Да мало ли интересного может храниться в вашей голове, книгах на ваших полках или в интернете! Делитесь этим на страницах нашего сайта, общайтесь и, как всегда, <a href="/shop/" taget="_blank">получайте Рестики</a>!</p> <br/>
                    </div>
                    <div class="tab-pane"  id="a3">
                        <p>Огромный кулинарный раздел нашего сайта, постоянно пополняемый редакцией и самими пользователями, ни за что не даст умереть голодной смертью! Даже если вы блюдёте фигуру или у вас строгая спортивная диета, а может, просто любите овсянку и уже попробовали её приготовить во всех возможных вариантах, у нас всегда есть что-то новое и интересное, что можно сохранить в своём Избранном, чтобы всегда иметь под рукой. </p>
                        <p>Присоединяйтесь! Делитесь своими кулинарными тайнами и убойными рецептами, рассказывайте, как достигли форм Афродиты или победили с помощью диеты недуг!</p> <br/>
                    </div>
                    <div class="tab-pane"  id="a4">
                        <p>Всё было вкусно, красиво, душевно и осталась масса приятных впечатлений от отдыха в ресторане? Не держите это в себе, не будьте жадиной! Хорошим нужно делиться! Пусть и другие узнают, как здорово вы провели время! А если вдруг случилась досадная неприятность, вам испортили отдых, про это тоже стоит рассказать. И даже добавить фото или видео! Во-первых, это как сеанс групповой терапии – ведь так много людей могут вас поддержать, а во-вторых, дайте ресторану шанс исправиться. Может, это была случайность, и они очень дорожат вашим мнением и сделают всё, чтобы поправить случившиеся. Пишите, делитесь, радуйтесь вместе с нами, жалуйтесь нам, а мы за каждое ваше ценное слово на нашем сайте будем, как всегда, расставаться с <a href="/shop/" taget="_blank">рестиками и подарками</a>. </p><br/>
                    </div>
                </div>
                            
                <div class="clear"></div>                                        
                <div class="grey_block pull-left" style="width:348px; padding-top:10px;padding-bottom:10px;">
                    <h2 class="left" style="margin-top:0px;margin-bottom:5px;">Быстрая регистрация</h2>                    
                    <div class="clear"></div>
                    <div style="line-height:18px;margin-bottom:5px;">Если вы зарегистрированы на одном из этих сайтов, вы можете пройти быструю регистрацию. </div>                    
                    <div style="float: left; background: #FFF; padding:5px; width:135px;">
                        <?$APPLICATION->IncludeComponent("restoran:user.vkontakte_auth", ".default", array("PREFIX"=>"a"
                            ),
                            false
                        );?>
            </div>
                    <div style="float: left; margin-left: 20px;  background: #FFF; padding:5px;">
                        <?$APPLICATION->IncludeComponent("restoran:user.facebook_auth", ".default", array(
                            ),
                            false
                        );?>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="pull-right" >
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:advertising.banner",
                                "",
                                Array(
                                        "TYPE" => "on_register",
                                        "NOINDEX" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                ),
                        false
                        );?>
                    </div>
                <div class="clear"></div> 
                <?$APPLICATION->IncludeComponent("restoran:main.register", "register_form", Array(
                        "USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
                        "SHOW_FIELDS" => array(	// Поля, которые показывать в форме
                                0 => "LAST_NAME",
                                1 => "NAME",
                                2 => "EMAIL",                                
                                3 => "PERSONAL_PHONE",
                                4 => "PERSONAL_NOTES",
                                5 => "PERSONAL_GENDER",
                                6 => "PERSONAL_BIRTHDAY",
                                7 => "WORK_COMPANY",
                                8 => "WORK_POSITION",
                                9 => "WORK_NOTES",
                                10 => "PERSONAL_PROFESSION",
                                11 => "PERSONAL_CITY",
                                12 => "PERSONAL_PAGER"                                
                            
                        ),
                        "REQUIRED_FIELDS" => array(	// Поля, обязательные для заполнения
//                                0 => "LAST_NAME",
//                                1 => "NAME",
                                0 => "EMAIL",
                                1 => "PERSONAL_PROFESSION",
                        ),
                        "AUTH" => "N",	// Автоматически авторизовать пользователей
                        "USE_BACKURL" => "N",	// Отправлять пользователя по обратной ссылке, если она есть
                        "SUCCESS_PAGE" => "/auth/register_confirm.php",	// Страница окончания регистрации
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "USER_PROPERTY" => array(	// Показывать доп. свойства
                                0 => "UF_USER_AGREEMENT",
                                1 => "UF_KITCHEN"
                        )
                        ),
                        false
                );?>
            </div>
            <div class="tab-pane" style="width:auto; min-height:52px;">
                
            </div>            
            <div class="clear"></div>
        </div>
</div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>