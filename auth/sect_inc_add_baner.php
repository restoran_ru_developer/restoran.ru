<?if (CITY_ID!="kld"):?>
<!--<div class="plans">
    <div class="plan-1 " align="center">        
        <p class="plan-desc">960*90 px<br/><i>вверху страницы</i></p>
        <img src="/tpl/images/baners/baner1.png" width="180" style="margin-right:0px"><br />
        <p style="margin-bottom:0px">Анимация: <span class="plan-desc">550</span> <span class="rouble">e</span></p>
        <i>за 1000 показов на всех страницах</i>
        <div class="dotted"></div>
        <p style="margin-bottom:0px">Статика: <span class="plan-desc">100 000</span> <span class="rouble">e</span></p>
        <i>за 1 неделю показов на всех страницах</i>
    </div>
    <div class="plan-2 " align="center">
        <p class="plan-desc">240*400 px<br/><i>справа</i></p>
        <img src="/tpl/images/baners/baner2.png" width="180" style="margin-right:0px"><br />
        <p style="margin-bottom:0px">Анимация: <span class="plan-desc">550</span> <span class="rouble">e</span></p>
        <i>за 1000 показов на всех страницах</i>
        <div class="dotted"></div>
        <p style="margin-bottom:0px">Статика: <span class="plan-desc">100 000</span> <span class="rouble">e</span></p>
        <i>за 1 неделю показов на всех страницах</i>
    </div>
    <div class="plan-3 " align="center">
        <p class="plan-desc">240*400 px<br/><i>справа внизу</i></p>
        <img src="/tpl/images/baners/baner3.png" width="180" style="margin-right:0px"><br />
        <p style="margin-bottom:0px">Анимация: <span class="plan-desc">550</span> <span class="rouble">e</span></p>
        <i>за 1000 показов на всех страницах</i>
        <div class="dotted"></div>
        <p style="margin-bottom:0px">Статика: <span class="plan-desc">100 000</span> <span class="rouble">e</span></p>
        <i>за 1 неделю показов на всех страницах</i>
    </div>
    <div class="plan-4 end" align="center" >
        <p class="plan-desc">728*90 px<br/><i>внизу страницы</i></p>
        <img src="/tpl/images/baners/baner4.png" width="180" style="margin-right:0px"><br />
        <p style="margin-bottom:0px">Анимация: <span class="plan-desc">550</span> <span class="rouble">e</span></p>
        <i>за 1000 показов на всех страницах</i>
        <div class="dotted"></div>
        <p style="margin-bottom:0px">Статика: <span class="plan-desc">100 000</span> <span class="rouble">e</span></p>
        <i>за 1 неделю показов на всех страницах</i>
    </div>
<div class="clear"></div>                        
</div>-->
<?
$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"banners_for_reg",
	Array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "sale",
		"IBLOCK_ID" => "155",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array("PREVIEW_PICTURE"),
		"PROPERTY_CODE" => array("POSITION", "SIZE", "PRICE_ANIMATION", "PRICE_STATIC"),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);
?>
<?else:?>
    <p>Растяжка баннер вверху страницы 960х90px (статика) — 2 500 рублей неделя</p>
    <p>Баннер справа вверху 240х400px (ротация) — 1 800 рублей неделя </p>
    <p>Баннер справа внизу 240х400px (ротация) — 1 300 рублей неделя </p>
    <p>Растяжка баннер внизу страницы 960х90px (статика) — 1 500 рублей неделя </p>
<?endif;?>