<?
define("LANG", "ru"); 

define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

header ("Content-Type:text/xml");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$_REQUEST["ID"] = intval(str_replace("#", "", $_REQUEST["ID"]));
if($_REQUEST["ID"]<1) die("<error>не указан ресторан</error>");

$obCache = new CPHPCache; 
$life_time = 24*60*60;  
$cache_id = "restoran_".$_REQUEST["ID"];

if($obCache->InitCache($life_time, $cache_id, "/mobile_app/".$cache_id."/")){
   // получаем закешированные переменные
   $vars = $obCache->GetVars();
   $EXIT = $vars["EXIT"];
}else{
	CModule::IncludeModule("iblock");
	
	$EXIT = '<?xml version="1.0" encoding="UTF-8"?>';
	$EXIT .= '<restoran>';
	
	$res = CIBlockElement::GetByID($_REQUEST["ID"]);
	if($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arFields["PROPERTIES"] = $ob->GetProperties();	  
		
		//echo '<pre>';
		//var_dump($arFields);
		//echo '</pre>';
		
		if($arFields['PREVIEW_PICTURE']!=""){
			$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>137, 'height'=>90), BX_RESIZE_IMAGE_EXACT, true); 
			$arFields['PREVIEW_PICTURE'] = "http://".$_SERVER["SERVER_NAME"].$file["src"];
		}
		
	
		////Очищаем адреса
		$arFields["PROPERTIES"]["address"]["VALUE"][0] = str_replace("Москва, ", "", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		$arFields["PROPERTIES"]["address"]["VALUE"][0] = str_replace("Санкт-Петербург, ", "", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		
		$tar = explode(";", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		if(count($tar)>1) $arFields["PROPERTIES"]["address"]["VALUE"][0]=$tar[0]; 
		///
		
		//subway
		
		//kitchen
		
		//ideal_place_for
		
		//photos
		if(count($arFields["PROPERTIES"]["photos"]["VALUE"])>1){
			$PHOTOS='';
			foreach($arFields["PROPERTIES"]["photos"]["VALUE"] as $pid){
				$file = CFile::ResizeImageGet($pid, array('width'=>47, 'height'=>40), BX_RESIZE_IMAGE_EXACT, true); 
				$PHOTOS.='<small>http://'.$_SERVER["SERVER_NAME"].$file["src"].'</small>';
			}
		}
		
  		$EXIT .= '<id>'.$arFields["ID"].'</id>';
  		$EXIT .= '<name>'.$arFields["NAME"].'</name>';
  		$EXIT .= '<address>'.$arFields["PROPERTIES"]["address"]["VALUE"][0].'</address>';
  		$EXIT .= '<stars>'.rand(0, 5).'</stars>';
  		$EXIT .= '<main_pic>'.$arFields["PREVIEW_PICTURE"].'</main_pic>';
  		$EXIT .= '<metro>'.$arFields["PROPERTIES"]["subway"]["VALUE"][0].'</metro>';
  		$EXIT .= '<phone>'.$arFields["PROPERTIES"]["phone"]["VALUE"][0].'</phone>';
  		$EXIT .= '<map>'.$arFields["PROPERTIES"]["map"]["VALUE"][0].'</map>';
  		$EXIT .= '<photos>'.$PHOTOS.'</photos>';
	}
	
	$EXIT .='</restoran>';
	
	if($obCache->StartDataCache()){
		$obCache->EndDataCache(array("EXIT"=> $EXIT)); 
	}
}

echo $EXIT

?>