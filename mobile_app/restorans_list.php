<?
define("LANG", "ru"); 

define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

header ("Content-Type:text/xml");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//11 - Москва
//12 - Спб

$CITY= 11;

if($_REQUEST["page_size"]=="") $_REQUEST["page_size"]=25;
if($_REQUEST["page_number"]=="") $_REQUEST["page_number"]=1;

$obCache = new CPHPCache; 
$life_time = 24*60*60;  
$cache_id = "restorans_".$CITY."_".$_REQUEST["page_number"];

if($obCache->InitCache($life_time, $cache_id, "/mobile_app/".$cache_id."/")){
   // получаем закешированные переменные
   $vars = $obCache->GetVars();
   $EXIT = $vars["EXIT"];
}else{
	
	CModule::IncludeModule("iblock");
	
	$EXIT = '<?xml version="1.0" encoding="UTF-8"?>';
	$EXIT .= '<restorans>';
	
	$arFilter = Array("IBLOCK_ID"=>$CITY, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, Array("nPageSize"=>$_REQUEST["page_size"], "iNumPage"=>$_REQUEST["page_number"]), false);
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arFields["PROPERTIES"] = $ob->GetProperties();	  
		
		if($arFields['PREVIEW_PICTURE']!=""){
			$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>47, 'height'=>40), BX_RESIZE_IMAGE_EXACT, true); 
			$arFields['PREVIEW_PICTURE'] = "http://".$_SERVER["SERVER_NAME"].$file["src"];
		}
		
	
		////Очищаем адреса
		$arFields["PROPERTIES"]["address"]["VALUE"][0] = str_replace("Москва, ", "", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		$tar = explode(";", $arFields["PROPERTIES"]["address"]["VALUE"][0]);
		if(count($tar)>1) $arFields["PROPERTIES"]["address"]["VALUE"][0]=$tar[0]; 
		///
		
		$arFields["NAME"] = str_replace("&", "&amp;", $arFields["NAME"]);
		
		$maps = explode(",", $arFields["PROPERTIES"]["map"]["VALUE"]);
		
		
		$EXIT .= '<restoran>';
  		$EXIT .= '<id>'.$arFields["ID"].'</id>';
  		$EXIT .= '<name>'.$arFields["NAME"].'</name>';
  		$EXIT .= '<address>'.$arFields["PROPERTIES"]["address"]["VALUE"][0].'</address>';
  		$EXIT .= '<stars>'.rand(0, 5).'</stars>';
  		$EXIT .= '<pic>'.$arFields["PREVIEW_PICTURE"].'</pic>';
  		$EXIT .= '<lat>'.$maps[0].'</lat>';
  		$EXIT .= '<lon>'.$maps[1].'</lon>';
  		$EXIT .= '<map>'.$arFields["PROPERTIES"]["map"]["VALUE"].'</map>';
  		$EXIT .= '</restoran>';
		
		
		
	}
	
	$EXIT .='</restorans>';
	
	if($obCache->StartDataCache()){
		$obCache->EndDataCache(array("EXIT"=> $EXIT)); 
	}
}

echo $EXIT

?>