<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<script src="javascripts/components/handlebars.js/handlebars.runtime-1.0.0-rc.1.js"></script>
  <script src="javascripts/templates/photo-list.tmpl.js"></script>
  <script src='javascripts/app.js' type='text/javascript' charset='utf-8'></script>
  <link rel='stylesheet' href='stylesheets/application.css' type='text/css' media='screen'>
  <div id="content">
    <div style="width:980px;">
        <a href="https://www.facebook.com/cafefatcat" target="_blank"><img src="images/logo.png"></a>
        <div style="float:right; right:0; padding:35px; font-size:13px; width:430px; height:190px; background:url(images/text.png) top left no-repeat;">
        Ресторан.ру и уютнейшее кафе Fat Cat объявляют конкурс!<br>
        Призы – отличные! Получить их просто!<br>
        Запости фото своего толстого котика с тегом <strong>#restoranfatcat</strong> в Instagram, призывай друзей ставить лайки, кто наберет больше всех, получит один из трех классных призов.<br>
        <strong>1 место</strong> - Участие в пьяном уроке с ужином + дисконтная карта 15%; <strong>2 место</strong> - Пицца Fat Cat + дисконтная карта 15%; <strong>3 место</strong> - Набор гурмана Fat Cat (фартук, магниты)+ дисконтная карта 10%!<br>
        Объявление победителей 02.04.2013<br>
        Удачи вам и вашим котикам!
        </div>
                <div class="loading">
            <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01">
            </div>
            <div class="f_circleG" id="frotateG_02">
            </div>
            <div class="f_circleG" id="frotateG_03">
            </div>
            <div class="f_circleG" id="frotateG_04">
            </div>
            <div class="f_circleG" id="frotateG_05">
            </div>
            <div class="f_circleG" id="frotateG_06">
            </div>
            <div class="f_circleG" id="frotateG_07">
            </div>
            <div class="f_circleG" id="frotateG_08">
            </div>
            </div>
            </div>
            <div id='photos-wrap'>
            </div>
            <div class='paginate'>
            <a class='button more'  style='display:none;' data-max-tag-id='' href='#'>Показать еще...</a>
            </div>
        <div class="restoran_text bl" style="background:#fff; padding:20px; padding-bottom:60px; box-shadow:0px 1px 10px #000">
        <div class="left article_rest"><a href="/msk/detailed/restaurants/fat_cat/"><img class="indent" src="http://www.restoran.ru/upload/resize_cache/iblock/5c0/395_298_2dc1b6dd45ef539ffbeb914c32bb88cf5/72909_483687425027634_796336618_n.jpg" width="148"></a><br>
                        <a class="font14" href="/msk/detailed/restaurants/fat_cat/">Fat Cat</a>,<br>
                        <p></p><b>Кухня:</b> Итальянская, Средиземноморская, Американская<br><b>Средний счет :</b> 1000-1500р.<br><p></p> </div>
        <div class="right" style="width:775px;"><h1>Fat Cat</h1><p>Москва - город одиноких людей, так нуждающихся в тепле и ласке. Fat Cat – новый, по-настоящему семейный ресторан, теплый и простой по атмосфере, готов согреть своим теплом тех, кому в эти морозы хочется по-настоящему домашнего уюта. </p>
        <p>Start-up’ом ресторана занимается Женя Юркина (известная по запуску Uilliam’s, «Лемончелло», перезапуску секретного «Газгольдера» и др.), она стремится к тому, чтобы и Москве можно было попробовать самые вкусные рецепты из французских и итальянских деревень и модных мегаполисов - хипстерского Бруклина и романтичного Ноттинг Хилла.</p>

        <p>А название ресторана пришло не только из абстрактной любви к котам. В Орландо существует маленькая семейная гастрономическая лавка, в которой делают фантастические соусы под маркой fat cat. Так в московском ресторане появились их карибское карри fat cat, которое подают с нежнейшим ростбифом (435 руб), сладко-острый пири-пири соус, который восхитителен к запеченному цыпленку (450 руб). А жгуче-красный, похожий на аджику, соус Гуахильо играет не последнюю роль в фирменном бургере из баранины с хумусом, тхиной и печеной паприкой (575 руб).</p>

        <p>Вот так и сложилась гастрономическая концепция заведения: «Наша бабушка из Прованса переехала в Бруклин и захватила с собой любимого кота».</p>

        <p>Шеф-повар «Толстого Кота» - Сергей Сутугин, ученик и в прошлом правая рука Нино Грациано («Семифредо») создал нетипичную для ресторана не в центре карту блюд – гурманскую!</p>
        </div>
        <div class="clear"></div>
        <hr>
        <br>
        <h1>Пьяные уроки</h1>
        <p>Разбираться в еде и вине, понимать и говорить по-французски и итальянски! Думается, по выходным, хотя м.б. и на буднях в Fat Cat стартует новое полезное развлечение: гастрономический французский/итальянский: обедать/ужинать= интеллигентно напиваться в кампании преподавателя французского и/или итальянского языков. В простой, непринужденной атмосфере мы познакомимся с достойными внимания регионами, узнаем какие винные хозяйства стоит посетить, что попробовать и как объясниться, чтобы при этом не выглядеть рашн деревяшн, а вполне себе подкованным гурманом, космополитом… Никакой скучной грамматики, полезные фразы и речевые обороты, чуточку произношения, и много самой полезной лексики! Цель уроков, чтобы путешествуя, вы знали, куда едете, что едите и пьете - словом, смогли поддержать разговор с аборигеном, будь то продавец сыров на городской ярмарке или подошедший к вашему столику повар мишленовского ресторана, в котором вы только что упоительно отужинали!</p>
        </div>
        <div class="clear"></div>
    </div>    
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>