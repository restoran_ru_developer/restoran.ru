<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Основные события на сайте");
$APPLICATION->SetPageProperty("keywords", "события на сайте");
$APPLICATION->SetPageProperty("title", "Ресторан.ру - основные события на сайте");
?>
<div class="content">
    <div class="block">
        <div class="left-side">
            <h1 class="index-h" ><?=$APPLICATION->ShowTitle("h1")?></h1>
            <div class="clearfix"></div>
            <?
//            $ar1 = getArIblock('news', CITY_ID);
//            $ar3 = getArIblock('catalog', CITY_ID);
//            $ar4 = getArIblock('overviews', CITY_ID);
//            $ar5 = getArIblock('blogs', CITY_ID);


            $ar1 = getArIblock('news', CITY_ID);
            $ar2 = getArIblock('afisha', CITY_ID);
            $ar3 = getArIblock('catalog', CITY_ID);
            $ar4 = getArIblock('overviews', CITY_ID);
            $ar5 = getArIblock('blogs', CITY_ID);
//
//            $str = CIBlockElement::SubQuery("ID", array(
//                "IBLOCK_ID" => $ar2["ID"],
//                "PROPERTY_SHOW_EVENT_IN_INDEX_NEWS_VALUE" => "Y",
//            ));
//FirePHP::getInstance()->info($str);

            //            $GLOBALS['all_news_filter'] = array("PROPERTY_SHOW_EVENT_IN_INDEX_NEWS_VALUE" => "Y");
            $GLOBALS['all_news_filter'] = array("!PROPERTY_sleeping_rest_VALUE" => "Да","PROPERTY_NETWORK_REST" => false,
                "!ID" => CIBlockElement::SubQuery("ID", array(
                    "IBLOCK_ID" => $ar2["ID"],
                    "!PROPERTY_SHOW_EVENT_IN_INDEX_NEWS_VALUE" => "Y",
                ))
            );

            $APPLICATION->IncludeComponent(
                "restoran:catalog.list_multi",
                "",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],145),//
                    "NEWS_COUNT" => 100,
                    "SORT_BY1" => 'created',
                    "SORT_ORDER1" => 'DESC',
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "all_news_filter",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array(),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y-H:i",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => $USER->IsAdmin()?"N":"Y",//Y
                    "CACHE_TIME" => "36000004",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                ),
                false
            );?>
        </div>
        <div class="right-side">
        <?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
        "TYPE" => "right_2_main_page",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
    ),
    false
);?>
            <div class="title">Популярные</div>
            <?
$arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
$arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
$APPLICATION->IncludeComponent(
    "restoran:catalog.list",
    "recomended",
    Array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => $arRestIB["ID"],
        "NEWS_COUNT" => "5",
        "SORT_BY1" => "PROPERTY_rating_date",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "PROPERTY_stat_day",
        "SORT_ORDER2" => "DESC",
        "SORT_BY3" => "NAME",
        "SORT_ORDER3" => "ASC",
        "FILTER_NAME" => "arPFilter",
        "FIELD_CODE" => array("CREATED_BY"),
        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "120",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "REST_PROPS"=>"Y"
    ),
    false
);?>
            <div class="title">Рекомендуем</div>
            <?
$arRestIB = getArIblock("catalog", CITY_ID);
//$arrFilterSim["PROPERTY_RECOMENDED_VALUE"] = "Да";
$arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
$arPFilter["!PREVIEW_PICTURE"] = false;
$arPFilter["!PROPERTY_restoran_ratio"] = false;
?>
<?
$APPLICATION->IncludeComponent(
    "restoran:catalog.list",
    "recomended",
    Array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => $arRestIB["ID"],
        "NEWS_COUNT" => "3",
        "SORT_BY1" => "PROPERTY_restoran_ratio",
        "SORT_ORDER1" => "asc,nulls",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arPFilter",
        "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
        "PROPERTY_CODE" => array("COMMENTS","RATIO"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "120",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "10",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "A" => "recomended",
        "REST_PROPS"=>"Y"
    ),
    false
);?>
<?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
        "TYPE" => "right_1_main_page",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
    ),
    false
);?>
<?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
        "TYPE" => "right_3_main_page",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
    ),
    false
);?>
	</div>
	<div class="clearfix"></div>
        <?$APPLICATION->IncludeComponent(
    "bitrix:advertising.banner",
    "",
    Array(
        "TYPE" => "bottom_rest_list",
        "NOINDEX" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
    ),
    false
);?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>