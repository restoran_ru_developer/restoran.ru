<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Копирование ресторана");
?>
<?
if (!$USER->IsAuthorized()) die("Permission denied");
if (CSite::InGroup(array(1,9,14,15))):
    function iCopyIbElement($elementToCopy){
        CModule::IncludeModule("iblock");
        $resource = CIBlockElement::GetByID($elementToCopy);

        if ($ob = $resource->GetNextElement())
        {
            $arFields = $ob->GetFields();

            $arFields['PROPERTIES'] = $ob->GetProperties();
            global $USER;
            if($arFields['PROPERTIES']['user_bind']['VALUE'][0]==$USER->GetId()||$USER->IsAdmin()||CSite::InGroup(array(1,14,15))){
                $arFieldsCopy = $arFields;
                unset($arFieldsCopy['ID'], $arFieldsCopy['TMP_ID'], $arFieldsCopy['WF_LAST_HISTORY_ID'], $arFieldsCopy['SHOW_COUNTER'], $arFieldsCopy['SHOW_COUNTER_START'],
                    $arFieldsCopy['CREATED_DATE'],$arFieldsCopy['DATE_CREATE'],$arFieldsCopy['DATE_CREATE_UNIX'],$arFieldsCopy['DETAIL_PAGE_URL'],
                    $arFieldsCopy['IBLOCK_NAME'],$arFieldsCopy['LANG_DIR'],$arFieldsCopy['LIST_PAGE_URL'],$arFieldsCopy['ACTIVE_FROM'],$arFieldsCopy['ACTIVE_TO'],
                    $arFieldsCopy['TIMESTAMP_X_UNIX'],$arFieldsCopy['TIMESTAMP_X'],$arFieldsCopy['USER_NAME'],$arFieldsCopy['XML_ID'],$arFieldsCopy['LOCK_STATUS'],
                    $arFieldsCopy['LOCKED_USER_NAME'],$arFieldsCopy['IBLOCK_EXTERNAL_ID'],$arFieldsCopy['EXTERNAL_ID'],$arFieldsCopy['CREATED_USER_NAME'],
                    $arFieldsCopy['CREATED_BY'],$arFieldsCopy['BP_PUBLISHED'],$arFieldsCopy['WF_COMMENTS'],$arFieldsCopy['WF_DATE_LOCK'],$arFieldsCopy['WF_LOCKED_BY'],
                    $arFieldsCopy['WF_NEW'],$arFieldsCopy['WF_PARENT_ELEMENT_ID'],$arFieldsCopy['WF_STATUS_ID']);

                foreach($arFieldsCopy as $key=>$key_val){
                    if(preg_match('/^~/',$key)){
                        unset($arFieldsCopy[$key]);
                    }
                }

                $arFieldsCopy['ACTIVE'] = 'N';


//                print_r($arFields['PROPERTIES']);

                $copy_num = 0;
                $arSelect = Array("ID");
                $arFilter = Array("IBLOCK_ID"=>intval($arFieldsCopy['IBLOCK_ID']),'PROPERTY_user_bind'=>$arFields['PROPERTIES']['user_bind']['VALUE'][0],'NAME'=>$arFieldsCopy['NAME'].' копия%');
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                while($ob = $res->Fetch())
                {
                    $copy_num++;
                }

                if(!$copy_num){
                    $arFieldsCopy['NAME'] = $arFieldsCopy['NAME'].' копия';
                }
                else {
                    $arFieldsCopy['NAME'] = $arFieldsCopy['NAME'].' копия '.++$copy_num;
                }

                $arFieldsCopy['CODE'] = '';
                global $USER;
                $arFieldsCopy['MODIFIED_BY'] = $USER->GetID();
                $arFieldsCopy['PROPERTY_VALUES'] = array();

                foreach ($arFields['PROPERTIES'] as $property)
                {
                    if ($property['PROPERTY_TYPE']=='L'){
                        if ($property['MULTIPLE']=='Y'){
                            $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = array();
                            foreach($property['VALUE_ENUM_ID'] as $enumID){
                                $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']][] = array(
                                    'VALUE' => $enumID
                                );
                            }
                        } else {
                            $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = array(
                                'VALUE' => $property['VALUE_ENUM_ID']
                            );
                        }
                    }
                    elseif ($property['PROPERTY_TYPE']=='F')
                    {
                        if ($property['MULTIPLE']=='Y') {
                            if (is_array($property['VALUE']))
                            {
                                foreach ($property['VALUE'] as $key => $arElEnum)
                                    $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']][$key]=CFile::CopyFile($arElEnum);
                            }
                        }
                        else $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = CFile::CopyFile($property['VALUE']);
                    }
                    else {
                        $arFieldsCopy['PROPERTY_VALUES'][$property['CODE']] = $property['VALUE'];
                    }
                }

                unset($arFieldsCopy['PROPERTIES']);
//        unset($arFieldsCopy['PREVIEW_TEXT']);
                unset($arFieldsCopy['SEARCHABLE_CONTENT']);
//        unset($arFieldsCopy['DETAIL_TEXT']);
//            print_r($arFieldsCopy);

                $el = new CIBlockElement();
                if(!$NEW_ID = $el->Add($arFieldsCopy,false,false)){
                    echo "Error: ".$el->LAST_ERROR;
                }
                else {
                    echo 'Элемент скопирован. ID нового элемента: '.$NEW_ID;
                    global $APPLICATION;
                    LocalRedirect($APPLICATION->GetCurDir()."?copy_success=Y");
                }
            }
            else {?>
                <p class="font16" style="color:#cf141d">Доступ запрещен.</p>
            <?}
        }
    }
    ?>
    <div id="content">
        <?
        if($_REQUEST['rest_copy']=='Y'&&intval($_REQUEST['ID'])){
            iCopyIbElement(intval($_REQUEST['ID']));
        }
        ?>
    </div>
<?else:?>
    <div id="content">
        <p class="font16" style="color:#24A6CF">Доступ запрещен.</p>
        <Br />
    </div>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>