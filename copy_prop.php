<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
$IBLOCK_ID = 11;
$TARGETIB_ID = 13;

if(CModule::IncludeModule("iblock"))
{
    $ibp = new CIBlockProperty;

    $res = CIBlock::GetByID($IBLOCK_ID);
    if($ar_res = $res->Fetch()) {
        //echo "<pre>"; print_r($ar_res); echo "</pre>";
    }
    $proplist = CIBlockProperty::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBLOCK_ID));
    while ($ar_property = $proplist->GetNext())
    {
        $arFields = Array();
        $ar_enum_list = Array();
        $is_property = CIBlockProperty::GetByID($ar_property["ID"]);
        if($ar_prop = $is_property->Fetch()) {
            $id=$ar_prop["ID"];
            unset($ar_prop["ID"]);
            unset($ar_prop["XML_X"]);
            unset($ar_prop["TIMESTAMP_X"]);
            $ar_prop["IBLOCK_ID"] = $TARGETIB_ID;
            $arFields = $ar_prop;

            $db_enum_list = CIBlockProperty::GetPropertyEnum($id, Array("SORT"=>"ASC"), Array());
            while($ar_enum_list = $db_enum_list->Fetch())
            {
                unset($ar_enum_list["ID"]);
                unset($ar_enum_list["PROPERTY_ID"]);
                unset($ar_enum_list["XML_ID"]);
                unset($ar_enum_list["TMP_ID"]);
                unset($ar_enum_list["EXTERNAL_ID"]);
                $arFields["VALUES"][] = $ar_enum_list;
            }

            //$PropID = $ibp->Add($arFields);

            v_dump($arFields);
        }
    }
}
?>