<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Рестораны");
?> <i>Уже в грядущие выходные, 25 и 26 октября, весь город будет есть ночью! В Москве впервые пройдут &laquo;Гастрономические ночи&raquo;. Московские рестораны продлят часы работы - с 23.00 до 03.00. Стали известны меню ресторанов-участников! Это очень важно для любителей перекусить ночью!</i> 
<br />
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/chelsea-pub/" ><img class="indent" src="images/01_chelsea.jpg" width="150"  /></a> </div>
 
  <div class="right article_rest_text"> 
    <p><b>CHELSEA GastroPub</b> 
      <br />
     <span style="line-height: 1.45em;"><i>М. Гнездниковский переулок, 12/27</i></span></p>
   
    <p>0,33 пиво Heineken + пивное мини ассорти, 500 руб. 
      <br />
     <span style="line-height: 1.45em;">Креветка в темпуре, сырный шарик халапеньо, куриное крылышко на гриле, гренка чесночная, свиное ребрышко Jack Daniel's (50г.) + бананы, жаренные в кляре, с ванильно-ягодным соусом, 500 руб.</span></p>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><img class="indent" src="images/02_Funky.jpg" width="150"  /></div>
 
  <div class="right article_rest_text"> 
    <p><b>FunkyLab</b> 
      <br />
     <i>Б. Полянка, 7/10 стр. 1</i></p>
   
    <ul> 
      <li class="loaded">Funky ОбразЦы 490 руб.&nbsp;<span>Дегустационный сет, в котором представлены бестселлеры шеф-повара Funky Lab Евгения Вахрушева: креветка-васаби, каре ягненка, тунец на гриле и крылышко угнетенное кантри на подушке из перлотто д&rsquo;Оро.</span> </li>
     
      <li class="loaded">Lab is&hellip; 190 руб. 
        <br />
       <span>Смузи со вкусом клубника-банан культовой жвачки &ldquo;Love is…&rdquo;.</span> </li>
     
      <li class="loaded">Gew&uuml;rztraminer Espiritu de Chili 150 руб.&nbsp; 
        <br />
       Gewürztraminer сложен во всем - от названия до букета. Пожалуй, самое яркое белое вино с фруктовым и цветочным ароматом.</li>
     
      <li class="loaded">Фанки 200 руб.&nbsp; 
        <br />
       Лабораторный коктейль на основе рома в комбинации с мандариновым сиропом, апельсиновым и лимонным фрэшами.</li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/siniykot/" ><img class="indent" src="images/03_Blue_cat.jpg" width="150"  /></a></div>
 
  <div class="right article_rest_text"> 
    <p><b>«Синий кот»</b> 
      <br />
     <span style="line-height: 1.45em;"><i>Большой Черкасский переулок, 15</i></span></p>
   
    <ul> 
      <li><span style="line-height: 1.45em;">Морепродукты по-провансальски с сырами Рокфор и Мюнстер - 870 руб. Рекомендуемое сопровождение:&nbsp; Мюскаде Севр э Мэн. Домэн Гадэ Пэр и Фис Долина Луары LOIRE VALLEY 2011 MUSCADET SEVRE ET MAINE. DOMAINE GADAIS PERE &amp;FILS 150мл - 320 руб., 0,75 л - 1600 руб.</span></li>
     
      <li><span style="line-height: 1.45em;">Ile flottante (плавающий остров) - 90 руб. 
          <br />
         Рекомендуемое сопровождение: Сотерн. Шато Бриатт Рудэ. 0,1 л сладкое Бордо&nbsp; BORDEAUX SWEET SAUTERNE. CHATEAU BRIATTE ROUDES. 0,1 L 2008 100 мл.- 360 р&nbsp;</span></li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"> <img class="indent" src="images/04_UK.jpg" width="150"  /> </div>
 
  <div class="right article_rest_text"> 
    <p><b>UK</b> 
      <br />
     <i>Красный Октябрь</i></p>
   
    <ul> 
      <li>Суп морковный крем с облепихой, шевре и кервель&nbsp;</li>
     
      <li>Один салат на выбор:&nbsp; 
        <br />
       - Лакс лосось, яйцо 61, листья по сезону, заправка киндзмари&nbsp; 
        <br />
       - Говяжий язык, разные капусты, заправка сливочный имбирь&nbsp; 
        <br />
       - Печень трески, спаржа, зеленая фасоль, цукини, риоха винегрет&nbsp;</li>
     
      <li>Одно горячее на выбор:&nbsp; 
        <br />
       - Рагу из раков и кефали, сливочный соус с паприкой&nbsp; 
        <br />
       - Парментье с бычьими хвостами и антоновкой, микс салат&nbsp; 
        <br />
       - Конфи из бедра индейки, карри с кардамоном, тыква и шпинат &nbsp;</li>
     
      <li>Напиток - Аранчата из cицилийских апельсинов</li>
     </ul>
   Стоимость 1300 р. за три блюда + напиток</div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"> <img class="indent" src="images/05_bed.jpg" width="150"  /> </div>
 
  <div class="right article_rest_text"> 
    <p><b>бэд:wine&amp;soul</b> 
      <br />
     <i>Пресненский Вал, 6 стр. 2</i></p>
   
    <ul> 
      <li>Брускетта с сыром Дорблю, свежим инжиром и нежным свекольным кремом &ndash; 570 руб.Выбор сомелье:&nbsp;Reserve Mouton Cadet Sauternes,&nbsp;2010,&nbsp;Baron Philippe de Rothschild,&nbsp;Semillion 90%, Sauvignon Blanc 10%,&nbsp;France, Bordeaux, Sauternes AOC</li>
     
      <li>Тар-тар из лосося с воздушным кремом из сельдерея – 525 руб. 
        <br />
       <span>Выбор сомелье:&nbsp;</span>Pouilly-Fume &quot;Les Chante-Alouettes&quot;,&nbsp;Jean-Max Roger,&nbsp;2011,&nbsp;Sauvignon Blanc 100%,&nbsp;France, Vall&eacute;e de la Loire, Pouilly-Fumé AOC</li>
     
      <li>Теплый камамбер со свежими ягодами – 610 руб.<span>Выбор сомелье:&nbsp;Curtefranca,&nbsp;Ca`Del Bosco,&nbsp;2012,&nbsp;Chardonnay 80%, Pinot Bianco 20%,&nbsp;Italy, Lombardia, Curtefranca DOC</span> </li>
     
      <li>Половина цесарки на углях с киноа и луком конфи – 1247 руб.Выбор сомелье:&nbsp;Pinot Noir,&nbsp;Chapel Peak,&nbsp;2011,&nbsp;Pinot Noir 100%,&nbsp;New Zealand, Marlborough,&nbsp;France, Bordeaux, Sauternes AOC</li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/prozhektor/" ><img class="indent" src="images/06_prozhektor.jpg" width="150"  /></a></div>
 
  <div class="right article_rest_text"> 
    <p><b>Прожектор 
        <br />
       </b><span style="line-height: 1.45em;"><i>Славянская площадь, 2</i></span></p>
   
    <ul> 
      <li><span style="line-height: 1.45em;">Соте из мидий - 680 р.</span></li>
     
      <li><span style="line-height: 1.45em;">Морской гребешок с шафрановым пюре и овощами - 730 р.</span></li>
     
      <li><span style="line-height: 1.45em;">Телячья корейка со спаржей - 540 р.</span></li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/doubledutch/" ><img class="indent" src="images/07_DD.jpg" width="150"  /></a></div>
 
  <div class="right article_rest_text"> 
    <p><b>Double Dutch 
        <br />
       </b><span style="line-height: 1.45em;"><i>1-я Тверская-Ямская, 7</i></span></p>
   
    <ul> 
      <li><span style="line-height: 1.45em;">Мильфей с белыми грибами и пюре из пастернака</span></li>
     
      <li><span style="line-height: 1.45em;">Телечья лопатка</span></li>
     
      <li><span style="line-height: 1.45em;">Сливовый пай с миндалем</span></li>
     </ul>
   Стоимость 1490 руб.</div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/tarantino/" ><img class="indent" src="images/08_tarantino.jpg" width="150"  /></a> </div>
 
  <div class="right article_rest_text"> 
    <p><b>Тарантино 
        <br />
       </b><span style="line-height: 1.45em;"><i>Новый Арбат, 15</i></span></p>
   
    <ul> 
      <li class="loaded"><span style="line-height: 1.45em;">Легкий салат из теплой утки с козьим сыром и цукини гриль - 360 руб.</span></li>
     
      <li class="loaded"><span style="line-height: 1.45em;">Седьмое ребрышко молочного теленка с печеным перцем и баклажаном - 760 руб.</span></li>
     
      <li class="loaded"><span style="line-height: 1.45em;">Лазанья «Аматричча», приготовленная в печки с соусом из овечьего сыра - 390 руб.</span></li>
     
      <li class="loaded"><span style="line-height: 1.45em;">Капкейк из тыквы с кленовым сиропом - 250 руб.</span></li>
     </ul>
   21.00 – Пицца-шоу от шеф-повара Виктора Апасьева </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"> 	<img class="indent" src="images/09_8oz.jpg" width="148"  /> </div>
 
  <div class="right article_rest_text"> 
    <p><b>8Oz</b> 
      <br />
     <i>ЦПКИО им. Горького</i></p>
   
    <ul> 
      <li>Стейк салат - 430 руб.Телячья вырезка гриль, листья салата, тост из чиабатты с козьим сыром</li>
     
      <li>Крем-суп из пастернака - 320 руб.<span>Подается с тостами со сливочным сыром и копченым лососем</span> </li>
     
      <li>Горячее блюдо «Фишкейки» - 490 руб.<span>С черными спагетти под соусом биск</span> </li>
     
      <li>Шоколадный брауни - 260 руб.</li>
     
      <li>Горячий шоколадный кекс с ванильным мороженым</li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"> 	<img class="indent" src="images/10_kin.jpg" width="150"  /> </div>
 
  <div class="right article_rest_text"> 
    <p><b>Крылышко и ножка</b> 
      <br />
     <i>Проспект Мира, 77, к. 2</i></p>
   
    <ul> 
      <li>Копченое яйцо с соком из сморчков (290 р.) + Marston’s Oyster’s Stout (135 р.)</li>
     
      <li>Запечённая лопатка поросенка (420 р.) + ESB Champion (135 р.)</li>
     
      <li>Луковый пирог с трюфелем и копченым гусем (390 р.) + ESB Champion (135 р.)</li>
     
      <li>Копченая сосиска в тесте с картофельным кремом (320 р.) + London Pride (135 р.)</li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"> <img class="indent" src="images/11_FK.jpg" width="150"  /> </div>
 
  <div class="right article_rest_text"> 
    <p><b>Фани Кабани</b> 
      <br />
     <i>Малая Дмитровка, 5/9</i></p>
   
    <ul> 
      <li>Лосось Root Style – 369 руб.</li>
     
      <li>Country-салат – 299 руб.</li>
     
      <li>Фингеры из пеленгаса с чесночным «Айоли» и «Tabasco» - 379 руб.</li>
     
      <li>Охотничий грибной суп с уткой – 319 руб.</li>
     
      <li>Бургер Funny Style – 269 руб.</li>
     
      <li>Мурманская треска со «стью» из грибов – 499 руб.</li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/terrine/" ><img class="indent" src="images/12_terrine.jpg" width="150"  /></a></div>
 
  <div class="right article_rest_text"> 
    <p><b>Французский ресторан TERRINE 
        <br />
       </b><span style="line-height: 1.45em;"><i>Зубовский бульвар, 4</i></span></p>
   
    <p>Авторские коктейли Terrine:</p>
   
    <ul> 
      <li><span style="line-height: 1.45em;">Between the sheets&nbsp; (лимонная цедра, ликер Strega, ликер Гран Марнье, сок лимона, содовая) – 550 рублей</span></li>
     
      <li><span style="line-height: 1.45em;">Бельведер&nbsp; (джин на улуне, ликер фиалковый, сухой вермут, сок лимона, ангостура битер, белок) - 600 рублей</span></li>
     
      <li><span style="line-height: 1.45em;">Желтая пресса (водка, пюре банан, карамельный сироп, белок, сок лимона, сок апельсина) - 400 рублей</span></li>
     
      <li><span style="line-height: 1.45em;">Эйпл Физз (кальвадос, сок лимона, фруктоза, белок, сидр) – 550 рублей</span></li>
     
      <li><span style="line-height: 1.45em;">Golden Dream (куантро, гальяно голд страйк, апельсиновый сок, сливки) - 480 рублей</span></li>
     
      <li><span style="line-height: 1.45em;">Aviation (джинн, мараскино, сок лимона, ликер фиалки) – 600 рублей</span></li>
     
      <li><span style="line-height: 1.45em;">TheGodfather&nbsp; - (скотч, миндальный ликер) - 300 руб.</span></li>
     </ul>
   25.10, 20.00 – магическое cocktail-show: коктейли превращаются….в сорбеты, спуманте, смуси. Формулу выбираете вы, от нас -&nbsp; ловкость рук и потрясающий вкус! Wellcome-коктейль гостям гастрономических ночей! Пароль – на <a href="https://www.facebook.com/RestoranTerrine?fref=ts" target="_blank" >FB-странице Terrine</a> </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"> <img class="indent" src="images/13_kadril.jpg" width="150"  /> </div>
 
  <div class="right article_rest_text"> 
    <p><b>«Моя Кадриль»</b> 
      <br />
     <i>Пушечная, 7/5</i></p>
   
    <ul> 
      <li>Салат с кенийскими бобами по-азиатски с коктейлем Nuts Saut – 650 р.</li>
     
      <li>Шейка лангустина на пюре из сельдерея с коктелем Чили тини – 1040 р.</li>
     
      <li>Австралийская вырезка с мятым картофелем и клюквенным взваром - 1320 р.<span>Коктейль Gorn</span> </li>
     
      <li>Малиновый мусс с соусом из голубики 800 р.<span>Коктейль Сграпино маракуйя</span> </li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 
<div class="restoran_text bl"> 
  <div class="left article_rest"><a href="/msk/detailed/restaurants/veranda_3205/" ><img class="indent" src="/upload/resize_cache/iblock/63a/148_98_240cd750bba9870f18aada2478b24840a/13.jpg" width="148"  /></a> 
    <br />
   <a class="font14" href="/msk/detailed/restaurants/veranda_3205/" >Веранда 32.05</a>, 
    <br />
   
    <p></p>
   
    <div class="rating"> 
      <div class="small_star_a" alt="0"></div>
     
      <div class="small_star_a" alt="1"></div>
     
      <div class="small_star_a" alt="2"></div>
     
      <div class="small_star_a" alt="3"></div>
     
      <div class="small_star_a" alt="4"></div>
     
      <div class="clear"></div>
     </div>
   <b>Кухня:</b> Авторская, Европейская, Русская 
    <br />
   <b>Средний счет :</b> 1000-1500р. 
    <br />
   
    <p></p>
   <a href="/msk/detailed/restaurants/chestnaya_kukhnya/" ><img class="indent" src="/upload/resize_cache/iblock/c78/148_98_240cd750bba9870f18aada2478b24840a/img_3348-panorama_small.jpg" width="148"  /></a> 
    <br />
   <a class="font14" href="/msk/detailed/restaurants/chestnaya_kukhnya/" >Честная кухня</a>, 
    <br />
   
    <p></p>
   
    <div class="rating"> 
      <div class="small_star_a" alt="0"></div>
     
      <div class="small_star" alt="1"></div>
     
      <div class="small_star" alt="2"></div>
     
      <div class="small_star" alt="3"></div>
     
      <div class="small_star" alt="4"></div>
     
      <div class="clear"></div>
     </div>
   <b>Кухня:</b> Домашняя, Европейская, Русская 
    <br />
   <b>Средний счет :</b> 1000-1500р. 
    <br />
   
    <p></p>
   </div>
 
  <div class="right article_rest_text"> 
    <p>В ближайшее время ожидаем специальное меню от:</p>
   
    <ul> 
      <li class="loaded"><b>FF Bar&amp;Restaurant</b>&nbsp; 
        <br />
       <i>ул.Тимура Фрунзе, 11 
          <br />
         
          <br />
         </i> </li>
     
      <li class="loaded"><b>WT4</b>&nbsp; 
        <br />
       <i>Берсеневская наб., 6/3 
          <br />
         
          <br />
         </i> </li>
     
      <li class="loaded"> <b><a href="http://www.restoran.ru/msk/detailed/restaurants/chestnaya_kukhnya/" >Честная кухня</a>&nbsp;</b> 
        <br />
       <i>Садовая-Черногрязская, 10 
          <br />
         
          <br />
         </i> </li>
     
      <li class="loaded"> <b><a href="http://www.restoran.ru/msk/detailed/restaurants/veranda_3205/" >32.05</a>&nbsp;</b> 
        <br />
       <span><i>Сад Эрмитаж</i></span> </li>
     </ul>
   </div>
 
  <div class="clear"></div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>