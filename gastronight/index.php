<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гастрономические ночи с Restoran.ru");
?>
    <div class="bg"></div>
        <div class="logo">
            <a href="/"><img src="images/g_logo.png" /></a>
        </div>
        <div class="content">
            <div class="press menu_items">
                <a href="/gastronight/press.php"><img src="images/g_press.png" alt="Пресс-релиз" title="Пресс-релиз" /></a>
            </div>
            <div class="rest menu_items">
                <a href="/gastronight/rest.php"><img src="images/g_rest.png" alt="Рестораны" title="Рестораны" /></a>
            </div>
            <div class="ex menu_items">
                <a href="#"><img src="images/g_ex.png" alt="Эксперты" title="Эксперты" /></a>
            </div>
            <div class="inst menu_items">
                <a href="/gastronight/instagram.php"><img src="images/g_inst.png" alt="Instagram конкурс" title="Instagram конкурс" /></a>
            </div>
            <div class="photo menu_items">
                <a href="/gastronight/photoreports.php"><img src="images/g_photo.png" alt="Instagram конкурс" title="Instagram конкурс" /></a>
            </div>
        </div>        
        <script>
            $(document).ready(function(){       
                setTimeout("g_press()",100);                                                                
                setTimeout("g_rest()",250);                                                                
                setTimeout("g_ex()",400);                                                                
                setTimeout("g_inst()",550);                                                                
                setTimeout("g_photo()",700);                                                                
            });
            function g_press()
            {
                $(".press").css("left","110px");
            }
            function g_rest()
            {
               $(".rest").css("bottom","80px");
            }
            function g_ex()
            {
                $(".ex").css("top","64%");
            }
            function g_inst()
            {
                $(".inst").css("bottom","80px");
            }
            function g_photo()
            {
                $(".photo").css("top","55%");
            }
        </script>    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>