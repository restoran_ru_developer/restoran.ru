<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Instagram - конкурс")
?>
<style>
    #photos-wrap {
  width: 700px;
  margin: 0 auto;
  margin-left:10px;
  position: relative;
  z-index: 1;
  margin-left:-20px;
}

.avatar {
  width: 30px;
  height: 30px;
  padding: 2px;
  position: absolute;
  bottom: 10px;
  right: 10px;
  background: rgba(255,255,255,0.6);
  border-radius:100px;
}
	
.photo_i  {
  float: left;
  position: relative;
  width: 143px;
  height: 143px;  
  margin: 10px;  
  border:5px solid #FFF;
}
.photo_i img.main {
/*	padding-left:15px;
	padding-top:15px;*/
}
.photo_i .heart {
  height: 24px;
    width: 18px;
    position: absolute;
    left: 10px;
    top: -5px;
    padding: 3px 5px 0 22px;
    font-size: 12px;
    font-weight: bold;
    line-height: 16px;    
  background: url('images/g_heart.png') left center no-repeat;
}

.paginate {
  display: block;
  clear: both;
  margin: 10px;
  text-align: center;
  margin: 0 auto;
  padding: 20px 0;
  height: 100px;
}

.button {
  height: 32px;
  padding: 10px;
  background-color: #E0E0E0;
  border: 1px solid #ddd;
  background-image: -moz-linear-gradient(top,white 0,#E0E0E0 100%);
  background-image: -ms-linear-gradient(top,white 0,#E0E0E0 100%);
  background-image: -o-linear-gradient(top,white 0,#E0E0E0 100%);
  background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0,white),color-stop(100%,#E0E0E0));
  background-image: -webkit-linear-gradient(top,white 0,#E0E0E0 100%);
  background-image: linear-gradient(to bottom,white 0,#E0E0E0 100%);
  border-radius: 3px;
  cursor: pointer;
  text-decoration:none;
  font-size: 12px;
  color: #222;
  text-shadow: white 1px 0px;
}


/*circle*/

#floatingCirclesG{
position:relative;
width:128px;
height:128px;
-moz-transform:scale(0.6);
-webkit-transform:scale(0.6);
-ms-transform:scale(0.6);
-o-transform:scale(0.6);
transform:scale(0.6);
}

.f_circleG{
position:absolute;
background-color:#FFFFFF;
height:23px;
width:23px;
-moz-border-radius:12px;
-moz-animation-name:f_fadeG;
-moz-animation-duration:0.88s;
-moz-animation-iteration-count:infinite;
-moz-animation-direction:linear;
-webkit-border-radius:12px;
-webkit-animation-name:f_fadeG;
-webkit-animation-duration:0.88s;
-webkit-animation-iteration-count:infinite;
-webkit-animation-direction:linear;
-ms-border-radius:12px;
-ms-animation-name:f_fadeG;
-ms-animation-duration:0.88s;
-ms-animation-iteration-count:infinite;
-ms-animation-direction:linear;
-o-border-radius:12px;
-o-animation-name:f_fadeG;
-o-animation-duration:0.88s;
-o-animation-iteration-count:infinite;
-o-animation-direction:linear;
border-radius:12px;
animation-name:f_fadeG;
animation-duration:0.88s;
animation-iteration-count:infinite;
animation-direction:linear;
}

#frotateG_01{
left:0;
top:52px;
-moz-animation-delay:0.33s;
-webkit-animation-delay:0.33s;
-ms-animation-delay:0.33s;
-o-animation-delay:0.33s;
animation-delay:0.33s;
}

#frotateG_02{
left:15px;
top:15px;
-moz-animation-delay:0.44s;
-webkit-animation-delay:0.44s;
-ms-animation-delay:0.44s;
-o-animation-delay:0.44s;
animation-delay:0.44s;
}

#frotateG_03{
left:52px;
top:0;
-moz-animation-delay:0.55s;
-webkit-animation-delay:0.55s;
-ms-animation-delay:0.55s;
-o-animation-delay:0.55s;
animation-delay:0.55s;
}

#frotateG_04{
right:15px;
top:15px;
-moz-animation-delay:0.66s;
-webkit-animation-delay:0.66s;
-ms-animation-delay:0.66s;
-o-animation-delay:0.66s;
animation-delay:0.66s;
}

#frotateG_05{
right:0;
top:52px;
-moz-animation-delay:0.77s;
-webkit-animation-delay:0.77s;
-ms-animation-delay:0.77s;
-o-animation-delay:0.77s;
animation-delay:0.77s;
}

#frotateG_06{
right:15px;
bottom:15px;
-moz-animation-delay:0.88s;
-webkit-animation-delay:0.88s;
-ms-animation-delay:0.88s;
-o-animation-delay:0.88s;
animation-delay:0.88s;
}

#frotateG_07{
left:52px;
bottom:0;
-moz-animation-delay:0.99s;
-webkit-animation-delay:0.99s;
-ms-animation-delay:0.99s;
-o-animation-delay:0.99s;
animation-delay:0.99s;
}

#frotateG_08{
left:15px;
bottom:15px;
-moz-animation-delay:1.1s;
-webkit-animation-delay:1.1s;
-ms-animation-delay:1.1s;
-o-animation-delay:1.1s;
animation-delay:1.1s;
}

@-moz-keyframes f_fadeG{
0%{
background-color:#0EBCC2}

100%{
background-color:#FFFFFF}

}

@-webkit-keyframes f_fadeG{
0%{
background-color:#0EBCC2}

100%{
background-color:#FFFFFF}

}

@-ms-keyframes f_fadeG{
0%{
background-color:#0EBCC2}

100%{
background-color:#FFFFFF}

}

@-o-keyframes f_fadeG{
0%{
background-color:#0EBCC2}

100%{
background-color:#FFFFFF}

}

@keyframes f_fadeG{
0%{
background-color:#0EBCC2}

100%{
background-color:#FFFFFF}

}
</style>
<script src="javascripts/components/handlebars.js/handlebars.runtime-1.0.0-rc.1.js"></script>
  <script src="javascripts/templates/photo-list.tmpl.js"></script>
  <script src='javascripts/app.js' type='text/javascript' charset='utf-8'></script>
  <p>
      Снимайте красивые и вкусные блюда фестиваля, выкладывайте их в Instagram  с тегами <b>#restoranru</b> и <b>#яемночью</b>, призывайте друзей и знакомых ставить лайки, чьи фото наберут  больше всех лайков, получат призы  от Ресторан.ру      
  </p>
<div class="loading">
    <div id="floatingCirclesG">
    <div class="f_circleG" id="frotateG_01">
    </div>
    <div class="f_circleG" id="frotateG_02">
    </div>
    <div class="f_circleG" id="frotateG_03">
    </div>
    <div class="f_circleG" id="frotateG_04">
    </div>
    <div class="f_circleG" id="frotateG_05">
    </div>
    <div class="f_circleG" id="frotateG_06">
    </div>
    <div class="f_circleG" id="frotateG_07">
    </div>
    <div class="f_circleG" id="frotateG_08">
    </div>
    </div>
    </div>
    <div id='photos-wrap'>
    </div>
<div class='paginate'>
    <a class='button more'  style='display:none;' data-max-tag-id='' href='#'>Показать еще...</a>
</div>
  <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>