<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пресс-релиз");
?> 
 <p>На протяжении двух ночей рестораны-участники продлят время своей работы и предложат гостям специальное меню, дегустационные сеты, а также кулинарные мастер-классы, гастрономические лекции и перфомансы. Проект призван стать ярким культурным событием в сфере гастрономии, вдохновленный такими проектами, как &laquo;Ночь музеев&raquo; или «Ночь шоппинга». </p>
   
    <p>Среди участников фестиваля &ndash; рестораны и бары: <a href="http://www.restoran.ru/msk/detailed/restaurants/chestnaya_kukhnya/" target="_blank" >«Честная кухня»</a>, веранда <a href="http://www.restoran.ru/msk/detailed/restaurants/veranda_3205/" target="_blank" >«32.05»</a>, гастробар «Funky Lab», гастро-паб «Крылышко или ножка», <a href="http://www.restoran.ru/msk/detailed/restaurants/chelsea-pub/" target="_blank" >паб «Chelsea»</a>, бар «FF» и «WT4», «Double Dutch», <a href="http://www.restoran.ru/msk/detailed/restaurants/prozhektor/" target="_blank" >«Прожектор»</a>, «Моя Кадриль», <a href="http://www.restoran.ru/msk/detailed/restaurants/tarantino/" target="_blank" >ресторан «ТАРАНТИНО»</a>, французские рестораны <a href="http://www.restoran.ru/msk/detailed/restaurants/terrine/" target="_blank" >«Террин»</a> и <a href="http://www.restoran.ru/msk/detailed/restaurants/siniykot/" target="_blank" >«Синий Кот»</a>, кафе «Фанни Кабани», «8oz», «United Kitchen», ресторан-караоке «БЭД: wine&amp;soul». </p>
   
    <p>В рамках фестиваля также работает группа экспертов – признанные авторитеты в области гастрономии и ресторации: Андрей Захарин, Сергей Ерошенко, Олег Назаров, Уильям Ламберти, Елена Князева, Полина Дзюба, Михаил Идов, Владимир Раевский и другие. </p>
   
    <p>Основная идея фестиваля состоит в том, чтобы познакомить жителей столицы и экспертов с молодыми рестораторами – «рестораторами второй волны», как окрестили в тесных кругах дерзких и амбициозных носителей идеологии демократичных заведений с незаурядными меню и атмосферой. </p>
   
    <p>Организатором фестиваля выступает агентство «BRANDWOOD», специализирующееся исключительно на маркетинге и PR в сфере гастрономии и ресторации. </p>
   Партнерами проекта выступила TOYOTA, которая осуществляет комфортное передвижение экспертов фестиваля по заведениям-участникам, система лояльности STARCARD, делающая счастливыми всех гурманов Москвы, итальянские деликатесы Casa Rinaldi, проект «L-wine» и Jagermeister.</p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>