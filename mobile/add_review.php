<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$arReviewsIB = getArIblock("reviews", CITY_ID);

$APPLICATION->IncludeComponent(
        "restoran:comments_add_new", "with_rating_new_on_page", Array(
    "IBLOCK_TYPE" => "reviews",
    "IBLOCK_ID" => $arReviewsIB["ID"],
    "ELEMENT_ID" => $_REQUEST["id"],
    "IS_SECTION" => "N",
    "CACHE_TYPE" => "N"
        ), false
);
?>
  
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>