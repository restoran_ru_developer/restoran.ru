<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

function phone_number($sPhone){ 
    $sPhone = ereg_replace("[^0-9]",'',$sPhone); 
    if(strlen($sPhone) != 11) return(False); 
    $code = substr($sPhone, 0,1);
    $sArea = substr($sPhone, 1,3); 
    $sPrefix = substr($sPhone,4,3); 
    $sNumber1 = substr($sPhone,7,2); 
    $sNumber2 = substr($sPhone,9,2); 
    $sPhone ="+".$code. " (".$sArea.") ".$sPrefix."-".$sNumber1."-".$sNumber2; 
    return($sPhone); 
} 

//получает xml
function getRawPostData(){
	if ($_SERVER['REQUEST_METHOD'] != 'POST'){
      return null;
    }
    $fh = fopen('php://input', 'r');
    $postData = '';
    while ($line = fgets($fh)){
      $postData .= $line;
    }
	return $postData; 
}


function saxStartElement($parser,$name,$attrs){
    global $index, $sms;
    switch($name){
    	case 'data': 
    		$sms = array();
            break;
        default:
            $index = $name;
            break;
    };
}
   

function saxEndElement($parser,$name){
    global $sms, $index, $current_tag;

    if (is_array($sms) && $index!=""){
    	$sms[$index]=$current_tag;
        $current_tag = null;
    }
    $index = null;
}



function saxCharacterData($parser,$data){
    global $current_tag,$index;
   	
   	$current_tag.=$data;
	
}



$postData = getRawPostData();
file_put_contents('receiving.txt', $postData."\n", FILE_APPEND);

/*
$postData='<?xml version="1.0" encoding="UTF-8"?><data><authorization><login>rest_sms</login><password>8DsyFXMG</password></authorization><type>incoming</type><request><transaction_id>2810704960</transaction_id><short_number>1223</short_number><msisdn>79523971714</msisdn><message>Тест</message><datetime>20120413112742</datetime><qnt_parts>1</qnt_parts></request></data>';
*/

$parser = xml_parser_create();

xml_set_element_handler($parser,'saxStartElement','saxEndElement');
xml_set_character_data_handler($parser,'saxCharacterData');
xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,false);

if (!xml_parse($parser,$postData,true))
    die(sprintf('Ошибка XML: %s в строке %d',
        xml_error_string(xml_get_error_code($parser)),
        xml_get_current_line_number($parser)));

xml_parser_free($parser);




if($sms["login"]=="rest_sms" && $sms["password"]=="8DsyFXMG"){
	//v_dump($sms);		
	$telephone = $sms["msisdn"];
	$message=$sms["message"];
	$date=$sms["datetime"];
	$tid=$sms["transaction_id"];
	
	
	$hour=$date[8].$date[9];
	
	$date = $date[6].$date[7].".".$date[4].$date[5].".".$date[0].$date[1].$date[2].$date[3]." ".$date[8].$date[9].":".$date[10].$date[11].":".$date[12].$date[13];
		
	
	
	//echo $date;
	//echo '<br/>';
	//Смотрим время и формируем ответ
	//echo $hour;
	
	
	if($hour>=0 && $hour<=5){
		$otvet ="доброй ночи!";
	}
	
	if($hour>5 && $hour<11){
		$otvet = "доброе утро!";
	}
	
	
	if($hour>=11 && $hour<18){
		$otvet = "добрый день!";
	}
	
	
	if($hour>=18 && $hour<=23){
		$otvet = "добрый вечер!";
	}

	
	
	ob_clean();

		
	//теперь нужно добавить смску в инфоблок
	
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		die();
	}

	$PROP["transaction_id"] = $tid; 
	$PROP["new"] = 1548; 

	$el = new CIBlockElement;
	$arLoadProductArray = Array(
  		"MODIFIED_BY"    => $USER->GetID(), 
  		"IBLOCK_SECTION_ID" => false,         
  		"IBLOCK_ID"      => 149,
  		"NAME"           => phone_number($telephone),
  		"PROPERTY_VALUES"=> $PROP,
  		"ACTIVE"         => "Y",
  		"PREVIEW_TEXT"   => $message,
  	);
	if($ID = $el->Add($arLoadProductArray)){
		
		//создаем файлик временно
		$f = fopen($_SERVER["DOCUMENT_ROOT"]."/bs/alerts/sms/".$ID, "w");
		fwrite($f, $ID);
		fclose($f);
	
		header ("content-type: text/xml");
	
		echo'<?xml version="1.0" encoding="UTF-8" ?>
		<data>
		<authorization>
			<login>rest_sms</login>
			<password>8DsyFXMG</password>
		</authorization>
		<type>send</type>
		<request>
			<transaction_id>'.$tid.'</transaction_id>
			<message>'.$otvet.'</message>
		</request>
		</data>';
	}
	
	
	

}else die("Login or password incorrect!");



//file_put_contents('receiving.txt', serialize($sms), FILE_APPEND); 
  
?>