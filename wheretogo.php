<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Основные события на сайте");
$APPLICATION->SetPageProperty("keywords", "события на сайте");
$APPLICATION->SetPageProperty("title", "Ресторан.ру - основные события на сайте");
?>

<div class="content main_2015_summer_content">
    <div class="block">
        <h1 class="index-h" ><?=$APPLICATION->ShowTitle("h1")?></h1>
        <div class="clearfix"></div>
        <div class="index-journal-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "wtg-full-page",
                    Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "system",
                        "IBLOCK_ID" => "4514",
                        "NEWS_COUNT" => "999",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "ID",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array("DETAIL_PICTURE"),
                        "PROPERTY_CODE" => array("WTG_BG_PIC",'WTG_BG_PIC_FULL'),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => CITY_ID,
                        "INCLUDE_SUBSECTIONS" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                    ),
                    false,
                    Array(
                        'ACTIVE_COMPONENT' => 'Y'
                    )
                );?>



<?//$APPLICATION->IncludeComponent(
//                    "restoran:news.list_no_cached_template",
//                    "index-wtg-block",
//                    Array(
//                        "DISPLAY_DATE" => "N",
//                        "DISPLAY_NAME" => "Y",
//                        "DISPLAY_PICTURE" => "Y",
//                        "DISPLAY_PREVIEW_TEXT" => "N",
//                        "AJAX_MODE" => "N",
//                        "IBLOCK_TYPE" => "system",
//                        "IBLOCK_ID" => "4514",
//                        "NEWS_COUNT" => "999",
//                        "SORT_BY1" => "SORT",
//                        "SORT_ORDER1" => "DESC",
//                        "SORT_BY2" => "ID",
//                        "SORT_ORDER2" => "ASC",
//                        "FILTER_NAME" => "",
//                        "FIELD_CODE" => array("DETAIL_PICTURE"),
//                        "PROPERTY_CODE" => array("WTG_BG_PIC"),
//                        "CHECK_DATES" => "Y",
//                        "DETAIL_URL" => "",
//                        "PREVIEW_TRUNCATE_LEN" => "",
//                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
//                        "SET_STATUS_404" => "N",
//                        "SET_TITLE" => "N",
//                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//                        "ADD_SECTIONS_CHAIN" => "N",
//                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//                        "PARENT_SECTION" => "",
//                        "PARENT_SECTION_CODE" => CITY_ID,
//                        "INCLUDE_SUBSECTIONS" => "N",
//                        "CACHE_TYPE" => "A",
//                        "CACHE_TIME" => "36000000",
//                        "CACHE_FILTER" => "Y",
//                        "CACHE_GROUPS" => "N",
//                        "PAGER_TEMPLATE" => ".default",
//                        "DISPLAY_TOP_PAGER" => "N",
//                        "DISPLAY_BOTTOM_PAGER" => "N",
//                        "PAGER_TITLE" => "Новости",
//                        "PAGER_SHOW_ALWAYS" => "N",
//                        "PAGER_DESC_NUMBERING" => "N",
//                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//                        "PAGER_SHOW_ALL" => "N",
//                        "AJAX_OPTION_JUMP" => "N",
//                        "AJAX_OPTION_STYLE" => "N",
//                        "AJAX_OPTION_HISTORY" => "N",
//                    ),
//                    false,
//                    Array(
//                        'ACTIVE_COMPONENT' => 'Y'
//                    )
//                );?>

        </div>

    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>