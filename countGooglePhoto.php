<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
$rsRestMsk = CIBlockElement::GetList(
    Array("PROPERTY_sleeping_rest" => "ASC", "NAME" => "ASC"),
    Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => "11",
        "!SECTION_ID" => 226,
        "!PROPERTY_google_photo" => false,
    ),
    false,
    false,
    Array("ID", "NAME", "PROPERTY_sleeping_rest")
);
echo "<h2>В Москве с фото для Google ресторанов и банкетных залов: ".$rsRestMsk->SelectedRowsCount()."</h2>";
while($arRestMsk = $rsRestMsk->GetNext()) {
    echo $arRestMsk["NAME"]." (".($arRestMsk["PROPERTY_SLEEPING_REST_VALUE"] ? '<i>спящий</i>' : '<i>не спящий</i>').")<br />";
}

$rsRestSpb = CIBlockElement::GetList(
    Array("PROPERTY_sleeping_rest" => "ASC", "NAME" => "ASC"),
    Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => "12",
        "!SECTION_ID" => 44170,
        "!PROPERTY_google_photo" => false,
    ),
    false,
    false,
    Array("ID", "NAME", "PROPERTY_sleeping_rest")
);
echo "<h2>В СПб с фото для Google ресторанов и банкетных залов: ".$rsRestSpb->SelectedRowsCount()."</h2>";
while($arRestSpb = $rsRestSpb->GetNext()) {
    echo $arRestSpb["NAME"]." (".($arRestSpb["PROPERTY_SLEEPING_REST_VALUE"] ? '<i>спящий</i>' : '<i>не спящий</i>').")<br />";
}

$rsRestSpb = CIBlockElement::GetList(
    Array("PROPERTY_sleeping_rest" => "ASC", "NAME" => "ASC"),
    Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => "16",
        "!SECTION_ID" => 44510,
        "!PROPERTY_google_photo" => false,
    ),
    false,
    false,
    Array("ID", "NAME", "PROPERTY_sleeping_rest")
);
echo "<h2>В Краснодаре с фото для Google ресторанов и банкетных залов: ".$rsRestSpb->SelectedRowsCount()."</h2>";
while($arRestSpb = $rsRestSpb->GetNext()) {
    echo $arRestSpb["NAME"]." (".($arRestSpb["PROPERTY_SLEEPING_REST_VALUE"] ? '<i>спящий</i>' : '<i>не спящий</i>').")<br />";
}
?>