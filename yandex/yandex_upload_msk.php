<?
if ($argv[1] != 'pars_admin') {
    die();
}

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer(); ?>

<?
//@ini_set("memory_limit", "256M");
//if(!$USER->IsAdmin())
//  return false;

CModule::IncludeModule("iblock");
function getdaynum($a)
{
    if (substr_count($a, "будни") > 0)
        return "12345";
    if (substr_count($a, "выходные") > 0)
        return "67";
    if (substr_count($a, "пн-чт") > 0)
        return "1234";
    if (substr_count($a, "пн-пт") > 0)
        return "12345";
    if (substr_count($a, "пт-сб") > 0)
        return "56";
    if (substr_count($a, "пт,сб") > 0)
        return "56";
    if (substr_count($a, "сб-вс") > 0)
        return "67";
    if (substr_count($a, "сб,вс") > 0)
        return "67";
    if (substr_count($a, "пн") > 0)
        return "1";
    if (substr_count($a, "вт") > 0)
        return "2";
    if (substr_count($a, "ср") > 0)
        return "3";
    if (substr_count($a, "чт") > 0)
        return "4";
    if (substr_count($a, "пт") > 0)
        return "5";
    if (substr_count($a, "сб") > 0)
        return "6";
    if (substr_count($a, "вс") > 0)
        return "7";
    return "1234567";
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$av_bil = Array("до 1000р", "1000-1500р.", "1500-2000р", "2000-3000р", "от 3000р");
$av_bil_yandex = Array("price_cheap", "price_reasonable", "price_expensive", "price_very_expensive", "price_very_expensive");
$av_bil_yandex_from_to = Array("0-1000", "1000-1500", "1500-2000", "2000-3000", "3000-5000");


//Тип ресторана

/*
 * Пивной ресторан
 * Гастропаб
 * Бар
 * Ресторан
 * Банкетный зал
 * Клуб
 * Суши-бар
 * Загородный ресторан
 * Центр развлечений
 * Кофейня
 * Паб
 * Теплоход
 * Караоке-бар
 * Чайный ресторан
 * Кейтеринг
 * Чайная
 * Кафе-бар
 * Стейк-хаус
 * Гастробар
 * Чайхана
 * Кондитерская
 * Винотека
 * Фаст-фуд/Стритфуд
 * Кабаре
 * Халяльный ресторан
 * Кафе
 * */


/*
 *
    Кафе (184106390)
    Кофейня (35193114937)
    Ресторан (184106394)
    Бар, паб (184106384)
    Спорт-бар (770931537)
    Быстрое питание (184106386)
    Пиццерия (184106392)
    Столовая (184106396)
    Суши-бар (1387788996)
 * */

$prop_type = Array('Бар','Ресторан','Суши-бар','Загородный ресторан','Кофейня','Паб','Караоке-бар','Чайный ресторан','Кафе-бар','Халяльный ресторан','Кафе');

$prop_type_yandex_type_number = Array('184106384','184106394','1387788996','184106394','35193114937','184106384','184106384','184106394','184106390','184106394','184106390');

$prop_type_for_type = Array('Пивной ресторан','Гастропаб','Чайная','Стейк-хаус','Винотека','Фаст-фуд/Стритфуд');

$prop_type_yandex_type = array('beer_restaurant','gastropub','tea_house','steak_house','vinotheque','fast_food');






//$prop_type = Array("Банкетный зал", "Бар", "Караоке-бар", "Кафе", "Кейтеринг", "Кофейня", "Паб", "Ресторан", "Суши-бар", "Чайный ресторан",'Фаст-фуд/Стритфуд',
//'Гастропаб','Пивной ресторан','Винотека','Стейк-хаус','Чайная');
//
//$prop_type_yandex = Array("", "", "", "", "", "", "", "", "", "tea_house",'fast_food',
//    'gastropub','beer_restaurant','wine_cellar','steak_house','tea_house');
//
//$prop_type_yandex_type = Array("184108315", "184106384", "19244809283", "184106390", "184106394", "35193114937", "184106384", "184106394", "1387788996", "184106390",'184106386');



//особенности - features
$prop_features_bool = Array('Летняя веранда','Круглосуточно');
$prop_features_yandex_bool = Array('summer_terrace','around_the_clock_work1');

//особенности - features - features_institution
$prop_features = Array('Ресторан у воды','При отеле','В торговом или развлекательном центре');
$prop_features_yandex = Array('location_near_the_water','restaurant_in_the_hotel','restaurant_in_the_shopping_center');



//Кухня
$prop_kitchen = Array("Австралийская", "Авторская", "Азербайджанская", "Азиатская", "Американская", "Английская", "Арабская",
    "Аргентинская", "Армянская", "Белорусская",
    "Восточная", "Грузинская", "Домашняя", "Еврейская", "Европейская", "Индийская",
    "Испанская", "Итальянская", "Кавказская", "Китайская", "Корейская", "Латиноамериканская", "Мясная",
    "Мексиканская", "Немецкая", "Паназиатская", "Рыбная", "Русская", "Средиземноморская", "Тайская", "Татарская", "Турецкая", "Узбекская",
    "Украинская", "Французская", "Фьюжн", "Чешская", "Японская");
$prop_kitchen_yandex = Array("australia_cuisine",  "authors_cuisine", "azerbaijani_cuisine", "asian_cuisine", "american_cuisine", "english_cuisine", "arabian_cuisine",
    "argentine_cuisine", "armenian_cuisine", "belarusian_cuisine",
    "middle_eastern_cuisine", "georgian_cuisine", "home_cuisine", "jewish_cuisine", "european_cuisine", "indian_cuisine",
    "spanish_cuisine", "italian_cuisine", "caucasian_cuisine", "chinese_cuisine", "korean_cuisine", "latin_american_cuisine",
    "meat_cuisine", "mexican_cuisine",
    "german_cuisine", "pan_asian_cuisine", "fish_cuisine", "russian_cuisine", "mediterranean_cuisine", "thai_cuisine", "tatar_cuisine",  "turkish_cuisine",
    "uzbek_cuisine", "ukrainian_cuisine", "french_cuisine", "fusion_cuisine", "czech_cuisine", "japanese_cuisine");

//$prop_credit = Array("Master Card / Eurocard","Diners Club","Union","Visa","Maestro","American Express");
//$prop_credit_yandex = Array("mastercard","diners_club_international","union_card","visa","maestro","american_express");
//
//$prop_music = Array("DJ","Фоновая","Джаз","Поп/рок","Живая музыка","Классика","Хиты 80-х, 90-х");
//$prop_music_yandex = Array("djs_music","background_music","jazz","pop_music","live_music","classical_music","80-90th-music");

// start execution
$time_start = microtime_float();

file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/yandex/yandex_upload.xml", '', LOCK_EX);

$str = "<companies xmlns:xi='http://www.w3.org/2001/XInclude' version='1.0'>\n";

file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/yandex/yandex_upload.xml", $str, FILE_APPEND | LOCK_EX);
$str = "";
$rsIblock = CIBlock::GetList(
    Array("ID" => "ASC"),
    Array(
        "SITE_ID" => "s1",
        "TYPE" => "catalog",
        "ID" => Array(11, 12)
    ),
    false
);
while ($arIblock = $rsIblock->Fetch()) {
    $rsRest = CIBlockElement::GetList(
        Array("rand" => "ASC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $arIblock["ID"],
            "!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
            "!PROPERTY_photos" => false,
            "!PROPERTY_lat" => false,
            ">PROPERTY_lat" => 0,
            "!PROPERTY_sleeping_rest_VALUE" => "Да",
            "PROPERTY_working_VALUE" => "Да",
            "!ID" => Array(387733, 388019),
//                        "ID" => 1237781
            "!PROPERTY_google_photo" => false
        ),
        false,
        false,//Array("nTopCount"=>10),
        Array("ID", "TIMESTAMP_X", "NAME", "TAGS", "DETAIL_PAGE_URL", "DETAIL_TEXT", "DATE_CREATE")
    );
    while ($arRest = $rsRest->GetNext()) {
        //$str = '';
        // get photo
        $photoKey = 0;
        $strPhoto = "\t\t<photos gallery-url='https://www.restoran.ru" . $arRest["DETAIL_PAGE_URL"] . "'>\n";
        $rsProp = CIBlockElement::GetProperty(
            $arRest["IBLOCK_ID"],
            $arRest["ID"],
            Array(),
            Array(
                "CODE" => "google_photo"
            )
        );
        while ($arProp = $rsProp->Fetch()) {
            if (intval($arProp["VALUE"]) > 0) {
                $arFilters = Array(
                    array("name" => "watermark", "position" => "br", "size" => "real", 'alpha_level' => '30', "file" => $_SERVER['DOCUMENT_ROOT'] . "/tpl/images/watermark1.png")
                );
                $arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array(), BX_RESIZE_IMAGE_PROPORTIONAL, true, $arFilters, 100);
                //$arPhoto = CFile::GetPath($arProp["VALUE"]);

                $strPhoto .= "\t\t\t<photo url='https://www.restoran.ru" . $arPhoto["src"] . "'";
                if ($arProp["DESCRIPTION"])
                    $strPhoto .= " alt='" . $arProp["DESCRIPTION"] . "'";
                $strPhoto .= " />\n";
                $photoKey++;
                if ($photoKey > 2)
                    break;
            }
        }
        $strPhoto .= "\t\t</photos>\n";
        //if($photoKey > 0)
        {
            // tmp date
            $tmpDate = strtotime($arRest["TIMESTAMP_X"]) * 1000;

            $str .= "\t<company>\n";
            $str .= "\t\t<company-id>" . $arRest["ID"] . "</company-id>\n";
            //$str .= "\t\t<book-mode>static</book-mode>\n";
            //$str .= "\t\t<company-id>".$arRest["ID"]."</company-id>\n";
            $name_temp = explode("/", $arRest["NAME"]);
            $name_temp = explode("(", $name_temp[0]);
            $arRest["NAME"] = $name_temp[0];
            $str .= "\t\t<name lang='ru'>" . htmlspecialchars($arRest["NAME"]) . "</name>\n";
            if ($arRest["TAGS"]) {
                $str .= "\t\t<name-other lang='ru'>" . htmlspecialchars($arRest["TAGS"]) . "</name-other>\n";
            }
//                                $str .= "\t\t<post-index></post-index>\n";
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "address"
                )
            );
            if ($arProp = $rsProp->Fetch())
                if ($arProp["VALUE"]) {
                    $address = str_replace($arIblock["NAME"] . ",", "", $arProp["VALUE"]);
                    $address = $arIblock["NAME"] . ", " . $address;
                    $str .= "\t\t<address lang='ru'>" . $address . "</address>\n";
                }
            $str .= "\t\t<country lang='ru'>Россия</country>\n";
            //$str .= "\t\t<add-url>http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."</add-url>\n";
            if ($arIblock["ID"] == 11)
                $str .= "\t\t<locality-name lang='ru'>город Москва</locality-name>\n";
            elseif ($arIblock["ID"] == 12)
                $str .= "\t\t<locality-name lang='ru'>город Санкт-Петербург</locality-name>\n";
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "out_city"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"]) {
                    $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                    if ($arPayment = $rsPayment->GetNext())
                        $str .= "\t\t<sub-locality-name>" . $arPayment["NAME"] . "</sub-locality-name>\n";
                }
            } else {
                $rsProp = CIBlockElement::GetProperty(
                    $arRest["IBLOCK_ID"],
                    $arRest["ID"],
                    Array(),
                    Array(
                        "CODE" => "area"
                    )
                );
                if ($arProp = $rsProp->Fetch()) {
                    if ($arProp["VALUE"]) {
                        $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                        if ($arPayment = $rsPayment->GetNext())
                            $str .= "\t\t\t<sub-locality-name>" . $arPayment["NAME"] . "</sub-locality-name>\n";
                    }
                }
            }


            $f = 0;
            $str_l = "";
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "lat"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"]) {
                    $str_l .= "\t\t\t<lat>" . $arProp["VALUE"] . "</lat>\n";
                    $f = 1;
                }
            }

            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "lon"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"]) {
                    $str_l .= "\t\t\t<lon>" . $arProp["VALUE"] . "</lon>\n";
                    $f = 1;
                }
            }
            if ($f == 1) {
                $str .= "\t\t<coordinates>\n";
                $str .= $str_l;
                $str .= "\t\t</coordinates>\n";
            }

            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "phone"
                )
            );
            $is_tel = 0;
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"]) {
                    $reg = "/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                    preg_match_all($reg, $arProp["VALUE"], $matches);
                    $TELs = array();
                    for ($p = 1; $p < 5; $p++) {
                        foreach ($matches[$p] as $key => $v) {
                            //if($p==1 && $v!="") $TELs[$key].="+7";
                            $TELs[$key] .= $v;
                        }
                    }
                    foreach ($TELs as $key => $T) {
                        if (strlen($T) > 7) {
                            $TELs[$key] = "+7 (" . substr($T, 0, 3) . ") " . substr($T, 3, 3) . "-" . substr($T, 6, 2) . "-" . substr($T, 8, 2);
                        } else {
                            unset($TELs[$key]);
                        }
                    }
                    foreach ($TELs as $t) {
                        if ($t != "+7 (495) 988-26-56" && $t != "+7 (499) 988-26-56" && $t != "+7 (812) 740-18-20") {
                            $str .= "\t\t<phone>\n";
                            $str .= "\t\t\t<ext/>\n\t\t\t<type>phone</type>\n\t\t\t<number>" . $t . "</number>\n\t\t\t<info/>\n";
                            $str .= "\t\t</phone>\n";
                            $is_tel++;
                        }
                    }
                }
            }
            if ($is_tel == 0) {
                $rsProp = CIBlockElement::GetProperty(
                    $arRest["IBLOCK_ID"],
                    $arRest["ID"],
                    Array(),
                    Array(
                        "CODE" => "real_tel"
                    )
                );
                if ($arProp = $rsProp->Fetch()) {
                    if ($arProp["VALUE"]) {
                        $reg = "/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                        preg_match_all($reg, $arProp["VALUE"], $matches);
                        $TELs = array();
                        for ($p = 1; $p < 5; $p++) {
                            foreach ($matches[$p] as $key => $v) {
                                //if($p==1 && $v!="") $TELs[$key].="+7";
                                $TELs[$key] .= $v;
                            }
                        }
                        foreach ($TELs as $key => $T) {
                            if (strlen($T) > 7) {
                                $TELs[$key] = "+7 (" . substr($T, 0, 3) . ") " . substr($T, 3, 3) . "-" . substr($T, 6, 2) . "-" . substr($T, 8, 2);
                            } else {
                                unset($TELs[$key]);
                            }
                        }
                        foreach ($TELs as $t) {
                            if ($t != "+7 (495) 988-26-56" && $t != "+7 (499) 988-26-56" && $t != "+7 (812) 740-18-20") {
                                $str .= "\t\t<phone>\n";
                                $str .= "\t\t\t<ext/>\n\t\t\t<type>phone</type>\n\t\t\t<number>" . $t . "</number>\n\t\t\t<info/>\n";
                                $str .= "\t\t</phone>\n";
                            }
                        }
                    }
                }
            }
            //Email
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "email"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"])
                    $str .= "\t\t<email>" . strtolower($arProp["VALUE"]) . "</email>\n";
            }
            //Site
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "site"
                )
            );
            $tmpWebsite = array();
            if ($arProp = $rsProp->Fetch())
                if ($arProp["VALUE"]) {
                    if (strstr($arProp["VALUE"], ','))
                        $tmpWebsite = explode(",", $arProp["VALUE"]);
                    elseif (strstr($arProp["VALUE"], ';'))
                        $tmpWebsite = explode(";", $arProp["VALUE"]);
                    else
                        $tmpWebsite[0] = $arProp["VALUE"];

                    $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);
                }
            if ($tmpWebsite[0]) {
                $tmpWebsite[0] = str_replace("@", "", $tmpWebsite[0]);
                $tmpWebsite[0] = str_replace("–", "-", $tmpWebsite[0]);
                if (substr_count($tmpWebsite[0], ".restoran.ru")) {
                    //$str .= "\t\t<url></url>\n";
                } else
                    $str .= "\t\t<url>" . "http://" . trim($tmpWebsite[0]) . "</url>\n";
            }
            $str .= "\t\t<info-page>https://www.restoran.ru" . $arRest["DETAIL_PAGE_URL"] . "</info-page>\n";
            //Working time
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "opening_hours"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"]) {
                    $arProp["VALUE"] = html_entity_decode($arProp["VALUE"]);
                    $arProp["VALUE"] = htmlspecialchars_decode($arProp["VALUE"]);
                    $arProp["VALUE"] = stripslashes($arProp["VALUE"]);
                    $str .= "\t\t<working-time lang='ru'>" . htmlspecialchars($arProp["VALUE"]) . "</working-time>\n";
                    $work = htmlspecialchars($arProp["VALUE"]);
                }
            }

            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "type"
                )
            );
            $rubric_id_arr = array();
            $yandex_type_arr = array();
            while ($arProp = $rsProp->Fetch()) {
                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                if ($arPayment = $rsPayment->Fetch()) {
                    if (in_array($arPayment["NAME"], $prop_type)) {
                        $rubric_id = str_replace($prop_type, $prop_type_yandex_type_number, $arPayment["NAME"]);
                        if(!in_array($rubric_id,$rubric_id_arr)){
                            $rubric_id_arr[] = $rubric_id;
                        }
                    }
                    if (in_array($arPayment["NAME"], $prop_type_for_type)) {
                        $type_code = str_replace($prop_type_for_type, $prop_type_yandex_type, $arPayment["NAME"]);
                        if(!in_array($type_code,$yandex_type_arr)){
                            $yandex_type_arr[] = $type_code;
                        }
                    }
                }
            }
            if ($rubric_id_arr[0]){
                foreach ($rubric_id_arr as $type2_value) {
                    $str .= "\t\t<rubric-id>" . $type2_value . "</rubric-id>\n";
                }
            }
            else
                $str .= "\t\t<rubric-id>184106394</rubric-id>\n";



            $str .= "\t\t<actualization-date>" . $tmpDate . "</actualization-date>\n";

            $str .= $strPhoto;

            //Wi-fi

            $str .= "\t\t<feature-unit name='money' value='rub'/>\n";

            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "wi_fi"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"] == "Да")
                    $str .= "\t\t<feature-boolean name='wi_fi' value='1'/>\n";

            }

            //business_lunch
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "business_lunch"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp["VALUE"])
                    $str .= "\t\t<feature-boolean name='business_lunch' value='1'/>\n";
            }


            $arProp = array();
            $feature = "";
            $feature_multiple = array();
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "features"
                )
            );
            while ($arProp = $rsProp->Fetch()) {
                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                if ($arPayment = $rsPayment->Fetch()) {
                    if (in_array($arPayment["NAME"], $prop_features_bool)) {
                        $feature = str_replace($prop_features_bool, $prop_features_yandex_bool, $arPayment["NAME"]);
                        if($feature){
                            $str .= "\t\t<feature-boolean  name='".$feature."' value='1'/>\n";
                        }
                    }
                    if (in_array($arPayment["NAME"], $prop_features)) {
                        $feature_multiple[] = str_replace($prop_features, $prop_features_yandex, $arPayment["NAME"]);
                    }
                }
            }


            /**средний счёт**/
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "average_bill"
                )
            );
            if ($arProp = $rsProp->GetNext()) {
                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                if ($arPayment = $rsPayment->Fetch()) {
                    if (in_array($arPayment["NAME"], $av_bil)) {
                        $kitchen = str_replace($av_bil, $av_bil_yandex_from_to, $arPayment["NAME"]);
                        $avarage_bill = str_replace($av_bil, $av_bil_yandex, $arPayment["NAME"]);
                        $avarage_bill_from_to = explode('-',$kitchen);
                        $str .= "\t\t<feature-enum-single  name='price_category' value='".$avarage_bill."' />\n";
                        $str .= "\t\t<feature-range-in-units-single  name='average_bill2' unit='money' unit-value='rub' from='".$avarage_bill_from_to[0]."' to='".$avarage_bill_from_to[1]."' />\n";
                    }
                }
            }


            /**кухня**/
            $arProp = array();
            $kitchen = "";
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "kitchen"
                )
            );
            while ($arProp = $rsProp->GetNext()) {
                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                if ($arPayment = $rsPayment->Fetch()) {
                    if (in_array($arPayment["NAME"], $prop_kitchen)) {
                        $kitchen = str_replace($prop_kitchen, $prop_kitchen_yandex, $arPayment["NAME"]);
                        $str .= "\t\t<feature-enum-multiple  name='type_cuisine' value='" . $kitchen . "'/>\n";
                    }
                }
            }

            /**тип заведения**/
            foreach ($yandex_type_arr as $type_val){
                if($type_val){
                    $str .= "\t\t<feature-enum-multiple  name='type_public_catering' value='" . $type_val . "'/>\n";
                }
            }

            /**оплата картой**/
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "credit_cards"
                )
            );
            if ($arProp = $rsProp->Fetch()) {
                if ($arProp['VALUE']) {
                    $str .= "\t\t<feature-boolean  name='payment_by_credit_card' value='1'/>\n";
                }
            }


            /**особенности заведения**/

            foreach ($feature_multiple as $feature_val){
                if($feature_val){
                    if($feature_val=='restaurant_in_the_hotel'){//нет такого типа, но есть особенность
                        $str .= "\t\t<feature-enum-multiple  name='type_public_catering' value='" . $feature_val . "'/>\n";
                    }
                    else {
                        $str .= "\t\t<feature-enum-multiple  name='features_institution' value='" . $feature_val . "'/>\n";
                    }
                }
            }



            /**бесплатная парковка**/

            $arProp = array();
            $rsProp = CIBlockElement::GetProperty(
                $arRest["IBLOCK_ID"],
                $arRest["ID"],
                Array(),
                Array(
                    "CODE" => "parking"
                )
            );
            while ($arProp = $rsProp->Fetch()) {
                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                if ($arPayment = $rsPayment->Fetch()) {
                    if ($arPayment["NAME"]=='Неорганизованная бесплатная'||$arPayment["NAME"]=='Охраняемая бесплатная') {
                        $str .= "\t\t<feature-enum-multiple  name='features_institution' value='free_parking1'/>\n";
                    }
                }
            }





            $str .= "\t</company>\n";
        }
    }
}
$str .= "<xi:include href='https://www.restoran.ru/yandex/known-features_food.xml'/>";
$str .= "</companies>";
$time_end = microtime_float();
$time = $time_end - $time_start;
echo $time . "s.\n";

file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/yandex/yandex_upload.xml", $str, FILE_APPEND | LOCK_EX);
?>
