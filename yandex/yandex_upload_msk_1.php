<?
$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
//@ini_set("memory_limit", "256M");
//if(!$USER->IsAdmin())
  //  return false;

CModule::IncludeModule("iblock");
function getdaynum($a)
{
    if (substr_count($a, "будни")>0)
        return "12345";
    if (substr_count($a, "выходные")>0)
        return "67";
    if (substr_count($a, "пн-чт")>0)
        return "1234";
    if (substr_count($a, "пн-пт")>0)
        return "12345";
    if (substr_count($a, "пт-сб")>0)
        return "56";
    if (substr_count($a, "пт,сб")>0)
        return "56";
    if (substr_count($a, "сб-вс")>0)
        return "67";
    if (substr_count($a, "сб,вс")>0)
        return "67";
    if (substr_count($a, "пн")>0)
        return "1";
    if (substr_count($a, "вт")>0)
        return "2";
    if (substr_count($a, "ср")>0)
        return "3";
    if (substr_count($a, "чт")>0)
        return "4";
    if (substr_count($a, "пт")>0)
        return "5";
    if (substr_count($a, "сб")>0)
        return "6";
    if (substr_count($a, "вс")>0)
        return "7";    
    return "1234567";
}
function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$av_bil = Array("до 1000р","1000-1500р.","1500-2000р","2000-3000р","от 3000р");
$av_bil_yandex = Array("fivehundred_thousand","thousand_thousandfivehundred","thousandfivehundred_twothousandfivehundred","thousandfivehundred_twothousandfivehundred","over_twothousandfivehundred");
	

//Тип ресторана
$prop_type = Array("Банкетный зал","Бар","Караоке-бар","Кафе","Кейтеринг","Кофейня/Кондитерская","Паб","Ресторан","Суши-бар","Чайный ресторан");
$prop_type_yandex = Array("restaurant","bar","bar","cafe_type","restaurant","coffee_house","pub","restaurant","fish_restaurant","cafe_type");
//$prop_type_yandex_type = Array("184106394","184106384","184106384","184106390","184106394","184106390","184106384","184106394","184106384","184106390");
$prop_type_yandex_type = Array("184108315","184106384","19244809283","184106390","184106394","184106390","184106384","184106394","1387788996","184106390");

//Кухня
$prop_kitchen = Array("Австралийская","Австрийская","Авторская","Азербайджанская","Азиатская","Американская","Английская","Арабская",
                      "Аргентинская","Армянская","Белорусская","Болгарская","Бразильская",
                      "Восточная","Вьетнамская","Грузинская","Домашняя","Еврейская","Европейская","Индийская",
                      "Испанская","Итальянская","Кавказская","Китайская","Корейская","Кубинская","Латиноамериканская","Ливанская","Мясная",
                      "Мексиканская","Немецкая","Паназиатская","Рыбная","Русская","Средиземноморская","Тайская","Татарская","Тибетская","Турецкая","Узбекская",
                      "Украинская","Французская","Фьюжн","Чешская","Югославская","Японская");
$prop_kitchen_yandex = Array("australia_cuisine","austrian_cuisine","authors_cuisine","azerbaijani_cuisine","asian_cuisine","american_cuisine","english_cuisine","arabian_cuisine",
                             "argentine_cuisine","armenian_cuisine","belarusian_cuisine","bulgarian_cuisine","brazilian_cuisine",
                            "middle_eastern_cuisine","vietnamese_cuisine","georgian_cuisine","home_cuisine","jewish_cuisine","european_cuisine","indian_cuisine",
                             "spanish_cuisine","italian_cuisine","caucasian_cuisine","chinese_cuisine","korean_cuisine","cuban_cuisine","latin_american_cuisine",
                            "lebanese_cuisine","meat_cuisine","mexican_cuisine",
                            "german_cuisine","pan_asian_cuisine","fish_cuisine","russian_cuisine","mediterranean_cuisine","thai_cuisine","tatar_cuisine","tibetan_cuisine","turkish_cuisine",
                            "uzbek_cuisine","ukrainian_cuisine","french_cuisine","fusion_cuisine","czech_cuisine","yugoslav_cuisine","japanese_cuisine");

//$prop_credit = Array("Master Card / Eurocard","Diners Club","Union","Visa","Maestro","American Express");
//$prop_credit_yandex = Array("mastercard","diners_club_international","union_card","visa","maestro","american_express");
//
//$prop_music = Array("DJ","Фоновая","Джаз","Поп/рок","Живая музыка","Классика","Хиты 80-х, 90-х");
//$prop_music_yandex = Array("djs_music","background_music","jazz","pop_music","live_music","classical_music","80-90th-music");

// start execution
$time_start = microtime_float();

//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/yandex_upload.xml", '', LOCK_EX);

$str = "<companies xmlns:xi='http://www.w3.org/2001/XInclude' version='1.0'>\n";

//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/yandex_upload.xml", $str, FILE_APPEND | LOCK_EX);
$str = "";
$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
                "ID" => Array(11,12)
	),
	false
);
echo "<table>";

while($arIblock = $rsIblock->Fetch()) {
	$rsRest = CIBlockElement::GetList(
		Array("rand" => "ASC"),
		Array(
			"ACTIVE" => "Y",                        
			"IBLOCK_ID" => $arIblock["ID"],
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
                        "!PROPERTY_photos" => false,
                        "!PROPERTY_lat" => false,
                        ">PROPERTY_lat" => 0,
                        "!PROPERTY_sleeping_rest_VALUE" => "Да",                        
                        "PROPERTY_working_VALUE" => "Да",
                        "!ID"=>Array(387733,388019),
//                        "ID" => 1237781
			//"!PROPERTY_google_photo" => false
		),
		false,
		false,//Array("nTopCount"=>10),
		Array("ID","TIMESTAMP_X", "NAME", "TAGS","DETAIL_PAGE_URL","DETAIL_TEXT", "DATE_CREATE")
	);	        
	while($arRest = $rsRest->GetNext()) {            
                //$str = '';
		// get photo
		$photoKey = 0;
		$strPhoto = "\t\t<photos gallery-url='http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."'>\n";
		$rsProp = CIBlockElement::GetProperty(
			$arRest["IBLOCK_ID"],
			$arRest["ID"],
			Array(),
			Array(
				"CODE" => "google_photo"
			)
		);
                $s = $rsProp->SelectedRowsCount();
                ?>
<tr>
    <td><?=$arRest['NAME']?></td>
    <td><?=$s?></td>    
</tr>
<?
                continue;
		while($arProp = $rsProp->Fetch()) {
			if(intval($arProp["VALUE"]) > 0) {                                                        
                            $arFilters = Array(
                                array("name" => "watermark", "position" => "br", "size"=>"real",'alpha_level'=>'30', "file"=>$_SERVER['DOCUMENT_ROOT']."/tpl/images/watermark1.png")
                            );
				$arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array(), BX_RESIZE_IMAGE_PROPORTIONAL, true,$arFilters,100);                                  
				//$arPhoto = CFile::GetPath($arProp["VALUE"]);                                  
                                
				$strPhoto .= "\t\t\t<photo url='http://www.restoran.ru".$arPhoto["src"]."'";
                                if ($arProp["DESCRIPTION"])
                                    $strPhoto .= " alt='".$arProp["DESCRIPTION"]."'";
                                $strPhoto .= " />\n";                                
				$photoKey++;
				if($photoKey > 2)
					break;
			}
		}
                $strPhoto .= "\t\t</photos>\n";
		//if($photoKey > 0) 
                    {
			// tmp date
			$tmpDate = strtotime($arRest["TIMESTAMP_X"])*1000;
	
			$str .= "\t<company id='".$arRest["ID"]."'>\n";
				$str .= "\t\t<book-mode>static</book-mode>\n";
				//$str .= "\t\t<company-id>".$arRest["ID"]."</company-id>\n";
                                $name_temp = explode("/",$arRest["NAME"]);
                                $name_temp = explode("(",$name_temp[0]);
                                $arRest["NAME"] = $name_temp[0];
				$str .= "\t\t<name lang='ru'>".htmlspecialchars($arRest["NAME"])."</name>\n";
                                if ($arRest["TAGS"])
                                {
                                    $str .= "\t\t<name-other lang='ru'>".htmlspecialchars($arRest["TAGS"])."</name-other>\n";
                                }
                                $str .= "\t\t<post-index></post-index>\n";
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "address"
					)
				);
				if($arProp = $rsProp->Fetch())
					if($arProp["VALUE"]) {
						$address = str_replace($arIblock["NAME"].",", "", $arProp["VALUE"]);
						$address = $arIblock["NAME"].", ".$address;
						$str .= "\t\t<address lang='ru'>".$address."</address>\n";
					}
                                $str .= "\t\t<country lang='ru'>Россия</country>\n";               
                                //$str .= "\t\t<add-url>http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."</add-url>\n";
                                if ($arIblock["ID"]==11)
                                    $str .= "\t\t<locality-name lang='ru'>город Москва</locality-name>\n";				                       
                                elseif ($arIblock["ID"]==12)
                                    $str .= "\t\t<locality-name lang='ru'>город Санкт-Петербург</locality-name>\n";				                       
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "out_city"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                        {
                                            $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                if($arPayment = $rsPayment->GetNext())   
						$str .= "\t\t<sub-locality-name>".$arPayment["NAME"]."</sub-locality-name>\n";
                                        }
                                }
                                else
                                {
                                    $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "area"
					)
                                    );
                                    if($arProp = $rsProp->Fetch())
                                    {
                                            if($arProp["VALUE"])
                                            {
                                                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                if($arPayment = $rsPayment->GetNext())   
                                                    $str .= "\t\t\t<sub-locality-name>".$arPayment["NAME"]."</sub-locality-name>\n";
                                            }
                                    }
                                }   
                                        
                                
                                $f = 0;                                
                                $str_l = "";
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lat"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                        {
						$str_l .= "\t\t\t<lat>".$arProp["VALUE"]."</lat>\n";
                                                $f = 1;
                                        }
                                }
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lon"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
					{
                                            $str_l .= "\t\t\t<lon>".$arProp["VALUE"]."</lon>\n";
                                            $f = 1;
                                        }
                                }
                                if ($f == 1)
                                {
                                    $str .= "\t\t<coordinates>\n";
                                    $str .= $str_l;
                                    $str .= "\t\t</coordinates>\n";
                                }
                                                                        
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "phone"
					)
				);                     
                                $is_tel = 0;
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                        {
                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                            preg_match_all($reg, $arProp["VALUE"], $matches);
                                            $TELs=array();
                                            for($p=1;$p<5;$p++){
                                                    foreach($matches[$p] as $key=>$v){
                                                            //if($p==1 && $v!="") $TELs[$key].="+7";
                                                            $TELs[$key].=$v;
                                                    }	
                                            }
                                            foreach($TELs as $key => $T){
                                                    if(strlen($T)>7) { 
                                                            $TELs[$key] = "+7 (".substr($T, 0,3).") ".substr($T,3,3)."-".substr($T,6,2)."-".substr($T,8,2);                                                        
                                                    }
                                                    else {unset($TELs[$key]);}
                                            }
                                            foreach($TELs as $t)
                                            {
                                                if ($t!="+7 (495) 988-26-56"&&$t!="+7 (499) 988-26-56"&&$t!="+7 (812) 740-18-20")
                                                {
                                                    $str .= "\t\t<phone>\n";
                                                    $str .= "\t\t\t<ext/>\n\t\t\t<type>phone</type>\n\t\t\t<number>".$t."</number>\n\t\t\t<info/>\n";
                                                    $str .= "\t\t</phone>\n";
                                                    $is_tel++;
                                                }
                                            }
                                        }
                                }
                                if ($is_tel==0)
                                {
                                    $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "real_tel"
					)
                                    );                                                         
                                    if($arProp = $rsProp->Fetch())
                                    {
                                        if($arProp["VALUE"])
                                        {
                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                            preg_match_all($reg, $arProp["VALUE"], $matches);
                                            $TELs=array();
                                            for($p=1;$p<5;$p++){
                                                    foreach($matches[$p] as $key=>$v){
                                                            //if($p==1 && $v!="") $TELs[$key].="+7";
                                                            $TELs[$key].=$v;
                                                    }	
                                            }
                                            foreach($TELs as $key => $T){
                                                    if(strlen($T)>7) { 
                                                            $TELs[$key] = "+7 (".substr($T, 0,3).") ".substr($T,3,3)."-".substr($T,6,2)."-".substr($T,8,2);                                                        
                                                    }
                                                    else {unset($TELs[$key]);}
                                            }
                                            foreach($TELs as $t)
                                            {
                                                if ($t!="+7 (495) 988-26-56"&&$t!="+7 (499) 988-26-56"&&$t!="+7 (812) 740-18-20")
                                                {
                                                    $str .= "\t\t<phone>\n";
                                                    $str .= "\t\t\t<ext/>\n\t\t\t<type>phone</type>\n\t\t\t<number>".$t."</number>\n\t\t\t<info/>\n";
                                                    $str .= "\t\t</phone>\n";                                                   
                                                }
                                            }
                                        }
                                    }
                                }
                                //Email
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "email"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                            $str .= "\t\t<email>".strtolower($arProp["VALUE"])."</email>\n";
                                }
                                //Site
                                $rsProp = CIBlockElement::GetProperty(
                                        $arRest["IBLOCK_ID"],
                                        $arRest["ID"],
                                        Array(),
                                        Array(
                                                "CODE" => "site"
                                        )
                                );
                                $tmpWebsite = array();
                                if($arProp = $rsProp->Fetch())
                                        if($arProp["VALUE"]) {
                                            if(strstr($arProp["VALUE"], ','))
                                                $tmpWebsite = explode(",", $arProp["VALUE"]);
                                            elseif(strstr($arProp["VALUE"], ';'))
                                                $tmpWebsite = explode(";", $arProp["VALUE"]);
                                            else
                                                $tmpWebsite[0] = $arProp["VALUE"];

                                            $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);
                                        }
                                if ($tmpWebsite[0])
                                {
                                    $tmpWebsite[0] = str_replace("@","", $tmpWebsite[0]);
                                    $tmpWebsite[0] = str_replace("–","-", $tmpWebsite[0]);                                    
                                    if (substr_count($tmpWebsite[0], ".restoran.ru"))
                                    {
                                        //$str .= "\t\t<url></url>\n";                                
                                    }
                                    else
                                        $str .= "\t\t<url>"."http://".trim($tmpWebsite[0])."</url>\n";                                
                                }
                                $str .= "\t\t<info-page>http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."</info-page>\n";
                                //Working time
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "opening_hours"
					)
				);
                                if($arProp = $rsProp->Fetch())
                                {
                                    if($arProp["VALUE"])
                                    {
                                        $arProp["VALUE"] = html_entity_decode($arProp["VALUE"]);
                                        $arProp["VALUE"]  = htmlspecialchars_decode($arProp["VALUE"] );                                        
                                        $arProp["VALUE"]  = stripslashes($arProp["VALUE"]);                                        
                                        $str .= "\t\t<working-time lang='ru'>".htmlspecialchars($arProp["VALUE"])."</working-time>\n";
                                        $work = htmlspecialchars($arProp["VALUE"]);                                                                                                                                                                
                                    }
                                }
                                $type2 = array();
                                while($arProp = $rsProp->GetNext()) {
                                        $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                        if($arPayment = $rsPayment->Fetch())
                                        {
                                            if (in_array($arPayment["NAME"], $prop_type))
                                            {
                                                $type = str_replace($prop_type, $prop_type_yandex, $arPayment["NAME"]);
                                                $type2[] = str_replace($prop_type, $prop_type_yandex_type,$arPayment["NAME"]);								                                                
                                            }								
                                        }
                                }
                                if ($type2[0])
                                    $str .= "\t\t<rubric-id>".$type2[0]."</rubric-id>\n";
                                else
                                    $str .= "\t\t<rubric-id>184106394</rubric-id>\n";
                                $str .= "\t\t<actualization-date>".$tmpDate."</actualization-date>\n";
                                if ($arRest["DETAIL_TEXT"])
                                {
                                    $obParser = new CTextParser;                                                                        
                                    $arRest["DETAIL_TEXT"] = str_replace("&ndash;","-", $arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = str_replace("&nbsp;"," ", $arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = str_replace("&laquo;","&quot;", $arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = str_replace("&raquo;","&quot;", $arRest["DETAIL_TEXT"]);
//                                    $arRest["DETAIL_TEXT"] = html_entity_decode($arRest["DETAIL_TEXT"]);
//                                    $arRest["DETAIL_TEXT"]  = htmlspecialchars($arRest["DETAIL_TEXT"] );                                        
//                                    $arRest["DETAIL_TEXT"]  = stripslashes($arRest["DETAIL_TEXT"]);                                        
                                    $arRest["DETAIL_TEXT"] = strip_tags($arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = substr($arRest["DETAIL_TEXT"], 0,255)."...";
                                    $str .= "\t\t<description lang='ru'>".htmlspecialchars($arRest["DETAIL_TEXT"])."</description>\n";
                                }
                                $str .= $strPhoto;
                                //Отзывы
                                $reviews = "";
                                if ($arIblock["IBLOCK_ID"]==11)
                                    $rev_IB = 49;
                                elseif($arIblock["IBLOCK_ID"]==12)
                                    $rev_IB = 115;
                                $rev_res = CIBlockElement::GetList(Array("id"=>"desc"),Array("IBLOCK_ID"=>$rev_IB,"ACTIVE"=>"Y","!DETAIL_TEXT"=>false,"!DETAIL_TEXT"=>"","PROPERTY_ELEMENT"=>$arRest["ID"]),false,Array("nTopCount"=>3));
                                while ($arRev = $rev_res->Fetch())
                                {
                                    $time = date("Y-m-d H:i:s",strtotime($arRev["TIMESTAMP_X"]));
                                    $time = str_replace(" ","T",$time);
                                    $reviews .= "\t\t\t<review>\n";
                                    $reviews .= "\t\t\t\t<locale>ru</locale>\n";
                                    $reviews .= "\t\t\t\t<type>biz</type>\n";
                                    $reviews .= "\t\t\t\t<url>http://www.restoran.ru/msk/opinions?tid=".$arRev["ID"]."</url>\n";
                                                                                                          
                                    $arRev["PREVIEW_TEXT"] = str_replace("&ndash;","-", $arRev["PREVIEW_TEXT"]);
                                    $arRev["PREVIEW_TEXT"] = str_replace("&nbsp;"," ", $arRev["PREVIEW_TEXT"]);
                                    $arRev["PREVIEW_TEXT"] = html_entity_decode($arRev["PREVIEW_TEXT"]);
                                    $arRev["PREVIEW_TEXT"]  = htmlspecialchars($arRev["PREVIEW_TEXT"] );                                        
                                    $arRev["PREVIEW_TEXT"]  = stripslashes($arRev["PREVIEW_TEXT"]);                                        
                                    $arRev["PREVIEW_TEXT"] = strip_tags($arRev["PREVIEW_TEXT"]);
                                    $arRev["DETAIL_TEXT"] = strip_tags($arRev["DETAIL_TEXT"]);
                                    
                                    $reviews .= "\t\t\t\t<description>".$arRev["PREVIEW_TEXT"]."</description>\n";
                                    if ($arRev["DETAIL_TEXT"])
                                    {
                                        $reviews .= "\t\t\t\t<rating>".$arRev["DETAIL_TEXT"]."</rating>\n";
                                    }
                                    $reviews .= "\t\t\t\t<reviewer>\n";
                                    $reviews .= "\t\t\t\t\t<vcard>\n";
                                    $reviews .= "\t\t\t\t\t\t<fn>".htmlspecialchars($arRev["NAME"])."</fn>\n";
                                    $reviews .= "\t\t\t\t\t</vcard>\n";
                                    $reviews .= "\t\t\t\t</reviewer>\n";
                                    $reviews .= "\t\t\t\t<reviewsurl>http://www.restoran.ru/msk/opinions/</reviewsurl>\n";
                                    $reviews .= "\t\t\t\t<dtreviewed>".$time."</dtreviewed>\n";                
                                    $reviews .= "\t\t\t</review>\n";
                                }
                                if ($reviews)
                                {
                                    $str .= "\t\t<reviews>\n";
                                    $str .= $reviews;
                                    $str .= "\t\t</reviews>\n";
                                }
				$str .="\t\t<services>\n";
                                $str .="\t\t\t<service id=".'"1"'.">\n";
                                $str .="\t\t\t\t<title>забронировать столик</title>\n";
                                $str .="\t\t\t\t<schedules>\n";
                                $str .="\t\t\t\t\t<schedule res-id=".'"1"'.">\n";
                                //часы
                                    if (substr_count($work,";")>0)                                        
                                        $t = explode(";",$work);
                                    else
                                        $t = explode(",",$work);
                                    foreach ($t as $tt)
                                    {
                                        $daynum = getdaynum($tt);
                                        $str .= "\t\t\t\t\t\t<days-of-week mask='".$daynum."'>\n";
                                        preg_match("/([0-9]+)[:0-9]+[ ]?-[ ]?([0-9]+)[:0-9]+/", $tt, $time);
                                        
                                        if ($time[1])
                                        {                                            
                                            $a = $time[1]*1;
                                            $b = ($time[1]>$time[2])?($time[2]+24):$time[2];                                            
                                        }
                                        else
                                        {
                                            $a = "0";
                                            $b = "23";
                                        }

                                        for ($i=$a; $i<$b; $i++)
                                        {
                                            if ($i==$a)
                                            {
                                                $str .= "\t\t\t\t\t\t\t<time-frame from='";                                            
                                                if ($i>=24)
                                                    $str .=  "0".($i-24).":00'";
                                                elseif ($i<10)
                                                    $str .=  "0".$i.":00'";
                                                else
                                                    $str .=  $i.":00'";
                                            }
                                            if ($i==($b-1))
                                            {
                                                $str .=" to='";
                                                if ($i>=24)
                                                    $str .=  "0".($i-23).":00";
                                                elseif ($i<10)
                                                    $str .=  "0".$i.":00";
                                                else
                                                {
                                                    if (($i+1)==24)
                                                        $str .=  "00:00";
                                                    else
                                                        $str .=  ($i+1).":00";
                                                }
                                                $str .= "' />\n";
                                            }
                                        }
                                        $str .= "\t\t\t\t\t\t</days-of-week>\n";
                                    }                                    
                                    $str .="\t\t\t\t\t</schedule>\n";                               
                                    $str .="\t\t\t\t\t<schedule res-id=".'"2"'.">\n";
                                //часы
                                    if (substr_count($work,";")>0)                                        
                                        $t = explode(";",$work);
                                    else
                                        $t = explode(",",$work);
                                    foreach ($t as $tt)
                                    {
                                        $daynum = getdaynum($tt);                                        
                                        $str .= "\t\t\t\t\t\t<days-of-week mask='".$daynum."'>\n";
                                        preg_match("/([0-9]+)[:0-9]+[ ]?-[ ]?([0-9]+)[:0-9]+/", $tt, $time);
                                        
                                        if ($time[1])
                                        {                                            
                                            $a = $time[1];
                                            $b = ($time[1]>$time[2])?($time[2]+24):$time[2];                                            
                                        }
                                        else
                                        {
                                            $a = "0";
                                            $b = "23";
                                        }
                                        $a = (int)$a;
                                        $b = (int)$b;                                        
                                        for ($i=$a; $i<$b; $i++)
                                        {
                                            if ($i==$a)
                                            {                                                
                                                $str .= "\t\t\t\t\t\t\t<time-frame from='";                                            
                                                if ($i>=24)
                                                    $str .=  "0".($i-24).":00'";
                                                elseif ($i<10)
                                                    $str .=  "0".$i.":00'";
                                                else
                                                    $str .=  $i.":00'";
                                            }
                                            if ($i==($b-1))
                                            {
                                                $str .=" to='";
                                                if ($i>=24)
                                                    $str .=  "0".($i-23).":00";
                                                elseif ($i<10)
                                                    $str .=  "0".$i.":00";
                                                else
                                                {
                                                    if (($i+1)==24)
                                                        $str .=  "00:00";
                                                    else
                                                        $str .=  ($i+1).":00";
                                                }
                                                $str .= "' />\n";
                                            }
                                        }
                                        $str .= "\t\t\t\t\t\t</days-of-week>\n";                                        
                                    }
                                    $str .="\t\t\t\t\t</schedule>\n";                               
                                    $str .="\t\t\t\t</schedules>\n";                               
                                    $str .="\t\t\t</service>\n";
                                    $str .="\t\t</services>\n";
                                    $str .="\t\t<resources>\n";
                                    $str .="\t\t\t<resource id='1'>\n";
                                    $str .="\t\t\t\t<name>столик для некурящих</name>\n";                
                                    $str .="\t\t\t\t<attributes>\n";                
                                    $str .="\t\t\t\t\t<attribute name='smoke' value='false' />\n";                
                                    $str .="\t\t\t\t</attributes>\n";
                                    $str .="\t\t\t</resource>\n";
                                    $str .="\t\t\t<resource id='2'>\n";
                                    $str .="\t\t\t\t<name>столик для курящих</name>\n";                
                                    $str .="\t\t\t\t<attributes>\n";                
                                    $str .="\t\t\t\t\t<attribute name='smoke' value='true' />\n";                
                                    $str .="\t\t\t\t</attributes>\n";
                                    $str .="\t\t\t</resource>\n";
                                    $str .="\t\t</resources>\n";
                                    //Выдержка из меню
                                    
                                    $sql = "SELECT COUNT(a.ID) as ELEMENT_CNT,b.ID FROM b_iblock_element as a 
                                            JOIN b_iblock as b ON a.IBLOCK_ID = b.ID WHERE b.ACTIVE='Y' AND b.IBLOCK_TYPE_ID='rest_menu_ru' AND b.DESCRIPTION=".$DB->ForSql($arRest["ID"])." ";                                    
                                    $db_list = $DB->Query($sql);
                                    if($ar_result = $db_list->GetNext())
                                    {                                        
                                        if ($ar_result["ELEMENT_CNT"]>0)
                                        {
                                            $str .="\t\t<promo-prices prices-url='http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."menu/'>\n";
                                            $menu_id = $ar_result['ID'];
                                            $r = CIBlockElement::GetList(Array('tags'=>"desc","rand"=>"asc"),Array("IBLOCK_ID"=>$menu_id,">CATALOG_PRICE_1"=>0),false, Array("nTopCount"=>5),Array("ID","NAME","TAGS","SORT","CATALOG_GROUP_1"));
                                            while ($a = $r->Fetch())
                                            {                                                
                                                $a["NAME"] = html_entity_decode($a["NAME"]);
                                                $a["NAME"] = stripslashes($a["NAME"]);
                                                $a["NAME"] = htmlspecialchars($a["NAME"]);
                                                $a["NAME"] = str_replace("'", '"', $a["NAME"]);
                                                $a["NAME"] = str_replace("/", "|", $a["NAME"]);
                                                $a["NAME"] = str_replace("\/", "|", $a["NAME"]);
                                                $str .="\t\t\t<promo-price name='".$a["NAME"]."' value='".$a["CATALOG_PRICE_1"]."' currency='RUB'></promo-price>\n";
                                            }                                                                                        
                                            $str .="\t\t</promo-prices>\n";
                                        }
                                    }                                    
                                //Wi-fi
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "wi_fi"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                {
                                	if($arProp["VALUE"]=="Да")	
                                            $str .= "\t\t<feature-boolean name='wi_fi' value='1'/>\n";                                                                                                                                                                                
                                        
                                }
                                
                                 //business_lunch
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "business_lunch"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                {
                                	if($arProp["VALUE"])	
                                            $str .= "\t\t<feature-boolean name='business_lunch' value='1'/>\n";       
                                }
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "average_bill"
							)
						);
						if($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $av_bil))
                                                            {
                                                                $kitchen = str_replace($av_bil, $av_bil_yandex, $arPayment["NAME"]);
								$str .= "\t\t<feature-enum-single  name='average_check' value='".$kitchen."'/>\n";
                                                            }								
                                                        }								
						}
                                        
                                                $arProp = array();
                                                $kitchen= "";
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kitchen"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $prop_kitchen))
                                                            {
                                                                $kitchen = str_replace($prop_kitchen, $prop_kitchen_yandex, $arPayment["NAME"]);
								$str .= "\t\t<feature-enum-multiple  name='type_cuisine' value='".$kitchen."'/>\n";
                                                            }								
                                                        }								
						}
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "type"
							)
						);
                                                $type = array();
                                                $type2 = array();
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $prop_type))
                                                            {
                                                                $type = str_replace($prop_type, $prop_type_yandex, $arPayment["NAME"]);
                                                                $type2[] = str_replace($prop_type, $prop_type_yandex_type,$arPayment["NAME"]);								
								$str .= "\t\t<feature-enum-multiple  name='type_public_catering' value='".$type."'/>\n";
                                                            }								
                                                        }
						}
//                                                if ($type2[0])
//                                                    $str .= "\t\t<rubric-id>".$type2[0]."</rubric-id>\n";
//                                                else
//                                                    $str .= "\t\t<rubric-id>184106394</rubric-id>\n";
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "credit_cards"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $prop_credit))
                                                            {
                                                                $cred = str_replace($prop_credit, $prop_credit_yandex, $arPayment["NAME"]);
								$str .= "\t\t<feature-enum-multiple  name='accepted_credit_cards' value='".$cred."'/>\n";
                                                            }
                                                        }
						}
								
			$str .= "\t</company>\n";                                        
		}	                
	}        
        
}
echo "</table>";
$str .= "<xi:include href='http://www.restoran.ru/yandex/known-features_food.xml'/>";
$str .= "</companies>";
$time_end = microtime_float();
$time = $time_end - $time_start;
echo $time."s.\n";

//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/yandex_upload.xml", $str, FILE_APPEND | LOCK_EX);
?>
