<?
$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();?>

<?
//@ini_set("memory_limit", "256M");
if(!$USER->IsAdmin())
    return false;

CModule::IncludeModule("iblock");

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
//Тип ресторана        
$prop_type = Array("Банкетный зал","Бар","Караоке-бар","Кафе","Кейтеринг","Кофейня/Кондитерская","Паб","Ресторан","Суши-бар","Чайный ресторан");
$prop_type_yandex = Array("banquet_hall","bar","karaoke_bar","cafe_type","catering","coffee_house","pub","restaurant","sushi_bar","tea_house");
$prop_type_yandex_type = Array("184106394","184106384","184106384","184106390","184106394","184106390","184106384","184106394","1387788996","184106390");

//Кухня
$prop_kitchen = Array("Австралийская","Австрийская","Авторская","Азербайджанская","Азиатская","Американская","Английская","Арабская",
                      "Аргентинская","Армянская","Белорусская","Болгарская","Бразильская",
                      "Восточная","Вьетнамская","Грузинская","Домашняя","Еврейская","Европейская","Индийская",
                      "Испанская","Итальянская","Кавказская","Китайская","Корейская","Кубинская","Латиноамериканская","Ливанская","Мясная",
                      "Мексиканская","Немецкая","Паназиатская","Рыбная","Русская","Средиземноморская","Тайская","Татарская","Тибетская","Турецкая","Узбекская",
                      "Украинская","Французская","Фьюжн","Чешская","Югославская","Японская");
$prop_kitchen_yandex = Array("australia_cuisine","austrian_cuisine","authors_cuisine","azerbaijani_cuisine","asian_cuisine","american_cuisine","english_cuisine","arabian_cuisine",
                             "argentine_cuisine","armenian_cuisine","belarusian_cuisine","bulgarian_cuisine","brazilian_cuisine",
                            "middle_eastern_cuisine","vietnamese_cuisine","georgian_cuisine","home_cuisine","jewish_cuisine","european_cuisine","indian_cuisine",
                             "spanish_cuisine","italian_cuisine","caucasian_cuisine","chinese_cuisine","korean_cuisine","cuban_cuisine","latin_american_cuisine",
                            "lebanese_cuisine","meat_cuisine","mexican_cuisine",
                            "german_cuisine","pan_asian_cuisine","fish_cuisine","russian_cuisine","mediterranean_cuisine","thai_cuisine","tatar_cuisine","tibetan_cuisine","turkish_cuisine",
                            "uzbek_cuisine","ukrainian_cuisine","french_cuisine","fusion_cuisine","czech_cuisine","yugoslav_cuisine","japanese_cuisine");

$prop_credit = Array("Master Card / Eurocard","Diners Club","Union","Visa","Maestro","American Express");
$prop_credit_yandex = Array("mastercard","diners_club_international","union_card","visa","maestro","american_express");

$prop_music = Array("DJ","Фоновая","Джаз","Поп/рок","Живая музыка","Классика","Хиты 80-х, 90-х");
$prop_music_yandex = Array("djs_music","background_music","jazz","pop_music","live_music","classical_music","80-90th-music");

// start execution
$time_start = microtime_float();

file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/second/yandex_upload.xml", '', LOCK_EX);

$str = "<companies xmlns:xi='http://www.w3.org/2001/XInclude' version='2.1'>\n";

file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/second/yandex_upload.xml", $str, FILE_APPEND | LOCK_EX);
$str = '';
$rsIblock = CIBlock::GetList(
	Array("ID" => "ASC"),
	Array(
		"SITE_ID" => "s1",
		"TYPE" => "catalog",
                "ID" => Array(11,12)
	),
	false
);
while($arIblock = $rsIblock->Fetch()) {
	$rsRest = CIBlockElement::GetList(
		Array("rand" => "ASC"),
		Array(
			"ACTIVE" => "Y",
			"IBLOCK_ID" => $arIblock["ID"],
			"!SECTION_ID" => Array("33", "226", "104", "44170", "44509", "44510"),
                        "!PROPERTY_photos" => false,
                        "!PROPERTY_lat" => false,
                        ">PROPERTY_lat" => 0,
                        "!PROPERTY_sleeping_rest_VALUE" => "Да",                        
			/*"!PROPERTY_google_photo" => false*/
		),
		false,
		false,//Array("nTopCount"=>1),
		Array("ID","TIMESTAMP_X", "NAME", "TAGS","DETAIL_PAGE_URL","DETAIL_TEXT", "DATE_CREATE")
	);	
	while($arRest = $rsRest->GetNext()) {            
                //$str = '';
		// get photo
		$photoKey = 0;
		$strPhoto = "\t\t<photos gallery-url='http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."'>\n";
		$rsProp = CIBlockElement::GetProperty(
			$arRest["IBLOCK_ID"],
			$arRest["ID"],
			Array(),
			Array(
				"CODE" => "photos"
			)
		);
		while($arProp = $rsProp->Fetch()) {
			if(intval($arProp["VALUE"]) > 0) {                            
				//$arPhoto = CFile::ResizeImageGet($arProp["VALUE"], array(), BX_RESIZE_IMAGE_PROPORTIONAL, true,$watermark);                                  
				$arPhoto = CFile::GetPath($arProp["VALUE"]);                                  
                                
				$strPhoto .= "\t\t\t<photo url='http://".SITE_SERVER_NAME.$arPhoto."' ";
                                if ($arProp["DESCRIPTION"])
                                    $strPhoto .= "alt='".htmlspecialchars($arProp["DESCRIPTION"])."'";
                                $strPhoto .= " />\n";                                                                    
				$photoKey++;
				if($photoKey > 2)
					break;
			}
		}
                $strPhoto .= "\t\t</photos>\n";
		//if($photoKey > 0) 
                    {
			// tmp date
			$tmpDate = strtotime($arRest["TIMESTAMP_X"])*1000;
	
			$str .= "\t<company>\n";
				$str .= "\t\t<company-id>".$arRest["ID"]."</company-id>\n";
                                $name_temp = explode("/",$arRest["NAME"]);
                                $name_temp = explode("(",$name_temp[0]);
                                $arRest["NAME"] = $name_temp[0];
				$str .= "\t\t<name lang='ru'>".htmlspecialchars($arRest["NAME"])."</name>\n";
                                if ($arRest["TAGS"])
                                {
                                    $str .= "\t\t<name-other lang='ru'>".htmlspecialchars($arRest["TAGS"])."</name-other>\n";
                                }
                                //$str .= "\t\t<add-url>http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."</add-url>\n";
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "address"
					)
				);
				if($arProp = $rsProp->Fetch())
					if($arProp["VALUE"]) {
						$address = str_replace($arIblock["NAME"].",", "", $arProp["VALUE"]);
						$address = $arIblock["NAME"].", ".$address;
						$str .= "\t\t<address lang='ru'>".$address."</address>\n";
					}	
	
				$str .= "\t\t<country lang='ru'>Россия</country>\n";                                      
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "out_city"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                        {
                                            $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                if($arPayment = $rsPayment->GetNext())   
						$str .= "\t\t<sub-locality-name>".$arPayment["NAME"]."</sub-locality-name>\n";
                                        }
                                }
                                else
                                {
                                    $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "area"
					)
                                    );
                                    if($arProp = $rsProp->Fetch())
                                    {
                                            if($arProp["VALUE"])
                                            {
                                                $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                                if($arPayment = $rsPayment->GetNext())   
                                                    $str .= "\t\t\t<sub-locality-name>".$arPayment["NAME"]."</sub-locality-name>\n";
                                            }
                                    }
                                }   
                                        
                                
                                $f = 0;                                
                                $str_l = "";
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lat"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                        {
						$str_l .= "\t\t\t<lat>".$arProp["VALUE"]."</lat>\n";
                                                $f = 1;
                                        }
                                }
	
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "lon"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
					{
                                            $str_l .= "\t\t\t<lon>".$arProp["VALUE"]."</lon>\n";
                                            $f = 1;
                                        }
                                }
                                if ($f == 1)
                                {
                                    $str .= "\t\t<coordinates>\n";
                                    $str .= $str_l;
                                    $str .= "\t\t</coordinates>\n";
                                }
                                                                        
				$rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "phone"
					)
				);                                
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                        {
                                            $reg="/(?:\\+)?[78]?(?:\\s|-)*[\\(]?(\\d{3,4})?[\\)]?(?:\\s|-)*(\\d{1,3})+?(?:\\s|-)+?(\\d{1,3})+?(?:\\s|-)?(\\d{2,3})+?/";
                                            preg_match_all($reg, $arProp["VALUE"], $matches);
                                            $TELs=array();
                                            for($p=1;$p<5;$p++){
                                                    foreach($matches[$p] as $key=>$v){
                                                            //if($p==1 && $v!="") $TELs[$key].="+7";
                                                            $TELs[$key].=$v;
                                                    }	
                                            }
                                            foreach($TELs as $key => $T){
                                                    if(strlen($T)>7) { 
                                                            $TELs[$key] = "+7 (".substr($T, 0,3).") ".substr($T,3,3)."-".substr($T,6,2)."-".substr($T,8,2);                                                        
                                                    }
                                                    else {unset($TELs[$key]);}
                                            }
                                            foreach($TELs as $t)
                                            {
                                                $str .= "\t\t<phone>\n";
                                                $str .= "\t\t\t<ext/>\n\t\t\t<type>phone</type>\n\t\t\t<number>".$t."</number>\n\t\t\t<info/>\n";
                                                $str .= "\t\t</phone>\n";
                                            }
                                        }
                                }
                                
                                //Email
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "email"
					)
				);
				if($arProp = $rsProp->Fetch())
                                {
					if($arProp["VALUE"])
                                            $str .= "\t\t<email>".strtolower($arProp["VALUE"])."</email>\n";
                                }
                                //Site
                                $rsProp = CIBlockElement::GetProperty(
                                        $arRest["IBLOCK_ID"],
                                        $arRest["ID"],
                                        Array(),
                                        Array(
                                                "CODE" => "site"
                                        )
                                );
                                if($arProp = $rsProp->Fetch())
                                        if($arProp["VALUE"]) {
                                            if(strstr($arProp["VALUE"], ','))
                                                $tmpWebsite = explode(",", $arProp["VALUE"]);
                                            elseif(strstr($arProp["VALUE"], ';'))
                                                $tmpWebsite = explode(";", $arProp["VALUE"]);
                                            else
                                                $tmpWebsite[0] = $arProp["VALUE"];

                                            $tmpWebsite[0] = str_replace("http://", "", $tmpWebsite[0]);
                                        }
                                if ($tmpWebsite[0])
                                {
                                    $tmpWebsite[0] = str_replace("–", "-", $tmpWebsite[0]);
                                    if (substr_count($tmpWebsite[0], "restoran.ru")>0)
                                        $str .= "\t\t<url></url>\n";                                
                                    else
                                        $str .= "\t\t<url>"."http://".trim($tmpWebsite[0])."</url>\n";                                
                                }
                                    $str .= "\t\t<add-url>"."http://www.restoran.ru".$arRest["DETAIL_PAGE_URL"]."</add-url>\n";
                                        
                                //Working time
                                $rsProp = CIBlockElement::GetProperty(
					$arRest["IBLOCK_ID"],
					$arRest["ID"],
					Array(),
					Array(
						"CODE" => "opening_hours"
					)
				);
                                if($arProp = $rsProp->Fetch())
                                {
                                    if($arProp["VALUE"])
                                    {
                                        //$arProp["VALUE"] = html_entity_decode($arProp["VALUE"]);
                                        //$arProp["VALUE"]  = htmlspecialchars_decode($arProp["VALUE"] );                                        
                                        //$arProp["VALUE"]  = stripslashes($arProp["VALUE"]);                                        
                                        $str .= "\t\t<working-time lang='ru'>".htmlspecialchars($arProp["VALUE"])."</working-time>\n";
                                    }
                                }
                                $type = array();
                                $type2 = array();
                                while($arProp = $rsProp->GetNext()) {
                                        $rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
                                        if($arPayment = $rsPayment->Fetch())
                                        {
                                            if (in_array($arPayment["NAME"], $prop_type))
                                            {
                                                $type = str_replace($prop_type, $prop_type_yandex, $arPayment["NAME"]);
                                                $type2[] = str_replace($prop_type, $prop_type_yandex_type,$arPayment["NAME"]);								
                                                //$str .= "\t\t<feature-enum-multiple  name='type_public_catering' value='".$type."'/>\n";
                                            }								
                                        }
                                }
                                if ($type2[0])
                                    $str .= "\t\t<rubric-id>".$type2[0]."</rubric-id>\n";
                                else
                                    $str .= "\t\t<rubric-id>184106394</rubric-id>\n";
                                $str .= "\t\t<actualization-date>".$tmpDate."</actualization-date>\n";
                                if ($arRest["DETAIL_TEXT"])
                                {
                                    $obParser = new CTextParser;                                                                        
                                    $arRest["DETAIL_TEXT"] = str_replace("&ndash;","-", $arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = str_replace("&nbsp;"," ", $arRest["DETAIL_TEXT"]);
//                                    $arRest["DETAIL_TEXT"] = html_entity_decode($arRest["DETAIL_TEXT"]);
//                                    $arRest["DETAIL_TEXT"]  = htmlspecialchars_decode($arRest["DETAIL_TEXT"] );                                        
//                                    $arRest["DETAIL_TEXT"]  = stripslashes($arRest["DETAIL_TEXT"]);                                        
                                    $arRest["DETAIL_TEXT"] = strip_tags($arRest["DETAIL_TEXT"]);
                                    $arRest["DETAIL_TEXT"] = substr($arRest["DETAIL_TEXT"], 0,255)."...";
                                    $str .= "\t\t<description lang='ru'>".htmlspecialchars($arRest["DETAIL_TEXT"])."</description>\n";
                                }
					$str .= $strPhoto;
                                //Wi-fi
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "wi_fi"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                	if($arProp["VALUE"]=="Да")	
                                            $str .= "\t\t<feature-boolean name='wi_fi' value='1'/>\n";
                                
                                                                                
                                //karaoke
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "entertainment"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                	if($arProp["VALUE"]=="1290")	
                                            $str .= "\t\t<feature-boolean name='karaoke' value='1'/>\n"; 
                                        
                                //Sport_translating
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "entertainment"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                	if($arProp["VALUE"]=="2013")	
                                            $str .= "\t\t<feature-boolean name='sports_broadcasts' value='1'/>\n";                                        
                                        
                                //Breakfast
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "breakfast"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                	if($arProp["VALUE"]=="Y")	
                                            $str .= "\t\t<feature-boolean name='breakfast' value='1'/>\n";
                                
                                 //business_lunch
                                $rsProp = CIBlockElement::GetProperty(
                                                $arRest["IBLOCK_ID"],
                                                $arRest["ID"],
                                                Array(),
                                                Array(
                                                        "CODE" => "business_lunch"
                                                )
                                );
                                if($arProp = $rsProp->Fetch())
                                	if($arProp["VALUE"])	
                                            $str .= "\t\t<feature-boolean name='business_lunch' value='1'/>\n";       
                                 
                                        
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kitchen"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $prop_kitchen))
                                                            {
                                                                $kitchen = str_replace($prop_kitchen, $prop_kitchen_yandex, $arPayment["NAME"]);
								$str .= "\t\t<feature-enum-multiple  name='type_cuisine' value='".$kitchen."'/>\n";
                                                            }								
                                                        }								
						}
                                                $rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "type"
							)
						);
                                                $type = array();
                                                $type2 = array();
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $prop_type))
                                                            {
                                                                $type = str_replace($prop_type, $prop_type_yandex, $arPayment["NAME"]);
                                                                $type2[] = str_replace($prop_type, $prop_type_yandex_type,$arPayment["NAME"]);								
								$str .= "\t\t<feature-enum-multiple  name='type_public_catering' value='".$type."'/>\n";
                                                            }								
                                                        }
						}
//                                                if ($type2[0])
//                                                    $str .= "\t\t<rubric-id>".$type2[0]."</rubric-id>\n";
//                                                else
//                                                    $str .= "\t\t<rubric-id>184106394</rubric-id>\n";
						$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "credit_cards"
							)
						);
						while($arProp = $rsProp->GetNext()) {
							$rsPayment = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arPayment = $rsPayment->Fetch())
                                                        {
                                                            if (in_array($arPayment["NAME"], $prop_credit))
                                                            {
                                                                $cred = str_replace($prop_credit, $prop_credit_yandex, $arPayment["NAME"]);
								$str .= "\t\t<feature-enum-multiple  name='accepted_credit_cards' value='".$cred."'/>\n";
                                                            }
                                                        }
						}
		
						/*$rsProp = CIBlockElement::GetProperty(
							$arRest["IBLOCK_ID"],
							$arRest["ID"],
							Array(),
							Array(
								"CODE" => "kolichestvochelovek"
							)
						);
						if($arProp = $rsProp->GetNext())
							$rsSeating = CIBlockElement::GetByID($arProp["VALUE"]);
							if($arSeating = $rsSeating->Fetch())
								$str .= "\t\t<feature-numeric-single name='seats' value='".str_replace("&nbsp;", "", $arSeating["NAME"])."'/>\n";
                                                */                                                
			$str .= "\t</company>\n";                        
                        //usleep(200000);
		}	                
	}
        //sleep(5);
}
$str .= "\t<xi:include href='known-features_food_ru.xml'/>\n";
$str .= "</companies>";
$time_end = microtime_float();
$time = $time_end - $time_start;
echo $time."s.\n";

file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/second/yandex_upload.xml", $str, FILE_APPEND | LOCK_EX);
?>
