<?

if($argv[1] != 'pars_admin'){
    die();
}

$_SERVER["DOCUMENT_ROOT"]="/home/bitrix/www";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


$time_start = microtime_float();

file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/reviews/export-feed.xml", '', LOCK_EX);

$str = "<reviews>\n";

file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/reviews/export-feed.xml", $str, FILE_APPEND | LOCK_EX);
$str = "";


$rsIblock = CIBlock::GetList(
    Array("ID" => "ASC"),
    Array(
        "SITE_ID" => "s1",
        "TYPE" => "reviews",
        "ID" => Array(49,115)//
    ),
    false
);
while($arIblock = $rsIblock->Fetch()) {
    $rsRest = CIBlockElement::GetList(
        Array("created" => "DESC"),
        Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $arIblock["ID"],
        ),
        false,
        false,//Array("nTopCount"=>10)
        Array("ID", 'PREVIEW_TEXT', "DETAIL_TEXT", 'PROPERTY_plus', 'PROPERTY_minus','PROPERTY_COMMENTS',"DATE_CREATE", 'PROPERTY_ELEMENT', 'CREATED_BY', "NAME")
    );
    while($arRest = $rsRest->Fetch()) {
        if(!$arRest['CREATED_BY']){
            continue;
        }

        $res = CIBlockElement::GetByID($arRest['PROPERTY_ELEMENT_VALUE']);
        if(!$THIS_COMMENT_RESTAURANT_ARR = $res->GetNext()){
            continue;
        }

        if($arIblock["ID"]==49){
            $domain_root_url = 'https://www.restoran.ru';
        }
        elseif($arIblock["ID"]==115){
            $domain_root_url = 'https://spb.restoran.ru';
        }

        if($arIblock["ID"]==49){
            $domain_section_url = 'https://www.restoran.ru/msk/opinions/';
        }
        elseif($arIblock["ID"]==115){
            $domain_section_url = 'https://spb.restoran.ru/spb/opinions/';
        }

        $str .= "\t<review>\n";
            $str .= "\t\t<locale>ru</locale>\n";
            $str .= "\t\t<type>biz</type>\n";
            $str .= "\t\t<url>$domain_section_url?tid=".$arRest['ID']."</url>\n";
            $str .= "\t\t<description>".$arRest['PREVIEW_TEXT']."</description>\n";

            $plus_str = '';
            if($arRest['PROPERTY_plus_VALUE']){
                $plus_str = "\t\t\t<pro>".$arRest['PROPERTY_plus_VALUE']."</pro>\n";
                $str .= "\t\t<pros>$plus_str</pros>\n";
            }

            $minus_str = '';
            if($arRest['PROPERTY_minus_VALUE']){
                $plus_str = "\t\t\t<contra>".$arRest['PROPERTY_minus_VALUE']."</contra>\n";
                $str .= "\t\t<contras>$plus_str</contras>\n";
            }

            $str .= "\t\t<rating>".$arRest['DETAIL_TEXT']."</rating>\n";
            $str .= "\t\t<comments>".($arRest['PROPERTY_COMMENTS_VALUE']?$arRest['PROPERTY_COMMENTS_VALUE']:0)."</comments>\n";

            $str .= "\t\t<dtreviewed>".date('c',strtotime($arRest['DATE_CREATE']))."</dtreviewed>\n";


            if($arRest['PROPERTY_ELEMENT_CODE']){
                $str .= "\t\t<reviewsurl>".$domain_section_url."restaurants/".$arRest['PROPERTY_ELEMENT_CODE']."/</reviewsurl>\n";
            }

            $rsUser = CUser::GetByID($arRest['CREATED_BY']);

            $user_fields = '';
            if($arUser = $rsUser->Fetch()){
                if($arUser['NAME']){
                    $user_fields .= "\t\t\t\t<fn>".$arUser['NAME']."</fn>\n";
                }

                $user_fields .= "\n\t\t\t\t<url>".$domain_section_url."users/id".$arUser['ID']."/"."</url>\n";
                $user_fields .= "\t\t\t\t<email>".$arUser['EMAIL']."</email>\n";

                //
                $str .= "\t\t<reviewer>\n\t\t\t<vcard>".$user_fields."\t\t\t</vcard>\n\t\t</reviewer>\n";
            }

            //  about restaurant
            $restoran_fields = '';

            $restoran_fields .= "\n\t\t\t<fn>".$THIS_COMMENT_RESTAURANT_ARR['NAME']."</fn>";
            $restoran_fields .= "\n\t\t\t<category>restaurant</category>\n";


            $db_props = CIBlockElement::GetProperty($arIblock["ID"], $THIS_COMMENT_RESTAURANT_ARR['ID'], array("sort" => "asc"), Array("CODE"=>"address"));
            if($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $restoran_fields_address .= "\t\t\t\t\t<adr>".$ar_props['VALUE']."</adr>\n";
                    $restoran_fields_address .= "\t\t\t\t\t<street-address>".$ar_props['VALUE']."</street-address>\n";
                    $restoran_fields_address .= "\t\t\t\t\t<country-name>Россия</country-name>\n";
                    $restoran_fields_address .= "\t\t\t\t\t<locality>".$arIblock["NAME"]."</locality>\n";

                    $restoran_fields .= "\t\t\t\t<adrs>".$restoran_fields_address."</adrs>\n";
                }
            }

            $restoran_fields .= "\t\t\t\t<localurl>".$domain_root_url.$THIS_COMMENT_RESTAURANT_ARR["DETAIL_PAGE_URL"]."</localurl>\n";

            $str .= "\t\t<item>\n\t\t\t<vcard>".$restoran_fields."\t\t\t</vcard>\n\t\t</item>\n";









        $str .= "\t</review>\n";
    }
}

$str .= "</reviews>";

$time_end = microtime_float();
$time = $time_end - $time_start;
echo $time."s.\n";

file_put_contents($_SERVER["DOCUMENT_ROOT"]."/yandex/reviews/export-feed.xml", $str, FILE_APPEND | LOCK_EX);