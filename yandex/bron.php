<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
include('jsonrpc.php');

class JsonRpcServer
{
    public function book( $a )
    {                       
        global $USER;
        CModule::IncludeModule("iblock");                                 
        $el = new CIBlockElement;
        $PROP = array();
        $result = array();
        $a = (array)$a;
        $attr = (array)$a["attributes"];
        
        $PROP["yandexid"] = $a["bookId"];  
        $PROP["rest"] = $a["organizationId"];  
        //$PROP["guest"] = $arResult["arrVALUES"]["form_text_35"];
       

        $res = CIBlockElement::GetByID($a["organizationId"]);
        if ($ar = $res->Fetch())
        {
            $rest_iblock = $ar["IBLOCK_ID"];
            if ($ar["IBLOCK_ID"]==11)
                $SECTION=83822;
            elseif($ar["IBLOCK_ID"]==12)
                $SECTION=83823;                                    
        }        
        //$TEL=  substr($a["phone"], 1);
        $TEL = str_replace("+", "", $a["phone"]);
        if (substr($TEL, 0,1)=="7")
            $TEL = substr($TEL, 1);
        $PROP["user"]=$USER->GetID();
        $arrFilter=array("PROPERTY_TELEPHONE"=>$TEL, "IBLOCK_ID"=>104);
        $res = CIBlockElement::GetList(Array(), $arrFilter, false, false, false);
        if($ob = $res->GetNextElement())
        {
            $arFieldsu = $ob->GetFields();
            //такой телефон или email есть в базе клиентов
            $PROP["client"]=$arFieldsu["ID"];
        }
        else
        {
            //такого клиента нет в базе, нужно его добавить
            $PROP_client["SURNAME"] = $a["fullname"]; 
            $PROP_client["TELEPHONE"] = $TEL; 
            $PROP_client["EMAIL"] = $a["email"];       

            $PROP_client["user"] = $USER->GetID();

            //$PROP_client["TELEPHONE"] = str_replace(" ", "", $PROP_client["TELEPHONE"]);

            $arLoadProductArray_client = Array(
                    "IBLOCK_ID"=>104,
                    "IBLOCK_SECTION" => $SECTION,
                    "PROPERTY_VALUES"=> $PROP_client,
                    "NAME"           => $a["fullname"],
                    "ACTIVE"         => "Y",
                    "PREVIEW_TEXT"   => "Клиент создан автоматически",
            );

            if($CLIENT_ID = $el->Add($arLoadProductArray_client)){
                    $PROP["client"]=$CLIENT_ID;
            }

        }



        // }

        $PROP["new"] = 1549;
        $PROP["status"] = 1418;        
        $PROP["source"] = 2440;
        $PROP["guest"] = $attr["guestsCount"];
        
        $PROP["type"] = 1498;
        $a["dateTime"] = str_replace("T", "", $a["dateTime"]);
        $date = date("d.m.Y",strtotime($a["dateTime"]));
        $time = date("H:i",strtotime($a["dateTime"]));
                
        if ($a["resourceId"]==1)
            $smoke = "Некурящий зал";
        if ($a["resourceId"]==2)
            $smoke = "Курящий зал";
        
        if ($rest_iblock==11)
            $iblock_section_id=83659;
        elseif($rest_iblock==12)
            $iblock_section_id=83660;
                       
        $arLoadProductArray = Array(
            "ACTIVE_FROM" => $date." ".$time.":00",
            "MODIFIED_BY"    => false,
            "IBLOCK_SECTION_ID" => $iblock_section_id,      
            "IBLOCK_ID"      => 105,
            "PROPERTY_VALUES"=> $PROP,
            "PREVIEW_TEXT" => "Пожелания:".$a["comment"]."\n".$smoke,
            "PREVIEW_TEXT_TYPE" => "text",
            "NAME"           => ($USER->GetFullName())?$USER->GetFullName():"Незарегистированный пользователь",
            "ACTIVE"         => "Y",   
            "CREATED_BY"=>false        
        );




            //var_dump($arLoadProductArray);
            //die();

        $PRODUCT_ID = $el->Add($arLoadProductArray);
        if ($PRODUCT_ID)        
        {
            $result["status"] = "ACCEPTED";        
            $result["url"] = "http://www.restoran.ru/order.php?yid=".$a["bookId"];
            
            $el->Update($PRODUCT_ID, array("CREATED_BY"=>false, "MODIFIED_BY"=>false));
            $ser = str_replace("/m", "", $_SERVER["DOCUMENT_ROOT"]);
            $fp = fopen($ser."/bs/orders_log.txt", "a"); 
            $mytext = $PRODUCT_ID."\r\n".serialize($_SERVER)."\r\n\r\n"; 
            fwrite($fp, $mytext); 
            fclose($fp);
            $f = fopen($ser."/bs/alerts/orders/".$PRODUCT_ID, "w");
            if ($rest_iblock==11)
                fwrite($f, "msk");
            elseif($rest_iblock==12)
                fwrite($f, "spb");            
            fclose($f);
        }
        else
            $result["status"] = "REJECTED";                
        return $result;        
    }
     
    public function getBookStatus( $a )
    {
        CModule::IncludeModule("iblock");  
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>105,"PROPERTY_yandexid"=>$a),false,Array("nTopCount"=>1),Array("ID","PROPERTY_status"));
        if ($ar = $res->Fetch())
        {                           
            if ($ar["PROPERTY_STATUS_VALUE"]=="1467")
                $result = "APPROVED";
            elseif ($ar["PROPERTY_STATUS_VALUE"]=="1809")
                $result = "CANCELLED_BY_ORGANIZATION";
            elseif($ar["PROPERTY_STATUS_VALUE"]=="1499")
                $result = "COME";
            elseif($ar["PROPERTY_STATUS_VALUE"]=="1500")
                $result = "DID_NOT_COME";
            elseif($ar["PROPERTY_STATUS_VALUE"]=="1501")
                $result = "CANCELLED_BY_USER";            
            else
                $result = "ACCEPTED";            
            return $result;
        }                          
        throw new Exception( 'Invalid Request', -32407);
    }
    
    public function cancelBook ($a)
    {
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_ID"=>105,"PROPERTY_yandexid"=>$a),false,Array("nTopCount"=>1),Array("ID","IBLOCK_ID"));
        if ($ar = $res->Fetch())
        {
            CIBlockElement::SetPropertyValuesEx($ar["ID"], $ar["IBLOCK_ID"], array("status" => 1501));
            CIBlockElement::SetPropertyValuesEx($ar["ID"], $ar["IBLOCK_ID"], array("new" => ""));
            return 1;
        }        
        throw new Exception( 'Invalid Request', -32407);
    }
}
$server = new JsonRpc( new JsonRpcServer() );
$server->process();
?>