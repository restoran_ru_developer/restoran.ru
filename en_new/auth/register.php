<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<div id="content">
    <div id="tabs_block2">
        <ul class="tabs" style="width:auto">
            <li>
                <a href="#">
                    <div class="left tab_left"></div>
                    <div class="left name">Зарегистрироваться как пользователь</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="left tab_left"></div>
                    <div class="left name">Для ресторатора</div>
                    <div class="left tab_right"></div>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
        <!-- tab "panes" -->
        <div class="panes">
            <div class="pane" style="width:auto; min-height:52px;">
                <script>
                    $(document).ready(function(){
                        $(".black_block a").click(function(){
                           if ($(this).parent().hasClass("active")||$(this).parent().hasClass("new_active"))
                           {
                            $(".black_block td.active").removeClass("active");
                            $(".black_block td.new_active").removeClass("new_active");
                            $(".vsp").hide();
                           }
                           else
                           {
                               $(".black_block td.active").removeClass("active");
                                $(".black_block td.new_active").removeClass("new_active");
                                if ($(this).parent().hasClass("new"))
                                    $(this).parent().addClass("new_active");
                                else
                                    $(this).parent().addClass("active");
                                $(".vsp").hide();
                                $("#vsp"+$(this).attr("href")).show();
                           }
                           return false;
                        });
                    });
                </script>
                <div class="black_block">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><a href="1" class="another js">Бронирование<br />столиков online</a></td>
                            <!--<td><a href="2" class="another js">Покупка купонов</a></td>-->
                            <td><a href="3" class="another js">Ведение блога</a></td>
                            <td><a href="4" class="another js">Рецепты<br />и диеты</a></td>
                            <td><a href="5" class="another js">Добавление отзывов</a></td>
                            <td class="new"><a href="6" class="another js">Общение online</a></td>
                        </tr>
                    </table>
                    <div class="grey_block vsp" id="vsp1">
                        <p>Забронировать столик или банкет в любом ресторане города – это очень просто! Вы можете оформить бронь с главной страницы сайта, можете из каталога, а можете и со страницы понравившегося ресторана! Просто нажмите кнопку, и через 5 минут оператор сообщит подробности вашего резерва. А наши зарегистрированные пользователи, помимо приятно проведённого вечера в любимом ресторане, ещё получают Рестики, которые потом можно обменять на <a href="/shop/" taget="_blank">классные подарки</a>, и имеют возможность следить за бронью в личном кабинете. </p>
                        <p>Нет никакой возможности заполнить форму или связаться с нами? Просто кликните на «Позвоните мне», и наши операторы немедленно свяжутся с вами! </p>
                       <br/>
                    </div>
                    <div class="grey_block vsp" id="vsp3">
                        <p>Знаете много интересного про еду? Любите рестораны? Много путешествуете и можете рассказать про тайный ингредиент священного напитка индейцев танана? Поделиться рецептом, полностью восстановленным по одной из книг Джорджа Мартина? Может, рецептом простого, но безумно вкусного бутерброда? Да мало ли интересного может храниться в вашей голове, книгах на ваших полках или в интернете! Делитесь этим на страницах нашего сайта, общайтесь и, как всегда, <a href="/shop/" taget="_blank">получайте Рестики</a>!</p> <br/>
                    </div>
                    <div class="grey_block vsp" id="vsp4">
                        <p>Огромный кулинарный раздел нашего сайта, постоянно пополняемый редакцией и самими пользователями, ни за что не даст умереть голодной смертью! Даже если вы блюдёте фигуру или у вас строгая спортивная диета, а может, просто любите овсянку и уже попробовали её приготовить во всех возможных вариантах, у нас всегда есть что-то новое и интересное, что можно сохранить в своём Избранном, чтобы всегда иметь под рукой. </p>
                        <p>Присоединяйтесь! Делитесь своими кулинарными тайнами и убойными рецептами, рассказывайте, как достигли форм Афродиты или победили с помощью диеты недуг! И – да-да, не забывайте, что всю ценную информацию мы обмениваем на рестики, а потом на <a href="/shop/" taget="_blank">подарки</a>!</p> <br/>
                    </div>
                    <div class="grey_block vsp" id="vsp5">
                        <p>Всё было вкусно, красиво, душевно и осталась масса приятных впечатлений от отдыха в ресторане? Не держите это в себе, не будьте жадиной! Хорошим нужно делиться! Пусть и другие узнают, как здорово вы провели время! А если вдруг случилась досадная неприятность, вам испортили отдых, про это тоже стоит рассказать. И даже добавить фото или видео! Во-первых, это как сеанс групповой терапии – ведь так много людей могут вас поддержать, а во-вторых, дайте ресторану шанс исправиться. Может, это была случайность, и они очень дорожат вашим мнением и сделают всё, чтобы поправить случившиеся. Пишите, делитесь, радуйтесь вместе с нами, жалуйтесь нам, а мы за каждое ваше ценное слово на нашем сайте будем, как всегда, расставаться с <a href="/shop/" taget="_blank">рестиками и подарками</a>. </p><br/>
                    </div>
                    <div class="grey_block vsp" id="vsp6">
                        Text here5 ...<br/><Br /><br/><br/><br/>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="grey_block left" style="width:348px; padding-top:10px;padding-bottom:10px;">
                    <h2 class="left" style="margin-top:0px;margin-bottom:5px;">Быстрая регистрация</h2>                    
                    <div class="clear"></div>
                    <div style="line-height:18px;margin-bottom:5px;">Если вы зарегистрированы на одном из этих сайтов, вы можете пройти быструю регистрацию. </div>
                    <div style="float: left; background: #FFF; padding:5px; width:135px;">
                        <?$APPLICATION->IncludeComponent("restoran:user.vkontakte_auth", ".default", array(
                            ),
                            false
                        );?>
                    </div>
                    <div style="float: left; margin-left: 20px;  background: #FFF; padding:5px;">
                        <?$APPLICATION->IncludeComponent("restoran:user.facebook_auth", ".default", array(
                            ),
                            false
                        );?>
                    </div>
                    <div class="clear"></div>
                </div>
                                    <div class="left">
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:advertising.banner",
                                "",
                                Array(
                                        "TYPE" => "on_register",
                                        "NOINDEX" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "0"
                                ),
                        false
                        );?>
                    </div>
                <div class="clear"></div>
                <br />
                <?$APPLICATION->IncludeComponent("bitrix:main.register", "register_form", Array(
                        "USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
                        "SHOW_FIELDS" => array(	// Поля, которые показывать в форме
                                0 => "LAST_NAME",
                                1 => "NAME",
                                2 => "EMAIL",                                
                                3 => "PERSONAL_PHONE",
                                4 => "PERSONAL_NOTES",
                                5 => "PERSONAL_GENDER",
                                6 => "PERSONAL_BIRTHDAY",
                                7 => "WORK_COMPANY",
                                8 => "WORK_POSITION",
                                9 => "WORK_NOTES",
                                10 => "PERSONAL_PROFESSION",
                                11 => "PERSONAL_CITY",
                                12 => "PERSONAL_PAGER"
                            
                        ),
                        "REQUIRED_FIELDS" => array(	// Поля, обязательные для заполнения
                                0 => "LAST_NAME",
                                1 => "NAME",
                                2 => "EMAIL",
                                3 => "PERSONAL_PROFESSION",
                        ),
                        "AUTH" => "N",	// Автоматически авторизовать пользователей
                        "USE_BACKURL" => "N",	// Отправлять пользователя по обратной ссылке, если она есть
                        "SUCCESS_PAGE" => "/auth/register_confirm.php",	// Страница окончания регистрации
                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                        "USER_PROPERTY" => array(	// Показывать доп. свойства
                                0 => "UF_USER_AGREEMENT",
                                1 => "UF_KITCHEN"
                        )
                        ),
                        false
                );?>
            </div>
            <div class="pane" style="width:auto; min-height:52px;">
                <div class="black_block">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><a href="7" class="another js">Добавление ресторанов</a></td>
                            <td><a href="8" class="another js">Добавление доставки, еды, фирмы</a></td>
                            <td><a href="9" class="another js">Рекламные места &ndash; баннеры</a></td>
                        </tr>
                    </table>
                    <div class="grey_block vsp" id="vsp7">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc_add_rest",
                                    "AREA_FILE_RECURSIVE" => "N",
                                    "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    </div>
                    <div class="grey_block vsp" id="vsp8">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc_add_food",
                                    "AREA_FILE_RECURSIVE" => "N",
                                    "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    </div>
                    <div class="grey_block vsp" id="vsp9">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                    "AREA_FILE_SHOW" => "sect",
                                    "AREA_FILE_SUFFIX" => "inc_add_baner",
                                    "AREA_FILE_RECURSIVE" => "N",
                                    "EDIT_TEMPLATE" => ""
                            )
                        );?>
                    </div>
                </div>
                <Br />
                <p class="font18">Уважаемые рестораторы, на данный момент кабинет профессионального пользователя находится в разработке и функция регистрации отключена. </p>
                <p class="font18">Здесь вы можете ознакомиться с нашим коммерческим предложением по размещению в каталоге.</p>
            </div>
        </div>
        
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>