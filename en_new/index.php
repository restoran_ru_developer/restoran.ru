<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("title", "Restoran.ru");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Restoran.ru");
$arRestIB = getArIblock("catalog", CITY_ID);
$arIndexIB = getArIblock("index_page", CITY_ID);
// add script for tab switcher
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/index_page.js");
?>
<?
if($arIndexIB["ID"]):
    global $arrFirstBlock;
    $arrFirstBlock["SECTION_CODE"] = "first";

    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "index_block_scrollable",
        Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "index_page",
            "IBLOCK_ID" => $arIndexIB["ID"],
            "NEWS_COUNT" => "999",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "",
            "SORT_ORDER2" => "",
            "FILTER_NAME" => "arrFirstBlock",
            "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
            "PROPERTY_CODE" => array(
                0  => "ELEMENTS",
                1  => "IBLOCK_ID",
                2  => "IBLOCK_TYPE",
                3  => "SECTION",
                4  => "COUNT",
                5  => "LINK",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "120",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "7200",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
        ),
        false
        );
endif;
?>
<div id="content">
    <div class="left left_block">
        <?
        if($arIndexIB["ID"]):
            global $arrOthersBlock;
            $arrOthersBlock["!SECTION_CODE"] = "first";

            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "index_block",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrOthersBlock",
                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "7200",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
                );
            endif;?> 
                <div id="tabs_block_vkfc">
                    <ul class="tabs">
                        <li>
                            <a href="#" class="current">
                                <div class="left tab_left"></div>
                                <div class="left name">Join us</div>
                                <div class="left tab_right"></div>
                                <div class="clear"></div>
                            </a>
                        </li>
                    </ul>
                    <!-- tab "panes" -->
                    <div class="panes">
                            <div class="pane" main="main_page" style="display: block; ">
                                <?if (CITY_ID=="spb"):?>
                                    <div class="new_restoraunt left" style="margin-right:10px;">
                                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                        <!-- VK Widget -->
                                        <div id="vk_groups"></div>
                                        <script type="text/javascript">
                                        VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 33051123);
                                        </script>      
                                    </div>
                                <?else:?>
                                    <div class="new_restoraunt left" style="margin-right:10px;">
                                        <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                        <!-- VK Widget -->
                                        <div id="vk_groups"></div>
                                        <script type="text/javascript">
                                        VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 10265458);
                                        </script>      
                                    </div>
                                <?endif;?>
                                <div class="new_restoraunt left" style="margin-right:10px;">                                    
                                    <div id="vk_groups1"></div>
                                    <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups1", {mode: 0, width: "232", height: "290"}, 16519704);
                                    </script>
                                </div>
                                <div class="new_restoraunt left" style="margin-right:0px;">
                                    <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                                </div>
                                <div class="clear"></div>
                            </div>
                    </div>
                </div>
    </div>
    <div class="right right_block">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "inc_right_block",
                "EDIT_TEMPLATE" => ""
            ),
        false
        );?>
        <br /><br />
        <?//  require_once $_SERVER["DOCUMENT_ROOT"].'/index_inc_right_block_vote.php';?>
        <?/*$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "inc_right_block_vote",
                "EDIT_TEMPLATE" => ""
            ),
        false
        );*/?>
    </div>
    <div class="clear"></div>
    <div class="baner">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "bottom_rest_list",
            		"NOINDEX" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
            );?>
        </div>
    <h2>For User</h2>
    <p class="columns">
        <?$APPLICATION->IncludeFile(
            "/index_inc.php",
            Array(),
            Array("MODE"=>"html")
        );?>
    </p>
    
</div>
<div id="baner_inv_block">
    <div id="baner_right_index_1">
        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "",
            Array(
                    "TYPE" => "right_index_1",
                    "NOINDEX" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
            ),
        false
        );?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>