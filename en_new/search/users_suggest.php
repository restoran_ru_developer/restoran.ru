<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();

$obCache = new CPHPCache; 

$life_time = 0;//8*60*60; 
$cache_id = $_REQUEST["q"].$USER->GetUserGroupString(); 

if($obCache->InitCache($life_time, $cache_id, "/")):
    // получаем закешированные переменные
    $vars = $obCache->GetVars();
    $users = $vars["USERS"];
else:
    // иначе обращаемся к базе
    $filter = Array("KEYWORDS"=>"%".trim($_REQUEST["q"])."%");
    $arParams = array();
    $users = array();
    $arParams["SELECT"] = Array("ID","NAME");
    $rsUsers = CUser::GetList(($by="name"), ($order="asc"), $filter);
    while ($arUsers = $rsUsers->GetNext())
    {
        $users[] = Array("NAME"=>$arUsers["NAME"]." ".$arUsers["PERSONAL_PROFESSION"]." ".$arUsers["LAST_NAME"],"ID"=>$arUsers["ID"],);
    }
endif;

// начинаем буферизирование вывода
if($obCache->StartDataCache()):

    
    // выбираем из базы параметры элемента инфо-блока
    foreach ($users as $user)
    {
        echo implode("###",$user)."\n";
    }

    // записываем предварительно буферизированный вывод в файл кеша
    // вместе с дополнительной переменной
    $obCache->EndDataCache(array(
        "USERS"    => $users
        )); 
endif;
?>