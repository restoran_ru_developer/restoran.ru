<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расширенный поиcк Ресторан.ру");
$arRestIB = getArIblock("catalog", CITY_ID);
$arKuponIB = getArIblock("kupons", CITY_ID);
$arOverviewsIB = getArIblock("overviews", CITY_ID);
$arInterviewIB = getArIblock("interview", CITY_ID);
$arPhotoreportsIB = getArIblock("photoreports", CITY_ID);
$arAfishaIB = getArIblock("afisha", CITY_ID);
$arNewsIB = getArIblock("news", CITY_ID);
$arVideoIB = getArIblock("videonews", CITY_ID);
$arReviewsIB = getArIblock("reviews", CITY_ID);
$arFirmsIB = getArIblock("firms", CITY_ID);
?>
<div id="content">
    <div class="left" style="width:728px;">
    <?
    switch($_GET["search_in"]) {
        case 'all':
            include('all.php');
        break;
        case 'rest':
            include('rest.php');
        break;
        case 'dostavka':
            include('dostavka.php');
        break;
        case 'kupons':
            include('kupons.php');
        break;
        case 'afisha':
            include('afisha.php');
        break;
        case 'news':
            include('news.php');
        break;    
        case 'newss':
            include('newss.php');
        break;    
        case 'recepts':
            include('recept.php');
        break;
        case 'recipe':
            include('recipe.php');
        break;
        case 'firms':
            include('firms.php');
        break;
        case 'firms_news':
            include('firms_news.php');
        break;
        case 'blog':
            include('blog.php');
        break;
        case 'overviews':
            include('overviews.php');
        break;
        case 'interview':
            include('interview.php');
        break;
        case 'reviews':
            include('reviews.php');
        break;
        case 'master_classes':
            include('master_classes.php');
        break;
        default:
            include('rest.php');
        break;
    }
    ?>
    </div>
    <div class="right">
        <div class="baner2">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_2_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
            <br /><br />
            <div class="top_block">Recommended</div>
            <?
            $arRestIB = getArIblock("catalog", CITY_ID);
            $arPFilter["!PROPERTY_sleeping_rest_VALUE"] = 'Да';
            $arPFilter["!PREVIEW_PICTURE"] = false;
            $arPFilter["!PROPERTY_restoran_ratio"] = false;            
            ?>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "rand",
                    "SORT_ORDER1" => "",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "recomended"
                ),
            false
            );?>
            <br /><br />
                    
        <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_1_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        <br /><Br />
        <div class="top_block">Popular</div>
            <?
            $APPLICATION->IncludeComponent(
                "restoran:catalog.list",
                "recomended",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => $arRestIB["ID"],
                    "NEWS_COUNT" => "3",
                    "SORT_BY1" => "show_counter",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "rand",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arPFilter",
                    "FIELD_CODE" => array("CREATED_BY","DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("COMMENTS","RATIO"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "A" => "popular"
                ),
            false
            );?>
<br /><br />
        <?$APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/order_rest_".CITY_ID.".php"),
            Array(),
            Array("MODE"=>"html")
	    );?>
        <br />
        <div align="right">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                    "TYPE" => "right_3_main_page",
                    "NOINDEX" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "0"
                ),
                false
            );?>
        </div>
    </div>
    <div class="clear"></div>
        <br /><br />
    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "bottom_rest_list",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
		false
		);?>  
    <br /><br />
</div>
<br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>