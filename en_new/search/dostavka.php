<?
global $arrFilter;
CModule::IncludeModule("iblock");
$res = CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arRestIB["ID"],"CODE"=>"dostavka"),false);
if ($ar = $res->GetNext())
    $arrFilter = array("PARAMS" => array("iblock_section" => $ar["ID"]));
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "rest",
    Array(
            "USE_SUGGEST" => "N",
            "AJAX_MODE" => "N",
            "RESTART" => "Y",
            "NO_WORD_LOGIC" => "Y",
            "USE_LANGUAGE_GUESS" => "Y",
            "CHECK_DATES" => "Y",
            "USE_TITLE_RANK" => "Y",
            "DEFAULT_SORT" => "rank",
            "FILTER_NAME" => "arrFilter",
            "SHOW_WHERE" => "N",
            "SHOW_WHEN" => "N",
            "PAGE_RESULT_COUNT" => "15",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600000",
            "CACHE_NOTES" => "",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Результаты поиска",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "search_rest_list",
            "arrFILTER" => array("iblock_catalog"),
            "arrFILTER_iblock_catalog" => array($arRestIB["ID"]),
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
        )
);?>