<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->RestartBuffer();

// normalize data
$_REQUEST["cuisine"] = explode(",", $_REQUEST["cuisine"]);
$_REQUEST["average_bill"] = explode(",", $_REQUEST["average_bill"]);
$_REQUEST["subway"] = explode(",", $_REQUEST["subway"]);

// select where search
switch($_REQUEST["search_in"]) {
    case 'all':
        include('suggest/all.php');
    break;
    case 'rest':
        include('suggest/rest.php');
    break;
    case 'dostavka':
        include('suggest/dostavka.php');
    break;
    case 'kupons':
        include('suggest/kupons.php');
    break;
    case 'afisha':
        include('suggest/afisha.php');
    break;
    case 'news':
        include('suggest/news.php');
    break;
    case 'recipe':
        include('suggest/recipe.php');
    break;
    case 'blog':
        include('suggest/blog.php');
    break;
    case 'overviews':
        include('suggest/overviews.php');
    break;
    case 'interview':
        include('suggest/interview.php');
    break;
    case 'master_classes':
        include('suggest/master_classes.php');
    break;
    default:
        include('suggest/rest.php');
    break;
}
?>