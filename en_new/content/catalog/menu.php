<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
if ($_REQUEST["page"])
    $_REQUEST["PAGEN_1"] = (int)$_REQUEST["page"];
    if (!$_REQUEST["PARENT_SECTION_ID"]):
        //global $DB;
        CModule::IncludeModule("iblock");
        /*$sql = "SELECT ID,NAME FROM b_iblock_element WHERE CODE='".$DB->ForSql($_REQUEST["RESTOURANT"])."' LIMIT 1";
        $res = $DB->Query($sql);
        if ($ar = $res->Fetch())
        {
            $id = $ar["ID"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
        }*/
        $arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arIB["ID"],"CODE"=>$_REQUEST["RESTOURANT"]),false,Array("nTopCount"=>1),Array("ID","NAME","DETAIL_PAGE_URL"));
        if ($ar = $res->GetNext())
        {
            $id = $ar["ID"];
            $name = $ar["NAME"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
            $url = $ar["DETAIL_PAGE_URL"];
        }
               
        /*$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>(int)$id);
        $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
        if($ar_result = $db_list->Fetch())
        {
            $menu = $ar_result['ID'];
        }*/
        global $DB;
        $sql = "SELECT ID FROM b_iblock WHERE DESCRIPTION=".$DB->ForSql($id);
        $db_list = $DB->Query($sql);
        if($ar_result = $db_list->GetNext())
        {
            $menu = $ar_result['ID'];
        }
        //$_REQUEST["SECTION_ID"] = false;
        //$_REQUEST["PARENT_SECTION_ID"] = false;
    endif;
    ?>
<div id="content">
    <h1 class="with_link">Menu of restaurant «<a href="<?=$url?>" alt="<?=$ar["NAME"]?>" title="<?=$ar["NAME"]?>"><?=$ar["NAME"]?></a>»</h1>
<?
if ($menu)
{
    $APPLICATION->IncludeComponent(
        "bitrix:catalog",
        "dostavka",
        Array(
                "AJAX_MODE" => "Y",
                "SEF_MODE" => "N",
                "IBLOCK_TYPE" => "rest_menu_ru",
                "IBLOCK_ID" => $menu,
                "USE_FILTER" => "N",
                "USE_REVIEW" => "N",
                "USE_COMPARE" => "N",
                "SHOW_TOP_ELEMENTS" => "Y",
                "PAGE_ELEMENT_COUNT" => "12",
                "LINE_ELEMENT_COUNT" => "3",
                "ELEMENT_SORT_FIELD" => "id",
                "ELEMENT_SORT_ORDER" => "asc",
                "LIST_PROPERTY_CODE" => array("map"),
                "INCLUDE_SUBSECTIONS" => "Y",
                "LIST_META_KEYWORDS" => "-",
                "LIST_META_DESCRIPTION" => "-",
                "LIST_BROWSER_TITLE" => "-",
                "DETAIL_PROPERTY_CODE" => array("map"),
                "DETAIL_META_KEYWORDS" => "map",
                "DETAIL_META_DESCRIPTION" => "map",
                "DETAIL_BROWSER_TITLE" => "map",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "7200",
                "CACHE_NOTES" => "",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "PRICE_CODE" => array("BASE"),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "LINK_IBLOCK_TYPE" => "",
                "LINK_IBLOCK_ID" => "",
                "LINK_PROPERTY_SID" => "",
                "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                "USE_ALSO_BUY" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "TOP_ELEMENT_COUNT" => "9",
                "TOP_LINE_ELEMENT_COUNT" => "3",
                "TOP_ELEMENT_SORT_FIELD" => "sort",
                "TOP_ELEMENT_SORT_ORDER" => "asc",
                "TOP_PROPERTY_CODE" => array("map"),
                "VARIABLE_ALIASES" => Array(
                        "SECTION_ID" => "SECTION_ID",
                        "ELEMENT_ID" => "ELEMENT_ID"
                ),
                "AJAX_OPTION_SHADOW" => "Y",
                "AJAX_OPTION_JUMP" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "AJAX_OPTION_ADDITIONAL" => ""
        ),
    false
    );
}
else
{
    ShowError("Меню не найдено");
}?>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>