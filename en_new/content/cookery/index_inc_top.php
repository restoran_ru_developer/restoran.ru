<div class="top_baner">
	<div class="top_baner_block">
		<div id="subscribe">
			<div class="left">
				<?$APPLICATION->IncludeComponent(
					"bitrix:advertising.banner",
					"",
					Array(
						"TYPE" => "top_content_main_page",
						"NOINDEX" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0"
					),
				false
				);?>
			</div>
			<div class="right">
				<?$APPLICATION->IncludeComponent(
					"bitrix:subscribe.form",
					"main_page_subscribe",
					Array(
						"USE_PERSONALIZATION" => "Y",
						"PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
						"SHOW_HIDDEN" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "3600",
						"CACHE_NOTES" => ""
					),
				false
				);?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>