<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once($BX_DOC_ROOT."/lang/".SITE_LANGUAGE_ID."/index.php");
$APPLICATION->SetTitle("Кулинария");
$arInterviewIB = getArIblock("interview", CITY_ID);
?>
<div id="content">
    <div class="left" style="width:730px">
        <!--<h1>Кулинария</h1>-->
		<?
                global $addFil;
                $arrFil = Array("CODE"=>$spec);
                $APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"special",
			Array(
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "cookery",
				"IBLOCK_ID" => 72,
				"NEWS_COUNT" => "12",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "ID",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arrFil",
				"FIELD_CODE" => array("DETAIL_PICTURE","TAGS"),
				"PROPERTY_CODE" => array(
					"RECEPTS",
				),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "j F Y G:i",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			),
		false
		);?>		
   </div>    
    <div class="right" style="width:240px">
       <div id="search_article">
           <?$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.form",
                    "main_page_subscribe",
                    Array(
                            "USE_PERSONALIZATION" => "Y",
                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                            "SHOW_HIDDEN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => ""
                    ),
            false
            );?>            
        </div>
        <div align="right">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/right_baner1.png" />
        </div>
		<!--<br />
		<div class="figure_border">
            <div class="top"></div>
            <div class="center">
                <span>У меня в холодильнике</span><br />
				<a href="#" class="another">выбери ингридиенты</a><br />
				- найди рецепт
            </div>
            <div class="bottom"></div>
        </div>-->
        <br /><br />
        <div class="tags">
			<?$APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
					"SORT" => "CNT",
					"PAGE_ELEMENTS" => "20",
					"PERIOD" => "",
					"URL_SEARCH" => "/search/index.php",
					"TAGS_INHERIT" => "Y",
					"CHECK_DATES" => "Y",
					"FILTER_NAME" => "",
					"arrFILTER" => array(
							0 => "iblock_cookery",
					),
					"arrFILTER_iblock_cookery" => array(
							0 => "139",
					),
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"FONT_MAX" => "24",
					"FONT_MIN" => "12",
					"COLOR_NEW" => "24A6CF",
					"COLOR_OLD" => "24A6CF",
					"PERIOD_NEW_TAGS" => "",
					"SHOW_CHAIN" => "Y",
					"COLOR_TYPE" => "N",
					"WIDTH" => "100%",
                                        "SEARCH_IN" => "recipe"
					),
					$component
			);?> 
        </div>
        <!--<br />
		<div class="black_hr"></div>
        <div align="right">
            <a href="#" class="uppercase">ВСЕ ТЕГИ</a>
        </div>
        <br />
		<div class="top_block">Видео-рецепты</div>		
		<?
                   /* $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 139,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => 57429,
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );*/
                ?>
		<div class="black_hr"></div>
		<div align="right"><a class="uppercase" href="/content/cookery/videoretsepty/">ВСЕ ВИДЕО</a></div>-->
		<br /><br />
		<div class="top_block"><i>Топ-3</i> рецептов</div>
                <?
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 139,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                ?>
		<!--<hr />
		<div align="right"><a href="#">ВСЕ НОВЫЕ</a></div>-->
		<br /><br />
		<!--<div class="top_block">Опрос</div>
            <?$APPLICATION->IncludeComponent(
                    "bitrix:voting.current",
                    "",
                    Array(
                            "CHANNEL_SID" => "COOKERY",
                            "VOTE_ID" => "2",
                            "VOTE_ALL_RESULTS" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => "",
                            "AJAX_MODE" => "Y",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => ""
                    )
            );?> -->		
    </div>
    <div class="clear"></div>
    <br /><br />
    <img src="<?=SITE_TEMPLATE_PATH?>/images/top_baner.png" />    
    <br /><br />
</div>   
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>