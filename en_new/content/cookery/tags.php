<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Популярные теги");
?>
<div id="content">
    <div class="left"  style="width:730px; line-height:32px">
        <h1>Популярные теги</h1>
        <i>
        <?
            $APPLICATION->IncludeComponent("bitrix:search.tags.cloud", "interview_list", array(
                "SORT" => "CNT",
                "PAGE_ELEMENTS" => "60",
                "PERIOD" => "",
                "URL_SEARCH" => "/search/index.php",
                "TAGS_INHERIT" => "Y",
                "CHECK_DATES" => "Y",
                "FILTER_NAME" => "",
                "arrFILTER" => array(
                                0 => "iblock_cookery",
                ),
                "arrFILTER_iblock_".$_REQUEST["IBLOCK_TYPE_ID"] => array(
                                0 => 139,
                                0 => 145,
                ),
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "FONT_MAX" => "40",
                "FONT_MIN" => "14",
                "COLOR_NEW" => "24A6CF",
                "COLOR_OLD" => "24A6CF",
                "PERIOD_NEW_TAGS" => "",
                "SHOW_CHAIN" => "Y",
                "COLOR_TYPE" => "N",
                "SEARCH_IN" => "recepts",
                "WIDTH" => "100%"
                ),
                $component
            );?> 
            </i>
        <div class="clear"></div>
    </div>
    <div class="right" style="width:240px">
    <?$APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"",
			Array(
				"TYPE" => "right_2_main_page",
				"NOINDEX" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0"
			),
			false
		);?>
       <!-- <div id="search_article">
            <?/*$APPLICATION->IncludeComponent(
                    "bitrix:subscribe.form",
                    "main_page_subscribe",
                    Array(
                            "USE_PERSONALIZATION" => "Y",
                            "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                            "SHOW_HIDDEN" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600",
                            "CACHE_NOTES" => ""
                    ),
            false
            );*/?>           
        </div>-->
        <!--<br />
		<div class="top_block">Видео-рецепты</div>		
		<?
                   /* $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 139,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "Y",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => 57429,
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );*/
                ?>
		<div class="black_hr"></div>
		<div align="right"><a class="uppercase" href="/content/cookery/videoretsepty/">ВСЕ ВИДЕО</a></div>-->
		<br /><br />
		<div class="top_block"><i>Топ-3</i> рецептов</div>
                <?
                    $APPLICATION->IncludeComponent(
                        "restoran:catalog.list",
                        "cook_one",
                        Array(
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "AJAX_MODE" => "N",
                                "IBLOCK_TYPE" => "cookery",
                                "IBLOCK_ID" => 139,
                                "NEWS_COUNT" => 3,
                                "SORT_BY1" => "ID",
                                "SORT_ORDER1" => "DESC",
                                "SORT_BY2" => "",
                                "SORT_ORDER2" => "",
                                "FILTER_NAME" => "arrFilterTop4",
                                "FIELD_CODE" => array("CREATED_BY"),
                                "PROPERTY_CODE" => array("ratio","reviews_bind"),
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "PREVIEW_TRUNCATE_LEN" => "200",
                                "ACTIVE_DATE_FORMAT" => "j F Y G:i",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_FILTER" => "Y",
                                "CACHE_GROUPS" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "Новости",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N"
                        ),
                    false
                    );
                ?>	
    </div>
    <div class="clear"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>