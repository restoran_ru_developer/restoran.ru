<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("News");
$ttt = RestIBlock::GetTypeBySectionCode($_REQUEST["SECTION_CODE"]);
if ($ttt)
{
    $type = $ttt;    
}
else
{
    $type = str_replace($_REQUEST["CITY_ID"], "", $_REQUEST["SECTION_CODE"]);
    if ($type=="restoransnews")
        $type = "news";
    elseif($type=="restvew"||$type=="obzor_")
        $type = "overviews";
    elseif($type=="pryamayarech"||$type=="intervew")
        $type = "interview";
    elseif($type=="photos")
        $type = "photoreports";
    elseif($type=="videonovosti")
        $type = "videonews";
    elseif($type=="mcfromchif")
    {
        $APPLICATION->SetTitle("Мастер-классы");
        $type = "cookery";
    }
    else
    {
        $type="news";
    }
}
if ($_REQUEST["SECTION_CODE"]=="restoratoram")
{
    $type = "firms_news";
    $arIB = getArIblock($type, "msk");
}
else
    $arIB = getArIblock($type, $_REQUEST["CITY_ID"]);
?>
<?
 $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"statya_detail",
	Array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => $type,
		"IBLOCK_ID" => $arIB["ID"],
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"CHECK_DATES" => "N",
		"FIELD_CODE" => Array("CREATED_BY","DATE_CREATE"),
		"PROPERTY_CODE" => Array("COMMENTS","RESTORAN"),
		"IBLOCK_URL" => "",
		"META_KEYWORDS" => "keywords",
		"META_DESCRIPTION" => "description",
		"BROWSER_TITLE" => "title",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "j F Y G:i",
		"USE_PERMISSIONS" => "N",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "86400",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);
 ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>