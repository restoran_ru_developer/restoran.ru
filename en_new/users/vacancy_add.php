<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление вакансии");
// get resume iblock info
$arVacIB = getArIblock("vacancy", CITY_ID);
?>
<div id="content">
    <h1>Добавление вакансии</h1>


    <?$APPLICATION->IncludeComponent(
	"restoran:editor2_fixed",
	"vacancy",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",	
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$arVacIB["ID"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Должность", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			array("NAME"=>"Раздел", "TYPE"=>"select", "CODE"=>"SECTION_ID", "VALUE_FROM"=>"IBLOCK_SECTION_ID", "VALUES_LIST"=>"SECTIONS"),
			array("NAME"=>"Контактное лицо", "TYPE"=>"short_text", "CODE"=>"PROPERTY_COMPANY_FIO", "VALUE_FROM"=>"PROPERTIES__COMPANY_FIO__VALUE"),
			array("NAME"=>"Телефон", "TYPE"=>"short_text", "CODE"=>"PROPERTY_CONTACT_PHONE", "VALUE_FROM"=>"PROPERTIES__CONTACT_PHONE__VALUE"),
			array("NAME"=>"Адрес", "TYPE"=>"short_text", "CODE"=>"PROPERTY_ADDRESS", "VALUE_FROM"=>"PROPERTIES__ADDRESS__VALUE"),
			array("NAME"=>"Метро", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_SUBWAY_BIND", "VALUE_FROM"=>"PROPERTIES__SUBWAY_BIND__VALUE", "VALUES_LIST"=>"PROPS__SUBWAY_BIND__LIST"),
			array("NAME"=>"Сайт", "TYPE"=>"short_text", "CODE"=>"PROPERTY_site", "VALUE_FROM"=>"PROPERTIES__site__VALUE"),
			array("NAME"=>"Заработная плата (от)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_WAGES_OF", "VALUE_FROM"=>"PROPERTIES__WAGES_OF__VALUE"),
			array("NAME"=>"График работы", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_SHEDULE", "VALUE_FROM"=>"PROPERTIES__SHEDULE__VALUE", "VALUES_LIST"=>"PROPS__SHEDULE__LIST"),
			array("NAME"=>"Опыт работы", "TYPE"=>"select", "CODE"=>"PROPERTY_EXPERIENCE", "VALUE_FROM"=>"PROPERTIES__EXPERIENCE__VALUE", "VALUES_LIST"=>"PROPS__EXPERIENCE__LIST"),
			array("NAME"=>"Образование", "TYPE"=>"select", "CODE"=>"PROPERTY_EDUCATION", "VALUE_FROM"=>"PROPERTIES__EDUCATION__VALUE", "VALUES_LIST"=>"PROPS__EDUCATION__LIST"),
			array("NAME"=>"Возраст (от)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_AGE_FROM", "VALUE_FROM"=>"PROPERTIES__AGE_FROM__VALUE"),
			array("NAME"=>"Возраст (до)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_AGE_TO", "VALUE_FROM"=>"PROPERTIES__AGE_TO__VALUE"),
			array("NAME"=>"Обязанност", "TYPE"=>"short_text", "CODE"=>"PROPERTY_RESPONSIBILITY", "VALUE_FROM"=>"PROPERTIES__RESPONSIBILITY__VALUE"),
			array("NAME"=>"Дополнительно", "TYPE"=>"short_text", "CODE"=>"PROPERTY_ADD_INFO", "VALUE_FROM"=>"PROPERTIES__ADD_INFO__VALUE")
		),
	),
false
);?>    


</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>