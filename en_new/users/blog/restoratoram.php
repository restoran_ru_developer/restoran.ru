<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Блоги пользователя");
?>


<?



if($_REQUEST["RAZDEL"]!=""){
	global $arrFilter;
	$arrFilter["SECTION_ID"]=$_REQUEST["RAZDEL"];
	
}


$arGroups = $USER->GetUserGroupArray();
foreach($arGroups as $v) $GRs[]=$v;
if($USER->GetID()==(int)$_REQUEST["USER_ID"] || in_array(14, $GRs) || in_array(15, $GRs) || $USER->IsAdmin()){
	$REDACTOR="Y";
}else{
	$REDACTOR="N";
}


if(!in_array(14, $GRs) && !in_array(15, $GRs) && !$USER->IsAdmin()){
	$arrFilter["CREATED_BY"] = (int)$_REQUEST["USER_ID"];
}



if($_REQUEST["act"]=="search_by_name" && $_REQUEST["NAME"]!="") $arrFilter["NAME"]="%".$_REQUEST["NAME"]."%";
?>


<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section_notact",
	"redactor_editor",
	Array(
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "firms_news",
		"IBLOCK_ID" => "2423",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => "",
		"ELEMENT_SORT_FIELD" => "DATE_ACTIVE_FROM",
		"ELEMENT_SORT_ORDER" => "DESC",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => "",
		"PRICE_CODE" => "",
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => "",
		"USE_PRODUCT_QUANTITY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Публикации",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"ARTICLE_EDOTOR_PAGE"=>"/users/id".(int)$_REQUEST["USER_ID"]."/blog/editor/restoratoram.php",
		"REDACTOR"=>$REDACTOR
	)
);?>