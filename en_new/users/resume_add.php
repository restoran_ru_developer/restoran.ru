<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление резюме");
// get resume iblock info
$arResumeIB = getArIblock("resume", CITY_ID);

//var_dump($arResumeIB);
?>
<div id="content">
    <h1>Добавление резюме</h1>
    
    
     <?$APPLICATION->IncludeComponent(
	"restoran:editor2_fixed",
	"vacancy",
	Array(
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_NOTES" => "",	
		"ELEMENT_ID"=>$_REQUEST["ID"],
		"IBLOCK_ID"=>$arResumeIB["ID"],
		"PARENT_SECTION"=>$_REQUEST["SECTION"],
		"REQ"=>array(
			array("NAME"=>"Должность", "TYPE"=>"short_text", "CODE"=>"NAME", "VALUE_FROM"=>"NAME"),
			
			array("NAME"=>"ФИО", "TYPE"=>"short_text", "CODE"=>"PROPERTY_COMPANY_FIO", "VALUE_FROM"=>"PROPERTIES__COMPANY_FIO__VALUE"),
			
			array("NAME"=>"Образование", "TYPE"=>"select", "CODE"=>"PROPERTY_EDUCATION", "VALUE_FROM"=>"PROPERTIES__EDUCATION__VALUE", "VALUES_LIST"=>"PROPS__EDUCATION__LIST"),
			array("NAME"=>"Опыт работы", "TYPE"=>"select", "CODE"=>"PROPERTY_EXPERIENCE", "VALUE_FROM"=>"PROPERTIES__EXPERIENCE__VALUE", "VALUES_LIST"=>"PROPS__EXPERIENCE__LIST"),
			
			array("NAME"=>"Пожелания к месту работы (ЗП от)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_SUGG_WORK_ZP_FROM", "VALUE_FROM"=>"PROPERTIES__SUGG_WORK_ZP_FROM__VALUE"),
			array("NAME"=>"Пожелания к месту работы (ЗП до)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_SUGG_WORK_ZP_TO", "VALUE_FROM"=>"PROPERTIES__SUGG_WORK_ZP_TO__VALUE"),
			array("NAME"=>"Пожелания к месту работы (график)", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_SUGG_WORK_GRAFIK", "VALUE_FROM"=>"PROPERTIES__SUGG_WORK_GRAFIK__VALUE_ENUM_ID", "VALUES_LIST"=>"PROPS__SUGG_WORK_GRAFIK__LIST"),
			array("NAME"=>"Пожелания к месту работы (метро)", "TYPE"=>"multi_select", "CODE"=>"PROPERTY_SUGG_WORK_SUBWAY", "VALUE_FROM"=>"PROPERTIES__SUGG_WORK_SUBWAY__VALUE", "VALUES_LIST"=>"PROPS__SUGG_WORK_SUBWAY__LIST"),
			array("NAME"=>"Пожелания к месту работы (переезд)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_SUGG_WORK_MOVE", "VALUE_FROM"=>"PROPERTIES__SUGG_WORK_MOVE__VALUE"),
			
			array("NAME"=>"Опыт работы (Когда?) Дата приема", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EXP_WHEN_1", "VALUE_FROM"=>"PROPERTIES__EXP_WHEN_1__VALUE", "BL"=>"Y"),
			array("NAME"=>"Опыт работы (Когда?) Дата увольнения", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EXP_WHEN_2", "VALUE_FROM"=>"PROPERTIES__EXP_WHEN_2__VALUE", "BL"=>"Y"),
			array("NAME"=>"Опыт работы (Где?)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EXP_WHERE", "VALUE_FROM"=>"PROPERTIES__EXP_WHERE__VALUE", "BL"=>"Y"),
			array("NAME"=>"Опыт работы (Должность)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EXP_POST", "VALUE_FROM"=>"PROPERTIES__EXP_POST__VALUE", "BL"=>"Y"),
			array("NAME"=>"Опыт работы (Обязанности)", "TYPE"=>"short_text", "CODE"=>"PROPERTY_EXP_RESPONS", "VALUE_FROM"=>"PROPERTIES__EXP_RESPONS__VALUE", "BL"=>"Y"),
		),
		"BACK_LINK"=>"/users/id".(int)$_REQUEST["USER_ID"]."/work/"
	),
false
);?>    
    
    
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>