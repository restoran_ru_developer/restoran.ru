<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Друзья");
?>

<?// pass users IDs for relationship?>
<?/*$APPLICATION->IncludeComponent(
    "restoran:user.friends_add",
    "",
    Array(
        "FIRST_USER_ID" => "1",
        "SECOND_USER_ID" => "3",
        "RESULT_CONTAINER_ID" => "add_friend_result"
    ),
    false
);*/?>

<style type="text/css">
    /* TODO tmp style */
    .wrap-div {width:975px; margin:0 auto;}
</style>
<div class="wrap-div">
    <div id="tabs_block6" class="tabs">
        <ul class="tabs big">
                <li>                                
                    <a href="#" class="current">
                        <div class="left tab_left"></div>
                        <div class="left name">Друзья</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>
                    </a>                                                                
                </li>
                <li><a href="#">
                        <div class="left tab_left"></div>
                        <div class="left name">Подписки</div>
                        <div class="left tab_right"></div>
                        <div class="clear"></div>                                    
                    </a>
                </li>
        </ul>
        <div class="panes">
            <div class="pane big" style="display: block; ">
                <?$APPLICATION->IncludeComponent("bitrix:socialnetwork.messages_requests","",Array(
                        "SET_NAVCHAIN" => "N", 
                        "PATH_TO_USER" => "/users/id#user_id#/", 
                        "PATH_TO_MESSAGE_FORM" => "", 
                        "PAGE_VAR" => "page", 
                        "USER_VAR" => "user_id", 
                        "PATH_TO_SMILE" => "/bitrix/images/socialnetwork/smile/", 
                        "ITEMS_COUNT" => "30" 
                    )
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:socialnetwork.user_friends",
                    "user_friends_list",
                    Array(
                        "SET_NAV_CHAIN" => "N",
                        "ITEMS_COUNT" => "16",
                        "PATH_TO_USER" => "",
                        "PATH_TO_LOG" => "",
                        "PATH_TO_USER_FRIENDS_ADD" => "",
                        "PATH_TO_USER_FRIENDS_DELETE" => "",
                        "PATH_TO_SEARCH" => "",
                        "PAGE_VAR" => "",
                        "USER_VAR" => "",
                        "ID" => (int)$_REQUEST["USER_ID"],
                        "SET_TITLE" => "N"
                    ),
                    false
                );?>
            </div>
            <div class="pane big">
                <?$APPLICATION->IncludeComponent(
                    "restoran:subscribe.users",
                    "",
                    Array(
                        "USER_ID" => (int)$_REQUEST["USER_ID"]
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
    
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>