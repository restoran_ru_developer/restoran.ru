<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<script src='/bitrix/components/restoran/user.profile/templates/.default/chosen.jquery.js'></script>
<link href="/bitrix/components/restoran/user.profile/templates/.default/chosen.css"  type="text/css" rel="stylesheet" />
<div id="content">
    <?$APPLICATION->IncludeComponent(
            "restoran:user.profile",
            "",
            Array(
                "USER_PROPERTY_NAME" => "",
                "SET_TITLE" => "Y",
                "AJAX_MODE" => "N",
                "USER_PROPERTY" => array("UF_KITCHEN", "UF_HIDE_CONTACTS"),
                "SEND_INFO" => "Y",
                "CHECK_RIGHTS" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            ),
    false
    );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>