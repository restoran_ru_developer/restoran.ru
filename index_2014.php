<div class="content">
    <div class="block">
        <div class="left-side">
            <?
            if($arIndexIB["ID"]):
                global $arrFirstBlock;
                $arrFirstBlock["SECTION_CODE"] = "first";

                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "index_block_scrollable",
                    Array(
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "AJAX_MODE" => "N",
                        "IBLOCK_TYPE" => "index_page",
                        "IBLOCK_ID" => $arIndexIB["ID"],
                        "NEWS_COUNT" => "999",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "",
                        "SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFirstBlock",
                        "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                        "PROPERTY_CODE" => array(
                            0  => "ELEMENTS",
                            1  => "IBLOCK_ID",
                            2  => "IBLOCK_TYPE",
                            3  => "SECTION",
                            4  => "COUNT",
                            5  => "LINK",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "120",
                        "ACTIVE_DATE_FORMAT" => "j F Y",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "CACHE_TYPE" => "Y",
                        "CACHE_TIME" => "14402",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "N",
                        "CACHE_NOTES" => "n",            
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "TABS"=>"Y"
                    ),
                    false
                    );
            endif;
            ?>
        </div>
        <div class="right-side">
            <?$APPLICATION->IncludeComponent(
                "bitrix:advertising.banner",
                "",
                Array(
                        "TYPE" => "right_index_1",
                        "NOINDEX" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "0"
                ),
            false
            );?>
        </div>
    </div>
    <?
    if($arIndexIB["ID"]):
        global $arrFirstBlock;
        $arrFirstBlock["SECTION_CODE"] = "first";

        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "index_block_scrollable",
            Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "index_page",
                "IBLOCK_ID" => $arIndexIB["ID"],
                "NEWS_COUNT" => "999",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "",
                "SORT_ORDER2" => "",
                "FILTER_NAME" => "arrFirstBlock",
                "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                "PROPERTY_CODE" => array(
                    0  => "ELEMENTS",
                    1  => "IBLOCK_ID",
                    2  => "IBLOCK_TYPE",
                    3  => "SECTION",
                    4  => "COUNT",
                    5  => "LINK",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "120",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "CACHE_TYPE" => "Y",
                "CACHE_TIME" => "14402",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "CACHE_NOTES" => "n",            
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",                
            ),
            false
            );
    endif;?>    
</div>
<div class="block">
    <div class="left-side">
        <?
        if($arIndexIB["ID"]):
            global $arrOthersBlock;
            $arrOthersBlock["!SECTION_CODE"] = "first";

            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                //($USER->IsAdmin())?"index_block_new":"index_block",
                "index_block_new",
                Array(
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "index_page",
                    "IBLOCK_ID" => $arIndexIB["ID"],
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "arrOthersBlock",
                    "FIELD_CODE" => array("DETAIL_PICTURE","LIST_PAGE_URL"),
                    "PROPERTY_CODE" => array(
                        0  => "ELEMENTS",
                        1  => "IBLOCK_ID",
                        2  => "IBLOCK_TYPE",
                        3  => "SECTION",
                        4  => "COUNT",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "120",
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "CACHE_TYPE" => "Y",
                    "CACHE_TIME" => "14414",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",

                ),
                false
                );
            endif;?> 
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#soc"  data-toggle="tab">Присоединяйтесь к нам</a></li>                        
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active sm" id="soc">                
                        <?if (CITY_ID=="spb"):?>
                            <div class="pull-left" style="margin-right:10px;">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 33051123);
                                </script>
<!--                                        <script type="text/javascript">
                                    var vk_1 = 0;
                                    $(document).ready(function(){
                                       $(window).scroll(function()
                                       {
                                           if ($(window).scrollTop()>eval($("#tabs_block_vkfc").offset().top*1-$(window).height())&&!vk_1)
                                           {
                                               VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 33051123);
                                               VK.Widgets.Group("vk_groups1", {mode: 0, width: "232", height: "290"}, 16519704);
                                               vk_1 = 1;
                                           }
                                       });
                                    });

                                </script>  -->
                            </div>                                
                        <?elseif(CITY_ID=="tmn"):?>                                
                            <div class="pull-left" style="margin-right:10px;">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 53908509);
                                </script>      
                            </div>
                            <div class="pull-left" style="margin-right:10px;">
                                <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ruTumen&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                            </div>
                        <?elseif(CITY_ID=="kld"):?>                                
                            <div class="pull-left" style="margin-right:10px;">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 41981796);
                                </script>      
                            </div>
                            <div class="pull-left" style="margin-right:10px;">
                                <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru.kld&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                            </div>                                
                        <?elseif(CITY_ID=="ast"):?>                                
                            <div class="pull-left" style="margin-right:10px;">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 47212619);
                                </script>      
                            </div>
                        <?elseif(CITY_ID=="msk"):?>
                            <div class="pull-left" style="margin-right:10px;">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "216", height: "290"}, 10265458);
                                </script>
                            </div>
                        <?else:?>
                            <div class="pull-left" style="margin-right:10px;">
                                <script type="text/javascript" src="//vk.com/js/api/openapi.js?60"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups", {mode: 0, width: "232", height: "290"}, 10265458);
                                </script>
                            </div>
                        <?endif;?>

                        <?if(CITY_ID=='msk'):?>
                            <div class="pull-left" style="margin-right:<?=(CITY_ID=="tmn"||CITY_ID=="kld")?"0px":"10px"?>">
                                <iframe src='/inwidget/index.php' scrolling='no' frameborder='no' style='border:none;width:260px;height:330px;overflow:hidden;'></iframe>
                            </div>
                        <?else:?>
                            <div class="pull-left" style="margin-right:<?=(CITY_ID=="tmn"||CITY_ID=="kld")?"0px":"10px"?>">
                                <div id="vk_groups1"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups1", {mode: 0, width: "232", height: "290"}, 16519704);
                                </script>
                            </div>
                        <?endif?>

                        <?if(CITY_ID=='msk'):?>
                            <div class="pull-left" style="margin-right:0px;">
                                <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru&amp;width=226&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:226px; height:290px;" allowTransparency="true"></iframe>
                            </div>
                        <?elseif (CITY_ID!="tmn"&&CITY_ID!="kld"):?>
                            <div class="pull-left" style="margin-right:0px;">
                                <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRestoran.ru&amp;width=240&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:290px;" allowTransparency="true"></iframe>
                            </div>
                        <?endif;?>
                        <div class="clearfix"></div>
                    </div>
                </div>
    </div>
    <div class="right-side">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "inc_right_block",
                "EDIT_TEMPLATE" => ""
            ),
        false
        );?>
        <br /><br />
        <?  require_once $_SERVER["DOCUMENT_ROOT"].'/index_inc_right_block_vote.php';?>
        <?/*$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "page",
                "AREA_FILE_SUFFIX" => "inc_right_block_vote",
                "EDIT_TEMPLATE" => ""
            ),
        false
        );*/?>
    </div>
    <div class="clearfix"></div>
    <div class="i_b_r">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:advertising.banner",
            	"",
            	Array(
            		"TYPE" => "bottom_rest_list",
            		"NOINDEX" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "0"
            	),
            false
            );?>
        </div>   
</div>