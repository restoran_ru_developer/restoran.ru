$(document).ready(function() {
    setTimeout("getFilter()",10);           
    $('.filter_popup a').click(function(){        
        var id = $(this).parents('.filter_popup').attr('val');
        var code = $(this).parents('.filter_popup').attr('code');
        if ($(this).attr("code"))
            code = $(this).attr("code");
        if ($(this).hasClass("selected"))
        {
            $(this).removeClass("selected");
            $("#hid"+$(this).attr("id")).remove();
            $("#val"+$(this).attr("id")).remove();
            if ($("#new_filter_results").find(".fil").length==0)
            {
                $("#multi"+id).hide();
                $("#new_filter_results").hide();
            }
        }
        else
        {
            //$("html,body").animate({scrollTop:"300px"}, 300);
            $(this).addClass("selected");
            if (code=="breakfast")
                $(this).parents('.filter_popup').append("<input type='hidden' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"]' value='"+$(this).attr("id")+"' />");
            else
                $(this).parents('.filter_popup').append("<input type='hidden' id='hid"+$(this).attr("id")+"' name='arrFilter_pf["+code+"][]' value='"+$(this).attr("id")+"' />");
            if ($("#new_filter_results").is(":hidden"))
            $("#new_filter_results").css("display","block");

            if (!$("#new_filter_results #val"+$(this).attr("id")).attr("id"))
            {
                $('<li class="fil" id="val'+$(this).attr("id")+'" val="'+$(this).attr("id")+'">' + $(this).attr("val") + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#multi'+id);
                $('#multi'+id).show();
            }
        }    
        //if(code!="subway")
            $(".filter_popup").hide();
    });        
    
    $('#new_filter_results').on('click', 'a',function() {
        var value = $(this).parent().attr("val");
        if ($(this).parent().parent().find(".fil").length==1)
            $(this).parent().parent().hide();
        if ($(this).parent().parent().parent().find(".fil").length==1)
            $("#new_filter_results").css("display","block");
        $(this).parent().remove();       
        $("#"+value).removeClass("selected");
        $("#hid"+value).remove();
    });
    
    $("#new_filter .filter_block").hover(function(){
        $(this).addClass("filter_block_active");
    },function(){
        $(this).removeClass("filter_block_active");
    });

});
function getFilter()
{  
    $('.filter_popup input').each(function(){
        var id = $(this).parents('.filter_popup').attr('val');
        console.log(id);
        if ($("#new_filter_results").is(":hidden"))
                $("#new_filter_results").css("display","block");  
        $('<li class="fil" id="val'+$(this).attr("value")+'" val="'+$(this).attr("value")+'">' + $(this).attr("val") + '<a href="javascript:void(0)"><img src="/tpl/images/delete_filter_item.png" /></a></li>').prependTo('#new_filter_results ul#multi'+id);
        $('ul#multi'+id).show();
    });
}