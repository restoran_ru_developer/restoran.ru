// IE Safari detect ( for button fixes )
(function($) {
    var userAgent = navigator.userAgent.toString().toLowerCase();
    if ((userAgent.indexOf('safari') != -1) && !(userAgent.indexOf('chrome') != -1) && (navigator.platform=="Win32" || navigator.platform=="Win64")) {
        $("body").addClass("safari");
    }
})(jQuery);

$('a.hide').click( function(e) {
    e.preventDefault();
    if ($(this).hasClass('open')) {
        $('.tab-inner').hide();
        $('a.hide').addClass('open');
        $(this).toggleClass('open');
        $(this).parents('.tab').children('.tab-inner').show();
        if($(this).closest('.resto-detail').length > 0) {
            $(window).scrollTop($(this).closest('.container').position().top + ($(this).height() * $(this).parents('.tab').index('.container .tab')));
        }
    } else {
        $(this).toggleClass('open');
        $(this).parents('.tab').children('.tab-inner').hide();
    }
});
$('a.table-time').click( function(e) {
    e.preventDefault();
    if ($(this).hasClass('selected')) {
        return false;
    }
    else {
        $('a.table-time').removeClass('selected');
        $(this).addClass('selected');
    }
});
if($('.msg-error').length > 0) {
    $(window).scrollTop($('.msg-error').position().top-25);
}