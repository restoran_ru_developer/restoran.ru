<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?><?
if ($_REQUEST["page"])
    $_REQUEST["PAGEN_1"] = (int)$_REQUEST["page"];
    if (!$_REQUEST["PARENT_SECTION_ID"]):
        //global $DB;
        CModule::IncludeModule("iblock");
        /*$sql = "SELECT ID,NAME FROM b_iblock_element WHERE CODE='".$DB->ForSql($_REQUEST["RESTOURANT"])."' LIMIT 1";
        $res = $DB->Query($sql);
        if ($ar = $res->Fetch())
        {
            $id = $ar["ID"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
        }*/
        $arIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
        $res = CIBlockElement::GetList(Array(),Array("IBLOCK_TYPE"=>"catalog","IBLOCK_ID"=>$arIB["ID"],"CODE"=>$_REQUEST["RESTOURANT"]),false,Array("nTopCount"=>1),Array("ID","NAME","DETAIL_PAGE_URL"));
        if ($ar = $res->GetNext())
        {
            $id = $ar["ID"];
            $name = $ar["NAME"];
            $APPLICATION->SetTitle("Меню ".$ar["NAME"]);
            $url = $ar["DETAIL_PAGE_URL"];
        }
               
        /*$arFilter = Array('IBLOCK_ID'=>151, 'GLOBAL_ACTIVE'=>'Y', 'UF_RESTORAN'=>(int)$id);
        $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false);
        if($ar_result = $db_list->Fetch())
        {
            $menu = $ar_result['ID'];
        }*/
        global $DB;
        $sql = "SELECT ID FROM b_iblock WHERE DESCRIPTION=".$DB->ForSql($id);
        $db_list = $DB->Query($sql);
        if($ar_result = $db_list->GetNext())
        {
            $menu = $ar_result['ID'];
        }
        //$_REQUEST["SECTION_ID"] = false;
        //$_REQUEST["PARENT_SECTION_ID"] = false;
    endif;
    ?>
<div id="content">
    <h1 class="with_link">Меню ресторана «<a href="<?=$url?>" alt="<?=$ar["NAME"]?>" title="<?=$ar["NAME"]?>"><?=$ar["NAME"]?></a>»</h1>
<?
if ($menu)
{
    $APPLICATION->IncludeComponent("bitrix:catalog", "template1", Array(
	"IBLOCK_TYPE" => "rest_menu_ru",	// Тип инфоблока
	"IBLOCK_ID" => $menu,	// Инфоблок
	"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
	"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
	"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
	"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
	"SEF_MODE" => "N",	// Включить поддержку ЧПУ
	"SEF_FOLDER" => "/facebook/",	// Каталог ЧПУ (относительно корня сайта)
	"AJAX_MODE" => "Y",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "Y",	// Тип кеширования
	"CACHE_TIME" => "7200",	// Время кеширования (сек.)
	"CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
	"CACHE_GROUPS" => "N",	// Учитывать права доступа
	"SET_TITLE" => "N",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"USE_ELEMENT_COUNTER" => "Y",	// Использовать счетчик просмотров
	"USE_FILTER" => "N",	// Показывать фильтр
	"USE_REVIEW" => "N",	// Разрешить отзывы
	"USE_COMPARE" => "N",	// Использовать компонент сравнения
	"PRICE_CODE" => array(	// Тип цены
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
	"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
	"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
	"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
	"PRODUCT_PROPERTIES" => "",	// Характеристики товара, добавляемые в корзину
	"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
	"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
	"SHOW_TOP_ELEMENTS" => "Y",	// Выводить топ элементов
	"TOP_ELEMENT_COUNT" => "9",	// Количество выводимых элементов
	"TOP_LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке таблицы
	"TOP_ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем товары в разделе
	"TOP_ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки товаров в разделе
	"TOP_PROPERTY_CODE" => array(	// Свойства
		0 => "",
		1 => "map",
		2 => "",
	),
	"SECTION_COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
	"SECTION_TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
	"PAGE_ELEMENT_COUNT" => "12",	// Количество элементов на странице
	"LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке таблицы
	"ELEMENT_SORT_FIELD" => "id",	// По какому полю сортируем товары в разделе
	"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки товаров в разделе
	"LIST_PROPERTY_CODE" => array(	// Свойства
		0 => "",
		1 => "map",
		2 => "",
	),
	"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
	"LIST_META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства раздела
	"LIST_META_DESCRIPTION" => "-",	// Установить описание страницы из свойства раздела
	"LIST_BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства раздела
	"DETAIL_PROPERTY_CODE" => array(	// Свойства
		0 => "",
		1 => "map",
		2 => "",
	),
	"DETAIL_META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"DETAIL_META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"DETAIL_BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"LINK_IBLOCK_TYPE" => "",	// Тип инфоблока, элементы которого связаны с текущим элементом
	"LINK_IBLOCK_ID" => "",	// ID инфоблока, элементы которого связаны с текущим элементом
	"LINK_PROPERTY_SID" => "",	// Свойство, в котором хранится связь
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL на страницу, где будет показан список связанных элементов
	"USE_ALSO_BUY" => "N",	// Показывать блок "С этим товаром покупают"
	"USE_STORE" => "N",	// Показывать блок "Количество товара на складе"
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
	"PAGER_TITLE" => "Товары",	// Название категорий
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "",	// Название шаблона
	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	"VARIABLE_ALIASES" => array(
		"SECTION_ID" => "SECTION_ID",
		"ELEMENT_ID" => "ELEMENT_ID",
	)
	),
	false
);
}
else
{
    ShowError("Меню не найдено");
}?>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>