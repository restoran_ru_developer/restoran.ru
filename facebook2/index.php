<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle(""); 
if (!$_REQUEST["CITY_ID"])
    $_REQUEST["CITY_ID"] = CITY_ID;
$arRestIB = getArIblock("catalog", $_REQUEST["CITY_ID"]);
$name = (int)$_REQUEST["ID"];
if ($name)
{
    CModule::IncludeModule("iblock");
    $res = CIBlockElement::GetList(Array(),Array("ID"=>$name,"IBLOCK_ID"=>$arRestIB["ID"]));
    if ($ar = $res->Fetch())
    {
        global $rest2;
        $rest2 = $ar["NAME"];
    }
    $db_props = CIBlockElement::GetProperty($arRestIB["ID"], $rest["ID"], array("sort" => "asc"), Array("CODE"=>"address"));
    if($ar_props = $db_props->Fetch())
        $adres = $ar_props["VALUE"];
}
?>
<script>
    $(document).ready(function(){
        $("#form_checkbox_smokeb").change(function(){
            $("#typezal").html("Курящий зал");
        });
        $("#form_checkbox_smokea").change(function(){
            $("#typezal").html("Некурящий зал");
        });
        $("#ptime").change(function(){
            $("#ptime2").html($("#ptime").val());
        });
        $("#pnum").change(function(){
            $("#pnum2").html($("#pnum").val());
        });
    });
</script>
<div id="container">
			<div id="content">
<div class="opinions">

<div id="title">Ресторан</div>

<div class="text">
<div class="name"><?=$rest2?></div>
<div class="address"><?=$adres?></div>
</div>


<div class="text2">
<b id="pdate"><?=($_REQUEST["form_date_33"])?$_REQUEST["form_date_33"]:date("d.m.Y")?></b><br />
<span>Время:</span> <b id="ptime2"><?=($_REQUEST["form_text_34"])?$_REQUEST["form_text_34"]:"19:00"?></b><br />
<span>Кол-во человек:</span> <b id="pnum2"><?=($_REQUEST["form_text_35"])?$_REQUEST["form_text_35"]:"2"?></b><br />
<b id="typezal">
    <?
    if ($_REQUEST["form_checkbox_smoke"]==66)
        echo "Курящий зал";
    else
        echo "Некурящий зал";
    ?>
    </b><br />
</div>


</div>

			</div><!-- #content-->
		</div><!-- #container-->


<aside id="sideRight" class="order-right">
			<div class="order-right-block" style="padding-top:30px;">                            
				<h2 align="center">Бронирование столика</h2>
				<?       
    $APPLICATION->IncludeComponent(
        "restoran:form.result.new", "facebook2", Array(
        "SEF_MODE" => "N",
        "WEB_FORM_ID" => "3",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "CACHE_TYPE" => "N",
        //"CACHE_TIME" => "3600",
        "USE_CAPTCHA" => "Y", // Использовать CAPTCHA
        "VARIABLE_ALIASES" => Array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID"
        )
            ), false
    );?>
			</div>
		</aside><!-- #sideRight -->
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>